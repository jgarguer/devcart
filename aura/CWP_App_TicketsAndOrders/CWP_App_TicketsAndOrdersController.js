({
    showModal: function(component, event, helper) {
        var option = event.currentTarget.value;
        if (option !== "") {
            var key = 'v.' + option;
            component.set(key, true);
            var toggleBackdrop = component.find("backdrop");
            $A.util.removeClass(toggleBackdrop, "toggle");
        }

    },

    hideModal: function(component, event, helper) {
        var option = event.currentTarget.value;
        if (option !== "") {
            var key = 'v.' + option;
            component.set(key, false);
            var toggleBackdrop = component.find("backdrop");
            $A.util.addClass(toggleBackdrop, "toggle");
        }
    },
    
    backModal:function(component, event, helper) {
    	var toggleBackdrop = component.find("backdrop");
       $A.util.addClass(toggleBackdrop, "toggle");
       component.set('v.incident', false);
       component.set('v.change', false);
       component.set('v.billingInquiry', false);
       component.set('v.query', false);
       component.set('v.complaint', false);
       component.set('v.creation', false);
       component.set('v.termination', false);
       
    }
})