({
    doInit : function(cmp, event, helper) {

        //GET OPPTY ID FROM URL, EN LEX SE PASARA POR PARAMETRO
        var optyId = window.location.href;
        var i = optyId.indexOf('xxxx');
        optyId = optyId.substring(i+5);
        if (cmp.get('v.opportunityId') != null ) {
            optyId = cmp.get('v.opportunityId');
        }
        var commID = window.location.href;
        if (commID.indexOf('isComm') > 0){            
            i = commID.indexOf('isComm');            
            var communityName = commID.substring(i+7);
            var j = communityName.indexOf('XsxSx');
            communityName = communityName.substring(0,j);
            cmp.set('v.community', communityName);
            cmp.set('v.isCommunity', true);
        }

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        var stringToday = yyyy+'-'+mm+'-'+dd;

        cmp.set('v.fechaRealCierre', stringToday);

        console.log(optyId);
        
        var action = cmp.get("c.getOpportunity");
        action.setParams({
            optyId: optyId
        });
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                console.log(response.getReturnValue());
                var o = response.getReturnValue();
                console.log('action o ' + o.Name);
                console.log('action o ' + o.StageName);
                console.log('action o ' + o.AccountId);
                console.log('action o ' + o.VE_Express__c);

                cmp.set('v.opportunity', o);
                
                if ( o.StageName == 'F6 - Prospecting') {
                    console.log('ETAPA OPP: ' + o.StageName);
                    var messages = cmp.get('v.messages');
                    cmp.set('v.isNotPreoportunidad', false);
                }   
                else{

                    console.log($A.get("$Locale.language"));
                    console.log('ETAPA REAL: ' + o.StageName + ' ESPERADA: F6 - Prospecting ');//+ $A.get("$Label.c.VE_F6Preoportunidad"));
                	if ( o.StageName != 'F4 - Offer Development' && o.StageName != 'F3 - Offer Presented' && o.StageName != 'F2 - Negotiation'){
                    	var messages = cmp.get('v.messages');
                        messages.push($A.get("$Label.c.VE_Etapa_no_valida"));
                        cmp.set('v.messages', messages);
                        var bac = cmp.find('bac');
                        $A.util.addClass(bac,'slds-hide'); 
                        }
                    }
            }
            else{
                console.log(response);
                alert(response.getState());
            }
        });
        $A.enqueueAction(action);
        
        var action2 = cmp.get("c.stageValues");
        action2.setCallback(this, function(response) {
                var o = cmp.get('v.opportunity');
                console.log('action 2 ' + o.Name);
                console.log('action 2 ' + o.StageName);
                console.log('action 2 ' + o.AccountId);
                console.log('action 2 ' + o.VE_Express__c);

            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var res = response.getReturnValue();
                console.log('action 2 res' + response.getReturnValue().getName);
                var mapa = new Map();
                for(var i = 0; i<res.length;i++){
                    mapa.set(res[i].value, res[i].label);
                }
                cmp.set("v.stageValues", res);
                var stage = cmp.get("v.opportunity").StageName;
                cmp.set("v.actualOppStage", mapa.get(stage));
            }
        });
        $A.enqueueAction(action2);
        
        var action3 = cmp.get("c.getOppLostReason");
        var stage = $A.get("$Label.c.VE_F1CanceladaSusp");
        action3.setParams({
                    stage: stage
                });
        var inputsel = cmp.find("lostreason");
    	var opts=[];
        
        action3.setCallback(this, function(response) {
            for(var i=0; i< response.getReturnValue().length; i++) {
                opts.push({class: "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
            }
            inputsel.set("v.options", opts);
                            var o = cmp.get('v.opportunity');
                console.log('action3 ' + o.Name);
                console.log('action3 ' + o.StageName);
                console.log('action3 ' + o.AccountId);
                console.log('action3 ' + o.VE_Express__c);
        });
        $A.enqueueAction(action3); 
        
        
        var action5 = cmp.get("c.getF1Stages");
        var inputsel2 = cmp.find("levels");
        var opts2=[];
        
        action5.setCallback(this, function(response) {
            for(var i=0; i< response.getReturnValue().length; i++) {
            	opts2.push({class: "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
        	}

                var o = cmp.get('v.opportunity');
                console.log('action5 ' + o.Name);
                console.log('action5 ' + o.StageName);
                console.log('action5 ' + o.AccountId);
                console.log('action5 ' + o.VE_Express__c);

        	inputsel2.set("v.options", opts2);
        });
        $A.enqueueAction(action5); 
        
    },

    startProccess: function(cmp, evt, helper){
                var o = cmp.get('v.opportunity');
                console.log('startProccess ' + o.Name);
                console.log('startProccess ' + o.StageName);
                console.log('startProccess ' + o.AccountId);
                console.log('startProccess ' + o.VE_Express__c);
        var isNotPreoportunidad = cmp.get('v.isNotPreoportunidad');

        if ( isNotPreoportunidad == false){
            var tomarOpty = cmp.get('v.tomarOpty');
            var o = cmp.get('v.opportunity');
                        
            if ( tomarOpty == 'Rechazar' && !o.BI_Motivo_de_perdida__c){
                var messages = cmp.get('v.messages');
                messages.push('Debes seleccionar un motivo de pérdida.');
                cmp.set('v.messages', messages);
            }
            else{
                var bac = cmp.find('bac');
                $A.util.addClass(bac,'slds-hide');

                var action = cmp.get("c.takeOpportunity");
                action.setParams({
                    o: o,
                    tipoCierre: tomarOpty
                });
                action.setCallback(this, function(response) {
                    if (cmp.isValid() && response.getState() === "SUCCESS") {
                        var fo = JSON.parse( response.getReturnValue() );
                        console.log(fo);

                        if ( fo.o == null ){
                            var cmpTarget = cmp.find('bclose');
                            $A.util.removeClass(cmpTarget, 'slds-hide'); 
                            $A.util.addClass(cmpTarget, 'slds-button slds-button--neutral');
                            
                            var messages = cmp.get('v.messages');
                            var errorMsg = fo.m;                          

                            var i = errorMsg.indexOf('TION,');
                            errorMsg = errorMsg.substring(i+6);                            

                            i = errorMsg.indexOf(':');
                            errorMsg = errorMsg.substring(0,i);                            

                            var find = '&quot;';
                            var re = new RegExp(find, 'g');

                            errorMsg = errorMsg.replace(re, '');                    

                            messages.push('No se ha podido avanzar la oportunidad: '+errorMsg);
                            cmp.set('v.messages', messages);
                        }
                        else{
                            var oo = cmp.get('v.opportunity');
                            oo.StageName = fo.o.StageName;
                            cmp.set('v.opportunity', oo);
                            var messages = cmp.get('v.messages');
                            messages.push(fo.o.StageName);
                            cmp.set('v.messages', messages);
                            cmp.set('v.show', true); //OAJF: Fix eHelp 03127668
                        }
                    }                    
                    else{
                        console.log(response);
                        alert(response.getState());
                    }
                });
                $A.enqueueAction(action);
            }
        }
        else{

            var cmpTarget = cmp.find('bac');                
            var o = cmp.get('v.opportunity');

            var fv = cmp.get('v.fechaRealCierre');
            console.log('startProccess fechaRealCierre ' + fv);

            console.log('startProccess oportunidad Id ' + o.Id);
            console.log('startProccess oportunidad Stage ' + o.StageName);
            var doit = true;

            if ( fv == null || fv.length==0 ){  
                doit=false;              
                var feccierrediv = cmp.find('feccierrediv');
                var feccierremsg = cmp.find('feccierremsg');

                $A.util.addClass(feccierrediv,'slds-has-error');
                $A.util.removeClass(feccierremsg,'slds-hide');
            }

            if ( (cmp.get('v.tipoCierre')==$A.get("$Label.c.VE_F1ClosedLost") || cmp.get('v.tipoCierre')==$A.get("$Label.c.VE_F1CanceladaSusp")) && (!o.BI_Motivo_de_perdida__c || o.BI_Motivo_de_perdida__c.length==0 ) ){
                doit=false;
                var reasondiv = cmp.find('reasondiv');
                var reasonmsg = cmp.find('reasonmsg');

                $A.util.addClass(reasondiv,'slds-has-error');
                $A.util.removeClass(reasonmsg,'slds-hide');
            }   

            if (doit){     
                $A.util.addClass(cmpTarget, 'slds-hide');
                helper.forwardOppty(cmp, helper);                        
            }

        }
       
    },
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner,'slds-hide');
    },
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner,'slds-hide');   
    },
    cerrarVentana: function(cmp, evt, helper){
        var o = cmp.get('v.opportunity');
        var isCommunity = cmp.get('v.isCommunity');
        var url = '/'+o.Id;
        if ( isCommunity ){
            var community = cmp.get('v.community'); 
            url = '/'+community+'/'+o.Id;
        }
//Manuel Medina 2016-07-24 changed due to LockService
//       window.opener.location = url;                 
        if (cmp.get('v.opportunityId') == null) {
        	window.location.assign(url);
        } else { //coming from SF1
            sforce.one.navigateToURL(url);
        }
    },

    //Define si muestra o no el botón, en funcion del StageName, si es F1 lo muestra
    mostrarBoton: function(cmp, evt, helper){       
        console.log(evt);
        
        console.log('XXXXX'+evt.getParams().value.StageName);
        if (evt.getParams().value.StageName=='F1 - Closed Won' 
            || evt.getParams().value.StageName=='F1 - Closed Lost' 
            || evt.getParams().value.StageName=='F1 - Cancelled | Suspended'){
            var cmpTarget = cmp.find('bclose'); //Busca el boton bclose
            $A.util.removeClass(cmpTarget, 'slds-hide'); 
            $A.util.addClass(cmpTarget, 'slds-button slds-button--neutral');
        }
        
    },
    onGroup: function(cmp, evt) {
         var elem = evt.getSource().getElement();
         console.log(elem);
         var selected = elem.textContent;         
         cmp.set("v.tipoCierre", selected);
    },
    tipoSelected: function(cmp, evt, helper){
        var selected = cmp.find("levels").get("v.value");        
        cmp.set("v.tipoCierre", selected);
        console.log(selected);
        var reasondiv = cmp.find('reasondiv');

        if ( selected == $A.get("$Label.c.VE_F1ClosedLost") || selected == $A.get("$Label.c.VE_F1CanceladaSusp")){
            $A.util.removeClass(reasondiv, 'slds-hide');
    
            var action4 = cmp.get("c.getOppLostReason");
            //var stage = cmp.get("v.opportunity").StageName;
            //var stage = 'F1 - Perdida';
            action4.setParams({
                        stage: selected
                    });
            var inputsel = cmp.find("lostreason");
            var opts=[];
            
            action4.setCallback(this, function(response) {
                for(var i=0; i< response.getReturnValue().length; i++) {
                    opts.push({class: "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
                }
                inputsel.set("v.options", opts);
            });
            $A.enqueueAction(action4); 
            
        }
        else{
            $A.util.addClass(reasondiv, 'slds-hide');
        }
    },
    tipoSelected2: function(cmp, evt, helper){
        var selected = cmp.find("levels2").get("v.value");             
        cmp.set("v.tomarOpty", selected);
        var reasondiv = cmp.find('reasondiv');        

        if ( selected == 'Rechazar'){
            $A.util.removeClass(reasondiv, 'slds-hide');
        }
        else{
            $A.util.addClass(reasondiv, 'slds-hide');
        }
    },
    reasonChanged: function(cmp, evt, helper){
        var selected = cmp.find("lostreason").get("v.value"); 
        var opty = cmp.get('v.opportunity');
        opty.BI_Motivo_de_perdida__c=selected;
        cmp.set('v.opportunity',opty);
    }

})