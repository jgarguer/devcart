({
    MAX_FILE_SIZE: 4500000,
    /* 6 000 000 * 3/4 to account for base64 */
    CHUNK_SIZE: 450000,
    /* Use a multiple of 4 */
    total_files:0,
    
    uploaded_files:0,
    
    save: function(component, caso, mode) {
        debugger;
        var adjuntos= component.get("v.listaAdjuntos");
        if(mode == 'inci'){
            adjuntos= component.get("v.listaAdjuntosInci");
        }
        
        if(adjuntos.length >0){
            total_files=adjuntos.length;
            NProgress.start();
            for (var i=0; adjuntos.length > i; i++) {
                
                this.savefile(adjuntos[i], caso, component);
            }
        }else{
            document.getElementById("backgroundmodalwait").style.display='none';
        }
    },
    
    
    uploadChunk: function(file, fileContents, fromPos, toPos, attachId, caso, component) {
        var action = component.get("c.saveTheChunk");
        var chunk = fileContents.substring(fromPos, toPos);
        
        action.setParams({
            fileName: file.name,
            fileDescription: file.desc,
            base64Data: encodeURIComponent(chunk),
            contentType: file.type,
            fileId: attachId,
            caso: caso
        });
        
        var self = this;
        action.setCallback(this, function(a) {
            attachId = a.getReturnValue();
            
            fromPos = toPos;
            toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);
            
            if (fromPos < toPos) {
                self.uploadChunk(file, fileContents, fromPos, toPos, attachId, caso, component);
            }else{
                //alert('call integración'+attachId);
                var actionI = component.get("c.integraWorkInfo");
                actionI.setParams({attachId:attachId});
                actionI.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        // alert('lanzado');
                    }
                });
                $A.enqueueAction(actionI);
                self.uploaded_files = self.uploaded_files + 1;
                if(self.uploaded_files == total_files){
                    document.getElementById("backgroundmodalwait").style.display='none';
                    NProgress.done();
                    self.uploaded_files=0;
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    savefile: function(file, caso, component) {
        if (file.size > this.MAX_FILE_SIZE) {
            alert('File size cannot exceed ' + this.MAX_FILE_SIZE + ' bytes.\n' +
                  'Selected file size: ' + file.size);
            NProgress.done();
            return;
        }
        
        var fr = new FileReader();
        
        var self = this;
        fr.onload = $A.getCallback(function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            
            fileContents = fileContents.substring(dataStart);
            
            var fromPos = 0;
            var toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);
            
            // start with the initial chunk
            self.uploadChunk(file, fileContents, fromPos, toPos, '', caso, component);
            
        });
        fr.readAsDataURL(file);
    }
})