({
    updateDetails : function(component, event) {
        document.getElementById("backgroundmodalwait").style.display='';
        document.getElementById('backgroundmodal').style.display='block';
        var id = event.getParam("OiId");                  
        component.set("v.orderId", id);
        
        var ind= event.getParam("index");
        if(ind==0){
            document.getElementById("cwp-prevItembtn").classList.add("tlf-btn--disabled");
            document.getElementById("cwp-prevItembtn").disabled=true;
        }else {
            document.getElementById("cwp-prevItembtn").classList.remove("tlf-btn--disabled");
            document.getElementById("cwp-prevItembtn").disabled=false;
        }
        if(ind==(event.getParam("numRecords")-1)){
            document.getElementById("cwp-nextItembtn").classList.add("tlf-btn--disabled");
            document.getElementById("cwp-nextItembtn").disabled=true;
        }else {
            document.getElementById("cwp-nextItembtn").classList.remove("tlf-btn--disabled");
            document.getElementById("cwp-nextItembtn").disabled=false;
        }
        
        component.set("v.indexElem", event.getParam("index"));
        component.set("v.title", "main Attributes");
        var action = component.get("c.getOrderItem");
        action.setParams({ "id" : id });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.item", response.getReturnValue());
                
            }
        });
        var action2 = component.get("c.getAccountHierarchy");
        action2.setParams({ "id" : id });
        action2.setCallback(this, function(response){
            var state = response.getState();	
            if (state === "SUCCESS") {
                component.set("v.itemAccount", response.getReturnValue());
            }
        });
        var action3 = component.get("c.getDetailsAttributes");
        action3.setParams({ "id" : id });
        action3.setCallback(this, function(response){
            var state = response.getState();	
            if (state === "SUCCESS") {
                debugger;
                var a =response.getReturnValue();
                component.set("v.showOi", false);
                component.set("v.OisDetail", a);
                component.set("v.titlehead", component.get("v.item").NE__ProdName__c+' '+ $A.get("$Label.c.CWP_DetailAttributes"));
            }
        });
        var action4 = component.get("c.hasSecondLevel");
        action4.setParams({ "id" : id });
        action4.setCallback(this, function(response){
            var state = response.getState();	
            if (state === "SUCCESS") {
                component.set("v.hasSecondLevel", response.getReturnValue());
            }
        });
        var action5 = component.get("c.getProductsNames");
        action5.setParams({ "id" : id });
        action5.setCallback(this, function(response){
            var state = response.getState();	
            if (state === "SUCCESS") {
                var ret = [];
                var aux = response.getReturnValue();
                var index = 0;
                for (var i = 0 ; i < aux.length ; i++){
                    if (i > 0){
                        if (aux[i].TGS_Product_Name__c == ret[index-1].TGS_Product_Name__c){
                            ret[index-1].Name = aux[i].Name +','+ ret[index-1].Name;
                        }else{
                            ret[index]=aux[i];
                            index ++;
                        }
                    }else{
                        ret[index]=aux[i];
                        index ++;
                    }
                }                                                
                component.set("v.itemNamesLevel2", ret);
            }
        });
        var action6 = component.get("c.showNewIncidenceButton");
        component.get("v.item");
        action6.setParams({ "id" : id });
        action6.setCallback(this, function(response){
            debugger;
            var state = response.getState();	
            if (state === "SUCCESS") {
                debugger;
                component.set("v.showIncidenceButton", response.getReturnValue());
            }
        });
        
        var action7 = component.get("c.showNewModificationButton");
        component.get("v.item");
        action7.setParams({ "id" : id });
        action7.setCallback(this, function(response){
            debugger;
            var state = response.getState();	
            if (state === "SUCCESS") {
                component.set("v.showModificationButton", response.getReturnValue());
                document.getElementById("backgroundmodalwait").style.display='none';
                document.getElementById("tlf-detail").style.display='block';
                document.getElementById("tlf-detail").classList.add('in');
                document.getElementsByTagName("body")[0].classList.add('modal-open');
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
                document.getElementById("tlf-detail").style.display='block';
                document.getElementById("tlf-detail").classList.add('in');
                document.getElementsByTagName("body")[0].classList.add('modal-open');
            }
        });
        
        $A.enqueueAction(action);
        $A.enqueueAction(action2);
        $A.enqueueAction(action3);
        $A.enqueueAction(action4);
        $A.enqueueAction(action5);
        $A.enqueueAction(action6);
        $A.enqueueAction(action7);
    },
    secondAttrs : function(component, event){
        document.getElementById("backgroundmodalwait").style.display='';
        component.set("v.titlehead", $A.get("$Label.c.CWP_SecondLevelAtt"));
        var action3 = component.get("c.getDetailsAttributes2");
        var id= event.currentTarget.id;
        id=id.replace("s2---", "");
        action3.setParams({ "id" : id });
        var aux2 = 'OrderId';
        component.set("v.display", 'display:none');
        action3.setCallback(this, function(response){
            var state = response.getState();	
            if (state === "SUCCESS") {
                var a =response.getReturnValue();
                component.set("v.OisDetail", a);
                component.set("v.showOi", true);
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action3);
        
    },
    secondAttrsDetails : function(component, event){
        debugger;
        document.getElementById("backgroundmodalwait").style.display='';
        component.set("v.titlehead", event.currentTarget.text);
        var action3 = component.get("c.getDetailsAttributes2Detailed");
        var aux2 = event.currentTarget.text;
        component.set("v.title", aux2 + ' OrderId');
        component.set("v.display", 'display:none');
        var id = event.currentTarget.id;
        action3.setParams({ "id" : id });
        action3.setCallback(this, function(response){
            var state = response.getState();	
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                component.set("v.OisDetail",data);
                component.set("v.showOi", true);
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }	
        });
        $A.enqueueAction(action3);
    },
    updateMainDetails : function(component, event){
        debugger;
        document.getElementById("backgroundmodalwait").style.display='';
        var id = event.currentTarget.id;
        id=id.replace("s1---", "");
        var action3 = component.get("c.getDetailsAttributes");
        action3.setParams({ "id" : id });
        component.set("v.title", "main Attributes");
        component.set("v.display", 'display:none');
        action3.setCallback(this, function(response){
            var state = response.getState();	
            if (state === "SUCCESS") {
                var a =response.getReturnValue();
                component.set("v.showOi", false);
                component.set("v.OisDetail", a);
                component.set("v.titlehead", component.get("v.item").NE__ProdName__c+' '+ $A.get("$Label.c.CWP_DetailAttributes"));
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action3);
    },
    selectedAttrib : function(component, event){
        debugger;
        var id = event.currentTarget.selectedOptions["0"].id;
        var index = event.currentTarget.selectedOptions["0"].index;
        var text = event.currentTarget.selectedOptions["0"].text;
        
        if(index==0){
            document.getElementById("s1---"+id).click(); 
        }else if (index==1){
            document.getElementById("s2---"+id).click();
        }else{  
            document.getElementById(id).click();
        }
    },    
    closeModal : function(component, event, helper) {
        document.getElementById('backgroundmodal').style.display='none';
        document.getElementById("tlf-incident").style.display='none';
        document.getElementById("tlf-incident").classList.remove('in');
        document.getElementById('tlf-detail').style.display="block";
        document.getElementById('tlf-detail').classList.add('in');
    },
    closeModalDetail : function(component, event, helper) {
        document.getElementById('backgroundmodal').style.display='none';
        document.getElementById('tlf-detail').style.display="none";
        document.getElementById('tlf-detail').classList.remove('in');
        document.getElementsByTagName('modal-open').classList.remove('modal-open');
    },
    showBackground : function(component, event, helper) {
        document.getElementById('backgroundmodal').style.display='block';
    },
    showSpinner : function (component, event, helper) {
        document.getElementById("backgroundmodalwait").style.display='';
    },
    hideSpinner : function (component, event, helper) {
        document.getElementById("backgroundmodalwait").style.display='none';
    },
    onFocusInc : function(component, event, helper){
        document.getElementById("subject-incident").style.border="";
        document.getElementById("subject-incident").style.color="#16325c"; 
        document.getElementById("description-incident").style.border="";
        document.getElementById("description-incident").style.color="#16325c";  
    },
    onFocusMod : function(component, event, helper){
        document.getElementById('description-modification').style.border="";
        document.getElementById('description-modification').style.color="#16325c";  
    },
    creaInc : function(component, event, helper) {
        debugger;
        
        var orderId=component.get("v.item");
        var subject= document.getElementById('subject-incident').value;
        var description= document.getElementById('description-incident').value;
        
        
        if(subject==''){
            document.getElementById("subject-incident").style.border="solid 2px red"; 
            document.getElementById("subject-incident").style.color="red"; 
        }
        if(description==''){
            document.getElementById("description-incident").style.border="solid 2px red"; 
            document.getElementById("description-incident").style.color="red"; 
        }
        if(subject!='' && description!='' ){
            document.getElementsByTagName('body')[0].classList.remove("modal-open");
            document.getElementById("backgroundmodalwait").style.display='';
            document.getElementById("subject-incident").style.border="";
            document.getElementById("subject-incident").style.color="#16325c"; 
            document.getElementById("description-incident").style.border="";
            document.getElementById("description-incident").style.color="#16325c"; 
            var e = document.getElementById("impact-incident");
            var impact = e.options[e.selectedIndex].value;
            e = document.getElementById("urgency-incident");
            var urgency= e.options[e.selectedIndex].value;
            var lParam=[orderId.Id,subject,description,impact,urgency];
            
            var action = component.get("c.createCaseInc");
            document.getElementById("tlf-incident").style.display='none';
            document.getElementById("tlf-incident").classList.remove('in');
            action.setParams({ "params" : lParam });
            action.setCallback(this, function(response){
                var state = response.getState();	
                if (state === "SUCCESS") {
                    debugger;
                    var caso = response.getReturnValue();
                    //alert('incidencia creada '+caso);
                    // 
                    var actionInt = component.get("c.integraCaseInc");
                    
                    actionInt.setParams({ "cNumber" : caso });
                    actionInt.setCallback(this, function(response){
                        var state = response.getState();	
                        if (state === "SUCCESS") {
                            debugger;
                            var caso = response.getReturnValue();
                            // alert('incidencia creada '+caso);
                            var txt=$A.get("$Label.c.CWP_IncidentCreated");
                            txt+=": ";
                            txt+= caso;
                            component.set("v.textIncidencia",txt);
                            document.getElementById("tlf-alert-file-incident").style.display='block';
                            document.getElementById("tlf-alert-file-incident").classList.add("in");
                            helper.save(component, caso, 'inci');
                            
                            document.getElementsByTagName('body')[0].classList.add("modal-open");
                        }
                        
                    });
                    $A.enqueueAction(actionInt); 
                }
                else {
                    var txt=$A.get("$Label.c.CWP_IncidentError");
                    component.set("v.textIncidencia",txt);
                    var a= document.getElementById("btnAlertInc");
                    a.click();
                    document.getElementById("backgroundmodalwait").style.display='none';
                    document.getElementById("tlf-incident").style.display='block';
                    document.getElementById("tlf-incident").classList.add('in');                    
                }
            });
            $A.enqueueAction(action);   
        }
        
    },
    openModificationtModal: function(component, event, helper) {
        document.getElementById('description-modification').value="";
        document.getElementById('description-modification').style.border="";
        document.getElementById('description-modification').style.color="#16325c"; 
        document.getElementById('tlf-modification').style.display='block';
        document.getElementById('tlf-modification').classList.add('in');
        document.getElementById('tlf-detail').style.display="none";
        document.getElementById('tlf-detail').classList.remove('in');
        component.set("v.listaAdjuntos",[]);
        window.scrollTo( 0, 0 );
        
    },
    openIncidentModal : function(component, event, helper) {
        document.getElementById('tlf-detail').style.display="none";
        document.getElementById('tlf-detail').classList.remove('in');
        document.getElementById("tlf-incident").style.display='block';
        document.getElementById("tlf-incident").classList.add('in');
        document.getElementById("backgroundmodalwait").style.display='';
        document.getElementById("subject-incident").style.border="";
        document.getElementById("subject-incident").style.color="#16325c"; 
        document.getElementById("description-incident").style.border="";
        document.getElementById("description-incident").style.color="#16325c"; 
        document.getElementById('subject-incident').value="";
        document.getElementById('description-incident').value="";
        document.getElementById("urgency-incident").selectedIndex=0;
        document.getElementById("impact-incident").selectedIndex=0;
        component.set("v.listaAdjuntosInci",[]);
        var action = component.get("c.getPiciklistIncidence");
        action.setCallback(this, function(response){
            var state = response.getState();	
            if (state === "SUCCESS") {
                var data = JSON.parse(response.getReturnValue());
                
                component.set("v.incidentImpactListItems", data[0]);
                component.set("v.incidentUrgencyListItems", data[1]);
                document.getElementById("backgroundmodalwait").style.display='none';
            }	
        });
        $A.enqueueAction(action);           
        window.scrollTo( 0, 0 );
    },
    crearMod : function(component, event, helper) {
        debugger;
        var orderId=component.get("v.item");
        var description= document.getElementById('description-modification').value;
        if(description==''){
            document.getElementById("description-modification").style.border="solid 2px red"; 
            document.getElementById("description-modification").style.color="red"; 
        }else {
            document.getElementById('tlf-modification').style.display="none";
            document.getElementsByTagName('body')[0].classList.remove("modal-open");
            document.getElementById("backgroundmodalwait").style.display='';
            var lParam=[orderId.Id,description];
            var action = component.get("c.createCaseMod");
            action.setParams({ "params" : lParam });
            action.setCallback(this, function(response){
                var state = response.getState();	
                if (state === "SUCCESS") {
                    var caso = response.getReturnValue();
                    var txt=$A.get("$Label.c.CWP_ModificationCreated");
                    txt+=": ";
                    txt+= caso;
                    component.set("v.textIncidencia",txt);
                    var a= document.getElementById("btnAlertMod");
                    a.click();
                    
                    helper.save(component, caso, 'modf');
                    document.getElementById("backgroundmodalwait").style.display='none';
                    document.getElementsByTagName('body')[0].classList.add("modal-open");
                    
                } else {
                    var txt=$A.get("$Label.c.CWP_IncidentError");
                    component.set("v.textIncidencia",txt);
                    var a= document.getElementById("btnAlertMod");
                    a.click();
                    document.getElementById('tlf-modification').style.display="block";
                    document.getElementById("backgroundmodalwait").style.display='none';
                    document.getElementsByTagName('body')[0].classList.add("modal-open");
                    
                }   
            });
            $A.enqueueAction(action);           
        }
    },
    addAtachmentModif : function(component, event, helper) {
        
        
        var listaAdjuntos = component.get("v.listaAdjuntos");
        var adjunto= document.getElementById("file-attach-Modif").files;
        
        if(document.getElementById("description-file-mod").value == ''){
            document.getElementById("description-file-mod").style.border="solid 2px red"; 
            document.getElementById("description-file-mod").style.color="red"; 
            return;
        }
        if(adjunto.length >0){
            adjunto[0].desc=document.getElementById("description-file-mod").value;
            listaAdjuntos.push(adjunto[0]);
        }
        document.getElementById('tlf-atachment-file-modification').style.display="none";
        document.getElementById("description-file-mod").value='';
        document.getElementById('file-attach-Modif').value='';
        document.getElementById("description-file-mod").style.border=""; 
        document.getElementById("description-file-mod").style.color="#16325c"; 
        component.set("v.listaAdjuntos",listaAdjuntos);
        document.getElementById('tlf-atachment-file-modification').style.display="none";
        document.getElementById('tlf-atachment-file-modification').classList.remove("in");
        document.getElementById('file-attach-Modif').value='';
        document.getElementById('tlf-modification').style.display='block';
        document.getElementById('tlf-modification').classList.add('in');
    },
    removeAtachmentModif : function(component, event, helper) {
        var file = event.currentTarget.id; 
        var listaAdjuntos = component.get("v.listaAdjuntos");
        
        for(var i = listaAdjuntos.length - 1; i >= 0; i--) {
            if(listaAdjuntos[i].name === file) {
                listaAdjuntos.splice(i, 1);
            }
        }
        component.set("v.listaAdjuntos",listaAdjuntos);
    },
    addAtachmentInci : function(component, event, helper) {
        var listaAdjuntos = component.get("v.listaAdjuntosInci");
        var adjunto= document.getElementById("file-attach-Inci").files;
        
        if(document.getElementById("description-file-Inc").value == ''){
            document.getElementById("description-file-Inc").style.border="solid 2px red"; 
            document.getElementById("description-file-Inc").style.color="red"; 
            return;
        }
        if(adjunto.length >0){
            adjunto[0].desc=document.getElementById("description-file-Inc").value;
            listaAdjuntos.push(adjunto[0]);
        }
        document.getElementById("description-file-Inc").style.border=""; 
        document.getElementById("description-file-Inc").style.color="#16325c"; 
        component.set("v.listaAdjuntosInci",listaAdjuntos);
        
        document.getElementById('tlf-atachment-file-incident').style.display="none";
        document.getElementById('tlf-atachment-file-incident').classList.remove("in");
        document.getElementById('file-attach-Inci').value='';
        document.getElementById('tlf-incident').style.display='block';
        document.getElementById('tlf-incident').classList.add('in');
        window.scrollTo( 0, 0 );
    },
    removeAttachmentInci : function(component, event, helper) {
        var file = event.currentTarget.id; 
        var listaAdjuntos = component.get("v.listaAdjuntosInci");
        
        for(var i = listaAdjuntos.length - 1; i >= 0; i--) {
            if(listaAdjuntos[i].name === file) {
                listaAdjuntos.splice(i, 1);
            }
        }
        component.set("v.listaAdjuntosInci",listaAdjuntos);
    },  
    closeModalIncAlert : function(component, event, helper) {
        document.getElementById('tlf-incident').style.display="none";
        document.getElementById('tlf-alert-file-incident').style.display="none";
        document.getElementById('backgroundmodal').style.display="none";
        document.getElementsByTagName('body')[0].classList.remove("modal-open");
        window.scrollTo( 0, 0 );
        
    },
    closeModalModAlert : function(component, event, helper) {
        document.getElementById('tlf-modification').style.display="none";
        document.getElementById('tlf-alert-file-modification').style.display="none";
        document.getElementById('backgroundmodal').style.display="none";
        document.getElementsByTagName('body')[0].classList.remove("modal-open");
        window.scrollTo( 0, 0 );
        
    },
    closeModalInci : function(component, event, helper) {
        document.getElementById('tlf-atachment-file-incident').style.display="none";
        document.getElementById('file-attach-Inci').value='';
        document.getElementById("description-file-Inc").value=''; 
        window.scrollTo( 0, 0 );
    }, 
    closeModalModif : function(component, event, helper) {
        document.getElementById('tlf-modification').style.display='none';
        document.getElementById('tlf-modification').classList.remove('in');
        document.getElementById('tlf-atachment-file-modification').style.display="none";
        document.getElementById('tlf-detail').style.display="block";
        document.getElementById('tlf-detail').classList.add('in');
        document.getElementById('file-attach-Modif').value='';
        window.scrollTo( 0, 0 );
    },
    closeModalAttModif : function(component, event, helper) {
        document.getElementById('tlf-atachment-file-modification').style.display="none";
        document.getElementById('tlf-atachment-file-modification').classList.remove("in");
        document.getElementById('file-attach-Modif').value='';
        document.getElementById('tlf-modification').style.display='block';
        document.getElementById('tlf-modification').classList.add('in');
        window.scrollTo( 0, 0 );
    },
    closeModalAttInci : function(component, event, helper) {
        document.getElementById('tlf-atachment-file-incident').style.display="none";
        document.getElementById('tlf-atachment-file-incident').classList.remove("in");
        document.getElementById('file-attach-Inci').value='';
        document.getElementById('tlf-incident').style.display='block';
        document.getElementById('tlf-incident').classList.add('in');
        window.scrollTo( 0, 0 );
    },
    openAttchModif : function(component, event, helper) {
        document.getElementById('tlf-atachment-file-modification').style.display="block";
        document.getElementById('tlf-atachment-file-modification').classList.add('in');
        document.getElementById('tlf-modification').style.display='none';
        document.getElementById('tlf-modification').classList.remove('in');
        document.getElementById('file-attach-Modif').value='';
        window.scrollTo( 0, 0 );
    },
    openAttchInci : function(component, event, helper) {
        debugger;
        document.getElementById('tlf-atachment-file-incident').style.display="block";
        document.getElementById('tlf-atachment-file-incident').classList.add('in');
        document.getElementById('tlf-incident').style.display='none';
        document.getElementById('tlf-incident').classList.remove('in');
        document.getElementById('file-attach-Inci').value='';
        window.scrollTo( 0, 0 );
    },
    prevItem : function(component, event, helper) {
        var ev = $A.get("e.c:CWP_pagination_evt");
        ev.setParams({'type':"prev"});
        ev.setParams({'index': component.get("v.indexElem")});
        ev.fire();
    },
    nextItem : function(component, event, helper) {
        var ev = $A.get("e.c:CWP_pagination_evt");
        ev.setParams({'type':"next"});
        ev.setParams({'index': parseInt(component.get("v.indexElem"))});
        ev.fire();
    },  
    updateNameModif : function(component, event, helper){
        var fileInput = document.getElementById("file-attach-Modif");
        var file = fileInput.files[0];
        document.getElementById("labelbuttonmodif").innerHTML=file.name;
    },  
    updateNameInci : function(component, event, helper){
        var fileInput = document.getElementById("file-attach-Inci");
        var file = fileInput.files[0];
        document.getElementById("labelbuttoninci").innerHTML=file.name;
    }
})