({
    doInit : function(component, event, helper) {   
        // 
        helper.getInicialTickets(component, event, helper);
    },
    nextPageTickets : function(component, event, helper) {
        //Calculamos el nuevo tableControl
        //
        var tableController= component.get('v.ticketsTableController');
        tableController.iniIndex+=tableController.viewSize;
        
        tableController.finIndex+=tableController.viewSize;
        if (!tableController.finIndex>tableController.numRecords)
            tableController.finIndex=tableController.numRecords;
        //Controlamos la paginación para la primera y la última página 
        if (tableController.iniIndex<=tableController.numRecords) 
            helper.getTickets(component, event, helper,tableController);
        
    },
    prevPageTickets : function(component, event, helper) {
        //Calculamos el nuevo tableControl
        var tableController= component.get('v.ticketsTableController');
        
        tableController.iniIndex-=tableController.viewSize;
        tableController.finIndex-=tableController.viewSize;
        if (tableController.iniIndex>0 ){
            helper.getTickets(component, event, helper,tableController);
        }
    },
    changeSizeTickets : function(component, event, helper) {
        //Calculamos el nuevo tableControl
        var tableController= component.get('v.ticketsTableController');
        var e = document.getElementById("paginationTickets");
        var newSize = e.options[e.selectedIndex].value;
        tableController.viewSize=newSize;
        tableController.iniIndex=1;
        tableController.finIndex=newSize;
        helper.getTickets(component, event, helper,tableController);
        
    },
    advancedFilterTickets : function(component, event, helper) {
        helper.getFieldValues(component, event, helper);
    }, 
    getServices : function(component, event, helper) {
        debugger;
        var e = document.getElementById("selectServiceFamilyTickets");
        var selectedServiceFamily = e.options[e.selectedIndex].value;
        
        var action = component.get("c.getServicesList");
        action.setParams({ serviceFamily :selectedServiceFamily});
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                debugger;
                var res = response.getReturnValue();
                component.set('v.serviceFields', JSON.parse(res["TGS_Product_Tier_2__c"]));
                component.set('v.serviceUnitFields', JSON.parse(res["TGS_Product_Tier_3__c"]));
            }
        });
        $A.enqueueAction(action);
    },
    getServicesUnits: function(component, event, helper) {
        debugger;
        var e = document.getElementById("selectServiceFamilyTickets");
        var selectedServiceFamily = e.options[e.selectedIndex].value;
        var e = document.getElementById("selectServiceTickets");
        var selectedService = e.options[e.selectedIndex].value;
        
        var action = component.get("c.getServiceUnitList");
        action.setParams({ serviceFamily :selectedServiceFamily, service :selectedService});
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                debugger;
                var res = response.getReturnValue();
                component.set('v.serviceUnitFields', JSON.parse(res["TGS_Product_Tier_3__c"]));
            }
        });
        $A.enqueueAction(action);
    },
    
    search: function(component, event, helper){
    	var field=component.get('v.searchField');
    	var value= document.getElementById('buscar').value;
    	helper.getFilteredRecords(component, event, field,value);
    },
    
    changeField: function(component, event, helper){
    	component.set('v.searchField',event.currentTarget.value);
    },
	closeModal : function(component, event, helper) {
        document.getElementById('backgroundmodal').style.display='none';
    },
    showBackground : function(component, event, helper) {
        document.getElementById('backgroundmodal').style.display='block';
    },
    showModal:function(component, event, helper){
        document.getElementById('backgroundmodal').style.display='block';
    	event.preventDefault();
    	var toggleProducto = component.find("producto");
        $A.util.removeClass(toggleProducto, "slds-hide");
		var toggleBackdrop = component.find("backdrop");
        $A.util.removeClass(toggleBackdrop, "slds-hide");
        component.set("v.caseSelected",component.get("v.ticketsList")[event.currentTarget.id]);
    },
    
    hideModal : function(component, event, helper){
        document.getElementById('backgroundmodal').style.display='none';
		var toggleProducto = component.find("producto");
        $A.util.addClass(toggleProducto, "slds-hide");
		var toggleBackdrop = component.find("backdrop");
        $A.util.addClass(toggleBackdrop, "slds-hide");
    },
    
    sortTickets:function(component, event, helper){
    	event.preventDefault();
    	var field=event.currentTarget.id;
    	helper.getSortRecords(component, event, field);
    },
    
})