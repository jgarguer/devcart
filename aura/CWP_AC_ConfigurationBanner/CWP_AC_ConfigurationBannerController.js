({
    
    doInit : function (cmp, evt, helper) { 
    	var recordId =  cmp.get("v.recordId");
        var action = cmp.get("c.getQuote");
        action.setParams({
            recordId : recordId
        });
         action.setCallback(this, function(response) {
             cmp.set("v.quoteName", response.getReturnValue())
         });
        $A.enqueueAction(action);     
         
    },
    
	exportPDF : function(cmp, event, helper) {
        
		var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": window.location.origin + "/" + window.location.pathname.split("/")[1] + "/apex/CWP_AC_OfertaAutocotizador?orderId=" + cmp.get("v.recordId")
            });
            urlEvent.fire();
        
	},
    	backButton : function(cmp, evt, helper) {
            
		var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": "/cwp-quotation-tool"
            });
            urlEvent.fire();
        
        window.location.reload();
	}
})