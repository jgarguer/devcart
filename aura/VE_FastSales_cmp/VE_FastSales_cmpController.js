({
    doInit: function(cmp, evt, helper){                

        var acctid = window.location.href;
//        if (acctid.includes('one.app')) {
        if (acctid.indexOf('one.app') > -1) {
            cmp.set('v.sfoneUI', "true");
        } else if (acctid.indexOf('lightning') > -1) {
            cmp.set('v.sfoneUI', "true");
        }
        var i = acctid.indexOf('xxxx');
        
        acctid = acctid.substring(i+5);
        if(acctid.endsWith('SxSxS')) {
            var j = acctid.lastIndexOf('SxSxS');//JEG 30/08/2017
        	acctid = acctid.substring(0,j);
        }
        if (cmp.get('v.accountId') != null ) {
            acctid = cmp.get('v.accountId');
        }
        var commID = window.location.href;
        if (commID.indexOf('isComm') > 0){            
            i = commID.indexOf('isComm');            
            var communityName = commID.substring(i+7);
            j = communityName.indexOf('XsxSx');
            communityName = communityName.substring(0,j);
            cmp.set('v.community', communityName);
            cmp.set('v.isCommunity', true);
        }

        var today = new Date();  
        var nextWeek = new Date(today);
        nextWeek.setDate(nextWeek.getDate() + 7);     

        console.log(nextWeek);

        var opty = cmp.get('v.opportunity');

        var dd = nextWeek.getDate();
        var mm = parseInt(nextWeek.getMonth()+1); //January is 0!
        
        if(mm<10 && mm.toString().length ==1) {
			mm='0'+mm;
//            alert("hubo arreglo");
        }
        
        if(dd<10 && dd.toString().length ==1) {
			dd='0'+dd;
//            alert("hubo arreglo2");
        }

        var yyyy = nextWeek.getFullYear();

        var stringToday = yyyy+'-'+mm+'-'+dd;

        opty.CloseDate=stringToday.toString();
        
        opty.BI_Fecha_de_vigencia__c=stringToday.toString();

        cmp.set('v.opportunity',opty);
        console.log('ID DE CUENTA' + acctid);
        
        // Manuel Medina -- 2016-07-23 -- Added only in Prod because previous line do not work at Prod and SFPREPRODF
     	cmp.find("InputFecha1").set("v.value", opty.CloseDate);   
        cmp.find("InputFecha2").set("v.value", opty.BI_Fecha_de_vigencia__c);
        // Manuel Medina -- 2016-07-23

        var action = cmp.get("c.getAccount");
        action.setParams({acctId: acctid});
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var miacct = response.getReturnValue();
                
                cmp.set('v.account', miacct);                            
                
                opty.BI_Country__c = miacct.BI_Country__c;

                cmp.set('v.opportunity',opty);

                // Manuel Medina -- 2016-07-23 -- Added only in Prod because previous line do not work at Prod and SFPREPRODF
                cmp.find("InputSelectDynamic").set("v.value", opty.BI_Country__c);      
                
                //Guillermo Muñoz -- 13/09/2016 -- Comentado por problemas en UAT (el atributo que intenta rellenar ya no existe).
                /*if (opty.BI_Country__c =="Peru"){
                    cmp.set('v.reqLicitacion',true)
                }*/

                var opts2 = [
                    { class: "optionClass", label: "ARS - Peso argentino",  labelClass: "texto", value: "ARS" },
                    { class: "optionClass", label: "BRL - Real brasileño",  labelClass: "texto", value: "BRL" },
                    { class: "optionClass", label: "CLF - Unidades de fomento",  labelClass: "texto", value: "CLF" },
                    { class: "optionClass", label: "CLP - Peso chileno",  labelClass: "texto", value: "CLP" },
                    { class: "optionClass", label: "COP - Peso colombiano",  labelClass: "texto", value: "COP" },
                    { class: "optionClass", label: "CRC - Colón costarricense",  labelClass: "texto", value: "CRC" },
                    { class: "optionClass", label: "EUR - Euro",  labelClass: "texto", value: "EUR" },
                    { class: "optionClass", label: "GBP - Libra esterlina",  labelClass: "texto", value: "GBP" },                    
                    { class: "optionClass", label: "GTQ - Quetzal guatemalteco",  labelClass: "texto", value: "GTQ" },
                    { class: "optionClass", label: "MXN - Peso mexicano",  labelClass: "texto", value: "MXN" },
                    { class: "optionClass", label: "NIO - Córdoba nicaraguense",  labelClass: "texto", value: "NIO" },
                    { class: "optionClass", label: "PAB - Balboa panameño",  labelClass: "texto", value: "PAB" },
                    { class: "optionClass", label: "PEN - Nuevo sol peruano",  labelClass: "texto", value: "PEN" },
                    { class: "optionClass", label: "USD - Dólar de EE.UU.",  labelClass: "texto", value: "USD" },
                    { class: "optionClass", label: "UYU - Peso uruguayo",  labelClass: "texto", value: "UYU" },
                    { class: "optionClass", label: "VEF - Bolivar venezolano fuerte",  labelClass: "texto", value: "VEF" }
                ];

                console.log(miacct.CurrencyIsoCode);
                console.log(miacct.BI_Country__c);


                var optss = [];
                var x = null;

                // Manuel Medina - 2016-07-23 -- Added extra var declaration
                var i=0;
                
                for (i=0; i<opts2.length; i++){
                    if ( miacct.CurrencyIsoCode == opts2[i].value){                        
                        x = opts2[i];
                        break;
                    }
                }
        
                optss[0]=x;
                for (i=0; i<opts2.length; i++){
                    optss[i+1]=opts2[i];
                }

                cmp.find("InputSelectDynamic2").set("v.options", optss);      

            }
        });
        $A.enqueueAction(action);
        
        var action3 = cmp.get("c.getSIMPOppType");
        var inputsel3 = cmp.find("SIMPopptype");
    	var opts3=[];
        
        action3.setCallback(this, function(response) {
            for(var i=0; i< response.getReturnValue().length; i++) {
                opts3.push({class: "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
            }
            inputsel3.set("v.options", opts3);
        });
        $A.enqueueAction(action3); 
        
        
        var action6 = cmp.get("c.getOppType");
        action6.setParams({
                    accId: acctid
                });
        var inputsel6 = cmp.find("InputSelectDynamic3");
    	var opts6=[];
        
        action6.setCallback(this, function(response) {
            for(var i=0; i< response.getReturnValue().length; i++) {
                opts6.push({class: "optionClass", label: response.getReturnValue()[i], value: response.getReturnValue()[i]});
            }
            inputsel6.set("v.options", opts6);
        });
        $A.enqueueAction(action6);
        
    },
    cancelButton: function(cmp,evt,helper){
        var a = cmp.get('v.account');
        var isCommunity = cmp.get('v.isCommunity');
        var url = '/'+a.Id;
        if ( isCommunity ){
            var community = cmp.get('v.community'); 
            url = '/'+community+'/'+a.Id;
        }

        //window.close();
        if (cmp.get('v.sfoneUI')== null ) {
		     window.location.assign(url);
        } else {
             sforce.one.navigateToURL(url);
        }
    },
    pressButton: function(cmp, evt, helper){
		
        var acct = cmp.get('v.account');
        var oppty = cmp.get('v.opportunity');

        var send = true;        

        if (oppty.BI_Opportunity_Type__c=='null'){
            send = false;
            var linnegmsg = cmp.find('linneg-msg');
            var tipoOptyDiv = cmp.find('tipoOpty');

            $A.util.addClass(tipoOptyDiv,'slds-has-error');
            $A.util.removeClass(linnegmsg,'slds-hide');
        }

        if (oppty.BI_SIMP_Opportunity_Type__c=='null'){
            send = false;
            var tipoppdiv = cmp.find('tipopp-div');
            var tipoppmsg = cmp.find('tipopp-msg');

            $A.util.addClass(tipoppdiv,'slds-has-error');
            $A.util.removeClass(tipoppmsg,'slds-hide');
        }        

        if (!oppty.Name || oppty.Name.length == 0){
            send = false;
            var onamediv = cmp.find('oname-div');
            var onamemsg = cmp.find('oname-msg');

            $A.util.addClass(onamediv,'slds-has-error');
            $A.util.removeClass(onamemsg,'slds-hide');
        }

        if (!oppty.CloseDate || oppty.CloseDate.length == 0){
            send = false;
            var fcirdiv = cmp.find('fcir-div');
            var fcirmsg = cmp.find('fcir-msg');

            $A.util.addClass(fcirdiv,'slds-has-error');
            $A.util.removeClass(fcirmsg,'slds-hide');
        }

        if (!oppty.BI_Fecha_de_vigencia__c || oppty.BI_Fecha_de_vigencia__c.length == 0){
            send = false;
            var fvigdiv = cmp.find('fvig-div');
            var fvigmsg = cmp.find('fvig-msg');

            $A.util.addClass(fvigdiv,'slds-has-error');
            $A.util.removeClass(fvigmsg,'slds-hide');
        }

        var separaStrVgn = oppty.BI_Fecha_de_vigencia__c.split('-');
        
        var separaStrCe = oppty.CloseDate.split('-');
        
		// Manuel Medina -- 2016-07-23 -- Added extra var declaration
		var i =0;
        
        for (i = 0; i < 3; i++) { 
//            alert('fuera: '+separaStrVgn[i]+' '+separaStrCe[i]);
            
              if(separaStrVgn[i]<10 && separaStrVgn[i].toString().length ==1) {
            	separaStrVgn[i]='0'+separaStrVgn[i];
              }
              if(separaStrCe[i]<10 && separaStrCe[i].toString().length ==1) {
		            separaStrCe[i]='0'+separaStrCe[i];
              }
//            alert('dentro: '+separaStrVgn[i]+' '+separaStrCe[i]);
            
//            alert('valor mes: '+separaStrVgn[i]+' '+separaStrVgn[i].toString().length);
        }
        
		oppty.BI_Fecha_de_vigencia__c = separaStrVgn[0]+'-'+separaStrVgn[1]+'-'+separaStrVgn[2];
		oppty.CloseDate = separaStrCe[0]+'-'+separaStrCe[1]+'-'+separaStrCe[2];
        oppty.BI_Licitacion__c = 'No';
        
//		oppty.BI_Fecha_de_vigencia__c = separaStrVgn[2]+'-'+separaStrVgn[1]+'-'+separaStrVgn[0];
//		oppty.CloseDate = separaStrCe[2]+'-'+separaStrCe[1]+'-'+separaStrCe[0];

//        alert ('valor vigencia: '+oppty.BI_Fecha_de_vigencia__c);
//        alert ('valor cierre: '+oppty.CloseDate);

        if ( send ){

            console.log(oppty);
            var action = cmp.get("c.insertOppty");
            action.setParams({
                o: oppty,
                a: acct
            });

            action.setCallback(this, function(response) {
                console.log(response);

                if (cmp.isValid() && response.getState() === "SUCCESS") {
                    var resOpt = response.getReturnValue();
                    console.log(resOpt);

                    if ( resOpt.BI_Probabilidad_de_exito__c=='75' ){
                        var check = false;
                            //using custom url param, not able to use Theme4t evaluation (UIThemeDescription) due to classic app is using lightning experience as well for this process.
                            if (cmp.get('v.sfoneUI') == null ){
                                var isCommunity = cmp.get('v.isCommunity');
                                var url = '/apex/NE__NewConfiguration?accId='+resOpt.AccountId+'&billAccId='+resOpt.AccountId+'&oppId='+resOpt.Id+'&servAccId='+resOpt.AccountId;
                                if ( isCommunity ){
                                    var community = cmp.get('v.community');
                                    url = '/'+community+'/apex/NE__NewConfiguration?accId='+resOpt.AccountId+'&billAccId='+resOpt.AccountId+'&oppId='+resOpt.Id+'&servAccId='+resOpt.AccountId;
                                }                        
                                
                                if (window.location.href.indexOf('jcjcjc') > 0){
                                    window.open(url);
                                }
                                else{
        //                            window.opener.location = url;
                                    window.location.assign(url);
                                }
                                //window.close();
                                window.location.assign(url);
                           } else {
                                sforce.one.navigateToURL('/apex/BI_Market?accId='+resOpt.AccountId+'&servAccId='+resOpt.AccountId+'&billAccId='+resOpt.AccountId+'&oppId='+resOpt.Id+'&confCurrency='+resOpt.CurrencyIsoCode);
                                 /* var urlEvent = $A.get("e.force:navigateToURL");
                                 urlEvent.setParams({
                                       "url": '/apex/BI_Market?accId='+resOpt.AccountId+'&servAccId='+resOpt.AccountId+'&billAccId='+resOpt.AccountId+'&oppId='+resOpt.Id+'&confCurrency='+resOpt.CurrencyIsoCode
                                 });
                                 urlEvent.fire();*/
                            }
                    }else{
                        alert('Se ha producido un error: '+resOpt.BI_Probabilidad_de_exito__c);
                    }           
                }
                else{
                    console.log(response);
                    alert(response.getState());
                    alert("From server: " + response.getReturnValue());
                }
            });
            $A.enqueueAction(action);
        }
        
    },
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');        
        $A.util.removeClass(spinner,'slds-hide');  
    },
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');        
        $A.util.addClass(spinner,'slds-hide');
    },
    handlerChangeId: function(cmp, evt, helper){
        console.log(evt);
        var opp = cmp.get('v.opportunity');
        opp.BI_Acuerdo_Marco__c=evt.getParams().value;        
        cmp.set('v.opportunity', opp);        
    }
})