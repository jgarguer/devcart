({
    openModel: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.showModal", true);
   },
 
   closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.showModal", false);
      var switchEvent = $A.get("e.c:E3_Matriz_Complejidad_SwitchEvent");
      switchEvent.setParams({'isFinished' : false})
   },
})