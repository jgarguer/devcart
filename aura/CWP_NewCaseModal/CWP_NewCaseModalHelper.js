({
    getsearchFields : function(component){
        var action = component.get("c.defineSearch");
        var list=component.get("v.reasons");
         action.setParams({ "pickValues" : list});
        //Callback    	        
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {                
                var returnValue = response.getReturnValue();
                if(returnValue != null){                                                            
                    component.set("v.searchFields", returnValue);                    
                }
            }
        });
        $A.enqueueAction(action);                
    }
})