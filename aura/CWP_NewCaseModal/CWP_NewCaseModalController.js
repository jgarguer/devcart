({
    doInit: function(component,event,helper){
        var action = component.get("c.getIdent");    
            action.setCallback(this, function(actionResult) {
                if(component.isValid() && actionResult.getState() == "SUCCESS"){
                    var returnValue = actionResult.getReturnValue();
                    if(returnValue != null){
                        component.set("v.ident", actionResult.getReturnValue());
                    }
                }
            });
            $A.enqueueAction(action);
        
        var action = component.get("c.getBIENUser");    
            action.setCallback(this, function(actionResult) {
                if(component.isValid() && actionResult.getState() == "SUCCESS"){
                    var returnValue = actionResult.getReturnValue();
                    if(returnValue != null){
                        //alert('BIENUSER '+actionResult.getReturnValue());
                        component.set("v.BIENUser", actionResult.getReturnValue());
                    }
                }
            });
            $A.enqueueAction(action);
        
        helper.getsearchFields(component);
    },
    
    saveCase: function(component, event, helper) {
        debugger;
        var checkFields=true;
        var reason=component.find('reason').get('v.value');
        var ParentId = '';
        var whereFrom = '';
        if(reason === undefined){
            reason = component.get('v.reasons')[0];
        }
        var subject=component.find('newIdeaTitle').get('v.value');
        var description=component.find('description').get('v.value');
        var purpose = 'test value';
        
        if(reason===undefined || reason==='TODO JAAG' ){
            checkFields=false;
        }else if(subject===undefined || subject==='' ){
            checkFields=false;
        }else if(description===undefined || description==='' ){
            checkFields=false;
        }else if(purpose===undefined || purpose==='' ){
            checkFields=false;
        }
        if (checkFields){
            //alert('entró al action');
            //var subject=document.getElementById("subject").value;
            //var description=document.getElementById("description").value;
            var action = component.get("c.setCase"); 
            //Callback
            action.setParams({
                "status": "Assigned",
                "reason": reason,
                "subject": subject,
                "description": description,
                "purpose": purpose,
                "ParentId": ParentId,
                "whereFrom":whereFrom
            }); 
            action.setCallback(this, function(actionResult) {
                //alert('antes de validación '+actionResult.getState());
                //alert(JSON.stringify(actionResult.getError()));
                if(component.isValid() && actionResult.getState() == "SUCCESS"){
                    //alert('SUCCESS '+actionResult.getReturnValue());
                    var returnValue = actionResult.getReturnValue();
                    /*if(returnValue != null){
                        alert(returnValue);
                    }*/
                }else{
                    var returnValue = JSON.stringify(actionResult.getError());
                    alert(JSON.stringify(actionResult.getError()));
                }
            });
            $A.enqueueAction(action);
            //document.getElementById("CWP_TeamComponent_div").style.display = "block";
            alert('Volvió del Apex controller');
        }
        else {
            alert('Revise los campos');
        }
        //alert('Antes de HideModal');
        hideModal(component);
    },
    
    hideModal : function(cmp) {
        alert('hideModal');
        var element = cmp.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
    }
})