({
	doInit : function(component, event, helper) {
	    var urlEvent = $A.get("e.force:navigateToURL");
    	urlEvent.setParams({
      		"url": 'https://telefonicab2b.force.com/gescam'
    	});
    	urlEvent.fire();

        var homeEvent = $A.get("e.force:navigateToObjectHome");
	    homeEvent.setParams({
    	    "scope": "Account"
	    });
    	homeEvent.fire();
}
})