({
	changedConfig : function(cmp, evt, helper) {
		
		//preloading header labels
		//$Label.c.BI_SF1_Contacto
		//$Label.c.BI_SF1_Cliente
		/** overwriting BI_SF1_Titulo_de_la_cabecera__c with the translated label instead of the value in the config */
		cmp.get("v.config").BI_SF1_Titulo_de_la_cabecera__c =  $A.get("$Label.c.BI_SF1_" + cmp.get("v.config").BI_SF1_Titulo_de_la_cabecera__c);

		if(cmp.get("v.config").BI_SF1_Custom_tab_listView_configuration__r!=undefined){
			var action = cmp.get("c.customViews");
			action.setParams({
				"objectType" : cmp.get("v.config").MasterLabel,
				/** the default value always in the first position of the list */
				"descriptionField" : cmp.get("v.config").BI_SF1_Custom_tab_listView_configuration__r[0].BI_SF1_Campos_a_mostrar__c,
				"whereClause" : cmp.get("v.config").BI_SF1_Custom_tab_listView_configuration__r[0].BI_SF1_filtro_query__c
			});
			action.setCallback(this, function(response){
				cmp.set("v.listRecords", response.getReturnValue());
				var length = response.getReturnValue()!=null? cmp.get("v.listRecords").length : '0'
				cmp.set('v.subtitle',cmp.get("v.config").BI_SF1_Custom_tab_listView_configuration__r[0].translatedLabel + ', ' + length + ' ' + $A.get("$Label.c.BI_SF1_Elemento"));
				$A.util.toggleClass(cmp.find("spinnerRecordList"), "slds-hide");
			});

			$A.enqueueAction(action);
		}
	
	},
	lookupClick : function(cmp, evt, helper) {
		console.log('lookupClick');
		/**
		 * This function call the Apex controller that retrieves the records filtered by the textfield
		 */
		$A.util.removeClass(cmp.find("spinnerRecordList"), "slds-hide");
		var action = cmp.get("c.lookUpRecords");
		action.setParams({
			"filter" : cmp.get("v.config").BI_SF1_Filtro_de_busqueda__c,
			"objectType" : cmp.get("v.config").MasterLabel,
			"textValue" : document.getElementById(cmp.get('v.config').MasterLabel+'lookupField').value,
			"descriptionField" : cmp.get("v.config").BI_SF1_Campo_descripcion__c
		});
		action.setCallback(this, function(response){
			cmp.set("v.listRecords", response.getReturnValue());
			var length = response.getReturnValue()!=null? cmp.get("v.listRecords").length : '0'
			cmp.set('v.subtitle',$A.get('$Label.c.BI_SF1_Buscar')+', ' + length + ' ' + $A.get("$Label.c.BI_SF1_Elemento"));
			$A.util.toggleClass(cmp.find("spinnerRecordList"), "slds-hide");
		});
		$A.enqueueAction(action);

	},

	changedListView : function(cmp, evt, helper) {
		/** function to capture the changes on the list view picklist */
		$A.util.removeClass(cmp.find("spinnerRecordList"), "slds-hide");
		console.log('changedListView');
		if(evt.getSource().get('v.value') != 'empty'){
			
			var myListView;
			var listViews = cmp.get("v.config").BI_SF1_Custom_tab_listView_configuration__r;
			for(var i in listViews){
			
				if(listViews[i].MasterLabel == evt.getSource().get('v.value')){
				
					myListView = listViews[i];
				}
				
			}

			var action = cmp.get("c.customViews");
			action.setParams({
				"objectType" : cmp.get("v.config").MasterLabel,
				"descriptionField" : myListView.BI_SF1_Campos_a_mostrar__c,
				"whereClause" : myListView.BI_SF1_filtro_query__c
			});
			action.setCallback(this, function(response){
				
				cmp.set("v.listRecords", response.getReturnValue());
				var length = response.getReturnValue()!=null? cmp.get("v.listRecords").length : '0'
				var subtitle = $A.get("$Label.c.BI_SF1_" + evt.getSource().get('v.value')) + ', ' + length + ' ' + $A.get("$Label.c.BI_SF1_Elemento");

				cmp.set('v.subtitle',subtitle);
				$A.util.toggleClass(cmp.find("spinnerRecordList"), "slds-hide");
			});
			$A.enqueueAction(action);
			
		}else{
			cmp.set("v.listRecords", []);
			cmp.set('v.subtitle','');
		}		
		
	},
	lookupKeyPressed : function(cmp, evt, helper) {

		/** When the key enter is pressed on the search bar apex method to query over the records is executed */
		console.log('lookupKeyPressed');
		cmp.set("v.tecla", evt.keyCode)
		if(evt.keyCode === 13){
			$A.util.removeClass(cmp.find("spinnerRecordList"), "slds-hide");
			
			var action = cmp.get("c.lookUpRecords");
			action.setParams({
				"filter" : cmp.get("v.config").BI_SF1_Filtro_de_busqueda__c,
				"objectType" : cmp.get("v.config").MasterLabel,
				"textValue" : document.getElementById(cmp.get('v.config').MasterLabel+'lookupField').value,
				"descriptionField" : cmp.get("v.config").BI_SF1_Campo_descripcion__c
			});
			action.setCallback(this, function(response){
				
				cmp.set("v.listRecords", response.getReturnValue());
				var length = response.getReturnValue()!=null? cmp.get("v.listRecords").length : '0'
				cmp.set('v.subtitle',$A.get('$Label.c.BI_SF1_Buscar')+', ' + length + ' ' + $A.get("$Label.c.BI_SF1_Elemento"));
				$A.util.toggleClass(cmp.find("spinnerRecordList"), "slds-hide");
			});
			$A.enqueueAction(action);
		}

	}
})