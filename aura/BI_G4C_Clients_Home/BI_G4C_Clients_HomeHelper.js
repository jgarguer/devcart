({
    getMyAccounts : function(cmp, helper) {
    setTimeout(function() {
         
          frame.contentWindow.postMessage(paramMap, '*');
        }, 2000);
        
    },
    doInicialGetFilterAccount: function(cmp, evt, helper) {
        
        helper.ocultarFiltroPropietario(cmp);
        
        var filters = cmp.get('v.filters');
        var filterElem='';
        for (var i = 0; i < filters.length; i++) {
            filterElem=filters[i];
            //console.log(filterElem);
            //by JB 29/07
            if (filterElem.indexOf("CampaignId")>-1){
                //console.log(filterElem);
                //by JB 29/07
                if(filterElem.toString().length>30){
                    cmp.set('v.campaniaFilter',true);
                }else{
                    cmp.set('v.campaniaFilter',false);
                }
            }
        }
        
        //console.log(filters);
        //by JB 29/07
        helper.getFilterAccounts(cmp, filters);
        helper.getFilterData(cmp);
        
        var modalfilter = cmp.find('modalfilterCampanias');
        $A.util.addClass(modalfilter,'slds-hide');
    },
    getFilterData : function(cmp, helper){
        var action = cmp.get("c.getFilterData");
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var filterResults = response.getReturnValue();
                if(filterResults != null){
                    cmp.set('v.propietarioFilter', filterResults[0]);
                    cmp.set('v.campanaFilter', filterResults[1]);
                    cmp.set('v.estadoFilter', filterResults[2]);
                }else{
                    cmp.set('v.propietarioFilter', 'No aplicado');
                    cmp.set('v.campanaFilter', 'No aplicado');
                    cmp.set('v.estadoFilter', 'No aplicado');
                }
            }
            else{
                var errorVar = $A.get("e.c:BI_G4C_Generic_error");
                errorVar.setParams({"header": "Error"});
                errorVar.setParams({"body": "GetFilterData: Error"});
                errorVar.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    filEstado: function (elem) {
        return elem.search('StageName') >= 0;
    },
    
    getUsuarios : function(cmp) {
        var action = cmp.get("c.getUsuarios");
        var newfilter = cmp.get('v.newfilter');
        action.setParams({
                filter: newfilter
        });
        
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var opts = JSON.parse(response.getReturnValue());
                cmp.find("selectPropietario").set("v.options", opts);
            }
            else{
                var errorVar = $A.get("e.c:BI_G4C_Generic_error");
                errorVar.setParams({"header": "Error"});
                errorVar.setParams({"body": "Error"});
                errorVar.fire();
                //console.log(response);
            }
        });
        $A.enqueueAction(action);
    },
    
    getOptions : function(cmp) {
        
        
        var action = cmp.get("c.getOptions");
        var auxLIDs= cmp.get("v.accounts");
        action.setParams({ lAccount :auxLIDs });
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var opts = JSON.parse(response.getReturnValue());
                cmp.find("InputSelectDynamic").set("v.options", opts);
            }
            
            
        });
        $A.enqueueAction(action);
    },
    getEstadoOportunidades : function(cmp) {
        var action = cmp.get("c.getEstadoOportunidades");
        var l= cmp.get("v.accounts");
        action.setParams({ campania :cmp.get("v.campaniaFiltrada"),lAccount:l });
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var opts = JSON.parse(response.getReturnValue());
                cmp.find("selectEstado").set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
    getFilterAccounts : function(cmp, p_filters,helper) {
        var filterString = '';
        var sinOp;
        for (var i=0; i<p_filters.length; i++){
            if (i>0){
                filterString +=' AND ';
            }
            filterString += p_filters[i];
        }
        
        //console.log(p_filters);
        //by JB 29/07
        var action = cmp.get("c.getFilterAccounts");
        var errorVar;
        action.setParams({filters: filterString,campania :cmp.get("v.campaniaFiltrada"), listaOwners :cmp.get("v.listaOwners"), filtroEstado :cmp.get("v.filtroEstado"), newFilter: cmp.get("v.newfilter") });
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var resAccs = response.getReturnValue();
                //console.log(resAccs);
                //by JB 29/07
                if(resAccs.length > 99){
                    errorVar = $A.get("e.c:BI_G4C_Generic_error");
                    errorVar.setParams({"header": "Advertencia"});
                    errorVar.setParams({"type": "WARNING"});
                    errorVar.setParams({"body": "La consulta ha superado el límite de resultados a mostrar en el mapa, se mostrarán los 100 primeros. En caso de necesidad aplicar un filtro"});
                    errorVar.fire();
                    
                }
                
                cmp.set('v.allAccounts', resAccs);
                var encontrado=false;
                for (var i = 0; i < resAccs.length && !encontrado; i++) {
                    if (resAccs[i].BI_G4C_ShippingLatitude__c){
                        cmp.set('v.selectedAccount', resAccs[i]);
                        encontrado=true;
                    }
                }   
                
                cmp.set('v.filtroAvanzado', true);
                
                this.newLimit(cmp);
                
                var modalfilter = cmp.find('modalfilter');
                $A.util.addClass(modalfilter, 'slds-hide');
            }
            else{
                errorVar = $A.get("e.c:BI_G4C_Generic_error");
                errorVar.setParams({"header": "Error"});
                errorVar.setParams({"body": "Error"});
                errorVar.fire();
            }
        });
        $A.enqueueAction(action);
        
    },
    contains: function(string, substring){
        return string.toLowerCase().indexOf(substring.toLowerCase()) > -1
    },
    applyFilters: function (cmp,helper){
        var allAccounts = cmp.get('v.allAccounts');
        var textFilter = cmp.get('v.textFilter');
        var bookmarkFilter = cmp.get('v.bookmarkFilter');
        var senyalizadoFilter = cmp.get('v.senyalizadoFilter');
        var visitedFilter = cmp.get('v.visitedFilter');
        var noVisitedFilter = cmp.get('v.noVisitedFilter');
        var toVisitFilter = cmp.get('v.toVisitFilter');
        var accounts = [];
        var event = $A.get("e.c:BI_G4C_AccountsLoaded");
        
        if ( textFilter && textFilter.length > 0 ){
            var j=0;
            for (var i=0; i<allAccounts.length; i++){
                if ( helper.contains(allAccounts[i].Name, textFilter) || (allAccounts[i].BillingStreet && helper.contains(allAccounts[i].BillingStreet, textFilter))){
                    accounts[j] = allAccounts[i];
                    j++;
                }
            }
        }
        else{
            accounts = allAccounts;
        }
        var result = accounts;
        accounts = [];
        if ( visitedFilter==='x' && noVisitedFilter==='x' ){
            accounts = result;
            cmp.set('v.accounts', accounts);
            event.setParams({"accounts": accounts});
            event.fire();
        }
        else{
            if ( visitedFilter==='x' ){
                var resultIds =[];
                for(i=0;i<result.length;i++){
                    resultIds.push(result[i].Id);
                }
                var aux= JSON.stringify(resultIds);
                var action = cmp.get("c.getNoVisitados");
                
                action.setParams({ StringIds : aux,campania :cmp.get("v.campaniaFiltrada"), listaOwners :cmp.get("v.listaOwners"), filtroEstado :cmp.get("v.filtroEstado") });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (cmp.isValid() && state === "SUCCESS") {
                        var res=response.getReturnValue();
                        for (i=0; i<res.length; i++){
                            accounts[i]=res[i];
                        }
                        cmp.set('v.accounts', accounts);
                        event.setParams({"accounts": accounts});
                        event.fire();
                    }
                    else{
                        //console.log("Unknown error");
                        //by JB 29/07
                        
                        
                    }
                });
                $A.enqueueAction(action);
                
            }else if ( noVisitedFilter==='x' ){
                resultIds =[];
                for(i=0;i<result.length;i++){
                    resultIds.push(result[i].Id);
                }
                aux= JSON.stringify(resultIds);
                action = cmp.get("c.getVisitados");
                action.setParams({ StringIds : aux,campania :cmp.get("v.campaniaFiltrada"), listaOwners :cmp.get("v.listaOwners"), filtroEstado :cmp.get("v.filtroEstado") });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (cmp.isValid() && state === "SUCCESS") {
                        var res=response.getReturnValue();
                        for (i=0; i<res.length; i++){
                            accounts[i]=res[i];
                        }
                        cmp.set('v.accounts', accounts);
                        event.setParams({"accounts": accounts});
                        event.fire();
                    }
                    else{
                        //console.log("Unknown error");
                        //by JB 29/07
                    }
                });
                $A.enqueueAction(action);
                
            }else{
                accounts = result;
                cmp.set('v.accounts', accounts);
                event.setParams({"accounts": accounts});
                event.fire();
            }
        }
        
    },
    setGeolocalization: function(cmp, eventId, geo) {
        var action = cmp.get("c.setGeolocalization");
        action.setParams({eventId:eventId,geo: geo});
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                
            }
            else{                
                var errorVar = $A.get("e.c:BI_G4C_Generic_error");
                errorVar.setParams({"header": "Error"});
                errorVar.setParams({"body": "Error"});
                errorVar.fire();
                
                
                //console.log(response);
                //by JB 29/07
            }
        });
        $A.enqueueAction(action);
    },
    replaceAll: function ( text, busca, reemplaza ){
        while (text.toString().indexOf(busca) !== -1)
            text = text.toString().replace(busca, reemplaza);
        return text;
    },
    checkReset: function ( cmp,evt, helper ) {
        var newFilterOptionOne = cmp.find('misClientes');
        $A.util.removeClass(newFilterOptionOne,'slds-is-selected');
        
        var newFilterOptionTwo = cmp.find('miembrosEquipo');
        $A.util.removeClass(newFilterOptionTwo,'slds-is-selected');
        
        var newFilterOptionThree = cmp.find('misClientesMiembrosEquipo');
        $A.util.removeClass(newFilterOptionThree,'slds-is-selected');
        
        var newFilterOptionFoure = cmp.find('todos');
        $A.util.removeClass(newFilterOptionFoure,'slds-is-selected');
        
        var filter = cmp.get('v.newfilter');
        var option = cmp.find(filter);
        $A.util.addClass(option,'slds-is-selected');
        
        helper.ocultarFiltroPropietario(cmp);
        
        var action = cmp.get("c.getUserName");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.UserID", response.getReturnValue());
                helper.doInicialGetFilterAccount(cmp, evt, helper);
            }
        });
        $A.enqueueAction(action);
    },
    checkResetVisitados: function ( cmp ) {
        var visitados = cmp.find('visitados');
        $A.util.removeClass(visitados,'slds-is-selected');
        var noVisitados = cmp.find('noVisitados');
        $A.util.removeClass(noVisitados,'slds-is-selected'); 
    },
    clearFilter: function ( cmp) {
        this.checkResetVisitados(cmp);
        
        cmp.set('v.campaniaFiltrada','');
        cmp.set('v.listaOwners','');
        cmp.set('v.filtroAvanzado',false);
        cmp.set('v.filtroEstado','');
        cmp.find('searchIT').set('v.value','');
        
        /*INI E001*/
        var filters = cmp.get('v.filters');
        filters.splice(0, filters.length);
        /*FIN E001*/
        cmp.set('v.actualPag', 1);
        
    },
    ocultarFiltroPropietario: function ( cmp) {
        // oculta filtro propietario
        var filter = cmp.get('v.newfilter');
        var filtroPropietario = cmp.find('filtroPropietario');
        if('misClientes' == filter || 'misClientesMiembrosEquipo' == filter ) {
            $A.util.addClass(filtroPropietario,'slds-hide');
        } else  {
            $A.util.removeClass(filtroPropietario,'slds-hide');
        }
    },
    newLimit: function(cmp) {
        var resAccs = cmp.get('v.allAccounts');
        
        var accsLimitListMapPagOne = [];
        var accsLimitListMapPagTwo = [];
        for(var i = 0; resAccs.length > i; i++  ) {
            // pag 1
            if(i < 99){
                accsLimitListMapPagOne.push(resAccs[i]);
            }
            // pag 2
            if(i > 99 && i < 199){
                accsLimitListMapPagTwo.push(resAccs[i]);
            }
        }
        
        var setAccPag = null;
        var pag = cmp.get('v.actualPag');
        var disabledNext = false;
        if(pag === 1) {
            setAccPag = accsLimitListMapPagOne;
            // disable back
            disabledNext = true;
        } else {
            setAccPag = accsLimitListMapPagTwo;
            // disable next
            disabledNext = false;
        }
        cmp.set('v.prev', disabledNext);
        cmp.set('v.next', !disabledNext);
        
        // en caso de el segundo este vacio siempre 
        // set la pag 1 
        if(accsLimitListMapPagTwo.length == 0 ) {
            // set pag 1
            cmp.set('v.actualPag',1);
            // disables 
            cmp.set('v.prev', true);
            cmp.set('v.next', true);
            setAccPag = accsLimitListMapPagOne;
        }
        cmp.set('v.accounts', setAccPag );
        var event = $A.get("e.c:BI_G4C_AccountsLoaded");
        event.setParams({"accounts": setAccPag});
        event.fire(); 
    },
})