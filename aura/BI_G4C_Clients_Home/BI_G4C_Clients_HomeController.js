({
    doInit: function(cmp, evt, helper){
        var elem = document.getElementById('mapWorkingDiv');
        if (elem!=null) {
            elem.parentElement.removeChild(elem);
        }
        var url = window.location.href;
        if ( url.indexOf('/g4c') >= 0 ){
            cmp.set('v.isCommunity', true);
        }
        else{
            cmp.set('v.isCommunity', false);
        }
        var action = cmp.get("c.getUserName");
        action.setCallback(this, function(response){
            
            
            response.getState();
            var state = response.getState();
            
            if (state === "SUCCESS") {
                cmp.set("v.UserID", response.getReturnValue());
                cmp.set('v.filters',[" OwnerId IN ('"+response.getReturnValue()+"') "]);
                helper.doInicialGetFilterAccount(cmp, evt, helper);
            }
        });
        $A.enqueueAction(action);
        
        helper.getFilterData(cmp);
        
        helper.ocultarFiltroPropietario(cmp);
    },
    
    doClearFilter: function(cmp, evt, helper){
        helper.clearFilter(cmp);
        helper.getFilterData(cmp);
        cmp.set('v.visitedFilter','');
        cmp.set('v.noVisitedFilter','');
        helper.doInicialGetFilterAccount(cmp, evt, helper);
    },
    
    showSpinner : function (component, event, helper) {
       var spinner = component.find('spinner');
       $A.util.removeClass(spinner,'slds-hide');
        /*  var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : true });
        evt.fire();*/
    },
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner,'slds-hide');
        /*var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : false });
        evt.fire();*/
    },
    doGetFilterAccount: function(cmp, evt, helper){
        
        var filters = cmp.get('v.filters');
        var filterElem='';
        for (var i = 0; i < filters.length; i++) {
            filterElem=filters[i];
            if (filterElem.indexOf("CampaignId")>-1){
                if(filterElem.toString().length>30){
                    cmp.set('v.campaniaFilter',true);
                }else{
                    cmp.set('v.campaniaFilter',false);
                }
            }
        }
        helper.getFilterAccounts(cmp, filters);
        helper.getFilterData(cmp);
        
        var modalfilter = cmp.find('modalfilterCampanias');
        $A.util.addClass(modalfilter,'slds-hide');
    },
    doGetFilterEstado: function(cmp, evt, helper){
        var filters = cmp.get('v.filters');
        var filterElem='';
        for (var i = 0; i < filters.length; i++) {
            filterElem=filters[i];
            if (filterElem.indexOf("CampaignId")>-1){
                if(filterElem.toString().length>30){
                    cmp.set('v.campaniaFilter',true);
                }else{
                    cmp.set('v.campaniaFilter',false);
                }
            }
            if (filterElem.indexOf("StageName")>-1){
                //console.log(filterElem);
                //by JB 29/07
                cmp.set('v.estadoFilter',true);
                
            }
            
        }
        //console.log(filters);
        //by JB 29/07
        helper.getFilterAccounts(cmp, filters);
        helper.getFilterData(cmp);
        
        var modalfilter = cmp.find('modalfilterEstado');
        $A.util.addClass(modalfilter,'slds-hide');
    },
    doGetFilterPropietario: function(cmp, evt, helper){
        var filters = cmp.get('v.filters');
        var filterElem='';
        for (var i = 0; i < filters.length; i++) {
            filterElem=filters[i];
            //console.log(filterElem);
            //by JB 29/07
            if (filterElem.indexOf("CampaignId")>-1){
                if(filterElem.toString().length>30){
                    cmp.set('v.campaniaFilter',true);
                }else{
                    cmp.set('v.campaniaFilter',false);
                }
            }
            if (filterElem.indexOf("StageName")>-1){
                cmp.set('v.estadoFilter',true);
            }
        }
        helper.getFilterAccounts(cmp, filters);
        helper.getFilterData(cmp);
        
        var modalfilter = cmp.find('modalfilterPropietario');
        $A.util.addClass(modalfilter,'slds-hide');
    },
    selectEstado: function(cmp, evt, helper){
        
        var filters = cmp.get('v.filters');
        for (var i=0; i<filters.length;i++) {
            
            if (filters[i].includes("StageName")){
                filters.splice(i, 1);
            }
        }
        var aux=cmp.find("selectEstado").get('v.value');
        aux=helper.replaceAll(aux,';','\', \'');
        var newFiltro =' StageName IN( \''+aux+'\') ' ;
        cmp.set('v.filtroEstado', newFiltro);
        filters.push(newFiltro);
        cmp.set('v.filters', filters);
    },
    selectPropietario: function(cmp, evt, helper){
        var filters = cmp.get('v.filters');
        for (var i=0; i<filters.length;i++) {
            
            if (filters[i].includes("OwnerId")){
                filters.splice(i, 1);
            }
        }
        var aux='(\'';
        aux+=cmp.find("selectPropietario").get('v.value');
        aux=helper.replaceAll(aux, ';','\', \'');
        aux+='\')';
        cmp.set('v.listaOwners', aux);
        var newFiltro =' OwnerId IN '+aux+' ';
        filters.push(newFiltro);
        cmp.set('v.filters', filters);
    },
    selectCampania: function(cmp, evt, helper){
        var filters = cmp.get('v.filters');
        filters = cmp.get('v.filters');
        for (var i=0; i<filters.length;i++) {
            if (filters[i].includes("CampaignId")){
                filters.splice(i, 1);
            }
        }
        //var newFiltro = 'campaignID' +cmp.find("InputSelectDynamic").get('v.value');
        var newFiltro = ' CampaignId =\''+cmp.find("InputSelectDynamic"
                                                  ).get('v.value')+'\'';
        cmp.set('v.campaniaFiltrada', cmp.find("InputSelectDynamic").get('v.value'));
        filters.push(newFiltro);
        cmp.set('v.filters', filters);
    },
    handleSaveSuccess: function(cmp, evt, helper){
        
        // var mapCmp = cmp.find("mapCmp");
        // var accounts = cmp.get('v.accounts');
        // mapCmp.changeSelected(accounts[3]);
        helper.getMyAccounts(cmp);
    },
    toggleMode: function(cmp, evt, helper){
        var mode = cmp.get('v.mode');
        if ( mode==='Modo visor' ){
            cmp.set('v.mode', 'Modo ediciÃ³n');
        }
        else{
            cmp.set('v.mode', 'Modo visor');
        }
    },
    refreshAccounts:function(cmp, evt, helper){
        
        helper.getMyAccounts(cmp);
    },
    handleGo4ClientsListEvt : function(component, event, helper) { 
        var payload = event.getParam("payload");
        var estado = event.getParam("estado");
        var accounts= event.getParam ("accounts");
        var divdetails = component.find('divdetails');
        $A.util.removeClass(divdetails,'slds-hide');
        component.set('v.selectedAccount', payload);
        
        //INI JULIO 
        //Evento para recargar centrar componente de la lista
        var pushAccount = $A.get("e.c:BI_G4C_pickAccount_Event");
        pushAccount.setParams({accountSelected: payload,accounts:accounts});
        pushAccount.fire();    
    },
    handleGo4ClientsHeaderEvt: function(cmp, evt, helper){
        
        var eType = evt.getParam("type");
        if (eType === 'Bookmark'){
            var account = evt.getParam("account");
            var accounts = cmp.get('v.accounts');
            for (var i=0; i<accounts.length; i++){
                if ( accounts[i].Id === account.Id ){
                    accounts[i]=account;
                    break;
                }
            }
            cmp.set('v.accounts', accounts);
            var allAccounts = cmp.get('v.allAccounts');
            for (i=0; i<allAccounts.length; i++){
                if ( allAccounts[i].Id === account.Id ){
                    allAccounts[i]=account;
                    break;
                }
            }
            cmp.set('v.allAccounts', allAccounts);
            helper.applyFilters(cmp, helper);
        }
        else{
            if (eType === 'Back'){
                var divdetails = cmp.find('divdetails');
                $A.util.addClass(divdetails,'slds-hide');
                helper.applyFilters(cmp, helper);
            }
            else{
                if (eType === 'Unfilter'){
                    helper.getMyAccounts(cmp);
                    cmp.set('v.filtroAvanzado', false);
                    cmp.set('v.fstreet', '');
                    cmp.set('v.fcp', '');
                }
            }
        }
    },
    doSearchChange: function(cmp, evt, helper){
        
        if ( evt.getParams().keyCode === 13){
            var searchIT = cmp.find('searchIT').get("v.value");
            cmp.set('v.textFilter', searchIT);
            //Ahora llamamos al servidor y traemos todas las que se parezcan al criterio 
            var aux='Account.Name LIKE \'%'+searchIT+'%\'';
            
            var filters = cmp.get('v.filters');
            for (var i=0; i<filters.length;i++) {
                
                if (filters[i].includes("Name LIKE")){
                    filters.splice(i, 1);
                }
            }
            if (searchIT!==''){
                filters.push(aux);
                cmp.set('v.filters', filters);
            }
            if (filters.length>0){
                helper.getFilterAccounts(cmp, filters);
            }else {
                
                
                helper.getMyAccounts(cmp);
                helper.getFilterData(cmp);
                cmp.set('v.filtroAvanzado',false);
            }
            //helper.applyFilters(cmp, helper);
        }
    },
    selectFilter2: function(cmp, evt, helper){
        var filter2 = cmp.find('filter2');
        $A.util.toggleClass(filter2,'filter-selected');
        var senyalizadoFilter = cmp.get('v.senyalizadoFilter');
        //console.log(senyalizadoFilter);
        // by JB 29/07
        cmp.set('v.senyalizadoFilter',(senyalizadoFilter)?false:true);
        
        helper.applyFilters(cmp, helper);
    },
    selectFilter1: function(cmp, evt, helper){
        var filter1 = cmp.find('filter1');
        $A.util.toggleClass(filter1,'filter-selected');
        
        var bookmarkFilter = cmp.get('v.bookmarkFilter');
        cmp.set('v.bookmarkFilter',(bookmarkFilter)?false:true);
        
        helper.applyFilters(cmp, helper);
        
    },
    selectFilter3: function(cmp, evt, helper){ 
        helper.checkResetVisitados(cmp);
        
        var visitedFilter = cmp.get('v.visitedFilter');
        var noVisitedFilter = cmp.get('v.noVisitedFilter');
        if ( noVisitedFilter && noVisitedFilter.length > 0 ){
            cmp.set('v.noVisitedFilter','');
        }
        else{
            cmp.set('v.noVisitedFilter','x');
            var clieckedItem = cmp.find('visitados');
            $A.util.addClass(clieckedItem,'slds-is-selected');
        }  
        if ( visitedFilter && visitedFilter.length > 0 ){
            clieckedItem = cmp.find('noVisitados');
            $A.util.addClass(clieckedItem,'slds-is-selected');
        }
        
        
        helper.applyFilters(cmp, helper);
    },
    selectFilter4: function(cmp, evt, helper) {
        helper.checkResetVisitados(cmp);
        var noVisitedFilter = cmp.get('v.noVisitedFilter');
        var visitedFilter = cmp.get('v.visitedFilter');
        if ( visitedFilter && visitedFilter.length > 0 ){
            cmp.set('v.visitedFilter','');
        }
        else{
            cmp.set('v.visitedFilter','x');
            var clieckedItem = cmp.find('noVisitados');
            $A.util.addClass(clieckedItem,'slds-is-selected');
        }
        if (noVisitedFilter && noVisitedFilter.length > 0 ){
            clieckedItem = cmp.find('visitados');
            $A.util.addClass(clieckedItem,'slds-is-selected');
            
        }
        
        helper.applyFilters(cmp, helper);
    },
    selectFilter5: function(cmp, evt, helper){
        var modalfilter = cmp.find('modalfilter');
        $A.util.removeClass(modalfilter,'slds-hide');
    },
    selectFilterPropietario: function(cmp, evt, helper){
        
        helper.getUsuarios(cmp);
        var modalfilter = cmp.find('modalfilterPropietario');
        $A.util.removeClass(modalfilter,'slds-hide');
    },
    selectFilterCampanias: function(cmp, evt, helper){
        
        helper.getOptions(cmp);
        var modalfilter = cmp.find('modalfilterCampanias');
        $A.util.removeClass(modalfilter, 'slds-hide');
    },
    selectFilterEstadoOp: function(cmp, evt, helper){
        helper.getEstadoOportunidades(cmp);
        var modalfilter = cmp.find('modalfilterEstado');
        $A.util.removeClass(modalfilter, 'slds-hide');
        
    },
    selectFilterEstado: function(cmp, evt, helper){
        
        helper.getOptions(cmp);
        var filterPanel = cmp.find('filter-panel');
        $A.util.toggleClass(filterPanel,'slds-is-open');
        var modalfilter = cmp.find('modalfilterEstado');
        $A.util.removeClass(modalfilter,'slds-hide');
        
    },
    selectFilter6: function(cmp, evt, helper){
        var filter6 = cmp.find('filter6');
        $A.util.toggleClass(filter6,'filter-selected');
        
        var toVisitFilter = cmp.get('v.toVisitFilter');
        cmp.set('v.toVisitFilter', (toVisitFilter)?false:true);
        
        helper.applyFilters(cmp, helper);
    },
    hideModalPropietario: function(cmp, evt, helper){
        
        var modalfilter = cmp.find('modalfilterPropietario');
        $A.util.addClass(modalfilter, 'slds-hide');
    },
    hideModalEstado: function(cmp, evt, helper){
        
        var modalfilter = cmp.find('modalfilterEstado');
        $A.util.addClass(modalfilter, 'slds-hide');
    },
    hideModalCampaign: function(cmp, evt, helper){
        
        var modalfilter = cmp.find('modalfilterCampanias');
        $A.util.addClass(modalfilter, 'slds-hide');
    },
    hideModalVisitas: function(cmp, evt, helper){
        var modalfilter = cmp.find('BI_G4C_visitasLista_id');
        $A.util.addClass(modalfilter, 'slds-hide');
    },
    hideModal: function(cmp, evt, helper){
        
        var modalfilter = cmp.find('modalfilter');
        $A.util.addClass(modalfilter, 'slds-hide');
    },
    handleLocationEvent: function(cmp, evt, helper){
        var eventid = evt.getParam('eventid');
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position){
                var lat = position.coords.latitude;
                var lon = position.coords.longitude;
                //console.log(lat+' '+lon);
                //by JB 29/07
                helper.setGeolocalization(cmp, eventid, lat+' '+lon);
            });
        }
        
    },
    openVisitas: function(cmp, evt, helper){
        var account = evt.getParam("account");
        var element = cmp.find('BI_G4C_visitasLista_id');
        $A.util.removeClass(element, 'slds-hide');
        
    },
    openError: function(cmp, evt, helper){
        var errorMessageHeader = evt.getParam("header");
        var errorMessageBody = evt.getParam("body");
        var type = evt.getParam("type");
        var element;
        cmp.set('v.errorMessageHeader',errorMessageHeader);
        cmp.set('v.errorMessageBody',errorMessageBody);
        if (type==='WARNING'){
            element = cmp.find('generic_info');
            $A.util.removeClass(element, 'slds-hide');
            
        }else{
            element = cmp.find('generic_error');
            $A.util.removeClass(element, 'slds-hide');
        }
    },  
    closeInfo: function(cmp, evt, helper){
        var element = cmp.find('generic_info');
        $A.util.addClass(element,'slds-hide');
    },
    closeError: function(cmp, evt, helper){
        var element = cmp.find('generic_error');
        $A.util.addClass(element,'slds-hide');
    },
    doNewFilter: function(cmp, evt, helper) {
        var menu = cmp.find('newFilter-panel');
        $A.util.toggleClass(menu,'slds-is-open');
        var menu2 = cmp.find('filter-panel');
        $A.util.removeClass(menu2,'slds-is-open');
    },
    doFilter: function (cmp, evt, helper){
        var menu = cmp.find('filter-panel');
        $A.util.toggleClass(menu,'slds-is-open');
        var menu2 = cmp.find('newFilter-panel');
        $A.util.removeClass(menu2,'slds-is-open');
    },
    doSelectedNewFilterOne: function(cmp, evt, helper) {
        helper.clearFilter(cmp);
        cmp.set('v.newfilter','misClientes');
        helper.checkReset(cmp,evt, helper);
    },
    doSelectedNewFilterTwo: function(cmp, evt, helper) {
        helper.clearFilter(cmp);
        cmp.set('v.newfilter','miembrosEquipo');
        helper.checkReset(cmp,evt, helper);
    },
    doSelectedNewFilterThree: function(cmp, evt, helper) {
        helper.clearFilter(cmp);
        cmp.set('v.newfilter','misClientesMiembrosEquipo');
        helper.checkReset(cmp,evt, helper);
    },
    doSelectedNewFilterFour: function(cmp, evt, helper) {
        helper.clearFilter(cmp);
        cmp.set('v.newfilter','todos');
        helper.checkReset(cmp,evt, helper);
    },
    doNext: function(cmp, evt, helper) {
        cmp.set('v.actualPag', 2);
        helper.newLimit(cmp);
    },
    doPrevious: function(cmp, evt, helper) {
        cmp.set('v.actualPag', 1);
        helper.newLimit(cmp);
    },
})