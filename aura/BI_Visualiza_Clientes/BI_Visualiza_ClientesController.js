({
    doInit: function(component, event, helper) {
       // alert('hola q tal');
    },
    verUno: function(component, event, helper) {
        var laCuenta = event.getSource().get("v.value");
        component.set("v.accounts", laCuenta);
        console.log(laCuenta);
    },
    search:function(component, event, helper) {

     //   var nombre = component.find("nombre").get("v.value");
	//	var elId =   component.find("identif").get("v.value");
		var nombre = component.get("v.nombre");
        var elId = component.get("v.identif");
        console.log(nombre);
        console.log(elId);
        var construyeQuery = "";
        if(nombre != undefined && nombre.length > 0 )
          construyeQuery +=  " and name like '%" +nombre+ "%' " ;  
        
                           
        if(elId != undefined && elId.length > 0 )
          construyeQuery += " and BI_No_Identificador_fiscal__c like '%" +elId+ "%' " ;    
        
        // Request from server
        var action = component.get("c.getAccountList");
        action.setParams({
        "laQuery"  : construyeQuery 
        });

 
        
        action.setCallback(this, function(result){
            var accounts = result.getReturnValue();
            console.log(accounts);
            component.set("v.accounts", accounts);
            // Let DOM state catch up.
            window.setTimeout(
                $A.getCallback( function() {
                    // Now set our preferred value
                    //component.find("a_opt").set("v.value", accounts[4].Id);
                }));
        });
        $A.enqueueAction(action);
        
	}
})