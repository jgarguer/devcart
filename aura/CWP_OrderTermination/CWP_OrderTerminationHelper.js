({
    getFamilyPickList: function(component, event) {
        
        var self=this;
        var errorcmp=component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getFamilyPickList');
            var subtype = component.get('v.caseSubType');
            action.setParams({
                'subType': subtype
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            //component.set('v.familyList', result.getReturnValue());
            var res = result.getReturnValue();
            component.set('v.familyList', JSON.parse(res["caseFamilySO"]));
            component.set('v.serviceMapTermination', JSON.parse(res["caseServiceSO"]));
            component.set('v.serviceUnitMapTermination', JSON.parse(res["caseServiceUnitSO"]));
            var familyList = component.get('v.familyList');
            if(familyList.length == 1){
                component.set('v.case.TGS_Product_Tier_1__c', familyList[0].value);
                //self.getServicePickList(component, event);
                var serviceMap = JSON.parse(res["caseServiceSO"]);
                var serviceList = serviceMap[familyList[0].value];
                component.set('v.serviceList', serviceList);
                //helper.getServicePickList(component, event);
                document.getElementById('servicecombo').disabled = false;
                if(serviceList.length == 1){
                    component.set('v.case.TGS_Product_Tier_2__c', serviceList[0].value);
                    var serviceUnitMap = JSON.parse(res["caseServiceUnitSO"]);
                    var serviceUnitList = serviceUnitMap[serviceList[0].value];
                    component.set('v.serviceUnitList', serviceUnitList);
                    document.getElementById('serviceunitcombo').disabled = false;
                    if(serviceUnitList.length == 1){
                        component.set('v.case.TGS_Product_Tier_3__c', serviceUnitList[0].value);
                    }
                }else{
                    document.getElementById('serviceunitcombo').disabled = true;
                }
            }else{
                document.getElementById('servicecombo').disabled = true;
                document.getElementById('serviceunitcombo').disabled = true;
                //self.getServicePickList(component, event);
                
            }
            self.getProducts(component,event, JSON.parse(res["caseServiceUnitSO"])); 
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },
    
    getProducts: function(component, event, unitMap){
        var products=[];
        var keys=Object.keys(unitMap);
        for	(i=0;i<keys.length;i++){
            var objects= unitMap[keys[i]];
            for (j=0;j<objects.length;j++){
                products.push(('\'' + objects[j].label +  '\''));
            }
        }
        component.set('v.productNames', products.join(','));
        
    },
    
    getServicePickList: function(component, event) {
        var self = this;
        var errorcmp=component.find("errorMsg");
        var promise = new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get('c.getServicePickList');
            action.setParams({
                'familyName': component.get('v.case.TGS_Product_Tier_1__c')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        }));
        promise.then(function(result) {
            debugger;
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.serviceList', result.getReturnValue());
            var serviceList = component.get('v.serviceList');
            if(serviceList.length == 1){
                component.set('v.case.TGS_Product_Tier_2__c', serviceList[0].value);
                self.getServiceUnitPickList(component, event);
            }else{
                self.getServiceUnitPickList(component, event);
                document.getElementById('serviceunitcombo').disabled = true;
            }
            
        }, function(err) {
            
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.addClass(errorcmp, "toggle");
            }
        })
    },
    
    getServiceUnitPickList: function(component, event) {
        var errorcmp=component.find("errorMsg");
        var promise = new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get('c.getServiceUnitPickList');
            action.setParams({
                'serviceName': component.get('v.case.TGS_Product_Tier_2__c')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        }));
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.serviceUnitList', result.getReturnValue());
            var serviceUnitList = component.get('v.serviceUnitList');
            if(serviceUnitList.length == 1){
                component.set('v.case.TGS_Product_Tier_3__c', serviceUnitList[0].value);
            }
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.addClass(errorcmp, "toggle");
            }
        })
    },
    
    
    getLegalEntityPickList: function(component, event) {
        var self = this;
        var errorcmp=component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getLegalEntityPickList');
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.entityList', result.getReturnValue());
            var entityList = component.get('v.entityList');
            if(entityList.length == 1){
                component.set('v.case.CWP_Legal_Entity__c', entityList[0].value);
                self.getBusinessUnitPicklist(component, event);
                document.getElementById('cost').disabled = true;
            }else{
                document.getElementById('business').disabled = true;
                document.getElementById('cost').disabled = true;
            }
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },
    
    getBusinessUnitPicklist: function(component, event) {
        var self = this;
        var errorcmp=component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getBusinessUnitPicklist');
            action.setParams({
                'legalEntityValue': component.get('v.case.CWP_Legal_Entity__c')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.businessUnitList', result.getReturnValue());
            var businessUnitList = component.get('v.businessUnitList');
            if(businessUnitList.length == 1){
                component.set('v.case.CWP_Business_Unit__c', businessUnitList[0].value);
                document.getElementById('business').value = businessUnitList[0].value;
                self.getCostCenterPicklist(component, event);
                document.getElementById('cost').disabled = false;
            }else{
                document.getElementById('cost').disabled = true;
            }
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.addClass(errorcmp, "toggle");
            }
        })
    },
    
    getCostCenterPicklist: function(component, event) {
        var errorcmp=component.find("errorMsg");
        var promise = new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get('c.getCostCenterPicklist');
            action.setParams({
                'businessUnitValue': component.get('v.case.CWP_Business_Unit__c')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        }));
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.costCenterList', result.getReturnValue());
            var costCenterList = component.get('v.costCenterList');
            if(costCenterList.length == 1){
                component.set('v.case.CWP_Cost_Center__c', costCenterList[0].value);
            }
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.addClass(errorcmp, "toggle");
            }
        })
    },
    
    getCaseFromOrder: function(component, event) {
        debugger;
        var errorcmp=component.find("errorMsg");
        var promise = new Promise($A.getCallback(function(resolve, reject) { 
            var action = component.get('c.getCaseFromOrder');
            action.setParams({
                'orderName': component.get('v.result')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        }));
        var me = this;
        promise.then(function(result) {
            debugger;
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            var a = result.getReturnValue(); 
            component.set('v.lookupList', JSON.parse( a[0] ));
            var d = JSON.parse( a[1] );
            var serviceFamilyName=d.TGS_Categorization_tier_1__c;
            var service=d.TGS_Categorization_tier_2__c;
            serviceUnit=d.TGS_Categorization_tier_3__c;
            
            var sf =new Object();
            sf.value=serviceFamilyName;
            sf.label=serviceFamilyName;
            sf.escapeItem=false;
            sf.disabled=false;
            component.set('v.familyList',[sf]);
            var s =new Object();
            s.value=service;
            s.label=service;
            s.escapeItem=false;
            s.disabled=false;
            component.set('v.serviceList', [s]);
            
            var su =new Object();
            su.value=serviceUnit;
            su.label=serviceUnit;
            su.escapeItem=false;
            su.disabled=false;
            component.set('v.serviceUnitList', [su]);
            
            component.set('v.serviceUnitList', [su]);
            if (serviceFamilyName != null || serviceFamilyName !== '') {
                component.set('v.case.TGS_Product_Tier_1__c', serviceFamilyName);
                document.getElementById("familycombo").selectedIndex = 1;
            }
            if (service != null || service !== '') {
                component.set('v.case.TGS_Product_Tier_2__c', service);
                
                document.getElementById("servicecombo").selectedIndex = 1;
            }
            if (serviceUnit != null || serviceUnit !== '') {
                component.set('v.case.TGS_Product_Tier_3__c', serviceUnit);
                document.getElementById("serviceunitcombo").selectedIndex = 1;
            }
            
            //  me.setComboFromLookup(component, event);
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
        
    },
    
    setComboFromLookup: function(component, event) {
        debugger;
        alert('--');
        var combolist = component.get('v.lookupList');
        var serviceUnit = combolist.NE__CatalogItem__r.NE__ProductId__r.Name;
        var self = this;
        
        var errorcmp=component.find("errorMsg");
        var promise = new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get('c.getRegularizedProducts');
            action.setParams({'serviceUnit': serviceUnit});
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        }));
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            debugger;
            var d =result.returnValue;
            var serviceFamilyName=d.TGS_Categorization_tier_1__c;
            var service=d.TGS_Categorization_tier_2__c;
            serviceUnit=d.TGS_Categorization_tier_3__c;
            
            var sf =new Object();
            sf.value=serviceFamilyName;
            sf.label=serviceFamilyName;
            sf.escapeItem=false;
            sf.disabled=false;
            component.set('v.familyList',[sf]);
            var s =new Object();
            s.value=service;
            s.label=service;
            s.escapeItem=false;
            s.disabled=false;
            component.set('v.serviceList', [s]);
            
            var su =new Object();
            su.value=serviceUnit;
            su.label=serviceUnit;
            su.escapeItem=false;
            su.disabled=false;
            
            component.set('v.serviceUnitList', [su]);
            if (serviceFamilyName != null || serviceFamilyName !== '') {
                component.set('v.case.TGS_Product_Tier_1__c', serviceFamilyName);
                document.getElementById("familycombo").selectedIndex = 1;
            }
            if (service != null || service !== '') {
                component.set('v.case.TGS_Product_Tier_2__c', service);
                
                document.getElementById("servicecombo").selectedIndex = 1;
            }
            if (serviceUnit != null || serviceUnit !== '') {
                component.set('v.case.TGS_Product_Tier_3__c', serviceUnit);
                document.getElementById("serviceunitcombo").selectedIndex = 1;
            }
        },function(err) {
            debugger;
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.addClass(errorcmp, "toggle");
            }
        })
        
    },
    
    clearCombo: function(component, event) {
        debugger;
        this.getFamilyPickList(component, event);
        this.getServicePickList(component, event);
        this.getServiceUnitPickList(component, event);
        
        component.set('v.case.TGS_Product_Tier_1__c', 'none');
        component.set('v.case.TGS_Product_Tier_2__c', 'none');
        component.set('v.case.TGS_Product_Tier_3__c', 'none');
    }
})