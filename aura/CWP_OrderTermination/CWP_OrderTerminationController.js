({
    doInit: function(component, event, helper) {
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        helper.getFamilyPickList(component, event);
        //helper.getServicePickList(component, event);
        //helper.getServiceUnitPickList(component, event);
        helper.getLegalEntityPickList(component, event);
        helper.getBusinessUnitPicklist(component, event);
        helper.getCostCenterPicklist(component, event);
        component.find('servicecombo').set("v.disabled", true);
        component.find('serviceunitcombo').set("v.disabled", true);
    },
    
    subject: function(component, event, helper) {
        var subject = component.find("subjecttext").get("v.value");
        component.set("v.subject", subject);
    },
    
    textArea: function(component, event, helper) {
        var description = component.find("descriptiontext").get("v.value");
        component.set("v.description", description);
    },
    
    servicePickList: function(component, event, helper) {
        debugger;
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        var key= event.currentTarget.value;
        if(key=='none'){
            //helper.getServicePickList(component, event);  
            //component.find('servicecombo').set("v.disabled", true); 
            //component.find('serviceunitcombo').set("v.disabled", true);
            component.set('v.case.TGS_Product_Tier_1__c', 'none');
            component.set('v.case.TGS_Product_Tier_2__c', 'none');
            component.set('v.case.TGS_Product_Tier_3__c', 'none');
            document.getElementById('servicecombo').disabled = false;
            document.getElementById('serviceunitcombo').disabled = false;
            document.getElementById('familycombo').value = 'none';
            document.getElementById('servicecombo').value = 'none';
            document.getElementById('serviceunitcombo').value = 'none';
            document.getElementById('serviceunitcombo').label = $A.get("$Label.c.TGS_CWP_SELOPT");
            document.getElementById('servicecombo').disabled = true;
            document.getElementById('serviceunitcombo').disabled = true;
        }else{
            var valueSel = document.getElementById("familycombo").value;
            component.set('v.case.TGS_Product_Tier_1__c', valueSel);
            document.getElementById('servicecombo').disabled = false;
            //helper.getServicePickList(component, event); 
            var serviceMap = component.get('v.serviceMapTermination');
            var serviceList = serviceMap[key];
            component.set('v.serviceList', serviceList);
            document.getElementById('servicecombo').disabled = false;
            if(serviceList.length == 1){
                component.set('v.case.TGS_Product_Tier_2__c', serviceList[0].value);
                var serviceUnitMap = component.get('v.serviceUnitMapTermination');
                var serviceUnitList = serviceUnitMap[serviceList[0].value];
                component.set('v.serviceUnitList', serviceUnitList);
                document.getElementById('serviceunitcombo').disabled = false;
                var products=[];
                var objects= serviceUnitList;
                for (j=0;j<objects.length;j++){
                    products.push(('\'' + objects[j].label +  '\''));
                }
                component.set('v.productNames', products);
                
                component.set('v.productNames', products);
                if(serviceUnitList.length == 1){
                    component.set('v.case.TGS_Product_Tier_3__c', serviceUnitList[0].value);
                }
                
            }else{
                
                var products=[];
                var serviceUnitMap = component.get('v.serviceUnitMapTermination');
                debugger;
                for (var item in serviceList) {
                    var objects= serviceUnitMap[serviceList[item].label];
                    for (j=0;j<objects.length;j++){
                        products.push(('\'' + objects[j].label +  '\''));
                    }
                }
                debugger;
                component.set('v.productNames', products);
                document.getElementById('serviceunitcombo').value = 'none';
                document.getElementById('serviceunitcombo').disabled = true;
            }
            //component.find('servicecombo').set("v.disabled", false);
        }
    },
    
    serviceUnitPickList: function(component, event, helper) {
        debugger;
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        var key= event.currentTarget.value;
        var serviceUnitMap = component.get('v.serviceUnitMapTermination');
        if(key=='none'){
            //helper.getServiceUnitPickList(component, event);
            //component.find('serviceunitcombo').set("v.disabled", true);
            helper.getProducts(component,event, serviceUnitMap); 
            
            component.set('v.case.TGS_Product_Tier_2__c', 'none');
            component.set('v.case.TGS_Product_Tier_3__c', 'none');
            document.getElementById('serviceunitcombo').disabled = true;
            document.getElementById('servicecombo').value = 'none';
            document.getElementById('serviceunitcombo').value = 'none';
            
        }else{
            var valueSel = document.getElementById("servicecombo").value;
            component.set('v.case.TGS_Product_Tier_2__c', valueSel);
            var jsserviceUnitList= serviceUnitMap[key];
            
            
            component.set('v.serviceUnitList', jsserviceUnitList); 
            document.getElementById('serviceunitcombo').disabled = false;
            if(jsserviceUnitList.length==1){
                component.set('v.case.TGS_Product_Tier_3__c', jsserviceUnitList[0].value);
                document.getElementById("serviceunitcombo").value= jsserviceUnitList[0].value;
            }
            debugger;
            var products=[];
            var objects= serviceUnitMap[key];
            for (j=0;j<objects.length;j++){
                products.push(('\'' + objects[j].label +  '\''));
            }
            component.set('v.productNames', products);
        }
    }, 
    onCHPC: function(component, event, helper) {
        debugger;
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        var key= event.currentTarget.value;
        var serviceUnitMap = component.get('v.serviceMapTermination');
        if(key=='none'){
            key=component.get('v.case.TGS_Product_Tier_2__c');
            var products=[];
            var objects= serviceUnitMap[key];
            for (j=0;j<objects.length;j++){
                products.push(('\'' + objects[j].label +  '\''));
            }
            component.set('v.productNames', products);
        }else{
            var products=[];
            products.push(('\'' + key +  '\''));
            
            component.set('v.productNames', products);
        }
    },
    legalEntityPickList: function(component, event, helper) {
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        helper.getLegalEntityPickList(component, event);
    },
    
    businessUnitPickList: function(component, event, helper) {
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        var valueSel = document.getElementById("entity").value;
        component.set('v.case.CWP_Legal_Entity__c', valueSel);
        if(valueSel == 'none'){
            document.getElementById('business').disabled = true;
            document.getElementById('cost').disabled = true;
            document.getElementById('business').value = 'none';
            document.getElementById('cost').value = 'none';
        }else{
            document.getElementById('business').disabled = false;
            document.getElementById('cost').disabled = true;
        }
        helper.getBusinessUnitPicklist(component, event);
    },
    
    costCenterPicklist: function(component, event, helper) {
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        var valueSel = document.getElementById("business").value;
        component.set('v.case.CWP_Business_Unit__c', valueSel);
        if(valueSel == 'none'){
            document.getElementById('cost').disabled = true;
            document.getElementById('cost').value = 'none';
        }else{
            document.getElementById('cost').disabled = false;
        }
        helper.getCostCenterPicklist(component, event);
    },
    
    getEventValue: function(cmp, event, helper) {
        var ShowResultValue = event.getParam('sObjectId');
        var Value = event.getParam('name');
        // set the handler attributes based on event data
        cmp.set('v.result', ShowResultValue);       
        if (Value === '') {
            //helper.clearCombo(cmp, event);
            helper.getFamilyPickList(cmp, event);
        } else {
            helper.getCaseFromOrder(cmp, event);
        }
        
    },
    disablePicklists : function(component, event, helper) {
        if(document.getElementById('familycombo').disabled == true){
            document.getElementById('familycombo').disabled = false;
            document.getElementById('familycombo').value = 'none';
            document.getElementById('servicecombo').value = 'none';
            document.getElementById('serviceunitcombo').value = 'none';
        }else{
            document.getElementById('familycombo').disabled = true;
        }
        if(document.getElementById('servicecombo').disabled == false){
            document.getElementById('servicecombo').disabled = true;
        }
        if(document.getElementById('serviceunitcombo').disabled == false){
            document.getElementById('serviceunitcombo').disabled = true;
        }
        
    }
})