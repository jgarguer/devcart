({
    openModal : function(cmp, evt, helper) {
        debugger;
        document.getElementById('backgroundmodal').style='';        
        document.getElementById("cuerpomail").style.display="block";
        document.getElementById("sendbuttonmail").style.display="";
        document.getElementById("estadomail").style.display="none";
        document.getElementById("estadofailmail").style.display="none";
        document.getElementById("bodyMsjId").style.border="1px solid #d8dde6"; 
        document.getElementById("subjectId").style.border="1px solid #d8dde6"; 
        document.getElementById("bodyMsjId").style.color="#333";
        document.getElementById("subjectId").style.color="#333";
        document.getElementById('backgroundmodal').style='';           
        document.getElementsByTagName('body')[0].classList='modal-open';
        document.getElementById('tlf-email').style.display='block';
        document.getElementById('tlf-email').classList.add('in');
        
        // si el valor es true mostramos/ocultamos la modal
        cmp.set("v.isButton", evt.getParam("isButton"));
        
        var idParam = evt.getParam("idContact");
        
        var action = cmp.get("c.getloadInfo");
        action.setParams({'idParam':idParam});
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                cmp.set("v.us", response.getReturnValue());
                
                var element = cmp.find('cwp_modal');
                $A.util.removeClass(element, 'slds-hide');
            }
        });
        $A.enqueueAction(action);
        
        var action2 = cmp.get("c.getMail");
        action2.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                cmp.set("v.mail", response.getReturnValue());
            }
        });
        $A.enqueueAction(action2);
        //INI - 05/04/2017 - MUR
        //Hide Subject and Body on open modal.
        cmp.find('bodyMsjId').getElement().value="";
        cmp.find('subjectId').getElement().value="";
        //FIN - 05/04/2017 - MUR
    },
    hideModal : function(cmp, evt, helper) {
        document.getElementById('backgroundmodal').style.display='none';
        debugger;
        if(document.getElementById('tlf-email').style.display=='block' && (document.getElementById('tlf-attachment').style.display=='none' || document.getElementById('tlf-attachment').style.display=='')){
             document.getElementsByTagName('body')[0].classList='';
        }else{
            document.getElementsByTagName('body')[0].classList='modal-open';
        }
       
        document.getElementById('tlf-email').style.display='none';
    },
    send : function(cmp,evt, helper) { 
        document.getElementById('backgroundmodal').style.display='block';
        var user = cmp.get('v.us');
        var bodyMsjId   = cmp.find('bodyMsjId').getElement().value;
        var subjectId   = cmp.find('subjectId').getElement().value;
        var mailFrom = cmp.get('v.mail');
        var mailTo = cmp.get('v.us.Email');
        if(bodyMsjId==''){
            document.getElementById("bodyMsjId").style.border="solid 2px red"; 
            document.getElementById("bodyMsjId").style.color="red"; 
        }
        if(subjectId==''){
            document.getElementById("subjectId").style.border="solid 2px red"; 
            document.getElementById("subjectId").style.color="red"; 
        }
        if(bodyMsjId!='' && subjectId!=''){
            
            var action = cmp.get("c.getSend");
            action.setParams({'emailTo':mailTo,
                              'emailFrom': mailFrom,
                              'emailSubject':subjectId,
                              'emailBody':bodyMsjId
                             });
            
            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS") {
                	document.getElementById('backgroundmodal').style.display='none';
                    document.getElementById("cuerpomail").style.display="none";
                    document.getElementById("estadomail").style.display="block";
                    document.getElementById("sendbuttonmail").style.display="none";
                }else{
                	document.getElementById('backgroundmodal').style.display='none';
                    document.getElementById("cuerpomail").style.display="none";
                    document.getElementById("estadofailmail").style.display="block";
                    document.getElementById("sendbuttonmail").style.display="none";
                }
            });
            $A.enqueueAction(action);
        }
    }
    
    
})