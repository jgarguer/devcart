({
    init: function(component, event, helper) {    	
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        helper.init(component, event);
        helper.getAccountSet(component, event);
        helper.getUrgencyPickList(component, event);
        helper.getImpactPickList(component, event);
        helper.getFamilyPickList(component, event);
        helper.getServicePickList(component, event);
        helper.getServiceUnitPickList(component, event);
        helper.getPriorityPickList(component, event);
    },
    servicePickList: function(component, event, helper) {
        component.set('v.error', '');
         var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
         var key= event.currentTarget.value;
         if(key=='none'){
        	 helper.getServicePickList(component, event);  
        	 //component.find('servicecombo').set("v.disabled", true); 
        	 //component.find('serviceunitcombo').set("v.disabled", true);
        	 component.set('v.case.TGS_Product_Tier_1__c', 'none');
        	 component.set('v.case.TGS_Product_Tier_2__c', 'none');
        	 component.set('v.case.TGS_Product_Tier_3__c', 'none');
        	 document.getElementById('servicecombo').disabled = true;
        	 document.getElementById('serviceunitcombo').disabled = true;
        	 document.getElementById('familycombo').value = 'none';
        	 document.getElementById('servicecombo').value = 'none';
        	 document.getElementById('serviceunitcombo').value = 'none';
         }else{
	       var serviceMap = component.get('v.serviceMap');
	       component.set('v.servicePickList', serviceMap[key]);
           
	       //component.find('servicecombo').set("v.disabled", false);
	       document.getElementById('servicecombo').disabled = false;
	       helper.setCombos(component, event);
         }
       
             
    },
    serviceUnitPickList: function(component, event, helper) {
        component.set('v.error', '');
         var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
         var key= event.currentTarget.value;
         var serviceUnitMap = component.get('v.serviceUnitMap');
         if(key=='none'){
        	  helper.getServiceUnitPickList(component, event);
        	 //component.find('serviceunitcombo').set("v.disabled", true);
        	 helper.getProducts(component,event, serviceUnitMap); 
        	 component.set('v.case.TGS_Product_Tier_2__c', 'none');
        	 component.set('v.case.TGS_Product_Tier_3__c', 'none');
        	 document.getElementById('serviceunitcombo').disabled = true;
        	 document.getElementById('servicecombo').value = 'none';
        	 document.getElementById('serviceunitcombo').value = 'none';
         }else{
	       var valueSel = document.getElementById("servicecombo").value;
           component.set('v.case.TGS_Product_Tier_2__c', valueSel);
	       component.set('v.serviceUnitPickList', serviceUnitMap[key]);       
	       //component.find('serviceunitcombo').set("v.disabled", false);
	       document.getElementById('serviceunitcombo').disabled = false;
       
	       var products=[];
	       var objects= serviceUnitMap[key];
             
	       for (j=0;j<objects.length;j++){
	        		products.push(('\'' + objects[j].label +  '\''));
	       }
             if(products.legth>1)
                 document.getElementById('serviceunitcombo').disabled = false;
             
	        component.set('v.productNames', products);
	        helper.setCombos(component, event);
         }     
    },
    loadData: function(component, event, helper) {
        component.set('v.attach',event.getParam('attachmentList'));

    },
    saveController: function(component, event, helper) {
        document.getElementById('backgroundmodalwait').style.display='';
        helper.save(component, event);
    },
    getEventValue: function(component, event, helper) {
        var resultValue = event.getParam('sObjectId');
        var Value = event.getParam('name');
        component.set('v.case.TGS_Customer_Services__c', resultValue);
        component.set('v.result', resultValue);
        if (Value === '') {
            helper.clearCombo(component, event);
        } else {
            helper.getCaseFromOrder(component, event);
        }
    },
    close: function(component, event, helper) {
    		var evt = $A.get('e.c:CWP_BackModal');
    		evt.fire();
            
    },
    closeModal: function(component, event, helper) {
    	var options=['incident', 'change', 'billingInquiry', 'query', 'complaint', 'creation', 'termination'];
       options.forEach(function(option) {
		 	component.set('v.'+option, false);
           if (document.getElementById(option) != null){
               document.getElementById(option).classList.remove("in");
               document.getElementById(option).style.display='none';               
           }
       });

      if(document.getElementById("backgroundmodalwait")!=null){
          document.getElementById("backgroundmodalwait").style.display='none';
       }
       document.getElementById('backgroundmodal').style.display='none';
       document.getElementsByTagName('body')[0].classList.remove('modal-open');            
    }
    
    
})