({
    MAX_FILE_SIZE: 4500000,
    /* 6 000 000 * 3/4 to account for base64 */
    CHUNK_SIZE: 450000,
    /* Use a multiple of 4 */
    
    filesToUpload : 0,
    filesUploaded : 0,
    init: function(component, event) {
        var errorcmp = component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.doInit');
            action.setParams({
                'caseType': component.get('v.caseType'),
                'caseSubType': component.get('v.caseSubType'),
                'inputCase': component.get('v.case')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            });
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.case.Type', result.getReturnValue().Type);
            component.set('v.case.RecordTypeId', result.getReturnValue().RecordTypeId);
            component.set('v.case.sobjectType', 'Case');
            component.set('v.case.status', 'Assigned');
            component.set('v.case.Origin', 'Customer Web Portal');
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },
    getAccountSet: function(component, event) {
        var errorcmp = component.find("errorMsg");
        var listaIDs = [];
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.accountList');
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            });
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.accountList', result.getReturnValue());
            listaIDs = component.get('v.accountList');
            listaIDs = listaIDs.join('\',\'');
            component.set('v.idList', listaIDs);
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },
    
    getPriorityPickList: function(component, event) {
        var errorcmp = component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getPriorityPickList');
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            });
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.priorityList', result.getReturnValue());
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },
    
    getUrgencyPickList: function(component, event) {
        var errorcmp = component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getUrgencyPickList');
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            });
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.urgencyList', result.getReturnValue());
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },
    getImpactPickList: function(component, event) {
        var errorcmp = component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getImpactPickList');
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.impactList', result.getReturnValue());
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },
    getFamilyPickList: function(component, event) {
        var me = this;
        var errorcmp = component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getFamilyPickList');
            var subtype = component.get('v.caseSubType');
            action.setParams({
                'subType': subtype
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            
            var res = result.getReturnValue();
            component.set('v.familyPickList', JSON.parse(res["caseFamilySO"]));
            component.set('v.serviceMap', JSON.parse(res["caseServiceSO"]));
            component.set('v.serviceUnitMap', JSON.parse(res["caseServiceUnitSO"]));
            var familyList = component.get('v.familyPickList');
            if(familyList.length == 1){
                component.set('v.case.TGS_Product_Tier_1__c', familyList[0].value);
                document.getElementById('familycombo').disabled = true;
                
                var service = component.get('v.serviceMap');
                if(service[Object.getOwnPropertyNames(service)[0]].length == 1){
                    component.set('v.case.TGS_Product_Tier_2__c', service[Object.getOwnPropertyNames(service)[0]].value);
                    document.getElementById('serviceunitcombo').disabled = false;
                }else{
                    document.getElementById('serviceunitcombo').disabled = true;
                }
                
            }else{
                document.getElementById('servicecombo').disabled = true;
                document.getElementById('serviceunitcombo').disabled = true;
            }
            me.getProducts(component, event, JSON.parse(res["caseServiceUnitSO"]));
            
            
            
            //component.set('v.familyPickList', result.getReturnValue());
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },
    
    getProducts: function(component, event, unitMap) {
        var products = [];
        var keys = Object.keys(unitMap);
        for (i = 0; i < keys.length; i++) {
            var objects = unitMap[keys[i]];
            for (j = 0; j < objects.length; j++) {
                products.push(('\'' + objects[j].label + '\''));
            }
        }
        component.set('v.productNames', products.join(','));
        
    },
      
    getServicePickList: function(component, event) {
        var errorcmp = component.find("errorMsg");
        var family = component.get('v.case.TGS_Product_Tier_1__c');
        if (family != null || family !== '' || family !== undefined) {
            var promise = new Promise(function(resolve, reject) {
                var action = component.get('c.getServicePickList');
                action.setParams({
                    'familyName': family
                });
                action.setCallback(this, function(result) {
                    if (result.getState() === 'SUCCESS') {
                        resolve(result);
                    } else {
                        reject(result);
                    }
                })
                $A.enqueueAction(action);
            });
            promise.then(function(result) {
                component.set('v.error', '');
                $A.util.addClass(errorcmp, "toggle");
                component.set('v.servicePickList', result.getReturnValue());
            }, function(err) {
                var errors = err.getError();
                if (errors[0] && errors[0].message) {
                    component.set("v.error", errors[0].message);
                    $A.util.addClass(errorcmp, "toggle");
                }
            })
        } else {
            var promise1 = new Promise(function(resolve, reject) {
                var action = component.get('c.getServicePickList');
                action.setParams({
                    'familyName': component.get('v.case.TGS_Product_Tier_1__c')
                });
                action.setCallback(this, function(result) {
                    if (result.getState() === 'SUCCESS') {
                        resolve(result);
                    } else {
                        reject(result);
                    }
                })
                $A.enqueueAction(action);
            });
            promise1.then(function(result) {
                component.set('v.error', '');
                $A.util.addClass(errorcmp, "toggle");
                component.set('v.servicePickList', result.getReturnValue());
            }, function(err) {
                var errors = err.getError();
                if (errors[0] && errors[0].message) {
                    component.set("v.error", errors[0].message);
                    $A.util.addClass(errorcmp, "toggle");
                }
            })
        }
    },
    
    getServiceUnitPickList: function(component, event) {
        var me = this;
        var errorcmp = component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getServiceUnitPickList');
            action.setParams({
                'familyName': component.get('v.case.TGS_Product_Tier_1__c'),
                'serviceName': component.get('v.case.TGS_Product_Tier_2__c')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.serviceUnitPickList', result.getReturnValue());
            me.setCombos(component, event);
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.addClass(errorcmp, "toggle");
            }
        })
    },
    setCombos: function(component, event) {
        if (component.get('v.familyPickList')!= null && component.get('v.familyPickList').length == 1) {
            var serviceFamilyName = component.get('v.familyPickList')[0].value;
            component.set('v.case.TGS_Product_Tier_1__c', serviceFamilyName);
            document.getElementById('familycombo').disabled = true;
            var serviceMap = component.get('v.serviceMap');
            component.set('v.servicePickList', serviceMap[serviceFamilyName]);
        }
        if (component.get('v.servicePickList')!= null && component.get('v.servicePickList').length == 1) {
            var service = component.get('v.servicePickList')[0].value;
            component.set('v.case.TGS_Product_Tier_2__c', service);
            var serviceUnitMap = component.get('v.serviceUnitMap');
            component.set('v.serviceUnitPickList', serviceUnitMap[service]);
              document.getElementById("servicecombo").options[1].selected=true;
            document.getElementById('servicecombo').disabled = true;
            var products = [];
            var objects = serviceUnitMap[service];
            for (j = 0; j < objects.length; j++) {
                products.push(('\'' + objects[j].label + '\''));
            }
            component.set('v.productNames', products);
            document.getElementById('serviceunitcombo').disabled = false;
        }
        if (component.get('v.serviceUnitPickList')!= null && component.get('v.serviceUnitPickList').length == 1) {
            var serviceUnit = component.get('v.serviceUnitPickList')[0].value;
            component.set('v.case.TGS_Product_Tier_3__c', serviceUnit);
            document.getElementById("serviceunitcombo").options[1].selected=true;
            document.getElementById('serviceunitcombo').disabled = true;
        }
    },
    
    getCaseFromOrder: function(component, event) {
        var errorcmp = component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getCaseFromOrder');
            action.setParams({
                'orderName': component.get('v.result')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        var me = this;
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.lookupList', result.getReturnValue());
            me.setComboFromLookup(component, event);
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
        
    },
    
    setComboFromLookup: function(component, event) {
        var combolist = component.get('v.lookupList');
        
        //var serviceFamilyName = combolist.NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name;
        //var service = combolist.NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name;
        
        
        var d =JSON.parse( combolist[1]);
        var serviceFamilyName=d.TGS_Categorization_tier_1__c;
        var service=d.TGS_Categorization_tier_2__c;
        serviceUnit=d.TGS_Categorization_tier_3__c;
        var sf =new Object();
        sf.value=serviceFamilyName;
        sf.label=serviceFamilyName;
        sf.escapeItem=false;
        sf.disabled=false;
        component.set('v.familyList',[sf]);
        var s =new Object();
        s.value=service;
        s.label=service;
        s.escapeItem=false;
        s.disabled=false;
        component.set('v.serviceList', [s]);
        
        var su =new Object();
        su.value=serviceUnit;
        su.label=serviceUnit;
        su.escapeItem=false;
        su.disabled=false;
        
        component.set('v.serviceUnitList', [su]);
        if (serviceFamilyName != null || serviceFamilyName !== '') {
            component.set('v.case.TGS_Product_Tier_1__c', serviceFamilyName);
            //document.getElementById("familycombo").selectedIndex = 1;
        }
        if (service != null || service !== '') {
            component.set('v.case.TGS_Product_Tier_2__c', service);
            
            //document.getElementById("servicecombo").selectedIndex = 1;
        }
        if (serviceUnit != null || serviceUnit !== '') {
            component.set('v.case.TGS_Product_Tier_3__c', serviceUnit);
            //document.getElementById("serviceunitcombo").selectedIndex = 1;
        }
        document.getElementById('familycombo').disabled = true;
        document.getElementById('servicecombo').disabled = true;
        document.getElementById('serviceunitcombo').disabled = true;
        
    },
    
    clearCombo: function(component, event) {
        component.set('v.case.TGS_Product_Tier_1__c', 'none');
        component.set('v.case.TGS_Product_Tier_2__c', 'none');
        component.set('v.case.TGS_Product_Tier_3__c', 'none');
        //component.find('familycombo').set("v.disabled", false);
    },
    
    save: function(component, event) {
        
        var evt = $A.get('e.c:CWP_BackModal');
        var errorcmp = component.find("errorMsg");
        var adjuntos = component.get('v.attach');
        var setAtt = false;
        if (adjuntos.length > 0){
            setAtt = true;
        }
        this.filesToUpload = adjuntos.length;
        console.log('adjuntos: ' + adjuntos);
        var action = component.get('c.doSave');
        var childIsCalled = component.get('v.childIsCalled');
        
        var subject = 'required';
        var description = 'required';
        var tier1 = 'required';        
        var tier2 = 'required';
        var tier3 = 'required';
        var tier1picklist = 'required';
        var tier2picklist = 'required';
        var tier3picklist = 'required';
        var impact = '';
        var urgency = '';
        var entity = '';
        var business = '';
        var evaluate = '1';
        
        if(document.getElementById('subject')){
            subject = document.getElementById('subject').value;
        }
        if(document.getElementById('description')){
            description = document.getElementById('description').value;
        }
        if(document.getElementById('familycombo')){
            tier1 = document.getElementById('familycombo').value;        
        }
        if(document.getElementById('servicecombo')){
            tier2 = document.getElementById('servicecombo').value;
        }
        if(document.getElementById('serviceunitcombo')){
            tier3 = document.getElementById('serviceunitcombo').value;
        }
        if(document.getElementById('impact')){
            impact = document.getElementById('impact').value;
        }
        if(document.getElementById('urgency')){
            urgency = document.getElementById('urgency').value;
        }
        if(document.getElementById('entity')){
            entity = document.getElementById('entity').value;
        }
        if(document.getElementById('business')){
            business = document.getElementById('business').value;
        }
        if(document.getElementById('tier1picklist')){
            tier1picklist = document.getElementById('tier1picklist').value;
        }
        if(document.getElementById('tier2picklist')){
            tier2picklist = document.getElementById('tier2picklist').value;
        }
        if(document.getElementById('tier3picklist')){
            tier3picklist = document.getElementById('tier3picklist').value;
        }
        if(subject==''){
            document.getElementById("subject").style.border="solid 2px red"; 
            document.getElementById("subject").style.color="red"; 
            document.getElementById("backgroundmodalwait").style.display='none';
            evaluate = '0';
        }
        if(description==''){
            document.getElementById("description").style.border="solid 2px red"; 
            document.getElementById("description").style.color="red"; 
            document.getElementById("backgroundmodalwait").style.display='none';
            evaluate = '0';
        }
        if(tier1=='none' && childIsCalled!= 'ordercreation' && childIsCalled!= 'orderetermination'){
            document.getElementById("familycombo").style.border="solid 2px red"; 
            document.getElementById("familycombo").style.color="red"; 
            document.getElementById("backgroundmodalwait").style.display='none';
            evaluate = '0';
        }
        if(tier2=='none' && childIsCalled!= 'ordercreation' && childIsCalled!= 'orderetermination'){
            document.getElementById("servicecombo").style.border="solid 2px red"; 
            document.getElementById("servicecombo").style.color="red"; 
            document.getElementById("backgroundmodalwait").style.display='none';
            evaluate = '0';
        }
        if(tier3=='none' && childIsCalled!= 'ordercreation' && childIsCalled!= 'orderetermination'){
            document.getElementById("serviceunitcombo").style.border="solid 2px red"; 
            document.getElementById("serviceunitcombo").style.color="red"; 
            document.getElementById("backgroundmodalwait").style.display='none';
            evaluate = '0';
        }
        if(tier1picklist=='none'){
            document.getElementById("tier1picklist").style.border="solid 2px red"; 
            document.getElementById("tier1picklist").style.color="red"; 
            document.getElementById("backgroundmodalwait").style.display='none';
            evaluate = '0';
        }
        if(tier2picklist=='none'){
            document.getElementById("tier2picklist").style.border="solid 2px red"; 
            document.getElementById("tier2picklist").style.color="red"; 
            document.getElementById("backgroundmodalwait").style.display='none';
            evaluate = '0';
        }
        if(tier3picklist=='none'){
            document.getElementById("tier3picklist").style.border="solid 2px red"; 
            document.getElementById("tier3picklist").style.color="red"; 
            document.getElementById("backgroundmodalwait").style.display='none';
            component.set('v.case.TGS_Op_Commercial_Tier_3__c',tier3picklist);
            evaluate = '0';
        }
        if (evaluate == '1'){
            if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='block';
            }
            if(tier1 != 'required'){component.set('v.case.TGS_Product_Tier_1__c',tier1)};
            if(tier2 != 'required'){component.set('v.case.TGS_Product_Tier_2__c',tier2)};
            if(tier3 != 'required'){component.set('v.case.TGS_Product_Tier_3__c',tier3)};
            if(tier1picklist != 'required'){component.set('v.case.TGS_Op_Commercial_Tier_1__c',tier1picklist)};
            if(tier2picklist != 'required'){component.set('v.case.TGS_Op_Commercial_Tier_2__c',tier2picklist)};
            if(tier3picklist != 'required'){component.set('v.case.TGS_Op_Commercial_Tier_3__c',tier3picklist)};
            if(urgency != 'none'){component.set('v.case.TGS_Urgency__c',urgency)};
            if(impact != 'none'){component.set('v.case.TGS_Impact__c',impact)};
            if(entity != 'none'){component.set('v.case.TGS_CWP_Legal_Entity__c',entity)};
            if(business != 'none'){component.set('v.case.TGS_Business_Unit',business)};
            action.setParams({
                'caseType': component.get('v.caseType'),
                'caseSubType': component.get('v.caseSubType'),
                'subject': subject,
                'description': description,
                'paramCase': component.get('v.case'),
                'attachmentList': setAtt
            });
            var self = this;
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    //alert('Caso insertado');
                   
                    document.getElementById('messageNewCase').innerHTML=result.getReturnValue();
                    document.getElementById('tlf-case-ok').classList.add('in');
                    document.getElementById('tlf-case-ok').style.display='block';
                    var casoaux = result.getReturnValue().split(' ');
                    var caso = casoaux[casoaux.length - 1];
                    
                    var actionInt = component.get("c.integraCase");
                    actionInt.setParams({ "cNumber" : caso });
                    actionInt.setCallback(this, function(response){
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            var caso = response.getReturnValue();
                            if(adjuntos.length >0){
                                NProgress.start();
                                for (var i=0; adjuntos.length > i; i++) {
                                    self.savefile(adjuntos[i], caso, component);
                                }
                            }else{
                                if(document.getElementById("backgroundmodalwait")!=null){
                                    document.getElementById("backgroundmodalwait").style.display='none';
                                }
                            }
                            
                        }
                        
                    });
                    $A.enqueueAction(actionInt); 
                    
                }else{
                    alert('error generando el caso');
                    document.getElementById("backgroundmodalwait").style.display='none';
                }
            })
            $A.enqueueAction(action);
        }
        
    },
    
    uploadChunk: function(file, fileContents, fromPos, toPos, attachId, caso, component) {
        var action = component.get("c.saveTheChunk");
        var chunk = fileContents.substring(fromPos, toPos);
        console.log("uploadChunk: "+fromPos+" - "+ toPos);
        action.setParams({
            fileName: file.name,
            fileDescription: file.desc,
            base64Data: encodeURIComponent(chunk),
            contentType: file.type,
            fileId: attachId,
            caso: caso
        });
        
        var self = this;
        action.setCallback(this, function(a) {
            attachId = a.getReturnValue();
            fromPos = toPos;
            toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);
            if (fromPos < toPos) {
                self.uploadChunk(file, fileContents, fromPos, toPos, attachId, caso, component);
            }else{
                	self.filesUploaded++;
                    var actionRoD = component.get("c.getIntegraRoD");
                    actionRoD.setParams({
                        attachId: attachId
                    });
                    actionRoD.setCallback(this, function(response) {
                        if(component.isValid() && response.getState() == "SUCCESS"){                
                            var res = response.getReturnValue();
                        }                
                    });
                    $A.enqueueAction(actionRoD);
                if (self.filesToUpload == self.filesUploaded){
					if(document.getElementById("backgroundmodalwait")!=null){
                    	document.getElementById("backgroundmodalwait").style.display='none';
                    }     
                    NProgress.done();
                    self.filesUploaded=0;
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    savefile: function(file, caso, component) {
        if (file.size > this.MAX_FILE_SIZE) {
            alert('File size cannot exceed ' + this.MAX_FILE_SIZE + ' bytes.\n' +
                  'Selected file size: ' + file.size);
            NProgress.done();
            document.getElementById("backgroundmodalwait").style.display='none';
            return;
        }
        
        var fileContents = file.fileContents;
        
        var fromPos = 0;
        var toPos = Math.min(fileContents.length, fromPos + this.CHUNK_SIZE);
        
        // start with the initial chunk
        this.uploadChunk(file, fileContents, fromPos, toPos, '', caso, component);
    }
})