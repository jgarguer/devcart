({
    doInit: function(component) {
        // Set the attribute value. 
        // You could also fire an event here instead.
        // 
       // document.getElementById("backgroundmodalwaitMP").style.display='';
        var action = component.get("c.getResumeList");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var obj=response.getReturnValue();
                
                var arrRet = new Array();
                for (var i = 0; i < Object.getOwnPropertyNames(obj).length; i++) {
                    var varitem=Object.getOwnPropertyNames(obj)[i];
                    var arrAux = new Array();                    
                    for (var j = 0; j< Object.getOwnPropertyNames(obj[varitem]).length; j++) {
                        var varitem2=Object.getOwnPropertyNames(obj[varitem])[j];                  
                        for (var k = 0; k< Object.getOwnPropertyNames(obj[varitem][varitem2]).length; k++) {
                            var objA ={"Name":Object.getOwnPropertyNames(obj)[i],
                                       "Name2":Object.getOwnPropertyNames(obj[varitem])[j],
                                       "Name3":Object.getOwnPropertyNames(obj[varitem][varitem2])[k],
                                       "Name4":obj[varitem][varitem2][Object.getOwnPropertyNames(obj[varitem][varitem2])[k]]};
                            arrAux.push(objA);
                        }
                    };                    
                    arrRet.push(arrAux);
                };
                component.set("v.resumeList", arrRet);
                document.getElementById("backgroundmodalwaitMP").style.display='none';
               // document.getElementById("backgroundmodalwaitMP").style.display='none';
            }
        });  
        $A.enqueueAction(action);
    },
    
    setPicklistsValues: function(cmp, evt, helper) {
        debugger
        var idx = evt.target.id;
		var cat= idx.split('$-$');
        var eventL = $A.get("e.c:CWP_IS_FromSummary");
        eventL.setParams({
            "isFromSummary":true, 
            "serviceUnit1": cat[0], 
            "serviceUnit2": cat[1],
            "serviceUnit3": cat[2]});
		eventL.fire();
    },
    handleClickEvt: function(cmp, event, helper) {
        debugger;
        var evtRec= event.getParam('isFromSummary');
        if(evtRec=== true){
            document.getElementById('CWP_IS_Resume_div').style.display='none';
        }else {
            document.getElementById('CWP_IS_serviceSelected_div').style.display='none';
            document.getElementById('CWP_IS_Resume_div').style.display='block';
        }
    }
    
})