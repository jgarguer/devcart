({
    doInit: function(component, event, helper) {
        
        helper.getMapVisibility(component, event);
        
    },
    
    setButtonVisiblility: function(component, event, helper) {
        
        var buttonVisibility = component.get('c.getButtonVisibility');
        buttonVisibility.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var booleanMap = response.getReturnValue();
                component.set("v.showService", booleanMap['showService']);
                component.set("v.showDocumentation", booleanMap['showDocumentation']);
                component.set("v.showBilling", booleanMap['showBilling']);
                component.set("v.showMarket", booleanMap['showMarket']);
            }
        });
        $A.enqueueAction(buttonVisibility);
        
    },
    
    setMainImage: function(component, event) {
        var imgUrl;
        var mainInfo = component.get('c.getMainImage');
        /* component.set("v.informationMenu", '{!$label.c.PCA_My_Service_menu}');
         component.set("v.title", '{!$label.c.PCA_MyServiceText}');*/
        var urlDomain = component.get("v.urlimg");
        mainInfo.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var mainInfo = response.getReturnValue();
                imgUrl = "url('" + urlDomain + "/servlet/servlet.FileDownload?file=" + mainInfo['imgId'] + "')";
                setTimeout(
                    function() {
                        if (mainInfo['imgId'] != undefined) {
                            document.getElementById('mainImage').style.backgroundImage = imgUrl;
                            document.getElementById('mainImage').style.padding = "2.5rem 4.0625rem";
                            document.getElementById('mainImage').style.minHeight = "21.875rem";
                            document.getElementById('mainImage').style.backgroundPosition = "center"
                        } else {
                            document.getElementById('mainImage').className = "tlf-slide";
                            
                        }
                        if (mainInfo['informationMenu'] != undefined) {
                            component.set("v.informationMenu", mainInfo['informationMenu']);
                        }
                        if (mainInfo['title'] != undefined) {
                            component.set("v.title", mainInfo['title']);
                        }
                    }, 1);
            }
        });
        $A.enqueueAction(mainInfo);
        
    },
    
    changeTab: function(component, event, helper) {
        event.preventDefault();
        var a = event.currentTarget.id;
        var mapURLs = {};
        mapURLs['pca_documentmanagement'] = 'cwp-documentation';
        mapURLs['pca_facturacioncobranza'] = 'cwp-billing';
        mapURLs['pca_catalogo'] = 'cwp-market-place-menu';
        if (a == 'pca_inventory') {
            var action = component.get('c.getUser');
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    component.set("v.isTGS", response.getReturnValue());
                    var esTGS = component.get('v.isTGS');
                    if (esTGS) {
                        mapURLs['pca_inventory'] = 'cwp-installed-services';
                        
                    } else {
                        mapURLs['pca_inventory'] = 'cwp-services';
                    }
                    
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "/" + mapURLs[a]
                    });
                    urlEvent.fire();
                }
            });
            $A.enqueueAction(action);
            
        } else {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/" + mapURLs[a]
            });
            urlEvent.fire();
        }
        
    },
    openOneDrive: function(cmp, event, helper) {
        debugger;
        event.preventDefault();
        var link = cmp.get('v.oneDriveLink');
        var urlEvent = $A.get("e.force:navigateToURL");
        if(link.indexOf("http")==-1)
            link= "https://"+link;
        urlEvent.setParams({
            "url": link
        });
        urlEvent.fire();
    }
})