/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis
Company:       Salesforce.com
Description:   Test Class that manage coverage of PCA_NewCaseCtrl Class

History: 

<Date>                     <Author>                <Change Description>
4/3/2017                  Everis             Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
({
    checkAt : function(component, event, helper) {
        var passwordOk = component.get("v.passwordOk");
        if(passwordOk){
            $("#errorUserPass").hide();
            var usuario = $("#username").val();
            var pass = $("#password").val();
        	helper.login(component, usuario, pass);
        }
        if($("#username").val().indexOf('@')>-1){
            $("#passwordDiv").show();
             $("#newPassword").show();
            
        	component.set("v.passwordOk",true);
        }else{
            if($("#username").val() != ''){
                var redireccion = $("#urlOracle").text();
                redireccion += '?sflogin='+$("#username").val();
                window.location.href= redireccion;
            }
        }
    },
    
    forgotPassword : function(component, event, helper){
        var usuario = $("#usernameForgot").val();
        helper.forgotPass(component, usuario);
    },
    
    afterScriptsLoaded : function(){
        $("#passwordDiv").hide();
        document.title = "CWP_Community";
        $("#AppBodyHeader").hide();
        $(".clearingBox").hide();
        $(".zen").hide();
        $("#app").show();
        
    },
    
    doInit : function(component, event, helper){
        
    },
    
    changeModal : function(){
        $("#vistaLogin").hide(); 
        $("#vistaFPass").show();
        $("#usernameForgot").val($("#username").val()); 
    },
    
    changeModal2 : function(){
        $("#username").val($("#usernameForgot").val());
        $("#resetEnviado").hide();
        $("#errorUserForgot").hide();
        $("#vistaLogin").show();
        $("#vistaFPass").hide();
        $("#password").val('');
        $("#errorUserPass").hide();
    }
    
})