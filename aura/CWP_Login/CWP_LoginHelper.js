/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis
Company:       Salesforce.com
Description:   Test Class that manage coverage of PCA_NewCaseCtrl Class

History: 

<Date>                     <Author>                <Change Description>
4/3/2017                  Everis             Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
({

    toBase64 : function(str) {
        return window.btoa(unescape(encodeURIComponent(str)));
    },

    login : function(component, username, pass){
        var action = component.get("c.getlogin");
        action.setParams({"username" : username, "password" : pass});
        action.setCallback(this, function(response){
            var state = response.getState();
            var url = response.getReturnValue();
            if(state === "SUCCESS"){
				if(url != null){
                	$("#errorUserPass").hide();
                	window.location.href= url;
            	}else{
                	$("#errorUserPass").show();
            }            }
        })
        $A.enqueueAction(action);
    },
    forgotPass : function(component, username){
        var action = component.get("c.getforgotPasswordApex");
        action.setParams({"usernameForgot" : username});
        action.setCallback(this, function(response){
            var state = response.getState();
            var Msg = response.getReturnValue();
            if(Msg){
            	$("#resetEnviado").show();
            	$("#errorUserForgot").hide();
        	}else{
            	$("#resetEnviado").hide();
            	$("#errorUserForgot").show();         
        	}
        })
        $A.enqueueAction(action);
    },
})