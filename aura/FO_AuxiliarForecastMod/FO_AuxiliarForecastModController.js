({	
	changeField : function(cmp, event, helper) {
        var err="";
        var campo = cmp.find("input-ui").get("v.value");
        $A.util.removeClass(cmp.find("input-ui"),"mintam");
        if(campo=="" || campo=="-"){
            cmp.find("input-ui").set("v.value","0");
            cmp.set("v.ValueFore","0");
            return;
        }
        
        debugger;
        if(campo==cmp.get("v.ValueFore"))
            return;
        
        if(!/^[0-9\-\.]+$/.test(campo) && campo!=null){
            var resultsToast = $A.get("e.force:showToast");
	            resultsToast.setParams({
    	            "title": "ERROR: ",
    		       	"message": $A.get("$Label.c.FO_Only_Numbers")+campo,
                    "type":"error"
        		});
            resultsToast.fire();
            //alert($A.get("$Label.c.FO_Only_Numbers")+campo);
            cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
            return;
        }
        var spliter = campo.toString().split(".");
        var ent= spliter[0];
        debugger;
       // var dec = spliter[1];
        if(ent.indexOf("-")==-1){
            if(ent.length>15){
                //alert($A.get("$Label.c.FO_Max_Length"));
                var resultsToast = $A.get("e.force:showToast");
	            resultsToast.setParams({
    	            "title": "ERROR: ",
    		       	"message": $A.get("$Label.c.FO_Max_Length"),
                    "type":"error"
        		});
            resultsToast.fire();
            	cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
            	return;
            }
        }else{
            if(ent.length>16){
               // alert($A.get("$Label.c.FO_Max_Length"));
                var resultsToast = $A.get("e.force:showToast");
	            resultsToast.setParams({
    	            "title": "ERROR: ",
    		       	"message": $A.get("$Label.c.FO_Max_Length"),
                    "type":"error"
        		});
            resultsToast.fire();
            	cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
            	return;
            }
        }
        if(Math.abs(campo)>Number(9999999999999999,99) ){
            //alert($A.get("$Label.c.FO_Max_Length"));
            var resultsToast = $A.get("e.force:showToast");
	            resultsToast.setParams({
    	            "title": "ERROR: ",
    		       	"message": $A.get("$Label.c.FO_Max_Length"),
                    "type":"error"
        		});
            resultsToast.fire();
            cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
            return;
        }
        if(cmp.get("v.Estado").indexOf("Enviado")!=-1){
            var resultsToast = $A.get("e.force:showToast");
	            resultsToast.setParams({
    	            "title": "ERROR: ",
    		       	"message": $A.get("$Label.c.FO_Err_Mod_Submited"),
                    "type":"error"
        		});
            resultsToast.fire();
            //alert($A.get("$Label.c.FO_Err_Mod_Submited"));
            cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
            return;
        }
        var fechSpl = new Date(cmp.get("v.FechaFin"));
        if(fechSpl<new Date()){
            var resultsToast = $A.get("e.force:showToast");
	            resultsToast.setParams({
    	            "title": "ERROR: ",
    		       	"message": $A.get("$Label.c.FO_Err_Past_Fore"),
                    "type":"error"
        		});
            resultsToast.fire();
            //alert($A.get("$Label.c.FO_Err_Past_Fore"));
            cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
            return;
        }
        
		var action = cmp.get("c.changeFore");
        action.setParams({"Id":cmp.get("v.Id_Forecast"),"value":campo,"manager":cmp.get("v.Manager")});
		action.setCallback(this,function(response){
			if(response.getState()==="SUCCESS"){
                var result = response.getReturnValue();
                if(result=="OK"){
                    debugger;
                	cmp.set("v.ValueFore",cmp.find("input-ui").get("v.value"));
                    var appEvent =  cmp.getEvent("FO_Event");
                    appEvent.setParams({"Id_per":cmp.get("v.Id_Periodo"),"Value_Fore":cmp.get("v.ValueFore"),"Manager_Fore":cmp.get("v.Manager")});
                    appEvent.fire();    
                }else{
                    cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
                    var resultsToast = $A.get("e.force:showToast");
	            	resultsToast.setParams({
    	            "title": "ERROR: ",
    		       	"message": $A.get("$Label.c.FO_Err_Upd_FO")+response.getReturnValue(),
                    "type":"error"
        			});
            		resultsToast.fire();
                    //alert($A.get("$Label.c.FO_Err_Upd_FO")+response.getReturnValue());
                }
        	    
            }else{
                cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
                var resultsToast = $A.get("e.force:showToast");
	            	resultsToast.setParams({
    	            "title": "ERROR: ",
    		       	"message": $A.get("$Label.c.FO_Err_Upd_FO")+" "+response.getError()[0].message,
                    "type":"error"
        			});
            		resultsToast.fire();
                //alert($A.get("$Label.c.FO_Err_Upd_FO")+" "+response.getError()[0].message);
            }                  
        });        
        $A.enqueueAction(action);
	},
    doInit : function(cmp,evt,helper){
        cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
        
    },
    actualizaCampo : function(cmp,event,helper){
        var campo = cmp.find("input-ui").get("v.value");
        if(campo=="" || campo=="-")
           return;
       if(!/^[0-9\-\.]+$/.test(campo) && campo!=null){
           alert($A.get("$Label.c.FO_Only_Numbers")+campo);
           cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
           return;
        }
        var spliter = campo.toString().split(".");
        var ent= spliter[0];
        debugger;
       // var dec = spliter[1];
        if(ent.indexOf("-")==-1){
            if(ent.length>15){
                alert($A.get("$Label.c.FO_Max_Length"));
            	cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
            	return;
            }
        }else{
            if(ent.length>16){
                alert($A.get("$Label.c.FO_Max_Length"));
            	cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
            	return;
            }
        }
        if(Math.abs(campo)>Number(9999999999999999,99) ){
            alert($A.get("$Label.c.FO_Max_Length"));
            cmp.find("input-ui").set("v.value",cmp.get("v.ValueFore"));
            return;
        }
        cmp.set("v.ValueFore",campo);
       
        var appEvt2 = cmp.getEvent("refreshSize");    
        
        appEvt2.setParams({"FO_attr":true});
        appEvt2.fire();
       
        
    },
    focusPocus : function(cmp,evt){
        if(cmp.get("v.ValueFore")==0 ||cmp.get("v.ValueFore")=="0"){
            cmp.find("input-ui").set("v.value","");
        }
        $A.util.addClass(cmp.find("input-ui"),"mintam");
    },
    refreshSize : function(cmp,evt){
    	cmp.find("input-ui").set("v.value",cmp.find("input-ui").get("v.value"));
	}
    
})