({
	closeFunction : function(cmp, event, helper) {
	    if (cmp.get('v.redirectURL') != null ) {
            sforce.one.navigateToURL(cmp.get('v.redirectURL'));
        }
	}
})