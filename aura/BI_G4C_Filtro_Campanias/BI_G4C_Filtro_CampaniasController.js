({
	doInit: function(cmp, evt, helper){
		//console.log('doInit');
		//by JB 29/07
		helper.getOptions(cmp);
	},
	showAddFilter : function(cmp, evt, helper) {
		//console.log('addFilter');
		//by JB 29/07		
		$A.util.removeClass(cmp.find('filter-div'),'slds-hide');
		var opts = cmp.find("InputSelectDynamic").get("v.options");
		for (var i=0; i<opts.length; i++){
			if (opts[i].value===cmp.find("InputSelectDynamic").get('v.value')){
				//console.log(opts[i]);
				//by JB 29/07
				cmp.set('v.selectedValue', opts[i]);
				break;
			}
		}
		//console.log(opts);
		//by JB 29/07
	},
	selectCampania: function(cmp, evt, helper){
        var filters = cmp.get('v.filters');
        //var newFiltro = 'campaignID' +cmp.find("InputSelectDynamic").get('v.value');
        var newFiltro = 'Id IN (SELECT Contact.AccountId FROM CampaignMember WHERE CampaignId ='+cmp.find("InputSelectDynamic").get('v.value');
		filters.push(newFiltro);
        cmp.set('v.filters', filters);
        //console.log(filters);
        //by JB 29/07	
	},
	cancelFilter: function(cmp, evt, helper){
		helper.clearFilterData(cmp);
	},
	operatorSelect: function(cmp, evt, helper){		
		cmp.set('v.operator', cmp.get('v.operator'));
	},
	onGroup: function(cmp, evt, helper){
		var elem = evt.getSource().getElement();
        var selected = elem.textContent;
        cmp.set('v.booleanFilterValue', (selected==='Verdadero')?true:false);        
        

	},
	addFilter: function(cmp, evt, helper){
		var filters = cmp.get('v.filters');
		var ptype = cmp.get('v.selectedValue.ptype');
		if ( ptype === 'BOOLEAN' ){
			var booleanFilterValue = cmp.get('v.booleanFilterValue');
			//console.log(booleanFilterValue);
			//by JB 29/07
			if (booleanFilterValue===true||booleanFilterValue===false){
				var newFiltro = cmp.get('v.selectedValue.value')+' = '+booleanFilterValue;
				filters.push(newFiltro);
			}

		}
		else{
			var operator = cmp.get('v.operator');
			var dbOp = '';
			var contains = false;
			if ( operator==='Equals' ){
				dbOp = '=';
			}
			else{
				if ( operator==='Not equals' ){
					dbOp = '!=';
				}
				else{
					if ( operator==='Greather than' ){
						dbOp = '>';
					}
					else{
						if ( operator==='Lower than' ){
							dbOp = '<';
						}
						else{
							if ( operator==='Contains' ){
								contains = true;
								dbOp = 'like';
							}
							else{
								if ( operator==='Not contains' ){
									contains = true;
									dbOp = 'like';
								}
							}
						}
					}
				}
			}
			
			if ( ptype==='DOUBLE'||ptype==='INTEGER'||ptype==='CURRENCY' ){									
				newFiltro = cmp.get('v.selectedValue.value')+' '+dbOp+' '+cmp.get('v.filterValue');				
			}
			else{				
				if ( contains === false ){
					newFiltro = cmp.get('v.selectedValue.value')+' '+dbOp+' \''+cmp.get('v.filterValue')+'\'';
				}
				else{
					newFiltro = cmp.get('v.selectedValue.value')+' '+dbOp+' \'%'+cmp.get('v.filterValue')+'%\'';
				}
			}
			
			filters.push(newFiltro);
		}
		cmp.set('v.filters', filters);
		//console.log(filters);
		//by JB 29/07
		helper.clearFilterData(cmp);
	},
	removeFilter: function(cmp, evt, helper){
		//console.log(evt);
		//by JB 29/07
		var currentIndex = -1;
		if (evt.target.nodeName==='use'){
			currentIndex=evt.target.parentNode.parentNode.dataset.index;
		}
		else{
			if (evt.target.nodeName==='svg'){
				currentIndex=evt.target.parentNode.dataset.index;
			}
		}

		var filters = cmp.get('v.filters');
		var outFilters = [];
		//var j = 0;

		for (var i=0; i<filters.length; i++){
			if (i!==currentIndex){
				outFilters.push(filters[i]);
				//outFilters[j] = filters[i];
				//j++;
			}
		}
		
		cmp.set('v.filters', outFilters);
		
	},
	clearAllFilter: function(cmp, evt, helper){
		cmp.set('v.filters', []);
	}
})