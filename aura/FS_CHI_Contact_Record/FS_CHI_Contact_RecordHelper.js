({
	notificate : function(component, event, helper, isWarning, notification){
        /* Variable Configuration */
        /* Empty */
        
        /*Text Definition*/
        if(isWarning && notification=='Error al validar el contacto. Contacte con el administrador.'){
            component.set('v.Notification_Title_Value', 'Error');
        } else if(isWarning){
            component.set('v.Notification_Title_Value', 'Advertencia');
        }
        else component.set('v.Notification_Title_Value', 'Información');
        
        component.set('v.Notification_Body_Value',notification);
    },
    check : function(component, event, helper, elementId, containerId, messageId){
        /* Variable Configuration */
        	/* Global */
        	var correct = true;
        
        	/* Fields */
        	var element = component.find(elementId);
        	var container = component.find(containerId);
        	var message = component.find(messageId);

        /* Check Integrity */
         var temp = element.get('v.value');
         if(temp == null || temp == '') correct = false;
         else{
             if(elementId == 'CMP_Document_Number'){
             //    if(temp.length === 9) 
                     correct = true;
               //  else correct = false;
             }
             else if(elementId == 'CMP_Phone' || elementId == 'CMP_Mobile'){                 
                 if(temp.length === 9) correct = true;
                 else correct = false;
             }
             else if(elementId == 'CMP_Email'){
                 if(temp.includes('@') && temp.includes('.')) correct = true;
                 else correct = false;
             }else correct = true;
         }
         
         if(!correct){
             $A.util.addClass(container,'slds-has-error');
             $A.util.removeClass(message,'slds-hide');
         }else{
             $A.util.removeClass(container,'slds-has-error');
             $A.util.addClass(message,'slds-hide');
         }
         console.log('Correct before return: ' + correct);
         return correct;
    },
    assing : function(component, event, helper, input, value){
        /* Variable Configuration */
        var element = component.find(input);
        
        element.set("v.value", value);
        element.set("v.disabled",value!='');
    },
    validate : function(component, event, helper, documentType, documentNumber, serialNumber){
        /* Variable Configuration */
        var firstname, lastName;
        
        var action = component.get("c.validateContact");
        action.setParams({documentType: 'RUT', documentNumber: documentNumber, SerialNumber: serialNumber});
        
        action.setCallback(this, function(response) {
            debugger;
            console.log("---- Notificaciones Error response.getState():" + response.getState());//everis - pablo bordas
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = JSON.parse(response.getReturnValue());
                console.log(returnValue);//everis - pablo bordas
                if(returnValue != null && returnValue.totalResults == 1){
                    /* JSON */
                    if(returnValue.contacts[0].contactDetails != null){
                        if(returnValue.contacts[0].contactDetails.name != null){
                            if(returnValue.contacts[0].contactDetails.name.givenName != null) 
                                helper.assing(component, event, helper, 'CMP_First_Name', returnValue.contacts[0].contactDetails.name.givenName);
                            if(returnValue.contacts[0].contactDetails.name.familyName != null) 
                                helper.assing(component, event, helper, 'CMP_Last_Name', returnValue.contacts[0].contactDetails.name.familyName);
                        }
                        if(returnValue.contacts[0].contactDetails.addresses != null){
                            if(returnValue.contacts[0].contactDetails.addresses[0].addressName != null) 
                                helper.assing(component, event, helper, 'CMP_Street_Name', returnValue.contacts[0].contactDetails.addresses[0].addressName);
                            if(returnValue.contacts[0].contactDetails.addresses[0].addressNumber != null) 
                                helper.assing(component, event, helper, 'CMP_Street_Number', returnValue.contacts[0].contactDetails.addresses[0].addressNumber.value);
                            if(returnValue.contacts[0].contactDetails.addresses[0].locality != null) 
                                helper.assing(component, event, helper, 'CMP_City', returnValue.contacts[0].contactDetails.addresses[0].locality);
                            if(returnValue.contacts[0].contactDetails.addresses[0].region != null)
                                helper.assing(component, event, helper, 'CMP_State', returnValue.contacts[0].contactDetails.addresses[0].region);
                        }
                    }
                    
                    /* Flags */
                    component.set('v.Manual_Value', false);
                    helper.notificate(component, event, helper, false, 'El contacto ha sido validado.');
                }else if (returnValue.totalResults == 666){
					console.log("----666----");//everis - pablo bordas
                    //console.log("----ELSE---- Notificaciones Error returnValue:" + returnValue.contacts[0].description);//everis - pablo bordas
                    component.set('v.Manual_Value', true);
                    helper.notificate(component, event, helper, true, returnValue.errorDescription);
                }else{
                    /* Flags */
                    component.set('v.Manual_Value', true);
                    helper.notificate(component, event, helper, true, 'No ha sido posible encontrar al contacto.');
                }
                
                /* Flags */
                component.set('v.Validated_Value', true);
                component.set('v.Validate_Button', false);
            }else{
                 /* Flags */
                    console.log("----Before else----");//everis - pablo bordas
                	console.log("----ELSE---- Notificaciones Error returnValue:" + returnValue[0].description);//everis - pablo bordas
                    component.set('v.Manual_Value', true);
                    helper.notificate(component, event, helper, true, 'Error al validar el contacto. Contacte con el administrador.' + returnValue[0].description);//everis - pablo bordas
            }
        });
        $A.enqueueAction(action);
    },
    create : function(component, event, helper, authorizations, references, representative, documentType, documentNumber, serialNumber, firstName, lastName, email, streetName, streetNumber, city, state, account, currency, manual, phone, mobile){
      var action = component.get("c.createContact");
        action.setParams({authorizations: authorizations, references: references, representative: representative, documentType: documentType, documentNumber: documentNumber, serialNumber: serialNumber, firstName: firstName, lastName: lastName, email: email, streetName: streetName, streetNumber: streetNumber, city: city, accountId: account, currencyCode: currency, manual: manual, state: state, phone: phone, mobile: mobile});
        if(account==null || account==""){
            helper.notificate(component, event, helper, true, 'Es obligatorio asociar un cliente al contacto.');
        }  
        else if (!authorizations.includes('Ninguno')){
            action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = JSON.parse(response.getReturnValue());
                if(returnValue != null){
                    if(returnValue.id != null) window.location.href = 'https://' + window.location.host + '/' + returnValue.id;
                    else if(returnValue.error != null)
                    {
                        var type = returnValue.error;
                        
                        if(type.includes("INVALID_EMAIL_ADDRESS")) helper.notificate(component, event, helper, true, 'La dirección de correo electrónico no válida.');
                        else if(type.includes("INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST, valor inapropiado para el campo de lista de selección restringida: RUT: [BI_Tipo_de_documento__c]")) helper.notificate(component, event, helper, true, 'El cliente asociado al contacto debe ser de Chile.');
                        else if(type.includes("FIELD_CUSTOM_VALIDATION_EXCEPTION")){
                            //if(type.includes("Identificador fiscal")) helper.notificate(component, event, helper, true, 'El número de identificador fiscal no dispone del formato adecuado. Ej. 20515888k'); 
                            if(type.includes("El número del RUT no es valido")) helper.notificate(component, event, helper, true, 'El número de identificador fiscal no dispone del formato adecuado. Ej. 20515888k');
                            if(type.includes("Documento fiscal")) helper.notificate(component, event, helper, true, 'Tipo de documento no válido para el país del contacto.');
                        }else if(type.includes("STRING_TOO_LONG")){
                            if(type.includes("Número de serie")) helper.notificate(component, event, helper, true, 'El número de serie introducido no dispone del formato adecuado.');
                        }
                        else helper.notificate(component, event, helper, true, type);
                    }
                }
            }
        });
        }else helper.notificate(component, event, helper, true, 'No es posible seleccionar el valor Ninguno junto a otros valores en Autorizaciones funcionales');
        $A.enqueueAction(action);
    },
    search : function(component, event, helper, searchValue){
        /* Restore Data */
        
        component.set('v.Search_Value',[]);
        
        var action = component.get("c.retrieveInfo");
        action.setParams({searchKey: searchValue});
        
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){
                    component.set('v.Search_Value', returnValue);
                    if(returnValue.length == 1){
                        component.set('v.oneContact', true);
                        component.set('v.isList', false);
                        component.set('v.Search_Value2', returnValue[0]);
                        var pcikls = component.find("CMP_Resultados");
            			pcikls.set("v.value", 'Chile*'+returnValue[0].Id);
                    }else{
                        component.set('v.oneContact', false);
                        component.set('v.isList', true);
                    }
                    //var prueba = component.find('prueba');
                    //$A.util.addClass(prueba,'slds-hide');
                }
            }
        });
        $A.enqueueAction(action);
        setTimeout(function(){},1000);
    },
    
    getSelectList : function(component, event, helper, attribute, sObject, field) {
        var action = component.get('c.getSelectOptions');
		action.setParams({ 
            'objObject':  sObject,
            'fld' : field
        });
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
                console.log(response.getReturnValue());
                if (field == 'BI_Tipo_de_contacto__c'){
                    var index = response.getReturnValue().indexOf("Chile")
                    var result = response.getReturnValue();
                    console.log(result);
                } else if (field == 'BI_Tipo_de_documento__c'){
                    var array = response.getReturnValue();
                    var indexPAS = response.getReturnValue().indexOf("PAS");
                    if (indexPAS > -1) {
                        array.splice(indexPAS, 1);
                    }
                    var indexPasaporte = response.getReturnValue().indexOf("Pasaporte");
                    if (indexPasaporte > -1) {
                        array.splice(indexPasaporte, 1);
                    }
                    var result = array;
                    console.log("encuentra rut "+result);
                } else if (field == 'CurrencyIsoCode'){
                    var result = response.getReturnValue();
                    console.log("encuentra curr "+result);
                } else if (field == 'FS_CORE_Referencias_Funcionales__c') {
                    var result = response.getReturnValue();
                    console.log("encuentra refe "+result);
                }
                component.set(attribute, result);
			} else {
				console.log('no he cargado cosas');
			}
		});
		$A.enqueueAction(action); 
    }
})