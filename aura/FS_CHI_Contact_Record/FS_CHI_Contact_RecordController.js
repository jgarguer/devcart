({
    doInit : function(component, event, helper){
        helper.getSelectList(component, event, helper, "v.authorizationsList", component.get("v.objInfo"), "BI_Tipo_de_contacto__c");
        helper.getSelectList(component, event, helper, "v.referencesList", component.get("v.objInfo"), "FS_CORE_Referencias_Funcionales__c");
        helper.getSelectList(component, event, helper, "v.documentTypeList", component.get("v.objInfo"), "BI_Tipo_de_documento__c");
        helper.getSelectList(component, event, helper, "v.currencyList", component.get("v.objInfo"), "CurrencyIsoCode");
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName = sURLVariables[0].split('=');
        if(sParameterName!=null && sParameterName.length >0 && sParameterName[1]!=null && sParameterName[1] != '' ){
            
            component.set("v.isFromContact",false);
            var pcikls = component.find("CMP_Resultados");
            pcikls.set("v.value", 'Chile*'+sParameterName[1]);

            
            helper.search(component, event, helper, sParameterName[1]);
        }
        else {
            console.log('else');
            component.set("v.isFromContact",true);
            helper.search(component, event, helper, null);
        } 
        
    },
    authorizated : function(component, event, helper){
        var authorizated = component.find('CMP_Authorizations').get('v.value');
        var representative = component.find('CMP_Representative').get('v.value');
        var validatedCustomer = component.get('v.Validate_Button');
        
        if(authorizated=='' && !representative) 
            if (!validatedCustomer){
                //component.set('v.Validated_Value',false);
            }else{
                
                if (((component.find('CMP_Document_Number').get('v.value') == '')||(component.find('CMP_Document_Number').get('v.value') == null)) 
                    && ((component.find('CMP_Serial_Number').get('v.value') == '')||(component.find('CMP_Serial_Number').get('v.value') == null))) component.set('v.Validated_Value',true);
                 
            }
        else 
            if (!validatedCustomer){
                component.set('v.Validated_Value',true);
            }else{
                component.set('v.Validated_Value',false);
            }
            
    },
    search : function(component, event, helper) {
        component.set("v.isFromContact",true);
        var searchValue = component.find('CMP_Search').get('v.value');
        
        if(searchValue != '') helper.search(component, event, helper, searchValue);
        else helper.search(component, event, helper, null);
    },
	validate : function(component, event, helper) {
        /* Variable Configuration */
      
          var containerPhone = component.find('CMP_Container_Phone');
        	var messagePhone = component.find('CMP_Container_Inside_Phone');
            var containerMobile = component.find('CMP_Container_Mobile');
        	var messageMobile = component.find('CMP_Container_Inside_Mobile');
            var containerLast = component.find('CMP_Container_Last_Name');
        	var messageLast = component.find('CMP_Container_Inside_Last_Name');
            var containerEmail = component.find('CMP_Container_Email');
        	var messageEmail = component.find('CMP_Container_Inside_Email');
            $A.util.removeClass(containerPhone,'slds-has-error');
            $A.util.addClass(messagePhone,'slds-hide');
            $A.util.removeClass(containerMobile,'slds-has-error');
            $A.util.addClass(messageMobile,'slds-hide');
            $A.util.removeClass(containerLast,'slds-has-error');
            $A.util.addClass(messageLast,'slds-hide');
            $A.util.removeClass(containerEmail,'slds-has-error');
            $A.util.addClass(messageEmail,'slds-hide');
            
            
        var mandatory = true;
        var documentType, documentValue, serialNumber;
        
        /* Mandatory Fields Validation */
        mandatory = (helper.check(component, event, helper,'CMP_Resultados', 'CMP_Container_Resultados', 'CMP_Container_Inside_Resultados') && mandatory);
        mandatory = (helper.check(component, event, helper,'CMP_Document_Number' ,'CMP_Container_Document_Number' ,'CMP_Container_Inside_Document_Number') && mandatory);
        mandatory = (helper.check(component, event, helper,'CMP_Serial_Number' ,'CMP_Container_Serial_Number' ,'CMP_Container_Inside_Serial_Number') && mandatory);
      //  var checkMobile = helper.check(component, event, helper,'CMP_Mobile', 'CMP_Container_Mobile', 'CMP_Container_Inside_Mobile');
       // var checkPhone =  helper.check(component, event, helper,'CMP_Phone', 'CMP_Container_Phone', 'CMP_Container_Inside_Phone');
        //mandatory = ((checkPhone && mandatory)|| (checkMobile && mandatory));
        //mandatory = (helper.check(component, event, helper,'CMP_Mobile', 'CMP_Container_Mobile', 'CMP_Container_Inside_Mobile') && mandatory);
        
        /* Invoke */
        if(mandatory){
            var container = component.find('CMP_Container_Resultados');
        	var message = component.find('CMP_Container_Inside_Resultados');
            var container2 = component.find('CMP_Container_Document_Number');
        	var message2 = component.find('CMP_Container_Inside_Document_Number');
            var container3 = component.find('CMP_Container_Serial_Number');
        	var message3 = component.find('CMP_Container_Inside_Serial_Number');
            var container4 = component.find('CMP_Container_Phone');
        	var message4 = component.find('CMP_Container_Inside_Phone');
            var container5 = component.find('CMP_Container_Mobile');
        	var message5 = component.find('CMP_Container_Inside_Mobile');
            $A.util.removeClass(container,'slds-has-error');
            $A.util.addClass(message,'slds-hide');
            $A.util.removeClass(container2,'slds-has-error');
            $A.util.addClass(message2,'slds-hide');
            $A.util.removeClass(container3,'slds-has-error');
            $A.util.addClass(message3,'slds-hide');
            $A.util.removeClass(container4,'slds-has-error');
            $A.util.addClass(message4,'slds-hide');
            $A.util.removeClass(container5,'slds-has-error');
            $A.util.addClass(message5,'slds-hide');
            if(component.find('CMP_Resultados').get('v.value').split("*")[0] != 'Chile'){
                component.set('v.Validated_Value', true);
                component.set('v.Manual_Value', true);
                helper.notificate(component, event, helper, true, 'Actualmente, no es posible validar contactos cuyo país sea distinto a Chile.');
            } 
            else {
                documentType = component.find('CMP_Document_Type').get('v.value');
                documentValue = component.find('CMP_Document_Number').get('v.value');
                serialNumber = component.find('CMP_Serial_Number').get('v.value');
                
                helper.validate(component, event, helper, documentType, documentValue, serialNumber);
            }
        }else{
            if (checkMobile && !checkPhone){ 
               var container4 = component.find('CMP_Container_Phone');
        	   var message4 = component.find('CMP_Container_Inside_Phone');
                $A.util.removeClass(container4,'slds-has-error');
                $A.util.addClass(message4,'slds-hide');
             }
            if (!checkMobile && checkPhone){ 
               var container5 = component.find('CMP_Container_Mobile');
        	   var message5 = component.find('CMP_Container_Inside_Mobile');
               $A.util.removeClass(container5,'slds-has-error');
               $A.util.addClass(message5,'slds-hide');
             }
        }
        
	},
    create : function(component, event, helper) {
        /* Variable Configuration */
        var mandatory = true;
        var manual, authorizations, references, representative, documentType, documentNumber, serialNumber, firstname, lastname, email, streetName, streetNumber, city, state, account, currency, phone, mobile;
        
            var container2 = component.find('CMP_Container_Document_Number');
        	var message2 = component.find('CMP_Container_Inside_Document_Number');
            var container3 = component.find('CMP_Container_Serial_Number');
        	var message3 = component.find('CMP_Container_Inside_Serial_Number');
           $A.util.removeClass(container2,'slds-has-error');
                $A.util.addClass(message2,'slds-hide');
           $A.util.removeClass(container3,'slds-has-error');
                $A.util.addClass(message3,'slds-hide');
        
        
        /* Mandatory Fields Validation */
        if(component.find('CMP_Authorizations').get('v.value') != '' || component.find('CMP_Representative').get('v.value') == true) mandatory = (helper.check(component, event, helper,'CMP_Document_Number' ,'CMP_Container_Document_Number' ,'CMP_Container_Inside_Document_Number') && mandatory);
        if(component.find('CMP_Authorizations').get('v.value') != '' || component.find('CMP_Representative').get('v.value') == true) mandatory = (helper.check(component, event, helper,'CMP_Serial_Number' ,'CMP_Container_Serial_Number' ,'CMP_Container_Inside_Serial_Number') && mandatory);
        
        //mandatory = (helper.check(component, event, helper,'CMP_First_Name' ,'CMP_Container_First_Name' ,'CMP_Container_Inside_First_Name') && mandatory);
        mandatory = (helper.check(component, event, helper,'CMP_Last_Name' ,'CMP_Container_Last_Name' ,'CMP_Container_Inside_Last_Name') && mandatory);
        mandatory = (helper.check(component, event, helper,'CMP_Email', 'CMP_Container_Email', 'CMP_Container_Inside_Email') && mandatory);
        var checkRUT = helper.check(component, event, helper,'CMP_Document_Number', 'CMP_Container_Document_Number', 'CMP_Container_Inside_Document_Number');
        console.log('Controller ----------- checkRUT: ' + checkRUT)
        var checkMobile = helper.check(component, event, helper,'CMP_Mobile', 'CMP_Container_Mobile', 'CMP_Container_Inside_Mobile');
        var checkPhone =  helper.check(component, event, helper,'CMP_Phone', 'CMP_Container_Phone', 'CMP_Container_Inside_Phone');
        mandatory = ((checkPhone && mandatory)|| (checkMobile && mandatory));
		//mandatory = (helper.check(component, event, helper,'CMP_Phone', 'CMP_Container_Phone', 'CMP_Container_Inside_Phone') && mandatory) || (helper.check(component, event, helper,'CMP_Mobile', 'CMP_Container_Mobile', 'CMP_Container_Inside_Mobile') && mandatory);
        //mandatory = (helper.check(component, event, helper,'CMP_Street_Name', 'CMP_Container_Street_Name', 'CMP_Container_Inside_Street_Name') && mandatory);
        //mandatory = (helper.check(component, event, helper,'CMP_Street_Number', 'CMP_Container_Street_Number', 'CMP_Container_Inside_Street_Number') && mandatory);
        //mandatory = (helper.check(component, event, helper,'CMP_City', 'CMP_Container_City', 'CMP_Container_Inside_City') && mandatory);
        //mandatory = (helper.check(component, event, helper,'CMP_State', 'CMP_Container_State', 'CMP_Container_Inside_State') && mandatory);
        if (checkMobile && !checkPhone){ 
               var container4 = component.find('CMP_Container_Phone');
        	   var message4 = component.find('CMP_Container_Inside_Phone');
                $A.util.removeClass(container4,'slds-has-error');
                $A.util.addClass(message4,'slds-hide');
             }
            if (!checkMobile && checkPhone){ 
               var container5 = component.find('CMP_Container_Mobile');
        	   var message5 = component.find('CMP_Container_Inside_Mobile');
               $A.util.removeClass(container5,'slds-has-error');
               $A.util.addClass(message5,'slds-hide');
             }
        mandatory = (helper.check(component, event, helper,'CMP_Resultados', 'CMP_Container_Resultados', 'CMP_Container_Inside_Resultados') && mandatory);
         
            
       
        if(mandatory){
            /* Retrieve Data */
            if(component.find('CMP_First_Name').get('v.value') != null) firstname = component.find('CMP_First_Name').get('v.value');
            if(component.find('CMP_Last_Name').get('v.value') != null) lastname = component.find('CMP_Last_Name').get('v.value');
            if(component.find('CMP_Email').get('v.value') != null) email = component.find('CMP_Email').get('v.value');
            if(component.find('CMP_Phone').get('v.value') != null) phone = component.find('CMP_Phone').get('v.value');
            if(component.find('CMP_Mobile').get('v.value') != null) mobile = component.find('CMP_Mobile').get('v.value');
            if(component.find('CMP_Authorizations').get('v.value') != null) authorizations = component.find('CMP_Authorizations').get('v.value');
            if(component.find('CMP_References').get('v.value') != null) references = component.find('CMP_References').get('v.value');
            if(component.find('CMP_Representative').get('v.value') != null) representative = component.find('CMP_Representative').get('v.value');
            
            if(component.find('CMP_Document_Type').get('v.value') != null) documentType = component.find('CMP_Document_Type').get('v.value');
            if(component.find('CMP_Document_Number').get('v.value') != null) documentNumber = component.find('CMP_Document_Number').get('v.value');
            if(component.find('CMP_Serial_Number').get('v.value') != null) serialNumber = component.find('CMP_Serial_Number').get('v.value');
            
            if(component.find('CMP_Street_Name').get('v.value') != null) streetName = component.find('CMP_Street_Name').get('v.value');
            if(component.find('CMP_Street_Number').get('v.value') != null) streetNumber = component.find('CMP_Street_Number').get('v.value');
            if(component.find('CMP_City').get('v.value') != null) city = component.find('CMP_City').get('v.value');
            if(component.find('CMP_State').get('v.value') != null) state = component.find('CMP_State').get('v.value');
            debugger;
            if(component.find('CMP_Resultados').get('v.value') != null) account = component.find('CMP_Resultados').get('v.value').split("*")[1];
            
            if(component.find('CMP_Currency').get('v.value') != null) currency = component.find('CMP_Currency').get('v.value');
            
            /* Creation Type */
            manual = component.get('v.Manual_Value');
            
            /* Invoke */
            helper.create(component, event, helper, authorizations, references, representative, documentType, documentNumber, serialNumber, firstname, lastname, email, streetName, streetNumber, city, state, account, currency, manual, phone, mobile);
        }
        
	},
    minimize : function(component, event, helper) {
        /* Warning Restoration */
        component.set('v.Notification_Title_Value', '');
        component.set('v.Notification_Body_Value', '');
    },
    reset : function(component, event, helper) {        
        /* Interface Restoration */
        if((component.find('CMP_Authorizations').get('v.value') != '' && component.get('v.Validated_Value')) || 
           (component.find('CMP_Authorizations').get('v.value') == '' && !component.get('v.Manual_Value'))|| ((component.get('v.Manual_Value'))&& !component.get('v.Validate_Button'))){
            //helper.assing(component, event, helper, 'CMP_First_Name', '');
            //helper.assing(component, event, helper, 'CMP_Last_Name', '');
            //helper.assing(component, event, helper, 'CMP_Email', '');
            
            helper.assing(component, event, helper, 'CMP_Street_Name', '');
            helper.assing(component, event, helper, 'CMP_Street_Number', '');
            helper.assing(component, event, helper, 'CMP_City', '');
            helper.assing(component, event, helper, 'CMP_State', '');
            
            /* Notification */
        helper.notificate(component, event, helper, true, 'La información fiscal validada ha sido modificada, introduzca la información del nuevo contacto.');
            component.set('v.Validate_Button', true);
            component.set('v.Manual_Value', true);
               var container = component.find('CMP_Container_Resultados');
        	var message = component.find('CMP_Container_Inside_Resultados');
            var container2 = component.find('CMP_Container_Document_Number');
        	var message2 = component.find('CMP_Container_Inside_Document_Number');
            var container3 = component.find('CMP_Container_Serial_Number');
        	var message3 = component.find('CMP_Container_Inside_Serial_Number');
            var container4 = component.find('CMP_Container_Phone');
        	var message4 = component.find('CMP_Container_Inside_Phone');
            var container5 = component.find('CMP_Container_Mobile');
        	var message5 = component.find('CMP_Container_Inside_Mobile');
             var container6 = component.find('CMP_Container_Email');
        	var message6 = component.find('CMP_Container_Inside_Email');
             var container7 = component.find('CMP_Container_Last_Name');
        	var message7 = component.find('CMP_Container_Inside_Last_Name');
            $A.util.removeClass(container,'slds-has-error');
            $A.util.addClass(message,'slds-hide');
            $A.util.removeClass(container2,'slds-has-error');
            $A.util.addClass(message2,'slds-hide');
            $A.util.removeClass(container3,'slds-has-error');
            $A.util.addClass(message3,'slds-hide');
            $A.util.removeClass(container4,'slds-has-error');
            $A.util.addClass(message4,'slds-hide');
            $A.util.removeClass(container5,'slds-has-error');
            $A.util.addClass(message5,'slds-hide');
            $A.util.removeClass(container6,'slds-has-error');
            $A.util.addClass(message6,'slds-hide');
            $A.util.removeClass(container7,'slds-has-error');
            $A.util.addClass(message7,'slds-hide');
        }
        
        /* Internal Control */
        if(component.find('CMP_Document_Number').get('v.value') != '') component.set('v.Validated_Value', false);
        if (component.find('CMP_Serial_Number').get('v.value') != '') component.set('v.Validated_Value', false);
        
        var representative = component.find('CMP_Representative').get('v.value');
        if (component.find('CMP_Serial_Number').get('v.value') == '' && component.find('CMP_Document_Number').get('v.value') == '' 
            && (component.find('CMP_Authorizations').get('v.value')=='') && !representative)   component.set('v.Validated_Value', true);
        
    },
    dismiss : function(component, event, helper) {
        /* Variable Configuration */
        var userAgent = navigator.userAgent;
        debugger;
        if(userAgent.includes('Macintosh') || userAgent.includes('Windows')) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
            var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
            var sParameterName = sURLVariables[0].split('=');
            
            if(sParameterName!=null && sParameterName.length >0 && sParameterName[1]!=null && sParameterName[1]!=''){
                window.location.href = 'https://' + window.location.host +'/'+sParameterName[1];
                
            }else{
                window.location.href = 'https://' + window.location.host +'/003/o';
            }
        }
        else
            
            sforce.one.back(true);
    },
    showSpinner : function (component, event, helper) {
    
        debugger;
        var spinner = component.find('spinner');
        var botonesOcultar = component.find('botonesOcultar');
       $A.util.removeClass(spinner,'slds-hide');
        $A.util.addClass(botonesOcultar,'slds-hide');
        
    },
     hideSpinner : function (component, event, helper) {
         debugger;
        var spinner = component.find('spinner');
         var botonesOcultar = component.find('botonesOcultar');
       $A.util.addClass(spinner,'slds-hide'); 
         $A.util.removeClass(botonesOcultar,'slds-hide');
    }
})