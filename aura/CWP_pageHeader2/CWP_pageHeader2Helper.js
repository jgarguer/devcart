({
    getInicialValues: function(cmp, event, helper) {
        var action = cmp.get("c.CWP_doInit");
        console.log('getInicialValues');
        action.setCallback(this, function(response) {

            var errorVar;
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var res = response.getReturnValue();

                helper.setDynamicMenu(cmp, JSON.parse(res["dynamicHeader"]));
                cmp.set('v.runningUser', JSON.parse(res["usuarioPortal"]));
                cmp.set('v.isTGS', JSON.parse(res["esTGS"]));
                if(cmp.get('v.isTGS') == true){
                    document.getElementById('imgheaderTGS').src=$A.get("$Resource.CWP_LightningAssets") + '/assets/img/contents/header/tgs_portal_header.png';
                    document.getElementById('imgheaderTGS').style.display='';
                    document.getElementById('imgheader').style='background:-webkit-linear-gradient(left, #01232D, #005370)';
                } else if(JSON.parse(res["esCarrier"])==true){
					document.getElementById('imgheaderTGS').src=$A.get("$Resource.CWP_LightningAssets") + '/assets/img/contents/header/wholesale-header.png';
                    document.getElementById('imgheaderTGS').style.display='';
                    document.getElementById('imgheader').style='background:-webkit-linear-gradient(left, #01232D, #005370)';                    
                }else{
                    document.getElementById('imgheaderPLA').src=$A.get("$Resource.CWP_LightningAssets") + '/assets/img/contents/header/tlf-bg_subheader.png';
                    document.getElementById('imgheaderPLA').style.display='';
                }
                var accounts = JSON.parse(res["listUsers"]);
                cmp.set("v.imageUserProfile",res["FullPhotoUrl"] );
                var aux = [];
                if (accounts.length > 1) {
                    var userAccount = JSON.parse(res["usuarioPortal"]).Contact.BI_Cuenta_activa_en_portal__c;
                    for (i = 0; i < accounts.length; i++) {
                        if (accounts[i].Id == userAccount) {
                            aux.push(accounts[i]);
                        }
                    }
                    for (i = 0; i < accounts.length; i++) {
                        if (accounts[i].Id != userAccount) {
                            aux.push(accounts[i]);
                        }
                    }
                    cmp.set('v.listUsers', aux);
                    console.log('disparo ' + cmp.get("v.runningUser").Id + '-' +  aux[0].Id);
                    var passUserAccIds = $A.get("e.c:CWP_AC_passUserAcc");
                    passUserAccIds.setParams({"currentUserId" : cmp.get("v.runningUser").Id, "currentAccId" : aux[0].Id});
                    passUserAccIds.fire();
                } else {
                    console.log(accounts[0].Id);
                    console.log(cmp.get("v.runningUser").Id);
                    cmp.set('v.listUsers', JSON.parse(res["listUsers"]));
                    
                    console.log(accounts);
                    var passUserAccIds = $A.get("e.c:CWP_AC_passUserAcc");
                    passUserAccIds.setParams({"currentUserId" : cmp.get("v.runningUser").Id, "currentAccId" : accounts[0].Id});
                    passUserAccIds.fire();
                    
                    console.log('disparo');
                }
            } else {
                /*LANZA ERROR
                                errorVar = $A.get("e.c:BI_G4C_Generic_error");
                                errorVar.setParams({"header": "Error"});
                                errorVar.setParams({"body": "Error"});
                                errorVar.fire();
                                */

            }
        });
        $A.enqueueAction(action);
    },

    setDynamicMenu: function(cmp, list) {

        var toggleText;
        for (var idDes in list) {
            if (!list[idDes]) {
                toggleText = cmp.find(idDes);
                $A.util.toggleClass(toggleText, "toggle");
                toggleText = cmp.find(idDes + "--m");
                $A.util.toggleClass(toggleText, "toggle");
            }
        }
        if (!list["pca_equipotelefonica"] && !list["pca_customcalendar"] && !list["pca_chattercustom"]) {
            toggleText = cmp.find("pca_rManagementL");
            $A.util.toggleClass(toggleText, "toggle");
            toggleText = cmp.find("pca_rManagementL" + "--m");
            $A.util.toggleClass(toggleText, "toggle");
        }
        if (!list["pca_report"] && !list["pca_monitoreo"] && !list["pca_reclamospedidos"] && !list["pca_portales"]) {
            toggleText = cmp.find("pca_sTrackingL");
            $A.util.toggleClass(toggleText, "toggle");
            toggleText = cmp.find("pca_rManagementL" + "--m");
            $A.util.toggleClass(toggleText, "toggle");
        }
        if (!list["pca_inventory"] && !list["pca_documentmanagement"] && !list["pca_facturacioncobranza"] && !list["pca_catalogo"]) {
            toggleText = cmp.find("pca_mServiceL");
            $A.util.toggleClass(toggleText, "toggle");
            toggleText = cmp.find("pca_rManagementL" + "--m");
            $A.util.toggleClass(toggleText, "toggle");
        }
    },

    getMapVisibility: function(component, event) {
        var action = component.get("c.getPermissionSet");
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                console.log(returnValue);
                if (returnValue != null) {
                    component.set('v.mapVisibility', returnValue);
                    if(returnValue.oneDriveLink != null){
                    	component.set('v.oneDriveLink', returnValue.oneDriveLink);
                    }  else{
                    	component.set('v.oneDriveLink', '');
                    }                  
                }
            }
        });
        $A.enqueueAction(action);


    },
    updateUser: function(component, event,helper) {
        var newAccountId = event.currentTarget.value;
        var action = component.get("c.updateUser");
        action.setParams({
            "newAccountId": newAccountId
        });
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if (returnValue != null) {
                    component.set("v.runningUser", returnValue);
                    window.location.reload(true);
                }
            }
        });
        $A.enqueueAction(action);
    }

})