({
        doInit: function(cmp, event, helper) {
            helper.getInicialValues(cmp, event, helper);
            helper.getMapVisibility(cmp, event);


        },
        render: function(component, helper) {
           /* var svg = document.getElementById("svg_content");
            var value = svg.innerText;
            value = value.replace("<![CDATA[", "").replace("]]>", "");
            svg.innerHTML = value;
            var gauge3 = loadLiquidFillGauge("fillgauge3", 60.1);*/
            setTimeout(function(){ 
                document.getElementById("navMobile").style.display='';
                document.getElementById("navDesktop").style.display='';
                document.getElementById("imgheader").style.display='';
                
               $("#backgroundmodalwaitAll").fadeOut(1000,function()
               { 
                   //eliminamos la capa de precarga
                   $(this).remove();
                   //permitimos scroll
                   $("body").css({"overflow-y":"auto"}); 
    
               });
            }, 500);
            
            
        },
        changeTab: function(cmp, event, helper) {
        	debugger;
            event.preventDefault();
            var a = event.currentTarget.id;
            var mapURLs = {};
            var esTGS = cmp.get('v.isTGS');
            //MAPEO WEB DESKTOP
            mapURLs['pca_home'] = '';
            mapURLs['pca_rManagementL'] = 'cwp-relationship-management';
            mapURLs['pca_equipotelefonica'] = 'cwp-telefonica-team';
            mapURLs['pca_customcalendar'] = 'cwp-calendar';
            mapURLs['pca_chattercustom'] = 'cwp-expert-community';
            mapURLs['pca_sTrackingL'] = 'cwp-service-tracking';
            mapURLs['pca_report'] = 'cwp-reports';
            mapURLs['pca_monitoreo'] = 'cwp-monitoring';
            mapURLs['pca_profile'] = 'profile/' + cmp.get("v.runningUser").Id;
            mapURLs['pca_login'] = 'login';
            mapURLs['pca-help'] = 'cwp-help';
            if (esTGS) {
                mapURLs['pca_reclamospedidos'] = 'cwp-tickets-orders';

            } else {
                mapURLs['pca_reclamospedidos'] = 'cwp-reclamo-pedidos';
            }
            mapURLs['pca_portales'] = 'cwp-portales';
            mapURLs['pca_myservice'] = 'cwp-my-services';

            if (esTGS) {
                mapURLs['pca_inventory'] = 'cwp-installed-services';

            } else {
                mapURLs['pca_inventory'] = 'cwp-services';
            }
            mapURLs['pca_documentmanagement'] = 'cwp-documentation';
            mapURLs['pca_facturacioncobranza'] = 'cwp-billing';
            mapURLs['pca_catalogo'] = 'cwp-market-place-menu';
            mapURLs['pca_ideascorner'] = 'cwp-ideas-corner';

            //MAPEO WEB MOBILE
            mapURLs['pca_home--m'] = '';
            //mapURLs['pca_rManagementL--m'] = 'cwp-relationship-management';
            mapURLs['pca_rManagementL--m'] = '#';
            mapURLs['pca_equipotelefonica--m'] = 'cwp-telefonica-team';
            mapURLs['pca_customcalendar--m'] = 'cwp-calendar';
            mapURLs['pca_chattercustom--m'] = 'cwp-expert-community';
            //mapURLs['pca_sTrackingL--m'] = 'cwp-service-tracking';
            mapURLs['pca_sTrackingL--m'] = '#';
            mapURLs['pca_report--m'] = 'cwp-reports';
            mapURLs['pca_monitoreo--m'] = 'cwp-monitoring';
            if (esTGS) {
                mapURLs['pca_reclamospedidos--m'] = 'cwp-tickets-orders';
            } else {
                mapURLs['pca_reclamospedidos--m'] = 'cwp-reclamo-pedidos';
            }
            mapURLs['pca_portales--m'] = 'cwp-portales';
            //mapURLs['pca_myservice--m'] = 'cwp-myservices';
            mapURLs['pca_myservice--m'] = '#';
            var esTGS = cmp.get('v.isTGS');
            if (esTGS) {
                mapURLs['pca_inventory--m'] = 'cwp-installed-services';

            } else {
                mapURLs['pca_inventory--m'] = 'cwp-services';
            }
            mapURLs['pca_documentmanagement--m'] = 'cwp-documentation';
            mapURLs['pca_facturacioncobranza--m'] = 'cwp-billing';
            mapURLs['pca_catalogo--m'] = 'cwp-market-place-menu';
            mapURLs['pca_ideascorner--m'] = 'cwp-ideas-corner';

debugger;
            var urlEvent = $A.get("e.force:navigateToURL");
	        /*urlEvent.setParams({
	            "url": "/"+ mapURLs[a]
	        });
	        urlEvent.fire();*/
            if(mapURLs[a]!='#'){
                document.location='/'+document.location.pathname.split('/')[1]+'/s/'+mapURLs[a];
            }
            


        },
        changeTabLogin: function(cmp, event, helper) {
debugger;
            event.preventDefault();
            cmp.set("v.runningUser", null);
            	/*
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": "../secur/logout.jsp",
                    //"isRedirect": true
                });
                urlEvent.fire();
                document.location="../secur/logout.jsp" ;*/
            	document.location='/'+document.location.pathname.split('/')[1]+'/secur/logout.jsp';
                
        },
        openOneDrive: function(cmp, event, helper) {
            debugger;
            event.preventDefault();
            var link = cmp.get('v.oneDriveLink');
            var urlEvent = $A.get("e.force:navigateToURL");
            if(link.indexOf("http")==-1)
            	link= "https://"+link;
            urlEvent.setParams({
                "url": link
            });
            urlEvent.fire();
            
        },
        changeUserAccount: function(cmp, event, helper) {
            helper.updateUser(cmp, event, helper);
        },
        doFinish: function(cmp, event, helper) {
            if (document.location.href.indexOf("/CWP/s/#") > 0) {
                document.getElementById("navbar").className = 'navbar-collapse collapse';
            }
            if (document.location.href.indexOf("cwp-relationship-management") > 0) {
                document.getElementById("pca_rManagementL").className = "tlf-nav__item selected";
            } else {
                if ( document.getElementById("pca_rManagementL") != null){
                	document.getElementById("pca_rManagementL").className = "tlf-nav__item";
                }
            }

            if (document.location.href.indexOf("cwp-telefonica-team") > 0) {
                if(document.getElementById("pca_rManagementL") != null){
                    document.getElementById("pca_rManagementL").className = "tlf-nav__item selected";
                }
                if(document.getElementById("pca_equipotelefonica").children[0] != null){
                    document.getElementById("pca_equipotelefonica").children[0].className = "tlf-nav__subLink active";
                }
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
                
            } else {
                if ( document.getElementById("pca_equipotelefonica") != null){
                	document.getElementById("pca_equipotelefonica").children[0].className = "tlf-nav__subLink";
                }
            }

            if (document.location.href.indexOf("cwp-calendar") > 0) {
                if( document.getElementById("pca_rManagementL") != null){
                    document.getElementById("pca_rManagementL").className = "tlf-nav__item selected";
                }
                if(document.getElementById("pca_customcalendar").children[0] != null){
                    document.getElementById("pca_customcalendar").children[0].className = "tlf-nav__subLink active";
                }
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }                                
            } else {
                if ( document.getElementById("pca_equipotelefonica") != null){
                	document.getElementById("pca_customcalendar").children[0].className = "tlf-nav__subLink";
                }
            }

            if (document.location.href.indexOf("cwp-expert-community") > 0) {
                if(document.getElementById("pca_rManagementL") != null){
                    document.getElementById("pca_rManagementL").className = 'tlf-nav__item selected';
                }
                if(document.getElementById("pca_chattercustom").children[0] != null){
                    document.getElementById("pca_chattercustom").children[0].className = "tlf-nav__subLink active";
                }    
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
            } else {
                if ( document.getElementById("pca_chattercustom") != null){
                	document.getElementById("pca_chattercustom").children[0].className = "tlf-nav__subLink";
                }    
            }

            if (document.location.href.indexOf("cwp-service-tracking") > 0) {
                if(document.getElementById("pca_sTrackingL") != null){
                	document.getElementById("pca_sTrackingL").className = "tlf-nav__item selected";
                }
            } else {
                if(document.getElementById("pca_sTrackingL") != null){
                	document.getElementById("pca_sTrackingL").className = "tlf-nav__item";
                }
            }

            if (document.location.href.indexOf("cwp-reports") > 0) {
                if(document.getElementById("pca_sTrackingL") != null){
                    document.getElementById("pca_sTrackingL").className = 'tlf-nav__item selected';
                }
                if(document.getElementById("pca_report").children[0] != null){
                    document.getElementById("pca_report").children[0].className = "tlf-nav__subLink active";
                }   
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
            } else {
                if ( document.getElementById("pca_report") != null){
                	document.getElementById("pca_report").children[0].className = "tlf-nav__subLink";
                }
            }

            if (document.location.href.indexOf("cwp-monitoring") > 0) {
                if(document.getElementById("pca_sTrackingL") != null){
                    document.getElementById("pca_sTrackingL").className = 'tlf-nav__item selected';
                }
                if(document.getElementById("pca_monitoreo").children[0] != null){
                    document.getElementById("pca_monitoreo").children[0].className = "tlf-nav__subLink active";
                }   
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
            } else {
                if ( document.getElementById("pca_monitoreo") != null){
                	document.getElementById("pca_monitoreo").children[0].className = "tlf-nav__subLink";
                }
            }

            if (document.location.href.indexOf("cwp-tickets-orders") > 0) {
                if(document.getElementById("pca_sTrackingL") != null){
                    document.getElementById("pca_sTrackingL").className = 'tlf-nav__item selected';
                }
                if(document.getElementById("pca_reclamospedidos").children[0] != null){
                    document.getElementById("pca_reclamospedidos").children[0].className = "tlf-nav__subLink active";
                }                
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
            } else {
                if ( document.getElementById("pca_reclamospedidos") != null){
                	document.getElementById("pca_reclamospedidos").children[0].className = "tlf-nav__subLink";
                }
            }

            if (document.location.href.indexOf("cwp-reclamo-pedidos") > 0) {
                if(document.getElementById("pca_sTrackingL") != null){
                    document.getElementById("pca_sTrackingL").className = 'tlf-nav__item selected';
                }
                if(document.getElementById("pca_reclamospedidos").children[0] != null){
                    document.getElementById("pca_reclamospedidos").children[0].className = "tlf-nav__subLink active";
                }
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
            } else {
                if ( document.getElementById("pca_reclamospedidos") != null){
                	document.getElementById("pca_reclamospedidos").children[0].className = "tlf-nav__subLink";
                }
            }

            if (document.location.href.indexOf("cwp-portales") > 0) {
                if(document.getElementById("pca_sTrackingL") != null){
                    document.getElementById("pca_sTrackingL").className = 'tlf-nav__item selected';
                }
                if(document.getElementById("pca_portales").children[0] != null){
                    document.getElementById("pca_portales").children[0].className = "tlf-nav__subLink active";
                }
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
            } else {
                if ( document.getElementById("pca_portales") != null){
                	document.getElementById("pca_portales").children[0].className = "tlf-nav__subLink";
                }
            }

            if (document.location.href.indexOf("cwp-my-services") > 0) {
                document.getElementById("pca_mServiceL").className = "tlf-nav__item selected";
            } else {
                if ( document.getElementById("pca_mServiceL") != null){
                    document.getElementById("pca_mServiceL").className = "tlf-nav__item";
                }
            }

            if (document.location.href.indexOf("cwp-installed-services") > 0) {
                if(document.getElementById("pca_mServiceL") != null){
                    document.getElementById("pca_mServiceL").className = 'tlf-nav__item selected';
                }
                if(document.getElementById("pca_inventory").children[0] != null){
                    document.getElementById("pca_inventory").children[0].className = "tlf-nav__subLink active";
                }
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
            } else {
                if ( document.getElementById("pca_inventory") != null){
                	document.getElementById("pca_inventory").children[0].className = "tlf-nav__subLink";
                }
            }

            if (document.location.href.indexOf("cwp-services") > 0) {
                if(document.getElementById("pca_mServiceL") != null){
                    document.getElementById("pca_mServiceL").className = 'tlf-nav__item selected';
                }
                if(document.getElementById("pca_inventory").children[0] != null){
                    document.getElementById("pca_inventory").children[0].className = "tlf-nav__subLink active";
                }           
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
            } else {
                if ( document.getElementById("pca_inventory") != null){
                	document.getElementById("pca_inventory").children[0].className = "tlf-nav__subLink";
                }
            }

            if (document.location.href.indexOf("cwp-documentation") > 0) {
                if(document.getElementById("pca_mServiceL") != null){
                    document.getElementById("pca_mServiceL").className = 'tlf-nav__item selected';
                }
                if(document.getElementById("pca_documentmanagement").children[0] != null){
                    document.getElementById("pca_documentmanagement").children[0].className = "tlf-nav__subLink active";
                }                
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
            } else {
                if ( document.getElementById("pca_documentmanagement") != null){
                	document.getElementById("pca_documentmanagement").children[0].className = "tlf-nav__subLink";
                }
            }

            if (document.location.href.indexOf("cwp-billing") > 0) {
                if(document.getElementById("pca_mServiceL") != null){
                    document.getElementById("pca_mServiceL").className = 'tlf-nav__item selected';
                }
                if(document.getElementById("pca_facturacioncobranza").children[0] != null){
                    document.getElementById("pca_facturacioncobranza").children[0].className = "tlf-nav__subLink active";
                }
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
            } else {
                if ( document.getElementById("pca_facturacioncobranza") != null){
                	document.getElementById("pca_facturacioncobranza").children[0].className = "tlf-nav__subLink";
                }
            }

            if (document.location.href.indexOf("cwp-market") > 0) {
                if(document.getElementById("pca_mServiceL") != null){
                    document.getElementById("pca_mServiceL").className = 'tlf-nav__item selected';
                }
                if(document.getElementById("pca_catalogo").children[0] != null){
                    document.getElementById("pca_catalogo").children[0].className = "tlf-nav__subLink active";
                }
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
            } else {
                if ( document.getElementById("pca_catalogo") != null){
                	document.getElementById("pca_catalogo").children[0].className = "tlf-nav__subLink";
                }
            }

            if (document.location.href.indexOf("cwp-ideas-corner") > 0) {
                if(document.getElementById("pca_ideascorner") != null){
                    document.getElementById("pca_ideascorner").className = 'tlf-nav__item selected';
                }
                if(document.getElementById("navbar") != null){
                    document.getElementById("navbar").className = 'navbar-collapse collapse';
                }
            } else {
                if ( document.getElementById("pca_ideascorner") != null){
                	document.getElementById("pca_ideascorner").className = "tlf-nav__item";
                }

            }

            /*var config3 = liquidFillGaugeDefaultSettings();
            config3.textVertPosition = 0.8;
            config3.waveAnimateTime = 5000;
            config3.waveHeight = 0.15;
            config3.waveAnimate = false;
            config3.waveOffset = 0.25;
            config3.valueCountUp = false;
            config3.displayPercent = false;
            var gauge3 = loadLiquidFillGauge("fillgauge3", 60.1);*/

        },
    	changePage : function(cmp, event, helper){

            if(event.kc){
               var idPage = event.getParam("page2Redirect");  
            } else {
               var idPage = event.currentTarget.id;  
            }
            
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/"+idPage
            });
        
            urlEvent.fire();
            
            var accId;
            
            if(cmp.get("v.listUsers").length>1){
                var doc = document.getElementById("name");
				accId = doc.options[doc.selectedIndex].value;
            }else{
                accId = cmp.find("currentAcc").getElement().name;
            }
            window.setTimeout(
                $A.getCallback(function() {
                    var passUserAccIds = $A.get("e.c:CWP_AC_passUserAcc");
                    passUserAccIds.setParams({"currentUserId" : cmp.get("v.runningUser").Id, "currentAccId" : accId});
                    console.log(passUserAccIds);
                    passUserAccIds.fire();
            
                }), 800
            );
 
    	}
    })