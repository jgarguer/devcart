({
    doInit : function(component, event, helper) {
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }
        helper.getInicialTickets(component, event, helper);
        // HasToSelectType
        var action = component.get("c.hasToSelectTypeF");
        action.setCallback(this, function(response) {
            debugger;
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                component.set('v.hasToSelectType', returnValue);
            }
        });
        $A.enqueueAction(action);
        // Current User
        var getCurrentUser = component.get("c.getCurrentUser");
        getCurrentUser.setCallback(this, function(responseUser) {
            debugger;
            if(component.isValid() && responseUser.getState() == "SUCCESS"){
                var returnValue = responseUser.getReturnValue();
                component.set('v.currentUser', returnValue);
            }
        });
        $A.enqueueAction(getCurrentUser);
        // CaseTypeOptions
        // 
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }
        var getCaseTypeOptions = component.get("c.getCaseTypeOpts");
        getCaseTypeOptions.setCallback(this, function(responseTypes) {
            debugger;
            if(component.isValid() && responseTypes.getState() == "SUCCESS"){
                if(document.getElementById("backgroundmodalwait")!=null){
                    document.getElementById("backgroundmodalwait").style.display='none';
                }
                var returnValue = responseTypes.getReturnValue();
                component.set('v.selecTypeList', returnValue);
                var list = component.get('v.selecTypeList');
                console.log(component.get('v.selecTypeList'));
            }
        });
        $A.enqueueAction(getCaseTypeOptions);
    },
    updateControllerSearch : function(component, event, helper) {
        
        //Calculamos el nuevo tableControl
        var tableController= component.get('v.ticketsTableController');
        var id = event.currentTarget.id;
        if(document.location.hash.indexOf('DESC') > 0){
            tableController.orderby=id + '::ASC';
            document.location.hash=id + '::ASC';
            event.preventDefault();
        }else{
            tableController.orderby=id + '::DESC';
        }
        
        helper.getTickets(component, event, helper,tableController);
    },
    updateControllerSearchText : function(component, event, helper) {
        debugger;
        //Calculamos el nuevo tableControl
        var tableController= component.get('v.ticketsTableController');
        var field = document.getElementById("selecciona").value;
        var val = document.getElementById("buscar").value;
        tableController.filter=field + ';'+val;
        helper.getTickets(component, event, helper,tableController);
    },
    nextPageTickets : function(component, event, helper) {
        debugger;
        //Calculamos el nuevo tableControl
        //
        var tableController= component.get('v.ticketsTableController');
        tableController.iniIndex+=tableController.viewSize;
        
        tableController.finIndex+=tableController.viewSize;
        if (!tableController.finIndex>tableController.numRecords)
            tableController.finIndex=tableController.numRecords;
        //Controlamos la paginación para la primera y la última página 
        if (tableController.iniIndex<=tableController.numRecords) 
            helper.getTickets(component, event, helper,tableController);
        
    },
    prevPageTickets : function(component, event, helper) {
        debugger;
        //Calculamos el nuevo tableControl
        var tableController= component.get('v.ticketsTableController');
        
        tableController.iniIndex-=tableController.viewSize;
        tableController.finIndex-=tableController.viewSize;
        if (tableController.iniIndex>0 ){
            helper.getTickets(component, event, helper,tableController);
        }
    },
    changeSizeTickets : function(component, event, helper) {
        debugger;
        //Calculamos el nuevo tableControl
        var tableController= component.get('v.ticketsTableController');
        var e = document.getElementById("paginationTickets");
        var newSize = e.options[e.selectedIndex].value;
        tableController.viewSize=newSize;
        tableController.iniIndex=1;
        tableController.finIndex=newSize;
        helper.getTickets(component, event, helper,tableController);
        
    },
    advancedFilterTickets : function(component, event, helper) {
        helper.getFieldValues(component, event, helper);
    }, 
    getServices : function(component, event, helper) {
        debugger;
        var e = document.getElementById("selectServiceFamilyTickets");
        var selectedServiceFamily = e.options[e.selectedIndex].value;
        
        var action = component.get("c.getServicesList");
        action.setParams({ serviceFamily :selectedServiceFamily});
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                debugger;
                var res = response.getReturnValue();
                component.set('v.serviceFields', JSON.parse(res["TGS_Product_Tier_2__c"]));
                component.set('v.serviceUnitFields', JSON.parse(res["TGS_Product_Tier_3__c"]));
            }
        });
        $A.enqueueAction(action);
    },
    getServicesUnits: function(component, event, helper) {
        debugger;
        var e = document.getElementById("selectServiceFamilyTickets");
        var selectedServiceFamily = e.options[e.selectedIndex].value;
        var e = document.getElementById("selectServiceTickets");
        var selectedService = e.options[e.selectedIndex].value;
        
        var action = component.get("c.getServiceUnitList");
        action.setParams({ serviceFamily :selectedServiceFamily, service :selectedService});
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                debugger;
                var res = response.getReturnValue();
                component.set('v.serviceUnitFields', JSON.parse(res["TGS_Product_Tier_3__c"]));
            }
        });
        $A.enqueueAction(action);
    },
    closeModal : function(component, event, helper) {
        if(document.getElementById('horaInicio')!=null){
            document.getElementById('horaInicio').value='none';
        }
        if(document.getElementById('asunto')!=null){
            document.getElementById('asunto').value='';
        }
        if(document.getElementById('descripcion')!=null){
            document.getElementById('descripcion').value=''
        }
        document.getElementById("tlf-detail").className ='modal fade tlf-new__modal';
        document.getElementById("tlf-detail").style.display ='none';
        document.getElementById("tlf-new").style.display='none';
        document.getElementById("tlf-new").classList.remove("in");
        document.getElementsByTagName("body")[0].classList.remove("modal-open");
        document.getElementById('backgroundmodal').style.display='none';
    },
    showBackground : function(component, event, helper) {
        document.getElementById('backgroundmodal').style.display='block';
    },
    viewAttach: function(component, event, helper) {
        var id = event.currentTarget.id;                
        document.getElementById("backgroundmodal").style.display='block';
        var action = component.get("c.getAttachs");
        action.setParams({ caseNumber :id});
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var response = response.getReturnValue();
                component.set('v.attachs', response);
            }
        });
        $A.enqueueAction(action);
    },
    newCase: function(component, event, helper) {
        debugger;
        component.set('v.showButtons', false);
        if(document.getElementById('contactData')!=null){
            document.getElementById("contactData").style.display='none';
        }
        document.getElementsByTagName("body")[0].className ='modal-open';
        if(document.getElementById('horaInicio')!=null){
            document.getElementById('horaInicio').value='none';
            document.getElementById("horaInicio").style.border=""; 
            document.getElementById("horaInicio").style.color="black";
        }
        if(document.getElementById("asunto") != null){
            document.getElementById("asunto").style.border=""; 
            document.getElementById("asunto").style.color="black";
        }
        if(document.getElementById("tipoCaso") != null){
            document.getElementById("tipoCaso").style.border=""; 
            document.getElementById("tipoCaso").style.color="black";
            document.getElementById("tipoCaso").selectedIndex=0;
        }
        
        if(document.getElementById("asuntoTech")!=null){
            document.getElementById("asuntoTech").style.border="";
            document.getElementById("asuntoTech").value=""; 
            document.getElementById("asuntoTech").style.color="black";
        }
        if(document.getElementById("descripcionTech")!=null){
            document.getElementById("descripcionTech").style.border="";
            document.getElementById("descripcionTech").value=""; 
            document.getElementById("descripcionTech").style.color="black";
        }
        if(document.getElementById("nombreId")!=null){
            document.getElementById("nombreId").style.border="";
            document.getElementById("nombreId").value=""; 
            document.getElementById("nombreId").style.color="black";
        }
        if(document.getElementById("apellidosId")!=null){
            document.getElementById("apellidosId").style.border="";
            document.getElementById("apellidosId").value=""; 
            document.getElementById("apellidosId").style.color="black";
        }
        if(document.getElementById("telefonoId")!=null){
            document.getElementById("telefonoId").style.border="";
            document.getElementById("telefonoId").value=""; 
            document.getElementById("telefonoId").style.color="black";
        }
        if(document.getElementById("movilId")!=null){
            document.getElementById("movilId").style.border="";
            document.getElementById("movilId").value=""; 
            document.getElementById("movilId").style.color="black";
        }
        if(document.getElementById("emailId")!=null){
            document.getElementById("emailId").style.border="";
            document.getElementById("emailId").value=""; 
            document.getElementById("emailId").style.color="black";
        }
        if(document.getElementById("lineaNegocio") != null){
            document.getElementById("lineaNegocio").style.border=""; 
            document.getElementById("lineaNegocio").style.color="black";
            document.getElementById("lineaNegocio").selectedIndex=0;
        }
        if(document.getElementById("serviceFam") != null){
            document.getElementById("serviceFam").style.border=""; 
            document.getElementById("serviceFam").style.color="black";
            document.getElementById("serviceFam").selectedIndex=0;
        }
        if(document.getElementById("buscarSede")!=null){
            
            document.getElementById("buscarSede").style.border="";
            document.getElementById("buscarSede").value=""; 
            document.getElementById("buscarSede").style.color="black";
        }
        if(document.getElementById("buscarCI")!=null){
            document.getElementById("buscarCI").style.border="";
            document.getElementById("buscarCI").value=""; 
            document.getElementById("buscarCI").style.color="black";
        }
        
        debugger;
        
        if(component.get('v.hasToSelectType')){
            component.set('v.isCasoComercial', false);
        }else {
            component.set('v.isCasoComercial', true);
            component.set('v.showButtons', true);
            helper.getReasonPicklistL(component, 'BI2_Caso_Padre');
        }
        component.set('v.blnCrearContacto', false);
        component.set('v.isTechCase', false);
        component.set('v.siteName','');
        component.set('v.selectedCI','');
        var listNew = [];
        component.set("v.attachsNames",listNew);
        component.set("v.attachsIDs",listNew);
        if(document.getElementById("labelbuttonarchivo")) document.getElementById("labelbuttonarchivo").innerHTML ='';
        if(document.getElementById("labelbuttonarchivo2")) document.getElementById("labelbuttonarchivo2").innerHTML ='';
        helper.deleteArch(component);
        document.getElementById("maxattchs").style.display="none";
        document.getElementById("backgroundmodal").style.display='block';
        document.getElementById("tlf-new").style.display='block';
        document.getElementById("tlf-new").classList.add("in");
        
    },
    viewDetails: function(component, event, helper) {
        debugger;
        component.set('v.showButtons', false);
        document.getElementById("backgroundmodalwait").style.display='block';
        document.getElementById("backgroundmodal").style.display='block';
        document.getElementById("tlf-detail").style.display='block';
        document.getElementById("tlf-detail").classList.add("in");
        
        var action = component.get("c.getCaseDetails");
        var id = event.currentTarget.id;
        action.setParams({ caseNumber :id});
        
        action.setCallback(this, function(response) {
            debugger;
            if(component.isValid() && response.getState() == "SUCCESS"){
                
                var res = response.getReturnValue();
                var caseRes= JSON.parse(res.caseDetail);
                component.set('v.showButtons', true);
                if(caseRes.RecordType.DeveloperName!==null && caseRes.RecordType.DeveloperName!==undefined &&( caseRes.RecordType.DeveloperName==='BIIN_Incidencia_Tecnica' || caseRes.RecordType.DeveloperName==='BIIN_Solicitud_Incidencia_Tecnica')){
                    component.set('v.ConsultaAccount', JSON.parse(res.ConsultaAccount));
                    component.set('v.ConsultaContact', JSON.parse(res.ConsultaContact));
                    component.set('v.caseParent', JSON.parse(res.caseParent));
                    component.set('v.isTechCase', true);
                    component.set('v.lworklog', JSON.parse(res.lworklog));
                    debugger;
                    var country= res.country;
                    if(country==='Peru')
                        component.set('v.esPeru', true);
                    else 
                        component.set('v.esPeru', false);
                    if(country==='Colombia')
                        component.set('v.esColombia', true);
                    else 
                        component.set('v.esColombia', false);
                    document.getElementById("tlf-detail").className ='modal fade tlf-new__modal in';
                    
                    document.getElementById("backgroundmodalwait").style.display='none';
                    document.getElementById("tlf-detail").style.display='block';
                }
                
                else {
                    component.set('v.isTechCase', false);
                    $A.enqueueAction(action2);
                    $A.enqueueAction(action3);
                }
                if(caseRes.RecordType.DeveloperName==='BIIN_Solicitud_Incidencia_Tecnica'){
                    $A.enqueueAction(action2);
                    $A.enqueueAction(action3);
                }
                
                component.set('v.caseDetail', caseRes);
            }
        });
        var action2 = component.get("c.getAttachs");
        action2.setParams({ caseNumber :id});
        action2.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var response = response.getReturnValue();
                component.set('v.attachs', response);
            }
        });
        var action3 = component.get("c.getComments");
        action3.setParams({ caseNumber :id});
        action3.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var response = response.getReturnValue();
                component.set('v.comments', response);
                document.getElementById("tlf-detail").className ='modal fade tlf-new__modal in';
                document.getElementById("tlf-detail").style.display='block';
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
        // $A.enqueueAction(action2);
        // $A.enqueueAction(action3);
        
    },
    closeComment: function(component, event, helper) {
        if(document.getElementById('descripcion2')!=null){
            document.getElementById('descripcion2').value=''
        }
        document.getElementById("descripcion2").style.border=""; 
        document.getElementById("descripcion2").style.color="black"; 
        document.getElementById("tlf-comment").style.display='none';
        document.getElementById("tlf-comment").className ='modal fade tlf-new__modal';
        document.getElementById("backgroundmodal").style.display='none';
        document.getElementById("tlf-attach-ok").style.display='none';
    },
    closeAttach: function(component, event, helper) {
        document.getElementById("backgroundmodal").style.display='none';
        document.getElementById("tlf-attachment").style='';
        document.getElementById("tlf-attachment").className ='modal fade tlf-new__modal';
    },
    saveComment: function(component, event, helper) {
        
        if(document.getElementById("descripcion2").value==''){
            document.getElementById("descripcion2").style.border="solid 2px red"; 
            document.getElementById("descripcion2").style.color="red"; 
            return;
        }
        document.getElementById("backgroundmodalwait").style.display='block';
        var id = event.currentTarget.id;   
        var value = document.getElementById("descripcion2").value;
        var action = component.get("c.newComment");
        action.setParams({ caseNumber :id, comment: value});
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                document.getElementById("tlf-comment-ok").style.display='block';
                document.getElementById("tlf-comment-ok").className ='modal fade tlf-new__modal in';
                document.getElementById("descripcion2").value='';
                document.getElementById("descripcion2").style.border=""; 
                document.getElementById("descripcion2").style.color="black"; 
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
    },
    saveCase: function(component, event, helper) {
        debugger;
        var recordType;
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }
        
        if(document.getElementById("tipoCaso") != null && document.getElementById("tipoCaso").value == 'none'){
            document.getElementById("tipoCaso").style.border="solid 2px red"; 
            document.getElementById("tipoCaso").style.color="red";
            document.getElementById("backgroundmodalwait").style.display='none';
            return 0;
        }
        if(document.getElementById("tipoCaso") != null){
            recordType = document.getElementById("tipoCaso").value;
        }else {
            var recordType ="BI2_Caso_Padre";
        }
        document.getElementById("backgroundmodalwait").style.display='block';
        document.getElementById("saveattach").style.display='none';
        document.getElementById("closesaveattach").style='';  
        var descr = '';
        var subject = '';
        var reason = '';
        
        if(component.get('v.isTechCase')){
            descr = document.getElementById("descripcionTech").value;
            
            subject = document.getElementById("asuntoTech").value;
            if(subject==''){
                document.getElementById("asuntoTech").style.border="solid 2px red"; 
                document.getElementById("asuntoTech").style.color="red";
                document.getElementById("backgroundmodalwait").style.display='none'; 
            }else {
                document.getElementById("asuntoTech").style.border=""; 
                document.getElementById("asuntoTech").style.color="black";
            }
            if(descr==''){
                document.getElementById("descripcionTech").style.border="solid 2px red"; 
                document.getElementById("descripcionTech").style.color="red";
                document.getElementById("backgroundmodalwait").style.display='none';
            }else {
                document.getElementById("descripcionTech").style.border=""; 
                document.getElementById("descripcionTech").style.color="black";
            }
            if(component.get('v.siteName')===''){
                document.getElementById("buscarSede").style.border="solid 2px red"; 
                document.getElementById("buscarSede").style.color="red";
                document.getElementById("backgroundmodalwait").style.display='none';
            }else {
                document.getElementById("buscarSede").style.border=""; 
                document.getElementById("buscarSede").style.color="black";
            }
            if(component.get('v.selectedCI')===''){
                document.getElementById("buscarCI").style.border="solid 2px red"; 
                document.getElementById("buscarCI").style.color="red";
                document.getElementById("backgroundmodalwait").style.display='none';
            }else {
                document.getElementById("buscarCI").style.border=""; 
                document.getElementById("buscarCI").style.color="black";
            }
            
            
        }else{
            descr = document.getElementById("descripcion").value;
            subject = document.getElementById("asunto").value;
            if(subject==''){
                document.getElementById("asunto").style.border="solid 2px red"; 
                document.getElementById("asunto").style.color="red"; 
            }
            reason = document.getElementById("horaInicio").value;
        }
        
        if(!component.get('v.isTechCase') && subject=='' ){
            document.getElementById("asunto").style.border="solid 2px red"; 
            document.getElementById("asunto").style.color="red"; 
            document.getElementById("backgroundmodalwait").style.display='none';
        }
        if(reason=='none'){
            document.getElementById("horaInicio").style.border="solid 2px red"; 
            document.getElementById("horaInicio").style.color="red"; 
            document.getElementById("backgroundmodalwait").style.display='none';
        }
        
        if(component.get('v.isTechCase')){
            if(subject!=='' && descr!=='' && component.get('v.siteName')!=='' &&  component.get('v.buscarCI')!==''){
                var insertTech = component.get("c.insertTechnicalCase");
                var contactDataList=[];
                debugger;
                if(component.get("v.blnCrearContacto")){
                    var camposInformados=true; 
                    if(document.getElementById("nombreId").value==''){
                        document.getElementById("nombreId").style.border="solid 2px red"; 
                        document.getElementById("nombreId").style.color="red";
                        camposInformados=false;
                    }
                    if(document.getElementById("apellidosId").value==''){
                        document.getElementById("apellidosId").style.border="solid 2px red"; 
                        document.getElementById("apellidosId").style.color="red";
                        camposInformados=false;
                    }
                    if(document.getElementById("telefonoId").value==''){
                        document.getElementById("telefonoId").style.border="solid 2px red"; 
                        document.getElementById("telefonoId").style.color="red";
                        camposInformados=false;
                    }
                    if(document.getElementById("movilId").value==''){
                        document.getElementById("movilId").style.border="solid 2px red"; 
                        document.getElementById("movilId").style.color="red";
                        camposInformados=false;
                    }
                    if(document.getElementById("emailId").value==''){
                        document.getElementById("emailId").style.border="solid 2px red"; 
                        document.getElementById("emailId").style.color="red";
                        camposInformados=false;
                    }
                    if (camposInformados){
                        document.getElementById("nombreId").style.border=""; 
                        document.getElementById("nombreId").style.color="black";
                        document.getElementById("apellidosId").style.border=""; 
                        document.getElementById("apellidosId").style.color="black";
                        document.getElementById("telefonoId").style.border=""; 
                        document.getElementById("telefonoId").style.color="black";
                        document.getElementById("movilId").style.border=""; 
                        document.getElementById("movilId").style.color="black";
                        document.getElementById("emailId").style.border=""; 
                        document.getElementById("emailId").style.color="black";
                        
                        contactDataList.push(document.getElementById("nombreId").value);
                        contactDataList.push(document.getElementById("apellidosId").value);
                        contactDataList.push(document.getElementById("telefonoId").value);
                        contactDataList.push(document.getElementById("movilId").value);
                        contactDataList.push(document.getElementById("emailId").value);
                        
                    }
                    else {
                        if(document.getElementById("backgroundmodalwait")!=null){
                            document.getElementById("backgroundmodalwait").style.display='none';
                        }
                        return 0;
                    }
                }
                insertTech.setParams({ currentUser : component.get('v.currentUser'), 
                                      siteName: component.get('v.siteName'), 
                                      siteDetailContacts: component.get('v.siteDetailContacts'),
                                      lineOfBus :document.getElementById('lineaNegocio').value,
                                      productService : document.getElementById('serviceFam').value,
                                      idProduct : document.getElementById('buscarCI').value, 
                                      recordTypeN: recordType,
                                      blnContactoConsultado: component.get('v.blnContactoConsultado'),
                                      blnCrearContacto : component.get('v.blnCrearContacto'),
                                      jsonDataContact: JSON.stringify(contactDataList), 
                                      subject: subject,
                                      descr :descr
                                     });
                insertTech.setCallback(this, function(response) {
                    if(component.isValid() && response.getState() == "SUCCESS"){
                        var control= JSON.parse(response.getReturnValue());
                        //Consultamos si el caso ha sido insertado 
                        debugger;
                        var control=JSON.parse(response.getReturnValue());
                        component.set('v.caseDetailNumber', control.newCase.CaseNumber);
                        document.getElementById("idNewCase").innerHTML=control.newCase.CaseNumber;
                        
                        
                        if(control.insertaCaso){
                            var actionIntegraRem = component.get("c.crearTicketRoD");
                            actionIntegraRem.setParams({ casoID : control.newCase.CaseNumber});
                            actionIntegraRem.setCallback(this, function(response) {
                                debugger; 
                                if(component.isValid() && response.getState() == "SUCCESS"){
                                    document.getElementById("tlf-new").style.display='none';
                                    document.getElementById("tlf-case-ok").style.display='block';
                                    document.getElementById("closesaveattach").style.display='none';                
                                    document.getElementById("tlf-case-ok").className ='modal fade tlf-new__modal in';
                                    document.getElementById("saveattach").style.display='none';
                                    document.getElementById("caseOkDiv").style.display='block';
                                    if(document.getElementById("backgroundmodalwait")!=null){
                                        document.getElementById("backgroundmodalwait").style.display='none';
                                        document.getElementById("backgroundmodal").style.display='none';
                                    }
                                    component.set('v.crearTicketRoD', 'false');
                                    
                                }else {console.log("Error")}
                            });
                            $A.enqueueAction(actionIntegraRem);                             
                            // helper.getInicialTickets(component, event, helper);
                        }
                        else{
                            //Hay que actualizar los datos del contacto
                            component.set('v.blnCrearContacto',control.blnCrearContacto);
                            component.set('v.blnContactoConsultado',true);
                            var action = component.get("c.getContactData");  
                            action.setCallback(this, function(response) {
                                if(component.isValid() && response.getState() == "SUCCESS"){
                                    debugger;
                                    document.getElementById("contactData").style.display='';
                                    var cData=JSON.parse(response.getReturnValue());
                                    document.getElementById("nombreId").value=cData.nombre;
                                    document.getElementById("apellidosId").value=cData.apellidos;
                                    document.getElementById("telefonoId").value=cData.telefono;
                                    document.getElementById("movilId").value=cData.movil;
                                    document.getElementById("emailId").value=cData.email; 
                                }
                                if(document.getElementById("backgroundmodalwait")!=null){
                                    document.getElementById("backgroundmodalwait").style.display='none';
                                    document.getElementById("backgroundmodal").style.display='none';
                                }
                            });
                            $A.enqueueAction(action);     
                        }
                    }
                
                });
                $A.enqueueAction(insertTech);
            }
        }else{
            if(subject!='' && reason!='none'){
                var status = 'Assigned';
                var confidential = false; 
                //            var recordType = '012w00000003BPcAAM';
                var action = component.get("c.insertCase");
                action.setParams({ descr :descr, subject: subject, reason: reason, status: status, confidential: confidential, recordTypeN: recordType});
                action.setCallback(this, function(response) {
                    if(component.isValid() && response.getState() == "SUCCESS"){
                        document.getElementById("tlf-new").style.display='none';
                        document.getElementById("tlf-case-ok").style.display='block';
                        document.getElementById("tlf-case-ok").className ='modal fade tlf-new__modal in';
                        document.getElementById("idNewCase").innerHTML=response.getReturnValue();
                        document.getElementById("descripcion").value = '';
                        document.getElementById("asunto").value = '';
                        
                    }
                    if(document.getElementById("backgroundmodalwait")!=null){
                        document.getElementById("backgroundmodalwait").style.display='none';
                    }
                });
                $A.enqueueAction(action);
            }
        }
    },
    saveCaseAttach: function(component, event, helper) {
        debugger;
        var recordType;
        if(document.getElementById("tipoCaso") != null && document.getElementById("tipoCaso").value == 'none'){ //JEG
            document.getElementById("tipoCaso").style.border="solid 2px red"; 
            document.getElementById("tipoCaso").style.color="red";
            document.getElementById("backgroundmodalwait").style.display='none';
            return 0;
        }
        if(document.getElementById("tipoCaso") != null){
            recordType = document.getElementById("tipoCaso").value;
        }else {
            var recordType ="BI2_Caso_Padre";
        }
        document.getElementById("backgroundmodalwait").style.display='block';
        document.getElementById("saveattach").style.display='none';
        document.getElementById("closesaveattach").style='';  
        var descr = '';
        var subject = '';
        var reason = '';
        
        if(component.get('v.isTechCase')){
            descr = document.getElementById("descripcionTech").value;
            
            subject = document.getElementById("asuntoTech").value;
            if(subject==''){
                document.getElementById("asuntoTech").style.border="solid 2px red"; 
                document.getElementById("asuntoTech").style.color="red";
                document.getElementById("backgroundmodalwait").style.display='none'; 
            }else {
                document.getElementById("asuntoTech").style.border=""; 
                document.getElementById("asuntoTech").style.color="black";
            }
            if(descr==''){
                document.getElementById("descripcionTech").style.border="solid 2px red"; 
                document.getElementById("descripcionTech").style.color="red";
                document.getElementById("backgroundmodalwait").style.display='none';
            }else {
                document.getElementById("descripcionTech").style.border=""; 
                document.getElementById("descripcionTech").style.color="black";
            }
            if(component.get('v.siteName')===''){
                document.getElementById("buscarSede").style.border="solid 2px red"; 
                document.getElementById("buscarSede").style.color="red";
                document.getElementById("backgroundmodalwait").style.display='none';
            }else {
                document.getElementById("buscarSede").style.border=""; 
                document.getElementById("buscarSede").style.color="black";
            }
            if(component.get('v.selectedCI')===''){
                document.getElementById("buscarCI").style.border="solid 2px red"; 
                document.getElementById("buscarCI").style.color="red";
                document.getElementById("backgroundmodalwait").style.display='none';
            }else {
                document.getElementById("buscarCI").style.border=""; 
                document.getElementById("buscarCI").style.color="black";
            }
        }else{
            descr = document.getElementById("descripcion").value;
            subject = document.getElementById("asunto").value;
            if(subject==''){
                document.getElementById("asunto").style.border="solid 2px red"; 
                document.getElementById("asunto").style.color="red"; 
            }
            reason = document.getElementById("horaInicio").value;
        }
        
        debugger;
        // Case Type
        
        
        if(!component.get('v.isTechCase') && subject==''){
            document.getElementById("asunto").style.border="solid 2px red"; 
            document.getElementById("asunto").style.color="red"; 
            document.getElementById("backgroundmodalwait").style.display='none';
        }
        if(reason=='none'){
            document.getElementById("horaInicio").style.border="solid 2px red"; 
            document.getElementById("horaInicio").style.color="red"; 
            document.getElementById("backgroundmodalwait").style.display='none';
        }
        
        if(component.get('v.isTechCase')){
            if(subject!=='' && descr!=='' && component.get('v.siteName')!=='' &&  component.get('v.buscarCI')!==''){
                var insertTech = component.get("c.insertTechnicalCase");
                var contactDataList=[];
                debugger;
                if(component.get("v.blnCrearContacto")){
                    var camposInformados=true; 
                    if(document.getElementById("nombreId").value==''){
                        document.getElementById("nombreId").style.border="solid 2px red"; 
                        document.getElementById("nombreId").style.color="red";
                        camposInformados=false;
                    }
                    if(document.getElementById("apellidosId").value==''){
                        document.getElementById("apellidosId").style.border="solid 2px red"; 
                        document.getElementById("apellidosId").style.color="red";
                        camposInformados=false;
                    }
                    if(document.getElementById("telefonoId").value==''){
                        document.getElementById("telefonoId").style.border="solid 2px red"; 
                        document.getElementById("telefonoId").style.color="red";
                        camposInformados=false;
                    }
                    if(document.getElementById("movilId").value==''){
                        document.getElementById("movilId").style.border="solid 2px red"; 
                        document.getElementById("movilId").style.color="red";
                        camposInformados=false;
                    }
                    if(document.getElementById("emailId").value==''){
                        document.getElementById("emailId").style.border="solid 2px red"; 
                        document.getElementById("emailId").style.color="red";
                        camposInformados=false;
                    }
                    if (camposInformados){
                        document.getElementById("nombreId").style.border=""; 
                        document.getElementById("nombreId").style.color="black";
                        document.getElementById("apellidosId").style.border=""; 
                        document.getElementById("apellidosId").style.color="black";
                        document.getElementById("telefonoId").style.border=""; 
                        document.getElementById("telefonoId").style.color="black";
                        document.getElementById("movilId").style.border=""; 
                        document.getElementById("movilId").style.color="black";
                        document.getElementById("emailId").style.border=""; 
                        document.getElementById("emailId").style.color="black";
                        
                        contactDataList.push(document.getElementById("nombreId").value);
                        contactDataList.push(document.getElementById("apellidosId").value);
                        contactDataList.push(document.getElementById("telefonoId").value);
                        contactDataList.push(document.getElementById("movilId").value);
                        contactDataList.push(document.getElementById("emailId").value);
                        
                    }
                    else {
                        if(document.getElementById("backgroundmodalwait")!=null){
                            document.getElementById("backgroundmodalwait").style.display='none';
                        }
                        return 0;
                    }
                }
                insertTech.setParams({ currentUser : component.get('v.currentUser'), 
                                      siteName: component.get('v.siteName'), 
                                      siteDetailContacts: component.get('v.siteDetailContacts'),
                                      lineOfBus :document.getElementById('lineaNegocio').value,
                                      productService : document.getElementById('serviceFam').value,
                                      idProduct : document.getElementById('buscarCI').value, 
                                      recordTypeN: recordType,
                                      blnContactoConsultado: component.get('v.blnContactoConsultado'),
                                      blnCrearContacto : component.get('v.blnCrearContacto'),
                                      jsonDataContact: JSON.stringify(contactDataList), 
                                      subject: subject,
                                      descr :descr
                                     });
                insertTech.setCallback(this, function(response) {
                    if(component.isValid() && response.getState() == "SUCCESS"){
                        var control= JSON.parse(response.getReturnValue());
                        //Consultamos si el caso ha sido insertado 
                        debugger;
                        var control=JSON.parse(response.getReturnValue());
                        component.set('v.caseDetailNumber', control.newCase.CaseNumber);
                        document.getElementById("idNewCase").innerHTML=control.newCase.CaseNumber;
                        if(control.insertaCaso){
                            document.getElementById("tlf-new").style.display='none';
                            document.getElementById("backgroundmodalwait").style.display='none';
                            document.getElementById("backgroundmodal").style.display='none';
                            document.getElementById("tlf-case-ok").style.display='block';
                            document.getElementById("closesaveattach").style.display='none';                
                            document.getElementById("tlf-case-ok").className ='modal fade tlf-new__modal in';
                            document.getElementById("saveattach").style.display='block';
                            document.getElementById("caseOkDiv").style.display='none';
                        }else{
                            //Hay que actualizar los datos del contacto
                            component.set('v.blnCrearContacto',control.blnCrearContacto);
                            component.set('v.blnContactoConsultado',true);
                            var action = component.get("c.getContactData");  
                            action.setCallback(this, function(response) {
                                if(component.isValid() && response.getState() == "SUCCESS"){
                                    debugger;
                                    document.getElementById("contactData").style.display='';
                                    var cData=JSON.parse(response.getReturnValue());
                                    document.getElementById("nombreId").value=cData.nombre;
                                    document.getElementById("apellidosId").value=cData.apellidos;
                                    document.getElementById("telefonoId").value=cData.telefono;
                                    document.getElementById("movilId").value=cData.movil;
                                    document.getElementById("emailId").value=cData.email; 
                                }
                                if(document.getElementById("backgroundmodalwait")!=null){
                                    document.getElementById("backgroundmodalwait").style.display='none';
                                }
                            });
                            $A.enqueueAction(action);     
                        }
                    }
                    if(document.getElementById("backgroundmodalwait")!=null){
                        document.getElementById("backgroundmodalwait").style.display='none';
                    }
                });
                $A.enqueueAction(insertTech);
            }
        }else{
            if(subject!='' && reason!='none'){
                var status = 'Assigned';
                var confidential = false; 
                //            var recordType = '012w00000003BPcAAM';
                var action = component.get("c.insertCase");
                action.setParams({ descr :descr, subject: subject, reason: reason, status: status, confidential: confidential, recordTypeN: recordType});
                action.setCallback(this, function(response) {
                    if(component.isValid() && response.getState() == "SUCCESS"){
                        document.getElementById("tlf-new").style.display='none';
                        document.getElementById("tlf-case-ok").style.display='block';
                        document.getElementById("closesaveattach").style.display='none';                
                        document.getElementById("tlf-case-ok").className ='modal fade tlf-new__modal in';
                        document.getElementById("idNewCase").innerHTML=response.getReturnValue();
                        document.getElementById("descripcion").value = '';
                        document.getElementById("asunto").value = '';
                        document.getElementById("saveattach").style.display='block';
                        component.set('v.caseDetailNumber', response.getReturnValue());
                        document.getElementById("backgroundmodalwait").style.display='none';
                        
                    }
                    if(document.getElementById("backgroundmodalwait")!=null){
                        document.getElementById("backgroundmodalwait").style.display='none';
                    }
                });
                $A.enqueueAction(action);
            }
        }
    },
    commentOk: function(component, event, helper) {
        document.getElementById("tlf-comment-ok").style.display='none';
        document.getElementById("tlf-comment-ok").className ='modal fade tlf-new__modal';
        document.getElementById("backgroundmodal").style.display='none';
        var id = event.currentTarget.id;
        var action = component.get("c.getComments");
        action.setParams({ caseNumber :id});
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var response = response.getReturnValue();
                component.set('v.comments', response);
            }
        });
        $A.enqueueAction(action);
    },   
    caseOk: function(component, event, helper) {
        debugger;
        document.getElementById("tlf-case-ok").style.display='none';
        document.getElementById("tlf-case-ok").className ='modal fade tlf-new__modal';
        document.getElementById("tlf-new").style.display='none';
        document.getElementsByTagName("body")[0].className ='';
        document.getElementById('backgroundmodal').style.display='none';
        document.getElementById('backgroundmodalwait').style.display='none';
        if (component.get('v.isTechCase')){            
            var actionIntegraRem = component.get("c.crearTicketRoD");
            actionIntegraRem.setParams({ casoID : component.get('v.caseDetailNumber')});
            actionIntegraRem.setCallback(this, function(response) {
                debugger; 
                if(component.isValid() && response.getState() == "SUCCESS"){
                    
                    
                }else {console.log("Error")}
            });
            $A.enqueueAction(actionIntegraRem);             
            
            
        }
        helper.getInicialTickets(component, event, helper);
    },  
    attachOk: function(component, event, helper) {
        document.getElementById("tlf-attach-ok").style.zIndex='7000';
        document.getElementById("tlf-attach-ok").style.display='block';
        document.getElementById("tlf-attach-ok").className ='modal fade tlf-new__modal';
        //document.getElementById("backgroundmodal").style.display='none';
        var id = event.currentTarget.id;
        var action = component.get("c.getAttachs");
        action.setParams({ caseNumber :id});
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var response = response.getReturnValue();
                component.set('v.attachs', response);
                helper.getInicialTickets(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    },        
    saveFile : function(component, event, helper) {
        var files=component.get('v.attachsIDs');
        
        if (files.length<3)
            helper.save(component, event, '');
        else{
            document.getElementById("saveattach").style.display="none";
            document.getElementById("maxattchs").style.display="block";
            
        }
        
    },
    saveFile2 : function(component, event, helper) {
        if (document.getElementById('archivo2').files.length)
            helper.save(component, event, '2');
    },
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner,'slds-hide');
    },
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner,'slds-hide');
    },
    checkmandatory : function (component, event, helper) {
        var select = document.getElementById("horaInicio").value;
        var text = document.getElementById("asunto").value;
        if(text != '' && select != ''){
            var element = component.find('closebuttonattachcase');
            $A.util.removeClass(element, "tlf-btn--disabled");
            element = component.find('savebuttonattachcase');
            $A.util.removeClass(element, "tlf-btn--disabled");
            document.getElementById("closebuttonattachcase").removeAttribute("disabled");
            document.getElementById("savebuttonattachcase").removeAttribute("disabled");            
        }else{
            var element = component.find('closebuttonattachcase');
            $A.util.addClass(element, "tlf-btn--disabled");
            element = component.find('savebuttonattachcase');
            $A.util.addClass(element, "tlf-btn--disabled");
            document.getElementById("closebuttonattachcase").createAttribute("disabled");
            document.getElementById("savebuttonattachcase").createAttribute("disabled");   
        }
        
    },  
    updateNameArchivo : function(component, event, helper){
        debugger;
        var fileInput = document.getElementById("archivo");
        var file = fileInput.files[0];
        if(file != null){
            if (file.size > helper.MAX_FILE_SIZE) {
                alert($A.get("$Label.c.CWP_COL_MaxFileExceed"));
                return;
            }else{
                document.getElementById("labelbuttonarchivo").innerHTML=file.name;
                
            }
        }
        
    },  
    updateNameArchivo2 : function(component, event, helper){
        debugger;
        var fileInput = document.getElementById("archivo2");
        var file = fileInput.files[0];
        if(file != null){
            if (file.size > helper.MAX_FILE_SIZE) {
                alert($A.get("$Label.c.CWP_COL_MaxFileExceed"));
                return;
            }else{
                document.getElementById("labelbuttonarchivo2").innerHTML=file.name;
                
            }
        }
        
    },
    updateControllerSearchTextEnter : function(component, event, helper){
        if(event.keyCode == 13){
            event.preventDefault();
            //Calculamos el nuevo tableControl
            var tableController= component.get('v.ticketsTableController');
            var field = document.getElementById("selecciona").value;
            var val = document.getElementById("buscar").value;
            tableController.filter=field + ';'+val;
            helper.getTickets(component, event, helper,tableController);
        }
    }, 
    showAlert : function(component, event, helper){
        debugger;
        if(document.getElementById("horaInicio").value.indexOf("Inconformidad")>-1)
            component.set('v.showAlert', true);
        else {
            component.set('v.showAlert', false);
        }
    }, 
    hideAlert : function(component, event, helper){
        component.set('v.showAlert', false);
        
    },
    selectingType : function(component, event, helper){
        debugger;
        var recordType;
        
        document.getElementById("tipoCaso").style.border=""; 
        document.getElementById("tipoCaso").style.color="black";
        
        document.getElementById("backgroundmodalwait").style.display='none';
        if(document.getElementById("tipoCaso")){
            recordType = document.getElementById("tipoCaso").value;
            if(recordType == 'BI2_caso_comercial'){
                component.set('v.isCasoComercial', true);
                component.set('v.isTechCase', false);
                component.set('v.showButtons', true);
            } if(recordType == 'BI2_Caso_Padre'){
                component.set('v.isCasoComercial', true);
                component.set('v.isTechCase', false);
                component.set('v.showButtons', true);
            } else if(recordType == 'BIIN_Solicitud_Incidencia_Tecnica'){
                component.set('v.isCasoComercial', false);
                component.set('v.isTechCase', true);
                component.set('v.showButtons', true);
            }else if(recordType == 'none'){
                component.set('v.isCasoComercial', false);
                component.set('v.isTechCase', false);
                component.set('v.showButtons', false);
            }
            if(recordType !== 'none'){
                helper.getReasonPicklistL(component, recordType);
            }
        }
    }, 
    selectLineOfBusiness : function(component, event, helper){
        debugger;
        if(document.getElementById("lineaNegocio") != null){
            // Current User
            if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='block';
            }
            var getDepValues = component.get("c.getDependentValues");
            getDepValues.setParams({ controllingValue : document.getElementById("lineaNegocio").value});
            getDepValues.setCallback(this, function(responseDepVal) {
                debugger;
                if(component.isValid() && responseDepVal.getState() == "SUCCESS"){
                    var returnValue = responseDepVal.getReturnValue();
                    component.set('v.famServiceList', returnValue);
                }
                if(document.getElementById("backgroundmodalwait")!=null){
                    document.getElementById("backgroundmodalwait").style.display='none';
                }
            });
            $A.enqueueAction(getDepValues);
        }
    },
    checkLength : function(component, event, helper){
        var id = event.target.id;
        if(id === 'buscarSede' && document.getElementById(id) != null && document.getElementById(id).value.length >= 3){
            document.getElementById('buscarSedeButton').classList.remove('tlf-btn--disabled');
            document.getElementById('buscarSedeButton').disabled = false;
        }else if(id === 'buscarSede' && document.getElementById(id) != null && document.getElementById(id).value.length < 3){
            document.getElementById('buscarSedeButton').classList.add('tlf-btn--disabled');
            document.getElementById('buscarSedeButton').disabled = true;
        }
        
        if(id == 'buscarCI' && document.getElementById(id) != null && document.getElementById(id).value.length >= 3){
            document.getElementById('buscarCIButton').classList.remove('tlf-btn--disabled');
            document.getElementById('buscarCIButton').disabled = false;
        }else if(id == 'buscarCI' && document.getElementById(id) != null && document.getElementById(id).value.length < 3){
            document.getElementById('buscarCIButton').classList.add('tlf-btn--disabled');
            document.getElementById('buscarCIButton').disabled = true;
        }
    }, 
    buscarSede : function(component, event, helper){
        debugger;
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }
        var siteName = document.getElementById('buscarSede').value;
        var accountId = component.get('v.currentUser').Contact.BI_Cuenta_activa_en_portal__c;
        if(siteName != null && siteName != ''){
            // Llamada a Buscar Sede
            var getContactCol = component.get("c.getContactCol");
            getContactCol.setParams({ siteName : siteName, 
                                     accountId : accountId});
            getContactCol.setCallback(this, function(responseContactCol) {
                debugger;
                if(component.isValid() && responseContactCol.getState() == "SUCCESS"){
                    var returnValue = responseContactCol.getReturnValue();
                    component.set('v.sedeList', returnValue);
                    document.getElementById('SedeResults').classList.add('in');
                }
                if(document.getElementById("backgroundmodalwait")!=null){
                    document.getElementById("backgroundmodalwait").style.display='none';
                }
            });
            $A.enqueueAction(getContactCol);
        }
    }, 
    selectSede : function(component, event, helper){
        debugger;
        var sedeList = component.get('v.sedeList');
        var selectedSede = sedeList[event.currentTarget.dataset.index];
        component.set('v.siteName', selectedSede.BIIN_Site__c);
        component.set('v.siteDetailContacts', selectedSede.TGS_Site_details_contacts__c);
        document.getElementById('buscarSede').value = selectedSede.TGS_Site_details_contacts__c;
        document.getElementById('SedeResults').classList.remove('in');
    },
    buscarCI : function(component, event, helper){
        debugger;
        document.getElementById("backgroundmodalwait").style.display='block';
        var CINameInput = document.getElementById('buscarCI').value;
        var accountId = component.get('v.currentUser').Contact.BI_Cuenta_activa_en_portal__c;
        var siteName = document.getElementById('buscarSede').value;
        if(CINameInput != null && CINameInput != ''){
            // Llamada a Buscar CI
            var getCICol = component.get("c.getCICol");
            getCICol.setParams({query : CINameInput,
                                accountId : accountId,
                                siteName : siteName});
            getCICol.setCallback(this, function(responseCICol) {
                debugger;
                if(component.isValid() && responseCICol.getState() == "SUCCESS"){
                    var returnValue = responseCICol.getReturnValue();
                    component.set('v.ciList', returnValue);
                    document.getElementById('CIResults').classList.add('in');
                }
                document.getElementById("backgroundmodalwait").style.display='none';
            });
            $A.enqueueAction(getCICol);
        }
    },
    selectCI : function(component, event, helper){
        debugger;
        var ciList = component.get('v.ciList');
        var selectedCI = ciList[event.currentTarget.dataset.index];
        component.set("v.selectedCI",selectedCI);
        document.getElementById('buscarCI').value = selectedCI.BIIN_Id_Producto__c;
        document.getElementById('CIResults').classList.remove('in');
    },
    getAttFromRemedy1 : function(component, event, helper){
        debugger;
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='';
        }
        var columna=event.target.id.split('---$$$$---');;
        var attNameParam=columna[1];
        var worklogIdParam=columna[0];
        var s= JSON.stringify(component.get("v.caseDetail"));
        var action = component.get("c.getAttFromRem");
        action.setParams({caseIncS : s,
                          attachName : attNameParam,
                          worklogId : worklogIdParam,
                          posicionWI : 'z2AF Work Log01'});
        action.setCallback(this, function(responseCICol) {
            debugger;
            if(component.isValid() && responseCICol.getState() == "SUCCESS"){
                var returnValue = responseCICol.getReturnValue();
                
                var url= $A.get("$Label.c.CWP_Url")+'/servlet/servlet.FileDownload?file='+returnValue;
                var win = window.open(url, '_blank');
                win.focus();
                
                
            }
            document.getElementById("backgroundmodalwait").style.display='none';
        });
        $A.enqueueAction(action);
        
    },
    getAttFromRemedy2 : function(component, event, helper){
        debugger;
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='';
        }
        var columna=event.target.id.split('---$$$$---');;
        var attNameParam=columna[1];
        var worklogIdParam=columna[0];
        var s= JSON.stringify(component.get("v.caseDetail"));
        var action = component.get("c.getAttFromRem");
        action.setParams({caseIncS : s,
                          attachName : attNameParam,
                          worklogId : worklogIdParam,
                          posicionWI : 'z2AF Work Log02'});
        action.setCallback(this, function(responseCICol) {
            debugger;
            if(component.isValid() && responseCICol.getState() == "SUCCESS"){
                var returnValue = responseCICol.getReturnValue();
                
                var url= $A.get("$Label.c.CWP_Url")+'/servlet/servlet.FileDownload?file='+returnValue;
                var win = window.open(url, '_blank');
                win.focus();
                
                
            }
            document.getElementById("backgroundmodalwait").style.display='none';
        });
        $A.enqueueAction(action);
        
    },
    getAttFromRemedy3 : function(component, event, helper){
        debugger;
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='';
        }
        var columna=event.target.id.split('---$$$$---');;
        var attNameParam=columna[1];
        var worklogIdParam=columna[0];
        var s= JSON.stringify(component.get("v.caseDetail"));
        var action = component.get("c.getAttFromRem");
        action.setParams({caseIncS : s,
                          attachName : attNameParam,
                          worklogId : worklogIdParam,
                          posicionWI : 'z2AF Work Log03'});
        action.setCallback(this, function(responseCICol) {
            debugger;
            if(component.isValid() && responseCICol.getState() == "SUCCESS"){
                var returnValue = responseCICol.getReturnValue();
                
                var url= $A.get("$Label.c.CWP_Url")+'/servlet/servlet.FileDownload?file='+returnValue;
                var win = window.open(url, '_blank');
                win.focus();
                
                
            }
            document.getElementById("backgroundmodalwait").style.display='none';
        });
        $A.enqueueAction(action);
        
    },
    openWorkInfo : function(component, event, helper){
        debugger;
        var id = event.target.id;
        component.set('v.caseDetailNumber', id);
        document.getElementById("tlf-workInfo").style.zIndex=9000;
        document.getElementById("tlf-workInfo").style.display='block';
        document.getElementById("creacion").style.display='block';
        document.getElementById("creado").style.display='none';
        style="display:none"
        
        document.getElementById("tlf-workInfo").className ='modal fade tlf-new__modal in';
        
    },
    closeWorkInfo  : function(component, event, helper){
        document.getElementById("tlf-workInfo").style.display='none';
        document.getElementById("tlf-workInfo").className ='modal fade tlf-new__modal';
        if(document.getElementById('horaInicio')!=null){
            document.getElementById('horaInicio').value='none';
        }
        if(document.getElementById('asunto')!=null){
            document.getElementById('asunto').value='';
        }
        if(document.getElementById('descripcion')!=null){
            document.getElementById('descripcion').value=''
        }
        document.getElementById("tlf-new").style.display='none';
        document.getElementById("tlf-new").classList.remove("in");
        document.getElementsByTagName("body")[0].classList.remove("modal-open");
        document.getElementById('backgroundmodal').style.display='none';
    },
    updateNameArchivo_WI1 : function(component, event, helper){
        var fileInput = document.getElementById("archivo_WI1");
        var file = fileInput.files[0];
        if(file != null){
            if (file.size > helper.MAX_FILE_SIZE) {
                alert($A.get("$Label.c.CWP_COL_MaxFileExceed"));
                return;
            }else{
                document.getElementById("labelbuttonarchivo_WI1").innerHTML=file.name;
            }
        }
        
    }, 
    updateNameArchivo_WI2 : function(component, event, helper){
        var fileInput = document.getElementById("archivo_WI2");
        var file = fileInput.files[0];
        if(file != null){
            if (file.size > helper.MAX_FILE_SIZE) {
                alert($A.get("$Label.c.CWP_COL_MaxFileExceed"));
                return;
            }else{
                document.getElementById("labelbuttonarchivo_WI2").innerHTML=file.name;
            }
        }
        
    }, 
    updateNameArchivo_WI3 : function(component, event, helper){
        var fileInput = document.getElementById("archivo_WI3");
        var file = fileInput.files[0];
        if(file != null){
            if (file.size > helper.MAX_FILE_SIZE) {
                alert($A.get("$Label.c.CWP_COL_MaxFileExceed"));
                return;
            }else{
                document.getElementById("labelbuttonarchivo_WI3").innerHTML=file.name;
            }
        }
        
    }, 
    saveWorkInfo : function(component, event, helper){
        debugger;
        var descr = document.getElementById("workInfodesc").value;
        
        var id= event.currentTarget.id;
        if(descr==''){
            document.getElementById("workInfodesc").style.border="solid 2px red"; 
            document.getElementById("workInfodesc").style.color="red";
            document.getElementById("backgroundmodalwait").style.display='none';
            return;
        }
        
        var attchs= [];
        if (document.getElementById('archivo_WI1').files.length)
            attchs.push('archivo_WI1');
        if (document.getElementById('archivo_WI2').files.length)
            attchs.push('archivo_WI2');
        if (document.getElementById('archivo_WI3').files.length)
            attchs.push('archivo_WI3');
        if(attchs.length>0)
            helper.saveWI(component, event, attchs);
        else {
            document.getElementById("backgroundmodalwait").style.display='block';
            var action = component.get("c.crearNota");
            action.setParams({caseId : id,
                              attIds: '', 
                              casoNota: document.getElementById("workInfodesc").value
                             });
            action.setCallback(this, function(res) {
                debugger;
                if(component.isValid() && res.getState() == "SUCCESS"){
                    var returnValue = res.getReturnValue();
                    
                    document.getElementById("creacion").style.display='none';
                    document.getElementById("creado").style.display='block';
                    document.getElementById("labelbuttonarchivo_WI1").innerHTML ='';
                    document.getElementById("labelbuttonarchivo_WI2").innerHTML ='';
                    document.getElementById("labelbuttonarchivo_WI3").innerHTML ='';
                    document.getElementById("workInfodesc").value ='';
                    document.getElementById("backgroundmodalwait").style.display='none';
                    document.getElementById("tlf-detail").style.display='none';
                    document.getElementById('backgroundmodal').style.display='none';
                }
                
            });
            $A.enqueueAction(action);
        }
    },
    closeCase : function(cmp,evt,helper) {
        document.getElementById("backgroundmodalwait").style.display='block';
        var caseSelec=cmp.get('v.caseDetail');
        debugger;
        var mensajeRet;
        if (caseSelec.TGS_SLA_Light__c!=='Red'){
            var action = cmp.get("c.closeCaseAPEX");
            action.setParams({caseJSON : JSON.stringify(caseSelec)});
            action.setCallback(this, function(res) {
                debugger;
                mensajeRet=$A.get("$Label.c.CWP_errorWhileClosing");
                if(cmp.isValid() && res.getState() == "SUCCESS"){
                    var returnValue = res.getReturnValue();
                    document.getElementById("backgroundmodalwait").style.display='none';
                    if(returnValue==='200'){
                        mensajeRet=  $A.get("$Label.c.CWP_caseClosed");
                    }else if(returnValue!== undefined){
                        mensajeRet= $A.get("$Label.c.BI2_Control_cierre_caso_padre");
                    }
                }
                cmp.set("v.msgCierre", mensajeRet);
                var d = document.getElementById("tlf-caseClosed-ok");
                d.className += " in";
                d.style.zIndex='9999';
                d.style.display='block';
                document.getElementById("backgroundmodalwait").style.display='none';
            });
            $A.enqueueAction(action);
        }else{
            mensajeRet=$A.get("$Label.c.CWP_cierreCaso");
            cmp.set("v.msgCierre", mensajeRet);
            var d = document.getElementById("tlf-caseClosed-ok");
            d.className += " in";
            d.style.zIndex='9999';
            d.style.display='block';
            document.getElementById("backgroundmodalwait").style.display='none';
        }
    },
    caseClosedOk : function(component, event, helper) {
        debugger;
        var d = document.getElementById("tlf-caseClosed-ok");
        d.style.display='none';
        d.classList.remove('in');
        component.set('v.showButtons', false);
        document.getElementById("backgroundmodalwait").style.display='block';
        document.getElementById("backgroundmodal").style.display='block';
        document.getElementById("tlf-detail").style.display='block';
        var action = component.get("c.getCaseDetails");
        var caseDet = component.get('v.caseDetail');
        action.setParams({ caseNumber :caseDet.CaseNumber});
        action.setCallback(this, function(response) {
            debugger;
            if(component.isValid() && response.getState() == "SUCCESS"){
                var res = response.getReturnValue();
                var caseRes= JSON.parse(res.caseDetail);
                component.set('v.showButtons', true);
                if(caseRes.RecordType.DeveloperName!==null && caseRes.RecordType.DeveloperName!==undefined &&( caseRes.RecordType.DeveloperName==='BIIN_Incidencia_Tecnica' || caseRes.RecordType.DeveloperName==='BIIN_Solicitud_Incidencia_Tecnica')){
                    component.set('v.ConsultaAccount', JSON.parse(res.ConsultaAccount));
                    component.set('v.ConsultaContact', JSON.parse(res.ConsultaContact));
                    component.set('v.caseParent', JSON.parse(res.caseParent));
                    component.set('v.isTechCase', true);
                    component.set('v.lworklog', JSON.parse(res.lworklog));
                    debugger;
                    var country= res.country;
                    if(country==='Peru')
                        component.set('v.esPeru', true);
                    else 
                        component.set('v.esPeru', false);
                    if(country==='Colombia')
                        component.set('v.esColombia', true);
                    else 
                        component.set('v.esColombia', false);
                    document.getElementById("tlf-detail").className ='modal fade tlf-new__modal in';
                    document.getElementById("backgroundmodalwait").style.display='none';
                }
                
                else {
                    component.set('v.isTechCase', false);
                    $A.enqueueAction(action2);
                    $A.enqueueAction(action3);
                }
                component.set('v.caseDetail', caseRes);
            }
        });
        var action2 = component.get("c.getAttachs");
        action2.setParams({ caseNumber :caseDet.CaseNumber});
        action2.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var response = response.getReturnValue();
                component.set('v.attachs', response);
            }
        });
        var action3 = component.get("c.getComments");
        action3.setParams({ caseNumber :caseDet.CaseNumber});
        action3.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var response = response.getReturnValue();
                component.set('v.comments', response);
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
        // $A.enqueueAction(action2);
        // $A.enqueueAction(action3);
    },
})