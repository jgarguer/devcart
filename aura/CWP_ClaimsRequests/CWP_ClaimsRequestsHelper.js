({
    MAX_FILE_SIZE: 4500000, /* 6 000 000 * 3/4 to account for base64 */
    CHUNK_SIZE: 450000, /* Use a multiple of 4 */
    
    getInicialTickets : function(component, evt,helper) {
        debugger;
        /* if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        } */
        var action = component.get("c.getRecords");  
        action.setParams({ tableControllerS :'init' });
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                var res = response.getReturnValue();
                component.set('v.ticketsList', JSON.parse(res["lisTickets"]));
                component.set('v.ticketsTableController', JSON.parse(res["offsetController"]));
                component.set('v.headersList', JSON.parse(res["recordsdHeaders"]));
                component.set('v.valuesForSize',JSON.parse(res["recordsPage"]));
            }
          /*  if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            } */
        });
        $A.enqueueAction(action);     
    },
    getTickets : function(component, event,helper, tableControl) {   
        
        document.getElementById("backgroundmodalwait").style.display='block';
        var action = component.get("c.getRecords");    
        action.setParams({ tableControllerS :JSON.stringify(tableControl) });
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                
                
                var returnValue = response.getReturnValue();
                
                var res = response.getReturnValue();
                component.set('v.ticketsList', JSON.parse(res["lisTickets"]));
                component.set('v.ticketsTableController', JSON.parse(res["offsetController"]));
                component.set('v.headersList', JSON.parse(res["recordsdHeaders"]));
                
                var id = document.location.hash.substring(1).split("::")[0];
                if(id!=''){
                    var clases = document.getElementsByClassName("headerSortUp");
                    for (x = 0; x < clases.length; x++)
                    {
                        clases[x].classList.remove('headerSortUp');
                    }
                    var clases = document.getElementsByClassName("headerSortDown");
                    for (x = 0; x < clases.length; x++)
                    {
                        clases[x].classList.remove('headerSortDown');
                    }
                    
                    if(document.location.hash.indexOf('DESC') > 0){
                        document.getElementById(id).parentElement.classList.remove('headerSortUp');
                        document.getElementById(id).parentElement.classList.add('headerSortDown');
                        event.preventDefault();
                    }else{
                        document.getElementById(id).parentElement.classList.add('headerSortUp');
                        document.getElementById(id).parentElement.classList.remove('headerSortDown');
                    }
                }
            }
            document.getElementById("backgroundmodalwait").style.display='none';
            
        });
        $A.enqueueAction(action);
    }, 
    getFieldValues : function(component, evt,helper) {   
        var action = component.get("c.getFieldsValues");    
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();                
                var res = response.getReturnValue();
                component.set('v.caseTypeFields', JSON.parse(res["type"]));
                component.set('v.serviceFamilyFields', JSON.parse(res["TGS_Product_Tier_1__c"]));
                component.set('v.serviceFields', JSON.parse(res["TGS_Product_Tier_2__c"]));
                component.set('v.serviceUnitFields', JSON.parse(res["TGS_Product_Tier_3__c"]));
                component.set('v.statusFields', JSON.parse(res["status"]));
                component.set('v.cityFields', JSON.parse(res["City"]));
                component.set('v.countryFields', JSON.parse(res["Country"]));
                component.set('v.toShowFields', JSON.parse(res["toShow"]));
            }
        });
        $A.enqueueAction(action);
    },  
    save : function(component, event, idInput) {
        debugger;
        document.getElementById("backgroundmodalwait").style.display='block';
        var fileInput = document.getElementById('archivo'+idInput).files;
        if(fileInput[0] == null){
            document.getElementById("backgroundmodalwait").style.display='none';
            return;
        }
        var id = event.currentTarget.id;
        var file = fileInput[0];
        
        if (file.size > this.MAX_FILE_SIZE) {
            alert('File size cannot exceed ' +this. MAX_FILE_SIZE + ' bytes.\n' +
                  'Selected file size: ' + file.size);
            return;
        }
        
        var fr = new FileReader();
        
        var self = this;
        fr.onload = $A.getCallback( function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            
            fileContents = fileContents.substring(dataStart);
            
            self.upload(component, file, fileContents, id);
        });
        
        fr.readAsDataURL(file);
    },
    getNameOfId: function (component){
        var action = component.get("c.getNameOfId");
        action.setParams({attachsIDs:component.get('v.attachsIDs')});
        action.setCallback(this, function(responseCICol){
        debugger;
        if(component.isValid() && responseCICol.getState() == "SUCCESS"){
                var returnValue = responseCICol.getReturnValue();
                component.set('v.attachsNames',returnValue);
 
            }
            document.getElementById("backgroundmodalwait").style.display='none';
        });
        $A.enqueueAction(action);
    },
    deleteAttachment: function (component,event){
        var action = component.get("c.deleteAttachment");
        var idAtt = event.currentTarget.id;
        action.setParams({attachmentId:idAtt,
                          caseId: component.get('v.caseDetailNumber')});
        action.setCallback(this, function(responseCICol){
        debugger;
        if(component.isValid() && responseCICol.getState() == "SUCCESS"){
                var returnValue = responseCICol.getReturnValue();
                component.set('v.attachsidsAftDelete',returnValue);
 
            }
            document.getElementById("backgroundmodalwait").style.display='none';
        });
        $A.enqueueAction(action);
    },
    upload: function(component, file, fileContents, id) {
        
        var fromPos = 0;
        var toPos = Math.min(fileContents.length, fromPos + this.CHUNK_SIZE);
        
        // start with the initial chunk
        this.uploadChunk(component, file, fileContents, fromPos, toPos, '', id);   
    },
    uploadChunk : function(component, file, fileContents, fromPos, toPos, attachId, id) {
        var actionFile = component.get("c.saveTheChunk"); 
        var chunk = fileContents.substring(fromPos, toPos);
        
        actionFile.setParams({
            parentId: component.get("v.parentId"),
            fileName: file.name,
            base64Data: encodeURIComponent(chunk),             
            contentType: file.type,
            fileId: attachId,
            caseNumber: id
        });
        
        var self = this;
        actionFile.setCallback(this, function(a) {
            if(component.isValid() && a.getState() == "SUCCESS"){
                attachId = a.getReturnValue();
                                
                fromPos = toPos;
                toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);
                
                if (fromPos < toPos) {
                    self.uploadChunk(component, file, fileContents, fromPos, toPos, attachId, id);  
                }else{   
                    debugger;
                    this.getNameOfId(component);
                    var attchs2= component.get('v.attachsIDs');
                    attchs2.push(attachId);
                    component.set('v.attachsIDs', attchs2);
                    
                    document.getElementById("backgroundmodalwait").style.display='none';
                    
                    document.getElementById("tlf-attach-ok").style='display:block;z-index:6000;';
                    debugger;
                    document.getElementById("tlf-attach-ok").className ='modal fade tlf-new__modal in';
                    
                    if(document.getElementById("labelbuttonarchivo"))
                        document.getElementById("labelbuttonarchivo").innerHTML ='';
                    if(document.getElementById("labelbuttonarchivo2"))
                        document.getElementById("labelbuttonarchivo2").innerHTML ='';
                    this.deleteArch(component);
                    window.scrollTo(0, 0);
                }
            }
            
        });
        
        $A.enqueueAction(actionFile); 
    },
    deleteArch : function(component) {
        debugger;
        if(document.getElementById("archivo")) 
                        document.getElementById("archivo").value='';
        if(document.getElementById("archivo2")) 
                        document.getElementById("archivo2").value='';
        if(document.getElementById("archivo_WI1")) 
                        document.getElementById("archivo_WI1").value='';
        if(document.getElementById("archivo_WI2")) 
                        document.getElementById("archivo_WI2").value='';
        if(document.getElementById("archivo_WI3")) 
                        document.getElementById("archivo_WI3").value='';
    },        
    saveWI : function(component, event, adjuntos) {
        debugger;
        document.getElementById("backgroundmodalwait").style.display='block';
        
        var id = event.currentTarget.id;
        var file0;
        var fr0 = new FileReader();
        var file1;
        var fr1 = new FileReader();
        var file2;
        var fr2 = new FileReader();
        
        for (i = 0; i < adjuntos.length; i++) {
            var fileInput = document.getElementById(adjuntos[i]).files;
            
            
            var self = this;
            if (i===0){
                file0 = fileInput[0];
                
                if (file0.size > this.MAX_FILE_SIZE) {
                    alert('File size cannot exceed ' +this. MAX_FILE_SIZE + ' bytes.\n' +
                          'Selected file size: ' + file0.size);
                    return;
                }
                fr0.onload = $A.getCallback( function() {
                    var fileContents = fr0.result;
                    var base64Mark = 'base64,';
                    var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                    
                    fileContents = fileContents.substring(dataStart);
                    
                    self.uploadWI(component, file0, fileContents, id);
                });
                fr0.readAsDataURL(file0);
            }else if(i===1){
                file1 = fileInput[0];
                
                if (file1.size > this.MAX_FILE_SIZE) {
                    alert('File size cannot exceed ' +this. MAX_FILE_SIZE + ' bytes.\n' +
                          'Selected file size: ' + file1.size);
                    return;
                }
                fr1.onload = $A.getCallback( function() {
                    var fileContents = fr1.result;
                    var base64Mark = 'base64,';
                    var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                    
                    fileContents = fileContents.substring(dataStart);
                    
                    self.uploadWI(component, file1, fileContents, id);
                });
                fr1.readAsDataURL(file1);
                
            }else if(i===2){
                file2 = fileInput[0];
                
                if (file2.size > this.MAX_FILE_SIZE) {
                    alert('File size cannot exceed ' +this. MAX_FILE_SIZE + ' bytes.\n' +
                          'Selected file size: ' + file2.size);
                    return;
                }
                fr2.onload = $A.getCallback( function() {
                    var fileContents = fr2.result;
                    var base64Mark = 'base64,';
                    var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                    
                    fileContents = fileContents.substring(dataStart);
                    
                    self.uploadWI(component, file2, fileContents, id);
                });
                fr2.readAsDataURL(file2);
            }
        }
    },
    uploadWI: function(component, file, fileContents, id) {
        
        var fromPos = 0;
        var toPos = Math.min(fileContents.length, fromPos + this.CHUNK_SIZE);
        
        // start with the initial chunk
        this.uploadChunkWI(component, file, fileContents, fromPos, toPos, '', id);   
    },
    uploadChunkWI : function(component, file, fileContents, fromPos, toPos, attachId, id) {
        var actionFile = component.get("c.saveTheChunk"); 
        var chunk = fileContents.substring(fromPos, toPos);
        
        actionFile.setParams({
            parentId: component.get("v.parentId"),
            fileName: file.name,
            base64Data: encodeURIComponent(chunk),             
            contentType: file.type,
            fileId: attachId,
            caseNumber: id
        });
        
        var self = this;
        actionFile.setCallback(this, function(a) {
            if(component.isValid() && a.getState() == "SUCCESS"){
                debugger;
                attachId = a.getReturnValue();
                
                fromPos = toPos;
                toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);
                
                if (fromPos < toPos) {
                    self.uploadChunkWI(component, file, fileContents, fromPos, toPos, attachId, id);  
                }else{
                    var attchs2= component.get('v.attachsIDs');
                    attchs2.push(attachId);
                    component.set('v.attachsIDs', attchs2);
                    var attchs= [];
                    if (document.getElementById('archivo_WI1').files.length)
                        attchs.push('archivo_WI1');
                    if (document.getElementById('archivo_WI2').files.length)
                        attchs.push('archivo_WI2');
                    if (document.getElementById('archivo_WI3').files.length)
                        attchs.push('archivo_WI3');
                    if(attchs.length === attchs2.length){
                        debugger;
                        var action = component.get("c.crearNota");
                        action.setParams({caseId : id,
                                          attIds: attchs2, 
                                          casoNota: document.getElementById("workInfodesc").value
                                         });
                        action.setCallback(this, function(res) {
                            debugger;
                            if(component.isValid() && res.getState() == "SUCCESS"){
                                var returnValue = res.getReturnValue();
                                
                                document.getElementById("creacion").style.display='none';
                                document.getElementById("creado").style.display='block';
                                document.getElementById("labelbuttonarchivo_WI1").innerHTML ='';
                                document.getElementById("labelbuttonarchivo_WI2").innerHTML ='';
                                document.getElementById("labelbuttonarchivo_WI3").innerHTML ='';
                                document.getElementById("workInfodesc").value ='';
                                document.getElementById("backgroundmodalwait").style.display='none';
                                document.getElementById("tlf-detail").style.display='none';
                                document.getElementById('backgroundmodal').style.display='none';
                                this.deleteArch(component);
                            }
                            
                        });
                        $A.enqueueAction(action);
                        
                    }
                    
                }
            }
            
        });
        
        $A.enqueueAction(actionFile); 
    },
    getReasonPicklistL : function(component,recordType) {
        debugger;
        document.getElementById("backgroundmodalwait").style.display='block';
        var action = component.get("c.getReasonPicklist");
        action.setParams({caseRecordtype : recordType});
        action.setCallback(this, function(res) {
            debugger;
            if(component.isValid() && res.getState() == "SUCCESS"){
                var returnValue = res.getReturnValue();
                component.set("v.reasonOptions",returnValue);
            }
            document.getElementById("backgroundmodalwait").style.display='none';
        });
        $A.enqueueAction(action);
    }

})