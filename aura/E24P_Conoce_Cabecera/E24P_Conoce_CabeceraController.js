({
    handleVerticalClick : function(component, event, helper) {
        var name = event.getSource().getLocalId();
        console.log('name:  ' + name)
        var appEvent = $A.get("e.c:E24P_verticalSelection");
        switch(name){
            case '1':
                console.log('case 1')
                appEvent.setParams({
                    "tab" : "tab1" });
                break;
            case '2':
                console.log('case 2')
                appEvent.setParams({
                    "tab" : "tab2" });
                break;
            case '3':
                console.log('case 3')
                appEvent.setParams({
                    "tab" : "tab3" });
        }
        console.log('appTab:' + appEvent.getParam("tab"))
        appEvent.fire();
       
    },
    
    handleTabSelection:function(component,event,helper){
        var tab = event.getParam("tab");
        switch(tab){
            case "tab1":
		component.set("v.verticalSelected", "descripcion");
                break;
            case "tab2":
                component.set("v.verticalSelected", "relacionamiento");
                break;
                            case "tab3":
                component.set("v.verticalSelected", "competidores");
        }
    }
})