({
	cancelDialog : function(cmp, event, helper) {
		var homeEvt = $A.get("e.force:navigateToObjectHome");
		homeEvt.setParams({
    	"scope": "FO_Forecast__c"
		});
		homeEvt.fire();
	},
    doInit : function(cmp,evt,help){
        
        cmp.find("forceRecord").getNewRecord(
        "FO_Forecast__c",
        null,
        false,
        $A.getCallback(function() {
            var rec = cmp.get("v.recordField");
           
             var action = cmp.get("c.getPer");
            action.setCallback(this,function(response){
                
               //cmp.set("v.recordField.FO_Periodo__c",response.getReturnValue().Id) ; 
               cmp.set("v.perName",response.getReturnValue())
            });   
            $A.enqueueAction(action);
            var actio2 = cmp.get("c.getNodes");
            actio2.setCallback(this,function(reponse){
                cmp.set("v.nodosSF",reponse.getReturnValue());
                var inputSe = cmp.find("IdNodo");
                inputSe.set("v.value",reponse.getReturnValue()[0].Id);
            });
            $A.enqueueAction(actio2);
        })
    );
       
    },
    saveRecord : function(cmp, event, helper) {
        var idNodo = cmp.find("IdNodo").get("v.value");
        if(idNodo==null){
            alert("Hay que seleccionar un nodo para poder crear un nodo");
            return;
        }
        
		cmp.set("v.recordField.FO_Nodo__c",idNodo);
        cmp.set("v.recordField.FO_Periodo__c",cmp.get("v.perName").Id);
        var tempRec = cmp.find("forceRecord");
        tempRec.saveRecord($A.getCallback(function(result) {
    		console.log(result.state);
		    var resultsToast = $A.get("e.force:showToast");
		    if (result.state === "SUCCESS") {
        		resultsToast.setParams({
		            "title": $A.get("$Label.c.FO_Toast_Title"),
    	        	"message": $A.get("$Label.c.FO_Toast_body")
        		});
	        	resultsToast.fire();  
                var recId = result.recordId;
				helper.navigateTo(cmp, recId);
    		} else if (result.state === "ERROR") {
    		    console.log('Error: ' + JSON.stringify(result.error));
                cmp.set("v.Error",result.error[0].message);
				$A.util.removeClass(cmp.find("error-msg"),"slds-hide");                
		       
	    	} else {
    	    	console.log('Unknown problem, state: ' + result.state + ', error: ' + JSON.stringify(result.error));
    		}
		}));
    }
})