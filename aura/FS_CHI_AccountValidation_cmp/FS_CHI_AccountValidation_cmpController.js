({
    doInit: function(cmp, evt, helper){   
 
            var action = cmp.get("c.doInitController");
        	console.log(cmp.get("v.accountId"));
       		action.setParams({ accountId : cmp.get("v.accountId") });
      
        
       
        action.setCallback(this, function(response){
            
            response.getState();
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var res= response.getReturnValue();
               
                var cmpTarget = cmp.find('banner');
                if(res.id==='200'){
                    res.title='Proceso Completado';
                    $A.util.addClass(cmpTarget, 'slds-theme--success');
                }else if(res.id==='300'){
                    res.title='Advertencia';
                    $A.util.addClass(cmpTarget, 'slds-theme--warning');
                }else{
                    res.title='Atención';
                    $A.util.addClass(cmpTarget, 'slds-theme--error');
                }
                cmp.set("v.response",res);
            }else{
                
               
                 res.title='Atención';
                 $A.util.addClass(cmpTarget, 'slds-theme--error');
                 res.error = 'Error al validar el cliente. Contacte con su Administrador';
            }
        });
         $A.enqueueAction(action);
        
    },
    showSpinner : function (cmp, evt, helper) {
        var spinner = cmp.find('spinner');
        $A.util.removeClass(spinner,'slds-hide');        
        var generic_error = cmp.find('generic_error');
        $A.util.addClass(generic_error,'slds-hide');
    },
    hideSpinner : function (cmp, evt, helper) {
        var spinner = cmp.find('spinner');
        $A.util.addClass(spinner,'slds-hide');
         var generic_error = cmp.find('generic_error');
        $A.util.removeClass(generic_error,'slds-hide');
    },
    closeError : function (cmp, evt, helper) {
        debugger;
        var lightningAppExternalEvent = $A.get("e.c:FS_CHI_evnt");
        lightningAppExternalEvent.fire();        
    },
})