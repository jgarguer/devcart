({
    doInit : function (component, event, helper) { 
        var action = component.get('c.getFooter');
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                
                if(returnValue != null){
                    var context = JSON.parse(response.getReturnValue());
                    
                    if(context.esTGS != undefined && context.esTGS){
                        component.set("v.showOldFooter", false);
                        $A.createComponent(
                            "c:TGS_PopUpFooter",
                            {
                                "userLang": context.userLang,
                                "firstTime": context.firstLogin
                            },
                            function(footerNew, status, errorMessage){
                                
                                if (status === "SUCCESS") {
                                    
                                    component.set("v.newFooter", footerNew);
                                }
                                else if (status === "INCOMPLETE") {
                                    console.log("No response from server or client is offline.")
                                    // Show offline error
                                }
                                    else if (status === "ERROR") {
                                        console.log("Error: " + errorMessage);
                                        // Show error message
                                    }
                            }
                        );
                        
                    } else {
                        component.set("v.context", context);
                        component.set("v.showOldFooter", true);
                    }
                }
            }
        });
        
        $A.enqueueAction(action);
        
    },
    openLink: function(cmp, event, helper) {
        debugger;
        event.preventDefault();
        var link = event.currentTarget.id;
        var urlEvent = $A.get("e.force:navigateToURL");
        if(link.indexOf("http")==-1)
            link= "https://"+link;
        urlEvent.setParams({
            "url": link
        });
        urlEvent.fire();
    },
})