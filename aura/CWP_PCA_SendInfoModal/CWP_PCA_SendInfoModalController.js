({
	openModal : function(cmp, evt, helper) {
        debugger;
        helper.resetAll(cmp);
        helper.getLoadInfo(cmp, evt, helper);
    },
    hideModal : function(cmp) {
        var element = cmp.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
    },
})