({
    init : function(component, event, helper) {
        var action = component.get("c.doGetEvent");
        action.setParams({
            "recordId" : component.get("v.recordId")
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var evento = response.getReturnValue();
                component.set("v.evento", evento );
                
                if (evento.BI_FVI_Estado__c==='Planificada') {
                    component.set("v.estadoBoolean", true);
                } else { 
                    var nombreEncuesta = component.get("v.nombreEncuesta");
                    component.set("v.estadoBoolean", false);
                    console.log('catch:' + evento);
                    //console.log('Survey Name: ' + evento.BI_FVI_SurveyTaken__r.Name);
                    
                    try{
                        
                        console.log('Survey taken name ' + evento.BI_FVI_SurveyTaken__r.Name);
                        window.open('https://telefonicab2b--devcart--c.cs89.visual.force.com/apex/TakeSurvey?id=a140E00000A6hOGQAZ&cId=none&caId=none&evId='+evento.Id+'&surId='+evento.BI_FVI_SurveyTaken__r.Name);  
                        
                    }catch(e){
                        console.log('He entrado en el catch')
                        window.open('https://telefonicab2b--devcart--c.cs89.visual.force.com/apex/TakeSurvey?id=a140E00000A6hOGQAZ&cId=none&caId=none&evId='+evento.Id);
                    }
                    
                    $A.get("e.force:closeQuickAction").fire();
                }
            }
        });
        $A.enqueueAction(action);
    }
})