({
    doInit : function(cmp, evt, helper) {
        
        var action = cmp.get("c.getImages");
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                console.log(response.getReturnValue());
                var mapInfo = response.getReturnValue();
                var lstImages = mapInfo.lstImages;
                var userLang = mapInfo.isoLang;
                
                if(userLang != 'es' && userLang != 'en_US' && userLang != 'pt_BR'){
                    userLang = 'en_US';
                }
                
                //$Label.c.TGS_SDNPortal
                //$Label.c.TGS_ServicesCatalog
                //$Label.c.TGS_SiteSurvey
                //$Label.c.TGS_PerformanceDash
                //$Label.c.TGS_Bandwidth
                //$Label.c.TGS_ServiceDetails
                for(var i in lstImages){
                    console.log(lstImages[i]);
                    lstImages[i].CWP_Resource__c = $A.get(lstImages[i].CWP_Resource__c);
                    
                    lstImages[i].title = $A.get('$Label.c.' + lstImages[i].CWP_Description_Label__c).split('#title#')[0];
                    lstImages[i].CWP_Description_Label__c = $A.get('$Label.c.' + lstImages[i].CWP_Description_Label__c).split('#title#')[1];
                	lstImages[i].CWP_Image_name__c =  userLang + '/' +lstImages[i].CWP_Image_name__c;
                }
                cmp.set('v.lstImages', lstImages);
                console.log(lstImages);
            }
        });
        $A.enqueueAction(action);
    },
    navigateClick : function(cmp, evt, helper){
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": $A.get('$Label.c.TGS_urlSeda')
        });
        urlEvent.fire();
    }
})