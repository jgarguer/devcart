({
    doInit: function(cmp, evt, helper){
        
        
        var action = cmp.get("c.doInitController");
        action.setParams({ contactId : cmp.get("v.contactId") });
        
        action.setCallback(this, function(response){
            
            debugger;
            response.getState();
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var res= response.getReturnValue();
               
                var cmpTarget = cmp.find('banner');
                if(res.id==='200'){
                     res.title='Proceso Completado';
                    $A.util.addClass(cmpTarget, 'slds-theme--success');
                }else if(res.id==='300'){
                    res.title='Advertencia';
                    $A.util.addClass(cmpTarget, 'slds-theme--warning');
                }else if(res.id==='500'){
                    res.title='Atención';
                    $A.util.addClass(cmpTarget, 'slds-theme--error');
                }
                cmp.set("v.response",res);
            }else{
                alert('ERROR');
            }
        });
         $A.enqueueAction(action);
        
    },
    showSpinner : function (cmp, evt, helper) {
        var spinner = cmp.find('spinner');
        $A.util.removeClass(spinner,'slds-hide');
       
    },
    hideSpinner : function (cmp, evt, helper) {
        var spinner = cmp.find('spinner');
        $A.util.addClass(spinner,'slds-hide');
         var generic_error = cmp.find('generic_error');
        $A.util.removeClass(generic_error,'slds-hide');
    },
    closeError : function (cmp, evt, helper) {
        debugger;
        var lightningAppExternalEvent = $A.get("e.c:FS_CHI_evnt");
        lightningAppExternalEvent.fire();        
    },
})