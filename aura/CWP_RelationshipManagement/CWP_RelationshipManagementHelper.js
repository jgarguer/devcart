({
		 getMapVisibility: function(component,event){
    	var action = component.get("c.getPermissionSet");
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){
                    component.set('v.mapVisibility', returnValue);
                }
            }
        });
        $A.enqueueAction(action);
        
    }
})