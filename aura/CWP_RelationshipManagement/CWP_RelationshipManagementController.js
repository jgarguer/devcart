({

    doInit : function(component, event,helper) {

            var imgUrl;
            var mainInfo = component.get('c.getServiceTrackingInfo');
            component.set("v.title", '{!$Label.c.PCA_Tab_RelMan_menu}');
            mainInfo.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var mainInfo = response.getReturnValue();
                    imgUrl= "url('/servlet/servlet.FileDownload?file=" + mainInfo['imgId']+ "')";
                    setTimeout(
                        function(){
                            if(mainInfo['title'] != undefined){
                                component.set("v.title", mainInfo['title']);
                            }
                            if(mainInfo['imgId'] != undefined){
                                document.getElementById('mainImageRM').style.backgroundImage = imgUrl;
                                document.getElementById('mainImageRM').style.padding="2.5rem 4.0625rem";
                                document.getElementById('mainImageRM').style.minHeight="21.875rem";
                                document.getElementById('mainImageRM').style.backgroundPosition="center"
                            }else{
                                document.getElementById('mainImageRM').className = "tlf-slide";     
                            }
                            //url(!$Resource.PCA_imgSlider + 'Relationship.png')
                    }, 1);
                }else{
                    alert("fallo!");
                }
            });
            $A.enqueueAction(mainInfo);
            
            helper.getMapVisibility(component, event);  
        
    },
    
    changeTab : function(component, event, helper) {
    
        event.preventDefault();
        var a=event.currentTarget.id;
        var mapURLs={};
        
		mapURLs['pca_equipotelefonica'] = 'cwp-telefonica-team';
        mapURLs['pca_customcalendar'] = 'cwp-calendar';
        mapURLs['pca_chattercustom'] = 'cwp-expert-community';        
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/"+ mapURLs[a]
        });
        urlEvent.fire();
    }

})