({
	redirect : function(cmp, evt, helper) {
		console.log('redirect');
		var recordId = evt.currentTarget.id;
		console.log(recordId);
		//Navigate to RecordDetail component, needed recordId
		var evt = $A.get("e.force:navigateToComponent");
		evt.setParams({
			componentDef : "c:BI_SF1_RecordDetail",
			componentAttributes: {
				"recordId" : recordId
			},
			isredirect : false
		});
		evt.fire();
	}
})