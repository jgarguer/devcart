({
	doInit : function(cmp, evt, helper) {
		var action = cmp.get("c.getOi");
        action.setParams({
            recordId : cmp.get("v.recordId")
        });
         action.setCallback(this, function(response) {
             console.log("entro en doInit");
             console.log(response.getReturnValue());
             cmp.set("v.oiData", response.getReturnValue());
         });
        $A.enqueueAction(action);
	}
})