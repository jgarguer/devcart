({
    openModal : function(component, event, helper) {
        document.getElementsByTagName("body")[0].className ='modal-open';
        document.getElementById("backgroundmodalwait").style.display='block';
        document.getElementById("backgroundmodal").style.display='block';
        document.getElementById("add-event").style.display='block';
        document.getElementById("add-event").className ='modal fade tlf-new__modal in';
        // Quito marcadores de error en caso de que los haya
        component.find('hourInitId').getElement().style.border="";
        component.find('hourInitId').getElement().style.color="";
        component.find('hourFinId').getElement().style.border="";
        component.find('hourFinId').getElement().style.color="";
        component.find('subjectevent').getElement().style.border="";
        component.find('subjectevent').getElement().style.color="";
        component.find("selectedUserId").getElement().style.border="";
        component.find("selectedUserId").getElement().style.color="";
        
        helper.resetAll(component);
        helper.getLoadInfo(component, event, helper);
    },
    hideModal : function(component, event, helper) {
    	//alert($A.get("$Label.c.CWP_ErrorCreatingEvent"));
        document.getElementsByTagName("body")[0].className ='';
        document.getElementById('backgroundmodal').style.display='none';
        document.getElementById("add-event").style.display='none';
        document.getElementById("add-event").className ='modal fade tlf-new__modal';
        //helper.hideModal(component, event);
    },
    save: function(component, event, helper) {
        document.getElementById('backgroundmodal').style.display='block';      
debugger;        
        var initHour = component.find("hourInitId").get("v.value");
        console.log('initHour from field: ' + initHour);
        var initHourControl=new Date (new Date().toDateString() + ' ' + initHour);
        var finishHour = component.find("hourFinId").get("v.value");
        var finishHourControl=new Date (new Date().toDateString() + ' ' + finishHour);
        

        document.getElementById('backgroundmodalwait').style.display='block';
        var data = component.get('v.data');
        var eventId = data.eventId;
        
        var eventObject = component.get('v.eventObject');
        var fecha = data.fecha;
        
        var selectedUser = '';
        var sel = component.get('v.selectedUser');
        for(var i = 0; sel.length > i; i++) {
            var temp = sel[i].split(',');
            selectedUser =  selectedUser+temp[0]+';';      
        }
        

        if (finishHourControl < initHourControl){
        	component.find('hourInitId').getElement().style.border="solid 2px red";
            component.find('hourInitId').getElement().style.color="red";
            component.find('hourFinId').getElement().style.border="solid 2px red";
            component.find('hourFinId').getElement().style.color="red";
        }
        var subjectevent = component.find('subjectevent').getElement().value;
        if (subjectevent == ''){
            component.find('subjectevent').getElement().style.border="solid 2px red";
            component.find('subjectevent').getElement().style.color="red";
        }
        if(selectedUser == ''){
         	component.find("selectedUserId").getElement().style.border="solid 2px red";
            component.find("selectedUserId").getElement().style.color="red";           
        }

        var webDate = data.day;
        
        var errorMessageUserNotSelected = 'No ha seleccionado ningún usuario';
        
        if(selectedUser != '' && subjectevent != '' && (finishHourControl > initHourControl)){
            var action = component.get("c.doSave");
            var auxiliar = JSON.stringify(eventObject)
            action.setParams({'eventS':auxiliar, 'fecha':fecha,'selectedUser':selectedUser,'initHour':initHour,'finishHour':finishHour, 'webDate' : webDate});
            
            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS") {
        			
                    if(response.getReturnValue()){
                        var event = component.getEvent('updateCalendar');
                        event.fire();
                        document.getElementById('backgroundmodal').style.display='none';                        
                    }
                } else {
                  //  alert('Error Creating Event');
                    document.getElementById('backgroundmodal').style.display='none';
					document.getElementById('backgroundmodalwait').style.display='none';
                }
            });
            $A.enqueueAction(action);
        }else{
        	//document.getElementById('backgroundmodal').style.display='none';
            document.getElementById('backgroundmodalwait').style.display='none';
        }
    },
    deleteEvent: function(component, event, helper) {
        document.getElementById('backgroundmodalwait').style.display='block';
        var data = component.get('v.data');
        var eventId = data.eventId;
        
        var action = component.get("c.getDeleteEvent");
        
        action.setParams({'eventId':eventId});
        
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                if(response.getReturnValue()){
                    helper.hideModal(component, event);
                    var event = component.getEvent('updateCalendar');
                    event.fire();   
                }
                
            }else{
              //  alert('Error Deleting Event');
                document.getElementById('backgroundmodalwait').style.display='none';
            }
        });
        $A.enqueueAction(action); 
    },
    add: function(component, event, helper) {
        var allUsers = 'allUser';
        var selectedUsers = 'selectedUser';
        
        helper.addRemove(component,allUsers,selectedUsers);
        
    },
    remove: function(component, event, helper) {
        var allUsers = 'allUser';
        var selectedUsers = 'selectedUser';
        
        helper.addRemove(component,selectedUsers,allUsers);
    }
})