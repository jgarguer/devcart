({
    getLoadInfo : function(component, event, helper) {
        var eventId = event.getParam("id");
        var pastDay = event.getParam("pastDay");
        var day = event.getParam("day");
        if(eventId==null){        	
        	 var action = component.get("c.getEmpetyInfo");	
	        action.setParams({'day':day});
	        
	        action.setCallback(this, function(response) {
	            if (response.getState() === "SUCCESS") {
	                component.set('v.data', response.getReturnValue());
	                helper.getSelectedUser(component, event);
	                helper.getUsers(component, event);
	                helper.getEvent(component, event);
	                helper.getHours(component, event);
	                //this.showModal(component, event);
	            }
	        });
	        $A.enqueueAction(action);
        }else{    
	        var action = component.get("c.getLoadInfo");
	
	        action.setParams({'eventId':eventId,'pastDay':pastDay,'day':day});
	        
	        action.setCallback(this, function(response) {
	            if (response.getState() === "SUCCESS") {
	                var resp=response.getReturnValue();
	            	var data={};
	            	data['eventId']= resp.Id;
	            	data['edit']= true;
	            	data['fecha']= new Date(resp.StartDateTime).toISOString().slice(0,10);
	            	data['initHour']= this.getHour(resp.StartDateTime);
	            	data['finishHour']= this.getHour(resp.EndDateTime);
	            	component.set('v.data', data);
	            	
	            	var eventObject={};
	            	eventObject['Subject__c']=resp.Subject;
	            	eventObject['Description__c']=resp.Description;	            	
	                eventObject['Id']=resp.Id;	 
	                component.set('v.eventObject', eventObject);
	                
	                helper.getSelectedUser(component, event);
	                helper.getUsers(component, event);
	                helper.getEvent(component, event);
	                helper.getHours(component, event);
	                //this.showModal(component, event);
	            }
	        });
	        $A.enqueueAction(action);
      }
    },
    
    getHour: function(date){
    	var date=new Date(date);
    	
    	var h=date.getHours();
    	var m=date.getMinutes();
    	
    	var hourSt = '';
        if(h < 10) {
            hourSt+='0';
        }
        hourSt+=h+':';
        
        if(m < 10) {
            hourSt+='0';
        }
        hourSt+=m;
        
        return hourSt;
    },
    
    getEvent : function(component, event) {
        var data = component.get('v.data');
        var eventId = data.eventId;
        var action = component.get("c.getEvent");
        action.setParams({'eventId':eventId});
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                response.getReturnValue();   
            }
        });
        $A.enqueueAction(action);
    },
    getUsers : function(component, event) {
        debugger;
        var data = component.get('v.data');
        var eventId = data.eventId;
        
        var action = component.get("c.getUsers");
        
        action.setParams({'eventId':eventId});
        
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") { 
                document.getElementById("backgroundmodalwait").style.display='none';
                component.set('v.allUser',response.getReturnValue());
                var datos = response.getReturnValue();
                var opts = this.dynamicOptions(datos);
                console.log('Users'+response.getReturnValue());
                component.find("allUserId").set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
    getSelectedUser : function(component, event) {
        var data = component.get('v.data');
        var eventId = data.eventId;
        
        var action = component.get("c.getSelectedUser");
        
        action.setParams({'eventId':eventId});
        
        action.setCallback(this, function(response) {
            console.log('Selected User'+response.getState());
            if (response.getState() === "SUCCESS") {
                component.set('v.selectedUser',response.getReturnValue());
                var datos = response.getReturnValue();
                var opts = this.dynamicOptions(datos);
                
                component.find("selectedUserId").set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    }, 
    getHours : function(component, event) {
        var action = component.get("c.getHours");
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                component.set('v.hoursList',response.getReturnValue());
                var data = component.get('v.data');
                component.find('hourInitId').set("v.value",data.initHour );
                component.find('hourFinId').set("v.value",data.finishHour );
                
                //this.showModal(component, event);
            } 
        });
        $A.enqueueAction(action);
    },
    dynamicOptions : function(data) {
        var opts = [];
        for(var i = 0; data.length > i; i++){
            var temp = data[i].split(',');
            var add = { label: temp[1], value: temp[0] };
            opts.push(add);
        }
        return opts;
    },
    resetAll : function(component) {
        component.set('v.data', null);
        component.set('v.allUser', null);
        component.set('v.selectedUser', null);
        component.set('v.event', null);
        component.set('v.data', null);
        component.find('selectedUserId').set("v.options", []);
        component.find('allUserId').set("v.options", []);
        var eventObject={};
	    eventObject['Subject__c']="";
	    eventObject['Description__c']="";	
	    eventObject['sobjectType']= 'Event__c';
        component.set('v.eventObject', eventObject);
        component.set("v.errorMessage", "");
    },
    addRemove : function(component, IdRemove, IdAdd) {
        debugger;
        var allUsers = component.get('v.'+IdRemove);
        var selectedUsers = component.get('v.'+IdAdd);
        
        var values = component.find(IdRemove+"Id").get("v.value");
        
        if(values != null && values.length > 0) {
            var idsSelected = values.split(';');
            for(var j = 0; idsSelected.length > j; j++) {
                for(var i = 0; allUsers.length > i; i++) {
                    var temp = allUsers[i].split(',');
                    if(temp[0] ==  idsSelected[j]) {
                        selectedUsers.push(allUsers[i]);
                    }   
                }
            }
            
            var newAllUsers = [];
            for(var i = 0; allUsers.length > i; i++) {
                var add = true;
                var index
                for(var j = 0; selectedUsers.length > j; j++) {
                    if(allUsers[i] == selectedUsers[j]) {
                        add = false;
                    }
                }
                if(add) {
                    newAllUsers.push(allUsers[i]);
                }
            }

            component.find(IdRemove+"Id").set("v.value",'');
            
            // set var data
            component.set('v.'+IdRemove, newAllUsers);
            component.set('v.'+IdAdd,selectedUsers);
            
            component.find(IdRemove+"Id").set("v.options", this.dynamicOptions(newAllUsers));
            component.find(IdAdd+"Id").set("v.options", this.dynamicOptions(selectedUsers));
        }
    },
    
    showModal: function(component, event) {
        document.getElementsByTagName("body")[0].className ='modal-open';
        document.getElementById('backgroundmodal').style.display='';
        /*var toggleDialog = component.find("modalDialog");
        var toggleBackdrop = component.find("backdrop");
        
        $A.util.removeClass(toggleDialog, "slds-fade-in-close");
        $A.util.removeClass(toggleDialog, "slds-hide");        
        $A.util.addClass(toggleDialog, "slds-fade-in-open");
        $A.util.addClass(toggleDialog, "slds-show");        
        $A.util.removeClass(toggleBackdrop, "slds-backdrop--close");
        $A.util.removeClass(toggleBackdrop, "slds-hide");        
        $A.util.addClass(toggleBackdrop, "slds-backdrop--open");
        $A.util.addClass(toggleBackdrop, "slds-show");*/
    },
    
    hideModal: function(component, event) {
        document.getElementsByTagName("body")[0].className ='';
        document.getElementById('backgroundmodal').style.display='none';
        document.getElementById('add-event').style.display='none';
        document.getElementById('backgroundmodalwait').style.display='none';
        document.getElementById("add-event").className ='modal fade tlf-new__modal';
        /*var toggleDialog = component.find("modalDialog");
        var toggleBackdrop = component.find("backdrop");
             
        $A.util.removeClass(toggleDialog, "slds-fade-in-open");
        $A.util.removeClass(toggleDialog, "slds-show");      
        $A.util.addClass(toggleDialog, "slds-fade-in-close");
        $A.util.addClass(toggleDialog, "slds-hide");            
        $A.util.removeClass(toggleBackdrop, "slds-backdrop--open");
        $A.util.removeClass(toggleBackdrop, "slds-show");
        $A.util.addClass(toggleBackdrop, "slds-backdrop--close");
        $A.util.addClass(toggleBackdrop, "slds-hide"); */
    }
})