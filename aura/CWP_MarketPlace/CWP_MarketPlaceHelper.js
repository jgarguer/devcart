({
    initComponent : function(cmp, evt, helper) {
        var action = cmp.get("c.iniciaComponente");
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var res = response.getReturnValue();                
                try{                
                    cmp.set('v.marketPlaceFamilies', JSON.parse(res["marketPlaceFamilies"]));
                    cmp.set('v.products', JSON.parse(res["products"]));
                    cmp.set('v.url', res["url"]);                    
                } catch(e){
                	alert(e);
                }
            } else{            	
            	/*LANZA ERROR
                var errorVar = $A.get("e.c:BI_G4C_Generic_error");
                errorVar.setParams({"header": "Error"});
                errorVar.setParams({"body": "Error"});
                errorVar.fire();
                */                
            }
        });
        $A.enqueueAction(action);
    },
    
     showModal : function(component, event){
		var toggleProducto = component.find("producto");
        $A.util.removeClass(toggleProducto, "toggle");
		document.getElementById('backgroundmodal').style.display='block';
    }
})