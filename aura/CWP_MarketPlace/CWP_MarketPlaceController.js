({
    doInit : function(component, event, helper) {
     
            helper.initComponent(component, event, helper);
           
    }, 
    navigate : function(component, event, helper) {
        //Find the text value of the component with aura:id set to "address"
        var id=event.currentTarget.id;
        var products = component.get("v.products");
        var product = products[event.currentTarget.dataset.index];
        
        // Fire the event to open CWP_Catalogo_Producto
        var openCatalogoProducto =  $A.get('e.c:CWP_EventOpenCatalog');
        openCatalogoProducto.setParams({
        	'product' : product
        });
        
        openCatalogoProducto.fire();
        
        helper.showModal(component, event);
        
        //alert('TODO: '+id+' hay que hacer el componente del PCA_Catalogo EX: https://uat-telefonicab2b.cs81.force.com/empresasplatino/PCA_Catalogo_Product?id=a1iw0000002dKcVAAU');
        
        
    },
    
    hideModal : function(component, event, helper){
		var toggleProducto = component.find("producto");
        $A.util.addClass(toggleProducto, "toggle");
		document.getElementById('backgroundmodal').style.display='none';
        document.getElementById('alertDiv').style.display='none';
        document.getElementById('backgroundmodal2').style.display="none";
    }
   
    
    
})