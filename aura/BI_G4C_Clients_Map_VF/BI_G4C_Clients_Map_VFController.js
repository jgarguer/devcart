({
    doInit: function(component, event, helper){
        var latit =40.515492;
        var longit=-3.666391;             
       /* if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                latit = position.coords.latitude;
                longit = position.coords.longitude;
                component.set("v.lati", latit);
                component.set("v.longi", longit);
            });
        } */
    },
        
    recargaIframe : function(component, event, helper) {
        var latMap=component.get("v.lati");
        var lonMap=component.get("v.longi");
        var accounts = event.getParam('accounts');
        var frame = document.getElementById('iframePad');
        var paramMap = {"listAccounts":accounts, "latMap":latMap,"lonMap":lonMap};
        var delay=500; 
        console.debug(accounts);
        setTimeout(function() {
          frame.contentWindow.postMessage(paramMap, '*');
        }, delay);
        
    },
    pickAccount : function(component, event, helper) {
        
        var accountSelected = event.getParam('accountSelected');
        var accounts = event.getParam('accounts');
        var latMap = accountSelected.BI_G4C_ShippingLatitude__c;
        var lonMap = accountSelected.BI_G4C_ShippingLongitude__c;
                
        var frame = document.getElementById('iframePad');
        var paramMap = {"listAccounts":accounts,"accountSelected":accountSelected};
        frame.contentWindow.postMessage(paramMap, '*');
    }

})