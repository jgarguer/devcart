({
	loadPortals : function(component) {
		var action = component.get("c.getPortals");     
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
                    if(returnValue == "")
                    component.set("v.portalsAv", false);
        			component.set("v.portals", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	}
})