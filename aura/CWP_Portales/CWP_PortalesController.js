({
	doInit : function(component, event, helper) {
        //pca_portales
        /*var tabSelected= event.getParam('tabSelected');
        if(tabSelected=== 'pca_portales'){
			helper.loadPortals(component);	
            var comp = component.find('CWP_Portales_div');
            $A.util.removeClass(component, 'slds-hide');
        }else {
            var comp = component.find('CWP_Portales_div');
            $A.util.addClass(component, 'slds-hide');
        }*/
       	var action = component.get("c.getPortals");
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                component.set("v.portals", returnValue);
            }
        });
        $A.enqueueAction(action); 
	}
})