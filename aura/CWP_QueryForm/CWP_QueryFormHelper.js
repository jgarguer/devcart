({
	getUrgencyPickList: function(component, event) {
        var errorcmp = component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getUrgencyPickList');
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            });
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.urgencyList', result.getReturnValue());
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },
    getImpactPickList: function(component, event) {
        var errorcmp = component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getImpactPickList');
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
            component.set('v.impactList', result.getReturnValue());
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },
})