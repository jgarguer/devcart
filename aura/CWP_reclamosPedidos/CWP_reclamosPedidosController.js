({
	 doInit : function(component, event, helper) {   
        helper.getInicialTickets(component, event, helper);
    },
    closeModal : function(component, event, helper) {
        document.getElementById('backgroundmodal').style.display='none';
    },
    showBackground : function(component, event, helper) {
        document.getElementById('backgroundmodal').style.display='block';
    },
    nextPageTickets : function(component, event, helper) {
        //Calculamos el nuevo tableControl
        //
        var tableController= component.get('v.ticketsTableController');
        tableController.iniIndex+=tableController.viewSize;
        
        tableController.finIndex+=tableController.viewSize;
        if (!tableController.finIndex>tableController.numRecords)
            tableController.finIndex=tableController.numRecords;
        //Controlamos la paginación para la primera y la última página 
        if (tableController.iniIndex<=tableController.numRecords) 
            helper.getTickets(component, event, helper,tableController);
        
    },
    prevPageTickets : function(component, event, helper) {
        //Calculamos el nuevo tableControl
        var tableController= component.get('v.ticketsTableController');
        
        tableController.iniIndex-=tableController.viewSize;
        tableController.finIndex-=tableController.viewSize;
        if (tableController.iniIndex>0 ){
            helper.getTickets(component, event, helper,tableController);
        }
    },
    changeSizeTickets : function(component, event, helper) {
        //Calculamos el nuevo tableControl
        var tableController= component.get('v.ticketsTableController');
        var e = document.getElementById("paginationTickets");
        var newSize = e.options[e.selectedIndex].value;
        tableController.viewSize=newSize;
        tableController.iniIndex=1;
        tableController.finIndex=newSize;
        helper.getTickets(component, event, helper,tableController);
        
    },
    advancedFilterTickets : function(component, event, helper) {
        helper.getFieldValues(component, event, helper);
    }, 
    
     search: function(component, event, helper){
    	var field=component.get('v.searchField');
    	var value= document.getElementById('buscar').value;
    	helper.getFilteredRecords(component, event, field,value);
    },
    
    changeField: function(component, event, helper){
    	component.set('v.searchField',event.currentTarget.value);
    },
    
    showModal:function(component, event, helper){
        document.getElementById('backgroundmodal').style.display='block';
    	event.preventDefault();
    	var elem= component.get("v.ticketsList")[event.currentTarget.id];
    	helper.getRecorDetail(component, event,elem.id);
    	var toggleProducto = component.find("producto");
        $A.util.removeClass(toggleProducto, "slds-hide");
		var toggleBackdrop = component.find("backdrop");
        $A.util.removeClass(toggleBackdrop, "slds-hide");
        component.set("v.caseSelected",elem);
    },
    
    hideModal : function(component, event, helper){
        document.getElementById('backgroundmodal').style.display='none';
		var toggleProducto = component.find("producto");
        $A.util.addClass(toggleProducto, "slds-hide");
		var toggleBackdrop = component.find("backdrop");
        $A.util.addClass(toggleBackdrop, "slds-hide");
    },
    
    sortTickets:function(component, event, helper){
        debugger;
    	event.preventDefault();
    	var field=event.currentTarget.id;
    	helper.getSortRecords(component, event, field);
    },
    showSpinner : function (component, event, helper) {
        /*var spinner = component.find('spinner');
        $A.util.removeClass(spinner,'slds-hide');*/
        document.getElementById('backgroundmodalwait').style.display='';
     },
    hideSpinner : function (component, event, helper) {
       /*var spinner = component.find('spinner');
        $A.util.addClass(spinner,'slds-hide');*/
        document.getElementById('backgroundmodalwait').style.display='none';
    },
    updateControllerSearchTextEnter : function(component, event, helper){
        if(event.keyCode == 13){
            event.preventDefault();
            //Calculamos el nuevo tableControl
            var field=component.get('v.searchField');
            var value= document.getElementById('buscar').value;
            helper.getFilteredRecords(component, event, field,value);
        }
    }
})