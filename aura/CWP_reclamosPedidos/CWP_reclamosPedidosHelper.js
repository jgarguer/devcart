({
	getInicialTickets : function(component, evt,helper) {
        var action = component.get("c.getRecords");  
        action.setParams({ tableControllerS :'init' });
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                var res = response.getReturnValue();
                component.set('v.ticketsList', JSON.parse(res["lisTickets"]));
                component.set('v.ticketsTableController', JSON.parse(res["offsetController"]));
                component.set('v.headersList', JSON.parse(res["recordsdHeaders"]));
            }
        });
        $A.enqueueAction(action);
    },
    getTickets : function(component, evt,helper, tableControl) {   
        var action = component.get("c.getRecords");    
        action.setParams({ tableControllerS :JSON.stringify(tableControl) });
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                
                var res = response.getReturnValue();
                component.set('v.ticketsList', JSON.parse(res["lisTickets"]));
                component.set('v.ticketsTableController', JSON.parse(res["offsetController"]));
                component.set('v.headersList', JSON.parse(res["recordsdHeaders"]));
                console.log(res["offsetController"]);
            }
        });
        $A.enqueueAction(action);
    }, 
    getFieldValues : function(component, evt,helper) {   
        var action = component.get("c.getFieldsValues");    
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                debugger;
                
                var res = response.getReturnValue();
                component.set('v.caseTypeFields', JSON.parse(res["type"]));
                component.set('v.serviceFamilyFields', JSON.parse(res["TGS_Product_Tier_1__c"]));
                component.set('v.serviceFields', JSON.parse(res["TGS_Product_Tier_2__c"]));
                component.set('v.serviceUnitFields', JSON.parse(res["TGS_Product_Tier_3__c"]));
                component.set('v.statusFields', JSON.parse(res["status"]));
                component.set('v.cityFields', JSON.parse(res["City"]));
                component.set('v.countryFields', JSON.parse(res["Country"]));
				component.set('v.toShowFields', JSON.parse(res["toShow"]));
            }
        });
        $A.enqueueAction(action);
    },
    
    getFilteredRecords : function(component, event,field,value) {
        var action = component.get("c.getFilteredRecords");  
        action.setParams({ "field" :field, "value":value });
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                var res = response.getReturnValue();
                component.set('v.ticketsList', JSON.parse(res["lisTickets"]));
                component.set('v.ticketsTableController', JSON.parse(res["offsetController"]));
                component.set('v.headersList', JSON.parse(res["recordsdHeaders"]));
            }
        });
        $A.enqueueAction(action);
    },
     getSortRecords : function(component, event,field) {
        var action = component.get("c.getSortRecords");
        action.setParams({ "field" :field, "direction":component.get('v.direction')});
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                var res = response.getReturnValue();
                component.set('v.ticketsList', JSON.parse(res["lisTickets"]));
                component.set('v.ticketsTableController', JSON.parse(res["offsetController"]));
             //   component.set('v.headersList', JSON.parse(res["recordsdHeaders"]));
                
                var elementossHeader = ['cwp--1','cwp--2','cwp--3','cwp--4']
                for(var i=0; i<elementossHeader.length; i++){
                    document.getElementById(elementossHeader[i]).classList.remove('headerSortUp');
                    document.getElementById(elementossHeader[i]).classList.remove('headerSortDown');
                } 
                debugger;
                if(component.get('v.direction')==='ASC'){
                    document.getElementById(field).parentElement.classList.add('headerSortUp');
                }else{
                    
                    document.getElementById(field).parentElement.classList.add('headerSortDown');
                }
                
                if(component.get('v.direction')=='ASC'){
                	component.set('v.direction','DESC');
                }else{
                	component.set('v.direction','ASC');
                }
            }
        });
        $A.enqueueAction(action);
    },
        
    getRecorDetail: function(component, event, recordId){
    
    var action = component.get("c.getRecorDetail");
    action.setParams({"recordId" : recordId});
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                var res = response.getReturnValue();
                debugger
                component.set('v.detailList', JSON.parse(res["detailList"]));                
            }
        });
        $A.enqueueAction(action);
    
    }
})