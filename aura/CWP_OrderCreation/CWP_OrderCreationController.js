({
    doInit: function(component, event, helper) {
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        helper.getFamilyPickList(component, event, helper);
        //helper.getServicePickList(component, event);
        //helper.getServiceUnitPickList(component, event);
        helper.getLegalEntityPickList(component, event);
        helper.getBusinessUnitPicklist(component, event);
        helper.getCostCenterPicklist(component, event);
        component.find('servicecombo').set("v.disabled", true);
		component.find('serviceunitcombo').set("v.disabled", true);
    },

    subject: function(component, event, helper) {
        var subject = component.find("subjecttext").get("v.value");
        component.set("v.subject", subject);
    },

    textArea: function(component, event, helper) {
        var description = component.find("descriptiontext").get("v.value");
        component.set("v.description", description);
    },

    servicePickList: function(component, event, helper) {
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
          var key= event.currentTarget.value;
       if(key=='none'){
        	 //helper.getServicePickList(component, event);  
        	 //component.find('servicecombo').set("v.disabled", true); 
        	 //component.find('serviceunitcombo').set("v.disabled", true);
        	 
        	 component.set('v.case.TGS_Product_Tier_1__c', 'none');
        	 component.set('v.case.TGS_Product_Tier_2__c', 'none');
        	 component.set('v.case.TGS_Product_Tier_3__c', 'none');
        	 document.getElementById('servicecombo').disabled = true;
        	 document.getElementById('serviceunitcombo').disabled = true;
        	 document.getElementById('familycombo').value = 'none';
        	 document.getElementById('servicecombo').value = 'none';
        	 document.getElementById('serviceunitcombo').value = 'none';
         }else{
        	 var valueSel = document.getElementById("familycombo").value;
        	 component.set('v.case.TGS_Product_Tier_1__c', valueSel);
	         var serviceMap = component.get('v.serviceMapCreation');
	         var serviceList = serviceMap[key];
	         component.set('v.serviceList', serviceMap[key]);
	         //component.find('servicecombo').set("v.disabled", false);
	         document.getElementById('servicecombo').disabled = false;
	         if(serviceList.length == 1){
	        	 component.set('v.case.TGS_Product_Tier_2__c', serviceList[0].value);
	        	 var serviceUnitMap = component.get('v.serviceUnitMapCreation');
            	 var serviceUnitList = serviceUnitMap[serviceList[0].value];
            	 component.set('v.serviceUnitList', serviceUnitList);
            	 document.getElementById('serviceunitcombo').disabled = false;
            	 if(serviceUnitList.length == 1){
            		 component.set('v.case.TGS_Product_Tier_3__c', serviceUnitList[0].value);
            	 }
	         }else{
	        	 document.getElementById('serviceunitcombo').value = 'none';
	        	 document.getElementById('serviceunitcombo').disabled = true;
	         }
         }
    },

    serviceUnitPickList: function(component, event, helper) {
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        var key= event.currentTarget.value;
        var serviceUnitMap = component.get('v.serviceUnitMapCreation');
        if(key=='none'){
        	  //helper.getServiceUnitPickList(component, event);
        	 //component.find('serviceunitcombo').set("v.disabled", true);
        	 component.set('v.case.TGS_Product_Tier_2__c', 'none');
        	 component.set('v.case.TGS_Product_Tier_3__c', 'none');
        	 document.getElementById('serviceunitcombo').disabled = true;
        	 document.getElementById('servicecombo').value = 'none';
        	 document.getElementById('serviceunitcombo').value = 'none';
         }else{
	       var valueSel = document.getElementById("servicecombo").value;
           component.set('v.case.TGS_Product_Tier_2__c', valueSel);
	       component.set('v.serviceUnitList', serviceUnitMap[key]);       
	       //component.find('serviceunitcombo').set("v.disabled", false);
	       document.getElementById('serviceunitcombo').disabled = false;
	       var serviceUnitList = serviceUnitMap[key];
	       if(serviceUnitList.length == 1){
    		   component.set('v.case.TGS_Product_Tier_3__c', serviceUnitList[0].value);
    	   }
         }
    },

    legalEntityPickList: function(component, event, helper) {
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        
        helper.getLegalEntityPickList(component, event, helper);
    },

    businessUnitPickList: function(component, event, helper) {
    	debugger;
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        var valueSel = document.getElementById("entity").value;
        component.set('v.case.CWP_Legal_Entity__c', valueSel);
        if(valueSel == 'none'){
        	document.getElementById('business').value = 'none';
        	document.getElementById('cost').value = 'none';
        	document.getElementById('business').disabled = true;
        	document.getElementById('cost').disabled = true;
        	
        }else{
        	document.getElementById('business').disabled = false;
        	document.getElementById('cost').disabled = true;
        }
        helper.getBusinessUnitPicklist(component, event);
    },

    costCenterPicklist: function(component, event, helper) {
    debugger;
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        var valueSel = document.getElementById("business").value;
        component.set('v.case.CWP_Business_Unit__c', valueSel);
        if(valueSel == 'none'){
        	document.getElementById('cost').disabled = true;
        	document.getElementById('cost').value = 'none';
        }else{
        	document.getElementById('cost').disabled = false;
        }
        helper.getCostCenterPicklist(component, event);
    },

    save: function(component, event, helper) {
    	var valueSel = document.getElementById("serviceunitcombo").value;
        component.set('v.case.TGS_Product_Tier_3__c', valueSel);
    	var valueSel = document.getElementById("cost").value;
        component.set('v.case.CWP_Cost_Center__c', valueSel);
        helper.save(component, event);
    }
})