({
    getFamilyPickList: function(component, event, helper) {
    debugger;
    var self = this;
    var errorcmp=component.find("errorMsg");
        var promise = new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get('c.getFamilyPickList');
            var subtype = component.get('v.caseSubType');
              action.setParams({
                'subType': subtype
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        }));
        promise.then(function(result) {
        component.set('v.error', '');
        $A.util.addClass(errorcmp, "toggle");
        	debugger;
            //component.set('v.familyList', result.getReturnValue());
             	var res = result.getReturnValue();
            	component.set('v.familyList', JSON.parse(res["caseFamilySO"]));
                component.set('v.serviceMapCreation', JSON.parse(res["caseServiceSO"]));
                component.set('v.serviceUnitMapCreation', JSON.parse(res["caseServiceUnitSO"]));
                var familyList = component.get('v.familyList');
	            if(familyList.length == 1){
	            	component.set('v.case.TGS_Product_Tier_1__c', familyList[0].value);
	            	var serviceMap = JSON.parse(res["caseServiceSO"]);
	            	var serviceList = serviceMap[familyList[0].value];
	            	component.set('v.serviceList', serviceList);
	            	//helper.getServicePickList(component, event);
	            	if(serviceList.length == 1){
	            		var serviceUnitMap = JSON.parse(res["caseServiceUnitSO"]);
		            	var serviceUnitList = serviceUnitMap[serviceList[0].value];
		            	component.set('v.serviceUnitList', serviceUnitList);
	            	}else{
	            		document.getElementById('serviceunitcombo').disabled = true;
	            	}
	            	
	            }else{
	            	document.getElementById('servicecombo').disabled = true;
	            	document.getElementById('serviceunitcombo').disabled = true;
	            }
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },

    getServicePickList: function(component, event) {
    var self = this;
    var errorcmp=component.find("errorMsg");
        var promise = new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get('c.getServicePickList');
            action.setParams({
                'familyName': component.get('v.case.TGS_Product_Tier_1__c')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        }));
        promise.then(function(result) {
        component.set('v.error', '');
        $A.util.addClass(errorcmp, "toggle");
            component.set('v.serviceList', result.getReturnValue());
            var serviceList = component.get('v.serviceList');
            if(serviceList.length == 1){
            	component.set('v.case.TGS_Product_Tier_2__c', serviceList[0].value);
            	self.getServiceUnitPickList(component, event);
            }
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.addClass(errorcmp, "toggle");
            }
        })
    },

    getServiceUnitPickList: function(component, event) {
    var errorcmp=component.find("errorMsg");
        var promise = new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get('c.getServiceUnitPickList');
            action.setParams({
                'serviceName': component.get('v.case.TGS_Product_Tier_2__c')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        }));
        promise.then(function(result) {
        component.set('v.error', '');
        $A.util.addClass(errorcmp, "toggle");
            component.set('v.serviceUnitList', result.getReturnValue());
            var serviceUnitList = component.get('v.serviceUnitList');
            if(serviceUnitList.length == 1){
            	component.set('v.case.TGS_Product_Tier_3__c', serviceUnitList[0].value);
            }
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
               component.set("v.error", errors[0].message);
                $A.util.addClass(errorcmp, "toggle");
            }
        })
    },


    getLegalEntityPickList: function(component, event, helper) {
    var self = this;
    var errorcmp=component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getLegalEntityPickList');
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
        component.set('v.error', '');
        $A.util.addClass(errorcmp, "toggle");
            component.set('v.entityList', result.getReturnValue());
            var entityList = component.get('v.entityList');
            if(entityList.length == 1){
            	component.set('v.case.CWP_Legal_Entity__c', entityList[0].value);
            	self.getBusinessUnitPicklist(component, event);
            	document.getElementById('cost').disabled = true;
            }else{
            	document.getElementById('business').disabled = true;
            	document.getElementById('cost').disabled = true;
            }
            
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },

    getBusinessUnitPicklist: function(component, event) {
    var self = this;
    var errorcmp=component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getBusinessUnitPicklist');
            action.setParams({
                'legalEntityValue': component.get('v.case.CWP_Legal_Entity__c')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
        component.set('v.error', '');
        $A.util.addClass(errorcmp, "toggle");
            component.set('v.businessUnitList', result.getReturnValue());
            var businessUnitList = component.get('v.businessUnitList');
            if(businessUnitList.length == 1){
            	component.set('v.case.CWP_Business_Unit__c', businessUnitList[0].value);
            	document.getElementById('business').value = businessUnitList[0].value;
            	self.getCostCenterPicklist(component, event);
            	document.getElementById('cost').disabled = false;
            }else{
            	document.getElementById('cost').disabled = true;
            }
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.addClass(errorcmp, "toggle");
            }
        })
    },

    getCostCenterPicklist: function(component, event) {
    var errorcmp=component.find("errorMsg");
        var promise = new Promise($A.getCallback(function(resolve, reject) {
            var action = component.get('c.getCostCenterPicklist');
            action.setParams({
                'businessUnitValue': component.get('v.case.CWP_Business_Unit__c')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        }));
        promise.then(function(result) {
        component.set('v.error', '');
        $A.util.addClass(errorcmp, "toggle");
            component.set('v.costCenterList', result.getReturnValue());
            var costCenterList = component.get('v.costCenterList');
            if(costCenterList.length == 1){
            	component.set('v.case.CWP_Cost_Center__c', costCenterList[0].value);
            }
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                $A.util.addClass(errorcmp, "toggle");
            }
        })
    },

    save: function(component, event) {
    var errorcmp=component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.doSave');
            action.setParams({
                'paramCase': component.get('v.case')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
            component.set('v.error', '');
            $A.util.addClass(errorcmp, "toggle");
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                component.set("v.error", errors[0].message);
                 $A.util.removeClass(errorcmp, "toggle");
            }
        });
    }
})