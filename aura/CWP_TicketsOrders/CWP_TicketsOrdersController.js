({
    doInit : function(component, event, helper) {
        // reset campo
        debugger;
        
        var tabSelected= event.getParam('tabSelected');
        if(tabSelected=== 'ticketsOrders'){
            // Ejecutamos código que lanzaríamos en el doInit
            var comp = component.find('CWP_TicketsOrdersDiv');
            $A.util.removeClass(comp, 'slds-hide');
            
        } else {
            var comp = component.find('CWP_TicketsOrdersDiv');
            $A.util.addClass(comp, 'slds-hide');
        }
        var index0 = component.get("v.inicio");
        var indexN = component.get("v.fin");
        var avance = component.get("v.avance");
        if(tabSelected==='ticketsOrders'){
            var headerList = component.get('c.getTableHeader');
            headerList.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var headerMap = response.getReturnValue();
                    helper.setHeader(component, event, helper, headerMap);
                }
            });
            $A.enqueueAction(headerList);
            var caseList = component.get('c.getCaseList');
            caseList.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var retList = response.getReturnValue();
                    var caseListToOrder = helper.makeListSortable(component, event, helper, retList); //construcción de la tabla como lista de listas
                    helper.getPaginatedList(component, event, helper, caseListToOrder, index0, indexN, avance); //paginacion de la lista de listas
                    debugger;
                }else{
                    alert("fallo!");
                }
            });
            $A.enqueueAction(caseList);
        } 
    },
    
    addComment : function(component, event, helper) {
        
        var commentBody = document.getElementById('descripción2').value;
        var parentId = component.get("v.caseToShow")[9];
        
        var action = component.get("c.addNewComment");         
        action.setParams({
            parentId: String(parentId),
            commentBody: String(commentBody)
        });
        action.setCallback(this, function(response) {
            debugger;
            if(response.getState() === "SUCCESS") {
                document.getElementById('descripción2').value = '';
                alert(response.getReturnValue());
            }    
        });
        $A.enqueueAction(action);   
    },
    
    
    applyFilter : function(component, event, helper){
        var newFilter = document.getElementById('filterSO').value;
        var filterValueInput = document.getElementById('buscar').value;
        var filterValue='';
        if(filterValueInput!=undefined){
            filterValue=String(filterValueInput).toLowerCase();
        }
        var columnMap = component.get("v.columnMap");
        var columnIndex= columnMap[newFilter];
        var listToFilter = component.get('v.caseList');
        var retList = [];
        if(filterValue!=''){
            for(var i =0; i<listToFilter.length; i++){
                var valueToCompare = String(listToFilter[i][columnIndex]).toLowerCase();
                if(valueToCompare.indexOf(filterValue)>-1){
                    retList.push(listToFilter[i]);
                }
            }
        }else{
            retList=listToFilter;
        }    
        var avance = component.get("v.avance");
        helper.getPaginatedList(component, event, helper, retList, 0, avance, avance); //paginacion de la lista de listas
    },
    
    
    
    getAttachmentList : function(component, event, helper){
        var caseIdToGetAttachment = event.target.id;
        var caseId = caseIdToGetAttachment.split('=')[1]
        var caseAndAttachmentMap = component.get("v.caseAndAttachmentMap");
        if(caseAndAttachmentMap.hasOwnProperty(caseId)){
            var attachList = caseAndAttachmentMap[caseId];
            component.set("v.attachmentList", attachList);
        }
    },
    
    getCaseDetail : function(component, event, helper){
        //debugger;
        var caseIdToGetDetail = event.target.id;
        var caseId = caseIdToGetDetail.split('=')[1];
        var caseDetailMap = component.get("v.caseDetailMap");
        if(caseDetailMap.hasOwnProperty(caseId)) {
            var caseToShow = caseDetailMap[caseId];
            //component.set("v.caseToShow", caseToShow);
            debugger;
            var action = component.get("c.getCaseComment");
            action.setParams({
                "parentId" : caseToShow[9]
            });
            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS") {
                    component.set("v.casesComment", response.getReturnValue());
                    component.set("v.caseToShow", caseToShow);
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    saveTheCase : function(component, event, helper){
        var reason = document.getElementById('reasonField').value;
        var subject = document.getElementById('subjectField').value;
        var description = document.getElementById('descriptionField').value;
        
        var caseCreation = component.get('c.saveNewCase');
        caseCreation.setParams({
            "subject" : String(subject),
            "reason" : String(reason),
            "description" : String(description)
        });
        
        caseCreation.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state ==="SUCCESS"){
                var retCase = response.getReturnValue();
                var newList = [retCase];
                var finalList = newList.concat(component.get("v.originalCaseList"));
                helper.makeListSortable(component, event, helper, finalList);
            }
        });
        $A.enqueueAction(caseCreation);
    },
    
    getNewAvance : function(component, event, helper){
        var newAvance = document.getElementById('pagination').value; //id de la picklist donde se seleccionan los valores de list size
        var retList = component.get("v.caseList"); //lista completa, sin paginar
        var indexN = Math.min(newAvance, retList.length);
        helper.getPaginatedList(component, event, helper, retList, 0, indexN, newAvance); //metodo que ejecuta la paginacion
    },
    
    listPaginateForward : function(component, event, helper){
        var retList = component.get("v.caseList");
        var index0 = parseInt(component.get("v.inicio"));	
        var indexN = parseInt(component.get("v.fin"));	
        var avance = parseInt(component.get("v.avance"));
        if(index0+avance<retList.length){
            index0 = index0 + avance;
            indexN = index0 + avance;
            indexN = Math.min(indexN, retList.length);
            helper.getPaginatedList(component, event, helper, retList, index0, indexN, avance);
        }
        
    },
    
    listPaginateBackward : function(component, event, helper){
        var retList = component.get("v.caseList");
        var index0 = parseInt(component.get("v.inicio"));	
        var indexN = parseInt(component.get("v.fin"));	
        var avance = parseInt(component.get("v.avance"));
        if(indexN-avance>0){
            indexN = index0;
            index0 = indexN - avance;
            index0 = Math.max(index0, 0);
            helper.getPaginatedList(component, event, helper, retList, index0, indexN, avance);
        }
        
    },
    
    getFieldToOrderBy : function(component, event, helper){
        var fieldToOrder = event.target.id;
        var direction = component.get("v.direction");
        if(direction==="DESC"){
            direction="ASC";
        }else{
            direction="DESC"
        }
        component.set("v.direction", direction);
        helper.getOrderedList(component, event, helper, fieldToOrder, direction);
    },
    
    attachFile1 : function(component, event, helper) {
        //alert('llamada a adjuntar archivo');
        var fileInput = component.find("archivo").getElement();
        var file = fileInput.files[0];
        //alert('el archivo	' + file);
        /*if (file.size > this.MAX_FILE_SIZE) {
            alert('File size cannot exceed ' + this.MAX_FILE_SIZE + ' bytes.\n' +
    	          'Selected file size: ' + file.size);
    	    return;
        }*/
        
        var fr = new FileReader();
        
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            
            fileContents = fileContents.substring(dataStart);
            
            /******************/
            var parentId = component.get("v.caseToShow")[9];
            alert(parentId);
            var action = component.get("c.saveTheFile"); 
            action.setParams({
                parentId: String(parentId),
                fileName: String(file.name),
                base64Data: String(encodeURIComponent(fileContents)), 
                contentType: String(file.type)
            });
            //alert(action);
            action.setCallback(this, function(a){
                //alert('dentro de la llamada 1');
                var attachId = a.getReturnValue();
                //alert('dentro de la llamada 2');
                console.log(attachId);
            });
            //alert('llamada');
            $A.enqueueAction(action);   
            //alert('fin de la llamada');
            /*$A.run(function() {
                $A.enqueueAction(action); 
            });*/
            /******************/
            
            //helper.upload1(component, event, helper, file, fileContents);
        };
        
        fr.readAsDataURL(file);
        
    },
    
    
    
})