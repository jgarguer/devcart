({
    doInit : function(component, event, helper) {
        helper.getNumTotalResult(component,event);
    },
    
     getReportId : function(component, event, helper){   
    	 helper.showModal(component, event);
    	  var index = parseInt(event.currentTarget.dataset.index,10);
    	  var reports = component.get("v.reportRecordsListToShow"); 
    	  var id=reports[index];
    	  
    	  component.set("v.reportId", id[0]);		
    },
    
    hideModal: function(component, event, helper) {
        document.getElementById('backgroundmodal').style='display:none'; 
        var toggleDialog = component.find("modaldialog");
        var toggleBackdrop = component.find("backdrop");
        $A.util.addClass(toggleBackdrop, "toggle");
        $A.util.addClass(toggleDialog, "toggle");
    },
    
    getNewAvance : function(component, event, helper){
    	var av = parseInt(event.currentTarget.value,10);
    	component.set("v.avance",av);
    	var skip = component.get("v.skip");
    	
        helper.getReportList(component,event,skip);
    },   
    
    
    listPaginateForward : function(component, event, helper){
        var skip = component.get("v.skip") + component.get("v.avance");
        
        if(skip < component.get("v.listSize")){
    	 helper.getReportList(component,event,skip);
    	}
    },

    listPaginateBackward : function(component, event, helper){
    	var skip = component.get("v.skip") - component.get("v.avance"); 
    	if(skip < 0){
    	skip=0;
    	}
    	
        helper.getReportList(component,event,skip);
    },
    
    sortTable: function(component,event,helper){
    	var order=event.currentTarget.id;
    	component.set("v.order", order);    	
    	var skip = component.get("v.skip");
    	helper.getReportList(component,event,skip);
    }
   
})