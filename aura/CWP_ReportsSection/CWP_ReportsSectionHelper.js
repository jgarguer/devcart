({
    getNumTotalResult: function(component, event) {
        var action = component.get("c.getNumTotalResult");
        //Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if (returnValue != null) {
                	var json= JSON.parse(returnValue);
                    component.set("v.listSize", json.mapSize); //tamaño de la lista global
                    component.set("v.reportIds", json.ghi.reportIds);
                    var skip = component.get("v.skip");                    
    	                
                    this.getReportList(component, event, skip);
                }
            }
        });
        $A.enqueueAction(action);
    },

    getReportList: function(component, event, skip) {
        var action = component.get("c.getReportList");
        var listSize = component.get("v.listSize");
        var offset = Math.min(skip,listSize);
        var reports = component.get("v.reportIds");
        var avance = component.get("v.avance");
        var order= component.get("v.order");
        var desc= !component.get("v.descendente");
        
        action.setParams({
            "offset": offset,
            "numTotalResult": avance,
            "reports": reports,
            "orderBy": order,
            "descendente": desc
        });
        
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if (returnValue != null) {
                    returnValue = JSON.parse(returnValue);
                    var rowsArray = [];
                    returnValue.forEach(function(arrayItem) {

                        var cellsArray = [];
                        cellsArray.push(arrayItem.Id);
                        cellsArray.push(arrayItem.Name);
                        cellsArray.push(arrayItem.Description);
                        cellsArray.push(arrayItem.LastModifiedDate);

                        rowsArray.push(cellsArray);

                    });

                    returnValue = rowsArray;
                    component.set("v.reportRecordsListToShow", returnValue); //lista para mostrar (la que se pinta en el iterador) 
                    component.set("v.skip",offset);
                    component.set("v.descendente",desc);
                    this.getPagination(component, event);                         
                }
            }
        });
        $A.enqueueAction(action);
    },
    
     showModal: function(component, event) {
         document.getElementById('backgroundmodal').style=''; 
		var toggleDialog = component.find("modaldialog");
        var toggleBackdrop = component.find("backdrop");
        $A.util.removeClass(toggleBackdrop, "toggle");
        $A.util.removeClass(toggleDialog, "toggle");
    },
    
    getPagination : function(component, event) {
    	var skip = parseInt(component.get("v.skip"),10);
    	var avance = parseInt(component.get("v.avance"),10);
    	var listSize = component.get("v.listSize");
        var indexStart = skip + 1;
        var indexEnd = Math.min(skip + avance,listSize);
        
        
        var infoList = indexStart + " - " + indexEnd + ' Out of ' + listSize;
        component.set("v.infoList", infoList); //string con los datos de qué elementos están viendo       
    },

    
})