({
    doInit: function(component, event, helper) {

        helper.getTeam(component);
        helper.getEscalation(component);
		
    },

    /*FOT 02032017 - method to get the id of the user of whom a meeting is getting requested and open the calendar with its events*/
    getUserId: function(component, event, helper) {
        var userId = event.target.id;
        var evt = $A.get("e.c:CWP_EventChangeComponent");
        evt.setParams({
            "toOpen": 'calendar',
            "userId": userId
        });
        evt.fire();
    },

    getCalendar: function(component, event, helper) {
        var idx = event.target.id;
//        var urlEvent = $A.get("e.force:navigateToURL");
//        urlEvent.setParams({
//            "url": '/cwp-calendar#id='+idx,
//            "isredirect": false
//        });
//        urlEvent.fire();
        
        //window.location.href = 'https://crqdev-telefonicab2b.cs80.force.com/CWP/s/cwp-calendar#id='+idx;
        debugger;
        var prefix = $A.get("$Label.c.CWP_community_URL");
        window.location.href = prefix + '/s/cwp-calendar#id='+idx;
    },

    readyPrint: function(component, event, helper) {
        var readyMap = component.get("v.mapToReady");

        readyMap[event.getParam("keyToMap")] = true;
        component.set("v.mapToReady", readyMap);
        var isReady = true;

        var key;
        for (key in readyMap) {
            if (readyMap[key] == false) {
                isReady = false;
            }
        }

        if (isReady) {

            setTimeout(function() {
                component.set("v.loaded", true);
                if ($("[data-function='data-selectpicker']").length > 0) {
                    $("[data-function='data-selectpicker']").selectpicker();
                }
                if ($("[data-function='data-carousel__team_uno']").length > 0) {
                    $("[data-function='data-carousel__team_uno']").slick({
                        infinite: true,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        autoplay: false,
                        arrows: true,
                        dots: false,
                        draggable: false,
                        centerMode: false,
                        speed: 500,

                        responsive: [{
                            breakpoint: 1024,
                            settings: {
                                infinite: true,
                                slidesToShow: 2,
                                draggable: true,
                                centerMode: false
                            }
                        }, {
                            breakpoint: 768,
                            settings: {
                                infinite: true,
                                draggable: true,
                                centerMode: false,
                                slidesToShow: 1
                                
                            }
                        }]
                    });
                }
                if ($("[data-function='data-carousel__team']").length > 0) {
                    $("[data-function='data-carousel__team']").slick({
                       infinite: true,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        autoplay: false,
                        arrows: true,
                        dots: false,
                        draggable: false,
                        centerMode: false,
                        speed: 500,

                        responsive: [{
                            breakpoint: 1024,
                            settings: {
                                infinite: true,
                                slidesToShow: 2,
                                draggable: true,
                                centerMode: false
                            }
                        }, {
                            breakpoint: 768,
                            settings: {
                                infinite: true,
                                draggable: true,
                                centerMode: false,
                                slidesToShow: 1
                                
                            }
                        }]
                    });
                }
            }, 1);
            for (key in readyMap) {
                readyMap[key] = false;
            }
            component.set("v.mapToReady", readyMap);
            component.set("v.load", "true");
        }
    },
    openDetailContact: function(component, event, helper) {
        var contactos = component.get("v.listHierarchyWrapper");
        var contact = contactos[event.currentTarget.dataset.index];
        var modal = $A.get("e.c:CWP_EquipoTelefonicaModalEvent");
        modal.setParams({
            "id": contact.UsernameId,
            "clase": 'Equipo'

        });
        modal.fire();

    },
    sendMailCarrousel2: function(component, event, helper) {
        document.getElementById('backgroundmodal').style='';  
        document.getElementById('tlf-email').style.display='block'; 
        document.getElementById('tlf-email').classList.add('in');
        document.getElementsByTagName('body')[0].classList='modal-open';
        var contactos = component.get("v.listEscalationWrapper");
        var contact = contactos[event.currentTarget.dataset.index];

        var modalSend = $A.get("e.c:CWP_SendInfoModalEvent");
        modalSend.setParams({
            "idContact": contact.UsernameId,
            "isButton": false
        });

        modalSend.fire();
    },
    
    sendMail: function(component, event, helper) {
        debugger;
        document.getElementById('backgroundmodal').style='';           
        document.getElementsByTagName('body')[0].classList='modal-open';
        document.getElementById('tlf-email').style.display='block';
        document.getElementById('tlf-email').classList.add('in');
        var contactos = component.get("v.listHierarchyWrapper");
        var contact = contactos[event.currentTarget.dataset.index];

        var modalSend = $A.get("e.c:CWP_SendInfoModalEvent");
        modalSend.setParams({
            "idContact": contact.UsernameId,
            "isButton": false
        });

        modalSend.fire();
    },
    editContact: function(component, event, helper) {
        document.getElementById('backgroundmodal').style='';        
        event.preventDefault();
        var idx = event.target.id;

        var action = component.get("c.getContact");
        action.setParams({
            "user": idx
        });
        //Callback
        action.setCallback(this, function(actionResult) {
            if (component.isValid() && actionResult.getState() == "SUCCESS") {
                var returnValue = actionResult.getReturnValue();
                if (returnValue != null) {
                    if (returnValue != "")
                        idx = returnValue;
                    //component.set("v.TrueOrFalse", true);
                }
            }
        });
        $A.enqueueAction(action);
        //var contactos = component.get("v.listHierarchyWrapper");
        //var contact = contactos[event.currentTarget.dataset.index];               
        //idx='0032500000YQ7P0AAL';

        /*var modalEdit = $A.get("e.c:CWP_EditContactModalEventTeam");
        modalEdit.setParams({"idContact":idx});
        modalEdit.fire();*/
        var modalEdit = $A.get("e.c:CWP_EquipoTelefonicaModalEvent");
        modalEdit.setParams({
            "id": idx
        });
        modalEdit.fire();

    },



})