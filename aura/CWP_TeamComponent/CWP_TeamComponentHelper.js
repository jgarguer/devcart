({
    
    getTeam : function(component) {   
        var action = component.get("c.getTeam2");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
                var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
                    if(returnValue != "")
                    component.set("v.TrueOrFalse", true);
        			component.set("v.listHierarchyWrapper", actionResult.getReturnValue());
					var loadedEvent = $A.get("e.c:CWP_CargaFinalizada");
					loadedEvent.setParams({
						"loaded": true,
						"keyToMap" : 'getTeam'
						});
					loadedEvent.fire();
            	}
            }
        });
        $A.enqueueAction(action);
	},
    
    getEscalation : function(component) {
        var action = component.get("c.getEscalation");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
            		console.log(actionResult.getReturnValue());
        			component.set("v.listEscalationWrapper", actionResult.getReturnValue());
                    if(returnValue != "")
                    component.set("v.TrueOrFalseEsc", true);
                    var loadedEvent = $A.get("e.c:CWP_CargaFinalizada");
					loadedEvent.setParams({
						"loaded": true,
						"keyToMap" : 'getEscalation'
						});
					loadedEvent.fire();

            	}
            }
        });
        $A.enqueueAction(action);
	}

})