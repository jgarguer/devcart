({
	doInit : function(cmp, evt, helper) {
		console.log('doInitAccountTab')
		var action = cmp.get("c.getInfo");
		action.setParams({
			"MasterLabel" : "Account"
		});
		action.setCallback(this, function(response){
			//preloading custom View Labels
			//$Label.c.BI_SF1_TodosLosClientes
			//$Label.c.BI_SF1_MisClientes
			//$Label.c.BI_SF1_VistosRecientemente
			for(var i in response.getReturnValue().metadataConfig.BI_SF1_Custom_tab_listView_configuration__r){
				response.getReturnValue().metadataConfig.BI_SF1_Custom_tab_listView_configuration__r[i].translatedLabel = $A.get("$Label.c.BI_SF1_" + response.getReturnValue().metadataConfig.BI_SF1_Custom_tab_listView_configuration__r[i].MasterLabel);
			}
			cmp.set("v.config", response.getReturnValue().metadataConfig);

		});
		$A.enqueueAction(action);

	}
})