({
	disablePicklists : function(component, event, helper) {
        if(document.getElementById('familycombo').disabled == true){
            document.getElementById('familycombo').disabled = false;
            document.getElementById('familycombo').value = 'none';
            document.getElementById('servicecombo').value = 'none';
            document.getElementById('serviceunitcombo').value = 'none';
        }else{
            document.getElementById('familycombo').disabled = true;
        }
        if(document.getElementById('servicecombo').disabled == false){
            document.getElementById('servicecombo').disabled = true;
        }
        if(document.getElementById('serviceunitcombo').disabled == false){
            document.getElementById('serviceunitcombo').disabled = true;
        }
        

	},
	mainInit: function(component, event, helper){
		component.find('servicecombo').set("v.disabled", true);
		component.find('serviceunitcombo').set("v.disabled", true);
	}
})