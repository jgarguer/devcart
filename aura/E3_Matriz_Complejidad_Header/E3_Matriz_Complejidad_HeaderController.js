({
	refreshMatrix : function(component, event, helper) {
        // Fire event to update complexity matrix
        var submitEvent = $A.get("e.c:E3_Matriz_Complejidad_SubmitEvent");
        submitEvent.setParams({
            "message" : "Actualizar matriz de complejidad" });
        submitEvent.fire(); 
        
		// Fire event to update complexity matrix
        var refreshEvent = $A.get("e.c:E3_Matriz_Complejidad_RefreshEvent");
        refreshEvent.setParams({
            "message" : "Refrescar matriz de complejidad" });
        refreshEvent.fire(); 		
	},
    
    setTab : function(component, event, helper) {
        var tabSelected = event.getParam("header");
        switch(tabSelected){
            case "Core" : 
            	component.set("v.selected", tabSelected);
            	break;
            case "Tradicional" : 
            	component.set("v.selected", tabSelected);
            	break;
            case "Digital" : 
            	component.set("v.selected", tabSelected);
        }
	}
})