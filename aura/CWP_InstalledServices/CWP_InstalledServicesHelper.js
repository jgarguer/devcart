({
    getFromSummary : function(cmp,c) {
        document.getElementById("backgroundmodalwait").style.display='';
        
        var action2 = cmp.get("c.getResumeList");
        action2.setCallback(this, function(response1) {
            if (cmp.isValid() && response1.getState() === "SUCCESS") {
                debugger;
                c.resumeList=response1.getReturnValue();
                
                var action = cmp.get("c.loadInfoFromSummary");
                action.setParams({"contextString": JSON.stringify(c)});     
                action.setCallback(this, function(response) {
                    if (cmp.isValid() && response.getState() === "SUCCESS") {
                        debugger;
                        var context = JSON.parse(response.getReturnValue());
                        cmp.set("v.context", context);
                        document.getElementById("Name$$$()$$$field").parentElement.classList.add('headerSortDown');
                        document.getElementById("backgroundmodalwait").style.display='none';
                    }else{
                        document.getElementById("backgroundmodalwait").style.display='none';
                    }
                });
                $A.enqueueAction(action);
                
                
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action2);
    },
    getNewPagination : function(cmp,c) {
        document.getElementById("backgroundmodalwait").style.display='';
        
        debugger;
        var action = cmp.get("c.getNewTableController");
        action.setParams({"contextString": JSON.stringify(c)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                debugger;
                var context = JSON.parse(response.getReturnValue());
                cmp.set("v.context", context);
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
    },
    onchangeServiceFamilyJS : function(cmp,c) {
        document.getElementById("backgroundmodalwait").style.display='';
        
        debugger;
        var action = cmp.get("c.onchangeServiceFamily");
        action.setParams({"contextString": JSON.stringify(c)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                debugger;
                var context = JSON.parse(response.getReturnValue());
                cmp.set("v.context", context);
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
    },
    onchangeServiceJS : function(cmp,c) {
        document.getElementById("backgroundmodalwait").style.display='';
        debugger;
        var action = cmp.get("c.onchangeService");
        action.setParams({"contextString": JSON.stringify(c)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                debugger;
                var context = JSON.parse(response.getReturnValue());
                cmp.set("v.context", context);
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
    }, 
    
    aplyFiltersFromAdvancedJS : function(cmp,c) {
        document.getElementById("backgroundmodalwait").style.display='';
        debugger;
        var action = cmp.get("c.aplyFiltersFromAdvanced");
        action.setParams({"contextString": JSON.stringify(c)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                debugger;
                var context = JSON.parse(response.getReturnValue());
                cmp.set("v.context", context);
                var elementos = document.getElementsByClassName("cwp-columOrder");
                for(var i=0; i<elementos.length; i++){
                    document.getElementById(elementos[i].id).classList.remove('headerSortUp');
                    document.getElementById(elementos[i].id).classList.remove('headerSortDown');
                }
                document.getElementById("Name$$$()$$$field").parentElement.classList.add('headerSortDown');
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
    }
    
    
})