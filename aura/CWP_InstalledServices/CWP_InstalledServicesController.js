({
    doInit : function(cmp, event, helper) {
        
        var evtRec= event.getParam('isFromSummary');
        if(evtRec===true){        
            if( navigator.userAgent.match(/Android/i)
               || navigator.userAgent.match(/webOS/i)
               || navigator.userAgent.match(/iPhone/i)
               || navigator.userAgent.match(/iPad/)
               || navigator.userAgent.match(/iPod/i)
               || navigator.userAgent.match(/BlackBerry/i)
               || navigator.userAgent.match(/Windows Phone/i)
              )
            {
                cmp.set("v.displayExcel", false);
            }
            else {
                cmp.set("v.displayExcel", true);
            }
            // A Petición del COE se habilita siempre la descarga de archivos Excel
         cmp.set("v.displayExcel", true);
            var context = {
                pickListFamilyServiceValue: event.getParam('serviceUnit1'),
                pickListServiceValue: event.getParam('serviceUnit2'),
                pickListServiceUnitValue: event.getParam('serviceUnit3') };
            
            helper.getFromSummary(cmp,context);
            document.getElementById("pagination2").value=5;
            document.getElementById('CWP_IS_serviceSelected_div').style.display='block';
            var comp = cmp.find('CWP_IS_serviceSelected_div');
            $A.util.removeClass(cmp, 'slds-hide');
            
        }else {
            var comp = cmp.find('CWP_IS_serviceSelected_div');
            $A.util.addClass(cmp, 'slds-hide');
        }
    },
    backToSummary : function(cmp, event, helper) {
        debugger;
        var context = cmp.get('v.context');
        context.selPreQuery='';
        context.hayPrequery=false;
        document.getElementById("showFiltersDiv").style.display="inline";
        document.getElementById("showPrequeryDiv").style.display="none";
        document.getElementById("open").style.display="inline";
        document.getElementById("closePre").style.display="none";
        
        var eventL = $A.get("e.c:CWP_IS_FromSummary");
        eventL.setParams({
            "isFromSummary":false, 
            "serviceUnit1": '', 
            "serviceUnit2": '',
            "serviceUnit3": ''});
        eventL.fire();
    },
    doNextPage:function(cmp, event, helper) {
        debugger;
        var controller = cmp.get('v.context');
        controller.pagination="forward";
        helper.getNewPagination(cmp,controller);
    },
    doPrevPage:function(cmp, event, helper) {
        var controller = cmp.get('v.context');
        controller.pagination="backward";
        helper.getNewPagination(cmp,controller);
    },
    doChangePageView:function(cmp, event, helper) {
        var controller = cmp.get('v.context');
        
        controller.pagination=document.getElementById("pagination2").value;
        helper.getNewPagination(cmp,controller);
    },
    doChangeServiceFamily:function(cmp, event, helper) {
        var controller = cmp.get('v.context');
        controller.pickListFamilyServiceValue=document.getElementById("select-ServiveFamily").value;
        helper.onchangeServiceFamilyJS(cmp,controller);
    },
    doChangeService:function(cmp, event, helper) {
        var controller = cmp.get('v.context');
        controller.pickListServiceValue=document.getElementById("select-Service").value;
        helper.onchangeServiceJS(cmp,controller);
    },
    doChangeServiceUnit:function(cmp, event, helper) {
        var controller = cmp.get('v.context');
        controller.pickListServiceUnitValue=document.getElementById("select-ServiceUnitT3").value;
        helper.getFromSummary(cmp,controller);
        document.getElementById("pagination2").value=5;
        
    },
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner,'slds-hide');
    },
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner,'slds-hide');
    },
    commitFilter : function (cmp, event, helper) {
        
        if (document.getElementById("inputByFieldRFS date") !=null){
            var chkdate=document.getElementById("inputByFieldRFS date").value;
            if(chkdate !=''){
                if(!chkdate.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/)){
                    document.getElementById("inputByFieldRFS date").style.border="solid 2px red"; 
                    document.getElementById("inputByFieldRFS date").style.color="red"; 
                    return null;
                }
            }else{
                document.getElementById("inputByFieldRFS date").style.border="";
                document.getElementById("inputByFieldRFS date").style.color="black"; 
            } 
        }
        
        debugger;
        var context = cmp.get('v.context');
        var idAux='';
        var myMap = {};
        var myMap2 ={};
        
        for (var i = 0; i < context.filterByAttribute.length; i++){
            if(document.getElementById("inputByAttribute"+ context.filterByAttribute[i]).value != ''){
                idAux=document.getElementById("inputByAttribute"+ context.filterByAttribute[i]).id.replace('inputByAttribute','');
                myMap[idAux]=document.getElementById("inputByAttribute"+ context.filterByAttribute[i]).value;
            }
        }
        
        for (var i = 0; i < context.filterByFieldOrderedList.length; i++){
            if(document.getElementById("inputByField"+ context.filterByFieldOrderedList[i]).value != ''){
                idAux=document.getElementById("inputByField"+ context.filterByFieldOrderedList[i]).id.replace('inputByField','');
                myMap2[idAux]=document.getElementById("inputByField"+ context.filterByFieldOrderedList[i]).value;
                
            }
        }
        
        context.filterByAttMapJSON=JSON.stringify(myMap);
        context.filterByFieldMapJSON=JSON.stringify(myMap2);
        context.tableControl.iniIndex=1;
        //sendPreFilters(JSON.stringify(myMap),JSON.stringify(myMap2),1);
        helper.aplyFiltersFromAdvancedJS(cmp,context);
    },
    
    clearFilterFields : function (cmp, event, helper) {
        event.preventDefault();
        var context = cmp.get('v.context');
        if (document.getElementById("inputByFieldRFS date") !=null){
            document.getElementById("inputByFieldRFS date").style.border="";
        }
        for (var i = 0; i < context.filterByAttribute.length; i++){
            if(document.getElementById("inputByAttribute"+ context.filterByAttribute[i]).value != ''){
                document.getElementById("inputByAttribute"+ context.filterByAttribute[i]).value = '';
            }
        }
        
        for (var i = 0; i < context.filterByFieldOrderedList.length; i++){
            if(document.getElementById("inputByField"+ context.filterByFieldOrderedList[i]).value != ''){
                document.getElementById("inputByField"+ context.filterByFieldOrderedList[i]).value = '';
            }
        }
        helper.getFromSummary(cmp,context);
    },
    showExcelFilter : function (cmp, event, helper) {
        debugger;
        document.getElementById("backgroundmodalwait").style.display='';
        var context = cmp.get('v.context');
        var columna=event.target.id;
        columna=columna.split('$$()$$');
        context.pickedRow=columna[0];
        if (context.filterByAttribute.indexOf(context.pickedRow)>-1){
             context.typeExcelFilter='att';
        }else{
            context.typeExcelFilter='field';
        }
           
        var action = cmp.get("c.loadExcelModal");
        
        action.setParams({"contextString": JSON.stringify(context)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                debugger;
                var context = JSON.parse(response.getReturnValue());
                console.log(context.filterListExcel);
                cmp.set("v.context", context);
                setTimeout(
                    function(){
                        $("#nameID").chosen();
                        $("#nameID").trigger("chosen:updated")
                    }, 10); 
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
    },
    addExcelFilter : function (cmp, event, helper) {
        event.preventDefault();
        document.getElementById("backgroundmodalwait").style.display='';
        var value =document.getElementById('nameID').value;
        var context = cmp.get('v.context');
        context.inputFilterValue=value;
        var action = cmp.get("c.aplicaFiltroExcel");
        action.setParams({"contextString": JSON.stringify(context)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                debugger;
                var context = JSON.parse(response.getReturnValue());
                console.log(context.filterListExcel);
                cmp.set("v.context", context); 
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
        
    }, 
    removeExcelFilter : function (cmp, event, helper) {
        debugger;
        document.getElementById("backgroundmodalwait").style.display='';
        event.preventDefault();
        var value =event.target.id;
        var context = cmp.get('v.context');
        context.inputFilterValue=value;
        var action = cmp.get("c.quitaFiltroExcel");
        action.setParams({"contextString": JSON.stringify(context)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                debugger;
                var context = JSON.parse(response.getReturnValue());
                console.log(context.filterListExcel);
                cmp.set("v.context", context); 
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
        
    },
    sortTable : function (cmp, event, helper) {
        debugger;
        document.getElementById("backgroundmodalwait").style.display='';
        var value =event.target.id;
        var context = cmp.get('v.context');
        var columna=event.target.innerText;
        //columna=columna.split('$$$()$$$');
        if(columna===context.sortedRow) {
            context.sortedOrder= (context.sortedOrder==='DESC') ? 'ASC' : 'DESC';
        }else{
                   context.sortedOrder='ASC'; 
        }
        context.sortedRow=columna;
        debugger;
        var elementos = document.getElementsByClassName("cwp-columOrder");
        for(var i=0; i<elementos.length; i++){
            document.getElementById(elementos[i].id).classList.remove('headerSortUp');
            document.getElementById(elementos[i].id).classList.remove('headerSortDown');
        }
        if(context.sortedOrder=='ASC'){
            if (value.indexOf('@')==-1)
                document.getElementById(value).parentElement.classList.add('headerSortUp');
            else
                document.getElementById(value).parentElement.parentElement.classList.add('headerSortUp');
        }
        else{
            if (value.indexOf('@')==-1)
                document.getElementById(value).parentElement.classList.add('headerSortDown');
            else
                document.getElementById(value).parentElement.parentElement.classList.add('headerSortDown');
        }
        var action = cmp.get("c.sortExcelTable");
        action.setParams({"contextString": JSON.stringify(context)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                debugger;
                var context = JSON.parse(response.getReturnValue());
                console.log(context.filterListExcel);
                cmp.set("v.context", context); 
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
        
    },
    getPrequerys : function (cmp, event, helper) {
        var context = cmp.get('v.context');
        debugger;
        document.getElementById("backgroundmodalwait").style.display='';
        var action = cmp.get("c.getPreFilterQuerysList");
        action.setParams({"contextString": JSON.stringify(context)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                debugger;
                var context = JSON.parse(response.getReturnValue());
                console.log(context.filterListExcel);
                cmp.set("v.context", context);
                document.getElementById("showFiltersDiv").style.display="none";
                document.getElementById("showPrequeryDiv").style.display="inline";
                document.getElementById("open").style.display="none";
                document.getElementById("closePre").style.display="inline";          
                
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
        
    },
    closePrequerys : function (cmp, event, helper) {
        var context = cmp.get('v.context');
        context.selPreQuery='';
        context.hayPrequery=false;
        document.getElementById("showFiltersDiv").style.display="inline";
        document.getElementById("showPrequeryDiv").style.display="none";
        document.getElementById("open").style.display="inline";
        document.getElementById("closePre").style.display="none";
        
        helper.getFromSummary(cmp,context);
    },
    filterWithPrequery : function (cmp, event, helper) {
        debugger;
        var context = cmp.get('v.context');
        context.selPreQuery=document.getElementById("select-prequery").value;
        document.getElementById("backgroundmodalwait").style.display='';
        var action = cmp.get("c.getSelectedPreQuery");
        action.setParams({"contextString": JSON.stringify(context)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                debugger;
                var context = JSON.parse(response.getReturnValue());
                console.log(context.filterListExcel);
                cmp.set("v.context", context);
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
        
    }, 
    showPDF : function (cmp, event, helper) {
        debugger;
        document.getElementById("backgroundmodalwait").style.display='';
        var landscape = document.getElementById('radio-landscape').checked;
        var orientation;
        if(landscape === true){
        	orientation = 'landscape';
        }else{
        	orientation = 'portrait';
        }
        var context = cmp.get('v.context');
        var action = cmp.get("c.exportPDF");
        action.setParams({"contextString": JSON.stringify(context),
        				  "orientation" : orientation
        				});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                document.getElementById("CWP_exportServices_PDF_L").click();
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action); 
    },
    showExcel : function (cmp, event, helper) {
        debugger;
        var context = cmp.get('v.context');
        var action = cmp.get("c.exportPDF");
        document.getElementById("backgroundmodalwait").style.display='';
        action.setParams({"contextString": JSON.stringify(context),"orientation" : 'Excel'});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                document.getElementById("CWP_exportServices_Excel_L").click();
                document.getElementById("backgroundmodalwait").style.display='none';
            }else{
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action); 
    },
    resetStyle : function (cmp, event, helper) {
        document.getElementById("inputByFieldRFS date").style.border="";
        document.getElementById("inputByFieldRFS date").style.color="black";                                                                                                        
    }, 
    showDetailModal : function (cmp, event, helper) {
        
        var context = cmp.get('v.context');
        var ev = $A.get("e.c:CWP_MyService_services_details_event");
        var a = event.target.id;
        ev.setParams({'OiId':a});
        var ini= parseInt(context.tableControl.iniIndex)+parseInt(event.target.dataset.index)-1;
        ev.setParams({'index': ini});
        ev.setParams({'numRecords':context.tableControl.numRecords});
        ev.fire();                                                
    }, 
    changeItemDetails : function (cmp, event, helper) {
        document.getElementById("backgroundmodalwait").style.display='';
        var tipo= event.getParam('type');
        var prevIndex= event.getParam('index');
        var context = cmp.get('v.context');
        //Controlamos botón pulsado
        if(tipo=='next') {
            //Controlamos si mostramos en la tabla el elemento al que queremos hacer referencia
            //Si el indice que nos pasan es menor que el mayor que tenemos cargado
            if(prevIndex%context.tableControl.viewSize!=4 || prevIndex==context.tableControl.iniIndex-1){
                prevIndex++;                
                var elem=parseInt((prevIndex)%context.tableControl.viewSize);
                elem=(elem<0?(context.tableControl.finIndex-1):elem);
                
                var id= context.listaResultados[elem].name;
                id='OI-'+id;
                debugger;
                var ev = $A.get("e.c:CWP_MyService_services_details_event");
                ev.setParams({'OiId':id});
                var ini= prevIndex;
                ini=(ini==0)?context.tableControl.iniIndex+context.tableControl.viewSize:ini;
                ev.setParams({'index': ini});
                ev.setParams({'numRecords':context.tableControl.numRecords});
                ev.fire();
                document.getElementById("backgroundmodalwait").style.display='none';
                
            }else {
                context.pagination="forward";
                debugger;
                var action = cmp.get("c.getNewTableController");
                action.setParams({"contextString": JSON.stringify(context)});     
                action.setCallback(this, function(response) {
                    if (cmp.isValid() && response.getState() === "SUCCESS") {
                        debugger;
                        var context2 = JSON.parse(response.getReturnValue());
                        cmp.set("v.context", context2);
                        var id= context2.listaResultados[0].name;
                        id='OI-'+id;
                        var ev = $A.get("e.c:CWP_MyService_services_details_event");
                        ev.setParams({'OiId':id});
                        ev.setParams({'index': context2.tableControl.iniIndex-1});
                        ev.setParams({'numRecords':context2.tableControl.numRecords});
                        ev.fire();
                        document.getElementById("backgroundmodalwait").style.display='none';
                    }else{
                        document.getElementById("backgroundmodalwait").style.display='none';
                    }
                });
                $A.enqueueAction(action);
            }
        }
        else{
            debugger;
            
            //Controlamos si mostramos en la tabla el elemento al que queremos hacer referencia
            //
            prevIndex--;
            if(context.tableControl.iniIndex-1<= prevIndex && prevIndex<= context.tableControl.finIndex-1){
                var id= context.listaResultados[prevIndex].name;
                id='OI-'+id;
                var ev = $A.get("e.c:CWP_MyService_services_details_event");
                ev.setParams({'OiId':id});
                ev.setParams({'index': prevIndex});
                ev.setParams({'numRecords':context.tableControl.numRecords});
                ev.fire();
                document.getElementById("backgroundmodalwait").style.display='none';
                
            }
            else{
                context.pagination="backward";
                debugger;
                var action = cmp.get("c.getNewTableController");
                action.setParams({"contextString": JSON.stringify(context)});     
                action.setCallback(this, function(response) {
                    if (cmp.isValid() && response.getState() === "SUCCESS") {
                        debugger;
                        var context2 = JSON.parse(response.getReturnValue());
                        cmp.set("v.context", context2);
                        var id= context2.listaResultados[context2.tableControl.viewSize-1].name;
                        id='OI-'+id;
                        var ev = $A.get("e.c:CWP_MyService_services_details_event");
                        ev.setParams({'OiId':id});
                        ev.setParams({'index': context2.tableControl.finIndex-1});
                        ev.setParams({'numRecords':context2.tableControl.numRecords});
                        ev.fire();
                        document.getElementById("backgroundmodalwait").style.display='none';
                    }else{
                        document.getElementById("backgroundmodalwait").style.display='none';
                    }
                });
                $A.enqueueAction(action);
            }
            
        }
        
    }
    
})