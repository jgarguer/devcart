({
    
    getUserAccId : function(cmp, event, helper){
        console.log('eventoContainer ' + event.getParam("currentUserId"));
        console.log($A.get("$Label.c.CWP_Url")+'/resource/CWP_AC_HTMLFooter.html')
        cmp.set("v.useracc", event.getParam("currentUserId") + '-' + event.getParam("currentAccId"))
    },
    
    changedUserAcc : function(cmp, event, helper){
    	console.log('cambiaUSer ' + cmp.get("v.useracc"));
        
        $A.createComponent(
            "c:CWP_AC_QuoteHistory",
		{
            "aura:id" : "quoteHistory",
            "useracc" : cmp.get("v.useracc")
        }, 
        function(quoteHistory, status, errorMessage){
            if(status=="SUCCESS"){
                cmp.set("v.quoteHistory", quoteHistory);
                
            }else{
                console.log(errorMessage);
            }
        });
    },
    
	showQuoteTable : function(cmp, evt, helper) {
        
        $A.createComponent(
            "c:CWP_AC_BulkAutoQuotes",
		{
            "aura:id" : "bulkTable",
            "useracc" : cmp.get("v.useracc")
        }, 
        function(bulkAutoQuotes, status, errorMessage){
            if(status=="SUCCESS"){
                cmp.set("v.bulkTable", bulkAutoQuotes);
                
            }else{
                console.log(status);
                console.log(errorMessage);
            }
        });
        
        cmp.find("quoteHistory").destroy();
        var hideButton = cmp.find("header");
        $A.util.addClass(hideButton, 'slds-hide');
        
	},
    reloadHistory : function(cmp, evt, helper){
        console.log('evento refresco ' + cmp.get("v.useracc"));
        
        var hideButton = cmp.find("header");
        $A.util.removeClass(hideButton, 'slds-hide');
         
        $A.createComponent(
            evt.getParam("cmpCrear"),
		{
            "aura:id" : "quoteHistory",
            "useracc" : cmp.get("v.useracc")
        }, 
        function(quoteHistory, status, errorMessage){
            if(status=="SUCCESS"){
                cmp.set("v.quoteHistory", quoteHistory);
                
            }
        });
    }
})