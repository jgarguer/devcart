({
	cancel : function(component, event, helper) {
		component.destroy();
	},

	action : function(component, event, helper) {
		var action = component.get("c.eliminar");
        action.setParams({
            "recordId": component.get("v.recordId"),
            "objName": component.get("v.objName")
        });
        action.setCallback(this, function(response){
            if(component.isValid() && response.getState() === "SUCCESS"){ 
                var toastSucc = $A.get("e.force:showToast");
        		toastSucc.setParams({
	                "title": "Eliminar Contacto",
	                "type": "SUCCESS",
	                "message": "El contacto ha sido eliminado correctamente"
        		});
				toastSucc.fire();
				var redEvt = $A.get("e.force:navigateToComponent");
			    redEvt.setParams({
			        componentDef : "c:BI_SF1_ContactTab",
			        componentAttributes: {
			            
			        },
			        "isRedirect": true
			    });
			    redEvt.fire();
			    component.destroy();
            }else{
                var toastErr = $A.get("e.force:showToast");
	    		toastErr.setParams({
		            "title": "Eliminar Contacto",
		            "type": "ERROR",
		            "message": "El contacto no ha podido ser eliminado"
        		});
        		toastErr.fire();
        		component.destroy();
            }
        });

        $A.enqueueAction(action);
	}
})