({
	previousPage : function(cmp, pageNumber, totalPages) {
        pageNumber--;
        console.log("pagenambe "+pageNumber);
        if(pageNumber>=0){
            this.setPage(cmp, pageNumber);
        }
	},
    
    nextPage : function(cmp, pageNumber, totalPages) {

        pageNumber++;
        if(pageNumber<= (totalPages - 1)){
            console.log("setPageNextPage");
            this.setPage(cmp, pageNumber);
        } 
	},
    
    moveToPageNumber : function(cmp, pageNumber, totalPages) {
        pageNumber = cmp.find("moveToPageNumber").get("v.value");
        console.log(pageNumber);
        this.setPage(cmp, pageNumber);
	},
    
    moreRecords : function(cmp, pageNumber, totalPages) {

        console.log(cmp.get("v.numMaximo"));
        var lst_useracc = cmp.get("v.useracc").split("-");
        var action = cmp.get("c.getQuotes");
        action.setParams({
            user : lst_useracc[0],
            acc : lst_useracc[1],
            recordsPerPage : parseInt(cmp.get("v.numMaximo"))
        });
         action.setCallback(this, function(response) {
             var arrayPages = []
             for(var i in response.getReturnValue().quotes){
                 arrayPages.push(parseInt(i));
             }
             response.getReturnValue().arrayPages = arrayPages;
             cmp.set("v.dataTable", response.getReturnValue());
             cmp.set("v.PgNumber", 0);
             cmp.set("v.quotePage", response.getReturnValue().quotes[cmp.get("v.PgNumber")]);
         });
        $A.enqueueAction(action);
	},
    
    setPage : function(cmp, pageNumber) {
        console.log("Entra en setPage " + pageNumber);
		cmp.set("v.PgNumber", pageNumber);
        cmp.set("v.quotePage", cmp.get("v.dataTable").quotes[pageNumber]);
	}
})