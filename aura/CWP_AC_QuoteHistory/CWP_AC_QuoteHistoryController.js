({
    doInit : function(cmp, event, helper){
    	console.log('doInit');
        console.log(cmp.get("v.useracc"));
        var lst_useracc = cmp.get("v.useracc").split("-");
        console.log(lst_useracc);
       	console.log(cmp.get("v.numMaximo"));
        var action = cmp.get("c.getQuotes");
        action.setParams({
            user : lst_useracc[0],
            acc : lst_useracc[1],
            recordsPerPage : cmp.get("v.numMaximo")
        });
         action.setCallback(this, function(response) {
             console.log("entro en doInit");
             console.log(response.getReturnValue());
             var arrayPages = []
             for(var i in response.getReturnValue().quotes){
                 arrayPages.push(parseInt(i));
             }
             response.getReturnValue().arrayPages = arrayPages;
             cmp.set("v.dataTable", response.getReturnValue());
             
             cmp.set("v.quotePage", response.getReturnValue().quotes[cmp.get("v.PgNumber")]);
         });
        $A.enqueueAction(action);
    },
    
    quoteSelected : function(cmp, event, helper){
        var quoteId = event.currentTarget.id;
        cmp.set("v.recordId", quoteId);
        
        var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": "/order/"+quoteId
            });
            urlEvent.fire();
    },
    
    tableNavigation : function(cmp, event, helper){
        var pageNumber = cmp.get("v.PgNumber");
        var totalPages = cmp.get("v.dataTable").quotes.length;
        console.log("Entra en navegacion");
        var nvgtClicked = event.currentTarget.id;
        
        console.log("Entra en navegacion "+nvgtClicked);
        switch(nvgtClicked){
            case "previousPage":
                console.log("Entra en navegacion "+nvgtClicked);
                helper.previousPage(cmp, pageNumber, totalPages);
                break;
            case "nextPage":
                console.log("Entra en navegacion "+nvgtClicked);
                helper.nextPage(cmp, pageNumber, totalPages);
                break;
        }
        
        
        
    },
    
    picklistNav : function(cmp, evt, helper){
    	console.log("Entra en navegacion picklist");
        var pageNumber = cmp.get("v.PgNumber");
        var totalPages = cmp.get("v.dataTable").quotes.length;
        var nvgtClicked = evt.getSource().getLocalId();
        console.log(nvgtClicked);
        switch(nvgtClicked){
            case "moveToPageNumber":
                helper.moveToPageNumber(cmp, pageNumber, totalPages);
                break;
            case "recordsToShowList":
                helper.moreRecords(cmp);
                break;
        }
	}
    
})