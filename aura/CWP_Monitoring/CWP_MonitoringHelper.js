({
    loadInfo: function(component) {
        var action = component.get("c.getAplis");
        //Callback
        action.setCallback(this, function(actionResult) {
            if (component.isValid() && actionResult.getState() == "SUCCESS") {
                var returnValue = actionResult.getReturnValue();
                if (returnValue != null) {
                    if (returnValue == "") {
                        component.set("v.appsAv", false);
                    }
                    component.set("v.apps", actionResult.getReturnValue());
                    //                    if (component.get("v.isTGS") == false) {
                    //                        this.loadBienInfo(component);
                    //                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    isTgs: function(component) {
        var action = component.get("c.getTgs");
        //Callback
        action.setCallback(this, function(actionResult) {
            if (component.isValid() && actionResult.getState() == "SUCCESS") {
                var returnValue = actionResult.getReturnValue();
                if (returnValue != null) {
                    component.set("v.isTGS", returnValue);
                }
            }
        });
        $A.enqueueAction(action);
    },

    loadBienInfo: function(component) {

        var action = component.get("c.getBien");
        //Callback
        action.setCallback(this, function(actionResult) {
            if (component.isValid() && actionResult.getState() == "SUCCESS") {
                var returnValue = actionResult.getReturnValue();
                if (returnValue != null) {
                    for (i = 0; i < returnValue.length; i++) {
                        var section = returnValue[i].BI_Section__c;
                        var url = returnValue[i].BI_URL__c;
                        if (section == "Top Performance") {
                            component.set('v.performance', url);
                        } else if (section == "Consolidated View") {
                            if(url.indexOf('URL ')>0){
                                component.set('v.consolidate', url.substring(5));
                            }else{
                                component.set('v.consolidate', url);
                            }
                            
                        } else if (section == "Traffic Management") {
                            component.set('v.traffic', url);
                        } else if (section == "Network Quality") {
                            component.set('v.network', url);
                        }
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
})