({
    close : function(cmp, event, helper) {
        cmp.destroy();
    },
    navigateTo : function(cmp, event, helper){
           
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/tgs-portal-sdn"
        });
        urlEvent.fire();
        cmp.destroy();
    }
})