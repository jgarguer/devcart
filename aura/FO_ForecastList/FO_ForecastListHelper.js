({
    drawTable : function(cmp,helper) {
        //Function para pintar la tabla con todos los datos
        console.log("LLEGHUE");
        var periodos = cmp.get("v.InfoPeriodos");
        if(cmp.get("v.recordId")==null)
            var perId = cmp.find("Idperio").get("v.value");
        else{
             $A.util.removeClass(cmp.find("refresh-button"),"slds-hide");
             var perId = cmp.get("v.PeriodoWId");
        }
            
        var fechIni = new Date(periodos[perId].BI_FVI_Fecha_Inicio__c);
        var fechFin = new Date(periodos[perId].BI_FVI_Fecha_Fin__c);
        var chosenFore= cmp.get("v.ChosenFore");
        //mis periodos hijos, tenfo que hace la suma de todos los hijos para ponerla en el padre
        //Tengo que recorrerme los anteriores y los ajustados de mi mes, sumarlo al Q y sumarlo al Año(Yupi)
        var periSon = cmp.get("v.InfoPeriodosHijos");
        //Id de los periodso ordenados que no son semana
        var keys =[];
        //{Id_padre,[semanas]}
        //Solo semanas
        var sem={};
        //Nombre ordenados por fehcas de los peridoos
        var nameSort=[];
        var opps = cmp.get("v.Opps");
        var foresAnt = cmp.get("v.ForeANT");       
        var foresAjus = cmp.get("v.ForeAjus");
        var objetivos = cmp.get("v.Objetivo");
        var stagesName = cmp.get("v.Stages");
        //Objetivos en la tabla
        var objTabla = [];
        //Objetivos acumulados
        var objAcumTabla = [];
        //Actuals y acum de Mes para arriba
        var actualsAcum = [];
        var actuals = [];
        //Primero vamos a ordenar las oportunidades en 2 (Cerradas y no cerradas)
        //Dentr de las cerradas hemos de evaluar las que son de semana o las que son de superiores
        var oppsClosed =  {};
        var oppsClosedSem =  {};
        //{periodo,{StageName,NAV}}
        var oppsOpened = {};
        //{Sequece,[actual week]}
        var actualsWeek ={};
        //Forecast este esta dividio en 2 dependiendo de si el periodo a acabado o no
        //foreNot={Fo anteriores,FO mes actual(Con todo sumao), FO siguinetes,annuañ}
        var foreNot=new Object();
        foreNot.ant=[];
        foreNot.actMonth=[];
        foreNot.futMonth=[];
        foreNot.actQ=[];
        foreNot.futQ=[];
        foreNot.year=0;
        var foreAjust=new Object();
        foreAjust.ant=[];
        foreAjust.futM=[];
        foreAjust.actQ=[];
        foreAjust.fut=[];
        foreAjust.year=[];
        //Aqui sin los ajustables
        var mpFore={};
        var foreOpen=[];
        var foreClosed=[];
        //{Id_Per,Total de las sumas}
        var forePerTot={};
        var forePerAjusTot={};
        var totalFore=[];
        //{Id_per,Suma de los hijos u anteriores}
        var mp_periSum={};
        var forecastWeekOpen={};
        var forecastWeekClosed={};
        //{Seq_Semana,{fore.pas,fore.fut}}
        var foreAuxTotalWeek={};
        var totalWeekFore=[];
        //Pipeline section
        var totalPipePer=[];
        var totalPipeStage=[];
        var totalPipeStageMap={};
        var auxStageCut=[];
        //TACKER
        var conversionForePipe=[];
        var exitFore=[];
        var coberturaExitFore=[];
        var coberturaObjetivo=[];
        var coberturaAcum=[];
        //totals sum mes,Q,año
        var periMes=periodos[perId].FO_Periodo_Padre__c;
        var periQ=periodos[periMes].FO_Periodo_Padre__c;
        var periAnn=periodos[periQ].FO_Periodo_Padre__c;
        var mp_Pa = {};
        mp_Pa[periMes]=0;
        mp_Pa[periQ]=0;
        mp_Pa[periAnn]=0;
      /*  Quieren el nombre netero for(var k = 0;k<stagesName.length;k++){
            if(auxStageCut.indexOf(stagesName[k].substring(0,2))==-1)
                auxStageCut.push(stagesName[k].substring(0,2));
        }*/
        //stagesName=auxStageCut;
        var stages = [];
        for(var k in stagesName){
            stages.push(k);
        }
            
         stages.sort(function(a,b){
             
            if(a>b){
                return 1;
            } else{
                return -1;
            }
            });
        console.log("Stages:"+stages);
        
        for(var k = 0;k<stages.length;k++){
            /* NOmbre entero de las etapas
            if(totalPipeStageMap[stagesName[k].substring(0,2)]==null){
                totalPipeStageMap[stagesName[k].substring(0,2)]=[];    
            }*/
            if(totalPipeStageMap[stages[k]]==null){
                totalPipeStageMap[stages[k]]=[];    
            }
            
        }
        //Ordenacion de oportunidades
        for(var i = 0;i<opps.length;i++){
            var op =opps[i];
            debugger;
            if(op.StageName.startsWith("F1")){
                for(var key in periodos){
                    if(op.BI_Fecha_de_cierre_real<=periodos[key].BI_FVI_Fecha_Fin__c && op.BI_Fecha_de_cierre_real>=periodos[key].BI_FVI_Fecha_Inicio__c){
                        var acum = 0;
                        if(periodos[key].FO_Type__c!="Semana"){
                            if(oppsClosed[key]!=null)
                                acum=Number(parseFloat(oppsClosed[key]).toFixed(2));
                            acum+=Number(parseFloat(op.BI_Net_annual_value_NAV).toFixed(2));
                            oppsClosed[key]=acum;
                        }else{
                            if(oppsClosedSem[key]!=null)
                                acum=Number(parseFloat(oppsClosedSem[key]).toFixed(2));
                            acum+=Number(parseFloat(op.BI_Net_annual_value_NAV).toFixed(2));
                            oppsClosedSem[key]=acum;
                        }
                    }
                }
            }
            else{
                //no esta cerrado
                //aqui se agrupa por periodo y tipo de oportunidad
                for(var key in periodos){
                    if(periodos[key].FO_Type__c!="Semana" ){
                        var fechIniPer = new Date(periodos[key].BI_FVI_Fecha_Inicio__c);
                        var fechFinPer = new Date(periodos[key].BI_FVI_Fecha_Fin__c);
                        if(fechIniPer<=new Date(op.CloseDate) && fechFinPer>=new Date(op.CloseDate)){
                            var aux ={};
                            if(oppsOpened[key]!=null){
                                aux=oppsOpened[key];
                            }
                            var acum = 0;
                            if(aux[op.StageName]!=null)
                                acum=Number(parseFloat(aux[op.StageName]).toFixed(2));
                            acum=Number(parseFloat(acum).toFixed(2))+Number(parseFloat(op.BI_Net_annual_value_NAV).toFixed(2));
                            aux[op.StageName]=acum;
                            oppsOpened[key]=aux;
                        }
                    }
                }
                
            }
                
        }
        //FIN Ordenacion de oportunidades
        //Ordenacion de periodos
        for(var key in periodos){
            if(periodos[key].FO_Type__c!="Semana"){
                keys.push(key);
            }else{
                var semana=[];
                if(sem[periodos[key].FO_Periodo_Padre__c]!=null){
                    semana=sem[periodos[key].FO_Periodo_Padre__c];
                }
                 semana.push(key);
                sem[periodos[key].FO_Periodo_Padre__c]=semana;
            }
                
        }
        keys.sort(function(a,b){
           var fechIniA = periodos[a].BI_FVI_Fecha_Fin__c;
           var fechIniB = periodos[b].BI_FVI_Fecha_Fin__c;
            if(fechIniA>fechIniB){
                return 1;
            }else if(fechIniA<fechIniB){
                return -1;
            }else{
                var fechFinA = periodos[a].BI_FVI_Fecha_Inicio__c;
                var fechFinB = periodos[b].BI_FVI_Fecha_Inicio__c;
                if(fechFinA>fechFinB){
                    return -1;
                }else
                    return 1;
            }
        });
        for(var key in sem){
            var ord = sem[key];
            ord.sort(function(a,b){
                var fechIniA = periodos[a].BI_FVI_Fecha_Fin__c;
                var fechIniB = periodos[b].BI_FVI_Fecha_Fin__c;
                if(fechIniA>fechIniB){
                    return 1;
                }else if(fechIniA<fechIniB){
                    return -1;
                }else{
                    var fechFinA = periodos[a].BI_FVI_Fecha_Inicio__c;
                    var fechFinB = periodos[b].BI_FVI_Fecha_Inicio__c;
                    if(fechFinA>fechFinB){
                        return -1;
                    }else
                        return 1;
                }
                });
            sem[key]=ord;
        }
        //Fin de ordenacion de periodos
        //Ordenacion de oportunindades
       
        var obj = new Object();
        obj.Name="";
        nameSort.push(obj);
        var acumTot =0;
        var acumMes =0;
        var acumActMes=0;
        var acumActTot=0;
        //creacion de la tabla
        console.log("vamos aver si entramos");
        for(var j=0;j<keys.length;j++){
            var aux =new Object();
            aux.Name=periodos[keys[j]].Name;
            aux.Type=periodos[keys[j]].FO_Type__c;
            nameSort.push(aux);
            //Objetivos
            if(objetivos[keys[j]]==null){
                objTabla.push("N/A");    
            }
            else{
                objTabla.push(objetivos[keys[j]].BI_FVI_Objetivo__c);
                console.log(keys[j]); 
            }
            if(periodos[keys[j]].FO_Type__c=="Mes"){
                    if(objetivos[keys[j]]==null){
                        objAcumTabla.push(acumMes);
                    }else{
                        acumMes+=objetivos[keys[j]].BI_FVI_Objetivo__c;
                        objAcumTabla.push(acumMes);    
                    }
                    
                }else if(periodos[keys[j]].FO_Type__c=="Trimestre"){
                    if(objetivos[keys[j]]==null){
                        objAcumTabla.push(acumTot);    
                    }else{
                        acumTot+=objetivos[keys[j]].BI_FVI_Objetivo__c;
                        objAcumTabla.push(acumTot);    
                    }
                    
                }else{
                    //Es anual
                    if(objetivos[keys[j]]==null){
                        objAcumTabla.push(0);     
                    }else{
                        objAcumTabla.push(objetivos[keys[j]].BI_FVI_Objetivo__c);         
                    }
                    
                }
            //Actuals por mes, trimestre y año
            if(oppsClosed[keys[j]]!=null){
                actuals.push(Math.round(oppsClosed[keys[j]]));
                if(periodos[keys[j]].FO_Type__c=="Mes"){
                   acumActMes+=Math.round(oppsClosed[keys[j]]);
                   actualsAcum.push(acumActMes);
                }else if(periodos[keys[j]].FO_Type__c=="Trimestre"){
                  acumActTot+=Math.round(oppsClosed[keys[j]]);  
                  actualsAcum.push(acumActTot);
                }else{
                    actualsAcum.push(Math.round(oppsClosed[keys[j]]));
                }
            }else{
                actuals.push(0);
                if(periodos[keys[j]].FO_Type__c=="Mes"){
                  actualsAcum.push(Math.round(acumActMes));
                }else if(periodos[keys[j]].FO_Type__c=="Trimestre"){
                  actualsAcum.push(Math.round(acumActTot));
                }else{
                  actualsAcum.push(0);
                }
            }
            //Actuals por semana
            if(sem[keys[j]]!=null){
                //Estamos en un mes con semana, vamos a buscar las opps con este actual
                
                var weeks = sem[keys[j]];
                for(var k=0;k<weeks.length;k++){
                    var sum=0;
                    var week = weeks[k];
                    var pe = periodos[week];
                    var seq = periodos[week].FO_Secuencia__c;
                    var auxForeSem = new Object();
                    if(foreAuxTotalWeek[seq]==null){
                        auxForeSem.pasado=[]
                        auxForeSem.futuro=[];
                        auxForeSem.nominal=[];
                        auxForeSem.act=[];
                        auxForeSem.sinScope=[];
                        var auxFore = new Object();
                        auxFore.valueFore="W"+seq;
                        auxForeSem.nominal.push(auxFore);
                    }else{
                        auxForeSem=foreAuxTotalWeek[seq];
                    }
                    var actual =[];
                    if(actualsWeek[seq]!=null){
                        actual=actualsWeek[seq];
                    }else{
                        actual = new Object();
                        actual.nominal=[];
                        actual.pasado=[];
                        actual.act=[];
                        actual.sinScope=[];
                        actual.nominal.push("W"+seq);
                        actual.act.push("");
                    }
                    var act = 0;
                    if(oppsClosedSem[week]!=null){
                        act=oppsClosedSem[week];
                    }
                    actual.act.push(act);
                    //cosole.log(seq+" : "+actual );
                    actualsWeek[seq]=actual;
                    //Aqui van lo Forecast por semana asi evitamos el hacerlo varias veces
                    //Hay que evaular igual que abajo Los Forecast antiguos van como no editables 
                    var foreSem =[];
                    var fechFinWeek = new Date(pe.BI_FVI_Fecha_Fin__c);
                    //recordamos estos son los anteriormente hechos
                    if(week==perId){
                        sum=chosenFore.FO_Total_Forecast__c;
                        var auxFore = new Object();
                        auxFore.fore = chosenFore.Id;
                        auxFore.valueFore=chosenFore.FO_Total_Forecast__c;
                        auxFore.periodo = week;
                        auxFore.Estado = chosenFore.FO_Estado__c;
                        auxFore.FechaFin=periodos[week].BI_FVI_Fecha_Fin__c;
                        auxForeSem.act.push(auxFore);
                        mpFore[week]=auxFore;
                    }else if(foresAjus[week]!=null){
                        //Los ajustados
                        sum=foresAjus[week].FO_Total_Forecast_Ajustado__c;
                        var auxFore = new Object();
                        auxFore.fore = foresAjus[week].Id;
                        auxFore.valueFore=foresAjus[week].FO_Total_Forecast_Ajustado__c;
                        auxFore.periodo = week;
                        auxFore.Estado = foresAjus[week].FO_Estado_Forecast__c;
                        auxFore.FechaFin=periodos[week].BI_FVI_Fecha_Fin__c;
                        if(fechFinWeek<fechFin){
                            auxForeSem.pasado.push(auxFore);
                        }else if(fechFin>new Date()){
                            auxForeSem.futuro.push(auxFore);
                        }else{
                            auxForeSem.pasado.push(auxFore);
                        }
                        foreAuxTotalWeek[seq]=auxForeSem;
                         mpFore[week]=auxFore;
                    }else{
                        //Hay que ver si pertenecen a nuestro mes o no
                        //Vame si estamos fuera de la semana no hay que evaluar nada!
                        //Se pone a 0 no deberia pero se pone al no estar cargado, lo ponemos a 0 no editable al no hacerlo se queda asi:
                        var auxFore = new Object();
                        auxFore.fore = "NAN";
                        auxFore.valueFore="";
                        auxFore.periodo = week
                        if(fechFinWeek<fechFin){
                            auxForeSem.pasado.push(auxFore);    
                        }else{
                            auxForeSem.sinScope.push(auxFore);    
                        }
                        foreAuxTotalWeek[seq]=auxForeSem;
                        
                    }
                   
                    forePerTot[week]=sum;
                    
                }
                if(weeks.length<5){
                    
                     var auxForeSem = new Object();
                    if(foreAuxTotalWeek[5]==null){
                        auxForeSem.pasado=[];
                        auxForeSem.futuro=[];
                        auxForeSem.nominal=[];
                        auxForeSem.act=[];
                        auxForeSem.sinScope=[];
                        var auxForeDum = new Object();
                        auxForeDum.valueFore="W"+5;
                        auxForeSem.nominal.push(auxForeDum);
                    }else{
                        auxForeSem=foreAuxTotalWeek[5];
                    }
                    var auxFore = new  Object();
                    auxFore.valueFore="";
                    auxForeSem.sinScope.push(auxFore);
                    foreAuxTotalWeek[5]=auxForeSem;
                    if(actualsWeek[5]!=null){
                        actualsWeek[5].act.push("");
                    }else{
                        var act = new Object();
                        act.nominal=[];
                        act.pasado=[];
                        act.act=[];
                        act.sinScope=[];
                        act.nominal.push("W5");
                        act.act.push("");
                        actualsWeek[5]=act;
                        
                    }
                }
            }else{
                //Hay que rellenar a nulo!!!               
                for(var s = 1;s<=5;s++){
                    var actualSem=new Object();
                  
                    var auxForeSem = new Object();
                    if(foreAuxTotalWeek[s]==null){
                        auxForeSem.pasado=[];
                        auxForeSem.nominal=[];
                        auxForeSem.futuro=[];
                        auxForeSem.act=[];
                        auxForeSem.sinScope=[];
                        var auxForeDum = new Object();
                        auxForeDum.valueFore="W"+s;
                        auxForeSem.nominal.push(auxForeDum);
                    }else{
                        auxForeSem=foreAuxTotalWeek[s];
                    }
                    var auxFore = new Object();
                    auxFore.fore = "NAN";
                    auxFore.valueFore="";
                    auxFore.periodo = "NAN";
                    
                    if(actualsWeek[s]!=null){
                        actualSem=actualsWeek[s];    
                    }else{
                        actualSem.nominal=[];
                        actualSem.pasado=[];
                        actualSem.act=[];
                        actualSem.sinScope=[];
                        actualSem.nominal.push("W"+s);
                    }
                    console.log("comparacion de fechas: per->"+periodos[keys[j]].BI_FVI_Fecha_Fin__c+" act "+fechFin);
                    if(new Date(periodos[keys[j]].BI_FVI_Fecha_Fin__c)>=fechFin){
                        auxForeSem.sinScope.push(auxFore);
                        actualSem.sinScope.push("");
                    }else{
                        actualSem.pasado.push("");
                        auxForeSem.pasado.push(auxFore);    
                    }
                    actualsWeek[s]=actualSem;
                    foreAuxTotalWeek[s]=auxForeSem;
                    
                }
            }
            //FOrecast
            //
            //Nueva forma de evaluar esto tenemos 6 tipos de periodos ordenados
            //Antiguos,con el valor de la suma de suS FO no editables
            //Mes actual, Ajustable pero no editable
            //Mes futuros, Ajustables y editables
            //Q actual,Ajustable pero no editable
            //Q futuros, ajustable y editable
            //Año, ajustable no editable
            //
            //
            //foreNot.ant=[];
            //
            //
            //
        // Deprecated    var Ant=foresAnt[keys[j]];
            var Ajus=foresAjus[keys[j]];
            var sum = 0;
            //forePerTot
            var auxForeTot = new Object();
            
            if(Ajus!=null){
                sum=Ajus.FO_Total_Forecast_Ajustado__c;
                auxForeTot.fore = Ajus.Id;
                auxForeTot.AjusManager = Ajus.FO_Ajuste_Forecast_Manager__c;
                auxForeTot.AjusReal = Ajus.FO_Forecast_Final__c;
                auxForeTot.valueFore = Ajus.FO_Total_Forecast_Ajustado__c;
                auxForeTot.periodo = keys[j];
                auxForeTot.Estado = Ajus.FO_Estado_Forecast__c;
                auxForeTot.FechaFin=periodos[keys[j]].BI_FVI_Fecha_Fin__c;
                mpFore[keys[j]]=auxForeTot;
                forePerAjusTot[keys[j]]=Ajus.FO_Ajuste_Forecast_Manager__c;
            }else{
                auxForeTot.fore = "N/A";
                auxForeTot.valueFore = "";
                auxForeTot.AjusManager = "";
                auxForeTot.AjusReal = "";
                auxForeTot.Estado = "N/A";
                auxForeTot.periodo = keys[j];
                forePerAjusTot[keys[j]]=0;
                mpFore[keys[j]]=auxForeTot;
            }
            forePerTot[keys[j]]=sum;
           
            //Pipeline
            debugger;
            if(oppsOpened[keys[j]]!=null){
                var acum = 0;
                for(var k=0;k<stages.length;k++){
                    var opsValue = 0;
                    if(oppsOpened[keys[j]][stages[k]]!=null)
                        opsValue=Number(parseFloat(oppsOpened[keys[j]][stages[k]]).toFixed(2));
                    acum+=Number(parseFloat(opsValue.toFixed(2)));
                    totalPipeStageMap[stages[k]].push(opsValue);
                }
                totalPipePer.push(acum);
                
            }else{
                totalPipePer.push("0");
                for(var k=0;k<stages.length;k++){
                    totalPipeStageMap[stages[k]].push(0);
                }
            }
                
        }
        var weeksActualForm=[];
        var weeksForeForm=[];
        var pipeForStage=[];
        debugger;
        for(var stage in totalPipeStageMap){
            var auxPipe = new Object();
            auxPipe.stage=stagesName[stage];
            auxPipe.values=totalPipeStageMap[stage];
            pipeForStage.push(auxPipe);
        }
        
        for(var seq in foreAuxTotalWeek){
            weeksForeForm.push(foreAuxTotalWeek[seq]);
            weeksActualForm.push(actualsWeek[seq]);
        }
        for(var j =0;j<keys.length;j++){
            if(mp_Pa[keys[j]]==null){
               //totalFore.push(forePerTot[keys[j]]);
            }else{
                var res=0;
                var resAjus=0;
                debugger;
                var tp = periSon[keys[j]];
                for(var i = 0;i<tp.length;i++){
                    var per = tp[i].Id;
                    res=Number(parseFloat(res).toFixed(2))+Number(parseFloat(forePerTot[per]).toFixed(2));
                    if(forePerAjusTot[per]!=null)
                        resAjus=Number(parseFloat(resAjus).toFixed(2))+Number(parseFloat(forePerAjusTot[per]).toFixed(2));
                }
                forePerTot[keys[j]]=res;
                mp_Pa[keys[j]]=res;
                if(keys[j]!=periodos[perId].FO_Periodo_Padre__c)
                    forePerAjusTot[keys[j]]=resAjus;
               // totalFore.push(res);
            }
            
           var per = periodos[keys[j]];
            if(new Date(per.BI_FVI_Fecha_Fin__c)<fechFin){
               foreNot.ant.push(mpFore[per.Id]);
               foreAjust.ant.push(mpFore[per.Id]);
            }else if(per.FO_Type__c=="Mes" && new Date(per.BI_FVI_Fecha_Inicio__c)<=fechIni){
                mpFore[per.Id].valueFore=Math.round(mp_Pa[keys[j]]);
                mpFore[per.Id].AjusManager=Math.round(forePerAjusTot[keys[j]]);
                mpFore[per.Id].AjusReal=Math.round(Number(parseFloat(mp_Pa[keys[j]]).toFixed(2))+Number(parseFloat(forePerAjusTot[keys[j]]).toFixed(2)));
                foreNot.actMonth.push(mpFore[per.Id]);   
                foreAjust.futM.push(mpFore[per.Id]);
            }else if(per.FO_Type__c=="Mes"){
                mpFore[per.Id].valueFore=Math.round(mpFore[per.Id].valueFore);
                mpFore[per.Id].AjusManager=Math.round(mpFore[per.Id].AjusManager);
                mpFore[per.Id].AjusReal=Math.round(mpFore[per.Id].AjusReal);
                foreAjust.futM.push(mpFore[per.Id]);
                if(fechFin>new Date())
                    foreNot.futMonth.push(mpFore[per.Id]);
                else
                    foreNot.actMonth.push(mpFore[per.Id]);                
            }else if(per.FO_Type__c=="Trimestre" && new Date(per.BI_FVI_Fecha_Inicio__c)<=fechIni){
                mpFore[per.Id].valueFore=Math.round(mp_Pa[keys[j]]);
                mpFore[per.Id].AjusManager=Math.round(forePerAjusTot[keys[j]]);
                mpFore[per.Id].AjusReal=Math.round(Number(parseFloat(mp_Pa[keys[j]]).toFixed(2))+Number(parseFloat(forePerAjusTot[keys[j]]).toFixed(2)));
                foreAjust.actQ.push(mpFore[per.Id]);
                foreNot.actQ.push(mpFore[per.Id]);
            }else if(per.FO_Type__c=="Trimestre"){
                mpFore[per.Id].valueFore=Math.round(mpFore[per.Id].valueFore);
                mpFore[per.Id].AjusManager=Math.round(mpFore[per.Id].AjusManager);
                mpFore[per.Id].AjusReal=Math.round(mpFore[per.Id].AjusReal);
                foreAjust.fut.push(mpFore[per.Id]);
                 if(fechFin>new Date())
                    foreNot.futQ.push(mpFore[per.Id]);
                else
                    foreNot.actQ.push(mpFore[per.Id]);
            }else{
                mpFore[per.Id].valueFore=Math.round(mp_Pa[keys[j]]);
                mpFore[per.Id].AjusManager=Math.round(forePerAjusTot[keys[j]]);
                mpFore[per.Id].AjusReal=Math.round(Number(parseFloat(mp_Pa[keys[j]]).toFixed(2))+Number(parseFloat(forePerAjusTot[keys[j]]).toFixed(2)));
                foreAjust.year.push(mpFore[per.Id]);
                foreNot.year=mpFore[per.Id]; 
            }
            if(new Date(per.BI_FVI_Fecha_Fin__c)<fechFin){
                conversionForePipe.push("");
                exitFore.push("");
                coberturaExitFore.push("");
            }else{
                
                if(totalPipePer[j]==0){
                    conversionForePipe.push("N/A");
                }else{
                    conversionForePipe.push(mpFore[per.Id].AjusReal/totalPipePer[j]);
                }
                exitFore.push(Number(parseFloat(mpFore[per.Id].AjusReal).toFixed(2))+Number(parseFloat(actuals[j]).toFixed(2)));
                if(objTabla[j]==0 || objTabla[j]=="N/A"){
                    coberturaExitFore.push("N/A");
                }else{
                    coberturaExitFore.push((Number(parseFloat(mpFore[per.Id].AjusReal).toFixed(2))+Number(parseFloat(actuals[j]).toFixed(2)))/Number(parseFloat(objTabla[j]).toFixed(2)));
                }       
            }
            if(objTabla[j]==0 || objTabla[j]=="N/A"){
                coberturaObjetivo.push("N/A");
            }else{
                coberturaObjetivo.push((actuals[j]/objTabla[j]));
            }
            if(objAcumTabla[j]==0 || objAcumTabla[j]=="N/A"){
                coberturaAcum.push("N/A");
            }else{
                coberturaAcum.push((actualsAcum[j]/objAcumTabla[j]));
            }
        }
        debugger;
        console.log("semanas: "+weeksActualForm);
        console.log("Llegamos al final y seteamos");
        cmp.set("v.NameSort",nameSort);
        console.log(objTabla);
        cmp.set("v.ObjetivoSort",objTabla);
        cmp.set("v.ObjetivoSortAcum",objAcumTabla);
        cmp.set("v.ActualsSort",actuals);
        cmp.set("v.ActualsSortAcum",actualsAcum);
        cmp.set("v.ActualsWeekSortAcum",weeksActualForm);
        cmp.set("v.OpenForecast",foreOpen);
        cmp.set("v.CloseForecast",foreClosed);
        cmp.set("v.TotalFore",totalFore);        
        cmp.set("v.weeksActualForm",weeksForeForm);
        cmp.set("v.totalPipePer",totalPipePer);
        cmp.set("v.pipeForStage",pipeForStage);
        cmp.set("v.ForeNot",foreNot);
        cmp.set("v.TotalForeAjust",foreAjust);
        cmp.set("v.MpFore",mpFore);
        cmp.set("v.conversionForePipe",conversionForePipe);
        cmp.set("v.coberturaAcum",coberturaAcum);
        cmp.set("v.exitFore",exitFore);
        cmp.set("v.coberturaExitFore",coberturaExitFore);
        cmp.set("v.coberturaObjetivo",coberturaObjetivo);
        $A.util.removeClass(cmp.find("table"),"slds-hide");s
        cmp.set("v.flagObj",false);
        cmp.set("v.actualsSymbol","[+]");
        var spinner = cmp.find('spinner');    
        $A.util.addClass(spinner,'slds-hide');  
    },
    
    recordSelected : function(cmp,evt,helper){
        //AQUI invocamos para buscar el Forecast
        //para lo que buscamos el nodo y el periodo
        var nodo = cmp.get("v.NodoWId");
        var Idper = cmp.get("v.PeriodoWId");
        console.log("HOLA: "+Idper+"|| Nodo: "+nodo);
        var finishOpp=false;
        var finishFore=true;
        var finishObje=false;
        var finishAjus=false;
         $A.util.addClass(cmp.find("table"),"slds-hide");
        if(Idper=="N/A"){
            $A.util.addClass(cmp.find("fechasPeriodo"),"slds-hide");
            $A.util.addClass(cmp.find("DatosFore"),"slds-hide");
            $A.util.addClass(cmp.find("table"),"slds-hide");
        }else{
            var periodos={};
            var hijos={};
            var perBuscarNoW=[];
            var actionPer=cmp.get("c.getPeriodosForecast");
            actionPer.setParams({"idPeriodo":Idper});
            actionPer.setCallback(this,function(response){
                console.log("hicimos la de los peridos");
            if(response.getState()==="SUCCESS"){
                console.log("hicimos la de los peridos=SUCCEES");
                var result = response.getReturnValue();                                 
                for(var j = 0;j<result.length;j++){
                    periodos[result[j].Id]=result[j];
                    var lst_son = hijos[result[j].FO_Periodo_Padre__c];
                    if(lst_son==null){
                       lst_son=[]; 
                    }
                    lst_son.push(result[j]);
                    hijos[result[j].FO_Periodo_Padre__c]=lst_son;   
                    if(result[j].FO_Type__c!="Semana"){
                        perBuscarNoW.push(result[j].Id);
                    }
                }
                cmp.set("v.ChosenPer",periodos[Idper]);
                cmp.set("v.InfoPeriodos",periodos);
                cmp.set("v.InfoPeriodosHijos",hijos);
                $A.util.removeClass(cmp.find("fechasPeriodo"),"slds-hide");
                console.log(hijos);
                var periodosBuscar = [];
                periodosBuscar.push(Idper);
                console.log("PEriodos:"+periodos);
                var parent = periodos[Idper].FO_Periodo_Padre__c;
                var fechFin = "";
                var fechIni = "";
                //Esto es caso de tener que ir hacia arriba, ahora tendremos que mirar hacia abajo y ver sus hijos y susesivamente...
                while(parent!=null){
                    var listaHijos = hijos[parent];
                    console.log("Esto son los hijos del padre"+parent+" hijos:"+listaHijos);
                    if(periodos[parent]!=null){
                        fechIni=periodos[parent].BI_FVI_Fecha_Inicio__c;
                        fechFin=periodos[parent].BI_FVI_Fecha_Fin__c;
                        parent=periodos[parent].FO_Periodo_Padre__c;
                    }else{
                        parent=null;
                    }    
                }           
                var actionFo = cmp.get("c.getForecasts");
                actionFo.setParams({"periodo":Idper,"nodo":nodo});
                actionFo.setCallback(this,function(response2){
                    console.log("Forecast");
                    if(cmp.isValid() && response2.getState()==="SUCCESS"){
                        var result = response2.getReturnValue();
                        var mp ={};
                        console.log(result);
                        var fore=result[0];
                        cmp.set("v.ChosenFore",fore);
                        if(fore.RecordType.DeveloperName=="FO_Manager"){
                            cmp.set("v.Manager",true);
                        }else{
                            cmp.set("v.Manager",false);
                        }
                        $A.util.removeClass(cmp.find("DatosFore"),"slds-hide");
                        var dateFine = periodos[Idper].BI_FVI_Fecha_Fin__c;
                        debugger;
                        var actionOpp = cmp.get("c.getOpps");
		                actionOpp.setParams({"fechIni":fechIni,"fechFin":fechFin,"idNodo":nodo,"idFore":fore.Id});
        		        actionOpp.setCallback(this,function(responseOpps){
                		    console.log("Oppp");
		                    if(cmp.isValid() && responseOpps.getState()==="SUCCESS"){
                                
        		                cmp.set("v.Opps",responseOpps.getReturnValue());
                        		var resultOpps = responseOpps.getReturnValue();
                		        var fechIniPer=periodos[Idper].BI_FVI_Fecha_Inicio__c;
                        		var fechFinPer=periodos[Idper].BI_FVI_Fecha_Fin__c;
        		                var oppRes = new Object();
                		        oppRes.total=0;
                        		oppRes.commit=0;
		                        oppRes.upside=0;
        		                oppRes.non=0;
                		        for(var i = 0;i<resultOpps.length;i++){
                                   
                        		    if(resultOpps[i].CloseDate>=fechIniPer && resultOpps[i].CloseDate<=fechFinPer){
                                		var op =resultOpps[i];
		                                oppRes.total+=op.BI_Net_annual_value_NAV;
        	                        if(op.BI_O4_Committed__c==true){
            	                        oppRes.commit+=op.BI_Net_annual_value_NAV;
                	                }else if(op.BI_O4_Upside__c==true){
                    	                oppRes.upside+=op.BI_Net_annual_value_NAV;
                        	        }else{
                            	        oppRes.non+=op.BI_Net_annual_value_NAV;
                                	}
                            	}
                        	}
	                        cmp.set("v.ChosenPipe",oppRes);
    	                    finishOpp=true;
        	                if(finishOpp==true && finishFore==true && finishObje==true && finishAjus==true){
                            //Llamamos al helper para pinta la tabla dinamica.
            	                helper.drawTable(cmp,helper);
                	        }
                    	}else if(response2.getState()==="ERROR"){
                        	var errors = response2.getError();
	                        if(errors){
    	                        if(errors[0] && errors[0].message){
        	                        console.log("Error message en Opp:"+errors[0].message);
            	                }
                	            alert("Hubo un error buscando las Oportunidades para esta configuración, por favor dirigete a tu adminsitrador");
                    	    }
	                    }
    		        });
                        var actionForeAjust = cmp.get("c.getAjustes");
                        actionForeAjust.setParams({"IdFore":fore.Id});
                        actionForeAjust.setCallback(this,function(response2){
                            console.log("Ajus:"+fore.Id);
                            if(response2.getState()==="SUCCESS"){
                                var result = response2.getReturnValue();
                                var mp_ajus = {};
                                debugger;
                                for(var z=0;z<result.length;z++){
                                    mp_ajus[result[z].FO_Periodo__c]=result[z];
                                    console.log("Ajus["+result[z].FO_Periodo__c+"] :"+result[z]);
                                }
                                console.log("Ajustado:"+mp_ajus);
                                cmp.set("v.ForeAjus",mp_ajus);
                                finishAjus=true;
                                console.log("AJ");
                                if(finishOpp==true && finishFore==true && finishObje==true && finishAjus==true){
                                    //Llamamos al helper para pinta la tabla dinamica.
                                    helper.drawTable(cmp,helper);
                                }
                            }
                        });
                         $A.enqueueAction(actionOpp);
                        $A.enqueueAction(actionForeAjust);
                        
                    }else if(response2.getState()==="ERROR"){
                       var errors = response2.getError();
                        if (errors) {                        
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " +
                                 errors[0].message);
                            }
                        }
                    }
                });
                
                var actionObj = cmp.get("c.getObjetivo");
                actionObj.setParams({"nodo":nodo,"fechIni":fechIni,"fechFin":fechFin,"periodos":perBuscarNoW});
                actionObj.setCallback(this,function(response2){
                    console.log("Objetivo");
                     if(cmp.isValid() && response2.getState()==="SUCCESS"){
                        var result = response2.getReturnValue();
                         var mp = {};
                         for(var i = 0;i<result.length;i++){
                             mp[result[i].BI_FVI_IdPeriodo__c]=result[i];
                         }
                         cmp.set("v.Objetivo",mp);
                         finishObje=true;
                         if(finishOpp==true && finishFore==true && finishObje==true && finishAjus==true){
                            //Llamamos al helper para pinta la tabla dinamica.
                            helper.drawTable(cmp,helper);
                         }
                    }else if(response2.getState()==="ERROR"){
                        var errors = response2.getError();
                        if(errors){
                            if(errors[0] && errors[0].message){
                                console.log("Error message en Obj:"+errors[0].message);
                            }
                            alert("Hubo un error buscando los Objetivos para esta configuración, por favor dirigete a tu adminsitrador");
                        }
                    }                 
                });
                
                $A.enqueueAction(actionFo);
               
                $A.enqueueAction(actionObj);
                
            }                     
            });                                 
            $A.enqueueAction(actionPer);
            
        }
        
       
    }
})