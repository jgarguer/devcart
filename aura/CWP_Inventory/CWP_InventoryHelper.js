({
	getPaginatedList : function(component, event, helper, listToProcess, index0, indexN, avance) {
        var retList=[];
        indexN = Math.min(indexN, listToProcess.length);
        for(var i = index0; i<indexN; i++){
            retList.push(listToProcess[i]);
        }
        var infoList = String(index0+1) + " - " + String(indexN) + ' Out of ' + String(listToProcess.length);
        component.set("v.infoList", infoList);
        component.set("v.assetList", listToProcess);
        component.set("v.inicio", index0);
        component.set("v.fin", indexN);
        component.set("v.avance", avance);
        component.set("v.listSize", listToProcess.length);
        component.set("v.assetListToOrder", retList);
	},
    
    getSortableList : function(component, event, helper, rcvList){
        var retList = [];
        for(var i =0; i<rcvList.length; i++){
            var innerList = [rcvList[i].Name, rcvList[i].Description__c, rcvList[i].Cantidad__c];
            retList.push(innerList)
        }
        return retList;   
    },
    
    getOrderedList : function(component, event, helper, fieldToOrder, direction){
    	//alert(fieldToOrder);
    	//alert(direction);
        var switching, i, x, y, shouldSwitch;
    	var table = component.get("v.assetList");//document.getElementById('assetTable');
        
        var columnToSort =[];
        var mapToOrder = new Object();
		for(var i =0; i<table.length; i++){
            if(mapToOrder.hasOwnProperty(table[i][fieldToOrder])){
                mapToOrder[table[i][fieldToOrder]].push(i);
            }else{
                var innerList =[i];
                mapToOrder[table[i][fieldToOrder]] = innerList;
            }
        }
        columnToSort = Object.keys(mapToOrder);
        
        columnToSort.sort();
        var inicio;
        var fin;
        var orderedList = [];
        debugger;
        if(direction==="DESC"){
            for(var i =columnToSort.length-1; i>=0; i--){
                for(var j = 0; j< mapToOrder[columnToSort[i]].length; j++){
                    orderedList.push(table[mapToOrder[columnToSort[i]][j]]);
                }
            }
        }else if(direction==="ASC"){
            for(var i =0; i< columnToSort.length; i++){
                for(var j = 0; j< mapToOrder[columnToSort[i]].length; j++){
                    orderedList.push(table[mapToOrder[columnToSort[i]][j]]);
                }
            }
        }
        this.getPaginatedList(component, event, helper, orderedList, 0, component.get("v.avance"), component.get("v.avance"));
    },

})