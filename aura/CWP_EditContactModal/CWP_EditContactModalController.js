({	
    openModal : function(cmp, evt) {
        document.getElementById('backgroundmodal').style=''; 
        var idContact = evt.getParam("idContact");  
        var action = cmp.get("c.getloadInfo");
        action.setParams({'idContact':idContact});
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                cmp.set("v.cont", response.getReturnValue());
                var element = cmp.find('cwp_modal');
                $A.util.removeClass(element, 'slds-hide');
            }
        });
        $A.enqueueAction(action);
        
    },
    hideModal : function(cmp) {
        document.getElementById('backgroundmodal').style='display:none'; 
        var element = cmp.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
    },
    executeEvent : function() {
        var idContact;
        idContact = '0032500000Z4HPUAA3';
        var modal = $A.get("e.c:CWP_EditContactModalEvent");
        modal.setParams({"idContact": idContact});    
        modal.fire();
    },
    hideModalMail : function(component, event) {
        var contacto = component.get("v.cont");
        
        var modalSend = $A.get("e.c:CWP_SendInfoModalEvent");
        modalSend.setParams({"idContact": contacto.Id, "isButton": false});     
        modalSend.fire();  
        
        var element = component.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
    },
    hideModalRequest : function(component, event) {
        var contacto = component.get("v.cont");
        var evt = $A.get("e.c:CWP_menu_Evt");
        var idx = event.target.id;
        evt.setParams({"tabSelected": 'calendar', "userId":contacto.Id});
        evt.fire();
        
        var element = component.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
    },
    hideModalProfile : function(component, event) {
        var element = component.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
    },

})