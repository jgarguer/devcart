({
	showFinalModal : function(component, event, helper) {
        $A.createComponent("c:E3_Matriz_Complejidad_modal2", function (newCompo, status, error){
            component.set("v.body", newCompo);
        });
	},
    doInit: function(component, event, helper) {
        // Get complexity matrix id
        var complexityMatrix; 
		var matrixId = null;
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if(pair[0] == 'matrixId'){ 
                matrixId= pair[1];
            }
        }
        
        // Validate matrix id parameter is sended
        if (matrixId != null) {
            var action = component.get("c.getComplexityMatrix");
        	action.setParams({ matrixId : matrixId });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    complexityMatrix = response.getReturnValue();
                    // Send complexity matrix to view
                    component.set('v.complexityMatrix', complexityMatrix);

                    // Calculate core progress bar
                    var coreChecks = [
                        complexityMatrix.MC_Comunicaciones_avanzadas_check__c, 
                        complexityMatrix.MC_Datos_Fijos_e_Internet_check__c,
                        complexityMatrix.MC_Datos_Moviles_check__c,
                        complexityMatrix.MC_Networking_check__c,
                        complexityMatrix.MC_Terminales_Moviles_check__c,
                        complexityMatrix.MC_TV_Video_check__c
                    ];
                    var coreProgress = helper.calculateProgressBar(coreChecks);
                    var coreProgressBar = document.getElementById('core-progress');
                    coreProgressBar.style.width = coreProgress+'%';
                    
                    // Calculate traditional progress bar
                    var traditionalChecks = [
                        complexityMatrix.MC_Servicios_Telef_Movil_check__c, 
                        complexityMatrix.MC_SMS_check__c,
                        complexityMatrix.MC_Otros_check__c
                    ];
                    var traditionalProgress = helper.calculateProgressBar(traditionalChecks);
                    var traditionalProgressBar = document.getElementById('traditional-progress');
                    traditionalProgressBar.style.width = traditionalProgress+'%';
                    
                    // Calculate digital progress bar
                    var digitalChecks = [
                        complexityMatrix.MC_Cloud_check__c, 
                        complexityMatrix.MC_Contenidos_y_Aplicaciones_check__c,
                        complexityMatrix. MC_EHEALTH_check__c,
                        complexityMatrix. MC_IOT_check__c,
                        complexityMatrix. MC_OUTSOURCING_check__c,
                        complexityMatrix. MC_Puesto_de_Trabajo_check__c,
                        complexityMatrix. MC_Seguridad_check__c,
                        complexityMatrix. MC_Servicios_Financieros_check__c,
                        complexityMatrix. MC_TED_check__c,
                        complexityMatrix. MC_TIS_check__c
                    ];
                    var digitalProgress = helper.calculateProgressBar(digitalChecks);
                    var digitalProgressBar = document.getElementById('digital-progress');
                    digitalProgressBar.style.width = digitalProgress+'%';
                    console.log('Calculate checks');
                    
                    // Calculate checks
                    // Core checks
                    var checkCore1 = document.getElementById('check-core-1');
                    var checkCore2 = document.getElementById('check-core-2');
                    var checkCore3 = document.getElementById('check-core-3');
                    var checkCore4 = document.getElementById('check-core-4');
                    var checkCore5 = document.getElementById('check-core-5');
                    var checkCore6 = document.getElementById('check-core-6');
                    var white = '#FFF';
                    var coreColor = '#F99C24';
                    checkCore1.style.backgroundColor = complexityMatrix.MC_Comunicaciones_avanzadas_check__c?coreColor:white;
                    checkCore2.style.backgroundColor = complexityMatrix.MC_Datos_Fijos_e_Internet_check__c?coreColor:white;
                    checkCore3.style.backgroundColor = complexityMatrix.MC_Datos_Moviles_check__c?coreColor:white;
                    checkCore4.style.backgroundColor = complexityMatrix.MC_Networking_check__c?coreColor:white;
                    checkCore5.style.backgroundColor = complexityMatrix.MC_Terminales_Moviles_check__c?coreColor:white;
                    checkCore6.style.backgroundColor = complexityMatrix.MC_TV_Video_check__c?coreColor:white;
                    
                   	// Find products
                   	console.log('xxxcomplexityMatrix.E3_Opportunity__c ' + complexityMatrix.E3_Opportunity__c);
                   	action = component.get("c.getOrderItems");
                    action.setParams({ opportunityId : complexityMatrix.E3_Opportunity__c });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            var orderItems = response.getReturnValue();
                            orderItems.forEach(function(element) {
                            	console.log(element);
                            });
                        }
                    });
                    
                }
                else if (state === "INCOMPLETE") {
                    console.log('INCOMPLETE');
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            
			// enqueue action
        	$A.enqueueAction(action);
        } else {
            console.log('ERROR MATRIZ DE COMPLEJIDAD NO CARGADA');
        }
	},
    
    fireIndexEvent : function(component, event, helper) {
        var name = event.getSource().getLocalId();
        console.log('name:  ' + name)
        var appEvent = $A.get("e.c:E3_Matriz_Complejidad_indexEvent");
        console.log(appEvent);
        switch(name){
            case '1':
                console.log('case 1')
                appEvent.setParams(
                    {"header" : "Core"});
                	helper.fireIndexEvent2("comAvanz");
                break;
            case '2':
                console.log('case 2')
                appEvent.setParams(                    
                    {"header" : "Core"});
                	helper.fireIndexEvent2("datosFijosInternet");
                break;
            case '3':
                console.log('case 3')
                appEvent.setParams(                    
                    {"header" : "Core"});
                	helper.fireIndexEvent2("networking");
                break;
            case '4':
                console.log('case 4')
                appEvent.setParams(                    
                    {"header" : "Core"});
                	helper.fireIndexEvent2("datosMoviles");
                break;
            case '5':
                console.log('case 5')
                appEvent.setParams(                    
                    {"header" : "Core"});
                	helper.fireIndexEvent2("teminalesMov");
                break;
            case '6':
                console.log('case 6')
                appEvent.setParams(                    
                    {"header" : "Core"});
                	helper.fireIndexEvent2("tvVideo");
                break;
            case '7':
                console.log('case 7')
                appEvent.setParams(                    
                    {"header" : "Tradicional"});
                	helper.fireIndexEvent2("servTelefMovil");
                break;
            case '8':
                console.log('case 8')
                appEvent.setParams(
                    {"header" : "Tradicional"});
                	helper.fireIndexEvent2("sms");
                break;
            case '9':
                console.log('case 9')
                appEvent.setParams(                    
                    {"header" : "Tradicional"});
                	helper.fireIndexEvent2("otros");
                break;
            case '10':
                console.log('case 10')
                appEvent.setParams(                    
                    {"header" : "Digital"});
                	helper.fireIndexEvent2("cloud");
                break;
            case '11':
                console.log('case 11')
                appEvent.setParams(                    
                    {"header" : "Digital"});
                	helper.fireIndexEvent2("contenidoAplicaciones");
                break;
            case '12':
                console.log('case 12')
                appEvent.setParams(                    
                    {"header" : "Digital"});
                	helper.fireIndexEvent2("ehealth");
                break;
            case '13':
                console.log('case 13')
                appEvent.setParams(                    
                    {"header" : "Digital"});
                	helper.fireIndexEvent2("iot");
                break;
            case '14':
                console.log('case 14')
                appEvent.setParams(                    
                    {"header" : "Digital"});
                	helper.fireIndexEvent2("outsourcing");
                break;
            case '15':
                console.log('case 15')
                appEvent.setParams(                    
                    {"header" : "Digital"});
                	helper.fireIndexEvent2("puestoTrabajo");
                break;
            case '16':
                console.log('case 16')
                appEvent.setParams(
                    {"header" : "Digital"});
                	helper.fireIndexEvent2("seguridad");
                break;
            case '17':
                console.log('case 17')
                appEvent.setParams(                    
                    {"header" : "Digital"});
                	helper.fireIndexEvent2("servFinancieros");
                break;
            case '18':
                console.log('case 18')
                appEvent.setParams(                    
                    {"header" : "Digital"});
                	helper.fireIndexEvent2("ted");
                break;
            case '19':
                console.log('case 19')
                appEvent.setParams(                    
                    {"header" : "Digital"});
                	helper.fireIndexEvent2("tis");
        }
        console.log('appTab:' + appEvent.getParam("header"));
        appEvent.fire();
    }
})