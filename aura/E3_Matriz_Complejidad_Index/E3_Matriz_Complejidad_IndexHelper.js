({
	calculateProgressBar : function(checks) {
        var progress = 0;
        var factor = 100/checks.length;
        for(var i = 0; i < checks.length; i++){
            if (checks[i] == true) {
                progress+=factor;
            }
        }
        // Validate progress not greater than 100 (that shouldn't happen)
        progress = progress > 100?100:progress;
        return progress;
		
	},
    
    fireIndexEvent2 : function(name) {
        console.log('name: ' + name);
        var appEvent2 = $A.get("e.c:E3_Matriz_Complejidad_indexEvent2");
    	appEvent2.setParams({"subHeader" : name});
        appEvent2.fire();
	}
})