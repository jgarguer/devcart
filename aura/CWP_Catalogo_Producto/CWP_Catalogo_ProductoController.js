({
	doInit : function(component, event, helper) {
		component.set("v.alert", "");
		var toggleAlert=component.find("alertDiv");
		$A.util.addClass(toggleAlert, "slds-hide");
		component.set("v.product", event.getParam("product"));
		var product = component.get("v.product");
		console.log('producto: ' + product);
		var desc1Split = product.BI_Texto_descriptivo_P1__c.split(/\n/);
		component.set("v.desc1Split", desc1Split);
		var desc2Split = product.BI_Texto_descriptivo_P2__c.split(/\n/);
		component.set("v.desc2Split", desc2Split);
		helper.showComponent(component, event);
	},
	
	askMoreInfo : function(component, event, helper){
		var prodId = component.get("v.product").Id;
		var product = component.get("v.product");
		var action = component.get("c.createTask");
		action.setParams({
			"prodId" : component.get("v.product").Id,
			"productJSON" : JSON.stringify(product)
		});
		//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
                    
					component.set("v.alert", returnValue);
					var toggleAlert=component.find("alertDiv");
					$A.util.removeClass(toggleAlert, "slds-hide");
                    document.getElementById('alertDiv').style.display="block";
                    document.getElementById('backgroundmodal2').style.display="block";
				}
            }
        });
        $A.enqueueAction(action);
	},
    hideModal : function(component, event, helper){
		document.getElementById('backgroundmodal').style.display='none';
        document.getElementById('alertDiv').style.display='none';    
        document.getElementById('backgroundmodal2').style.display="none";
    }
})