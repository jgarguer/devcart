({
	doInit : function(cmp, event, helper) {
		var idFore = cmp.get("v.recordId");
        var action = cmp.get("c.actualizaFO");
        console.log("LLEGUE DENTRO");
        action.setParams({"idFore":idFore});
        action.setCallback(this,function(response){
            if(response.getState()==="SUCCESS"){
                var result=response.getReturnValue();
                if(result.indexOf("ERROR")!=-1){
                    alert($A.get("$Label.c.FO_Error")+result);
                }
            }else{
                var Errors = response.getError();
                alert($A.get("$Label.c.FO_Error")+Errors[0].message);
            }
        });
        $A.enqueueAction(action);
	}
})