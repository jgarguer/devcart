({
	createModal : function(cmp, modalresource, modalheader, bodymessage, contentStyle, showfooter) {
        console.log('createModal');
        console.log('createModal', modalresource);
        console.log('createModal', modalheader);
        console.log('createModal', bodymessage);
		$A.createComponent("c:TGS_RecyclableModal", {
                "header": modalheader,
                "resource": modalresource,
            	"message" : bodymessage,
            	"contentStyle" : contentStyle,
            	"footer": showfooter
            },
            function(modal, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = cmp.get("v.body");
                    body.push(modal);
                    cmp.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
	}
})