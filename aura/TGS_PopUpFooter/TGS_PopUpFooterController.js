({
	popUp : function(cmp, event, helper) {
        var userLang = cmp.get("v.userLang");
		var modalHeader = $A.getReference("$Label.c."+event.currentTarget.id);
        if(userLang!='pt_BR' && userLang!='es' && userLang != 'en_US'){
            userLang = 'en_US';
        }
        var modalResource = "/"+event.currentTarget.id+"/"+event.currentTarget.id+"-"+userLang+".html";
        
        helper.createModal(cmp, modalResource, modalHeader, null, 'min-height:80%;min-width:80%;background : #fafaf9;padding-left:10px;', false);
	},
    doInit : function (cmp, event, helper) {
        console.log(cmp.get("v.firstTime"));
        if(cmp.get("v.firstTime") === false){
			helper.createModal(cmp, null, "SEDA" , $A.get("$Label.c.TGS_welcomeSDN"), 'min-height:20%;min-width:50%;', true);            
        }
    }
})