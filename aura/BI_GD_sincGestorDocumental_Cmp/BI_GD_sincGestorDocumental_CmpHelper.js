({
    
    Notificate : function(component, event, helper, text, isError){
        /*Variable Definition*/
        debugger;
        var notification = component.get('v.notification');
        var alert_Container = component.find('alert_Container');
        var alert_Element = component.find('alert_Element');
        
        /*Type Setup*/
        if(isError) $A.util.addClass(alert_Element,'slds-theme--error');
        else $A.util.addClass(alert_Element,'slds-theme--success');
        
        /*Text Definition*/
        notification = text;
        component.set('v.notification',notification);
        
        $A.util.removeClass(alert_Container,'slds-hide');
    },
    WebService : function(component, event, helper){
        /*Variable Declaration*/
        debugger;
        var contractId = component.get('v.contractId');
        var action = component.get("c.sinc");
        action.setParams({contId:contractId});
        
        action.setCallback(this, function(response) {
            document.getElementById("roleAlertDialog").style.display="block";
            var returnValue = component.get('v.returnValue');
            var returnColor = component.get('v.returnColor');
            var modal = component.get('v.modal');
            modal = 'slds-modal__footer slds-theme--default';
            //Sincronización realizada correctamente
            returnValue = response.getReturnValue();
            console.log(returnValue);
            
            
            var contador = component.get('v.contador');
            contador = (returnValue.length/22 );
            
            
            var color = component.get('v.color') ;
            if(returnValue.includes('Sincronización realizada correctamente')){
                returnColor = 'slds-modal__header slds-theme--success slds-theme--alert-texture';
            }else{
                returnColor = 'slds-modal__header slds-theme--error slds-theme--alert-texture';
            }
            component.set('v.returnValue',returnValue);
            component.set('v.returnColor',returnColor);
            component.set('v.contador',contador);
            component.set('v.modal',modal);
            
            //alert(returnValue);
            //helper.redirection(component, event);
        });
        $A.enqueueAction(action);
    },
    
    redirection:function(component, event){
        var addressId = '/'+component.get("v.contractId");
        if( (typeof sforce != 'undefined') && (sforce != null) ) {
            sforce.one.navigateToURL(addressId);
        } else {
            window.location.assign(addressId);
        }
    },
    
})