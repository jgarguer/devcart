({
	getDataValue : function(cmp,accountId) {
		var action = cmp.get("c.getContactData");
		action.setParams({acctId:accountId});   
		console.log('getDataValue-->'+accountId);
		action.setCallback(this, function(response) {
			if (cmp.isValid() && response.getState() === "SUCCESS") {    
				var todos = JSON.parse(response.getReturnValue()); 
				var contactos = [];
				var oportunidades = [];
				var indexcontact = 0;
				var indexoportunity = 0;
				for (var i = 0; i < todos.length; i++) { 
					if (todos[i].tipo === "Contacto"){
						contactos[indexcontact] = todos[i];
						indexcontact++;
					} else if (todos[i].tipo === "Oportunidad") {
						oportunidades[indexoportunity] = todos[i];
						indexoportunity++;
					}
				}
				cmp.find("listaContacto").set("v.options", contactos);
				cmp.find("listaOportunidad").set("v.options", oportunidades);
				cmp.find("bInicial").set("v.value", true);
			}
		});
		$A.enqueueAction(action);
		
	},
	getAuxData : function(cmp) {
		var action = cmp.get("c.getContactData");
		action.setParams({acctId:cmp.get('v.evento').WhatId});     
		action.setCallback(this, function(response) {
			if (cmp.isValid() && response.getState() === "SUCCESS") {    
				var todos = JSON.parse(response.getReturnValue()); 
				var contactos = [];
				var oportunidades = [];
				var indexcontact = 0;
				var indexoportunity = 0;
				for (var i = 0; i < todos.length; i++) { 
					if (todos[i].tipo === "Contacto"){
						contactos[indexcontact] = todos[i];
						indexcontact++;
					}
					if (todos[i].tipo === "Oportunidad"){
						oportunidades[indexoportunity] = todos[i];
						indexoportunity++;
					}
				}
				cmp.find("listaContacto").set("v.options", contactos);
				cmp.find("listaOportunidad").set("v.options", oportunidades);
			}
		});
		
		$A.enqueueAction(action);
		
	},



	getEventData : function(cmp, eventId) {
		console.log("KQC! GetEventDataStart!");
		//console.log('getEventData');	
		var action = cmp.get("c.getEventData");  
		action.setParams({eventId:eventId});     
		action.setCallback(this, function(response) {
			if (cmp.isValid() && response.getState() === "SUCCESS") {   
				var eventHelper = response.getReturnValue();                                    
				console.log('eventHelper-->'+eventHelper);
				var eventHelperJson=JSON.parse(eventHelper);
				console.log('eventHelperJson-->'+eventHelperJson);
				
				var contact = eventHelperJson.c;
				debugger;
				if (contact){
					var auxData = cmp.get('v.auxData');
					var opts = [{ class: "optionClass", label: contact[0].Name, value: contact[0].Id, selected: "true" } ];
					 
					cmp.find("listaContacto").set("v.options", opts);
					auxData.ContactId=contact.value;
					auxData.BI_G4C_Oportunidad__c=eventHelperJson.o.Name;
					console.log('AUX DATA OPO-'+auxData.BI_G4C_Oportunidad__c);
					auxData.BI_G4C_IdOportunidad__c=eventHelperJson.o.Id;
					
					var Eevent=eventHelperJson.e;
					console.log('Eeventhelper'+Eevent.ActivityDateTime);
					if(Eevent.BI_FVI_Estado__c === "Iniciada"){
					  
						var action = cmp.get("c.getContactData");
						console.log('event Account-ID-'+Eevent.WhatId);
						action.setParams({acctId:Eevent.WhatId});  
						//----
							action.setCallback(this, function(response) {
								if (cmp.isValid() && response.getState() === "SUCCESS") {    
									var todos = JSON.parse(response.getReturnValue()); 
									var oportunidades = [];
									var indexoportunity = 0;
									for (var i = 0; i < todos.length; i++) { 
										if (todos[i].tipo === "Oportunidad"){
											var opote=todos[i];
											oportunidades[indexoportunity] = todos[i];
											auxData.BI_G4C_Oportunidad__c=oportunidades[indexoportunity] ;
											console.log('AUXDATA NEW OPO-'+auxData.BI_G4C_Oportunidad__c);
											indexoportunity++;
										   //opts.push({ class: "optionClass", label: opote, value:opote});
										}
									}
									//cmp.find("listaOportunidad").set("v.options", opts);
									console.log('listaoportunidad--->'+oportunidades);
									cmp.find("listaOportunidad").set("v.options", oportunidades);
								}
							});
							
							$A.enqueueAction(action);
					  
					}else{
						opts = [{ class: "optionClass", label: auxData.BI_G4C_Oportunidad__c, value: auxData.BI_G4C_IdOportunidad__c, selected: "true" }];
				   		 cmp.find("listaOportunidad").set("v.options", opts);
					}
	
					auxData.ContactName=contact[0].Name;
					auxData.ContactPhone=contact[0].Phone;
					auxData.ContactMail=contact[0].Email;
					cmp.set('v.auxData', auxData);
				} 
				console.log("KQC! Llegando al Map!");
			   debugger;
			  
				var map = {"1":"ene","2":"feb","3":"mar","4":"abr","5":"may","6":"jun","7":"jul","8":"ago","9":"sept","10":"oct","11":"nov","12":"dic"}
				//var map = {"1":"1","2":"2","3":"3","4":"4","5":"5","6":"6","7":"7","8":"8","9":"9","10":"10","11":"11","12":"12"}
				console.log("KQC! LOCALE : " + $A.get("$Locale.language"));
				var evento = eventHelperJson.e;

				console.log('evento var-->'+evento.ActivityDateTime);
			  	var d=new Date(evento.ActivityDateTime);
                
				console.log('idinterno-->'+evento.BI_Identificador_Interno__c);
				console.log('fecha del event a mostrar-->'+d);
				var hors=d.getHours()>9?d.getHours():'0'+d.getHours();
				console.log('hora de la fecha-->'+hors);
				var mins=d.getMinutes()>9?d.getMinutes():'0'+d.getMinutes();
				console.log('minuto de la fecha-->'+mins);
				
				console.log('Mes from map-->'+map[parseInt(d.getMonth())+1]);
				//'0' + MyDate.getDate()).slice(-2)
				console.log('fecha-->'+('0'+d.getDate()).slice(-2)+'-'+(parseInt(d.getMonth())+1)+'-'+d.getFullYear()+"T" + hors+':'+mins);
                console.log('getEventData::dia/mes/año de la fecha-->'+d.getDate()+'-'+(parseInt(d.getMonth())+1)+'-'+d.getFullYear());
                console.log('getEventData::'+map[parseInt(d.getMonth())+1]);
                console.log('getEventData::hora de la fecha-->'+hors+':'+mins);
				cmp.set('v.evHora', hors+':'+mins);
                cmp.set('v.evFecha',('0'+d.getDate()).slice(-2)+'-'+map[parseInt(d.getMonth())+1]+'-'+d.getFullYear()+"T" + hors+':'+mins);
				//cmp.set('v.evFecha',d.getDate()+'-'+map[parseInt(d.getMonth())+1]+'-'+d.getFullYear()+"T" + hors+':'+mins);
			 
				console.log('ESTADO-->'+evento.BI_FVI_Estado__c);
				console.log('¿Emailed?'+evento.BI_Correo_electronico_enviado__c);
				if(evento.BI_Correo_electronico_enviado__c){
					cmp.set('v.isEnviado', "true");  
				}
				if(evento.BI_FVI_Estado__c==="Cualificada"){
						 cmp.set('v.isCualif', "true");
						 console.log('RESULTADO VISITA-->'+evento.BI_G4C_ResultadoVisita__c );
						 console.log('RESULTADO-->'+evento.BI_FVI_Resultado__c);
						
						 cmp.set('v.notasReunion', evento.BI_FVI_Resultado__c); 
						 cmp.set('v.resultadoReunion', evento.BI_G4C_ResultadoVisita__c); 
						
				 }

				if (evento.IsReminderSet===true){	
					/* 
					var s = evento.ActivityDateTime;
					var a = s.split(/[^0-9]/);					
					var d1=new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
					s = evento.ReminderDateTime;
					a = s.split(/[^0-9]/);
					var d2=new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
					var minis = d1-d2;		            
					minis /= 60000;		            		           
					cmp.set('v.auxData.avisar',true);
					cmp.set('v.auxData.avisoEnMins',minis);*/
				}	
				/*
				console.log('oportunidad seleccionada-->'+cmp.find("listaOportunidad").get("v.value"));
				evento.BI_G4C_Oportunidad__c = cmp.find("listaOportunidad").get("v.value");
				console.log('nueva oportunidad del evento-->'+evento.BI_G4C_Oportunidad__c);
				*/
				cmp.set('v.evento', evento);              
			}
			else{            	
				//alert('error');
				//console.log(response);
			}
		});
		$A.enqueueAction(action);
	},
	existeFecha: function(fecha){
		
		var result=false;
		var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
		if(fecha!== undefined){
			if ((fecha.match(RegExPattern)) && (fecha!='')) {
				var fechaf = fecha.split("/");
				var day = fechaf[0];
				var month = fechaf[1];
				var year = fechaf[2];
				var date = new Date(year,month,'0');
				if((day-0)>(date.getDate()-0)){
					result=false;
				}
				result=true;
			}
		}
		return result;
		
		return true;
	},
	
	compruebaFecha: function(fecha){
	   console.log("KQC! Comprobando fecha!"); 
		var result=false;
		var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
		if(fecha!== undefined){
			if ((fecha.match(RegExPattern)) && (fecha!='')) {
				var fechaf = fecha.split("/");
				var d = fechaf[0];
				var m = fechaf[1];
				var y = fechaf[2];
				result= m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= (new Date(y, m, 0)).getDate();
			}
		}
		console.log("KQC! Result: " + result);
		return result;
	},
	compruebaHora: function(hora){
		
		var result=false;
		var RegExPattern = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
		if(hora!==undefined){
			
			if ((hora.match(RegExPattern)) && (hora!='')) {
				
				var horaf = hora.split(":");
				var hh = horaf[0];
				var mm = horaf[1];
				result= hh >= 0 && hh < 24 && mm >= 0 && mm < 60;
			}
		}
		return result
	},
	
	fieldValidation: function(cmp, evt, helper){
		
		var resul = true;
		var evento = cmp.get('v.evento'); 
		var auxData = cmp.get('v.auxData');
		var evFecha=cmp.get('v.evFecha');
		var evHora=cmp.get('v.evHora');      
		console.log('Field Validation-->FECHA EN EL HELPER-->'+evFecha);
          
		if(evFecha!=null){
			var fechaHora = evFecha.split("T");
			var fechaHora2 = evFecha.split(":");
			console.log("KQCwip -- laFecha " + fechaHora);
			console.log("KQCwip -- laFecha2 " + fechaHora2);
			if(!(fechaHora.length == 2) && !(fechaHora2.length == 2)){
			  var fvisitamsg = cmp.find('fvisitamsg');
			  $A.util.removeClass(fvisitamsg, 'slds-hide');
			  resul = false;
			}
		}
       
		if(resul===true){
			evento.ActivityDateTime= evFecha;            
		}
		 console.log('KQCwip:::'+evFecha);
		if (evento.DurationInMinutes === undefined ||evento.DurationInMinutes === null || evento.DurationInMinutes.length === 0){
			var durationmsg = cmp.find('durationmsg');
			$A.util.removeClass(durationmsg, 'slds-hide');
			resul = false;
		}
		
		if (evento.Subject === undefined  || evento.Subject.length === 0){
			evento.Subject="Visita a Cliente";
			/*  var asuntomsg = cmp.find('asuntomsg');
			$A.util.removeClass(asuntomsg, 'slds-hide');
			resul = false;*/
		}

		/* D460 Agrega texto para evitar validación BI_ECU_Obligatoriedad_Descripcion de Evento */
		if ((evento.Description === undefined  || evento.Description.length === 0) && auxData.Profile == 'BI_Standard_ECU' ){
			evento.Description="Descripción del evento";
		}
		
		
		/* if (auxData.avisar === true && (auxData.avisoEnMins ===undefined  || auxData.avisoEnMins.length === 0)){
			var avisomsg = cmp.find('avisomsg');
			$A.util.removeClass(avisomsg, 'slds-hide');
			resul = false;
		}*/
		if (cmp.find("listaOportunidad").get("v.value") === 'Este cliente no tiene oportunidades'){
			var descriptionmsg = cmp.find('opportunitymsg');
			$A.util.removeClass(descriptionmsg, 'slds-hide');
			resul = false;
		}
		
		if (cmp.find("listaContacto").get("v.value") === 'Este cliente no tiene contactos'){
			descriptionmsg = cmp.find('contactmsg');
			$A.util.removeClass(descriptionmsg, 'slds-hide');
			resul = false;
		}
		
		//JEG 
		if ((cmp.find("lookUser") !== undefined) && ("{}" == JSON.stringify(cmp.find("lookUser").get("v.selectedRecord")))){
		   var descriptionmsg = cmp.find('asignadomsg');
			$A.util.removeClass(descriptionmsg, 'slds-hide'); 
			resul = false; 
		}
		
		return resul;
	},
	removeAllErrors: function(cmp){
		var fvisitamsg = cmp.find('fvisitamsg');
		$A.util.addClass(fvisitamsg, 'slds-hide');
		var hvisitamsg = cmp.find('hvisitamsg');
		$A.util.addClass(hvisitamsg, 'slds-hide');
		var durationmsg = cmp.find('durationmsg');
		$A.util.addClass(durationmsg, 'slds-hide');
		var asuntomsg = cmp.find('asuntomsg');
		$A.util.addClass(asuntomsg, 'slds-hide');
		var descriptionmsg = cmp.find('descriptionmsg');
		$A.util.addClass(descriptionmsg, 'slds-hide');
		var avisomsg = cmp.find('avisomsg');
		$A.util.addClass(avisomsg, 'slds-hide');
	   var asignadomsg = cmp.find('asignadomsg');
		$A.util.addClass(asignadomsg, 'slds-hide');
	},
	showCualificar: function(cmp, evt, helper){
		debugger;
		helper.removeAllErrors(cmp);
		var pasaValidations = helper.fieldValidation(cmp, evt, helper);
		if (pasaValidations){
			var x = function(a){
				//console.log(a);
				//by JB
			}
			
			$A.createComponent(
				"c:BI_G4C_Visitas_Cualificar",
				{
					evento: cmp.get('v.evento'),
					auxData: cmp.get('v.auxData')
				},          
				function(newCmp,status){
					var modalfilter = cmp.find('BI_G4C_Visitas_Cualificar_id');
					$A.util.removeClass(modalfilter,'slds-hide');
					$A.util.removeClass(modalfilter,'.slds-modal--large');
					var body = cmp.get("v.body");
					body.push(newCmp);
					cmp.set("v.body", body);
					cmp.set("v.clicked", false); //JEG
				}
			);
			
		}
	},
	updateVisita: function(cmp, evt, helper){
		var evento = cmp.get('v.evento');
        console.log('updateVisita:::EVENTO'+evento.ActivityDateTime);
        //var pp=(evento.ActivityDateTime).toString();
        //console.log('fecha PARSEADA'+$A.localizationService.formatDate());
		//console.log('fecha PARSEADA'+pp);
		//System.debug(DateTime.valueOfGmt(('06/08/2013 06:30:22').replaceAll('/','-')));
		//console.log(DateTime.valueOfGmt((evento.ActivityDateTime).replaceAll('/','-')));
		
        /*
		var d=evento.ActivityDateTime;
        console.log('updateVisita:::VAR d'+d);
		var ms=parseInt(d.getMonth(),10);
		//ms+=1;
        console.log('updateVisita:::año'+d.getFullYear());
        console.log('updateVisita:::mes'+ms);
        console.log('updateVisita:::dia'+d.getDate());
		*/        
		//evento.ActivityDateTime=d.getFullYear()+'-'+ms+'-'+d.getDate()+'T'+d.getHours()+':'+d.getMinutes();
        
        evento.ActivityDateTime=cmp.get('v.evFecha');
        
        
        console.log('updateVisita:::'+evento.ActivityDateTime);
		var auxData = cmp.get('v.auxData');
		var action = cmp.get("c.updateEvent");
		var fecha=cmp.get('v.evFecha');
		evento.BI_G4C_Oportunidad__c = cmp.find("listaOportunidad").get("v.value");
		action.setParams({e: JSON.stringify(evento), a: JSON.stringify(auxData),'fecha':fecha});
		action.setCallback(this, function(response) {
		   
			if (cmp.isValid() && response.getState() === "SUCCESS") {           
				var result = response.getReturnValue();
				/*Sub-Evento*/
				var ae = $A.get("e.c:BI_G4C_app_evt");
				ae.setParams({type:"refresh"});
				ae.fire();
				
				if (result === 'OK') {
					
				}
				else if (result === 'QA') helper.showCualificar(cmp, evt, helper);
			}else{
				//Mensaje de error "No se ha podido actualizar el evento"
				//alert('ERROR updateVisita'+response.getState());
			}
		});
		$A.enqueueAction(action);
	},
	geolocActionFiring:function(cmp,helper,geo){
		console.log('geolocActionFiring  geoloc-->'+geo);
		var evento = cmp.get('v.evento');
		 var action = cmp.get("c.geoloc");
		console.log('evento->'+evento.Id);
		console.log('ACTION'+action);
		action.setParams({'eventId':evento.Id ,
							  'geoL': geo});
			action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
				console.log('GEOLOC SUCCESS');
			}
			else{  
				console.log('geoloc FAILED');
			}
		});
		$A.enqueueAction(action);
	},

	moveVisita : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaHelper.js ## moveVisita');
		var evento = cmp.get('v.evento');
        console.log(evento);
		var owner = cmp.get('v.selectedLookupRecord');
		var ownerId = owner.Id;
		
		if(!evento.BI_FVI_Estado__c) {
			evento.BI_FVI_Estado__c = 'Nueva';
		}else if(evento.BI_FVI_Estado__c === 'Planificada'){
			evento.BI_FVI_Estado__c = 'Iniciada';   
		}else if (evento.BI_FVI_Estado__c ==='Iniciada'){
			evento.BI_FVI_Estado__c = 'Finalizada';
		}else if (evento.BI_FVI_Estado__c ==='Finalizada'){
			evento.BI_FVI_Estado__c = 'LLamaCualificar';
		}else if(evento.BI_FVI_Estado__c ==='Cualificada'){
			console.log('evento cualificado');
		}

		console.log('KQC! Event.BI_FVI_Estado__c --> ' + evento.BI_FVI_Estado__c);
		
		console.log('Antes AuxData');
		var auxData = cmp.get('v.auxData');
		console.log('Antes UpdateEvent');
		var action = cmp.get("c.updateEvent");
		console.log('Antes evFecha');
		var ff = cmp.get('v.evFecha');
		console.log('moveVisita--fecha seleccionada-->'+ff);
        
		evento.BI_G4C_Oportunidad__c = cmp.find("listaOportunidad").get("v.value");
		console.log('oportunidades-->'+evento.BI_G4C_Oportunidad__c);
		action.setParams({
			e : JSON.stringify(evento),
			a : JSON.stringify(auxData),
			owner : ownerId
		});
		action.setCallback(this, function(response) {
			if (cmp.isValid() && response.getState() === "SUCCESS") {           
				var result = response.getReturnValue();
				/*Sub-Evento*/
				if (result === 'QA') {
					helper.showCualificar(cmp, evt, helper);
				}else{
					var ae = $A.get("e.c:BI_G4C_app_evt");
					ae.setParams({type:"refresh"});
					ae.fire();
				}
			}else{
				//Mensaje de error "No se ha podido actualizar el evento"
				//alert('ERROR '+response.getState());
			}
			
		});
		
		$A.enqueueAction(action);
		
		cmp.set("v.evento", evento);
	},
	getUser : function(cmp, evt) {
		console.log('GetUser Function');
		var action = cmp.get("c.getCurrentUser");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				cmp.set("v.selectedLookupRecord", response.getReturnValue());
			}
		});
		console.log("Antes del Action en getUser");
		$A.enqueueAction(action);
		console.log("Despues del Action en getUser");
	},
	updateEmailSend : function(cmp,evento){
		 console.log('updateEmailSend');
		 var action = cmp.get("c.updatesend");
		 action.setParams({'eid':evento});
		 console.log('evento-->'+evento);
		 action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
			  console.log('Evento updated correctly');
			}else{
				console.log('cant update event');
			}
		});
		$A.enqueueAction(action);        
	}
})