/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Miguel Molina Cruz
Company:       everis
Description:   JS Controller for the Lightning Component BI_G4C_Visitas_Nueva

History:


<Date>                          <Author>                    <Code>					<Change Description>
20/06/2016                      Miguel Molina Cruz          -						Initial version
26/07/2016                      Miguel Molina Cruz          M001					Update contact email and phone for states after "Planificada" in the doInit method
25/05/2017                      Gawron, Julián                                      Adding getCurrentUserData
13/12/2017						Antonio Mendivil Azagra								Added prevPDF,enviarPDF
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
({
	checkStatus : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## checkStatus');
		var evento = cmp.get('v.evento');
		if(evento.BI_FVI_Estado__c==="Cualificada"){
			cmp.set('v.isCualif', "false");  
		}
		if(evento.BI_Correo_electronico_enviado__c){
			console.log('¿enviado?-->'+evento.BI_Correo_electronico_enviado__c);
			cmp.set('v.isEnviado', "true");
		}
	},
	prevPDF : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## prevPDF');
		//añadir validacion en funcion del segmento
		var evento = cmp.get('v.evento');
		var url = '/apex/BI_MinutaReunion_PDF?id=' + evento.Id;
		window.open(url);
	},
	enviarPDF : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## enviarPDF');
		var evento = cmp.get('v.evento');
		var eventID=cmp.get('v.evento').Id;
		var contacto=cmp.find("listaContacto").get('v.value');
		var action = cmp.get("c.sendPdf");
		console.log('eventid-->'+eventID+' contacto-->'+contacto);
		action.setParams({
			'visitaID': eventID,
			'cid': contacto
		});
		action.setCallback(this,(response) => {
			if (response.getState() === "SUCCESS") {
				alert("Email enviado correctamente");
				helper.updateEmailSend(cmp,eventID);
			} else {
				alert('Error enviando el email.');
			}
		});
		$A.enqueueAction(action);
	},
        
	doInit : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## doInit');
		var evento = cmp.get('v.evento');
		var auxData1 = cmp.get('v.auxData');
		console.log('controller doInit-->' + evento.Id);
		if(evento.Id != null)
			cmp.set('v.isNew','false');
		var creacionVisita = true;
		if (evento.Id === undefined) {
			console.log('KQC! true');
			helper.getAuxData(cmp);
			var action = cmp.get("c.datosCuenta");
			action.setParams({
				acctId:cmp.get('v.evento').WhatId
			});
			action.setCallback(this,(response) => {
				if (cmp.isValid() && response.getState() === "SUCCESS") {
					if (creacionVisita) {
						var auxData = cmp.get('v.auxData');
						var res = JSON.parse(response.getReturnValue());
						auxData.ContactId				= res[0].value;
						auxData.ContactName				= res[0].label;
						auxData.ContactPhone			= res[0].phone;
						auxData.ContactMail				= res[0].email;
						auxData.BI_G4C_Oportunidad__c	= res[0].OpName;
						cmp.set('v.auxData', auxData);
					}
				}
			});
			$A.enqueueAction(action);
		} else {
			creacionVisita = false;
			helper.getEventData(cmp, evento.Id);
		}
		 //JEG
		//Adding profile information to auxData
		debugger;
		if (auxData1.Profile === undefined) {
			var action = cmp.get("c.getCurrentUserData");
			action.setCallback(this,(response) => {
				if (cmp.isValid() && response.getState() === "SUCCESS") {
					var res = JSON.parse(response.getReturnValue());
					console.log("Profile Name: " + res.Profile.Name);
					var auxData = cmp.get('v.auxData');
					console.log("AuxData : " + auxData);
					auxData.Profile=res.Profile.Name;
					cmp.set('v.auxData', auxData);
					console.log("AuxData Profile : " + auxData.Profile);
				}
			});
			console.log("Antes de la action.");
			$A.enqueueAction(action);
			console.log("Despues de la action.");
		}
		console.log("## Antes del getUser ##");
        console.log("evt-->"+evt.ActivityDateTime);
		helper.getUser(cmp, evt);
	},
	
	emailphone : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## emailphone');
		var action = cmp.get("c.datosContacto");
		var aux = cmp.find("listaContacto").get('v.value');
		action.setParams({contactId:aux});
		//console.log(aux);
		//by JB 29/07
		action.setCallback(this,(response) => {
			if (cmp.isValid() && response.getState() === "SUCCESS") {
				var auxData = cmp.get('v.auxData');
				var res = JSON.parse(response.getReturnValue());
				auxData.ContactId				= res[0].value;
				auxData.ContactName				= res[0].label;
				auxData.ContactPhone			= res[0].phone;
				auxData.ContactMail				= res[0].email;
				auxData.BI_G4C_Oportunidad__c	= res[0].OpName;
				cmp.set('v.auxData',auxData);
				
			}
		});
		$A.enqueueAction(action);
	},
	
	cargaOpo : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## cargarOpo');
		var auxData = cmp.get('v.auxData');
		var selectCmp = cmp.find('listaOportunidad');
		auxData.BI_G4C_Oportunidad__c = selectCmp.get("v.value");
		console.log('cargaOpo::' + auxData.BI_G4C_Oportunidad__c);
		cmp.set('v.auxData',auxData);
	},
	test : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## test');
		$A.get("e.c:G4C_app_evt").fire();
	},
	avanzarVisita : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## avanzarVisita');
		var evFecha = cmp.get('v.evFecha');
		console.log(evFecha);
		var isClicked = cmp.get('v.clicked'); //JEG
		console.log('isClicked avanzarVisita' + isClicked);
		helper.removeAllErrors(cmp);
		if (helper.fieldValidation(cmp, evt, helper) === true && !isClicked) {
			cmp.set("v.clicked", true);
			console.log('avanzarVisita');
			helper.moveVisita(cmp,evt,helper); 
		}
		var evento = cmp.get('v.evento');
		if (evento.BI_FVI_Estado__c === 'Iniciada') {
			console.log('EVENTO.-->'+evento);
			console.log('Estado=iniciada--setgeoloc starting');
			navigator.geolocation.getCurrentPosition((position) => {
				var lat = position.coords.latitude;
				var lon = position.coords.longitude;
				//console.log(lat+' '+lon);
				//by JB 29/07
				helper.geolocActionFiring(cmp, evento, lat+' '+lon);
			});  
		}
	   
	},
	editarVisita : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## editarVisita');
		debugger;
		var evento = cmp.get("v.evento");
		if (evento.BI_FVI_Estado__c === 'Planificada') {
			helper.removeAllErrors(cmp);
			if (helper.fieldValidation(cmp,evt,helper) === true)
				helper.updateVisita(cmp,evt,helper);
		} else {
			var ladire = window.location.href;
			var esModif = ladire.includes("BI_ModifyEvent");
			if (esModif)
				window.history.back();
			var ae = $A.get("e.c:BI_G4C_app_evt");
			ae.setParams({type:"refresh"});
			ae.fire();
		} 
	},
	handleEvent : function(cmp,evt,helper){
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## handleEvent');
		var type = evt.getParam('type');
		
		if (type==='finalizada') {
			var device = $A.get("$Browser.formFactor");
			if (device === 'DESKTOP') {
				window.setTimeout($A.getCallback(() => {
						$A.get('e.force:hidePanel').fire();
					}), 500
				);
			} else {
				$A.get('e.force:hidePanel').fire();
			}
		}
	},
	
	closeError : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## closeError');
		var element = cmp.find('generic_error');
		$A.util.addClass(element,'slds-hide');
	},
	
	visitaCualificada : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## visitaCualificada');
		var evento = evt.getParam("event");
		cmp.set('v.evento', evento);
	},
	hideModalnewVisitas : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## hideModalnewVisitas');
		cmp.set("v.body",'');
		var confirm = cmp.find('BI_G4C_Visitas_Cualificar_id');
		$A.util.addClass(confirm, 'slds-hide');
	},
	changeFecha : function(cmp,evt,helper) {
		console.log('KQC! BI_G4C_Visitas_NuevaController.js ## changeFecha' + cmp.get('v.evFecha'));
		console.log('Change');
	},
})