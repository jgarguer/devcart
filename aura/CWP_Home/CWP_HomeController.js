({
    doInit : function (component, event, helper) { 
     var compEvent =  $A.get("e.c:CWP_menu_Evt")
        compEvent.setParams({"tabSelected" : "pca_home" });
        compEvent.fire();    
        helper.getUser(component, event, helper);
        //helper.getAccounts(component, event, helper);
        helper.getMapVisibility(component, event);
        helper.getShowServTandMyS(component, event, helper);
    },
    
    getCarouselImage : function(component, event,helper){
        var tabSelected= event.getParam('tabSelected');
        if(tabSelected=== 'pca_home'){
            var imageList = component.get('c.getImageList');
            
            imageList.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    
                    var imageWrapList = response.getReturnValue();
                    component.set("v.imageList", imageWrapList); 
                    setTimeout( function(){                    
                     var paintedList = document.getElementsByClassName("tlf-slider__item item active");
                        for(var i = 1; i<paintedList.length; i++){
                            paintedList[i].className = "tlf-slider__item item";
                        }
                        component.set("v.showImages", imageWrapList.length>0);                                              
                    }, 1);
                    
                }
            });
            $A.enqueueAction(imageList);
            
            var comp = component.find('CWP_Home_div');
            $A.util.removeClass(comp, 'slds-hide');
            
        }else {
            var comp = component.find('CWP_Home_div');
            $A.util.addClass(comp, 'slds-hide');
        }
        
        
        
    },
    
    displayComponent : function (component, event, helper) {
        /* onCLick Element Data-Id */
        var divs = component.find("CWP_Container").getElement().childNodes;
        var auraId = event.target.getAttribute('data-id');
        
        /* Hide all */
        for(var item in divs){
            $A.util.addClass(divs[item], "slds-hide");
        }
        
        /* Display Selected Item */
        var element = component.find(auraId+'_Component');
        $A.util.removeClass(element, "slds-hide");
        
        /* Setup Selector */
        var desktop = component.find('Desktop_'+auraId+'_Selector');
        var device = component.find('Device_'+auraId+'_Selector');
        $A.util.addClass(desktop, "selected");
        $A.util.addClass(device, "active");
    },
    
    redirectChatter : function (component, event, helper) {
        helper.gotoURL(component, event, helper);
    },
    
      changeTab : function(cmp, event, helper) {
        event.preventDefault();
        var a=event.currentTarget.id;
        var mapURLs={};
        
		    mapURLs['pca_rManagementL'] = 'cwp-relationship-management';        
        mapURLs['pca_sTrackingL'] = 'cwp-service-tracking';
        mapURLs['pca_myservice'] = 'cwp-my-services';
        mapURLs['pca_ideascorner'] = 'cwp-ideas-corner';
        mapURLs['pca_profile']='profile';
        
        if(a=="pca_profile"){
        
        	var action = cmp.get('c.getUserInfo');
        	action.setCallback(this, function(response) {
	            if (response.getState() === "SUCCESS") {
	                var returnValue = response.getReturnValue();
	                if(returnValue != null){
	                    cmp.set('v.user', returnValue);
	                    var user=cmp.get('v.user');
	                    var urlEvent = $A.get("e.force:navigateToURL");
	                    urlEvent.setParams({
	                    	"url": "/"+ mapURLs[a] + "/"+ user.Id
	                    });
	                    urlEvent.fire();	                    
	                    
	                }
	            }
        	});
        	$A.enqueueAction(action);
        
        }else{
	        var urlEvent = $A.get("e.force:navigateToURL");
	        urlEvent.setParams({
	            "url": "/"+ mapURLs[a]
	        });
	        urlEvent.fire();
        }
        
    },
    changePage : function(cmp, event, helper){
        var pageEvt = $A.get("e.c:CWP_AC_RedirectEvent");
        pageEvt.setParams({"page2Redirect": event.currentTarget.id});
        pageEvt.fire();
    },
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner,'slds-hide');
     },
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner,'slds-hide');
    },
    
    
})