({
    spinner : function(component, show){
        /* Variable Definition */
        var component = component.find('spinner');
        
        if(show) $A.util.removeClass(component,'slds-hide');
        else $A.util.addClass(component,'slds-hide');
        
    },
        
    getUser : function(component, event, helper){
        var action = component.get('c.getUserInfo');
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){
                   
                    
                    //component.set('v.esTGS', returnValue.Profile.Name.includes("TGS"));
                    component.set('v.esTGS', returnValue.Profile.Name.indexOf("TGS")>-1);
                    component.set('v.user', returnValue);
                    
                }
            }
        });
        $A.enqueueAction(action);
    },
    getAccounts : function(component, event, helper){
        /* Attibute Initialization */
        component.set('v.accounts', []);
        
        /* Execute Apex Method */
        var action = component.get("c.getUserAccounts");
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){
                    component.set('v.accounts', returnValue);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getRedirect: function(component, event) {
        var action = component.get("c.redirectToChatter");
        action.setCallback(this, function(response) {
            var state = response.getState();
            var returnValue = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.redirectChatter", returnValue);
               }
		});
        $A.enqueueAction(action);
    },
    
    getShowServTandMyS: function(component, event, helper) {       
        var action = component.get("c.getShowServTandMyS");
        action.setCallback(this, function(response) {
             if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.showServTandMyS",returnValue);
        	}   
        });
        $A.enqueueAction(action);
    },
    
    getMapVisibility: function(component,event){
    	var action = component.get("c.getPermissionSet");
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                if(returnValue != null){
                    component.set('v.mapVisibility', returnValue);
                }
            }
        });
        $A.enqueueAction(action);
    
    
    },
    gotoURL : function (component, event) {
        event.stopPropagation();
        var url = $A.get("$Label.c.CWP_Domain");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url+"/_ui/core/chatter/ui/ChatterPage?noredirect=1",
          "isredirect": "false"
        });
        urlEvent.fire();
    }
    
})