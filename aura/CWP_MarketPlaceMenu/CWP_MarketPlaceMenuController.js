({
	myAction : function(component, event, helper) {
		
	},
	
	setMainImage : function(component, event){
            var imgUrl;
            var mainInfo = component.get('c.getMainImage');
           /* component.set("v.informationMenu", '{!$label.c.PCA_My_Service_menu}');
            component.set("v.title", '{!$label.c.PCA_MyServiceText}');*/
        	var urlDomain = component.get("v.urlimg");
            mainInfo.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var mainInfo = response.getReturnValue();
                    imgUrl= "url('"+urlDomain+"/servlet/servlet.FileDownload?file=" + mainInfo['imgId']+ "')";
                    setTimeout(
                        function(){
                            if(mainInfo['imgId'] != undefined){
                                document.getElementById('mainImage').style.backgroundImage =  imgUrl;
                                document.getElementById('mainImage').style.padding="2.5rem 4.0625rem";
                                document.getElementById('mainImage').style.minHeight="21.875rem";
                                document.getElementById('mainImage').style.height="350px";
                                document.getElementById('mainImage').style.backgroundSize="1024px 350px";
                                document.getElementById('mainImage').style.backgroundRepeat="no-repeat";
                                document.getElementById('mainImage').style.backgroundPosition="center bottom"; 
                            }else{
                                document.getElementById('mainImage').className = "tlf-slide";
                                
                            }
                            if(mainInfo['informationMenu'] != undefined){
                                component.set("v.informationMenu", mainInfo['informationMenu']);
                            }
                            if(mainInfo['title'] != undefined){
                                component.set("v.title", mainInfo['title']);
                            }
                    }, 1);
                }else{
                    alert("fallo!");
                }
            });
            $A.enqueueAction(mainInfo);
        
    },
    
    goToCatalog : function(component, event, helper) {
    	var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/cwp-market-place"
        });
        urlEvent.fire();
    },
    
    changeTab: function(component, event, helper) {
        event.preventDefault();
        var a = event.currentTarget.id;
        var mapURLs = {};
        mapURLs['inventory'] = 'cwp-market-place';
        mapURLs['documentation'] = 'cwp-offers';
        mapURLs['billing'] = 'cwp-ordenescompra';
      
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/" + mapURLs[a]
        });
        urlEvent.fire();
    },
})