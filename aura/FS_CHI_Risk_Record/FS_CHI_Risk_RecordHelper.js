({
    setMessage: function(component, event, helper) {
        /*Variable Definition*/
        var clientId = component.get("v.clientId");
        
        /*Process Lightning Action*/
        var action = component.get("c.consulta");
        action.setParams({
            "IdAccount": clientId
        });
        
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            var message = component.find("message_container");
            var title = component.find('title_container');
            var spinner = component.find('spinner_id');
            var prompt = component.find('prompt_container');
            
            if(returnValue != null){             
                if(returnValue == 'success'){     
                    /*Success*/
                    $A.util.removeClass(title,'slds-theme--info');
                    $A.util.addClass(title,'slds-theme--success');
                    
                    component.set('v.title_message', 'Proceso completado');
                    component.set('v.content_message', 'La información de riesgo ha sido recuperada correctamente para el cliente actual.');
                    
                } else {
                    /*Error*/
                    $A.util.removeClass(title,'slds-theme--info');
                    $A.util.addClass(title,'slds-theme--error'); 
                    
                    component.set('v.title_message', 'Atención');
                    if(returnValue == 'errorCatch' || returnValue == 'errorNull' ){
                        component.set('v.content_message', 'No ha sido posible recuperar la información de riesgo del cliente actual.'); 
                    } else if(returnValue == 'errorCreditType'){
                        component.set('v.content_message', 'El campo tipo de crédito ha de ser informado.');  
                    } else if(returnValue == 'errorPais'){
                        component.set('v.content_message', 'No es posible recuperar la información de riesgo para el país seleccionado.');       
                    }else component.set('v.content_message', returnValue);    
                }
                
            } else {
                /*Error*/
                $A.util.removeClass(title,'slds-theme--info');
                $A.util.addClass(title,'slds-theme--error');
                
                component.set('v.title_message', 'Atención');
                component.set('v.content_message', 'Ha ocurrido un error al obtener la información de riesgo');               
            }
            
            /*Hide Spinner*/
            $A.util.removeClass(spinner,'slds-show');
            $A.util.addClass(spinner,'slds-hide'); 
            $A.util.removeClass(prompt,'slds-hide');
            $A.util.addClass(prompt,'slds-show');
        });
        $A.enqueueAction(action);
    },
    returnRedirection:function(component, event){
        /*Variable Definition*/
        var clientId = '/'+component.get("v.clientId");
        
        if( (typeof sforce != 'undefined') && (sforce != null) ) sforce.one.navigateToURL(clientId);
        else window.location.assign(clientId);
    }
})