({
    iniciaComponente : function(component, evt, helper) {
        
        var element = component.find('BI_G4C_Modal_Direccion_id');
        $A.util.removeClass(element, 'slds-hide');
        var acc=evt.getParam("account");
        component.set('v.account', evt.getParam("account"));
        component.set('v.accounts', evt.getParam("accounts"));
        debugger;
        var action = component.get("c.getCountryPickList");
        
          
	    component.find("calleComercial").set("v.value", acc.ShippingStreet!==undefined ? acc.ShippingStreet:'');
        component.find("ciudadComercial").set("v.value", acc.ShippingCity!==undefined ? acc.ShippingCity:'');
        component.find("provinciaComercial").set("v.value", acc.ShippingState!==undefined ? acc.ShippingState:'');
        component.find("codigoPostalComercial").set("v.value", acc.ShippingPostalCode!==undefined ? acc.ShippingPostalCode:'');
          
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                component.set("v.countriesValues", response.getReturnValue());
                component.find("paisComercial").set("v.value",response.getReturnValue()[0]);
            } else {
                component.set('v.errorMessage', '');                
                var errorVar = $A.get("e.c:BI_G4C_Generic_error");
                errorVar.setParams('header', 'Error');
                errorVar.setParams('body', 'Error');
                errorVar.fire();
            }
        });
        $A.enqueueAction(action);
    },
    hideModal : function(component, evt, helper) {
        var calleComercial = component.find('calleComercial');
        var numCalle= component.find('numCalleComercial');
        var ciudadComercial = component.find('ciudadComercial');
        var provinciaComercial = component.find('provinciaComercial');
        var codigoPostalComercial = component.find('codigoPostalComercial');
        var fromArray = [calleComercial,numCalle,ciudadComercial,provinciaComercial,codigoPostalComercial];
        component.set('v.errorMessage', '');
        // reset modal
        for(var i = 0; fromArray.length > i; i++) {
            var campo = fromArray[i];
            campo.set("v.value","");
        }
        var confirm = component.find('BI_G4C_Modal_Direccion_id');
        $A.util.addClass(confirm, 'slds-hide');
    },
    saveComercialAddres: function(component, evt, helper) {
        var calleComercial = component.find('calleComercial');
        var numCalle= component.find('numCalleComercial');
        var ciudadComercial = component.find('ciudadComercial');
        var provinciaComercial = component.find('provinciaComercial');
        var codigoPostalComercial = component.find('codigoPostalComercial');
       if(codigoPostalComercial.get("v.value").trim() == "")
           codigoPostalComercial.set("v.value","N/A");
        var paisComercial = component.find("paisComercial").get("v.value");
        debugger; 
        var acc = component.get('v.account');
        var fromArray = [calleComercial,numCalle,ciudadComercial,provinciaComercial,codigoPostalComercial];
        
       
        var error = false; 
        for(var i = 0; fromArray.length > i; i++) {
            var campo = fromArray[i];
            if(campo.get("v.value").trim() == "") {
                component.set('v.errorMessage', 'Los campos marcados en rojo son obligatorios.');
                error = true;
            }
        }
        
        if(!error) {
                       
            component.set('v.errorMessage', '');
            var action = component.get("c.getSaveCommercialAddress");
            
            action.setParams({'acc': ''+acc.Id,
                              'calleComercial':calleComercial.get("v.value").trim(), 
                              'ciudadComercial': ciudadComercial.get("v.value").trim(), 
                              'numComercial':numCalle.get("v.value").trim(),
                              'provinciaComercial':provinciaComercial.get("v.value").trim(),
                              'codigoPostalComercial':codigoPostalComercial.get("v.value").trim(),  
                              'paisComercial':paisComercial});
            
            action.setCallback(this, function(response) {
                debugger;
                if (response.getState() === "SUCCESS") {
                    if(response.getReturnValue() === true){
                    // reset modal
                    for(var i = 0; fromArray.length > i; i++) {
                        var campo = fromArray[i];
                        campo.set("v.value","");
                    }
                    var confirm = component.find('BI_G4C_Modal_Direccion_id');
            		$A.util.addClass(confirm, 'slds-hide');
                    var compEvent = $A.get("e.c:BI_G4C_nuevo_cliente_localizado");
                    compEvent.fire();
                    }else 
                        component.set('v.errorMessage', 'No se ha podido localizar dirección con la información proporcionada.');
                } else {
                    component.set('v.errorMessage', '');                    
                    var errorVar = $A.get("e.c:BI_G4C_Generic_error");
                    errorVar.setParams('header', 'Error');
                    errorVar.setParams('body', 'Error');
                    errorVar.fire();
                }
            });
            $A.enqueueAction(action);  
        }   
    },
    
})