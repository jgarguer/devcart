({
         MAX_FILE_SIZE: 4 500 000,
    /* 6 000 000 * 3/4 to account for base64 */
    CHUNK_SIZE: 950 000,
    /* Use a multiple of 4 */
    
    filesToUpload : 0,
    filesUploaded : 0,
    
    getTicketsContext : function(component, event, helper){
    	var action = component.get("c.getTicketsContextController");
        action.setParams({tContextString : 'init'});
    	action.setCallback(this, function(response) {
           
            if(component.isValid() && response.getState() == "SUCCESS"){
                var returnValue = response.getReturnValue();
                var res = response.getReturnValue();
                console.log('respuesta de getTicketsContext: ' + JSON.parse(res));
                component.set('v.ticketsContext', JSON.parse(res));
                console.log('iniIndex: ' + JSON.parse(res).tableControl.iniIndex);
                document.getElementById('CaseNumber').parentElement.classList.add('headerSortDown');
            }
             if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
    },
    
    getOrdersContext : function(component, event, helper){
    	var ordersCont = component.get('v.ordersContext');
    	var action = component.get("c.getTicketsContextController");
       
    	if(ordersCont == null){
            if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='block';
            }
    		action.setParams({tContextString : 'init orders'});
    		action.setCallback(this, function(response) {
	    		if(document.getElementById("backgroundmodalwait")!=null){
                    document.getElementById("backgroundmodalwait").style.display='none';
                }
                document.getElementById('CaseNumber;Orders').parentElement.classList.add('headerSortDown');
	            if(component.isValid() && response.getState() == "SUCCESS"){
	                //var returnValue = response.getReturnValue();
	                var res = response.getReturnValue();
	                component.set('v.ordersContext', JSON.parse(res));
	            }
	        });
	        $A.enqueueAction(action);
    	}
    },
    
    getNewPagination : function(cmp, event, helper, c, isDetail) {
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }
        var action = cmp.get("c.getNewTableController");
        action.setParams({"contextString": JSON.stringify(c)});     
        action.setCallback(this, function(response) {
            if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var context = JSON.parse(response.getReturnValue());
                if(context.table === 'orders'){
                	cmp.set("v.ordersContext", context);
                }else{
                	cmp.set("v.ticketsContext", context);
                	if(isDetail){
                		var index = cmp.get('v.tableIndexDetail')
                		var caseNumber = context.casesList[index].CaseNumber;
                		// Habilito/Deshabilito botones
		    			// Previous
		    			if(context.tableControl.iniIndex-1 === 0 && index == 0){
			        		document.getElementById('previousItemButton').classList.add('tlf-btn--disabled');
			        		document.getElementById('previousItemButton').disabled = true;
			        	}else{
			        		document.getElementById('previousItemButton').classList.remove('tlf-btn--disabled');
			        		document.getElementById('previousItemButton').disabled = false;
			        	}
			        	// Next
			        	if(context.tableControl.iniIndex + index >= context.tableControl.numRecords ){
			        		document.getElementById('nextItemButton').classList.add('tlf-btn--disabled');
			        		document.getElementById('nextItemButton').disabled = true;
			        	}else{
			        		document.getElementById('nextItemButton').classList.remove('tlf-btn--disabled');
			        		document.getElementById('nextItemButton').disabled = false;
			        	}
                		helper.getDetails(cmp, event, helper, caseNumber);
                	}
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getTickets : function(component, evt,helper, tableControl) {   
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }
        var action = component.get("c.getTickets");    
        action.setParams({ tableControllerS :JSON.stringify(tableControl) });
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                if(document.getElementById("backgroundmodalwait")!=null){
                    document.getElementById("backgroundmodalwait").style.display='none';
                }
                var returnValue = response.getReturnValue();
                
                var res = response.getReturnValue();
                component.set('v.ticketsList', JSON.parse(res["lisTickets"]));
                component.set('v.ticketsTableController', JSON.parse(res["offsetController"]));
            }
        });
        $A.enqueueAction(action);
        
    }, 
    getFieldValues : function(component, evt,helper, context) {
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }   
        // Con context
        var actionAdvancedFilt = component.get("c.getAdvancedFiltersValuesForTickets");
        //actionAdvancedFilt.setParams({contextString : JSON.stringify(component.get('v.ticketsContext'))});
        actionAdvancedFilt.setParams({contextString : JSON.stringify(context)});
        actionAdvancedFilt.setCallback(this, function(response) {
        	if(component.isValid() && response.getState() == "SUCCESS"){
                if(document.getElementById("backgroundmodalwait")!=null){
                    document.getElementById("backgroundmodalwait").style.display='none';
                }
        		if(JSON.parse(response.getReturnValue()).table == 'tickets'){
        			component.set('v.ticketsContext', JSON.parse(response.getReturnValue()));
        		}else if(JSON.parse(response.getReturnValue()).table == 'orders'){
        			component.set('v.ordersContext', JSON.parse(response.getReturnValue()));
        		}
        	}
        });
        $A.enqueueAction(actionAdvancedFilt);
    },
    
    changeTypeOption : function(component, event, helper, context){
    	//var context = component.get('v.ticketsContext');
    	if(context.table == 'orders'){
    		context.pickListCaseTypeValue=document.getElementById("select-caseTypeOr").value;
    		component.set('v.ordersContext', context);
    	}else{
    		context.pickListCaseTypeValue=document.getElementById("select-caseType").value;
    		component.set('v.ticketsContext', context);
    	}
        
        
        helper.llamadagetTicketsContextController(component, event, helper, context);        
    },
    
    changeServiceFamOpt : function(component, event, helper, context){
    	//var context = component.get('v.ticketsContext');
    	if(context.table == 'orders'){
    		context.pickListFamilyServiceValue=document.getElementById("select-ServiveFamilyOr").value;
    		component.set('v.ordersContext', context);
    	}else{
			context.pickListFamilyServiceValue=document.getElementById("select-ServiveFamily").value;
			component.set('v.ticketsContext', context);
		}
        helper.llamadagetTicketsContextController(component, event, helper, context);
    },
    
    changeServiceOpt : function(component, event, helper, context){
    	//var context = component.get('v.ticketsContext');
    	if(context.table == 'orders'){
			context.pickListServiceValue=document.getElementById("selectServiceTicketsOr").value;
    		component.set('v.ordersContext', context);
    	}else{
			context.pickListServiceValue=document.getElementById("selectServiceTickets").value;
			component.set('v.ticketsContext', context);
		}
        helper.llamadagetTicketsContextController(component, event, helper, context);
    },
    
    changeServiceUnitOpt : function(component, event, helper, context){
    	//var context = component.get('v.ticketsContext');
    	if(context.table == 'orders'){
			context.pickListServiceUnitValue=document.getElementById("select-servUnitOr").value;
    		component.set('v.ordersContext', context);
    	}else{
			context.pickListServiceUnitValue=document.getElementById("select-servUnit").value;
			component.set('v.ticketsContext', context);
		}
        helper.llamadagetTicketsContextController(component, event, helper, context);	
    },
    
    changeStatusOpt : function(component, event, helper, context){
    	//var context = component.get('v.ticketsContext');
    	if(context.table == 'orders'){
			context.pickListStatusValue=document.getElementById("select-estadoOr").value;
    		component.set('v.ordersContext', context);
    	}else{
			context.pickListStatusValue=document.getElementById("select-estado").value;
			component.set('v.ticketsContext', context);
		}
        helper.llamadagetTicketsContextController(component, event, helper, context);
    },
    
    changeCountryOpt : function(component, event, helper, context){
    	//var context = component.get('v.ticketsContext');
    	if(context.table == 'orders'){
			context.pickListCountryValue=document.getElementById("select-countryOr").value;
    		component.set('v.ordersContext', context);
    	}else{
			context.pickListCountryValue=document.getElementById("select-country").value;
			component.set('v.ticketsContext', context);
		}        
        helper.llamadagetTicketsContextController(component, event, helper, context);
    },
    
    changeCityOpt : function(component, event, helper, context){
    	//var context = component.get('v.ticketsContext');
    	if(context.table == 'orders'){
			context.pickListCityValue=document.getElementById("select-cityOr").value;
    		component.set('v.ordersContext', context);
    	}else{
			context.pickListCityValue=document.getElementById("select-city").value;
			component.set('v.ticketsContext', context);
		}
        helper.llamadagetTicketsContextController(component, event, helper, context);
    },
    
    changeShowOpt : function(component, event, helper, context){
    	//var context = component.get('v.ticketsContext');
    	if(context.table == 'orders'){
			context.pickListShowValue=document.getElementById("select-showOr").value; 
    		component.set('v.ordersContext', context);
    	}else{
			context.pickListShowValue=document.getElementById("select-show").value; 
			component.set('v.ticketsContext', context);
		}        
        helper.llamadagetTicketsContextController(component, event, helper, context);
    },
    
    applyKeyNumberFilter : function(component, event, helper, context){
    	//var context = component.get('v.ticketsContext');
    	if(context.table == 'orders'){
			context.keyValueFilter =document.getElementById("keyValueFilterOr").value; 
	        context.caseNumberFilter = document.getElementById("caseNumberFilterOr").value;
    		component.set('v.ordersContext', context);
    	}else{
			context.keyValueFilter =document.getElementById("keyValueFilter").value; 
	        context.caseNumberFilter = document.getElementById("caseNumberFilter").value;
	        component.set('v.ticketsContext', context);
		}        
        helper.llamadagetTicketsContextController(component, event, helper, context);
    },
    
    clearAllFilters : function(component, event, helper, context){
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }
    	var action = component.get("c.clearAdvancedFilters");
        //action.setParams({tContextString : JSON.stringify(component.get('v.ticketsContext'))});
        action.setParams({tContextString : JSON.stringify(context)});
        action.setCallback(this, function(response) {
        	if(component.isValid() && response.getState() == "SUCCESS"){
                if(document.getElementById("backgroundmodalwait")!=null){
                    document.getElementById("backgroundmodalwait").style.display='none';
                }
	        	if(context.table == 'orders'){
	                component.set('v.ordersContext', JSON.parse(response.getReturnValue()));
	                document.getElementById("keyValueFilterOr").value = component.get('v.ordersContext').keyValueFilter;
	                document.getElementById("caseNumberFilterOr").value = component.get('v.ordersContext').caseNumberFilter;
                }else{
                	component.set('v.ticketsContext', JSON.parse(response.getReturnValue()));
	                document.getElementById("keyValueFilter").value = component.get('v.ticketsContext').keyValueFilter;
	                document.getElementById("caseNumberFilter").value = component.get('v.ticketsContext').caseNumberFilter;
                }
            }else{
                if(document.getElementById("backgroundmodalwait")!=null){
                    document.getElementById("backgroundmodalwait").style.display='none';
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    orderByHelp : function(component, event, helper, context){
    	helper.llamadagetTicketsContextController(component, event, helper, context);
    },
    
    llamadagetTicketsContextController : function (component, event, helper, context){
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }
    	var action = component.get("c.getTicketsContextController");
        //action.setParams({tContextString : JSON.stringify(component.get('v.ticketsContext'))});
        action.setParams({tContextString : JSON.stringify(context)});
        action.setCallback(this, function(response) {
        	if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        	if(component.isValid() && response.getState() == "SUCCESS"){
        		if(context.table == 'orders'){
        			component.set('v.ordersContext', JSON.parse(response.getReturnValue()));
        		}else{
        			component.set('v.ticketsContext', JSON.parse(response.getReturnValue()));
        		}
        	}
        });
        $A.enqueueAction(action);
    },
    
    getExcelPicklist : function(component, event, helper, context){
		//var context = component.get('v.ticketsContext');
		if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='block';
            }
    	var action = component.get("c.loadPickListExcelModal");
        action.setParams({"tContextString": JSON.stringify(context)});     
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var context = JSON.parse(response.getReturnValue());
                //console.log(context.pickListfilterExcel);
                if(context.table === 'orders'){
                	component.set("v.ordersContext", context);
                	setTimeout(
                    function(){
                        $("#nameIDOrder").chosen();
                        $("#nameIDOrder").trigger("chosen:updated")
                    }, 10); 
                }else{
                	component.set("v.ticketsContext", context);
                	setTimeout(
                    function(){
                        $("#nameID").chosen();
                        $("#nameID").trigger("chosen:updated")
                    }, 10); 
                }
            }
             if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
            
        });
        $A.enqueueAction(action);
    },
    
    addExcelFilterValue : function(component, event, helper, context){
        if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='block';
            }

    	var value;
    	if(context.table === 'orders'){
    		value =document.getElementById('nameIDOrder').value;
    	}else{
    		value =document.getElementById('nameID').value;
    	}
        
        if(value != ''){
	        //var context = component.get('v.ticketsContext');
	        context.inputExcelFilterValue=value;
	        if(context.table === 'orders'){
	        	component.set("v.ordersContext", context);
	        }else{
	        	component.set("v.ticketsContext", context);
	        }
	        var action = component.get("c.addExcelFilterValueCtr");
	        action.setParams({"tContextString": JSON.stringify(context)});     
	        action.setCallback(this, function(response) {
	            if (component.isValid() && response.getState() === "SUCCESS") {
	                var context = JSON.parse(response.getReturnValue());
	                //console.log(context.pickListfilterExcel);
	                if(context.table === 'orders'){
			        	component.set("v.ordersContext", context);
			        }else{
			        	component.set("v.ticketsContext", context);
			        }
	            }
                 if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
	        });
	        $A.enqueueAction(action);
        }
    },
    
    removeExcelFilterValue : function(component, event, helper, context){
        if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='block';
            }

    	var value =event.target.id;
    	console.log('borrar filtro: ' + event.target.id);
        //var context = component.get('v.ticketsContext');
        context.inputExcelFilterValue=value;
        var action = component.get("c.quitaFiltroExcel");
        action.setParams({"tContextString": JSON.stringify(context)});     
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                //var context = JSON.parse(response.getReturnValue());
                if(context.table === 'orders'){
                	component.set("v.ordersContext", JSON.parse(response.getReturnValue()));
                }else{
                	component.set("v.ticketsContext", JSON.parse(response.getReturnValue()));
                }
            }
            if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
    },
    save: function(component, caso) {
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }
        var adjuntos= component.get("v.caseDetailedAttachments");  
        if(adjuntos.length >0){
            NProgress.start();
            for (var i=0; adjuntos.length > i; i++) {
                if(adjuntos[i].aux == 'js'){
                    this.filesToUpload ++;
                    this.savefile(adjuntos[i], caso, component);
                }
			}
        }else{
            if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        }
    },


    uploadChunk: function(file, fileContents, fromPos, toPos, attachId, caso, component) {
        var action = component.get("c.saveTheChunk");
        var chunk = fileContents.substring(fromPos, toPos);

        action.setParams({
            fileName: file.name,
            fileDescription: file.Description,
            base64Data: encodeURIComponent(chunk),
            contentType: file.type,
            fileId: attachId,
            caso: caso
        });

        var self = this;
        action.setCallback(this, function(a) {
            attachId = a.getReturnValue();

            fromPos = toPos;
            toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);

            if (fromPos < toPos) {
                self.uploadChunk(file, fileContents, fromPos, toPos, attachId, caso, component);
            }else{
            	self.filesUploaded++;
                
                var action2 = component.get("c.getCommentsCaseDetails");    
		        action2.setParams({ caseNumber :caso });
		        action2.setCallback(this, function(response) {
		            if(component.isValid() && response.getState() == "SUCCESS"){                
		                var res = response.getReturnValue();
		                component.set('v.caseDetailedComments', res);
		            }
		        });
		        $A.enqueueAction(action2);
                var action3 = component.get("c.getAttachmentsCaseDetails");    
                action3.setParams({ caseNumber :caso });
                action3.setCallback(this, function(response) {
                    if(document.getElementById("backgroundmodalwait")!=null){
	                    document.getElementById("backgroundmodalwait").style.display='none';
	                }
	                if(component.isValid() && response.getState() == "SUCCESS"){                
	                    var res = response.getReturnValue();
	                    //component.set('v.caseDetailedAttachments', res);
	                    component.set('v.caseDetailedAttachments', JSON.parse(res));
	                }
	                
		        });
		        $A.enqueueAction(action3);
		        if(self.filesToUpload==self.filesUploaded){  
		        	
                    var actionRoD = component.get("c.getIntegraRoD");
                    actionRoD.setParams({
                        caseNumber: caso
                    });
                    actionRoD.setCallback(this, function(response) {
                        if(component.isValid() && response.getState() == "SUCCESS"){                
                            var res = response.getReturnValue();
                            NProgress.done();
                        }                
                    });
                    $A.enqueueAction(actionRoD);
                }
            }
        });
		$A.enqueueAction(action);
    },
        
 	savefile: function(file, caso, component) {
                if (file.size > this.MAX_FILE_SIZE) {
                    alert('File size cannot exceed ' + this.MAX_FILE_SIZE + ' bytes.\n' +
                        'Selected file size: ' + file.size);
                    document.getElementById("backgroundmodalwait").style.display='none';
                    NProgress.done();
                    return;
                }
    
                var fr = new FileReader();
    
                var self = this;
                fr.onload = $A.getCallback(function() {
                    var fileContents = fr.result;
                    var base64Mark = 'base64,';
                    if(fileContents == null){
                    	fileContents='';
                	}
                    var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
    
                    fileContents = fileContents.substring(dataStart);
    
                	var fromPos = 0;
                    var toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);
            
                    // start with the initial chunk
                    self.uploadChunk(file, fileContents, fromPos, toPos, '', caso, component);

                });
                fr.readAsDataURL(file);
    },
    
    getDetails : function(component, event, helper, caseNumber){
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }
    	var action = component.get("c.getCaseDetails");    
        action.setParams({ caseNumber :caseNumber });
        action.setCallback(this, function(response) {
        	if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
            if(component.isValid() && response.getState() == "SUCCESS"){                
                var res = response.getReturnValue();
                component.set('v.caseDetailed', res);
                var aux = component.get('v.caseDetailed.TGS_Customer_Services__r.Name');
                if (aux != null){
                    document.getElementById('tabcomponent2').style.display='';
                    var appEvent = $A.get("e.c:CWP_ServiceTracking_TicketsPedidos_details_event");
                    var caseNumberDetail = component.get("v.caseDetailed").CaseNumber;
                    appEvent.setParams({ "OiId" : caseNumberDetail });
                    appEvent.fire();
                }else{
                    document.getElementById('tabcomponent2').style.display='none';
                }
            }
        });
        var action2 = component.get("c.getCommentsCaseDetails");    
        action2.setParams({ caseNumber :caseNumber });
        action2.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){                
                var res = response.getReturnValue();
                component.set('v.caseDetailedComments', res);
            }
        });
        var action3 = component.get("c.getAttachmentsCaseDetails");    
        action3.setParams({ caseNumber :caseNumber });
        action3.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){                
                var res = response.getReturnValue();
                component.set('v.caseDetailedAttachments', JSON.parse(res));
            }
        });
        var action4 = component.get("c.getCaseHistoryList");    
        action4.setParams({ caseNumber :caseNumber });
        action4.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){                
                var res = response.getReturnValue();
                component.set('v.caseDetailedHistory', res);
            }
        });
        
        $A.enqueueAction(action);
        $A.enqueueAction(action2);
        $A.enqueueAction(action3);
        $A.enqueueAction(action4);
    },
    
    getOrdersContextNew : function(component, event, helper){
    	var ordersCont = component.get('v.ordersContext');
    	var action = component.get("c.getTicketsContextController");
       

            if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='block';
            }
    		action.setParams({tContextString : 'init orders'});
    		action.setCallback(this, function(response) {
	    		if(document.getElementById("backgroundmodalwait")!=null){
                    document.getElementById("backgroundmodalwait").style.display='none';
                }
	            if(component.isValid() && response.getState() == "SUCCESS"){
	                //var returnValue = response.getReturnValue();
	                var res = response.getReturnValue();
	                component.set('v.ordersContext', JSON.parse(res));
	            }
	        });
	        $A.enqueueAction(action);
    	},
    
})