({
    doInit : function(component, event, helper) {

        //helper.getInicialTickets(component, event, helper);
        /*$(document).ready(function() {
            if(window.location.hash==='#orders'){
                    document.getElementById('tab2').click();
            }
        });*/
         if( navigator.userAgent.match(/Android/i)
               || navigator.userAgent.match(/webOS/i)
               || navigator.userAgent.match(/iPhone/i)
               || navigator.userAgent.match(/iPad/)
               || navigator.userAgent.match(/iPod/i)
               || navigator.userAgent.match(/BlackBerry/i)
               || navigator.userAgent.match(/Windows Phone/i)
              )
            {
                component.set("v.displayExcel", false);
            }
            else {
                component.set("v.displayExcel", true);
            }
        // A Petición del COE se habilita siempre la descarga de archivos Excel
         component.set("v.displayExcel", true);
        
        helper.getTicketsContext(component, event, helper);
    },
    doNextPage:function(cmp, event, helper) {
        var controller = cmp.get('v.ticketsContext');
        controller.pagination="forward";
        helper.getNewPagination(cmp, event, helper, controller, false);
    },
    
    doNextPageOrders : function(cmp, event, helper) {
        var controller = cmp.get('v.ordersContext');
        controller.pagination="forward";
        helper.getNewPagination(cmp, event, helper, controller, false);
    },
    
    doPrevPage:function(cmp, event, helper) {
        var controller = cmp.get('v.ticketsContext');
        controller.pagination="backward";
        helper.getNewPagination(cmp, event, helper, controller, false);
    },
    
    doPrevPageOrders:function(cmp, event, helper) {
        var controller = cmp.get('v.ordersContext');
        controller.pagination="backward";
        helper.getNewPagination(cmp, event, helper, controller, false);
    },
    
    doChangePageView:function(cmp, event, helper) {
        var controller = cmp.get('v.ticketsContext');
        
        controller.pagination=document.getElementById("pagination2").value;
        helper.getNewPagination(cmp, event, helper, controller, false);
    },
    
    doChangePageViewOrders : function(cmp, event, helper) {
        var controller = cmp.get('v.ordersContext');
        
        controller.pagination=document.getElementById("paginationOrders").value;
        helper.getNewPagination(cmp, event, helper, controller, false);
    },
    
    doInitOrder : function(component, event, helper) {   
        window.location.hash='orders';
        component.set('v.tableTickets', false);
        helper.getOrdersContext(component, event, helper);
        
    },
    setTicketHash : function(component, event, helper) {  
    	component.set('v.tableTickets', true); 
        window.location.hash='tickets';
    },
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner,'slds-hide');
     },
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner,'slds-hide');
    },
    
    showExcelFilter : function (cmp, event, helper) {
        document.getElementById('backgroundmodal').style.display='block';
        var context = cmp.get('v.ticketsContext');
        var columna=event.target.id;
        context.pickedRow=columna;
        console.log('columna seleccionada: ' + columna);
        cmp.set('v.ticketsContext', context);
        helper.getExcelPicklist(cmp, event, helper, context);
    },
    
    showExcelFilterOrder : function (cmp, event, helper) {
        
        document.getElementById('backgroundmodal').style.display='block';
        var context = cmp.get('v.ordersContext');
        var columna=event.target.id;
        context.pickedRow=columna;
        console.log('columna seleccionada: ' + columna);
        cmp.set('v.ordersContext', context);
        helper.getExcelPicklist(cmp, event, helper, context);
    },
    
    addExcelFilter : function(component, event, helper){
    	document.getElementById('backgroundmodal').style.display='none';
    	event.preventDefault();
        helper.addExcelFilterValue(component, event, helper, component.get('v.ticketsContext'));
        
    },
    
    addExcelFilterOrder : function(component, event, helper){
    	document.getElementById('backgroundmodal').style.display='none';
    	event.preventDefault();
        helper.addExcelFilterValue(component, event, helper, component.get('v.ordersContext'));
        
    },
    
    removeExcelFilter : function(component, event, helper){
        event.preventDefault();
        helper.removeExcelFilterValue(component, event, helper, component.get('v.ticketsContext'));
    },
    
    removeExcelFilterOrder : function(component, event, helper){
        event.preventDefault();
        helper.removeExcelFilterValue(component, event, helper, component.get('v.ordersContext'));
    },
    
    advancedFilterTickets : function(component, event, helper) {
        var context= component.get('v.ticketsContext');
        if (context.pickListCaseTypeOptions==undefined)
        	helper.getFieldValues(component, event, helper, component.get('v.ticketsContext'));
    }, 
    
    advancedFilterOrders : function(component, event, helper) {
        var context= component.get('v.ordersContext');
        if (context.pickListCaseTypeOptions==undefined)
            helper.getFieldValues(component, event, helper, component.get('v.ordersContext'));
    }, 
    
    changeType : function(component, event, helper){
    	helper.changeTypeOption(component, event, helper, component.get('v.ticketsContext'));
    },
    changeTypeOrder : function(component, event, helper){
    	helper.changeTypeOption(component, event, helper, component.get('v.ordersContext'));
    },
    
    changeServiceFamily : function(component, event, helper){
    	helper.changeServiceFamOpt(component, event, helper, component.get('v.ticketsContext'));
    },
    
    changeServiceFamilyOrder : function(component, event, helper){
    	helper.changeServiceFamOpt(component, event, helper, component.get('v.ordersContext'));
    },
    
    changeService : function(component, event, helper){
    	helper.changeServiceOpt(component, event, helper, component.get('v.ticketsContext'));
    },
    
    changeServiceOrder :  function(component, event, helper){
    	helper.changeServiceOpt(component, event, helper, component.get('v.ordersContext'));
    },
    
    changeServiceUnit : function(component, event, helper){
    	helper.changeServiceUnitOpt(component, event, helper, component.get('v.ticketsContext'));
    },
    
    changeServiceUnitOrder : function(component, event, helper){
    	helper.changeServiceUnitOpt(component, event, helper, component.get('v.ordersContext'));
    },
    
    changeStatus : function(component, event, helper){
    	helper.changeStatusOpt(component, event, helper, component.get('v.ticketsContext'));
    },
    
    changeStatusOrder : function(component, event, helper){
    	helper.changeStatusOpt(component, event, helper, component.get('v.ordersContext'));
    },
    
    changeCountry : function(component, event, helper){
    	helper.changeCountryOpt(component, event, helper, component.get('v.ticketsContext'));
    },
    
    changeCountryOrder : function(component, event, helper){
    	helper.changeCountryOpt(component, event, helper, component.get('v.ordersContext'));
    },
    
    changeCity : function(component, event, helper){
    	helper.changeCityOpt(component, event, helper, component.get('v.ticketsContext'));
    },
    
    changeCityOrder : function(component, event, helper){
    	helper.changeCityOpt(component, event, helper, component.get('v.ordersContext'));
    },
    
    changeShow : function(component, event, helper){
    	helper.changeShowOpt(component, event, helper, component.get('v.ticketsContext'));
    },
    
    changeShowOrder : function(component, event, helper){
    	helper.changeShowOpt(component, event, helper, component.get('v.ordersContext'));
    },
    
    keyNumberFilter : function(component, event, helper){
    	helper.applyKeyNumberFilter(component, event, helper, component.get('v.ticketsContext'));
    },
    
    keyNumberFilterOrder : function(component, event, helper){
    	helper.applyKeyNumberFilter(component, event, helper, component.get('v.ordersContext'));
    },
    
    clearFilters : function(component, event, helper){
    	helper.clearAllFilters(component, event, helper, component.get('v.ticketsContext'));
    },
    
    clearFiltersOrder : function(component, event, helper){
    	helper.clearAllFilters(component, event, helper, component.get('v.ordersContext'));
    },
    
    orderBy : function(component, event, helper){
        var context = component.get('v.ticketsContext');
        var id = event.currentTarget.id;
        if(document.location.hash.indexOf('ASC') < 0){
            context.sortedRow=id;
            context.sortedOrder = 'ASC ';
            document.location.hash=id + '::ASC';
            event.preventDefault();
            // Pinto los indicadores de ordenación y borro los que no apliquen
    		var elementos = document.getElementsByClassName("tlf-table__head__link");
            for(i=0; i<10; i++){
            	console.log(elementos[i].id);
            	if(elementos[i].id !=  context.sortedRow){
            		document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortUp');
            		document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortDown');
            	}
            }
    		document.getElementById(context.sortedRow).parentElement.classList.remove('headerSortDown');
            document.getElementById(context.sortedRow).parentElement.classList.add('headerSortUp');
	            
	    	
        }else{
        	context.sortedRow=id;
        	if(id == 'TGS_Customer_Services__r.NE__OrderId__r.Site__r.BI_Sede__r.BI_Localidad__c' || id == 'Subject'){
        		context.sortedOrder = 'DESC NULLS LAST';
        	}else{
        		context.sortedOrder = 'DESC';
        	}
            
            document.location.hash=id + '::DESC';
            event.preventDefault();
            
            // Pinto los indicadores de ordenación y borro los que no apliquen
    		var elementos = document.getElementsByClassName("tlf-table__head__link");
            for(i=0; i<10; i++){
            	console.log(elementos[i].id);
            	if(elementos[i].id !=  context.sortedRow){
            		document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortUp');
            		document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortDown');
            	}else{
            		document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortUp');
            		document.getElementById(elementos[i].id).parentElement.classList.add('headerSortDown');
            	}
            }
    		
	            
        }
        component.set("v.ticketsContext", context);
        helper.orderByHelp(component, event, helper, context);
    },
    
    orderByOrders : function(component, event, helper){
        var context = component.get('v.ordersContext');
        var id = event.currentTarget.id;
        if(document.location.hash.indexOf('ASC') < 0){
            context.sortedRow=id.split(';')[0];
            context.sortedOrder = 'ASC ';
            document.location.hash=id + '::ASC';
            event.preventDefault();
            // Pinto los indicadores de ordenación y borro los que no apliquen
    		var elementos = document.getElementsByClassName("tlf-table__head__link");
            for(i=0; i<20; i++){
            	console.log(elementos[i].id);
            	if(elementos[i].id !=  id){
	            	if(elementos[i].id.indexOf("Orders") > 0){
	            		document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortUp');
	            		document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortDown');
	            	}
            	}else{
            		if(elementos[i].id.indexOf("Orders") > 0){
            			document.getElementById(id).parentElement.classList.remove('headerSortDown');
            			document.getElementById(id).parentElement.classList.add('headerSortUp');
            		}
            	}
            }
    		
        }else{
            
            context.sortedRow=id.split(';')[0];
        	if(context.sortedRow == 'Order__r.Site__r.BI_Sede__r.BI_Localidad__c' || id == 'Subject'){
        		context.sortedOrder = 'DESC NULLS LAST';
        	}else{
        		context.sortedOrder = 'DESC';
        	}
            
            document.location.hash=id + '::DESC';
            event.preventDefault();
            // Pinto los indicadores de ordenación y borro los que no apliquen
    		var elementos = document.getElementsByClassName("tlf-table__head__link");
            for(i=0; i<20; i++){
            	console.log(elementos[i].id);
            	if(elementos[i].id !=  id){
	            	if(elementos[i].id.indexOf("Orders") > 0){
	            		document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortUp');
	            		document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortDown');
            		}
            	}else{
	            	if(elementos[i].id.indexOf("Orders") > 0){
	            		document.getElementById(id).parentElement.classList.remove('headerSortUp');
	            		document.getElementById(id).parentElement.classList.add('headerSortDown');
            		}
            	}
            }
        }
        component.set("v.ordersContext", context);
        helper.orderByHelp(component, event, helper, context);
    },
    
    closeModal : function(component, event, helper) {
            document.getElementById('backgroundmodal').style.display='none';
    },
    showBackground : function(component, event, helper) {
            document.getElementById('backgroundmodal').style.display='block';
    },
    hideBackground : function(component, event, helper) {
            document.getElementById('backgroundmodal').style.display='none';
    },
    
    exportExcelTickets : function (cmp, event, helper) {
        var context = cmp.get('v.ticketsContext');
        var action = cmp.get("c.exportExcelPDF");
        action.setParams({"tContextString": JSON.stringify(context)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
				document.getElementById("CWP_exportServices_Excel_L").click();
            }
        });
        $A.enqueueAction(action); 
    },
    
    exportExcelOrders : function (cmp, event, helper) {
        var context = cmp.get('v.ordersContext');
        var action = cmp.get("c.exportExcelPDF");
        action.setParams({"tContextString": JSON.stringify(context)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
				document.getElementById("CWP_exportOrders_Excel_L").click();
            }
        });
        $A.enqueueAction(action); 
    },
    
    exportPDFtickets : function (cmp, event, helper) {
        var context = cmp.get('v.ticketsContext');
        var action = cmp.get("c.exportExcelPDF");
        action.setParams({"tContextString": JSON.stringify(context)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
				document.getElementById("CWP_exportServices_PDF_L").click();
            }
        });
        $A.enqueueAction(action); 
    },
    
    exportPDForders : function (cmp, event, helper) {
        var context = cmp.get('v.ordersContext');
        var action = cmp.get("c.exportExcelPDF");
        action.setParams({"tContextString": JSON.stringify(context)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
				document.getElementById("CWP_exportOrders_PDF_L").click();
            }
        });
        $A.enqueueAction(action); 
    },
    
    
   showModal: function(component, event, helper) {
        var option = event.currentTarget.value;
        if (option !== "") {
            if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='block';
            }
            var key = 'v.' + option;
            component.set(key, true);
            document.getElementById(option).classList.add("in");
            document.getElementById(option).style.display='block';
            document.getElementById('backgroundmodal').style.display='block';
            document.getElementsByTagName('body')[0].classList.add('modal-open');
            document.getElementById('combo').value='default';
        }

    },

    hideModal: function(component, event, helper) {
        var option = event.currentTarget.value;
        if (option !== "") {
            var key = 'v.' + option;
            component.set(key, false);
            document.getElementById(option).classList.remove("in");
            document.getElementById(option).style.display='none';
            document.getElementById('backgroundmodal').style.display='none';
            document.getElementsByTagName('body')[0].classList.remove('modal-open');
        }
    },
    
    backModal:function(component, event, helper) {
       var toggleBackdrop = component.find("backdrop");
       $A.util.addClass(toggleBackdrop, "toggle");
       component.set('v.incident', false);
       component.set('v.change', false);
       component.set('v.billingInquiry', false);
       component.set('v.query', false);
       component.set('v.complaint', false);
       component.set('v.creation', false);
       component.set('v.termination', false);
       
    },
    showdetails:function(component, event, helper) {
        document.getElementById('description-file').style.border='1px solid #d8dde6';
        document.getElementById('description-file').color='black';
        document.getElementById('commentText').style.border='1px solid #d8dde6';
        document.getElementById('commentText').color='black';
    	document.getElementById('backgroundmodal').style.display='block';
        var caseNumber = event.currentTarget.id;
        var detailTab = event.currentTarget.title.split(';')[1];
        component.set('v.detailTab', detailTab);
        var tableIndex = event.currentTarget.dataset.index;
        component.set('v.tableIndexDetail', tableIndex);
        var tContext;
        if(detailTab === 'Tickets'){
        	tContext = component.get("v.ticketsContext");
        }else{
        	tContext = component.get("v.ordersContext");
        }
        // Habilito/Deshabilito botones de next/previous item del modal de detalle
        // Previous
    	if(tContext.tableControl.iniIndex-1 === 0 && tableIndex === '0'){
    		document.getElementById('previousItemButton').classList.add('tlf-btn--disabled');
    		document.getElementById('previousItemButton').disabled = true;
    	}else{
    		document.getElementById('previousItemButton').classList.remove('tlf-btn--disabled');
    		document.getElementById('previousItemButton').disabled = false;
    	}
    	// Next
    	if(tContext.tableControl.iniIndex + +tableIndex >= tContext.tableControl.numRecords ){
    		document.getElementById('nextItemButton').classList.add('tlf-btn--disabled');
    		document.getElementById('nextItemButton').disabled = true;
    	}else{
    		document.getElementById('nextItemButton').classList.remove('tlf-btn--disabled');
    		document.getElementById('nextItemButton').disabled = false;
    	}
        helper.getDetails(component, event, helper, caseNumber);
    },
    
    cancelTicket:function(component, event, helper) {
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }
        var caseNumber = event.currentTarget.id;
        //document.getElementById('backgroundmodal').style.display='none'; 
        var action = component.get("c.setCancelTicket");    
        action.setParams({ caseNumber :caseNumber });
        action.setCallback(this, function(response) {
            if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
            if(component.isValid() && response.getState() == "SUCCESS"){      
                var res = response.getReturnValue();
                component.set('v.caseDetailed', res);
            }else{
                alert('Ha ocurrido un error cancelando el Ticket');
            }
        });
        $A.enqueueAction(action);       
    },
    reopenTicket:function(component, event, helper) {
        var caseNumber = event.currentTarget.id;
        //document.getElementById('backgroundmodal').style.display='none';
         if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='block';
            }
        var action = component.get("c.setReopenTicket");    
        action.setParams({ caseNumber :caseNumber });
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){      
                var res = response.getReturnValue();
                component.set('v.caseDetailed', res);
            }else{
                alert('Ha ocurrido un error reabriendo el Ticket');
            }
            
             if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);       
    },
    acceptTicket:function(component, event, helper) {
        var caseNumber = event.currentTarget.id;
        //document.getElementById('backgroundmodal').style.display='none';
         if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='block';
            }
        var action = component.get("c.setAcceptTicket"); 
        action.setParams({ caseNumber :caseNumber });
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){      
                var res = response.getReturnValue();
                component.set('v.caseDetailed', res);
            }else{
                alert('Ha ocurrido un error aceptando el Ticket');
            }
             if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);       
    },
    addComment:function(component, event, helper) {
    	
        var caseNumber = event.currentTarget.id;
        var comment = document.getElementById('commentText').value;
        if (comment == null || comment == ''){
            document.getElementById('commentText').style.border='2px solid red';
            document.getElementById('commentText').color='red';
            return;
        }
        if(document.getElementById("backgroundmodalwait")!=null){
            document.getElementById("backgroundmodalwait").style.display='block';
        }
    	var action = component.get("c.saveCommentCase");    
        action.setParams({ caseNumber :caseNumber, comment :comment });
        action.setCallback(this, function(response) {
            if(component.isValid() && response.getState() == "SUCCESS"){
                var res = response.getReturnValue();
                component.set('v.caseDetailedComments', res);
                document.getElementById('commentText').value = '';
            }
            if(document.getElementById("backgroundmodalwait")!=null){
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);    
    },
    closeModalAtt: function(component, event, helper) {
        document.getElementById('tlf-atachment-file').style.display="none";
        document.getElementById('tlf-detail').style.display="block";
    },
    saveAtt: function(component, event, helper) {
     	var listaAdjuntos = component.get("v.caseDetailedAttachments");
        var adjunto= document.getElementById("file-attach").files;
        var description = document.getElementById("description-file").value;
        
        if (description == null || description == ''){
            document.getElementById('description-file').style.border='2px solid red';
            document.getElementById('description-file').color='red';
            return;
        }

         if(adjunto.length >0){
             adjunto[0].aux="js";
             adjunto[0].Description = description;
             listaAdjuntos.push(adjunto[0]);
         }
         component.set("v.caseDetailedAttachments",listaAdjuntos);
         document.getElementById('tlf-atachment-file').style.display="none";
         document.getElementById('tlf-detail').style.display="block";
    },
    deleteAtt : function(component, event, helper) {
        var file = event.currentTarget.id; 
        var listaAdjuntos = component.get("v.caseDetailedAttachments");

        for(var i = listaAdjuntos.length - 1; i >= 0; i--) {
            if(listaAdjuntos[i].name === file) {
               listaAdjuntos.splice(i, 1);
            }
        }
        component.set("v.caseDetailedAttachments",listaAdjuntos);
    },
    saveAtt2Case: function(component, event, helper) {
        var caso = event.currentTarget.id; 
        helper.save(component, caso);
        // Refresco las tablas de comentarios y adjuntos
    },
    
    nextItem : function(component, event, helper){
    	var detailTab = component.get('v.detailTab');
    	var tableIndex = component.get('v.tableIndexDetail');
    	var nexTableIndex = +tableIndex + 1;
    	var tContext;
    	if(detailTab === 'Tickets'){
    		tContext = component.get('v.ticketsContext');
    	}else{
    		tContext = component.get('v.ordersContext');
    	}
    	// Si el índice de la tabla es menor que el size de la misma, no tengo que actualizar el tContext
		if(tableIndex < tContext.tableControl.viewSize-1){
			var caseNumber = tContext.casesList[nexTableIndex].CaseNumber;
			helper.getDetails(component, event, helper, caseNumber);
			component.set('v.tableIndexDetail', nexTableIndex);
			// Habilito/Deshabilito botones
			// Previous
			if(tContext.tableControl.iniIndex-1 === 0 && nexTableIndex === '0'){
        		document.getElementById('previousItemButton').classList.add('tlf-btn--disabled');
        		document.getElementById('previousItemButton').disabled = true;
        	}else{
        		document.getElementById('previousItemButton').classList.remove('tlf-btn--disabled');
        		document.getElementById('previousItemButton').disabled = false;
        	}
        	// Next
        	if(tContext.tableControl.iniIndex + nexTableIndex >= tContext.tableControl.numRecords ){
        		document.getElementById('nextItemButton').classList.add('tlf-btn--disabled');
        		document.getElementById('nextItemButton').disabled = true;
        	}else{
        		document.getElementById('nextItemButton').classList.remove('tlf-btn--disabled');
        		document.getElementById('nextItemButton').disabled = false;
        	}
		}else{
			// Si el índice del siguiente elemento de la tabla es mayor que el size de la misma, hay que recalcular el tContext y coger el index=0
			tContext.pagination="forward";
			component.set('v.tableIndexDetail', 0);
			helper.getNewPagination(component, event, helper, tContext, true);
		}
    },
    
    previousItem : function(component, event, helper){
    	var detailTab = component.get('v.detailTab');
    	var tableIndex = component.get('v.tableIndexDetail');
    	var prevTableIndex = +tableIndex - 1;
    	var tContext;
    	if(detailTab === 'Tickets'){
    		tContext = component.get('v.ticketsContext');
    	}else{
    		tContext = component.get('v.ordersContext');
    	}
    	// Si prevTableIndex está dentro de la tabla, no hay que actualizar el tContext
		if(prevTableIndex >= 0){
			var caseNumber = tContext.casesList[prevTableIndex].CaseNumber;
			helper.getDetails(component, event, helper, caseNumber);
			component.set('v.tableIndexDetail', prevTableIndex);
			// Habilito/Deshabilito botones
			// Previous
			if(tContext.tableControl.iniIndex-1 === 0 && prevTableIndex == 0){
        		document.getElementById('previousItemButton').classList.add('tlf-btn--disabled');
        		document.getElementById('previousItemButton').disabled = true;
        	}else{
        		document.getElementById('previousItemButton').classList.remove('tlf-btn--disabled');
        		document.getElementById('previousItemButton').disabled = false;
        	}
        	// Next
        	if(tContext.tableControl.iniIndex + prevTableIndex >= tContext.tableControl.numRecords ){
        		document.getElementById('nextItemButton').classList.add('tlf-btn--disabled');
        		document.getElementById('nextItemButton').disabled = true;
        	}else{
        		document.getElementById('nextItemButton').classList.remove('tlf-btn--disabled');
        		document.getElementById('nextItemButton').disabled = false;
        	}
		}else{
			// Si prevTableIndex == -1, habrá que recalcular el tContext para que coja la página anterior y el tableIndex se igualará al viewSize-1
			tContext.pagination="backward";
			var newIndex = tContext.tableControl.viewSize - 1;
			component.set('v.tableIndexDetail', newIndex);
			helper.getNewPagination(component, event, helper, tContext, true);
		}
    },
    caseOk: function(component, event, helper){
       helper.getTicketsContext(component, event, helper);
       helper.getOrdersContextNew(component, event, helper);
       var options=['incident', 'change', 'billingInquiry', 'query', 'complaint', 'creation', 'termination'];
       options.forEach(function(option) {
		 	component.set('v.'+option, false);
           if (document.getElementById(option) != null){
               document.getElementById(option).classList.remove("in");
               document.getElementById(option).style.display='none';               
           }
       });

       document.getElementById('backgroundmodal').style.display='none';
       document.getElementsByTagName('body')[0].classList.remove('modal-open');
       document.getElementById('tlf-case-ok').style.display='none';
       document.getElementById('tlf-case-ok').classList.remove("in");
	},  
    updateName : function(component, event, helper){
        var fileInput = document.getElementById("file-attach");
        var file = fileInput.files[0];
        document.getElementById("labelbutton").innerHTML=file.name;
    },
    
    openAttac : function(component, event, helper){
    	document.getElementById('tlf-atachment-file').style.display = 'block';
    	document.getElementById('tlf-atachment-file').classList.add('in');
    	document.getElementById('tlf-detail').style.display = 'none';
    },
    donwloadAttachmentJS : function(component, event, helper){
        var atts = component.get('v.caseDetailedAttachments');
        var at = atts[event.currentTarget.dataset.index];
        var body= 'data:application/octet-stream;base64,'+at.Body;
       	download(body,at.Name,at.ContentType);
    }, 
})