({
    updateDetails : function(component, event) {
        //document.getElementById('tabsb1master').click();
        var caseid = event.getParam("OiId");
        var id = '';
        var actionOrderId = component.get("c.getOrderItemFromCaseId");
        actionOrderId.setParams({ "caseid" : caseid });
        actionOrderId.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.orderId", response.getReturnValue());
                id = response.getReturnValue();
                
                
                component.set("v.title", "main Attributes");
                var action = component.get("c.getOrderItem");
                action.setParams({ "id" : id });
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        component.set("v.item", response.getReturnValue());
                        document.getElementById(id).click();
                    }
                });
                var action2 = component.get("c.getAccountHierarchy");
                action2.setParams({ "id" : id });
                action2.setCallback(this, function(response){
                    var state = response.getState();	
                    if (state === "SUCCESS") {
                        component.set("v.itemAccount", response.getReturnValue());
                    }
                });
                var action3 = component.get("c.getDetailsAttributes");
                action3.setParams({ "id" : id });
                action3.setCallback(this, function(response){
                    var state = response.getState();	
                    if (state === "SUCCESS") {
                        component.set("v.itemDetailAttr2", response.getReturnValue());
                    }
                });
                var action4 = component.get("c.hasSecondLevel");
                action4.setParams({ "id" : id });
                action4.setCallback(this, function(response){
                    var state = response.getState();	
                    if (state === "SUCCESS") {
                        component.set("v.hasSecondLevel", response.getReturnValue());
                    }
                });
                var action5 = component.get("c.getProductsNames");
                action5.setParams({ "id" : id });
                action5.setCallback(this, function(response){
                    var state = response.getState();	
                    if (state === "SUCCESS") {
                        var ret = [];
                        var aux = response.getReturnValue();
                        var index = 0;
                        for (var i = 0 ; i < aux.length ; i++){
                            if (i > 0){
                                if (aux[i].TGS_Product_Name__c == ret[index-1].TGS_Product_Name__c){
                                    ret[index-1].Name = aux[i].Name +','+ ret[index-1].Name;
                                }else{
                                    ret[index]=aux[i];
                                    index ++;
                                }
                            }else{
                                ret[index]=aux[i];
                                index ++;
                            }
                        }                                                
                        component.set("v.itemNamesLevel2", ret);
                    }
                });
                
                $A.enqueueAction(action);
                $A.enqueueAction(action2);
                $A.enqueueAction(action3);
                $A.enqueueAction(action4);
                $A.enqueueAction(action5);
            }
        });
        $A.enqueueAction(actionOrderId);
    },
    secondAttrs : function(component, event){
        debugger;
        component.set("v.titlehead", $A.get("$Label.c.CWP_SecondLevelAtt"));
        var action3 = component.get("c.getDetailsAttributes2");
        action3.setParams({ "id" : event.currentTarget.id });
        component.set("v.display", 'display:none');
        action3.setCallback(this, function(response){
            var state = response.getState();	
            if (state === "SUCCESS") {
                var a =response.getReturnValue();
                component.set("v.OisDetail", a);
                component.set("v.showOi", true);
                
            }
        });
        $A.enqueueAction(action3);
        
    },
    secondAttrsDetails : function(component, event){
        var action3 = component.get("c.getDetailsAttributes2Detailed");
        
        debugger;
        component.set("v.titlehead", event.currentTarget.text);
        component.set("v.display", 'display:none');
        var id = event.currentTarget.id;
        action3.setParams({ "id" : id });
        action3.setCallback(this, function(response){
            var state = response.getState();	
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                component.set("v.OisDetail",data);
                component.set("v.showOi", true);
            }	
        });
        $A.enqueueAction(action3);
    },
    updateMainDetails : function(component, event){
        component.set("v.titlehead", $A.get("$Label.c.CWP_DetailAttributes"));
        var id = event.currentTarget.id;
        var action3 = component.get("c.getDetailsAttributes");
        action3.setParams({ "id" : id });
        component.set("v.title", "main Attributes");
        component.set("v.display", 'display:none');
        action3.setCallback(this, function(response){
            debugger;
            var state = response.getState();	
            
            if (state === "SUCCESS") {
                var a =response.getReturnValue();
                component.set("v.showOi", false);
                component.set("v.OisDetail", a);
                component.set("v.titlehead", component.get("v.item").NE__ProdName__c+' '+ $A.get("$Label.c.CWP_DetailAttributes"));
            }
        });
        $A.enqueueAction(action3);
    },
    selectedAttrib : function(component, event){
        debugger;
        var id = event.currentTarget.selectedOptions["0"].id;
        var index = event.currentTarget.selectedOptions["0"].index;
        var text = event.currentTarget.selectedOptions["0"].text;
        debugger;
        
        if(index==0){
            console.log('id->>'+id);
           document.getElementById(id).click(); 
        }else if (index==1){
   console.log('id->>'+id);
            document.getElementById("s2"+id).click();
        }else{  
            console.log('id->>'+id);
            document.getElementById(id).click();
        }
        
    },
    closeModal : function(component, event, helper) {
        document.getElementById('backgroundmodal').style.display='none';
    },
    
})