({
	updateMatrix : function(component, event, helper) {
        // Fire event to update complexity matrix
        var cmpEvent = $A.get("e.c:E3_Matriz_Complejidad_SubmitEvent");
        cmpEvent.setParams({
            "message" : "Actualizar matriz de complejidad" });
        cmpEvent.fire(); 
	},
    
    setSubTab : function(component, event, helper) {
    	var subTabSelected = event.getParam("subHeader");
        switch(subTabSelected){
            case "commAvanz":
                component.set("v.selected1", subTabSelected);
                break;
            case "datosf":
                component.set("v.selected1", subTabSelected);
                break;
            case "datosMoviles":
                component.set("v.selected1", subTabSelected);
                break;
            case "networking":
                component.set("v.selected1", subTabSelected);
                break;
            case "teminalesMov":
                component.set("v.selected1", subTabSelected);
                break;
            case "tvVideo":
                component.set("v.selected1", subTabSelected);
                break;
            case "servTelefMovil":
                component.set("v.selected2", subTabSelected);
                break;
            case "sms":
                component.set("v.selected2", subTabSelected);
                break;
            case "otros":
                component.set("v.selected2", subTabSelected);
                break;
            case "cloud":
                component.set("v.selected3", subTabSelected);
                break;
            case "contenidoAplicaciones":
                component.set("v.selected3", subTabSelected);
                break;
            case "ehealth":
                component.set("v.selected3", subTabSelected);
                break;
            case "iot":
                component.set("v.selected3", subTabSelected);
                break;
            case "outsourcing":
                component.set("v.selected3", subTabSelected);
                break;
            case "puestoTrabajo":
                component.set("v.selected3", subTabSelected);
                break;
            case "seguridad":
                component.set("v.selected3", subTabSelected);
                break;
            case "servFinancieros":
                component.set("v.selected3", subTabSelected);
                break;
            case "ted":
                component.set("v.selected3", subTabSelected);
                break;
            case "tis":
                component.set("v.selected3", subTabSelected);
        }
	}
})