({
        searchHelper : function(cmp, event,helper, getInputkeyWord) {
        var accid=cmp.get('v.account').Id;
        var action = cmp.get("c.customSearch");
        console.log('accid-->'+accid+' keyword-->'+getInputkeyWord);
      action.setParams({
            'searchKeyWord': getInputkeyWord,
             'accountId': accid,
          });
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                cmp.set('v.allEvents', response.getReturnValue());
                helper.filterEvents(cmp);
            }
            else{
                //alert('error');
            }
        });
        $A.enqueueAction(action);
    },
       getEventsByAccountOrderBy : function(cmp, helper,event, sortFieldName) {
            var getInputkeyWord = cmp.get("v.SearchKeyWord");
            var accid=cmp.get('v.account').Id;
        	var isascen=cmp.get('v.isAsc');
            var sc = cmp.get('v.selectedTabsoft');
           //////
       if( getInputkeyWord.length > 0 ){     
             var action = cmp.get("c.customSearchAndOrder");
            action.setParams({
            'accountId': accid,
            'fieldToOrder' : sortFieldName,
            'searchKeyWord' :getInputkeyWord,
           'isASC' : isascen,
                'fieldToOrder' : sc,
          });
                   action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                cmp.set('v.allEvents', response.getReturnValue());
                helper.filterEvents(cmp);
            }
            else{
                //alert('error');
            }
        });
        $A.enqueueAction(action);
        }
        else{  
             
           //////
       console.log('getEventsByAccountOrderBy');
        var action = cmp.get("c.getEventByAccountOrderASC");
        console.log(sortFieldName);
        
        
           console.log('ACCOUNT just before query'+accid+' ISASC: '+isascen);
       action.setParams({
            'accountId': accid,
            'fieldToOrder' : sortFieldName,
            'dir' : 'ASC',
           'isASC' : isascen
          });
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                cmp.set('v.allEvents', response.getReturnValue());
                helper.filterEvents(cmp);
            }
            else{
                //alert('error');
            }
        });
        $A.enqueueAction(action);
     }       
    },
    getEventsByAccount : function(cmp, helper) {
        var action = cmp.get("c.getEventByAccount");
        action.setParams({accountId:cmp.get('v.account').Id});
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                cmp.set('v.allEvents', response.getReturnValue());
                helper.filterEvents(cmp);
            }
            else{
                //alert('error');
            }
        });
        $A.enqueueAction(action);
    },
   
    filterEvents: function(cmp){
        var events = [];
        cmp.set('v.noData', true);
        
        var j = 0;
        var allEvents = cmp.get('v.allEvents');
        var showCaducadas = cmp.get('v.showCaducadas');
        var showFinalizadas = cmp.get('v.showFinalizadas');
        
        for (var i=0; i<allEvents.length; i++){
            if (showCaducadas===true){
                events[j]=allEvents[i];
                j++;
            }
            else{
                if (allEvents[i].BI_G4C_Caducada__c===false){
                    events[j]=allEvents[i];
                    j++;
                }
            }
        }
        
        var auxEvents = events;
        events = [];
        j=0;
        
        for (i=0; i<auxEvents.length; i++){
            if (showFinalizadas===false){
                if (auxEvents[i].BI_FVI_Estado__c!=='Cualificada'){
                    events[j]=auxEvents[i];
                    j++;
                }
            }
            else{
                events[j]=auxEvents[i];
                j++;
            }
        }
        
        if(events.length !== 0) cmp.set('v.noData', false);
        cmp.set('v.events', events);
    },
    showVisitPanel: function(cmp, eventId){
        var x = function(a){
            //console.log(a);
            //by JB 29/07
        };
        var account = cmp.get('v.account');
        var evento = {
            Location: account.BillingStreet,
            WhatId: account.Id
        };
        if (eventId){
            evento = {
                Id: eventId,
                Location: account.BillingStreet,
                WhatId: account.Id
            };
        }
        $A.createComponent(
            "c:BI_G4C_Visitas_Nueva",
            {
                evento: evento,
                auxData: {name: account.Name, pais: account.BI_Country__c} /*Modificacion*/
            },
            function(newCmp,status){
                var modalfilter = cmp.find('BI_G4C_Visitas_Nueva_id');
                $A.util.removeClass(modalfilter,'slds-hide');
                var body = cmp.get("v.body");
                body.push(newCmp);
                cmp.set("v.body", body);
                cmp.set("v.clicked", false);
            }
        );
    },
    
    removeVisit: function(cmp, evt, helper){
        
        var event = cmp.get('v.eventoAEliminar');
        
        var action = cmp.get("c.deleteVisita");
        action.setParams({eventId: event.Id});
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                //console.log(response);
                var res = response.getReturnValue();
                if (res === 'OK' ){
                    helper.getEventsByAccount(cmp, helper);
                    helper.hideConfirm(cmp);
                }
            }
            else{
                //alert('error');
                cmp.set('v.errorMessageHeader','Error');
                cmp.set('v.errorMessageBody','Error');
                
                var element = cmp.find('generic_error');
                $A.util.removeClass(element, 'slds-hide');
            }
        });
        $A.enqueueAction(action);
    },
    
    hideConfirm: function(cmp){
        var confirm = cmp.find('confirm-delete');
        $A.util.addClass(confirm, 'slds-hide');
    },
    hideBack: function(cmp){
        var bck = cmp.find('backButton');
        $A.util.addClass(bck,'slds-hide');
    },
    setDataInit : function(cmp,helper) {
        var id = window.location.href.split('&id=')[1].substring(0,18);     
            if(window.location.href.split('&tour').length > 1 ){
                id = id.split('&tour')[0];
            }
            cmp.set('v.showCaducadas', true);
            cmp.set("v.showFinalizadas", true);
            
            var action = cmp.get("c.getAccountById");
            action.setParams({accountId:id});
            action.setCallback(this, function(response) {
                if (cmp.isValid() && response.getState() === "SUCCESS") {
                    cmp.set('v.account', response.getReturnValue());
                    helper.getEventsByAccount(cmp, helper);
                }
            });
            $A.enqueueAction(action); 
    },
    
    sortHelper: function(component, event,helper, sortFieldName) {
       
      console.log('sorthelper');
      var currentDir = component.get("v.arrowDirection");
 	  
      if (currentDir == 'arrowdown') {
         // set the arrowDirection attribute for conditionally rendred arrow sign  
         component.set("v.arrowDirection", 'arrowup');
         // set the isAsc flag to true for sort in Assending order.  
         component.set("v.isAsc", true);
      } else {
         component.set("v.arrowDirection", 'arrowdown');
         component.set("v.isAsc", false);
      }
      // call the onLoad function for call server side method with pass sortFieldName 
      this.getEventsByAccountOrderBy(component,helper, event, sortFieldName);
   },

})