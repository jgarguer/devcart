({
init: function (component, event, helper) {
    component.set('v.isLoadingTodos', true);
     component.set('v.isLoadingPriorizados', true);
        component.set('v.columnsTodosClientes', [
            {label: 'Nombre del Cliente', fieldName: 'E24P_Cliente__c', type: 'button', sortable:'true', typeAttributes:
             {variant: 'base', label: {fieldName:'E24P_Cliente__c'}}},
            {label: 'Comercial', fieldName: 'E24P_Comercial__c', type: 'text', sortable:'true'},
            {label: 'Jefe', fieldName: 'E24P_Jefe__c', type: 'text', sortable:'true'},
            {label: 'Ingr. Est. Año en Curso (EAI)', fieldName: 'E24P_Ing_Est_Ano_en_Curso_EAI__c', sortable:'true', type: 'percent', typeAttributes: { maximumFractionDigits: '0'}},
            {label: 'Priorizada', fieldName: 'E24P_Priorizado__c', type: 'boolean', editable:'true', sortable:'true'}
        ]);
    
    component.set('v.columnsPriorizados', [
            {label: 'Nombre del Cliente', fieldName: 'E24P_Cliente__c', type: 'button',typeAttributes:
             {variant: 'base', label: {fieldName:'E24P_Cliente__c'}}},
            {label: 'Comercial', fieldName: 'E24P_Comercial__c', type: 'text'},
            {label: 'Jefe', fieldName: 'E24P_Jefe__c', type: 'text'},
       	    {label: 'Última modificación', fieldName: 'LastModifiedDate', type: 'date'}
        ]);
    
    
            var action = component.get('c.doFetchAccPlan');
        action.setCallback(this, function(response) {
            
            if (response.getState() == 'SUCCESS') {
                console.log("RESPONSE", response.getReturnValue());
                var data = response.getReturnValue();
                for(var i=0; i<data.length; i++){
                    data[i].E24P_Cliente__c=data[i].E24P_Cliente__r.Name;
                    data[i].E24P_Ing_Est_Ano_en_Curso_EAI__c = data[i].E24P_Ing_Est_Ano_en_Curso_EAI__c/100;
                }
                component.set('v.dataTodosClientes',data);
                helper.sortDataTodos(component, 'E24P_Ing_Est_Ano_en_Curso_EAI__c', 'desc');
                component.set('v.isLoadingTodos', false);
            } else if (response.getState() == 'ERROR') {
                console.log("Error al obtener los datos de la DataTable");
            }
        });
        $A.enqueueAction(action);
    
                var actionP = component.get('c.doFetchAccPlanPriorizados');
        actionP.setCallback(this, function(response) {
            
            if (response.getState() == 'SUCCESS') {
                console.log("RESPONSE", response.getReturnValue());
                var dataP = response.getReturnValue();
                for(var i=0; i<dataP.length; i++){
                    dataP[i].E24P_Cliente__c=dataP[i].E24P_Cliente__r.Name;
                }
                component.set('v.dataPriorizados',dataP);
                component.set('v.isLoadingPriorizados', false);
            } else if (response.getState() == 'ERROR') {
                console.log("Error al obtener los datos de la DataTable");
            }
        });
        $A.enqueueAction(actionP);
   
},
    
        handleSave: function (component, event, helper) {
        
        helper.saveDataTable(component, event, helper);
    },
    
       updateColumnSortingTodos: function (component, event, helper) {
        component.set('v.isLoadingTodos', true);
        setTimeout(function() {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortedByTodos", fieldName);
        component.set("v.sortedDirectionTodos", sortDirection);
        helper.sortDataTodos(component, fieldName, sortDirection);
        component.set('v.isLoadingTodos', false);
        }, 0);
       }
    
})