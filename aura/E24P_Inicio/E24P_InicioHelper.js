( {
    saveDataTable : function(component, event, helper) {
        var editedRecords =  event.getParam('draftValues');
        var totalRecordEdited = editedRecords.length;
        console.log('Total Record Edited: ' + totalRecordEdited);
        var action = component.get("c.doUpdateAccPlan");
        action.setParams({
            "editedAccPlanList":editedRecords
        });       
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() === true){
                    helper.showToast({
                        "title": totalRecordEdited+" Registros actualizados",
                        "type": "success",
                        "message" :" "
                    });
                    helper.reloadDataTable();
                } else{ //if update got failed
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error al actualizar"
                    });
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    
    /*
     * Show toast with provided params
     * */
    showToast : function(params){
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(params);
            toastEvent.fire();
        } else{
            alert(params.message);
        }
    },
    reloadDataTable : function(){
        var refreshEvent = $A.get("e.force:refreshView");
        if(refreshEvent){
            refreshEvent.fire();
        }
        
    },
    
    sortDataTodos: function (component, fieldName, sortDirection) {
        var data = component.get("v.dataTodosClientes");
        var reverse = sortDirection !== 'asc';
        
        data = Object.assign([],data.sort(this.sortBy(fieldName, reverse ? -1 : 1)));
        component.set("v.dataTodosClientes", data);
    },
    
    sortBy: function (field, reverse, primer) {
        var key = primer
        ? function(x) { return primer(x[field]) }
        : function(x) { return x[field] };
        
        return function (a, b) {
            var A = key(a);
            var B = key(b);
            return reverse * ((A > B) - (B > A));
        };
    },
})