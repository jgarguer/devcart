({
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier López 
    Company:       GIT
    Description:   Do Init to preCharge all needed fields
    
    IN:            9 String with the fields that are necessary to create a new User
    OUT:           Roles
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    26/11/2016              Javier López Andradas    Initial Version
    17/02/2017              Javier López Andradas    Sorting List and new Error Messajes
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	 doInit: function(component, evt, helper) {
    
    component.set("v.licenciaRes","");

    var action = component.get("c.getRoles");
    action.setParams({"prima":"prima"});
    action.setCallback(this, function(response) {
      console.log("Roles status "+ response.getState());
      if(component.isValid() && response.getState()==="SUCCESS"){
        var Roles = response.getReturnValue();
        var nullable = new Object();
        nullable.Id = null;
        nullable.Name = null;
        var list = [];
        list.push(nullable);
        for(var i = 0;i<Roles.length;i++){
              var rol = new Object();
              rol.Id = Roles[i].Id;
              rol.Name = Roles[i].Name;
              list.push(rol);
        }

        component.set("v.auxRole",list);
      }
    
    });
    $A.enqueueAction(action); 

    var action2 = component.get("c.getProfiles");
    action2.setParams({"dos":"dos"});
    action2.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var Profiles = response.getReturnValue();
        var list2 = [];
        var nullable = new Object();
        nullable.Id = null;
        nullable.Name = null;
        list2.push(nullable);
        for(var j=0;j<Profiles.length;j++){
          var profile = new Object();
          profile.Id=Profiles[j].Id;
          profile.Name=Profiles[j].Name;
          list2.push(profile);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.auxProfile",list2);
    });
    $A.enqueueAction(action2); 

    var action3 = component.get("c.getPermisosValues");
    action3.setParams({"dos":"dos"});
    action3.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var permisos = response.getReturnValue();
        var list2 = [];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<permisos.length;j++){
          var permiso = new Object();
          permiso.value=permisos[j].value;
          permiso.label=permisos[j].label;
          list2.push(permiso);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }

      component.set("v.Permisos",list2);
    });
    $A.enqueueAction(action3); 

    var action4 = component.get("c.getPaisesValues");
    action4.setParams({"dos":"dos"});
    action4.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var paises = response.getReturnValue();
        var list2 = [];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<paises.length;j++){
          var pais = new Object();
          pais.value=paises[j].value;
          pais.label=paises[j].label;
          list2.push(pais);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.Paises",list2);
    });
    $A.enqueueAction(action4); 

    var list4 = []
    var manager =new Object();
    manager.NombreApellido = "--Ninguno--";
    manager.UserName="";
    list4.push(manager);
    component.set("v.Managers",list4);


    var action5 = component.get("c.getDivisas");
    action5.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var divisa = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<divisa.length;j++){
          var dv = new Object();
          dv.value=divisa[j].value;
          dv.label=divisa[j].value+" - "+divisa[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.Divisas",list2);
    });
    $A.enqueueAction(action5);


    var action6 = component.get("c.getLeguage");
    action6.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var divisa = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<divisa.length;j++){
          var dv = new Object();
          dv.value=divisa[j].value;
          dv.label=divisa[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.Lenguages",list2);
      component.find("Lenguages-ui").set("v.value","es");

    });
    $A.enqueueAction(action6);

    var action7 = component.get("c.regionalConfig");
    action7.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var divisa = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<divisa.length;j++){
          var dv = new Object();
          dv.value=divisa[j].value;
          dv.label=divisa[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.LocaleSidKeys",list2);
      component.find("LocaleSidKey-ui").set("v.value","es_ES");
    });
    $A.enqueueAction(action7);
         
    var action8 = component.get("c.timeZone");
    action8.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var divisa = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<divisa.length;j++){
          var dv = new Object();
          dv.value=divisa[j].value;
          dv.label=divisa[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.TimeZoneSidKeys",list2);
      component.find("TimeZoneSidKey-ui").set("v.value","Europe/Paris");
    });
    $A.enqueueAction(action8);
    
    var action9 = component.get("c.SegmentoRegional");
    action9.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var segmentos = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<segmentos.length;j++){
          var dv = new Object();
          dv.value=segmentos[j].value;
          dv.label=segmentos[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.Segmentos",list2);
    });
    $A.enqueueAction(action9);
    
    var action10 = component.get("c.emailCode");
    action10.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var segmentos = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value=null;
        nullable.label=null;
        list2.push(nullable);
        for(var j=0;j<segmentos.length;j++){
          var dv = new Object();
          dv.value=segmentos[j].value;
          dv.label=segmentos[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.EmailCodes",list2);
      component.find("EmailCode-ui").set("v.value","ISO-8859-1");
    });
    $A.enqueueAction(action10);
    var action11 = component.get("c.getClasificacion");
    action11.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        var segmentos = response.getReturnValue();
        var list2 =[];
        var nullable = new Object();
        nullable.value = null;
        nullable.label = null;
        list2.push(nullable);
        for(var j=0;j<segmentos.length;j++){
          var dv = new Object();
          dv.value=segmentos[j].value;
          dv.label=segmentos[j].label;
          list2.push(dv);
        }
        list2.sort(function(a,b){
          if(a.label>b.label){
            return 1;
          }else{
            return -1;  
          }
        });
      }
      component.set("v.Clasificaciones",list2);

    });
    $A.enqueueAction(action11);

    component.find("textarea").set("v.value","Por favor arrastre un fichero csv, separado por comas");
    component.set("v.Create","true");
    component.find("button").set("v.label","Crear");
    //component.set("v.campos","hola");

  },

  selectedRoleOrCountry: function(component,event,helper){
    helper.findManager(component);
  },
  fillAliasField: function(component,event,helper){
    var FName= component.find("FName-fd").get("v.value");
    var lName = component.find("lName-fd").get("v.value");
    if((FName!="undefined" && FName!=null) && (lName!="undefined" && lName!=null)){
      var out = "";
      for(var j = 0;j<4;j++){
        if(FName.charAt(j)!=null)
          out=out+FName.charAt(j);
      }
      for(var j =0;j<4;j++){
        if(lName.charAt(j)!=null)
          out=out+lName.charAt(j);
      }
      component.find("Alias-fd").set("v.value",out);
    }
  },

  checkUserName: function(component,event,helper){
    helper.checkUSerName(component,helper);
  },

  fillUserName: function(component,event,helper){
    var email= component.find("email-input").get("v.value");
    if(component.get("v.Create")=="true"){
      component.find("UserName-input").set("v.value",email);
      helper.checkUSerName(component,helper);
    }
  },
  darPermisos: function(component,event,helper){
    var per= component.find("allPer-ui").get("v.value").split(";");
    var allPermisos = component.get("v.allPermisos");
    var asigPer = component.get("v.selectPer");
    for(var j  =0;j<per.length;j++){
      for(var i =0;i<allPermisos.length;i++){
        if(allPermisos[i].Id == per[j]){
          console.log(allPermisos[i]);
          asigPer.push(allPermisos[i]);
          allPermisos.splice(i,1);
        }
      }
    }
    asigPer.sort(function(a, b){
      var NameA = a.Name.toUpperCase();
      var NameB = b.Name.toUpperCase();
        if(NameA>NameB){
          return 1;
        }
        if(NameA<NameB){
          return -1;
        }
          return 0;
        });
    component.set("v.allPermisos",allPermisos);
    component.set("v.selectPer",asigPer);
  },
  quitarPermisos: function(component,event,helper){
    var per= component.find("TempPer-ui").get("v.value").split(";");
    var allPermisos = component.get("v.allPermisos");
    var asigPer = component.get("v.selectPer");
    for(var j  =0;j<per.length;j++){
      for(var i =0;i<asigPer.length;i++){
        if(asigPer[i].Id == per[j]){
          allPermisos.push(asigPer[i]);
          asigPer.splice(i,1);
        }
      }
    }
    allPermisos.sort(function(a, b){
      var NameA = a.Name.toUpperCase();
      var NameB = b.Name.toUpperCase();
        if(NameA>NameB){
          return 1;
        }
        if(NameA<NameB){
          return -1;
        }
          return 0;
        });
    component.set("v.allPermisos",allPermisos);
    component.set("v.selectPer",asigPer);
  },
  darQueue: function(component,event,helper){
    var queue= component.find("allQueue-ui").get("v.value").split(";");
    var allQueue = component.get("v.allQueues");
    var asigQueue = component.get("v.QueuesUser");
    for(var j  =0;j<queue.length;j++){
      for(var i =0;i<allQueue.length;i++){
        if(allQueue[i].Id == queue[j]){
          asigQueue.push(allQueue[i]);
          allQueue.splice(i,1);
        }
      }
    }
    asigQueue.sort(function(a, b){
      var NameA = a.Name.toUpperCase();
      var NameB = b.Name.toUpperCase();
        if(NameA>NameB){
          return 1;
        }
        if(NameA<NameB){
          return -1;
        }
          return 0;
        });
    component.set("v.allQueues",allQueue);
    component.set("v.QueuesUser",asigQueue);
  },
  quitarQueue: function(component,event,helper){
    var queue= component.find("TempQueue-ui").get("v.value").split(";");
    var allQueue = component.get("v.allQueues");
    var asigQueue = component.get("v.QueuesUser");
    for(var j  =0;j<queue.length;j++){
      for(var i =0;i<asigQueue.length;i++){
        if(asigQueue[i].Id == queue[j]){
          allQueue.push(asigQueue[i]);
          asigQueue.splice(i,1);
        }
      }
    }
    allQueue.sort(function(a, b){
      var NameA = a.Name.toUpperCase();
      var NameB = b.Name.toUpperCase();
        if(NameA>NameB){
          return 1;
        }
        if(NameA<NameB){
          return -1;
        }
          return 0;
        });
    component.set("v.allQueues",allQueue);
    component.set("v.QueuesUser",asigQueue);
  },
  asignarQueue: function(component,event,helper){
    var list_Id=[];
    var asigQueue = component.get("v.QueuesUser");
    for(var i =0;i<asigQueue.length;i++){
      console.log("Estas son las colas a asignar: "+asigQueue[i].Name);
        list_Id.push(asigQueue[i].Id);
    }
    var action = component.get("c.setQueue");
    action.setParams({"IdQueue":list_Id,"UserName":component.find("UserName-input").get("v.value")});
    action.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        if(response.getReturnValue()=='OK'){
          alert('Colas del usuario actualizadas correctamente');
        }else{
          alert('Error al actualizar las colas del usuario:\n'+response.getReturnValue());
        }
      }
    });
    $A.enqueueAction(action);
  },
  asignarPermisos: function(component,event,helper){
    var list_Id=[];
    var asiPer = component.get("v.selectPer");
    for(var i =0;i<asiPer.length;i++){
        list_Id.push(asiPer[i].Id);
    }
    var action = component.get("c.setPermissionsSet");
    action.setParams({"IdPer":list_Id,"UserName":component.find("UserName-input").get("v.value")});
    action.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS"){
        if(response.getReturnValue()=='OK'){
          alert('Permisos del usuario actualizados correctamente');
        }else{
          alert('Error al actualizar los permisos del usuario:\n'+response.getReturnValue());
        }
      }
    });
    $A.enqueueAction(action);
  },

  showLic : function(component,event,helper){
      var lic = component.find("Clasificacion-ui").get("v.value");
      var pr = component.find("fProfile-ui").get("v.value");
      var lst_per = component.get("v.auxProfile");
      var prName ="";
      for(var i=0;i<lst_per.length;i++){
        if(lst_per[i].Id==pr)
          prName=lst_per[i].Name;
      }
      var action = component.get("c.leftLic");
      action.setParams({"lic":lic});
      action.setCallback(this,function(response){
        if(component.isValid()&& response.getState()==="SUCCESS"){
          component.set("v.licenciaRes",response.getReturnValue());
        }
      });
      $A.enqueueAction(action);
      if(prName!=null && lic!=null){
        if(prName.indexOf("Chatter")==-1){
          if(lic.indexOf("Chatter")!=-1){
            alert("No se puede escoger un perfil de licencia Salesforce para este tipo de clasificacion, por favor escoge un perfil de Chatter");
            component.find("fProfile-ui").set("v.value","");
          }
        }else{
           if(lic.indexOf("Chatter")==-1){
             alert("No se puede escoger una licencia de Chatter para este tipo de clasificacion, por favor escioge la clasificacion de chatter");
             component.find("Clasificacion-ui").set("v.value","");
             component.set("v.licenciaRes","");
           }
        }
      }
      if(lic.indexOf("Chatter")!=-1){
        component.find("fRole-ui").set("v.value","")
      }
  },
  perfCla : function(component,event,helper){
    var lic = component.find("Clasificacion-ui").get("v.value");
    var pr = component.find("fProfile-ui").get("v.value");
    var lst_per = component.get("v.auxProfile");
    var prName ="";
    for(var i=0;i<lst_per.length;i++){
      if(lst_per[i].Id==pr)
        prName=lst_per[i].Name;
    }
    if(prName!=null && lic!=null){
        if(prName.indexOf("Chatter")==-1){
          if(lic.indexOf("Chatter")!=-1){
            alert("No se puede escoger un perfil de licencia Salesforce para este tipo de clasificacion, por favor escoge un perfil de Chatter");
            component.find("fProfile-ui").set("v.value","");
          }
        }else{
           if(lic.indexOf("Chatter")==-1){
             alert("No se puede escoger una licencia de Chatter para este tipo de clasificacion, por favor escioge la clasificacion de chatter");
             component.find("Clasificacion-ui").set("v.value","");
             component.set("v.licenciaRes","");
           }
        }
      }

  },
  cancel: function(component,event,helper){
       component.set("v.Create",true);
       component.set("v.licenciaRes","");
       component.find("email-input").set("v.value","");
       component.find("button").set("v.label","Crear");
       component.find("UserName-input").set("v.value","");
       component.find("Cargo-input").set("v.value","");
       component.find("Company-input").set("v.value","");
       component.find("Departament-input").set("v.value","");
       component.find("FName-fd").set("v.value","");
       component.find("lName-fd").set("v.value","");
       component.find("Alias-fd").set("v.value","");
       component.find("telefono-input").set("v.value","");
       component.find("movil-input").set("v.value","");
       component.find("fRole-ui").set("v.value","");
       component.find("fProfile-ui").set("v.value","");
       component.find("Paises-ui").set("v.value","");
       component.find("Permisos-ui").set("v.value","");
       component.find("Manager-ui").set("v.value","");
       component.find("Division-input").set("v.value","");
       component.find("TimeZoneSidKey-ui").set("v.value","");
       component.find("Lenguages-ui").set("v.value","");
       component.find("Divisas-ui").set("v.value","");
       component.find("Apodo-fd").set("v.value","");
       component.find("LocaleSidKey-ui").set("v.value","");
       component.find("Active").set("v.value",true);
       component.find("ResetPass").set("v.value",false);
       component.find("Service").set("v.value",false);
       component.find("Marketing").set("v.value",false);
       component.find("DNI-fd").set("v.value","");
       component.find("PERSTC-fd").set("v.value","");
       component.find("PERSMC-fd").set("v.value","");
       component.find("Segmento-ui").set("v.value","");
       component.find("Clasificacion-ui").set("v.value","");
       component.find("fProfile-ui").set("v.label","Perfil*");
       component.find("Paises-ui").set("v.label","Pais*");
       component.find("Permisos-ui").set("v.label","Permisos*")
       component.find("TimeZoneSidKey-ui").set("v.label","Zona horaria*");
       component.find("LocaleSidKey-ui").set("v.label","Configuración local*");
       component.find("Lenguages-ui").set("v.label","Idioma*");
       component.find("Divisas-ui").set("v.label","Divisa*");
       component.find("EmailCode-ui").set("v.label","Codificación email*");
       component.find("fRole-ui").set("v.label","Función*");
       component.find("Clasificacion-ui").set("v.label","Clasificacion del Usuario *");
       $A.util.addClass(component.find("MessageActive"),"slds-hide");
       $A.util.addClass(component.find("Per-lab"),"slds-hide");
       $A.util.addClass(component.find("Per-lay"),"slds-hide");
       $A.util.addClass(component.find("Per-head"),"slds-hide");
       $A.util.addClass(component.find("Queue-lay"),"slds-hide");
       $A.util.addClass(component.find("Queue-head"),"slds-hide");
       component.find("textarea").set("v.value","Por favor arrastre un fichero csv, separado por comas");
       $A.util.removeClass(component.find("File-id"),"slds-hide");
  },
  createUpdate: function(component,event,helper){

    var create = component.get("v.Create");
    var email = component.find("email-input").get("v.value");
    var UsrName = component.find("UserName-input").get("v.value");
    var area = component.find("area-input").get("v.value");
    var region = component.find("region-input").get("v.value");
    var zona = component.find("zona-input").get("v.value");
    var cartera = component.find("cartera-input").get("v.value");
    var Title = component.find("Cargo-input").get("v.value");
    var Company = component.find("Company-input").get("v.value");
    var Departament = component.find("Departament-input").get("v.value");
    var fName = component.find("FName-fd").get("v.value");
    var lName = component.find("lName-fd").get("v.value");
    var Alias = component.find("Alias-fd").get("v.value");
    var telefono = component.find("telefono-input").get("v.value");
    var movil = component.find("movil-input").get("v.value");
    var RoleId = component.find("fRole-ui").get("v.value");
    var ProfileId = component.find("fProfile-ui").get("v.value");
    var Country = component.find("Paises-ui").get("v.value");
    var Permiso = component.find("Permisos-ui").get("v.value");
    var managerUsrName = component.find("Manager-ui").get("v.value");
    var Division = component.find("Division-input").get("v.value");
    var TimeZone = component.find("TimeZoneSidKey-ui").get("v.value");
    var Lenguage = component.find("Lenguages-ui").get("v.value");
    var Divisa = component.find("Divisas-ui").get("v.value");
    var Apodo = component.find("Apodo-fd").get("v.value");
    var TimeZoneLo = component.find("LocaleSidKey-ui").get("v.value");
    var isActive = component.find("Active").get("v.value");
    var resetPass =  component.find("ResetPass").get("v.value");
    var emailEncode =  component.find("EmailCode-ui").get("v.value");
    var DNI = component.find("DNI-fd").get("v.value");
    var PERSTC = component.find("PERSTC-fd").get("v.value");
    var PERSMC = component.find("PERSMC-fd").get("v.value");
    var Segmento = component.find("Segmento-ui").get("v.value");
    var service = component.find("Service").get("v.value");
    var marketing = component.find("Marketing").get("v.value");
    var clasficacion = component.find("Clasificacion-ui").get("v.value");
    var tiwsInformationArray = new Array (area,region,zona,cartera);
    for (var i = 0; i < tiwsInformationArray.length; i++) {
        if (tiwsInformationArray[i] == null){
            tiwsInformationArray[i] = ' ';
        }     
     }    
    var tiwsInformation = tiwsInformationArray.join("|*|").concat("|*|");
      console.log ('tiwsInformationArray = ' + tiwsInformationArray)
      console.log ('tiwsInformation1 = ' + tiwsInformation)
    if(clasficacion=="Chatter" && RoleId!=null){
      alert("Los usuarios de Chatter no pueden tener un rol asociado");
      component.find("fRole-ui").set("v.value","");
    }else{
      if ((create=="true"  && (email==null || UsrName==null || lName==null || Alias==null ||  ProfileId==null || Country==null || Permiso==null || TimeZone==null || Lenguage==null || Divisa==null || Apodo==null || TimeZoneLo==null || emailEncode==null || clasficacion==null) || (RoleId==null && clasficacion.indexOf("Chatter")==-1)) || (create=="false" && UsrName==null)){
      alert("Hay algún campo obligatorio sin rellenar por favor asegúrese de que estan rellenos los campos con *");
    }else{
      var spinner = component.find('spinnerCharged');
      $A.util.removeClass(spinner,'slds-hide');
      if(create=="true"){
        var emailSpl = email.split("@");
        if(emailSpl.length > 2){
          alert("Fromato de email incorrecto, demasiados @");
        }else if(emailSpl.length == 1){
           $A.util.addClass(spinner,'slds-hide');
          alert("Fromato de email incorrecto, sin @");
        }else{
          var emailSpl2 = emailSpl[1].split(".");
          if(emailSpl2.length==1){
             $A.util.addClass(spinner,'slds-hide');
            alert("Formato de email incorrecto, sin extensión \".XXX\"");
          }else if(emailSpl2[0]==""){
             $A.util.addClass(spinner,'slds-hide');
            alert("El correo no tiene dominio");
          }else{
            var action = component.get("c.createUser");
            action.setParams({"email":email,"tiwsInformation":tiwsInformation,"UsrName":UsrName,"Title":Title,"Company":Company,"Departament":Departament,"fName":fName,"lName":lName,"Alias":Alias,"telefono":telefono,"movil":movil,"RoleId":RoleId,"ProfileId":ProfileId,"Country":Country,"Permiso":Permiso,"managerUsrName":managerUsrName,"Division":Division,"TimeZone":TimeZone,"Lenguage":Lenguage,"Divisa":Divisa,"Apodo":Apodo,"TimeZoneLo":TimeZoneLo,"encodeEmail":emailEncode,"isActive":isActive,"resetPass":resetPass,"DNI":DNI,"PERSTC":PERSTC,"PERSMC":PERSMC,"Segmento":Segmento,"Service":service,"Marketing":marketing,"Clasificacion":clasficacion});
              console.log('tiwsInformation = ' + tiwsInformation);
              console.log(action.getParams());
              var result;
        action.setCallback(this,function(response){ 
        if(component.isValid() && response.getState()==="SUCCESS"){
              result = response.getReturnValue();
              if(result == "OK"){
                 $A.util.addClass(spinner,'slds-hide');
                alert("Usuario creado correctamente");
              }else{
                 $A.util.addClass(spinner,'slds-hide');
                alert("Fallo de creacion del usuario\n"+result);
              }
            }else if(response.getState()==="ERROR"){
                var errors = response.getError();
                if(errors){
                    if (errors[0] && errors[0].message) {
                       $A.util.addClass(spinner,'slds-hide');
                        alert("Fallo en la creación del usuario por favor revise el mensaje\n"+ errors[0].message);
                    }

                }
            }
            });
             $A.enqueueAction(action);
          }
        }

      }else{
        if(email==null){
          var actionUp=component.get("c.updateUser");
          actionUp.setParams({"email":email,"tiwsInformation":tiwsInformation,"UsrName":UsrName,"Title":Title,"Company":Company,"Departament":Departament,"fName":fName,"lName":lName,"Alias":Alias,"telefono":telefono,"movil":movil,"RoleId":RoleId,"ProfileId":ProfileId,"Country":Country,"Permiso":Permiso,"managerUsrName":managerUsrName,"Division":Division,"TimeZone":TimeZone,"Lenguage":Lenguage,"Divisa":Divisa,"Apodo":Apodo,"TimeZoneLo":TimeZoneLo,"encodeEmail":emailEncode,"isActive":isActive,"resetPass":resetPass,"DNI":DNI,"PERSTC":PERSTC,"PERSMC":PERSMC,"Segmento":Segmento,"Service":service,"Marketing":marketing,"Clasificacion":clasficacion});
          console.log('tiwsInformation' + tiwsInformation);
            var resultUp;
          actionUp.setCallback(this,function(response){ 
          if(component.isValid() && response.getState()==="SUCCESS"){
            resultUp = response.getReturnValue();
            if(resultUp == "OK"){
               $A.util.addClass(spinner,'slds-hide');
              alert("Usuario actualizado correctamente");
            }else{
               $A.util.addClass(spinner,'slds-hide');
              alert("Fallo de creacion del usuario\n"+resultUp);
            }
          }else if(response.getState()==="ERROR"){
            var errors = response.getError();
            if(errors){
              if (errors[0] && errors[0].message) {
                 $A.util.addClass(spinner,'slds-hide');
                alert("Fallo en la creación del usuario por favor revise el mensaje\n"+ errors[0].message);
              }
            }
          }
        });
          $A.enqueueAction(actionUp);
        }else{
          var emailSpl = email.split("@");
          if(emailSpl.length > 2){
             $A.util.addClass(spinner,'slds-hide');
          alert("Fromato de email incorrecto, demasiados @");
          }else if(emailSpl.length == 1){
             $A.util.addClass(spinner,'slds-hide');
            alert("Fromato de email incorrecto, sin @");
          }else{
            var emailSpl2 = emailSpl[1].split(".");
    
            if(emailSpl2.length==1){
               $A.util.addClass(spinner,'slds-hide');
              alert("Formato de email incorrecto, sin extensión \".XXX\"");
            }else if(emailSpl2[0]==""){
               $A.util.addClass(spinner,'slds-hide');
              alert("El correo no tiene dominio");
            }else{
              var actionUp2=component.get("c.updateUser");
              actionUp2.setParams({"email":email,"UsrName":UsrName,"tiwsInformation":tiwsInformation,"Title":Title,"Company":Company,"Departament":Departament,"fName":fName,"lName":lName,"Alias":Alias,"telefono":telefono,"movil":movil,"RoleId":RoleId,"ProfileId":ProfileId,"Country":Country,"Permiso":Permiso,"managerUsrName":managerUsrName,"Division":Division,"TimeZone":TimeZone,"Lenguage":Lenguage,"Divisa":Divisa,"Apodo":Apodo,"TimeZoneLo":TimeZoneLo,"encodeEmail":emailEncode,"isActive":isActive,"resetPass":resetPass,"DNI":DNI,"PERSTC":PERSTC,"PERSMC":PERSMC,"Segmento":Segmento,"Service":service,"Marketing":marketing,"Clasificacion":clasficacion});
              var resultUp;
              actionUp2.setCallback(this,function(response){ 
              if(component.isValid() && response.getState()==="SUCCESS"){
                resultUp = response.getReturnValue();
                if(resultUp == "OK"){
                   $A.util.addClass(spinner,'slds-hide');
                  alert("Usuario actualizado correctamente");
                }else{
                   $A.util.addClass(spinner,'slds-hide');
                  alert("Fallo de creacion del usuario\n"+resultUp);
                }
              }else if(response.getState()==="ERROR"){
                var errors = response.getError();
                 $A.util.addClass(spinner,'slds-hide');
                if(errors){
                  if (errors[0] && errors[0].message) {
                    alert("Fallo en la creación del usuario por favor revise el mensaje\n"+ errors[0].message);
                  }
                }
              }
              });
              $A.enqueueAction(actionUp2);
            }
          }
        }
      }
    
    }
    }
    
  },
  upPer:function(component,event,helper){
    //Aqui ira la parte de los permisos hacia el siste
  },
  darPer:function(component,event,helper){
    //Aqui ira la parte de los permisos en el front(dar)
  },
  quitPer:function(component,event,helper){
    //Aqui ira la parte de los permisos en el front(quitar)
  },
  onDragOver: function(component, event) {
        event.preventDefault();
    },
  createUpdateFile: function(component,event,helper){
  event.stopPropagation();
  event.preventDefault();
   var files =event.dataTransfer.files;
    console.log(files[0]);
   //var file = component.find('file')[1];
   //console.log(file);
   //var files =(event.target.files || event.dataTransfer.files); 
   var rFile = new FileReader();
  component.find("textarea").set("v.value",files[0].name);
   rFile.onload=function(event){
    console.log(event);
    //var src = event.getSource();
    console.log("Leyo el fichero");
    var getFields=component.get("c.getFieldsSF");
   var linesCsv = rFile.result.split("\n");
   console.log(linesCsv);
   var users = {};
   var pos={};
  
   var fields = {};
   
   var errors=[];
   var camposMal=[];
   var camposBien=[];
   var userUpd=[];
   var userDel=[];
   var userCre=[];
   var lentgh = linesCsv.lentgh;
   var linesCsv2=[];
   for(var j = 0;j<linesCsv.length;j++){
    if(linesCsv[j]!=undefined && linesCsv[j]!=""){
      linesCsv2[j]=linesCsv[j].split("\r")[0];  
    }
   }
    
   var user = linesCsv2[0].split(',');
    
   //Sacamos los campos definidos para la ADM 
   getFields.setCallback(this,function(response){
    console.log("Primera llamada");
    if(component.isValid() && response.getState()==="SUCCESS"){
      var Names = response.getReturnValue();
      console.log(Names);
      for(var i  = 0 ;i<user.length;i++){
        //comprobamos que existen los campos del csv en la Org si exiten a un mapa auxiliar para encontrarlos más rapidamente+
        console.log("Esto es i: "+i+" Esto es indexOd i: "+Names.indexOf(user[i]));
        if(Names.indexOf(user[i])!=-1){
          pos[user[i]]=i;
          camposBien.push(user[i]);
        }else{
          camposMal.push(user[i]);
        }
      }
      //Si es distinto de 0 es que se han introducido mal los campos, que lo revise el usuario
      console.log("Campos válidos: "+Names);
      if(camposMal.length>0){
        var salida ="Estos campos no estan definidos para la Administración Delegada\n";
        for(var i = 0;i<camposMal.length;i++){
          salida=salida+camposMal[i]+"\n";
        }
        component.find("textarea").set("v.value","Por favor arrastre un fichero csv, separado por comas");
        salida=salida+"Por favor introduzca un csv con los campos correctos";
        alert(salida);
      }else{
        console.log(pos);
        //Comprobamos que se haya introducido correctamente los 3 campos obligatorios.
        if(pos["Username"]==null || pos["IsActive"]==null || pos["BI_Clasificacion_de_Usuario__c"]==null)
          alert("Falta alguno de los campos obligatorios: UserName, isActive o BI_Clasificacion_de_Usuario__c");
        else{
          //Comprobamos si alguno de los uaurios existen en el sistema, nos devolverá el UserName y el isActive
           var userNames =[];
           var Managers = [];
          for(var i = 1;i<linesCsv2.length;i++){
              var split = linesCsv2[i].split(",");
              userNames.push(split[pos["Username"]]);
              if(pos["ManagerId"]!=null){
                if(split[pos["ManagerId"]]!=null){
                  Managers.push(split[pos["ManagerId"]]);    
                }
              
              }
              
              console.log(split[pos["Username"]]);
            }
            console.log(userNames);
            var getExistentUsers = component.get("c.getLoadUsers");
            getExistentUsers.setParams({"userNames":userNames});
            getExistentUsers.setCallback(this,function(response){
              if(component.isValid() && response.getState()==="SUCCESS"){
                var users_aux = {};
                var res = response.getReturnValue();
                //Lo guardamos en un mapa para que más delante sea facil la extraccion de los valores.
                console.log(res);
                for(var j = 0;j<res.length;j++){
                  console.log(res[j].Username);
                  console.log(res[j].IsActive);
                  users_aux[res[j].Username]=res[j].IsActive;
                }
                for(var i = 1;i<linesCsv2.length;i++){
                  var aux_obj={};

                  var linea = linesCsv2[i].split(",");
                  var userName=linea[pos["Username"]];
                  var isAct = linea[pos["IsActive"]];
                  for(var j=0;j<camposBien.length;j++){
                    aux_obj[camposBien[j]]=linea[pos[camposBien[j]]]
                  }
                  if(users_aux[userName]!=null){
                    console.log(" el otro: "+users_aux[userName]+" IsActive: "+isAct);
                    if(users_aux[userName].toString()=="true" && isAct.toString()=="true"){
                      userUpd.push(aux_obj);
                    }else if(users_aux[userName].toString()=="false" && isAct.toString()=="true"){
                      userUpd.push(aux_obj);
                    }else if(users_aux[userName].toString()=="true" && isAct.toString()=="false"){
                      userDel.push(aux_obj);
                    }else{
                      alert("No se puede actualizar un usuario desactivado:"+ userName);
                    }
                  }else{
                    if(isAct.toString()=="true"){
                      userCre.push(aux_obj);  
                    }else{
                      alert("No se puede crear un usuario inactivo: "+userName);
                    }
                    
                  }
                }
                var getManagers = component.get("c.getManagersCsv");
                getManagers.setParams({"names":Managers});
                getManagers.setCallback(this,function(resonse){
                   if(component.isValid() && response.getState()==="SUCCESS"){

                    //AQUI comporbamos los Ids de los managers

                    var mp_Ids = resonse.getReturnValue();
                    var keysMan = Object.keys(mp_Ids);
                    console.log("ESTAS SON LAS KEYS"+keysMan[0]);
                    console.log("MAPEOS DE IDS CON USUARIO"+mp_Ids);
                    for(var y = 0;y<userUpd.length;y++){
                      var us_aux = userUpd[y];
                      console.log("HEY MANAGER ID"+us_aux["ManagerId"]);
                      if(us_aux["ManagerId"]!=null){
                        var ManId = mp_Ids[us_aux["ManagerId"]];
                        console.log("Entron 1 VALRO"+ManId);
                        if(ManId!=null){
                          console.log("Deberia cambiar");
                          userUpd[y]["ManagerId"]=ManId;
                        }
                      }
                      
                    }
                    for(var y = 0;y<userDel.length;y++){
                      var us_aux = userDel[y];
                      if(us_aux["ManagerId"]!=null){
                        var ManId = mp_Ids[us_aux["ManagerId"]];
                        if(ManId!=null){
                          userDel[y]["ManagerId"]=ManId;
                        }
                      }
                      
                    }
                    for(var y = 0;y<userCre.length;y++){
                      var us_aux = userCre[y];
                      if(us_aux["ManagerId"]!=null){
                        var ManId = mp_Ids[us_aux["ManagerId"]];
                        if(ManId!=null){
                          userCre[y]["ManagerId"]=ManId;
                        }
                      }
                      
                    }

                    component.set("v.userUpd",userUpd);
                    component.set("v.userDel",userDel);
                    component.set("v.userCre",userCre);
                    document.getElementById("tableId").innerHTML ='';
                    var lst_tableRow=[];
                    for(var j=0;j<userUpd.length;j++){
                      var tableRow = document.createElement('tr');
                      $A.util.addClass(tableRow,"Upd");
                      var tableDataCheck = document.createElement('td');
                      var checkbox = document.createElement('input');
                      checkbox.setAttribute("type", "checkbox");
                      checkbox.setAttribute("id","Upd"+j);
                      tableDataCheck.appendChild(checkbox);
                      tableRow.appendChild(tableDataCheck);
                      for(var i =0; i<camposBien.length;i++){
                        var tableData = document.createElement('td');
                        console.log(userUpd[j][camposBien[i]]);
                        var tableDataNode = document.createTextNode(userUpd[j][camposBien[i]]);
                        tableData.appendChild(tableDataNode);
                        tableRow.appendChild(tableData);
                      }
                      lst_tableRow.push(tableRow);
                    }
                    for(var j=0;j<userDel.length;j++){
                      console.log(userDel[j]["Username"]);
                      var tableRow = document.createElement('tr');
                       $A.util.addClass(tableRow,"Del");
                       var tableDataCheck = document.createElement('td');
                       var checkbox = document.createElement('input');
                       checkbox.setAttribute("type", "checkbox");
                       checkbox.setAttribute("id","Del"+j);
                       tableDataCheck.appendChild(checkbox);
                       tableRow.appendChild(tableDataCheck);
                        for(var i =0; i<camposBien.length;i++){
                          var tableData = document.createElement('td');
                          var tableDataNode = document.createTextNode(userDel[j][camposBien[i]]);
                          tableData.appendChild(tableDataNode);
                          tableRow.appendChild(tableData);
                        }
                       lst_tableRow.push(tableRow);
                    }
                    for(var j=0;j<userCre.length;j++){
                      console.log(userCre[j]["Username"]);
                      var tableRow = document.createElement('tr');
                       $A.util.addClass(tableRow,"Cre");
                       var tableDataCheck = document.createElement('td');
                       var checkbox = document.createElement('input');
                       checkbox.setAttribute("type", "checkbox");
                       checkbox.setAttribute("id","Cre"+j);
                       tableDataCheck.appendChild(checkbox);
                       tableRow.appendChild(tableDataCheck);
                      for(var  i =0; i<camposBien.length;i++){
                          var tableData = document.createElement('td');
                          var tableDataNode = document.createTextNode(userCre[j][camposBien[i]]);
                          tableData.appendChild(tableDataNode);
                          tableRow.appendChild(tableData);
                      }
                      lst_tableRow.push(tableRow);
                       
                    }
                    for(var z = 0;z<lst_tableRow.length;z++){
      
                      document.getElementById("tableId").appendChild(lst_tableRow[z]);
                    }
                    var aux = [];
                    aux.push("Confirmar");
                    for(var j = 0;j<camposBien.length;j++)  
                     aux.push(camposBien[j]);
                    component.set("v.campos",aux);
                    $A.util.removeClass(component.find("modal-id"),"slds-hide");
                    $A.util.addClass(component.find("Conf-lab-Id"),"slds-hide");
                    $A.util.addClass(component.find("Conf-Id"),"slds-hide");
                    $A.util.addClass(component.find("Dat-Id"),"slds-hide");
                    $A.util.addClass(component.find("File-id"),"slds-hide");
                   }
                });
               $A.enqueueAction(getManagers); 
              }
            });
            $A.enqueueAction(getExistentUsers); 
        }
      }
      
    }
   });
   $A.enqueueAction(getFields);
   };
    rFile.readAsText(files[0],'ISO-8859-15');
  },

    showSpinner : function (cmp, event) {
        var spinner = cmp.find('spinner');        
        $A.util.removeClass(spinner,'slds-hide');  
    },
    hideSpinner : function (cmp, event) {
        var spinner = cmp.find('spinner');        
        $A.util.addClass(spinner,'slds-hide');
    },
    returnHomePage : function (cmp, event) {
        var url = '/';
        window.location.assign(url);  
    },createEvent : function(cmp,event){

    },
    cancelMas : function(component,event){
      $A.util.addClass(component.find("modal-id"),"slds-hide");
      $A.util.removeClass(component.find("Conf-lab-Id"),"slds-hide");
      $A.util.removeClass(component.find("Conf-Id"),"slds-hide");
      $A.util.removeClass(component.find("Dat-Id"),"slds-hide");
      $A.util.removeClass(component.find("File-id"),"slds-hide");
      component.find("textarea").set("v.value","\nPor favor arrastre un fichero csv, separado por comas\n");
      $A.util.addClass(component.find("error_modal"),"slds-hide");
      $A.util.addClass(component.find("button-estado"),"slds-hide");
      $A.util.addClass(component.find("estado_modal"),"slds-hide");
    },


    subidaConf: function(component,event,helper){
      var lst_Upd = component.get("v.userUpd");
      var lst_Del = component.get("v.userDel");
      var lst_Cre = component.get("v.userCre");
      var lst_acep = [];
      //map {Username:map_USER{FieldNAme:valueField,FieldNAme:valueField}}
      var lst_UpdAcept = {};
      var lst_DelAcept = {};
      var lst_CreAcept = {};
      //map {Nombre Licencia:valor suma/resta}
      var mp_Up={};
      var mp_dow={};
      var isChecked = false;
      for(var j=0;j<lst_Upd.length;j++){
        var check = document.getElementById("Upd"+j).checked;
        if(check==true){
          isChecked=true;
          //map {Username:map_USER{FieldNAme:valueField,FieldNAme:valueField}}
          lst_UpdAcept[lst_Upd[j]["Username"]]=lst_Upd[j];
          var usr = new Object();
          usr.sobjectType ="User";
          usr.Username =lst_Upd[j]["Username"];
          usr.BI_Clasificacion_de_Usuario__c = lst_Upd[j]["BI_Clasificacion_de_Usuario__c"]; 
          lst_acep.push(usr);
        }
      }
      for(var j=0;j<lst_Del.length;j++){
        var check = document.getElementById("Del"+j).checked;
        if(check==true){
          isChecked=true;
          //map {Username:map_USER{FieldNAme:valueField,FieldNAme:valueField}}
          lst_DelAcept[lst_Del[j]["Username"]]=lst_Del[j];
          var usr = new Object();
          usr.sobjectType ="User";
          usr.Username =lst_Del[j]["Username"];
          usr.BI_Clasificacion_de_Usuario__c = lst_Del[j]["BI_Clasificacion_de_Usuario__c"]; 
          lst_acep.push(usr);
          if(mp_dow[lst_Del[j]["BI_Clasificacion_de_Usuario__c"]]==null){
            mp_dow[lst_Del[j]["BI_Clasificacion_de_Usuario__c"]]=1;
          }else{
            mp_dow[lst_Del[j]["BI_Clasificacion_de_Usuario__c"]]=mp_dow[lst_Del[j]["BI_Clasificacion_de_Usuario__c"]]+1;
          }
        }
      }
      for(var j=0;j<lst_Cre.length;j++){
        var check = document.getElementById("Cre"+j).checked;
        if(check==true){
          isChecked=true;
          //map {Username:map_USER{FieldNAme:valueField,FieldNAme:valueField}}
          lst_CreAcept[lst_Cre[j]["Username"]]=lst_Cre[j];
          var usr = new Object();
          usr.Username =lst_Cre[j]["Username"];
          usr.sobjectType ="User";
          usr.BI_Clasificacion_de_Usuario__c = lst_Cre[j]["BI_Clasificacion_de_Usuario__c"]; 
          lst_acep.push(usr);
          if(mp_Up[lst_Cre[j]["BI_Clasificacion_de_Usuario__c"]]==null){
            mp_Up[lst_Cre[j]["BI_Clasificacion_de_Usuario__c"]]=1;
          }else{
            mp_Up[lst_Cre[j]["BI_Clasificacion_de_Usuario__c"]]=mp_Up[lst_Cre[j]["BI_Clasificacion_de_Usuario__c"]]+1;
          }
        }
      }
      if(isChecked==false){
        alert("Por favor asegurese de tener alguna casilla marcada");
        return;
      }
      lst_userName=[];
      for(var j = 0;j<lst_acep.length;j++){
        lst_userName.push(lst_acep[j]["Username"]);
      }
      console.log(lst_acep[0]);
      console.log("A enviar: "+lst_acep);
      //Recogemos las licencias de los usuarios a actualizar a fin de comprobar si hay alguna actualizacion de las mismas
      var usersLics = component.get("c.getLicenses");
      usersLics.setParams({"users":lst_acep});
      usersLics.setCallback(this,function(response){
        console.log(response.getReturnValue());
        console.log(response.getState());
        if(component.isValid() && response.getState()==="SUCCESS"){
          var res = response.getReturnValue();
          var usrs = res["Users"];
          for(var j =0;j<usrs.length;j++){
            if(lst_UpdAcept[usrs[j].Username]!=null){
              if(lst_UpdAcept[usrs[j].Username]["BI_Clasificacion_de_Usuario__c"]!=usrs[j].BI_Clasificacion_de_Usuario__c){
                if(mp_Up[lst_UpdAcept[usrs[j].Username]["BI_Clasificacion_de_Usuario__c"]]==null){
                  mp_Up[lst_UpdAcept[usrs[j].Username]["BI_Clasificacion_de_Usuario__c"]]=1;
                }else{
                  mp_Up[lst_UpdAcept[usrs[j].Username]["BI_Clasificacion_de_Usuario__c"]]=mp_Up[lst_UpdAcept[usrs[j].Username]["BI_Clasificacion_de_Usuario__c"]]+1;
                }
                if(mp_dow[usrs[j].BI_Clasificacion_de_Usuario__c]==null){
                  mp_dow[usrs[j].BI_Clasificacion_de_Usuario__c]=1;
                }else{
                  mp_dow[usrs[j].BI_Clasificacion_de_Usuario__c]=mp_dow[usrs[j].BI_Clasificacion_de_Usuario__c]+1;
                }

              }
            }
          }
          var auxLic = Object.keys(mp_dow);
          for(var i = 0;i<Object.keys(mp_Up).length;i++){
            if(Object.keys(mp_Up)[i] in mp_dow){

            }else{
              auxLic.push(Object.keys(mp_Up)[i]);
            }
          }
          console.log("licencias buscadas: "+auxLic);
          //Buscamos como esta el estado de las licencias en el entorno
          var licsNames = component.get("c.getLicensesOrg");
          licsNames.setParams({"names":auxLic});
          licsNames.setCallback(this,function(response){
            if(component.isValid() && response.getState()==="SUCCESS"){

              //mp {Nombre licencia:Licencia}
              var res2 = response.getReturnValue();
              var keys = Object.keys(res2);
              console.log("Keys devueltas:"+keys);
              //error de tipo 1 no existe esa licencia en el entorno
              var errorLic1 = [];
              //error de tipo 2 no quedan liecencias disponibles
              var errorLic2 = [];
              for(var j = 0;j<auxLic.length;j++){
                 console.log("Respuesta "+res2[auxLic[j]]+"PAra lic: "+auxLic[j]);
                if(res2[auxLic[j]]==null){
                  errorLic1.push(auxLic[j]);
                }else{
                  var sum ;
                  if(mp_Up[auxLic[j]]!=null)
                    sum=mp_Up[auxLic[j]];
                  else
                    sum=0;
                  var resta;
                  if(mp_dow[auxLic[j]]!=null)
                    resta=mp_dow[auxLic[j]];
                  else
                    resta=0; 
                  //sumamos y restamos

                  console.log("Usadas: "+res2[auxLic[j]].BI_Numero_Licencias_Usadas__c);
                  console.log("sum: "+sum);
                  console.log("resta: "+resta);
                  var tot = res2[auxLic[j]].BI_Numero_Licencias_Usadas__c+sum-resta;
                  console.log(auxLic[j]+"===>"+tot);
                  if(tot>res2[auxLic[j]].BI_Numero_Total_Licencias__c)
                    errorLic2.push("Número de licencias usadas después de actualizar los usuarios = "+tot+". Número de licencias disponibles = "+res2[auxLic[j]].BI_Numero_Total_Licencias__c);
                }
              }
              if(errorLic1.length!=0 || errorLic2.length!=0){
                var newline = String.fromCharCode(13, 10);
                var result="ERROR en el conteo de licencias:"+newline;
                if(errorLic1.length!=0){
                  result=result+"LICENCIAS NO DISPONIBLES:<br/>";
                  for(var z =0 ;z<errorLic1.length;z++)
                    result=result+errorLic1[z]+"<br/>";
                }
                if(errorLic2.length!=0){
                  result=result+"NÚMERO MÁXIMO DE LICENCIAS ALCANZADAS:<br/>"
                  for(var z = 0;z<errorLic2.length;z++){
                    result=result+errorLic2[z]+"<br/>";
                  }
                }
                //Mostramos el modal con todos los errores
                $A.util.removeClass(component.find("error_modal"),"slds-hide");
                $A.util.addClass(component.find("modal-id"),"slds-hide");
                component.find("textError").set("v.value",result);
              }else{
                //AQUI ya actualizamos
                //MOSTRAMOS una tabla de tipo modal justo donde iria el de errores y mostramos el estado de la actualizacion por orden
                var lst_us=[];
                var usDel = Object.keys(lst_DelAcept);
                var usCre = Object.keys(lst_CreAcept);
                var usUpd = Object.keys(lst_UpdAcept);
                document.getElementById("tableSub").innerHTML ='';
                for(var j =0;j<usDel.length;j++){
                  var tableRow = document.createElement('tr');
                  tableRow.setAttribute("id","row"+j);
                  var tableData = document.createElement('td');
                  var tableDataNode = document.createTextNode(usDel[j]);
                  tableData.appendChild(tableDataNode);
                  tableRow.appendChild(tableData);
                  var dataRowStatus = document.createElement('td');
                  var statusNode = document.createTextNode("Encolado");
                  dataRowStatus.setAttribute("id","status"+j);
                  dataRowStatus.appendChild(statusNode);
                  tableRow.appendChild(dataRowStatus);
                  var tableresult = document.createElement('td');
                  tableresult.setAttribute("id","result"+j);
                  tableresult.setAttribute("type","String");
                  tableRow.appendChild(tableresult);
                  var us = lst_DelAcept[usDel[j]];
                  us["Type"]="Del";
                  lst_us.push(us);
                  document.getElementById("tableSub").appendChild(tableRow);
                }
                var record = usDel.length;
                for(var j = 0;j<usUpd.length;j++){
                  var id = record+j;
                  var tableRow = document.createElement('tr');
                  tableRow.setAttribute("id","row"+id);
                  var tableData = document.createElement('td');
                  var tableDataNode = document.createTextNode(usUpd[j]);
                  tableData.appendChild(tableDataNode);
                  tableRow.appendChild(tableData);
                  var dataRowStatus = document.createElement('td');
                  var statusNode = document.createTextNode("Encolado");
                  dataRowStatus.setAttribute("id","status"+id);
                  dataRowStatus.appendChild(statusNode);
                  tableRow.appendChild(dataRowStatus);
                  var tableresult = document.createElement('td');
                  tableresult.setAttribute("id","result"+id);
                  tableresult.setAttribute("type","String");
                  tableRow.appendChild(tableresult);
                  var us = lst_UpdAcept[usUpd[j]];
                  us["Type"]="Upd";
                  lst_us.push(us);
                  document.getElementById("tableSub").appendChild(tableRow);
                }
                record = record+usUpd.length;
                for(var j = 0;j<usCre.length;j++){
                  var id = record+j;
                  var tableRow = document.createElement('tr');
                  tableRow.setAttribute("id","row"+id);
                  var tableData = document.createElement('td');
                  var tableDataNode = document.createTextNode(usCre[j]);
                  tableData.appendChild(tableDataNode);
                  tableRow.appendChild(tableData);
                  var dataRowStatus = document.createElement('td');
                  var statusNode = document.createTextNode("Encolado");
                  dataRowStatus.setAttribute("id","status"+id);
                  dataRowStatus.appendChild(statusNode);
                  tableRow.appendChild(dataRowStatus);
                  var tableresult = document.createElement('td');
                  tableresult.setAttribute("id","result"+id);
                  tableresult.setAttribute("type","String");
                  tableRow.appendChild(tableresult);
                  var us = lst_CreAcept[usCre[j]];
                  us["Type"]="Cre";
                  lst_us.push(us);
                  document.getElementById("tableSub").appendChild(tableRow);
                  console.log("Al crear la tabla result"+j+"status")
                }
                $A.util.removeClass(component.find("estado_modal"),"slds-hide");
                $A.util.addClass(component.find("button-Mas"),"slds-hide");
                $A.util.addClass(component.find("button-Mas2"),"slds-hide");
                $A.util.addClass(component.find("error_modal"),"slds-hide");
                record = record+usCre.length;
                var errorFin=false;
                if(lst_us.length!=0){
                  //avoid non calling
                  helper.recursiveCall(component,0,lst_us,event,helper);  
                }
                
    
                $A.util.removeClass(component.find("button-estado"),"slds-hide");
                
              }
              
            }else if(response.getState()==="ERROR"){
              alert(response.getError()[0].message);
            }
          });
          $A.enqueueAction(licsNames);
        }
      });
      $A.enqueueAction(usersLics);
    },
    hideModal : function(cmp,event){
      $A.util.addClass(cmp.find("error_modal"),"slds-hide");
      $A.util.addClass(cmp.find("button-estado"),"slds-hide");
      $A.util.addClass(cmp.find("estado_modal"),"slds-hide");
      $A.util.addClass(cmp.find("error_modal"),"slds-hide");
      $A.util.removeClass(cmp.find("modal-id"),"slds-hide");
    }

})