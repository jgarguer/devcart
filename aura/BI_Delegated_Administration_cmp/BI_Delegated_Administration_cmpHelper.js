({
	checkUSerName : function(component,helper) {
	var UsName = component.find("UserName-input").get("v.value");
    var action = component.get("c.checkUserNameMethod");
    var create = new Object();
    action.setParams({"userName":UsName});
    action.setCallback(this,function(response){  
      if(component.isValid() && response.getState()==="SUCCESS"){
   
        if (response.getReturnValue() != null){
          create = response.getReturnValue();
          alert("¡ATENCIÓN! Ha introducido un usuario ya existente si continua se actualizará");
          component.set("v.Create","false");
          $A.util.removeClass(component.find("lNameNM-lab"),"slds-hide");
          $A.util.addClass(component.find("lName-lab"),"slds-hide");
          $A.util.removeClass(component.find("AliasNM-lab"),"slds-hide");
          $A.util.addClass(component.find("Alias-lab"),"slds-hide");
          $A.util.removeClass(component.find("EmailNM-lab"),"slds-hide");
          $A.util.addClass(component.find("Email-lab"),"slds-hide");
          $A.util.removeClass(component.find("ApodoNM-lab"),"slds-hide");
          $A.util.addClass(component.find("Apodo-lab"),"slds-hide");
          component.find("button").set("v.label","Actualizar");
          component.find("fProfile-ui").set("v.label","Perfil");
          component.find("Paises-ui").set("v.label","Pais");
          component.find("Permisos-ui").set("v.label","Permisos");
          component.find("TimeZoneSidKey-ui").set("v.label","Zona horaria");
          component.find("LocaleSidKey-ui").set("v.label","Configuración local");
          component.find("Lenguages-ui").set("v.label","Idioma");
          component.find("Divisas-ui").set("v.label","Divisa");
          component.find("fRole-ui").set("v.label","Función");
          component.find("EmailCode-ui").set("v.label","Codificación email");
          component.find("Clasificacion-ui").set("v.label","Clasificacion del Usuario");
          $A.util.removeClass(component.find("Per-lab"),"slds-hide");
          $A.util.removeClass(component.find("Per-lay"),"slds-hide");
          $A.util.removeClass(component.find("Per-head"),"slds-hide");
          $A.util.removeClass(component.find("Queue-lay"),"slds-hide");
          $A.util.removeClass(component.find("Queue-head"),"slds-hide");
          $A.util.addClass(component.find("File-id"),"slds-hide");
          var lProfile;
          var UserNameGestor;
          var lRole;
          var eRoleId=create.UserRoleId;
          var eProfId = create.ProfileId;
          var lst_Rol = component.get("v.auxRole");
          var lst_Prof = component.get("v.auxProfile");
          for(var i=0;i<lst_Rol.length;i++){
            if(lst_Rol[i].Id == eRoleId){
              component.find("fRole-ui").set("v.value",lst_Rol[i].Id);
              break;
            }
          }
          for(var i=0;i<lst_Prof.length;i++){
            if(lst_Prof[i].Id == eProfId){
              component.find("fProfile-ui").set("v.value",lst_Prof[i].Id);
              break;
            }
          }
		    component.find("EmailCode-ui").set("v.value",create.EmailEncodingKey);
    	  component.find("DNI-fd").set("v.value",create.BI_DNI_Per__c);
    	  component.find("PERSTC-fd").set("v.value",create.BI_Peru_STC_Per__c);
    	  component.find("PERSMC-fd").set("v.value",create.BI_Peru_SMC_Per__c);
		      component.find("Segmento-ui").set("v.value",create.BI_Segmento_regional__c);
       	  component.find("email-input").set("v.value",create.Email);
       	  component.find("Cargo-input").set("v.value",create.Title);
       	  component.find("Company-input").set("v.value",create.CompanyName);
       	  component.find("Departament-input").set("v.value",create.Division);
       	  component.find("FName-fd").set("v.value",create.FirstName);
       	  component.find("lName-fd").set("v.value",create.LastName);
       	  component.find("Alias-fd").set("v.value",create.Alias);
       	  component.find("telefono-input").set("v.value",create.Phone);
       	  component.find("movil-input").set("v.value",create.MobilePhone);
       	  component.find("Paises-ui").set("v.value",create.Pais__c);
       	  component.find("Permisos-ui").set("v.value",create.BI_Permisos__c);
       	  component.find("Division-input").set("v.value",create.Department);
       	  component.find("TimeZoneSidKey-ui").set("v.value",create.TimeZoneSidKey);
       	  component.find("Lenguages-ui").set("v.value",create.LanguageLocaleKey);
       	  component.find("Divisas-ui").set("v.value",create.CurrencyIsoCode);
       	  component.find("Apodo-fd").set("v.value",create.CommunityNickName);
       	  component.find("LocaleSidKey-ui").set("v.value",create.LocaleSidKey);
       	  component.find("Active").set("v.value",create.IsActive);
       	  component.find("ResetPass").set("v.value",false);
       	  component.find("Service").set("v.value",create.UserPermissionsSupportUser);
       	  component.find("Marketing").set("v.value",create.UserPermissionsMarketingUser);
          component.find("Clasificacion-ui").set("v.value",create.BI_Clasificacion_de_Usuario__c);
           if(create.IsActive == false){
           $A.util.removeClass(component.find("MessageActive"),"slds-hide");
          }else{
            $A.util.addClass(component.find("MessageActive"),"slds-hide");
          }
          if(create.ManagerId!=null && create.ManagerId !='undefinied')
              component.set("v.ManagerId",create.ManagerId);
       
          this.findManager(component);
          var actionAllPer = component.get("c.getPermissionSetUser");
          actionAllPer.setParams({"user":UsName});
          actionAllPer.setCallback(this,function(response1){
            if(component.isValid() && response1.getState()==="SUCCESS"){
              var AllPer=response1.getReturnValue();
              var mapAll={};
              for(var i = 0;i<AllPer.length;i++){
                mapAll[AllPer[i].Id]=AllPer[i].Name;

              }
              AllPer.sort(function(a,b){
              var NameA = a.Name.toUpperCase();
              var NameB = b.Name.toUpperCase();
                if(NameA>NameB){
                  return 1;
                }
                if(NameA<NameB){
                  return -1;
                }
                  return 0;
                });
              component.set("v.selectPer",AllPer);
              var actionPerUser=component.get("c.getPermissionsSetsAll");
              actionPerUser.setCallback(this,function(response2){
                console.log(response2);
                if(component.isValid() && response2.getState()==="SUCCESS"){
                  var PerUser = response2.getReturnValue();
                  var lis_Per=[];
                  for(var i = 0;i<PerUser.length;i++){
                    if(PerUser[i].Id in mapAll){
                    
                    }else{
                      lis_Per.push(PerUser[i]);
                    }
                  }
                  lis_Per.sort(function(a,b){
                  var NameA = a.Name.toUpperCase();
                  var NameB = b.Name.toUpperCase();
                    if(NameA>NameB){
                      return 1;
                    }
                    if(NameA<NameB){
                      return -1;
                    }
                      return 0;
                    });
                  component.set("v.allPermisos",lis_Per);
                  helper.showLic(component);
                }
              });
              $A.enqueueAction(actionPerUser);
            }
          });
          $A.enqueueAction(actionAllPer);

          var actionUserQueue = component.get("c.getQueuesUser");
          actionUserQueue.setParams({"UserName":UsName});
          actionUserQueue.setCallback(this,function(response3){
            console.log("LLEGA a l aparte de las c olas");
            if(component.isValid() && response3.getState()==="SUCCESS"){
              console.log("ENTRA");
              var queueUser = response3.getReturnValue();
              var mapQ ={};
              for(var i = 0;i<queueUser.length;i++){
                mapQ[queueUser[i].Id]=queueUser[i].Name;
              }
              queueUser.sort(function(a,b){
              var NameA = a.Name.toUpperCase();
              var NameB = b.Name.toUpperCase();
                if(NameA>NameB){
                  return 1;
                }
                if(NameA<NameB){
                  return -1;
                }
                  return 0;
                });

              component.set("v.QueuesUser",queueUser);
              var actionAllQueue = component.get("c.getQueues");
              actionAllQueue.setCallback(this,function(response4){
                if(component.isValid() && response4.getState()==="SUCCESS"){
                  var allQueue= response4.getReturnValue();
                  var lis_queueNOT = [];
                  for(var i = 0;i<allQueue.length;i++){
                    if(allQueue[i].Id in mapQ){

                    }else{
                      console.log(allQueue[i]);
                      lis_queueNOT.push(allQueue[i]);
                    }
                  }
                  lis_queueNOT.sort(function(a,b){
                    var NameA = a.Name.toUpperCase();
                    var NameB = b.Name.toUpperCase();
                    if(NameA>NameB){
                      return 1;
                    }
                    if(NameA<NameB){
                      return -1;
                    }
                      return 0;
                    });
                  component.set("v.allQueues",lis_queueNOT);
                }
              });
              $A.enqueueAction(actionAllQueue);
            }
          });
          $A.enqueueAction(actionUserQueue);



        }else{
          component.set("v.Create","true");
          component.find("button").set("v.label","Crear");

          component.find("Active").set("v.value",true);
          $A.util.addClass(component.find("lNameNM-lab"),"slds-hide");
          $A.util.removeClass(component.find("lName-lab"),"slds-hide");
          $A.util.addClass(component.find("AliasNM-lab"),"slds-hide");
          $A.util.removeClass(component.find("Alias-lab"),"slds-hide");
          $A.util.addClass(component.find("EmailNM-lab"),"slds-hide");
          $A.util.removeClass(component.find("Email-lab"),"slds-hide");
          $A.util.addClass(component.find("ApodoNM-lab"),"slds-hide");
          $A.util.removeClass(component.find("Apodo-lab"),"slds-hide");
          component.find("fProfile-ui").set("v.label","Perfil*");
          component.find("Paises-ui").set("v.label","Pais*");
          component.find("Permisos-ui").set("v.label","Permisos*")
          component.find("TimeZoneSidKey-ui").set("v.label","Zona horaria*");
          component.find("LocaleSidKey-ui").set("v.label","Configuración local*");
          component.find("Lenguages-ui").set("v.label","Idioma*");
          component.find("Divisas-ui").set("v.label","Divisa*");
          component.find("EmailCode-ui").set("v.label","Codificación email*");
          component.find("fRole-ui").set("v.label","Rol*");
          component.find("Clasificacion-ui").set("v.label","Clasificacion del Usuario *");
          $A.util.addClass(component.find("MessageActive"),"slds-hide");
          $A.util.addClass(component.find("Per-lab"),"slds-hide");
          $A.util.addClass(component.find("Per-lay"),"slds-hide");
          $A.util.addClass(component.find("Queue-lay"),"slds-hide");
          $A.util.addClass(component.find("Per-head"),"slds-hide");
          $A.util.addClass(component.find("Queue-head"),"slds-hide");
          $A.util.removeClass(component.find("File-id"),"slds-hide");
     	  var apodo = UsName.split("@");
        helper.showLic(component);
     	  component.find("Apodo-fd").set("v.value",apodo[0]);
        } 
      }

    });
      $A.enqueueAction(action);

	},
  showLic : function(component){
      var lic = component.find("Clasificacion-ui").get("v.value");
      var action = component.get("c.leftLic");
      action.setParams({"lic":lic});
      action.setCallback(this,function(response){
        if(component.isValid()&& response.getState()==="SUCCESS"){
          component.set("v.licenciaRes",response.getReturnValue());
        }
      });
      $A.enqueueAction(action);
  },

	findManager: function(component){
		var Role = component.find("fRole-ui").get("v.value");
    	var country = component.find("Paises-ui").get("v.value");
    	if((Role!="undefinied" && Role!=null) && (country!="undefinied" && country!=null)){
    	  var action5 = component.get("c.getManagers");
    	  action5.setParams({"prima":Role,"seconda":country});
    	  action5.setCallback(this,function(response){
    	    if(component.isValid() && response.getState()==="SUCCESS"){
    	      var managers = response.getReturnValue();
    	      var list = [];
    	      var none = new Object();
    	      none.NombreApellido = "--Ninguno--";
    	      none.UserName = "";
    	      list.push(none);
    	      for(var j=0;j<managers.length;j++){
    	        var manager = new Object();
    	        manager.NombreApellido = managers[j].FirstName+" "+managers[j].LastName;
    	        manager.Id = managers[j].Id;
    	        list.push(manager);
    	      }
    	    }
    	    component.set("v.Managers",list);
          for(var j=0;j<managers.length;j++){
            if(managers[j].Id == component.get("v.ManagerId")){
              component.find("Manager-ui").set("v.value",managers[j].Id);
              break;
            }
          }
    
    	  });
    	  $A.enqueueAction(action5);
    	}
	},
  NumOfPaises: function(component){
    var callOutPaises = component.get("c.numberLicensesMassive");
    callOutPaises.setParams({"paises":component.get("v.paisesAux")});
    callOutPaises.setCallback(this,function(response){
      if(component.isValid() && response.getState()==="SUCCESS")
        component.set("v.licencias",response.getReturnValue());
    });
    $A.enqueueAction(callOutPaises);
  },
    recursiveCall : function(component,numberJ,lst_us,event,helper){
     var create =false;
      var us;
      var llamada;
      var j = numberJ;
      if(lst_us[j]["Type"].toString()=="Del"){
        us = lst_us[j];
        var status = document.getElementById("status"+j);
        status.childNodes[0].nodeValue="Desactivando...";
      }else if(lst_us[j]["Type"].toString()=="Upd"){
        var status = document.getElementById("status"+j);
        status.childNodes[0].nodeValue="Actualizando...";
        us = lst_us[j];
      }else{
        us = lst_us[j];
        var status = document.getElementById("status"+j);
        status.childNodes[0].nodeValue="Insertando...";
        create =true;
      }
      if(create==false){
        llamada = component.get("c.updateUser");
      }else{
        llamada=component.get("c.createUser");
        if(us["ProfileId"]==null || us["EmailEncodingKey"]==null || us["BI_Permisos__c"]==null || us["Username"]==null || us["BI_Segmento_regional__c"]==null || us["Pais__c"]==null || us["CommunityNickname"]==null || us["LanguageLocaleKey"]==null || us["CurrencyIsoCode"]==null || us["Email"]==null || us["Division"]==null || us["LocaleSidKey"]==null || us["IsActive"]==null || us["Alias"]==null || us["BI_Clasificacion_de_Usuario__c"]==null || us["LastName"]==null || us["UserRoleId"]==null || us["TimeZoneSidKey"]==null ){
          console.log("Esto es al crear status"+j);
          var status = document.getElementById("status"+j);
          status.childNodes[0].nodeValue="Fallo en la creación";
          var textRes = document.createTextNode("Falta algun campo obligatorio por favor revisa el csv: ");
          console.log("Esto es al crear : result"+j)
          document.getElementById("result"+j).appendChild(textRes);
        }
      }
      us.tiwsInformation = us.TIWS_Area__c + '|*|' + us.TIWS_Cartera__c + '|*|' +us.TIWS_Zone__c+ '|*|' +us.TIWS_Region__c ;
      llamada.setParams({"email":us["Email"],"tiwsInformation":us["tiwsInformation"],"UsrName":us["Username"],"Ttle":us["Title"],"Company":us["CompanyName"],"Departament":us["Department"],"fName":us["FirstName"],"lName":us["LastName"],"Alias":us["Alias"],"telefono":us["Phone"],"movil":us["MobilePhone"],"RoleId":us["UserRoleId"],"ProfileId":us["ProfileId"],"Country":us["Pais__c"],"Permiso":us["BI_Permisos__c"],"managerUsrName":us["ManagerId"],"Division":us["Division"],"TimeZone":us["TimeZoneSidKey"],"Lenguage":us["LanguageLocaleKey"],"Divisa":us["CurrencyIsoCode"],"Apodo":us["CommunityNickname"],"TimeZoneLo":us["LocaleSidKey"],"encodeEmail":us["EmailEncodingKey"],"isActive":us["IsActive"],"resetPass":false,"DNI":us["BI_DNI_Per__c"],"PERSTC":us["BI_Peru_STC_Per__c"],"PERSMC":us["BI_Peru_SMC_Per__c"],"Segmento":us["BI_Segmento_regional__c"],"Service":false,"Marketing":false,"Clasificacion":us["BI_Clasificacion_de_Usuario__c"]});
       llamada.setCallback(this,function(response){
        if(component.isValid() && response.getState()==="SUCCESS"){
          var res = response.getReturnValue();
          if(res.toString()!="OK"){
            console.log("Este es el que busco status"+j);
            var status = document.getElementById("status"+j);
            status.childNodes[0].nodeValue="ERROR";
            var textRes = document.createTextNode(res.toString());
            console.log("Este es el que busco resutl"+j);
            document.getElementById("result"+j).innerHTML=res.replace( "\n","<br/>");
            $A.util.removeClass(component.find("button-Mas"),"slds-hide");
            $A.util.removeClass(component.find("button-Mas2"),"slds-hide");
          }else{
            var status = document.getElementById("status"+j);
            status.childNodes[0].nodeValue="FINALIZADO";
            j=j+1;
            if(j<lst_us.length){
              helper.recursiveCall(component,j,lst_us,event,helper);
            }else{
              $A.util.removeClass(component.find("button-Mas"),"slds-hide");
              $A.util.removeClass(component.find("button-Mas2"),"slds-hide");
              alert("FINALIZADO CON ÉXITO LA ACTUALIZACIÓN");
            }
          }
        }else{
          var res = response.getError();
                if (res) {
                    if (res[0] && res[0].message) {
                        var status = document.getElementById("status"+j);
                        status.childNodes[0].nodeValue="ERROR";
                        console.log("En el fallo porqeu no sube!!!");
                        var textRes = document.createTextNode(res[0].message.replace( "\n","<br>"));
                        console.log("Este es el que busco resutl "+j);
                        console.log("Fallo "+res[0].message);
                        document.getElementById("result"+j).innerHTML=res[0].message.replace( "\n","<br/>");
                        $A.util.removeClass(component.find("button-Mas"),"slds-hide");
                        $A.util.removeClass(component.find("button-Mas2"),"slds-hide");
                    }
                }else{
                  alert("Error inesperado por favo contacta con el CoE\n Adjunta el archivo csv");
                }  
        }
      });
      $A.enqueueAction(llamada);
    }
})