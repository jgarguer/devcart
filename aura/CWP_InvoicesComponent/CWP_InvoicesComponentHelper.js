({
    getInitialParams: function(component, event) {
        var me = this;
        $A.util.removeClass(component.find('waitingDiv'), 'waitingDiv');
        var action = component.get("c.init");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.accountRecord', response.getReturnValue());
                me.getTopLevelItems(component, component.get('v.accountRecord').Id, 'FCIC');
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        $A.enqueueAction(action);
    },

    getDisplayTypes: function(component, event) {
        var action = component.get("c.generateDisplayTypes");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.displayTypes', response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        $A.enqueueAction(action);
    },

    getTopLevelItems: function(component, accountId, displayType) {
        var action = component.get("c.fetchInvoices");
        action.setParams({
            "accId": accountId,
            "selectedDisplayType": displayType
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.topLevelItems', JSON.parse(response.getReturnValue()));
                $A.util.addClass(component.find('waitingDiv'), 'waitingDiv');
            } else if (state === "ERROR") {
                var errors = response.getError();
                $A.util.addClass(component.find('waitingDiv'), 'waitingDiv');
                $A.util.removeClass(component.find('errorDiv'), 'waitingDiv');
            }
        });
        $A.enqueueAction(action);
    },

    changeOption: function(component, event) {
        $A.util.addClass(component.find('errorDiv'), 'waitingDiv');
        $A.util.removeClass(component.find('waitingDiv'), 'waitingDiv');
        var me = this;
        var accountId = component.get('v.accountRecord').Id;
        component.set('v.selectedDisplayType', event.currentTarget.value);
        var displayType = component.get('v.selectedDisplayType');
        this.getTopLevelItems(component, accountId, displayType);
    },

    getDetails: function(component, event) {
    	var elem=event.currentTarget;
        if (elem.dataset.level === 'level3') {
        	var accountId = component.get('v.accountRecord').Id;
        	var displayType = component.get('v.selectedDisplayType');
            var yearMonth = elem.dataset.yearmonth;
            var rama = elem.dataset.rama;
            var famOne = elem.dataset.famone;
            var famTwo = elem.dataset.famtwo;
            var action = component.get("c.getDetails");
            action.setParams({
                "accountID": accountId,
                "invoiceDate": yearMonth,
                "invoiceRama": rama,
                "invoiceFamilia1": famOne,
                "invoiceFamilia2": famTwo,
                "tipoFacturacion": displayType
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set('v.detailList', response.getReturnValue());
                    document.getElementById(elem.id+'table').style.display="";                    
                } else if (state === "ERROR") {
                    var errors = response.getError();
                }
            });
            $A.enqueueAction(action);
      }else{
    	  document.getElementById(elem.id+'table').style.display="";
      }


    }
})