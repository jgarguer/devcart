({
    doInit: function(component, event, helper) {
    	var toggleText= component.find('tituloBlue');
    	var toggleSelect= component.find('tipoFacturaList');
    	
	    if(component.get('v.isPortalUser')){
	    	$A.util.removeClass(toggleText, 'noPortalUser');
	    	$A.util.addClass(toggleSelect, 'selectPortalUser');
	    }else{
	    	$A.util.addClass(toggleText, 'noPortalUser');
	    	$A.util.removeClass(toggleSelect, 'selectPortalUser');
	    }
	    
	    helper.getInitialParams(component,event);
	    helper.getDisplayTypes(component,event);	   
    },

    back: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/cwp-billing#"
        });
        urlEvent.fire();
    },

    changeOption: function(component, event, helper) {
    	helper.changeOption(component, event);       
    },
    
    getDeatils:function(component,event,helper){
    	var signo= document.getElementById(event.currentTarget.id).innerHTML;
    	if(signo==="+"){
    		document.getElementById(event.currentTarget.id).innerHTML="-";
    		 helper.getDetails(component,event);
    	}else{
    		document.getElementById(event.currentTarget.id).innerHTML="+";
    		document.getElementById(event.currentTarget.id+'table').style.display="none";
    	}       
    },
    
    print: function(component,event,helper){
    	window.print();
    }
})