({
    pickAccount : function(component, event, helper) {
        var accounts = component.get('v.accounts');
        var account = accounts[event.currentTarget.dataset.index];
        var compEvent = component.getEvent("listEvent");
        compEvent.setParams({payload: account, estado: true, accounts:accounts});
        compEvent.fire();

    },
    showModalCommercialAddress: function(component, event) {
        var accounts = component.get('v.accounts');
        var account = accounts[event.currentTarget.dataset.index];
        var modal = $A.get("e.c:BI_G4C_Modal_Direccion");
        modal.setParams({"account": account, "accounts":accounts});    
		modal.fire();
    }
})