({
	getLastNews : function(component) {
        var action = component.get("c.getLastNews");
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() === "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue !== null){
        			component.set("v.listRssShow", actionResult.getReturnValue());
        			// test map
        			var loadedEvent = $A.get("e.c:CWP_CargaFinalizada");
					loadedEvent.setParams({
						"loaded": true,
						"keyToMap" : 'getLastNews'
						});
					loadedEvent.fire();
            	}
            }
        });
        $A.enqueueAction(action);
        
	},
	
	getImagesCarousel : function(component){
		var action = component.get("c.getVideosWrapper");
		//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.list_vids", returnValue);
					// Lanzo el evento de que las imágenes han sido cargadas					
					var loadedEvent = $A.get("e.c:CWP_CargaFinalizada");
					loadedEvent.setParams({
						"loaded": true,
						"keyToMap" : 'getImagesCarousel'
						});
					loadedEvent.fire();
				}
            }
        });
        $A.enqueueAction(action);
	},
	
	getSectorialNews : function(component){
		var action = component.get("c.getChatterGroups");
		//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.listChatterGroupWrapper", returnValue);
				}
            }
        });
        $A.enqueueAction(action);
	},
    
    getPopularIdeas : function(component){
        var action = component.get("c.obtainPopularIdeas");
        action.setParams({
        	"searchResult" : false,
        	"searchText" : ''
        });
        
        //Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.listPopularIdeasWrapper", returnValue);
				}
            }
        });
        $A.enqueueAction(action);
    },
    
    getRecentIdeas : function(component){
    	var action = component.get("c.obtenerIdeasRecientes");
    	action.setParams({
        	"searchResult" : false,
        	"searchText" : ''
        });
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.listRecentIdeasWrapper", returnValue);
				}
            }
        });
        $A.enqueueAction(action);
    },
    
    getMyIdeas : function(component){
    	var action = component.get("c.obtainMyIdeas");
    	action.setParams({
        	"searchResult" : false,
        	"searchText" : ''
        });
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.listMyIdeasWrapper", returnValue);
				}
            }
        });
        $A.enqueueAction(action);
    },
    
    getFAQs : function(component){
    	var action = component.get("c.getRecoverySolutions");
    	action.setParams({
    		"showAll" : false,
    		"searchResult" : false,
    		"searchText" : '' 
    	});
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.solutionsPlatino", returnValue);
				}
            }
        });
        $A.enqueueAction(action);
    },
    
    abrirDetalle : function(component, event, helper){
        debugger;
        // Ocultar este componente y lanzar evento
        var isIdea = component.get("v.isIdea");
        var isFaq = component.get("v.isFAQ");
        if(isIdea){
        	helper.getComments(component, event, helper);
        }else if(isFaq){
        	helper.getAllFAQs(component, event, helper);
        }
    },
    
    getComments : function(component, event, helper){
    	var action = component.get("c.getAllComments");
    	var ideasListAux = component.get("v.listPopularIdeasWrapper").concat(component.get("v.listRecentIdeasWrapper"));
    	var ideasList = ideasListAux.concat(component.get("v.listMyIdeasWrapper"));
    	var aux= JSON.stringify(ideasList); 
    	action.setParams({
    		"receivedListJSON" : JSON.stringify(ideasList)
    	});
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.mapIdeaComments", returnValue);
					helper.fireEventOpenDetail(component, event, helper, returnValue);
				}
            }
        });
        $A.enqueueAction(action);
    },
    
    fireEventOpenDetail : function(component, event, helper, mapComments){
	    	var openDetailEvent = component.getEvent("openDetailIdea");
	        var isIdea = component.get("v.isIdea");
	        var isFaq = component.get("v.isFAQ");
	    	if(isIdea){
	        	var commentList = mapComments[component.get("v.detalleIdea.id")];
	        	openDetailEvent.setParams({
		            isDetail : true,
		            idDetail : component.get("v.selectedIdea"),
		            detalleIdea : component.get("v.detalleIdea"),
		            listPopularIdeasWrapper : component.get("v.listPopularIdeasWrapper"),
		            listRecentIdeasWrapper : component.get("v.listRecentIdeasWrapper"), 
		            listMyIdeasWrapper : component.get("v.listMyIdeasWrapper"),
		            mapIdeaComments : mapComments,
		            commentWrapperList : commentList,
		            isIdea : true,
		            isFAQ : false,
		        }).fire();
	    	}else if(isFaq){
	        	openDetailEvent.setParams({
		            isDetail : true,
		            solutionsPlatino : component.get("v.solutionsPlatino"),
		            detalleFAQ : component.get("v.detalleFAQ"),
		            isIdea : false,
		            isFAQ : true,
		        }).fire();
	    	}	
    },
    
    getAllFAQs : function(component, event, helper){
    	var action = component.get("c.getRecoverySolutions");
    	action.setParams({
    		"showAll" : true,
    		"searchResult" : false,
    		"searchText" : '' 
    	});
    	//Callback
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
            	var returnValue = response.getReturnValue();
                if(returnValue !== null){
					component.set("v.solutionsPlatino", returnValue);
					helper.fireEventOpenDetail(component, event, helper);
				}
            }
        });
        $A.enqueueAction(action);
    }
})