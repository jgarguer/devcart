({

    doInit: function(component, event, helper) {
        helper.callChangeTab1(component);
        helper.loadInfo(component);
        helper.getsearchFields(component);
        helper.getLastModificationDate(component);
        helper.getLastModificationFactDate(component);

        helper.getHeader(component);
        helper.getRecords(component, event, helper);
        component.set("v.isloaded", 'true');
        //$A.set("rendered","true");
    },
    /*
    doInit2 : function(component, event, helper) {                
        
            helper.callChangeTab1(component);        	
            helper.loadInfo(component);        
            helper.getsearchFields(component);        
            helper.getLastModificationDate(component);        
            helper.getLastModificationFactDate(component);        
            
            helper.getHeader(component);
            helper.getRecords(component,event,helper);
            var comp = component.find('CWP_facturacion_div');
            $A.util.removeClass(component, 'slds-hide');            
        
    },
    */
    clickSearch: function(component, event, helper) {
        helper.loadInfo(component);
        helper.getsearchFields(component);
        helper.getHeaderSearch(component);
        helper.getRecords(component);

        var newAvance = document.getElementById('paginationBilling').value; //id de la picklist donde se seleccionan los valores de list size        
        var retList = component.get("v.billingList"); //lista completa, sin paginar
        var indexN = Math.min(newAvance, retList.length);
        helper.getPaginatedList(component, event, helper, retList, 0, indexN, newAvance); //metodo que ejecuta la paginacion		
    },

    getBillingId: function(component, event, helper) {
        var popUpFactura = component.get("v.billingList");
        var inicio = component.get("v.inicio");
        var indexTotal = parseInt(inicio) + parseInt(event.currentTarget.dataset.index);
        var selectedFactura = popUpFactura[indexTotal];

        component.set("v.billingId", selectedFactura[0]);
        component.set("v.billingNumber", selectedFactura[1]);
        component.set("v.fechaDeEmision", selectedFactura[2]);
        component.set("v.fechaDeVencimiento", selectedFactura[3]);
        component.set("v.importeFactura", selectedFactura[4]);
        component.set("v.importeTax", selectedFactura[5]);
        component.set("v.tax", selectedFactura[6]);
        helper.showModalBilling(component, event, helper);
    },

    getDebtId: function(component, event, helper) {

        var popUpDebtStatus = component.get("v.billingList");
        var inicio = component.get("v.inicio");
        var indexTotal = parseInt(inicio) + parseInt(event.currentTarget.dataset.index);
        var selectedDebt = popUpDebtStatus[indexTotal];

        component.set("v.debtId", selectedDebt[0]);
        component.set("v.debtReference", selectedDebt[1]);
        component.set("v.debtType", selectedDebt[2]);
        component.set("v.debt0_30", selectedDebt[3]);
        component.set("v.debt30_60", selectedDebt[4]);
        component.set("v.debt60_90", selectedDebt[5]);
        component.set("v.debt90_120", selectedDebt[6]);
        helper.showModalAccount(component, event, helper);
    },

    createDebtTaskAction: function(component, event, helper) {
        helper.callCreateDebtTask(component);
    },

    createAccountStatusTaskAction: function(component, event, helper) {
        helper.callCreateAccounStatusTask(component);
    },

    changeTab1: function(component, event, helper) {
        debugger;
        //document.getElementById('tab2').remove('tab-pane fade in active');
        //var element = document.getElementById('tab1');
        //element.classList.add("tab-pane fade in active");        
        //$('#tab2').removeClass('active in');
        //$('#tab1').addClass('active in');

        component.set("v.listFacturacionHeader", null);
        component.set("v.billingListToShow", null);
        component.set("v.searchText", null);
        component.set("v.tabActive", 'tab1');


        helper.callChangeTab1(component);
        helper.loadInfo(component);
        helper.getsearchFields(component);

        helper.getHeader(component);
        helper.getRecords(component);

        var retList = component.get("v.billingList"); //lista completa, sin paginar
        if (retList.length > 0) {
            debugger;
            var newAvance = document.getElementById('paginationBilling').value; //id de la picklist donde se seleccionan los valores de list size
            document.getElementById("paginationAccountStatus").selectedIndex = 0;
            var indexN = Math.min(newAvance, retList.length);
            helper.getPaginatedList(component, event, helper, retList, 0, indexN, newAvance); //metodo que ejecuta la paginacion                
        }

    },

    changeTab2: function(component, event, helper) {
        debugger;
        //document.getElementById('tab1').remove('tab-pane fade in active');
        //var element = document.getElementById('tab2');
        //element.classList.add("tab-pane fade in active");
        //$('#tab1').removeClass('active in');
        //$('#tab2').addClass('active in');

        component.set("v.listFacturacionHeader", null);
        component.set("v.billingListToShow", null);
        component.set("v.searchText", null);
        component.set("v.tabActive", 'tab2');

        helper.callChangeTab2(component);
        helper.loadInfo(component);
        helper.getsearchFields(component);

        helper.getHeader(component);
        helper.getRecords(component);

        var retList = component.get("v.billingList"); //lista completa, sin paginar
        if (retList.length > 0) {
            debugger;
            var newAvance = document.getElementById('paginationAccountStatus').value; //id de la picklist donde se seleccionan los valores de list size            
            document.getElementById("paginationBilling").selectedIndex = 0;
            var indexN = Math.min(newAvance, retList.length);
            helper.getPaginatedList(component, event, helper, retList, 0, indexN, newAvance); //metodo que ejecuta la paginacion        
        }

    },

    /***	PAGINATION AND SORTING	***/
    getNewAvance: function(component, event, helper) {
        debugger;
        var paginationId = event.target.id
        var newAvance = document.getElementById(paginationId).value; //id de la picklist donde se seleccionan los valores de list size
        var retList = component.get("v.billingList"); //lista completa, sin paginar
        var indexN = Math.min(newAvance, retList.length);
        helper.getPaginatedList(component, event, helper, retList, 0, indexN, newAvance); //metodo que ejecuta la paginacion
    },


    listPaginateForward: function(component, event, helper) {
        event.preventDefault();
        var retList = component.get("v.billingList");
        var index0 = parseInt(component.get("v.inicio"));
        var indexN = parseInt(component.get("v.fin"));
        var avance = parseInt(component.get("v.avance"));
        if (index0 + avance < retList.length) {
            index0 = index0 + avance;
            indexN = indexN + avance;
            indexN = Math.min(indexN, retList.length);
            helper.getPaginatedList(component, event, helper, retList, index0, indexN, avance);
        }

    },

    listPaginateBackward: function(component, event, helper) {
        event.preventDefault();
        var retList = component.get("v.billingList");
        var index0 = parseInt(component.get("v.inicio"));
        var indexN = parseInt(component.get("v.fin"));
        var avance = parseInt(component.get("v.avance"));
        if (indexN - avance > 0) {
            indexN = index0;
            index0 = indexN - avance;
            index0 = Math.max(index0, 0);
            helper.getPaginatedList(component, event, helper, retList, index0, indexN, avance);
        }
    },

    /*
    getFieldToOrderBy : function(component, event, helper){        
        var fieldToOrder =parseInt(event.currentTarget.dataset.index);
        fieldToOrder = fieldToOrder + 1;
        //alert(fieldToOrder);
        var direction = component.get("v.direction");
        if(direction==="DESC"){
            direction="ASC";
        }else{
            direction="DESC"
        }
        component.set("v.direction", direction);
        helper.getOrderedList(component, event, helper, fieldToOrder, direction);        
    },
    */

    getNewOrderedList: function(component, event, helper) {
        event.preventDefault();
        component.set("v.searchText", null);

        helper.loadInfo(component);
        helper.getsearchFields(component);
        //helper.getHeaderSearch(component);        
        helper.getHeader(component);

        var fieldToOrder = parseInt(event.currentTarget.dataset.index);
        var fieldToOrderId = document.getElementById(fieldToOrder);
        var fieldToOrderTitle = fieldToOrderId.title;
        var direction = component.get("v.direction");
        if (direction === "DESC") {
            direction = "ASC";
        } else {
            direction = "DESC"
        }
        component.set("v.direction", direction);

        helper.getNewOrderedList(component, event, helper, fieldToOrderTitle, direction); // ¡ important !        
        helper.getRecords(component, event, helper);

        /*
        var newAvance = document.getElementById('pagination').value; //id de la picklist donde se seleccionan los valores de list size        
        var retList = component.get("v.billingList"); //lista completa, sin paginar
        var indexN = Math.min(newAvance, retList.length);
        helper.getPaginatedList(component, event, helper, retList, 0, indexN, newAvance); //metodo que ejecuta la paginacion		
        */
    },
    
    closeModalBilling : function(component, event, helper) {
        document.getElementById('backgroundmodal').style='display:none';
        helper.hideModalBilling(component, event);
    },
    
     closeModalAccount : function(component, event, helper) {
         document.getElementById('backgroundmodal').style='display:none';
        helper.hideModalAccount(component, event);
    }

})