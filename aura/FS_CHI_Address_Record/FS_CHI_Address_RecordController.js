({
    doInit : function(component, event, helper){
        debugger;
        helper.getAddress(component, event, helper);
    },    
    SelectionNormalizeAddress : function(component, event){
        var indexSelected = event.currentTarget.dataset.index;
        var addresses = component.get('v.enable_addresses');
        
        //Contiene los registros de la direccion normalizada seleccionada
        component.set('v.address_selected', Number(indexSelected));
        component.set('v.address_doubt_selected', -1);
        component.set('v.enable_addresses', addresses);
	},
    SelectionDoubtAddress : function(component, event){
    	var indexSelected = event.currentTarget.dataset.index;
        var addresses = component.get('v.doubt_addresses');
        
        component.set('v.address_doubt_selected', Number(indexSelected));
        component.set('v.address_selected', -1);
        component.set('v.doubt_addresses', addresses);
    },    
    NormalizedAddress: function(component, event, helper){
        helper.setNormalizedAddress(component, event);
    },
    CancelButton: function(component, event, helper){
     	helper.cancelRedirection(component, event);   
    },
    showSpinner : function (component, event, helper) {
    
        debugger;
        var spinner = component.find('spinner');
        var botonesOcultar = component.find('botonesOcultar');
       $A.util.removeClass(spinner,'slds-hide');
        $A.util.addClass(botonesOcultar,'slds-hide');
    },
     hideSpinner : function (component, event, helper) {
         debugger;
        var spinner = component.find('spinner');
         var botonesOcultar = component.find('botonesOcultar');
       $A.util.addClass(spinner,'slds-hide');
         $A.util.removeClass(botonesOcultar,'slds-hide');
    }
})