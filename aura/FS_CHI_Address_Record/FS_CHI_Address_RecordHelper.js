({
    getAddress:function(component, event, helper){
        
        var addressId = component.get("v.addressId");
        var action = component.get("c.SearchAddress");
        action.setParams({
            "addressId": addressId
        });
        
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            if(returnValue != null){
                component.set('v.contact_address_id', returnValue[0].BI_interno_id_direccion__c);
                component.set('v.contact_address_status', returnValue[0].Estado__c);
                component.set('v.contact_address_name', returnValue[0].Name);
                component.set('v.contact_address_street', returnValue[0].BI_Direccion__c);
                component.set('v.contact_address_number', returnValue[0].BI_Numero__c);
                component.set('v.contact_address_prov', returnValue[0].BI_Provincia__c);
                component.set('v.contact_address_country', returnValue[0].BI_Country__c);
                component.set('v.contact_address_postal_code', returnValue[0].BI_Codigo_postal__c);
                component.set('v.contact_address_district', returnValue[0].BI_Distrito__c);
                component.set('v.contact_address_location', returnValue[0].BI_Localidad__c);
                component.set('v.contact_address_floor', returnValue[0].BI_Piso__c);
                component.set('v.contact_address_apartment', returnValue[0].BI_Apartamento__c);
            }
            
            var status = returnValue[0].Estado__c;
            var country = returnValue[0].BI_Country__c;
            
            var message = component.find("message_container");
            var boton = component.find('normalize_button');
            
            if(status == 'Validado'){
                $A.util.removeClass(message,'slds-hide');
                $A.util.addClass(message,'slds-show');
                
                $A.util.removeClass(boton,'slds-show');
                $A.util.addClass(boton,'slds-hide');
                
                component.set('v.dynamic_message', 'Esta dirección ya se encuentra validada');
            } else if(country != 'Chile'){
                $A.util.removeClass(message,'slds-hide');
                $A.util.addClass(message,'slds-show');
                
                $A.util.removeClass(boton,'slds-show');
                $A.util.addClass(boton,'slds-hide');
                
                component.set('v.dynamic_message', 'No es posible nomalizar direcciones para el país informado ');
            } else helper.getNormalizedAddresses(component, event, helper);
        });
        $A.enqueueAction(action);
    },
    getNormalizedAddresses:function(component, event, helper){
        
        var addressId = component.get("v.addressId");
        var action = component.get("c.getNormalizedAddress");
        
        action.setParams({
            "addressId": addressId
        });
        
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            
            if(returnValue != null){
                var res = JSON.parse(returnValue);
                
                component.set('v.enable_addresses', res[0]);
                component.set('v.doubt_addresses', res[1]);
                component.set('v.address_selected', 0);
                component.set('v.address_doubt_selected', -1);
            } else {
                var message = component.find("message_container");
                var boton = component.find('normalize_button');
                $A.util.removeClass(message,'slds-hide');
                $A.util.addClass(message,'slds-show');
                
                $A.util.removeClass(boton,'slds-show');
                $A.util.addClass(boton,'slds-hide');
                
                component.set('v.dynamic_message', 'Ha ocurrido un problema al validar la dirección.');
            }
        });
        $A.enqueueAction(action);
    },
    setNormalizedAddress:function(component, event){

        var indexSelectedAddress = null;
        var addresses = null;
        var action = component.get("c.updateAddress");
        
        if(component.get("v.address_selected") == -1){
            indexSelectedAddress = component.get("v.address_doubt_selected");
            addresses = component.get('v.doubt_addresses');
        } else {
            indexSelectedAddress = component.get("v.address_selected");
            addresses = component.get('v.enable_addresses');
        }
        
        action.setParams({
            addressId: component.get("v.addressId"),
            direccion_2: addresses[indexSelectedAddress].BI_Direccion_2__c,
            pais: addresses[indexSelectedAddress].BI_Country__c,
            provincia: addresses[indexSelectedAddress].BI_Provincia__c,
            distrito: addresses[indexSelectedAddress].BI_Distrito__c,
            localidad: addresses[indexSelectedAddress].BI_Localidad__c,
            calle: addresses[indexSelectedAddress].BI_Direccion__c,
            numero: addresses[indexSelectedAddress].BI_Numero__c,
            comple1: addresses[indexSelectedAddress].BI_COL_Complemento_1__c,
            comple2: addresses[indexSelectedAddress].BI_COL_Complemento_2__c,
            piso: addresses[indexSelectedAddress].BI_Piso__c,
            apartamento: addresses[indexSelectedAddress].BI_Apartamento__c,
            codigoPostal: addresses[indexSelectedAddress].BI_Codigo_postal__c,
            longitud: ""+addresses[indexSelectedAddress].BI_Longitud__Longitude__s+"",
            latitud: ""+addresses[indexSelectedAddress].BI_Longitud__Latitude__s+"",
            bloque: addresses[indexSelectedAddress].FS_CHL_Bloque__c,
            tipoComplejo: addresses[indexSelectedAddress].FS_CHL_Tipo_Complejo_Habitacional__c,
            nomComplejo: addresses[indexSelectedAddress].FS_CHL_Nombre_Complejo_Habitacional__c,
            numCasilla: addresses[indexSelectedAddress].FS_CHL_Numero_de_Casilla__c,
            zonaRed: ""+addresses[indexSelectedAddress].FS_CHL_Es_Zona_Roja__c,
            listaNegra: ""+addresses[indexSelectedAddress].FS_CHL_Es_Lista_Negra__c                        
        });
        action.setCallback(this, function(a) {
            debugger;
            var returnValue = a.getReturnValue();
            if(returnValue != null && returnValue == 'SUCCESS'){
                var addressIdURL ='/'+component.get("v.addressId");
                if( (typeof sforce != 'undefined') && (sforce != null) ) {
                    sforce.one.navigateToURL(addressIdURL);
                } else {
                    window.location.assign(addressIdURL);
                }   
            } else {
                var message = component.find("message_container");
                var boton = component.find('normalize_button');
                $A.util.removeClass(message,'slds-hide');
                $A.util.addClass(message,'slds-show');
                
                $A.util.removeClass(boton,'slds-show');
                $A.util.addClass(boton,'slds-hide');
                
                component.set('v.dynamic_message', 'Ha ocurrido un problema al intentar normalizar la dirección. Favor intentelo más tarde.');
            }
        });
        $A.enqueueAction(action);
    },
    cancelRedirection:function(component, event){
        var addressId = '/'+component.get("v.addressId");
        if( (typeof sforce != 'undefined') && (sforce != null) ) {
            sforce.one.navigateToURL(addressId);
        } else {
            window.location.assign(addressId);
        }
    }
})