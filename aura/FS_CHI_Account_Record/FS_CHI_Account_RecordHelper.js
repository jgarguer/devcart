({
	notificate : function(component, event, helper, isWarning, notification){
        /* Variable Configuration */
        /* Empty */
        
        /*Text Definition*/
        if(isWarning) {
        component.set('v.Notification_Title_Value', 'Advertencia');
        }
        else {
              component.set('v.Notification_Title_Value', 'Información');
        }                  
        component.set('v.Notification_Body_Value',notification);
    },
    check : function(component, event, helper, elementId, containerId, messageId){
        /* Variable Configuration */
        	/* Global */
        	var correct = true;
        
        	/* Fields */
        	var element = component.find(elementId);
        	var container = component.find(containerId);
        	var message = component.find(messageId);
        
        /* Check Integrity */
         var temp = element.get('v.value');
         if(temp == null || temp == '') correct = false;
         else{
             if(elementId == 'CMP_Email'){
                 if(temp.includes('@') && temp.includes('.')) correct = true;
                 else correct = false;
             }else correct = true;
         }
         
         if(!correct){
             $A.util.addClass(container,'slds-has-error');
             $A.util.removeClass(message,'slds-hide');
         }else{
             $A.util.removeClass(container,'slds-has-error');
             $A.util.addClass(message,'slds-hide');
         }
         
         return correct;
    },
    assing : function(component, event, helper, input, value){
        /* Variable Configuration */
        var element = component.find(input);
        
        element.set("v.value", value);
        element.set("v.disabled",value!='');
    },
    validate : function(component, event, helper, documentType, documentNumber){
        /*Variable Declaration*/
        /* Empty */
        
        var action = component.get("c.retrieveJSON");
        action.setParams({documentType: documentType, documentValue: documentNumber});
        
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = JSON.parse(response.getReturnValue());
                if(returnValue != null && returnValue.totalResults == 1){
                    /* JSON */
                    if(returnValue.customersList.customerList[0].customerDetails != null && returnValue.customersList.customerList[0].customerDetails.customerName != null) helper.assing(component, event, helper, 'CMP_Name', returnValue.customersList.customerList[0].customerDetails.customerName);
               /*     if(returnValue.customerList[0].contactingModes != null){
                       returnValue.customerList[0].contactingModes.forEach(function(element) {
                           if(element.contactMode == 'phone')  helper.assing(component, event, helper, 'CMP_Phone', element.details);
                          else if(element.contactMode == 'fax') helper.assing(component, event, helper, 'CMP_Fax', element.details);
                        });
                    }*/
                    
                    /* Flags */
                    component.set('v.Manual_Value', false);
                    helper.notificate(component, event, helper, false, 'El cliente ha sido validado.');
                }else{
                    /* Flags */
                    component.set('v.Manual_Value', true);
                    helper.notificate(component, event, helper, true, 'No ha sido posible encontrar al cliente.');
                }
                
                /* Flags */
                component.set('v.Show_buttons', true);
            }else{
                component.set('v.Manual_Value', true);
                    helper.notificate(component, event, helper, true, 'Error al validar el cliente');
                component.set('v.Validated_Value', true);
            }
        });
        $A.enqueueAction(action);
    },
    //create : function(component, event, helper, country, documentType, documentValue, name, phone, fax, currency, manual){
    create : function(component, event, helper, country, documentType, documentValue, name, currency, manual){    
        var action = component.get("c.createAccount");
        //action.setParams({country: country, documentType: documentType, documentValue: documentValue, name: name, phone: phone, fax: fax, currencyCode: currency, manual: manual});
        action.setParams({country: country, documentType: documentType, documentValue: documentValue, name: name, currencyCode: currency, manual: manual});
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var returnValue = JSON.parse(response.getReturnValue());
                if(returnValue != null){
                    if(returnValue.id != null) window.location.href = 'https://' + window.location.host + '/' + returnValue.id;
                    else if(returnValue.error != null)
                    {
                        var type = returnValue.error;
                        
                        if(type.includes("INVALID_EMAIL_ADDRESS")) helper.notificate(component, event, helper, true, 'La dirección de correo electrónico no válida.');
                        else if(type.includes("FIELD_CUSTOM_VALIDATION_EXCEPTION")){
                            if(type.includes("Identificador fiscal")) helper.notificate(component, event, helper, true, 'El número de identificador fiscal no dispone del formato adecuado. Ej. 20515888k');
                            if(type.includes("Documento fiscal")) helper.notificate(component, event, helper, true, 'Tipo de documento no válido para el país del contacto.');
                            }else if(type.includes("DUPLICATE_VALUE")){
                               helper.notificate(component, event, helper, true, 'El cliente ya existe');
                             }else  helper.notificate(component, event, helper, true, type);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getSelectList : function(component, event, helper, attribute, sObject, field) {
        var action = component.get('c.getSelectOptions');
		action.setParams({ 
            'objObject':  sObject,
            'fld' : field
        });
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
                console.log(response.getReturnValue());
                if (field == 'BI_Country__c'){
                    var index = response.getReturnValue().indexOf("Chile")
                    var result = response.getReturnValue().slice(index, index+1);
                    console.log(result);
                } else if (field == 'BI_Tipo_de_identificador_fiscal__c'){
                    var index = response.getReturnValue().indexOf("RUT")
                    var result = response.getReturnValue().slice(index, index+1);
                    console.log("encuentra rut "+result);
                } else if (field == 'CurrencyIsoCode'){
                    var index = response.getReturnValue().indexOf("CLP")
                    var result = response.getReturnValue().slice(index, index+1);
                    console.log("encuentra curr "+result);
                }
                component.set(attribute, result);
			} else {
				console.log('no he cargado cosas');
			}
		});
		$A.enqueueAction(action); 
    }
})