({
    doInit : function(component, event, helper){
        helper.getSelectList(component, event, helper, "v.countryList", component.get("v.objInfo"), "BI_Country__c");
        helper.getSelectList(component, event, helper, "v.documentTypeList", component.get("v.objInfo"), "BI_Tipo_de_identificador_fiscal__c");
        helper.getSelectList(component, event, helper, "v.currencyList", component.get("v.objInfo"), "CurrencyIsoCode");
    },
	validate : function(component, event, helper) {
        /* Variable Configuration */
        var mandatory = true;
        var documentType, documentValue;
        
        /* Mandatory Fields Validation */
        mandatory = (helper.check(component, event, helper,'CMP_Document_Number', 'CMP_Container_Document_Number', 'CMP_Container_Inside_Document_Number') && mandatory);
        
        /* Invoke */
        if(mandatory){
            console.log(component.find('CMP_Country').get('v.value'));
            if(component.find('CMP_Country').get('v.value')!= 'Chile'){
                console.log('holi' +component.find('CMP_Country').get('v.value'));
                component.set('v.Validated_Value', true);
                component.set('v.Manual_Value', true);
                helper.notificate(component, event, helper, true, 'Actualmente, no es posible validar contactos cuyo país sea distinto a Chile.');
            } else{
                documentType = component.find('CMP_Document_Type').get('v.value');
                documentValue = component.find('CMP_Document_Number').get('v.value');
                
                helper.validate(component, event, helper, documentType, documentValue);
            }
        }
	},
    create : function(component, event, helper) {
        /* Variable Configuration */
        var mandatory = true;
        //var country, documentType, documentValue, name, phone, fax, currency, manual;
        var country, documentType, documentValue, name, currency, manual;
        /* Mandatory Fields Validation */
        mandatory = (helper.check(component, event, helper,'CMP_Document_Number', 'CMP_Container_Document_Number', 'CMP_Container_Inside_Document_Number') && mandatory);
        mandatory = (helper.check(component, event, helper,'CMP_Name', 'CMP_Container_Name', 'CMP_Container_Inside_Name') && mandatory);
        
        /* Invoke */
        if(mandatory){
            /* Retrieve Data */
            if(component.find('CMP_Country').get('v.value') != null) country = component.find('CMP_Country').get('v.value');
            if(component.find('CMP_Document_Type').get('v.value') != null) documentType = component.find('CMP_Document_Type').get('v.value');
            if(component.find('CMP_Document_Number').get('v.value') != null) documentValue = component.find('CMP_Document_Number').get('v.value');
            
            if(component.find('CMP_Name').get('v.value') != null) name = component.find('CMP_Name').get('v.value');
         /*   if(component.find('CMP_Phone').get('v.value') != null) phone = component.find('CMP_Phone').get('v.value');*/
            
            if(component.find('CMP_Currency').get('v.value') != null) currency = component.find('CMP_Currency').get('v.value');
            
            /* Creation Type */
            manual = component.get('v.Manual_Value');
            
            /* Invoke */
            //helper.create(component, event, helper, country, documentType, documentValue, name, phone, fax, currency, manual);
            helper.create(component, event, helper, country, documentType, documentValue, name, currency, manual);
        }
	},
    minimize : function(component, event, helper) {
        /* Warning Restoration */
        component.set('v.Notification_Title_Value', '');
        component.set('v.Notification_Body_Value', '');
    },
    reset : function(component, event, helper) {
        console.log(component.find('CMP_Country').get('v.value'));
        /* Interface Restoration */
        if(component.get('v.Validated_Value')){
            helper.assing(component, event, helper, 'CMP_Document_Type', '');
            helper.assing(component, event, helper, 'CMP_Document_Number', '');
            
            helper.assing(component, event, helper, 'CMP_Name', '');
            helper.assing(component, event, helper, 'CMP_Phone', '');
            helper.assing(component, event, helper, 'CMP_Fax', '');
            
            /* Notification */
            helper.notificate(component, event, helper, true, 'La información fiscal validada ha sido modificada, introduzca la información del nuevo cliente.');
        }
        
        /* Internal Control */
        component.set('v.Validated_Value', false);
        component.set('v.Manual_Value', true);
    },
    dismiss : function(component, event, helper) {
        /* Variable Configuration */
        var userAgent = navigator.userAgent;
        
        if(userAgent.includes('Macintosh') || userAgent.includes('Windows')) window.location.href = 'https://' + window.location.host +'/001/o';
        else force.one.back(true);
    },
    showSpinner : function (component, event, helper) {
    
       
        var spinner = component.find('spinner');
        var botonesOcultar = component.find('botonesOcultar');
       $A.util.removeClass(spinner,'slds-hide');
        $A.util.addClass(botonesOcultar,'slds-hide');
    },
     hideSpinner : function (component, event, helper) {
       
        var spinner = component.find('spinner');
         var botonesOcultar = component.find('botonesOcultar');
       $A.util.addClass(spinner,'slds-hide');
         $A.util.removeClass(botonesOcultar,'slds-hide');
    }
})