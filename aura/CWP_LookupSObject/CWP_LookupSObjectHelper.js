({
    initStatus: {},
    init: function(cmp) {
        var required = cmp.get('v.required');
        if (required) {
            var cmpTarget = cmp.find('lookup');
            $A.util.addClass(cmpTarget, 'slds-is-required');
        }
        if (cmp.get('v.classLookup')) {
            $A.util.addClass(cmp.find('lookup'), cmp.get('v.classLookup'));
        }
        if (cmp.get('v.classLookuplist')) {
            $A.util.addClass(cmp.find('lookuplist'), cmp.get('v.classLookuplist'));
        }
        if (cmp.get('v.classLookupPill')) {
            $A.util.addClass(cmp.find('lookup-pill'), cmp.get('v.classLookupPill'));
        }
        if (cmp.get('v.classLookupLabel')) {
            $A.util.addClass(cmp.find('lookupLabel'), cmp.get('v.classLookupLabel'));
        }
    },
    doSearchInterval: function(component) {
        try {
            var that = this;
            var callMethod = function() {
                that.doSearch(component);
            }
            var timeout = component.get('v.timeout');
            if (timeout) {
                clearTimeout(timeout);
                timeout = null;
            }
            timeout = setTimeout(callMethod, 2000);
            component.set('v.timeout', timeout);
        } catch (err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                var start = errors[0].message.indexOf('Caused by: common.apex.runtime.impl.ExecutionException: ') + 56;
                var error = errors[0].message.substring(start).trim();
                component.set("v.error", error);
                var errorcmp = component.find("errorMsg");
                $A.util.removeClass(errorcmp, "toggle");
            }
        }
    },
    doSearch: function(cmp) {
        try {
            var searchString = cmp.get('v.searchString').replace(/[^a-zA-Z0-9 ]/gi, '');
            cmp.set('v.searchString', searchString);
            var inputElement = cmp.find('lookup');
            var lookupList = cmp.find('lookuplist');
            var lookupPill = cmp.find('lookup-pill');
            inputElement.set('v.errors', null);
            if (typeof searchString === 'undefined' || searchString.length < 2) {
                $A.util.addClass(lookupList, 'slds-hide');
                $A.util.addClass(lookupPill, 'slds-hide');
                return;
            }
            $A.util.removeClass(lookupList, 'slds-hide');
            var sObjectAPIName = cmp.get('v.sObjectAPIName');
            var filter = cmp.get('v.filter');
            var action = cmp.get('c.lookup');
            var queryLanguage = cmp.get('v.queryLanguage');
            action.setAbortable();
            action.setParams({
                "searchString": searchString,
                "sObjectAPIName": sObjectAPIName,
                "filter": filter,
                "queryLanguage": queryLanguage
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (cmp.isValid() && state === "SUCCESS") {
                    var matches = response.getReturnValue();
                    if (matches.length === 0) {
                        cmp.set('v.matches', null);
                        return;
                    }
                    cmp.set('v.matches', matches);
                    $A.util.removeClass(lookupList, 'slds-hide');
                    $A.util.addClass(lookupPill, 'slds-hide');
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            this.displayToast('Error', errors[0].message);
                        }
                    } else {
                        this.displayToast('Error', 'Unknown error.');
                    }
                }
            });
            $A.enqueueAction(action);
        } catch (err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                var start = errors[0].message.indexOf('Caused by: common.apex.runtime.impl.ExecutionException: ') + 56;
                var error = errors[0].message.substring(start).trim();
                cmp.set("v.error", error);
                var errorcmp = cmp.find("errorMsg");
                $A.util.removeClass(errorcmp, "toggle");
            }
        }
    },
    handleSelection: function(cmp, event) {
        var objectId = this.resolveId(event.currentTarget.id);
        cmp.set('v.recordId', objectId);
        var objectLabel = event.currentTarget.innerText;
        cmp.set('v.name', objectLabel);
        cmp.set("v.searchString", objectLabel);
        var evt = $A.get('e.c:CWP_UpdateLookupId');
        evt.setParams({
            'sObjectId': objectId,
            'componentId': cmp.get('v.componentId'),
            'name': objectLabel
        });
        evt.fire();
        var func = cmp.get('v.callback');
        if (func) {
            func({
                id: objectId,
                name: objectLabel
            });
        }
        $A.util.addClass(cmp.find("lookuplist"), 'slds-hide');
        $A.util.addClass(cmp.find('lookup'), 'slds-hide');
        $A.util.removeClass(cmp.find("lookup-pill"), 'slds-hide');
        $A.util.addClass(cmp.find('lookup-div'), 'slds-has-selection');
        
    },
    toggleClass: function(id, style) {
        $A.util.toggleClass(id, style);
    },
    clearSelection: function(cmp, event) {
        var objectId = this.resolveId(event.currentTarget.id);
        cmp.set("v.searchString", '');
        cmp.set('v.recordId', null);
        var evt = $A.get('e.c:CWP_UpdateLookupId');
        evt.setParams({
            'sObjectId': objectId,
            'componentId': cmp.get('v.componentId'),
            'name': ''
        });
        evt.fire();
        var func = cmp.get('v.callback');
        if (func) {
            func({
                id: '',
                name: ''
            });
        }
        $A.util.addClass(cmp.find("lookup-pill"), 'slds-hide');
        $A.util.addClass(cmp.find('lookuplist'), 'slds-hide');
        $A.util.removeClass(cmp.find('lookup-div'), 'slds-has-selection');
        $A.util.removeClass(cmp.find('lookup'), 'slds-hide');
    },
    resolveId: function(elmId) {
        var i = elmId.lastIndexOf('_');
        return elmId.substr(i + 1);
    },
    displayToast: function(title, message) {
        var toast = $A.get("e.force:showToast");
        if (toast) {
            toast.setParams({
                "title": title,
                "message": message
            });
            toast.fire();
        }
    },
    loadFirstValue: function(cmp) {
        var action = cmp.get('c.getCurrentValue');
        var self = this;
        action.setParams({
            'type': cmp.get('v.sObjectAPIName'),
            'value': cmp.get('v.recordId'),
        });
        action.setCallback(this, function(a) {
            if (a.error && a.error.length) {
                return $A.error('Unexpected error: ' + a.error[0].message);
            }
            var result = a.getReturnValue();
            cmp.set("v.searchString", result);
            if (null != result) {
                this.toggleClass(cmp.find("lookup-pill"), 'slds-hide');
                this.toggleClass(cmp.find('lookup-div'), 'slds-has-selection');
            }
        });
        $A.enqueueAction(action);
    }
})