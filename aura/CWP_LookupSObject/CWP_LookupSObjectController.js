({
    /**
     * Search an SObject for a match
     */
    search: function(cmp, event, helper) {
        var isVF = cmp.get('v.theme');
        
        if (isVF !== undefined) { //Visualforce
            helper.doSearch(cmp);
        } else { //Lightning
            helper.doSearchInterval(cmp);
        }
    },
    
    /**
     * Select an SObject from a list
     */
    select: function(cmp, event, helper) {
        helper.handleSelection(cmp, event);
    },
    /**
     * Clear the currently selected SObject
     */
    clear: function(cmp, event, helper) {
        helper.clearSelection(cmp, event);
    },
    
    init: function(cmp, event, helper) {
        try {
            //first load the current value of the lookup field
            helper.init(cmp);
            helper.loadFirstValue(cmp);
            
        } catch (ex) {
            cmp.set("v.error", ex);
        }
    }
})