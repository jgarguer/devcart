({
	doInit: function(component, event, helper) {
        console.log('KQC!!!!!');
        component.set("v.iFrameBulkGridURL", "https://" + window.location.hostname + "/" + window.location.pathname.split("/")[1] + "/apex/CWP_AC_BulkAutoQuotesVf");        
        console.log("RNAdebug -> doInit BAQCjs");
        //RNA 19/04/2018 - Commenting needless line. We are getting UserAcc and CliAcc in the event message.
        //helper.getuseracc(component);
       
        
        //RNA 25/01/2018 - Adding listener to redirect to endBulk Order URL
        var listener = function(eventUrl){
			if(component.isValid()){
                console.log("RNAdebug -> BAQCjs -> listener function eventURL");
                console.log(eventUrl.detail);
 				eventUrl.stopImmediatePropagation();
				var valuePassed = eventUrl.detail; 
                //RNA 30/01/2018 - Opening new window with PDF and redirecting to summary
                window.open('/empresasplatino/apex/CWP_AC_OfertaAutocotizador?orderId=' + valuePassed, '_blank', 'toolbar=0,location=0,menubar=0');        
                window.open('/empresasplatino/s/order/' + valuePassed, '_self');
            }
            
            var valuePassed = eventUrl.detail; 
            if(valuePassed=='emptyClient'){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": $A.get("$Label.c.CWP_AC_CustomerFieldRequired"),
                    "type": "error"
                });
                toastEvent.fire();
            }
            
		}
        
        var listenerError = function(eventUrlError){
			if(component.isValid()){
                console.log("RNAdebug -> BAQCjs -> listener function eventURLERROR");
                console.log(eventUrlError.detail);
                
 				eventUrlError.stopImmediatePropagation();
				var valuePassed = eventUrlError.detail; 
                if(valuePassed=='emptyClient'){
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": $A.get("$Label.c.CWP_AC_CustomerFieldRequired"),
                            "type": "error"
                        });
                        toastEvent.fire();
                }
                if(valuePassed=='dataNotValid'){
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title: "Error!",
                            message: $A.get("$Label.c.CWP_AC_FieldValidationError"),
                            type: "error"
                        });
                        toastEvent.fire();
                }
                
                if(valuePassed=='emptyTable'){
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title: "Error!",
                            message: $A.get("$Label.c.CWP_AC_NoValidRows"),
                            type: "error"
                        });
                        toastEvent.fire();
                }
                
              
                
                //MOF 05/02/2018 - Validation for max. 100 sites in the grid -> Throwing error
                if(valuePassed=='limitRows'){
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title: "Error!",
                            message: $A.get("$Label.c.CWP_AC_RowsLimit"),
                            type: "error"
                        });
                        toastEvent.fire();
                }
                
                //RNA 05/02/2018 - Validation for mismatching ServicesCost <-> Bulk data
                if(valuePassed=='nullBulk'){
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title: "Mismatch!",
                            message: $A.get("$Label.c.CWP_AC_SiteMismatch"),
                            type: "warning"
                        });
                        toastEvent.fire();
                }
                
                
  				if(valuePassed=='emptyAddress'){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Address error!",
                        "message": $A.get("$Label.c.CWP_AC_AddressError"),
                        "type": "error"
                    });
                    toastEvent.fire();
                }
                if(valuePassed=='bulkFired'){
                    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title: "BulkFired!",
                            message: $A.get("$Label.c.CWP_AC_BulkFired"),
                            type: "info"
                        });
                        toastEvent.fire();
                }
            }
		}
        
        window.document.addEventListener("bulkRedirection", listener, false);
        window.document.addEventListener("bulkError", listenerError, false);

        
        console.log('init component');
    	
        var vfOrigin = "https://" + window.location.hostname;
        window.addEventListener("message", function(event) {
            console.log(event.origin);
            console.log(event.data);
           
            //SECURITY FILTER
            /*if (event.origin !== vfOrigin) {
                console.log('Not the expected origin: Reject the message!');
                console.log(event.origin);
                console.log(vfOrigin);
                return;
            }*/
            
            // Handle the message
            //component.set("v.iFrameBulkProgressURL", "https://" + window.location.hostname + "/" + window.location.pathname.split("/")[1] + "/apex/CWP_AC_BulkProgress?BIRId=");
            //component.set("v.birId",event.data);
            component.set("v.gridVisible",true);
	
            console.log(event.data);
        }, false);
        console.log('KQC!');
        var action = cmp.get("c.getAccClient");
        action.setParams({'firstname' : cmp.get("v.identifier")});
        action.setCallback(this,(response) => {
            var state = response.getState();
            alert('state' + state);
        });
        
        $A.enqueueAction(action);
    },
    importData : function(component, event, helper) {
        event.preventDefault();
        console.log('Init import');
    	//helper.startImport(component);	
    },
    backButton : function(cmp, evt, helper){
        helper.crearCmp(cmp, "c:CWP_AC_QuoteHistory");
        console.log("eventback");
    },
    startImport : function(component, event, helper) {
        var message = "startImport";
        var useraccount = component.get("v.useracc");
        message = message + "/" + useraccount;
        //RNAdebug 24/01/2018 - Changing URL from VisualForce Origin
        var vfOrigin = "https://" + window.location.hostname + "/" + window.location.pathname.split("/")[1] + "/apex/CWP_AC_BulkAutoQuotesVf"; 
        var vfWindow = component.find("iFrameBulkGrid").getElement().contentWindow;
        vfWindow.postMessage(message, vfOrigin);
    },
    exportCSV : function(component, event, helper) {
        var message = "exportCSV";
        var vfOrigin = "https://" + window.location.hostname + "/" + window.location.pathname.split("/")[1] + "/apex/CWP_AC_BulkAutoQuotesVf"; 
        var vfWindow = component.find("iFrameBulkGrid").getElement().contentWindow;        
        vfWindow.postMessage(message, vfOrigin);
    },
})