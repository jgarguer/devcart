({
    init: function(component, event, helper) {
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        document.getElementById("backgroundmodalwait").style.display='block';
        helper.getLegalEntityPickList(component, event);
        helper.getTier1PickList(component, event);
    },
    businessUnitPickList: function(component, event, helper) {
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        var valueSel = document.getElementById("entity").value;
        component.set('v.case.CWP_Legal_Entity__c', valueSel);
        if(valueSel == 'none'){
        	document.getElementById('business').value = 'none';
        	document.getElementById('business').disabled = true;
        }else{
        	document.getElementById('business').disabled = false;
        }
        helper.getBusinessUnitPicklist(component, event);
    },
    loadTier2: function(component, event, helper) {
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        helper.getTier2PickList(component, event);
        var valueSel = document.getElementById("tier1picklist").value;
        component.set('v.case.TGS_Op_Commercial_Tier_1__c', valueSel);
        if(valueSel == 'none'){
        	document.getElementById('tier2picklist').disabled = true;
        	document.getElementById('tier3picklist').disabled = true;
        	document.getElementById('tier2picklist').value = 'none';
        	document.getElementById('tier3picklist').value = 'none';
        }else{
        	document.getElementById('tier2picklist').disabled = false;
        	document.getElementById('tier3picklist').disabled = true;
        }
    },
    loadTier3: function(component, event, helper) {
        component.set('v.error', '');
        var err = component.find("errorMsg");
        $A.util.addClass(err, "toggle");
        helper.getTier3PickList(component, event);
        var valueSel = document.getElementById("tier2picklist").value;
        component.set('v.case.TGS_Op_Commercial_Tier_2__c', valueSel);
        if(valueSel == 'none'){
        	document.getElementById('tier3picklist').disabled = true;
        	document.getElementById('tier3picklist').value = 'none';
        }else{
        	document.getElementById('tier3picklist').disabled = false;
        }
    }
})