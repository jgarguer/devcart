({
    getLegalEntityPickList: function(component, event) {
        var self = this;
    	var errorcmp=component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getLegalEntityPickList');
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
	        component.set("v.error", '');
	        $A.util.addClass(errorcmp, "toggle");
            component.set('v.entityList', result.getReturnValue());
            var entityList = component.get('v.entityList');
            if(entityList.length == 1){
            	component.set('v.entity', entityList[0].value);
            	self.getBusinessUnitPicklist(component, event);
            }else{
            	document.getElementById('business').disabled = true;
            }
            document.getElementById("backgroundmodalwait").style.display='none';
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                var start = errors[0].message.indexOf('Caused by: common.apex.runtime.impl.ExecutionException: ') + 56;
                var error = errors[0].message.substring(start).trim();
                component.set("v.error", error);
                $A.util.removeClass(errorcmp, "toggle");
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        })
    },

    getBusinessUnitPicklist: function(component, event) {
    	var errorcmp=component.find("errorMsg");
        var entity = document.getElementById('entity').value;
        component.set('v.entity', entity);
        var promise = new Promise(function(resolve, reject) {            
            var action = component.get('c.getBusinessUnitPicklist');
            action.setParams({
                'legalEntityValue': component.get('v.entity')
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
        	component.set("v.error", '');
	        $A.util.addClass(errorcmp, "toggle");
            component.set('v.businessUnitList', result.getReturnValue());
            var businessUnitList = component.get('v.businessUnitList');
            if(businessUnitList.length == 1){
            	component.set('v.case.CWP_Business_Unit__c', businessUnitList[0].value);
            	document.getElementById('business').value = businessUnitList[0].value;
            }
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                var start = errors[0].message.indexOf('Caused by: common.apex.runtime.impl.ExecutionException: ') + 56;
                var error = errors[0].message.substring(start).trim();
                component.set("v.error", error);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },

    getTier1PickList: function(component, event) {
    	var errorcmp=component.find("errorMsg");
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getTier1PickList');
            action.setParams({});
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
        	component.set("v.error", '');
	        $A.util.addClass(errorcmp, "toggle");
            component.set('v.tier1PickList', result.getReturnValue());
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                var start = errors[0].message.indexOf('Caused by: common.apex.runtime.impl.ExecutionException: ') + 56;
                var error = errors[0].message.substring(start).trim();
                component.set("v.error", error);
                $A.util.removeClass(errorcmp, "toggle");
            }
        })
    },

    getTier2PickList: function(component, event) {
        document.getElementById("backgroundmodalwait").style.display='block';
    	var errorcmp=component.find("errorMsg");
        var tier1 = document.getElementById('tier1picklist').value;
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getTier2PickList');
            action.setParams({
                'tier1Value': tier1
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
        	component.set("v.error", '');
	        $A.util.addClass(errorcmp, "toggle");
            component.set('v.tier2PickList', result.getReturnValue());
            document.getElementById("backgroundmodalwait").style.display='none';
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                var start = errors[0].message.indexOf('Caused by: common.apex.runtime.impl.ExecutionException: ') + 56;
                var error = errors[0].message.substring(start).trim();
                component.set("v.error", error);
                $A.util.removeClass(errorcmp, "toggle");
            }
            document.getElementById("backgroundmodalwait").style.display='none';
        })
    },
    getTier3PickList: function(component, event) {
    	var errorcmp=component.find("errorMsg");
        document.getElementById("backgroundmodalwait").style.display='block';
        var tier2 = document.getElementById('tier2picklist').value;
        var promise = new Promise(function(resolve, reject) {
            var action = component.get('c.getTier3PickList');
            action.setParams({
                'tier2Value': tier2
            });
            action.setCallback(this, function(result) {
                if (result.getState() === 'SUCCESS') {
                    resolve(result);
                } else {
                    reject(result);
                }
            })
            $A.enqueueAction(action);
        });
        promise.then(function(result) {
        	component.set("v.error", '');
	        $A.util.addClass(errorcmp, "toggle");
            component.set('v.tier3PickList', result.getReturnValue());
            document.getElementById("backgroundmodalwait").style.display='none';
        }, function(err) {
            var errors = err.getError();
            if (errors[0] && errors[0].message) {
                var start = errors[0].message.indexOf('Caused by: common.apex.runtime.impl.ExecutionException: ') + 56;
                var error = errors[0].message.substring(start).trim();
                component.set("v.error", error);
                $A.util.removeClass(errorcmp, "toggle");
            }
            document.getElementById("backgroundmodalwait").style.display='none';
        })
    },
})