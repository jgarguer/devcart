({
	calculateProgressBar : function(checks) {
        var progress = 0;
        var factor = 100/checks.length;
        for(var i = 0; i < checks.length; i++){
            if (checks[i] == true) {
                progress+=factor;
            }
        }
        
        return progress;
		
	}
})