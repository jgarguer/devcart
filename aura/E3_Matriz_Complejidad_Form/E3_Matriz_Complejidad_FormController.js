({
	updateMatrix : function(component, event) {
        var value = event.getParam("message");
        console.log("Received application event with param = "+ value);
        var complexityMatrix = component.get("v.complexityMatrix");
        console.log("Create expense: " + JSON.stringify(complexityMatrix));
        // Validate matrix id parameter is sended
        if (complexityMatrix != null) {
            // Update complextity matrix
            var action = component.get("c.updateComplexityMatrix");
        	action.setParams({matrix : complexityMatrix});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log('guardado :D');
                }
                else if (state === "INCOMPLETE") {
                    console.log('INCOMPLETE');
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            
			// enqueue action
        	$A.enqueueAction(action);
        } else {
            console.log('ERROR MATRIZ DE COMPLEJIDAD NO GUARDADA');
        }

    },
    
    doInit: function(component) {
        // Get complexity matrix id
        var complexityMatrix; 
		var matrixId = null;
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if(pair[0] == 'matrixId'){ 
                matrixId= pair[1];
            }
        }
        
        // Validate matrix id parameter is sended
        if (matrixId != null) {
            var action = component.get("c.getComplexityMatrix");
        	action.setParams({ matrixId : matrixId });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    complexityMatrix = response.getReturnValue();
                    // Send complexity matrix to view
                    component.set('v.complexityMatrix', complexityMatrix);
                }
                else if (state === "INCOMPLETE") {
                    console.log('INCOMPLETE');
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                     errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            
			// enqueue action
        	$A.enqueueAction(action);
        } else {
            console.log('ERROR MATRIZ DE COMPLEJIDAD NO CARGADA');
        }
	}
	
})