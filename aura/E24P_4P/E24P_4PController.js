({
	handleClickInicio : function(component, event, helper) {
		var cmpTarget = component.find('inicio');
        $A.util.addClass(cmpTarget, 'negrita');
        var cmpTarget = component.find('conoce');
        $A.util.removeClass(cmpTarget, 'negrita');
        component.set("v.inicio",true);
        component.set("v.conoce",false);
	},
    
   	handleClickConoce : function(component, event, helper) {
		var cmpTarget = component.find('conoce');
        $A.util.addClass(cmpTarget, 'negrita');
   		var cmpTarget = component.find('inicio');
    	$A.util.removeClass(cmpTarget, 'negrita');
        component.set("v.conoce",true);
        component.set("v.inicio",false);
	}
})