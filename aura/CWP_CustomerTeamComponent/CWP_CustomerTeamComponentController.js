({
    doInit : function(component, event, helper) {    		
        var getRecordsAction = component.get("c.getRecordsView");    
        getRecordsAction.setCallback(this, function(actionResult) {
            debugger;
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
                var returnValue = actionResult.getReturnValue();
                if(returnValue != null){
                    
                    component.set("v.tableController", JSON.parse(actionResult.getReturnValue()));
                    //component.set("v.BIENUser", false);
                }
            }
        });
        $A.enqueueAction(getRecordsAction);
        
        helper.getHeaders(component); 
        
        // Initialize input select options
        var opts = [
            { "class": "optionClass", label: "5", value: "5"},
            { "class": "optionClass", label: "10", value: "10", selected: "true" },
            { "class": "optionClass", label: "25", value: "25" },
            { "class": "optionClass", label: "50", value: "50" },
            { "class": "optionClass", label: "100", value: "100" },
            { "class": "optionClass", label: "200", value: "200" }];
        component.find("InputSelectDynamic").set("v.options", opts);
        //INI - 06/04/2017 - MUR Disable button
        helper.enableNext(component);
        helper.enableBack(component);
        //FIN - 06/04/2017 - MUR Disable button
    },
    
    defineViews: function(component, event, helper) {
        helper.defineViews(component);
    },
    
    hideModal: function(component, event, helper) {
        //    	var toggleTlf = component.find("tlf");
        //        $A.util.addClass(toggleTlf, "slsd-hide");
        //		var toggleBackdrop = component.find("backdrop");
        //        $A.util.addClass(toggleBackdrop, "slsd-hide");
        document.getElementById('backgroundmodal').style='display:none';                
        document.getElementById('tlf').style.display="none";
        document.getElementById('backdrop').style.display="none";
    },
    showModal : function(component, event, helper) {
        document.getElementById("cuerpo").style.display="block";
        document.getElementById("sendbutton").style.display="";
        document.getElementById("estado").style.display="none";
        document.getElementById("estadofail").style.display="none";
        component.find('newIdeaTitle').getElement().style.border="1px solid #d8dde6"; 
        component.find('newIdeaTitle').getElement().style.color="#333"; 
        document.getElementById('backgroundmodal').style='';                
        document.getElementById('tlf').style.display="block";
        document.getElementById('backdrop').style.display="block";
        
    },
    saveCase: function(component, event, helper) {
        var whereFrom= '';
        var checkFields=true;
        var reason=component.find('reason').get('v.value');
        if(reason === undefined){
            reason = component.get('v.reasons')[0];
            whereFrom = 'EqTel';
        }
        var subject=component.find('newIdeaTitle').get('v.value');
        var description=component.find('description').get('v.value');
        var purpose = 'test value';
        
        if(subject==''){
            component.find('newIdeaTitle').getElement().style.border="solid 2px red"; 
            component.find('newIdeaTitle').getElement().style.color="red"; 
        }        
        if (subject != ''){
            var action = component.get("c.setCase"); 
            action.setParams({
                "status": "Assigned",
                "reason": reason,
                "subject": subject,
                "description": description,
                "purpose": purpose,
                "whereFrom": whereFrom
            }); 
            action.setCallback(this, function(actionResult) {
                if(actionResult.getState() == "SUCCESS"){
                    var returnValue = actionResult.getReturnValue();
                    document.getElementById("cuerpo").style.display="none";
                    document.getElementById("sendbutton").style.display="none";
                    document.getElementById("estado").style.display="block";
                    document.getElementById("estadofail").style.display="none";
                }else{
                    document.getElementById("cuerpo").style.display="none";
                    document.getElementById("sendbutton").style.display="none";
                    document.getElementById("estado").style.display="none";
                    document.getElementById("estadofail").style.display="block";
                }
            });
            $A.enqueueAction(action);
        }
        
    }, 
    
    openDetailContact : function(component, event, helper){
        var contactos = component.get("v.fieldSet");
        var contact = contactos[event.currentTarget.dataset.index];
        console.log(contact);
        var modal = $A.get("e.c:CWP_EditContactModalEvent");
        modal.setParams({"idContact": contact.Id});    
        modal.fire();
    },
    
    doNextPage : function(component, event, helper){
        debugger;
        var controller = component.get('v.tableController');
        controller.paginationFlag="forward";
        helper.getNewPagination(component,controller);
        
    },
    doPrevPage : function(component, event, helper){
        debugger;
        var controller = component.get('v.tableController');
        controller.paginationFlag="backward";
        helper.getNewPagination(component,controller);
        
    },
    orderBy : function(component, event, helper){
        debugger;
        var controller = component.get('v.tableController');
        
        var orderByField = event.currentTarget.id;
        
        controller.sortedRow= orderByField;
        
        if(controller.sortOrder === null|| controller.sortOrder === ''  || controller.sortOrder  === 'DESC'){
            controller.sortOrder = 'ASC';
            var elementos = document.getElementsByClassName("tlf-table__head__link");
            for(i=0; i<elementos.length; i++){
                console.log(elementos[i].id);
                if(elementos[i].id !=  orderByField){
                    document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortUp');
                    document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortDown');
                }
            }
            document.getElementById(orderByField).parentElement.classList.remove('headerSortDown');
            document.getElementById(orderByField).parentElement.classList.add('headerSortUp');
            
        }else if(controller.sortOrder === 'ASC'){
            controller.sortOrder = 'DESC';
            var elementos = document.getElementsByClassName("tlf-table__head__link");
            for(i=0; i<elementos.length; i++){
                if(elementos[i].id !=  orderByField){
                    document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortUp');
                    document.getElementById(elementos[i].id).parentElement.classList.remove('headerSortDown');
                }
            }
            document.getElementById(orderByField).parentElement.classList.remove('headerSortUp');
            document.getElementById(orderByField).parentElement.classList.add('headerSortDown');
        }
        controller.paginationFlag='';
        controller.iniIndex=1;
        controller.finIndex=controller.tableSize;
        
        component.set('v.tableController', controller);
        
        helper.getNewPagination(component,controller);
        
    },
    onChange: function(component, event, helper) {
        debugger;
        var controller = component.get('v.tableController');
        var dynamicCmp = component.find("InputSelectDynamic");
        
        controller.paginationFlag=dynamicCmp.get("v.value");
        helper.getNewPagination(component,controller);
        
    },
})