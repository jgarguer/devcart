({
	getHeaders : function(component) {
        
        var action = component.get("c.getHeaders");     
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
            		console.log(actionResult.getReturnValue());
        			component.set("v.fieldSetRecords", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    
    getRecords : function(component) {
        
        var action = component.get("c.getRecords"); 
        action.setParams({
    		"orderField" : '',
    		"orderDirection" : ''
    	});
        //action.setParams({ "ident" : component.get("v.ident")});
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.fieldSet", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    
    indice : function(component) {
        
        var action = component.get("c.getIndice");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.index", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    numRecords : function(component) {
        
        var action = component.get("c.getNumRecords");    
        var dynamicCmp = component.find("InputSelectDynamic");
        component.set("v.numRecords",dynamicCmp.get("v.value"));
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.total", actionResult.getReturnValue());
                    
            	}
            }
        });
        $A.enqueueAction(action);
        
	},
    
    enableNext : function(component) {
        var action = component.get("c.getHasNext");  
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.nextButton", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    
    enableBack : function(component) {
        var action = component.get("c.getHasBack");  
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.backButton", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    
    
    defineViews: function(component) {  
        var action = component.get("c.getVista");    
        var dynamicCmp = component.find("InputSelectDynamic");
        action.setParams({ "page" : dynamicCmp.get("v.value")});
        
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
                if(returnValue != null){
        		component.set("v.fieldSet", JSON.parse(actionResult.getReturnValue()));
            	}
            }
        });
        $A.enqueueAction(action);
	},
    
    pages: function(component) {  
        var action = component.get("c.getPages");    
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.pages", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    onClick: function(component) {  
        var action = component.get("c.getNext");   
        var dynamicCmp = component.find("InputSelectDynamic");
        action.setParams({ "indice" : component.get("v.index"), "numRec" : component.get("v.numRecords"), "page" : dynamicCmp.get("v.value")  });
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.fieldSet", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
        //INI - 06/04/2017 - MUR Disable button
        var action = component.get("c.getHasNext");  
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.nextButton", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
        //FIN - 06/04/2017 - MUR Disable button
	},
    onClickBack: function(component) {  
        var action = component.get("c.getBack");  
        var dynamicCmp = component.find("InputSelectDynamic");
        action.setParams({ "indice" : component.get("v.index"), "numRec" : component.get("v.numRecords"), "page" : dynamicCmp.get("v.value")  });
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.fieldSet", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
        //INI - 06/04/2017 - MUR Disable button
        var action = component.get("c.getHasBack");  
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			component.set("v.backButton", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
        //FIN - 06/04/2017 - MUR Disable button
        
	},
    ini: function(component) {  
        var action = component.get("c.getAttr"); 
        var dynamicCmp = component.find("InputSelectDynamic");
        action.setParams({ "indice" : component.get("v.index"), "numRec" : component.get("v.numRecords"), "page" : dynamicCmp.get("v.value") });
        
        //Callback
        action.setCallback(this, function(actionResult) {
            if(component.isValid() && actionResult.getState() == "SUCCESS"){
            	var returnValue = actionResult.getReturnValue();
            	if(returnValue != null){
        			//component.set("v.fieldSet", actionResult.getReturnValue());
            	}
            }
        });
        $A.enqueueAction(action);
	},
    getsearchFields : function(component){
        var action = component.get("c.defineSearch");
        var list=component.get("v.reasons");
         action.setParams({ "pickValues" : list});
        //Callback    	        
        action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {                
                var returnValue = response.getReturnValue();
                if(returnValue != null){                                                            
                    component.set("v.searchFields", returnValue);                    
                }
            }
        });
        $A.enqueueAction(action);                
    }, 
    getNewPagination : function(cmp,c) {
        debugger;
        var action = cmp.get("c.getNewTableController");
        action.setParams({"contextString": JSON.stringify(c)});     
        action.setCallback(this, function(response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                debugger;
                var context = JSON.parse(response.getReturnValue());
                cmp.set("v.tableController", context);
                document.getElementById("backgroundmodalwait").style.display='none';
            }
        });
        $A.enqueueAction(action);
    },
})