({
    /*******************************************************************************************/
    /*queda pendiente borrar la variable del evento parameter container al salir del calendario*/
    /*******************************************************************************************/
    doInit : function(component, event, helper) {
    	var url =   window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    	var userId;
    	for (var i=0;i<url.length;i++) {
    		var params = url[i].split("=");
    		userId=params[1];
    	}
    	component.set("v.teamUserId",userId);
	    if(userId!=null){	    	
	    	helper.openCalendar(component, event, helper);
	    }else{
	    helper.doInitHelper(component, event, helper);
	    }
        
    },    
  
    
    updateCalendar : function(component, event, helper) {
        var thisDate = new Date();
        
        var thisMonthB1 = thisDate.getMonth()+1;
        var thisYear = thisDate.getFullYear(); 
        var firstWeekDay = component.get("v.firstWeekDay");
        helper.callToGetDaysList(component, thisMonthB1,thisYear,firstWeekDay);
    },
    
    getNextMonth : function(component, event, helper){
        var thisMonth = component.get("v.thisMonth");
        var thisYear = component.get("v.thisYear");
        var firstWeekDay = component.get("v.firstWeekDay");
        var monthToGet = thisMonth+1;
        if(monthToGet==13){
            monthToGet=1;
            thisYear++;
        }
        helper.callToGetDaysList(component, monthToGet, thisYear, firstWeekDay);
        return false;
    },
    
    getPriorMonth : function(component, event, helper){
        var thisMonth = component.get("v.thisMonth");
        var thisYear = component.get("v.thisYear");
        var firstWeekDay = component.get("v.firstWeekDay");
        var monthToGet = thisMonth-1;
        if(monthToGet==0){
            monthToGet=12;
            thisYear--;
        }
        helper.callToGetDaysList(component, monthToGet, thisYear, firstWeekDay);
        return false;
    },
    
    editEvent : function(component, event, helper){
        document.getElementsByTagName("body")[0].className ='modal-open';
        document.getElementById("backgroundmodalwait").style.display='block';
        document.getElementById("backgroundmodal").style.display='block';
        document.getElementById("add-event").style.display='block';
        document.getElementById("add-event").className ='modal fade tlf-new__modal';
		var id;
        var pastDay;
        var day;
        // id del evento
        id=event.currentTarget.dataset.id;
        //  fecha menor a hoy = true
        pastDay=false;
        // fecha del evento
        day=event.currentTarget.dataset.today;
        var modal = $A.get("e.c:CWP_CustomEventDetailModalEvent");
        modal.setParams({"id": id,"pastDay": pastDay,"day": day});    
        modal.fire();
    },
    newEvent : function(component, event, helper){
        debugger;
        document.getElementsByTagName("body")[0].className ='modal-open';
        document.getElementById("backgroundmodalwait").style.display='block';
        document.getElementById("backgroundmodal").style.display='block';
        document.getElementById("add-event").style.display='block';
        document.getElementById("add-event").className ='modal fade tlf-new__modal in';
        var id;
        var pastDay;
        var day;
        day = event.target.id;
        var utc = new Date().toJSON().slice(0,10);
        var date = new Date(day);
        var today = new Date(utc);
        pastDay=false;
        var appEvent = $A.get("e.c:CWP_CustomEventDetailModalEvent");
        appEvent.setParams({'id':id, 'pastDay':pastDay, 'day':day});
		appEvent.fire();
    }    
})