({
    doInitHelper : function(component, event, helper){
        this.queryUser(component);
    },
    
    queryUser : function(component) {
    	var action = component.get('c.getUser');
    	action.setCallback(this, function(response) {
    		var state = response.getState();
    		if (state === 'SUCCESS') {
    			component.set('v.user', response.getReturnValue());
    			var weekRaw = [$A.get('$Label.c.PCA_Domingo'),$A.get('$Label.c.PCA_Lunes'),$A.get('$Label.c.PCA_Martes'),$A.get('$Label.c.PCA_Miercoles'),$A.get('$Label.c.PCA_Jueves'),$A.get('$Label.c.PCA_Viernes'),$A.get('$Label.c.PCA_Sabado')];
		        var week = [];
		        var user = component.get('v.user');
		        var firstWeekDay;
		        if (user.LanguageLocaleKey === 'en_US') {
		        	firstWeekDay = parseInt(0);
		        } else {
		        	firstWeekDay = parseInt(1);
		        }
		        component.set("v.firstWeekDay", firstWeekDay);
		        for(var i = firstWeekDay; i<weekRaw.length; i++){
		            week.push(weekRaw[i]);
		        }
		        for(var i= 0; i<firstWeekDay; i++){
		            week.push(weekRaw[i]);
		        }
		        component.set("v.weekList", week);        
		        var thisDate = new Date();
        
				var thisMonthB1 = thisDate.getMonth()+1;
				var thisYear = thisDate.getFullYear();       
				
				this.callToGetDaysList(component, thisMonthB1,thisYear,firstWeekDay);
				this.callToGetInfo(component);
    		}
    	});
    	$A.enqueueAction(action);
    },
    
      openCalendar: function(component, event, helper){
    	  var userId= component.get('v.teamUserId');
    	var action = component.get('c.getTeamUser');
        
        action.setParams({
            "teamUserId" : userId
        });
        
        action.setCallback(this, function(response){
        	var state = response.getState();
        	if(state === "SUCCESS"){
        	component.set('v.user', response.getReturnValue());
    			var weekRaw = [$A.get('$Label.c.PCA_Domingo'),$A.get('$Label.c.PCA_Lunes'),$A.get('$Label.c.PCA_Martes'),$A.get('$Label.c.PCA_Miercoles'),$A.get('$Label.c.PCA_Jueves'),$A.get('$Label.c.PCA_Viernes'),$A.get('$Label.c.PCA_Sabado')];
		        var week = [];
		        var user = component.get('v.user');
		        var firstWeekDay;
		        if (user.LanguageLocaleKey === 'en_US') {
		        	firstWeekDay = parseInt(0);
		        } else {
		        	firstWeekDay = parseInt(1);
		        }
		        component.set("v.firstWeekDay", firstWeekDay);
		        for(var i = firstWeekDay; i<weekRaw.length; i++){
		            week.push(weekRaw[i]);
		        }
		        for(var i= 0; i<firstWeekDay; i++){
		            week.push(weekRaw[i]);
		        }
		        component.set("v.weekList", week);        
		        var thisDate = new Date();
        
				var thisMonthB1 = thisDate.getMonth()+1;
				var thisYear = thisDate.getFullYear();       
				
				this.callToGetDaysList(component, thisMonthB1,thisYear,firstWeekDay);
				var header="Calendar of " + user.Name;
				component.set("v.calendarHeader", header);
            }
		});
 		$A.enqueueAction(action);
    	
    },
    
    callToGetInfo : function(component){
        var calendarInfo = component.get('c.getCalendarInfo');

        calendarInfo.setCallback(this, function(response){
        	var state = response.getState();
        	if(state === "SUCCESS"){
                component.set("v.calendarHeader", response.getReturnValue());
            }
		});
 		$A.enqueueAction(calendarInfo);
    },
    
    callToGetDaysList : function(component, month, year, firstWeekDay){
        var monthMap = new Object();
        monthMap[1] = $A.get("$Label.c.TGS_CWP_JAN");
        monthMap[2] = $A.get("$Label.c.TGS_CWP_FEB");
        monthMap[3] = $A.get("$Label.c.TGS_CWP_MAR");
        monthMap[4] = $A.get("$Label.c.TGS_CWP_APR");
        monthMap[5] = $A.get("$Label.c.TGS_CWP_MAY");
        monthMap[6] = $A.get("$Label.c.TGS_CWP_JUN");
        monthMap[7] = $A.get("$Label.c.TGS_CWP_JUL");
        monthMap[8] = $A.get("$Label.c.TGS_CWP_AUG");
        monthMap[9] = $A.get("$Label.c.TGS_CWP_SEP");
        monthMap[10] = $A.get("$Label.c.TGS_CWP_OCT");
        monthMap[11] = $A.get("$Label.c.TGS_CWP_NOV");
        monthMap[12] = $A.get("$Label.c.TGS_CWP_DEC");
        var monthYear = monthMap[month] + "  " + year;
        
        var calendarUser="current";
        var action = component.get('c.getDayList');
        if (component.get("v.teamUserId") != null){
            calendarUser=component.get("v.teamUserId");
        }
        action.setParams({
            "month1" : String(month),
            "year1" : String(year),
            "firstWeekDay" : String(firstWeekDay),
            "calendarUser" : String(calendarUser)
        });
        
        action.setCallback(this, function(response){
        	var state = response.getState();
        	if(state === "SUCCESS"){
                document.getElementById('backgroundmodal').style.display='none';
                document.getElementById('add-event').style.display='none';
                document.getElementById('backgroundmodalwait').style.display='none';
                document.getElementById("add-event").className ='modal fade tlf-new__modal';
                document.getElementsByTagName("body")[0].className ='';
                component.set("v.daysList", response.getReturnValue());
		        component.set("v.monthYear", monthYear);
		        component.set("v.thisYear", year);
		        component.set("v.thisMonth", month);
                document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[0].innerText=
                    document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[0].innerText.substring(0,1);
                document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[1].innerText=
                    document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[1].innerText.substring(0,1);
                document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[2].innerText=
                    document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[2].innerText.substring(0,1);
                document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[3].innerText=
                    document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[3].innerText.substring(0,1);
                document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[4].innerText=
                    document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[4].innerText.substring(0,1);
                document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[5].innerText=
                    document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[5].innerText.substring(0,1);
                document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[6].innerText=
                    document.getElementsByClassName('tlf-calendar__table__head__col__text tlf-calendar__table__head__col__text--mbl')[6].innerText.substring(0,1);
            }
		});
 		$A.enqueueAction(action);
    }
})