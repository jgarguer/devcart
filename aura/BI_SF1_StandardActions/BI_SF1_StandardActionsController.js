({
	cancel : function(cmp, evt, helper) {
		cmp.destroy();
	},
	actionModal : function(cmp, evt, helper){
		console.log("save");
		/** Using lightning data service to update the record */
		cmp.get("v.body")[0].find("edit").get("e.recordSave").fire();
		var refreshEvt = cmp.getEvent("refreshCompactDetail");
		refreshEvt.fire();
		cmp.destroy();
	}
})