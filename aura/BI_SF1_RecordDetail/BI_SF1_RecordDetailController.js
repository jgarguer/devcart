({
	
	doInit : function(cmp, evt, helper) {
		console.log('doInitRecordDetail');

		//Getting custom metadata configuration Layout assigments SF1
		var action = cmp.get("c.getLayoutConfig");
		action.setParams({
			"recordId" : cmp.get("v.recordId")
		});
		action.setCallback(this, function(response){

			cmp.set("v.recordConfig", response.getReturnValue());

			if(response.getReturnValue()){
				/** Here I'm creating the component dynamically once 
				 the custom metadata configuration is retrieved **/
				$A.createComponent(
					"c:BI_SF1_fieldSetForm",
					{
						"fieldSetName": response.getReturnValue().layoutConfig.BI_SF1_Conjunto_de_campos__c,
						"recordId": cmp.get("v.recordId"),
						"record": response.getReturnValue().objInfo.recordInfo,
						"objName" : response.getReturnValue().layoutConfig.BI_SF1_Objeto__c
					},
					function(detailCmp, status, errorMessage){
						
						if (status === "SUCCESS") {
						
							cmp.set("v.detailComp", detailCmp);
						}
						else if (status === "INCOMPLETE") {
							console.log("No response from server or client is offline.")
							// Show offline error
						}
						else if (status === "ERROR") {
							console.log("Error: " + errorMessage);
							// Show error message
						}
					}
				);

				//comments needed to preload quickAction Labels
				//$Label.c.BI_SF1_NuevoCaso
				//$Label.c.BI_SF1_NuevoEvento
				//$Label.c.BI_SF1_NuevaTarea
				//$Label.c.BI_SF1_Modificar
				//$Label.c.BI_SF1_MasDatos
				//$Label.c.BI_SF1_NuevoContacto
				//$Label.c.BI_SF1_Eliminar
				for(var i in response.getReturnValue().layoutConfig.quickAction_SF1__r){
					/*  For every quickAction a Label is needed with the prefix BI_SF1_ I'm overwriting the position Label of the
						Object to set the translated value*/
					response.getReturnValue().layoutConfig.quickAction_SF1__r[i].Label = $A.get("$Label.c.BI_SF1_" + response.getReturnValue().layoutConfig.quickAction_SF1__r[i].Label);
						
					}
				}

			//By default is false and is set to true when the doInit has finished, used to control the loading screen spinner
			cmp.set("v.isLoaded", true);
		});
		$A.enqueueAction(action);

	},

	createRelatedCmp : function (cmp, evt, helper) {
		var clicked = cmp.get("v.clicked");
		//Needed comments to preload related list custom Labels
		//$Label.c.BI_SF1_Oportunidades
		//$Label.c.BI_SF1_Contactos
		//$Label.c.BI_SF1_ActividadesAbiertas
		//$Label.c.BI_SF1_Sedes
		//$Label.c.BI_SF1_Casos

		//attribute clicked controls when Related tab is clicked
		//It only renders the relaredList component once
		if(!clicked){
			var listActions = cmp.get("v.recordConfig").layoutConfig.Related_lists_configuration__r;
			/*	Overwriting the position BI_SF1_Nombre_lista_relacionada__c with the translated value of the label, every Label needs BI_SF1_ prefiz */
			for(var i in cmp.get("v.recordConfig").layoutConfig.Related_lists_configuration__r){
				listActions[i].BI_SF1_Nombre_lista_relacionada__c = $A.get("$Label.c.BI_SF1_" + listActions[i].BI_SF1_Nombre_lista_relacionada__c);
			}
		$A.createComponent(

			"c:BI_SF1_RelatedList",
			{
				"relatedList": listActions,
				"recordId": cmp.get("v.recordId"),
				"objName" : cmp.get("v.recordConfig").layoutConfig.BI_SF1_Objeto__c
			},
			function(relatedComp, status, errorMessage){
				
				if (status === "SUCCESS") {
					cmp.set("v.clicked", true);
					cmp.set("v.relatedComp", relatedComp);
				}
				else if (status === "INCOMPLETE") {
					console.log("No response from server or client is offline.")
					// Show offline error
				}
				else if (status === "ERROR") {
					console.log("Error: " + errorMessage);
					// Show error message
				}
			}
		);}

	},

	actionClicked : function (cmp, evt, helper) {
		/*	This function do the redirection to the right function when an action is clicked */
		console.log("actionclicked " + evt.currentTarget.id);
		var id_action = evt.currentTarget.id.split("-");
		var divMoreActns = cmp.find("moreContent");
		var divFooter = cmp.find("footer");
		var divCerrar = cmp.find("cerrar");

		$A.util.addClass(divMoreActns, "slds-hide");
		$A.util.removeClass(divFooter, "slds-hide");
		$A.util.addClass(divCerrar, "slds-hide");
		/** id_action contains the id of the div clicked and it looks like this 
		  * "0000001-redirectDetail" so I've the id of the record in the position [0]+
		  * and the name of the action I want to call in the second position [1] 
		  */
		switch(id_action[1]) {
			case "redirectDetail":
				helper.redirectDetail(id_action[0]);
				break;
			case "modify":
				helper.modify(cmp, id_action[0]);
				break;
			case "delete":
				helper.delete(cmp, id_action[0], id_action[1]);
				break;
			/** By the default I call the newRecord action so if in the configuration an action is called
			 * newCase will be redirected to newRecord function and takes only the object name + recordId */
			default:
				helper.newRecord(cmp, id_action[0], id_action[1].replace('new', ''), id_action[2]);
				break;
		}
	},

	more : function (cmp, evt, helper) {
		/** This function is used to show the actions if there're more than 4
		 * and hide the footer with the other actions
		*/
		var divMoreActns = cmp.find("moreContent");
		var divFooter = cmp.find("footer");
		var divCerrar = cmp.find("cerrar");

		$A.util.removeClass(divMoreActns, "slds-hide");
		$A.util.addClass(divFooter, "slds-hide");
		$A.util.removeClass(divCerrar, "slds-hide");
		
	},

	cerrar : function (cmp, evt, helper) {
		/** This one do the opposite of more() function it shows the footer and hide the div
		 * containing every action
		*/
		var divMoreActns = cmp.find("moreContent");
		var divFooter = cmp.find("footer");
		var divCerrar = cmp.find("cerrar");

		$A.util.addClass(divMoreActns, "slds-hide");
		$A.util.removeClass(divFooter, "slds-hide");
		$A.util.addClass(divCerrar, "slds-hide");
		
	},

	refresh : function (cmp, evt, helper) {
		/**
		 * HARD REFRESH needed to update recordViewForm salesforce standard component
		 * that's not working properly, in a future should be replaced with the force:refreshView
		 */
		console.log('refresco');
        location.reload(true);
		//$A.get('e.force:refreshView').fire();
	},

	refreshDetail : function (cmp, evt, helper) {
		console.log('refresh cmp');
		/**
		 * HARD REFRESH needed to update recordViewForm salesforce standard component
		 * that's not working properly, in a future should be replaced with the force:refreshView
		 * TIMEOUT is needed because it refresh before Apex finish
		 */
		window.setTimeout(function(){location.reload(true)}, 500);
		
	}


})