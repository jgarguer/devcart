({
	redirectDetail : function (recordId) {
		console.log("redirect to detail page");
		/**Redirects to the standard recordDetail page */
		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
		  "recordId": recordId,
		  "slideDevName": "detail"
		});
		navEvt.fire();
	},

	modify : function (cmp,recordId) {
		/**
		 * BI_SF1_StandardActions is used as a template, emulating standard modify screen that popup in the same screen
		 * BI_SF1_fieldSetForm prints the form of the records , new or view
		 * So I create inside BI_SF1_StandardActions the component BI_SF1_fieldSetForm
		 * to emulate the standard modify on mobile
		 */
		$A.createComponents([
			["c:BI_SF1_StandardActions",{
				"title" : $A.get("$Label.c.BI_CoE_Modificar") + " " +  cmp.get('v.recordConfig').layoutConfig.BI_SF1_Objeto__c
			}],
			["c:BI_SF1_fieldSetForm",{
				"fieldSetName": cmp.get('v.recordConfig').layoutConfig.BI_SF1_Conjunto_de_campos__c,
				"recordId": recordId,
				"record": cmp.get('v.recordConfig').objInfo.recordInfo,
				"objName" : cmp.get('v.recordConfig').layoutConfig.BI_SF1_Objeto__c,
				"view" : 'EDIT'
			}]
			],
			function(components, status, errorMessage){
				if (status === "SUCCESS") {
					var modal = components[0];
					var fieldForm = components[1];

					modal.set("v.body", fieldForm);
					cmp.set("v.body", modal);
				}
				else if (status === "INCOMPLETE") {
					console.log("No response from server or client is offline.")
					// Show offline error
				}
				else if (status === "ERROR") {
					console.log("Error: " + errorMessage);
					// Show error message
				}
			}
		);
	},
	delete : function (cmp, recordId, actions) {
		/** Delete functions create BI_SF1_RecyclableModal that emulate the standard modals with a confirmation error
		 * message */
		$A.createComponent(
			"c:BI_SF1_RecyclableModal",
			{
				"recordId": cmp.get("v.recordId"),
				"title": "Eliminar "+cmp.get("v.recordConfig").layoutConfig.BI_SF1_Objeto__c,
				"message": "¿Está seguro de que desea eliminar este/a "+cmp.get("v.recordConfig").layoutConfig.BI_SF1_Objeto__c+"?",
				"actions": actions,
				"objName": cmp.get("v.recordConfig").layoutConfig.BI_SF1_Objeto__c
			},
			function(modalCmp, status, errorMessage){
				
				if (status === "SUCCESS") {
				
					cmp.set("v.body", modalCmp);
				}
				else if (status === "INCOMPLETE") {
					console.log("No response from server or client is offline.")
					// Show offline error
				}
				else if (status === "ERROR") {
					console.log("Error: " + errorMessage);
					// Show error message
				}
			}
		);
	},
	newRecord : function (cmp, recordId, objName, contador) {
		/** Calls the component fieldSetForm with the NEW attribute
		 * Params of the object in the clickable div
		*/
		$A.createComponent(
			"c:BI_SF1_fieldSetForm",
			{
				"recordId": recordId,
				"objName" : objName,
				"record": cmp.get('v.recordConfig').objInfo.recordInfo,
				"defaultFields": cmp.get("v.recordConfig").layoutConfig.quickAction_SF1__r[contador].BI_SF1_Campos_por_defecto__c,
				"view": "NEW"
			},
			function(modalCmp, status, errorMessage){
				
				if (status === "SUCCESS") {
				
					cmp.set("v.body", modalCmp);
				}
				else if (status === "INCOMPLETE") {
					console.log("No response from server or client is offline.")
					// Show offline error
				}
				else if (status === "ERROR") {
					console.log("Error: " + errorMessage);
					// Show error message
				}
			}
		);
	},
	ventaExpress : function (cmp, recordId) {
		/** VentaExpress function call the standardAction componen
		 * to show the FastSales_cmp in the same component
		 * THIS FUNCTIONALITY IS NOT YET IMPLEMENTED FastSales_cmp MODIFICATIONS ARE NEEDED
		*/
		$A.createComponents([
			["c:BI_SF1_StandardActions",{
			
			}],
			["c:VE_FastSales_cmp",{
				"accountId": recordId,
				"isComponent" : true
			}]
			],
			function(components, status, errorMessage){
				if (status === "SUCCESS") {
					var modal = components[0];
					var ve = components[1];
					modal.set("v.body", ve);
					cmp.set("v.body", modal);
				}
				else if (status === "INCOMPLETE") {
					console.log("No response from server or client is offline.")
					// Show offline error
				}
				else if (status === "ERROR") {
					console.log("Error: " + errorMessage);
					// Show error message
				}
			}
		);
	}
})