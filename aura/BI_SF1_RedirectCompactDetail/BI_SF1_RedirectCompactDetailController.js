({
	doInit : function(cmp, evt, helper) {
    var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
        componentDef : "c:BI_SF1_RecordDetail",
        componentAttributes: {
            "recordId" : cmp.get("v.recordId"),
            "isredirect" : false
        }
    });
    evt.fire();
	}
})