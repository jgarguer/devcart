({
	getOptions : function(cmp) {
		var action = cmp.get("c.getOptions");
   		action.setCallback(this, function(response) {
	   		if (cmp.isValid() && response.getState() === "SUCCESS") {
	   			var opts = JSON.parse(response.getReturnValue());

				cmp.find("InputSelectDynamic").set("v.options", opts);

				// TIPOS DE DATOS
				// var slo = []
				// for(i=0; i<opts.length; i++){
				// 	var x = opts[i].ptype;
				// 	var enco = false;

				// 	for(j=0; j<slo.length; j++){
				// 		if (slo[j]==x){
				// 			enco = true;
				// 			break;
				// 		}
				// 	}

				// 	if (enco==false){
				// 		slo.push(x);
				// 	}
				// }
				// console.log(slo);
            }else {
                //alert('Error');
				var errorVar = $A.get("e.c:BI_G4C_Generic_error");
				errorVar.setParams('header', 'Error');
				errorVar.setParams('body', 'Error');
				errorVar.fire();
            }
   		});
   		$A.enqueueAction(action);
	},
	clearFilterData: function(cmp){

		cmp.set('v.filterValue','');
		cmp.find('operator-selector').set('v.value','Equals');
		$A.util.addClass(cmp.find('filter-div'),'slds-hide');
	}
})