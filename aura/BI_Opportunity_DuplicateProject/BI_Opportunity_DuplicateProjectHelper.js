({
    clearFields:function(component,event){
        //clear custom lookup
         var pillTarget = component.find("lookup-pill");
         var lookUpTarget = component.find("lookupField"); 
        
         $A.util.addClass(pillTarget, 'slds-hide');
         $A.util.removeClass(pillTarget, 'slds-show');
        
         $A.util.addClass(lookUpTarget, 'slds-show');
         $A.util.removeClass(lookUpTarget, 'slds-hide');
      
         component.set("v.SearchKeyWord",null);
         component.set("v.listOfSearchRecords", null );
         component.set("v.selectedRecord", {} );
        //clear template lookup
         var pillTarget = component.find("lookup-pillByTemplate");
         var lookUpTarget = component.find("lookupFieldByTemplate"); 
        
         $A.util.addClass(pillTarget, 'slds-hide');
         $A.util.removeClass(pillTarget, 'slds-show');
        
         $A.util.addClass(lookUpTarget, 'slds-show');
         $A.util.removeClass(lookUpTarget, 'slds-hide');
      
         component.set("v.SearchKeyWordByTemplate",null);
         component.set("v.listOfSearchRecordsByTemplate", null );
         component.set("v.selectedRecordByTemplate", {} );
        
        //clear copy name
        component.set("v.newname","");
    },
    insertcopy:function(component,event,oldname,newname){
       
    },
    
    getuser:function(component,event,uitheme){
        var action = component.get("c.getuser");
        action.setCallback(this, function(response) {
            var state = response.getState();
                var storeResponse = response.getReturnValue();
                // set searchResult list with return value from server.
                component.set("v.uitheme", storeResponse);
        });
         // enqueue the Action  
        $A.enqueueAction(action); 
    },
	searchHelper : function(component,event,getInputkeyWord) {
	  // call the apex class method 
     var action = component.get("c.fetchLookUpValues");
      // set param to method  
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName' : component.get("v.objectAPIName")
          });
      // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
              // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                // set searchResult list with return value from server.
                component.set("v.listOfSearchRecords", storeResponse);
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);
    
	},
    searchHelperByTemplate : function(component,event,getInputkeyWord) {
        console.log('searchHelperByTemplate--keyword' + getInputkeyWord);
	  // call the apex class method 
     var action = component.get("c.fetchLookUpValuesByTemplate");
      // set param to method  
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName' : component.get("v.objectAPIName")
          });
      // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
              // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                // set searchResult list with return value from server.
                component.set("v.listOfSearchRecordsByTemplate", storeResponse);
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);
    
	},
})