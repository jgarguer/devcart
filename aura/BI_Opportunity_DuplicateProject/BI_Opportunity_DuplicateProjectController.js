({  redirect:function(component,event,helper){
        var uitheme = component.get("v.uitheme");
        helper.getuser(component,event,uitheme);
        console.log(uitheme);
        if(uitheme == 'Theme4d' ){
            window.location="{!$Label.BI_Opportunity_URL_Redirect_Lightning}";
        }else {
            window.history.back();
  		 }
    },
    savecopy:function(component,event,helper){
        component.set("v.Spinner", true ); 
       	var idopp=component.get('v.idopp');
        console.log('ID DE LA OPO: ' + idopp);
        
        var newname=component.get("v.newname");
        var oldname=component.get("v.selectedRecord");
        var oldnameByTemplate=component.get("v.selectedRecordByTemplate");
         console.log('SAVE COPY--'+newname,oldname,oldnameByTemplate);
        //check what lookup is filter
        //
  		//typeof something === "undefined"

  		console.log('before IF'+'By Template '+oldnameByTemplate.Name+' Custom '+oldname.Name);
        if(!$A.util.isEmpty(oldnameByTemplate) &&  !$A.util.isEmpty(oldname)){
            component.set("v.Spinner", false ); 
            console.log('By Template '+oldnameByTemplate.Name+' Custom '+oldname.Name);
            alert('Por favor selecciona solo un proyecto para duplicar y deja el otro campo vacio.Plantilla o personalizado.');  
        }else if($A.util.isEmpty(oldnameByTemplate) && !$A.util.isEmpty(oldname)){
            console.log('CUSTOM PROJECT'+' Custom '+oldname.Name);
            //we need to call the component and insert the new value
           //call apex class method
          var action = component.get('c.getProjectbyNameAndEdit');
            action.setParams({
                'Name': oldname.Name,
                "newName":newname,
                "idOpp":idopp
            })
          action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                 component.set("v.Spinner", false ); 
             alert('Copia personalizada creada.');
            }else{
              alert('Copia personalizada no creada'); 
                 component.set("v.Spinner", false ); 
            }
      });
      $A.enqueueAction(action); 
        }else if(!$A.util.isEmpty(oldnameByTemplate) && $A.util.isEmpty(oldname)){
             //BY TEMPLATE
             console.log('template PROJECT'+' TEMPLATE '+oldnameByTemplate.Name);
          var action = component.get('c.getProjectbyNameAndEdit');
            action.setParams({
                'Name': oldnameByTemplate.Name,
                "newName":newname,
                 "idOpp":idopp
            })
          action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
               component.set("v.Spinner", false ); 
             alert('Plantilla copiada correctamente');
                
            }else{
              alert('Plantilla no copiada correctamente');  
                component.set("v.Spinner", false ); 
            }
      });
      $A.enqueueAction(action);         
        } 
        //clearing fields
        helper.clearFields(component,event);
    },
   onfocus : function(component,event,helper){
        var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 5 Records order by createdDate DESC  
         var getInputkeyWord = '';
         helper.searchHelper(component,event,getInputkeyWord);
    },   
    onfocusByTemplate : function(component,event,helper){
        var forOpen = component.find("searchResByTemplate");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 5 Records order by createdDate DESC  
         var getInputkeyWord = '';
         helper.searchHelperByTemplate(component,event,getInputkeyWord);
    },
    keyPressController : function(component, event, helper) {
       // get the search Input keyword   
		var getInputkeyWord = component.get("v.SearchKeyWord");
       // check if getInputKeyWord size id more then 0 then open the lookup result List and 
       // call the helper 
       // else close the lookup result List part.   
        if( getInputkeyWord.length > 0 ){
             var forOpen = component.find("searchRes");
               $A.util.addClass(forOpen, 'slds-is-open');
               $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
             component.set("v.listOfSearchRecords", null ); 
             var forclose = component.find("searchRes");
               $A.util.addClass(forclose, 'slds-is-close');
               $A.util.removeClass(forclose, 'slds-is-open');
          }
         
	},   
    keyPressControllerByTemplate : function(component, event, helper) {
        
       // get the search Input keyword   
		var getInputkeyWord = component.get("v.SearchKeyWordByTemplate");
       // check if getInputKeyWord size id more then 0 then open the lookup result List and 
       // call the helper 
       // else close the lookup result List part.
         
        if( getInputkeyWord.length > 0 ){
             var forOpen = component.find("searchResByTemplate");
               $A.util.addClass(forOpen, 'slds-is-open');
               $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelperByTemplate(component,event,getInputkeyWord);
            console.log('keyPressControllerByTemplate--INPUT KEY:'+getInputkeyWord);
        }
        else{  
             component.set("v.listOfSearchRecordsByTemplate", null ); 
             var forclose = component.find("searchResByTemplate");
               $A.util.addClass(forclose, 'slds-is-close');
               $A.util.removeClass(forclose, 'slds-is-open');
          }
         
	},
    
  // function for clear the Record Selaction 
    clear :function(component,event,heplper){
      
         var pillTarget = component.find("lookup-pill");
         var lookUpTarget = component.find("lookupField"); 
        
         $A.util.addClass(pillTarget, 'slds-hide');
         $A.util.removeClass(pillTarget, 'slds-show');
        
         $A.util.addClass(lookUpTarget, 'slds-show');
         $A.util.removeClass(lookUpTarget, 'slds-hide');
      
         component.set("v.SearchKeyWord",null);
         component.set("v.listOfSearchRecords", null );
         component.set("v.selectedRecord", {} );
         
    }, 
    clearByTemplate :function(component,event,heplper){
      
         var pillTarget = component.find("lookup-pillByTemplate");
         var lookUpTarget = component.find("lookupFieldByTemplate"); 
        
         $A.util.addClass(pillTarget, 'slds-hide');
         $A.util.removeClass(pillTarget, 'slds-show');
        
         $A.util.addClass(lookUpTarget, 'slds-show');
         $A.util.removeClass(lookUpTarget, 'slds-hide');
      
         component.set("v.SearchKeyWordByTemplate",null);
         component.set("v.listOfSearchRecordsByTemplate", null );
         component.set("v.selectedRecordByTemplate", {} );
         
    },
    
  // This function call when the end User Select any record from the result list.   
    handleComponentEvent : function(component, event, helper) {
    // get the selected Account record from the COMPONETN event 	 
       var selectedAccountGetFromEvent = event.getParam("recordByEvent");
	    console.log('HANDLE COMPONENT--RECORD BY EVENT--'+selectedAccountGetFromEvent.Name);
	   component.set("v.selectedRecord" , selectedAccountGetFromEvent); 
       
        var forclose = component.find("lookup-pill");
           $A.util.addClass(forclose, 'slds-show');
           $A.util.removeClass(forclose, 'slds-hide');
      
        
        var forclose = component.find("searchRes");
           $A.util.addClass(forclose, 'slds-is-close');
           $A.util.removeClass(forclose, 'slds-is-open');
        
        var lookUpTarget = component.find("lookupField");
            $A.util.addClass(lookUpTarget, 'slds-hide');
            $A.util.removeClass(lookUpTarget, 'slds-show');  
      
	},
      // This function call when the end User Select any record from the result list BY TEMPLATE.   
    handleComponentEventByTemplate : function(component, event, helper) {
       
    // get the selected Account record from the COMPONETN event 	 
       var selectedAccountGetFromEventByTemplate = event.getParam("recordByEventByTemplate");
	   console.log('selected record by template '+selectedAccountGetFromEventByTemplate.Name);
	   component.set("v.selectedRecordByTemplate" , selectedAccountGetFromEventByTemplate); 
       
        var forclose = component.find("lookup-pillByTemplate");
           $A.util.addClass(forclose, 'slds-show');
           $A.util.removeClass(forclose, 'slds-hide');
      
        
        var forclose = component.find("searchResByTemplate");
           $A.util.addClass(forclose, 'slds-is-close');
           $A.util.removeClass(forclose, 'slds-is-open');
        
        var lookUpTarget = component.find("lookupFieldByTemplate");
            $A.util.addClass(lookUpTarget, 'slds-hide');
            $A.util.removeClass(lookUpTarget, 'slds-show');  
      
	},
  // automatically call when the component is done waiting for a response to a server request.  
    hideSpinner : function (component, event, helper) {
          
    },
 // automatically call when the component is waiting for a response to a server request.
    showSpinner : function (component, event, helper) {
      
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
         
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       
    },
    
    hidediv : function(component,event,helper){
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, "slds-is-close");
        $A.util.removeClass(forclose, "slds-is-open");
            
   },
        hidedivByTemplate : function(component,event,helper){
        var forclose = component.find("searchResByTemplate");
        $A.util.addClass(forclose, "slds-is-close");
        $A.util.removeClass(forclose, "slds-is-open");
         
            
   }
    
    
})