({
	rerender: function (component, helper) {
        var auxData = component.get('v.auxData');
        var evento = component.get('v.evento');
        
        //Save Data
        evento.BI_FVI_Resultado__c = component.find("comentariosEvento").get("v.value");
        evento.BI_G4C_ResultadoVisita__c = component.find("resultadoEvento").get("v.value");
        component.set('v.evento', evento);
        
        //Print Data
        component.find("comentariosEvento").set("v.value", evento.BI_FVI_Resultado__c);
        component.find("resultadoEvento").set("v.value", evento.BI_G4C_ResultadoVisita__c);
    }
})