({
    doInit : function(component, event,helper) {
        
        helper.getMapVisibility(component, event);  
//        helper.setButtonVisiblility(component, event); 
//        helper.setMainImage(component, event); 
        
    },    
    
    setButtonVisiblility : function(component, event) {

            var buttonVisibility = component.get('c.getButtonVisibility');
            buttonVisibility.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var booleanMap = response.getReturnValue();
                    component.set("v.showReports", booleanMap['showReports']);
                    component.set("v.showMonitoreo", booleanMap['showMonitoreo']);
                    component.set("v.showPedido", booleanMap['showPedido']);
                    component.set("v.showPortales", booleanMap['showPortales']);
                }
                /*if(state==="ERROR"){
                    var errors = response.getError();
                    if(errors){
                        if(errors[0] && errors[0].message) {
                            alert("Error message: " + errors[0].message);
                        }
                    }else{
                        console.log("Unknown error");
                    }
                }*/
            });
            $A.enqueueAction(buttonVisibility);
           
        
    },
    
    setMainImage : function(component, event){
            var imgUrl;
            var mainInfo = component.get('c.getMainImage');
            component.set("v.informationMenu", '{!$label.c.PCA_Tab_Service_Track_menu}');
            component.set("v.title", '{!$label.c.PCA_ServiceTrackingText}');
        	var urlDomain = component.get("v.urlimg");
        	//var urlDomain = "";
            mainInfo.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS"){
                    var mainInfo = response.getReturnValue();
                    imgUrl= "url('"+urlDomain+"/servlet/servlet.FileDownload?file=" + mainInfo['imgId']+ "')";
                    setTimeout(
                        function(){
                            if(mainInfo['imgId'] != undefined){
                                document.getElementById('mainImage').style.backgroundImage =  imgUrl;
                                document.getElementById('mainImage').style.padding="2.5rem 4.0625rem";
                                document.getElementById('mainImage').style.minHeight="21.875rem";                               
                            }else{
                                document.getElementById('mainImage').className = "tlf-slide";
                                
                            }
                            if(mainInfo['informationMenu'] != undefined){
                                component.set("v.informationMenu", mainInfo['informationMenu']);
                            }
                            if(mainInfo['title'] != undefined){
                                component.set("v.title", mainInfo['title']);
                            }
                        }, 1);
                }
                /*if(state==="ERROR"){
                    var errors = response.getError();
                    if(errors){
                        if(errors[0] && errors[0].message) {
                            alert("Error message: " + errors[0].message);
                        }
                    }else{
                        console.log("Unknown error");
                    }
                }*/
            });
            $A.enqueueAction(mainInfo);
        
    },
    
    changeTab : function(component, event, helper) {
    	debugger;
        event.preventDefault();
        var a=event.currentTarget.id;
        var mapURLs={};
        
		mapURLs['pca_report'] = 'cwp-reports';
        mapURLs['pca_monitoreo'] = 'cwp-monitoring';
        mapURLs['pca_portales'] = 'cwp-portales';
        if(a == 'pca_reclamospedidos'){
        	var action = component.get('c.getUser');
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    //component.set("v.isTGS", response.getReturnValue());
                    //var esTGS = component.get('v.isTGS');
                    if (response.getReturnValue()) {
                        mapURLs['pca_reclamospedidos'] = 'cwp-tickets-orders';

                    } else {
                        mapURLs['pca_reclamospedidos'] = 'cwp-reclamo-pedidos';
                    }
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "/" + mapURLs[a]
                    });
                    urlEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }else{
        	var urlEvent = $A.get("e.force:navigateToURL");
	        urlEvent.setParams({
	            "url": "/"+ mapURLs[a]
	        });
	        urlEvent.fire();
        }
    }
})