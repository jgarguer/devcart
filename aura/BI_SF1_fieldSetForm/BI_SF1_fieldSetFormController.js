({
	doInit : function(cmp, evt, helper) {

		console.log('doInitFieldSet ' + cmp.get('v.view'));
		//Depending on the view param, load the view mode
		//or the new mode
		if(cmp.get('v.view') == 'VIEW'){
			helper.initView(cmp);
		}else if(cmp.get('v.view') == 'NEW'){
			helper.initNew(cmp);
		}else{
			cmp.set("v.loaded", true);
		}
		
	},

	redirect : function(cmp, evt, helper) {

		console.log('clickDiv' + evt.currentTarget.id);
		//Event that redirect the user to the standard record detail page
		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
		  "recordId": evt.currentTarget.id,
		  "slideDevName": "detail"
		});
		navEvt.fire();
	}
	
})