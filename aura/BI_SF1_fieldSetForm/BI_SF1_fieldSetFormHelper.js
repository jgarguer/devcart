({
	initView : function(cmp) {
		console.log('fieldSetForminitView');
		//Calling apex to get the API name of the fields
		//that are going to be displayed on the screen
		var action = cmp.get("c.getMapFields");
		action.setParams({
			"fieldSet" : cmp.get("v.fieldSetName"),
			"recordId" : cmp.get("v.recordId"),
			"record" : cmp.get("v.record")
		});
		action.setCallback(this, function(response){
			console.log(response.getReturnValue());
			cmp.set("v.fieldSet", response.getReturnValue());
			//control slow loading to show everything at once
			cmp.set("v.loaded", true);
			
		});

		$A.enqueueAction(action);
	},

	initNew : function(cmp){
		
		console.log('fieldSetForminitNew');

		//Calling Apex to get the recordType of the object to create
		var action = cmp.get("c.getRecordtypeId");
		action.setParams({
			"objName" : cmp.get("v.objName")
		});
		action.setCallback(this, function(response){
			console.log(response.getReturnValue());
			var mapRt = response.getReturnValue();
			
			//RegularExpresion to get all the values between '#'
			var matches = cmp.get("v.defaultFields").match(/(#)(.)*?\1/g);
			var defaultValues = cmp.get("v.defaultFields");
			
			console.log(cmp.get("v.record"));
			var record = cmp.get("v.record");

			//Iteration to bind default values
			for(var j in matches){
				if(matches[j].slice(1, -1).split('-')[0] == 'recordTypeId'){
					console.log(mapRt[matches[j].slice(1, -1).split('-')[1]]);
					defaultValues = defaultValues.replace(matches[j], mapRt[matches[j].slice(1, -1).split('-')[1]]);
				}else{
					defaultValues = defaultValues.replace(matches[j], record[matches[j].slice(1, -1)]);
				}
				console.log(matches[j]);

			}
			console.log(defaultValues);
			var defaultValues = JSON.parse(defaultValues);

			//calling standard event to create a record with the given default values and the retrieved recordId
			var createRecordEvent = $A.get("e.force:createRecord");
			var keys = Object.keys(defaultValues)
			//Iterating over the default values and setting into the createRecord event the values
			for(var i in keys){
				console.log(keys[i]);
				console.log(defaultValues[keys[i]]);
				createRecordEvent.setParam(keys[i], defaultValues[keys[i]]);

			}

			createRecordEvent.fire();
		});
	
		$A.enqueueAction(action);
	},

})