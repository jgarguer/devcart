({
    next : function(component, event, helper) {
        //Get the current selected tab value
        var currentTab = component.get("v.selTabId");
        
        if(currentTab == 'tab1'){
            component.set("v.selTabId" , 'tab2');   
        }else if(currentTab == 'tab2'){
            component.set("v.selTabId" , 'tab3');     
        }
    },
    
    
    back : function(component, event, helper) {
        //Get the current selected tab value  
        var currentTab = component.get("v.selTabId");
        
        if(currentTab == 'tab2'){
            component.set("v.selTabId" , 'tab1');     
        } else if(currentTab == 'tab3'){
            component.set("v.selTabId" , 'tab2');     
        }
    },
    
    update : function(component, event, helper) {
        var action = component.get("c.doLoadAccountPlan");
        action.setParams({
            "acc_planId" : component.get("v.acc_planId")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.acc_plan", response.getReturnValue());
                console.log(component.get("v.acc_plan"));
                console.log('contacto')
                console.log(response.getReturnValue().E24P_Contacto_con_afinidad_1__r);
                component.set("v.selectedLookUpRecord1",response.getReturnValue().E24P_Contacto_con_afinidad_1__r)
                component.set("v.selectedLookUpRecord2",response.getReturnValue().E24P_Contacto_con_afinidad_2__r)
                component.set("v.selectedLookUpRecord3",response.getReturnValue().E24P_Contacto_con_afinidad_3__r)
                component.set("v.selectedLookUpRecord4",response.getReturnValue().E24P_Contacto_con_afinidad_4__r)
                component.set("v.selectedLookUpRecord5",response.getReturnValue().E24P_Contacto_con_afinidad_5__r)
            }
        });
        $A.enqueueAction(action);
        
        
        var action2 = component.get("c.doGetPicklist");
        action2.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("RESPONSE", response.getReturnValue());
                var finalSucursales = [];
                var finalEmpleados = [];
                var finalPresencia = [];
                var finalCanales =[];
                var finalEjecutiva=[];
                var finalOperacional=[];
                var finalVenta=[];
                var finalProveedores=[];
                var finalVinculo=[];
                var finalFuturo=[];
                var finalPlanes=[];
                
                var sucursales = response.getReturnValue().sucursales;
                for(var key in sucursales){
                    finalSucursales.push({value:sucursales[key], label:key})         
                }
                var empleados = response.getReturnValue().empleados;
                for(var key in empleados){
                    finalEmpleados.push({value:empleados[key], label:key})         
                }
                var presencia = response.getReturnValue().presencia;
                for(var key in presencia){
                    finalPresencia.push({value:presencia[key], label:key})         
                }
                
                var canales = response.getReturnValue().canales;
                for(var key in canales){
                    finalCanales.push({value:canales[key], label:key})         
                }
                
                var operacional = response.getReturnValue().operacional;
                for(var key in operacional){
                    finalOperacional.push({value:operacional[key], label:key})         
                }
                
                var ejecutiva = response.getReturnValue().ejecutiva;
                for(var key in ejecutiva){
                    finalEjecutiva.push({value:ejecutiva[key], label:key})         
                }
                
                var venta = response.getReturnValue().venta;
                for(var key in venta){
                    finalVenta.push({value:venta[key], label:key})         
                }
                
                var proveedores = response.getReturnValue().proveedores;
                for(var key in proveedores){
                    finalProveedores.push({value:proveedores[key], label:key})         
                }
                
                var vinculo = response.getReturnValue().vinculo;
                for(var key in vinculo){
                    finalVinculo.push({value:vinculo[key], label:key})         
                }
                
                var futuro = response.getReturnValue().futuro;
                for(var key in futuro){
                    finalFuturo.push({value:futuro[key], label:key})         
                }
                
                var planes = response.getReturnValue().planes;
                for(var key in planes){
                    finalPlanes.push({value:planes[key], label:key})         
                }
                component.set("v.sucursales", finalSucursales);
                component.set("v.presencia", finalPresencia);
                component.set("v.empleados", finalEmpleados);
                component.set("v.canales", finalCanales);
                component.set("v.operacional", finalOperacional);
                component.set("v.ejecutiva", finalEjecutiva);
                component.set("v.venta", finalVenta);
                component.set("v.proveedores", finalProveedores);
                component.set("v.vinculo", finalVinculo);
                component.set("v.futuro", finalFuturo);
                component.set("v.planes", finalPlanes);
                
                                console.log('Final Empleados debajo:')
                console.log(finalEmpleados);
                
                                                console.log('Final proveedores debajo:')
                console.log(finalProveedores);
                
                var canalesStr = component.get("v.acc_plan.E24P_Canales_utilizados_por_cliente__c");
				if (canalesStr !== null) {
   					var canalesArr = canalesStr.split(";");
				}
                component.set("v.canalesSelected",canalesArr);
                console.log('canalesArr ' + canalesArr);   
            }
            
        });
        $A.enqueueAction(action2);
       
        
    },
    
    handleChange: function (component, event) {
        // get the updated/changed values   
        var selectedOptionValue = event.getParam("value");
        component.set("v.canalesSelected" , selectedOptionValue);
        var acc_plan = component.get("v.acc_plan");
        acc_plan.Canales_utilizados_por_cliente__c = selectedOptionValue;
        component.set("v.acc_plan", acc_plan)
        
        console.log(selectedOptionValue);
        
    },
    
    save: function(component, event, helper) {
        var action = component.get('c.doSave');
        action.setParams({
            "acc_plan":component.get('v.acc_plan')
        });
        action.setCallback(this, function(response) {
            
            if (response.getState() == 'SUCCESS') {
             if(response.getReturnValue() === true){
                helper.showToast({
                    "title": "Account Plan guardado con éxito",
                    "type": "success",
                    "message" : " "
                });
                helper.reloadDataTable();
            } else{ //if update got failed
                helper.showToast({
                    "title": "Error!!",
                    "type": "error",
                    "message": "Error al actualizar"
                });
            }
            } 
            
        });
        $A.enqueueAction(action);
    },
    
     createLDA: function (component,event,helper) {
         component.set("v.modelLDAOpen", true);
         
    },
    
        createContact: function (component,event,helper) {
         component.set("v.modelContactOpen", true);
         
    },
    
    createCompetidor: function (component,event,helper) {
        var createRecordEvent = $A.get('e.force:createRecord');
         console.log('createRecordevent: ' + createRecordEvent)
        if ( createRecordEvent ) {
            createRecordEvent.setParams({
                'entityApiName': 'E24P_DFE__c',
                });
            createRecordEvent.fire();
        } else {
            /* Create Record Event is not supported */
            alert("No se ha podido crear la línea de acción");
        }
    },
    
    handleVerticalSelectionEvent: function(component,event,helper){
        console.log('Entro en el handle')
        var tab = event.getParam("tab");
        
		component.set("v.selTabId", tab);

    },
    
    handleSelectTab: function(component,event,helper){
    	var name = component.get("v.selTabId")
        console.log('name:  ' + name)
        var appEvent = $A.get("e.c:E24P_tabSelection");
        switch(name){
            case 'tab1':
                console.log('case 1')
                appEvent.setParams({
                    "tab" : "tab1" });
                break;
            case 'tab2':
                console.log('case 2')
                appEvent.setParams({
                    "tab" : "tab2" });
                break;
            case 'tab3':
                console.log('case 3')
                appEvent.setParams({
                    "tab" : "tab3" });
        }
        console.log('appTab:' + appEvent.getParam("tab"))
        appEvent.fire();
	},
        closeLDAModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "False"  
        component.set("v.modelLDAOpen", false);
    },
    closeContactModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "False"  
        component.set("v.modelContactOpen", false);
    },
});