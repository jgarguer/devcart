({
	doInit : function(cmp, evt, helper) {
		
		console.log('doInitRelated')
		//Calling Apex to retrieve a list of object that contains everything needed to configure related lists
		var action = cmp.get("c.getRecords");
		action.setParams({
			"jsonRelatedList" : JSON.stringify(cmp.get("v.relatedList")),
			"recordId" : cmp.get("v.recordId"),
			"objName" : cmp.get("v.objName")
		});
		action.setCallback(this, function(response){
			cmp.set('v.formattedRelated', response.getReturnValue());
		
		});
		$A.enqueueAction(action);
	}, 
	redirectRelatedList : function(cmp, evt, helper) {
		console.log('redirectRelatedList');
		//standard event that navigates to the stardad related list of the given object
		var relatedListEvent = $A.get("e.force:navigateToRelatedList");
		relatedListEvent.setParams({
			"relatedListId": evt.currentTarget.id.split('-')[0],
			"parentRecordId": evt.currentTarget.id.split('-')[1]
		});
		relatedListEvent.fire();
	}
	
})