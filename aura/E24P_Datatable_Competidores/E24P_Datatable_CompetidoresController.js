({
    init: function (component, event, helper) {
            component.set('v.isLoading', true);

        component.set('v.columns', [
            {label: 'Competidor', fieldName: 'E24P_Competidor__c', type: 'text'},
            {label: 'Fortaleza', fieldName: 'E24P_Fortaleza__c', type: 'text', editable: 'true'},
            {label: 'Debilidad', fieldName: 'E24P_Debilidad__c', type: 'text', editable: 'true'},
            {label: 'Nuestra Estrategia', fieldName: 'E24P_Nuestra_Estrategia__c', type: 'text', editable: 'true'},
            {label: 'Línea de acción', fieldName: 'E24P_Plan_de_accion__c', type:'text'},
            {label: 'Vincular', type: 'button', typeAttributes:
             {label:'Vincular línea de acción', title:'Haga click para vincular', name:'vincular_lda', iconName:'utility:add', class: 'btn_next'}
            }
        ]);
        component.set('v.columnsLDA',[
            {label:'Línea de acción', fieldName:'Name', type: 'text'},
            {label:'Estado', fieldName:'BI_Estado__c', type: 'text'},
            {label:'Prioridad', fieldName:'E24P_Prioridad__c', type: 'text'},
            {label:'Tipo de actividad', fieldName:'E24P_Tipo_de_actividad__c', type: 'text'},
            {label:'Responsable', fieldName:'E24P_Responsable__c', type: 'text'}
        ]);
        
        var action = component.get('c.doFetchDFE');
        action.setCallback(this, function(response) {
            
            if (response.getState() == 'SUCCESS') {
                console.log("RESPONSE", response.getReturnValue());
                var data = response.getReturnValue();
                for(var i=0; i<data.length; i++){
                    data[i].E24P_Competidor__c=data[i].E24P_Competidor__r.Name;
                    if (data[i].E24P_Plan_de_accion__r!=undefined){
                        data[i].E24P_Plan_de_accion__c=data[i].E24P_Plan_de_accion__r.Name;
                    }
                }
                
                console.log("DATA: " + data);
                component.set('v.data',data);
                            component.set('v.isLoading', false);

            } else if (response.getState() == 'ERROR') {
                console.log("error");
            }
        });
        $A.enqueueAction(action);
        
        
        var actionLDA = component.get('c.doFetchLDA');
        actionLDA.setCallback(this, function(response) {
            
            if (response.getState() == 'SUCCESS') {
                console.log("RESPONSE", response.getReturnValue());
                var dataLDA = response.getReturnValue();
                console.log(dataLDA);
                component.set('v.dataLDA',response.getReturnValue());
            } else if (response.getState() == 'ERROR') {
                console.log("error");
            }
        });
        $A.enqueueAction(actionLDA);
    },
    
    
    handleSave: function (component, event, helper) {
        
        helper.saveDataTable(component, event, helper);
    },    
    
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        component.set("v.selectedDFEId",row.Id);
        console.log(row.Id)
        if (row.E24P_Plan_de_accion__c != undefined){
            var objetoLDA = {'Id': row.E24P_Plan_de_accion__r.Id,'Name': row.E24P_Plan_de_accion__r.Name};
            component.set("v.selectedLDA", objetoLDA);
        }
        
        component.set("v.modelOpen", true);
        
        
    },
    
    createLDA: function (component,event,helper) {
        var createRecordEvent = $A.get('e.force:createRecord');
        console.log('createRecordevent: ' + createRecordEvent)
        if ( createRecordEvent ) {
            createRecordEvent.setParams({
                'entityApiName': 'BI_Plan_de_accion__c',
            });
            createRecordEvent.fire();
        } else {
            /* Create Record Event is not supported */
            alert("No se ha podido crear la línea de acción");
        }
    },
    
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "False"  
        component.set("v.modelOpen", false);
        component.set("v.selectedLDA", "");
    },
    
    saveModel: function(component, event, helper) {
        var selectedLDA = component.get("v.selectedLDA");
        console.log(JSON.stringify(selectedLDA));
        var selectedLDAID = selectedLDA.Id;
        component.set("v.modelOpen", false);
        var selectedDFE = component.get("v.selectedDFEId");
        console.log(selectedDFE)
        var actualizar = {'Id' : selectedDFE, 'E24P_Plan_de_accion__c' : selectedLDAID}
        console.log(actualizar)
        var action = component.get("c.doUpdateLDALookup");
        action.setParams({
            "Lookup":actualizar
        });     
        
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() === true){
                    helper.showToast({
                        "title": "Línea de acción vinculada con éxito",
                        "type": "success",
                        "message" :" "
                    });
                    helper.reloadDataTable();
                } else{ //if update got failed
                    helper.showToast({
                        "title": "Error!!",
                        "type": "error",
                        "message": "Error al guardar"
                    });
                }
            }
        });
        $A.enqueueAction(action);
    },
    
})