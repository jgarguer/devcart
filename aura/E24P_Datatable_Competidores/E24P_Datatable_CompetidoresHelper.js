( {
    saveDataTable : function(component, event, helper) {
        console.log('Entro en el helper');
    var editedRecords =  event.getParam('draftValues');
    var totalRecordEdited = editedRecords.length;
        console.log('Total Record Edited: ' + totalRecordEdited);
    var action = component.get("c.doUpdateDFE");
    action.setParams({
        "editedDFEList":editedRecords
    });       
    action.setCallback(this,function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            if(response.getReturnValue() === true){
                helper.showToast({
                    "title": totalRecordEdited+" Registros actualizados",
                    "type": "success",
                    "message" :" "
                });
                helper.reloadDataTable();
            } else{ //if update got failed
                helper.showToast({
                    "title": "Error!!",
                    "type": "error",
                    "message": "Error al actualizar"
                });
            }
        }
    });
    $A.enqueueAction(action);

},
    
    /*
     * Show toast with provided params
     * */
        showToast : function(params){
            var toastEvent = $A.get("e.force:showToast");
            if(toastEvent){
                toastEvent.setParams(params);
                toastEvent.fire();
            } else{
                alert(params.message);
            }
        },
            reloadDataTable : function(){
                var refreshEvent = $A.get("e.force:refreshView");
                if(refreshEvent){
                    refreshEvent.fire();
                }
                
            },
})