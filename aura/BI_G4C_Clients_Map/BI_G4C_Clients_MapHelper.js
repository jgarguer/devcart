({
    formatCad : function(cad) {
        var nameCuenta=cad;
        nameCuenta=cad.split(" ");
        var nameAux='';
        var i=0;
        var bEncontrado=false;
        while (i < nameCuenta.length && !bEncontrado) { 
            if (nameCuenta[i].length>25){
                bEncontrado=true;
                nameAux+=nameCuenta[i].slice(0,23)+'...';
            }else
                nameAux+=nameCuenta[i];
            nameAux+=' ';
            i++;
        }
        return nameAux;
    }
})