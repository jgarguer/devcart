({
    jsLoaded: function(component, event, helper) {
        
        var latit =40.515492;
        var longit=-3.666391;             
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                latit = position.coords.latitude;
                longit = position.coords.longitude;
                component.set("v.lati", latit);
                component.set("v.longi", longit);
            });
        }
        var map = component.get('v.map');
        var elem = document.getElementById('mapWorkingDiv');
        var parent = elem.parentElement;
        elem.parentElement.removeChild(elem);
        var node = document.createElement("div");
        node.id = "mapWorkingDiv";
        node.setAttribute("class","cBI_G4C_Clients_Map");
        node.setAttribute("data-aura-class","cBI_G4C_Clients_Map");
        parent.appendChild(node);
        
        if(!navigator.userAgent.match('(Mobi)')){
            map = new L.map('mapWorkingDiv', {
                zoomControl: true,
                touchZoom:true,
                scrollWheelZoom: true,
                dragging: true
                
            })
            .setView([latit, longit], 14);
        }else{
            map = new L.map('mapWorkingDiv', {
                zoomControl: false,
                touchZoom:false,
                scrollWheelZoom: false,
                dragging: false
                
            })
            .setView([latit, longit], 14);
        }
        
        
        L.tileLayer(
            'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
            {
                attribution: 'Tiles © Esri'
            }).addTo(map);
        
        var legend =  L.control({position: 'topright'});
        legend.onAdd = function (map) {
            var div =  L.DomUtil.create('div', 'info legend');
            div.innerHTML ='<span class="slds-col"> <img src="/resource/BI_G4C_Leyenda" alt="" width="200" height="33"/></span>';
            return div;
        };
        legend.addTo(map);
        
        // component.find("map").set("v.value", map);
        
        
        
        //component.set('v.markers', markers);
    },
    
    
    accountsLoaded: function(component, event, helper) {
        
        // Add markers
        
        var map = component.get('v.map');
        var accounts = event.getParam('accounts');
        var markers = component.get('v.markers');
        var markersAux = [];
        var accountSinOp=[];
        var aux=0;
        var legend;
        var latit = component.get("v.lati");
        var longit=component.get("v.longi");      
        var elem;
        var parent;
        var node;
        latit =40.515492;
        longit=-3.666391;      
        try{
            if (markers) {
                markers.clearLayers();
            }
        }catch(err) {
            
        }
        if (accounts && accounts.length> 0) {
            elem = document.getElementById('mapWorkingDiv');
            console.log('elem'+elem);
            parent = elem.parentElement;
            elem.parentElement.removeChild(elem);
            node = document.createElement("div");
            node.id = "mapWorkingDiv";
            node.setAttribute("class","cBI_G4C_Clients_Map");
            node.setAttribute("data-aura-class","cBI_G4C_Clients_Map");
            parent.appendChild(node);
            if(!navigator.userAgent.match('(Mobi)')){
                map = new L.map('mapWorkingDiv', {
                    zoomControl: true,
                    touchZoom:true,
                    scrollWheelZoom: true,
                    dragging: true
                })
                .setView([latit, longit], 14);
            }else{
                map = new L.map('mapWorkingDiv', {
                    zoomControl: false,
                    touchZoom:false,
                    scrollWheelZoom: false,
                    dragging: false
                    
                })
                .setView([latit, longit], 14);
            }
            
            
            
            L.tileLayer(
                'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
                {
                    attribution: 'Tiles © Esri'
                }).addTo(map);
            var legend =  L.control({position: 'topright'});
            legend.onAdd = function (map) {
                var div =  L.DomUtil.create('div', 'info legend');
                div.innerHTML ='<span class="slds-col"> <img src="/resource/BI_G4C_Leyenda" alt="" width="200" height="33"/></span>';
                return div;
            };
            legend.addTo(map);
            //    component.set("v.map", map);
            var markers = new L.FeatureGroup();
            component.set('v.markers', markers);
            
            
            
            markers = new L.FeatureGroup();
            component.set('v.markers', markers);
            var intermedio='orange';
            var finOK='green';
            var finKO='red';
            var sinAccion='blue';
            var stageMap={
                'F6 - Prospecting':sinAccion,
                'F5 - Solution Definition':intermedio,
                'F4 - Offer Development':intermedio,
                'F3 - Offer Presented':intermedio,
                'F2 - Negotiation':intermedio,
                'F1 - Closed Won':finOK,
                'F1 - Closed Lost':finKO,
                'F1 - Cancelled | Suspended':finKO,
                'Closed Won':finOK,
                'Closed Lost':finKO,
                'Cancelled | Suspended':finKO,
                'Open':intermedio
                
            };
            var shadow = '/resource/BI_G4C_leaflet_v1/images/marker-shadow.png';
            var blue = '/resource/BI_G4C_leaflet_v1/images/marker-icon.png';
            // var blue = '/resource/BI_G4C_MarkerBlue';
            var red = '/resource/BI_G4C_MarkerRed';
            var green = '/resource/BI_G4C_MarkerGreen';
            var grey = '/resource/BI_G4C_MarkerGrey';
            var orange = '/resource/BI_G4C_MarkerOrange';
            
            var LeafIcon = L.Icon.extend({
                options: {
                    shadowUrl: shadow,
                    iconSize:     [25, 41],
                    // shadowSize: [50, 64],
                    iconAnchor:   [13, 41],
                    // shadowAnchor: [4, 62],
                    popupAnchor:  [0, -40]
                }
            });
            
            var blueIcon = new LeafIcon({iconUrl: blue}),
                redIcon = new LeafIcon({iconUrl: red}),
                greyIcon = new LeafIcon({iconUrl: grey}),
                greenIcon = new LeafIcon({iconUrl: green}),
                orangeIcon = new LeafIcon({iconUrl: orange});
            for (var k = 0; k < accounts.length; k+=1) {
                var account = accounts[k];
                if (account.ShippingLatitude && account.ShippingLongitude) {
                    var latLng = [account.ShippingLatitude, account.ShippingLongitude];
                    var myicon;
                    
                    var colorAux=(account.Opportunities!==undefined)? stageMap[account.Opportunities[0].StageName] :'gris';
                    switch(colorAux) {
                        case 'red':
                            myicon = redIcon;
                            break;
                        case 'green':
                            myicon = greenIcon;
                            break;
                        case 'blue':
                            myicon = blueIcon;
                            break;
                        case 'orange':
                            myicon = orangeIcon;
                            break;
                        default:
                            accountSinOp[aux]=account;
                            aux+=1;
                            myicon= greyIcon;
                    }
                    var calle='';
                    
                    if(account.ShippingStreet!==undefined) {
                        calle+=account.ShippingStreet;
                        calle+=' ';
                    }
                    if(account.ShippingPostalCode!==undefined) {
                        calle+='(';
                        calle+=account.ShippingPostalCode;
                        calle+=')';
                    }
                    
                    
                    
                    
                    var marker = L.marker(latLng, {
                        title: account.Name,
                        payload: account,
                        icon: myicon
                    }).on('click', function(e) {
                        var miacct = e.target.options.payload;
                        
                    }).bindPopup('<strong>'+helper.formatCad(account.Name)+'</strong><br/>'+helper.formatCad(calle)+'<br/>'+account.ShippingCity)
                    .openPopup();
                    
                    markers.addLayer(marker);
                    markersAux.push(marker);
                    if(aux>0){
                        component.set('v.accountsSinOP',accountSinOp);
                    }
                    
                    if (k === 0) {      
                        map.panTo(latLng);
                    }
                }
            }
            map.addLayer(markers);
            
            
            // component.set('v.markers',markers);
            // component.set('v.listaMarkers',markersAux);
            //component.set("v.map", map);
            // component.find("map").set("v.value", map);
        }else{
            elem = document.getElementById('mapWorkingDiv');
            parent = elem.parentElement;
            elem.parentElement.removeChild(elem);
            node = document.createElement("div");
            node.id = "mapWorkingDiv";
            node.setAttribute("class","cBI_G4C_Clients_Map");
            node.setAttribute("data-aura-class","cBI_G4C_Clients_Map");
            parent.appendChild(node);
            if(!navigator.userAgent.match('(Mobi)')){
                map = new L.map('mapWorkingDiv', {
                    zoomControl: true,
                    touchZoom:true,
                    scrollWheelZoom: true,
                    dragging: true
                    
                })
                .setView([latit, longit], 14);
            }else{
                map = new L.map('mapWorkingDiv', {
                    zoomControl: false,
                    touchZoom:false,
                    scrollWheelZoom: false,
                    dragging: false
                    
                })
                .setView([latit, longit], 14);
            }
            
            
            L.tileLayer(
                'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
                {
                    attribution: 'Tiles © Esri'
                }).addTo(map);
            var legend =  L.control({position: 'topright'});
            legend.onAdd = function (map) {
                var div =  L.DomUtil.create('div', 'info legend');
                div.innerHTML ='<span class="slds-col"> <img src="/resource/BI_G4C_Leyenda" alt="" width="200" height="33"/></span>';
                return div;
            };
            legend.addTo(map);
            //  component.set("v.map", map);
            var markers = new L.FeatureGroup();
            component.set('v.markers', markers);
            
            
            
        }
        // map.setView([40.515492, -3.666391], 18);
    },  
    
    doChangeSelected : function(component, event, helper) {
        
        var params = event.getParam('arguments');
        if (params) {
            var account = params.account;
            var estado= params.estado;
            var accounts = params.accounts;
            
            var map = component.get('v.map');
            var markers = component.get('v.markers');
            var accountSinOp=[];
            var aux=0;
            var markersAux = [];
            var elem = document.getElementById('mapWorkingDiv');
            var parent = elem.parentElement;
            elem.parentElement.removeChild(elem);
            var node = document.createElement("div");
            node.id = "mapWorkingDiv";
            node.setAttribute("class","cBI_G4C_Clients_Map");
            node.setAttribute("data-aura-class","cBI_G4C_Clients_Map");
            parent.appendChild(node);
            if(!navigator.userAgent.match('(Mobi)')){
                map = new L.map('mapWorkingDiv', {
                    zoomControl: true,
                    touchZoom:true,
                    scrollWheelZoom: true,
                    dragging: true,
                    
                    
                })
                .setView([account.ShippingLatitude, account.ShippingLongitude], 17);
            }else{
                map = new L.map('mapWorkingDiv', {
                    zoomControl: false,
                    touchZoom:false,
                    scrollWheelZoom: false,
                    dragging: false
                    
                })
                .setView([account.ShippingLatitude, account.ShippingLongitude], 17);
            }
            
            
            
            L.tileLayer(
                'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
                {
                    attribution: 'Tiles © Esri'
                }).addTo(map);
            var legend =  L.control({position: 'topright'});
            legend.onAdd = function (map) {
                var div =  L.DomUtil.create('div', 'info legend');
                div.innerHTML ='<span class="slds-col"> <img src="/resource/BI_G4C_Leyenda" alt="" width="200" height="33"/></span>';
                return div;
            };
            legend.addTo(map);
            //  component.set("v.map", map);
            
            
            
            var intermedio='orange';
            var finOK='green';
            var finKO='red';
            var sinAccion='blue';
            var stageMap={
                'F6 - Prospecting':sinAccion,
                'F5 - Solution Definition':intermedio,
                'F4 - Offer Development':intermedio,
                'F3 - Offer Presented':intermedio,
                'F2 - Negotiation':intermedio,
                'F1 - Closed Won':finOK,
                'F1 - Closed Lost':finKO,
                'F1 - Cancelled | Suspended':finKO,
                'Closed Won':finOK,
                'Closed Lost':finKO,
                'Cancelled | Suspended':finKO,
                'Open':intermedio
                
            };
            var shadow = '/resource/BI_G4C_leaflet_v1/images/marker-shadow.png';
            var blue = '/resource/BI_G4C_leaflet_v1/images/marker-icon.png';
            // var blue = '/resource/BI_G4C_MarkerBlue';
            var red = '/resource/BI_G4C_MarkerRed';
            var green = '/resource/BI_G4C_MarkerGreen';
            var grey = '/resource/BI_G4C_MarkerGrey';
            var orange = '/resource/BI_G4C_MarkerOrange';
            
            var LeafIcon = L.Icon.extend({
                options: {
                    shadowUrl: shadow,
                    iconSize:     [25, 41],
                    // shadowSize: [50, 64],
                    iconAnchor:   [13, 41],
                    // shadowAnchor: [4, 62],
                    popupAnchor:  [0, -40]
                }
            });
            
            var blueIcon = new LeafIcon({iconUrl: blue}),
                redIcon = new LeafIcon({iconUrl: red}),
                greyIcon = new LeafIcon({iconUrl: grey}),
                greenIcon = new LeafIcon({iconUrl: green}),
                orangeIcon = new LeafIcon({iconUrl: orange});
            
            /////
            for (var k = 0; k < accounts.length; k+=1) {
                var accountAux = accounts[k];
                if (accountAux.ShippingLatitude && accountAux.ShippingLongitude) {
                    var latLng = [accountAux.ShippingLatitude, accountAux.ShippingLongitude];
                    var myicon;
                    
                    var colorAux=(accountAux.Opportunities!==undefined)? stageMap[accountAux.Opportunities[0].StageName] :'gris';
                    switch(colorAux) {
                        case 'red':
                            myicon = redIcon;
                            break;
                        case 'green':
                            myicon = greenIcon;
                            break;
                        case 'blue':
                            myicon = blueIcon;
                            break;
                        case 'orange':
                            myicon = orangeIcon;
                            break;
                        default:
                            accountSinOp[aux]=accountAux;
                            aux+=1;
                            myicon= greyIcon;
                    }
                    var calle='';
                    
                    if(accountAux.ShippingStreet!==undefined) {
                        calle+=accountAux.ShippingStreet;
                        calle+=' ';
                    }
                    if(accountAux.ShippingPostalCode!== undefined) {
                        calle+='(';
                        calle+=accountAux.ShippingPostalCode;
                        calle+=')';
                    }
                    var marker = L.marker(latLng, {
                        title: accountAux.Name,
                        payload: accountAux,
                        icon: myicon
                    }).on('click', function(e) {
                        var miacct = e.target.options.payload;
                        
                    }).bindPopup('<strong>'+helper.formatCad(accountAux.Name)+'</strong><br/>'+helper.formatCad(calle)+'<br/>'+accountAux.ShippingCity)
                    .openPopup();
                    
                    markers.addLayer(marker);
                    markersAux.push(marker);
                    if(aux>0){
                        component.set('v.accountsSinOP',accountSinOp);
                    }
                    if (account === accountAux) {      
                        L.marker([account.ShippingLatitude, account.ShippingLongitude],{icon: myicon}).addTo(map)
                        .bindPopup('<strong>'+helper.formatCad(account.Name)+'</strong><br/>'+helper.formatCad(calle)+'<br/>'+account.ShippingCity)
                        .openPopup();
                    }else{
                        L.marker([accountAux.ShippingLatitude, accountAux.ShippingLongitude],{icon: myicon}).addTo(map)
                        .bindPopup('<strong>'+helper.formatCad(accountAux.Name)+'</strong><br/>'+helper.formatCad(calle)+'<br/>'+accountAux.ShippingCity)
                        ;
                        
                    }
                }
            }
            // map.addLayer(markers);
            
        }
    } 
})