({
    init : function(component, event, helper) {
        var attachment = component.set('v.attachment', {'sobjectType':'Attachment'});
    },
    showFileModal : function(component, event, helper) {
        //var divFileModal = component.find('fileModal');
        document.getElementById('fileModal').style.display='';
        document.getElementById('fileModal').classList.add("in");
        //$A.util.removeClass(divFileModal, 'hide');
    },
    closeFileModal : function(component, event, helper) {
        //var divFileModal = component.find('fileModal');
        document.getElementById('fileModal').classList.remove("in");
        document.getElementById('fileModal').style.display='none';
        document.getElementById("attachmentDescription").style.border="";
        document.getElementById("attachmentDescription").style.color="#16325c";
        document.getElementById('attachmentDescription').value='';
        document.getElementById('file-attach2').value='';
        
        //$A.util.addClass(divFileModal, 'hide');
    },
    handleCaptureUploadFileEvent : function(component, event, helper) {
        var attachmentList = component.get('v.attachmentList');
        event.setParam('attachmentList', attachmentList);
    },
    onFocusDesc : function(component, event, helper){
        document.getElementById('attachmentDescription').style.border="";
        document.getElementById('attachmentDescription').style.color="#16325c";  
    },
    throwFileEvent : function(component, event, helper) {
        debugger;
        var MAX_FILE_SIZE = 4500000;
        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];
        var fileDesc = '';
        var attach = 0;
        if (document.getElementById("attachmentDescription") != null){
            fileDesc = document.getElementById("attachmentDescription").value;
            if(fileDesc==''|| fileDesc==undefined){
                document.getElementById("attachmentDescription").style.border="solid 2px red"; 
                document.getElementById("attachmentDescription").style.color="red"; 
                attach = 1;
                return;
            }
        }
        if (attach==0){
            
            if (file.size > MAX_FILE_SIZE) {
                /*if (document.getElementById('tlf-case-ok') != null){
                    document.getElementById('tlf-case-ok').style.display="block";
                    document.getElementById('tlf-case-ok').classList.add('in');
                    if (document.getElementById('messageNewCase')){
                        document.getElementById('messageNewCase').innerHTML('File size cannot exceed ' + this.MAX_FILE_SIZE + 
                                                                            ' bytes.\n' + 'Selected file size: ' + file.size);
                    }                       
                }*/
                alert('File size cannot exceed ' + MAX_FILE_SIZE + 
                      ' bytes.\n' + 'Selected file size: ' + file.size);
                return;
            }

            var attachmentList = component.get('v.attachmentList');
            file.desc=fileDesc;	
            var fr = new FileReader();
            fr.onload = $A.getCallback(function() {
                var fileContents = fr.result;
                var base64Mark = 'base64,';
                if(fileContents == null){
                    fileContents='';
                }
                var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                
                fileContents = fileContents.substring(dataStart);
                file.fileContents=fileContents;
                attachmentList.push(file);
                debugger;
                component.set('v.attachmentList', attachmentList);
                var appEvent = $A.get("e.c:CWP_UploadFileEvent");
                appEvent.setParams({ "attachmentList" : attachmentList });
                appEvent.fire();
                document.getElementById('fileModal').classList.remove("in");
                document.getElementById('fileModal').style.display='none';
                document.getElementById("attachmentDescription").style.border="";
                document.getElementById("attachmentDescription").style.color="#16325c";
                document.getElementById('attachmentDescription').value='';
                document.getElementById('file-attach2').value='';
            });
            fr.readAsDataURL(file);
            
        }
    },  
    updateName : function(component, event, helper){
        debugger;
        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];
        document.getElementById("labelbutton").innerHTML=file.name;
    },
     removeAttachment : function(component, event, helper) {
         debugger;
        var file = event.currentTarget.id;
        var listaAdjuntos = component.get("v.attachmentList");
        
        for(var i = listaAdjuntos.length - 1; i >= 0; i--) {
            if(listaAdjuntos[i].name === file) {
                listaAdjuntos.splice(i, 1);
            }
        }
        component.set("v.attachmentList",listaAdjuntos);
    },  
    
})