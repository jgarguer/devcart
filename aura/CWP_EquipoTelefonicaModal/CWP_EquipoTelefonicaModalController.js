({
    openModal : function(cmp, evt) {
        debugger;
        document.getElementById('tlf-attachment').style.display='block';
        document.getElementsByTagName('body')[0].classList='modal-open';
        document.getElementById('backgroundmodal').style='';
        document.getElementById('tlf-attachment').classList.add('in');
        cmp.set("v.data",'');
        var id = evt.getParam("id");
        var type = evt.getParam("type");
        var back = evt.getParam("back");
        var clase = evt.getParam("clase");
        
        var action = cmp.get("c.getloadInfo");
        
        action.setParams({'id':id,'type':type,'back':back,'clase':clase});
        
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                cmp.set("v.data", response.getReturnValue());
                debugger;
                var element = cmp.find('cwp_modal');
                $A.util.removeClass(element, 'slds-hide');
            }
        });
        $A.enqueueAction(action);
        
        var actionLabel = cmp.get("c.getLabels");
        actionLabel.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                cmp.set("v.labelsObjectType", response.getReturnValue());
            }
        });
        $A.enqueueAction(actionLabel);
        
    },
    hideModal : function(cmp) {
        var element = cmp.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
        document.getElementById('backgroundmodal').style='display:none';
        debugger;
        if(document.getElementById('tlf-email').style.display=='block' && document.getElementById('tlf-attachment').style.display=='block'){
             document.getElementsByTagName('body')[0].classList='modal-open';
        }else{
            document.getElementsByTagName('body')[0].classList='';
        }
        document.getElementById('tlf-attachment').style.display='none';
    },
    correo : function(cmp, evt, helper) {
    debugger;
        //document.getElementById('backgroundmodal').style='';
          var idContact = cmp.get("v.data").UserId;
        document.getElementById('sendEmail---'+idContact).click();
        document.getElementById('tlf-attachment').style.display='none';
        /*
      
        var modal = $A.get("e.c:CWP_SendInfoModalEvent");
        modal.setParams({"idContact": idContact, "isButton": true});
        
        var element = cmp.find('cwp_modal');
        $A.util.addClass(element, 'slds-hide');
        
        modal.fire();
        */
        
    },
    cita : function(cmp, evt, helper) {
        var idContact = cmp.get("v.data").UserId;
        window.location.href="./cwp-calendar#"+idContact;
    },
    perfil : function(cmp, evt, helper) {
        var idContact = cmp.get("v.data").UserId;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/profile/"+idContact 
        });
        urlEvent.fire();
        
    },
    
    // eliminar cuando se implemente
    executeEvent : function() {
        var id;
        var type;
        var back;
        var clase;
        
        //Datos de prueba
        //id=00525000001fHkXAAU&clase=Equipo
        id = '00525000001fHkXAAU';
        clase='Equipo';
        var modal = $A.get("e.c:CWP_EquipoTelefonicaModalEvent");
        modal.setParams({"id": id, "back":back});    
        modal.fire();
    },
    modalAction: function(cmp, evt, helper) {
        var element = cmp.find('cwp_modal');
        debugger;
        // si el valor es true mostramos/ocultamos la modal 
        if(evt.getParam("showModal")){
            $A.util.removeClass(element, 'slds-hide');
        } else {
            $A.util.addClass(element, 'slds-hide');
        }  
    },
    
    
    
})