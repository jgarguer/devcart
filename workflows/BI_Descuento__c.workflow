<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_Notificacion_por_email_Pricing</fullName>
        <description>01. Notificación por email a Pricing</description>
        <protected>false</protected>
        <recipients>
            <recipient>pricing.chile@proyectobien.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Proceso_de_descuento_solicitud</template>
    </alerts>
    <alerts>
        <fullName>BI_Notificacion_por_email_al_Propietario</fullName>
        <description>01. Notificación por email al Propietario</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Proceso_de_descuento_rechazado</template>
    </alerts>
    <alerts>
        <fullName>BI_Notificacion_por_email_al_Propietario_1</fullName>
        <description>01. Notificación por email al Propietario</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Proceso_de_descuento_aprobado</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_02_Cambio_estado_Pendiente</fullName>
        <field>BI_Estado_del_Proceso__c</field>
        <literalValue>Pendiente</literalValue>
        <name>02. Cambio estado &quot;Pendiente&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Cambio_estado_pricing</fullName>
        <description>Se envía el descuento a Pricing para su aprobación, al enviarse cambia su estado a &quot;Pricing&quot;</description>
        <field>BI_Estado_del_Proceso__c</field>
        <literalValue>Pricing</literalValue>
        <name>02. Cambio estado &quot;Pricing&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Cambipo_estado_Aprobado</fullName>
        <field>BI_Estado_del_Proceso__c</field>
        <literalValue>Aprobado</literalValue>
        <name>02. Cambio estado &quot;Aprobado&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Descuento_Cerrado</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BI_Cerrado</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Descuento - Cerrado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Descuento_Fecha_inicio</fullName>
        <field>BI_Fecha_de_inicio__c</field>
        <formula>TODAY()</formula>
        <name>Descuento - Fecha inicio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Descuento_cancelado_Casilla</fullName>
        <field>BI_Estado_del_Proceso__c</field>
        <literalValue>Cancelado</literalValue>
        <name>Descuento cancelado Casilla</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Descuento - Fecha inicio</fullName>
        <actions>
            <name>BI_Descuento_Fecha_inicio</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Descuento Bloqueado</fullName>
        <actions>
            <name>BI_Descuento_Cerrado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Descuento__c.BI_Estado_del_Proceso__c</field>
            <operation>equals</operation>
            <value>Rechazado,Aprobado,Cerrado,Cancelado</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Descuento cancelado</fullName>
        <actions>
            <name>BI_Descuento_cancelado_Casilla</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Descuento__c.BI_Cancelar_descuento__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
