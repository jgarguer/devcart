<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_O4_BidTeam</fullName>
        <description>BI_O4_BidTeam</description>
        <protected>false</protected>
        <recipients>
            <field>BI_O4_Name2__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_BidTeam/BI_O4_BidTeam</template>
    </alerts>
</Workflow>
