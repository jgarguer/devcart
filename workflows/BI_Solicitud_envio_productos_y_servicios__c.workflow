<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>X21_Envio_para_aprobaci_n_Visado_Aprobado</fullName>
        <description>2. Envio para aprobación | Visado - Aprobado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Proceso_de_visado_aprobado</template>
    </alerts>
    <alerts>
        <fullName>X22_Envio_para_aprobaci_n_Visado_Aprobado</fullName>
        <description>2. Envio para aprobación | Visado - Aprobado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Proceso_de_visado_aprobado</template>
    </alerts>
    <alerts>
        <fullName>X2_Envio_para_aprobaci_n_Visado_Aprobado</fullName>
        <description>2. Envio para aprobación | Visado - Aprobado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Proceso_de_visado_aprobado</template>
    </alerts>
    <alerts>
        <fullName>X31_Envio_para_aprobaci_n_Visado_Rechazado</fullName>
        <description>3. Envio para aprobación | Visado - Rechazado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Proceso_de_visado_rechazado</template>
    </alerts>
    <alerts>
        <fullName>X32_Envio_para_aprobaci_n_Visado_Rechazado</fullName>
        <description>3. Envio para aprobación | Visado - Rechazado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Proceso_de_visado_rechazado</template>
    </alerts>
    <alerts>
        <fullName>X3_Envio_para_aprobaci_n_Visado_Rechazado</fullName>
        <description>3. Envio para aprobación | Visado - Rechazado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Proceso_de_visado_rechazado</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_Aprobacion_Recambios</fullName>
        <field>BI_Estado_de_recambios__c</field>
        <literalValue>Enviando</literalValue>
        <name>Aprobación: Recambio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Aprobacion_Servicio</fullName>
        <field>BI_Estado_de_servicios__c</field>
        <literalValue>Aprobado</literalValue>
        <name>Aprobación: Servicio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Aprobacion_Solicitud</fullName>
        <field>BI_Estado_de_solicitud__c</field>
        <literalValue>Abierto</literalValue>
        <name>Aprobación: Solicitud</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Aprobacion_Venta</fullName>
        <field>BI_Estado_de_ventas__c</field>
        <literalValue>Aprobado</literalValue>
        <name>Aprobación: Venta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Cambio_de_estado_Recambio</fullName>
        <field>BI_Estado_de_recambios__c</field>
        <literalValue>Visando</literalValue>
        <name>Cambio de estado: Recambio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Cambio_de_estado_Servicio</fullName>
        <field>BI_Estado_de_servicios__c</field>
        <literalValue>Visando</literalValue>
        <name>Cambio de estado: Servicio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Cambio_de_estado_Ventas</fullName>
        <field>BI_Estado_de_ventas__c</field>
        <literalValue>Visando</literalValue>
        <name>Cambio de estado: Venta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Linea_de_recambio_Pendiente</fullName>
        <field>BI_Estado_de_recambios__c</field>
        <literalValue>Pendiente</literalValue>
        <name>Línea de recambio - Pendiente</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Rechazo_Recambio</fullName>
        <field>BI_Estado_de_recambios__c</field>
        <literalValue>Pendiente</literalValue>
        <name>Rechazo: Recambio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Rechazo_Servicio</fullName>
        <field>BI_Estado_de_servicios__c</field>
        <literalValue>Vacío</literalValue>
        <name>Rechazo: Servicio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Rechazo_Solicitud</fullName>
        <field>BI_Estado_de_solicitud__c</field>
        <literalValue>Abierto</literalValue>
        <name>Rechazo: Solicitud</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Rechazo_Venta</fullName>
        <field>BI_Estado_de_ventas__c</field>
        <literalValue>Vacío</literalValue>
        <name>Rechazo: Venta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Solicitud_no_retornada</fullName>
        <field>BI_Retornada__c</field>
        <literalValue>0</literalValue>
        <name>Aprobación: Solicitud no retornada</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Linea_de_Recambio_Vacia</fullName>
        <description>eHelp 02146940</description>
        <field>BI_Estado_de_recambios__c</field>
        <literalValue>Vacío</literalValue>
        <name>Linea de Recambio - Vacia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_Linea de recambio</fullName>
        <actions>
            <name>BI_Linea_de_recambio_Pendiente</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Solicitud_envio_productos_y_servicios__c.BI_Numero_de_lineas_de_recambio__c</field>
            <operation>greaterOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>Actualiza el campo estado de recambio a &quot;Pendiente&quot; al tener alguna línea de recambio</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Linea de recambio vacia</fullName>
        <actions>
            <name>Linea_de_Recambio_Vacia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Solicitud_envio_productos_y_servicios__c.BI_Numero_de_lineas_de_recambio__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>Actualiza el campo estado de recambia a &quot;Vacio&quot; al no tener ninguna linea de recambio</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
