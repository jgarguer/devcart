<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TGS_Update_Clear</fullName>
        <field>IsActive__c</field>
        <literalValue>1</literalValue>
        <name>Update Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TGS Trigger BH</fullName>
        <actions>
            <name>TGS_Update_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>TGS_Business_Hours__c.IsActive__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>TGS_Business_Hours__c.IsActive__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <description>Workflow para lanzar el update del TGS Business hours</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
