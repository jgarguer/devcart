<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_COL_Actualizar_Estado_Aprobado</fullName>
        <field>BI_COL_Estado_de_la_aprobacion__c</field>
        <literalValue>Aprobado</literalValue>
        <name>BI_COL_Actualizar_Estado_Aprobado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_COL_Actualizar_Estado_Rechazo</fullName>
        <field>BI_COL_Estado_de_la_aprobacion__c</field>
        <literalValue>Rechazado - Inactivo</literalValue>
        <name>BI_COL_Actualizar_Estado_Rechazo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_COL_Bloquea_MIDAS_enviado_aprobacion</fullName>
        <description>Acrualiza el campo bloqueo de MIDAS a TRUE</description>
        <field>BI_COL_Bloqueado__c</field>
        <literalValue>1</literalValue>
        <name>Bloquea MIDAS enviado para aprobación</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_COL_Cambio_estado_MIDAS</fullName>
        <field>BI_COL_Estado_de_la_aprobacion__c</field>
        <literalValue>Pendiente Aprobacion</literalValue>
        <name>Cambio estado MIDAS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_COL_Envia_registro_aprobacion</fullName>
        <description>Actualiza el valor del campo para que pueda identificarse que se ha enviado un MIDAS para aprobación</description>
        <field>BI_COL_Estado_de_la_aprobacion__c</field>
        <literalValue>Pendiente Aprobacion</literalValue>
        <name>Envía registro a aprobación</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
