<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_Avisar_ejecutivo_cobros_actualizacion_factura</fullName>
        <description>BI_Avisar_ejecutivo_cobros_actualización_factura</description>
        <protected>false</protected>
        <recipients>
            <recipient>BI_Ejecutivo_de_cobros</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Comunicar_ejecutivo_cobros_se_ha_crado_una_nueva_factura</template>
    </alerts>
    <rules>
        <fullName>BI_Avisar_por_correo_a_ejecutivo_de_cobros_creacion_facturacion</fullName>
        <actions>
            <name>BI_Avisar_ejecutivo_cobros_actualizacion_factura</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Avisa al ejecutivo de cobros que se ha creado facturas</description>
        <formula>BI_Dias_retraso_de_pago__c &gt;0</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
