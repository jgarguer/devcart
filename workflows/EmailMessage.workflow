<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_COL_Actualizacion_to_Address</fullName>
        <field>BI_COL_To_Address__c</field>
        <formula>ToAddress</formula>
        <name>BI_COL_Actualizacion to Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_COL_Caso_Asignable</fullName>
        <description>Marca el caso como no asignable luego del mail2case</description>
        <field>BI_COL_Asignable__c</field>
        <literalValue>1</literalValue>
        <name>BI COL Caso Asignable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Comentario_Nuevo_True</fullName>
        <field>BI_PER_Comentario_nuevo__c</field>
        <literalValue>1</literalValue>
        <name>Comentario_Nuevo_True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Estado_del_caso_EnCurso</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Estado_del_caso_EnCurso</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>BI_COL_Actualizacion_to_Address_mail2case</fullName>
        <actions>
            <name>BI_COL_Actualizacion_to_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_COL_Caso_Asignable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Actualiza el correo destino para controlar mail to case</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_CasoComercil_II_EnCurso_True</fullName>
        <actions>
            <name>BI_Comentario_Nuevo_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_Estado_del_caso_EnCurso</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>D-000638 Un caso comercial II que esté en el estado “Pendiente” y el cliente envié un correo electrónico (respuesta) el estado cambie a “En curso” y el campo “Comentario cliente” se marque.</description>
        <formula>AND(  TEXT(Parent.Status) = &apos;Pending&apos;,  ISPICKVAL(Status,&apos;3&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
