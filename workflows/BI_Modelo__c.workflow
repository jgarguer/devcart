<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_Actualizar_validador_modelo_servicio</fullName>
        <description>Actualiza el campo BI_Validador_duplicidad__c del objeto BI_Modelo__c</description>
        <field>BI_Validador_duplicidad__c</field>
        <formula>Name &amp; &quot;-&quot; &amp; RecordTypeId</formula>
        <name>BI_Actualizar_validador_modelo_servicio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_Evitar_duplicados</fullName>
        <actions>
            <name>BI_Actualizar_validador_modelo_servicio</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rellena el campo único para evitar duplicados.</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
