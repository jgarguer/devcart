<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_actualiza_Validador_Id_legado</fullName>
        <field>BI_Validador_Id_de_legado__c</field>
        <formula>BI_Id_sistema_legado__c  &amp; &apos; &apos;  &amp;  BI_Codigo_ISO__c &amp;  BI_Sistema_legado__c</formula>
        <name>BI  actualiza Validador Id legado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI  actualiza Validador Id legado</fullName>
        <actions>
            <name>BI_actualiza_Validador_Id_legado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Facturas__c.BI_Id_sistema_legado__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
