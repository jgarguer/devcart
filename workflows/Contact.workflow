<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI2_Actualizar_EsAutorizado_False</fullName>
        <field>BI2_Es_Autorizado__c</field>
        <literalValue>0</literalValue>
        <name>Actualizar EsAutorizado False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI2_Actualizar_Es_Autorizado_True</fullName>
        <field>BI2_Es_Autorizado__c</field>
        <literalValue>1</literalValue>
        <name>Actualizar Es Autorizado True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Act_Check_Representante_legal_false</fullName>
        <field>BI_Representante_legal__c</field>
        <literalValue>0</literalValue>
        <name>Act_Check_Representante_legal_false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Act_Check_Representante_legal_true</fullName>
        <field>BI_Representante_legal__c</field>
        <literalValue>1</literalValue>
        <name>Act_Check_Representante_legal_true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizacion_Codigo_unico</fullName>
        <field>BI_Codigo_unico__c</field>
        <formula>Email  &amp;  Account.Id</formula>
        <name>BI_Actualización_Codigo_unico</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_FVI_Contacto_Cliente_FVI</fullName>
        <description>Work-Around: Field Update que chequea a true el Activo cada vez que se crea un Contacto FVI Cliente</description>
        <field>BI_Activo__c</field>
        <literalValue>1</literalValue>
        <name>BI_FVI_Contacto Cliente FVI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_actualiza_Validador_Id_legado</fullName>
        <field>BI_Validador_Id_de_legado__c</field>
        <formula>BI_Id_del_contacto__c &amp; &apos; &apos;  &amp;  BI_Codigo_ISO__c &amp; &apos; &apos;  &amp; BI_Sistema_legado__c</formula>
        <name>BI  actualiza Validador Id legado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI  actualiza Validador Id legado</fullName>
        <actions>
            <name>BI_actualiza_Validador_Id_legado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.BI_Id_del_contacto__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI2_Actualizar_EsAutorizado_False</fullName>
        <actions>
            <name>BI2_Actualizar_EsAutorizado_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (  !$Setup.BI_bypass__c.BI_migration__c,  OR(  AND(  TEXT(BI_Country__c) != &apos;Peru&apos;,  NOT(INCLUDES(BI_Tipo_de_contacto__c, &quot;General&quot;)),  NOT(INCLUDES(BI_Tipo_de_contacto__c, &quot;Autorizado&quot;))),  AND( TEXT(BI_Country__c) = &apos;Peru&apos;,  ISBLANK(BI_Tipo_de_contacto__c))  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI2_Actualizar_EsAutorizado_True</fullName>
        <actions>
            <name>BI2_Actualizar_Es_Autorizado_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (  !$Setup.BI_bypass__c.BI_migration__c,  OR(  AND(  TEXT(BI_Country__c)= &apos;Peru&apos;,  OR(  INCLUDES(BI_Tipo_de_contacto__c, &quot;General&quot;),  INCLUDES(BI_Tipo_de_contacto__c, &quot;Autorizado&quot;),  INCLUDES(BI_Tipo_de_contacto__c, &quot;Administrativo&quot;),  INCLUDES(BI_Tipo_de_contacto__c, &quot;Cobranza&quot;),  INCLUDES(BI_Tipo_de_contacto__c, &quot;Comercial&quot;),  INCLUDES(BI_Tipo_de_contacto__c, &quot;Encuesta Satisfacción&quot;),  INCLUDES(BI_Tipo_de_contacto__c, &quot;Facturación&quot;),  INCLUDES(BI_Tipo_de_contacto__c, &quot;General&quot;),  INCLUDES(BI_Tipo_de_contacto__c, &quot;PostVenta&quot;),  INCLUDES(BI_Tipo_de_contacto__c, &quot;Técnico&quot;))),  AND(  TEXT(BI_Country__c) != &apos;Peru&apos;,  OR( INCLUDES(BI_Tipo_de_contacto__c, &quot;General&quot;),  INCLUDES(BI_Tipo_de_contacto__c, &quot;Autorizado&quot;)))  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizacion_Codigo_unico</fullName>
        <actions>
            <name>BI_Actualizacion_Codigo_unico</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISCHANGED( AccountId ), ISCHANGED( Email))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizar_Check_Representante_legal_false</fullName>
        <actions>
            <name>BI_Act_Check_Representante_legal_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ( RecordType.DeveloperName &lt;&gt;&apos;TGS_Active_Contact&apos;),  NOT(INCLUDES(BI_Tipo_de_contacto__c, &quot;Representante legal&quot;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizar_Check_Representante_legal_true</fullName>
        <actions>
            <name>BI_Act_Check_Representante_legal_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ( RecordType.DeveloperName&lt;&gt;&apos;TGS_Active_Contact&apos;), INCLUDES(BI_Tipo_de_contacto__c, &quot;Representante legal&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_FVI_Contact_Active_WA</fullName>
        <actions>
            <name>BI_FVI_Contacto_Cliente_FVI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Contacto Cliente FVI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BI_Country__c</field>
            <operation>equals</operation>
            <value>Peru</value>
        </criteriaItems>
        <description>Work-around para que los contactos que se creen desde la tablet de tipo Cliente se marquen como activos al llegar a SF</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
