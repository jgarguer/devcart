<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_COL_Actualiza_Fecha_RFB</fullName>
        <field>BI_COL_Fecha_inicio_de_cobro_RFB__c</field>
        <formula>BI_COL_Fecha_instalacion_servicio_RFS__c</formula>
        <name>BI_COL_Actualiza_Fecha_RFB</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_COL_MS_no_renovable</fullName>
        <description>Cambia el valor del campo &quot;Renovable&quot; de la MS por False.</description>
        <field>BI_COL_Renovable__c</field>
        <literalValue>0</literalValue>
        <name>BI_COL_MS_no_renovable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_COL_UpdateEstadoPendiente</fullName>
        <description>Cada vez que se crea una MS el valor del campo estado debe ser igual a Pendiente.</description>
        <field>BI_COL_Estado__c</field>
        <literalValue>Pendiente</literalValue>
        <name>Update Estado Pendiente</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_COL_Actualiza_Fecha_RFS</fullName>
        <actions>
            <name>BI_COL_Actualiza_Fecha_RFB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  BI_COL_Segmento_Cliente__c &lt;&gt; &quot;EMPRESAS&quot;,  ISCHANGED( BI_COL_Fecha_instalacion_servicio_RFS__c )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_COL_MS_no_renovable</fullName>
        <actions>
            <name>BI_COL_MS_no_renovable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Marca la MS como no renovable si la clasificación del servicio es &apos;COBRO UNICO&apos; o si está asociada a una oportunidad marcada como autoconsumo</description>
        <formula>OR(   ISPICKVAL(BI_COL_Clasificacion_Servicio__c, &apos;COBRO UNICO&apos;),  BI_COL_Oportunidad__r.BI_COL_Autoconsumo__c  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_COL_UpdateEstadoPendiente</fullName>
        <actions>
            <name>BI_COL_UpdateEstadoPendiente</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>El estado de la modificiación al ser creada debe estar en estado pendiente</description>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
