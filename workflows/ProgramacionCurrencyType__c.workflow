<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Copy_Consecutivo2</fullName>
        <field>Consecutivo2__c</field>
        <formula>Consecutivo__c</formula>
        <name>Copy Consecutivo2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copia Consecutivo 2</fullName>
        <actions>
            <name>Copy_Consecutivo2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ProgramacionCurrencyType__c.Consecutivo2__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Currency Type copia el conscutivo en consecutivo 2</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
