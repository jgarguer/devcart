<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CopiaFolioaKey</fullName>
        <field>Key__c</field>
        <formula>Name</formula>
        <name>Copia Folio a Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Exportado_24hrs</fullName>
        <field>Exportado_24_hrs__c</field>
        <literalValue>1</literalValue>
        <name>Exportado 24hrs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alarma de programaciones exportadas a SAP y sin respuesta en un periodo de 24</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Programacion__c.Exportado__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>Programacion__c.Facturado__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <description>Alarma de programaciones exportadas a SAP y sin respuesta en un periodo de 24</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Exportado_24hrs</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Programacion__c.LastModifiedDate</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Copia Folio a Key</fullName>
        <actions>
            <name>CopiaFolioaKey</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Programacion__c.Key__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
