<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_ARG_Aviso_Vto</fullName>
        <description>BI ARG Aviso de Vencimiento</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_ARG_Notificacion_fecha_de_vencimiento_contrato</template>
    </alerts>
    <alerts>
        <fullName>BI_Alerta_renovacion_contrato_por_finalizacion</fullName>
        <description>Alerta de correo que envía al propietario de la cuenta de que faltan siete días para finalización de contrato</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_comunicar_ejecutivo_clientes_renovacion_contrato</template>
    </alerts>
    <alerts>
        <fullName>BI_COL_Envia_correo_dueno_contrato_Falten_2_meses_finalizar</fullName>
        <description>Envía correo al dueño del contrato cuando se falten 2 meses para finalizar el contrato</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_BI_COL/BI_COL_Vencimiento_del_contrato</template>
    </alerts>
    <alerts>
        <fullName>BI_COL_Vencimiento_Contrato_30diasAntes</fullName>
        <description>Envía correo electrónico al propietario del contrato 30 días antes de la fecha de vencimiento del contrato.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_BI_COL/BI_COL_Vencimiento_Contrato_30diasAntes</template>
    </alerts>
    <alerts>
        <fullName>BI_FVI_Notificacion_Adjuntos_Contratos</fullName>
        <description>Notificación Adjuntos Contratos</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_FVI/BI_FVI_Notificacion_Adjuntos_Contratos_3</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_COL_Actualizar_Nombre_Contrato</fullName>
        <description>Actuliaza el nombre del contrato cuando es para colombia</description>
        <field>Name</field>
        <formula>ContractNumber &amp;&quot; - &quot;&amp;TEXT(YEAR(DATEVALUE(CreatedDate)))</formula>
        <name>BI_COL_Actualizar Nombre Contrato</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_COL_Adiciones_Presupuesto_contrato</fullName>
        <field>BI_COL_Presupuesto_contrato__c</field>
        <formula>BI_COL_Adiciones__c + BI_COL_Presupuesto_inicial_contrato__c</formula>
        <name>Adiciones Presupuesto contrato</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Duracion_contrato_Meses</fullName>
        <field>ContractTerm</field>
        <formula>CEILING(BI_Oportunidad__r.BI_Duracion_del_contrato_Meses__c)</formula>
        <name>Duracion contrato Meses</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_StarDate_FinDeImplementacion</fullName>
        <description>D-000612</description>
        <field>StartDate</field>
        <formula>BI_Fecha_fin_de_implementacion__c</formula>
        <name>BI_StarDate_FinDeImplementacion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Campo_fecha_de_creacion</fullName>
        <field>BI_Fecha_de_creacion__c</field>
        <formula>TODAY()</formula>
        <name>Campo_fecha_de_creación</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_ARG_Aviso_Vencimiento</fullName>
        <active>true</active>
        <description>Creada por demanda 321</description>
        <formula>AND( Account.TGS_Es_MNC__c=FALSE,  TEXT( Account.BI_Country__c) =  $Label.BI_Argentina,  NOT(ISBLANK(TEXT(OwnerExpirationNotice ))) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_ARG_Aviso_Vto</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.BI_ARG_Fecha_Aviso_Vto__c</offsetFromField>
            <timeLength>-12</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_Actualizar_campo_fecha_de_creacion</fullName>
        <actions>
            <name>Campo_fecha_de_creacion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_COL_Actualizar Nombre Contrato</fullName>
        <actions>
            <name>BI_COL_Actualizar_Nombre_Contrato</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Regla de flujo la cual se encarga de actualizar el name del contrato cuando este es de Colombia</description>
        <formula>AND ( !$Setup.BI_bypass__c.BI_migration__c, 	 (TEXT($User.Pais__c) = &apos;Colombia&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_COL_Adiciones Presupuesto contrato</fullName>
        <actions>
            <name>BI_COL_Adiciones_Presupuesto_contrato</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (     BI_COL_Presupuesto_inicial_contrato__c &lt;&gt; null,     BI_COL_Adiciones__c &lt;&gt; null,     !$Setup.BI_bypass__c.BI_migration__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_COL_Aviso dos mese antes vencimiento</fullName>
        <active>true</active>
        <formula>(TEXT($User.Pais__c) = &apos;Colombia&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_COL_Vencimiento_Contrato_30diasAntes</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>BI_COL_Envia_correo_dueno_contrato_Falten_2_meses_finalizar</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_COL_Aviso dos mese antes vencimiento OLD</fullName>
        <active>false</active>
        <formula>(TEXT($User.Pais__c) = &apos;Colombia&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_COL_Envia_correo_dueno_contrato_Falten_2_meses_finalizar</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.EndDate</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_Comunicar_a_ejecutivo_clientes_gestionar_renovacion</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract.BI_Autorenovable__c</field>
            <operation>equals</operation>
            <value>NO</value>
        </criteriaItems>
        <description>Regla por la cual el ejecutivo clientes recibirá un aviso de vencimiento del contrato 7 días antes.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_Alerta_renovacion_contrato_por_finalizacion</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contract.BI_Fecha_fin_de_servicio__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_Duracion_contrato_meses</fullName>
        <actions>
            <name>BI_Duracion_contrato_Meses</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_Rellenar_StarDate_ConFechaFinImple</fullName>
        <actions>
            <name>BI_StarDate_FinDeImplementacion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.BI_Fecha_fin_de_implementacion__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>D-000612</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
