<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_SUB_Prop_Pedido</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <ccEmails>alvars-d@hotmail.com</ccEmails>
        <description>BI_SUB_Prop_Pedido</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ASIGNAR_PEDIDO_SUBSIDIO</template>
    </alerts>
    <rules>
        <fullName>BI_SUB_Correo_Propietario</fullName>
        <actions>
            <name>BI_SUB_Prop_Pedido</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( OwnerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
