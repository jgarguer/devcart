<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_Notificaci_n_de_una_nueva_Demanda</fullName>
        <description>BI_Notificación de una nueva Demanda</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Propietario_demandas</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_Actualiza_Fecha_Solicitud_Analizada</fullName>
        <field>BI_Fecha_solicitud_analizada__c</field>
        <formula>Today()</formula>
        <name>Actualiza Fecha de Solicitud Analizada</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_Fecha_de_Certificado</fullName>
        <field>BI_Fecha_de_Certificado__c</field>
        <formula>Today()</formula>
        <name>Actualiza Fecha de Certificado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_Fecha_de_Pase_a_UAT</fullName>
        <description>Al cambiar la Demanda a “8-Certificación”, la fecha de pase a UAT será actualizada con la fecha del día.</description>
        <field>BI_Fecha_de_Pase_a_UAT__c</field>
        <formula>Today()</formula>
        <name>Actualiza Fecha de Pase a UAT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_Alerta_Propietario</fullName>
        <actions>
            <name>BI_Notificaci_n_de_una_nueva_Demanda</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_Demanda_Cambia_a_estado_4</fullName>
        <actions>
            <name>BI_Actualiza_Fecha_Solicitud_Analizada</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Demanda__c.BI_Estado_de_la_demanda__c</field>
            <operation>equals</operation>
            <value>4- Pend. Estimación</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Demanda_Cambia_a_estado_8</fullName>
        <actions>
            <name>BI_Actualiza_Fecha_de_Pase_a_UAT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Demanda__c.BI_Estado_de_la_demanda__c</field>
            <operation>equals</operation>
            <value>8- Certificación</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Demanda_Cambia_a_estado_9</fullName>
        <actions>
            <name>BI_Actualiza_Fecha_de_Certificado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Demanda__c.BI_Estado_de_la_demanda__c</field>
            <operation>equals</operation>
            <value>9- Pendiente de PAP</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
