<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_O4_Offer_Request_Accepted</fullName>
        <description>Offer Request Accepted</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Offer_Request/BI_O4_Offer_Request_Accepted</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Offer_Request_Answered</fullName>
        <description>Offer Request Answered</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Offer_Request/BI_O4_Offer_Request_Answered</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Offer_Request_Re_Answered</fullName>
        <description>Offer Request Re-Answered</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Offer_Request/BI_O4_Offer_Request_Re_Answered</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Offer_Request_Rejected</fullName>
        <description>Offer Request Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Offer_Request/BI_O4_Offer_Request_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_O4_OR_Created_State</fullName>
        <description>Offer request</description>
        <field>BI_O4_OR_State__c</field>
        <literalValue>TNA Created</literalValue>
        <name>OR Created State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Prov_Accept_Reject_Date_Stamp</fullName>
        <description>Offer request</description>
        <field>BI_O4_Provider_Acceptance_Rejection_Date__c</field>
        <formula>Now()</formula>
        <name>OR Provider Accept/Reject Date Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Ready_State</fullName>
        <description>Offer request</description>
        <field>BI_O4_OR_State__c</field>
        <literalValue>TIWS ACCEPT2</literalValue>
        <name>OR Ready State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_Obsolete</fullName>
        <description>Offer request</description>
        <field>BI_O4_Obsolete__c</field>
        <literalValue>0</literalValue>
        <name>OR_Reset_Obsolete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_OfferPresentedDate</fullName>
        <field>BI_O4_Offer_Presented_Date__c</field>
        <name>OR_Reset_OfferPresentedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_OfferReady</fullName>
        <field>BI_O4_Offer_Ready__c</field>
        <literalValue>0</literalValue>
        <name>OR_Reset_OfferReady</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_OfferRejected</fullName>
        <description>Offer request</description>
        <field>BI_O4_Offer_Rejected__c</field>
        <literalValue>0</literalValue>
        <name>OR_Reset_OfferRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_OfferRejectionDate</fullName>
        <description>Offer request</description>
        <field>BI_O4_Offer_Rejection_Date__c</field>
        <name>OR_Reset_OfferRejectionDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_OfferRejectionReason</fullName>
        <description>Offer request</description>
        <field>BI_O4_Offer_Rejection_Reason__c</field>
        <name>OR_Reset_OfferRejectionReason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_OfferReqSubmittedDate</fullName>
        <description>Offer request</description>
        <field>BI_O4_Offer_Req_Submitted_Date__c</field>
        <name>OR Reset OfferReqSubmittedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_OrderFormAcceptanceDate</fullName>
        <description>Offer request</description>
        <field>BI_O4_Order_Form_Acceptance_Date__c</field>
        <name>OR_Reset_OrderFormAcceptanceDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_PartialOffer</fullName>
        <description>Offer request</description>
        <field>BI_O4_Partial_Offer__c</field>
        <literalValue>0</literalValue>
        <name>OR_Reset_PartialOffer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_PresentedToCustomer</fullName>
        <description>Offer request</description>
        <field>BI_O4_Presented_to_Customer__c</field>
        <literalValue>0</literalValue>
        <name>OR_Reset_PresentedToCustomer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_ProvAcceptanceRejectionD</fullName>
        <description>Offer request</description>
        <field>BI_O4_Provider_Acceptance_Rejection_Date__c</field>
        <name>OR_Reset_ProviderAcceptanceRejectionD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_ProvReasonForRejection</fullName>
        <description>Offer request</description>
        <field>BI_O4_Provider_Rejection_Reason__c</field>
        <name>OR_Reset_ProviderReasonForRejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_ProviderAcceptance</fullName>
        <description>Offer request</description>
        <field>BI_O4_Provider_Acceptance__c</field>
        <name>OR_Reset_ProviderAcceptance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_ProviderComments</fullName>
        <description>Offer request</description>
        <field>BI_O4_Provider_Comments__c</field>
        <name>OR_Reset_ProviderComments</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_ProviderExistingMRC</fullName>
        <description>Offer request</description>
        <field>BI_O4_Provider_Existing_MRC__c</field>
        <name>OR_Reset_ProviderExistingMRC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_ProviderMRC</fullName>
        <description>Offer request</description>
        <field>BI_O4_Provider_MRC__c</field>
        <name>OR_Reset_ProviderMRC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_ProviderNRC</fullName>
        <description>Offer request</description>
        <field>BI_O4_Provider_NRC__c</field>
        <name>OR_Reset_ProviderNRC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_ReadyForBillingDate</fullName>
        <description>Offer request</description>
        <field>BI_O4_Ready_for_Billing_Date__c</field>
        <name>OR_Reset_ReadyForBillingDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_ReadyForServiceDate</fullName>
        <description>Offer request</description>
        <field>BI_O4_Ready_for_Service_Date__c</field>
        <name>OR_Reset_ReadyForServiceDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Reset_SIGMAProject</fullName>
        <description>Offer request</description>
        <field>BI_O4_SIGMA_Project__c</field>
        <name>OR_Reset_SIGMAProject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Sent_State</fullName>
        <description>Offer request</description>
        <field>BI_O4_OR_State__c</field>
        <literalValue>Sent to TIWS</literalValue>
        <name>OR Sent State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_TIWS_Accepted_State</fullName>
        <description>Offer request</description>
        <field>BI_O4_OR_State__c</field>
        <literalValue>TIWS ACCEPT1</literalValue>
        <name>OR TIWS Accepted State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_TIWS_Rejection_State</fullName>
        <description>Offer request</description>
        <field>BI_O4_OR_State__c</field>
        <literalValue>TIWS X</literalValue>
        <name>OR TIWS Rejection State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_TNA_Final_Acceptance</fullName>
        <field>BI_O4_OR_State__c</field>
        <literalValue>GO</literalValue>
        <name>OR TNA Final Acceptance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_TNA_Rejection_State</fullName>
        <field>BI_O4_OR_State__c</field>
        <literalValue>TNA X</literalValue>
        <name>OR TNA Rejection State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Update_Name</fullName>
        <description>Offer request</description>
        <field>Name</field>
        <formula>IF(
ISPICKVAL( CreatedBy.Pais__c  ,&apos;United States&apos;),  BI_O4_Offer_Req_Number__c  &amp; &quot; - &quot; &amp;  BI_O4_Oportunidad__r.BI_O4_Project_Number__c  &amp; &quot; - &quot; &amp; UPPER ( LEFT ( RecordType.Name, 2) )&amp;&quot; - US&quot;, BI_O4_Offer_Req_Number__c &amp; &quot; - &quot; &amp; BI_O4_Oportunidad__r.BI_O4_Project_Number__c &amp; &quot; - &quot; &amp; UPPER ( LEFT ( RecordType.Name, 2) )&amp;&quot; - PR&quot;)</formula>
        <name>OR_Update_Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Update_OfferReqSubmittedDate</fullName>
        <description>Offer request</description>
        <field>BI_O4_Offer_Req_Submitted_Date__c</field>
        <formula>Now()</formula>
        <name>OR Update OfferReqSubmittedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Update_Provider_Response_Date</fullName>
        <description>Offer request</description>
        <field>BI_O4_Provider_Response_Date__c</field>
        <formula>Now()</formula>
        <name>OR Update Provider Response Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Update_Provider_Support_Email</fullName>
        <description>Offer request</description>
        <field>BI_O4_Provider_Support_Engineer_Email__c</field>
        <formula>CASE ( BI_O4_Provider_Support_Engineer__c, 
&quot;Amada Fermin&quot;, &quot;amada.fermin.ext@telefonica.com&quot;, 
&quot;Gustavo Lipolis&quot;,&quot;gustavo.lipolis@telefonica.com&quot;, 
&quot;Marcelo Cursino&quot;,&quot;marcelo.cursino@telefonica.com&quot;, 
&quot;Yves Muller&quot;,&quot;yves.muller@telefonica.com&quot;, 
&quot;Zoran Jovanovic&quot;, &quot;zoran.jovanovic.ext@telefonica.com&quot;, 
NULL 
)</formula>
        <name>OR Update Provider Support Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_OR_Update_Rejection_Date</fullName>
        <field>BI_O4_Offer_Rejection_Date__c</field>
        <formula>Now()</formula>
        <name>BI_O4_OR_Update_Rejection_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_O4_Offer Acknowledged</fullName>
        <actions>
            <name>BI_O4_OR_TNA_Final_Acceptance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_O4_Offer_Request__c.BI_O4_finalTNAacceptance__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>Offer Request</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Offer Re-Submitted</fullName>
        <actions>
            <name>BI_O4_Offer_Request_Re_Answered</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Offer Request</description>
        <formula>and(BI_O4_Offer_Rejected__c = False , PRIORVALUE( BI_O4_Offer_Rejected__c ) = True)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Offer Rejection Update</fullName>
        <actions>
            <name>BI_O4_OR_Reset_OfferPresentedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_OfferReady</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_TNA_Rejection_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Update_Rejection_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_O4_Offer_Request__c.BI_O4_Offer_Rejected__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>Offer Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Offer Request Accepted</fullName>
        <actions>
            <name>BI_O4_Offer_Request_Accepted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Prov_Accept_Reject_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_TIWS_Accepted_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_O4_Offer_Request__c.BI_O4_Provider_Acceptance__c</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <description>Offer Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Offer Request Answered</fullName>
        <actions>
            <name>BI_O4_Offer_Request_Answered</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Ready_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_OfferRejected</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_OfferRejectionDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_OfferRejectionReason</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Update_Provider_Response_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_O4_Offer_Request__c.BI_O4_Offer_Ready__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>Offer Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Offer Request Created</fullName>
        <actions>
            <name>BI_O4_OR_Reset_OfferRejected</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_OfferRejectionDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_OfferRejectionReason</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_PresentedToCustomer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_ProvAcceptanceRejectionD</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_ProvReasonForRejection</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_ProviderAcceptance</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_ProviderExistingMRC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_ProviderMRC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Update_Provider_Support_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Offer request</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Offer Request Created 2</fullName>
        <actions>
            <name>BI_O4_OR_Reset_Obsolete</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_OfferPresentedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_OfferReady</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_OrderFormAcceptanceDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_PartialOffer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_ProviderComments</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_ProviderNRC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_ReadyForBillingDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_ReadyForServiceDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_SIGMAProject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Offer request</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Offer Request Created 3</fullName>
        <actions>
            <name>BI_O4_OR_Created_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Reset_OfferReqSubmittedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Offer request</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Offer Request Name Update</fullName>
        <actions>
            <name>BI_O4_OR_Update_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Offer request</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Offer Request Rejected</fullName>
        <actions>
            <name>BI_O4_Offer_Request_Rejected</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BI_O4_OR_Prov_Accept_Reject_Date_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_OR_TIWS_Rejection_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_O4_Offer_Request__c.BI_O4_Provider_Acceptance__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Offer request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Offer Request Sent</fullName>
        <actions>
            <name>BI_O4_OR_Sent_State</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_O4_Offer_Request__c.BI_O4_Offer_Req_Submitted_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>BI_O4_Offer_Request__c.BI_O4_Provider_Acceptance__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Offer request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Offer Request Set Date Sent</fullName>
        <actions>
            <name>BI_O4_OR_Update_OfferReqSubmittedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Offer request</description>
        <formula>ConnectionReceivedId != NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
