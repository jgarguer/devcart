<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_COL_Actualizar_Nombre_Anexo</fullName>
        <field>Name</field>
        <formula>TEXT(BI_COL_Grupo__c)</formula>
        <name>BI_COL_Actualizar Nombre Anexo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_COL_Actualizar_name_FUN</fullName>
        <field>Name</field>
        <formula>&quot;FUN - &quot;+ BI_COL_Consecutivo_FUN__c</formula>
        <name>BI_COL_Actualizar name FUN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_COL_Actualizar Nombre Anexo</fullName>
        <actions>
            <name>BI_COL_Actualizar_Nombre_Anexo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_COL_Anexos__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>BI Anexos</value>
        </criteriaItems>
        <description>Regla de flujo la cual crea el nombre para el fun de manera automática</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_COL_Actualizar Nombre Tramite</fullName>
        <actions>
            <name>BI_COL_Actualizar_name_FUN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_COL_Anexos__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>BI FUN</value>
        </criteriaItems>
        <description>Regla encargada de actualizar le nombre al FUN</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
