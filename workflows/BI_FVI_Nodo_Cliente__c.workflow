<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_FVI_Actualizar_Exclusivo_IdCliente</fullName>
        <description>Actualiza el valor del campo Exclusivo con el Id del Cliente para garantizar que solo existe una referencia al mismo cliente para los tipos de nodo indicados en la condición de la regla.</description>
        <field>BI_FVI_Exclusivo__c</field>
        <formula>CASESAFEID(BI_FVI_IdCliente__c)</formula>
        <name>Actualizar Exclusivo IdCliente</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_FVI_Actualizar_Exclusivo_IdNodo_IdCli</fullName>
        <description>Actualiza el valor del campo Exclusivo a IdNodo+IdCliente para garantizar que solo exista una ocurrencia de la relación &quot;Nodo+Cliente&quot;</description>
        <field>BI_FVI_Exclusivo__c</field>
        <formula>CASESAFEID(BI_FVI_IdNodo__c) + &quot;_&quot; + CASESAFEID(BI_FVI_IdCliente__c)</formula>
        <name>Actualizar Exclusivo IdNodo+IdCliente</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_FVI_Exclusividad_Cliente_Nodo_1</fullName>
        <actions>
            <name>BI_FVI_Actualizar_Exclusivo_IdCliente</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Regla que Actualiza el Campo &quot;Exclusivo&quot; para que en el caso de los nodos de tipo &quot;NAB&quot;, &quot;NCP&quot;, &quot;NAV&quot;, &quot;NAT&quot;,&quot;NAG&quot;,&quot;NCO&quot; y &quot;NAO&quot; se rellene con el valor del campo IdCliente y para los otros IdNodo+IdCliente</description>
        <formula>OR (BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NAB&quot;,        BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NCP&quot;,        BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NAV&quot;,        BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NAT&quot;,        BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NAG&quot;,        BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NCO&quot;,        BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NAO&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_FVI_Exclusividad_Cliente_Nodo_2</fullName>
        <actions>
            <name>BI_FVI_Actualizar_Exclusivo_IdNodo_IdCli</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Regla que Actualiza el Campo &quot;Exclusivo&quot; para que en el caso de los nodos que NO SON de tipo &quot;NAB&quot;, &quot;NCP&quot;, &quot;NAV&quot;, &quot;NAT&quot;, &quot;NAG&quot;,&quot;NCO&quot; y &quot;NAO&quot; se rellene con el valor del campo IdNodo+IdCliente y para los otros IdCliente</description>
        <formula>NOT OR ( BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NAB&quot;,  BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NCP&quot;,  BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NAV&quot;,    BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NAT&quot;,    BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NAG&quot;,    BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NCO&quot;,    BI_FVI_IdNodo__r.RecordType.DeveloperName = &quot;BI_FVI_NAO&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
