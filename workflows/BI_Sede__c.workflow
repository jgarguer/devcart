<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_Actualizar_nombre_sede</fullName>
        <field>Name</field>
        <formula>LEFT(BI_Direccion__c &amp; &quot;, &quot; &amp; BI_Numero__c &amp; &quot; (&quot; &amp; 
BI_Codigo_postal__c  &amp; &quot; &quot;  &amp;   
BI_Localidad__c &amp; &quot;)&quot; &amp; &quot; &quot; &amp; BI_Provincia__c,80)</formula>
        <name>BI_Actualizar_nombre_sede</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_COL_Actualizar_Pais_Direccion</fullName>
        <description>Actualiza el campo país de la dirección cuando el usuario es de Colombia</description>
        <field>BI_Country__c</field>
        <literalValue>Colombia</literalValue>
        <name>BI_COL_Actualizar Pais Dirección</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_COL_Actualizar Pais Direccion</fullName>
        <actions>
            <name>BI_COL_Actualizar_Pais_Direccion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Flujo de trabajo el cual se encarga de actualizar siempre el pais de la dirección cuando es Colombia</description>
        <formula>AND ( !$Setup.BI_bypass__c.BI_migration__c, 	 (TEXT($User.Pais__c) = &apos;Colombia&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Rellenar_nombre_Sede</fullName>
        <actions>
            <name>BI_Actualizar_nombre_sede</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT($Permission.BI_Skip_Sede_concatenation)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
