<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Aviso_nueva_solicitud_de_aprobacion_sobre_prefactibilidad</fullName>
        <description>Aviso nueva solicitud de aprobacion sobre prefactibilidad</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_BI_COL/Aprobacion_Prefactibilidad_Pendiente</template>
    </alerts>
    <fieldUpdates>
        <fullName>EnvioEmailCial_en_true</fullName>
        <description>pone en true el campo [Enviar Email Cial] para disparar nuevamente el process Builder y haga envió a diferentes destinatarios.</description>
        <field>BI_COL_Enviar_email_cial__c</field>
        <literalValue>1</literalValue>
        <name>EnvioEmailCial en true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Envio_Email_TRUE</fullName>
        <field>BI_COL_Enviar_email__c</field>
        <literalValue>1</literalValue>
        <name>Envio Email TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Envio_Email_TRUE_Rechazo</fullName>
        <field>BI_COL_Enviar_email__c</field>
        <literalValue>1</literalValue>
        <name>Envio Email TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fecha_de_Aprobacion</fullName>
        <field>BI_COL_Fecha_aprovacion_prefactibilidad__c</field>
        <formula>TODAY()</formula>
        <name>Fecha de Aprobacion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fecha_de_Aprobacion_Rechazo</fullName>
        <field>BI_COL_Fecha_aprovacion_prefactibilidad__c</field>
        <formula>TODAY()</formula>
        <name>Fecha de Aprobacion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Registro_Rechazado</fullName>
        <field>BI_COL_Registro_aprobado__c</field>
        <literalValue>Rechazada</literalValue>
        <name>Registro Rechazado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Registro_aprobado</fullName>
        <field>BI_COL_Registro_aprobado__c</field>
        <literalValue>Aprobada</literalValue>
        <name>Registro aprobado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
