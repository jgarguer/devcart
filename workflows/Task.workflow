<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_Actualiza_fecha_de_cierre</fullName>
        <description>Actualiza el valor del campo Fecha de Cierre de Tarea con el valor de la fecha actual.

Creado por la Demanda 173</description>
        <field>BI_Fecha_de_Cierre__c</field>
        <formula>TODAY()</formula>
        <name>Actualiza fecha de cierre</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_Actualiza_fecha_de_cierre</fullName>
        <actions>
            <name>BI_Actualiza_fecha_de_cierre</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Actualiza la fecha de cierre de la Tarea cuando el estado pase a &apos;Completada&apos;

Creada por la Demanda 173</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
