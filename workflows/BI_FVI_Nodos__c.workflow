<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_FVI_Actualizar_IdNodo</fullName>
        <field>BI_FVI_IdNodo__c</field>
        <formula>Name</formula>
        <name>Actualizar IdNodo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_FVI_NodoUnico</fullName>
        <actions>
            <name>BI_FVI_Actualizar_IdNodo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_FVI_Nodos__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Copia el valor del NAME al camo IdNodo (que es exclusivo para que genere una excepcion si encuentra algun otro igual)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
