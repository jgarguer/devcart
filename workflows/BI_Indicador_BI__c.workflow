<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_Actualizar_Estado_Indicador_BI</fullName>
        <field>BI_Estado_indicador__c</field>
        <literalValue>Validado</literalValue>
        <name>Actualizar Estado Indicador BI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI Cambio de estado Indicador BI</fullName>
        <actions>
            <name>BI_Actualizar_Estado_Indicador_BI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (NOT ISNULL(BI_Validacion_del_indicador__c),          NOT ISBLANK(BI_Validacion_del_indicador__c),          ISCHANGED(BI_Validacion_del_indicador__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
