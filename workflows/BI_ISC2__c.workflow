<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_Actualizar_Performance_media_GAP_CORE</fullName>
        <field>BI_Performance_media_GAP_CORE_Aux__c</field>
        <formula>BI_Performance_media_GAP_CORE__c</formula>
        <name>BI_Actualizar_Performance_media_GAP_CORE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_Actualizar_Performance_media_GAP</fullName>
        <actions>
            <name>BI_Actualizar_Performance_media_GAP_CORE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
