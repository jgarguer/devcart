<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_Solicitud_de_aprobacion_de_Bolsa_de_dinero_Solicitud</fullName>
        <description>Solicitud de aprobación de Bolsa de dinero - Solicitud</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Proceso_de_Bolsa_de_dinero_solicitud</template>
    </alerts>
    <alerts>
        <fullName>BI_Solicitud_de_aprobacion_de_bolsa_de_dinero_aprobada</fullName>
        <description>Solicitud de aprobación de bolsa de dinero - aprobada</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Proceso_de_bolsa_de_dinero_aprobado</template>
    </alerts>
    <alerts>
        <fullName>BI_Solicitud_de_aprobacion_de_bolsa_de_dinero_rechazado</fullName>
        <description>Solicitud de aprobación de bolsa de dinero - Rechazado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Proceso_de_Bolsa_de_dinero_rechazado</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_Bolsa_de_dinero_Cancelada_Casilla</fullName>
        <field>BI_Estado_del_proceso__c</field>
        <literalValue>Cancelado</literalValue>
        <name>Bolsa de dinero Cancelada Casilla</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Bolsa_de_dinero_Fecha_Inicio</fullName>
        <field>BI_Fecha_de_inicio__c</field>
        <formula>TODAY()</formula>
        <name>Bolsa de dinero - Fecha Inicio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Bolsa_de_dinero_Pricing</fullName>
        <description>Estado inicial del Beneficio al ser creado por Ejecutivo de Cliente.</description>
        <field>BI_Estado_del_proceso__c</field>
        <literalValue>Pricing</literalValue>
        <name>Bolsa de dinero Pricing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Bolsa_de_dinero_RT_Cerrado</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BI_Cerrado_Asociado_a_modelo</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Bolsa de dinero RT Cerrado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Bolsa_de_dinero_RT_Cerrado_Libre</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BI_Cerrado_Libre</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Bolsa de dinero RT Cerrado Libre</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Bolsa_de_dinero_aprobada</fullName>
        <description>El beneficio fue aprobado.</description>
        <field>BI_Estado_del_proceso__c</field>
        <literalValue>Cerrado</literalValue>
        <name>Bolsa de dinero aprobada</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Bolsa_de_dinero_rechazado</fullName>
        <description>El Beneficio fue rechazado por Marketing.</description>
        <field>BI_Estado_del_proceso__c</field>
        <literalValue>Pendiente</literalValue>
        <name>Bolsa de dinero rechazado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_CHI_Actualizacion_saldo_disponible_ok</fullName>
        <field>BI_Saldo_disponible__c</field>
        <formula>BI_Monto_Inicial__c</formula>
        <name>BI_CHI_Actualizacion saldo disponible</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_CHI_Actualizar saldo disponible</fullName>
        <actions>
            <name>BI_CHI_Actualizacion_saldo_disponible_ok</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISNEW(),  ISCHANGED( BI_Monto_Inicial__c )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Bolsa de dinero - Fecha Inicio</fullName>
        <actions>
            <name>BI_Bolsa_de_dinero_Fecha_Inicio</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Bolsa de dinero Bloqueada Asociado a modelo</fullName>
        <actions>
            <name>BI_Bolsa_de_dinero_RT_Cerrado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Bolsa_de_dinero__c.BI_Estado_del_proceso__c</field>
            <operation>equals</operation>
            <value>Rechazado,Cerrado,Cancelado</value>
        </criteriaItems>
        <criteriaItems>
            <field>BI_Bolsa_de_dinero__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Asociado a modelo</value>
        </criteriaItems>
        <description>La bolsa de dinero ha sido bloqueada al haber sido , Cerrada, Cancelada o Rechazada</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Bolsa de dinero Bloqueada Libre</fullName>
        <actions>
            <name>BI_Bolsa_de_dinero_RT_Cerrado_Libre</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Bolsa_de_dinero__c.BI_Estado_del_proceso__c</field>
            <operation>equals</operation>
            <value>Rechazado,Cerrado,Cancelado</value>
        </criteriaItems>
        <criteriaItems>
            <field>BI_Bolsa_de_dinero__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Libre</value>
        </criteriaItems>
        <description>La bolsa de dinero ha sido bloqueada al haber sido , Cerrada, Cancelada o Rechazada</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Bolsa de dinero Cancelada</fullName>
        <actions>
            <name>BI_Bolsa_de_dinero_Cancelada_Casilla</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Bolsa_de_dinero__c.BI_Cancelar_bolsa_de_dinero__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
