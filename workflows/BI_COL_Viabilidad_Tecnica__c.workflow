<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Respuesta_Viabilidad</fullName>
        <description>Respuesta Viabilidad</description>
        <protected>false</protected>
        <recipients>
            <field>BI_COL_Destinatario_Viabilidad__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_BI_COL/BI_COL_Respuesta_Viabilidad</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_COL_Actualizar_Destinatario</fullName>
        <field>BI_COL_Destinatario_Viabilidad__c</field>
        <formula>BI_COL_Destinatario__c</formula>
        <name>BI_COL_Actualizar Destinatario</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_COL_Actualizar_Destinatario</fullName>
        <actions>
            <name>BI_COL_Actualizar_Destinatario</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_COL_Viabilidad_Tecnica__c.BI_COL_Destinatario__c</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <description>Regla de flujo para la actualización del destinatario para el envío de la notificacón</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_COL_Respuesta de Viabilidad_COL</fullName>
        <actions>
            <name>Respuesta_Viabilidad</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND (   OR(      ISPICKVAL(BI_COL_Estado__c, &quot;Solicitud Reestudio&quot;),      ISPICKVAL(BI_COL_Estado__c, &quot;Cancelado&quot;),      ISPICKVAL(BI_COL_Estado__c, &quot;Entregado&quot;),      ISPICKVAL(BI_COL_Estado__c, &quot;Devuelto&quot;),      ISPICKVAL(BI_COL_Estado__c, &quot;En reestudio&quot;),      ISPICKVAL(BI_COL_Estado__c, &quot;Reestudio entregado&quot;)    ),    NOT ISBLANK(BI_COL_Fec__c),    !$Setup.BI_bypass__c.BI_migration__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
