<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_Notificacion_linea_recambio_No_entregado</fullName>
        <description>Notificación linea recambio No entregado</description>
        <protected>false</protected>
        <recipients>
            <recipient>Service Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Solicitud_de_recambio_no_entregado</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_CHI_Actualiza_Tipo_Solicitud_Recambio</fullName>
        <field>BI_Tipo_de_solicitud__c</field>
        <formula>TEXT(BI_Solicitud__r.BI_Tipo_solicitud__c)</formula>
        <name>BI_CHI_Actualiza Tipo Solicitud Recambio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_CHI_Linea de recambio No entregado</fullName>
        <actions>
            <name>BI_Notificacion_linea_recambio_No_entregado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Linea_de_Recambio__c.BI_Estado_de_linea_de_recambio__c</field>
            <operation>equals</operation>
            <value>No entregado</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_CHi_Actualizar Tipo Solicitud Recambio</fullName>
        <actions>
            <name>BI_CHI_Actualiza_Tipo_Solicitud_Recambio</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
