<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_Notificacion_de_tarea_proyecto</fullName>
        <description>BI_Notificacion de tarea proyecto</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Notificacion_asignacion_tarea_proyecto</template>
    </alerts>
    <alerts>
        <fullName>BI_Notificacion_fecha_de_vencimiento_tarea_de_proyecto</fullName>
        <description>Notificación fecha de vencimiento tarea de proyecto</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Notificacion_fecha_de_vencimiento_tarea</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_Actualizar_campo_Duracion_Tarea</fullName>
        <field>BI_Date_of_completed__c</field>
        <formula>NOW()</formula>
        <name>Actualizar campo Duracion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Compute_Dedication</fullName>
        <field>BI_O4_Dedication__c</field>
        <formula>IF( 
Estimated_Hours__c / 40 &lt; 1, 
Estimated_Hours__c / 40, 
1)</formula>
        <name>BI_O4_Compute_Dedication</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Update_Priority</fullName>
        <field>Priority__c</field>
        <literalValue>2</literalValue>
        <name>BI_O4_Update_Priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_Asignacion Tarea de Proyecto</fullName>
        <actions>
            <name>BI_Notificacion_de_tarea_proyecto</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifica al usuario asignado a una tarea de proyecto dicha tarea</description>
        <formula>AND( OR( ISBLANK(Assigned_To__c), ISNULL(Assigned_To__c) ), !$Setup.BI_bypass__c.BI_migration__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Notificacion fecha vencimiento tarea proyecto</fullName>
        <active>true</active>
        <description>Notificación al usuario responsable de la tarea de proyecto que está a punto de finalizar el vencimiento</description>
        <formula>AND( Complete__c = FALSE, !$Setup.BI_bypass__c.BI_migration__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_Notificacion_fecha_de_vencimiento_tarea_de_proyecto</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Milestone1_Task__c.Due_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_O4_Task_Dedication_validation</fullName>
        <actions>
            <name>BI_O4_Compute_Dedication</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Milestone1_Task__c.BI_O4_Dedication__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Update_Priority_2</fullName>
        <actions>
            <name>BI_O4_Update_Priority</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Milestone1_Task__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>BI_O4_Customer_Management_Task</value>
        </criteriaItems>
        <criteriaItems>
            <field>Milestone1_Task__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>BI_O4_Internal_Management_Task</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_actualizar_Date_Of_Completed_TareaProyecto</fullName>
        <actions>
            <name>BI_Actualizar_campo_Duracion_Tarea</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  RecordType.DeveloperName = &apos;BI_HandOver_y_Establishment&apos;,  (Complete__c),true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
