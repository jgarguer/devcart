<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_Actualizar_Campo_Duracion_Asignacion</fullName>
        <field>BI_Duracion_asignacion_del_caso__c</field>
        <formula>0</formula>
        <name>Actualizar Campo Duracion de Asignacion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI Duracion Mayor a Cero</fullName>
        <actions>
            <name>BI_Actualizar_Campo_Duracion_Asignacion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Asignacion_de_casos__c.BI_Duracion_asignacion_del_caso__c</field>
            <operation>lessThan</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
