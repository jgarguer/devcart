<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_O4_Update_Services</fullName>
        <field>BI_O4_Services__c</field>
        <literalValue>None</literalValue>
        <name>BI_O4_Update_Services</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_O4_Completar_Campo_Services</fullName>
        <actions>
            <name>BI_O4_Update_Services</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Milestone1_Time__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Internal Management Time</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
