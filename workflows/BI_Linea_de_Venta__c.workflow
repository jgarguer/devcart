<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_CHI_Notificacion_linea_venta_no_entregado</fullName>
        <description>Notificación linea venta no entregado</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_Solicitud_de_venta_no_entregado</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_CHI_Actualizar_Tipo_solicitud</fullName>
        <field>BI_Tipo_de_solicitud__c</field>
        <formula>TEXT(BI_Solicitud__r.BI_Tipo_solicitud__c)</formula>
        <name>BI_CHI_Actualizar Tipo solicitud</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_CHI_Actualizar Tipo solicitud</fullName>
        <actions>
            <name>BI_CHI_Actualizar_Tipo_solicitud</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_CHI_Linea de venta no entregado</fullName>
        <actions>
            <name>BI_CHI_Notificacion_linea_venta_no_entregado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Linea_de_Venta__c.BI_Estado_de_linea_de_venta__c</field>
            <operation>equals</operation>
            <value>No entregado</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
