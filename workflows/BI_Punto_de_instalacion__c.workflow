<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_Actualizar_punto_instalacion</fullName>
        <field>Name</field>
        <formula>BI_Sede__r.BI_Direccion__c &amp;&quot; (&quot; &amp;  BI_Sede__r.BI_Localidad__c  &amp; &quot;)&quot;</formula>
        <name>BI_Actualizar_punto_instalacion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_Actualizar_Punto_instalacion</fullName>
        <actions>
            <name>BI_Actualizar_punto_instalacion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  NOT($Permission.BI_Skip_Sede_concatenation),  OR(    ISBLANK(Name),    CONTAINS(Name,  $Label.BI_CORE_Automatico )   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
