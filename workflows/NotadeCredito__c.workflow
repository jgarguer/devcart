<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ActualizacionKey</fullName>
        <field>Key__c</field>
        <formula>Name</formula>
        <name>Actualizacion Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Exportado_Asignaci_n</fullName>
        <field>Fecha_Solicitud__c</field>
        <formula>Today()</formula>
        <name>Exportado Asignación</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Exportado Asignación</fullName>
        <actions>
            <name>Exportado_Asignaci_n</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>NotadeCredito__c.Exportado__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>La fecha de solicitud debe de ser igual a la fecha de en que se procesa el envío a SAP</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nota de Credito Key</fullName>
        <actions>
            <name>ActualizacionKey</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>NotadeCredito__c.Key__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
