<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_FVI_Act_Exclusivo_Trafico</fullName>
        <field>BI_FVI_Exclusivo__c</field>
        <formula>IF( OR (ISNULL(BI_FVI_Cuenta__c),BI_FVI_Cuenta__c=&quot;&quot;)  ,   TEXT(BI_FVI_Country__c) ,TEXT(BI_FVI_Country__c) + &quot;_&quot; + CASESAFEID(BI_FVI_Cuenta__c) )</formula>
        <name>Actualizar Exclusivo Trafico</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_FVI_Trafico_Exclusivo</fullName>
        <actions>
            <name>BI_FVI_Act_Exclusivo_Trafico</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rellena el campo exclusivo para que solo exista un registro por cliente y si es null el cliente que exista solo 1 por pais.</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
