<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_O4_Change_survey_status_to_Close</fullName>
        <field>BI_O4_Status__c</field>
        <literalValue>Close</literalValue>
        <name>Change survey status to Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Change_survey_status_to_Lunched</fullName>
        <field>BI_O4_Status__c</field>
        <literalValue>Launched</literalValue>
        <name>Change survey status to Lunched</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Changue_record_type_to_close</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BI_O4_Close</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Changue record type to close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_O4_Change survey status to Lunched</fullName>
        <actions>
            <name>BI_O4_Change_survey_status_to_Lunched</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>BI_O4_Launch_date__c  = TODAY ()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Close survey</fullName>
        <actions>
            <name>BI_O4_Change_survey_status_to_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_Changue_record_type_to_close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Close survey when today is the close date.</description>
        <formula>BI_O4_Close_date__c  &lt;=TODAY()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
