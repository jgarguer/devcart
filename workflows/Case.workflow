<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI2_Alerta_NotificacionCliente_Caso_Pendiente</fullName>
        <description>BI2 Alerta de Notificación al Cliente de Caso Pendiente</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_Notificacion_Caso_PendienteCliente</template>
    </alerts>
    <alerts>
        <fullName>BI2_Alerta_Notificacion_Caso_Cancelado</fullName>
        <description>BI2 Alerta de Notificación Caso Cancelado</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_Notificacion_Caso_Cancelado</template>
    </alerts>
    <alerts>
        <fullName>BI2_Alerta_Notificacion_Caso_Cerrado</fullName>
        <description>BI2 Alerta de Notificación Caso Cerrado</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_Notificacion_Caso_Cerrado</template>
    </alerts>
    <alerts>
        <fullName>BI2_Alerta_Notificacion_Caso_Pendiente</fullName>
        <description>BI2 Alerta de Notificación Caso Pendiente</description>
        <protected>false</protected>
        <recipients>
            <recipient>Asesor Advisor</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_Notificacion_Caso_Pendiente</template>
    </alerts>
    <alerts>
        <fullName>BI2_COL_Alerta_Cambio_propietario_nuevo_prop</fullName>
        <description>BI2_COL_Alerta_Cambio_propietario_nuevo_prop</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI2_COL_Plantillas_de_correo/BI2_COL_Cambio_porpietario_nuevo_prop</template>
    </alerts>
    <alerts>
        <fullName>BI2_COL_Alerta_Cierre_caso_comercial_SM</fullName>
        <description>BI2 COL Alerta Cierre caso comercial SM</description>
        <protected>false</protected>
        <recipients>
            <field>BI2_COL_Correo_electronico_Service_Manag__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI2_COL_Plantillas_de_correo/BI2_COL_Cierre_caso_comercial_SM</template>
    </alerts>
    <alerts>
        <fullName>BI2_COL_Alerta_Notificacion_Encuesta</fullName>
        <description>BI2_COL_Alerta_Notificacion_Encuesta</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI2_COL_Plantillas_de_correo/BI2_COL_Notificacion_Encuesta</template>
    </alerts>
    <alerts>
        <fullName>BI2_COL_Alerta_apertura_caso_comercial_y_tecnico</fullName>
        <description>BI2 COL Alerta apertura caso comercial y tecnico</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI2_COL_Plantillas_de_correo/BI2_COL_Apertura_de_caso</template>
    </alerts>
    <alerts>
        <fullName>BI2_COL_Apertura_caso_creador_no_SFDC</fullName>
        <description>BI2_COL_Apertura_caso_creador_no_SFDC</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI2_COL_Plantillas_de_correo/BI2_COL_Apertura_de_caso</template>
    </alerts>
    <alerts>
        <fullName>BI2_COL_Apertura_caso_prop_cta</fullName>
        <description>BI2_COL_Apertura_caso_prop_cta</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI2_COL_Plantillas_de_correo/BI2_COL_Apertura_de_caso_Prop_Cta</template>
    </alerts>
    <alerts>
        <fullName>BI2_COL_Apertura_del_caso_Comercial_y_Tecnico</fullName>
        <description>BI2 COL Apertura del caso Comercial y Técnico incluye Service Manager</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI2_COL_Plantillas_de_correo/BI2_COL_Apertura_de_caso</template>
    </alerts>
    <alerts>
        <fullName>BI2_COL_Apertura_del_caso_SM</fullName>
        <description>BI2_COL Apertura del caso SM</description>
        <protected>false</protected>
        <recipients>
            <field>BI2_COL_Correo_electronico_Service_Manag__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI2_COL_Plantillas_de_correo/BI2_COL_Apertura_de_caso_SM</template>
    </alerts>
    <alerts>
        <fullName>BI2_COL_Cierre_de_caso_Comercial_y_Tecnico</fullName>
        <description>BI2 COL Cierre de caso Comercial y Técnico</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI2_COL_Plantillas_de_correo/BI2_COL_Cierre_de_caso</template>
    </alerts>
    <alerts>
        <fullName>BI2_COL_Cierre_de_caso_prop_cta</fullName>
        <description>BI2_COL Cierre de caso prop cta</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI2_COL_Plantillas_de_correo/BI2_COL_Cierre_de_caso_Prop_Cta</template>
    </alerts>
    <alerts>
        <fullName>BI2_COL_Notificacion_CUN_Caso_comercial_2</fullName>
        <description>BI2_COL_Notificacion CUN Caso comercial 2</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI2_COL_Plantillas_de_correo/BI2_COL_Notificacion_CUN</template>
    </alerts>
    <alerts>
        <fullName>BI2_Envia_Emai_Autorizado_Cuenta_CCenter</fullName>
        <description>BI2_Envia_Emai_Autorizado_Cuenta_CCenter</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_Notificar_Nuevo_Caso_Call_Center</template>
    </alerts>
    <alerts>
        <fullName>BI2_Envia_Emai_Autorizado_Cuenta_PP</fullName>
        <description>BI2_Envia_Emai_Autorizado_Cuenta_PP</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_Notificar_Nuevo_Caso_Portal_Plantino</template>
    </alerts>
    <alerts>
        <fullName>BI2_Envia_Email_Cliente_Caso</fullName>
        <description>BI2 Envía Email al contacto del cliente cuando el Caso esté resuelto</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_Notificacion_Caso_Resuelto</template>
    </alerts>
    <alerts>
        <fullName>BI2_Envia_Email_Propietario_Cuenta_NuevoCaso</fullName>
        <description>BI2 Envía Email al propietario de la Cuenta cuando se crea un nuevo Caso</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_NotificacionPropietarioCuenta_Caso_Abierto</template>
    </alerts>
    <alerts>
        <fullName>BI2_NotificacionClienteCasoTecnicoCodCUN</fullName>
        <description>BI2 Notificación Cliente Caso Técnico con Cod CUN</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_NotificacionClienteCasoCodCUN</template>
    </alerts>
    <alerts>
        <fullName>BI2_NotificacionClienteCasoTecnicoCodCaso</fullName>
        <description>BI2 Notificación Cliente Caso Técnico con Cod Caso</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_NotificacionClienteCasoCodCaso</template>
    </alerts>
    <alerts>
        <fullName>BI2_NotificacionUsuarioCasoTecnicoCodCUN</fullName>
        <description>Bi2 Notificación Usuario Caso Técnico con Cod CUN</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_NotificacionUsuarioCasoCodCUN</template>
    </alerts>
    <alerts>
        <fullName>BI2_NotificacionUsuarioCasoTecnicoCodCaso</fullName>
        <description>Bi2 Notificación Usuario Caso Técnico con Cod Caso</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_NotificacionAsesorCasoCodCaso</template>
    </alerts>
    <alerts>
        <fullName>BI2_Notificacion_Creacion_Casos_Hijos</fullName>
        <description>BI2_Notificacion_Creacion_Casos_Hijos</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_De_Correo_BI2/BI2_Notificaci_n_Creacion_Casos_Hijos</template>
    </alerts>
    <alerts>
        <fullName>BI_ARG_Gestion_Adm_Cobranzas_Email_Alert</fullName>
        <description>BI ARG Gestion Adm Cobranzas Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_ARG_Gestion_Adm_Cobranzas_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>BI_ARG_Gestion_de_Cancelacion</fullName>
        <description>BI_ARG_Gestion_de_Cancelacion</description>
        <protected>false</protected>
        <recipients>
            <field>BI_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_ARG_Gestion_de_Cancelacion</template>
    </alerts>
    <alerts>
        <fullName>BI_ARG_NotificarOwnerCancelarCaso</fullName>
        <description>Notificar al Owner la cancelacion del caso</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_Caso_Cerrado</template>
    </alerts>
    <alerts>
        <fullName>BI_BJ_Creador_Caso_PAC</fullName>
        <description>Enviar notificacion al creador del caso cuando el estado es Pendiente de Aceptacion Cliente Bajas/Cancelaciones</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_BJ_Creador_PAC</template>
    </alerts>
    <alerts>
        <fullName>BI_BJ_Notificacion_creador_Caso</fullName>
        <description>Enviar notificacion al creador del caso cuando el estado es Vencido o Cancelado</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_BJ_Creador_Caso_cerrado</template>
    </alerts>
    <alerts>
        <fullName>BI_CGE_Envia_correo_asignacion</fullName>
        <description>BI_CGE_Envia_correo_asignacion</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CGE_Asignacion_Caso</template>
    </alerts>
    <alerts>
        <fullName>BI_CGE_Envia_correo_cambio_estado</fullName>
        <description>BI_CGE_Envia_correo_cambio_estado</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>BI_Solicitante__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CGE_Cambio_Estado</template>
    </alerts>
    <alerts>
        <fullName>BI_CGE_Envia_correo_cierre_caso</fullName>
        <description>BI_CGE_Envia_correo_cierre_caso</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>BI_Solicitante__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CGE_Cierre_Caso</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Alarma_SLA_atencion_70</fullName>
        <description>Chile - Alarma SLA atención 70%</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CHI_Alarma_SLA_atencion_70</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Alarma_SLA_atencion_80</fullName>
        <description>Chile - Alarma SLA atención 80%</description>
        <protected>false</protected>
        <recipients>
            <field>BI_CHI_Supervisor_del_propietario_Aux__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CHI_Alarma_SLA_atencion_80</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Alarma_SLA_atencion_90</fullName>
        <description>Chile - Alarma SLA atención 90%</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>BI_CHI_Supervisor_del_propietario_Aux__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CHI_Alarma_SLA_atencion_90</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Alarma_SLA_solucion_60</fullName>
        <description>Chile - Alarma SLA solución 60%</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CHI_Alarma_SLA_solucion_60</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Alarma_SLA_solucion_70</fullName>
        <description>Chile - Alarma SLA solución 70%</description>
        <protected>false</protected>
        <recipients>
            <field>BI_CHI_Supervisor_del_propietario_Aux__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CHI_Alarma_SLA_solucion_70</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Alarma_SLA_solucion_80</fullName>
        <description>Chile - Alarma SLA solución 80%</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>BI_CHI_Supervisor_del_propietario_Aux__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CHI_Alarma_SLA_solucion_80</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Caso_proveniente_del_segmento_Top_75</fullName>
        <description>Chile - Caso proveniente del segmento &quot;Top 75&quot;</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CHI_Caso_proveniente_del_segmento_Top_75</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Notificacin_descuento_aprobado</fullName>
        <description>Notificación de descuento aprobado</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_CHI_Descuento_aprobado</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Notificacion_asignacion_caso</fullName>
        <description>Notificación de asignación de caso</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_CHI_Creacion_de_caso_interno</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Notificacion_caso_soporteventa</fullName>
        <description>Notificacion de asignacion o modificacion de Caso Interno de departamento Soporte de Venta</description>
        <protected>false</protected>
        <recipients>
            <recipient>Propietario | Owner Creator</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>TGS_Assignee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_CHI_Caso_interno_soporteventa</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Notificacion_descuento_rechazado</fullName>
        <description>Notificación de descuento rechazado</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/BI_CHI_Descuento_rechazado</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Requerimiento_del_tipo_Agendamiento_de_equipos</fullName>
        <description>Chile - Requerimiento del tipo &quot;Agendamiento de equipos&quot;</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CHI_Requerimiento_del_tipo_Agendamiento_de_equipos</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Requerimiento_del_tipo_Alta_de_tv_digital_etc</fullName>
        <description>Chile - Requerimiento del tipo “Alta de tv digital&quot;, etc.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CHI_Requerimiento_del_tipo_Alta_de_tv_digital_etc</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Requerimiento_en_estado_Pendiente_12_horas</fullName>
        <description>Chile - Requerimiento en estado &quot;Pendiente&quot; (12 horas)</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>BI_CHI_Supervisor_del_propietario_Aux__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CHI_Requerimiento_en_estado_Pendiente_12_horas</template>
    </alerts>
    <alerts>
        <fullName>BI_CHI_Requerimiento_en_estado_Pendiente_6_horas</fullName>
        <description>Chile - Requerimiento en estado &quot;Pendiente&quot; (6 horas)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_CHI_Requerimiento_en_estado_Pendiente_6_horas</template>
    </alerts>
    <alerts>
        <fullName>BI_COL_NotificacionEmailToCase</fullName>
        <description>Notificación Email to case</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_BI_COL/BI_COL_NotificacionEmailToCase</template>
    </alerts>
    <alerts>
        <fullName>BI_Caso_relacionado_con_el_proveedor</fullName>
        <description>BI_Caso relacionado con el proveedor</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_Caso_relacionado_con_el_proveedor</template>
    </alerts>
    <alerts>
        <fullName>BI_E2C_ARG_Cobranzas_Notificar_Caso_Nuevo</fullName>
        <description>BI E2C ARG - Cobranzas - Notificar Caso Nuevo</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_E2C_ARG_Cobranzas_Caso_Nuevo</template>
    </alerts>
    <alerts>
        <fullName>BI_E2C_ARG_Cobranzas_Notificar_Caso_Nuevo_Cliente_No_Existente</fullName>
        <description>BI E2C ARG - Cobranzas - Notificar Caso Nuevo Cliente No Existente</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_E2C_ARG_Cobranzas_Caso_Nuevo_Cliente_No_Existente</template>
    </alerts>
    <alerts>
        <fullName>BI_Email_To_Case_ARG_Cobranzas_Notificar_Caso_Cerrado</fullName>
        <description>BI Email To Case ARG - Cobranzas - Notificar Caso Cerrado</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_E2C_ARG_Cobranzas_Caso_Cerrado</template>
    </alerts>
    <alerts>
        <fullName>BI_Envio_de_Correo_Caso_eHelp</fullName>
        <description>BI Cuando un caso eHelp es creado por primera vez se manda un correo al Usuario FInal</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_Caso_eHelp_Caso_Nuevo</template>
    </alerts>
    <alerts>
        <fullName>BI_Envio_de_email_Portal</fullName>
        <description>BI_Envío_de_email_contacto_Portal</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Envio_de_email_contacto_Portal_cliente</template>
    </alerts>
    <alerts>
        <fullName>BI_FVI_Notificacion_Reasignacion_de_Caso</fullName>
        <description>Notificación Reasignacion de Caso</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_FVI/BI_FVI_Notificacion_Asignacion_Caso</template>
    </alerts>
    <alerts>
        <fullName>BI_Notif_Opty_Owner</fullName>
        <description>BI_Notif_Opty_Owner</description>
        <protected>false</protected>
        <recipients>
            <field>BI_Opportunity_Owner__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_Caso_Interno_Caso_Cerrado</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Caso_Hijo_Cotizado</fullName>
        <description>Caso Hijo Cotizado</description>
        <protected>false</protected>
        <recipients>
            <field>BI_O4_Email_Responsable_Preventa_Global__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Approvals/BI_O4_Fecha_de_Cotizaci_n</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Caso_Hijo_Pendiente_Aclaraci_n</fullName>
        <description>Caso Hijo Pendiente Aclaración</description>
        <protected>false</protected>
        <recipients>
            <field>BI_O4_Email_Responsable_Preventa_Global__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Approvals/BI_O4_Caso_Hijo_Pendiente_Aclaracion</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Caso_Hijo_Rechazado</fullName>
        <description>Caso Hijo Rechazado</description>
        <protected>false</protected>
        <recipients>
            <field>BI_O4_Email_Responsable_Preventa_Global__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Approvals/BI_O4_Casos_Hijos_Rechazados</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Caso_Padre_Pendiente_Valoraci_n</fullName>
        <description>Caso Padre Pendiente Valoración</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Approvals/BI_O4_Caso_Padre_Pendiente_Aclaracion</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_NotAsignacion</fullName>
        <description>BI_O4_NotAsignacion</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Casos/BI_O4_Notificaciondeasignacion</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_NotResuelto</fullName>
        <description>BI_O4_NotResuelto</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Casos/BI_O4_Notificacionderesolucion</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_PE_CambioPropietario</fullName>
        <description>BI_O4_PE_CambioPropietario</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Casos/BI_O4_PE_CambioPropietario</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_PE_CasoPadreResuelto</fullName>
        <description>BI_O4_PE_CasoPadreResuelto</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Casos/BI_O4_PE_CasoResueltoPadre</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_PE_Creado</fullName>
        <description>BI_O4_PE_Creado</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Casos/BI_O4_PE_Creado</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_PE_Maintopicsrechazados</fullName>
        <description>BI_O4_PE_Maintopicsrechazados</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Casos/BI_O4_PE_Maintopicsrechazados</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_At_emp_cancelado</fullName>
        <description>BI_PER_At_emp_cancelado</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_PER_At_emp_cancelado</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_At_emp_cerrado</fullName>
        <description>BI_PER_At_emp_cerrado</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_PER_At_emp_cerrado</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_At_emp_resuelto</fullName>
        <description>BI_PER_At_emp_resuelto</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_PER_At_emp_resuelto</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_creacion_autorizada</fullName>
        <description>BI_PER_creacion_autorizada</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_PER_Creacion_autorizada</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_creacion_no_autorizada</fullName>
        <description>BI_PER_creacion_no_autorizada</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_PER_Creacion_no_autorizada</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Enviar_Alerta</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <ccEmails>alvars-d@hotmail.com</ccEmails>
        <description>Enviar Alerta</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_CANCELADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Estado_AprobadoDirector</fullName>
        <ccEmails>geralmontes@homail.com</ccEmails>
        <description>Estado Aprobado Director</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Estado_AprobadoGerente</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <description>Estado Aprobado Gerente</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Estado_AprobadoGestion</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <description>Estado Aprobado Gestion</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Estado_AprobadoJefe</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <description>Estado Aprobado Jefe</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Estado_AprobadoPricing</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <description>Estado Aprobado Pricing</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Estado_Aprobado_PricingInicial</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <description>Estado Aprobado Pricing Inicial</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Estado_RechazadoDirector</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <description>Estado Rechazado Director</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Estado_RechazadoGerente</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <description>Estado Rechazado Gerente</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Estado_RechazadoGestion</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <description>Estado Rechazado Gestion</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Estado_RechazadoJefe</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <description>Estado Rechazado Jefe</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Estado_RechazadoPricing</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <description>Estado Rechazado Pricing</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_SUB_Estado_Rechazado_PricingInicial</fullName>
        <ccEmails>geralmontes@hotmail.com</ccEmails>
        <description>Estado Rechazado Pricing Inicial</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_SUB_ESTADO_SUBSIDIO</template>
    </alerts>
    <alerts>
        <fullName>BI_eHelp_Alerta_cuando_el_Caso_esta_Cerrado</fullName>
        <description>eHelp Alerta cuando el Caso esta Cerrado</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_Caso_eHelp_Caso_Cerrado</template>
    </alerts>
    <alerts>
        <fullName>Caso_eHelp_Actualizar_al_UsuarioFinal</fullName>
        <description>Caso eHelp - Mandar correo al Usuario Final</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_Caso_eHelp_Comunicaci_n_Interna</template>
    </alerts>
    <alerts>
        <fullName>Comunicar_ejecutivo_de_cobros_nuevo_caso_tipo_reclamo</fullName>
        <description>Comunica un nuevo caso tipo reclamo al ejecutivo de cobros para que lo tramite en caso de que fuera mecesario</description>
        <protected>false</protected>
        <recipients>
            <recipient>BI_Ejecutivo_de_cobros</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_comunicar_ejecutivo_de_cobros_creacion_de_un_nuevo_caso_tipo_reclamo</template>
    </alerts>
    <alerts>
        <fullName>PER_eHelp_Alerta_cuando_el_Caso_esta_Cerrado</fullName>
        <description>PER eHelp Alerta cuando el Caso esta Cerrado</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_Caso_eHelp_Caso_Cerrado</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_for_template_4_1_when_case_is_asi</fullName>
        <description>TGS Send an email for template 4.1 when new case is received in the queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>GSM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Quality Responsible</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Vendor Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_4_1_Case_Assigned_Order</template>
    </alerts>
    <alerts>
        <fullName>TGS_1_1_Case_Creation</fullName>
        <description>TGS 1.1. Case Creation</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_1_1_Case_Creation</template>
    </alerts>
    <alerts>
        <fullName>TGS_1_2_Case_Creation_On_Behalf</fullName>
        <description>TGS 1.2. Case Creation (On Behalf)</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_1_2_Case_Creation_On_Behalf</template>
    </alerts>
    <alerts>
        <fullName>TGS_2_1_Case_Status_Changed</fullName>
        <description>TGS 2.1. Case Status Changed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_2_1_Case_Status_Changed</template>
    </alerts>
    <alerts>
        <fullName>TGS_2_1_Case_Status_Changed_Reopen</fullName>
        <description>TGS 2.1. Case Status Changed Reopen</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_2_1_c_Case_Status_Changed_Reopen</template>
    </alerts>
    <alerts>
        <fullName>TGS_2_2_Case_Reopen_On_Behalf</fullName>
        <description>TGS 2.2. Case Reopen (On Behalf)</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_2_2_b_Case_Status_Changed_Reopen_On_Behalf</template>
    </alerts>
    <alerts>
        <fullName>TGS_2_2_Case_Status_Changed_On_Behalf</fullName>
        <description>TGS 2.2. Case Status Changed (On Behalf)</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_2_2_Case_Status_Changed_On_Behalf</template>
    </alerts>
    <alerts>
        <fullName>TGS_4_1_Case_Creation</fullName>
        <description>TGS 4.1. Case Creation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_4_1_Case_Assigned_Order</template>
    </alerts>
    <alerts>
        <fullName>TGS_4_2_Case_Creation</fullName>
        <description>TGS 4.2. Case Creation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_4_2_Case_Assigned_Commercial</template>
    </alerts>
    <alerts>
        <fullName>TGS_5_1_Case_Status_Changed_Agent</fullName>
        <description>TGS 5.1. Case Status Changed (Agent)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_5_1_Case_Status_Changed_Agent</template>
    </alerts>
    <alerts>
        <fullName>TGS_InProgress_Rejected_Closed_Resolved_or_Cancelled</fullName>
        <description>TGS Send an email for template 5.1b when status In Progress, Rejected, Closed, Resolved or Cancelled</description>
        <protected>false</protected>
        <recipients>
            <recipient>integrationuser@telefonica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_5_1b_Case_Status_Changed_Agent_Commercial</template>
    </alerts>
    <alerts>
        <fullName>TGS_InProgress_tier3_Rating</fullName>
        <description>TGS Send an email for template 5.1b when status in progress and tier3 Rating</description>
        <protected>false</protected>
        <recipients>
            <recipient>TGS_Financial_Control_Queue</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_5_1b_Case_Status_Changed_Agent_Commercial</template>
    </alerts>
    <alerts>
        <fullName>TGS_Incident_Created_Email_Alert</fullName>
        <description>TGS Incident Created Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>daniel.leal.ortiz@everis.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/TGS_Notificacion_Cliente_Case_Open</template>
    </alerts>
    <alerts>
        <fullName>TGS_Incident_Resolved_Email_Alert</fullName>
        <description>TGS Incident Resolved Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>daniel.leal.ortiz@everis.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/TGS_Notificacion_Cliente_Case_Resolved</template>
    </alerts>
    <alerts>
        <fullName>TGS_Incident_Updated_Email_Alert</fullName>
        <description>TGS Incident Updated Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>daniel.leal.ortiz@everis.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/TGS_Notificacion_Cliente_Case_Update</template>
    </alerts>
    <alerts>
        <fullName>TGS_Send_an_email_for_template_4_2_Case_Assigned_GSM_Quality_Responsible_V</fullName>
        <description>TGS Send an email for template 4.2.Case Assigned - GSM-Quality Responsible-Vendor Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>integrationuser@telefonica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_4_2_Case_Assigned_Commercial</template>
    </alerts>
    <alerts>
        <fullName>TGS_Send_an_email_for_template_5_1b_Case_Status_Changed_Agent_Commercial</fullName>
        <description>TGS Send an email for template 5.1b. Case Status Changed (Agent - Commercial)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_5_1b_Case_Status_Changed_Agent_Commercial</template>
    </alerts>
    <alerts>
        <fullName>TGS_Survey_Notification_Mail</fullName>
        <description>TGS Survey Notification Mail</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_Cases_Survey</template>
    </alerts>
    <alerts>
        <fullName>TGS_Tier3_Discounts_Status_InProgress</fullName>
        <description>TGS Send an email for template 5.1b when Tier3 Discounts and Status In Progress</description>
        <protected>false</protected>
        <recipients>
            <recipient>GAM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_5_1b_Case_Status_Changed_Agent_Commercial</template>
    </alerts>
    <alerts>
        <fullName>TGS_tier3_rating_charges</fullName>
        <description>TGS Send an email for template 5.1b when tier3 rating and charges</description>
        <protected>false</protected>
        <recipients>
            <recipient>GAM</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_5_1b_Case_Status_Changed_Agent_Commercial</template>
    </alerts>
    <fieldUpdates>
        <fullName>Actualizar_descripcion</fullName>
        <field>Description</field>
        <formula>&quot;Cancelado por vencimiento de fecha limite&quot;</formula>
        <name>Actualizar descripción</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apagar_el_Caso_Actualizado_de_eHelp</fullName>
        <description>Cuando se envia un correo se apaga el flag de caso actualizado</description>
        <field>BI_Caso_Actualizado__c</field>
        <literalValue>0</literalValue>
        <name>Apagar el Caso Actualizado de eHelp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Asignar_caso_eHelp</fullName>
        <description>Asignar caso eHelp por primera vez</description>
        <field>OwnerId</field>
        <lookupValue>BI_Nivel_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Asignar caso eHelp</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI2_Actualizacion_Semaforo_SLA_Amarillo</fullName>
        <field>TGS_SLA_Light__c</field>
        <literalValue>Yellow</literalValue>
        <name>Actualización Semáforo SLA Amarillo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI2_Actualizacion_Semaforo_SLA_Rojo</fullName>
        <field>TGS_SLA_Light__c</field>
        <literalValue>Red</literalValue>
        <name>Actualización Semáforo SLA Rojo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI2_Actualizacion_Semaforo_SLA_Verde</fullName>
        <field>TGS_SLA_Light__c</field>
        <literalValue>Green</literalValue>
        <name>Actualización Semáforo SLA Verde</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI2_Actualizar_Fecha_de_resolucion</fullName>
        <field>BI2_fecha_hora_de_resolucion__c</field>
        <formula>NOW()</formula>
        <name>BI2_Actualizar_Fecha_de_resolucion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI2_COL_Actualiza_semaforo_SLA_Amarillo</fullName>
        <description>BI2_COL_Actualiza_semaforo_SLA_Amarillo</description>
        <field>TGS_SLA_Light__c</field>
        <literalValue>Yellow</literalValue>
        <name>Actualización de semáforo SLA Amarillo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI2_COL_Actualiza_semaforo_SLA_Rojo</fullName>
        <description>BI2_COL_Actualiza_semaforo_SLA_Rojo</description>
        <field>TGS_SLA_Light__c</field>
        <literalValue>Red</literalValue>
        <name>Actualización de semáforo SLA Rojo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI2_IsStopped_false</fullName>
        <description>Actualiza el campo IsStopped a False</description>
        <field>IsStopped</field>
        <literalValue>0</literalValue>
        <name>BI2_IsStopped_false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI2_IsStopped_true</fullName>
        <description>Actualiza el campo IsStopped a True</description>
        <field>IsStopped</field>
        <literalValue>1</literalValue>
        <name>BI2_IsStopped_true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_ARG_Asignacion_soporte_subsidio</fullName>
        <field>OwnerId</field>
        <lookupValue>BI_Soporte_Subsidios</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI ARG Asignacion soporte subsidio</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_ARG_Asignar_cola_Subtipo_SSDD_CON_Equ</fullName>
        <description>se creo para actualizar caso al seleccionar un subtipo de solicitud SSDD CON Equipo</description>
        <field>OwnerId</field>
        <lookupValue>BI_ARG_Altas_Iniciales_MAVE</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_ARG_Asignar_cola_Subtipo_SSDD_CON_Equ</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_ARG_Asignar_cola_Subtipo_SSDD_SIN_Equ</fullName>
        <description>se creo para actualizar caso al seleccionar un subtipo de solicitud SSDD SIN Equipo</description>
        <field>OwnerId</field>
        <lookupValue>BI_ARG_Soporte_Tecnico_SSDD</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_ARG_Asignar_cola_Subtipo_SSDD_SIN_Equ</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_ARG_Subtipo_no_previsionable</fullName>
        <field>BI_Subtype__c</field>
        <literalValue>No Previsionable</literalValue>
        <name>BI_ARG_Subtipo_no_previsionable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_ARG_Subtipo_previsionable</fullName>
        <field>BI_Subtype__c</field>
        <literalValue>Previsionable</literalValue>
        <name>BI_ARG_Subtipo_previsionable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_AccreditationRequest_ChangeOwner</fullName>
        <field>OwnerId</field>
        <lookupValue>BI_O4_Sales_Operations</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_AccreditationRequest_ChangeOwner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_AccreditationRequest_ChangeStatus</fullName>
        <field>Status</field>
        <literalValue>Assigned</literalValue>
        <name>BI_AccreditationRequest_ChangeStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_fecha_fin_actividad</fullName>
        <description>Creada por la Demanda 270</description>
        <field>BI_Fecha_fin_de_actividad__c</field>
        <formula>NOW()</formula>
        <name>Actualiza fecha fin actividad</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_fecha_respuesta</fullName>
        <description>Creada por la Demanda 270</description>
        <field>BI_Fecha_de_recepcion__c</field>
        <formula>NOW()</formula>
        <name>Actualiza fecha respuesta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_BJ_Actualizar_estado_caso</fullName>
        <field>Status</field>
        <literalValue>Vencido</literalValue>
        <name>BI_BJ_Actualizar_estado_caso</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_CHI_Asignacion_Minifront</fullName>
        <field>OwnerId</field>
        <lookupValue>BI_Soporte_Minifront</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI CHI Asignacion Minifront</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_E2C_ARG_Cobranzas_Campo_Categor_a</fullName>
        <description>Field Update que rellena el campo &quot;Categoría&quot; de los Casos Comerciales creados por Email To Case para ARG - Cobranzas con el valor de &quot;Nivel 1&quot;.</description>
        <field>BI_Categoria_del_caso__c</field>
        <literalValue>Nivel 1</literalValue>
        <name>BI E2C ARG - Cobranzas - Campo Categoría</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_E2C_ARG_Cobranzas_Campo_Motivo</fullName>
        <description>Field Update que rellena el campo &quot;Motivo del contacto&quot; de los Casos Comerciales creados por Email To Case para ARG - Cobranzas con el valor de &quot;Consulta&quot;.</description>
        <field>Reason</field>
        <literalValue>Administrative Request</literalValue>
        <name>BI E2C ARG - Cobranzas - Campo Motivo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_E2C_ARG_Cobranzas_Campo_Pa_s</fullName>
        <description>Field Update que rellena el campo &quot;País&quot; de los Casos Comerciales creados por Email To Case para ARG - Cobranzas con el valor &quot;Argentina&quot;.</description>
        <field>BI_Country__c</field>
        <literalValue>Argentina</literalValue>
        <name>BI E2C ARG - Cobranzas - Campo País</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_E2C_ARG_Cobranzas_Campo_Tipo</fullName>
        <description>Field Update que rellena el campo &quot;Tipo&quot; de los Casos Comerciales creados por Email To Case para ARG - Cobranzas con el valor de &quot;Consulta&quot;.</description>
        <field>Type</field>
        <literalValue>Question</literalValue>
        <name>BI E2C ARG - Cobranzas - Campo Tipo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Asig_caso_Soluciones_Seguridad</fullName>
        <description>BI MEX Asig caso Soluciones Seguridad</description>
        <field>OwnerId</field>
        <lookupValue>BI_MEX_Soluciones_Seguridad</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI MEX Asig caso Soluciones Seguridad</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Asignacion_Casos_Recaudacion</fullName>
        <description>Ehelp 03066111</description>
        <field>OwnerId</field>
        <lookupValue>BI_MEX_Casos_Internos_Recaudacion</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_MEX_Asignacion_Casos_Recaudacion</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Asignacion_caso_Gesti_n_Adm</fullName>
        <description>Actualizar campo propietario de casos MEX de Gestión Administrativa.</description>
        <field>OwnerId</field>
        <lookupValue>BI_MEX_Casos_gestion_administrativa</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_MEX_Asignacion_caso_Gestión_Adm</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Asignacion_caso_Mesa_Control</fullName>
        <description>Caso eHelp 194807. Se actualiza automáticamente el propietario de casos internos de Mesa de Control a un usuario.</description>
        <field>OwnerId</field>
        <lookupValue>BI_MEX_Casos_Mesa_de_control</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_MEX_Asignacion_caso_Mesa_Control</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Asignacion_caso_Order_Entry</fullName>
        <description>Caso eHelp 194807. Actualizar propietario de casos de Order Entry a una cola.</description>
        <field>OwnerId</field>
        <lookupValue>BI_MEX_Order_Entry</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_MEX_Asignacion_caso_Order_Entry</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Asignacion_caso_Preventa</fullName>
        <description>Se asigna el caso de Departamento Preventa y Tipo Solicitud Solicitar Propuesta a la cola Preventa Empresas</description>
        <field>OwnerId</field>
        <lookupValue>BI_MEX_Preventa_Empresas</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_MEX_Asignacion_caso_Preventa</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Asignacion_caso_Pricing_Fijo</fullName>
        <description>Caso eHelp 194807. Actualizar propietario de caso de Pricing Fijo MEX a un usuario.</description>
        <field>OwnerId</field>
        <lookupValue>lorena.alonso@telefonica.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>BI_MEX_Asignacion_caso_Pricing_Fijo</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Asignacion_caso_Soluciones_Cloud</fullName>
        <field>OwnerId</field>
        <lookupValue>BI_MEX_Soluciones_Cloud</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI MEX Asignación caso Soluciones Cloud</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Asignacion_caso_Soluciones_M2M</fullName>
        <description>BI MEX Asignación caso Soluciones M2M</description>
        <field>OwnerId</field>
        <lookupValue>BI_MEX_Soluciones_M2M</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI MEX Asignación caso Soluciones M2M</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Asignacion_casos_Implantacion</fullName>
        <field>OwnerId</field>
        <lookupValue>MEX_Implantacion_de_Proyectos</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_MEX_Asignacion_casos_Implantacion</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Asignacion_casos_Renovaciones</fullName>
        <description>Solicitado en ehelp 01856601</description>
        <field>OwnerId</field>
        <lookupValue>BI_MEX_Renovaciones</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_MEX_Asignacion_casos_Renovaciones</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Cola_PreventaXdpto_ing_preventa</fullName>
        <field>OwnerId</field>
        <lookupValue>BI_MEX_Preventa_Empresas</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_MEX_Cola_PreventaXdpto_ing_preventa</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Posventa_Refacturacion</fullName>
        <description>ehelp 03066111</description>
        <field>OwnerId</field>
        <lookupValue>BI_MEX_Casos_Interno_Postventa</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_MEX_Posventa_Refacturación</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Actualiza_Closed_Type_1</fullName>
        <description>Actualiza el valor del campo Closed Type a “Unquoted”</description>
        <field>BI_O4_Closed_Type__c</field>
        <literalValue>Unquoted</literalValue>
        <name>Actualiza Closed Type 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Actualiza_Closed_Type_2</fullName>
        <description>Actualiza el valor del campo Closed Type a “Quoted on time”</description>
        <field>BI_O4_Closed_Type__c</field>
        <literalValue>On Time</literalValue>
        <name>Actualiza Closed Type 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Actualiza_Closed_Type_3</fullName>
        <description>Actualiza el valor del campo Closed Type a “Quoted out of time”</description>
        <field>BI_O4_Closed_Type__c</field>
        <literalValue>Out of Time</literalValue>
        <name>Actualiza Closed Type 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Actualiza_Fecha_de_Cotizaci_n</fullName>
        <description>El campo Fecha de Cotización se informará con la fecha del día en que el caso haya pasado de Pendiente Cotización a Cotizada</description>
        <field>BI_O4_Fecha_de_Cotizaci_n__c</field>
        <formula>TODAY ()</formula>
        <name>Actualiza Fecha de Cotización</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Asignado</fullName>
        <field>Status</field>
        <literalValue>Assigned</literalValue>
        <name>BI_O4_Asignado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_CompetenceCenter</fullName>
        <field>OwnerId</field>
        <lookupValue>BI_O4_Competence_Center</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_O4_CompetenceCenter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_PE_GFSO</fullName>
        <field>OwnerId</field>
        <lookupValue>BI_O4_GFSO</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>BI_O4_PE_GFSO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Status_hijo</fullName>
        <description>Estado del caso hijo pasa a &apos;Cotizada&apos;</description>
        <field>Status</field>
        <literalValue>Cotizada</literalValue>
        <name>Status_hijo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Tipo_de_registro</fullName>
        <description>Llena el campo tipo de registro AUX</description>
        <field>BI_O4_Tipo_de_Registro_Aux__c</field>
        <formula>RecordType.DeveloperName</formula>
        <name>Tipo de registro</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Update_GenericoId</fullName>
        <field>BI_O4_Gen_rico_Preventa_ID__c</field>
        <formula>&apos;A-&apos; + CreatedBy.BI_O4_Presales_Unit__c + &apos;-&apos; + Account.TGS_Account_Mnemonic__c + &apos;-GEN-&apos; + CaseNumber</formula>
        <name>BI_O4_Update_GenericoId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Update_Subject</fullName>
        <description>Actualizar asunto</description>
        <field>Subject</field>
        <formula>&apos;A-&apos; + CreatedBy.BI_O4_Presales_Unit__c + &apos;-&apos; + Account.TGS_Account_Mnemonic__c + &apos;-GEN-&apos; + CaseNumber</formula>
        <name>BI_O4_Update_Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_Actualizar_Estado</fullName>
        <field>Status</field>
        <literalValue>Cancelled</literalValue>
        <name>Actualizar Estado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_Actualizar_Estado_Payback_Ini</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Pricing Inicial Aprobado</literalValue>
        <name>Actualizar Estado Payback_Ini</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_Actualizar_descripcion</fullName>
        <field>Description</field>
        <formula>&quot;Cancelado por vencimiento de fecha limite&quot;</formula>
        <name>Actualizar descripción</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_AprobadoGestion</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Gestión Aprobado</literalValue>
        <name>Aprobado Gestión</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_Aprobado_Director</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Director Aprobado</literalValue>
        <name>Aprobado Director</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_Aprobado_Gerente</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Gerente Aprobado</literalValue>
        <name>Aprobado Gerente</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_Aprobado_Jefe</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Jefe Aprobado</literalValue>
        <name>Aprobado Jefe</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_Aprobado_Pricing</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Pricing Aprobado</literalValue>
        <name>Aprobado Pricing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_Aprobado_Pricing_Inicial</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Pricing Inicial Aprobado</literalValue>
        <name>Aprobado Pricing Inicial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_Aprobar_Estado</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Aprobar Estado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_Cancelar_Estado</fullName>
        <field>Status</field>
        <literalValue>Cancelado</literalValue>
        <name>Cancelar Estado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_RechazadoDirector</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Director Rechazado</literalValue>
        <name>Rechazado Director</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_RechazadoGerente</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Gerente Rechazado</literalValue>
        <name>Rechazado Gerente</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_RechazadoGestion</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Gestión Rechazado</literalValue>
        <name>Rechazado Gestión</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_RechazadoJefe</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Jefe Rechazado</literalValue>
        <name>Rechazado Jefe</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_RechazadoPricing</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Pricing Rechazado</literalValue>
        <name>Rechazado Pricing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_SUB_Rechazado_PricingInicial</fullName>
        <field>BI_SUB_Status_Approval_Payback__c</field>
        <literalValue>Pricing Inicial Rechazado</literalValue>
        <name>Rechazado Pricing Inicial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Update_status_en_analisis</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>BI Update status en analisis</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Update_status_inprogress</fullName>
        <field>BI_Fecha_inicio_solucion_caso__c</field>
        <formula>NOW()</formula>
        <name>BI Update status in progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_eHelp_Actualizar_Razon_para_Cerrar</fullName>
        <field>Reason</field>
        <literalValue>Request fulfilled</literalValue>
        <name>BI eHelp Actualizar Razon para Cerrar</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_eHelp_Actualizar_Solucion_del_Caso</fullName>
        <description>Cuando el caso se cierra Automaticamente hay que Actualizar el Caso</description>
        <field>BI_Solucion_del_caso__c</field>
        <formula>BI_Solucion_del_caso__c &amp;&quot; Caso cerrado automaticamente por estar mas de 5 dias en estado &apos;Ptd de Confirmacion&apos; &quot;</formula>
        <name>BI eHelp Actualizar Solucion del Caso</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_eHelp_Cerrar_Caso_Auto</fullName>
        <description>Caso se cierra automatincamente</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>BI eHelp Cerrar Caso Auto</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_fill_fecha_recepcion</fullName>
        <field>BI_Fecha_de_recepcion__c</field>
        <formula>NOW()</formula>
        <name>BI fill fecha recepcion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Empty_Assigne</fullName>
        <field>TGS_Technical_Assignee__c</field>
        <name>Empty Assignee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pte_Informacion</fullName>
        <field>BI_O4_Pte_Informacion__c</field>
        <literalValue>1</literalValue>
        <name>Pte Información</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGSTest_L2_Ordering_Mobile</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Test_Acme_SD_Level_2</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS_Test_L2_Ordering_Mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_ActualizaCasillaDesarrollo</fullName>
        <field>TGS_Casilla_Desarrollo__c</field>
        <literalValue>0</literalValue>
        <name>TGS_ActualizaCasillaDesarrollo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Actualiza_responsable</fullName>
        <description>Actualiza el campo responsable con el nombre del usuario que realizó la acción.

Creado por la Demanda 288</description>
        <field>TGS_Responsable__c</field>
        <formula>$User.FirstName + &apos; &apos; + $User.LastName</formula>
        <name>Actualiza responsable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_CWP_Accept_Cancellation</fullName>
        <field>TGS_CWP_Accept_Cancellation__c</field>
        <literalValue>0</literalValue>
        <name>CWP_Accept_Cancellation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_Assign_SMC_Madrid_L1</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_SMC_Madrid_L1_Ordering_Mobile</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case Assign SMC Madrid L1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_Assign_SMC_Madrid_L2</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_SMC_Madrid_L2_Ordering_Mobile</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case Assign SMC Madrid L2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_Assign_to_Freephone</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Freephone</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case Assign to Freephone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_SLA_Start</fullName>
        <description>Desactiva el campo Stopped para seguir contando el SLA</description>
        <field>IsStopped</field>
        <literalValue>0</literalValue>
        <name>TGS_Case_SLA_Start</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_SLA_Stop</fullName>
        <description>Activa el campo Stopped del caso para dejar de contar los SLAs</description>
        <field>IsStopped</field>
        <literalValue>1</literalValue>
        <name>TGS Case SLA Stop</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_assign_to_CGP_CEMEX</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_CGP_CEMEX</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case assign to CGP CEMEX</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_assign_to_Demo_L1_Ordering</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Test_Demo_L1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case assign to Demo L1 Ordering</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_assign_to_Demo_L2_Ordering</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Test_Demo_L2</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case assign to Demo L2 Ordering</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_assign_to_EMEA_L1_Ordering</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_SMC_EMEA_L1_Ordering</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case assign to EMEA L1 Ordering</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_assign_to_Multiservice_Provisio</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Multiservice_Provision_Controller_TE</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case assign to Multiservice Provisio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_assign_to_Multiservice_Purchase</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Multiservice_Purchase_TEST</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case assign to Multiservice Purchase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_assign_to_Order_Handling</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Order_Handling</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case assign to Order Handling</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_assign_to_Provision_Siptrunk</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Provision_Siptrunk_TEST</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case assign to Provision Siptrunk</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_assign_to_SMC_EMEA_L1_Ordering</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_SMC_EMEA_L1_Ordering</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case assign to SMC EMEA L1 Ordering</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_assign_to_SMC_Miami_L1_Ord_GA</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_SMC_Miami_L1_Ordering_GA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case assign to SMC Miami L1 Ord GA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Case_assign_to_SipNOC</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_SipNOC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Case assign to SipNOC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_CasetoUDOCheck</fullName>
        <field>TGS_UDO_Update__c</field>
        <literalValue>1</literalValue>
        <name>TGS_CasetoUDOCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Empty_Assignee</fullName>
        <field>TGS_Assignee__c</field>
        <name>Empty Assignee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Get_Case_Contact_Email</fullName>
        <field>TGS_Case_Contact_Email__c</field>
        <formula>Contact.Email</formula>
        <name>TGS Get Case Contact Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Get_Case_Owner_Email</fullName>
        <field>TGS_Case_Owner_Email__c</field>
        <formula>Owner:Queue.QueueEmail</formula>
        <name>TGS Get Case Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_CEDEX_CC</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_CedEx_CC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: CEDEX CC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_CEDEX_CC_Network_Configuratio</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_CEDEX_CC_Network_Configuration</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: CEDEX CC Network Configuratio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_CEDEX_CC_Resources_Allocation</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_CEDEX_CC_Resources_Allocation</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: CEDEX CC Resources Allocation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Financial_Control</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Financial_Control_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: Financial Control</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Financial_Control_Test</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Financial_Control_Test</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: Financial Control Test</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Presales</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Presales_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: Presales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Provision_VPN</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Provision_VPN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner Provision VPN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_SMC_L1_Ordering_APAC_EMEA</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_SMC_APAC_EMEA_L1_Ordering_Mobile</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: SMC L1 Ordering APAC+EMEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_SMC_L1_Ordering_Miami</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_SMC_Miami_L1_Ordering_Mobile</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: SMC L1 Ordering Miami</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_SMC_L2_Ordering_EMEA</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_SMC_EMEA_L2_Ordering_Mobile</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: SMC L2 Ordering EMEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_SMC_L2_Ordering_Miami</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_SMC_Miami_L2_Ordering_Mobile</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: SMC L2 Ordering Miami</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_SMC_Miami_L1_Ordering_NSN_Ope</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_SMC_Miami_L1_Ordering_NSN_Operators</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: SMC Miami L1 Ordering NSN Ope</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_SMC_Miami_L1_Ordering_NSN_Voi</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_SMC_Miami_L1_Ordering_NSN_Voice</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: SMC Miami L1 Ordering NSN Voi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Test_Acme_SD_Level_1</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Test_ACME_SD_Level_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: Test Acme SD Level 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Test_CEDEX_CC</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_TEST_CEDEX_CC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: Test CEDEX CC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Test_CEDEX_CC_Network_Configu</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Test_CEDEX_CC_Network_Configuration</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: Test CEDEX CC Network Configu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Test_Multiservice_Provision_Co</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Test_Multiservice_Provision_Controll</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner Test Multiservice Provision Co</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Test_Multiservice_Purchase</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Test_MultiService_Purchase</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: Test Multiservice Purchase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Test_Order_Handling</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Test_Order_Handling</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner Test Order Handling</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Test_Provision_Siptrunk</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Test_Provision_Siptrunk</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: Test Provision Siptrunk</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Test_Provision_VPN</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Test_Provision_VPN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner Test Provision VPN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_Test_SIP_NOC</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Test_SIP_NOC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner Test SIP NOC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Owner_test_CEDEX_CC_Resources_Alloc</fullName>
        <field>OwnerId</field>
        <lookupValue>TGS_Test_CEDEX_CC_Resources_Allocation</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TGS Owner: test CEDEX CC Resources Alloc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_RFB_when_Status_Closed</fullName>
        <field>TGS_RFB_date__c</field>
        <formula>TGS_RFS_date__c</formula>
        <name>TGS RFB when Status=Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_RFS_when_Status_Resolved</fullName>
        <field>TGS_RFS_date__c</field>
        <formula>BLANKVALUE(TGS_RFS_date__c, TODAY())</formula>
        <name>TGS RFS when Status=Resolved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Resolve_Date_Clear</fullName>
        <field>TGS_Resolve_Date__c</field>
        <name>TGS_Resolve_Date_Clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Resolve_Date_Set</fullName>
        <field>TGS_Resolve_Date__c</field>
        <formula>TODAY()</formula>
        <name>TGS_Resolve_Date_Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Stop_Clock_SLA</fullName>
        <field>IsStopped</field>
        <literalValue>1</literalValue>
        <name>TGS Stop Clock SLA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Uncheck_product_configured</fullName>
        <field>TGS_Product_Configured__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck product configured</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Unstop_Clock_SLA</fullName>
        <field>IsStopped</field>
        <literalValue>0</literalValue>
        <name>TGS Unstop Clock SLA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Update_SLA_red</fullName>
        <field>TGS_SLA_Light__c</field>
        <literalValue>Red</literalValue>
        <name>Update SLA red</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Update_SLA_yellow</fullName>
        <field>TGS_SLA_Light__c</field>
        <literalValue>Yellow</literalValue>
        <name>Update SLA yellow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pais_Caso_eHelp</fullName>
        <description>Update with the Account Pais</description>
        <field>Pais_Caso_eHelp__c</field>
        <formula>TEXT(Account.BI_Country__c)</formula>
        <name>Update Pais Caso eHelp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Closed</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Update Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>bi_Actualizar_StatusChangeDate</fullName>
        <field>BI_Fecha_ltimo_cambio_de_estado__c</field>
        <formula>TODAY()</formula>
        <name>Actualizar StatusChangeDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>ARG_Asignacion_colas_por_subtipo_SSDD_SIN_Equipo</fullName>
        <actions>
            <name>BI_ARG_Asignar_cola_Subtipo_SSDD_SIN_Equ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>para asignar casos a cola según subtipo de solicitud</description>
        <formula>AND(ISPICKVAL(BI_Subtype__c, &apos;SSDD SIN Equipo&apos;), RecordType.DeveloperName = &apos;BI_Caso_Interno&apos;,OR(ISPICKVAL(Account.BI_Segment__c, &apos;Negocios&apos;),ISPICKVAL(Account.BI_Segment__c, &apos;Empresas&apos;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ARG_Asignar_Cola_subtipo_SSDD_CON_Equipo</fullName>
        <actions>
            <name>BI_ARG_Asignar_cola_Subtipo_SSDD_CON_Equ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Creada para asignar a una cola los caso que sean del subtipo SSDD CON Equipo</description>
        <formula>AND(ISPICKVAL(BI_Subtype__c, &apos;SSDD CON Equipo&apos;), RecordType.DeveloperName = &apos;BI_Caso_Interno&apos;,ISPICKVAL(Account.BI_Segment__c, &apos;Negocios&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Asignacion Post Venta Colmbia</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.BI_Tipo_de_funcionalidad_afectada__c</field>
            <operation>equals</operation>
            <value>Post Venta Colombia</value>
        </criteriaItems>
        <description>Asignación de casos eHelp al soporte de proyecto</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI ARG Asignacion de Casos Subsidio</fullName>
        <actions>
            <name>BI_ARG_Asignacion_soporte_subsidio</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Tipo_de_funcionalidad_afectada__c</field>
            <operation>equals</operation>
            <value>Gestión de Subsidios ARG</value>
        </criteriaItems>
        <description>Cuando se crear un caso eHelp en Gestión subsidio ARG se asigna a Soporte Subsidio</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI ARG Gestion Adm Cobranzas Email Notification</fullName>
        <actions>
            <name>BI_ARG_Gestion_Adm_Cobranzas_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Regla que ejecuta un Email Alert (enviado al creador del caso) al cerrar un caso del departamento Gestión Administrativa Cobranzas y tipo de solicitud Control y Adm. de Pagos cuando no tiene un caso comercial como principal.</description>
        <formula>AND(
	ISPICKVAL(BI_Department__c,&apos;Gestión Administrativa Cobranzas&apos;),
	Parent.RecordType.Name &lt;&gt; &apos;Caso Comercial&apos;,
	PRIORVALUE(IsClosed) = false, IsClosed = true,
	ISBLANK(BI_Nombre_de_la_Oportunidad__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI Actualiza el campo Pais Caso eHelp</fullName>
        <actions>
            <name>Update_Pais_Caso_eHelp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp</value>
        </criteriaItems>
        <description>Actualiza este campo para las reglas compartidas.  Cada pais puede ver sus casos eHelp.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI Asignacion de Casos eHelp</fullName>
        <actions>
            <name>Asignar_caso_eHelp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Tipo_de_funcionalidad_afectada__c</field>
            <operation>notEqual</operation>
            <value>Ola 4/5,Gestión de Subsidios ARG,Integración Minifront CHI</value>
        </criteriaItems>
        <description>Cuando se crear un caso eHelp en Chatter se asigna a Nivel 2</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI BJ - Cerrar casos pendientes</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pendiente Aceptación Cliente Bajas/Cancelaciones</value>
        </criteriaItems>
        <description>Regla para cerrar casos que estan en Pendiente Aceptacion Cliente durante 15 días</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_BJ_Actualizar_estado_caso</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI CHI Asignacion Minifront</fullName>
        <actions>
            <name>BI_CHI_Asignacion_Minifront</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Tipo_de_funcionalidad_afectada__c</field>
            <operation>equals</operation>
            <value>Integración Minifront CHI</value>
        </criteriaItems>
        <description>Cuando se crear un caso eHelp en Gestión subsidio ARG se asigna a Soporte Subsidio</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI Email To Case ARG - Cobranzas - Notificar Caso Cerrado</fullName>
        <actions>
            <name>BI_Email_To_Case_ARG_Cobranzas_Notificar_Caso_Cerrado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Regla que ejecuta un Email Alert al cerrar el Caso Comercial desde Email To Case para notificárselo al propietario de la Cuenta.</description>
        <formula>AND(  RecordType.DeveloperName = &apos;BI_Caso_Comercial_Abierto&apos;,  OR(  ISPICKVAL(Origin,&apos;Email To Case ARG - Cobranzas&apos;),  ISPICKVAL(Origin,&apos;Caso desde Correo Argentina&apos;)  ),  IsClosed = true,  NOT(ISBLANK(SuppliedEmail))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI Email To Case ARG - Cobranzas - Notificar Caso Nuevo</fullName>
        <actions>
            <name>BI_E2C_ARG_Cobranzas_Notificar_Caso_Nuevo</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Regla que ejecuta un Email Alert al crear el Caso Comercial desde Email To Case para notificar al Contacto (Cliente final) que mandó el correo</description>
        <formula>AND(RecordType.DeveloperName = &apos;BI_Caso_Comercial_Abierto&apos;, 
 OR(ISPICKVAL(Origin,&apos;Caso desde Correo Argentina&apos;), ISPICKVAL(Origin,&apos;Email To Case ARG - Cobranzas&apos;)),
 NOT(ISBLANK(SuppliedEmail)),NOT(ISBLANK(ContactId)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI Email To Case ARG - Cobranzas - Notificar Caso Nuevo Cliente No Existente</fullName>
        <actions>
            <name>BI_E2C_ARG_Cobranzas_Notificar_Caso_Nuevo_Cliente_No_Existente</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Regla que ejecuta un Email Alert al crear el Caso Comercial desde Email To Case para notificar al Contacto (Cliente final) que mandó el correo, no existiendo este Contacto en el sistema, por lo que se utiliza un template genérico.</description>
        <formula>AND(RecordType.DeveloperName = &apos;BI_Caso_Comercial_Abierto&apos;, OR(ISPICKVAL(Origin,&apos;Caso desde Correo Argentina&apos;), ISPICKVAL(Origin,&apos;Email To Case ARG - Cobranzas&apos;)), NOT(ISBLANK(SuppliedEmail)),ISBLANK(ContactId))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI Email To Case ARG - Cobranzas - Rellena campos</fullName>
        <actions>
            <name>BI_E2C_ARG_Cobranzas_Campo_Categor_a</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_E2C_ARG_Cobranzas_Campo_Motivo</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_E2C_ARG_Cobranzas_Campo_Pa_s</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_E2C_ARG_Cobranzas_Campo_Tipo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Regla que ejecuta varios Field Update al crear el Caso Comercial desde Email To Case para rellenar los campos necesarios.</description>
        <formula>AND(RecordType.DeveloperName = &apos;BI_Caso_Comercial_Abierto&apos;, OR(ISPICKVAL(Origin,&apos;Caso desde Correo Argentina&apos;), ISPICKVAL(Origin,&apos;Email To Case ARG - Cobranzas&apos;)),NOT(ISBLANK(SuppliedEmail)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI Envio de email contacto</fullName>
        <actions>
            <name>BI_Envio_de_email_Portal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(Origin, &apos;Portal cliente&apos;), ISCHANGED(Status))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI Envio de email contacto eHelp - Closed Case</fullName>
        <actions>
            <name>BI_eHelp_Alerta_cuando_el_Caso_esta_Cerrado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>Los casos eHelp deben mandar informacion cuando se cierran</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI Envio de email contacto eHelp - Primera Vez</fullName>
        <actions>
            <name>BI_Envio_de_Correo_Caso_eHelp</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp</value>
        </criteriaItems>
        <description>Los casos eHelp deben mandar informacion cuando se crean por primera vez y cuando se actualizan con un cambio.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI Envio de email contacto eHelp - Seguimiento</fullName>
        <actions>
            <name>Caso_eHelp_Actualizar_al_UsuarioFinal</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Apagar_el_Caso_Actualizado_de_eHelp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Caso_Actualizado__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>Los casos eHelp deben mandar informacion cuando se actualizan con un cambio.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI PER Envio de email contacto eHelp - Closed Case</fullName>
        <actions>
            <name>PER_eHelp_Alerta_cuando_el_Caso_esta_Cerrado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Pais_Caso_eHelp__c</field>
            <operation>equals</operation>
            <value>Peru</value>
        </criteriaItems>
        <description>Los casos eHelp deben mandar informacion cuando se cierran. En el caso de PER avisar no solo al propietario del caso si no también al que lo creó.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI eHelp - Cerrar Casos Pendientes</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Confirmation pending</value>
        </criteriaItems>
        <description>Regla para cerrar casos que estan en pendientes de Confirmacion por mas de 5 dias.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_eHelp_Actualizar_Razon_para_Cerrar</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>BI_eHelp_Actualizar_Solucion_del_Caso</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>BI_eHelp_Cerrar_Caso_Auto</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI2_Actualizar_Fecha_de_resolucion</fullName>
        <actions>
            <name>BI2_Actualizar_Fecha_de_resolucion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(Status), ISPICKVAL(Status, &quot;Resolved&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI2_COL_Envio_encuesta_cliente</fullName>
        <actions>
            <name>BI2_COL_Alerta_Notificacion_Encuesta</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.BI2_enviar_encuesta__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>Flujo de trabajo para enviar correo con link encuesta al cliente, siempre que se seleccione el campo Enviar encuesta.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI2_COL_Notificacion CUN casos comerciales</fullName>
        <actions>
            <name>BI2_COL_Notificacion_CUN_Caso_comercial_2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notifica al cliente la asignación del CUN en casos comerciales 2 para Colombia</description>
        <formula>IF(ISNEW(), NOT(ISBLANK(BI_COL_Codigo_CUN__c)),ISCHANGED(BI_COL_Codigo_CUN__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI2_Caso_Cancelado</fullName>
        <actions>
            <name>BI2_Alerta_Notificacion_Caso_Cancelado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Evalua si el estado del caso es Cancelado</description>
        <formula>AND(   AccountId &lt;&gt; null,  ContactId &lt;&gt; null,  !$Setup.BI_bypass__c.BI_migration__c, OR(   ISPICKVAL(Status, &apos;Cancelado&apos;),   ISPICKVAL(Status, &apos;Cancelled&apos;)   ),   OR(  RecordType.DeveloperName = &apos;BI2_caso_comercial&apos;,   RecordType.DeveloperName = &apos;BI2_Caso_Padre&apos; , RecordType.DeveloperName = &apos;BIIN_Solicitud_Incidencia_Tecnica&apos;, RecordType.DeveloperName = &apos;BIIN_Incidencia_Tecnica&apos; ), $Profile.Name != &apos;BI_Standard_COL&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI2_Caso_Cerrado</fullName>
        <actions>
            <name>BI2_Alerta_Notificacion_Caso_Cerrado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Evalua si el estado del caso es Cerrado</description>
        <formula>AND(   AccountId &lt;&gt; null,   ContactId &lt;&gt; null,   !$Setup.BI_bypass__c.BI_migration__c, OR(   ISPICKVAL(Status, &apos;Closed&apos;),   ISPICKVAL(Status, &apos;Cerrado&apos;)   ),   OR(   RecordType.DeveloperName = &apos;BI2_caso_comercial&apos;,   RecordType.DeveloperName = &apos;BI2_Caso_Padre&apos; , RecordType.DeveloperName = &apos;BIIN_Solicitud_Incidencia_Tecnica&apos;, RecordType.DeveloperName = &apos;BIIN_Incidencia_Tecnica&apos; ), $Profile.Name != &apos;BI_Standard_COL&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI2_Caso_PendienteResuelto_Detenido_false</fullName>
        <actions>
            <name>BI2_IsStopped_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Verifica el valor del campo Estado y si está Pendiente o Resuelto, convierte el valor del campo Detenido (IsStopped) en False</description>
        <formula>AND(  AccountId &lt;&gt; null, ContactId &lt;&gt; null, ISCHANGED(Status),   !$Setup.BI_bypass__c.BI_migration__c, IsStopped = true, NOT(ISPICKVAL(Status, &apos;Pending&apos;)), NOT(ISPICKVAL(Status, &apos;Pendiente&apos;)), NOT(ISPICKVAL(Status, &apos;Resolved&apos;)), NOT(ISPICKVAL(Status, &apos;Resuelto&apos;)), OR( RecordType.DeveloperName = &apos;BI2_caso_comercial&apos;, RecordType.DeveloperName = &apos;BI2_Caso_Padre&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI2_Caso_PendienteResuelto_Detenido_true</fullName>
        <actions>
            <name>BI2_IsStopped_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Verifica el valor del campo Estado y si está Pendiente o Resuelto, convierte el valor del campo Detenido (IsStopped) en True</description>
        <formula>AND(  AccountId &lt;&gt; null, ContactId &lt;&gt; null, ISCHANGED(Status),   !$Setup.BI_bypass__c.BI_migration__c, IsStopped = false, OR ( ISPICKVAL(Status, &apos;Pending&apos;), ISPICKVAL(Status, &apos;Pendiente&apos;), ISPICKVAL(Status, &apos;Resolved&apos;), ISPICKVAL(Status, &apos;Resuelto&apos;) ), OR( RecordType.DeveloperName = &apos;BI2_caso_comercial&apos;, RecordType.DeveloperName = &apos;BI2_Caso_Padre&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI2_Caso_Pendiente_emails</fullName>
        <actions>
            <name>BI2_Alerta_NotificacionCliente_Caso_Pendiente</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BI2_Alerta_Notificacion_Caso_Pendiente</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Cuando el Estado del Caso pasa a Pendiente, se envía un email al Cliente y al Asesor Advisor notificando el cambio de estado.</description>
        <formula>AND(  AccountId &lt;&gt; null, ContactId &lt;&gt; null, ISCHANGED( Status ),   !$Setup.BI_bypass__c.BI_migration__c, OR ( ISPICKVAL(Status, &apos;Pending&apos;), ISPICKVAL(Status, &apos;Pendiente&apos;) ), OR( RecordType.DeveloperName = &apos;BI2_caso_comercial&apos;, RecordType.DeveloperName = &apos;BI2_Caso_Padre&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_ARG_CasoCreado_Gestion_de_Cancelacion</fullName>
        <actions>
            <name>BI_ARG_Gestion_de_Cancelacion</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( BI_Nombre_de_la_Oportunidad__r.OwnerId &lt;&gt; OwnerId, ISPICKVAL(BI_Type__c,&apos;Gestión de Cancelación&apos;)    )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_ARG_NotificaHijoCancelado</fullName>
        <actions>
            <name>BI_ARG_NotificarOwnerCancelarCaso</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Demanda 586</description>
        <formula>And(  ISPICKVAL(Parent.BI_Type__c ,&apos;Gestión de Cancelación&apos; ), ISPICKVAL(Parent.Status,&apos;Closed&apos; ), ISPICKVAL(Status ,&apos;Cancelled&apos; )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_ARG_Subtipo_no_previsionable</fullName>
        <actions>
            <name>BI_ARG_Subtipo_no_previsionable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso Interno</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Department__c</field>
            <operation>equals</operation>
            <value>Gestión Reclamos de Facturación</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Type__c</field>
            <operation>equals</operation>
            <value>Diferencia de Cambio rechazada</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Type__c</field>
            <operation>equals</operation>
            <value>Deuda Generada</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Type__c</field>
            <operation>equals</operation>
            <value>Licitación</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_ARG_Subtipo_previsionable</fullName>
        <actions>
            <name>BI_ARG_Subtipo_previsionable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso Interno</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Department__c</field>
            <operation>equals</operation>
            <value>Gestión Reclamos de Facturación</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Type__c</field>
            <operation>equals</operation>
            <value>Reclamos por Baja No procesada / Retroactiva</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Type__c</field>
            <operation>equals</operation>
            <value>Adecuación de precios no aceptada por el Cliente</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Type__c</field>
            <operation>equals</operation>
            <value>Solicitud de Penalidades por no prestación de servicio</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Type__c</field>
            <operation>equals</operation>
            <value>Solicitud de Penalidades por Servicio no instalado</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Type__c</field>
            <operation>equals</operation>
            <value>Duplicidad de Servicios</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Type__c</field>
            <operation>equals</operation>
            <value>Bonificación de Precios no impactados</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Type__c</field>
            <operation>equals</operation>
            <value>Bonificación de Terminales Móviles no impactados</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_AccreditationRequest_WR</fullName>
        <actions>
            <name>BI_AccreditationRequest_ChangeOwner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_AccreditationRequest_ChangeStatus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.BI_Department__c</field>
            <operation>equals</operation>
            <value>Sales Operations</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Type__c</field>
            <operation>equals</operation>
            <value>Accreditation Request</value>
        </criteriaItems>
        <description>Cada vez que se cree un caso interno con departamento Sales Operations y Solicitation Type Accreditation Request, se asignará directamente a la queue Sales Operations, con status Asignado</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualiza_fecha_fin_actividad</fullName>
        <actions>
            <name>BI_Actualiza_fecha_fin_actividad</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow que actualiza el campo BI_Fecha_fin_de_actividad__c del caso para el tipo de registro Corporate Growth Engine.

Creado por la Demanda 270</description>
        <formula>AND(   RecordType.DeveloperName = &apos;BI_Corporate_Growth_Engine&apos;,  !PRIORVALUE(IsClosed),  IsClosed )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualiza_fecha_respuesta</fullName>
        <actions>
            <name>BI_Actualiza_fecha_respuesta</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Actualiza el campo BI_Fecha_fecha_de_recepcion__c del caso cuando el estado pasa de &quot;New&quot; a &quot;In Progress&quot;

Creada por la Demanda 270</description>
        <formula>AND(  RecordType.DeveloperName = &apos;BI_Corporate_Growth_Engine&apos;,   ISPICKVAL(PRIORVALUE(Status), &apos;New&apos;),   ISPICKVAL(Status, &apos;In Progress&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizar-Categoria-caso-Proveedor</fullName>
        <actions>
            <name>BI_Caso_relacionado_con_el_proveedor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Envio de un caso a proveedor para resolución cuando este pueda intervenir.</description>
        <formula>AND( ISPICKVAL(BI_Categoria_del_caso__c, &quot;Proveedores&quot;), !$Setup.BI_bypass__c.BI_migration__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizar_StatusChangeDate</fullName>
        <actions>
            <name>bi_Actualizar_StatusChangeDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISCHANGED( Status ),ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_BJ_Creador_Caso_PAC</fullName>
        <actions>
            <name>BI_BJ_Creador_Caso_PAC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.BI_O4_Tipo_de_Registro_Aux__c</field>
            <operation>equals</operation>
            <value>BI_BJ_Disconnect_or_Cancel</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pendiente Aceptación Cliente Bajas/Cancelaciones</value>
        </criteriaItems>
        <description>Envia una notificacion al creador del caso cuando el estado cambia a Pendiente de Aceptacion Cliente Bajas/ Cancelaciones</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_BJ_Notificacion_Caso_Cerrado</fullName>
        <actions>
            <name>BI_BJ_Notificacion_creador_Caso</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Envia una notificacion al creador del caso cuando el estado es Cancelado o Vencido</description>
        <formula>AND(  OR( ISPICKVAL(Status,&apos;Cancelado&apos;), ISPICKVAL(Status,&apos;Vencido&apos;)), RecordType.DeveloperName = &apos;BI_BJ_Disconnect_or_Cancel&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_CGE_Correo_asignacion</fullName>
        <actions>
            <name>BI_CGE_Envia_correo_asignacion</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow que lanza una alerta de correo electrónico al propietario del caso cuando este se crea o cambia de propietario.

Creado por la Demanda 270</description>
        <formula>AND(  OR(   ISNEW(),   ISCHANGED(OwnerId)  ),  RecordType.DeveloperName = &apos;BI_Corporate_Growth_Engine&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_CGE_Correo_cambio_estado</fullName>
        <actions>
            <name>BI_CGE_Envia_correo_cambio_estado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Envía una alerta de correo electrónico cuando un caso CGE cambia de estado(solo para estados NO cerrados).

Creado por la Demanda 270.</description>
        <formula>AND(  NOT(ISNEW()),  !IsClosed,  ISCHANGED(Status),  RecordType.DeveloperName = &apos;BI_Corporate_Growth_Engine&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_CGE_Correo_cierre_caso</fullName>
        <actions>
            <name>BI_CGE_Envia_correo_cierre_caso</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Envía una alerta de correo electrónico cuando se cierra un caso CGE.

Creado por la Demanda 270.</description>
        <formula>AND(   IsClosed,  RecordType.DeveloperName = &apos;BI_Corporate_Growth_Engine&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_CHI_Caso_Pendiente_12hrs</fullName>
        <active>true</active>
        <formula>AND(  ISPICKVAL(Owner:User.Pais__c, &quot;Chile&quot;), ISPICKVAL(Status, &quot;On Hold&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_CHI_Requerimiento_en_estado_Pendiente_12_horas</name>
                <type>Alert</type>
            </actions>
            <timeLength>12</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_CHI_Caso_Pendiente_6hrs</fullName>
        <active>true</active>
        <formula>AND(  ISPICKVAL(Owner:User.Pais__c, &quot;Chile&quot;), ISPICKVAL(Status, &quot;On Hold&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_CHI_Requerimiento_en_estado_Pendiente_6_horas</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_CHI_Notificacion_Agendamiento_de_equipos</fullName>
        <actions>
            <name>BI_CHI_Requerimiento_del_tipo_Agendamiento_de_equipos</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISPICKVAL(Owner:User.Pais__c, &quot;Chile&quot;), Subject = &quot;Agendamiento de equipos&quot; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_CHI_Notificacion_Top75</fullName>
        <actions>
            <name>BI_CHI_Caso_proveniente_del_segmento_Top_75</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISPICKVAL(Owner:User.Pais__c, &quot;Chile&quot;), TEXT(Account.BI_Subsegment_Local__c) = &quot;Top 75&quot; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_CHI_Notificacion_creacion_caso</fullName>
        <actions>
            <name>BI_CHI_Notificacion_asignacion_caso</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Lanza una alerta de correo electrónico que notifica al nuevo propietario del caso cuando este se modifique para el departamente &quot;Excepciones&quot; de Chile</description>
        <formula>AND(  ISCHANGED(OwnerId),   BI_CHI_Descuento__c != null,  ISPICKVAL(BI_Country__c, &quot;Chile&quot;),  ISPICKVAL(BI_Department__c, &quot;Excepciones&quot;),  ISPICKVAL(BI_Type__c, &quot;Solicitar Excepción&quot;),  NOT(ISBLANK(BI_CHI_Descuento__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_CHI_Notificacion_descuento_aprobado</fullName>
        <actions>
            <name>BI_CHI_Notificacin_descuento_aprobado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Lanza una alerta de correo electrónico que notifica al creador del caso que el descuento asociado ha sido aprobado</description>
        <formula>AND(  BI_CHI_Descuento__c != null,   ISPICKVAL(BI_Country__c, &quot;Chile&quot;),   ISPICKVAL(BI_Department__c, &quot;Excepciones&quot;),   ISPICKVAL(BI_Type__c, &quot;Solicitar Excepción&quot;),   ISCHANGED(IsClosed),  IsClosed = true,  ISPICKVAL(Status, &quot;Closed&quot;)   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_CHI_Notificacion_descuento_rechazado</fullName>
        <actions>
            <name>BI_CHI_Notificacion_descuento_rechazado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Lanza una alerta de correo electrónico que notifica al creador del caso que el descuento asociado ha sido rechazado</description>
        <formula>AND(  BI_CHI_Descuento__c != null,   ISPICKVAL(BI_Country__c, &quot;Chile&quot;),   ISPICKVAL(BI_Department__c, &quot;Excepciones&quot;),   ISPICKVAL(BI_Type__c, &quot;Solicitar Excepción&quot;),   ISCHANGED(IsClosed),  IsClosed = true,  NOT(ISPICKVAL(Status, &quot;Closed&quot;))   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_CHI_Requerimiento_del_tipo_Alta_de_tv_digital_etc</fullName>
        <actions>
            <name>BI_CHI_Requerimiento_del_tipo_Alta_de_tv_digital_etc</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISPICKVAL(Owner:User.Pais__c, &quot;Chile&quot;), OR( Subject = &quot;Alta de tv digital&quot;, Subject = &quot;Alta línea privada/ banda ancha / línea fija&quot;, Subject = &quot;Traslado línea privada/ banda ancha / línea fija&quot; ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_Comunicar_a_ejecutivo_de_cobros_creacion_de_nuevo_caso_reclamo</fullName>
        <actions>
            <name>Comunicar_ejecutivo_de_cobros_nuevo_caso_tipo_reclamo</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISPICKVAL(CreatedBy.BI_Permisos__c, &quot;Atención al Cliente&quot;),  NOT(ISBLANK(BI_Facturacion__c))  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_GPOS_GenericID</fullName>
        <actions>
            <name>BI_O4_Update_GenericoId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Tipo_registro_filtro__c = &apos;Genérico Posventa&apos;,ISBLANK(ParentId))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignacion_Casos_Posventa</fullName>
        <actions>
            <name>BI_MEX_Posventa_Refacturacion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>ehelp 03066111 - se asignan casos automáticamente a una cola dependiendo del Departamento y Tipo Solicitud.</description>
        <formula>TEXT(BI_Country__c) = &quot;Mexico&quot; &amp;&amp; TEXT(BI_Department__c) = &quot;Postventa&quot; &amp;&amp; TEXT(BI_Type__c) = &quot;Refacturación&quot; &amp;&amp; RecordType.DeveloperName = &apos;BI_Caso_Interno&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignacion_Casos_Recaudacion</fullName>
        <actions>
            <name>BI_MEX_Asignacion_Casos_Recaudacion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Peticion caso eHelp 03066111. Se asignan casos automáticamente a una cola dependiendo del Departamento y Tipo Solicitud.</description>
        <formula>TEXT(BI_Country__c) = &quot;Mexico&quot; &amp;&amp; TEXT(BI_Department__c) = &quot;Recaudacion&quot; &amp;&amp; RecordType.DeveloperName = &apos;BI_Caso_Interno&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignacion_casos_Internos_Gestion_Administrativa</fullName>
        <actions>
            <name>BI_MEX_Asignacion_caso_Gesti_n_Adm</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Caso eHelp 194807. Se crea una regla para asignar automáticamente los casos de Departamento Gestión Administrativa y Tipo Revision de documentación.</description>
        <formula>TEXT(BI_Country__c) = &quot;México&quot; &amp;&amp;  TEXT(BI_Department__c) = &quot;Gestión administrativa&quot; &amp;&amp;  TEXT(BI_Type__c) = &quot;Revisión de documentación&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignacion_casos_Internos_Implantacion_Proyectos</fullName>
        <actions>
            <name>BI_MEX_Asignacion_casos_Implantacion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.BI_Country__c</field>
            <operation>equals</operation>
            <value>Mexico</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Department__c</field>
            <operation>equals</operation>
            <value>Implantación de Proyectos</value>
        </criteriaItems>
        <description>Caso eHelp 488731. Se crea una regla para asignar automáticamente los casos de Departamento Implantacion de Proyectos a la cola</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignacion_casos_Internos_Mesa_de_Control</fullName>
        <actions>
            <name>BI_MEX_Asignacion_caso_Mesa_Control</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Caso eHelp 194807. Se asignan casos internos de México de Mesa de Control y Tipo Activación a una cola.</description>
        <formula>TEXT(BI_Country__c) = &quot;México&quot; &amp;&amp;  TEXT(BI_Department__c) = &quot;Mesa de control&quot; &amp;&amp;  TEXT(BI_Type__c)= &quot;Activación&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignacion_casos_Internos_Order_Entry</fullName>
        <actions>
            <name>BI_MEX_Asignacion_caso_Order_Entry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Se asignan automáticamente los casos de Order Entry a una cola.</description>
        <formula>ISPICKVAL(BI_Country__c, &quot;Mexico&quot;) &amp;&amp; TEXT(BI_Department__c) = &quot;Order Entry&quot; &amp;&amp; TEXT(BI_Type__c) = &quot;Solicitar Contrato&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignacion_casos_Internos_Preventa</fullName>
        <actions>
            <name>BI_MEX_Asignacion_caso_Preventa</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Peticion caso eHelp 194807. Se asignan casos automáticamente a un usuario o cola dependiendo del Departamento y Tipo Solicitud.</description>
        <formula>TEXT(BI_Country__c) = &quot;México&quot; &amp;&amp; TEXT(BI_Department__c) = &quot;Preventa&quot; &amp;&amp;  TEXT(BI_Type__c)= &quot;Solicitar Propuesta&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignacion_casos_Internos_Pricing_Fijo</fullName>
        <actions>
            <name>BI_MEX_Asignacion_caso_Pricing_Fijo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Caso eHelp 194807. Se asigna automaticamente los casos de Pricing Fijo a un usuario.</description>
        <formula>TEXT(BI_Country__c) = &quot;Mexico&quot; &amp;&amp;  TEXT(BI_Department__c) = &quot;Pricing Fijo&quot; &amp;&amp;  TEXT(BI_Type__c) = &quot;Solicitar Precio&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignacion_casos_Internos_Soluciones_Cloud</fullName>
        <actions>
            <name>BI_MEX_Asignacion_caso_Soluciones_Cloud</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Demanda 188. Se crea una regla para asignar automáticamente los casos de Departamento Soluciones Cloud.</description>
        <formula>TEXT(BI_Country__c) = &apos;Mexico&apos; &amp;&amp; ISPICKVAL( BI_Department__c, &apos;Soluciones Cloud&apos;) &amp;&amp; RecordType.DeveloperName = &apos;BI_Caso_Interno&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignacion_casos_Internos_Soluciones_M2M</fullName>
        <actions>
            <name>BI_MEX_Asignacion_caso_Soluciones_M2M</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Demanda 188. Se crea una regla para asignar automáticamente los casos de Departamento Soluciones M2M</description>
        <formula>TEXT(BI_Country__c) = &apos;Mexico&apos; &amp;&amp; ISPICKVAL( BI_Department__c, &apos;Soluciones M2M&apos;) &amp;&amp; RecordType.DeveloperName = &apos;BI_Caso_Interno&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignacion_casos_Internos_Soluciones_Seguridad</fullName>
        <actions>
            <name>BI_MEX_Asig_caso_Soluciones_Seguridad</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Demanda 188. Se crea una regla para asignar automáticamente los casos de Departamento Soluciones Seguridad</description>
        <formula>TEXT(BI_Country__c) = &apos;Mexico&apos; &amp;&amp; ISPICKVAL( BI_Department__c, &apos;Soluciones Seguridad&apos;) &amp;&amp; RecordType.DeveloperName = &apos;BI_Caso_Interno&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignacion_casos_Renovaciones</fullName>
        <actions>
            <name>BI_MEX_Asignacion_casos_Renovaciones</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Solicitada en ehelp 01856601. Se asignan a esta cola los casos cuyo departamento es &quot;Renovaciones&quot;</description>
        <formula>TEXT(BI_Country__c) = &quot;Mexico&quot; &amp;&amp; TEXT(BI_Department__c) = &quot;Renovaciones&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Asignar_Cola_Preventa</fullName>
        <actions>
            <name>BI_MEX_Cola_PreventaXdpto_ing_preventa</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.BI_Department__c</field>
            <operation>equals</operation>
            <value>Ingeniería preventa</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Country__c</field>
            <operation>equals</operation>
            <value>Mexico</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>BI_Standard_MEX</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_Notif_Opty_Owner</fullName>
        <actions>
            <name>BI_Notif_Opty_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  RecordType.Name = &apos;Caso Interno&apos;,  ISPICKVAL(BI_Country__c,&apos;Argentina&apos;),  IsClosed = true,  PRIORVALUE(IsClosed) = false,  !ISBLANK(BI_Nombre_de_la_Oportunidad__c),  !ISNULL(BI_Nombre_de_la_Oportunidad__c),  BI_Notificar_al_dueno_de_la_oportunidad__c = true )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_AsuntoCaso</fullName>
        <actions>
            <name>BI_O4_Update_GenericoId</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_Update_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Rellena el campo asunto para los casos padre de tipo registro Genérico preventa</description>
        <formula>AND(Tipo_registro_filtro__c = &apos;Genérico Preventa&apos;,     OR(ISBLANK(ParentId),          Parent.Tipo_registro_filtro__c = &apos;Caso Interno&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_GenericoPreventaCasoHijoPendienteAclaracion</fullName>
        <actions>
            <name>BI_O4_Caso_Hijo_Pendiente_Aclaraci_n</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Se enviará una notificación al Responsable Preventa Global cuando los casos hijos cambien de &quot;Pendiente Cotización&quot; a &quot;Pendiente Aclaracion&quot;</description>
        <formula>AND(  !$Setup.BI_bypass__c.BI_migration__c,  NOT (ISBLANK(ParentId)),  Parent.RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;,  RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;,  ISPICKVAL(PRIORVALUE(Status),&quot;Pendiente Cotización&quot;),  ISPICKVAL(Status,&quot;Pendiente Aclaración&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_GenericoPreventaCasoHijoRechazado</fullName>
        <actions>
            <name>BI_O4_Caso_Hijo_Rechazado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Se enviará una notificación al Responsable Preventa Global cuando los casos hijos cambien de &quot;Pendiente Cotización&quot; a &quot;Rechazado&quot;</description>
        <formula>AND(  !$Setup.BI_bypass__c.BI_migration__c,  NOT (ISBLANK(ParentId)),  Parent.RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;,  RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;,  ISPICKVAL(PRIORVALUE(Status),&quot;Pendiente Cotización&quot;), ISPICKVAL(Status,&quot;Rejected&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_GenericoPreventaCasoPadrePendienteAclaracion</fullName>
        <actions>
            <name>BI_O4_Caso_Padre_Pendiente_Valoraci_n</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Se enviará una notificación al Propietario del Caso Padre cuando el caso cambie de &quot;Pendiente Revisión&quot; a &quot;Pendiente Aclaración&quot;</description>
        <formula>AND(  !$Setup.BI_bypass__c.BI_migration__c,  OR(    AND(        NOT (ISBLANK(ParentId)),         Parent.RecordType.DeveloperName=&quot;BI_Caso_Interno&quot; 	),    ISBLANK(ParentId)   ), RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;,  ISPICKVAL(PRIORVALUE(Status),&quot;Pendiente Revisión&quot;),  ISPICKVAL(Status,&quot;Pendiente Aclaración&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_GenericoPreventaCierreCasosHijosV1</fullName>
        <actions>
            <name>BI_O4_Actualiza_Closed_Type_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Los casos hijos en estado Pendiente de Cotización se cerrarán como “Unquoted” (Campo closed Type)</description>
        <formula>AND( !$Setup.BI_bypass__c.BI_migration__c,  NOT (ISNULL(ParentId)), Parent.RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;, RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;, ISBLANK(BI_O4_Fecha_de_Cotizaci_n__c),  (IsClosed = TRUE)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_GenericoPreventaCierreCasosHijosV2</fullName>
        <actions>
            <name>BI_O4_Actualiza_Closed_Type_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Los casos hijos con Fecha de Cotización anterior o igual a la Fecha de entrega requerida, se cerrará como Quoted on Time (Campo closed Type)</description>
        <formula>AND(  !$Setup.BI_bypass__c.BI_migration__c,  NOT (ISNULL(ParentId)),  Parent.RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;,  RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;, BI_O4_Fecha_de_Cotizaci_n__c &lt;=  Parent.BI_O4_Fecha_presentaci_n_oferta__c, NOT ( ISNULL(BI_O4_Fecha_de_Cotizaci_n__c )), NOT ( ISNULL(Parent.BI_O4_Fecha_presentaci_n_oferta__c ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_GenericoPreventaCierreCasosHijosV3</fullName>
        <actions>
            <name>BI_O4_Actualiza_Closed_Type_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Los casos hijos con Fecha de Cotización posterior a la Fecha de entrega requerida, se cerrará como Quoted Out of Time (Campo closed Type)</description>
        <formula>AND(  !$Setup.BI_bypass__c.BI_migration__c,  NOT (ISNULL(ParentId)),  Parent.RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;,  RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;, BI_O4_Fecha_de_Cotizaci_n__c &gt; Parent.BI_O4_Fecha_presentaci_n_oferta__c,   NOT ( ISNULL(BI_O4_Fecha_de_Cotizaci_n__c )),  NOT ( ISNULL(Parent.BI_O4_Fecha_presentaci_n_oferta__c ) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_GenericoPreventaCotizado</fullName>
        <actions>
            <name>BI_O4_Status_hijo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Si los campos del caso hijo BI_O4_N_Sites__c y BI_O4_Tipo_oferta__c estan rellenos, el Status pasa a &quot;cotizada&quot;</description>
        <formula>AND(    NOT (ISBLANK(ParentId)),    Parent.RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;,               RecordType.DeveloperName = &quot;BI_O4_Gen_rico_Preventa&quot;,               OR(ISBLANK(PRIORVALUE(BI_O4_N_Sites__c)),        ISBLANK(PRIORVALUE(BI_O4_Tipo_oferta__c))),    ISPICKVAL(PRIORVALUE(Status),&quot;Pendiente Cotización&quot;),     NOT(ISBLANK(BI_O4_N_Sites__c)),    NOT(ISBLANK(TEXT(BI_O4_Tipo_oferta__c)))    )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_GenericoPreventaFechaCotizacion</fullName>
        <actions>
            <name>BI_O4_Actualiza_Fecha_de_Cotizaci_n</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>El campo Fecha de Cotización se rellenará cuando el caso pase de Pendiente de Cotización a Cotizado.</description>
        <formula>AND(  !$Setup.BI_bypass__c.BI_migration__c,   RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;,  ISPICKVAL(PRIORVALUE(Status),&quot;Pendiente Cotización&quot;),  ISPICKVAL(Status,&quot;Cotizada&quot;)    )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_GenericoPreventaFechaCotizacionHijos</fullName>
        <actions>
            <name>BI_O4_Caso_Hijo_Cotizado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Se enviará una notificación al Responsable Preventa Global cuando el campo Fecha de Cotización cambie en los casos hijos.</description>
        <formula>AND(  !$Setup.BI_bypass__c.BI_migration__c,  NOT (ISBLANK(ParentId)), Parent.RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;,  RecordType.DeveloperName=&quot;BI_O4_Gen_rico_Preventa&quot;,  ISCHANGED(BI_O4_Fecha_de_Cotizaci_n__c)   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_PE_CasoHijoCC</fullName>
        <actions>
            <name>BI_O4_PE_CambioPropietario</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BI_O4_Asignado</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_CompetenceCenter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>El caso padre se pasa a Competence Center</description>
        <formula>AND(     ISPICKVAL(BI_Department__c, &quot;Ingeniería preventa&quot;),     ISPICKVAL( BI_Type__c, &quot;Proyectos Especiales&quot;),     NOT(ISBLANK( ParentId )),  ISPICKVAL(Parent.BI_Type__c, &quot;Proyectos Especiales&quot;),     ISPICKVAL(BI_O4_Type_of_Support_Request__c,&quot;Technical&quot;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_PE_CasoHijoGFSO</fullName>
        <actions>
            <name>BI_O4_PE_CambioPropietario</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BI_O4_Asignado</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_PE_GFSO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>El caso padre se pasa a GFSO</description>
        <formula>AND(     ISPICKVAL(BI_Department__c, &quot;Ingeniería preventa&quot;),     ISPICKVAL( BI_Type__c, &quot;Proyectos Especiales&quot;),     NOT(ISBLANK( ParentId )),  ISPICKVAL(Parent.BI_Type__c, &quot;Proyectos Especiales&quot;),     ISPICKVAL(BI_O4_Type_of_Support_Request__c,&quot;Operational&quot;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_PE_CasoPadreCC</fullName>
        <actions>
            <name>BI_O4_Asignado</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_O4_CompetenceCenter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>El caso padre se pasa a Competence Center</description>
        <formula>AND(          ISPICKVAL(BI_Department__c, &quot;Ingeniería preventa&quot;),     ISPICKVAL( BI_Type__c, &quot;Proyectos Especiales&quot;),     OR(        ISBLANK( ParentId ),        AND(            NOT(ISBLANK( ParentId )),            NOT(ISPICKVAL(Parent.BI_Type__c, &quot;Proyectos Especiales&quot;))            )        ),     ISPICKVAL(Status, &quot;New&quot;)     )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Pte_Informacion</fullName>
        <actions>
            <name>Pte_Informacion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pdte Información</value>
        </criteriaItems>
        <description>Marca el campo &quot;Pte Información&quot; para informes</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Tipoderegistro</fullName>
        <actions>
            <name>BI_O4_Tipo_de_registro</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_At_Emp_aut</fullName>
        <actions>
            <name>BI_PER_creacion_autorizada</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Creado por demanda 322</description>
        <formula>AND(INCLUDES(BI_Notification_Preferences__c,&apos;Case Creation&apos;),   BI_PER_AtencionEmpresas__c,   Contact.BI2_IsAutorized__c,   TEXT(BI_Country__c) == $Label.BI_Peru,   ISCHANGED(BI_PER_AtencionEmpresas__c),   RecordType.DeveloperName == &apos;BI2_Caso_Padre&apos;    )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_At_Emp_no_aut</fullName>
        <actions>
            <name>BI_PER_creacion_no_autorizada</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Envia correos a contactos no autorizados y sin contacto
Creado por demanda 322</description>
        <formula>AND(   BI_PER_AtencionEmpresas__c,   OR(     ISBLANK(ContactId),      !Contact.BI2_IsAutorized__c   ),   TEXT(BI_Country__c) == $Label.BI_Peru,   ISCHANGED(BI_PER_AtencionEmpresas__c),   RecordType.DeveloperName == &apos;BI2_Caso_Padre&apos;    )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_At_Emp_resuelto</fullName>
        <actions>
            <name>BI_PER_At_emp_resuelto</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Envia correos a contactos autorizados cuando se resuelve el caso 
Creado por demanda 322</description>
        <formula>AND(   BI_PER_AtencionEmpresas__c,   Contact.BI2_IsAutorized__c,   TEXT(BI_Country__c) == $Label.BI_Peru,   ISCHANGED(Status),   ISPICKVAL(Status, &apos;Resolved&apos;),   RecordType.DeveloperName == &apos;BI2_Caso_Padre&apos;,   INCLUDES(BI_Notification_Preferences__c,&apos;Status Change&apos;)     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_At_emp_cancelado</fullName>
        <actions>
            <name>BI_PER_At_emp_cancelado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Envia correos a contactos autorizados cuando se cancela el caso 
Creado por demanda 322</description>
        <formula>AND(INCLUDES(BI_Notification_Preferences__c,&apos;Status Change&apos;),   BI_PER_AtencionEmpresas__c,   Contact.BI2_IsAutorized__c,   TEXT(BI_Country__c) == $Label.BI_Peru,   ISCHANGED(Status),   ISPICKVAL(Status, &apos;Cancelled&apos;),   RecordType.DeveloperName == &apos;BI2_Caso_Padre&apos;    )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_At_emp_cerrado</fullName>
        <actions>
            <name>BI_PER_At_emp_cerrado</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Envia correos a contactos autorizados cuando se cierra el caso 
Creado por demanda 322</description>
        <formula>AND(INCLUDES(BI_Notification_Preferences__c,&apos;Case Closure&apos;),   BI_PER_AtencionEmpresas__c,   Contact.BI2_IsAutorized__c,   TEXT(BI_Country__c) == $Label.BI_Peru,   ISCHANGED(Status),   ISPICKVAL(Status, &apos;Closed&apos;),   RecordType.DeveloperName == &apos;BI2_Caso_Padre&apos;    )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_Caso_Resuelto_emails</fullName>
        <actions>
            <name>BI2_Envia_Email_Cliente_Caso</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Cuando el Estado del Caso pasa a Resuelto, se envía un email al contacto asociado al caso.</description>
        <formula>AND(     NOT(ISBLANK(AccountId)),     NOT(ISBLANK(ContactId)),        ISCHANGED(Status),      OR(        TEXT(Status) = &apos;Resolved&apos;,        TEXT(Status) = &apos;Resuelto&apos;     ),     !$Setup.BI_bypass__c.BI_migration__c,      TEXT( BI_Country__c ) =  $Label.BI_Peru,     RecordType.DeveloperName = &apos;BI2_caso_comercial&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_SUB_Fecha_Vigencia</fullName>
        <actions>
            <name>Actualizar_descripcion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( ISPICKVAL(BI_Country__c, &apos;Argentina&apos;), OR( ISPICKVAL(BI_Type__c, &apos;Subsidio&apos;), ISPICKVAL(BI_Type__c, &apos;Subsidio Excepcional&apos;) ), NOT(ISPICKVAL(Status, &apos;Approved&apos;)), NOT(ISPICKVAL(Status, &apos;Rejected&apos;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_SUB_Enviar_Alerta</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>BI_SUB_Actualizar_Estado</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>BI_SUB_Actualizar_descripcion</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_actualiza_fecha_inicio_solucion</fullName>
        <actions>
            <name>BI_Update_status_inprogress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  $User.Id = OwnerId,  RecordType.DeveloperName = &apos;BI_Corporate_Growth_Engine&apos;, ISPICKVAL(Status, &apos;In Progress&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_actualiza_fecha_recepcion</fullName>
        <actions>
            <name>BI_Update_status_en_analisis</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_fill_fecha_recepcion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(      $User.Id =  OwnerId,      RecordType.DeveloperName = &apos;BI_Corporate_Growth_Engine&apos;,      ISPICKVAL(Status, &apos;New&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CoE Start Clock SLA sHelp_ITSD</fullName>
        <active>true</active>
        <formula>AND( OR( ISPICKVAL(Status, &apos;New&apos;), ISPICKVAL(Status, &apos;Reopened&apos;), ISPICKVAL(Status, &apos;Analyzing&apos;), ISPICKVAL(Status, &apos;Being processed&apos;) ), OR( RecordType.DeveloperName = &apos;Caso_eHelp&apos;, RecordType.DeveloperName = &apos;TGS_Caso_ITSD&apos; ), ISCHANGED(Status) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CoE Stop Clock SLA sHelp_ITSD</fullName>
        <active>true</active>
        <formula>AND( OR( ISPICKVAL(Status, &apos;Information pending&apos;), ISPICKVAL(Status, &apos;Confirmation pending&apos;), ISPICKVAL(Status, &apos;Production pending&apos;), ISPICKVAL(Status, &apos;Typing&apos;) ), OR( RecordType.DeveloperName = &apos;Caso_eHelp&apos;, RecordType.DeveloperName = &apos;TGS_Caso_ITSD&apos; ), ISCHANGED(Status) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CoE_Reopen_Milestones</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Reopened</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp,Caso ITSD</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CoE_Stop_Milestone1</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Analyzing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp,Caso ITSD</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CoE_Stop_Milestone2</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Being processed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp,Caso ITSD</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CoE_Stop_Milestone3</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Cancelled,Closed,New demand</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso eHelp,Caso ITSD</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notificacion_creacion_caso_interno_soporte_venta</fullName>
        <actions>
            <name>BI_CHI_Notificacion_caso_soporteventa</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.BI_Department__c</field>
            <operation>equals</operation>
            <value>Soporte a la Venta</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Type__c</field>
            <operation>equals</operation>
            <value>Activación y/o Despacho</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BI_Country__c</field>
            <operation>equals</operation>
            <value>Chile</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso Interno</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Case Assign To ACME SD L1</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_Acme_SD_Level_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	OR(  		TGS_Service__c = &apos;Universal WIFI&apos;,  		TGS_Service__c = &apos;SMDM Management&apos;,  		TGS_Service__c = &apos;M2M Jasper - SIM Request&apos;,  		TGS_Service__c = &apos;M2M Jasper - Service Change&apos;,  		TGS_Service__c = &apos;M2M Smart - Service Change&apos;,  		TGS_Service__c = &apos;M2M Smart - SIM Request&apos;,  		TGS_Service__c = &apos;M2M SBC - Service Request&apos;,  		TGS_Service__c = &apos;Colocation/Housing&apos;,  		TGS_Service__c = &apos;Threat Detection&apos;  	),  	OR( 		AND( 			ISPICKVAL(Status, &apos;Assigned&apos;),  			NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;))  		),  		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( 			ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;),  			ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;) 		),  		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( 			ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;),  			ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;) 		),	 		ISPICKVAL(Status, &apos;Resolved&apos;),  		ISPICKVAL(Status, &apos;Closed&apos;)  	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	), 	OR( 		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;,  		Account.Name = &apos;ACME&apos;  	)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Case assign To Financial Control</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Financial_Control</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  	OR(  		AND(  			RecordType.DeveloperName = &apos;Order_Management_Case&apos;,  			ISPICKVAL(Status, &apos;In Progress&apos;),  			ISPICKVAL(TGS_Status_reason__c,&apos;Financial Risk&apos;)  		),	 		AND(  			TGS_Service__c = &apos;Colocation/Housing&apos;,  			ISPICKVAL(Status, &apos;Pending&apos;),  			ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation - Not Paid&apos;), 			AND(  				Account.TGS_Aux_Holding__r.Name = &apos;Nokia&apos;,  				Account.Name = &apos;Nokia&apos; 			)   		)  	),  	AND(  		NOT(Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;),  		NOT(Account.Name = &apos;ACME&apos;)  	) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Case assign To Presales</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Presales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	RecordType.DeveloperName = &apos;Order Management Case&apos;, 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( 			ISPICKVAL(TGS_Status_reason__c,&apos;Quotation (Presales phase)&apos;), 			ISPICKVAL(TGS_Status_reason__c,&apos;Quotation (Presales phase - OB/Supplier)&apos;), 			ISPICKVAL(TGS_Status_reason__c,&apos;Quotation (Presales phase - cost)&apos;) 		), 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Not paid&apos;) 	), 	AND( 		Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;ACME&apos;, 		Account.Name &lt;&gt; &apos;ACME&apos;, 		Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;Nokia&apos;, 		Account.Name &lt;&gt; &apos;Nokia&apos; 	) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Case assign To Presales Test</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Presales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	RecordType.DeveloperName = &apos;Order_Management_Case&apos;, 	TGS_Service__c &lt;&gt; &apos;Colocation/Housing&apos;, 	OR( 		ISPICKVAL(Status,&apos;In Progress&apos;) &amp;&amp; OR ( 			ISPICKVAL(TGS_Status_reason__c,&apos;Quotation (Presales Phase)&apos;), 			ISPICKVAL(TGS_Status_reason__c,&apos;Quotation (Presales Phase - OB/Supplier)&apos;), 			ISPICKVAL(TGS_Status_reason__c,&apos;Quotation (Presales Phase - Cost)&apos;) 		), 		ISPICKVAL(Status,&apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Not Paid&apos;)	 	), 	OR( 		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;, 		Account.Name = &apos;ACME&apos; 	)		 )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Case assign To SMC L1 APAC_EMEA</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_SMC_L1_Ordering_APAC_EMEA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	OR(  			RecordType.DeveloperName = &apos;TGS_Billing_Inquiry&apos;, 			RecordType.DeveloperName = &apos;Order_Management_Case&apos; 	), 	OR(  		AND( ISPICKVAL(Status, &apos;Assigned&apos;), NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;))), 		ISPICKVAL(Status, &apos;Cancelled&apos;), 		ISPICKVAL(Status, &apos;Rejected&apos;), 		ISPICKVAL(Status, &apos;Resolved&apos;), 		ISPICKVAL(Status, &apos;Closed&apos;), 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( 												ISPICKVAL(TGS_Status_reason__c, &apos;In SD Validation&apos;), 												ISPICKVAL(TGS_Status_reason__c, &apos;In Provision&apos;) 											),	 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( 											ISPICKVAL(TGS_Status_reason__c, &apos;In SD Validation&apos;), 											ISPICKVAL(TGS_Status_reason__c, &apos;In Provision&apos;), 											ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation&apos;), 											ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation - Close&apos;) 										) 	), 	OR( 		Account.TGS_Aux_Holding__r.Name = &apos;CYBER BLUE&apos;,  		Account.Name = &apos;CYBER BLUE&apos;  	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Case assign To SMC L1 Miami</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_SMC_L1_Ordering_Miami</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR (3 AND 4) OR (5 AND 6)) AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Order Management Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Assigned,Cancelled,Rejected,Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Status_reason__c</field>
            <operation>equals</operation>
            <value>In SD Validation,Quotation (Margin - Cost),In Test</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Status_reason__c</field>
            <operation>equals</operation>
            <value>In SD Validation,Waiting for quotation aceptance,In Test,In Cancellation - Close</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>contains</operation>
            <value>OnStar</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Case assign To SMC L2 ACME</fullName>
        <actions>
            <name>Empty_Assigne</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGSTest_L2_Ordering_Mobile</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND ((2 AND 3) OR (4 AND 5)) AND 6 AND (7 OR 8 OR 9 OR 10 OR 11)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Order Management Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Status_reason__c</field>
            <operation>equals</operation>
            <value>In Provision,In Activation,In Configuration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Status_reason__c</field>
            <operation>equals</operation>
            <value>In Provision,In Cancellation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>contains</operation>
            <value>ACME</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Service__c</field>
            <operation>contains</operation>
            <value>M2M</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Service__c</field>
            <operation>equals</operation>
            <value>Universal WIFI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Service__c</field>
            <operation>contains</operation>
            <value>SMDM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Service__c</field>
            <operation>equals</operation>
            <value>Colocation/Housing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Service__c</field>
            <operation>equals</operation>
            <value>Threat Detection</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Case assign To SMC L2 APAC_EMEA</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND ((2 AND 3) OR (4 AND 5)) AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Order Management Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Status_reason__c</field>
            <operation>equals</operation>
            <value>Quotation (Margin),Quotation (Margin - OB/Supplier),In Provision,In Configuration,In Activation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Status_reason__c</field>
            <operation>equals</operation>
            <value>In Provision,In Cancellation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>contains</operation>
            <value>Cyber blue</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Case assign To SMC L2 EMEA</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_SMC_L2_Ordering_EMEA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR(	 	AND(      		OR(          			ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;In provision&apos;),          			ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp;  			OR( 				ISPICKVAL(TGS_Status_reason__c, &apos;In provision&apos;), 				ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation&apos;)                                          			)      		),      		OR(           			Account.TGS_Aux_Holding__r.Name = &apos;JCDecaux&apos;,           			Account.TGS_Aux_Holding__r.Name = &apos;Focus Mobile&apos;,           			Account.TGS_Aux_Holding__r.Name = &apos;NEC CORPORATION&apos;,          			Account.TGS_Aux_Holding__r.Name = &apos;Volvo&apos;,           			Account.Name = &apos;JCDecaux&apos;,           			Account.Name = &apos;Focus Mobile&apos;,           			Account.Name = &apos;NEC CORPORATION&apos;,            			Account.Name = &apos;Volvo&apos;       		)  	), 	AND( 	 		OR ( 		 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;In Provision&apos;), 		 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;In Provision&apos;), 		 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation&apos;) 	), 	 		OR( 		 			Account.TGS_Aux_Holding__r.Name = &apos;Delair&apos;, 		 			Account.Name = &apos;Delair&apos;, 		 			Account.TGS_Aux_Holding__r.Name = &apos;Lyonnaise des Eaux&apos;, 		 			Account.Name = &apos;Lyonnaise des Eaux&apos; 	 		), 	 		OR(  			ISCHANGED(TGS_Status_reason__c), 		 			ISCHANGED(Status) 	 		)	  	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Case assign To SMC L2 Miami</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_SMC_L2_Ordering_Miami</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND ((2 AND 3) OR (4 AND 5)) AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Order Management Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Status_reason__c</field>
            <operation>equals</operation>
            <value>In Provision</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Status_reason__c</field>
            <operation>equals</operation>
            <value>In Provision,In Cancellation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>contains</operation>
            <value>OnStar</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Case assign To Test Financial Control</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Financial_Control_Test</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(   RecordType.DeveloperName = &apos;Order_Management_Case&apos;,   OR(   AND(ISPICKVAL(Status, &apos;In Progress&apos;),   ISPICKVAL(TGS_Status_reason__c,&apos;Financial Risk&apos;)   ),  	AND( TGS_Service__c = &apos;Colocation/Housing&apos;,   ISPICKVAL(Status, &apos;Pending&apos;),   ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation - Not Paid&apos;)   )   ),   OR( Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;, Account.Name = &apos;ACME&apos;   )   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Case assign To Test MultiService Purchase</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_Multiservice_Purchase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  	OR( 		TGS_Service__c = &apos;Contact Centre&apos;, 		TGS_Service__c = &apos;Video&apos;, 		TGS_Service__c = &apos;ToIP&apos;, 		TGS_Service__c = &apos;CISCO HCS Service&apos;, 		TGS_Service__c = &apos;HCS Locations&apos;, 		TGS_Service__c = &apos;HCS Open Order&apos;, 		TGS_Service__c = &apos;HCS License&apos; 		), 		OR(  		ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Off-Net&apos;),  		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp; OR(  												ISPICKVAL(TGS_Status_reason__c,&apos;Off-Net Supplier Data Checked&apos;),  												ISPICKVAL(TGS_Status_reason__c,&apos;SO Launched (&amp; POF Required)&apos;), 												ISPICKVAL(TGS_Status_reason__c,&apos;SO Accepted&apos;) 											) 	),  	OR(  		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;,  		Account.Name = &apos;ACME&apos;  		)	  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Get Case Emails</fullName>
        <actions>
            <name>TGS_Get_Case_Contact_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Get_Case_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(  ISCHANGED( ContactId ),   ISCHANGED(  OwnerId ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner %3A TGS Order Handling W3</fullName>
        <actions>
            <name>TGS_Case_assign_to_Order_Handling</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  OR(  AND(  TGS_Service__c= &apos;Services&apos;,  OR(  ISPICKVAL(Status, &apos;Assigned&apos;) &amp;&amp; NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)),	ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c, &apos;In Order Handling&apos;), ISPICKVAL(TGS_Status_reason__c, &apos;Pending Task Resolution&apos;) )  )  ),  AND(  TGS_Service__c = &apos;Fixed Voice&apos;,  OR(  ISPICKVAL(Status, &apos;Assigned&apos;) &amp;&amp; NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)), ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c, &apos;In Order Handling&apos;), ISPICKVAL(TGS_Status_reason__c, &apos;Pending Task Resolution&apos;) ),  ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation&apos;), ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation - Close&apos;), ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation - Not Paid&apos;)  )/*,  ISPICKVAL(Status, &apos;Resolved&apos;)*/	 )  )  ),  AND(  NOT(Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;), NOT(Account.Name = &apos;ACME&apos;)  ),  OR(  ISCHANGED(TGS_Status_reason__c), ISCHANGED(Status)  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner %3A TGS Test Order Handling W3</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_Order_Handling</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	 OR( 		 AND( 			 TGS_Service__c= &apos;Services&apos;, 			 OR( 				 ISPICKVAL(Status, &apos;Assigned&apos;) &amp;&amp; NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)), 				ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( 					ISPICKVAL(TGS_Status_reason__c, &apos;In Order Handling&apos;), 					ISPICKVAL(TGS_Status_reason__c, &apos;Pending Task Resolution&apos;) 				) 			 ) 		 ), 		 AND( 			 TGS_Service__c = &apos;Fixed Voice&apos;, 			 OR( 				 ISPICKVAL(Status, &apos;Assigned&apos;) &amp;&amp; NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)), 				ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( 					ISPICKVAL(TGS_Status_reason__c, &apos;In Order Handling&apos;), 					ISPICKVAL(TGS_Status_reason__c, &apos;Pending Task Resolution&apos;) 					), 				ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( 					ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation&apos;), 					ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation - Close&apos;), 					ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation - Not Paid&apos;) 				 )/*, 				 ISPICKVAL(Status, &apos;Resolved&apos;)*/	 			 ) 		 ) 	 ), 	 OR( 		 Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;, 		Account.Name = &apos;ACME&apos; 	), 	 OR( 		 ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	 )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A CEDEX CC</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_CEDEX_CC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR(  	AND(  		OR(  			TGS_Service__c = &apos;Contact Centre&apos;, TGS_Service__c = &apos;Video&apos;, TGS_Service__c = &apos;ToIP&apos;, TGS_Service__c = &apos;Cisco HCS Service&apos;, TGS_Service__c = &apos;HCS Locations&apos;, TGS_Service__c = &apos;HCS Open Order&apos;, TGS_Service__c = &apos;HCS Video&apos;, TGS_Service__c = &apos;HCS ToIP&apos;, TGS_Service__c = &apos;HCS License&apos;, TGS_Service__c = &apos;Domestic Number&apos;, TGS_Service__c = &apos;International Number&apos;, TGS_Service__c = &apos;Setup and Add-ons&apos;, TGS_Service__c = &apos;WebEx&apos;  		),  		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Configuration&apos;),  			OR(  				Account.TGS_Aux_Holding__r.Name = &apos;Nokia&apos;,  				Account.Name = &apos;Nokia&apos;	 			)  	),  			AND(  			OR(  			TGS_Service__c = &apos;DDI Resale&apos;, TGS_Service__c = &apos;DDI Standard&apos;, TGS_Service__c = &apos;Special Number&apos;, TGS_Service__c = &apos;PNP&apos;  			),  			ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Configuration&apos;) &amp;&amp; OR(CreatedBy.Profile.Name &lt;&gt; &apos;TGS Integration User&apos;, CreatedBy.Profile.Name &lt;&gt; &apos;TGS Order Handling&apos;), AND( Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;ACME&apos;, Account.Name &lt;&gt; &apos;ACME&apos;, Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;Marchex&apos;)  			)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A CEDEX CC Network Configuration</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_CEDEX_CC_Network_Configuratio</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	TGS_Service__c = &apos;Fixed Voice&apos;, 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp;	ISPICKVAL(TGS_Status_reason__c, &apos;In Network Configuration&apos;), 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;In Network Configuration&apos;)	 	), 	AND( 		NOT(Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;), 		NOT(Account.Name = &apos;ACME&apos;) 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A CEDEX CC Resources Allocation</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_CEDEX_CC_Resources_Allocation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	TGS_Service__c = &apos;Fixed Voice&apos;, 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp;	ISPICKVAL(TGS_Status_reason__c, &apos;In Assignment&apos;), 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;In Assignment&apos;)	 	), 	AND( 		NOT(Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;), 		NOT(Account.Name = &apos;ACME&apos;) 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A CGP CEMEX</fullName>
        <actions>
            <name>TGS_Case_assign_to_CGP_CEMEX</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  	OR( 		TGS_Service__c = &apos;mWan&apos;, 		TGS_Service__c = &apos;mWan - Internet&apos; 	), 	OR(  		ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;),  		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp; OR(  												ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;),  												ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;) 											),  		ISPICKVAL(Status, &apos;Resolved&apos;),  		ISPICKVAL(Status, &apos;Closed&apos;)  	),  	OR( ISPICKVAL( Account.TGS_Aux_Customer_Country__r.TGS_SMC_Assigned_L1__c , &apos;18&apos;),  		ISPICKVAL( Account.TGS_Aux_Holding__r.TGS_SMC_Assigned_L1__c , &apos;18&apos;)  	)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Freephone</fullName>
        <actions>
            <name>TGS_Case_Assign_to_Freephone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule contains all the scenarios that trigger an owner change towards Freephone</description>
        <formula>AND( 	OR( 		TGS_Service__c = &apos;mSIP Site Trunk&apos;, 		TGS_Service__c = &apos;mSIP Site No Trunk&apos;, 		TGS_Service__c = &apos;mSIP Site Hosted UC&apos;, 		TGS_Service__c = &apos;mSIP Site CCA&apos;, 		TGS_Service__c = &apos;mSIP Site ADMIN&apos;, 		TGS_Service__c = &apos;DDI Standard&apos;, 		TGS_Service__c = &apos;DDI Resale&apos;, 		TGS_Service__c = &apos;Flat Rate&apos;, 		TGS_Service__c = &apos;PNP&apos;, 		TGS_Service__c = &apos;Special Number&apos; 	), 	OR( 		AND( 			ISPICKVAL(Status, &apos;Assigned&apos;), 			NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)) 		), 		ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp; OR( 			ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 			ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), 			ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;), 			ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;) 		), 		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp; OR( 			ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 			ISPICKVAL(TGS_Status_reason__c,&apos;In Order Handling&apos;), 			ISPICKVAL(TGS_Status_reason__c,&apos;Pending Task Resolution&apos;) 		), 		ISPICKVAL(Status, &apos;Resolved&apos;) 	), 	OR( 		ISPICKVAL( Account.TGS_Aux_Customer_Country__r.TGS_SMC_Assigned_L1__c , &apos;17&apos;), 		ISPICKVAL( Account.TGS_Aux_Holding__r.TGS_SMC_Assigned_L1__c , &apos;17&apos;) 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Multiservice Provision Controller</fullName>
        <actions>
            <name>TGS_Case_assign_to_Multiservice_Provisio</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule contains all the scenarios that trigger an owner change towards Multiservice Provision Controller</description>
        <formula>AND( 	OR( 		TGS_Service__c = &apos;Contact Centre&apos;, 		TGS_Service__c = &apos;Video&apos;, 		TGS_Service__c = &apos;ToIP&apos;, 		TGS_Service__c = &apos;HCS Video&apos;, 		TGS_Service__c = &apos;HCS ToIP&apos;, 		TGS_Service__c = &apos;Cisco HCS Service&apos;, 		TGS_Service__c = &apos;HCS Locations&apos;, 		TGS_Service__c = &apos;HCS License&apos;, 		TGS_Service__c = &apos;HCS Open Order&apos;, 		TGS_Service__c = &apos;Domestic Number&apos;, 		TGS_Service__c = &apos;International Number&apos;, 		TGS_Service__c = &apos;Setup and Add-ons&apos;, 		TGS_Service__c = &apos;WebEx&apos; 	), 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), 		ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;) 	), 	OR( 		Account.TGS_Aux_Holding__r.Name = &apos;Nokia&apos;, 		Account.Name = &apos;Nokia&apos; 	) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Multiservice Provision Controller w3</fullName>
        <actions>
            <name>TGS_Case_assign_to_Multiservice_Provisio</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule contains all the scenarios that trigger an owner change towards Multiservice Provision Controller</description>
        <formula>AND( 	TGS_Service__c = &apos;Fixed Voice&apos;, 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp;	ISPICKVAL(TGS_Status_reason__c, &apos;Provision Start&apos;), 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;Provision Start&apos;)	 	), 	AND( 		NOT(Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;), 		NOT(Account.Name = &apos;ACME&apos;) 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Multiservice Purchase</fullName>
        <actions>
            <name>TGS_Case_assign_to_Multiservice_Purchase</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule contains all the scenarios that trigger an owner change towards Multiservice Purchase</description>
        <formula>AND(  	OR(  		TGS_Service__c = &apos;Contact Centre&apos;, TGS_Service__c = &apos;Video&apos;, TGS_Service__c = &apos;ToIP&apos;, TGS_Service__c = &apos;HCS Video&apos;, TGS_Service__c = &apos;HCS ToIP&apos;, TGS_Service__c = &apos;Cisco HCS Service&apos;, TGS_Service__c = &apos;HCS Locations&apos;, TGS_Service__c = &apos;HCS Open Order&apos;, TGS_Service__c = &apos;HCS License&apos;  	),  	OR(  		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Off-Net&apos;), ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;Off-Net Supplier Data Checked&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;SO Launched(&amp; POF Required)&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;SO Accepted&apos;) )  	),  	OR(  		Account.TGS_Aux_Holding__r.Name = &apos;Nokia&apos;, Account.Name = &apos;Nokia&apos;  	)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Multiservice Purchase W3</fullName>
        <actions>
            <name>TGS_Case_assign_to_Multiservice_Purchase</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule contains all the scenarios that trigger an owner change towards Multiservice Purchase</description>
        <formula>AND( 	TGS_Service__c = &apos;Fixed Voice&apos;, 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( 			ISPICKVAL(TGS_Status_reason__c, &apos;Off-Net Supplier Data Checked&apos;), 			ISPICKVAL(TGS_Status_reason__c, &apos;SO Launched (&amp; POF Required)&apos;), 			ISPICKVAL(TGS_Status_reason__c, &apos;POF Signed&apos;),			 			ISPICKVAL(TGS_Status_reason__c, &apos;SO Accepted&apos;) 		), 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation - Off-Net&apos;)	 	), 	AND( 		NOT(Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;), 		NOT(Account.Name = &apos;ACME&apos;) 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Order Handling</fullName>
        <actions>
            <name>TGS_Case_assign_to_Order_Handling</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule contains all the scenarios that trigger an owner change towards Order Handling</description>
        <formula>OR(      AND(         OR(       TGS_Service__c = &apos;UC Mobile Service&apos;,       TGS_Service__c = &apos;UC Mobile Users&apos;,       TGS_Service__c = &apos;UC Mobile Open Order&apos;,      TGS_Service__c = &apos;mSIP Site Trunk&apos;,       TGS_Service__c = &apos;mSIP Site No Trunk&apos;,      TGS_Service__c = &apos;mSIP Site Hosted UC&apos;,       TGS_Service__c = &apos;mSIP Site ADMIN&apos;,       TGS_Service__c = &apos;mSIP Site CCA&apos;,       TGS_Service__c = &apos;Setup and Add-ons&apos;,       TGS_Service__c = &apos;WebEx&apos;,       TGS_Service__c = &apos;International Number&apos;,      TGS_Service__c = &apos;Domestic Number&apos;,       TGS_Service__c = &apos;Contact Centre&apos;,      TGS_Service__c = &apos;Video&apos;,       TGS_Service__c = &apos;ToIP&apos;,      TGS_Service__c = &apos;HCS Video&apos;,       TGS_Service__c = &apos;HCS ToIP&apos;,      TGS_Service__c = &apos;Cisco HCS Service&apos;,       TGS_Service__c = &apos;HCS Locations&apos;,       TGS_Service__c = &apos;HCS Open order&apos;,      TGS_Service__c = &apos;Flat Rate&apos;    ),    OR(       AND(        ISPICKVAL(Status, &apos;Assigned&apos;),        NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;))      ),      ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Order Handling&apos;),      ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;Pending Task Resolution&apos;)     ),    OR(       ISPICKVAL( Account.TGS_Aux_Customer_Country__r.TGS_SMC_Assigned_L2__c , &apos;73&apos;),      ISPICKVAL( Account.TGS_SMC_Assigned_L2__c , &apos;73&apos;)     ),    OR(       ISCHANGED(TGS_Status_reason__c),      ISCHANGED(Status)     )      ),     AND(     TGS_Service__c = &apos;Internet Resale&apos;,     OR(       AND(        ISPICKVAL(Status, &apos;Assigned&apos;),        NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;))      ),      ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Order Handling&apos;),      ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Off-Net&apos;)     ),    OR(       ISPICKVAL( Account.TGS_Aux_Customer_Country__r.TGS_SMC_Assigned_L2__c , &apos;73&apos;),      ISPICKVAL( Account.TGS_SMC_Assigned_L2__c , &apos;73&apos;)     ),    OR(       ISCHANGED(TGS_Status_reason__c),      ISCHANGED(Status)     )      ),     AND(     OR(       TGS_Service__c = &apos;DDI Standard&apos;,      TGS_Service__c = &apos;DDI Resale&apos;,      TGS_Service__c = &apos;Special Number&apos;,      TGS_Service__c = &apos;PNP&apos;    ),    OR(       AND(        ISPICKVAL(Status, &apos;Assigned&apos;),        NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)),         CreatedBy.Profile.Name =&apos;TGS Order Handling&apos;      ),      ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Order Handling&apos;) ,       ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;Pending Task Resolution&apos;)     ),    OR(       ISPICKVAL( Account.TGS_Aux_Customer_Country__r.TGS_SMC_Assigned_L2__c , &apos;73&apos;),      ISPICKVAL( Account.TGS_SMC_Assigned_L2__c , &apos;73&apos;)     ),    OR(       ISCHANGED(TGS_Status_reason__c),      ISCHANGED(Status)     )        ),   AND (     RecordType.DeveloperName = &apos;Order_Management_Case&apos;,     Account.TGS_Aux_Holding__r.Name = &apos;PITNEY BOWES&apos;,     TGS_Service__c = &apos;Internet Resale&apos;,     OR(       ISPICKVAL(Status, &apos;Assigned&apos;),       ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Order Handling&apos;),       ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Off-Net&apos;)      ),     OR( ISCHANGED(TGS_Status_reason__c), ISCHANGED(Status))   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Provision Siptrunk</fullName>
        <actions>
            <name>TGS_Case_assign_to_Provision_Siptrunk</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule contains all the scenarios that trigger an owner change towards Provision Siptrunk</description>
        <formula>AND(  	OR(  		AND(  			/* Broadsoft */  			OR( TGS_Service__c = &apos;UC Mobile Service&apos;, TGS_Service__c = &apos;UC Mobile Licenses&apos;, TGS_Service__c = &apos;UC Mobile Users&apos;, TGS_Service__c = &apos;UC Mobile Open Order&apos; ),  			OR(  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;Off-Net Supplier Data Checked&apos;), ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;SO Launched(&amp; POF Required)&apos;), ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;SO Accepted&apos;), ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Configuration&apos;), ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;), ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;), ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Off-Net&apos;)  			)  		),  		AND(  		/* mSIP */  			OR(  				TGS_Service__c = &apos;mSIP Site Trunk&apos;, TGS_Service__c = &apos;mSIP Site No Trunk&apos;, TGS_Service__c = &apos;mSIP Site Hosted UC&apos;, TGS_Service__c = &apos;mSIP Site ADMIN&apos;, TGS_Service__c = &apos;mSIP Site CCA&apos;, TGS_Service__c = &apos;DDI Standard&apos;, TGS_Service__c = &apos;DDI Resale&apos;, TGS_Service__c = &apos;Special Number&apos;, TGS_Service__c = &apos;PNP&apos;, TGS_Service__c = &apos;Flat Rate&apos;  			),  			OR(  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;Off-Net Supplier Data Checked&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;SO Launched(&amp; POF Required)&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;POF Signed&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;SO Accepted&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;),  				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;),  				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;),  				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Off-Net&apos;)	 			)  		)  	),  	AND( Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;ACME&apos;, Account.Name &lt;&gt; &apos;ACME&apos;, Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;ALLIANZ&apos; )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Provision VPN</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Provision_VPN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(    TGS_Service__c = &apos;Internet Resale&apos;,    OR(      ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;),      ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;)    ) ,    OR(     Account.TGS_Aux_Holding__r.Name = &apos;Nokia&apos;,      Account.Name = &apos;Nokia&apos;,     Account.TGS_Aux_Holding__r.Name = &apos;PITNEY BOWES&apos;   )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A SAS</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule contains all the scenarios that trigger an owner change towards SAS</description>
        <formula>AND( /* Broadsoft */   OR(    TGS_Service__c = &apos;UC Mobile Service&apos;, TGS_Service__c = &apos;UC Mobile Licenses&apos;,    TGS_Service__c = &apos;UC Mobile Users&apos;, TGS_Service__c = &apos;UC Mobile Open Order&apos;   ),    ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A SMC EMEA L1 Ordering</fullName>
        <actions>
            <name>TGS_Case_assign_to_EMEA_L1_Ordering</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule contains all the scenarios that trigger an owner change towards SMC EMEA L1 Ordering</description>
        <formula>OR( 	 	 
	AND( 
		/* mSIP */ 		 		 
		OR( 			 			 
			TGS_Service__c = &apos;mSIP Site Trunk&apos;, 			 			 
			TGS_Service__c = &apos;mSIP Site No Trunk&apos;, 			 			 
			TGS_Service__c = &apos;mSIP Site Hosted UC&apos;, 			 			 
			TGS_Service__c = &apos;mSIP Site ADMIN&apos;, 			 
			TGS_Service__c = &apos;mSIP Site CCA&apos;, 			 
			TGS_Service__c = &apos;DDI Standard&apos;, 			 
			TGS_Service__c = &apos;DDI Resale&apos;, 			 
			TGS_Service__c = &apos;Special Number&apos;, 			 
			TGS_Service__c = &apos;PNP&apos;, 			 
			TGS_Service__c = &apos;FLAT RATE&apos; 		 		 
		), 		 		 
		OR( 			 			 
			AND( 				 				 
				ISPICKVAL(Status, &apos;Assigned&apos;)/* &amp;&amp; NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;))*/, 				 				 
				CreatedBy.Profile.Name &lt;&gt; &apos;TGS Order Handling&apos; 			 			
			), 			 			
			ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 			 			
			ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR(ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;),ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;),ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;),ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;)), 			 			ISPICKVAL(Status, &apos;Resolved&apos;)
		), 		 		
		OR( 		 			
			Account.TGS_Aux_Holding__r.Name = &apos;GCC&apos;, 		 			
			Account.TGS_Aux_Holding__r.Name = &apos;NH Hoteles&apos;, 		 			
			Account.TGS_Aux_Holding__r.Name = &apos;Vestas Wind Systems A/S&apos;, 		 			
			Account.Name = &apos;GCC&apos;, 		 			Account.Name = &apos;NH Hoteles&apos;, 		 			
			Account.Name = &apos;Vestas Wind Systems A/S&apos; 		 		
		), 		 		
		OR( 			 			
			ISCHANGED(TGS_Status_reason__c), 			 			
			ISCHANGED(Status) 		 		
		) 	 	
	), 	 	
	AND(
		/* OLA 1 */ 		 		
		OR( 			 			
			RecordType.DeveloperName = &apos;TGS_Billing_Inquiry&apos;, 			 			
			RecordType.DeveloperName = &apos;Order_Management_Case&apos; 		 		
		), 		 		
		OR( 			 			
			AND( 				 				
				ISPICKVAL(Status, &apos;Assigned&apos;), 				 				
				NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)) 			 			
			), 			 			
			ISPICKVAL(Status, &apos;Cancelled&apos;), 			 			
			ISPICKVAL(Status, &apos;Rejected&apos;), 			 			
			ISPICKVAL(Status, &apos;Resolved&apos;), 			 			
			ISPICKVAL(Status, &apos;Closed&apos;), 			 			
			ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;In SD Validation&apos;), 			 			
			ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; 				 			
			OR( 					 				
				ISPICKVAL(TGS_Status_reason__c, &apos;In SD Validation&apos;), 					 				
				ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation - Close&apos;) 				 			
			) 		 		
		), 		 		
		OR( 			 			
			Account.TGS_Aux_Holding__r.Name = &apos;Delair&apos;, 			 			
			Account.Name = &apos;Delair&apos;,					 			
			Account.TGS_Aux_Holding__r.Name = &apos;Lyonnaise des Eaux&apos;, 			 			
			Account.Name = &apos;Lyonnaise des Eaux&apos; 		 		
		)
	/*,OR(ISCHANGED(TGS_Status_reason__c),ISCHANGED(Status))*/ 	 	
	)  
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A SMC EMEA L1 Ordering 2</fullName>
        <actions>
            <name>TGS_Case_assign_to_SMC_EMEA_L1_Ordering</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR( 	AND( /* mWAN */  		OR( 			TGS_Service__c = &apos;mWan&apos;, 			TGS_Service__c = &apos;mWan - Internet&apos; 		), 		OR( 			ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), 			ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp; OR( 				ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 				ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;) 			), 			ISPICKVAL(Status, &apos;Resolved&apos;), 			ISPICKVAL(Status, &apos;Closed&apos;) 		), 		AND( 			Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;HP&apos;, 			Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;Nokia&apos;, Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;ALLIANZ&apos;, 			Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;Trafigura&apos;, 			Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;CEMEX&apos;, 			Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;ACME&apos;, 			Account.Name &lt;&gt; &apos;HP&apos;, 			Account.Name &lt;&gt; &apos;Nokia&apos;, 			Account.Name &lt;&gt; &apos;Trafigura&apos;, 			Account.Name &lt;&gt; &apos;CEMEX&apos;, 			Account.Name &lt;&gt; &apos;ACME&apos; 		), 		OR( 			ISCHANGED(TGS_Status_reason__c), 			ISCHANGED(Status) 		) 	), 	AND( 		OR( 			RecordType.DeveloperName = &apos;TGS_Billing_Inquiry&apos;, 			RecordType.DeveloperName = &apos;Order_Management_Case&apos; 		), 		OR( 			AND( 				ISPICKVAL(Status, &apos;Assigned&apos;), 				NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)) 			), 			ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( 				ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 				ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;) 			), 			ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 			ISPICKVAL(Status, &apos;Resolved&apos;), 			ISPICKVAL(Status, &apos;Closed&apos;) 		), 		OR( 			Account.TGS_Aux_Holding__r.Name = &apos;JCDecaux&apos;, 			Account.TGS_Aux_Holding__r.Name = &apos;Focus Mobile&apos;, 			Account.TGS_Aux_Holding__r.Name = &apos;NEC CORPORATION&apos;, 			Account.TGS_Aux_Holding__r.Name = &apos;Volvo&apos;, 			Account.Name = &apos;JCDecaux&apos;, 			Account.Name = &apos;Focus Mobile&apos;, 			Account.Name = &apos;NEC CORPORATION&apos;, 			Account.Name = &apos;Volvo&apos; 		), 		OR( 			ISCHANGED(TGS_Status_reason__c), 			ISCHANGED(Status) 		) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A SMC EMEA L1 Ordering W3</fullName>
        <actions>
            <name>TGS_Case_assign_to_EMEA_L1_Ordering</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(	  OR(   AND(   OR(   TGS_Service__c = &apos;Blank SIM Emergency Stock&apos;,  TGS_Service__c = &apos;Bundles With Pooling&apos;,  TGS_Service__c = &apos;Device Emergency Stock&apos;,  TGS_Service__c = &apos;Mobile Accesory&apos;,  TGS_Service__c = &apos;Mobile Device&apos;,  TGS_Service__c = &apos;Mobile Intranet&apos;,  TGS_Service__c = &apos;SIM Card Suscription&apos;,  TGS_Service__c = &apos;White/Black Lists&apos;   ),   OR(   ISPICKVAL(Status, &apos;Assigned&apos;)/* &amp;&amp; NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;))*/, ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;)   ),   ISPICKVAL(Status, &apos;Resolved&apos;)   )   ),   AND(   TGS_Service__c = &apos;Fixed Voice&apos;,   OR(  ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;In Customer Configuration&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;) ),  ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;In Customer Configuration&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;)   ),   ISPICKVAL(Status, &apos;Resolved&apos;)   )   )   ),   AND(   OR(   Account.TGS_Aux_Holding__r.Name = &apos;G4S&apos;,  Account.TGS_Aux_Holding__r.Name = &apos;Fashion Partner Group&apos;  ),	  NOT(Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;), NOT(Account.Name = &apos;ACME&apos;), NOT(Account.TGS_Aux_Holding__r.Name = &apos;Repsol&apos;), NOT(Account.Name = &apos;Repsol&apos;)   )/*,	  OR(   ISCHANGED(TGS_Status_reason__c), ISCHANGED(Status)   )*/	  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A SMC EMEA L2 Ordering W3</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_SMC_L2_Ordering_EMEA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(	 	OR( 		AND( 			OR( 				TGS_Service__c = &apos;Blank SIM Emergency Stock&apos;, 				TGS_Service__c = &apos;Bundles With Pooling&apos;, 				TGS_Service__c = &apos;Device Emergency Stock&apos;, 				TGS_Service__c = &apos;Mobile Accesory&apos;, 				TGS_Service__c = &apos;Mobile Device&apos;, 				TGS_Service__c = &apos;Mobile Intranet&apos;, 				TGS_Service__c = &apos;SIM Card Suscription&apos;, 				TGS_Service__c = &apos;White/Black Lists&apos; 			),	 			AND( 				NOT(Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;), 				NOT(Account.Name = &apos;ACME&apos;), 				NOT(Account.TGS_Aux_Holding__r.Name = &apos;Repsol&apos;), 				NOT(Account.Name = &apos;Repsol&apos;) 			)			 		), 		AND( 			OR( 				TGS_Service__c= &apos;Mobile Line&apos;, 				TGS_Service__c= &apos;Services&apos; 			), 			OR( 				Account.TGS_Aux_Customer_Country__r.Name = &apos;Maersk - Norway&apos;, 				Account.TGS_Aux_Customer_Country__r.Name = &apos;Maersk - Sweeden&apos;, 				Account.TGS_Aux_Customer_Country__r.Name = &apos;Maersk - Singapore&apos;, 				Account.TGS_Aux_Holding__r.Name = &apos;Fashion Partner Group&apos;, 				Account.Name = &apos;Fashion Partner Group&apos;, 				Account.TGS_Aux_Holding__r.Name = &apos;G4S&apos;, 				Account.Name = &apos;G4S&apos;, 				Account.TGS_Aux_Holding__r.Name = &apos;IMMOCHAN&apos;, 				Account.Name = &apos;IMMOCHAN&apos;, 				Account.TGS_Aux_Holding__r.Name = &apos;MAGNA&apos;, 				Account.Name = &apos;MAGNA&apos; 			) 		) 	), 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;) 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A SMC L1 EMEA Ordering</fullName>
        <actions>
            <name>TGS_Case_assign_to_SMC_EMEA_L1_Ordering</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule contains all the scenarios that trigger an owner change towards SMC EMEA L1 Ordering</description>
        <formula>OR( 	AND( /* mWAN */  		OR( 			TGS_Service__c = &apos;mWan&apos;, 			TGS_Service__c = &apos;mWan - Internet&apos; 		), 		OR( 			ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), 			ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp; OR( 				ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 				ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;) 			), 			ISPICKVAL(Status, &apos;Resolved&apos;), 			ISPICKVAL(Status, &apos;Closed&apos;) 		), 		AND( 			Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;HP&apos;, 			Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;Nokia&apos;, 			Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;CEMEX&apos;, 			Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;ACME&apos;, 			Account.Name &lt;&gt; &apos;HP&apos;, 			Account.Name &lt;&gt; &apos;Nokia&apos;, 			Account.Name &lt;&gt; &apos;CEMEX&apos;, 			Account.Name &lt;&gt; &apos;ACME&apos; 		), 		OR( 			ISCHANGED(TGS_Status_reason__c), 			ISCHANGED(Status) 		) 	), 	AND( 		OR( 			RecordType.DeveloperName = &apos;TGS_Billing_Inquiry&apos;, 			RecordType.DeveloperName = &apos;Order_Management_Case&apos; 		), 		OR( 			AND( 				ISPICKVAL(Status, &apos;Assigned&apos;), 				NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)) 			), 			ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( 				ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 				ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;) 			), 			ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 			ISPICKVAL(Status, &apos;Resolved&apos;), 			ISPICKVAL(Status, &apos;Closed&apos;) 		), 		OR(                         Account.TGS_Aux_Holding__r.Name = &apos;Trafigura&apos;, 			Account.TGS_Aux_Holding__r.Name = &apos;JCDecaux&apos;, 			Account.TGS_Aux_Holding__r.Name = &apos;Focus Mobile&apos;, 			Account.TGS_Aux_Holding__r.Name = &apos;NEC CORPORATION&apos;, 			Account.TGS_Aux_Holding__r.Name = &apos;Volvo&apos;, 			Account.Name = &apos;JCDecaux&apos;, 			Account.Name = &apos;Focus Mobile&apos;, 			Account.Name = &apos;NEC CORPORATION&apos;, 			Account.Name = &apos;Volvo&apos;,                         Account.Name = &apos;Trafigura&apos;, Account.TGS_Aux_Holding__r.Name = &apos;GRDF&apos;, Account.TGS_Aux_Holding__r.Name = &apos;ORES&apos; 		), 		OR( 			ISCHANGED(TGS_Status_reason__c), 			ISCHANGED(Status) 		) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A SMC Miami L1 Ordering GA</fullName>
        <actions>
            <name>TGS_Case_assign_to_SMC_Miami_L1_Ord_GA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule contains all the scenarios that trigger an owner change towards SMC Miami L1 Ordering GA</description>
        <formula>OR(   AND(      OR( TGS_Service__c = &apos;mWan&apos;, TGS_Service__c = &apos;mWan - Internet&apos; ), OR( ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;) ), ISPICKVAL(Status, &apos;Resolved&apos;), ISPICKVAL(Status, &apos;Closed&apos;) ), OR( Account.TGS_Aux_Holding__r.Name = &apos;HP&apos;, Account.Name = &apos;HP&apos;,Account.TGS_Aux_Holding__r.Name = &apos;Paveca&apos; ), OR( ISCHANGED(TGS_Status_reason__c), ISCHANGED(Status) )    ),    AND(      OR( TGS_Service__c = &apos;mSIP Site Trunk&apos;, TGS_Service__c = &apos;mSIP Site No Trunk&apos;, TGS_Service__c = &apos;mSIP Site Hosted UC&apos;, TGS_Service__c = &apos;mSIP Site ADMIN&apos;, TGS_Service__c = &apos;mSIP Site CCA&apos;, TGS_Service__c = &apos;FLAT RATE&apos; ), OR( ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;) ), ISPICKVAL(Status, &apos;Resolved&apos;) ), AND( NOT(Account.TGS_Aux_Holding__r.Name = &apos;GCC&apos;), NOT(Account.TGS_Aux_Holding__r.Name = &apos;Nokia&apos;), NOT(Account.TGS_Aux_Holding__r.Name = &apos;Virtual ITFS&apos;), NOT(Account.TGS_Aux_Holding__r.Name = &apos;NH Hoteles&apos;), NOT(Account.TGS_Aux_Holding__r.Name = &apos;Vestas Wind Systems A/S&apos;), NOT(Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;), NOT(Account.Name = &apos;ACME&apos;), NOT(Account.Name = &apos;Nokia&apos;), NOT(Account.Name = &apos;Virtual ITFS&apos;), NOT(Account.Name = &apos;GCC&apos;), NOT(Account.Name = &apos;NH Hoteles&apos;), NOT(Account.Name = &apos;Vestas Wind Systems A/S&apos;) ), OR( ISCHANGED(TGS_Status_reason__c), ISCHANGED(Status) )    ),    AND(     OR( TGS_Service__c = &apos;DDI Standard&apos;, TGS_Service__c = &apos;DDI Resale&apos;, TGS_Service__c = &apos;Special Number&apos;, TGS_Service__c = &apos;PNP&apos; ), OR( AND( ISPICKVAL(Status, &apos;Assigned&apos;)/*, NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;))*/, CreatedBy.Profile.Name &lt;&gt; &apos;TGS Order Handling&apos; ), ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;) ), ISPICKVAL(Status, &apos;Resolved&apos;) ), AND( NOT(Account.TGS_Aux_Holding__r.Name = &apos;GCC&apos;), NOT(Account.TGS_Aux_Holding__r.Name = &apos;Nokia&apos;), NOT(Account.TGS_Aux_Holding__r.Name = &apos;Virtual ITFS&apos;), NOT(Account.TGS_Aux_Holding__r.Name = &apos;NH Hoteles&apos;), NOT(Account.TGS_Aux_Holding__r.Name = &apos;Vestas Wind Systems A/S&apos;), NOT(Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;), NOT(Account.Name = &apos;ACME&apos;), NOT(Account.Name = &apos;Nokia&apos;), NOT(Account.Name = &apos;Virtual ITFS&apos;), NOT(Account.Name = &apos;GCC&apos;), NOT(Account.Name = &apos;NH Hoteles&apos;), NOT(Account.Name = &apos;Vestas Wind Systems A/S&apos;) )   ),   AND(     RecordType.DeveloperName = &apos;Order_Management_Case&apos;,     Account.TGS_Aux_Holding__r.Name = &apos;PITNEY BOWES&apos;,     TGS_Service__c = &apos;Colocation/Housing&apos;,     OR(       ISPICKVAL(Status, &apos;Assigned&apos;),       ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;),       ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;),       ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation – Close&apos;),       ISPICKVAL(Status, &apos;Resolved&apos;),       ISPICKVAL(Status, &apos;Closed&apos;)     ),     OR( ISCHANGED(TGS_Status_reason__c), ISCHANGED(Status) )   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A SMC Miami L1 Ordering NSN Operators</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_SMC_Miami_L1_Ordering_NSN_Ope</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  	OR( 		AND( 			OR( 				TGS_Service__c = &apos;mWan&apos;, 				TGS_Service__c = &apos;mWan - Internet&apos; 			), 			OR( 				ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), 				ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( 					ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 					ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;) 				), 				ISPICKVAL(Status, &apos;Resolved&apos;), 				ISPICKVAL(Status, &apos;Closed&apos;) 			) 		), 		AND( 			TGS_Service__c = &apos;Internet Resale&apos;, 			OR( 				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp; OR( 					ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), 					ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;), 					ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;) 				), 				ISPICKVAL(Status, &apos;Resolved&apos;) 			) 		), 		AND( 			TGS_Service__c = &apos;Colocation/Housing&apos;, 			OR( 				AND( 					ISPICKVAL(Status, &apos;Assigned&apos;), 					NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)) 				), 				AND( 					NOT(ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( 						ISPICKVAL(TGS_Status_reason__c,&apos;Financial Risk&apos;), 						ISPICKVAL(TGS_Status_reason__c,&apos;Quotation (Presales Phase)&apos;), 						ISPICKVAL(TGS_Status_reason__c,&apos;Quotation (Presales Phase - OB/Supplier)&apos;), 						ISPICKVAL(TGS_Status_reason__c,&apos;Quotation (Presales Phase - Cost)&apos;) 					)), 					NOT(ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL( TGS_Status_reason__c,&apos;In Cancellation - Not Paid&apos;)) 				) 			) 		) 	), 	OR(  		Account.TGS_Aux_Holding__r.Name = &apos;Nokia&apos;, 		Account.Name = &apos;Nokia&apos; 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	)	 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A SMC Miami L1 Ordering NSN Voice</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_SMC_Miami_L1_Ordering_NSN_Voi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( OR( AND( OR( TGS_Service__c = &apos;UC Mobile Service&apos;, TGS_Service__c = &apos;UC Mobile Users&apos;,TGS_Service__c = &apos;UC Mobile Open Order&apos;, TGS_Service__c = &apos;mSIP Site Trunk&apos;,TGS_Service__c = &apos;mSIP Site No Trunk&apos;, TGS_Service__c = &apos;mSIP Site Hosted UC&apos;,TGS_Service__c = &apos;mSIP Site ADMIN&apos;, TGS_Service__c = &apos;FLAT RATE&apos;,TGS_Service__c = &apos;mSIP Site CCA&apos; ), OR( 		ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;), 	ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;), 	ISPICKVAL(Status, &apos;Resolved&apos;) ) ), 	AND( OR( AND( 	OR( 	TGS_Service__c = &apos;DDI Standard&apos;,TGS_Service__c = &apos;DDI Resale&apos;, 		TGS_Service__c = &apos;Special Number&apos;,TGS_Service__c = &apos;PNP&apos; ), CreatedBy.Profile.Name &lt;&gt; &apos;TGS Order Handling&apos; 	), 	TGS_Service__c = &apos;UC Mobile Licenses&apos; ), 	OR( AND( ISPICKVAL(Status, &apos;Assigned&apos;), NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)) ), ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 	ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;))	),AND( 	OR( TGS_Service__c = &apos;DDI Standard&apos;,TGS_Service__c = &apos;DDI Resale&apos;, 	TGS_Service__c = &apos;Special Number&apos;,TGS_Service__c = &apos;PNP&apos;, TGS_Service__c = &apos;UC Mobile Licenses&apos; ), 	OR( ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;), 	ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;), ISPICKVAL(Status, &apos;Resolved&apos;) 	) 	), AND( OR(TGS_Service__c = &apos;Contact Centre&apos;,TGS_Service__c = &apos;Video&apos;, 				TGS_Service__c = &apos;ToIP&apos;,TGS_Service__c = &apos;HCS Video&apos;, 	TGS_Service__c = &apos;HCS ToIP&apos;,TGS_Service__c = &apos;Cisco HCS Service&apos;, 	TGS_Service__c = &apos;HCS Locations&apos;,TGS_Service__c = &apos;HCS Open Order&apos;, TGS_Service__c = &apos;Domestic Number&apos;,TGS_Service__c = &apos;International Number&apos;, TGS_Service__c = &apos;Setup and Add-ons&apos;,TGS_Service__c = &apos;WebEx&apos; 	), 	OR(ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;), ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;), ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;), 	ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;), 				ISPICKVAL(Status, &apos;Resolved&apos;) 		) 	), 	AND( 	TGS_Service__c = &apos;HCS License&apos;,OR( AND( 				ISPICKVAL(Status, &apos;Assigned&apos;), 				NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)) 		), 	ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 	ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;), 		ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 	ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;), 				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), 	ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;), 	ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;), ISPICKVAL(Status, &apos;Resolved&apos;) ) ) 	), 	OR( 	Account.TGS_Aux_Holding__r.Name = &apos;Nokia&apos;, 	Account.Name = &apos;Nokia&apos; 	), 	OR( 	ISCHANGED(TGS_Status_reason__c), 	ISCHANGED(Status)	))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A SipNoc</fullName>
        <actions>
            <name>TGS_Case_assign_to_SipNOC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule contains all the scenarios that trigger an owner change towards SipNoc</description>
        <formula>OR( AND( OR( TGS_Service__c = &apos;mSIP Site Trunk&apos;, TGS_Service__c = &apos;mSIP Site No Trunk&apos;, TGS_Service__c = &apos;mSIP Site Hosted UC&apos;, TGS_Service__c = &apos;mSIP Site ADMIN&apos;, TGS_Service__c = &apos;mSIP Site CCA&apos;, TGS_Service__c = &apos;FLAT RATE&apos; ), ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Configuration&apos;), AND( Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;ACME&apos;, Account.Name &lt;&gt; &apos;ACME&apos; ) ), AND( OR( TGS_Service__c = &apos;DDI Standard&apos;, TGS_Service__c = &apos;DDI Resale&apos;, TGS_Service__c = &apos;Special Number&apos;, TGS_Service__c = &apos;PNP&apos; ), ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Configuration&apos;) &amp;&amp; OR(CreatedBy.Profile.Name =&apos;TGS Integration User&apos;, CreatedBy.Profile.Name =&apos;TGS Order Handling&apos;), AND( Account.TGS_Aux_Holding__r.Name &lt;&gt; &apos;ACME&apos;, Account.Name &lt;&gt; &apos;ACME&apos; ) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A TGS SMC Madrid L1 Ordering</fullName>
        <actions>
            <name>TGS_Case_Assign_SMC_Madrid_L1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(	 OR(	 AND(  OR(  TGS_Service__c = &apos;Blank SIM Emergency Stock&apos;,  TGS_Service__c = &apos;Bundles With Pooling&apos;,  TGS_Service__c = &apos;Device Emergency Stock&apos;,  TGS_Service__c = &apos;Mobile Accesory&apos;,  TGS_Service__c = &apos;Mobile Device&apos;, TGS_Service__c = &apos;Mobile Intranet&apos;,  TGS_Service__c = &apos;SIM Card Suscription&apos;,  TGS_Service__c = &apos;White/Black Lists&apos;,  TGS_Service__c= &apos;Mobile Line&apos;  ),  OR(  ISPICKVAL(Status, &apos;Assigned&apos;)/* &amp;&amp; NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;))*/	,  ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;)  ),  ISPICKVAL(Status, &apos;Resolved&apos;)  )	 ),  AND(  TGS_Service__c = &apos;Services&apos;,  OR(  ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;)  ),  ISPICKVAL(Status, &apos;Resolved&apos;)  )	 )	 ),  OR(  Account.TGS_Aux_Holding__r.Name = &apos;Repsol&apos;, Account.Name = &apos;Repsol&apos;, Account.TGS_Aux_Holding__r.Name = &apos;Ministerio de Empleo y Seguridad Social&apos;,  Account.Name = &apos;Ministerio de Empleo y Seguridad Social&apos;, Account.TGS_Aux_Holding__r.Name = &apos;Iberdrola&apos;, Account.Name = &apos;Iberdrola&apos;, Account.TGS_Aux_Customer_Country__r.Name = &apos;Maersk - Spain&apos;	)/*,  OR(  ISCHANGED(TGS_Status_reason__c), ISCHANGED(Status)  )*/	 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A TGS SMC Madrid L2 Ordering</fullName>
        <actions>
            <name>TGS_Case_Assign_SMC_Madrid_L2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(	 	OR(	 		AND( 			OR( 				TGS_Service__c = &apos;Blank SIM Emergency Stock&apos;, 				TGS_Service__c = &apos;Bundles With Pooling&apos;, 				TGS_Service__c = &apos;Device Emergency Stock&apos;, 				TGS_Service__c = &apos;Mobile Accesory&apos;, 				TGS_Service__c = &apos;Mobile Device&apos;, 				TGS_Service__c = &apos;Mobile Intranet&apos;, 				TGS_Service__c = &apos;SIM Card Suscription&apos;, 				TGS_Service__c = &apos;White/Black Lists&apos;	 			), 			OR( 				Account.TGS_Aux_Holding__r.Name = &apos;Repsol&apos;, 				Account.Name = &apos;Repsol&apos; 			)	 		), 		AND( 			OR( 				TGS_Service__c= &apos;Mobile Line&apos;,  				TGS_Service__c= &apos;Services&apos; 			), 			OR( 				Account.TGS_Aux_Customer_Country__r.Name = &apos;Iberdrola - Portugal&apos;, 				Account.TGS_Aux_Customer_Country__r.Name = &apos;Iberdrola - Spain&apos;, 				Account.TGS_Aux_Customer_Country__r.Name = &apos;Iberdrola - United Kingdom&apos;, 				Account.TGS_Aux_Customer_Country__r.Name = &apos;Maersk - Spain&apos;, 				Account.TGS_Aux_Customer_Country__r.Name = &apos;Ministerio de Empleo y Seguridad Social&apos;	 			) 		) 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	), 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;) 	)	 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A TGS SMC Miami L2 Ordering Mobile W3</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_SMC_L2_Ordering_Miami</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR(    AND(     OR(TGS_Service__c = &apos;Mobile Line&apos;,     TGS_Service__c= &apos;Services&apos;  ),       OR(       ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;),     ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;)      ),       OR(       Account.TGS_Aux_Customer_Country__r.Name = &apos;Iberdrola - Mexico&apos;, Account.TGS_Aux_Customer_Country__r.Name = &apos;Maersk - Nicaragua&apos;,      Account.TGS_Aux_Customer_Country__r.Name = &apos;Maersk - Panama&apos;,       Account.TGS_Aux_Customer_Country__r.Name = &apos;Maersk - Peru&apos;,Account.TGS_Aux_Customer_Country__r.Name = &apos;Maersk - Uruguay&apos;,      Account.TGS_Aux_Customer_Country__r.Name = &apos;Maersk - Venezuela&apos;        ),       OR(ISCHANGED(TGS_Status_reason__c),ISCHANGED(Status))     ),   AND (     RecordType.DeveloperName = &apos;Order_Management_Case&apos;,     Account.TGS_Aux_Holding__r.Name = &apos;PITNEY BOWES&apos;,     TGS_Service__c = &apos;Colocation/Housing&apos;,     OR(       ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;),       ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;),       ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;)     ),     OR( ISCHANGED(TGS_Status_reason__c), ISCHANGED(Status))   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test CEDEX CC</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_CEDEX_CC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR( 	AND( 		OR(  			TGS_Service__c = &apos;Contact Centre&apos;,  			TGS_Service__c = &apos;Video&apos;,  			TGS_Service__c = &apos;ToIP&apos;, 			TGS_Service__c = &apos;Cisco HCS Service&apos;, 			TGS_Service__c = &apos;HCS Locations&apos;, 			TGS_Service__c = &apos;HCS Open Order&apos;, 			TGS_Service__c = &apos;HCS Video&apos;, 			TGS_Service__c = &apos;HCS ToIP&apos;, 			TGS_Service__c = &apos;HCS License&apos;, 			TGS_Service__c = &apos;Domestic Number&apos;, 			TGS_Service__c = &apos;International Number&apos;, 			TGS_Service__c = &apos;Setup and Add-ons&apos;, 			TGS_Service__c = &apos;WebEx&apos;  		),  		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Configuration&apos;), 		OR(  			Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;,  			Account.Name = &apos;ACME&apos;  		)  	), 	AND( 		OR(  			TGS_Service__c = &apos;DDI Resale&apos;, 			TGS_Service__c = &apos;DDI Standard&apos;, 			TGS_Service__c = &apos;Special Number&apos;, 			TGS_Service__c = &apos;PNP&apos;  		),  		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Configuration&apos;) &amp;&amp; CreatedBy.Profile.Name &lt;&gt; &apos;TGS Order Handling&apos;, 		OR(  			Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;,  			Account.Name = &apos;ACME&apos;  		)  	) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test CEDEX CC Network Configuration</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_CEDEX_CC_Network_Configu</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	TGS_Service__c = &apos;Fixed Voice&apos;, 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp;	ISPICKVAL(TGS_Status_reason__c, &apos;In Network Configuration&apos;), 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;In Network Configuration&apos;)	 	), 	OR( 		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;, 		Account.Name = &apos;ACME&apos; 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test CEDEX CC Resources Allocation</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_test_CEDEX_CC_Resources_Alloc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	TGS_Service__c = &apos;Fixed Voice&apos;, 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp;	ISPICKVAL(TGS_Status_reason__c, &apos;In Assignment&apos;), 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;In Assignment&apos;)	 	), 	OR( 		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;, 		Account.Name = &apos;ACME&apos; 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test Demo L1 Ordering</fullName>
        <actions>
            <name>TGS_Case_assign_to_Demo_L1_Ordering</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule contains all the scenarios that trigger an owner change towards Test Demo L1 Ordering</description>
        <formula>AND( /* Demo Customer */  	OR(  		TGS_Service__c = &apos;Universal WIFI&apos;,  		TGS_Service__c = &apos;SMDM Management&apos;, 		TGS_Service__c = &apos;SMDM Inventory&apos; 	),  	OR(  		ISPICKVAL(Status, &apos;Assigned&apos;), 		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;),  		ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp; OR(  											ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;),  											ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;) 										),  		ISPICKVAL(Status, &apos;Resolved&apos;), 		ISPICKVAL(Status, &apos;Closed&apos;) 	),  	OR(  		Account.TGS_Aux_Holding__r.Name = &apos;Demo_Customer&apos;,  		Account.Name = &apos;Demo_Customer&apos;  	)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test Demo L2 Ordering</fullName>
        <actions>
            <name>TGS_Case_assign_to_Demo_L2_Ordering</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule contains all the scenarios that trigger an owner change towards Test Demo L2 Ordering</description>
        <formula>AND( /* Demo Customer */  	OR(  		TGS_Service__c = &apos;Universal WIFI&apos;,  		TGS_Service__c = &apos;SMDM Management&apos;, 		TGS_Service__c = &apos;SMDM Inventory&apos; 	),  	OR(  		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In provision&apos;),  		ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp; OR(  											ISPICKVAL(TGS_Status_reason__c,&apos;In provision&apos;),  											ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;) 										) 	),  	OR(  		Account.TGS_Aux_Holding__r.Name = &apos;Demo_Customer&apos;,  		Account.Name = &apos;Demo_Customer&apos;  	) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test MultiService Purchase</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_Multiservice_Purchase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  	OR( 		TGS_Service__c = &apos;Contact Centre&apos;, 		TGS_Service__c = &apos;Video&apos;, 		TGS_Service__c = &apos;ToIP&apos;, 		TGS_Service__c = &apos;HCS Video&apos;, 		TGS_Service__c = &apos;HCS ToIP&apos;, 		TGS_Service__c = &apos;Cisco HCS Service&apos;, 		TGS_Service__c = &apos;HCS Locations&apos;, 		TGS_Service__c = &apos;HCS Open Order&apos;, 		TGS_Service__c = &apos;HCS License&apos; 		), 		OR(  		ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Off-Net&apos;),  		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp; OR(  												ISPICKVAL(TGS_Status_reason__c,&apos;Off-Net Supplier Data Checked&apos;),  												ISPICKVAL(TGS_Status_reason__c,&apos;SO Launched (&amp; POF Required)&apos;), 												ISPICKVAL(TGS_Status_reason__c,&apos;SO Accepted&apos;) 											) 	),  	OR(  		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;,  		Account.Name = &apos;ACME&apos;  		)	  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test MultiService Purchase w3</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_Multiservice_Purchase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	TGS_Service__c = &apos;Fixed Voice&apos;, 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( 			ISPICKVAL(TGS_Status_reason__c, &apos;Off-Net Supplier Data Checked&apos;), 			ISPICKVAL(TGS_Status_reason__c, &apos;SO Launched (&amp; POF Required)&apos;), 			ISPICKVAL(TGS_Status_reason__c, &apos;POF Signed&apos;),			 			ISPICKVAL(TGS_Status_reason__c, &apos;SO Accepted&apos;) 		), 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;In Cancellation - Off-Net&apos;)	 	), 	OR( 		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;, 		Account.Name = &apos;ACME&apos; 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test Multiservice Provision Controller</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_Multiservice_Provision_Co</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  	OR(  		TGS_Service__c = &apos;Contact Centre&apos;,  		TGS_Service__c = &apos;Video&apos;,  		TGS_Service__c = &apos;ToIP&apos;, 		TGS_Service__c = &apos;HCS Video&apos;,  		TGS_Service__c = &apos;HCS ToIP&apos;,		 		TGS_Service__c = &apos;Cisco HCS Service&apos;,  		TGS_Service__c = &apos;HCS Locations&apos;, 		TGS_Service__c = &apos;HCS License&apos;, 		TGS_Service__c = &apos;HCS Open Order&apos;, 		TGS_Service__c = &apos;Domestic Number&apos;, 		TGS_Service__c = &apos;International Number&apos;, 		TGS_Service__c = &apos;Setup and Add-ons&apos;, 		TGS_Service__c = &apos;WebEx&apos;                 	 ),  	OR(  		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;),  		ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;) 	), 	OR(  		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;,  		Account.Name = &apos;ACME&apos;  	) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test Multiservice Provision Controller w3</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_Multiservice_Provision_Co</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	TGS_Service__c = &apos;Fixed Voice&apos;, 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp;	ISPICKVAL(TGS_Status_reason__c, &apos;Provision Start&apos;), 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c, &apos;Provision Start&apos;)	 	), 	OR( 		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;, 		Account.Name = &apos;ACME&apos; 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test Order Handling</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_Order_Handling</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR( 	AND( 		OR( 			TGS_Service__c = &apos;UC Mobile Service&apos;, 			TGS_Service__c = &apos;UC Mobile Licenses&apos;, 			TGS_Service__c = &apos;UC Mobile Users&apos;, 			TGS_Service__c = &apos;UC Mobile Open Order&apos;, 			TGS_Service__c = &apos;mSIP Site Trunk&apos;, 			TGS_Service__c = &apos;mSIP Site No Trunk&apos;, 			TGS_Service__c = &apos;mSIP Site Hosted UC&apos;, 			TGS_Service__c = &apos;mSIP Site ADMIN&apos;, 			TGS_Service__c = &apos;mSIP Site CCA&apos;, 			TGS_Service__c = &apos;Setup and Add-ons&apos;, 			TGS_Service__c = &apos;WebEx&apos;, 			TGS_Service__c = &apos;International Number&apos;, 			TGS_Service__c = &apos;Domestic Number&apos;, 			TGS_Service__c = &apos;Contact Centre&apos;, 			TGS_Service__c = &apos;Video&apos;, 			TGS_Service__c = &apos;ToIP&apos;, 			TGS_Service__c = &apos;HCS Video&apos;, 			TGS_Service__c = &apos;HCS ToIP&apos;, 			TGS_Service__c = &apos;Cisco HCS Service&apos;, 			TGS_Service__c = &apos;HCS Locations&apos;, 			TGS_Service__c = &apos;HCS Open Order&apos;, 			TGS_Service__c = &apos;FLAT RATE&apos; 		), 		OR( 			AND( 				ISPICKVAL(Status, &apos;Assigned&apos;), 				NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)) 			), 			ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Order Handling&apos;), 			ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;Pending Task Resolution&apos;) 		), 		OR( 			ISPICKVAL( Account.TGS_Aux_Customer_Country__r.TGS_SMC_Assigned_L2__c , &apos;28&apos;), 			ISPICKVAL( Account.TGS_Aux_Holding__r.TGS_SMC_Assigned_L2__c , &apos;28&apos;) 		), 		OR( 			ISCHANGED(TGS_Status_reason__c), 			ISCHANGED(Status) 		) 	), 	AND( 		TGS_Service__c = &apos;Internet Resale&apos;, 		OR( 			AND( 				ISPICKVAL(Status, &apos;Assigned&apos;), 				NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)) 			), 			ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Order Handling&apos;), 			ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Off-Net&apos;) 		), 		OR( 			ISPICKVAL( Account.TGS_Aux_Customer_Country__r.TGS_SMC_Assigned_L2__c , &apos;28&apos;), 			ISPICKVAL( Account.TGS_Aux_Holding__r.TGS_SMC_Assigned_L2__c , &apos;28&apos;) 		), 		OR( 			ISCHANGED(TGS_Status_reason__c), 			ISCHANGED(Status) 		) 	), 	AND( 		OR( 			TGS_Service__c = &apos;DDI Standard&apos;, 			TGS_Service__c = &apos;DDI Resale&apos;, 			TGS_Service__c = &apos;Special Number&apos;, 			TGS_Service__c = &apos;PNP&apos; 		), 		OR( 			AND( 				ISPICKVAL(Status, &apos;Assigned&apos;), 				NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)) /*, 				CreatedBy.Profile.Name =&apos;TGS Order Handling&apos; 			*/), 			ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Order Handling&apos;), 			ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;Pending Task Resolution&apos;) 		), 		OR( 			ISPICKVAL( Account.TGS_Aux_Customer_Country__r.TGS_SMC_Assigned_L2__c , &apos;28&apos;), 			ISPICKVAL( Account.TGS_Aux_Holding__r.TGS_SMC_Assigned_L2__c , &apos;28&apos;) 		), 		OR( 			ISCHANGED(TGS_Status_reason__c), 			ISCHANGED(Status) 		) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test Provision Siptrunk</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_Provision_Siptrunk</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	OR(  		AND(  			OR(  				TGS_Service__c = &apos;UC Mobile Service&apos;,   				TGS_Service__c = &apos;UC Mobile Users&apos;,  				TGS_Service__c = &apos;UC Mobile Open Order&apos;  			),  			OR(  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;Off-Net Supplier Data Checked&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;SO Launched(&amp; POF Required)&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;SO Accepted&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), 				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Configuration&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;), 			 				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;),  				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;),  				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Off-Net&apos;), 				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;) 			) 		),	 		AND( 			TGS_Service__c = &apos;UC Mobile Licenses&apos;, 			OR(  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;Off-Net Supplier Data Checked&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;SO Launched(&amp; POF Required)&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;SO Accepted&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), 				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Configuration&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;), 			 				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;),  				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;),  				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Off-Net&apos;)  			) 		),  		AND( /* mSIP */  			OR(  				TGS_Service__c = &apos;mSIP Site Trunk&apos;, 				TGS_Service__c = &apos;mSIP Site No Trunk&apos;,  				TGS_Service__c = &apos;mSIP Site Hosted UC&apos;,  				TGS_Service__c = &apos;mSIP Site ADMIN&apos;,  				TGS_Service__c = &apos;mSIP Site CCA&apos;,  				TGS_Service__c = &apos;DDI Standard&apos;,  				TGS_Service__c = &apos;DDI Resale&apos;,  				TGS_Service__c = &apos;Special Number&apos;,  				TGS_Service__c = &apos;PNP&apos;,  				TGS_Service__c = &apos;Flat Rate&apos;  			),  			OR(  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;Off-Net Supplier Data Checked&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;SO Launched(&amp; POF Required)&apos;), 				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;POF Signed&apos;), 			 				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;SO Accepted&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;),  				ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;),  				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;),  				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;), 				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Off-Net&apos;)			 			)  		)  	), 	OR( 		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;,  		Account.Name = &apos;ACME&apos; 	) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test Provision VPN</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_Provision_VPN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  		TGS_Service__c = &apos;Internet Resale&apos;,  	OR(  		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), 			 		ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;) 	) , 	OR(  		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;,  		Account.Name = &apos;ACME&apos;  	)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3A Test SIPNOC</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_SIP_NOC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR( 	AND( 		OR(  			TGS_Service__c = &apos;mSIP Site Trunk&apos;,  			TGS_Service__c = &apos;mSIP Site No Trunk&apos;,  			TGS_Service__c = &apos;mSIP Site Hosted UC&apos;,  			TGS_Service__c = &apos;mSIP Site ADMIN&apos;,  			TGS_Service__c = &apos;mSIP Site CCA&apos;,                         TGS_Service__c = &apos;Flat Rate&apos;  		),  		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Configuration&apos;), 		OR( 			Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;,  			Account.Name = &apos;ACME&apos; 		) 	), 	AND( 		OR( 			TGS_Service__c = &apos;DDI Standard&apos;,  			TGS_Service__c = &apos;DDI Resale&apos;,  			TGS_Service__c = &apos;Special Number&apos;, 	 			TGS_Service__c = &apos;PNP&apos; 		),  		ISPICKVAL(Status, &apos;In Progress&apos;)&amp;&amp;ISPICKVAL(TGS_Status_reason__c,&apos;In Configuration&apos;) &amp;&amp; CreatedBy.Profile.Name =&apos;TGS Order Handling&apos;, 		OR( 			Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;,  			Account.Name = &apos;ACME&apos; 		) 	) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3ATest Acme SD Level 1</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_Acme_SD_Level_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	OR( 		AND( 			AND( 				TGS_Service__c &lt;&gt; &apos;mWan&apos;, 				TGS_Service__c &lt;&gt; &apos;mWan - Internet&apos;, 				TGS_Service__c &lt;&gt; &apos;Universal WIFI&apos;, 				TGS_Service__c &lt;&gt; &apos;SMDM Management&apos;, 				TGS_Service__c &lt;&gt; &apos;M2M Jasper - SIM Request&apos;, 				TGS_Service__c &lt;&gt; &apos;M2M Jasper - Service Change&apos;, 				TGS_Service__c &lt;&gt; &apos;M2M Smart - Service Change&apos;, 				TGS_Service__c &lt;&gt; &apos;M2M Smart - SIM Request&apos;, 				TGS_Service__c &lt;&gt; &apos;M2M SBC - Service Request&apos;, 				TGS_Service__c &lt;&gt; &apos;Colocation/Housing&apos;, 				TGS_Service__c &lt;&gt; &apos;Threat Detection&apos; 			), 			OR( 				ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( 					ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), 					ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;) 				), 				AND( 					ISPICKVAL(Status, &apos;Resolved&apos;), 					RecordType.DeveloperName = &apos;Order_Management_Case&apos; 				) 			) 		), 		AND( 			AND( 				TGS_Service__c &lt;&gt; &apos;mWan&apos;, 				TGS_Service__c &lt;&gt; &apos;mWan - Internet&apos;, 				TGS_Service__c &lt;&gt; &apos;UC Mobile Service&apos;, 				TGS_Service__c &lt;&gt; &apos;UC Mobile Users&apos;, 				TGS_Service__c &lt;&gt; &apos;UC Mobile Open Order&apos; 			), 			ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;) 		), 		AND( 			OR( 				AND( 					OR( 						TGS_Service__c = &apos;DDI Standard&apos;, 						TGS_Service__c = &apos;DDI Resale&apos;, 						TGS_Service__c = &apos;Special Number&apos;, 						TGS_Service__c = &apos;PNP&apos; 					), 					CreatedBy.Profile.Name &lt;&gt; &apos;TGS Order Handling&apos; 				), 				TGS_Service__c = &apos;HCS License&apos;, 				TGS_Service__c = &apos;UC Mobile Licenses&apos; 			), 			OR( 				AND( 					ISPICKVAL(Status, &apos;Assigned&apos;), 					NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)) 				), 				ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 				ISPICKVAL(Status, &apos;Pending&apos;)&amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;) 			) 		), 		AND( 			OR( 				TGS_Service__c = &apos;Contact Centre&apos;, 				TGS_Service__c = &apos;Video&apos;, 				TGS_Service__c = &apos;ToIP&apos;, 				TGS_Service__c = &apos;HCS Video&apos;, 				TGS_Service__c = &apos;HCS ToIP&apos;, 				TGS_Service__c = &apos;Cisco HCS Service&apos;, 				TGS_Service__c = &apos;HCS Locations&apos;, 				TGS_Service__c = &apos;HCS Open order&apos;, 				TGS_Service__c = &apos;HCS License&apos;, 				TGS_Service__c = &apos;Domestic Number&apos;, 				TGS_Service__c = &apos;International Number&apos;, 				TGS_Service__c = &apos;Setup and Add-ons&apos;, 				TGS_Service__c = &apos;WebEx&apos; 			), 			OR( 				ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;), 				ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;) 			) 		), 		AND( 			OR( 				TGS_Service__c = &apos;mWan&apos;, 				TGS_Service__c = &apos;mWan - Internet&apos; 			), 			OR( 				ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( 					ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), 					ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;) 				), 				ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), 				ISPICKVAL(Status, &apos;Resolved&apos;), 				ISPICKVAL(Status, &apos;Closed&apos;) 			) 		) 	), 	OR( 		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;, 		Account.Name = &apos;ACME&apos; 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	)	 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3ATest Acme SD Level 1 W3</fullName>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Owner_Test_Acme_SD_Level_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(	 OR(	 AND(  OR(  TGS_Service__c = &apos;Blank SIM Emergency Stock&apos;, TGS_Service__c = &apos;Bundles With Pooling&apos;, TGS_Service__c = &apos;Device Emergency Stock&apos;, TGS_Service__c = &apos;Mobile Accesory&apos;, TGS_Service__c = &apos;Mobile Device&apos;, TGS_Service__c = &apos;Mobile Intranet&apos;, TGS_Service__c = &apos;SIM Card Suscription&apos;, TGS_Service__c = &apos;White/Black Lists&apos;, TGS_Service__c= &apos;Mobile Line&apos;  ),  OR(  ISPICKVAL(Status, &apos;Assigned&apos;) &amp;&amp; NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Assigned&apos;)), ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;In SD Validation&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;)  ),  ISPICKVAL(Status, &apos;Resolved&apos;)  )	 ),  AND(  TGS_Service__c = &apos;Services&apos;,  OR(  ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Close&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;)  ),  ISPICKVAL(Status, &apos;Resolved&apos;)  )  ),  AND(  TGS_Service__c = &apos;Fixed Voice&apos;,  OR(  ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;In Customer Configuration&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;) ),  ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; OR( ISPICKVAL(TGS_Status_reason__c,&apos;In Customer Configuration&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Test&apos;), ISPICKVAL(TGS_Status_reason__c,&apos;In Cancellation - Customer Acceptance&apos;)  ),  ISPICKVAL(Status, &apos;Resolved&apos;),  ISPICKVAL(Status, &apos;Closed&apos;)  )  )  ),  OR(  Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;, Account.Name = &apos;ACME&apos; ),  OR(  ISCHANGED(TGS_Status_reason__c), ISCHANGED(Status)  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Owner%3ATest Acme SD Level 2 W3</fullName>
        <actions>
            <name>TGSTest_L2_Ordering_Mobile</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Empty_Assignee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( 	OR( 		TGS_Service__c = &apos;Blank SIM Emergency Stock&apos;, 		TGS_Service__c = &apos;Bundles With Pooling&apos;, 		TGS_Service__c = &apos;Device Emergency Stock&apos;, 		TGS_Service__c = &apos;Mobile Accesory&apos;, 		TGS_Service__c = &apos;Mobile Device&apos;, 		TGS_Service__c = &apos;Mobile Intranet&apos;, 		TGS_Service__c = &apos;SIM Card Suscription&apos;, 		TGS_Service__c = &apos;White/Black Lists&apos;, 		TGS_Service__c = &apos;Mobile Line&apos;, 		TGS_Service__c = &apos;Services&apos; 	), 	OR( 		ISPICKVAL(Status, &apos;In Progress&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;), 		ISPICKVAL(Status, &apos;Pending&apos;) &amp;&amp; ISPICKVAL(TGS_Status_reason__c,&apos;In Provision&apos;) 	), 	OR( 		Account.TGS_Aux_Holding__r.Name = &apos;ACME&apos;, 		Account.Name = &apos;ACME&apos; 	), 	OR( 		ISCHANGED(TGS_Status_reason__c), 		ISCHANGED(Status) 	) 		 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Product configured</fullName>
        <actions>
            <name>TGS_Uncheck_product_configured</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(  ISCHANGED( Status ),  ISCHANGED( TGS_Status_reason__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS RFB when Status%3DClosed</fullName>
        <actions>
            <name>TGS_RFB_when_Status_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (ISPICKVAL(Status , &quot;Closed&quot;),$Permission.TGS_User)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS RFS when Status%3DResolved</fullName>
        <actions>
            <name>TGS_RFS_when_Status_Resolved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(   ISPICKVAL(Status , &quot;Resolved&quot;),   $Permission.TGS_User,   TGS_Service__c&lt;&gt;&apos;mWan&apos;,   TGS_Service__c&lt;&gt;&apos;mWan - Internet&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Case.TGS_Fecha_Cierre_Automatico__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TGS RFS when Status%3DResolved %28mWan%29</fullName>
        <actions>
            <name>TGS_RFS_when_Status_Resolved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
  ISPICKVAL(Status , &quot;Resolved&quot;),  
  $Permission.TGS_User,
  (TGS_Service__c=&apos;mWan&apos; || TGS_Service__c= &apos;mWan - Internet&apos;)
  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Stop Clock SLA</fullName>
        <actions>
            <name>TGS_Stop_Clock_SLA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending,Resolved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Unstop Clock SLA</fullName>
        <actions>
            <name>TGS_Unstop_Clock_SLA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(  AND(   ISPICKVAL(PRIORVALUE(Status),&apos;Pending&apos;),    TEXT(Status) != &apos;Pending&apos;  ),  AND(   ISPICKVAL(PRIORVALUE(Status),&apos;Resolved&apos;),    TEXT(Status) != &apos;Resolved&apos;  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS_Actualiza_responsable</fullName>
        <actions>
            <name>TGS_Actualiza_responsable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Caso Proveedor de Telco</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Responsable__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Actualiza el campo &apos;Responsable&apos; cuando se crea un caso con tipo de registro &apos;Caso Proveedor de Telco&apos;.

Creado por la Demanda 288</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TGS_Case_CWP_Cancellation</fullName>
        <actions>
            <name>TGS_CWP_Accept_Cancellation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 	ISPICKVAL(Status,&apos;In Progress&apos;), 	ISPICKVAL(PRIORVALUE(Status),&apos;Pending&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS_CasetoUDO</fullName>
        <actions>
            <name>TGS_CasetoUDOCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>UDO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Order Management Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Service__c</field>
            <operation>contains</operation>
            <value>Smart M2M</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.TGS_Service__c</field>
            <operation>contains</operation>
            <value>M2M Jasper</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS_Closed_Cases</fullName>
        <actions>
            <name>TGS_Survey_Notification_Mail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow rule for email survey sending.</description>
        <formula>AND(  MOD( Contact.TGS_of_Closed_Cases__c ,10) = 0 ,   RecordType.DeveloperName &lt;&gt; &apos;BI_Caso_Comercial_Abierto&apos;, RecordType.DeveloperName &lt;&gt; &apos;BI_Caso_Comercial_Cerrado&apos;, RecordType.DeveloperName &lt;&gt; &apos;BI_Caso_Interno&apos;, RecordType.DeveloperName &lt;&gt; &apos;BI_CasoTecnico&apos;, RecordType.DeveloperName &lt;&gt; &apos;Caso_eHelp&apos;, RecordType.DeveloperName &lt;&gt; &apos;Order_Management_Case&apos;,  Contact.TGS_Surveys__c  = True)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS_Control_Casilla_Desarrollo</fullName>
        <actions>
            <name>TGS_ActualizaCasillaDesarrollo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.TGS_Casilla_Desarrollo__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS_Resolve_Date_Clear</fullName>
        <actions>
            <name>TGS_Resolve_Date_Clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(   RecordType.DeveloperName = &apos;Order_Management_Case&apos;,   ISPICKVAL(PRIORVALUE(Status), &apos;Resolved&apos;),   NOT ISPICKVAL((Status), &apos;Resolved&apos;),   NOT ISPICKVAL((Status), &apos;Closed&apos;),   NOT ISPICKVAL((Status), &apos;Cancelled&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS_Resolve_Date_Set</fullName>
        <actions>
            <name>TGS_Resolve_Date_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(   RecordType.DeveloperName = &apos;Order_Management_Case&apos;,   ISCHANGED(Status),   ISPICKVAL(Status, &apos;Resolved&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS_Start_SLA</fullName>
        <actions>
            <name>TGS_Case_SLA_Start</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(    RecordType.DeveloperName = &apos;Order_Management_Case&apos;,    $Setup.TGS_User_Org__c.TGS_Is_TGS__c,    NOT(ISPICKVAL(Status, &apos;Pending&apos;)),    ISPICKVAL(PRIORVALUE(Status) ,&apos;Pending&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS_Stop_SLAs_Pending</fullName>
        <actions>
            <name>TGS_Case_SLA_Stop</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(    RecordType.DeveloperName = &apos;Order_Management_Case&apos;,    $Setup.TGS_User_Org__c.TGS_Is_TGS__c,    ISPICKVAL(Status, &apos;Pending&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS_new_ticket</fullName>
        <actions>
            <name>Send_an_email_for_template_4_1_when_case_is_asi</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Billing Inquiry,Complaint</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>startsWith</operation>
            <value>TGS</value>
        </criteriaItems>
        <description>send email when new ticket is in the queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
