<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_Alerta_una_semana_antes_del_cierre_de_la_campana</fullName>
        <description>BI_Alerta una semana antes del cierre de la campaña</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_Cierre_automatico_campana</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Alerta_una_semana_antes_cierre_campana_a_prop_de_leads_y_contactos</fullName>
        <description>BI_O4_Alerta una semana antes del cierre de la campaña a los propietarios de leads y contactos</description>
        <protected>false</protected>
        <recipients>
            <type>campaignMemberDerivedOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_Cierre_automatico_campana</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Comienzo_de_la_Campana</fullName>
        <description>BI_O4_Comienzo de la Campaña</description>
        <protected>false</protected>
        <recipients>
            <type>campaignMemberDerivedOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Comienzo_Activacion_Campana</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Desactivacion_de_la_Campana</fullName>
        <description>BI_O4_Desactivación de la Campaña</description>
        <protected>false</protected>
        <recipients>
            <type>campaignMemberDerivedOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_O4_Email_Desactivacion_de_campana</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Fin_de_la_Campana</fullName>
        <description>BI_O4_Fin de la Campaña</description>
        <protected>false</protected>
        <recipients>
            <type>campaignMemberDerivedOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_O4_Email_Cierre_automatico_campana</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_Activacion_campana</fullName>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>Activación campaña</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_Coste_presupuestado</fullName>
        <field>BudgetedCost</field>
        <formula>BI_Presupuesto_Actividades__c + BI_Presupuesto_Catering__c +  BI_Presupuesto_Local__c +  BI_Presupuesto_Marketing__c +  BI_Presupuesto_Otros__c +  BI_Presupuesto_Transportes__c</formula>
        <name>Actualiza Coste presupuestado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_Pais_Texto_Campanas</fullName>
        <field>BI_Pais_textoQQQQ__c</field>
        <formula>TEXT(BI_Country__c)</formula>
        <name>BI Actualiza País Texto Campañas</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizar_Campana_Estado_en_Planific</fullName>
        <field>Status</field>
        <literalValue>Planned</literalValue>
        <name>BI_Actualizar-Campaña_Estado_en_Planific</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizar_Campana_estado_En_curso</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>BI_Actualizar_Campaña estado En curso</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizar_activo_false</fullName>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>BI_Actualizar-activo-false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizar_estado_en_aprobacion</fullName>
        <description>Automáticamente la campaña se desbloqueará y el estado cambiará a en aprobación</description>
        <field>Status</field>
        <literalValue>In Approval</literalValue>
        <name>BI_Actualizar-estado en aprobación</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_E1_Activar_campana</fullName>
        <description>Activamos la campaña.</description>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>Activar campaña</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_E1_Desactivar_campa_a_fuera_de_plazo</fullName>
        <description>Desactiva la campaña que se encuentra fuera de plazo</description>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Desactivar campaña fuera de plazo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_E1_Desactivar_campana</fullName>
        <description>Desactivamos la campaña</description>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Desactivar campaña</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_E1_Finalizar_campana</fullName>
        <description>La campaña pasa a estado &apos;Finalizada&apos;</description>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Finalizar campaña</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_actualiza_coste_real</fullName>
        <field>ActualCost</field>
        <formula>BI_Gastos_Actividades__c +  BI_Gastos_Catering__c +  BI_Gastos_Local__c +  BI_Gastos_Marketing__c +  BI_Gastos_Otros__c +  BI_Gastos_Transportes__c</formula>
        <name>Actualiza coste real</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Campaig_Closed</fullName>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Campaig Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_campaign_status</fullName>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Change campaign status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_Actualiza_coste_presupuestado_y_real</fullName>
        <actions>
            <name>BI_Actualiza_Coste_presupuestado</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_actualiza_coste_real</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Eventos</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizar-Activo-true</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>Cuando se llegue a la fecha de fin de la campaña, se desactivará la campaña</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_Actualizar_activo_false</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>8</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_Actualizar-campana-activo-true</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>Comunica al propietario el cierre de su campaña en 7 días.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_Alerta_una_semana_antes_del_cierre_de_la_campana</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_Actualizar-estado-encurso</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>Cuando la fecha de inicio cumpla una serie de características se activará la campaña</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_Activacion_campana</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Campaign.StartDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_Actualizar-estado-finalizada</fullName>
        <actions>
            <name>BI_Actualizar_activo_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Status, &apos;Completed&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_E1_Activar_campaña_en_curso</fullName>
        <actions>
            <name>BI_E1_Activar_campana</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>Las campañas que pasen a estado &apos;En curso&apos; deberán pinear el valor de isActive a true.
Autor: Pablo de Andrés.
Empresa: Everis.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_E1_Desactivar_campaña_finalizada</fullName>
        <actions>
            <name>BI_E1_Desactivar_campana</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Las campañas que lleguen al estado &apos;Finalizada&apos; cambian pasan a pinear isActive como false.
Autor: Pablo de Andrés.
Empresa: Everis.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_E1_En_curso_y_pasa_fecha_fin</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>Las campañas que se encuentren en estado &apos;En curso&apos; y alcancen su fecha fin pasarán al estado &apos;Finalizada&apos;.
Autor: Pablo de Andrés.
Empresa: Everis.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_E1_Desactivar_campa_a_fuera_de_plazo</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>BI_E1_Finalizar_campana</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_O4_Activacion de la Campana</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_O4_Comienzo_de_la_Campana</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>BI_Activacion_campana</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Campaign.StartDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_O4_Desactivacion de la Campana</fullName>
        <actions>
            <name>BI_O4_Desactivacion_de_la_Campana</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISCHANGED(IsActive),  PRIORVALUE(IsActive) = TRUE  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Fin de la Campana</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_O4_Fin_de_la_Campana</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_O4_Notificacion fin de campana</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.IsActive</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_O4_Alerta_una_semana_antes_cierre_campana_a_prop_de_leads_y_contactos</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Campaign Stage</fullName>
        <actions>
            <name>Campaig_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISNULL(EndDate)) &amp;&amp; NOT(ISBLANK(EndDate))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Campaign status</fullName>
        <active>true</active>
        <formula>ISPICKVAL(Status , &apos;In Progress&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Change_campaign_status</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Campaign.EndDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
