<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_Comunicacion_aprobacion_campana</fullName>
        <description>BI_Comunicación aprobación campaña</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_Destinatarios_de_Campana_aprobada</template>
    </alerts>
    <alerts>
        <fullName>BI_Comunicacion_campana_rechazada</fullName>
        <description>BI_Comunicación campaña rechazada</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_Destinatarios_de_la_Campana_rechazada</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_Actualizar_Estado_aprobado</fullName>
        <field>BI_Estado__c</field>
        <literalValue>Approved</literalValue>
        <name>BI_Actualizar-Estado-aprobado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizar_estado_rechazado</fullName>
        <field>BI_Estado__c</field>
        <literalValue>Rechazado</literalValue>
        <name>BI_Actualizar-estado-rechazado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
