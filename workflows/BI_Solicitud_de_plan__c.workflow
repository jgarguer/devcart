<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_CHI_Plan_enviado_a_Operaciones_Aprobado</fullName>
        <description>2. Plan enviado a Operaciones - Aprobado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/X7_Envio_Plan_Operaciones_aprobado</template>
    </alerts>
    <alerts>
        <fullName>X2_Plan_enviado_a_Pricing_Aprobado</fullName>
        <description>2. Plan enviado a Pricing - Aprobado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/X5_Envio_Plan_Pricing_aprobado</template>
    </alerts>
    <alerts>
        <fullName>X2_Plan_enviado_a_Pricing_Rechazado</fullName>
        <description>3. Plan enviado a Pricing - Rechazado</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_Plantillas_Agendamiento_Chile/X5_Envio_Plan_Pricing_rechazado</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_Plan_Aprobado_pricing</fullName>
        <description>El departamento de pricing aprueba el plan y es enviado a Operaciones</description>
        <field>BI_Estado_solicitud__c</field>
        <literalValue>Aprobado</literalValue>
        <name>Plan aprobado Pricing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Plan_Cancelado_Casilla</fullName>
        <field>BI_Estado_solicitud__c</field>
        <literalValue>Cancelado</literalValue>
        <name>Plan Cancelado Casilla</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Plan_Pricing</fullName>
        <description>El Ejecutivo de Clientes o Service Manager envía a marketing el Plan para su proceso de aprobación</description>
        <field>BI_Estado_solicitud__c</field>
        <literalValue>Pricing</literalValue>
        <name>Plan Pricing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Plan_Rechazado_por_pricing</fullName>
        <description>El plan ha sido rechazado por pricing, vuele al estado pendiente para que sea modificado</description>
        <field>BI_Estado_solicitud__c</field>
        <literalValue>Pendiente</literalValue>
        <name>Plan rechazado Pricing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Plan_Req_RT_Cerrado_Plana</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BI_Cerrado_Plana</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Plan Req RT Cerrado Plana</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Plan_Req_RT_Cerrado_Por_destino</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BI_Cerrado_Por_destino</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Plan Req RT Cerrado Por destino</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Plan_aprobado_Logistica</fullName>
        <field>BI_Estado_solicitud__c</field>
        <literalValue>Activado</literalValue>
        <name>Plan aprobado Logística</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Plan_rechazado_Operaciones</fullName>
        <description>Operaciones rechaza el plan, vuelve al estado Aprobado</description>
        <field>BI_Estado_solicitud__c</field>
        <literalValue>Pendiente</literalValue>
        <name>Plan rechazado Operaciones</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Plan Cancelado</fullName>
        <actions>
            <name>BI_Plan_Cancelado_Casilla</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Solicitud_de_plan__c.BI_Cancelar_plan__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>Al marcar la casilla de &quot;Cancelar plan&quot;, el estado pasa a Cancelado</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Plan Req Comer Bloqueado Plana</fullName>
        <actions>
            <name>BI_Plan_Req_RT_Cerrado_Plana</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Solicitud_de_plan__c.BI_Estado_solicitud__c</field>
            <operation>equals</operation>
            <value>Cancelado</value>
        </criteriaItems>
        <criteriaItems>
            <field>BI_Solicitud_de_plan__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cerrado - Plana</value>
        </criteriaItems>
        <description>El Plan ha sido bloqueado al haber sido Cancelado o Rechazado</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Plan Req Comer Bloqueado Por destino</fullName>
        <actions>
            <name>BI_Plan_Req_RT_Cerrado_Por_destino</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Solicitud_de_plan__c.BI_Estado_solicitud__c</field>
            <operation>equals</operation>
            <value>Cancelado</value>
        </criteriaItems>
        <criteriaItems>
            <field>BI_Solicitud_de_plan__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Cerrado - Por destino</value>
        </criteriaItems>
        <description>El Plan ha sido bloqueado al haber sido Cancelado o Rechazado</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
