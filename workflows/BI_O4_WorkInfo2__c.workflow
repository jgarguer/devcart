<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_Aviso_de_vencimiento_del_propietario</fullName>
        <description>BI_Aviso_de_vencimiento_del_propietario</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Aviso_de_vencimiento_del_propietario</template>
    </alerts>
    <rules>
        <fullName>BI_Aviso de vencimiento del propietario</fullName>
        <active>true</active>
        <criteriaItems>
            <field>BI_O4_WorkInfo2__c.BI_O4_Estado__c</field>
            <operation>equals</operation>
            <value>Activo</value>
        </criteriaItems>
        <criteriaItems>
            <field>BI_O4_WorkInfo2__c.BI_Sub_Estado__c</field>
            <operation>equals</operation>
            <value>Vigente</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_Aviso_de_vencimiento_del_propietario</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>BI_O4_WorkInfo2__c.BI_Fecha_de_envio_de_Alerta__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
