<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_ARG_Prospectos_Empresas</fullName>
        <description>BI_ARG_Prospectos_Empresas</description>
        <protected>false</protected>
        <recipients>
            <recipient>marisa.avila@telefonica.com.prod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_ARG_Creacion_Prospectos</template>
    </alerts>
    <alerts>
        <fullName>FS_CORE_Email_para_prospecto_aprobado</fullName>
        <description>FS CORE Email para prospecto aprobado</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FS_CHILE_PERU/FS_CORE_Aprobacion_Conversion_Prospecto</template>
    </alerts>
    <alerts>
        <fullName>FS_CORE_Email_para_prospecto_rechazado</fullName>
        <description>FS CORE Email para prospecto rechazado</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FS_CHILE_PERU/FS_CORE_Rechazo_Conversion_Prospecto</template>
    </alerts>
    <alerts>
        <fullName>FS_CORE_Solicitud_de_aprobacion_de_prospecto</fullName>
        <description>FS CORE Solicitud de aprobación de prospecto</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FS_CHILE_PERU/FS_CORE_Creacion_De_Solicitud_De_Conversion</template>
    </alerts>
    <alerts>
        <fullName>TIWS_Assigned_Lead</fullName>
        <description>TIWS Assigned Lead</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TIWS/TIWS_Assigned_Lead</template>
    </alerts>
    <alerts>
        <fullName>TIWS_Creacion_Lead_Comunidad</fullName>
        <description>TIWS Creacion Lead Comunidad</description>
        <protected>false</protected>
        <recipients>
            <recipient>TIWS_Business_Acceleration</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TIWS/TIWS_Leads_Creation_notification</template>
    </alerts>
    <alerts>
        <fullName>TIWS_Rejection_by_Commecial_Planning</fullName>
        <description>TIWS Rejection by Commecial Planning</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TIWS/TIWS_Lead_Rejection_by_Commercial_Planning</template>
    </alerts>
    <alerts>
        <fullName>TIWS_Solicitud_Conversion_Lead</fullName>
        <description>TIWS Solicitud Conversion Lead</description>
        <protected>false</protected>
        <recipients>
            <recipient>TIWS_Commercial_Planning</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TIWS/TIWS_Assigned_Lead</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_Actualiza_Pais_Texto_Candidatos</fullName>
        <field>Country</field>
        <formula>TEXT(BI_Country__c)</formula>
        <name>BI Actualiza País Texto Candidatos</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizacion_Campo_Account_Source</fullName>
        <description>D-000517</description>
        <field>BI_AccountSource__c</field>
        <formula>TEXT(LeadSource)</formula>
        <name>BI_Actualizacion Campo Account Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizacion_de_Campo_Campa_a_TBS</fullName>
        <description>D-000517</description>
        <field>BI_Campana_TBS_Opp__c</field>
        <formula>BI_Campana_TBS_Con__c</formula>
        <name>BI_Actualizacion de Campo Campaña TBS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI Actualiza Pais Texto Candidatos</fullName>
        <actions>
            <name>BI_Actualiza_Pais_Texto_Candidatos</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_ARG_Creacion_prospecto_empresas</fullName>
        <actions>
            <name>BI_ARG_Prospectos_Empresas</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(      ISPICKVAL(BI_Segment__c, &apos;Empresas&apos;),      ISPICKVAL(BI_Country__c, &apos;Argentina&apos;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizacion Account Source</fullName>
        <actions>
            <name>BI_Actualizacion_Campo_Account_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>D-000517 - Obtiene el valor del campo LeadSource</description>
        <formula>OR(      AND(ISNEW(),       NOT(ISBLANK(TEXT(LeadSource)))  ),    ISCHANGED(LeadSource) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizacion Campaña TBS</fullName>
        <actions>
            <name>BI_Actualizacion_de_Campo_Campa_a_TBS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Demanda 000517</description>
        <formula>OR(    AND(    ISNEW(),     NOT(ISBLANK(BI_Campana_TBS_Con__c))   ),  ISCHANGED(BI_Campana_TBS_Con__c), ISCHANGED(BI_Campana_TBS_Opp__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TIWS_Creacion_Lead_Comunidad</fullName>
        <actions>
            <name>TIWS_Creacion_Lead_Comunidad</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.TIWS_Gestion_TIWS__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>TIWS_Leads_Community</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TIWS_Solicitud_Conversion_Lead</fullName>
        <actions>
            <name>TIWS_Solicitud_Conversion_Lead</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Pending Conversion</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.TIWS_Gestion_TIWS__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>Se ejecuta para notificar cuando el estado de un Lead pasa a estado &apos;Pending Conversion&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
