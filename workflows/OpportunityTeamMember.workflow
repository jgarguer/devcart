<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_COL_Notifica_Oportunidad_Ingenierio</fullName>
        <description>BI_COL_Notifica_Oportunidad_Ingenierio</description>
        <protected>false</protected>
        <recipients>
            <recipient>Presale Engineering</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_BI_COL/BI_COL_Notificacion_Oportunidad_Ingeniero_Preventa</template>
    </alerts>
    <rules>
        <fullName>BI_COL_Notifica_Ing_Preventa</fullName>
        <actions>
            <name>BI_COL_Notifica_Oportunidad_Ingenierio</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( ISPICKVAL(TeamMemberRole, &quot;Presale Engineering&quot;), (TEXT($User.Pais__c) = &apos;Colombia&apos;),   !$Setup.BI_bypass__c.BI_migration__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
