<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HO_Validation_Date</fullName>
        <field>HO_Validation_Date__c</field>
        <formula>TODAY()</formula>
        <name>Fecha Validacion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HO_eSoft_Fecha_Validacion</fullName>
        <actions>
            <name>HO_Validation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(HO_Status__c) &amp;&amp;  ISPICKVAL(HO_Status__c , &apos;Accepted&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
