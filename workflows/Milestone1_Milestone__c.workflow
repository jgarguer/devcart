<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_Actualizar_campo_Duracion</fullName>
        <field>BI_Date_of_completed__c</field>
        <formula>NOW()</formula>
        <name>Actualizar campo Duracion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_actualizar_Date_Of_Completed_Hito</fullName>
        <actions>
            <name>BI_Actualizar_campo_Duracion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  RecordType.DeveloperName = &apos;BI_HandOver_y_Establishment&apos;,  (Complete__c),true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
