<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TGS_Update_Unique_Account</fullName>
        <field>TGS_Validation_ID__c</field>
        <formula>TGS_Account__c</formula>
        <name>TGS Update Unique Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TGS_Unique_Account_AddInfo</fullName>
        <actions>
            <name>TGS_Update_Unique_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
