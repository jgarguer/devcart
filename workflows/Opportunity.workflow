<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_ARG_Avisar_a_administrador_de_contrato</fullName>
        <ccEmails>admincontratos.ar@telefonica.com</ccEmails>
        <description>BI_ARG_Avisar a administrador de contrato</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Comunicar_administracion_de_contratos_cierre_de_una_oportunidad_renovacion</template>
    </alerts>
    <alerts>
        <fullName>BI_ARG_Avisar_al_administrador_de_que_existe_una_oportunidad_pendiente_de_contra</fullName>
        <ccEmails>admincontratos.ar@telefonica.com</ccEmails>
        <description>BI_ARG_Avisar_al_administrador_de_que_existe_una_oportunidad_pendiente_de_contrato</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_informar_de_que_oportunidad_ganada_requiere_de_contrato</template>
    </alerts>
    <alerts>
        <fullName>BI_ARG_Aviso_renovacion_oportunidad</fullName>
        <ccEmails>admincontratos.ar@telefonica.com</ccEmails>
        <description>Aviso renovacion oportunidad Arg</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Renovacion_oportunidad</template>
    </alerts>
    <alerts>
        <fullName>BI_ARG_Generar_nuevo_contrato</fullName>
        <ccEmails>admincontratos.ar@telefonica.com</ccEmails>
        <description>Generar nuevo contrato Arg</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Generar_nuevo_contrato</template>
    </alerts>
    <alerts>
        <fullName>BI_Aviso_renovacion_oportunidad</fullName>
        <ccEmails>admincontratos.ar@telefonica.com</ccEmails>
        <description>Aviso renovacion oportunidad</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Renovacion_oportunidad</template>
    </alerts>
    <alerts>
        <fullName>BI_Comunica_prop_2_dias_antes_fecha_vigencia</fullName>
        <description>Comunica al propietario de la Opp 2 días antes de la fecha de vigencia</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_comunicar_propietario_opp_2_d_as_antes_fecha_vigencia</template>
    </alerts>
    <alerts>
        <fullName>BI_ECU_Oportunidad_ganada</fullName>
        <description>BI ECU Oportunidad ganada</description>
        <protected>false</protected>
        <recipients>
            <field>BI_Email_del_Gestor_del_propietario__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_ECU_Envio_email_oportunidad_ganada</template>
    </alerts>
    <alerts>
        <fullName>BI_Envio_de_email_resultado_proceso_Comite</fullName>
        <description>BI Envío de email resultado proceso Comité</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Envio_email_estado_proceso_de_aprobacion_comite</template>
    </alerts>
    <alerts>
        <fullName>BI_Envio_de_email_resultado_proceso_comercial</fullName>
        <description>BI Envío de email resultado proceso comercial</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Envio_email_estado_proceso_de_aprobacion_comercial</template>
    </alerts>
    <alerts>
        <fullName>BI_Envio_de_email_resultado_proceso_economico</fullName>
        <description>BI Envío de email resultado proceso económico</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Envio_email_estado_proceso_de_aprobacion_economica</template>
    </alerts>
    <alerts>
        <fullName>BI_Envio_de_email_resultado_proceso_tecnico</fullName>
        <description>BI Envío de email resultado proceso técnico</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Envio_email_estado_proceso_de_aprobacion_tecnica</template>
    </alerts>
    <alerts>
        <fullName>BI_FVI_Notificacion_Error_Gestor_Contratos</fullName>
        <description>Notificación ante errores con Gestor de Contrato</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_FVI/BI_FVI_Notificacion_Error_Gestor_Contratos</template>
    </alerts>
    <alerts>
        <fullName>BI_MEX_Alerta_correo_FCV</fullName>
        <ccEmails>miguel.fananas@telefonica.com.b2b</ccEmails>
        <description>Alerta por FCV mayor de 50.000€ ó 500.000€, Mexico</description>
        <protected>false</protected>
        <recipients>
            <field>BI_Email_del_Gestor_del_propietario__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_MEX_Alerta_FCV</template>
    </alerts>
    <alerts>
        <fullName>BI_Notificacion_7_dias_de_vencer</fullName>
        <description>Notificación a 7 días de vencerse</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Notificacion_temprana_Oportunidad_proxima_a_vencer</template>
    </alerts>
    <alerts>
        <fullName>BI_Notificacion_7_dias_de_vencer_ARG</fullName>
        <description>Notificación a 7 días de vencerse ARG</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Notificacion_temprana_Oportunidad_proxima_a_vencer</template>
    </alerts>
    <alerts>
        <fullName>BI_Notificacion_Revisar_FCV</fullName>
        <description>BI Notificación Revisar FCV</description>
        <protected>false</protected>
        <recipients>
            <field>BI_Email_del_Gestor_del_propietario__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Notificacion_Revisar_FCV</template>
    </alerts>
    <alerts>
        <fullName>BI_Notificacion_Revisar_FCV_no_gestor</fullName>
        <description>BI Notificación Revisar FCV no gestor</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Notificacion_Revisar_FCV</template>
    </alerts>
    <alerts>
        <fullName>BI_Notificacion_a_1_dia_de_vencer</fullName>
        <description>Notificación a 1 día de vencerse</description>
        <protected>false</protected>
        <recipients>
            <field>BI_Email_del_Gestor_del_propietario__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Notificacion_temprana_Su_Oportunidad_vence_manana</template>
    </alerts>
    <alerts>
        <fullName>BI_Notificacion_a_1_dia_de_vencer_ARG</fullName>
        <description>Notificación a 1 día de vencerse ARG</description>
        <protected>false</protected>
        <recipients>
            <field>BI_Email_del_Gestor_del_propietario__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Notificacion_temprana_Su_Oportunidad_vence_manana</template>
    </alerts>
    <alerts>
        <fullName>BI_Notificacion_asignacion_nueva_oportunidad</fullName>
        <description>BI_Notificacion asignacion nueva oportunidad</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_PlantillaCreacionOportunidad</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_Alerta_Jefes_Top_2</fullName>
        <description>BI_PER_Alerta Jefes Top 2  Provincia</description>
        <protected>false</protected>
        <recipients>
            <recipient>PER_NEG_Jefe_Top_2_Provincias</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>PER_NEG_Supervisor_Top_2_Provincias_Canal_1</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>PER_NEG_Supervisor_Top_2_Provincias_Canal_2</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>PER_NEG_Supervisor_Top_2_Provincias_Canal_3</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>PER_NEG_Supervisor_Top_2_Provincias_Canal_4</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Notificacion_Cambio_Oportunidades</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_Alerta_Oportunidad_Ganada_Negocios</fullName>
        <ccEmails>hacaroal@tgestiona.com.pe</ccEmails>
        <description>BI_PER_Alerta Oportunidad Ganada - Negocios</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>PER_NEG_Gestor_de_aseguramiento</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>PER_NEG_Preventa</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>eduardo.canchano@telefonica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/Oportunidades_Ganadas</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_Alerta_Top_2_Lima</fullName>
        <description>BI_PER_Alerta Top 2 Lima para Negocios</description>
        <protected>false</protected>
        <recipients>
            <recipient>PER_NEG_Jefe_Top_2_Lima</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>PER_NEG_Supervisor_Top_2_Lima_Canal_1</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>PER_NEG_Supervisor_Top_2_Lima_Canal_2</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_PlantillaCreacionOportunidad</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_Alerta_al_Encargado_de_Licitaciones</fullName>
        <description>BI_PER_Alerta al Encargado de Licitaciones y EECC</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Notificacion_Cambio_Oportunidades</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_Avisar_a_administrador_de_contrato</fullName>
        <description>BI_PER_Avisar a aministrador de contrato</description>
        <protected>false</protected>
        <recipients>
            <recipient>narda.cornejo@telefonica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Comunicar_administracion_de_contratos_cierre_de_una_oportunidad_renovacion</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_Avisar_a_administrador_de_contrato_empresas</fullName>
        <description>BI_PER_Avisar a aministrador de contrato empresas</description>
        <protected>false</protected>
        <recipients>
            <recipient>soportebienperu@telefonica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Comunicar_administracion_de_contratos_cierre_de_una_oportunidad_renovacion</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_Avisar_al_administrador_de_que_existe_una_oportunidad_pendiente_de_contra</fullName>
        <description>BI_PER_Avisar_al_administrador_de_que_existe_una_oportunidad_pendiente_de_contrato</description>
        <protected>false</protected>
        <recipients>
            <recipient>narda.cornejo@telefonica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_informar_de_que_oportunidad_ganada_requiere_de_contrato</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_Aviso_renovacion_oportunidad</fullName>
        <description>Aviso renovacion oportunidad Per</description>
        <protected>false</protected>
        <recipients>
            <recipient>narda.cornejo@telefonica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Renovacion_oportunidad</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_Aviso_renovacion_oportunidad_empresas</fullName>
        <description>Aviso renovacion oportunidad Per empresas</description>
        <protected>false</protected>
        <recipients>
            <recipient>soportebienperu@telefonica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Renovacion_oportunidad</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_Encargado_de_la_Opportunidad</fullName>
        <description>BI_PER_Encargado de la Opportunidad</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Notificacion_Cambio_Oportunidades</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_Generar_nuevo_contrato</fullName>
        <description>Generar nuevo contrato Per</description>
        <protected>false</protected>
        <recipients>
            <recipient>narda.cornejo@telefonica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Generar_nuevo_contrato</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_Generar_nuevo_contrato_empresas</fullName>
        <description>Generar nuevo contrato Per empresas</description>
        <protected>false</protected>
        <recipients>
            <recipient>jaime.araujo@telefonica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Generar_nuevo_contrato</template>
    </alerts>
    <alerts>
        <fullName>BI_PER_empresas_Avisar_al_administrador_de_que_existe_una_oportunidad_contrato</fullName>
        <description>BI_PER_Avisar_al_administrador_de_que_existe_una_oportunidad_pendiente_de_contrato_empresas</description>
        <protected>false</protected>
        <recipients>
            <recipient>jaime.araujo@telefonica.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/BI_Email_informar_de_que_oportunidad_ganada_requiere_de_contrato</template>
    </alerts>
    <alerts>
        <fullName>FS_CORE_Revision_de_la_asignacion_del_cliente</fullName>
        <description>FS CORE Revisión de la asignación del cliente</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales Intelligence</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FS_CHILE_PERU/FS_CORE_Validacion_Inteligencia_Comercial</template>
    </alerts>
    <alerts>
        <fullName>TIWS_1_dias_antes_Closed_date</fullName>
        <description>TIWS_1_dias_antes_Closed_date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TIWS/TIWS_1_dias_antes_Closed_date</template>
    </alerts>
    <alerts>
        <fullName>TIWS_7_dias_antes_Closed_date</fullName>
        <description>TIWS_7_dias_antes_Closed_date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TIWS/TIWS_7_dias_antes_Closed_date</template>
    </alerts>
    <alerts>
        <fullName>TIWS_83_dias_despues_Last_modified_date</fullName>
        <description>TIWS_83_dias_despues_Last_modified_date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TIWS/TIWS_83_dias_despues_Last_modified_date</template>
    </alerts>
    <alerts>
        <fullName>TIWS_89_dias_despues_Last_modified_date</fullName>
        <description>TIWS_89_dias_despues_Last_modified_date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TIWS/TIWS_89_dias_despues_Last_modified_date</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_ARG_Actualiza_Duplicado</fullName>
        <description>Actualiza el campo Duplicado de la oportunidad con el valor False.

Creado por el eHelp 01942984</description>
        <field>BI_ARG_Duplicado__c</field>
        <literalValue>0</literalValue>
        <name>BI_ARG_Actualiza_Duplicado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_ARG_ActualizacionFactibilidadTecnica</fullName>
        <description>Actualiza el campo BI_Factibilidad_tecnica_legado_arg__c cuando se crea una oportunidad de VE</description>
        <field>BI_Factibilidad_tecnica_legado_arg__c</field>
        <literalValue>1</literalValue>
        <name>BI_ARG_ActualizacionFactibilidadTecnica</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_ARG_ActualizacionNoRequiereComite</fullName>
        <description>Actualiza el campo BI_No_requiere_comite_arg_c cuando se crea una oportunidad de VE</description>
        <field>BI_No_requiere_comite_arg__c</field>
        <literalValue>1</literalValue>
        <name>BI_ARG_ActualizacionNoRequiereComite</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_ARG_ActualizacionNoRequierePricing</fullName>
        <description>Actualiza el campo BI_No_requiere_Pricing_arg__c  cuando se crea una oportunidad de VE</description>
        <field>BI_No_requiere_Pricing_arg__c</field>
        <literalValue>1</literalValue>
        <name>BI_ARG_ActualizacionNoRequierePricing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_Analisis_de_Riesgo_a_CR</fullName>
        <field>BI_Analisis_de_riesgo__c</field>
        <literalValue>Enviar a validación</literalValue>
        <name>BI Actualiza Análisis de Riesgo a CR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_Analisis_de_Riesgo_a_SR</fullName>
        <field>BI_Analisis_de_riesgo__c</field>
        <literalValue>Sin riesgo</literalValue>
        <name>BI Actualiza Análisis de Riesgo a Sin ri</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_Comite_de_negocio</fullName>
        <field>BI_Comite_de_negocios__c</field>
        <literalValue>Disponible para envío</literalValue>
        <name>BI Actualiza Comité de negocio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_Oferta_tecnica_pend_aprob</fullName>
        <field>BI_Oferta_tecnica__c</field>
        <literalValue>Pendiente de aprobación</literalValue>
        <name>BI Actualiza Oferta tecnica pend aprob</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_Rechazo_comite</fullName>
        <field>BI_Comite_de_negocios__c</field>
        <literalValue>Rechazado Comité</literalValue>
        <name>BI_Actualiza Rechazo comite</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_Tipo_de_registro_Completo</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BI_Ciclo_completo</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>BI Actualiza Tipo de registro Completo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_estado_aprobado_Comite</fullName>
        <field>BI_Comite_de_negocios__c</field>
        <literalValue>Aprobado Comité</literalValue>
        <name>BI Actualiza estado aprobado Comité</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_estado_aprobado_F_comercial</fullName>
        <field>BI_Analisis_de_riesgo__c</field>
        <literalValue>Riesgo aprobado</literalValue>
        <name>BI Actualiza estado aprobado F.comercial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_estado_aprobado_F_economica</fullName>
        <field>BI_Oferta_economica__c</field>
        <literalValue>Descuento aprobado</literalValue>
        <name>BI Actualiza estado aprobado F.económica</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_estado_aprobado_F_tecnica</fullName>
        <field>BI_Oferta_tecnica__c</field>
        <literalValue>Oferta técnica aprobada</literalValue>
        <name>BI Actualiza estado aprobado F.tecnica</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_estado_pdte_apr_F_comercial</fullName>
        <field>BI_Analisis_de_riesgo__c</field>
        <literalValue>Pendiente de aprobación</literalValue>
        <name>BI Actualiza estado pdte apr F.comercial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_estado_pend_F_economica</fullName>
        <field>BI_Oferta_economica__c</field>
        <literalValue>Pendiente de aprobación</literalValue>
        <name>BI Actualiza estado pend F.economica</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_estado_pend_aprob_Comite</fullName>
        <field>BI_Comite_de_negocios__c</field>
        <literalValue>Pendiente de aprobación</literalValue>
        <name>BI Actualiza estado pend aprob Comité</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_estado_rechazado_Comite</fullName>
        <field>BI_Comite_de_negocios__c</field>
        <literalValue>Rechazado Comité</literalValue>
        <name>BI Actualiza estado rechazado Comité</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_estado_rechazado_F_economic</fullName>
        <field>BI_Oferta_economica__c</field>
        <literalValue>Descuento rechazado</literalValue>
        <name>BI Actualiza estado rechazado F.económic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_estado_rechazado_F_tecnica</fullName>
        <field>BI_Oferta_tecnica__c</field>
        <literalValue>Oferta técnica rechazada</literalValue>
        <name>BI Actualiza estado rechazado F.tecnica</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizacion_Fecha_cierre</fullName>
        <field>CloseDate</field>
        <formula>BI_Fecha_de_cierre_real__c</formula>
        <name>Actualización  Fecha cierre</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizar_FCV</fullName>
        <field>Amount</field>
        <formula>BI_Ingreso_por_unica_vez__c + (BI_Duracion_del_contrato_Meses__c * BI_Recurrente_bruto_mensual__c)</formula>
        <name>BI_Actualizar FCV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizar_ProbabilidadDeExito</fullName>
        <field>BI_Probabilidad_de_exito__c</field>
        <literalValue>100</literalValue>
        <name>BI_Actualizar_ProbabilidadDeExito</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizar_Type_de_cliente</fullName>
        <field>Type</field>
        <literalValue>Customer</literalValue>
        <name>BI Actualizar Type de cliente</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizar_campo_legalizada_no_requie</fullName>
        <field>BI_Legalizada__c</field>
        <literalValue>No requiere contrato</literalValue>
        <name>BI_Actualizar campo legalizada no requie</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizar_casilla_desarrollo</fullName>
        <field>BI_Casilla_desarrollo__c</field>
        <literalValue>0</literalValue>
        <name>BI_Actualizar_casilla_desarrollo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_COL_Actual_Analisis_Riesgo_Aprobado</fullName>
        <field>BI_Analisis_de_riesgo__c</field>
        <literalValue>Riesgo aprobado</literalValue>
        <name>BI_COL_Actual Analisis Riesgo_Aprobado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_COL_Actual_Analisis_Riesgo_Rechazado</fullName>
        <field>BI_Analisis_de_riesgo__c</field>
        <literalValue>Riesgo rechazado</literalValue>
        <name>BI_COL_Actual Analisis Riesgo_Rechazado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Email_del_Gestor_del_propietario</fullName>
        <description>Este Field Update rellena el campo BI_Email_del_Gestor_del_propietario__c con el valor de BI_Email_del_Gestor_del_propietario_TXT__c</description>
        <field>BI_Email_del_Gestor_del_propietario__c</field>
        <formula>BI_Email_del_Gestor_del_propietario_TXT__c</formula>
        <name>BI Email del Gestor del propietario</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Enviar_Economica</fullName>
        <field>BI_Oferta_economica__c</field>
        <literalValue>Enviar a aprobación</literalValue>
        <name>BI_Enviar_Economica</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Estado_rechazado_F_Comercial</fullName>
        <field>BI_Analisis_de_riesgo__c</field>
        <literalValue>Riesgo rechazado</literalValue>
        <name>BI Estado rechazado F.Comercial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_FVI_Actualizar_SubEstado</fullName>
        <description>se actualiza el campo a Oferta Sincronizada</description>
        <field>BI_FVI_SubEstado__c</field>
        <literalValue>Oferta Sincronizada</literalValue>
        <name>BI_FVI_Actualizar_SubEstado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_FVI_Actualizar_imp_rec_inicial</fullName>
        <description>Actualiza el Importe recurrente inicial con el valor de recurring charge la primera vez</description>
        <field>BI_FVI_Importe_Recurrente_Inicial__c</field>
        <formula>BI_Recurrente_bruto_mensual__c</formula>
        <name>BI_FVI_Actualizar_imp_rec_inicial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_FVI_Actualizar_importe_inicial</fullName>
        <field>BI_FVI_Importe_Inicial__c</field>
        <formula>BI_Ingreso_por_unica_vez__c</formula>
        <name>BI_FVI_Actualizar_importe_inicial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Fecha_Real</fullName>
        <description>Pone la fecha del día actual en el campo Fecha de cierre real</description>
        <field>BI_Fecha_de_cierre_real__c</field>
        <formula>TODAY()</formula>
        <name>BI_Fecha_Real</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Fecha_Real_Nula</fullName>
        <description>Actualiza la fecha real de cierre de una oportunidad con un valor nulo.</description>
        <field>BI_Fecha_de_cierre_real__c</field>
        <name>Fecha Real Nula</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Legalizada_PendienteContrato</fullName>
        <field>BI_Legalizada__c</field>
        <literalValue>Pendiente contrato</literalValue>
        <name>BI_Legalizada_PendienteContrato</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_MEX_Actualizar_casilla_enviado</fullName>
        <description>Actualiza la casilla BI_MEX_Enviado__c para que la alerta de correo no se envíe mas de una vez</description>
        <field>BI_MEX_Enviado__c</field>
        <literalValue>1</literalValue>
        <name>Actualizar casilla enviado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Modifica_legalizada</fullName>
        <field>BI_Legalizada__c</field>
        <literalValue>Pendiente contrato</literalValue>
        <name>Modifica_legalizada</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_ActCampoAppEconomicaTNA</fullName>
        <description>TNA Aprobación Económica</description>
        <field>BI_Oferta_economica__c</field>
        <literalValue>Descuento aprobado</literalValue>
        <name>BI_O4_ActCampoAppEconomicaTNA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_ActCampoAppEconomicaTNARechazo</fullName>
        <description>TNA Aprobación económica</description>
        <field>BI_Oferta_economica__c</field>
        <literalValue>Descuento rechazado</literalValue>
        <name>BI_O4_ActCampoAppEconomicaTNARechazo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Aprobado</fullName>
        <field>BI_Oferta_economica__c</field>
        <literalValue>Descuento aprobado</literalValue>
        <name>BI_O4_Aprobado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_FactEconomicaTNA</fullName>
        <field>BI_Oferta_economica__c</field>
        <literalValue>Pendiente de aprobación</literalValue>
        <name>BI_O4_FactEconomicaTNA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Rechazado</fullName>
        <field>BI_Oferta_economica__c</field>
        <literalValue>Descuento rechazado</literalValue>
        <name>BI_O4_Rechazado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Opty_RecordType_Rapido_Completo</fullName>
        <description>Regla para actualizar el Record Type de Rápido a Completo cuando se cambie en el Campo &apos;Ciclo de venta&apos;.</description>
        <field>RecordTypeId</field>
        <lookupValue>BI_Ciclo_completo</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>BI Opty RecordType Rapido - Completo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_RequiereContrato_True</fullName>
        <field>BI_Requiere_contrato__c</field>
        <literalValue>1</literalValue>
        <name>BI_RequiereContrato_True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_USA_ExpirationDate_VE</fullName>
        <description>Demanda 489
Actualiza campo BI_Fecha_de_vigencia__c al pasar a F3</description>
        <field>BI_Fecha_de_vigencia__c</field>
        <formula>(Today() + 10)</formula>
        <name>BI_USA_ExpirationDate_VE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_actualiza_a_etata_F5</fullName>
        <field>StageName</field>
        <literalValue>F5 - Solution Definition</literalValue>
        <name>BI actualiza a etata F5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_comite_de_negocios_no_disponible</fullName>
        <field>BI_Comite_de_negocios__c</field>
        <name>comite de negocios no disponible</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FS_CORE_Conversion_a_Cliente_Actual</fullName>
        <description>Se convierte de cliente potencial a cliente actual</description>
        <field>Type</field>
        <literalValue>Customer</literalValue>
        <name>FS CORE Conversion a Cliente Actual</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TIWS_Close_Date</fullName>
        <description>Migración TIWS: cuando CloseDate = TODAY el campo &quot;BI_Fecha_de_cierre_real__c&quot; toma el valor TODAY</description>
        <field>BI_Fecha_de_cierre_real__c</field>
        <formula>TODAY()</formula>
        <name>TIWS_Close_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TIWS_Not_Updated</fullName>
        <description>Migración TIWS: Cierra la oportunidad cuando  la fecha actual sea igual a  &quot;Fecha de cierre estimada&quot;</description>
        <field>BI_Motivo_de_perdida__c</field>
        <literalValue>No actualizada</literalValue>
        <name>TIWS_Not_Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TIWS_Stage</fullName>
        <description>Establece que StageName = &apos;F1 - Cancelled | Suspended&apos;</description>
        <field>StageName</field>
        <literalValue>F1 - Cancelled | Suspended</literalValue>
        <name>TIWS_Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI Email del Gestor del porpietario de la Oportunidad</fullName>
        <actions>
            <name>BI_Email_del_Gestor_del_propietario</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Esta Workflow Rule lanza un Field Update para rellenar el campo BI_Email_del_Gestor_del_propietario__c con el valor de BI_Email_del_Gestor_del_propietario_TXT__c</description>
        <formula>AND( (ISBLANK(BI_Email_del_Gestor_del_propietario__c) ||    ISCHANGED(BI_Email_del_Gestor_del_propietario_TXT__c)),  NOT(IsClosed) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI Opty Cambio RecordType Rapido - Completo</fullName>
        <actions>
            <name>BI_Opty_RecordType_Rapido_Completo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Regla para actualizar el Record Type de Rápido a Completo cuando se cambie en el Campo &apos;Ciclo de venta&apos;.</description>
        <formula>AND( ISPICKVAL(PRIORVALUE(BI_Ciclo_ventas__c),&apos;Rápido&apos;), ISPICKVAL(BI_Ciclo_ventas__c,&apos;Completo&apos;), ISBLANK(TEXT(BI_Tipo_de_renovacion__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_ARG_Actualiza_Duplicado</fullName>
        <actions>
            <name>BI_ARG_Actualiza_Duplicado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Regla que desmarca la casilla Duplicado de la oportunidad cuando esta pasa a F3 o cuando se cambia el cliente de la oportunidad para oportunidades de Argentina.

Creado por el eHelp 01942984.</description>
        <formula>OR(  AND(   ISCHANGED(AccountId),   BI_ARG_Duplicado__c,   TEXT(BI_Country__c) = $Label.BI_Argentina  ),  AND(   ISCHANGED(StageName),   TEXT(StageName) = $Label.BI_F3OfertaPresentada,   BI_ARG_Duplicado__c,   TEXT(BI_Country__c) = $Label.BI_Argentina  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_ARG_CheckProcesosAprobacion</fullName>
        <actions>
            <name>BI_ARG_ActualizacionFactibilidadTecnica</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_ARG_ActualizacionNoRequiereComite</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_ARG_ActualizacionNoRequierePricing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BI_Country__c</field>
            <operation>equals</operation>
            <value>Argentina</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.VE_Express__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>F4 - Offer Development</value>
        </criteriaItems>
        <description>EHelp 03046537 - Marca los check para saltar los procesos de aprobación cuando es una oportunidad de Venta Express</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_ARG_Email_comunicar_administrador_de_contratos_cierre_retencion</fullName>
        <actions>
            <name>BI_ARG_Avisar_a_administrador_de_contrato</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Comunica a administrador de contratos el cierre de una oportunidad de retención.</description>
        <formula>IsClosed = True &amp;&amp;   ISPICKVAL(BI_Tipo_de_renovacion__c  , &apos;Retención&apos;)  &amp;&amp;   ISPICKVAL(BI_Country__c , &apos;Argentina&apos;) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_ARG_Generar contrato tras aceptacion nueva oferta</fullName>
        <actions>
            <name>BI_ARG_Generar_nuevo_contrato</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Se va a generar un aviso al gestor de contrato cuando el cliente acepte una oferta para que genere un contrato</description>
        <formula>BI_Requiere_contrato__c  = true &amp;&amp;   ISPICKVAL(StageName, &apos;F1 - Closed Won&apos;)  &amp;&amp;  ISPICKVAL(BI_Country__c , &apos;Argentina&apos;) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_ARG_Renovacion oportunidad</fullName>
        <actions>
            <name>BI_ARG_Aviso_renovacion_oportunidad</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Si se produce una renovación automática se generará una alerta automática al administrador de contratos para el cambio de la fecha del contrato (actualización de la fecha de este)</description>
        <formula>( ISPICKVAL( BI_Tipo_de_renovacion__c , &apos;Renegociación&apos;) ||  ISPICKVAL( BI_Tipo_de_renovacion__c , &apos;Renovación&apos;)) &amp;&amp;  ISPICKVAL(BI_Country__c , &apos;Argentina&apos;) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_ARG_crear_una_tarea_al_Administrador_del_contrato</fullName>
        <actions>
            <name>BI_ARG_Avisar_al_administrador_de_que_existe_una_oportunidad_pendiente_de_contra</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BI_ARG_Oferta_requiere_de_contrato</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Cuando se marca la casilla Requiere contrato y esta ganada, se deberá crear una tarea al Administrador del contrato y un aviso para la creación de un contrato si fuera necesario.</description>
        <formula>BI_Requiere_contrato__c =true &amp;&amp;  ISPICKVAL(StageName , &apos;F1 - Closed Won&apos;) &amp;&amp;  ISPICKVAL(BI_Country__c , &apos;Argentina&apos;) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualiza Analisis de Riesgo a Con riesgo</fullName>
        <actions>
            <name>BI_Actualiza_Analisis_de_Riesgo_a_CR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR(  ISPICKVAL(Account.BI_Riesgo__c, &apos;2 - Requiere autorización&apos;), ISPICKVAL(Account.BI_Riesgo__c, &apos;3 - Bloquea al cliente&apos;) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualiza Analisis de Riesgo a Sin riesgo</fullName>
        <actions>
            <name>BI_Actualiza_Analisis_de_Riesgo_a_SR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISPICKVAL(Account.BI_Riesgo__c, &apos;1 - Sin problema&apos;), ISPICKVAL(Account.BI_Riesgo__c, &apos;&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualiza Comite de negocio</fullName>
        <actions>
            <name>BI_Actualiza_Comite_de_negocio</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.BI_Oferta_economica__c</field>
            <operation>equals</operation>
            <value>Descuento rechazado</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualiza Comite de negocio rechazo</fullName>
        <actions>
            <name>BI_comite_de_negocios_no_disponible</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.BI_Oferta_economica__c</field>
            <operation>notEqual</operation>
            <value>Descuento rechazado</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualiza tipo del cliente</fullName>
        <actions>
            <name>BI_Actualizar_Type_de_cliente</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>F1 - Closed Won,Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>notEqual</operation>
            <value>Customer</value>
        </criteriaItems>
        <description>Demanda 518
Actualiza el tipo a customer, cuando tenga una oportunidad cerrada ganada</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualiza_Fecha_Cierre_Real</fullName>
        <actions>
            <name>BI_Fecha_Real</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>F1 - Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>F1 - Cancelled | Suspended</value>
        </criteriaItems>
        <description>Lanza un Field Update que rellenara el campo Fecha de Cierre Real con el día en el que se produzca el cambio de etapa.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualiza_Legalizada</fullName>
        <actions>
            <name>BI_Modifica_legalizada</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.BI_Requiere_contrato__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>Actualiza el campo Legalizada cuando la oportunidad requiere contrato.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizar FCV</fullName>
        <actions>
            <name>BI_Actualizar_FCV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(RecordType.Name = &quot;Agrupación&quot;), !$Setup.BI_CHI_SISON__c.BI_CHI_WS__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizar campo legalizada</fullName>
        <actions>
            <name>BI_Actualizar_campo_legalizada_no_requie</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>F1 - Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.BI_Requiere_contrato__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.BI_Ultrarapida__c</field>
            <operation>equals</operation>
            <value>Falso</value>
        </criteriaItems>
        <description>Actualizar en una Oportunidad F1-Ganada, el campo &quot;legalizada&quot; con el valor &quot;No requiere contrato&quot; cuando el campo &quot;Requiere contrato&quot; no esta marcado.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizar_Fecha_de_cierre_estimado</fullName>
        <actions>
            <name>BI_Actualizacion_Fecha_cierre</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Actualiza la fecha de cierre estimado con el valor del campo Fecha de cierre real una vez que este campo sea rellenado.</description>
        <formula>NOT(ISBLANK(  BI_Fecha_de_cierre_real__c  )) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c ) &amp;&amp;  TEXT(BI_Country__c) != &apos;Argentina&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizar_casilla_desarrollo</fullName>
        <actions>
            <name>BI_Actualizar_casilla_desarrollo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>!$Setup.BI_bypass__c.BI_migration__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Cambio_Probabilidad_Ganada</fullName>
        <actions>
            <name>BI_Actualizar_ProbabilidadDeExito</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>F1 - Closed Won</value>
        </criteriaItems>
        <description>Cambia el campo BI_Probabilidad_de_exito__c a 100 cuando la oportunidad pasa a F1-Ganada. eHelp 03227879</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Comunicacion propietario opp 2 dias antes vigencia</fullName>
        <active>true</active>
        <description>Comunica al propietario de la oportunidad de que la fecha de vigencia es en 7 días.</description>
        <formula>AND(  NOT(ISPICKVAL(StageName,&quot;F1 - Closed Lost&quot;)),NOT(ISPICKVAL(StageName,&quot;F1 - Cancelled | Suspended&quot;)), NOT(ISPICKVAL(StageName,&quot;F1 - Closed Won&quot;)),  NOT(ISPICKVAL(StageName,&quot;Cancelled | Suspended&quot;)), NOT(ISPICKVAL(StageName,&quot;Closed Won&quot;)) , NOT(ISPICKVAL(StageName,&quot;Closed Lost&quot;))) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_Comunica_prop_2_dias_antes_fecha_vigencia</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.BI_Fecha_de_vigencia__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_ECU_Envio_email_oprtunidad_ganada</fullName>
        <actions>
            <name>BI_ECU_Oportunidad_ganada</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>F1 - Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.BI_Country__c</field>
            <operation>equals</operation>
            <value>Ecuador</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Elimina_Fecha_Cierre_Real</fullName>
        <actions>
            <name>BI_Fecha_Real_Nula</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF( 	OR( 		ISPICKVAL(PRIORVALUE(StageName), &quot;F1 - Cancelled | Suspended&quot;), 		ISPICKVAL(PRIORVALUE(StageName), &quot;F1 - Closed Lost&quot;) 	), 	NOT( 		OR( 			ISPICKVAL(StageName, &quot;F1 - Closed Lost&quot;), 			ISPICKVAL(StageName, &quot;F1 - Cancelled | Suspended&quot;) 		) 	),false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Email_comunicar_administrador_de_contratos_cierre_retencion</fullName>
        <active>true</active>
        <description>Comunica a administrador de contratos el cierre de una oportunidad de retención.</description>
        <formula>IsClosed = True &amp;&amp;   ISPICKVAL(BI_Tipo_de_renovacion__c  , &apos;Retención&apos;)  &amp;&amp;  !ISPICKVAL(BI_Country__c , &apos;Argentina&apos;) &amp;&amp;   !ISPICKVAL(BI_Country__c , &apos;Peru&apos;) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_FVI_Actualizar_imp_rec_inicial</fullName>
        <actions>
            <name>BI_FVI_Actualizar_imp_rec_inicial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Actualiza el campo BI_FVI_Importe_Recurrente_Inicial__c con el valor del campo BI_Recurrente_bruto_mensual__c la primera vez que se calcula</description>
        <formula>AND( ISPICKVAL( BI_Opportunity_Type__c , &apos;Fijo&apos;),OR(ISNULL(BI_FVI_Importe_Recurrente_Inicial__c),BI_FVI_Importe_Recurrente_Inicial__c = 0), BI_Recurrente_bruto_mensual__c &gt; 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_FVI_Actualizar_importe_inicial</fullName>
        <actions>
            <name>BI_FVI_Actualizar_importe_inicial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Actualiza el campo BI_FVI_Importe_Inicial__c con el primer cálculo del BI_Ingreso_por_unica_vez__c</description>
        <formula>AND( ISPICKVAL( BI_Opportunity_Type__c , &apos;Fijo&apos;),OR(ISNULL(BI_FVI_Importe_Inicial__c),BI_FVI_Importe_Inicial__c = 0),  BI_Ingreso_por_unica_vez__c &gt; 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_FVI_SubEstado_Sincronizado</fullName>
        <actions>
            <name>BI_FVI_Actualizar_SubEstado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.BI_FVI_SubEstado__c</field>
            <operation>equals</operation>
            <value>Pdte Sincronizar Oferta</value>
        </criteriaItems>
        <description>Cambia el estado &quot;Pdte Sincronizar Oferta&quot; a &quot;Oferta Sincronizada&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Generar Tarea automatica arg</fullName>
        <actions>
            <name>BI_Completar_Parque_comercial_competencia_arg</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Genera una actividad al Ejecutivo indicando: Completar Parque comercial Competencia.</description>
        <formula>NOT(ISPICKVAL(BI_ARG_Competidor_perdida__c, &quot;&quot;)) &amp;&amp; ISPICKVAL(BI_Country__c , &apos;Argentina&apos;) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Generar contrato tras aceptacion nueva oferta</fullName>
        <active>true</active>
        <description>Se va a generar un aviso al gestor de contrato cuando el cliente acepte una oferta para que genere un contrato</description>
        <formula>BI_Requiere_contrato__c = true &amp;&amp; ISPICKVAL(StageName, &apos;F1 - Closed Won&apos;) &amp;&amp; TEXT(BI_Country__c) &lt;&gt; &apos;Peru&apos; &amp;&amp; TEXT(BI_Country__c) &lt;&gt; &apos;Argentina&apos; &amp;&amp; (!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Alerta_FCV</fullName>
        <actions>
            <name>BI_MEX_Alerta_correo_FCV</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BI_MEX_Actualizar_casilla_enviado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( BI_MEX_Enviar__c, NOT(BI_MEX_Enviado__c), ISPICKVAL(BI_Country__c,&apos;Mexico&apos;), NOT(BI_MEX_Control_alerta_FCV__c), Owner.Profile.Name = &apos;BI_Standard_MEX&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_MEX_Alerta_FCV2</fullName>
        <actions>
            <name>BI_MEX_Alerta_correo_FCV</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BI_MEX_Actualizar_casilla_enviado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  BI_MEX_Enviar__c,  NOT(BI_MEX_Enviado__c),  ISPICKVAL(BI_Country__c,&apos;Mexico&apos;),  BI_MEX_Control_alerta_FCV__c,  Owner.Profile.Name = &apos;BI_Standard_MEX&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Notificacion_1AntesDeVencerse</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>F1 - Partially Won,F1 - Closed Won,F1 - Closed Lost,F1 - Cancelled | Suspended</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.BI_Country__c</field>
            <operation>notEqual</operation>
            <value>Argentina</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_Notificacion_a_1_dia_de_vencer</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_Notificacion_1AntesDeVencerse_ARG</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>F1 - Partially Won,F1 - Closed Won,F1 - Closed Lost,F1 - Cancelled | Suspended,F6 - Prospecting,F5 - Solution Definition</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.BI_Country__c</field>
            <operation>equals</operation>
            <value>Argentina</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_Notificacion_a_1_dia_de_vencer_ARG</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_Notificacion_7AntesDeVencerse</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>F1 - Partially Won,F1 - Closed Won,F1 - Closed Lost,F1 - Cancelled | Suspended</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.BI_Country__c</field>
            <operation>notEqual</operation>
            <value>Argentina</value>
        </criteriaItems>
        <description>Demanda 548</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_Notificacion_7_dias_de_vencer</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_NuevaOportunidad</fullName>
        <actions>
            <name>BI_Notificacion_asignacion_nueva_oportunidad</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Criterios de entrada
Cuando el que crea la oportunidad
Acciones
Creación de alerta de correo electrónico al propietario de la oportunidad
Creación de la plantilla</description>
        <formula>AND(CreatedBy.Profile.Name  &lt;&gt; &apos;Administrador del sistema&apos;,CreatedBy.Profile.Name  &lt;&gt; &apos;System Administrator&apos;, CreatedBy.Profile.Name  &lt;&gt; &apos;BI_Usuario de integracion&apos;,CreatedBy.Profile.Name  &lt;&gt; &apos;BI_Usuario de operaciones&apos;,  RecordType.DeveloperName &lt;&gt; &apos;BI_FVI_Ciclo_completo&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_Desbloqueo de cuentas que no han sido gestionadas en 90 dias</fullName>
        <active>true</active>
        <description>Desbloqueo de cuentas que no han sido gestionadas en 90 días de Negocios</description>
        <formula>AND( ISPICKVAL(BI_Country__c , &quot;Peru&quot;),  TODAY()- DateValue(LastModifiedDate) &gt; 90, ISPICKVAL(Account.BI_Segment__c, &apos;Negocios&apos;), !$Setup.BI_bypass__c.BI_migration__c  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_PER_Encargado_de_la_Opportunidad</name>
                <type>Alert</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_PER_Email_comunicar_administrador_de_contratos_cierre_retencion</fullName>
        <actions>
            <name>BI_PER_Avisar_a_administrador_de_contrato</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Comunica a administrador de contratos el cierre de una oportunidad de retención.</description>
        <formula>IsClosed = True &amp;&amp;   ISPICKVAL(BI_Tipo_de_renovacion__c  , &apos;Retención&apos;)  &amp;&amp;   ISPICKVAL(BI_Country__c , &apos;Peru&apos;) &amp;&amp; TEXT(Account.BI_Segment__c) = &quot;Negocios&quot; &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_Email_comunicar_administrador_de_contratos_cierre_retencion_empresas</fullName>
        <actions>
            <name>BI_PER_Avisar_a_administrador_de_contrato_empresas</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Comunica a administrador de contratos el cierre de una oportunidad de retención.</description>
        <formula>IsClosed = True &amp;&amp;   ISPICKVAL(BI_Tipo_de_renovacion__c  , &apos;Retención&apos;)  &amp;&amp;   ISPICKVAL(BI_Country__c , &apos;Peru&apos;) &amp;&amp; ISPICKVAL(Account.BI_Segment__c, &quot;Empresas&quot;) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_Generar contrato tras aceptacion nueva oferta</fullName>
        <actions>
            <name>BI_PER_Generar_nuevo_contrato</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Se va a generar un aviso al gestor de contrato cuando el cliente acepte una oferta para que genere un contrato</description>
        <formula>BI_Requiere_contrato__c  = true &amp;&amp;   ISPICKVAL(StageName, &apos;F1 - Closed Won&apos;)  &amp;&amp;  ISPICKVAL(BI_Country__c , &apos;Peru&apos;) &amp;&amp; ISPICKVAL(Account.BI_Segment__c, &apos;Negocios&apos;) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_Generar contrato tras aceptacion nueva oferta empresas</fullName>
        <actions>
            <name>BI_PER_Generar_nuevo_contrato_empresas</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Se va a generar un aviso al gestor de contrato cuando el cliente acepte una oferta para que genere un contrato</description>
        <formula>BI_Requiere_contrato__c  = true &amp;&amp;   ISPICKVAL(StageName, &apos;F1 - Closed Won&apos;)  &amp;&amp;  ISPICKVAL(BI_Country__c , &apos;Peru&apos;) &amp;&amp; ISPICKVAL(Account.BI_Segment__c, &apos;Empresas&apos;) &amp;&amp; (!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_NEG Notificacion de creacion de oportunidades de empresas del Estado</fullName>
        <actions>
            <name>BI_PER_Alerta_al_Encargado_de_Licitaciones</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notificación de creación de oportunidades de empresas del Estado
Alerta a EECC, Encargado de Licitaciones</description>
        <formula>AND(         ISPICKVAL( Account.BI_Ambito__c, &apos;Público&apos;),        ISPICKVAL(Account.BI_Segment__c,  &apos;Negocios&apos;),        !$Setup.BI_bypass__c.BI_migration__c     )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_NEG Notifiicacion de vencimiento de contrato con Telefonica</fullName>
        <active>true</active>
        <description>Notifiicación de vencimiento de contrato con Telefónica
Alerta de correo electrónico (30 días antes de Fecha Ganada + Tiempo de contrato)
Alerta a EECC</description>
        <formula>AND( ISPICKVAL(BI_Country__c , &quot;Peru&quot;),      ISPICKVAL(Account.BI_Segment__c, &apos;Negocios&apos;),      IsWon,      !$Setup.BI_bypass__c.BI_migration__c    )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BI_PER_Encargado_de_la_Opportunidad</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.BI_Fecha_de_fin_de_contrato__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>BI_PER_NEG_Creacion de oportunidades Top 2 - Lima</fullName>
        <actions>
            <name>BI_PER_Alerta_Top_2_Lima</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Creación de la oportunidad de cliente SubSegmento Local = Alto Valor Lima: Top 2</description>
        <formula>AND(  ISPICKVAL(BI_Country__c , &quot;Peru&quot;),   ISPICKVAL(Account.BI_Segment__c, &apos;Negocios&apos;),  ISPICKVAL(Account.BI_Subsegment_Local__c, &quot;Alto valor Lima:top 2&quot;), !$Setup.BI_bypass__c.BI_migration__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_NEG_Creacion de oportunidades Top 2 - Provincia</fullName>
        <actions>
            <name>BI_PER_Alerta_Jefes_Top_2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Creación de la oportunidad de cliente SubSegmento Local = Alto Valor Provincia: Top 2</description>
        <formula>AND(  ISPICKVAL(BI_Country__c , &quot;Peru&quot;),   ISPICKVAL(Account.BI_Segment__c, &apos;Negocios&apos;),  ISPICKVAL(Account.BI_Subsegment_Local__c, &quot;Alto valor provincias: top 2&quot;), !$Setup.BI_bypass__c.BI_migration__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_NEG_Oportunidad Ganada Regla</fullName>
        <actions>
            <name>BI_PER_Alerta_Oportunidad_Ganada_Negocios</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Oportunidad Ganada - Negocios</description>
        <formula>AND(  IsWon , ISPICKVAL(BI_Country__c , &quot;Peru&quot;), ISPICKVAL(Account.BI_Segment__c,&apos;Negocios&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_Renovacion oportunidad</fullName>
        <actions>
            <name>BI_PER_Aviso_renovacion_oportunidad</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Si se produce una renovación automática se generará una alerta automática al administrador de contratos para el cambio de la fecha del contrato (actualización de la fecha de este)</description>
        <formula>( ISPICKVAL( BI_Tipo_de_renovacion__c , &apos;Renegociación&apos;) || ISPICKVAL( BI_Tipo_de_renovacion__c , &apos;Renovación&apos;)) &amp;&amp;  ISPICKVAL(BI_Country__c, &apos;Peru&apos;) &amp;&amp; TEXT(Account.BI_Segment__c) = &apos;Negocios&apos; &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_Renovacion oportunidad empresas</fullName>
        <actions>
            <name>BI_PER_Aviso_renovacion_oportunidad_empresas</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Si se produce una renovación automática se generará una alerta automática al administrador de contratos para el cambio de la fecha del contrato (actualización de la fecha de este)</description>
        <formula>( ISPICKVAL( BI_Tipo_de_renovacion__c , &apos;Renegociación&apos;) || ISPICKVAL( BI_Tipo_de_renovacion__c , &apos;Renovación&apos;)) &amp;&amp;  ISPICKVAL(BI_Country__c , &apos;Peru&apos;) &amp;&amp; ISPICKVAL(Account.BI_Segment__c, &apos;Empresas&apos;) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_RequiereContrato_True</fullName>
        <actions>
            <name>BI_Legalizada_PendienteContrato</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_RequiereContrato_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>D-000623cuando en la creación de una oportunidad local para Perú los campos “Requiere contrato” y “Legalizada” tengan valores por defecto; los valores por defecto por campo son:
•	“Requiere contrato” = marcado
•	“Legalizada” = “Pendiente contrato”</description>
        <formula>AND( ISPICKVAL(BI_Country__c,&apos;Peru&apos;), RecordType.DeveloperName = &apos;BI_Ciclo_completo&apos;, ISPICKVAL(Account.BI_Segment__c,&apos;Empresas&apos;) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_crear_una_tarea_al_Administrador_del_contrato</fullName>
        <actions>
            <name>BI_PER_Avisar_al_administrador_de_que_existe_una_oportunidad_pendiente_de_contra</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BI_PER_Oferta_requiere_de_contrato</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Cuando se marca la casilla Requiere contrato y esta ganada, se deberá crear una tarea al Administrador del contrato y un aviso para la creación de un contrato si fuera necesario.</description>
        <formula>BI_Requiere_contrato__c =true &amp;&amp;  ISPICKVAL(StageName , &apos;F1 - Closed Won&apos;) &amp;&amp;  ISPICKVAL(BI_Country__c , &apos;Peru&apos;) &amp;&amp; ISPICKVAL(Account.BI_Segment__c, &apos;Negocios&apos;) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_PER_crear_una_tarea_al_Administrador_del_contrato_empresas</fullName>
        <actions>
            <name>BI_PER_empresas_Avisar_al_administrador_de_que_existe_una_oportunidad_contrato</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BI_PER_Oferta_requiere_de_contrato_empresas</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>Cuando se marca la casilla Requiere contrato y esta ganada, se deberá crear una tarea al Administrador del contrato y un aviso para la creación de un contrato si fuera necesario.</description>
        <formula>BI_Requiere_contrato__c =true &amp;&amp;  ISPICKVAL(StageName , &apos;F1 - Closed Won&apos;) &amp;&amp;  ISPICKVAL(BI_Country__c , &apos;Peru&apos;) &amp;&amp;  ISPICKVAL(Account.BI_Segment__c, &apos;Empresas&apos;) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Rapido_a_Completo</fullName>
        <actions>
            <name>BI_Actualiza_Tipo_de_registro_Completo</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_actualiza_a_etata_F5</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( RecordType.Name = &quot;Ciclo rápido&quot;, ISPICKVAL(BI_Ciclo_ventas__c, &quot;Completo&quot;), !$Setup.BI_bypass__c.BI_migration__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Renovacion oportunidad</fullName>
        <actions>
            <name>BI_Aviso_renovacion_oportunidad</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Si se produce una renovación automática se generará una alerta automática al administrador de contratos para el cambio de la fecha del contrato (actualización de la fecha de este)</description>
        <formula>( ISPICKVAL( BI_Tipo_de_renovacion__c , &apos;Renegociación&apos;) || ISPICKVAL( BI_Tipo_de_renovacion__c , &apos;Renovación&apos;)) &amp;&amp;  !ISPICKVAL(BI_Country__c , &apos;Argentina&apos;)  &amp;&amp;  !ISPICKVAL(BI_Country__c , &apos;Peru&apos;) &amp;&amp;(!$Setup.BI_bypass__c.BI_migration__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_Tarea_Licitaciones_F2</fullName>
        <actions>
            <name>BI_Revision_de_Ofertas_de_la_Competencia</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>AND( ISPICKVAL(BI_Ciclo_ventas__c, &quot;Licitación&quot;), ISPICKVAL(StageName, &quot;F2 - Negotiation&quot;), NOT(ISPICKVAL($User.Pais__c, &quot;Argentina&quot;)), !$Setup.BI_bypass__c.BI_migration__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_USA_ExpirationDate</fullName>
        <actions>
            <name>BI_USA_ExpirationDate_VE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Demanda 489 - Se completa la fecha expiration date, al pasar a F3, con 10 días.</description>
        <formula>AND(  VE_Express__c = true,  ISPICKVAL(BI_Country__c, &apos;United States&apos;),  ISPICKVAL(StageName, &apos;F3 - Offer Presented&apos;),  ISBLANK(BI_Fecha_de_vigencia__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_crear_una_tarea_al_Administrador_del_contrato</fullName>
        <actions>
            <name>BI_Oferta_requiere_de_contrato</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Cuando se marca la casilla Requiere contrato y esta ganada, se deberá crear una tarea al Administrador del contrato y un aviso para la creación de un contrato si fuera necesario.</description>
        <formula>BI_Requiere_contrato__c =true &amp;&amp; ISPICKVAL(StageName , &apos;F1 - Closed Won&apos;) &amp;&amp; !ISPICKVAL(BI_Country__c, &apos;Argentina&apos;) &amp;&amp; !ISPICKVAL(BI_Country__c , &apos;Peru&apos;) &amp;&amp; !$Setup.BI_bypass__c.BI_migration__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FS CORE Modificar Cliente Potencial a Cliente Actual en F1</fullName>
        <actions>
            <name>FS_CORE_Conversion_a_Cliente_Actual</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1  AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Prospect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>F1 - Closed Won</value>
        </criteriaItems>
        <description>Los clientes potenciales seran convertidos a clientes actuales al paso de la primera oportunidad a F1</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FS CORE Revisión de la asignación del cliente</fullName>
        <actions>
            <name>FS_CORE_Revision_de_la_asignacion_del_cliente</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>FS_CORE_Conversion_a_Cliente_Actual</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1  AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Account.BI_Country__c</field>
            <operation>equals</operation>
            <value>Chile</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Prospect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>F1 - Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.FS_CHI_propietario_provisional__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TIWS_Cerrar_Oportunidad_F1</fullName>
        <active>false</active>
        <description>Migración TIWS: Cuando la oportunidad llega al close date se debe cerrar como  F1 – Cancelado y con motivo de cierre un valor nuevo que se llame “Not updated”.</description>
        <formula>AND (      Account.TIWS_Gestion_TIWS__c = true,      NOT(ISBLANK(CloseDate)),      NOT(ISPICKVAL(StageName, &apos;F1 - Cancelled | Suspended&apos;) ),      NOT(ISPICKVAL(StageName, &apos;F1 - Closed Lost | Suspended&apos;) ),      NOT(ISPICKVAL(StageName, &apos;F1 - Closed Won&apos;) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TIWS_83_dias_despues_Last_modified_date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.LastModifiedDate</offsetFromField>
            <timeLength>83</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TIWS_Close_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>TIWS_Not_Updated</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>TIWS_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.LastModifiedDate</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TIWS_7_dias_antes_Closed_date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TIWS_Close_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>TIWS_Not_Updated</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>TIWS_Stage</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TIWS_1_dias_antes_Closed_date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>TIWS_89_dias_despues_Last_modified_date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.LastModifiedDate</offsetFromField>
            <timeLength>89</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>BI_ARG_Oferta_requiere_de_contrato</fullName>
        <assignedTo>administracion.b2b@telefonicab2b.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Oferta requiere de contrato</subject>
    </tasks>
    <tasks>
        <fullName>BI_Completar_Parque_comercial_competencia_arg</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Completar Parque comercial competencia</subject>
    </tasks>
    <tasks>
        <fullName>BI_Oferta_requiere_de_contrato</fullName>
        <assignedTo>Business Development</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Oferta requiere de contrato</subject>
    </tasks>
    <tasks>
        <fullName>BI_PER_Oferta_requiere_de_contrato</fullName>
        <assignedTo>narda.cornejo@telefonica.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Oferta requiere de contrato</subject>
    </tasks>
    <tasks>
        <fullName>BI_PER_Oferta_requiere_de_contrato_empresas</fullName>
        <assignedTo>soportebienperu@telefonica.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Oferta empresas requiere de contrato</subject>
    </tasks>
    <tasks>
        <fullName>BI_Revision_de_Ofertas_de_la_Competencia</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Revisión de Ofertas de la Competencia</subject>
    </tasks>
</Workflow>
