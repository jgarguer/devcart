<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI2_Crear_Motivo_Tipologia</fullName>
        <field>BI2_PER_Motivo_Tipologia__c</field>
        <formula>TEXT (BI_Country__c) + &quot;-&quot; + TEXT(BI2_PER_Motivo_del_caso__c) + &quot;-&quot; +  TEXT(BI2_PER_Tipologia__c)</formula>
        <name>BI2_Crear Motivo-Tipología</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI2_PER_Crear_Criterio_Asignacion</fullName>
        <field>BI_Referencia_del_criterio__c</field>
        <formula>TEXT (BI_Country__c) + &quot;-&quot; + TEXT(BI2_PER_Motivo_del_caso__c) + &quot;-&quot; +  TEXT(BI2_PER_Tipologia__c) + &quot;-&quot; + TEXT(BI2_PER_SubTipologia__c) + &quot;-&quot; + TEXT(BI2_PER_Linea_de_Negocio__c) + &quot;-&quot; +  TEXT(BI2_PER_Producto_Servicio__c) + &quot;-&quot; +  TEXT(BI2_PER_Descripcion_de_producto__c) + &quot;-&quot; +  TEXT(BI2_PER_Territorio__c) + &quot;-&quot; +  TEXT(BI2_PER_Limite_inferior__c) + &quot;-&quot; +  TEXT(BI2_PER_Limite_superior__c)</formula>
        <name>BI2_PER_Crear Criterio Asignación</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Validar_criterio_unico</fullName>
        <field>BI_Referencia_del_criterio__c</field>
        <formula>TEXT(BI_Country__c) +&quot;-&quot;+ TEXT(BI_Segment__c) +&quot;-&quot;+ TEXT(BI_Line_of_Business__c) +&quot;-&quot;+ TEXT(BI_Typology__c)</formula>
        <name>BI_Validar_criterio_unico</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI2_PER_Crear Criterio Asignacion</fullName>
        <actions>
            <name>BI2_Crear_Motivo_Tipologia</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI2_PER_Crear_Criterio_Asignacion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(TEXT(BI_Country__c)&lt;&gt;&quot;Chile&quot;,TEXT(BI_Country__c)&lt;&gt;&quot;Colombia&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Validar_criterio_unico</fullName>
        <actions>
            <name>BI_Validar_criterio_unico</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_Criterio_de_asignacion__c.BI_Country__c</field>
            <operation>equals</operation>
            <value>Chile</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
