<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_Actualiza_PK</fullName>
        <field>BI_PK__c</field>
        <formula>TEXT(BI_Country__c) &amp;&quot;_&quot;&amp;
 TEXT(BI_Ciclo_ventas__c )&amp;&quot;_&quot;&amp;
TEXT( BI_Etapa__c) &amp;&quot;_&quot;&amp;
 BI_Asunto__c</formula>
        <name>BI_Actualiza_PK</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_PK2</fullName>
        <field>BI_PK2__c</field>
        <formula>TEXT(BI_Country__c) &amp;&quot;_&quot;&amp; 
TEXT(BI_Ciclo_ventas__c )&amp;&quot;_&quot;&amp; 
TEXT( BI_Etapa__c)&amp;&quot;_&quot;&amp; 
IF(BI_Ultrarapida__c, &apos;V&apos;,&apos;F&apos;)</formula>
        <name>BI_Actualiza PK2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_Actualiza_PK</fullName>
        <actions>
            <name>BI_Actualiza_PK</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BI_Actualiza_PK2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Evaluar la regla cuando un registro es creado, y cada vez que se modifica</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
