<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_FVI_NotificacionEventoAlerta</fullName>
        <description>Notificar a Contacto del Evento</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_FVI/BI_FVI_NotificacionEventoPlantilla</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_FVI_Actualizar_FechaHoraCreacion</fullName>
        <field>BI_FVI_FechaHoraCreacion__c</field>
        <formula>CreatedDate</formula>
        <name>Actualizar Fecha Creacion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_FVI_Rellena_FechaHoraCreacion_SF</fullName>
        <actions>
            <name>BI_FVI_Actualizar_FechaHoraCreacion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.BI_FVI_FechaHoraCreacion__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Rellena la fecha de creacion (Campo creado para la app) cuando el campo viene vacio con el create date.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
