<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_Actualiza_check_recalculo_permisos</fullName>
        <field>BI_recalcular_permisos__c</field>
        <literalValue>0</literalValue>
        <name>BI Actualiza check recalculo permisos</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_Desactiva recalculo de permisos</fullName>
        <actions>
            <name>BI_Actualiza_check_recalculo_permisos</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.BI_recalcular_permisos__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <description>Desactivar el check de recalculo de permisos una vez el trigger ha asignado los necesarios</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
