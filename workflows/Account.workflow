<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_Actualiza_Carterizado_texto</fullName>
        <field>BI_Carterizado_texto__c</field>
        <formula>BI_Carterizado__c</formula>
        <name>BI Actualiza Carterizado texto</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_Holding_UpdField</fullName>
        <description>Actualiza el campo BI_Holding__c = true</description>
        <field>BI_Holding__c</field>
        <literalValue>Sí</literalValue>
        <name>BI_Actualiza_Holding_UpdField</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualiza_Holding_UpdField_No</fullName>
        <description>Actualiza el campo Cliente virtual a &quot;No&quot;</description>
        <field>BI_Holding__c</field>
        <literalValue>No</literalValue>
        <name>BI_Actualiza_Holding_UpdField_No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Actualizar_validadorfiscal_true</fullName>
        <description>Actualiza el campo validador fiscal.</description>
        <field>BI_Validador_Fiscal__c</field>
        <formula>IF(  
ISNULL(BI_No_Identificador_fiscal__c)  ||  ISBLANK(BI_No_Identificador_fiscal__c), BI_Codigo_ISO__c &amp; (&quot;_&quot;) &amp; BI_Ref_Cliente_Agrupacion__c, BI_Codigo_ISO__c &amp; (&quot;_&quot;) &amp;  (TEXT(BI_Tipo_de_identificador_fiscal__c))
&amp; (&quot;_&quot;) &amp; BI_No_Identificador_fiscal__c
)</formula>
        <name>BI_Actualizar-validadorfiscal-true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_CHI_Actualizar_propietario_del_client</fullName>
        <field>OwnerId</field>
        <lookupValue>ximena.diaz@movistar.cl</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>BI_CHI_Actualizar_propietario_del_client</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_FVI_Actualizar_campo_Fecha_Ingreso_NT</fullName>
        <description>Actualizar_campo_Fecha_Ingreso_NT_blanco</description>
        <field>BI_FVI_FechaIngresoNAT__c</field>
        <name>Actualizar campo Fecha Ingreso NT blanco</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_FVI_Nodo_c_No_contenga_NAT</fullName>
        <description>Si el campo &quot;BI_FVI_Nodo__c&quot; no tiene valor &quot;NAT&quot;, el campo &quot;BI_FVI_FechaIngresoNT__c&quot; ha de actualizarse a &quot;Blanco&quot;</description>
        <field>BI_FVI_FechaIngresoNAT__c</field>
        <name>Nodo__c_No_contenga_NAT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_FVI_Nodo_c_contenga_NAT</fullName>
        <field>BI_FVI_FechaIngresoNAT__c</field>
        <formula>Today()</formula>
        <name>Nodo__c_contenga_NAT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_FVI_Rellena_Tipo_Nodo_Actualizar</fullName>
        <field>BI_FVI_TIPO_NODO__c</field>
        <formula>BI_FVI_Nodo__r.RecordType.DeveloperName</formula>
        <name>BI_FVI_Rellena_Tipo_Nodo_Actualizar</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_Propietario_del_cliente_Desarrollo</fullName>
        <field>OwnerId</field>
        <lookupValue>administracion.b2b@telefonicab2b.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>BI Propietario del cliente Desarrollo c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_actualiza_Validador_Id_legado</fullName>
        <field>BI_Validador_Id_de_legado__c</field>
        <formula>BI_Id_del_cliente__c &amp; &apos; &apos; &amp;  BI_Codigo_ISO__c  &amp; &apos; &apos; &amp;  BI_Sistema_legado__c</formula>
        <name>BI  actualiza Validador Id legado</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_rellena_subsegmento_texto</fullName>
        <field>BI_Subsegmento_local_textoQQQQ__c</field>
        <formula>TEXT(BI_Subsegment_Local__c)</formula>
        <name>BI_rellena_subsegmento_texto</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Actualiza_fecha_activacion</fullName>
        <description>Actualiza el campo &apos;Fecha de activación&apos; con la fecha actual.

Creado por la Demanda 288.</description>
        <field>TGS_Fecha_Activacion__c</field>
        <formula>TODAY()</formula>
        <name>Actualiza fecha activación</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Actualiza_fecha_desactivacion</fullName>
        <description>Actualiza el campo &apos;Fecha de desactivación&apos; con la fecha actual. 

Creado por la Demanda 288.</description>
        <field>TGS_Deactivation_Date__c</field>
        <formula>TODAY()</formula>
        <name>Actualiza fecha desactivación</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Main_Accoun_Name</fullName>
        <field>TGS_Main_Account_Name__c</field>
        <formula>TGS_Aux_Holding__r.Name</formula>
        <name>Main Accoun Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Previous_Account_Manager</fullName>
        <field>TGS_Previous_Account_Manager__c</field>
        <formula>PRIORVALUE( TGS_Owner_name__c )</formula>
        <name>TGS Previous Account Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_R030_Activation_date</fullName>
        <description>Activation Date from LE, BU, CC must be first day of the month</description>
        <field>TGS_Fecha_Activacion__c</field>
        <formula>DATE(YEAR(TODAY()), MONTH(TODAY()),1)</formula>
        <name>TGS_R030_Activation_date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI  actualiza Validador Id legado</fullName>
        <actions>
            <name>BI_actualiza_Validador_Id_legado</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BI_Id_del_cliente__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI Actualiza Carterizado texto</fullName>
        <actions>
            <name>BI_Actualiza_Carterizado_texto</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Nombre_Cuenta__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI Actualiza Pais Texto Cuentas</fullName>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI Propietario del cliente Desarrollo comercial</fullName>
        <actions>
            <name>BI_Propietario_del_cliente_Desarrollo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sí el Cliente ha sido creado por un usuario distinto a DC se asigna el registro a &quot;Desarrollo Comercial&quot;.</description>
        <formula>AND(      NOT(ISPICKVAL(Owner.BI_Permisos__c, &quot;Desarrollo Comercial&quot;)),   (TEXT($User.Pais__c) &lt;&gt; &apos;Colombia&apos;),   !$Setup.BI_bypass__c.BI_migration__c,    TEXT(BI_Country__c) &lt;&gt; &quot;Argentina&quot;,    TEXT(BI_Country__c) &lt;&gt; &quot;Peru&quot;,    TEXT(BI_Country__c) &lt;&gt; &quot;Guatemala&quot;,    TEXT(BI_Country__c) &lt;&gt; &quot;Panama&quot;,    TEXT(BI_Country__c) &lt;&gt; &quot;Nicaragua&quot;,    TEXT(BI_Country__c) &lt;&gt; &quot;España&quot;,    TEXT(BI_Country__c) &lt;&gt; &quot;Spain&quot;,    TEXT(BI_Country__c) &lt;&gt; &quot;Brazil&quot;,    TEXT(BI_Country__c) &lt;&gt; &quot;Brasil&quot;,    TEXT(BI_Country__c) &lt;&gt; &quot;El Salvador&quot;,    TEXT(BI_Country__c) &lt;&gt; &quot;Costa Rica&quot;,    TEXT(BI_Country__c) &lt;&gt; &quot;Ecuador&quot; &amp;&amp;  TEXT(BI_Country__c) &lt;&gt; &quot;Mexico&quot;,    NOT($Permission.BI_Skip_account_owner_to_desarrollo_comercial)  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualiza_Holding</fullName>
        <actions>
            <name>BI_Actualiza_Holding_UpdField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4</booleanFilter>
        <criteriaItems>
            <field>Account.BI_RecordType_DevName__c</field>
            <operation>equals</operation>
            <value>TGS_Cost_Center</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BI_RecordType_DevName__c</field>
            <operation>equals</operation>
            <value>TGS_Business_Unit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BI_RecordType_DevName__c</field>
            <operation>equals</operation>
            <value>TGS_Customer_Country</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BI_RecordType_DevName__c</field>
            <operation>equals</operation>
            <value>TGS_Holding</value>
        </criteriaItems>
        <description>Cuando es creada y cuando se modifica el tipo de registro. Actualiza el campo BI_Holding__c</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualiza_Holding_No</fullName>
        <actions>
            <name>BI_Actualiza_Holding_UpdField_No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BI_RecordType_DevName__c</field>
            <operation>equals</operation>
            <value>TGS_Legal_Entity</value>
        </criteriaItems>
        <description>Actualiza el campo Cliente virtual a &quot;No&quot; para determinados tipos de registro.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_Actualizar-validadorfiscal-true</fullName>
        <actions>
            <name>BI_Actualizar_validadorfiscal_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Actualiza el contenido del campor validador fiscal.</description>
        <formula>NOT( $Setup.TGS_User_Org__c.TGS_Is_TGS__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_CHI_Actualizar_propietario_del_cliente_al_convertir</fullName>
        <actions>
            <name>BI_CHI_Actualizar_propietario_del_client</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( BI_CHI_Convertido__c = true, BI_Codigo_ISO__c = &quot;CHI&quot; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BI_FVI_Actualizar_FechaIngresoNT_a_blanco</fullName>
        <actions>
            <name>BI_FVI_Actualizar_campo_Fecha_Ingreso_NT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Actualizar el campo BI_FVI_FechaHoraUltimaVisita__c a Blanco cuando el campo BI_FVI_Nodo__c se actualice a Blanco o sea distinto de NAT.</description>
        <formula>OR(      (BI_FVI_Nodo__c=&quot;&quot;),      NOT(BI_FVI_Nodo__r.RecordType.DeveloperName = &quot;BI_FVI_NAT&quot;)             )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_FVI_Rellena_Tipo_Nodo</fullName>
        <actions>
            <name>BI_FVI_Rellena_Tipo_Nodo_Actualizar</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_Rellena_subsegmento_local</fullName>
        <actions>
            <name>BI_rellena_subsegmento_texto</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS Previous Account Manager</fullName>
        <actions>
            <name>TGS_Previous_Account_Manager</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS_Actualiza_fecha_activacion</fullName>
        <actions>
            <name>TGS_Actualiza_fecha_activacion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Actualiza el campo Fecha de activación cuando un cliente se activa.

Creado por la Demanda 288</description>
        <formula>AND(  TEXT(BI_Activo__c) = &apos;Sí&apos;,  OR(   ISCHANGED(BI_Activo__c),   ISNEW()  ),  OR(   RecordType.DeveloperName = &apos;TGS_Proveedor_de_Telco_Grupo_Economico&apos;,   RecordType.DeveloperName = &apos;TGS_Proveedor_de_Telco_Pais&apos;,   RecordType.DeveloperName = &apos;TGS_Proveedor_de_Telco&apos;  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS_Actualiza_fecha_desactivacion</fullName>
        <actions>
            <name>TGS_Actualiza_fecha_desactivacion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Actualiza el campo Fecha de desactivación cuando un cliente se desactiva.

Creado por la Demanda 288</description>
        <formula>AND(  ISCHANGED(BI_Activo__c),  TEXT(BI_Activo__c) = &apos;No&apos;,  OR(   RecordType.DeveloperName = &apos;TGS_Proveedor_de_Telco_Grupo_Economico&apos;,   RecordType.DeveloperName = &apos;TGS_Proveedor_de_Telco_Pais&apos;,   RecordType.DeveloperName = &apos;TGS_Proveedor_de_Telco&apos;  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS_Main_Account_Name</fullName>
        <actions>
            <name>TGS_Main_Accoun_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TGS_R030_Activation_date</fullName>
        <actions>
            <name>TGS_R030_Activation_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISBLANK(TGS_Fecha_Activacion__c), ISNEW(),  OR(   RecordType.DeveloperName = &apos;TGS_Legal_Entity&apos;,   RecordType.DeveloperName = &apos;TGS_Business_Unit&apos;,   RecordType.DeveloperName = &apos;TGS_Cost_Center&apos;  ) )&amp;&amp; !$Setup.BI_bypass__c.BI_migration__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
