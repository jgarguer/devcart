<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BI_CHI_Actualizacion_cantidad_disponible</fullName>
        <field>BI_Cantidad_disponible__c</field>
        <formula>BI_Cantidad__c</formula>
        <name>BI_CHI_Actualizacion cantidad disponible</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_CHI_Actualizar cantidad disponible</fullName>
        <actions>
            <name>BI_CHI_Actualizacion_cantidad_disponible</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),ISCHANGED(BI_Cantidad__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
