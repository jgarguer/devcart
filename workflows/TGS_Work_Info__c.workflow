<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>TGS_Send_a_email_when_a_work_info_its_created</fullName>
        <description>Send a email when a work info its created</description>
        <protected>false</protected>
        <recipients>
            <field>TGS_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TGS_Notificacion_Cliente_Case_Update</template>
    </alerts>
    <alerts>
        <fullName>TGS_Work_Infos_Notification</fullName>
        <description>TGS Work Infos Notification</description>
        <protected>false</protected>
        <recipients>
            <field>TGS_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_3_1_Case_Workinfo_Update</template>
    </alerts>
    <alerts>
        <fullName>TGS_Work_Infos_Notification_For_Assigned_Group</fullName>
        <description>TGS Work Infos Notification for Assigned Group</description>
        <protected>false</protected>
        <recipients>
            <field>TGS_Case_owner_email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plantillas_de_correo_BIEN/TGS_3_1_b_Case_Workinfo_Update</template>
    </alerts>
    <fieldUpdates>
        <fullName>TGS_Case_owner_email</fullName>
        <field>TGS_Case_owner_email__c</field>
        <formula>TGS_Case__r.Owner:Queue.QueueEmail</formula>
        <name>TGS Case owner email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TGS_Get_Contact_s_Email</fullName>
        <field>TGS_Contact_Email__c</field>
        <formula>TGS_Case__r.Contact.Email</formula>
        <name>TGS Get Contact&apos;s Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>TGS Get Emails</fullName>
        <actions>
            <name>TGS_Case_owner_email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TGS_Get_Contact_s_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TGS Send email update</fullName>
        <actions>
            <name>TGS_Send_a_email_when_a_work_info_its_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>TGS_Work_Info__c.TGS_Public__c</field>
            <operation>equals</operation>
            <value>Verdadero</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Incident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Query</value>
        </criteriaItems>
        <description>Workflow que acciona el envio de correos de tipo Update cuando se crea el work info asociado a un caso.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
