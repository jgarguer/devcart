<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BI_O4_Disconnection_Cancelled_Notification</fullName>
        <description>BI_O4_Disconnection Cancelled Notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>BI_O4_Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>BI_O4_Provider_Reviewer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Disconnections/BI_O4_Disconnection_Cancelled</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Disconnection_Change_Notification</fullName>
        <description>Disconnection Change Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Disconnections/BI_O4_Disconnection_Change</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Disconnection_Closed_Notification</fullName>
        <description>BI_O4_Disconnection Closed Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>BI_O4_Project_Manager_Leader__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>BI_O4_Project_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>BI_O4_Provider_Reviewer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Disconnections/BI_O4_Disconnection_Closed</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Disconnection_Creation_Notification</fullName>
        <description>BI_O4_Disconnection Creation Notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Disconnections/BI_O4_Disconnection_Creation</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Disconnection_approved</fullName>
        <description>Disconnection approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Disconnections/BI_O4_Disconnection_Approval_Notification</template>
    </alerts>
    <alerts>
        <fullName>BI_O4_Disconnection_rejected</fullName>
        <description>Disconnection rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BI_O4_Disconnections/BI_O4_Disconnection_Approval_Process_Rejected_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>BI_O4_Clear_Review_Rejected</fullName>
        <description>Disconnection</description>
        <field>BI_O4_Review_Rejected__c</field>
        <literalValue>0</literalValue>
        <name>Clear Review Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Set_Review_Rejected</fullName>
        <description>Disconnection</description>
        <field>BI_O4_Review_Rejected__c</field>
        <literalValue>1</literalValue>
        <name>Set Review Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Turn_off_Flag</fullName>
        <description>Disconnection</description>
        <field>BI_O4_In_Approval_Process__c</field>
        <literalValue>0</literalValue>
        <name>Turn off Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BI_O4_Turn_on_Flag</fullName>
        <description>Disconnection</description>
        <field>BI_O4_In_Approval_Process__c</field>
        <literalValue>1</literalValue>
        <name>Turn on Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BI_O4_Disconnection Cancelled</fullName>
        <actions>
            <name>BI_O4_Disconnection_Cancelled_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_O4_Disconnection__c.BI_O4_Status__c</field>
            <operation>equals</operation>
            <value>Cancelled By Customer,Cancelled By Telefonica</value>
        </criteriaItems>
        <description>Disconnection</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Disconnection Change</fullName>
        <actions>
            <name>BI_O4_Disconnection_Change_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Disconnection</description>
        <formula>AND(  NOT(ISNEW()),  ISCHANGED(BI_O4_Status__c),  OR(  ISPICKVAL(BI_O4_Status__c, &quot;Provider Reviewer Assignment&quot;), ISPICKVAL(BI_O4_Status__c, &quot;Request Review&quot;), ISPICKVAL(BI_O4_Status__c, &quot;Penalty Review With Customer&quot;), ISPICKVAL(BI_O4_Status__c, &quot;Penalties Dispute Review&quot;), ISPICKVAL(BI_O4_Status__c, &quot;Negotiate With Customer&quot;), ISPICKVAL(BI_O4_Status__c, &quot;Negotiation Details Review&quot;), ISPICKVAL(BI_O4_Status__c, &quot;Financial Planning Review&quot;), ISPICKVAL(BI_O4_Status__c, &quot;Project Manager Assignment&quot;), ISPICKVAL(BI_O4_Status__c, &quot;Disconnection Execution&quot;)  )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Disconnection Closed</fullName>
        <actions>
            <name>BI_O4_Disconnection_Closed_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BI_O4_Disconnection__c.BI_O4_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Disconnection</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BI_O4_Disconnection Creation</fullName>
        <actions>
            <name>BI_O4_Disconnection_Creation_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Disconnection</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
