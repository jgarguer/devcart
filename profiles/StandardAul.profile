<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <custom>false</custom>
    <layoutAssignments>
        <layout>ADM_Direcciones_IP__c-Formato Dirección IP</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ANI__c-Formato ANI</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>AT_Adoption_Trend__c-Adoption Trend Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Account-BI_Clientes</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>AccountBrand-Account BrandLayout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>AccountTeamMember-Formato Miembro del equipo de clientes</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Asset-Asset Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI2_Asignacion_Plantilla__c-BI2_Asignación_Plantilla</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI2_Paginas_casos_tecnicos__mdt-BI2 Páginas casos técnicos Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI2_Stop_Status__c-Formato Stop Status</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Account_Dummy_User__c-Account External User</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Aprobacion_Miembros_Campana__c-Formato Aprobacion Miembros de Campa_a</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Asignacion_de_casos__c-Asignacion del caso Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Asignacion_demanda__c-Formato Cambio de estado Demanda</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Bolsa_de_dinero__c-Bolsa de dinero - Asociado a modelo</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Bolsas_especiales__c-Formato Bolsa especial</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_Anexos__c-BI_COL_FUN</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_Cobro_Parcial__c-Formato Cobro Parcial</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_Configuracion_campos_interfases__c-BI_COL_Formato_Configuracion_campos_interfaces</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_Descripcion_de_servicio__c-BI_COL_Formato_Descripcion_de_servicio</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_Historico_Colombia__c-Formato Histórico Colombia</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_Homologacion_integraciones__c-Formato Homologación integraciones</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_MIDAS__c-Formato MIDAS</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_Modificacion_de_Servicio__c-BI_COL_Formato Aplicaciones</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_Opcion_callejero__c-Formato Opción callejero</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_PreFactibilidad__c-Formato PreFactibilidad</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_Proyectos_Colombia__c-Formato Proyectos Colombia</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_Scoring__c-Formato Scoring</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_Seguimiento_Quiebre__c-Formato Seguimiento de Quiebre</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COL_Viabilidad_Tecnica__c-Formato Viabilidad Técnica</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_COT_MEX_PuntoDeInstalacion_AccOpp__c-Puntos de Instalacion Relacion Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Campanas_Cuenta__c-Campañas Cuenta Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Catalogo_PP__c-Catalogo %7C PP Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Catalogo_de_servicio__c-Formato Servicio plan</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Cliente_PP__c-Formato Cliente %7C PP</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Cobranza__c-BI_Formato Cobranza</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Codigo_de_cliente__c-Formato Código de cliente</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Col_Ciudades__c-Formato Ciudades</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Comentario__c-BI_Formato Comentario</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Comentarios_de_la_Oportunidad__c-BI_Formato Comentario Opp</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Configuracion_Caso_PP__c-Formato Configuración Caso PP</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Configuracion_de_documentacion__c-Configuracion de documentación Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Contact_Customer_Portal__c-Contact Customer Portal Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Criterio_de_asignacion__c-Criterio de asignacion Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Demanda__c-Formato Demanda</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Descuento__c-Formato Descuento</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Descuento_para_modelo__c-Formato Descuento para modelo</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_CPF__c-CPF Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Contacto_Sede__c-Formato Contacto - Sede</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Datos_del_servicio__c-Formato Servicio Adicional</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_E_Learning_Items__c-Formato E-Learning - Items</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Historico_Parque_y_Competencia__c-Formato Historico Parque y Competencia</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Item_ELearning_Usuario__c-Formato Item E-Learning - Usuario</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Log_de_Accesos_Recomendaciones__c-Formato Log de Accesos Recomendaciones</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_MailConfiguration__mdt-BI_FVI_MailConfiguration Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Nodo_Cliente__c-Nodo Cliente Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Nodo_Objetivo_Desempeno__c-BI_FVI_Formato_Nodo_Objetivo_Desempeno</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Nodos_Objetivos__c-BI_FVI_Formato_Nodo_Objetivo</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Nodos__c-Nodo Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_OfflineCatalog__c-BI_FVI_OfflineCatalog Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Recomendacion_Cliente__c-Formato Recomendación Cliente</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Recomendaciones__c-Formato Recomendación</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Rentabilidad_Neta__c-BI_FVI_Formato_Rentabilidad_Neta</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_FVI_Trafico__c-Formato Tráfico</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Facturacion__c-BI_Formato Facturacion</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Facturas__c-Formato Factura</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Gastos__c-BI_Gastos</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Historial_de_adjuntos_caso__c-Formato Historial de adjuntos caso</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Historial_de_adjuntos_oportunidad__c-Formato Historial de adjuntos oportunidad</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_ISC2__c-BI_Formato ISC CORE</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_ImageHome__c-Imagenes en home Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Indicador_BI__c-Formato Indicador BI</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Licencias_Pais__c-Licencias Pais Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Linea_de_Recambio__c-Formato Linea de Recambio</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Linea_de_Recarterizacion__c-Formato Recarterización</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Linea_de_Servicio__c-Formato Línea de servicio</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Linea_de_Venta__c-Formato Linea de Venta</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Log__c-Formato BI Log</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Log_de_Recarterizacion__c-Formato Log de Recarterización</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_MEX_Log_Linea__c-Formato Log Linea</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Modelo__c-Formato Modelo</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Modelos_disponibles__c-Formato Modelo disponible</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Multimarca_PP__c-Pie de pagina</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Approvals__c-BI_O4_GATE 1</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_BID_team_member__c-BI_O4_BID Team Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_BT_ACCESO__c-Acceso Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_BT_CPE__c-CPE Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_BT_IP_PORT__c-IP_PORT Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_BT_PUERTO_FISICO__c-PUERTO_FISICO Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_BT_SITE__c-Site Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_CSI_Questionarie__c-BI_O4_Formato CSI Questionarie</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Customer_Decision_Making_Model__c-BI_O4_CustomerDecisionMakingModel</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Disconnection__c-BI_O4_Disconnection</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Incentive_Details__c-BI_O4_Incentive Details Opportunity</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Offer_Request__c-BI_O4_Offer request- Capacity</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Oportunidad_Target__c-Oportunidad-Target Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Opportunity_Qualify__c-BI_O4_Opportunity Qualify Growth</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Proposal_Item__c-BI_O4_ProposalItemLayout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Revenue__c-BI_O4_Revenue</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Risk_Assessment__c-BI_O4_RiskAssessment</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Solution__c-Formato Solution</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Survey_CSI_CIP__c-BI_O4_Survey CSICSP Close</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Target__c-Target Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_Total_Targets__c-Total Targets Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_O4_WorkInfo2__c-BI_O4_WorkInfo Opp Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Objetivo_Comercial__c-BI_Objetivos_Comerciales</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Opportunity_snapshot__c-Formato Opportunity snapshot</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Periodo__c-BI_Periodos</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Permisos_PP__c-Permisos %7C PP Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Plan_de_accion__c-BI_Plan de Accion Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Portal_local__c-Formato Portal local</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Proceso_de_asignacion__c-Proceso de asignacion Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Productos_proyecto__c-Formato Productos</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Promociones__c-Formato BI_Promociones</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Punto_de_instalacion__c-Formato Punto de instalacion</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Rama__c-BI_Ramas</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Recarterizacion__c-Formato Recarterización</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Recurso__c-Recurso Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Registro_Datos_Desempeno__c-Registro Datos Desempeño Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Registro_Datos_FOCO_Error_Log__c-BI_Registro_Datos_FOCO_Error_Log</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Registro_Datos_FOCO__c-BI_Registros_Datos_FOCO</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Registros_Datos_FOCO_Stage__c-BI_Registros_Datos_FOCO_Stage</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_SF1_Custom_tab_listView_configuration__mdt-Custom tab listView configuration Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_SF1_Custom_tabs_configuration__mdt-Custom tab configuration Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_SF1_Layout_assignments_SF1__mdt-Layout assignments SF1 Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_SF1_Related_list_configuration__mdt-Related list configuration Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_SF1_quickActions_SF1__mdt-quickAction SF1 Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_SUB_Pedido_Subsidio__c-Formato Pedido Subsidio</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_SUB_ProductosOI_PedidoSubsidio__c-Formato ProductosOI x PedidoSubsidio</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Sede__c-Formato Direccion</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Service_Tracking_URL_PP__c-Formato Service Tracking URL %7C PP</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Solicitud_de_plan__c-Formato de pagina Plana</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Solicitud_de_recarterizacion__c-Solicitud de recarterización Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Solicitud_envio_productos_y_servicios__c-Formato Solicitud envio de productos y servicios</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Tarea_Obligatoria__c-Formato Tarea Obligatoria</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Tasa_de_cambio_presupuestaria__c-Formato Tasa de cambio presupuestaria</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Transaccion_de_bolsa_de_dinero__c-Formato Transaccion de bolsa de dinero</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Usuario_Dummy__c-Formato Usuario Dummy</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Vacaciones__c-Formato Vacaciones</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>BI_Videoteca__c-Videoteca Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CWP_AC_ServicesCost__c-ServicesCost Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CWP_Carousel__mdt-CWP Carousel Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CWP_CatalogItemConfiguration__c-CWP_OrderField</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Campaign-Campaign Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CampaignMember-BI Miembros de Campana</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CampaignMemberStatus-Formato Estado de los destinatarios de la campaña</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-BI2_COL_Casos_comerciales_II</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CaseClose-BI2_Cerrar_Casos</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CaseMilestone-Formato de eventos clave de casos</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ChannelProgram-Channel Program Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ChannelProgramLevel-Channel Program Level Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ChannelProgramMember-Channel Program Member Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CollaborationGroup-Group Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Configuracion__c-Formato de Configuracion SF-SAP</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contact-TGS Contact</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ContentVersion-Versión del contenido</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contract-Contract Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ContractLineItem-Formato de partida de contrato</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CurrencyTypeFacturacion__c-Formato de CurrencyTypeFacturacion</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DuplicateRecordSet-Formato Conjunto de registro duplicado</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Account_Plan_Links__c-Formato E24P Account Plan Links</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Account_Plan__c-Formato Account Plan</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Aplicabilidad__c-Formato E24P_Aplicabilidad</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Argumentario__c-Formato Argumentario</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Competidor__c-Formato Competidor</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Contacto_Account_Planning__c-Formato E24P_Contacto-Account Planning</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_DFE__c-Formato E24P_DFE</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Familia__c-Formato E24P_Familia</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Iniciativa_Estrategica__c-Formato Iniciativa Estratégica</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Necesidad_Sectorial__c-Formato Necesidades Sectoriales</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Porcentaje_Competidores__c-Formato Porcentaje Competidores</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Propuesta_Plan_de_accion__c-Formato Propuesta-Plan de acción</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Propuesta_comercial__c-Formato Propuesta comercial</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Sectores_Configuracion__c-Formato Sectores Configuracion</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Share_of_Wallet__c-Formato Share of Wallet</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E24P_Solucion_Sectorial__c-Formato Solución Sectorial</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E3_Big_Deal__c-Formato E3 Big Deal</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E3_MC_Configuracion__c-Formato Configuración</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>E3_Matriz_de_Complejidad__c-Formato Matriz de Complejidad</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>EmailMessage-Formato Mensaje de correo electrónico - Correo electrónico para registro de casos</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Entitlement-Formato de asignación</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>EntityMilestone-Formato de eventos clave de objeto</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Equipo__c-Equipo Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Event-BI_Eventos</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Event__c-Reunión Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>FAQ_TIP__kav-Formato FAQ %26 TIP</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>FO_Forecast_Futuro__c-Formato Forecast Futuro</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>FO_Forecast_Item__c-Formato Línea de Forecast</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>FO_Forecast__c-FO_Formato Forecast Manager</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>FS_CORE_TeamMemberToSincronize__c-Cartera del cliente Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Factura_OLI__c-Formato de Factura OLI</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Factura__c-Formato de Factura</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Facturas_SenS__c-Formato de Facturas_SenS</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>FeedItem-Formato Elemento de noticias en tiempo real</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Global-Soporte eHelp</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>HO_Agrupadores_Project__c-Formato Agrupador Proyecto</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>HO_Service_Unit__c-Formato Service unit</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>HO_e_SOF__c-Formato e-SOF</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Hierarchy__c-Hierarchy Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Idea-Idea Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Inventario_Competencia__c-Formato Inventario Competencia</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Lead-BI_Prospectos</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveAgentSession-Live Agent Session Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatTranscript-Live Chat Transcript Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatTranscriptEvent-Live Chat Transcript Event Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>LiveChatVisitor-Live Chat Visitor Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Log__c-Formato Log de errores</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Macro-Formato Macro</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Market_Place_PP__c-Market Place %7C PP Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Milestone1_Expense__c-Project Expense Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Milestone1_Log__c-Project Log Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Milestone1_Milestone__c-Project Milestone Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Milestone1_Project__c-Project Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Milestone1_Task__c-Project Task Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Milestone1_Time__c-Project Time Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Monitoreo_Infinity__c-Monitoreo Infinity Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>NE_TGS_GME_Bundle_Accessory__c-Formato GME Repository</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>NE_TGS_GME_Profile_Accessory__c-Formato GME Accessory</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>NE_TGS_GME_Profile__c-Formato GME_Profile</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>NE_TGS_GME_Service_Package__c-Formato GME Service Package</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>NewsLetter__kav-Formato NewsLetter</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>NotaCredito_OLI__c-Formato de Nota de credito parcial</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>NotadeCredito__c-Formato de Nota de Credito</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Opportunity-Opportunity Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityLineItem-Opportunity Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityTeamMember-Opportunity Team Member Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PartnerFundAllocation-Partner Fund Allocation Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PartnerFundClaim-Partner Fund Claim Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PartnerFundRequest-Partner Fund Request Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PartnerMarketingBudget-Partner Marketing Budget Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Pedido_PE__c-Formato de Pedido PE</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Pedido__c-Formato de Pedido</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Pricebook2-Price Book Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PricebookEntry-Price Book Entry</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Proceso_y_Procedimiento__kav-Formato Proceso y Procedimiento</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProfileSkill-Habilidad Formato</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProfileSkillEndorsement-Recomendación Formato</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProfileSkillUser-Usuario con habilidades Formato</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProgramacionCurrencyType__c-Formato de ProgramacionCurrencyType</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Programacion__c-Formato de Facturacion SB</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Project_Snapshot__c-Project Snapshot Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Question-Formato Pregunta</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>QuickText-Quick Text Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Quote-Formato de presupuesto</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>QuoteLineItem-Formato de partida de presupuesto</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>RegionQQQQ__c-BI_Pais</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Registro_de_actualizacion__c-Formato de Registro de actualizacion</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Registro_de_agrupacion__c-Formato de Registro de agrupacion</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Reply-Formato Respuesta</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Reportes_PP__c-Reporte PP Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Scorecard-Scorecard Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ScorecardAssociation-Scorecard Association Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ScorecardMetric-Scorecard Metric Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ServiceContract-Formato del contrato de servicio</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Servicio_Parque__c-Plan Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Solution-Solution Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Stop_Status__c-Formato Stop Status</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SurveyQuestionResponse__c-Survey Question Answer Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SurveyTaker__c-SurveyTaker Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Survey_Question__c-Survey Question Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Survey__c-Survey Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Account_Additional_Information__c-Formato Account Additional Information</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Account_Addresses__c-Account Address Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Asignaciones__c-Formato Asignación</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Audit_Item__c-Audit Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_BSM_Profile__c-Formato BSM Profile</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Business_Hours__c-TGS Business Hours Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Case_Template__c-TGS Case Template Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Categorization_Tier_Dependence__c-Categorization Tier Dependence Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Error_Integrations__c-Formato Error Integrations</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_GM_Repository__c-Formato GM Repository</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Portal_User_Configuration_Product__c-Formato Portal User Configuration Product</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Portal_User_Configuration__c-Formato Portal User Configuration</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Proposed_Solutions__c-Proposed Solutions Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_SLA_Assigner__c-TGS SLA Assigner Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_SMDM_Billing_Detail__c-Formato TGS SMDM Billing Detail</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_SMDM_Data_Collect__c-Formato TGS SMDM Data Collect</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_SMDM_Discounts__c-SMDM Discounts Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Site_ISOCode__mdt-TGS Site ISOCode Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Status_machine__c-Formato Status Machine</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_Work_Info__c-Work Info Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TGS_mWan_Resiliency__mdt-mWan Resiliency Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TIWS_Country_Master__c-Formato Country Master</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Task-BI_Tareas</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Territory2-Formato Territorio</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Territory2Model-Formato Modelo territorial</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Territory2Type-Formato Tipo de territorio</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>TipoCambio__c-Formato de Tipo de cambio</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>User-BI_Usuario</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserAlt-Formato Perfil de usuario</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserAppMenuItem-Formato Aplicación</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>VE_mLog__c-Formato mLog</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkBadge-Formato Insignia recibida</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkBadgeDefinition-Formato Insignia</layout>
    </layoutAssignments>
    <userLicense>Salesforce Platform</userLicense>
    <userPermissions>
        <enabled>true</enabled>
        <name>AddDirectMessageMembers</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AllowUniversalSearch</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AllowViewKnowledge</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ApexRestServices</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ApiEnabled</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AssignTopics</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterEditOwnPost</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterFileLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterInternalUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterInviteExternalUsers</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterOwnGroups</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ContentWorkspaces</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CreateCustomizeFilters</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CreateCustomizeReports</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CreateTopics</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CustomMobileAppsAccess</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>DistributeFromPersWksp</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditEvent</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditTask</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditTopics</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EmailMass</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EmailSingle</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EnableCommunityAppLauncher</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EnableNotifications</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ExportReport</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ImportPersonal</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>LightningExperienceUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ListEmailSend</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>MassInlineEdit</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>RemoveDirectMessageMembers</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>RunReports</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SelectFilesFromSalesforce</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ShowCompanyNameAsUserBadge</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SubscribeToLightningDashboards</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>UseWebLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewHelpLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewRoles</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewSetup</name>
    </userPermissions>
</Profile>
