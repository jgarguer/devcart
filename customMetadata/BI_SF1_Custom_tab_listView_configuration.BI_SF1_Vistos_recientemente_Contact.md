<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>VistosRecientemente</label>
    <protected>false</protected>
    <values>
        <field>BI_SF1_Campos_a_mostrar__c</field>
        <value xsi:type="xsd:string">Account.Name;Email</value>
    </values>
    <values>
        <field>BI_SF1_Custom_tab_configuration__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>BI_SF1_Vista_por_defecto__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>BI_SF1_filtro_query__c</field>
        <value xsi:type="xsd:string">WHERE LastViewedDate &gt; :yesterday LIMIT 10</value>
    </values>
</CustomMetadata>
