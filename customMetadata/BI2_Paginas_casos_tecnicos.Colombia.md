<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Colombia</label>
    <protected>false</protected>
    <values>
        <field>BI2_Pagina_consulta__c</field>
        <value xsi:type="xsd:string">BI2_COL_DetalleIncidencia_pag</value>
    </values>
    <values>
        <field>BI2_Pagina_consulta_portal_platino__c</field>
        <value xsi:type="xsd:string">BI2_COL_PCA_DetalleIncidencia_pag</value>
    </values>
    <values>
        <field>BI2_Pagina_consulta_solicitud__c</field>
        <value xsi:type="xsd:string">BI2_COL_DetalleSolicitud_pag</value>
    </values>
    <values>
        <field>BI2_Pagina_creacion__c</field>
        <value xsi:type="xsd:string">BI2_COL_CreacionIncidencia_pag</value>
    </values>
    <values>
        <field>BI2_Pagina_creacion_portal_platino__c</field>
        <value xsi:type="xsd:string">BI2_COL_PCA_CreacionIncidencia_pag</value>
    </values>
</CustomMetadata>
