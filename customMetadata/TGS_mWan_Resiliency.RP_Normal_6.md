<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RP - Normal - 6</label>
    <protected>false</protected>
    <values>
        <field>TGS_Access_Mode_Values__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>TGS_DataType__c</field>
        <value xsi:type="xsd:string">ResiliencyProfile</value>
    </values>
    <values>
        <field>TGS_ResiliencyType_Key__c</field>
        <value xsi:type="xsd:string">Constraint</value>
    </values>
    <values>
        <field>TGS_ResiliencyType_Value__c</field>
        <value xsi:type="xsd:string">IPSec excluded</value>
    </values>
    <values>
        <field>TGS_Type__c</field>
        <value xsi:type="xsd:string">Normal Access Redundancy</value>
    </values>
</CustomMetadata>
