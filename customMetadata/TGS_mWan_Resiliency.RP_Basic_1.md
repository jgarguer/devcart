<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RP - Basic - 1</label>
    <protected>false</protected>
    <values>
        <field>TGS_Access_Mode_Values__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>TGS_DataType__c</field>
        <value xsi:type="xsd:string">ResiliencyProfile</value>
    </values>
    <values>
        <field>TGS_ResiliencyType_Key__c</field>
        <value xsi:type="xsd:string">Quantity of CPEs</value>
    </values>
    <values>
        <field>TGS_ResiliencyType_Value__c</field>
        <value xsi:type="xsd:string">1</value>
    </values>
    <values>
        <field>TGS_Type__c</field>
        <value xsi:type="xsd:string">Basic</value>
    </values>
</CustomMetadata>
