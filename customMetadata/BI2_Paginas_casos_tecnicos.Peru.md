<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Peru</label>
    <protected>false</protected>
    <values>
        <field>BI2_Pagina_consulta__c</field>
        <value xsi:type="xsd:string">BIIN_Detalle_Incidencia</value>
    </values>
    <values>
        <field>BI2_Pagina_consulta_portal_platino__c</field>
        <value xsi:type="xsd:string">PCA_Reclamo_PopUpDetail</value>
    </values>
    <values>
        <field>BI2_Pagina_consulta_solicitud__c</field>
        <value xsi:type="xsd:string">BIIN_Detalle_Solicitud</value>
    </values>
    <values>
        <field>BI2_Pagina_creacion__c</field>
        <value xsi:type="xsd:string">BIIN_CreacionIncidencia</value>
    </values>
    <values>
        <field>BI2_Pagina_creacion_portal_platino__c</field>
        <value xsi:type="xsd:string">PCA_Reclamo_PopUpNew</value>
    </values>
</CustomMetadata>
