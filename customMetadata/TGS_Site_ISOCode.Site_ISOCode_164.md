<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Site ISOCode - 164</label>
    <protected>false</protected>
    <values>
        <field>TGS_Country__c</field>
        <value xsi:type="xsd:string">Pitcairn</value>
    </values>
    <values>
        <field>TGS_IsoCode__c</field>
        <value xsi:type="xsd:string">PN</value>
    </values>
</CustomMetadata>
