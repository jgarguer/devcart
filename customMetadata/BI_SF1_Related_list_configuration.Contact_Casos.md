<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Cases</label>
    <protected>false</protected>
    <values>
        <field>BI_SF1_Icono__c</field>
        <value xsi:type="xsd:string">standard:case</value>
    </values>
    <values>
        <field>BI_SF1_Layout_assignments_SF1__c</field>
        <value xsi:type="xsd:string">Administrador_del_sistema_Contact</value>
    </values>
    <values>
        <field>BI_SF1_Nombre_lista_relacionada__c</field>
        <value xsi:type="xsd:string">Casos</value>
    </values>
    <values>
        <field>BI_SF1_Orden__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
</CustomMetadata>
