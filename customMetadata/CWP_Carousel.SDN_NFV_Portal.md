<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PortalSDN</label>
    <protected>false</protected>
    <values>
        <field>CWP_Description_Label__c</field>
        <value xsi:type="xsd:string">TGS_SDNPortal</value>
    </values>
    <values>
        <field>CWP_Image_name__c</field>
        <value xsi:type="xsd:string">carrusel_1.png</value>
    </values>
    <values>
        <field>CWP_Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>CWP_Path__c</field>
        <value xsi:type="xsd:string">/export_assets/</value>
    </values>
    <values>
        <field>CWP_Resource__c</field>
        <value xsi:type="xsd:string">$Resource.CWP_Carousel</value>
    </values>
</CustomMetadata>
