<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NuevoEvento</label>
    <protected>false</protected>
    <values>
        <field>BI_SF1_Campos_por_defecto__c</field>
        <value xsi:type="xsd:string">{&quot;entityApiName&quot; : &quot;Event&quot; ,&quot;defaultFieldValues&quot; : {&quot;AccountId&quot; : &quot;#AccountId#&quot;,&quot;WhatId&quot; : &quot;#AccountId#&quot;,&quot;WhoId&quot; : &quot;#Id#&quot;}, &quot;recordTypeId&quot; : &quot;#recordTypeId-BI_Evento#&quot;}</value>
    </values>
    <values>
        <field>BI_SF1_Icono__c</field>
        <value xsi:type="xsd:string">action:new_event</value>
    </values>
    <values>
        <field>BI_SF1_Layout_assignments_SF1__c</field>
        <value xsi:type="xsd:string">Administrador_del_sistema_Contact</value>
    </values>
    <values>
        <field>BI_SF1_Orden__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>BI_SF1_accion__c</field>
        <value xsi:type="xsd:string">newEvent</value>
    </values>
</CustomMetadata>
