<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Administrador del sistema</label>
    <protected>false</protected>
    <values>
        <field>BI_SF1_Campos_query_Id__c</field>
        <value xsi:type="xsd:string">Id, AccountId</value>
    </values>
    <values>
        <field>BI_SF1_Campos_subtitulo_cabecera__c</field>
        <value xsi:type="xsd:string">Account.Name</value>
    </values>
    <values>
        <field>BI_SF1_Campos_titulo_cabecera__c</field>
        <value xsi:type="xsd:string">Name</value>
    </values>
    <values>
        <field>BI_SF1_Conjunto_de_campos__c</field>
        <value xsi:type="xsd:string">BI_SF1_Conjunto_de_campos_SF1</value>
    </values>
    <values>
        <field>BI_SF1_DefaultConfig__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>BI_SF1_Icono_de_la_cabecera__c</field>
        <value xsi:type="xsd:string">standard:contact</value>
    </values>
    <values>
        <field>BI_SF1_Objeto__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>BI_SF1_Perfil__c</field>
        <value xsi:type="xsd:string">Administrador del sistema</value>
    </values>
    <values>
        <field>BI_SF1_Tipo_de_registro__c</field>
        <value xsi:type="xsd:string">TGS_Active_Contact</value>
    </values>
</CustomMetadata>
