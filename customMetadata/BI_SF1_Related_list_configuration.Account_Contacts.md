<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contacts</label>
    <protected>false</protected>
    <values>
        <field>BI_SF1_Icono__c</field>
        <value xsi:type="xsd:string">standard:contact</value>
    </values>
    <values>
        <field>BI_SF1_Layout_assignments_SF1__c</field>
        <value xsi:type="xsd:string">Administrador_del_sistema_Account</value>
    </values>
    <values>
        <field>BI_SF1_Nombre_lista_relacionada__c</field>
        <value xsi:type="xsd:string">Contactos</value>
    </values>
    <values>
        <field>BI_SF1_Orden__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
</CustomMetadata>
