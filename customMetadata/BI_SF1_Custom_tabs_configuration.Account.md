<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Account</label>
    <protected>false</protected>
    <values>
        <field>BI_SF1_Campo_descripcion__c</field>
        <value xsi:type="xsd:string">BI_No_Identificador_fiscal__c</value>
    </values>
    <values>
        <field>BI_SF1_Filtro_de_busqueda__c</field>
        <value xsi:type="xsd:string">Name;BI_No_Identificador_fiscal__c;BI_Country__c</value>
    </values>
    <values>
        <field>BI_SF1_Icono_de_la_cabecera__c</field>
        <value xsi:type="xsd:string">standard:account</value>
    </values>
    <values>
        <field>BI_SF1_Titulo_de_la_cabecera__c</field>
        <value xsi:type="xsd:string">Cliente</value>
    </values>
</CustomMetadata>
