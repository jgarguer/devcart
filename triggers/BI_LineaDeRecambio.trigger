trigger BI_LineaDeRecambio on BI_Linea_de_Recambio__c (after insert, before insert, before delete, after update, before update) {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Trigger on BI_Linea_de_Recambio__c
    
    History:
    
    <Date>			  <Author> 	        <Description>
    25/02/2015        Pablo Lozon     	Initial version
    25/05/2015		  Pablo Oliva
    20/07/2017        Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Linea_de_Recambio__c');
    
	list<BI_Linea_de_Recambio__c> lOld = trigger.old;
	list<BI_Linea_de_Recambio__c> lNew = trigger.new;
	
	if(!isTriggerDisabled){
		if (trigger.isBefore){
			if (trigger.isInsert){
				BI_LineaDeRecambioMethods.insertLineaDeRecambioIMP(lNew);
				BI_LineaDeRecambioMethods.actualizarHoraSolicitud(lNew);
			}
			if(trigger.isUpdate){
				BI_LineaDeRecambioMethods.actualizarLineaDeServicio(trigger.new);
			}
			else if (trigger.isDelete){
				BI_LineaDeRecambioMethods.eliminarLineaDeServicio(trigger.old);
				BI_LineaDeRecambioMethods.updateDesc(trigger.old);
			}
		}
		else if (trigger.isAfter){
			if (trigger.isInsert){
				BI_LineaDeServicioHelper.insertLineaDeServicioIMP(lNew);
				BI_LineaDeRecambioMethods.insertLineaDeRecambioValidate(trigger.newMap);
			}else if(trigger.isUpdate){
				BI_LineaDeRecambioMethods.updateSol(trigger.new, trigger.old);
				BI_LineaDeRecambioMethods.validateBackOffice(trigger.new, trigger.old);
				BI_LineaDeRecambioMethods.actualizarLineaDeServicioStatus(trigger.new, trigger.old);
			}
		}
	}
}