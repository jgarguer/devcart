trigger BI_LineaDeVenta on BI_Linea_de_Venta__c (after insert, before insert, before delete, after update, before update) {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Trigger on BI_Linea_de_Venta__c
    
    History:
    
    <Date>			  <Author> 	        <Description>
    25/02/2015        Pablo Lozon     	Initial version
    20/07/2017        Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Linea_de_Venta__c');
    
    if(!isTriggerDisabled){
		if (trigger.isBefore){
			if (trigger.isInsert){
				BI_LineaDeVentaMethods.insertLineaDeVentaIMP(trigger.new);
				BI_LineaDeVentaMethods.actualizarHoraSolicitud(trigger.new);
			}
			if (trigger.isUpdate){
				BI_LineaDeVentaMethods.actualizarLineaDeServicio(trigger.new);
			}
			else if (trigger.isDelete){
				BI_LineaDeVentaMethods.deleteLineaDeServicio(trigger.old);
				BI_LineaDeVentaMethods.updateDesc(trigger.old);
			}
		}else{
			if (trigger.isInsert){
				BI_LineaDeServicioHelper.insertLineaDeServicioIMP(trigger.new);
				BI_LineaDeVentaMethods.insertLineaDeVentaSplitAndValidate(trigger.newMap);
			}else if(trigger.isUpdate){
				BI_LineaDeVentaMethods.updateSol(trigger.new, trigger.old);
				BI_LineaDeVentaMethods.validateBackOffice(trigger.new, trigger.old);
				BI_LineaDeVentaMethods.actualizarLineaDeServicioStatus(trigger.new, trigger.old);
			}
		}
	}
}