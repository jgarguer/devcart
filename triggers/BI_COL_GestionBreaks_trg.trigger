/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Trigger Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           		Descripción
*           -----   ----------      --------------------            		---------------
* @version   1.0    2015-04-20      Manuel Esthiben Mendez Devia (MEMD)     Cloned Trigger
* @version	 2.0    20-Jul-2017     Guillermo Muñoz              			Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
*************************************************************************************************************/
trigger BI_COL_GestionBreaks_trg on BI_COL_Seguimiento_Quiebre__c (after insert, after update, before insert, 
before update) 
{

	//GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_COL_Seguimiento_Quiebre__c');
    
    if(!isTriggerDisabled){
		if(!system.isFuture() && trigger.new.size()==1)
		{
			system.debug(trigger.new[0].BI_COL_Transaccion__c+'---- estado transsaccion trigger');

			if(trigger.isBefore)
			{
				if(trigger.isInsert)
				{
					BI_COL_GestionBreaks_ctr.gestionQuiebres(trigger.new[0],'crear','');
				}
				
				if(trigger.isUpdate)
				{	
					if(trigger.old[0]!=null)
					{
						BI_COL_GestionBreaks_ctr.gestionQuiebres(trigger.new[0],'actualizar',trigger.old[0].BI_COL_Observaciones__c);
					}else
						{
							BI_COL_GestionBreaks_ctr.gestionQuiebres(trigger.new[0],'actualizar','');
						}
				}	
			}
			
			if(trigger.isAfter)
			{
				if(trigger.new[0].BI_COL_Aplazar_MS__c)
				{ 
	    			BI_COL_GestionBreaks_ctr.actualizarMs(trigger.new[0].Id);
				}
				
				if(trigger.isInsert && userinfo.getName()!='INTERFAZ SALESFORCE')
				{
					BI_COL_GestionBreaks_ctr.envioQuiebreSW(trigger.new[0].Id, trigger.new[0].BI_COL_Observaciones__c, 'crear',trigger.new[0].BI_COL_Fecha_contactar_cliente__c);
				}
				
				if(trigger.isUpdate && userinfo.getName()!='INTERFAZ SALESFORCE')
				{
					 if(Limits.getFutureCalls() < Limits.getLimitFutureCalls()){BI_COL_GestionBreaks_ctr.envioQuiebreSW(trigger.new[0].Id, trigger.old[0].BI_COL_Observaciones__c, 'actualizar', trigger.old[0].BI_COL_Fecha_contactar_cliente__c);}
				}
					
			}
	    }
	}
}