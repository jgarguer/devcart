trigger BI_Registro_Datos_Desempeno on BI_Registro_Datos_Desempeno__c (after insert, after update) {

	//GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Registro_Datos_Desempeno__c');
    
    if(!isTriggerDisabled){
		if(trigger.isInsert){
			if(trigger.isAfter){
				BI_FVI_DesempenoMethods.generaNodo_Objetivo_Desempeno(trigger.new , trigger.old);
				BI_FVI_DesempenoMethods.recopilaDatosDesempenoParaOppYRevenues(trigger.new);
			}
		}else if(trigger.isUpdate){
			if(trigger.isAfter){
				BI_FVI_DesempenoMethods.actualizaValor(trigger.new , trigger.old);
			}
		}
	}
}