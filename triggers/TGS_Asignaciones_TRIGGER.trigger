/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Juan Carlos Terrón
    Company:       New Energy Aborda
    Description:   Trigger to handle TGS_Asignaciones__c custom object events.
    
    History:   
    <Date>                  <Author>                <Change Description>   
    14/11/2016				Juan Carlos Terrón		Initial Version.     
    20/07/2017      		Guillermo Muñoz     	Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
trigger TGS_Asignaciones_TRIGGER on TGS_Asignaciones__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	BI_bypass__c bypass = BI_bypass__c.getInstance();
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
	Boolean TGS = userTGS.TGS_Is_TGS__c;

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('TGS_Asignaciones__c');
    
    if(!isTriggerDisabled){
		if (Trigger.isBefore && Trigger.isInsert)
		{
			if(TGS)	TGS_Asignaciones_Methods.generate_TGS_AssignId(Trigger.new);
		}
		else if (Trigger.isAfter && Trigger.isUpdate)
		{
			if(TGS)	TGS_Asignaciones_Methods.modify_TGS_AssignId(Trigger.newMap, Trigger.oldMap);
		}
	}
}