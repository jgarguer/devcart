trigger NE_CatalogItem on NE__Catalog_Item__c (before update, before insert) {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Trigger on NE__Catalog_Item__c
    
    History:
    
    <Date>			  <Author> 	               <Description>
    05/05/2014        Pablo Oliva     	       Initial version
    15/05/2017        Cristina Rodríguez       Solve Multiple Trigger On same sObject (Added UpdateCurrencyIsoCode.trigger)
    20/07/2017        Guillermo Muñoz          Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('NE__Catalog_Item__c');
    
    if(!isTriggerDisabled){
        if(trigger.isBefore){
            //if(trigger.isInsert){
            //}else 
            if(trigger.isUpdate){ 
            	BI_NECatalogItemMethods.inactivateCampaign(trigger.new, trigger.old);
            }
            if(trigger.isUpdate || trigger.isInsert){
                BI_NECatalogItemMethods.updateCurrencyIsoCode(trigger.new);
            }
            //else if(trigger.isDelete){
            //}
            //else if(trigger.isUnDelete){
            //}
        }
        //else{ 
            //if(trigger.isInsert){ 
            //}
            //else if(trigger.isUpdate){
            //}
            //else if(trigger.isDelete){
            //}         
        //}
    }

}