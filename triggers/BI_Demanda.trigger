/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Masferrer
Company:       Aborda
Description:   Trigger for BI_Demanda__c

History: 

<Date>                          <Author>                    <Change Description>

01/03/2016                     Antonio Masferrer              Initial Version
20/07/2017      			   Guillermo Muñoz     	   		  Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
30/10/2017				Angel F. Santaliestra Pasias		Added BI_DemandaMethods.calculateTimeBetweenStages.
-------------------------------------------------------------------------------------------------------------------------------------------------------*/
trigger BI_Demanda on BI_Demanda__c (before insert, before update, after insert, after update) {
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Demanda__c');
    
    if(!isTriggerDisabled){
		if(trigger.isBefore){

			if(trigger.isInsert || trigger.isUpdate){
				
				BI_DemandaMethods.asignaPropietario(trigger.new, trigger.old);
				BI_DemandaMethods.asignaTipoDemanda(trigger.new, trigger.old);
			}
			if(trigger.isUpdate){
				BI_DemandaMethods.calculateTimeBetweenStages(trigger.new, trigger.old);
			}
		}
			
		if(trigger.isAfter){

			if(trigger.isUpdate){
				
				BI_DemandaMethods.cambioAsignacion(trigger.new, trigger.old);
			}
		}
	}
}