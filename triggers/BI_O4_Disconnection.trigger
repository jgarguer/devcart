/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Jose Miguel Fierro
	Company:       Salesforce.com
	Description:   Methods executed by the BI_O4_Disconnection__c trigger.

	History

	<Date>          <Author>                      <Description>
	28/06/2016      Jose Miguel Fierro            Initial version
	20/07/2017      Guillermo Muñoz     		  Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

trigger BI_O4_Disconnection on BI_O4_Disconnection__c (before insert, before update, after update) {
	BI_bypass__c bypass = BI_bypass__c.getInstance();
	TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
	Boolean TGS = userTGS.TGS_Is_TGS__c;
	Boolean BI = userTGS.TGS_Is_BI_EN__c;

	//GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_O4_Disconnection__c');

    if(!isTriggerDisabled){
		if(Trigger.isBefore && !bypass.BI_skip_trigger__c) {
			if(Trigger.isInsert) {
				if(BI) {
					BI_O4_DisconnectionMethods.setCustomFieldAccountOwner(Trigger.new);
				}
			}
			if(Trigger.isUpdate) {
				if(BI) {
					BI_O4_DisconnectionMethods.validateRequiredFields(Trigger.new);
					BI_O4_DisconnectionMethods.checkRelatedCaseBillingRequest(Trigger.newMap, Trigger.oldMap);
					BI_O4_DisconnectionMethods.setRecordsOwnership(Trigger.new, Trigger.oldMap);
					BI_O4_DisconnectionMethods.setCustomFieldAccountOwner(Trigger.new);
				}
			}
		}
		if(Trigger.isAfter && !bypass.BI_skip_trigger__c) {
			if(Trigger.isUpdate) {
				if(BI) {
					if(!BI_O4_DisconnectionMethods.isProcessRunning()) {
						BI_O4_DisconnectionMethods.setProcessRunning();
						BI_O4_DisconnectionMethods.submitForApproval(Trigger.newMap, Trigger.oldMap);
					}
				}
			}
		}
	}
}