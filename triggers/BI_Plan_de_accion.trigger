trigger BI_Plan_de_accion on BI_Plan_de_accion__c (after update, before update, before insert) {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Trigger on BI_Plan_de_accion__c
    
    History:
    
    <Date>			  <Author> 	        <Description>
    30/01/2015        Pablo Oliva     	Initial version
    27/09/2016		  Miguel Cabrera 	Llamadas a metodo escalationBeforeAfterUpdate, escalationAfterInsert, escalationBeforeInsert añadidos
    20/07/2017        Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    BI_bypass__c bypass = BI_bypass__c.getInstance();

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Plan_de_accion__c');
    
    if(!isTriggerDisabled){
		if (trigger.isBefore){
			
			if (trigger.isInsert){
		    	BI_PlanDeAccionMethods.escalationBeforeInsert(trigger.new);
		    }
		    if (trigger.isUpdate){
		    	            
	            if(!bypass.BI_migration__c){
	            	BI_PlanDeAccionMethods.check_status(trigger.newMap, trigger.old);
	            }

		    	BI_PlanDeAccionMethods.check_HasOpenTask(trigger.newMap, trigger.old);
		    	BI_PlanDeAccionMethods.escalationBeforeAfterUpdate(trigger.new,true,false,true,trigger.oldMap);
		    }
		    //if (trigger.isDelete){
		    //}
		
		} else { 
		        
			if (trigger.isInsert){ 
		    	BI_PlanDeAccionMethods.asociarUsuariosAccountTeam(trigger.new);
				BI_PlanDeAccionMethods.escalationAfterInsert(trigger.new);
		    }
		    if (trigger.isUpdate){
		    	BI_PlanDeAccionMethods.updateOrigin(trigger.new, trigger.old);
		    	BI_PlanDeAccionMethods.update_taskStatus(trigger.newMap, trigger.old);
		    	BI_PlanDeAccionMethods.escalationBeforeAfterUpdate(trigger.new,false,true,true,trigger.oldMap);
		    }
		    //if (trigger.isDelete){
		    //}
		    
	    }
	}
}