trigger Opportunity on Opportunity (before insert, before update, after insert, after update, before delete, after delete) {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Diego Arenas
    Company:       Salesforce.com
    Description:   Methods executed by the Opportunity trigger.

    History

    <Date>            <Author>              <Description>
    11/02/2014        Diego Arenas          Initial version
    13/05/2014        Pablo Oliva           Method BI_OpportunityMethods.changeOwner added
    16/05/2014        Pablo Oliva           Method BI_OpportunityMethods.changeOwner deleted
    19/05/2014        Ignacio Llorca        Method BI_OpportunityMethods.blockUpdateOppTask added
    19/05/2014        Pablo Oliva           Method BI_OpportunityMethods.createTask added
    21/05/2014        Ignacio Llorca        Method BI_OpportunityMethods.changeOwner added
    23/06/2014        Ignacio Llorca        Method BI_OpportunityMethods.updateTaskStatus added
    04/08/2014        Ana Escrich           Method BI_OpportunityMethods.addEconomicInfo added
    04/08/2014        Ana Escrich           Method BI_OpportunityMethods.deductEconomicInfo added
    05/08/2014        Ana Escrich           Method BI_OpportunityMethods.recalculateParentOpportunityData added
    05/08/2014        Ana Escrich           Method BI_OpportunityMethods.addEconomicInfo deleted
    05/08/2014        Ana Escrich           Method BI_OpportunityMethods.deductEconomicInfo deleted
    02/09/2014        Micah Burgos          Method BI_OpportunityMethods.checkAndCompleteHiddenFields added
    29/09/2014        Bejoy Babu            Method BI_FOCO_OppTriggerHelper.afterIns added
    29/09/2014        Bejoy Babu            Method BI_FOCO_OppTriggerHelper.afterUpd added
    29/09/2014        Bejoy Babu            Method BI_FOCO_OppTriggerHelper.afterDel added
    16/12/2014        Pablo Oliva           Method BI_OpportunityMethods.preventChangeStatus added
    27/10/2015        Guillermo Muñoz       Method BI_OpportunityMethods.validateCurrencyIsoCodeCAM added
    29/10/2015        Gustavo Kupcevich     Method BI_SCP_BigDeals_Triggers.insertOpportunityBefore added
    29/10/2015        Gustavo Kupcevich     Method BI_SCP_BigDeals_Triggers.insertOpportunityAfter added
    29/10/2015        Gustavo Kupcevich     Method BI_SCP_BigDeals_Triggers.deleteOpportunity added
    20/11/2015        Guillermo Muñoz       Method BI_OpportunityMethods.validateClosedCasesARG added
    27/11/2015        Antonio Masferrer     Method BI_OpportunityMethods.sumTotalModificationService added
    21/01/2016        Micah Burgos          Change !bypass.BI_migration__c to !bypass.BI_skip_trigger__c. BI_migration__c is used for ValidationRules and some code validations and BI_skip_trigger__c disable the trigger.
    05/02/2016        Guillermo Muñoz       Method BI_OpportunityMethods.checkReturnedCasesARG added
    18/02/2016        Javier Suarez         Method VE_venta_express_trg_code.moveOppty added
	22/02/2016        Javier Suarez         Method SIMP_startStage.setStageName added
    02/03/2016        Micah Burgos          Add BI_OpportunityMethods.change_currency(trigger.new, trigger.old) method.
    27/04/2016        Antonio Pardo         Method BI_OpportunityMethods.checkFCVMEX added
    04/06/2016        Humberto Nunes        Se Omitio la ejecucion de todos los triggers cuando se hace una actualizacion del campo BI_FVI_SubEstado__c
    06/06/2016        Guillermo Muñoz       Method BI_OpportunityMethods.validateAccountPartner added
    20/06/2016        Alejandro Martinez    BI_OpportunityMethods.updateOpportunityClient added
    25/06/2016        Humberto Nunes        Se adiciono la llamada del metodo "BI_OpportunityMethods.FillActosComerciales" Despues de Actualizar
    01/07/2016        Jorge Galindo         Se adiciono la llamada del metodo "BI_FVI_AutoSplitMethods" Despues de Actualizar
    04/07/2016        Manuel Medina         Method VE_venta_express_trg_code.uncheckExpress
	06/07/2016        Antonio Masferrer     Añadida la llamada al método crearCasosInternosFVI despues de actualizar
    19/07/2016        Jorge Galindo         Si el update en la Opty proviene de la ejecucion del  "BI_FVI_AutoSplitMethods" no hay que llamar a los triggers de actualizacion
    19/07/2016        Alfonso Alvarez       Añadido comentario para identificar llamada isFuture.
    21/07/2016        Pablo Lozón          Quitadas las llamadas a Future.
    22/07/2016        Jorge Galindo         Quitados cambios 19/07/2016
    29/07/2016        Antonio Pardo         Method BI_OpportunityMethods.block_SISON_update modified added trigger oldMap
    01/08/2016        Fernando Arteaga      Added updateOpptyStage / validateStageChange methods from BI_O4_OpportunityMethods class
	07/08/2016		  Jorge Galindo			Encapsulate all BI functionality to avoid launching when is not a BI user
    25/08/2016        Guillermo Muñoz       Method BI_OpportunityMethods.updateComisionableMEX added
    12/08/2016        Humberto Nunes        Se a agregdo la variable Boolean FVI = userTGS.BI_FVI_Is_FVI__c y se ha usaado en el After Insert
    29/08/2016        Guillermo Muñoz       Method BI_OpportunityMethods.updateAccountARG added
    15/09/2016        Guillermo Muñoz       Method BI_OpportunityMethods.validateClosedCasesPER added
    05/10/2016        Fernando Arteaga      Don't execute new methods if running within Queueable or Future context on update trigger
    06/10/2016        Patricia Castillo     Added call validateUserModifierARG (before insert/update) y updateContractFieldsARG (after update) (D321)
	19/10/2016		  Sara Núñez			Changed function to validateClosedCases
    17/11/2016        Humberto Nunes        Add call to updateOppFromOrder
    08/11/2016        Gawron, Julián        Add call to BI_OpportunityMethods.updateTasaPresuItemsFields
    05/01/2017        Alvaro Garcia         Add call to BI_OpportunityMethods.fillOportunidadInterna
    21/01/2017        Guillermo Muñoz       Add call to BI_O4_OportunityMethods.validateGate1Status and BI_O4_OpportunityMethods.generateSalesOperationCases
    26/01/2017        Alvaro Garcia         Change BI_OpportunityMethods.putOppTeamMember
    25/01/2017        Alberto Fernández     Add call to BI_O4_OportunityMethods.updateOrdersTdCP
    09/02/2017        Alvaro Garcia         Added call to BI_O4_OpportunityMethods.Accountability
    14/02/2017        Gawron, Julián        Uncomment validateUserModifierARG
    01/03/2017        Guillermo Muñoz       Method BI_O4_OpportunityMethods.calculateChilds added
    09/03/2017        Guillermo Muñoz       Deprecated BI_O4_OpportunityMethods.updateTdCPinOI()
    13/03/2017        Humberto Nunes        Se omitio la llamada al metodo BI_OpportunityMethods.FillActosComerciales por DUPLICADA solo se dejo la de FVI
    15/03/2017        Guillermo Muñoz       Method BI_O4_OpportunityMethods.generateCases added and method BI_O4_OpportunityMethods.generateSalesOperationCases deprecated
    12/05/2017        Óscar Bartolo         Add to method BI_O4_OpportunityMethods.Accountability and BI_O4_OpportunityMethods.lookForGSE
    07/06/2017	      Óscar Bartolo         Change call Accountability to updateAccountability when is update
	08/06/2017	      Óscar Bartolo         Add to updateAccountabilityParent
    21/06/2017        Guillermo Muñoz       BI_OpportunityMethods.updateTasaPresuItemsFields moved from after update to before update
	03/04/2017        Eduardo Ventura       Integración de métodos para sincronización con Fullstack. [FS_CORE 001]
    22/06/2017        Javier Almirón        Add call to BI_O4_OpportunityMethods.NavPresup method in Trigger.isBefore.(If BI).IsUpdate/IsDelete.
	27/06/2017        Eduardo Ventura       Actualizar datos de account a opportunity para crédito. [FS_CHI 001]
    29/06/2017        Victoria Gutiérrez    Add method sincWithFullStack in update
    19/06/2017        Humberto Nunes        Se encapsulo el Metodo BI_FVI_AutoSplitMethods.autoSplit para usuarios BI y FVI
    20/07/2017        Guillermo Muñoz       Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    01/09/2017        Alberto Fernández     Added call to BI_O4_OpportunityMethods.updateQuoteSkipGates on before update
    02/10/2017        Alvaro Sevilla        Se Adiciono invocacion al metodo enviarEmailOfertaFVI para FVI
    05/10/2017        Javier Almirón        Replace the calls to  BI_O4_OpportunityMethods.wonOpenSumFields
    25/10/2017        Gawron, Julián        Added call to BI_OpportunityMethods.fieldReplyForecastCategoryName on before insert and update
    17/11/2017        Javier Almirón        Added call to BI_O4_OpportunityMethods.oppTargetAssign after insert and after update
    21/02/2018        Alvaro Sevilla        Added invocation to method validarOptyconSubsidio in before update
    01/03/2018        Alvaro Sevilla        Se adiciono invocacion con bandera al metodo CreatedPedSubsidio
    05/03/2018        Javier Almirón        Added call to BI_O4_OpportunityMethods.TIWS_Validation
    28/02/2018        Jaime Regidor         Added call to BI_OpportunityMethods.validateContractF1
    25/04/2018		  Manuel Ochoa			Added condition to avoid call of FOCO  methods for batch jobs from BULK
    26/04/2018        Alvaro Sevilla        Se adiciono invocacion a metodo validarCamposSubsidioyIDSubsidio
    10/05/2018        Alfonso Alvarez       Se realiza invocación a método duracionOpty2MS de la clase BI_COL_Opportunity_ctr
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    system.debug('#~Prueba PB');
    Map<String, BI_FOCO_Trigger__c> triggerMap = BI_FOCO_Trigger__c.getAll();
    /*
    Custom setting creation in order to use "Toggle FOCO Trigger" (If On_Off__c value is set to false, the trigger "FOCUS" is disabled)
    */
     //OMITIR TODO ESTE PROCESO SI ES UNA ACTUALIZACION DEL CAMPO "BI_FVI_SubEstado__c" Porque se hace con un @Future y aca se llama a otro y da error.
    if(Test.isRunningTest() && triggerMap.isEmpty() && !(trigger.isUpdate && trigger.new[0].BI_FVI_SubEstado__c != trigger.old[0].BI_FVI_SubEstado__c))
    {
        insert new List<BI_FOCO_Trigger__c>{new BI_FOCO_Trigger__c(Name = 'Opportunity Insert', On_Off__c = true),
                                            new BI_FOCO_Trigger__c(Name = 'Opportunity Update', On_Off__c = true),
                                            new BI_FOCO_Trigger__c(Name = 'Opportunity Delete', On_Off__c = true)};

        triggerMap = BI_FOCO_Trigger__c.getAll();

    }
    BI_bypass__c bypass = BI_bypass__c.getInstance();
    /*FS_CORE 001-INI Eduardo Ventura 27/06/2017*/
    FS_CORE_Integracion__c ORGConfiguration = FS_CORE_Integracion__c.getInstance();
     /*FS_CORE 001-FIN Eduardo Ventura 27/06/2017*/

    // JGL 07/08/2016
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c; // HN 12/08/2016
    // END JGL 07/08/2016

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Opportunity');

    if(!isTriggerDisabled){
        if (trigger.isBefore && !bypass.BI_skip_trigger__c)
        {
            if(!NETriggerHelper.getTriggerFired('BI_Opportunity_TriggerHelper BEFORE')) BI_Opportunity_TriggerHelper.loader_TriggerData(Trigger.new);
            if(FVI) // HN 17/11/2016
            {
               BI_OpportunityMethods.updateOppFromOrder(trigger.New,trigger.old);
            }
            if (trigger.isInsert){
                SIMP_startStage.setStageName(Trigger.New);
                //BI_OpportunityMethods.blockDuplicateOppName(trigger.new);

                /*Este trigger calcula los campos “Estimated_Year_End_Revenue__c” y  “Full_Contract_Value__c” dependiendo de reglas pre establecidas.
                Este trigger en ningún momento interrumpe la carga de Oportunidades.*/
                //Certa_SCP.UtilTriggers.opportunitySetCurrentYearCharges(trigger.New);
                //BI_Active_Opp_trigger_change_owner__c ChangeOwnerToggle = BI_Active_Opp_trigger_change_owner__c.getInstance();
                //if (ChangeOwnerToggle.BI_fire_change_owner__c){
                //BI_OpportunityMethods.calculate_formulaFields(trigger.new,trigger.old); //Deprecated
                //BI_bypass__c bypass = BI_bypass__c.getInstance();

                BI_SCP_BigDeals_Triggers.insertOpportunityBefore(trigger.new);
                BI_OpportunityMethods.fillOportunidadInterna(trigger.new, null);

                // Encapsulate all BI meethods
                if(BI){//JGL 07/08/2016
                    if(!bypass.BI_migration__c){
                        BI_OpportunityMethods.changeOwnerAndCountry(trigger.new);
                        BI_OpportunityMethods.validateCurrencyIsoCodeCAM(trigger.new,null);

                        BI_O4_OpportunityMethods.updateTdCP(trigger.new, null); //JEG
                        BI_OpportunityMethods.validateUserModifierARG(trigger.new, null);
                        /* FS_CHI 001 INI Edu Ventura 27/06/2017 */ BI_OpportunityMethods.updateInfoOpportunityByAccount(trigger.new); /* FS_CHI 001 FIN Edu 27/06/2017*/
                    }

                    BI_OpportunityMethods.checkFCVMEX(trigger.new, trigger.old);
                    BI_OpportunityMethods.validateAccountPartner(trigger.new, null);

                    BI_O4_OpportunityMethods.Accountability(trigger.new);
                    BI_OpportunityMethods.fieldReplyForecastCategoryName(trigger.new, trigger.old); //JEG 25/10/2017

                }//JGL 07/08/2016

            }
            if (trigger.isUpdate){
                //BI_OpportunityMethods.sincWithFullStack(trigger.new, trigger.oldMap);

                VE_venta_express_trg_code.uncheckExpress(trigger.new, trigger.old);

			VE_venta_express_trg_code.veTakeOpportunity(trigger.new, trigger.old);

                VE_venta_express_trg_code.moveOppty(trigger.new);

                BI_SCP_BigDeals_Triggers.updateOpportunityBefore(trigger.new, trigger.oldMap);
                BI_OpportunityMethods.fillOportunidadInterna(trigger.new, trigger.oldMap);

                // Encapsulate all BI meethods
                if(BI){//JGL 07/08/2016

                    /*Álvaro López - 11/07/2017*/
                    if(!FVI){
                        BI_OpportunityMethods.validatePreviousRecurringCharge(trigger.new, trigger.oldMap);
                    }
                    BI_OpportunityMethods.saveFieldUpdate(trigger.new, trigger.old); //JEG D-000494
                    BI_OpportunityMethods.preventChangesIfF1(trigger.new, trigger.old);
                    //BI_OpportunityMethods.sumTotalModificationService(trigger.new);
                    //BI_OpportunityMethods.calculate_formulaFields(trigger.new,trigger.old); //Deprecated
                    BI_OpportunityMethods.validarOptyconSubsidio(trigger.new, trigger.old);

                    //BI_bypass__c bypass = BI_bypass__c.getInstance();
                    if(!bypass.BI_migration__c){
                        BI_OpportunityMethods.preventChangeStatus(trigger.new, trigger.old);

                        BI_OpportunityMethods.block_SISON_update(trigger.newMap,trigger.oldMap);
                        //BI_OpportunityMethods.unlock_SISON_update(trigger.new,trigger.old);
                        BI_OpportunityMethods.validateUserModifierARG(trigger.new, trigger.oldMap);
                        //BI_OpportunityMethods.block_sison_update(trigger.new,trigger.old);
                        BI_O4_OpportunityMethods.validateGate1Status(trigger.new, trigger.oldMap);
                    }

                    BI_OpportunityMethods.checkAndCompleteHiddenFields(trigger.new, trigger.old);
                    BI_OpportunityMethods.updateOfferFields(trigger.new, trigger.old);
                    //BI_OpportunityMethods.blockDuplicateOppName(trigger.new);

                    /*Este trigger calcula los campos “Estimated_Year_End_Revenue__c” y  “Full_Contract_Value__c” dependiendo de reglas pre establecidas.
                    Este trigger en ningún momento interrumpe la carga de Oportunidades.*/
                    //Certa_SCP.UtilTriggers.opportunitySetCurrentYearCharges(trigger.New);
                    BI_OpportunityMethods.updateTaskStatus(trigger.new, trigger.old);
                    BI_OpportunityMethods.blockUpdateOppTask(trigger.old, trigger.new);
                    //03/12/2014              Micah Burgos            Comment because not in use.Ecuador opp can´t change BI_Tipo_de_oferta_Per__c
                    //BI_OpportunityMethods.changeOppType(trigger.new, trigger.old);
                    BI_COL_Opportunity_ctr.fnValidarCierreOportunidad( trigger.new, trigger.old);
                    BI_OpportunityMethods.validateCurrencyIsoCodeCAM(trigger.new,trigger.old);

                    BI_OpportunityMethods.validateClosedCasesARG(trigger.new,trigger.old);
                    BI_OpportunityMethods.validateContractF1(trigger.new,trigger.old);
                    BI_OpportunityMethods.checkReturnedCasesARG(trigger.new,trigger.old);
                    BI_OpportunityMethods.checkFCVMEX(trigger.new, trigger.old);
                    BI_OpportunityMethods.validateAccountPartner(trigger.new, trigger.old);
                    BI_OpportunityMethods.updateComisionableMEX(trigger.new, trigger.old);
                    BI_O4_OpportunityMethods.updateTdCP(trigger.new, trigger.oldMap); //JEG
                    BI_O4_OpportunityMethods.updateQuoteSkipGates(trigger.new, trigger.oldMap);
                    BI_OpportunityMethods.fieldReplyForecastCategoryName(trigger.new, trigger.old); //JEG 25/10/2017

                    if(!bypass.BI_migration__c){
                        BI_OpportunityMethods.validateClosedCases(trigger.new, trigger.old);//Changed SNM 19/10/2016
                    }
                    // FAR 01/08/2016 - Infinity W4+5: Validate stage changes
                    // FAR 05/10/2016 - Don't execute methods if Queueable or Future context
                    if (!System.isFuture() && !System.isQueueable())
                    {
                        BI_O4_OpportunityMethods.validateStageChange(trigger.newMap, trigger.oldMap);
                        if (!BI_O4_OpportunityMethods.hasBeenExecutedOnce) {
                            BI_O4_OpportunityMethods.updateOpptyStage(trigger.new, trigger.old);
                        }
                        BI_O4_OpportunityMethods.summarizeParentEconomicInfoOnSync(trigger.newMap, trigger.oldMap);
                    }
    				//OBP 05/06/2017
                    BI_O4_OpportunityMethods.UpdateAccountability(trigger.new);
                    BI_OpportunityMethods.updateTasaPresuItemsFields(trigger.new, trigger.old);

                    // HN 29/01/2018
                    BI_OpportunityMethods.Create_RDD_NCJ(trigger.new, trigger.old);

                    // HN 12/02/2018
                    BI_OpportunityMethods.Delete_RDD_NCJ(trigger.new, trigger.old);


                }//JGL 07/08/2016
            }
            //if (trigger.isDelete){

            //}
        }else if(!bypass.BI_skip_trigger__c){  //isAfter:
            if (trigger.isInsert){
                if(!NETriggerHelper.getTriggerFired('BI_Opportunity_TriggerHelper AFTER')) BI_Opportunity_TriggerHelper.loader_TriggerData(Trigger.newMap,Trigger.oldMap);
                SIMP_startStage.setOptyMember(Trigger.New);
                BI_SCP_BigDeals_Triggers.insertOpportunityAfter(trigger.new);

                BI_OpportunityMethods.updateAccOferta(trigger.new, null);//D398

                // Encapsulate all BI meethods
                if(BI){//JGL 07/08/2016
                    //BI_bypass__c bypass = BI_bypass__c.getInstance();
                    if(!bypass.BI_migration__c){
                        BI_OpportunityMethods.createTask(trigger.new, trigger.old);
                        BI_O4_OpportunityMethods.oppTargetAssign (trigger.new , trigger.oldMap);
                    }

                    //SIMP_startStage.setOptyMember(Trigger.New);
                    BI_O4_OpportunityMethods.wonOpenSumFieldsAuxiliar1 (trigger.new, trigger.oldMap);
                    BI_OpportunityMethods.updateTaskStatus(trigger.new, trigger.old);
                    BI_OpportunityMethods.putOppTeamMember(trigger.new);

    				// FAR 01/08/2016 - recalculateParentOpportunityData is replaced by updateOpportunityStage
    				//BI_OpportunityMethods.recalculateParentOpportunityData(trigger.new, null);

    				// FAR 16/09/2016
    				if (!System.isFuture() && !System.isQueueable())
    					BI_O4_OpportunityMethods.calcParentCountriesAndScope(Trigger.newMap, null);

                    // 25-04-2018 - Manuel Ochoa - added condition for batch jobs from BULK
                    if(!System.isFuture() && !System.isBatch() && triggerMap.get('Opportunity Insert').On_Off__c == true ){ //PLL 21/07/2016
                        List<String> idArr = BI_FOCO_OppTriggerHelper.getIdArray(trigger.new);
                        if(idArr.size()> 0)
                            BI_FOCO_OppTriggerHelper.afterIns(idArr);
                    }

                    BI_O4_OpportunityMethods.calculateChilds(Trigger.new, null);

                    // OBP 05/06/2017
                    BI_O4_OpportunityMethods.lookForGSE(Trigger.new);
                    //OBP 08/06/2017
                    BI_O4_OpportunityMethods.UpdateAccountabilityParent(trigger.new, trigger.old);
                }//JGL 07/08/2016
            }
            else if (trigger.isUpdate){
                 // AJAE 10/05/2018
                 BI_COL_Opportunity_ctr.duracionOpty2MS(trigger.old,trigger.new);
                 // FIN AJAE 10/05/2018
                 BI_SCP_BigDeals_Triggers.updateOpportunityAfter(trigger.new, trigger.oldMap);

                 BI_OpportunityMethods.updateAccOferta(trigger.new, trigger.oldMap);//D398
                 if(BI){
                    // ASD 01/03/2018 demanda 556
                    boolean wasFired = NETriggerHelper.getTriggerFired('CreatedPedSubsidio');
                    if(Test.isRunningTest())
                        wasFired = false;
                    if(!wasFired)
                    {
                        BI_OpportunityMethods.CreatedPedSubsidio(trigger.new, trigger.old);
                        NETriggerHelper.setTriggerFired('CreatedPedSubsidio');

                    }
                 }

                // Encapsulate all BI meethods
                if(BI || FVI) // HN 19/06/2017
                {
                    BI_FVI_AutoSplitMethods.autoSplit(trigger.newMap, trigger.oldMap);
                    //ASD 02/10/2017
                    BI_OpportunityMethods.generacionYenvioEmailOfertaFVI(trigger.new,trigger.old);
                }

                if(BI)
                {//JGL 07/08/2016
    	boolean wasFired    =   NETriggerHelper.getTriggerFired('NECheckOptyFields'); //CRM 15/05/2017  NECheckOptyFields.trigger
                    if(Test.isRunningTest())
                        wasFired = false;
                    if(!wasFired)
                    {
                        BI_OpportunityMethods.checkOptyFields(Trigger.new, Trigger.oldMap);
                    }//END CRM 15/05/2017  NECheckOptyFields.trigger

                //JGL 07/08/2016
                BI_O4_OpportunityMethods.NavPresup(trigger.new,trigger.oldMap);
                BI_O4_OpportunityMethods.wonOpenSumFieldsAuxiliar1 (trigger.new, trigger.oldMap);
                System.debug('ABAB FLAG T.0 - BI_O4_OpportunityMethods.oppTargetAssign');
                BI_O4_OpportunityMethods.oppTargetAssign (trigger.new , trigger.oldMap);

                // End JGL 01/07/2016

                    //BI_bypass__c bypass = BI_bypass__c.getInstance();
                    if(!bypass.BI_migration__c){
                        BI_OpportunityMethods.updateContractFieldsARG(trigger.new, trigger.old);//JEG 20/10/2017
                        //JAG - 01/03/2018
                        BI_O4_OpportunityMethods.TIWS_Validation (trigger.new, trigger.oldMap, trigger.newMap);

                        if(!NETriggerHelper.getTriggerFired('BI_OpportunityMethods.createTask')){
                            BI_OpportunityMethods.createTask(trigger.new, trigger.old);
                        }
                    }

                    // FAR 01/08/2016 - recalculateParentOpportunityData is replaced by updateOpportunityStage
                    //BI_OpportunityMethods.recalculateParentOpportunityData(trigger.new, trigger.old);
                    if (!System.isFuture() && !System.isQueueable())
                    {
                        BI_O4_OpportunityMethods.unCheckFlagStage(Trigger.new); //JEG FutureMethod Optimization
                        BI_O4_OpportunityMethods.calcParentCountriesAndScope(Trigger.newMap, Trigger.oldMap);
                        BI_O4_OpportunityMethods.summarizeParentEconomicInfoFromChildren(trigger.newMap, trigger.oldMap);
                        BI_O4_OpportunityMethods.updateOrderItemsInvoicingModelUnit(Trigger.new, Trigger.old);
                        if(!bypass.BI_migration__c){
                            BI_O4_OpportunityMethods.checkOppClosedWon(trigger.new, trigger.oldMap);
                        }
                    }

                    // 25-04-2018 - Manuel Ochoa - added condition for batch jobs from BULK
                    if(!System.isFuture() && !System.isBatch() && triggerMap.get('Opportunity Update').On_Off__c == true ){//PLL 21/07/201
                        List<String> idArr = BI_FOCO_OppTriggerHelper.getIdArray(trigger.new);

                        if(idArr.size()> 0)
                           BI_FOCO_OppTriggerHelper.afterUpd(idArr);
                    }

                    BI_OpportunityMethods.changeCasesTeam(trigger.newMap, trigger.oldMap);
                    BI_OpportunityMethods.updateAccountARG(trigger.new, trigger.old);
                    // BI_OpportunityMethods.FillActosComerciales(trigger.New,trigger.old); // OMITIDO POR DUPLICADO El 13/03/2017
                    //BI_OpportunityMethods.updateTasaPresuItemsFields(trigger.new, trigger.old);
                    //BI_O4_OpportunityMethods.generateSalesOperationCases(trigger.new, trigger.oldMap);
                    //BI_O4_OpportunityMethods.updateTdCPinOI(trigger.new, trigger.oldMap); //JEG
                    BI_O4_OpportunityMethods.calculateChilds(Trigger.new, Trigger.old);
                   //AMG 06/07/2016 PARA FVI
                   //BI_OpportunityMethods.crearCasosInternosFVI(trigger.new,trigger.old);
                   //END AMG 06/07/2016 PARA FVI
                    BI_O4_OpportunityMethods.generateCases(trigger.new, trigger.oldMap);
                    //OBP 08/06/2017
                    BI_O4_OpportunityMethods.UpdateAccountabilityParent(trigger.new, trigger.old);
                    /* INI FS_CORE 001 EV-27/06/2017 */ if (!System.isFuture() && ORGConfiguration != null && ORGConfiguration.FS_CORE_Peru__c) BI_OpportunityMethods.fullSyncToFullstack(trigger.old, trigger.new); /* FIN FS_CORE 001 */

                     BI_OpportunityMethods.validarCamposSubsidioyIDSubsidio(trigger.new,trigger.old);//ASD 26/04/2018
                }//JGL 07/08/2016

                if(FVI) // HN 12/08/2016
                {
                    BI_OpportunityMethods.FillActosComerciales(trigger.New,trigger.old);
                }
            }
            else if (trigger.isDelete){
                // Encapsulate all BI meethods
                   BI_SCP_BigDeals_Triggers.deleteOpportunity(trigger.old);

                if(BI){//JGL 07/08/2016

                    // FAR 01/08/2016 - recalculateParentOpportunityData is replaced by updateOpportunityStage
                    //BI_OpportunityMethods.recalculateParentOpportunityData(trigger.old, null);
                	BI_O4_OpportunityMethods.updateOpptyStageDelete(trigger.old);

                    // 25-04-2018 - Manuel Ochoa - added condition for batch jobs from BULK
                    if(!System.isFuture() && !System.isBatch() && triggerMap.get('Opportunity Delete').On_Off__c == true ){//PLL 21/07/201
                        String[] idArr = BI_FOCO_OppTriggerHelper.getIdArray(trigger.old);
                        if(idArr.size()> 0)
                            BI_FOCO_OppTriggerHelper.afterDel(idArr);
                    }
                    BI_O4_OpportunityMethods.calculateChilds(null, Trigger.old);
                    BI_O4_OpportunityMethods.NavPresup(trigger.new,trigger.oldMap);
            	}//JGL 07/08/2016
            }
        }
    }
}