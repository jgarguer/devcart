trigger CallOrder2Asset on Case (after update) 
{

    boolean wasFired            =   NETriggerHelper.getTriggerFired('CallOrder2Asset');

    //GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Case');
    
    if(!isTriggerDisabled){
        if(!wasFired)
        {
            NETriggerHelper.setTriggerFired('CallOrder2Asset');
            
            Id caseOrderManagementRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RTYPE_ORDER_MNGMNT).getRecordTypeId();                

            list<String> listOfOrdersId =   new list<String>();
            for(Case updCase : Trigger.new) 
            {
                if(updCase.Order__c != null)
                    listOfOrdersId.add(updCase.Order__c);
                if(updCase.Asset__c != null)   
                    listOfOrdersId.add(updCase.Asset__c);
            }

            list<String> assetsConfGeneratedId              =   new list<String>();
            list<NE__Order__c> confToUpd                    =   new list<NE__Order__c>();
            list<NE__OrderItem__c> confItemsToUpd           =   new list<NE__OrderItem__c>();
            map<String,NE__Order__c> mapOfConfigurations    =   new map<String,NE__Order__c>([SELECT Owner_Profile__c,NE__OrderStatus__c,NE__Configuration_Type__c,
                                                                                              NE__ServAccId__c, NE__BillAccId__c,
                                                                                                     (SELECT NE__ProdId__r.Name,TGS_Service_status__c, NE__Status__c 
                                                                                                      FROM NE__Order_Items__r)
                                                                                              FROM NE__Order__c 
                                                                                              WHERE Id IN: listOfOrdersId]);
                                                                                              
            boolean sendCis = false;
            for(Case updCase : Trigger.new) 
            {
                try
                {
                    NE__Order__c ord = mapOfConfigurations.get(updCase.Order__c);
                    Boolean profileTGS = NETriggerHelper.getTGSTriggerFired(ord.Owner_Profile__c);
                    
                    if(profileTGS == true && updCase.RecordTypeId == caseOrderManagementRecordTypeId)
                    {
                        case oldCase = Trigger.oldMap.get(updCase.id);
                        system.debug('[trigger.CallOrder2Asset] oldCase.Status: ' + oldCase.Status);
                        system.debug('[trigger.CallOrder2Asset] updCase.Status: ' + updCase.Status);
                        system.debug('[trigger.CallOrder2Asset] updCase.Type: '+updCase.Type);
                        
                        /** Case Status updated to "Resolved" **/
                        if(oldCase.Status != 'Resolved' && updCase.Status == 'Resolved' && updCase.Order__c != null && updCase.Asset__c == null)
                        {
                            // Registration: Create Asset
                            if(updCase.Type == 'New')
                            {
                                system.debug('[trigger.CallOrder2Asset] Call future with order id: ' + updCase.Order__c + ' caseid: ' + updCase.id);
                                String sessionId = UserInfo.getSessionId();
                                FutureCallOrder2Asset.callOrder2Asset(updCase.Order__c, updCase.id, sessionId);
                            }
                        }
                        
                        /** Case Status updated to "Resolved" but Asset already created **/
                        else if(oldCase.Status != 'Resolved' && updCase.Status == 'Resolved' && updCase.Order__c != null && updCase.Asset__c != null)
                        {
                            system.debug('[trigger.CallOrder2Asset] Case Resolved and Asset already created');
                            if(updCase.Type == 'New')
                            {
                                NE__Order__c assetToUpd = mapOfConfigurations.get(updCase.Asset__c);                            
                                for(NE__OrderItem__c assetItem : assetToUpd.NE__Order_Items__r)
                                {
                                    assetItem.NE__Status__c = 'Active';
                                    assetItem.TGS_Service_status__c = 'Received';
                                    confItemsToUpd.add(assetItem);
                                }
                                assetToUpd.NE__OrderStatus__c = 'In progress';
                                confToUpd.add(assetToUpd);
                            }
                        }
                        
                        /** Case Status updated to "Closed" **/
                        else if(oldCase.Status != 'Closed' && updCase.Status == 'Closed')
                        {
                            // Modification and Termination: Create Asset 
                            if(updCase.Type == 'Change' || updCase.Type == 'Disconnect')
                            {
                                system.debug('[trigger.CallOrder2Asset] Call future with order id: ' + updCase.Order__c + ' caseid: ' + updCase.id);
                                String sessionId = UserInfo.getSessionId();
                                FutureCallOrder2Asset.callOrder2Asset(updCase.Order__c, updCase.id, sessionId);
                            }
                            else
                            {
                                NE__Order__c assetToUpd = mapOfConfigurations.get(updCase.Asset__c);
                                assetToUpd.NE__OrderStatus__c = 'Active';
                                confToUpd.add(assetToUpd);
                                
                                for(NE__OrderItem__c assetItem : assetToUpd.NE__Order_Items__r)
                                {
                                    assetItem.TGS_Service_status__c = 'Deployed';
                                    confItemsToUpd.add(assetItem);
                                }
                                sendCis = true; 
                            }
                            
                            NE__Order__c ordToUpdate        =   mapOfConfigurations.get(updCase.Order__c);
                            ordToUpdate.NE__OrderStatus__c  =   'Completed';
                            update ordToUpdate;
                            for(NE__OrderItem__c orderItem : ordToUpdate.NE__Order_Items__r)
                            {
                                orderItem.NE__Service_Account__c = ordToUpdate.NE__ServAccId__c;
                                orderItem.NE__Billing_Account__c = ordToUpdate.NE__BillAccId__c;
                                confItemsToUpd.add(orderItem);
                            }
                        }
                        
                        /** Case reopened **/
                        else if(oldCase.Status == 'Resolved' && updCase.Status != 'Resolved' && updCase.Status != 'Closed' && updCase.Status != 'Cancelled' && updCase.Status != 'Rejected' && updCase.Order__c != null && updCase.Asset__c != null)
                        {
                            NE__Order__c assetConf = mapOfConfigurations.get(updCase.Asset__c);

                            for(NE__OrderItem__c assetItem : assetConf.NE__Order_Items__r)
                            {
                                assetItem.TGS_Service_status__c = 'Disposed';
                                confItemsToUpd.add(assetItem);
                            }
                            sendCis = true;
                        }
                        
                        /** Case Status updated to "Cancelled" or "Rejected" **/
                        else if((oldCase.Status != 'Cancelled' && oldCase.Status != 'Rejected') && (updCase.Status == 'Cancelled' || updCase.Status == 'Rejected'))
                        {
                            // Cancel the order (and its order items), so that the asset can be changed/disconnected again.
                            NE.OrderSummaryButtonExtension.cancelButton(updCase.Order__c);
                            
                            if(updCase.Asset__c != null && updCase.Status == 'Cancelled')
                            {
                                NE__Order__c    caseAssetConf           =       mapOfConfigurations.get(updCase.Asset__c);

                                // Registration Case
                                if(updCase.Type == 'New')
                                {
                                    for(NE__OrderItem__c assetItem : caseAssetConf.NE__Order_Items__r)
                                    {
                                        assetItem.TGS_Service_status__c = 'Disposed';
                                    }
                                    confItemsToUpd.addAll(caseAssetConf.NE__Order_Items__r);
                                    sendCis = true;
                                }
                                // Modification or Termination Case
                                else
                                {
                                    for(NE__OrderItem__c assetItem : caseAssetConf.NE__Order_Items__r)
                                    {
                                        assetItem.TGS_Service_status__c = 'Deployed';
                                    }
                                    confItemsToUpd.addAll(caseAssetConf.NE__Order_Items__r);
                                    sendCis = true;
                                    caseAssetConf.NE__OrderStatus__c = 'Active';
                                    confToUpd.add(caseAssetConf);
                                }
                            }
                        }
                        
                    }
                    
                }
                catch(Exception e){}
            }
            
            system.debug('[trigger.CallOrder2Asset] Configuration Items to update: '+confItemsToUpd);
            
            if(confItemsToUpd.size() > 0){
                TGS_CallRodWs.inFutureContextCI = true;
                update confItemsToUpd;
                TGS_CallRodWs.inFutureContextCI = false;
                if (sendCis){
                    /* Sends Integration with in ROD */
                    Set<Id> confitemsIds = new Set<Id>();
                    for(NE__OrderItem__c ci: confItemsToUpd){
                        
                        confitemsIds.add(ci.Id);
                    }
                    TGS_CallRoDWS.invokeWebServiceRODOrderItem(confitemsIds, true);            
                }
            }
            if(confToUpd.size() > 0)
                update confToUpd;
        }
    }
}