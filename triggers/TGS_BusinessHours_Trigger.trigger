/*------------------------------------------------------------
	Author:         Miguel Angel Galindo 
	Company:        Deloitte
	Description:    When an Business Hours is created this trigger update the hours.
	History
	<Date>          <Author>        		<Change Description>
	09-Dic-2014     Miguel Angel Galindo	Initial Version
	10-Mar-2015     Miguel Angel Galindo	Change encapsulation method
	18/03/2015		Marta Garcia			TGS Encapsulation 
	20/07/2017      Guillermo Muñoz     	Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
  trigger TGS_BusinessHours_Trigger on TGS_Business_Hours__c (before insert, before update) {
	TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('TGS_Business_Hours__c');
    
    if(!isTriggerDisabled){
		if(TGS){    
		    
			new TGS_BusinessHoursUpdate(Trigger.new);
		}
	}
}