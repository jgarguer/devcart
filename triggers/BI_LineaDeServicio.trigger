trigger BI_LineaDeServicio on BI_Linea_de_Servicio__c (before insert, after insert, after update, before delete) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Trigger on BI_Linea_de_Servicio__c
    
    History:
    
    <Date>			  <Author> 	        <Description>
    25/02/2015        Ignacio Llorca     Initial version
    04/02/2016        Guillermo Mu�oz    Method "BI_LineaDeServicioMethods.substractLine" added   
	20/07/2017        Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Linea_de_Servicio__c');
    
    if(!isTriggerDisabled){
		if (trigger.isBefore){
			if (trigger.isInsert){
				//BI_LineaDeServicioMethods.insertLineaDeServicioIMP(trigger.new);
				BI_LineaDeServicioMethods.insertLineaDeServicioIMP(trigger.new);
			}
			/*else if (trigger.isDelete){
				//BI_LineaDeRecambioMethods.eliminarLineaDeServicio(trigger.old);
			}*/
			if(trigger.isDelete){
				BI_LineaDeServicioMethods.substractLine(trigger.new, trigger.old);
			}
		}
		else if (trigger.isAfter){
			if (trigger.isInsert){
				BI_LineaDeServicioMethods.countRequests(trigger.new);
				BI_LineaDeServicioMethods.ValidateMass(trigger.newMap);
			}else if(trigger.isUpdate){
				BI_LineaDeServicioMethods.updateSol(trigger.new, trigger.old);
				BI_LineaDeServicioMethods.validateBackOffice(trigger.new, trigger.old);
			}
		}
	}
}