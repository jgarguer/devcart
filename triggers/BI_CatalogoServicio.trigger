trigger BI_CatalogoServicio on BI_Catalogo_de_servicio__c (before insert, before update, before delete) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by the BI_Facturaccion trigger.
    
    History

    <Date>            <Author>          <Description>
    09/03/2015      Ignacio Llorca      Initial version
    01/01/2015		Fernando Arteaga	Added validatePlanServiceDuplicated method
    20/07/2017      Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Catalogo_de_servicio__c');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore){
            if (trigger.isInsert){
            	BI_CatalogoServicioMethods.blockEdition(trigger.new);
            	BI_CatalogoServicioMethods.validatePlan(trigger.new);
            	// FAR 01/04/2015
            	BI_CatalogoServicioMethods.validatePlanServiceDuplicated(trigger.new);
            } 
            if (trigger.isUpdate){
            	BI_CatalogoServicioMethods.blockEdition(trigger.new);
            }
            if (trigger.isDelete){
            	BI_CatalogoServicioMethods.blockDelete(trigger.old);
            }
        }else {  
            /*if (trigger.isInsert){ 
                
            }   
            else if (trigger.isUpdate){
            	
            } 
            else if (trigger.isDelete){
            } 
            */
        }
    }
}