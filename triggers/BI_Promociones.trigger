trigger BI_Promociones on BI_Promociones__c (after undelete, before delete, before update, before insert) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Trigger on BI_Promociones__c
    
    History:
    
    <Date>            <Author>              <Description>
    24/04/2014        Ignacio Llorca        Initial version
    07/10/2014        Pablo Oliva	        Method "changeCurrency" added
    20/07/2017        Guillermo Muñoz       Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Promociones__c');
    
    if(!isTriggerDisabled){
        if(trigger.isBefore){
            if(trigger.isInsert){
            	BI_PromocionesMethods.changeCurrencyAndValidateEconomic(trigger.new, trigger.old);
            }   
            else if(trigger.isUpdate){
                BI_PromocionesMethods.preventUpdatePromotion(trigger.old, trigger.new);
                BI_PromocionesMethods.changeCurrencyAndValidateEconomic(trigger.new, trigger.old);
            }
            else if(trigger.isDelete){
                BI_PromocionesMethods.preventDeletePromotion(trigger.old);
            }
        }
        else{
            if(trigger.isAfter){ 
                if(trigger.isInsert){ 
                }
                //else 
               // if(trigger.isUpdate){
               // }
                //else if(trigger.isDelete){
                //}
                else if(trigger.isUnDelete){
                    BI_PromocionesMethods.preventUndelete(trigger.new);
                }
            }         
        }
    }
}