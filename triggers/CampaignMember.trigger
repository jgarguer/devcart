trigger CampaignMember on CampaignMember (before insert, after insert, before update, after update, before delete) {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Trigger on CampaignMember
    
    History:
    
    <Date>			  <Author> 	        <Description>
    14/04/2014        Pablo Oliva     	Initial version
    30/04/2014		  Pablo Oliva		Change: Method checkManager deleted
    13/11/2014		  Pablo Oliva		Method "manageCampanasCuenta" added
    20/07/2017        Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('CampaignMember');
    
    if(!isTriggerDisabled){
        if(trigger.isBefore){
            if(trigger.isInsert){
            	BI_CampaignMemberMethods.preventStatusBlockRecord(trigger.new);
            }else if(trigger.isUpdate){ 
            	BI_CampaignMemberMethods.preventUpdate(trigger.new, trigger.old);
            }else if(trigger.isDelete){
            	BI_CampaignMemberMethods.preventDelete(trigger.oldMap);
            	BI_CampaignMemberMethods.manageCampanasCuenta(trigger.old, 1);
            }
            //else if(trigger.isUnDelete){
            //}
        }else{ 
            if(trigger.isInsert){ 
            	BI_CampaignMemberMethods.sendToApproval(trigger.new);
            	BI_CampaignMemberMethods.manageCampanasCuenta(trigger.new, 0);
                BI_CampaignMemberMethods.createTaskPreOpp(trigger.new, trigger.old);
            }
            //else if(trigger.isUpdate){
            	//BI_CampaignMemberMethods.createTaskPreOpp(trigger.new, trigger.old);
            //}
            //else if(trigger.isDelete){
            //}
            //else if(trigger.isUnDelete){
            //}         
        }
    }
}