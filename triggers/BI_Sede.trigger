/*-------------------------------------------------------------------------------
    Author:        Unknown
    Company:       Unknown
    Description:   trigger BI_Sede
    Test Class:    ...
    
    History:
     
    <Date>                  <Author>                <Change Description>
    Unknown                 Unknown                 Initial Version
    07/08/2016              Jorge Galindo           Encapsulate all BI functionality to avoid launching when is not a BI user
    17/09/2016              Alfonso Alvarez         Corregir el código de Jorge Galindo y Humberto Nunes, que parece que no se leen el código, y no saben que los triggers hay que encapsularlos con bypass.BI_migration__c
    20/07/2017              Guillermo Muñoz         Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    30/01/2018              Álvaro López            Added updateIsoCode method in after update
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
trigger BI_Sede on BI_Sede__c (before insert, before update, before delete, after insert, after update)
{
    BI_bypass__c bypass = BI_bypass__c.getInstance();
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c; // HN 12/08/2016
    Boolean TGS = userTGS.TGS_Is_TGS__c; // Álvaro López 30/01/2018

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Sede__c');
    
    if(!isTriggerDisabled){

        List<user> lstuser=[select Id, BI_Permisos__c,Pais__c from User where BI_Permisos__c='Sucursales' limit 1];
        User user=new User();
        if(lstuser.size()>0)
        {
            user=lstuser.get(0);
        }

    System.debug('-->'+user+'--->'+BI+'--->'+bypass.BI_migration__c);
    if(trigger.isAfter && !trigger.isDelete && trigger.new.size()<1000 && !system.isFuture() && !bypass.BI_migration__c && BI)
    {
        System.debug('\n\n Ingresa por aca ');
        BI_COL_Street_Addr_cls.EjecutaCallejero(BI_COL_Street_Addr_cls.fnGetIDsDireccion(trigger.new));
    }
    
    if(trigger.isDelete && !bypass.BI_migration__c && BI)
    {
        BI_COL_Street_Addr_cls.validateDelete(trigger.old);  
    }
            
    BI_COL_SucursalesValidacion_cls ctrValidacion=new BI_COL_SucursalesValidacion_cls(trigger.isDelete);
    BI_COL_FlagforTGRS_cls.flagTriggerTRSCuenta=false;
    


    set<String> direcciones=new set<String>();
        if(!trigger.isDelete && !bypass.BI_migration__c && trigger.isBefore)
        {
                for(BI_Sede__c suc:trigger.new)
                {
                    if(!direcciones.contains(suc.BI_Direccion__c))
                    {
                        direcciones.add(suc.BI_Direccion__c);
                    }else
                    {
                        suc.BI_COL_Estado_callejero__c='Dirección duplicada';
                    }           
                }
        }

        if(trigger.isUpdate && !system.isFuture() && trigger.isBefore && !bypass.BI_migration__c) 
            ctrValidacion.modificaEstadoCallejero(trigger.new, trigger.old);

            System.debug('--->'+system.Userinfo.getUserId()+'--->'+user.Id);
            
        if(user!=null && system.Userinfo.getUserId()!=user.Id && !bypass.BI_migration__c && BI)
        {
                ctrValidacion.isDelete=trigger.isDelete;
                if(trigger.isUpdate)
                {               
                    ctrValidacion.lstSucursal=trigger.new;
                    ctrValidacion.validaClientesSucursal();
                    //JSJ - 31.07.2015 
                    //Se comenta está linea porque usa código obsoleto en la clase de producción
                    //ctrValidacion.validaClientesSucursal();
                    //Fin JSJ
                }
                
        }

    //Add by Micah Burgos to send info to TRS about BI_Punto_de_instalación. [22/12/2015]
    if(trigger.isAfter && trigger.isUpdate && !system.isFuture() && !bypass.BI_migration__c && BI)
    {
        system.debug('·····' + userTGS.TGS_Is_BI_EN__c);

        BI_SedeMethods.updatePuntoInstalacionTRS(trigger.new);
        /*
        if(!System.isBatch() && Trigger.size < 1000){
            
            BI_SedeMethods.crearSedeTelefonica(trigger.new,trigger.old, 'Actualizar' );
        }*/
    }

    //Add Fco.Javier Sanz [15/04/2016]
    if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
        //JGL 07/08/2016
        if((BI || FVI) && !bypass.BI_migration__c) // HN 12/08/2016 // AJAE 17/09/2016 // FIN AJAE 17/09/2016
        {
            BI_SedeMethods.updateUsershippingAddress(trigger.newmap);
        }// END JGL 07/08/2016
        
    }
    /*Álvaro López 30/01/2018 - Added updateIsoCode method in after update*/
    if(trigger.isAfter){
        if(trigger.isUpdate){
            if(TGS){
                TGS_SiteMethods.updateIsoCode(Trigger.new);
            }
        }
    }
    
    //add by Angelo Salamanca[09/12/2016]
    if(trigger.isBefore && trigger.isUpdate){
        BI_SedeMethods.updateStatus(trigger.new,trigger.old);
    }
    }
}