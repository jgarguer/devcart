/************************************************************************************
 * Author: JAvier Lopez Andradas
 * Company: gCTIO
 * Description: TRigger for object FO_Forecast__c
 * 
 * 
 * <date>		<version>		<description>
 * 02/04/2018	1.0				Initial
 * 
 * ***************************************************************************************/
trigger FO_Forecast on FO_Forecast__c (before insert,after insert,before Update) {
    if(trigger.isAfter){
        if(trigger.isInsert){
            FO_ForecastMethods.changeRT(trigger.New);
            FO_ForecastMethods.createForecastFutur(trigger.New);
            FO_ForecastMethods.creaForecastItems(trigger.New);
            
        }
        
    }else{
        //Is Before
        if(trigger.isInsert){
            FO_ForecastMethods.validateForecast(trigger.New);
        }
        if(trigger.isUpdate){
            FO_ForecastMethods.updateForecastCommitUpside(trigger.New,trigger.Old);
        }
    }
}