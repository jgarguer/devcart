trigger BI_FVI_Nodos_Objetivos on BI_FVI_Nodos_Objetivos__c (after insert,before insert) {

    //GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_FVI_Nodos_Objetivos__c');
    
    if(!isTriggerDisabled){
		if(trigger.isAfter){
			if(trigger.isInsert){

				system.debug('\nAFTER INSERT:  ');
				BI_FVI_NodosObjetivosMethods.generaNodo_Objetivo_Desempeno(trigger.new, trigger.old);
			}
		}

		if(trigger.isBefore){
			if(trigger.isInsert){
				BI_FVI_NodosObjetivosMethods.asignaNiveles(trigger.new, trigger.old);
			}	
		}
	}
}