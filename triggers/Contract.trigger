trigger Contract on Contract (before insert, before update, after update) 
{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Diego Arenas
    Company:       Salesforce.com
    Description:   Trigger para controlar los métodos llamados de Contract
    History
    <11/02/2014>      <Diego Arenas>     <Añadir la llamada a UtilTriggers.ContractTrigger en before insert/update>
    <17/05/2016>      <Humberto Nunes>   <Añadir la llamada a UtilTriggers.ContractTrigger en before insert/update>
    <01/06/2016>      <Alvaro Sevilla>   <Arreglo de la estructura del trigger y se añadió invocación a clase BI_FVI_CasosContratosFVI>
    <06/06/2016>      <Humberto Nunes>   <Arreglo de la estructura del trigger y se añadió invocación a clase BI_ContractMethods.CreateRegistroDesemp>
    <08/06/2016>    <Antonio Masferrer>  <Añadir la llamada a BI_ContractMethods.zytrust>
    <06/10/2016>    <Patricia Castillo>  <Added call BI_ContractMethods.validateUserModifierARG y updateMultipicklistFromRelatedOptyARG (D321)>
    20/07/2017      Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    BI_bypass__c bypass = BI_bypass__c.getInstance();
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c;
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Contract');
    
    if(!isTriggerDisabled){
        if (trigger.isAfter){
 /* GD_28/06/2017-Marta Glez*/ if(trigger.isInsert) BI_ContractMethods.validateUserModifierARG(trigger.newMap, null); /* GD_FIN 28/06/2017-Marta Glez*/
            if(trigger.isUpdate){

                BI_ContractMethods.moverNCP(trigger.new , trigger.old);

                BI_ContractMethods.zytrust(trigger.new);
            
                BI_FVI_CasosContratosFVI.BI_FVI_CrearCasosInternos(trigger.new,trigger.old); 

                BI_ContractMethods.CreateRegistroDesemp(trigger.new,trigger.old); 


            }       
        
        }       
    
        if (trigger.isBefore){

            if (trigger.isInsert){
                /*Existen 2 tipos de contratos: Nuestros contratos y los contratos del cliente con la competencia, esta diferenciación se hace a través
                    del campo "isCompetitorContract__c”. Este trigger verifica que a la hora de crear o modificar un contrato no exista alguno marcado como 
                    contrato de la competencia sin tener cargado el campo “competitorName__c”.*/
                Certa_SCP.UtilTriggers.ContractTrigger(trigger.New);
                BI_ContractMethods.assignCurrency(trigger.new);
                if(BI && !bypass.BI_Migration__c) {
                    BI_ContractMethods.updateMultipicklistFromRelatedOptyARG(trigger.new);
                }
            
            
            }
            if (trigger.isUpdate){
                if(BI && !bypass.BI_Migration__c) {
                    BI_ContractMethods.validateUserModifierARG(trigger.newMap, trigger.oldMap);
                }
                /*Existen 2 tipos de contratos: Nuestros contratos y los contratos del cliente con la competencia, esta diferenciación se hace a través
                    del campo "isCompetitorContract__c”. Este trigger verifica que a la hora de crear o modificar un contrato no exista alguno marcado como 
                    contrato de la competencia sin tener cargado el campo “competitorName__c”.*/
                Certa_SCP.UtilTriggers.ContractTrigger(trigger.New);
            }

            BI_ContractMethods.FiltroOpp(trigger.new);
        
        }
    }
}