trigger Milestone1_Task on Milestone1_Task__c (after insert, after update, before insert, before update) {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Trigger  for controlling Milestone1_Task__c Methods
    History
    <29/09/2014>      <Ignacio Llorca>         Initial Version
    15/05/2017        Cristina Rodríguez       Solve Multiple Trigger On same sObject (merge with old Milestone1_Task_Trigger)
    20/07/2017        Guillermo Muñoz          Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Milestone1_Task__c');
    
    if(!isTriggerDisabled){    
        if (trigger.isBefore){
            if (trigger.isInsert){
            	//BI_Milestone1TaskMethods.createTaskProject(trigger.new);
            }
            if (trigger.isInsert || trigger.isUpdate){
                Milestone1_Task_Trigger_Utility.handleTaskBeforeTrigger(trigger.new); 
            }        
            if (trigger.isUpdate){
                //Milestone1_Task_Trigger_Utility.updateDuracion(trigger.new, trigger.oldMap); //JRM
            	if(!NETriggerHelper.getTriggerFired('BI_Milestone1TaskMethods.updateProjectTask'))
            		BI_Milestone1TaskMethods.updateTaskProjectStatus(trigger.new, trigger.old);
            }
            //if (trigger.isDelete){        
            //}
        }
        else { 
            if (trigger.isInsert){ 
            	BI_Milestone1TaskMethods.createTaskProject(trigger.new);
            	//BI_Milestone1TaskMethods.updateAssignedTask(trigger.new);
            }        
            //else if (trigger.isDelete){
            //} 
            //else if (trigger.isUpdate){
            //}       
        //}
            if(Trigger.isUpdate){
                    //shift Dates of successor Tasks if Task Due Date is shifted
                    Milestone1_Task_Trigger_Utility.checkSuccessorDependencies(trigger.oldMap, trigger.newMap);
            }
            if (trigger.isInsert || trigger.isUpdate){
                Milestone1_Task_Trigger_Utility.handleTaskAfterTrigger(trigger.new,trigger.old);
            }
        }
    }
}