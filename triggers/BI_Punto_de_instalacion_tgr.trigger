/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Trigger Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-03-24      Manuel Esthiben Mendez Devia (MEMD)     Cloned Trigger
*            1.1    2015-10-29      Jose Leonardo Lopez                     Added After delete and integrate TGS_Site_Trigger with encapsulation
*            1.2    2016-03-09      Jose Miguel Fierro                      Added methods to auto-fill searchable information
*            1.3    2016-06-22      José Luis gonzález Beltrán              Communicate Sede's Contact to RoD
*            1.4    07/08/2016      Jorge Galindo                           Encapsulate all BI functionality to avoid launching when is not a BI user
*            1.5    12/08/2016      Humberto Nunes                          Encapsulate all FVI functionality
*            1.6    31/01/2017      Alfonso Alvarez (AJAE)                  Modificación de Encapsulado para FVI y Go4Clients
*            1.7    20/07/2017      Guillermo Muñoz                         Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
*            1.8    30/01/2018      Álvaro López                            Added setIsoCode method in before insert
*************************************************************************************************************/
trigger BI_Punto_de_instalacion_tgr on BI_Punto_de_instalacion__c (before insert, before update, before delete, after insert, after update, after delete) {
    List<String> lstIdSucursales = new List<String>();
    List<BI_Punto_de_instalacion__c> lstSucursales = new List<BI_Punto_de_instalacion__c>();
    String objeto = '';
    //system.debug('\n Lista Sucursales ------>>>' + lstSucursales); // JMF: I don't think this wil be very informative at this point...
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c; // HN 12/08/2016

    BI_bypass__c bypass = BI_bypass__c.getInstance();
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Punto_de_instalacion__c');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore) {
            if (trigger.isBefore && trigger.isInsert && !bypass.BI_migration__c && (BI || FVI)) // HN 12/08/2016
            {
                BI_COL_CreateBranchWS_cls.Validate_Addressduplicate(trigger.new, trigger.old);
            }
            
            if(Trigger.isInsert && !bypass.BI_skip_trigger__c && !bypass.BI_migration__c) {
                if(TGS) {
                    TGS_SitesIntegration.addSearchInformation(Trigger.new);
                    TGS_SiteMethods.setIsoCode(Trigger.new); // Álvaro López 30/01/2018
                }
            }
            if(Trigger.isUpdate && !bypass.BI_skip_trigger__c && !bypass.BI_migration__c) {
                if(TGS) {
                    TGS_SitesIntegration.addSearchInformation(Trigger.new);
                }
            }
            if(Trigger.isDelete && !bypass.BI_skip_trigger__c && !bypass.BI_migration__c){
                if(BI){
                    BI_Punto_de_InstalacionMethods.prevenirBorradoSede(trigger.oldMap);          
                }
            }
            
        } else if (trigger.isAfter) {
            
            if (Trigger.isInsert) {
                if (TGS) {
                    System.debug('TGS_SiteTrigger: NEW SITE');
                    TGS_SitesIntegration.createSites(Trigger.newMap.keySet());
                }
                // INI adaptacion UNICA
                BIIN_UNICA_SiteMethods.updateRoDContact(trigger.new, trigger.old);
                // FIN adaptacion UNICA
            }
            
            if (Trigger.isUpdate ) {
                if (TGS && TGS_SitesIntegration.inFutureContextSite == false) {
                    Map<Id, String> oldNameSites = new Map<Id, String>();
                    for (BI_Punto_de_instalacion__c oldSite : Trigger.old) {
                        oldNameSites.put(oldSite.ID, oldSite.Name);
                    }
                    
                    System.debug('TGS_SiteTrigger: UPDATE SITE');
                    TGS_SitesIntegration.updateSites(Trigger.newMap.keySet(), oldNameSites);
                }
                // INI adaptacion UNICA
                BIIN_UNICA_SiteMethods.updateRoDContact(trigger.new, trigger.old);
                // FIN adaptacion UNICA
            }
            
            if (Trigger.isDelete) {
                if (TGS) {
                    System.debug('TGS_SiteTrigger: DELETE SITE');
                    TGS_SitesIntegration.deleteSites(Trigger.old);
                }
            }
            //if(!Trigger.isDelete){// Se estaba ejecutando after insert, after update al agregar after delete se ejecutaria tambien en ese momento
            //CAMBIAR
            if (!System.isFuture() && !System.isBatch() && !bypass.BI_migration__c && BI) {
                if (Trigger.size < 1000) {
                    lstSucursales = (trigger.isDelete) ? trigger.old : trigger.new;
                    for (BI_Punto_de_instalacion__c sucursal : lstSucursales) {
                        system.debug('\n sucursal.BI_Sede__r.BI_COL_Estado_callejero__c------>>>>' + sucursal.BI_Sede__r.BI_COL_Estado_callejero__c);
                        system.debug('\n sucursal.BI_Sede__r.BI_COL_Sucursal_en_uso__c------>>>>' + sucursal.BI_Sede__r.BI_COL_Sucursal_en_uso__c);
                        system.debug('\n valores========>>>>>' + lstSucursales);
                        
                        lstIdSucursales.add(sucursal.Id);
                        system.debug('\n Agregando la sucursal----------->>>>>' + sucursal.Id);
                        system.debug('\n lstIdSucursales----------->>>>>' + lstIdSucursales);
                        
                        
                    }
                    system.debug('\n lstIdSucursales========>>>>>' + lstIdSucursales);
                    if (lstIdSucursales != null && lstIdSucursales.size() > 0 && !Test.isRunningTest() ) {
                        String accion = trigger.isInsert ? 'Insertar' : trigger.isUpdate ? 'Actualizar' : 'Eliminar';
                        system.debug('-------------->ingresa al envio de las sucursales accion= ' + accion + '  ' + lstIdSucursales);
                        BI_COL_CreateBranchWS_cls.crearSucursalTelefonica(lstIdSucursales, accion);
                    }
                }
            }

            if (trigger.isInsert || trigger.isUpdate) {
                //JGL 07/08/2016
                
                //Inicio AJAE 31/01/2017
                if(BI || !FVI)  {
                  BI_G4C_BI_Sede_Methods.shippingAddressToDirComercial(trigger.new);
                }
                if(FVI) // HN 12/08/2016
                {
                  BI_Punto_de_InstalacionMethods.updateUsershippingAddressFromSede(trigger.new,trigger.old);
                }//end JGL 07/08/2016
                //Fin AJAE 31/01/2017
               
            }
        }
    }
}