trigger BI_COL_PreFactibilidad_tgr on BI_COL_PreFactibilidad__c (before insert, before update, after insert) // 
{
    //GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_COL_PreFactibilidad__c');
    
    if(!isTriggerDisabled){
        if(!Test.isRunningTest() && trigger.isInsert && trigger.isAfter)
        {
            List<Approval.ProcessRequest> reqs = new List<Approval.ProcessRequest>(); //listado de peticiones a aprobar 
            Approval.ProcessSubmitRequest aprobacion=new Approval.ProcessSubmitRequest(); //solicitud de aprobación
            aprobacion.setComments('Registro enviado para aprobación');
            aprobacion.setObjectId(trigger.new[0].id);
            reqs.add(aprobacion);
            try{
                
                    List<Approval.ProcessResult> result = Approval.process(reqs);
                
            }
            catch(Exception e)
                {
                System.debug('------------no es posible enviar a aprobación-->'+e);
                }
        } 

         // la clase de prueba de esta clase es [cup_calcular_complejidad_MS_tst]    
        System.debug('\n\n trigger.new[0].Enviar_Email__c= '+trigger.new[0].BI_COL_Enviar_email__c+
                    ' trigger.new[0].Enviar_Email_Cial__c; '+ trigger.new[0].BI_COL_Enviar_email_cial__c+
                    ' trigger.new[0].Destinatarios_Email__c '+trigger.new[0].BI_COL_Destinatarios_email__c+' \n\n');
            
        if(trigger.new.size() == 1 && trigger.isBefore && trigger.isUpdate)
        {
            if((trigger.new[0].BI_COL_Enviar_email__c == true || trigger.new[0].BI_COL_Enviar_email_cial__c == true))
            {
                System.debug('\n\n Ingresa al envio del corre  trigger.isBefore: '+trigger.isBefore+' \n\n');
                BI_COL_SendingEmailPrefeasibility_cls.envioMail_mtd(trigger.new[0]);
            }
            else
            {
                trigger.new[0].BI_COL_Enviar_email__c = false;
                trigger.new[0].BI_COL_Enviar_email_cial__c = false;
            }
                
        }
    }
}