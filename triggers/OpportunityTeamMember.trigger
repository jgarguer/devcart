trigger OpportunityTeamMember on OpportunityTeamMember (after insert, after update, after delete)
{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Trigger to execute methods in OpportunityTeamMember records
    
    History:
    
    <Date>            <Author>              <Description>
    30/08/2016        Fernando Arteaga      Initial version
    20/07/2017        Guillermo Muñoz     	Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    TGS_User_Org__c userOrg = TGS_User_Org__c.getInstance();

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('OpportunityTeamMember');
    
    if(!isTriggerDisabled){	
	    if (userOrg.TGS_Is_BI_EN__c || userOrg.TGS_Is_Admin__c)
	    {
	    	/*
			if (Trigger.isBefore)
		    {
		    	
		    }
		    */
			if (Trigger.isAfter)
			{
		    	if (Trigger.isInsert)
		    	{
					BI_O4_OpportunityTeamMemberMethods.updateQuotesWithOpptyTeamMembers(Trigger.new, null);
		    	}
		    	if (Trigger.isUpdate)
		    	{
		    		BI_O4_OpportunityTeamMemberMethods.updateQuotesWithOpptyTeamMembers(Trigger.new, Trigger.oldMap);
		    	}
		    	if (Trigger.isDelete)
		    	{
		    		BI_O4_OpportunityTeamMemberMethods.updateQuotesWithOpptyTeamMembersDelete(Trigger.old);
		    	}
			}
	    }
	}
}