/*-------------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       New Energy Aborda
    Description:   class for sharing object nodos
    Test Class:    BI_FVI_NodosShare_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    09/06/2016              Geraldine P. Montes     Initial Version
    17/06/2016              Alvaro sevilla          Added call to ColaborarNodosNAB method
	07/08/2016		  		Jorge Galindo			Encapsulate all BI functionality to avoid launching when is not a BI user
    12/08/2016              Humberto Nunes          Se a agregdo la variable Boolean FVI = userTGS.BI_FVI_Is_FVI__c y se ha usaado en el After Insert 
    16/09/2016              Humberto Nunes          Se hizo que la encapsulacion fuese para FVI o BI
    20/07/2017              Guillermo Muñoz         Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    20/04/2018              Javier López            Added method changeOwnerActivateNode
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
trigger BI_FVI_Nodos on BI_FVI_Nodos__c (after update, after insert) 
{
    // JGL 07/08/2016
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c; // HN 12/08/2016
    // END JGL 07/08/2016

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_FVI_Nodos__c');
    
    if(!isTriggerDisabled){
     
        if(trigger.isAfter)
        {
            if(trigger.isUpdate)
            {
                if(FVI || BI) // HN 12/08/2016 & 16/09/2016
                {// JGL 07/08/2016
                    BI_FVI_NodosMethods.NAGtoNAV(trigger.new, trigger.old);
                    BI_FVI_NodosMethods.ColaborarNodosNAB(trigger.new, trigger.old);  
                    BI_FVI_NodosMethods.changeOwnerAccounts(trigger.new, trigger.old);  
                    BI_FVI_NodosMethods.changeOwnerActivateNode(trigger.new,trigger.old);
                }// END JGL 07/08/2016
            }

            if (trigger.isInsert)
            {
                if(FVI || BI) // HN 12/08/2016 & 16/09/2016
                {// JGL 07/08/2016
                    BI_FVI_NodosMethods.NodoShare(trigger.new);
                    BI_FVI_NodosMethods.ColaborarNodosNAB(trigger.new, trigger.old);
                }// END JGL 07/08/2016
            }
        }
        /*else // trigger.isBefore
        {

        }*/
    }   
}