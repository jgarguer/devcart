trigger TGS_OrderItemAttributes on NE__Order_Item_Attribute__c (before insert,before update,after insert, after update) {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Álvaro López
    Company:       Accenture
    Description:   Trigger on NE__Order_Item_Attribute__c
    History:
    
    <Date>            <Author>             				<Description>
    23/02/2017        Álvaro López   					Initial version
	29/05/2017        Ana Alcoceba - Manuel Ochoa       Added before insert to insert NE__AttrEnterpriseId__c in new OIAs
	19/07/2017        Manuel Ochoa                      Added before update to fix value changes when OIAs come from bulk
    20/07/2017        Guillermo Muñoz                   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
	28/03/2018		   José Manuel García				Added call to originTocase method
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	BI_bypass__c bypass = BI_bypass__c.getInstance();
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c;
    system.debug('#Entrando en TGS_OrderItemAttributes ');


    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('NE__Order_Item_Attribute__c');
    
    if(!isTriggerDisabled){
        if (trigger.isAfter){
            if(trigger.isInsert){
                if(TGS){
                    TGS_NEOrderItemMethods.validateMSISDN(trigger.new);
                }
            }
        	if(trigger.isUpdate){
        		if(TGS){
        			TGS_NEOrderItemMethods.validateMSISDN(trigger.new);
        		}
        	}
        }
        
        if(trigger.isBefore){
            if(trigger.isInsert){  
                if(TGS){
                    TGS_NEOrderItemMethods.bulkAttEnterpriseId(trigger.new);
                }   
            }
            if(trigger.isUpdate){  
                if(TGS){            
                    TGS_NEOrderItemMethods.fixOIAValuesFromBulk(trigger.new,trigger.old);
                }   
            }
    	}
    }
}