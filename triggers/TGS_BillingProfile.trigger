trigger TGS_BillingProfile on NE__Billing_Profile__c (before insert, after update, before update, after insert) {
    
    BI_bypass__c bypass = BI_bypass__c.getInstance();
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    
    /*if(trigger.isBefore){
        if(trigger.isInsert){
        }
        
        if(trigger.isUpdate){
        }
        
        if(trigger.isDelete){
        }
        
    }*/
    
    //GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('NE__Billing_Profile__c');
    
    if(!isTriggerDisabled){
        if(trigger.isAfter){
            /*if(trigger.isInsert){
            }*/
            
            if(trigger.isUpdate){
                if(TGS){
                    TGS_BillingProfile_Handler.fillLastModifiedFD(Trigger.newMap.keySet());
                }
            }
            
            /*if(trigger.isDelete){
            }*/
        }
    }
}