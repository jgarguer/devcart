trigger BI_ISC2 on BI_ISC2__c (after insert, after update, before insert, before update) {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Trigger on BI_ISC2__c
    
    History:
    
    <Date>            <Author>          <Description>
    04/07/2014        Pablo Oliva       Initial version
    02/02/2015        Pablo Oliva       method added
    14/09/2016        Miguel Cabrera    BI_ISC2Methods.createPlandeAccion call method added
    20/07/2017        Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_ISC2__c');
    
    if(!isTriggerDisabled){
        if(trigger.isBefore){
            if(trigger.isInsert){
            	BI_ISC2Methods.updateAverage(trigger.new);
            }else if(trigger.isUpdate){ 
            	BI_ISC2Methods.updateAverage(trigger.new);
            }
            //else if(trigger.isDelete){
            //}
        }else{ 
            if(trigger.isInsert){ 
                BI_ISC2Methods.assignISC(trigger.new);
                BI_ISC2Methods.createPlandeAccion(trigger.new);
            }
            else if(trigger.isUpdate){
                //BI_ISC2Methods.createPlandeAccion(trigger.new);
            }
            //else if(trigger.isDelete){
            //}
            //else if(trigger.isUnDelete){
            //}         
        }
    }
}