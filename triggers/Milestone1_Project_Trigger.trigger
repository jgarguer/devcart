/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Milestones PM
    Company:       Milestones PM
    Description:   Trigger for Milestone1_Project__c
    History:
    
    <Date>            <Author>              <Description>
    25/09/2014        Milestones PM         Initial version
    05/07/2016        Jose Miguel Fierro    Added preventDeleteTemplate method
    30/05/2017        Jaime Regidor         Added setProjectOwner method
    20/07/2017        Guillermo Muñoz       Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
trigger Milestone1_Project_Trigger on Milestone1_Project__c (before update, before delete, before insert, after insert) {
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Milestone1_Project__c');
    
    if(!isTriggerDisabled){
        if(!NETriggerHelper.getTriggerFired('Milestone1_Project_Trigger')){

            if( Trigger.isUpdate ){
                //TODO can we delete this?
                Milestone1_Project_Trigger_Utility.handleProjectUpdateTrigger(trigger.new);
                 //Milestone1_Project_Trigger_Utility.updateDuracion(trigger.new, trigger.oldMap); //JRM 30/03/2017
                //BI_Milestone1ProjectMethods.setCurrencyChange(trigger.new, trigger.old);
                if (trigger.isBefore){
                    BI_Milestone1ProjectMethods.checkMandatoryAttachment(trigger.new, trigger.old, trigger.newMap);
                    BI_Milestone1ProjectMethods.setProjectOwner(trigger.new, trigger.oldMap);//JRM
                }
            } 
            else if( Trigger.isDelete ) {
                //cascades through milestones
                BI_Milestone1ProjectMethods.preventDeleteTemplate(Trigger.old); // JMF 05/07/2016
                Milestone1_Project_Trigger_Utility.handleProjectDeleteTrigger(trigger.old);
            }
            else if( Trigger.isInsert ) {
                //checks for duplicate names
                if (trigger.isBefore){
                    Milestone1_Project_Trigger_Utility.handleProjectInsertTrigger( trigger.new );
                    BI_Milestone1ProjectMethods.setProjectOwner(trigger.new,null);//JRM
                }else if (trigger.isAfter){
                    BI_Milestone1ProjectMethods.addOTM(trigger.new);
                }
            }
        }
    }
}