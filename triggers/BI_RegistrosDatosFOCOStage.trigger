trigger BI_RegistrosDatosFOCOStage on BI_Registros_Datos_FOCO_Stage__c (after insert) {

	//GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Registros_Datos_FOCO_Stage__c');
    
    if(!isTriggerDisabled){
		if (Trigger.isAfter) {
	    	BI_RegistrosDatosFOCOStageMethods.getInsertedCountries(trigger.new);
		}
	}
}