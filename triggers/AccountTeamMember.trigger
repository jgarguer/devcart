/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Orquestación de servicios para integraciones en Fullstack.

History:
<Date>							<Author>						<Change Description>
28/04/2017						Eduardo Ventura					Versión inicial
20/07/2017                      Guillermo Muñoz                 Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
18/08/2017						Julio Asenjo					Added condition to callout with estado= llamado						<J001>
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

trigger AccountTeamMember on FS_CORE_TeamMemberToSincronize__c (before insert, before update, before delete, after insert, after update, after delete) {
    /* SALESFORCE VARIABLES */
    FS_CORE_Integracion__c ORGConfiguration = FS_CORE_Integracion__c.getInstance();
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('FS_CORE_TeamMemberToSincronize__c');    

    if(!isTriggerDisabled){
        if (!System.isFuture() && ORGConfiguration != null && ORGConfiguration.FS_CORE_Peru__c){
            /* BYPASS DOESN'T SET */
            if(trigger.isBefore){
                /* BEFORE */
            } else{
                /* AFTER */
                Set<Id> accountIds = new Set<Id>();
                
                /* Trigger Actions */
                if(trigger.isInsert){
                    /* FULLSTACK FIRE */ 
                    for(FS_CORE_TeamMemberToSincronize__c item : trigger.new){
                        accountIds.add((Id) item.FS_CORE_IdAccount__c);
                    }
                }else if(trigger.isUpdate){ /* FULLSTACK FIRE */ for(FS_CORE_TeamMemberToSincronize__c item : trigger.new){accountIds.add((Id) item.FS_CORE_IdAccount__c);}
                }else if(trigger.isDelete){ /* FULLSTACK FIRE */ for(FS_CORE_TeamMemberToSincronize__c item : trigger.old){accountIds.add((Id) item.FS_CORE_IdAccount__c);}}
                
                /* FILTERED AND FIRE */
                //<J001>
                System.debug ('\n\n\n\t\t\t\t\taccountIds'+accountIds  +'\n\n\n\t\t\t isCalled-->'+FS_CORE_Fullstack_Manager.isCalled+'\n ');
                if(!FS_CORE_Fullstack_Manager.isCalled){
                    FS_CORE_Fullstack_Manager.isCalled=true;
                    for(Account item : [SELECT Id, FS_CORE_Estado_Sincronizacion__c FROM Account WHERE Id IN :accountIds AND (FS_CORE_Estado_Sincronizacion__c = 'Sincronizado' OR FS_CORE_Estado_Sincronizacion__c = 'Llamado')]){
                        System.debug('\n\n\n\t\t\t\t\t Calling ATM Process FROM TRIGGER TM--'+ item.FS_CORE_Estado_Sincronizacion__c+'\n\n\n');
                        FS_CORE_Fullstack_Manager.callout(item.Id, 2, false, null);
                    }
                }
            }
        }
    }
}