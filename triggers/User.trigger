trigger User on User (before update, after update, after insert,before insert) {
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fco.Javier Sanz
    Company:       Salesforce.com
    Description:   Trigger to handle methods from User
    
    History:
    
    <Date>                    <Author>               <Description>
    03/07/2014              Fco.Javier Sanz          Initial Version
    03/07/2014              Fco.Javier Sanz          After Insert 
    15/07/2014              Micah Burgos             check_ContactCustomerPortal(), createBI_ContactCustomerPortal
    17/02/2015              Jara Minguillón          Added call to addToTGSPortalGroup
    27/05/2015              Ricardo Pereira          Added call to addToBIPortalGroup
    10/11/2015              Francisco Ayllon         Added call to addToAdminCatalogGroup
    12/11/2015              Guillermo Muñoz          Added call to enableBIPortalUser, Added call to disaleBIPortalUser, deprecated addToBIPortalGroup
    18/04/2016              Antonio Masferrer        Added call to assignPortalPartnerDistribuidor
    14/06/2016              Alvaro sevilla           Added call to CrearGruposyAdicionarUsuarios and CrearGruposyAdicionarUsuariosBO methods
    15/06/2016              Humberto Nunees          Add call crearNodosPartner_CALL in Update TOO
    17/06/2016              Alvaro sevilla           Added call to CrearGruposyAdicionarUsuarios and CrearGruposyAdicionarUsuariosBO methods
    22/06/2016              José Luis gonzález Beltrán     Communicate User's Contact to RoD
    30/07/2016              Humberto Nunes          Call assignPermissionSetFVI in After Update TOO
    05/08/2016              Humberto Nunes          Se elimino la llamada a BI_UserMethods.assignPermissionSetFVI(trigger.new, trigger.old); porque se fusiono con assignBIPermissionSet
    12/09/2016              Guillermo Muñoz         Added call to enableBIPortalPartner
    20/07/2017              Guillermo Muñoz         Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('User');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore){
            if (trigger.isInsert && !System.isFuture()){ 
                //Method that check if exist this user on BI_ContactCustomerPortal
                BI_UserMethods.check_ContactCustomerPortal(trigger.new);

            }
            if (trigger.isupdate && !System.isFuture()){ 

                //Method that assign BI_UsuariosPortal permison set to a given user
                //BI_UserMethods.assingToPermissionSet(trigger.new);
                //BI_UserMethods.updateLastPasswordChange(trigger.new, trigger.old);
                
            }
        }else { 
            if (trigger.isInsert){

                if(!System.isFuture()){
                    
                    // CREA LOS NODOS PRINCIPALES DEL USUARIO 
                    BI_UserMethods.crearNodosPartner_CALL(trigger.new);   
                    //Method that create BI_ContactCustomerPortal
                    //Method that assign BI_UsuariosPortal permison set to a given user
                    //BI_UserMethods.assingToPermissionSet(trigger.old,trigger.new);
                    BI_UserMethods.assignBIPermissionSet(trigger.new, trigger.old);
                    BI_UserMethods.assignPortalPermissionSet(trigger.new, trigger.old);
                    //TGS Jara
                    BI_UserMethods.addToTGSPermissionSet(trigger.newMap.keySet());
                    BI_UserMethods.addToTGSPortalGroup(trigger.newMap.keySet());
                    //BI_UserMethods.addToBIPortalGroup(trigger.new);
                    //BI_UserMethods.addToAdminCatalogGroup(trigger.new);
                    BI_UserMethods.enableBIPortalUser(trigger.new,null);

                    BI_UserMethods.CrearGruposyAdicionarUsuarios(trigger.new, trigger.old);
                    BI_UserMethods.CrearGruposyAdicionarUsuariosBO(trigger.new, trigger.old);
                    // INI adaptacion UNICA
                    BIIN_UNICA_UserMethods.updateRoDContact(trigger.new, trigger.old);
                    // FIN adaptacion UNICA
                    BI_UserMethods.enableBIPortalPartner(trigger.new, trigger.old);
                }
                
                BI_UserMethods.createBI_ContactCustomerPortal(trigger.new);
            }
            if (trigger.isupdate){ 

                // CREA LOS NODOS PRINCIPALES DEL USUARIO 
                BI_UserMethods.crearNodosPartner_CALL(trigger.new);  

                BI_UserMethods.assignBIPermissionSet(trigger.new, trigger.old);
                BI_UserMethods.assignPortalPermissionSet(trigger.new, trigger.old);
                BI_UserMethods.enableBIPortalUser(trigger.new,trigger.old);
                BI_UserMethods.disableBIPortalUser(trigger.new,trigger.old);

                BI_UserMethods.CrearGruposyAdicionarUsuarios(trigger.new, trigger.old);
                BI_UserMethods.CrearGruposyAdicionarUsuariosBO(trigger.new, trigger.old);
                BI_UserMethods.enableBIPortalPartner(trigger.new, trigger.old);
                
            }
        }
    }
}