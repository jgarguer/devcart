trigger BI_Transaccion_bolsa_dinero on BI_Transaccion_de_bolsa_de_dinero__c (before insert, before update, before delete, after insert, after update, after delete) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by the BI_Facturaccion trigger.
    
    History

    <Date>            <Author>          <Description>
    09/03/2015      Ignacio Llorca      Initial version
    20/07/2017      Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Transaccion_de_bolsa_de_dinero__c');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore){
            if (trigger.isInsert){
            	BI_TransaccionBolsaDineroMethods.blockEdition(trigger.new);           
            } 
            if (trigger.isUpdate){
            	BI_TransaccionBolsaDineroMethods.blockEdition(trigger.new);
            }
            if (trigger.isDelete){
                BI_TransaccionBolsaDineroMethods.blockDelete(trigger.old);
            }
        }else {  
            if (trigger.isInsert){ 
                BI_TransaccionBolsaDineroMethods.recalculateMoneyBag(trigger.new, trigger.old);
            }   
            else if (trigger.isUpdate){
            	BI_TransaccionBolsaDineroMethods.recalculateMoneyBag(trigger.new, trigger.old);
            } 
            else if (trigger.isDelete){
                BI_TransaccionBolsaDineroMethods.recalculateMoneyBagDelete(trigger.old);
            }            
        }
    }
}