/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       27-Abr-2015     Javier Tibamoza              Creación de la Clase
    2.0       02-Sep-2016     Guillermo Muñoz              Añadido evento before update, cambiado el cuerpo del trigger y añadido método BI_COL_Midas_Version_cls.validateStatus()
    3.0       20-Jul-2017     Guillermo Muñoz              Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
****************************************************************************************************/
trigger BI_COL_Midas_tgr on BI_COL_MIDAS__c (before insert, before update, After insert, After update)
{
    //Carga la variable estatica con los valores de TRM USD
   // BI_COL_Midas_Version_cls.getUSD();
   TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_COL_MIDAS__c');
    
    if(!isTriggerDisabled){
        //BI_bypass__c bypass = BI_bypass__c.getInstance();
        //if(trigger.isBefore){

        //    if(trigger.isInsert){

        //        if(!bypass.BI_migration__c){

        //            list <BI_COL_MIDAS__c> listatrigger = Trigger.new; 
        //            BI_COL_Midas_Version_cls.midastgr(listatrigger);
        //        }
        //    }
        //    if(trigger.isUpdate){

        //        if(!bypass.BI_migration__c){

        //            BI_COL_Midas_Version_cls.validateStatus(trigger.newMap, trigger.oldMap);
        //        }

        //    }
        //}
        //else{

        //}
       
        /*for( BI_COL_MIDAS__c midasObj : Trigger.new )
        {
            midasObj.BI_COL_Estado_de_la_aprobacion__c = null;
            midasObj.BI_COL_Version__c = -1;
            
            list<AggregateResult> Mvercion= [SELECT MAX(BI_COL_Version__c) maxVersion FROM BI_COL_MIDAS__c WHERE BI_COL_Proyecto__c = : midasObj.BI_COL_Proyecto__c];
            
            List<AggregateResult> listMidas =[
                SELECT  Id, MAX(BI_COL_Version__c) maxVersion, BI_COL_Bloqueado__c bloqueado, BI_COL_Estado_de_la_aprobacion__c estado, BI_COL_Proyecto__r.StageName st
                FROM    BI_COL_MIDAS__c
                WHERE   BI_COL_Proyecto__c =: midasObj.BI_COL_Proyecto__c
                GROUP BY id, BI_COL_Bloqueado__c, BI_COL_Estado_de_la_aprobacion__c, BI_COL_Proyecto__r.StageName ];
            //Asigna La TRM Cargada se debe cargar previamente MidasCreation_cls.getUSD();                          
            midasObj.BI_COL_TRM_SFDC__c = BI_COL_Midas_Version_cls.monedaUSD;
            //Valida que la ultima prefactibilidad este aprobada
            mdcr.validarPrefactibilidad( Trigger.new );
            
            ID id = null;
            Integer maxVersion = -1;
            Boolean bloqueado = false;
            String estadoAprobacion = 'Rechazado - Inactivo';
            Boolean control = true;
            //Almacena el Id del midas que esta Aprobado o nulo
            list<Id> midasAN = new list<Id>(); 
            
            for( AggregateResult ar : listMidas )
            {
                id = (ID)ar.get('id');
                
                //Si el registro esta bloqueado actualiza la variable
                if( Boolean.valueOf( ar.get('bloqueado') ) )
                    bloqueado = Boolean.valueOf( ar.get('bloqueado') );
                
                estadoAprobacion = String.valueOf( ar.get('estado') );
                
                //Validamos si se puede crear o no un nuevo MIDAS
                if( ( ar.get('st') != 'F1 - Cerrada/Legalizada') && estadoAprobacion != 'Rechazado - Inactivo' && estadoAprobacion != 'Aprobado - Inactivo')
                {
                    control = false;
                    midasAN.add(ar.id);
                }

            }
            
            if( Mvercion[0].get('maxVersion') == null )
                midasObj.BI_COL_Version__c = 1;     
            else
                midasObj.BI_COL_Version__c = 1 + integer.valueOf( Mvercion[0].get('maxVersion') );
            
            if(!control)
            {
                if(bloqueado)
                    midasObj.addError('No se puede crear un nuevo MIDAS porque existe uno en proceso de aprobacion');
                else
                {
                    mdcr.actualizarMidas(midasAN);
                    if( maxVersion > -1 )
                        midasObj.BI_COL_Version__c = maxVersion + 1;
                }
            }
        }*/


        if (trigger.isAfter  ) //23/02/2017 GSPM: se retira sentencia IsBefore
        {
            if (trigger.isUpdate)
            {
                if(BI)
                {
                    //31/01/2017 START GSPM: 
                    if(!NETriggerHelper.getTriggerFired('BI_COL_Midas_Version_cls.SendEmail'))
                    {
                        BI_COL_Midas_Version_cls.SendEmail(trigger.new, trigger.old);
                    }
                    //31/01/2017 END GSPM: 
                }
              
            }
        }
    }
}