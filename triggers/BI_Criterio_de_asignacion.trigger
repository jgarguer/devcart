trigger BI_Criterio_de_asignacion on BI_Criterio_de_asignacion__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Guillermo Muñoz
	Company:       New Energy Aborda
	Description:   Methods executed by the BI_Criterio_de_asignacion trigger.

	History:
	 
	<Date>              <Author>                   <Change Description>
	13/02/2017          Guillermo Muñoz            Initial Version
	20/07/2017      	Guillermo Muñoz     	   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Criterio_de_asignacion__c');
    
    if(!isTriggerDisabled){
		if (Trigger.isBefore) {
		   	
		   	if(Trigger.isInsert){

		   		BI_Criterio_de_asignacionMethods.fillCriteriaReference(trigger.new);
		   	}
		   	else if(Trigger.isUpdate){
		   		
		   		BI_Criterio_de_asignacionMethods.fillCriteriaReference(trigger.new);
		   	}
		   
		}
		/*else if (Trigger.isAfter) {
		   
		}*/
	}
}