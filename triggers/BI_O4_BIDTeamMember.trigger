trigger BI_O4_BIDTeamMember on BI_O4_BID_team_member__c (after insert) {

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Methods executed by the BI_O4_BID_team_member__c trigger.

    History

    <Date>          <Author>                <Description>
    18/08/2016      Miguel Cabrera          initial version
    20/07/2017      Guillermo Muñoz         Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_O4_BID_team_member__c');
    
    if(!isTriggerDisabled){
    	if(trigger.isAfter){
    		if(trigger.isInsert){
    			BI_O4_BIDTeamMemberMethods.addBIDTeamMemberUserToOpp(trigger.new);
    		}
    	}
    }
}