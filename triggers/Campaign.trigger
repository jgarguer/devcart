trigger Campaign on Campaign (after update) {
  
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        Ignacio Llorca
  Company:       Salesforce.com
  Description:   Trigger on Campaign
   
  History:
   
  <Date>              <Author>          <Description> 
  15/04/2014          Ignacio Llorca    Initial version
  20/07/2017          Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
  //GMN 20/07/2017
  Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Campaign');
  
  if(!isTriggerDisabled){
    if (trigger.isBefore){
        //if (trigger.isInsert){
        //}
        //else if (trigger.isUpdate){
        //}
        //else if (trigger.isDelete){
        //}
    } else { 
      //if (trigger.isInsert){
      //}
      //else if (trigger.isDelete){
      //} 
      //else 
      if (trigger.isUpdate){
          BI_CampaignMethods.createCampaignMemberStatus(trigger.new, trigger.old);
          BI_CampaignMethods.assignTaskpreOpp(trigger.new, trigger.old);
          BI_CampaignMethods.cancelTask(trigger.new, trigger.old);
          BI_CampaignMethods.cancelpreOpp(trigger.new, trigger.old);
          BI_CampaignMethods.cancelContactTask(trigger.new,trigger.old);
      }
      //else if(trigger.isUnDelete){
      //}       
    }
  }
}