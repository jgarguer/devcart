trigger FeedComment on FeedComment (before insert) {

	BI_bypass__c bypass = BI_bypass__c.getInstance();
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c;
    Boolean Community = userTGS.TGS_Is_Community__c;


	if(trigger.isBefore){
        if (trigger.isInsert){
            if(BI){
               BI_COE_FeedCommentMethods.sendEmailCOE(trigger.new);
               BI_COE_FeedCommentMethods.reopenCase(trigger.new);
            }
        }   
    }
}