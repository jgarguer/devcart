/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Methods executed by the BI_Modelos_disponibles trigger.
    
    History

    <Date>            <Author>        	  <Description>
    27/03/2015		  Fernando Arteaga    Initial version
	20/07/2017        Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

trigger BI_Modelos_disponibles on BI_Modelos_disponibles__c (before insert)
{

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Modelos_disponibles__c');
    
    if(!isTriggerDisabled){
		if (trigger.isBefore)
		{
			if (trigger.isInsert)
			{
				BI_ModelosDisponiblesMethods.checkBolsaDineroHasOnlyOneRecordPerModel(Trigger.new, Trigger.old);
			}
		}
	}
}