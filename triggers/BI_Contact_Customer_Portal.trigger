trigger BI_Contact_Customer_Portal on BI_Contact_Customer_Portal__c (before insert, after insert,  after delete) {
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno Hernán
    Company:       Salesforce.com
    Description:   Methods executed by the BI_Contact_Customer_Portal trigger.
    
    History

    <Date>            <Author>                  <Description>
    25/05/2015        Antonio Moruno            Initial version
    19/10/2015        Micah Burgos              Micah Burgos    
    20/07/2017        Guillermo Muñoz           Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Contact_Customer_Portal__c');
    
    if(!isTriggerDisabled){
        if(trigger.isBefore){
            if(trigger.IsInsert){
                BI_CCP_Methods.insertPermission_CCP(trigger.old,trigger.new);
            }
        }else if (trigger.isAfter) {
            if(trigger.IsInsert){
                BI_CCP_Methods.insertAccountShare_CCP(trigger.new);
            }
            if(trigger.isDelete){
                BI_CCP_Methods.deleteAccountShare_CCP(trigger.new);
            }
        }
    }
}