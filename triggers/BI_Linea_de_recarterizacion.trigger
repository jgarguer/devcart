trigger BI_Linea_de_recarterizacion on BI_Linea_de_Recarterizacion__c (before insert) {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Trigger on BI_Linea_de_Recarterizacion__c
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/02/2016						Guillermo Muñoz				Initial version
		20/07/2017      				Guillermo Muñoz     		Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Linea_de_Recarterizacion__c');
    
    if(!isTriggerDisabled){
		if(trigger.isInsert){

			if(trigger.isBefore){
				BI_Linea_de_recarterizacionMethods.insertIdFromExternalId(trigger.new);
			}
		}
	}
}