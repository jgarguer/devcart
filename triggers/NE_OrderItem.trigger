/*------------------------------------------------------------
Author:         NewEnergy
Company:        NewEnergy
Description:    Trigger for changes in NE__OrderItem__c Object
                Call Integration to sinchronize OrderItems with RoD
History
<Date>          <Author>            <Change Description>
?               New Energy          Initial Version
20/08/2015      Francisco Ayllón    Unification with NE_OrderItem, migrated BI_NEOrderItemMethods inside the loop
                Micah Burgos      
20/06/2016      Álvaro López      Added setCaseService method to TGS after update
27-Aug-2015     Jose Lopez          Unification with NE_OrderItem, Added Rod integration for Order Item
29/09/2015      Marta García        Unification with NE_OrderItem, Added field TGS_Value_Attribute fill
12/01/2016      Guillermo Muñoz     Reevaluation of the approval level of the Opportunity
19/02/2016      Micah Burgos        Reevaluation of control fields if It changes the qty of products
12/08/2016      Jorge Galindo       we have to loop the map instead of trigger.new because can be differences because of NI_FVIBorrar trigger
20/08/2016      Geraldine Perez     Add Economic values ​​calculated for the opportunity to Colombia
31/08/2016      Fernando Arteaga    Infinity W4+5: use an opportunity map instead of a list of sObjects. Also new method calls added
23/09/2016      Alfonso Alvarez     Se incluye en este trigger, el código del trigger BI_FVIBorrar (el cual se desactivará)
05/10/2016      Fernando Arteaga    Infinity W4+5: add missing encapsulation
18/11/2016      Alvaro García       añadida condición para que no requiera validación económica cuando se trate  oportunidades hijas de una oportunidad Centralizada
23/01/2017      Alberto Fernández   Adding updateValPresupuestoOrder();
20/02/2017      Gawron, Julian      Adding afterDelete, actualizarAccountCoberturaDigitalOI
15/03/2017      Álvaro López        Added TGS_Billing_Date.inFutureContext flag
27/03/2017      Pedro Pachas        Modify condition in lines 153 and 183 to pass a process of economic analysis of Opportunity
12/04/2017      Humberto Nunes      SE AGREGO LA OMISION PARA CUANDO SEA FVI
17/04/2017      Humberto Nunes      SE AGREGO LA OMISION PARA CUANDO SEA FVI EN OTRO IF INTERNO YA QUE NO FUNCIONABA EN UAT
19/04/2017      Humberto Nunes      SE HIZO LA COMPARACION CON EL Recordtype de la OPP y no con el del Padre de la OPP Y SE CONSULTO ESE VALOR EN EL QUERY
20/404/2017     Alejandro Pantoja   Delete condition in lines 153 and 183 to pass a process of economic analysis of Opportunity and check off the factibility check in order to luch the EF. 
23/05/2017      Cristina Rodríguez  Added NESetDefaultOIFields.trigger and NECheckDeltaQuantity.trigger. Refactor.
01/06/2017      Álvaro López        Added CI status Active
21/06/2017      Guillermo Muñoz     Added trigger helper on BI_NEOrderItemMethods.updateSumatoria method
18/07/2017      Humberto Nunes      Encapsulado de FVI
20/07/2017      Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
01/08/2017      Guillermo Muñoz     Added Trigger.oldMap in BI_NEOrderItemMethods.neSetDefaultOIFields call
30/08/2017      Alvaro Sevilla      Added invocation to method campoSubsidioEquipoOpp
26/09/2017      Álvaro López        fillParentQty method call in after insert and after update.
05/10/2017      Jaime Regidor       Adding BI_NEOrderItemMethods.PaybackFijaValores
22/05/2018  	Manuel Ochoa    	If configuration comes from bulk, call updateOrderAndOrder method only in update
22/05/2018  	Manuel Ochoa    	If configuration comes from bulk, call updateOrderAndOrder method only in update
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
trigger NE_OrderItem on NE__OrderItem__c (after insert, after update, before insert, before update){   

    System.debug('YYYY Entra en Trigger 0');
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c; 

    /*Álvaro López - 13/07/2017 - Evita que valide el RBMA al modificar un order item*/
    NETriggerHelper.setTriggerFiredTest('BI_OpportunityMethods.validatePreviousRecurringCharge', true);

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('NE__OrderItem__c');
    
    // 21/05/2018 - Manuel Ochoa - if configuration comes from bulk, call updateOrderAndOrder method only in update
    Boolean bComesFromBulk=false;

    //JAG 26/01/2018
    BI_bypass__c bypass = BI_bypass__c.getInstance();
    if(!isTriggerDisabled){
        /******* START NECheckOrderItems.trigger *******/
        if(trigger.isAfter && !bypass.BI_skip_trigger__c){
            System.debug ('JAG Entro en trigger.isAfter');

            if(trigger.isInsert || trigger.isUpdate ){             
                System.debug('YYYY Entra en Trigger 1');                
                boolean wasFired_NECheckOrderItems    =   NETriggerHelper.getTriggerFired('NECheckOrderItems');
                System.debug('################## wasFired_NECheckOrderItems ' + NETriggerHelper.getTriggerFired('NECheckOrderItems'));
                
                if(!wasFired_NECheckOrderItems){

                    /*------------------------- START TGS ---------------------*/    
                    if(trigger.isUpdate){

                        if(TGS){

                            TGS_NEOrderItemMethods.callRodWs(Trigger.newMap, Trigger.isUpdate);                    
                        }
                    }
                    /*---------------------------END TGS ----------------------*/

                    NETriggerHelper.setTriggerFired('NECheckOrderItems'); 
                    // FAR 30/08/2016: Add BI_O4_Gross_margin_P__c, BI_O4_Acquisition_Margin__c, BI_O4_Retention_Margin__c, BI_O4_Growth_Margin__c fields 
                    Map<String,NE__OrderItem__c> mapOfOi    =   new Map<String,NE__OrderItem__c>([SELECT Id,NE__OrderId__c, NE__Status__c, NE__OrderId__r.NE__OptyId__c,NE__CatalogItem__r.NE__Base_OneTime_Fee__c,NE__CatalogItem__r.NE__BaseRecurringCharge__c,NE__CatalogItem__r.NE__Technical_Behaviour_Opty__c,NE__OrderId__r.RecordType.Name,NE__RecurringChargeOv__c,NE__OneTimeFeeOv__c,NE__BaseOneTimeFee__c,NE__BaseRecurringCharge__c,NE__Qty__c, RecordType.DeveloperName,NE__Billing_Account__c,NE__Billing_Account__r.TGS_Aux_Holding__c,NE__Service_Account__c,NE__Service_Account__r.TGS_Aux_Holding__c, BI_O4_Gross_margin_P__c, BI_O4_Acquisition_Margin__c, BI_O4_Retention_Margin__c, BI_O4_Growth_Margin__c, NE__CatalogItem__r.NE__ProductId__r.BI_COT_MEX_Analisis_Economico__c FROM NE__OrderItem__c WHERE Id IN: Trigger.new]);
                    
                    list<String> listOfOptyIds  =   new list<String>();
                    list<String> listOfOrderIds  =   new list<String>();
                    
                    Boolean IsTech = false;
                    
                    // JGL 12/08/2016   
                    //for(NE__OrderItem__c oiTrigger : Trigger.new) {
                    for(NE__OrderItem__c oi : mapOfOi.values()) {   
                        //NE__OrderItem__c    oi  =   mapOfOi.get(oiTrigger.id);
                        // END JGL 12/08/2016
                        
                        if(oi.NE__OrderId__r.NE__OptyId__c != null)
                            listOfOptyIds.add(oi.NE__OrderId__r.NE__OptyId__c);
                            
                        listOfOrderIds.add(oi.NE__OrderId__c);
                    }
                    //map<String,NE__Order__c> mapOfOrders    =   new map<String,NE__Order__c>([SELECT Id, NE__OptyId__c, NE__OrderStatus__c, BI_Ingreso_Recurrente_Anterior_Config__c, CurrencyIsoCode, (SELECT Id, BI_Ingreso_Recurrente_Anterior_Producto__c, NE__Qty__c, CurrencyIsoCode, NE__Parent_Order_Item__c, NE__Parent_Order_Item__r.NE__Qty__c, NE__BaseRecurringCharge__c, NE__BaseOneTimeFee__c, Recurring_Cost__c, One_Time_Cost__c, NE__ProdId__r.RecordType.Name FROM NE__Order_Items__r WHERE NE__Status__c = 'Pendiente de envío' OR NE__Status__c = 'Pending' OR NE__Status__c = 'Enviado' OR NE__Status__c = 'En tramitación') FROM NE__Order__c WHERE Id IN: listOfOrderIds AND NE__OptyId__c!=null]);
                    /* 01/06/2017 Álvaro López - Added CI status Active */
                    map<String,NE__Order__c> mapOfOrders    =   new map<String,NE__Order__c>([SELECT Id, NE__OptyId__c, NE__OrderStatus__c, BI_Ingreso_Recurrente_Anterior_Config__c, CurrencyIsoCode, NE__One_Time_Fee_Total__c, NE__Recurring_Charge_Total__c,BI_Original_Total_Recurring_Charge__c, BI_Original_Total_One_Time_Fee__c, BI_Cantidad_de_Equipos__c, BI_Cantidad_de_Servicios__c, BI_Cantidad_de_Otros_Productos__c, BI_Total_One_Time_Cost__c,BI_Total_Recurring_Cost__c , (SELECT Id, BI_Ingreso_Recurrente_Anterior_Producto__c, NE__Qty__c, CurrencyIsoCode, NE__Parent_Order_Item__c, NE__Parent_Order_Item__r.NE__Qty__c, NE__BaseRecurringCharge__c, NE__BaseOneTimeFee__c, Recurring_Cost__c, One_Time_Cost__c, NE__ProdId__r.RecordType.Name, NE__OneTimeFeeOv__c, NE__RecurringChargeOv__c, NE__OrderId__c , NE__ProdId__c FROM NE__Order_Items__r WHERE NE__Status__c = 'Pendiente de envío' OR NE__Status__c = 'Pending' OR NE__Status__c = 'Enviado' OR NE__Status__c = 'En tramitación' OR NE__Status__c = 'Active') FROM NE__Order__c WHERE Id IN: listOfOrderIds AND NE__OptyId__c!=null]);        // FAR 30/08/2016 - Add BI_O4_Tipo_de_OportunidadTNA__c field
                    // HN 19/04/2017 SE ADICIONO EL DEVELOPERNAME DEL RECORDTYPE
                    // 22/05/2018 - Manuel Ochoa - Added FOR UPDATE clausule to query
                    map<String,Opportunity> mapOfStdOpty    =   new map<String,Opportunity>([SELECT Id, 
                                                                                             		RecordTypeId, 
                                                                                             		RecordType.DeveloperName, 
                                                                                             		BI_Oportunidad_padre__c, 
                                                                                             		BI_Oportunidad_padre__r.RecordType.DeveloperName, 
                                                                                             		BI_Oportunidad_padre__r.BI_O4_Opportunity_Type__c, 
                                                                                             		Account.RecordType.DeveloperName, 
                                                                                             		BI_O4_Tipo_de_OportunidadTNA__c,
                                                                                             		BI_Oferta_economica__c,
                                                                                             		BI_Origen_de_la_oferta_tecnica__c,
                                                                                             		BI_Oferta_tecnica__c,
                                                                                             		BI_Descuento__c,
                                                                                             		BI_No_requiere_comite_arg__c,
                                                                                             		BI_No_requiere_Pricing_arg__c,
                                                                                             		BI_Factibilidad_tecnica_legado_arg__c,
                                                                                             		BI_Country__c,
                                                                                             		BI_Productos_numero__c,
                                                                                             		BI_Recurrente_bruto_mensual_anterior__c,
                                                                                             		CurrencyIsoCode 
                                                                                             		FROM Opportunity WHERE id IN: listOfOptyIds FOR UPDATE]);
                    // 22/05/2018 - Manuel Ochoa - Added FOR UPDATE clausule to query
                    list<Opportunity> listOfOptyToUpd       =   new list<Opportunity>();
                    list<NE__Order__c> listOfOrderToUpd     =   new list<NE__Order__c>();
                    Map<Id, Opportunity> mapOppsToUpd = new Map<Id, Opportunity>();
                    
                    BI_NEOrderItemMethods.fillOrderAndOptyFields(mapOfOi, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, IsTech, mapOppsToUpd, mapOfOrders, mapOfStdOpty, listOfOrderToUpd);
                    
                    // FAR 31/08/2016: Call updateOpptyForApproval, and if mapOppsToUpd contains the opps, set BI_Oferta_economica__c field
                    if (BI){

                        BI_O4_OrderItemMethods.setEconomicFeasibility(Trigger.new, Trigger.old, mapOppsToUpd, listOfOptyToUpd);
                    }
                    // END FAR 19/09/2016:
                    
                    // 22/05/2018 - Manuel Ochoa - if configuration comes from bulk, call updateOrderAndOrder method only in update
                    for(NE__OrderItem__c oi : Trigger.new){
                        if(oi.Bit2WinHUB__BulkConfigurationItemRequest__c!=null){
                            system.debug(LoggingLevel.ERROR, 'CI comes from bulk');
                            bComesFromBulk=true;
                        }
                    }
                    
                    if(bComesFromBulk){
                        if(trigger.isUpdate){
                            system.debug(LoggingLevel.ERROR, 'updateOrderAndOrder only in update');
                        	BI_NEOrderItemMethods.updateOrderAndOrder(listOfOrderToUpd, listOfOptyToUpd); 
                        }                                               
                    }else{
                        system.debug(LoggingLevel.ERROR, 'updateOrderAndOrder update and insert');
                      	BI_NEOrderItemMethods.updateOrderAndOrder(listOfOrderToUpd, listOfOptyToUpd);  
                    }
                    // 22/05/2018 - Manuel Ochoa - if configuration comes from bulk, call updateOrderAndOrder method only in update
                    
                }
                /* Mariano García 02/10/2017 - campoSubsidioEquipoOpp method was encapsulated to avoid too many queries error in tgs processes*/
                if(BI || FVI){
                    //ASD Se adicionó invocacion el metodo 30/08/2017
                    BI_NEOrderItemMethods.campoSubsidioEquipoOpp(trigger.new, trigger.old);
                }
                    
                boolean wasFired_CalculateOR   =   NETriggerHelper.getTriggerFired('CalculateOR');
                boolean wasFired_updateModServicio   =   NETriggerHelper.getTriggerFired('updateModServicio');//GSPM 21-05-2018
                if(BI){
                    System.debug('YYYY Entra en Trigger 2');
                    
                    if(!wasFired_CalculateOR) {
                        NETriggerHelper.setTriggerFired('CalculateOR');
                        BI_NEOrderItemMethods.CalculateOR(trigger.new, trigger.old);
                    }
                    
                    if(!wasFired_updateModServicio) {//GSPM START: 21-05-2018
                        NETriggerHelper.setTriggerFired('updateModServicio');
                        BI_NEOrderItemMethods.updateModServicio(trigger.new, trigger.old);
                    } //GSPM END: 21-05-2018

                    if(!NETriggerHelper.getTriggerFired('TasaFromStageName')){

                        BI_NEOrderItemMethods.updateSumatoria(trigger.new, trigger.old);
                    }
                    BI_NEOrderItemMethods.actualizarAccountCoberturaDigitalOI(trigger.new, trigger.old); //JEG D398 17/02/2017 
                    BI_NEOrderItemMethods.fillParentQty(Trigger.new, Trigger.oldMap, Trigger.isBefore); //JEG 11/10/2017
                }
            }

            if(trigger.isUpdate){

                if(TGS){

                    TGS_NEOrderItemMethods.setCaseService(trigger.new, trigger.old);
                }

                if (FVI)
                {
                    BI_FVIBorrarMethods.Borrar(Trigger.new); 
                }
                else
                {
                    /******* START NECheckDeltaQuantity.trigger *******/  
                    boolean wasFired_NECheckDeltaQuantity = NETriggerHelper.getTriggerFired('NECheckDeltaQuantity');
            
                    system.debug('*wasFired_NECheckDeltaQuantity ' + wasFired_NECheckDeltaQuantity + ' !wasFired_NECheckDeltaQuantity ' + !wasFired_NECheckDeltaQuantity);
                    
                    if(!wasFired_NECheckDeltaQuantity){

                        NETriggerHelper.setTriggerFired('NECheckDeltaQuantity');
                        BI_NEOrderItemMethods.neCheckDeltaQuantity(trigger.new);
                    }
                    /******* END NECheckDeltaQuantity.trigger *******/
                }          
            }

            //06/11/2017 GSPM START: Modificación de invocación al método duplicarOI
           if(trigger.isInsert || trigger.isUpdate ){ 

                boolean wasFired_duplicarOI   =   NETriggerHelper.getTriggerFired('duplicarOI');
                System.debug('====== wasFired_duplicarOI == 1 ======>>'+wasFired_duplicarOI);
                if(BI){
                    if(!wasFired_duplicarOI)
                    {                        
                        if(!System.isFuture() && !System.isQueueable() && !System.isScheduled() && !System.isBatch()){
                            NETriggerHelper.setTriggerFired('duplicarOI');
                            BI_NEOrderItemMethods.duplicarOI(Trigger.new, Trigger.old);}
                            System.debug('====== wasFired_duplicarOI == 2 ======>>'+wasFired_duplicarOI);
                    }
                }
            } 
            //06/11/2017 GSPM END: Modificación de invocación al método duplicarOI
            if(trigger.isDelete){
                
                if(BI){

                    BI_NEOrderItemMethods.actualizarAccountCoberturaDigitalOI(trigger.new, trigger.old); //JEG D398 20/02/2017
                }
            }
        }    
        /******* END NECheckOrderItems.trigger *******/

        /******* START NESetDefaultOIFields.trigger *******/
        if(trigger.isBefore && !bypass.BI_skip_trigger__c){
            System.debug ('JAG Entro en trigger.isBefore');

            if(trigger.isInsert || trigger.isUpdate ){ 

                boolean wasFired_NESetDefaultOIFields    =   NETriggerHelper.getTriggerFired('NESetDefaultOIFields');
                boolean testFired_NESetDefaultOIFields    =   NETriggerHelper.getTriggerFired('NESetDefaultOIFieldsTest');
                       
                //ALM 12/05/2017
                Map <String,NE__Product__c> mapOfCi;
                list<String> listofOIci =   new list<String>();

                //ALM 12/05/2017 - Moved for query reutilitation
                if(mapOfCi == null){
                    for(NE__OrderItem__c oiTrigger : Trigger.new)
                        listofOIci.add(oiTrigger.NE__ProdId__c);
                        
                    mapOfCi = NE_OrderItemTriggerHelper.getIdProduct(listofOIci);//JEG// new map<String,NE__Product__c>([SELECT id,recordType.DeveloperName, BI_COT_MEX_Analisis_Economico__c FROM NE__Product__c WHERE id in: listofOIci]);
                }     
                
                if(!wasFired_NESetDefaultOIFields && !testFired_NESetDefaultOIFields){

                    BI_NEOrderItemMethods.neSetDefaultOIFields(trigger.new,trigger.oldMap, mapOfCi);
                }

                if(!testFired_NESetDefaultOIFields){

                    if(Trigger.isBefore){
                        if(Trigger.isInsert){
                        if(TGS){
                            TGS_NEOrderItemMethods.fillCiSite(Trigger.new);
                        }
                        if (BI){

                            BI_O4_OrderItemMethods.setTotalPriceAndCost(Trigger.new);
                            BI_O4_OrderItemMethods.fillInvoicingModelAndUnit(Trigger.new, false);
                            BI_NEOrderItemMethods.agregaTasa(trigger.new); //JEG 03/02/2017
                            BI_NEOrderItemMethods.fillParentQty(Trigger.new, Trigger.oldMap, Trigger.isBefore); //JEG 11/10/2017
                        }
                    }
                    if(Trigger.isUpdate){

                        if(BI || FVI){

                        BI_NEOrderItemMethods.PaybackFijaValores(trigger.new, trigger.old);//JRM 17/10/2017

                        }

                        if(TGS) {

                            TGS_NEOrderItemMethods.fillRequiredData_update(Trigger.newMap, Trigger.oldMap);
                            TGS_NEOrderItemMethods.validateCIFields_update(Trigger.newMap, Trigger.oldMap);
                            System.debug('Llego al método.');
                            TGS_NEOrderItemMethods.cleaner_OrderItemPriceFields(Trigger.new);
                            System.debug('Salgo del método.');
                        }
                        if (BI){
                            BI_O4_OrderItemMethods.setTotalPriceAndCost(Trigger.new, Trigger.oldMap);
                            BI_NEOrderItemMethods.fillParentQty(Trigger.new, Trigger.oldMap, Trigger.isBefore); //JEG 11/10/2017
                        }
                    }
                }
                } 
            }
        }
        /******* END NESetDefaultOIFields.trigger *******/
    }
}