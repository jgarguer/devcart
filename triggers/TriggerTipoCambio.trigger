trigger TriggerTipoCambio on TipoCambio__c(after insert, after update){
	
	//GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('TipoCambio__c');
    
    if(!isTriggerDisabled){
		if (trigger.isAfter){
	        if (trigger.isUpdate || trigger.isInsert){
	        	BI_TriggerTipoCambioMethods.actMapCurrency(trigger.new);
	        }
	    }

	    /*
		Map<String, CurrencyTypeFacturacion__c> mapCurrencys = new Map<String, CurrencyTypeFacturacion__c>();
		//recorro la lista de objetos insertados o actualizados, dejando en un mapa solamente los últimos
		//que se insertaron o actualizaron de cada moneda
		Map<String, TipoCambio__c> mapTC = new Map<String, TipoCambio__c>();
		for(TipoCambio__c tc : Trigger.new){
			mapTC.put(tc.CodigoDivisa__c, tc);
		}
		
		List<CurrencyTypeFacturacion__c> listaCurrencys = [SELECT Id, IsoCode__c, IsCorporate__c, IsActive__c, ConversionRate__c FROM CurrencyTypeFacturacion__c WHERE IsoCode__c IN :mapTC.keySet()];
		
		//actualizo los currencys que ya existen
		for(CurrencyTypeFacturacion__c ctf : listaCurrencys){

			TipoCambio__c copiatc = mapTC.remove(ctf.IsoCode__c);
			ctf.ConversionRate__c = copiatc.Factor__c;
			mapCurrencys.put(ctf.IsoCode__c, ctf);
			if (ctf.IsoCode__c == 'MXN'){
				ctf.IsCorporate__c = true;
			}else{
				ctf.IsCorporate__c = true;
			}
		}
		//creo nuevos currencys si es necesario
		for(TipoCambio__c tc : mapTc.values()){
			CurrencyTypeFacturacion__c ctf = new CurrencyTypeFacturacion__c();
			ctf.IsoCode__c = tc.CodigoDivisa__c;
			ctf.ConversionRate__c = tc.Factor__c;
			if (ctf.IsoCode__c == 'MXN'){
				ctf.IsCorporate__c = true;
			}else{
				ctf.IsCorporate__c = true;
			}
			mapCurrencys.put(ctf.IsoCode__c, ctf);
		} 	
		
		for(List<CurrencyType> listaCT : [Select IsoCode, IsCorporate, Id, DecimalPlaces, ConversionRate,IsActive from CurrencyType]){
			for(CurrencyType ct : listaCT){
				if(mapCurrencys.containsKey(ct.IsoCode)){
					mapCurrencys.get(ct.IsoCode).IsCorporate__c = ct.IsCorporate;
					mapCurrencys.get(ct.IsoCode).IsActive__c = ct.IsActive;
				}
			}
		}  
		
		upsert mapCurrencys.values();
		*/
	}
	
}