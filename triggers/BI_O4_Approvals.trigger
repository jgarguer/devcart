trigger BI_O4_Approvals on BI_O4_Approvals__c (before insert, before update, after insert, after update) {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Salesforce.com
    Description:   Trigger to call methods of BI_O4_Approvals__c
    
    History:
    
    <Date>            <Author>           <Description>
    16/01/2017        Alvaro García      Add methods createEvent, updateDRB_Event, deleteDRB_Event, updateEvent_confirmedDate
    10/07/2017        Javier Almirón     Add method migAppOpp in when trigger is after, is update/insert and if (BI).
    20/07/2017        Guillermo Muñoz    Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    
    BI_bypass__c bypass = BI_bypass__c.getInstance();

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_O4_Approvals__c');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore && !bypass.BI_skip_trigger__c){
            
            //if (trigger.isInsert){

            //}
            
            //if (trigger.isUpdate){
                
            //}
        }

        else if(!bypass.BI_skip_trigger__c) { 
            
            if (trigger.isInsert){ 
             	if (BI) {
            	    BI_O4_ApprovalsMethods.createEvent(trigger.new);
                    BI_O4_ApprovalsMethods.migAppOpp(trigger.new, trigger.oldMap);
            	}
            }
            
            if (trigger.isUpdate){
                if (BI) {
                    BI_O4_ApprovalsMethods.updateDRB_Event(trigger.new, trigger.old);
                    BI_O4_ApprovalsMethods.deleteDRB_Event(trigger.new, trigger.old);
                    BI_O4_ApprovalsMethods.updateEvent_confirmedDate(trigger.new, trigger.old);
                    BI_O4_ApprovalsMethods.migAppOpp(trigger.new, trigger.oldMap);
                }
                
            }       
        }
    }
}