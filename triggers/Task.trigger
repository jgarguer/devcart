trigger Task on Task (before insert, before update, before delete, after update, after insert) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Diego Arenas
    Company:       Salesforce.com
    Description:   Trigger for controlling Task Methods
    History
    11/02/2014      Diego Arenas       UtilTriggers.rellenaDescripcionTask en before insert/update
    19/05/2014      Ignacio Llorca     Prevent delete of tasks whose BI_Relacionado_con_etapa__c = null
    29/01/2015      Pablo Oliva	       preventTaskForActionPlan method added.
    30/09/2015      Guillermo Muñoz    Method "checkRecurrenceTask" added
    21/01/2016      Micah Burgos       Change !bypass.BI_migration__c to !bypass.BI_skip_trigger__c. BI_migration__c is used for ValidationRules and some code validations and BI_skip_trigger__c disable the trigger.    
    20/07/2017      Guillermo Muñoz    Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
   12/12/2017		Álvaro López	   Added TGS_User_Org and BI methods encapsulated
    05/03/2018      Iñaki Frial        Method "checkBlockingTask" added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    BI_bypass__c bypass = BI_bypass__c.getInstance();

    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c;
    Boolean Community = userTGS.TGS_Is_Community__c;

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Task');
    
    if(!isTriggerDisabled){    
    	if(!bypass.BI_skip_trigger__c){
	    
		    if (trigger.isBefore){
		        if (trigger.isInsert){
		        	if(BI){
		            /*Se carga el campo description__c. Este trigger en ning�n momento interrumpe la carga de Tareas.*/
		            //Certa_SCP.UtilTriggers.rellenaDescripcionTask(trigger.New);
		            	BI_TaskMethods.preventTaskForActionPlan(trigger.new); 
		            	BI_TaskMethods.checkRecurrenceTask(trigger.new);
		            }
		           
		        }
		        if (trigger.isUpdate){
		        	if(BI){
		            /*Se carga el campo description__c. Este trigger en ning�n momento interrumpe la carga de Tareas.*/
		            //Certa_SCP.UtilTriggers.rellenaDescripcionTask(trigger.New);
			            BI_TaskMethods.check_HasClosedPlanDeAccion(trigger.new);	                    
		        }
		        }
		        if (trigger.isDelete){
		        	if(BI){
		            BI_TaskMethods.preventDeleteTask(trigger.old);
		        }
		    }
		    }
		    else { 
		        if (trigger.isInsert){
		        	system.debug('Triggerrrr');
		             BI_TaskMethods.checkBlockingTask(trigger.new);
		             
		        }
		        //else if (trigger.isDelete){
		        //} 
		        else if (trigger.isUpdate){
		        	if(BI){
		        if(!NETriggerHelper.getTriggerFired('BI_Milestone1TaskMethods.updateProjectTask'))
		             BI_TaskMethods.updateProjectTask(trigger.new, trigger.old);   
		        }       
		        	}
		        }
		        if(BI){
			        BI_TaskMethods.updateActionPlan(trigger.new);
			        BI_TaskMethods.checkBlockingTask(trigger.new);	
			        }
		    }	    
	    }
	}