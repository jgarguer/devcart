trigger Order2AssetAfterUpdate on Case (after update)
{     

    //GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Case');
    
    if(!isTriggerDisabled){

        boolean wasFired        =   NETriggerHelper.getTriggerFired('Order2AssetAfterUpdate');
        
        if(!wasFired)
        {
            NETriggerHelper.setTriggerFired('Order2AssetAfterUpdate');
            
            RecordType caseRT                   =   [SELECT id FROM RecordType WHERE SobjectType = 'case' and DeveloperName = 'Order_Management_Case'];
            list<String> confassetToUpdQuery    =   new list<String>();
            for(Case updCase : Trigger.new)
            {
                if(updCase.Asset__c != null)
                    confassetToUpdQuery.add(updCase.Asset__c);
            }
            
            if(confassetToUpdQuery.size() > 0)
            {
                map<String, NE__Order__c>   mapOfConfAsset  =   new map<String, NE__Order__c>([SELECT NE__Asset__c
                                                                                               //,TGS_RFB_date__c, TGS_RFS_date__c Deloitte (GAB) 17/04/2014 - Changed field for a formula one
                                                                                               FROM NE__Order__c 
                                                                                               WHERE Id IN: confassetToUpdQuery]);
                        
                list<String> assetToUpdQuery                =   new list<String>();
                for(NE__Order__c ordAsset : mapOfConfAsset.values())
                    assetToUpdQuery.add(ordAsset.NE__Asset__c);
                map<String, NE__Asset__c> commAssetMap  = new map<String, NE__Asset__c>([SELECT TGS_RFB_date__c, TGS_RFS_date__c FROM NE__Asset__c WHERE Id IN: assetToUpdQuery]);
                
                list<NE__Order__c> confToUpd    =   new list<NE__Order__c>();
                list<NE__Asset__c> assetToUpd   =   new list<NE__Asset__c>();
                
                for(Case updCase : Trigger.new) 
                {
                    try
                    {
                        case oldCase = Trigger.oldMap.get(updCase.id);
                        system.debug('updCase.Asset__c: '+updCase.Asset__c);
                        
                        if(updCase.RecordTypeId == caseRT.id && updCase.Asset__c != null)
                        {
                            NE__Order__c assetGenerated     = mapOfConfAsset.get(updCase.Asset__c);                
                            //assetGenerated.TGS_RFB_date__c  = updCase.TGS_RFB_date__c; Deloitte (GAB) 17/04/2014 - Changed field for a formula one
                            //assetGenerated.TGS_RFS_date__c  = updCase.TGS_RFS_date__c; Deloitte (GAB) 17/04/2014 - Changed field for a formula one
                            confToUpd.add(assetGenerated);
                            
                            NE__Asset__c commAsset    = commAssetMap.get(assetGenerated.NE__Asset__c);
                            commAsset.TGS_RFB_date__c = updCase.TGS_RFB_date__c;
                            commAsset.TGS_RFS_date__c = updCase.TGS_RFS_date__c;
                            assetToUpd.add(commAsset);
                            
                            system.debug('RFB and RFS updated.');
                        }
                    }
                    catch(Exception e){
                        system.debug('Exception: '+e+' at line: '+e.getLineNumber());
                    }
                }
                
                if(confToUpd.size() > 0)
                {
                    update confToUpd;
                    update assetToUpd;
                }
            }
            
        }
    }
}