/*------------------------------------------------------------
Author:         Ana Cirac 
Company:        Deloitte
Description:    When an WorkInfois created, SF updated this change in other 
                systems.
History
<Date>          <Author>            <Change Description>
02-Mar-2015     Ana Cirac           Initial Version
15/02/16        Miguel Cabrera      Conditions added for UDO queue
20/07/2017      Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
trigger TGS_WorkInfoTrigger on TGS_Work_Info__c (after insert, after update) {
  
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('TGS_Work_Info__c');
    
    if(!isTriggerDisabled){
        String profileName = [SELECT Name FROM Profile WHERE Id= :UserInfo.getProfileId()].Name; 
        List<QueueSobject> cola = [SELECT QueueId FROM QueueSobject WHERE Queue.Name = 'UDO'];
        
        if (TGS){
            List<TGS_Work_Info__c> workInfos = [SELECT iD, TGS_Description__c, TGS_ExternalID__c, TGS_Case__r.TGS_Ticket_Id__c, TGS_Case__r.RecordType.DeveloperName,TGS_Case__r.Type, TGS_Case__r.TGS_Service__c, TGS_Case__r.OwnerId, TGS_Case__r.TGS_UDO_Update__c, TGS_Case__r.Status FROM TGS_Work_Info__c WHERE Id in :Trigger.newMap.keySet()];
           
            for (TGS_Work_Info__c workInfo: workInfos){
                System.debug(workInfo.TGS_Case__r.RecordType.DeveloperName);
                if(workInfo.TGS_Case__r.RecordType.DeveloperName.equals('Order_Management_Case') && workInfo.TGS_Case__r.TGS_Service__c!=null && (workInfo.TGS_Case__r.TGS_Service__c.contains(Constants.PRODUCT_SMART_M2M)||workInfo.TGS_Case__r.TGS_Service__c.contains(Constants.PRODUCT_M2M_JASPER))){
                             String body = '{"code":"update","value":"'+workInfo.TGS_Description__c+'"}';
                             System.debug(body);
                             /* Call UDO Web Service */
                             if (!TGS_CallRodWs.inFutureContextWI && TGS_CallRodWs.firstRunWI && Trigger.isInsert && workInfo.TGS_Case__r.OwnerId == cola[0].QueueId && workInfo.TGS_Case__r.TGS_UDO_Update__c == false && workInfo.TGS_Case__r.Status == 'Assigned' && !profileName.equals(Constants.PROFILE_TGS_INTEGRATION_USER)){
                                 //TGS_CallRodWs.firstRunWI = false;
        
                                 TGS_CallUdo.invokeUDO('POST', 'api/tt/contacts/'+workInfo.TGS_Case__r.TGS_Ticket_Id__c+'/annotations/', body,workInfo.Id);
                             }    
                             
                }    
                
             }   

             
             if (!TGS_CallRodWs.inFutureContextWI && TGS_CallRodWs.firstRunWI){
                 TGS_CallRodWs.firstRunWI = false;
                 TGS_CallRodWs.invokeWebServiceRODWorkInfo(Trigger.newMap.KeySet(), Trigger.isUpdate);
             }
        }
    }
}