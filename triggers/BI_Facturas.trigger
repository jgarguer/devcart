trigger BI_Facturas on BI_Facturas__c (before insert, after insert) {
	
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Methods executed by the BI_Facturas trigger.
    
    History

    <Date>              <Author>            <Description>
    05/08/2014          Antonio Moruno      Initial version
    06/06/2016          Humberto Nunes      Add CreateRegistroDesemp
    20/07/2017          Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Facturas__c');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore)
        {
            if (trigger.isInsert)
            {
        		BI_FacturasMethods.createRegion_Facturas(trigger.new,null,null);               
            } 
        }

        if (trigger.isAfter)
        {
            if (trigger.isInsert)
            {
                BI_FacturasMethods.CreateRegistroDesemp(trigger.new);  
                BI_FacturasMethods.Create_RDD_NCJ_FAC(trigger.new);                             
            } 
        }
    }
}