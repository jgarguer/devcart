trigger CaseComment on CaseComment (after insert, before insert) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Trigger to manage case comments
    
    History:
    
    <Date>            <Author>          <Description>
    19/08/2014        Ana Escrich       Initial version
    20/07/2017        Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    BI_bypass__c bypass = BI_bypass__c.getInstance();
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean BI = userTGS.TGS_Is_BI_EN__c;

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('CaseComment');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore){
            if (trigger.isInsert){
                if(BI){
                    System.debug('BI:' + BI+' bypass.BI_Migration__c: '+bypass.BI_Migration__c);
                    if(!bypass.BI_Migration__c) {
                        PCA_CaseCommentMethods.validateCaseStatusBIEN(Trigger.new);
                    }
                }
            }
            //if (trigger.isUpdate){
            //}
            //if (trigger.isDelete){
            //}
        }else { 
            if (trigger.isInsert){ 
            	PCA_CaseCommentMethods.sendEmail(Trigger.New);
                PCA_CaseCommentMethods.reopenPERCases(trigger.newMap);
            }
            //else if (trigger.isDelete){
            //} 
            //else 
            //if(trigger.isUpdate){
            //}       
        }
    }
}