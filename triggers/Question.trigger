trigger Question on Question (after update) {

	//GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Question');
    
    if(!isTriggerDisabled){
		if (trigger.isAfter){
	        if (trigger.isUpdate){
	        	BI_QuestionMethods.questionCases(trigger.new, trigger.old);
	        }
	    }
	}
}