/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Geraldine Sofía Perez Montes (GSPM)
    Company:       Accenture LTDA
    Description:   Methods executed by BI_SUB_PedidoSubsidioMethods Triggers 
    
    History:
     
    <Date>                  <Author>                			<Change Description>
    23/02/2018              Geraldine Sofía Perez Montes           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

trigger BI_SUB_PedidoSubsidio on BI_SUB_Pedido_Subsidio__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
	Boolean BI = userTGS.TGS_Is_BI_EN__c;

		if (Trigger.isBefore) {
	    	//call your handler.before method
	    	if(trigger.isUpdate){
	    		if(BI){
	    			BI_SUB_PedidoSubsidioMethods.NoCreatePedido(trigger.new, trigger.old);
	    		}
	    	}
	    
		} else if (Trigger.isAfter) {
	    	if (trigger.isInsert){	            
	        }
	    	if(trigger.isUpdate){
	    		if(BI){
	    			  boolean wasFired_UpdateCaseSubsidio = NETriggerHelper.getTriggerFired('UpdateCaseSubsidio');
	    			if(!wasFired_UpdateCaseSubsidio)
               		{
	    			  	system.debug('>UpdateCaseSubsidio< ');
               			NETriggerHelper.setTriggerFired('UpdateCaseSubsidio');
	    				BI_SUB_PedidoSubsidioMethods.UpdateCaseSubsidio(trigger.new, trigger.old);
	    			}
	    		}

	    	}
	    
		}
}