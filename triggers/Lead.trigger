trigger Lead on Lead (before insert, before update) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Methods executed by the Lead trigger.
    
    History

    <Date>            <Author>          <Description>
    04/08/2014        Ana Escrich       Initial version
    21/01/2016        Micah Burgos      Change !bypass.BI_migration__c to !bypass.BI_skip_trigger__c. BI_migration__c is used for ValidationRules and some code validations and BI_skip_trigger__c disable the trigger.    
    20/07/2017        Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    BI_bypass__c bypass = BI_bypass__c.getInstance();

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Lead');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore && !bypass.BI_skip_trigger__c){
            if (trigger.isInsert){
                if(!bypass.BI_migration__c){
                    BI_LeadMethods.validateId(trigger.old, trigger.new);
                    
                    BI_LeadMethods.validateSegment(trigger.new, trigger.old);
                }   
            }
            if (trigger.isUpdate){
                if(!bypass.BI_migration__c){
                    BI_LeadMethods.validateId(trigger.old, trigger.new);
                   
                    BI_LeadMethods.validateSegment(trigger.new, trigger.old); 
                }
            }
            //if (trigger.isDelete){    
            //}
        }else if(!bypass.BI_skip_trigger__c) { 
            //if (trigger.isInsert){
            //}
            //else if (trigger.isUpdate){
            //} 
            //else if (trigger.isDelete){
            //} 
                  
        }
    }
}