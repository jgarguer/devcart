trigger BI_Descuento_para_modelo on BI_Descuento_para_modelo__c (before insert, before update, before delete) {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by the BI_Facturaccion trigger.
    
    History

    <Date>            <Author>          <Description>
    09/03/2015      Ignacio Llorca      Initial version
    20/07/2017      Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    BI_bypass__c bypass = BI_bypass__c.getInstance();

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Descuento_para_modelo__c');
    
    if(!isTriggerDisabled){    
        if(!bypass.BI_migration__c){
        
            if (trigger.isBefore){
                if (trigger.isInsert){
                    BI_DescuentoModeloMethods.blockEdition(trigger.new);
                    BI_DescuentoModeloMethods.insertAcc(trigger.new);
                } 
                if (trigger.isUpdate){
                    BI_DescuentoModeloMethods.blockEdition(trigger.new);
                }
                if (trigger.isDelete){
                    BI_DescuentoModeloMethods.blockDelete(trigger.old);
                }
            }else {  
                /*if (trigger.isInsert){ 
                    
                }   
                else if (trigger.isUpdate){
                    
                } 
                else if (trigger.isDelete){
                } 
                */
            }
        }
    }
}