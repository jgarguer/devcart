trigger Milestone1_Time_Trigger on Milestone1_Time__c (before insert, before update) 
{

	//GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Milestone1_Time__c');
    
    if(!isTriggerDisabled){
		if(Trigger.isBefore)
		{
			Milestone1_Time_Trigger_Utility.handleTimeBeforeTrigger(Trigger.new);
		}
	}
}