trigger BI_EmailMessage on EmailMessage (before insert, after insert, before update, after update) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Álvaro Hernando Gavilán
    Company:       Telefonica Global Technology
    Description:   Methods executed by the EmailMessage trigger.
    
    History

    <Date>          <Author>                <Description>
    11/06/2015      Álvaro Hernando         Initial version
    25/10/2016      Guillermo Muñoz         Trigger merged with BI2_EmailMessage
    20/07/2017      Guillermo Muñoz         Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('EmailMessage');

    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    if(!isTriggerDisabled){
        if (trigger.isBefore){
            if (trigger.isInsert){
            
            }
            if (trigger.isUpdate){

            }
            //if (trigger.isDelete){
            //}
        }else if(trigger.isAfter) { 
            if (trigger.isInsert){ 
                BI_EmailMessageMethods.fillFieldsARGCobranzas(trigger.new);
                if(BI) {
                    BI_EmailMessageMethods.fillE2CFields(Trigger.new);
                    BI2_EmailMessage.setAssignations(Trigger.new);
                }

            }
            //else if (trigger.isUpdate){

            //} 
            //else if (trigger.isDelete){
            //}     
        }
    }
} // end class