/*------------------------------------------------------------
    Author:         Miguel Angel Galindo 
    Company:        Deloitte
    Description:    Class to merge all triggers from the Configuration (NE__Order__c) object
    History
    <Date>          <Author>                <Change Description>
    09-Dic-2014     Miguel Angel Galindo    Merge NECheckOrder and GenerateOrderCase Triggers
    24/07/2015      Francisco Ayllon (CoE)  Added BI_BI_OrderMethods.updateOppFields methods && edited code to avoid errors for TGS methods 
    01/12/2015      Fernando Arteaga        BI_EN - CSB Integration
    16/02/2016      Fernando Arteaga        Code optimization to save SOQL queries
    03/03/2016      Oscar Iluiano           Added IO
    16/03/2016      Fernando Arteaga        Fill NE__Service_Account__c in OI to accomplish Installation_Point__c filter
    28/03/2016      Jose Miguel Fierro      Added TGS_Order_Handler method calls
    04/04/2016      Jose Miguel Fierro      Added before insert
    11/04/2016      Jose Miguel Fierro      Added if(!Triger.isInsert)
    13/04/2016      Guillermo Muñoz         Added BI_OrderMethods.setOrderStatus() and BI_OrderMethods.setOrderVersion() methods
    14/04/2016      Jose Miguel Fierro      Removed reference to fillRequiredInformation_insert and fillRequiredInformation_update
    15/04/2016      Jose Miguel Fierro      Added sanity check before overwriting CI fields
    25/04/2015      Guillermo Muñoz         Added BI_OrderMethods.setEditableDiscount() method  
    19/07/2016      Jorge Galindo           If coming from Autosplit don't launch triggers in Update
    03/10/2016      Jose Miguel Fierro      Re-Added reference fillRequiredInformation_insert, to fill in Order's Holding Name
    08/11/2016      Alvaro Garcia           Added setStatus in afterupdate
    10/11/2016      Alvaro García           Added BI_O4_OrderMethods.desactiveQuote
    11/11/2016      Alvaro García           Added BI_O4_OrderMethods.setNumberProduct
    20/02/2017      Gawron, Julián          Added actualizarAccountCoberturaDigital
    15/05/2017      Cristina Rodríguez      Create Method in TGS_Order_Handler. Refactor
    20/07/2017      Guillermo Muñoz         Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    16/10/2017      Javier Almirón          Added call to BI_O4_OrderMethods.updateNBAV
    26-02-2018      Manuel Ochoa            Added call to method setOrderStatus to change order status when comes from bulk
	04-04-2018		Manuel Ochoa			Call to setOrderStatus disabled on before update 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
trigger TGS_Order_Trigger on NE__Order__c (after insert, after update, before update, before insert, before delete) {
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    boolean wasFired = NETriggerHelper.getTriggerFired('TGS_Order_Trigger');
    boolean wasFired_NeCheckOrder = NETriggerHelper.getTriggerFired('TGS_Order_Handler.NeCheckOrder'); //Add by Micah Burgos y Alfonso
    /*Álvaro López - 13/07/2017 - Evita que valide el RBMA al modificar una OR*/
    NETriggerHelper.setTriggerFiredTest('BI_OpportunityMethods.validatePreviousRecurringCharge', true);

    system.debug('TGS_Order_Trigger --> TGS: ' + userTGS.TGS_Is_TGS__c + ' - BI:' + userTGS.TGS_Is_BI_EN__c + ' - wasFired: ' + wasFired +' - wasFired_NeCheckOrder: ' + wasFired_NeCheckOrder );
    system.debug('TGS_Order_Trigger --> isAfter: ' + Trigger.isAfter + ' - isBefore:' + Trigger.isBefore  + ' - isUpdate: ' + Trigger.isUpdate );
    //BI_GlobalVariables.count_Order_Updates++;
    //system.debug('TGS_Order_Trigger --> Iteración : ' +  BI_GlobalVariables.count_Order_Updates );
    
    //JGL 19/07/2016 Si viene desde el autosplit de la Opty no hay que ejecutar este código
    boolean wasFiredAutosplit   =   NETriggerHelper.getTriggerFired('BI_FVI_AutoSplit');    
    system.debug('TGS_Order_Trigger --> isAfter: ' + Trigger.isAfter + ' - isBefore:' + Trigger.isBefore  + ' - isUpdate: ' + Trigger.isUpdate + '- wasFiredAutosplit: ' + wasFiredAutosplit);
    //END JGL 19/07/2016
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('NE__Order__c');
    
    //JAG 26/01/2018
    BI_bypass__c bypass = BI_bypass__c.getInstance();
    
    if(!isTriggerDisabled){
        if(Trigger.isAfter && !bypass.BI_skip_trigger__c){

            /////////////////START TGS///////////////////////[Micah Burgos y Alfonso] Éste trigger debe de saltar para BI y TGS 
            //if(TGS)  // 15/02/2018 GSPM: Descomentado para evitar ejecución para BI
            //{ // 19/03/2018 GSPM: Comentado para ejecución para BI
                TGS_Order_Handler.NeCheckOrder(Trigger.new, wasFired_NeCheckOrder, Trigger.oldMap);
            //}

            /////////////////START COE/////////////////////
            if(trigger.isInsert){
                //if(BI && !BI_GlobalVariables.stopUpdateOpp){
                //   BI_OrderMethods.updateOppFields(trigger.new,trigger.old);
                //}
                if(BI){
                    BI_OrderMethods.setOrderStatus(trigger.new);
                    BI_O4_OrderMethods.desactiveQuote(trigger.new);
                    BI_OrderMethods.actualizarAccountCoberturaDigital(trigger.new, null); //JEG 17/02/2017
                }
            }
            /////////////////END COE///////////////////////


            /////////////////END TGS/////////////////////// 
            if(Trigger.isUpdate)
            {
                
                
                
                /////////////////START TGS///////////////////// 
                if(TGS && !wasFiredAutosplit) // JGL 19/07/2016 added wasFiredAutosplit
                {
                    TGS_Order_Handler.generateOrderCaseAfter(Trigger.new);
                    // START CRM 15/05/2017 - Added Method. Refactor TGS_Order_Trigger.trigger
                    TGS_Order_Handler.updateItems(Trigger.newMap,Trigger.oldMap);
                    /*******OLD                
                    Id orderRecordTypeId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, Constants.RECORD_TYPE_ORDER);
                    list<NE__Order__c> listOfConfigurations = new list<NE__Order__c>([SELECT Id, Site__c, NE__ServAccId__c, NE__AccountId__r.HoldingId__c, NE__ServAccId__r.HoldingId__c, // FAR 16/03/2016 - Added NE__ServAccId__c // JMF 15/04/2016 - Added HoldingId
                                                                                      (SELECT Id, Installation_Point__c, NE__Service_Account__c // FAR 16/03/2016 - Added NE__Service_Account__c
                                                                                       FROM NE__Order_Items__r)
                                                                                      FROM NE__Order__c 
                                                                                      WHERE Id IN: Trigger.new AND RecordTypeId =: orderRecordTypeId]);
                    
                    list<NE__OrderItem__c> orderItemsToUpd = new list<NE__OrderItem__c>();
                    
                    for(NE__Order__c orderTrigger : listOfConfigurations)
                    {
                        // JMF 15/04/2016 - Prevent bad data being copied to OrderItems
                        // JMF 19/04/2016 - Allow BU to be empty
                        if(orderTrigger.NE__ServAccId__c != null && orderTrigger.NE__AccountId__r.HoldingId__c != orderTrigger.NE__ServAccId__r.HoldingId__c) {
                            Trigger.newMap.get(orderTrigger.Id).NE__ServAccId__c.addError('The selected Business Unit must be within the Account\'s hierarchy');
                            continue;
                        }
                        NE__Order__c oldOrder = Trigger.oldMap.get(orderTrigger.Id);
                        if(orderTrigger.Site__c != oldOrder.Site__c)
                        {
                            for(NE__OrderItem__c orderItem : orderTrigger.NE__Order_Items__r)
                            {
                                if(orderItem.Installation_point__c != orderTrigger.Site__c)
                                {
                                    orderItem.Installation_point__c = orderTrigger.Site__c;
                                    // FAR 16/03/2016 - Fill the BU with the BU filled in the order (NE__ServAccId__c)
                                    orderItem.NE__Service_Account__c = orderTrigger.NE__ServAccId__c;
                                    orderItemsToUpd.add(orderItem);
                                }
                            }
                        }
                    }
                    
                    if(orderItemsToUpd.size() > 0) {
                        TGS_CallRodWs.inFutureContextCI = true; 
                            update orderItemsToUpd;
                        TGS_CallRodWs.inFutureContextCI = false;
                        
                    } 
                    */
                }
                
                if (BI) {
                    // FAR 27/09/2016
                    BI_O4_OrderMethods.setOpptyFieldsOnQuoteDelivery(Trigger.new, Trigger.oldMap);
                    BI_O4_OrderMethods.setStatus(Trigger.new, Trigger.oldMap);
                    BI_O4_OrderMethods.setNumberProduct(Trigger.new, Trigger.oldMap);
                    BI_OrderMethods.actualizarAccountCoberturaDigital(trigger.new, trigger.old); //JEG 17/02/2017
                    BI_O4_OrderMethods.updateNBAV(Trigger.new, Trigger.oldMap);
                }
                /////////////////END TGS///////////////////////
                /////////////////START COE/////////////////////
                //if(BI && !BI_GlobalVariables.stopUpdateOpp){
                //    BI_OrderMethods.updateOppFields(trigger.new,trigger.old);
                //}
                /////////////////END COE///////////////////////
                
                // FAR 23/11/2015
                if (BI && !wasFiredAutosplit) // JGL 19/07/2016 added wasFiredAutosplit
                {
                    BI_CSB_OrderMethods.createInternalCases(Trigger.new, Trigger.old);
                    // FAR 22/09/2016
                    //BI_O4_OrderMethods.activateQuote(Trigger.new, Trigger.oldMap);

                }
                // END FAR 23/11/2015
            }
            
        }else if(Trigger.isBefore && !bypass.BI_skip_trigger__c){
            /////////////////START TGS///////////////////////
            // Deloitte (GAB) 22/05/2015 - If the user is from integration, don't execute this code (for SMDM integration not creating Cases)
            // FAR 16/02/2016 - Query is not used anymore since the condition after is commented.
            //Id profileId = userinfo.getProfileId(); 
            //String profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;
            // END FAR 16/02/2016
            //if(TGS && profileName != Constants.PROFILE_TGS_INTEGRATION_USER)
            
            if(Trigger.isUpdate){
                if(TGS && !wasFiredAutosplit) { //JGL 19/07/2016
                    TGS_CostCenterHandler.fillCostCenter(Trigger.new,Trigger.old);
                    //TGS_Order_Handler.fillRequiredInformation_update(Trigger.newMap, Trigger.oldMap); // JMF 14/04/2016 - Replaced with lookup filter
                }
                if(BI){
                    // 26-02-2018 - Manuel Ochoa - Added method to change order status when comes from bulk
                    // 04-04-2018		Manuel Ochoa			Call to setOrderStatus disabled on before update 
                    //BI_OrderMethods.setOrderStatus(trigger.new);
                    // 26-02-2018 - Manuel Ochoa                
                    BI_OrderMethods.setEditableDiscount(Trigger.new, Trigger.old);
                }
            }

            if(Trigger.isInsert){
                if(TGS) {
                    //TGS_CostCenterHandler.fillCostCenter(Trigger.new,Trigger.old); // JMF 04/04/2016 - Wasn't called before
                    //TGS_Order_Handler.fillRequiredInformation_insert(Trigger.new); // JMF 14/04/2016 - Replaced with Lookup filter
                    TGS_Order_Handler.fillRequiredInformation_insert(Trigger.new); // JMF 03/10/2016 - Added to fill in the Holding Name
                }
                if(BI){
                    BI_OrderMethods.setOrderVersion(Trigger.new);
                    BI_OrderMethods.setEditableDiscount(Trigger.new, null);
                    BI_O4_OrderMethods.setQuoteVersion(Trigger.new);
                }
            }

            if(Trigger.isDelete) // OI 01-03-2016
            {
                System.debug('uno');
        
                if(TGS)
                {      
                    System.debug('I m ');
                    // JMF 27/10/2016 - Prevent regression in case status from deleting case
                    if(!NETriggerHelper.getTriggerFired('Case_Return_to_InProgress')) {
                        TGS_CostCenterHandler.deleteCase(Trigger.old);
                    }
                }
            }
            
            
            if(!Trigger.isDelete && !Trigger.isInsert) // OI 01-03-2016 // JMF 11-04-2016 !isInsert
            {
                if(TGS && !wasFiredAutosplit)//JGL 19/07/2016
                {
                    system.debug('[trigger.TGS_Order_Trigger] Trigger will be executed: ' + !wasFired);
                    if(!wasFired)
                    {
                        // START CRM 15/05/2017 - Added Method. Refactor TGS_Order_Trigger.trigger
                        TGS_Order_Handler.updateItemsBefore(Trigger.new, Trigger.newMap, Trigger.oldMap);
                        /*******OLD 
                        // NE 12/11/2015: Added filter on new Orders in order to call generateOrderCaseBefore after they are fully updated.
                        list<NE__Order__c> newOrderList = new list<NE__Order__c>();
                        
                        for(NE__Order__c ord : Trigger.new)
                        {
                            if((ord.NE__Configuration_Type__c == 'New' && ord.NE__AssetEnterpriseId__c == null) || (ord.NE__Configuration_Type__c != 'New' && ord.NE__AssetEnterpriseId__c != null))
                                newOrderList.add(ord);
                        }
                        
                        if(newOrderList.size() > 0)
                        {
                            NETriggerHelper.setTriggerFired('TGS_Order_Trigger');
                            TGS_Order_Handler.generateOrderCaseBefore(newOrderList, Trigger.newMap, Trigger.oldMap);
                        }
                        */
                        // END CRM 15/05/2017 - Added Method. Refactor TGS_Order_Trigger.trigger
                    }
                }
            }
            /////////////////END TGS///////////////////////
        }
    }
}