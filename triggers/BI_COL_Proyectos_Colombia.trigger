trigger BI_COL_Proyectos_Colombia on BI_COL_Proyectos_Colombia__c (before insert) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       NEAborda
    Description:   Methods executed by the BI_COL_Proyectos_Colombia__c trigger.
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    08/06/2016                      Guillermo Muñoz             Initial version
    20/07/2016                      Guillermo Muñoz             Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	//GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_COL_Proyectos_Colombia__c');
    
    if(!isTriggerDisabled){
		if(trigger.isBefore){

			if(trigger.isInsert){

				BI_COL_Proyectos_ColombiaMethods.setOwner(trigger.new, null);
			}
		}
		/*else{

		}*/
	}
}