trigger BI_Log on BI_Log__c (after insert) {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Trigger on BI_Log__c
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    08/03/2016                      Guillermo Muñoz             Initial version
		20/07/2017      				Guillermo Muñoz     		Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Log__c');
    
    if(!isTriggerDisabled){
		/*if(Trigger.isBefore){

		}*/
		if(Trigger.isAfter){
			if(Trigger.isInsert){
				BI_LogMethods.checkLogsToDelete(trigger.new);
			}
		}
	}
}