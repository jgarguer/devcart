trigger TriggerFactura on Factura__c (before update) {

	//GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Factura__c');
    
    if(!isTriggerDisabled){
		if(trigger.isUpdate){
			for(String facturaId : Trigger.newMap.keySet()){
				//si se intenta cambiar alguno de los siguientes datos se debe validar que la factura no tenga productos asociados
				if(Trigger.oldMap.get(facturaId).IVA__c != Trigger.newMap.get(facturaId).IVA__c
					|| Trigger.oldMap.get(facturaId).Cliente__c != Trigger.newMap.get(facturaId).Cliente__c)
				{
				/*
					List<Equipo_en_Sitio__c> les = [SELECT Id, Name FROM Equipo_en_Sitio__c WHERE Factura__c =: facturaId LIMIT 1];
					if(les != null && les.size() > 0)
						//Trigger.newMap.get(facturaId).addError('No se puede cambiar el IVA o el Cliente de la factura.');
				
				*/		
				}
				 
			}

			/* Developed By: juliom.ortega@gmail.com	
			*  Created Date: Oct 8st 2014
			*
			*/

			//==================================
			// Validamos que la sociedad ha cambiado
			SociedadFacturadora classSociedad = new SociedadFacturadora ();
			List<Factura__c> lsFacturas = new List<Factura__c> ();
			for  (Factura__c factura : Trigger.new){
				if (factura.Sociedad_Facturadora__c != Trigger.oldMap.get (factura.Id).Sociedad_Facturadora__c){
					lsFacturas.add(factura);
				}
			}
			if (lsFacturas != null && lsFacturas.size()>0){
				classSociedad.inicio(lsFacturas);
			}
		}
	}
}