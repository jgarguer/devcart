trigger TriggerPedidoPE on Pedido_PE__c ( after insert, after update) {

	//GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Pedido_PE__c');
    
    if(!isTriggerDisabled){
		String perfilId = '';
		for (Profile perfil : [SELECT Id, Name FROM Profile where Name  = 'Administrador del sistema']){
			 perfilId =perfil.Id;   
		}
		
		//if (userinfo.getProfileId() !=perfilId ){
			ADQ_DivisaUtility divisaUtility = new ADQ_DivisaUtility();
			List<CurrencyTypeFacturacion__c> systemcurrencies = divisaUtility.systemcurrencies;
			Set<String> setProgramaciones = new Set<String>();
			
			if(Trigger.isInsert || Trigger.isUpdate){
			//if(Trigger.isUpdate){
				Map<String, ProgramacionCurrencyType__c> mapProgramacionCurrencyType = new Map<String, ProgramacionCurrencyType__c>();
				Map<String, List<ProgramacionCurrencyType__c>> mapProgramacionCurrencyType_Buscar= new Map<String, List<ProgramacionCurrencyType__c>>();
				//guardo los ids de todas las programaciones
				for(Pedido_PE__c p : Trigger.new){
					setProgramaciones.add(p.Programacion__c);
				}
		 
				for(ProgramacionCurrencyType__c lpcttemp : [Select Id, IsoCode__c, IsCorporate__c, ConversionRate__c, Programaci_n__c 
																				From ProgramacionCurrencyType__c Where Programaci_n__c IN :setProgramaciones ]){
		
					if (!mapProgramacionCurrencyType_Buscar.containsKey(lpcttemp.Programaci_n__c)){
							mapProgramacionCurrencyType_Buscar.put (lpcttemp.Programaci_n__c, new List<ProgramacionCurrencyType__c> ());
					}		
					mapProgramacionCurrencyType_Buscar.get(lpcttemp.Programaci_n__c).add(lpcttemp);
		
				}
				for(String programacionId : setProgramaciones){
					for(CurrencyTypeFacturacion__c ct : systemcurrencies){
						//IsoCode, IsCorporate, Id, DecimalPlaces, ConversionRate
						ProgramacionCurrencyType__c pct = new ProgramacionCurrencyType__c();
						pct.Programaci_n__c = programacionId;
						pct.IsoCode__c = ct.IsoCode__c;
						pct.IsCorporate__c = ct.IsCorporate__c;
						//pct.DecimalPlaces__c = ct.DecimalPlaces;
						pct.ConversionRate__c = ct.ConversionRate__c;
						mapProgramacionCurrencyType.put(pct.Programaci_n__c + '_' + pct.IsoCode__c, pct);
						setProgramaciones.add(pct.Programaci_n__c);
						//Si ya tenemos 200 records los actualizamos/insertamos
						/*
						if(mapProgramacionCurrencyType.size() == 200){
							for(List<ProgramacionCurrencyType__c>  lpct : [Select Id, IsoCode__c, IsCorporate__c, ConversionRate__c, Programaci_n__c 
																				From ProgramacionCurrencyType__c Where Programaci_n__c IN :setProgramaciones And IsoCode__c =: ct.IsoCode__c]){
								for(ProgramacionCurrencyType__c pctItem : lpct){
									String key = pctItem.Programaci_n__c + '_' + pctItem.IsoCode__c;
									if(mapProgramacionCurrencyType.containsKey(key)){
										pctItem.IsCorporate__c = mapProgramacionCurrencyType.get(key).IsCorporate__c;
										//pctItem.DecimalPlaces__c = mapProgramacionCurrencyType.get(key).DecimalPlaces__c;
										pctItem.ConversionRate__c = mapProgramacionCurrencyType.get(key).ConversionRate__c;
										mapProgramacionCurrencyType.put(key, pctItem);
									}
								}
							}
							upsert mapProgramacionCurrencyType.values();
							mapProgramacionCurrencyType.clear();
							//setProgramaciones.clear();
						}
						*/
		
						if (mapProgramacionCurrencyType_Buscar != null){
							//if (mapProgramacionCurrencyType_Buscar.get () )
							//mapProgramacionCurrencyType.put(pct.Programaci_n__c + '_' + pct.IsoCode__c, pct);
							
							if (mapProgramacionCurrencyType_Buscar.get(programacionId)!= null){
								for(ProgramacionCurrencyType__c pctItem2 : mapProgramacionCurrencyType_Buscar.get(programacionId)){
									String key = pctItem2.Programaci_n__c + '_' + pctItem2.IsoCode__c;
										if(mapProgramacionCurrencyType.containsKey(key)){
											pctItem2.IsCorporate__c = mapProgramacionCurrencyType.get(key).IsCorporate__c;
											//pctItem.DecimalPlaces__c = mapProgramacionCurrencyType.get(key).DecimalPlaces__c;
											pctItem2.ConversionRate__c = mapProgramacionCurrencyType.get(key).ConversionRate__c;
											mapProgramacionCurrencyType.put(key, pctItem2);
										}
								}
							}
							
							/*
							
							for (String llave : mapProgramacionCurrencyType_Buscar.keySet()){
								for(ProgramacionCurrencyType__c pctItem2 : mapProgramacionCurrencyType_Buscar.get(llave)){
									String key = pctItem2.Programaci_n__c + '_' + pctItem2.IsoCode__c;
									if(mapProgramacionCurrencyType.containsKey(key)){
										pctItem2.IsCorporate__c = mapProgramacionCurrencyType.get(key).IsCorporate__c;
										//pctItem.DecimalPlaces__c = mapProgramacionCurrencyType.get(key).DecimalPlaces__c;
										pctItem2.ConversionRate__c = mapProgramacionCurrencyType.get(key).ConversionRate__c;
										mapProgramacionCurrencyType.put(key, pctItem2);
									}
								}	
		
							}
							*/
		
						}
		
						//upsert mapProgramacionCurrencyType.values();
							//mapProgramacionCurrencyType.clear();
		
					}
				}
				//Si quedaron objetos sin actualizar/insertar
				if(mapProgramacionCurrencyType.size() > 0){
					for(List<ProgramacionCurrencyType__c>  lpct : [Select Id, IsoCode__c, IsCorporate__c, ConversionRate__c, Programaci_n__c From ProgramacionCurrencyType__c Where Programaci_n__c IN :setProgramaciones]){
						for(ProgramacionCurrencyType__c pctItem : lpct){
							String key = pctItem.Programaci_n__c + '_' + pctItem.IsoCode__c;
							if(mapProgramacionCurrencyType.containsKey(key)){
								pctItem.IsCorporate__c = mapProgramacionCurrencyType.get(key).IsCorporate__c;
								//pctItem.DecimalPlaces__c = mapProgramacionCurrencyType.get(key).DecimalPlaces__c;
								pctItem.ConversionRate__c = mapProgramacionCurrencyType.get(key).ConversionRate__c;
								mapProgramacionCurrencyType.put(key, pctItem);
							}
						}
					}
					
				}
				
				if ( mapProgramacionCurrencyType.size() > 0 && mapProgramacionCurrencyType != null){
					upsert mapProgramacionCurrencyType.values();
					mapProgramacionCurrencyType.clear();
					setProgramaciones.clear();
				}
			}
		//}

	}
}