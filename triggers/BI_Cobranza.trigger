trigger BI_Cobranza on BI_Cobranza__c (before insert) {

  //GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
  Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Cobranza__c');
  
  if(!isTriggerDisabled){
	 if (trigger.isBefore){
        if (trigger.isInsert){
			BI_FacturasMethods.createRegion_Facturas(null,null,trigger.new);

        } 
      //  if (trigger.isUpdate){
      //  }
        //if (trigger.isDelete){
        //}
    }else {  
       // if (trigger.isInsert){ 
      //  }   
      //  else if (trigger.isUpdate){
        	        	
       // } 
        //else if (trigger.isDelete){
        //} 
              
    }
  }
}