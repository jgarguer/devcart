trigger Quote on Quote (before insert, after insert, before update, after update) {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Salesforce.com
    Description:   Trigger to control methods from Quote
    
    History:
    
    <Date>            <Author>                              <Description>
    03/01/2017		  Alvaro García							Added activeProposalItem
    10/07/2017        Javier Almirón                        Added migAppOppQuote
    20/07/2017        Guillermo Muñoz                       Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    01/09/2017        Alberto Fernández                     Added call to BI_O4_Quote_Methods.selectQuoteSkipGates() on before update
    20/10/2017		Oscar Bartolo			Added fillBusinessFields
    30/10/2017        Javier Almirón García                 Added call to BI_O4_Quote_Methods.userSkipGates
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    //Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    
    BI_bypass__c bypass = BI_bypass__c.getInstance();

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Quote');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore && !bypass.BI_skip_trigger__c){
            if (trigger.isInsert){
                if(BI){
                    BI_O4_Quote_Methods.userSkipGates(trigger.new);
               
                }
               
            }
            if (trigger.isUpdate){
                if(BI){
    	            BI_O4_Quote_Methods.selectQuoteSkipGates(trigger.new, trigger.oldMap);
		    BI_O4_Quote_Methods.fillBusinessFields(trigger.new);
                }
            }
            
        }else if(!bypass.BI_skip_trigger__c) { 
            if (trigger.isInsert){ 
                if(BI){
                    BI_O4_Quote_Methods.activeProposalItem(trigger.new,null);
                    BI_O4_Quote_Methods.migAppOppQuote(trigger.new, trigger.oldMap);
                }
            }
            
            if(trigger.isUpdate){
                if(BI){
                   BI_O4_Quote_Methods.activeProposalItem(trigger.new,trigger.old);
                   BI_O4_Quote_Methods.migAppOppQuote(trigger.new, trigger.oldMap);
                }
                
            }       
        }
    }
}