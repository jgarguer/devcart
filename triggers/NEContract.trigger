trigger NEContract on NE__Contract__c (before insert, before update, before delete, after insert, 
	after update, after delete, after undelete) {

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián E.
    Company:       Accenture
    Description:   Trigger para controlar los métodos llamados de NE__Contract__c ('Acuerdos Marco')
    History
    11/09/2017      Gawron, Julián E.     Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    BI_bypass__c bypass = BI_bypass__c.getInstance();
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c;   
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('NE__Contract__c');

    System.debug('Entrando en NEContract');
    if(!isTriggerDisabled){
 	    if (trigger.isBefore){
            if (Trigger.isInsert){
                if(BI){

                    //Validaciones por código
                    if(!bypass.BI_migration__c){
                        BI_NEContractMethods.verificarPermisos(Trigger.new, Trigger.old);                        
                    }
                }
    		}else if(Trigger.isUpdate){
                if (BI){

                    //Validaciones por código
                    if(!bypass.BI_migration__c){
                        BI_NEContractMethods.verificarPermisos(Trigger.new, Trigger.old);
                    }
                }
    		}else if(Trigger.isDelete){
                if (BI){

                	//Validaciones por código
                    if(!bypass.BI_migration__c){
                        BI_NEContractMethods.verificarPermisos(Trigger.new, Trigger.old);
                    }
                }
    		}
		} else if (Trigger.isAfter) {
		//isAfter
           if (Trigger.isInsert){
                if(BI){
                    BI_NEContractMethods.SetStartEndDateContractHeader(Trigger.new, Trigger.old, Trigger.isInsert, Trigger.isUpdate);
                }
            }else if(Trigger.isUpdate){
                if (BI){
                    BI_NEContractMethods.SetStartEndDateContractHeader(Trigger.new, Trigger.old, Trigger.isInsert, Trigger.isUpdate);
                }
            }
		}
	}//!isTriggerDisabled
}