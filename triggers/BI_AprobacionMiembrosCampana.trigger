trigger BI_AprobacionMiembrosCampana on BI_Aprobacion_Miembros_Campana__c (after update) {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Trigger on BI_AprobacionMiembrosCampana
    
    History:
    
    <Date>			  <Author> 	        <Description>
    14/04/2014        Pablo Oliva     	Initial version
    20/07/2017        Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Aprobacion_Miembros_Campana__c');

    if(!isTriggerDisabled){
        //if(trigger.isBefore){
            //if(trigger.isInsert){
            //}	
            //else if(trigger.isUpdate){ 
            //}
            //else if(trigger.isDelete){
            //}
            //else if(trigger.isUnDelete){
            //}
        //}else{
        if(trigger.isAfter){ 
            //if(trigger.isInsert){ 
            //}
            //else 
            if(trigger.isUpdate){
            	BI_AprobacionMiembrosCampanaMethods.updateCampaignMember(trigger.new, trigger.old);
            }
            //else if(trigger.isDelete){
            //}
            //else if(trigger.isUnDelete){
            //}         
        }
    }

}