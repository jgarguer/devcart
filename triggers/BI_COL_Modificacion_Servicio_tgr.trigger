/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       14-04-2015     Javier Tibamoza              Creación de la Clase
    2.0       18-08-2016     Andrea Castañeda             Mapping fields MS and CI
    3.0       29-12-2016     Geraldine Perez              Update class
****************************************************************************************************/
trigger BI_COL_Modificacion_Servicio_tgr on BI_COL_Modificacion_de_Servicio__c (after delete, after insert, after update, before insert, before update){           
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
     BI_bypass__c bypass = BI_bypass__c.getInstance();

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_COL_Modificacion_de_Servicio__c');
    
    if(!isTriggerDisabled){
        if( BI_COL_FlagforTGRS_cls.flagTriggers ){ return; }

        if( trigger.isBefore && ( Trigger.isUpdate || Trigger.isInsert ) && !bypass.BI_migration__c){
            BI_COL_Modificacion_Servicio_cls.fnCallBeforeUpdateInsert( trigger.new, trigger.old,trigger.isInsert, trigger.isUpdate );
        } 
        if( trigger.isAfter){

            if(Trigger.isUpdate && !bypass.BI_migration__c){
                BI_COL_Validates_Required_Field_cls.fnValidates_Required_Field_MS( trigger.new );
                BI_COL_Modificacion_Servicio_cls.fnCallAfterUpdateInsert( trigger.new, trigger.old,trigger.isInsert, trigger.isUpdate );
            }
            if(Trigger.isUpdate){
                BI_COL_Modificacion_Servicio_cls.sumTotalModificationService(Trigger.new, Trigger.old);
            }

            if(Trigger.isInsert){
                BI_COL_Modificacion_Servicio_cls.sumTotalModificationService(Trigger.new, null);
            } 

            if(Trigger.isDelete){
                BI_COL_Modificacion_Servicio_cls.sumTotalModificationService(null, Trigger.old);
            } 
        }
        if( Trigger.isBefore && Trigger.isUpdate && !bypass.BI_migration__c){
            BI_COL_Modificacion_Servicio_cls.fnCallBEforeUpdate(trigger.new, trigger.old, trigger.isInsert, trigger.isUpdate );
        }
         
        /*
        <Date> ---------------- 18-08-2016                         
        <Author> -------------- Andrea Castañeda (AC)
        <Company>-------------- Aborda                   
        <Change Description> -- AC: Mapeo del campo Estado de la MS con el CI
        */
        
       //06/11/2017 START GSPM:Modificación de invocación al metodo updateConfigurationItem y updateCIValEco
        boolean wasFired_updateConfigurationItem   =   NETriggerHelper.getTriggerFired('updateConfigurationItem');
        boolean wasFired_updateCIValEco   =   NETriggerHelper.getTriggerFired('updateCIValEco');
        system.debug('*wasFired_updateConfigurationItem' + wasFired_updateConfigurationItem + ' *wasFired_updateCIValEco ' + wasFired_updateCIValEco);
       
       if(Trigger.isAfter )
       {            
            if(Trigger.isInsert && !bypass.BI_migration__c) {
                
                //07/11/2016 ANCR: Se modifica validación para que el trigger se ejecute una sola vez
                if(!wasFired_updateConfigurationItem)
                {
                    NETriggerHelper.setTriggerFired('updateConfigurationItem');
                    BI_COL_Modificacion_Servicio_cls.updateConfigurationItem(Trigger.new, Trigger.old);
                }
            }
            else if ( Trigger.isUpdate && !bypass.BI_migration__c )
            {
                if(!wasFired_updateCIValEco)
                {
                    NETriggerHelper.setTriggerFired('updateCIValEco');
                    BI_COL_Modificacion_Servicio_cls.updateCIValEco(Trigger.new, Trigger.old);
                }
            }
        }
        //06/11/2017 END GSPM:Modificación de invocación al metodo updateConfigurationItem y updateCIValEco

         //21/02/2017 GSPM:
        if(Trigger.isAfter  && ( Trigger.isUpdate || Trigger.isInsert ) && !bypass.BI_migration__c )
        {
            if(BI)
            {
                if(!NETriggerHelper.getTriggerFired('BI_COL_Modificacion_Servicio_cls.updateMedioMs'))
                {
                    if(!System.isFuture())
                    BI_COL_Modificacion_Servicio_cls.updateMedioMs(Trigger.new, Trigger.old); 
                }
            }
            
        }
        //27/09/2016 GSPM:  Este metodo inicialmente se estaba llamando desde el trigger de OI - NECheckOrderItems
        //16/01/2017 START GSPM:
        //if(trigger.isAfter)
        //{
        //    if(trigger.isInsert || trigger.isUpdate )
        //    {   
        //        if(BI)
        //        {
        //            if(!NETriggerHelper.getTriggerFired('BI_NEOrderItemMethods.CalculateOR'))
        //            {
        //                if(!System.isFuture())
        //                BI_COL_Modificacion_Servicio_cls.CalculateOR(trigger.new, trigger.old);
        //            }
        //        }
        //    }
        //}
         //16/01/2017 END GSPM:
    }
}