trigger BI_Facturacion on BI_Facturacion__c (after update, before insert) {

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Methods executed by the BI_Facturaccion trigger.
    
    History

    <Date>            <Author>          <Description>
    23/07/2014        Micah Burgos      Initial version
    20/07/2017        Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Facturacion__c');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore){
            if (trigger.isInsert){
            	BI_FacturasMethods.createRegion_Facturas(null,trigger.new,null);
    			BI_FacturacionMethods.checkMaxDate(trigger.new);
            } 
            if (trigger.isUpdate){
            	

            }
            //if (trigger.isDelete){
            //}
        }else {  
            if (trigger.isInsert){ 
                
            }   
            else if (trigger.isUpdate){
            	
            	
                //Only on update because it has workflow rule.
                BI_FacturacionMethods.createTask(trigger.new); 
            } 
            //else if (trigger.isDelete){
            //} 
                  
        }
    }
}