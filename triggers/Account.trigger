trigger Account on Account (before insert, before update, after insert, after update, after delete) {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Diego Arenas
    Company:       Salesforce.com
    Description:   Trigger para controlar los métodos llamados de Account
    
    History:
    
    <Date>            <Author>                              <Description>
    11/02/2014        Diego Arenas                          Añadir la llamada a UtilTriggers.setStrategicContextLastUpdate en before insert/update>
    20/05/2014        Ignacio Llorca                        BI_AccountMethods.validateId added
    26/05/2014        Pablo Oliva                           BI_AccountMethods.validateRiskAndFraud added
    24/06/2014        Ignacio Llorca                        BI_AccountMethods.reopenSuspendedopps added
    10/07/2014        Pablo Oliva                           Methods BI_AccountMethods.updateIsParent, BI_AccountMethods.updateNumberChilds added
    16/03/2015        Miguel Angel Galindo                  Merge with TGS Trigger
    18/03/2015        Marta Garcia                          BI Encapsulation 
    22/06/2015        Carlos Carvajal                       Consumo servcios web  Colombia Canal Online y notificaciones callejero
    02/11/2015        Gustavo Kupcevich                     BI_SCP_BigDeals_Triggers.updateAccountAfter
    11/12/2015        Fernando Arteaga                      BI_EN - CSB Integration
    21/01/2016        Micah Burgos                          Change !bypass.BI_migration__c to !bypass.BI_skip_trigger__c. BI_migration__c is used for ValidationRules and some code validations and BI_skip_trigger__c disable the trigger. 
    25/04/2016        Antonio Pardo                         Method BI_AccountMethods.assignOwnerToPartnerAccVEN added (Demanda 190)
    26/05/2016        Guillermo Muñoz                       Method BI_AccountMethods.restoreVisibilityCustomerPortal added    
    26/05/2016        Jose Luis Gonzalez                    Adapt to UNICA, integration with RoD to create or update Legal Entity
    09/08/2016        Humberto Nunes                        Se adiciono la linea BI_AccountMethods.CreateQueue(trigger.new, trigger.old); en el AFTER INSERT. 
    12/08/2016        Juan Carlos Terrón                    Added lines 113-118 to if(BI) sentence.
    14/09/2016        Antonio Pardo                         Addeded !System.isFuture() before calling future method line 187
    04/10/2016        Sara Nuñez                            BI_AccountMethods.checkUserCanChangeOwner added
    19/10/2016        Juan Carlos Terrón                    Added TGS_Account_Hierarchy.generate_IntegrationId method call in order to generate IntegrationId field on Cost Center Accounts.
    22/12/2016        Alvaro García                         Added BI_AccountMethods.fillLegalAndCountryFields
    19/02/2017        Julio Asenjo                          Inclusión métodos para geolocalizar clientes.
    17/04/2017        Everis                                Added Flag in Line 152.
	27/06/2017		  Marta Gonzalez						Inclusion de trazas para FS (fullstack-Integracion con Perú)
    29/06/2017        Victoria Gutierrez                    Add method sendToFullStack
    18/07/2017        Humberto Nunes                        Encapsulado de FULL STACK para que no se ejecute con usuarios FVI
    20/07/2017        Guillermo Muñoz                       Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    18/08/2017		  Julio Asenjo							Added condition to callout with estado= llamado						<J001>
    21/08/2017        Javier Almirón                        Added call to BI_AccountMethods.iniFormula() and BI_AccountMethods.updateAccountsHolding()
    01/09/2017        Guillermo Muñoz                       Moved TGS_Account_Hierarchy.sendToFullStack
    10/10/2017        Gawron, Julián                        Added TGS_Account_Hierarchy.nullHierarchy
    08/11/2017        Álvaro López                          Added preventAccountToBeDeleted method call
    18/01/2018		  Pablo Bordas							EVER01 - actualizar datos de credito de account a opportunity.
    08/02/2018        Javier Almirón García                 Added calls to methods "TIWS_ValidationRuleCountryMaster" after insert and after update
    17/04/2018        Jaime Regidor                         Added call to method 'changeOwnerContract'
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
        /*Id pId = UserInfo.getUserId();
        PermissionSetAssignment [] ps = [SELECT Id, PermissionSet.Name, AssigneeId
                                        FROM PermissionSetAssignment
                                        WHERE AssigneeId = :pId AND PermissionSet.Name = 'TGS_User'];
        Boolean TGS = false;
        if(ps.size()>0){
            TGS = true;
        }*/
        
    BI_bypass__c bypass = BI_bypass__c.getInstance();
    /*FS_INI 27/06/2017*/
    FS_CORE_Integracion__c ORGConfiguration = FS_CORE_Integracion__c.getInstance();
    /*FS_FIN 27/06/2017*/

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Account');

    if(!isTriggerDisabled){
        if (trigger.isBefore && !bypass.BI_skip_trigger__c){
            if (trigger.isInsert){
                if(BI){
                /*Se carga el campo “Strategic_Context_Last_Update__c” dependiendo de ciertos criterios. Este trigger en ningún momento interrumpe la carga de Cuentas*/
                //Certa_SCP.UtilTriggers.setStrategicContextLastUpdate(trigger.New, trigger.oldMap, trigger.isInsert);
               
                    if(!bypass.BI_migration__c)
                        BI_AccountMethods.validateId(trigger.old, trigger.new);
                    BI_AccountMethods.updateIsParent(trigger.new);
                    //BI_AccountMethods.addSector(trigger.new);
                    SCP_UtilSyncFields.syncAccount(trigger.new);
                    UtilSync.AccountSync(Trigger.New);
                    BI_AccountMethods.validateSegment(trigger.new, trigger.old);
                    BI_AccountMethods.validateSubSegment(trigger.new, trigger.old);
        
                    BI2_AccountMethods.actualizarIdCola(trigger.new, trigger.old);
                    BI_AccountMethods.assignOwnerToPartnerAccVEN(trigger.new);
                    TGS_Account_Hierarchy.copyHierarchy(trigger.new, null);
                    BI_AccountMethods.fillLegalAndCountryFields(trigger.new, null);
                }
          
                /////////////////START TGS///////////////////////
                if(TGS){
                    TGS_Account_Hierarchy.copyHierarchy(trigger.new, null);
                }
                /////////////////END TGS///////////////////////
               
            }
            if (trigger.isUpdate){
                if(BI){
                    /*Se carga el campo “Strategic_Context_Last_Update__c” dependiendo de ciertos criterios. Este trigger en ningún momento interrumpe la carga de Cuentas*/
                    //Certa_SCP.UtilTriggers.setStrategicContextLastUpdate(trigger.New, trigger.oldMap, trigger.isInsert);
                    //BI_bypass__c bypass = BI_bypass__c.getInstance();
                    if(!bypass.BI_migration__c)
                        BI_AccountMethods.validateId(trigger.old, trigger.new);
                        BI_AccountMethods.changeOwnerContract( trigger.new ,trigger.old);
                    BI_AccountMethods.updateIsParent(trigger.new);
                    SCP_UtilSyncFields.syncAccount(trigger.new);
                    BI_AccountMethods.validateSegment(trigger.new, trigger.old);
                    BI_AccountMethods.validateSubSegment(trigger.new, trigger.old);
                    BI2_AccountMethods.actualizarIdCola(trigger.new, trigger.old);
                    BI2_AccountMethods.createAnsVEN(trigger.new, trigger.old);
                    /* FAR 03/05/2016 - Moved to after trigger
                    // FAR 11/12/2015 - BI_EN - CSB Integration
                    BI_AccountMethods.deactivateAccountInCSB(trigger.new, trigger.old);
                    System.debug('BI_CSB_AccountUtilsController.fireTriggerAccountSuspension: ' + BI_CSB_AccountUtilsController.fireTriggerAccountSuspension);
                    if (BI_CSB_AccountUtilsController.fireTriggerAccountSuspension)
                        BI_AccountMethods.suspendAccountInCSB(trigger.new, trigger.old);
                    */
                    // FAR 03/05/2016 - Update suspension/reactivation status
                    BI_AccountMethods.updateSuspensionStatus(trigger.new, trigger.old);
                    BI_AccountMethods.checkUserCanChangeOwner(trigger.new, trigger.old);
                    TGS_Account_Hierarchy.copyHierarchy(trigger.new, trigger.old);
                    TGS_Account_Hierarchy.nullHierarchy(trigger.new, trigger.old); //JEG 20/10/2017  
                    BI_AccountMethods.fillLegalAndCountryFields(trigger.new, trigger.old);
    		    BI_AccountMethods.validateGeoposition(trigger.old,trigger.new);
                    BI_AccountMethods.validateEditionUser(trigger.new,trigger.old);//JaimeRM
                    
                    /* INI FS_CORE 001 -Eduardo Ventura 27/06/2016*/ 
                    BI_AccountMethods.accountStatus(trigger.old, trigger.new);
                                        
                    if(!System.isFuture() && ORGConfiguration != null && ORGConfiguration.FS_CORE_Peru__c ){
                        /* Accounts */
                        BI_AccountMethods.updateToFullstack(trigger.old, trigger.new);
                        
                        /* ATM */
                        //J001
                        for(Account item : trigger.new){
                            System.debug('\n\n\n\t\t\t\t\tCalling ATM Process con '+ item.FS_CORE_Estado_Sincronizacion__c+'\n\n\n');
                            if(!item.FS_CORE_ATM_Massive__c &&  trigger.oldmap.get(item.Id).FS_CORE_Estado_Sincronizacion__c!='Llamado'){

                                FS_CORE_Fullstack_ATM.process(item);
                                //item.FS_CORE_Estado_Sincronizacion__c='Llamado';
                            }
                        }
                    }
                    /* FIN FS_CORE 001 -Eduardo Ventura 27/06/2016**/
                }
                /////////////////TGS///////////////////////
                if(TGS){
                    TGS_Account_Validations_Handler.checkActiveAccount(Trigger.new, Trigger.old); 
                    if(!bypass.BI_migration__c) //para migracion no debe validar estructura
                    TGS_Account_Validations_Handler.checkAccountStructure(Trigger.new, Trigger.old);
                    TGS_Account_Hierarchy.fillSitesInformation(Trigger.oldMap, Trigger.newMap);
                }
                /////////////////TGS///////////////////////
            }

            BI_COL_CreateCheckDigitNIT_cls.fnValidateDigit(trigger.new);
        }else if(!bypass.BI_skip_trigger__c) { 
            if (trigger.isInsert){ 
                if(BI){
                    PCA_AccountMethods.PCA_CreateChatterGroup(trigger.new, trigger.old);
                    BI_AccountMethods.updateNumberChilds(trigger.new, trigger.old);
                    BI_AccountMethods.createAccTeamMember(trigger.new);
                    BI_AccountMethods.assign_EntitlementsProcesses(trigger.new);
                    BI2_AccountMethods.createAnsVEN(trigger.new, trigger.old);
                    BI_AccountMethods.iniFormula(trigger.new);
    
                    //JCT-12/08/2016 Added lines 113-118 to if(BI) sentence.
                    // INI adaptacion UNICA
                    BIIN_UNICA_AccountMethods.createLegalEntity(trigger.new);
                    // FIN adaptacion UNICA
    
                    // ANEXADO EN EL AFTER INSERT PARA FVI
                    BI_AccountMethods.CreateQueue(trigger.new, trigger.old);

                    
                }

                if(!bypass.BI_migration__c){
                    //Javier Almirón - 07/02/2018
                    BI_AccountMethods.TIWS_ValidationRuleCountryMaster(Trigger.new, Trigger.oldMap);
                }

            
            }
            if (trigger.isDelete){
                if(BI){
                    BI_AccountMethods.updateNumberChilds(trigger.old, null);
                }

                if(TGS){
                    TGS_Account_Validations_Handler.preventAccountToBeDeleted(Trigger.old);
                }

            } 
            //else 
            if(trigger.isUpdate){
                if(BI){
                    BI_AccountMethods.reopenSuspendedopps(trigger.new, trigger.old);
                    BI_AccountMethods.validateRiskAndFraud(trigger.new, trigger.old);
                    PCA_AccountMethods.PCA_CreateChatterGroup(trigger.new, trigger.old);
                    BI_AccountMethods.updateNumberChilds(trigger.new, trigger.old);
                    BI_AccountMethods.restoreVisibilityCustomerPortal(trigger.new, trigger.old);
                    // FAR 03/05/2016 - Deactivation moved to after trigger, and Suspension / Reactivation is not triggered from buttons anymore
                    BI_AccountMethods.deactivateAccountInCSB(trigger.new, trigger.old);
                    BI_AccountMethods.suspendAccountInCSB(trigger.new, trigger.old);
                    //EVER01-INI
                    BI_AccountMethods.updateOpportunityByAccount(trigger.new, trigger.old);
                    //EVER01-FIN
                    BI_AccountMethods.updateAccountsHolding(trigger.new, trigger.oldMap);
                }
                BI_SCP_BigDeals_Triggers.updateAccountAfter(trigger.new, trigger.oldMap);
                // INI adaptacion UNICA
                BIIN_UNICA_AccountMethods.updateLegalEntity(trigger.new, trigger.old);
                // FIN adaptacion UNICA
                //Javier Almirón - 07/02/2018
                BI_AccountMethods.TIWS_ValidationRuleCountryMaster(Trigger.new, Trigger.oldMap);
            }       
        }
    
        List<String> lstIdClientes = new List<String>();
        system.debug('\n Clientes WS Telefonica: ' + Trigger.new);
        String accion='';
     
        if(Trigger.isAfter && BI_COL_FlagforTGRS_cls.flagTriggerTRSCuenta && BI && !bypass.BI_skip_trigger__c)
        {
    
            //BI_bypass__c bypass = BI_bypass__c.getInstance();
            if(Trigger.isDelete || Trigger.isInsert || trigger.isUndelete || trigger.isUpdate)
            {
    
                /*#########################################################################
                    canalOnlineAccount_tgr on Account (after insert, after update, after delete)
                #########################################################################*/
                //Determinamos el tipo de novedad
                String strTipoNovedad = 'Creacion';
                //Creamos una variable para referenciar las cuentas
                List<Account> listAccount = new List<Account>();
                         
                //Validamos que se haya insertado sólo una cuenta
                if(trigger.isDelete && Trigger.isAfter)
                {
                    strTipoNovedad = 'Retiro';
                    listAccount = trigger.old;
                }
                 else if(trigger.isUpdate && Trigger.isAfter)
                {
                    strTipoNovedad = 'Actualizacion';
                    listAccount = trigger.new;
                }
                else if(trigger.isInsert && Trigger.isAfter)
                {
                    listAccount = trigger.new;
                }
                if(listAccount.size() == 1)
                {
                    Account objAccount = listAccount[0];
                    System.debug('\n\n########------->Ejecución Trigger CrearClienteWS_tgr \n\n');
               // Linea 143y 144agregada por labril
                  if(!bypass.BI_migration__c && !System.isFuture())
                    //Invocamos el método futuro para consumir el web service enviando el id del contacto
                    BI_COL_ChannelOnline_cls.accountCanalOnline(objAccount.Id, strTipoNovedad);
                }
            }
        
        
        
        /* EVERIS AÑADE Método para controlar la localización de las cuentas*/
         if( Trigger.isAfter && Trigger.isUpdate ){
               // BI_AccountMethods.validateGeoposition(trigger.old,trigger.new);
            }
            /*#########################################################################
             CrearClienteWS_tgr on Account (after insert, after update, before delete)
            #########################################################################*/
            // FAR 11/05/2016 - Add !System.isQueueable() to avoid "too many async calls created" exception
            if(!System.isFuture() && !Test.isRunningTest() && !bypass.BI_migration__c && BI && !System.isQueueable())
            {
                System.debug('\n\n########------->Ejecución Trigger CrearClienteWS_tgr \n\n');
                if(Trigger.size<1000)
                {
                    if(Trigger.isDelete )
                    {
                        accion='Eliminar';
                        for(Account cliente : Trigger.old)
                        {
                            lstIdClientes.add(cliente.Id);
                        }
                    }
                    else 
                    {
                        accion=Trigger.isInsert?'Insertar':'Actualizar';
                        for(Account cliente : Trigger.new)
                        {
                            lstIdClientes.add(cliente.Id);
                        }
                    }
    
                    if(!lstIdClientes.isEmpty())
                    {
                        system.debug('..==.. TGR -- lstIdClientes ..==.. '+lstIdClientes+' accion= '+accion);
                        BI_COL_CreateClientWS_cls.crearClienteTelefonica(lstIdClientes,accion); 
                    }
                }
            }
            BI_COL_FlagforTGRS_cls.flagTriggerTRSCuenta=false;
        } 
        //If BI and TGS
        if(Trigger.isAfter){

            if(Trigger.isInsert || trigger.isUpdate){

                if(!System.isFuture()){
                    //TGS_Account_Hierarchy.sendToFullStack(Trigger.new, Trigger.oldMap); 
                }
            }
        }         

        if(Trigger.isAfter && Trigger.isInsert && TGS)
        {
            TGS_Account_Hierarchy.generate_IntegrationId(Trigger.new);
        }
    }  
}