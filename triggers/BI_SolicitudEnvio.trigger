trigger BI_SolicitudEnvio on BI_Solicitud_envio_productos_y_servicios__c (after update, before update) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Aborda.es
    Description:   Trigger on BI_Solicitud_envio_productos_y_servicios__c
    
    History:
    
    <Date>			  <Author> 	        <Description>
    25/02/2015        Pablo Oliva    	Initial version
    17/02/2016        Guillermo Muñoz   Method BI_SolicitudEnvioMethods.preventModification added
    20/07/2017        Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Solicitud_envio_productos_y_servicios__c');
    
    if(!isTriggerDisabled){
		if(trigger.isAfter){
			if(trigger.isUpdate){
				BI_SolicitudEnvioMethods.manageTransactions(trigger.new, trigger.old);
			}
		}	
		else if (trigger.isBefore){
			if (trigger.isUpdate){
				BI_SolicitudEnvioMethods.rejectedRequestTransactions(trigger.new, trigger.old);
				BI_SolicitudEnvioMethods.returnModelDiscounts(trigger.new, trigger.old);
				BI_SolicitudEnvioMethods.preventModification(trigger.new, trigger.old);
			}
		}
	}
}