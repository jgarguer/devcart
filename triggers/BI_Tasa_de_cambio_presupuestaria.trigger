trigger BI_Tasa_de_cambio_presupuestaria on BI_Tasa_de_cambio_presupuestaria__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
/*
    Author:        Guillermo Muñoz
    Company:       Accenture
    Description:   Methods executed by the Tasa_de_cambio_presupuestaria trigger.
    
    History

    <Date>          <Author>                <Description>

    01/12/2016      Guillermo Muñoz         Initial version
    20/07/2017      Guillermo Muñoz         Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Tasa_de_cambio_presupuestaria__c');
    
    if(!isTriggerDisabled){
        //BI_bypass__c bypass = BI_bypass__c.getInstance();
        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
        Boolean TGS = userTGS.TGS_Is_TGS__c;
        Boolean BI = userTGS.TGS_Is_BI_EN__c;
        Boolean wasFired = NETriggerHelper.getTriggerFired('BI_Tasa_de_cambio_presupuestaria');
        System.debug('BI_Tasa_de_cambio_presupuestaria wasFired:' + wasFired);
        if(!wasFired){
    		if (Trigger.isBefore) {

    			if (trigger.isInsert){ 
                    if(BI) {
                	   BI_Tasa_de_cambio_presupuestariaMethods.BI_actualiza_fecha_fin_anterior(Trigger.new, null);
                    }//is BI
                }//is insert
                if (trigger.isUpdate){ 
                    if(BI) {
                	   BI_Tasa_de_cambio_presupuestariaMethods.BI_actualiza_fecha_fin_anterior(Trigger.new, Trigger.old);
                    }//is BI
                }//isUpdate
    		} else if (Trigger.isAfter) {

    		}
        }//notWasFired
    }  
}//endTrigger