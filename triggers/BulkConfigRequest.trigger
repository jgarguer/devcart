trigger BulkConfigRequest on Bit2WinHUB__Bulk_Configuration_Request__c (after update) {
	/*------------------------------------------------------------------------------------------------------------------------
	Author:			Angel Felipe Santaliestra Pasias
	Company:		Accenture
	Description:	Trigger of the Bit2WinHUB__Bulk_Configuration_Request__c object.

	History

	<Date>			<Author>					<Description>
	22/03/2018		Angel F Santaliestra		Initial version.
	--------------------------------------------------------------------------------------------------------------------------*/
	Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Bit2WinHUB__Bulk_Configuration_Request__c');

	if (!isTriggerDisabled) {
		if (Trigger.isBefore) {
			if (Trigger.isInsert) {}
			if (Trigger.isUpdate) {}
			if (Trigger.isDelete) {}
		}
		if (Trigger.isAfter) {
			if (Trigger.isInsert) {}
			if (Trigger.isUpdate) {
				BI_O4_BT_BulkConfigRequestMethods.checkStatus(Trigger.NEW);
			}
			if (Trigger.isDelete) {}
		}
	}
}