trigger customEvent on Event__c (after insert, before insert) {

    //GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Event__c');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore){
                if(trigger.isInsert){
                    PCA_Event_GuestMethods.PCA_InsertEvent(Trigger.New);
                }
                //if (trigger.isDelete){
                //}
            }
        else { 
               /*
                else if (trigger.isUpdate){ 
                }
                else if (trigger.isDelete){
                    
                } */
                //else if (trigger.isUpdate){
                //}       
            //}
        }
    }
}