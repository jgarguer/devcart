trigger NE_Promotion on NE__Promotion__c (after update) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Trigger on BI_Promociones__c
    
    History:
    
    <Date>            <Author>              <Description>
    05/04/2014        Ignacio Llorca        Initial version
    20/07/2017        Guillermo Muñoz       Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('NE__Promotion__c');
    
    if(!isTriggerDisabled){    
  //  if(trigger.isBefore){
    //    if(trigger.isInsert){
     //   }   
     //   else if(trigger.isUpdate){
     //   }
     //   else if(trigger.isDelete){
     //   }
   // }
   // else{
        if(trigger.isAfter){ 
           // if(trigger.isInsert){ 
           // }
            //else 
            if(trigger.isUpdate){
            	BI_NEPromotionMethods.notAvailable (trigger.new, trigger.old);
            }
            //else if(trigger.isDelete){
            //}
           // else if(trigger.isUnDelete){
           // }
        }         
    //}
  }
}