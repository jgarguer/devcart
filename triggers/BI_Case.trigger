trigger BI_Case on Case (before insert, after insert, after update, before update, after delete, after undelete) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Methods executed by the Case trigger.

    History

    <Date>          <Author>                <Description>
    25/07/2014      Micah Burgos            Initial version
    16/03/2015      Miguel Angel Galindo    Merge with TGS Trigger
    18/03/2015      Marta Garcia            BI Encapsulation
    01/04/2015      Álvaro Hernando         sendEmailToOpptyOwner new method added
    26/05/2015      Miguel Angel Galindo    Changed first run on after insert
    02/06/2015      Ana Cirac               TGS Encapsulation RoD Integration
    30/06/2015      Álvaro Hernando         closeE2CARGCobranzasAndNotify new method added
    15/09/2015      Marta García            Add error mWan
    29/09/2015      Luis Miguel Alonso      setCiBillingDate new method added
    06/10/2015      Francisco Ayllon        sendEmailToOpptyOwner method Deprecated
    07/10/2015      Luis Miguel Alonso      changed BillingDate and CSUID condition
    07/10/2015      Jose Miguel Fierro      Added BI2
    09/10/2015      Jose Leonardo Lopez     Code encapsulation changes for migration and bi2 development
    14/10/2015      Ana Cirac               Add call to TGS_CallOrder2AssetHandler.callOrder2Asset to generate
                                            the asset
    02/11/2015      Jose Miguel Fierro      Moved BI2 into BIEN encapsulement
    18/11/2015      Marta García            Add call to TGS_Lookup_Handler.AddErrorLookup
    27/11/2015      Pablo Oliva             Method "fillRelatedCases" added, APP CalculateBusinessHoursAges transformed into BIEN format
    21/01/2016      Micah Burgos            Change !bypass.BI_migration__c to !bypass.BI_skip_trigger__c. BI_migration__c is used for ValidationRules and some code validations and BI_skip_trigger__c disable the trigger.
    24/02/2016      Miguel Cabrera          Add call to TGS_CaseMethods.caseEmailSend
    26/02/2016      Micah Burgos            Add BI_CaseMethods.relateToOffer on trigger.update.
    29/02/2016      Guillermo Muñoz        BI_CaseMethods.validateChangeOwnerReclamosARG added
    21/03/2016      Oscar Iuliano           TGS_CaseMethods.verifyNotClseCase
    28/03/2016      Jose Miguel Fierro      Added Trigger.old to TGS_fill_NeedConfiguration.fillNeedConfiguration
    06/04/2016      Fernando Arteaga        GAP 103
    20/04/2016      Antonio Masferrer       Added BI_CaseMethods.createAssignmentRule
    26/04/2016      Antonio Pardo           Added BI_CaseMethods.assignCaseToAccPartnerVEN
    06/06/2016      Fernando Arteaga        Don't generate CSUIDs if case type is Disconnect
    28/06/2016      Humberto Nunes          Added call to BI_CaseMethods.llenarDistribuidorAsignado
    07/07/2016      Patricia Castillo       Allow call TGS_fill_CSUID class also when type is disconnect but service is mWan
    18/07/2016      Patricia Castillo       Add condition to call cls that generates CSUIDs if mWan termination
    18/07/2016      Antonio Masferrer       Add call BI_CaseMethods.changeOwnerFVI
    11/08/2016      Humberto Nunes          Se a agregdo la variable Boolean FVI = userTGS.BI_FVI_Is_FVI__c y se ha usaado en el After Insert
    13/09/2016      Antonio Pardo           Moved assignEntitlements from before insert to after insert
    16/09/2016      Juan Carlos Terrón      Added TGS_fill_CSUID.fill_CSUID_EnterpriseID method call instead of csuid() method call. Line 282
    16/09/2016      Humberto Nunes          Se realizo la llamada al método BI_CaseMethods.EditOnlyOwnerFVI Antes de Modificar
    25/09/2016      Juan Carlos Terrón      Lines 292-294, added 2 method calls.
    27/09/2016      Juan Carlos Terrón      Added condition at Line 284 to call CSUID generator methods in every Closed-Disconnect case.
    25/09/2016      Juan Carlos Terrón      Changed TGS_fill_CSUID.fill_CSUID_EnterpriseID method call for TGS_fill_CSUID.fill_EnterpriseID(Trigger.newMap); call
                                            Also Added TGS_fill_CSUID.csuid(newCase); method call.
    27/092016       Juan Carlos Terrón      Changed the csuid method call to get a TGS_fill_CSUID.fill_EnterpriseID(Trigger.newMap) call as parameter after the
                                            csuid method definition reported on TGS_fill_CSUID class
    28/09/2016      Humberto Nunes          Se encapulo el metodo caseAssignment para BI o FVI
    29/09/2019      Humberto Nunes          Se comenzo a utilizar el metodo addTeamMemberFVI
    03/10/2016      Humberto Nunes          Se reactivo la llamada al metodo addTeamMemberFVI que se habia comentado por no controlar la existencia del CaseTeamMember
    19/10/2016      Jose Miguel Fierro      Se reemplaza query directa en el trigger por validateMandatorySiteMWAN
    24/10/2016      Antonio Pardo           Added method BI_CaseMethods.caseCreatorARG on before update
    02/11/2016      Juan Carlos Terrón      Added TGS_CaseMethods.assigner_TGS_ProductCategorization method call before insert and before update for TGS users.
    08/11/2016      Alvaro Garcia           Comment methods caseAsignGSE and caseAsignLSE, add this functionality in method setCaseOwner
    14/11/2016      Juan Carlos Terrón      Deleted TGS_CaseMethods.case_StatusRollback(Trigger.newMap, Trigger.oldMap); method call to avoid selfreference delete error.
    25/10/2016      Patricia Castillo       Added call calcResTimeWorkingDaysMEX (D343)
    30/11/2016      Juan Carlos Terrón      Changed TGS_CaseMethods.assignerHandlerTGS(Trigger.new, null); method call from AFTER INSERT event to BEFORE INSERT event.
    01/12/2016      Juan Carlos Terrón      Re-Added TGS_CaseMethods.case_StatusRollback due to a new version of the method.
    02/12/2016      Juan carlos Terrón      Re-Added TGS_CaseMethods.setMilestoneEndDateTGS method call after a previous replacement during the development phase.
    13/12/2016      Álvaro López            Added validateProductTier method call in after insert and after update
    02/01/2017      Guillermo Muñoz         Added BI_CaseMethods.setHolidaysAssignation call in after insert and after update
    03/01/2017      Juan Carlos Terrón      Changed the assignerHandlerSLA method call due to its declaration change to admit oldMap parameter.
                                            Re-Added  assignerHandlerTGS method call after enviroment refresh.
    04/01/2017      Guillermo Muñoz         Added BI_CaseMethods.validateRequiredFieldsCHI call in before insert and before update
    04/01/2017      Guillermo Muñoz         Added validateAttachmentsAndProductsCHI call in before insert
    04/01/2017      Guillermo Muñoz         Added BI_CaseMethods.validateCreatedCasesCHI call in before update
    31/01/2017      Alvaro sevilla          changed method  BI2_CaseMethods.setMilestoneEndDate from after to before update
    02/02/2017      Jose Miguel Fierro      Added assignCoECaseEntitlements, for eHelps and ITSDs
    02/02/2017      Alberto Fernandez       Added BI_CaseMethods.sendEmailCOE call in before insert
    06/02/2017      Alberto Fernandez       BI_CaseMethods.sendEmailCOE call moved from before insert to after insert
    08/02/2017      Guillermo Muñoz         Added COE_CaseMethods.assignCoEEntitlements, COE_CaseMethods.progressCaseMilestones, COE_CaseMethods.validateStatusChange
    16/02/2017      Álvaro López            Added validateProductTier_AUX method call in after insert and after update
    06/03/2017      Alberto Fernandez       Commented call to validateCreatedCasesCHI method remove on CaseMethodClass
    08/03/2017      Jose Miguel Fierro      Added getOrderCompleted method call in after update
    08/03/2017      Álvaro López            Added TGS_Billing_Date.setCiBillingDateFromPrevOrder method in after insert
    14/03/2017      Guillermo Muñoz         Added BI_O4_CaseMethods.assignLegalCase() call in before update
    14/03/2017      Álvaro López            Added TriggerFired for fill_CSUID
    15/03/2017      Álvaro López            Serialize entry case for setCiBillingDateFromPrevOrder
    16/03/2017      José María Barahona     Encapsulate TGS_Queue_Case.queueCase(Trigger.new, Trigger.newMap) call for community users
    04/04/2017      Guillermo Muñoz         Moved BI2_CaseMethods.setMilestoneEndDate() from before update to after update
    05/04/2017      Humberto Nunes           Se agrego la llamada a la funcion closeOppFromCase
    30/05/2017      Oscar Bartolo            Add call to method BI_O4_CaseMethods.fillGenericoPreventaId
    09/06/2017      Oscar Bartolo        Add call to method BI_O4_CaseMethods.fillGenericoPreventaId    and BI_O4_CaseMethods.updatePresentacionDate
    22/06/2017      Oscar Bartolo        Add call to method BI_O4_CaseMethods.closeChildCase
    30/06/2017      Jaime Regidor            Se cambio las llamadas al metodo caseAssignment para que no le influya el Bypass
    04/07/2017      Álvaro López            Removed else condition in order to force the second isAfter execution
    11/07/2017      Daniel Guzman/Guillermo  Change  BI2_CaseMethods.setMilestoneEndDate to check the method
    11/07/2017      Oscar Bartolo        Add call to method BI_O4_CaseMethods.createCaseTeamMember
    13/07/2017      Oscar Bartolo        Add call to method BI_O4_CaseMethods.updateCreatorRole and BI_O4_CaseMethods.updateAssignUserRole
    19/07/2017      Guillermo Muñoz         Moved BI2_CaseMethods.assignEntitlements from after insert to before insert and restored BI2_CaseMethods.setMilestoneEndDate
    20/07/2017      Guillermo Muñoz         Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    02/08/2017      Alvaro Sevilla          Added invocation to method  BI_CaseMethods.activarContratoCasoCerrado in conditional (BI || FVI)
    01/09/2017      Oscar Bartolo           Add call to method BI_O4_CaseMethods.updateEmailResponsablePreventa
    26/09/2017      Jaime Regidor           Added closeCaseCancelacion method call in after update
    05/10/2017      Oscar Bartolo           Add call to method BI_O4_CaseMethods.fillClienteInternoNombre
    18/10/2017     Javier Almirón           Calls to BI_O4_CaseMethods.fillClienteInternoNombre and BI_O4_CaseMethods.fillClienteInternoNombre out of BI encapsulating
    01/03/2018      Javier Almirón          Added Calls to BI_O4_CaseMethods."TIWS_AssignOwner_ProductSupport" after insert
    27/04/2018      Alvaro sevilla          add invacation to method activarOrderSubsidio and flag wasFired_rejectRecord for method rejectRecord
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    BI_bypass__c bypass = BI_bypass__c.getInstance();
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c;
    Boolean Community = userTGS.TGS_Is_Community__c;

    /*Id pId = UserInfo.getUserId();
    PermissionSetAssignment [] ps = [SELECT Id, PermissionSet.Name, AssigneeId
                                    FROM PermissionSetAssignment
                                    WHERE AssigneeId = :pId AND PermissionSet.Name = 'TGS_User'];

    if(ps.size()>0){
        TGS = true;
    }*/
    system.debug('#Entrando en BI_Case ');
    system.debug('$$$CASES:'+trigger.new);

    //Evitamos el Bypass //JEG 16/08/2017 Demanda interna eHelps
    if(trigger.isBefore){
        system.debug('>>trigger before<<');  
        if (trigger.isInsert){
            if(BI){
               BI_CaseMethods.assignMaxSLAHours(trigger.new ,trigger.old);
            }
        }
        if(trigger.isUpdate){
            if(BI){
               BI_CaseMethods.assignMaxSLAHours(trigger.new ,trigger.old);
            }
        }
    }else{
        system.debug('>>trigger after<<'); 
        //isAfter
        if (trigger.isInsert){
            system.debug('>>trigger after insert<<'); 
            if(BI){    
            system.debug('>>trigger after insert BI<<');             
               BI_CaseMethods.caseAssignmentEHelps(trigger.new ,trigger.old);               
               BI_CaseMethods.stepsCase(trigger.new ,trigger.old);
               BI_CaseMethods.stepsCaseEsc3(trigger.new ,trigger.old);
               system.debug('>>>>>>>>trigger lstCaseEntry<<<<<<<<<<' + trigger.new);
            }
        }
        if(trigger.isUpdate){
            if(BI)
            {
              boolean wasFired_submitCaseForApprovalPI = NETriggerHelper.getTriggerFired('submitCaseForApprovalPI');
               boolean wasFired_submitCaseForApproESC3 = NETriggerHelper.getTriggerFired('submitCaseForApproESC3');
               boolean wasFired_rejectRecord = NETriggerHelper.getTriggerFired('rejectRecord');
               boolean wasFired_updateQueues = NETriggerHelper.getTriggerFired('updateQueues');//GSPM 15/05/2018 Subsidio ARG
               if(!wasFired_submitCaseForApproESC3)
               {
                    NETriggerHelper.setTriggerFired('submitCaseForApproESC3');
                    BI_CaseMethods.submitCaseForApproESC3(trigger.new, trigger.old);
               }
               if(!wasFired_submitCaseForApprovalPI)
               {
                   if(!System.isFuture() && !System.isBatch()){
                        NETriggerHelper.setTriggerFired('submitCaseForApprovalPI');
                        BI_CaseMethods.submitCaseForApprovalPI(trigger.new, trigger.old);
                   }                
               }
               if(!wasFired_rejectRecord) {                   
                    NETriggerHelper.setTriggerFired('rejectRecord');
                    BI_CaseMethods.rejectRecord(trigger.new, trigger.old);
               } if(!wasFired_updateQueues) { //GSPM 15/05/2018 Subsidio ARG                  
                    NETriggerHelper.setTriggerFired('updateQueues');
                    BI_CaseMethods.updateQueues(trigger.new, trigger.old);

               }                  
               
               BI_CaseMethods.caseAssignmentEHelps(trigger.new ,trigger.old);
               BI_CaseMethods.activarOrderSubsidio(trigger.new, trigger.old); //ASD 27/04/2018 Subsidio ARG
            }
        }
    }

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Case');

    if(!isTriggerDisabled){
        if (trigger.isBefore  && !bypass.BI_skip_trigger__c){
            if (trigger.isInsert){
                if(BI){
                    BI2_CaseMethods.fillRequiredFields(trigger.new);
                    BI_CaseMethods.add_Country_from_Acc(trigger.new);
                    BI_CaseMethods.updateFields(trigger.old ,trigger.new);
                    BI_CaseMethods.setCaseOwner(trigger.new);
                    BI_CaseMethods.relateToOffer(trigger.new);
                    BI_CaseMethods.assign_Entitlements(trigger.new);
                    BI_CaseMethods.caseDays(trigger.new, trigger.old);
                    BI_CaseMethods.validateSegment(trigger.new, trigger.old);
                    BI_CaseMethods.calcResTimeWorkingDaysMEX(null,trigger.new);//PCP

                    if(!bypass.BI_migration__c){
                      BI_CaseMethods.updateSupervisor(trigger.old ,trigger.new);
                      BI_CaseMethods.tipoSolicitudSinOppPER(trigger.new, trigger.oldMap);
                      BI_CaseMethods.validateRequiredFieldsCHI(trigger.new, null);
                      BI_CaseMethods.validateAttachmentsAndProductsCHI(trigger.new, null);
                    }
                    //BI_CaseMethods.assignCaseToQueue(trigger.new);
                    //BI2_CaseMethods.setLastStatusChangeOnInsert(Trigger.new);
                    BI2_CaseMethods.updateOwner(Trigger.new, Trigger.old);
                    BI2_CaseMethods.updateContactNoOfCasesOnCaseInsert(trigger.new);
                    BI2_CaseMethods.CalculateBusinessHoursAges(trigger.new, trigger.old);
                    BI_CaseMethods.validateCasesARG(trigger.new, null);
                    BI_CaseMethods.assignCaseToAccPartnerVEN(trigger.new);
                    COE_CaseMethods.assignCoEEntitlements(trigger.new, null);
                    BI_CaseMethods.fillBusinessHours(Trigger.new);
                    BI2_CaseMethods.assignEntitlements(trigger.new, trigger.old);
                    //OBP-09/06/2017
                    BI_O4_CaseMethods.fillGenericoPreventaId(Trigger.new);
                    //OBP-13/07/2017
                    BI_O4_CaseMethods.updateCreatorRole(trigger.new);
                    //BI_O4_CaseMethods.fillClienteInternoNombre(trigger.new);
                }
		BI_O4_CaseMethods.fillClienteInternoNombre(trigger.new);
                if(FVI) { // JMF 13/10/2016 - Encapsulado para FVI
                   BI_CaseMethods.llenarDistribuidorAsignado(trigger.new);
                }
                if(TGS)
                {
                    TGS_CaseMethods.assigner_TGS_ProductCategorization(null, Trigger.new);
                    COE_CaseMethods.assignCoEEntitlements(trigger.new, null);
                }

            }
            if (trigger.isUpdate){

                // Inicio Humberto Nunes 16/09/2016
                if (BI || FVI)
                {
                  BI_CaseMethods.EditOnlyOwnerFVI(trigger.new, trigger.old);
                  BI_CaseMethods.addTeamMemberFVI(trigger.new, trigger.old); // 03/10/2016
                }
               // Fin Humberto Nunes 16/09/2016

                if(BI){

                    //ASD 31/01/2017 se paso el metodo BI2_CaseMethods.setMilestoneEndDate(Trigger.new); de after a before
                    if(!NETriggerHelper.getTriggerFired('BI2_CaseMethods.setMilestoneEndDate'))
                    {
                        System.debug('ejecuto before--->');
                        BI2_CaseMethods.setMilestoneEndDate(Trigger.new, Trigger.oldMap);
                    }

                    //ASD 31/01/2017
                    //END GMN 04/04/2017

                    BI2_CaseMethods.clearFieldsOnUpdate(Trigger.new, Trigger.old);
                    BI2_CaseMethods.setEscalado(Trigger.new, Trigger.old);
                    BI_CaseMethods.preventCloseParent(trigger.new, trigger.old);
                    // BI_CaseMethods.changeOwnerFVI(trigger.new, trigger.old);//18/07/2016 AMS // OMITIDO POR HN 18/11/2016

                    BI_CaseMethods.updateFields(trigger.old,trigger.new);
                    BI_CaseMethods.relateToOffer(trigger.new);
                    BI_CaseMethods.caseDays(trigger.new, trigger.old);
                    BI_CaseMethods.validateSegment(trigger.new, trigger.old);
                    BI_CaseMethods.caseCreatorARG(trigger.newMap, trigger.oldMap);
                    BI_CaseMethods.calcResTimeWorkingDaysMEX(trigger.oldMap,trigger.new);//PCP
                    if(!bypass.BI_migration__c){
                        BI_CaseMethods.updateSupervisor(trigger.old ,trigger.new);
                        BI_CaseMethods.preventChangeOwner(trigger.newMap, trigger.oldMap);
                        BI_CaseMethods.validateChangeOwnerReclamosARG(trigger.new, trigger.old);

                        BI_CaseMethods.tipoSolicitudSinOppPER(trigger.new, trigger.oldMap);
                        BI_CaseMethods.validateRequiredFieldsCHI(trigger.new, trigger.oldMap);
                    //D-426   BI_CaseMethods.validateCreatedCasesCHI(trigger.new, trigger.oldMap);
                    }

                    BI2_CaseMethods.assignEntitlements(trigger.new, trigger.old);
                    //BI2_CaseMethods.calculateBusinessHoursAges(Trigger.newMap, Trigger.oldMap);
                    BI2_CaseMethods.noCloseCaseWithOpenChildCases(Trigger.new, Trigger.old);
                    //BI_CaseMethods.assignCaseToQueue(trigger.new);
                    BI2_CaseMethods.CalculateBusinessHoursAges(trigger.new, trigger.old);
                    BI_CaseMethods.validateCasesARG(trigger.new, trigger.old);
                    // FAR 20/10/2016: Call fillPreviousOwner
                    BI_O4_CaseMethods.fillPreviousOwner(trigger.new, trigger.old);
                    //BI_O4_CaseMethods.caseAsignGSE(trigger.new);
                    //BI_O4_CaseMethods.caseAsignLSE(trigger.new);
                    BI_O4_CaseMethods.preventCloseChildCases(trigger.newMap,trigger.oldMap);
                    COE_CaseMethods.assignCoEEntitlements(trigger.new, trigger.oldMap);
                    COE_CaseMethods.progressCaseMilestones(trigger.new, trigger.oldMap);
                    COE_CaseMethods.validateStatusChange(trigger.new, trigger.oldMap);
                    BI_O4_CaseMethods.assignLegalCase(trigger.new, trigger.oldMap);
                    //OBP-22/06/2017
                    BI_O4_CaseMethods.closeChildCase(trigger.new, trigger.old);
                    //OBP-13/07/2017
                    BI_O4_CaseMethods.updateAssignUserRole(trigger.new, trigger.old);
                    BI_O4_CaseMethods.updateEmailResponsablePreventa(trigger.new, trigger.old);
                }


                /////////////////START TGS///////////////////////////
                if(TGS){
                    //if(NETriggerHelper.getTriggerFired('addErrorLookup'){
                    //NETriggerHelper.setTriggerFired('addErrorLookup');
                        TGS_Lookup_Handler.addErrorLookup(Trigger.new, Trigger.newMap, Trigger.old);
                    //}
                    TGS_fill_NeedConfiguration.fillNeedConfiguration(Trigger.new, Trigger.old);
                    if(!Community){
                        TGS_Queue_Case.queueCase(Trigger.new, Trigger.newMap);
                    }
                    TGS_ValidationBPHandler.addErrorBP(Trigger.new, Trigger.newMap);
                    // Add error mWan
                    //TGS_mWan_AddError.addErrormWan(Trigger.newMap.keySet());

                    TGS_CaseMethods.validateMandatorySiteMWAN(Trigger.newMap); // JMF 19/10/2016 - Removed unnecesary processing of query
                    //Used to not close the case, if the father is Active //21/03/2016 OI-
                    TGS_CaseMethods.verifyNotClseCase(Trigger.new);
                    //14/11/2016 Deleted TGS_CaseMethods.case_StatusRollback(Trigger.newMap, Trigger.oldMap); method call to avoid selfreference delete error.
                    TGS_CaseMethods.assigner_TGS_ProductCategorization(Trigger.oldMap, Trigger.new);
                    //03/01/2017 JCT Re-Added  assignerHandlerTGS method call after enviroment refresh.
                    TGS_CaseMethods.assignerHandlerTGS(Trigger.new, Trigger.oldMap);
                    //01/12/2016 JCT Re-Added TGS_CaseMethods.case_StatusRollback due to a new version of the method.
                    TGS_CaseMethods.case_StatusRollback(Trigger.newMap, Trigger.oldMap);
                    //01/12/2016 MGC Added update on Case Miletone CompletionDate
                    //02/01/2017 JCT Added setMilestoneEndDateTGS method call.
                    TGS_CaseMethods.setMilestoneEndDateTGS(Trigger.new);
                    // JMF 02/02/2017 - Add assignment of entitlements for ITSD cases
                    COE_CaseMethods.assignCoEEntitlements(trigger.new, trigger.oldMap);
                    COE_CaseMethods.progressCaseMilestones(trigger.new, trigger.oldMap);
                    COE_CaseMethods.validateStatusChange(trigger.new, trigger.oldMap);
                    //03/01/2017 JCT Changed the assignerHandlerSLA method call due to its declaration change to admit oldMap parameter.
                    TGS_SLA_Assigner_Handler.assignerHandlerSLA(Trigger.new,Trigger.oldMap);
                }
                /////////////////END TGS/////////////////////////////
            }
            //if (trigger.isDelete){
            //}

            /////////////////START TGS///////////////////////
            //if(TGS){
            //    TGS_SLA_Assigner_Handler.assignerHandlerSLA(Trigger.new); // JMF 19/10/2016 - Removed call to method that queried all SLAs on every pass
            //}
            /////////////////END TGS///////////////////////

            ///Jaime Regidor
        }else if(trigger.isAfter) {
            if (trigger.isInsert)
            {
                if(BI || FVI)
                {
                    BI_CaseMethods.caseAssignment(trigger.new, trigger.old);
                }

                if (BI){
                    BI_CaseMethods.TIWS_AssignOwner_ProductSupport (trigger.new);
                }


            }else if (trigger.isUpdate){

                 if(BI || FVI)
                {
                    BI_CaseMethods.caseAssignment(trigger.new, trigger.old);
                }
            }

        /*Álvaro López 04/07/2017 - Removed else condition in order to force the second isAfter execution*/
        }if(trigger.isAfter && !bypass.BI_skip_trigger__c) {
            /////////////////START TGS///////////////////////
              /*  Eliminacion Acting As
                list<id> caseIdListOld = new list<id>();
                String previousStatus = null;
                String previousStatusReason = null;
                String currentStatus = null;
                String currentStatusReason = null;
                Boolean sendEmail = false;
                if(TGS){
                    list<id> caseIdListNew = new list<id>();
                    caseIdListNew = TGS_EmailUtilsHandler.getListId(Trigger.new);
                    currentStatusReason = Trigger.new[0].TGS_Status_reason__c;
                    previousStatus = Trigger.new[0].Status;
                }
              Eliminacion Acting As */
            /////////////////END TGS///////////////////////

            if (trigger.isInsert)
            {
                if(BI)
                {
                    BI_CaseMethods.sendEmail_ARG(trigger.new, trigger.oldMap); //GSPM 28/12/2017 sending email to ARG queue
                }

                // JGL 07/08/2016 added to the encapsulated part of BI
                    //BI_CaseMethods.createAssignmentRule(trigger.new,trigger.old);
                    //BI_CaseMethods.createAssignmentRuleRows(trigger.new,trigger.old);
                // end JGL 07/08/2016
                /////////////////INTEGRATIONS///////////////////////
                if(TGS)
                {
                    // NE 26/11/2015 - Set TGS_CallRodWs.inFutureContext true for orders created via Bulk, in order to
                    // prevent the trigger to produce any effect during their creation.
                    for(Case newCase : [SELECT Order__r.NE__Channel__c FROM Case WHERE Id IN :Trigger.newMap.keySet()])
                    {
                        if(newCase.Order__r.NE__Channel__c == Constants.CONFIGURATION_CHANNEL_BULK)
                            TGS_CallRodWs.inFutureContext = true;
                    }
                    // END NE 26/11/2015

                    String case_toProcess;
                    for(Case newCase: Trigger.new){
                        case_toProcess = JSON.serialize(newCase);
                        /* Álvaro López 27/03/2017 - Added TGS_CallRodWs.inFutureContext and case type conditions */
                        if(!TGS_Billing_Date.inFutureContext && !TGS_CallRodWs.inFutureContext && newCase.Type != Constants.TYPE_NEW){
                            TGS_Billing_Date.setCiBillingDateFromPrevOrder(case_toProcess);
                        }
                    }

                    if (!TGS_CallRodWs.inFutureContext && TGS_CallRodWs.firstRun) {
                        TGS_CallRodWs.firstRun =false;
                        system.debug('INSERT');
                        TGS_CallRodWs.invokeWebServiceROD(Trigger.newMap.keySet(), null,null, false);
                    }
                    TGS_CaseMethods.assignerHandlerTGS(Trigger.new, null);
                    TGS_CaseMethods.validateProductTier_AUX(Trigger.new, Trigger.oldMap);

                }
                /////////////////END INTEGRATIONS///////////////////////
                if(BI)
                {
                    // JGL 07/08/2016 added to the encapsulated part of BI
                    // BI_CaseMethods.createAssignmentRule(trigger.new,trigger.old); // HN 11/08/2016  Comentado porque no es correcto que se ejecute aca.
                    // BI_CaseMethods.createAssignmentRuleRows(trigger.new,trigger.old); // HN 11/08/2016 Comentado porque no es correcto que se ejecute aca.
                    // end JGL 07/08/2016

                    //system.debug(trigger.new);
                    //Create CaseTeamMembers with AccountTeamMembers where Role = 'Ejecutivo de cuentas'. Condition -> Case.BI_Facturacion__c != null
                    BI_CaseMethods.createCaseTeam_Facturacion(trigger.new);
                    //Create CaseTeamMember with AccountOwner where Role = 'Ejecutivo de clientes'. Condition -> Case.BI_Facturas__c != null
                    BI_CaseMethods.createCaseTeam_Facturas(trigger.new);
                    BI_CaseMethods.sendEmail(trigger.new);
                    // BI_CaseMethods.caseAssignment(trigger.new, trigger.old); // 28/09/2016
                    BI_CaseMethods.createCaseTeamMembers(trigger.new);
                    BI_CaseMethods.createCaseTeam_Ecuador(trigger.new);
                    BI_CaseMethods.createCaseTeam_Community(trigger.new);
                    //BI_CaseMethods.checkFields(trigger.old, trigger.new); //Deprecated
                    BI2_CaseMethods.insertCaseUpdateContact(Trigger.new);
                    BI2_CaseMethods.fillRelatedCases(trigger.new, trigger.old);
                    //BI2_CaseMethods.assignEntitlements(trigger.new, trigger.old);
                    BI_CaseMethods.setHolidaysAssignation(trigger.new, null);
                    BI_CaseMethods.sendEmailCOE(trigger.new);
                    //OBP-11/07/2017
                    BI_O4_CaseMethods.createCaseTeamMember(trigger.new);
                    //BI_O4_CaseMethods.fillClienteInternoNombre(trigger.new);
                }

                BI_O4_CaseMethods.fillClienteInternoNombre(trigger.new);

                if (FVI) // HN 11/08/2016
                {
                    BI_CaseMethods.createAssignmentRule(trigger.new,trigger.old);
                    BI_CaseMethods.createAssignmentRuleRows(trigger.new,trigger.old);
                }

                /////////////////START TGS///////////////////////
                  /* Eliminación Acting As
                    if(TGS){
                        //TGS_EmailUtilsHandler.setAssignmentRule(Trigger.new);

                        previousStatus = Trigger.new[0].Status;

                        caseIdListOld = TGS_EmailUtilsHandler.getListId(Trigger.new);
                        sendEmail = true;
                        //TGS_EmailUtilsHandler.firstRun = true;
                    }

                  Eliminacion Acting As */
                /////////////////END TGS///////////////////////

            }
            else if (trigger.isUpdate){
                /////////////////INTEGRATIONS///////////////////////
                if(TGS){
                    if (!TGS_CallRodWs.inFutureContext && TGS_CallRodWs.firstRun) {
                        TGS_CallRodWs.firstRun =false;
                        system.debug('UPDATE');
                        TGS_CallRodWs.invokeWebServiceRODUpdate(Trigger.newMap.keySet(), Trigger.Old, true);
                    }

                    /* A. Cirac call RoD to generate the asset */
                    if(!NETriggerHelper.getTriggerFired('TGS_ProductOrdering_UNICA.createOrder'))TGS_CallOrder2AssetHandler.callOrder2Asset(Trigger.new, Trigger.oldMap);
                    if(trigger.isAfter){

                        if(PCA_Address_B2W_Controller.fireSendEmail){
                            TGS_CaseMethods.caseEmailSend(trigger.new);
                        }
                        System.debug(LoggingLevel.FINEST, 'Constants.firstRunBilling:' + Constants.firstRunBilling);

                        if(Constants.firstRunBilling){
                            Constants.firstRunBilling = false;

                            TGS_CaseMethods.fillBillingInfo(trigger.new, trigger.newMap, trigger.oldMap);
                            /*******OLD
                            for(Case newCase: Trigger.new){
                                Case oldCase = System.Trigger.oldMap.get(newCase.Id);
                                // JCT 27/09/2016 Added condition at Line 284 to call CSUID generator methods in every Closed-Disconnect case.
                                if( ( newCase.asset__c!=null && (
                                    ( newCase.type == Constants.TYPE_NEW && oldCase.status == Constants.CASE_STATUS_RESOLVED && newCase.status == Constants.CASE_STATUS_CLOSED) ||
                                    ( newCase.type != Constants.TYPE_NEW && newCase.asset__c != oldCase.asset__c) ) ) || //PCP 18/07/2016 - Add condition to call cls that generates CSUIDs
                                    ( newCase.asset__c == null && newCase.type == Constants.TYPE_DISCONNECT && (newCase.TGS_Service__c.equals(Constants.PRODUCT_MWAN_SU) || newCase.TGS_Service__c.equals(Constants.PRODUCT_MWAN_INTERNET)))||
                                    ( newCase.asset__c == null && newCase.type == Constants.TYPE_DISCONNECT && newCase.Status == Constants.CASE_STATUS_CLOSED)
                                  )
                                {
                                    Boolean wasFired_CSUID = NETriggerHelper.getTriggerFired('TGS_fill_CSUID');

                                    if(!wasFired_CSUID){
                                        if(Constants.isQueueableContext){
                                            if (newCase.Type != Constants.TYPE_DISCONNECT || // FAR 06/06/2016 - Don't generate CSUIDs if case type is Disconnect
                                                (newCase.Type == Constants.TYPE_DISCONNECT &&
                                                (newCase.TGS_Service__c.equals(Constants.PRODUCT_MWAN_SU) || newCase.TGS_Service__c.equals(Constants.PRODUCT_MWAN_INTERNET)))//PCP 07/07/2016 - Allow call TGS_fill_CSUID class also if disconnect and service mWan
                                                )
                                            {
                                                //15/09/2016 JCT Added TGS_fill_CSUID.fill_CSUID_EnterpriseID method call instead of csuid() method call.
                                                //25/09/2016 JCT Changed TGS_fill_CSUID.fill_CSUID_EnterpriseID method call for TGS_fill_CSUID.fill_EnterpriseID(Trigger.newMap); call
                                                //               Also Added TGS_fill_CSUID.csuid(newCase); method call.
                                                //27/092016 JCT  Changed the csuid method call to get a TGS_fill_CSUID.fill_EnterpriseID(Trigger.newMap) call as parameter after the
                                                //               csuid method definition reported on TGS_fill_CSUID class.
                                                TGS_fill_CSUID.csuid(newCase,TGS_fill_CSUID.fill_EnterpriseID(Trigger.newMap));
                                            }

                                            TGS_Billing_Date.setCiBillingDate(newCase);
                                        }else{
                                            System.enqueueJob(new TGS_Billing_Queueable(newCase));
                                        }
                                    }
                                }
                            }*/
                        }
                        TGS_CaseMethods.validateProductTier_AUX(Trigger.new, Trigger.oldMap);
                        if (!TGS_CallRodWs.inFutureContext) TGS_CaseMethods.getOrderCompleted(Trigger.newMap, Trigger.oldMap);
                     //   TGS_CaseMethods.launchEboundingNotifications(trigger.oldmap, trigger.newMap);
                    }
                    TGS_CaseMethods.fillCommAssetInfo(Trigger.new);
                    //INI - everis - 01/12/2016 - DSS218 - new automatic notifications in Infinity for mSIP service
                    System.debug('BPJ BI_CASE entro');
                    if (TGS_CaseMethods.firstRunNotification) TGS_CaseMethods.notificacionMSIP(Trigger.old, Trigger.new);
                    //FIN - everis - 01/12/2016 - DSS218 - new automatic notifications in Infinity for mSIP service
                }
                /////////////////END INTEGRATIONS///////////////////////

                if (FVI)
                {
                      BI_CaseMethods.actualizarSubestadoOpty(trigger.newMap, trigger.oldMap);
                }

                // 28/09/2016
                if(BI || FVI)
                {
                    //BI_CaseMethods.caseAssignment(trigger.new, trigger.old);
                    BI_CaseMethods.envioCorreoNotificacionAlCerrar(trigger.new, trigger.old);
                    BI_CaseMethods.closeOppFromCase(trigger.new, trigger.old);
                    BI_CaseMethods.activarContratoCasoCerrado(trigger.new, trigger.old);
                }
                // 28/09/2016

                if(BI){
                    //BI_CaseMethods.sendEmailToOpptyOwner(trigger.new, trigger.old);
                    BI_CaseMethods.addParentCaseTeam(trigger.new, trigger.old);
                    BI_CaseMethods.status_CaseMilestone(trigger.old, trigger.new);
                    //BI_CaseMethods.checkFields(trigger.old, trigger.new); //Deprecated
                    BI_CaseMethods.closeE2CARGCobranzasAndNotify(trigger.new, trigger.old);
                    //BI2_CaseMethods.setEscalado(Trigger.new, Trigger.old);
                    BI2_CaseMethods.updateCaseUpdateContact(Trigger.new, Trigger.old);
                    //BI2_CaseMethods.setMilestoneEndDate(Trigger.new, Trigger.oldMap);
                    BI2_CaseMethods.fillRelatedCases(trigger.new, trigger.old);
                    BI_O4_CaseMethods.ddoCotizadosParentCotizado(trigger.new, trigger.old);
                    BI_CaseMethods.setHolidaysAssignation(trigger.new, trigger.old);
                //OBP-09/06/2017
                BI_O4_CaseMethods.updatePresentacionDate(Trigger.new);
                BI_CaseMethods.closeCaseCancelacion(trigger.new, trigger.old);//JRM
                 BI_CaseMethods.sendEmail_ARG(trigger.new, trigger.oldMap); //JEG
                }

                /////////////////START TGS///////////////////////
                if(TGS){
                    if(!Test.isRunningTest()){
                        if(TGS_Survey_Launcher_Handler.firstRun){
                            TGS_Survey_Launcher_Handler.firstRun = false;
                            TGS_Survey_Launcher_Handler.surveyLaunchHandler(Trigger.old, Trigger.new);
                        }
                    }else{
                        TGS_Survey_Launcher_Handler.surveyLaunchHandler(Trigger.old, Trigger.new);
                    }

                    /* Eliminacion Acting As       caseIdListOld = TGS_EmailUtilsHandler.getListId(Trigger.old);
                    previousStatus = Trigger.old[0].Status;
                    previousStatusReason = Trigger.old[0].TGS_Status_reason__c;
                    if(currentStatus != previousStatus || currentStatusReason != previousStatusReason){
                        sendEmail = true;
                    }
                    Eliminacion Acting As */

                    //Used to not close the case, if the father is Active //21/03/2016 OI-
                    TGS_CaseMethods.verifyNotClseCase(Trigger.new);
                    //  Update the order Item Attribute, and Add the Asset at Case in Change.//21/03/2016 OI-
                    // TGS_CaseMethods.updateOrderItemAttribute(Trigger.new);
                }
                /////////////////END TGS///////////////////////
            }
            else if (trigger.isDelete){
                if(BI){
                    BI2_CaseMethods.deleteCaseUpdateContact(Trigger.old);
                    BI2_CaseMethods.fillRelatedCases(trigger.new, trigger.old);
                }
            }
            else if(Trigger.isUndelete) {
                if(BI){
                    BI2_CaseMethods.undeleteCaseUpdateContact(Trigger.new);
                    BI2_CaseMethods.fillRelatedCases(trigger.new, trigger.old);
                }
            }
            /////////////////START TGS///////////////////////
              /* Eliminacion Acting As        if(TGS){
                    System.debug('firstRun: '+TGS_EmailUtilsHandler.firstRun+' SendEmail: '+sendEmail);
                    if(TGS_EmailUtilsHandler.firstRun){
                        TGS_EmailUtilsHandler.firstRun = false;
                        if(sendEmail){
                            sendEmail = false;
                            list<id> caseIdListNew = new list<id>();
                            caseIdListNew = TGS_EmailUtilsHandler.getListId(Trigger.new);
                            if (!TGS_CallRodWs.inFutureContext){
                                TGS_EmailUtilsHandler.triggerPrepareEmail(caseIdListNew, previousStatus);
                            }
                        }
                    }
                }
              Eliminacion Acting As */
            /////////////////END TGS///////////////////////

        }
    }
}