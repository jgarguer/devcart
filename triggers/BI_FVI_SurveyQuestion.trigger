trigger BI_FVI_SurveyQuestion on Survey_Question__c (after update, after insert, before update, before insert)
{
	//GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Survey_Question__c');
    
    if(!isTriggerDisabled){
		if (trigger.IsAfter)
		{
		       BI_FVI_SurveyQuestionMethods.OnlyOneCSI(trigger.new);
		}
		 
		if (trigger.IsBefore)
		{
				BI_FVI_SurveyQuestionMethods.ValidateAnswersOnCSIQuestion(trigger.new);
		}
	}
}