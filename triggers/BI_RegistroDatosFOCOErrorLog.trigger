/*-------------------------------------------------------------------
	Author:         Virgilio Utrera
	Company:        Salesforce.com
	Description:    Inserts FOCO Error Log Ids into list of available FOCO Error Log Ids (custom setting)
	History
	<Date>          <Author>           <Change Description>
	26-Feb-2015     Virgilio Utrera    Initial version
	20-Jul-2017     Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

trigger BI_RegistroDatosFOCOErrorLog on BI_Registro_Datos_FOCO_Error_Log__c (after insert) {
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Registro_Datos_FOCO_Error_Log__c');
    
    if(!isTriggerDisabled){
		if(Trigger.isAfter) {
			BI_FOCOUtil.insertErrorLogIds(trigger.new);
		}
	}
}