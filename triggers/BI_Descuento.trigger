trigger BI_Descuento on BI_Descuento__c (before insert, before update, after update) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by the BI_Facturaccion trigger.
    
    History

    <Date>            <Author>          <Description>
    09/03/2015      Ignacio Llorca      Initial version
    20/07/2017      Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Descuento__c');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore){
            if (trigger.isInsert){
    			//BI_DescuentoMethods.blockEdition(trigger.new, trigger.old);
    			// FAR 27/05/2015: trigger.old cannot be used in insert context --> throws a null pointer exception 
    			BI_DescuentoMethods.blockEdition(trigger.new, new List<BI_Descuento__c>());
            } 
            if (trigger.isUpdate){
                BI_DescuentoMethods.blockEdition(trigger.new, trigger.old);
                BI_DescuentoMethods.updateAcc(trigger.new, trigger.old);
            }
            //if (trigger.isDelete){
            //}
        }else {  
            /*if (trigger.isInsert){ 
                
            }   
            else */
            if (trigger.isUpdate){
                BI_DescuentoMethods.updateDescMod(trigger.new, trigger.old);
            } 
            /*else if (trigger.isDelete){
            } 
            */
        }
    }
}