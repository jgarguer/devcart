trigger NEContractHeader on NE__Contract_Header__c (before insert, before update, before delete, after insert, 
	after update, after delete, after undelete) {

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián E.
    Company:       Accenture
    Description:   Trigger para controlar los métodos llamados de NE__Contract_Header__c ('Acuerdos Marco')
    History
    11/09/2017      Gawron, Julián E.     Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    BI_bypass__c bypass = BI_bypass__c.getInstance();
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c;   
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('NE__Contract_Header__c');
    System.debug('Entrando en NE__Contract_Header__c');
    if(!isTriggerDisabled){
 	    if (trigger.isBefore){
            if (Trigger.isInsert){
                if(BI){
                	BI_NEContractHeaderMethods.verificarPermisos(Trigger.new, Trigger.old);
                }
    		}else if(Trigger.isUpdate){
                if (BI){
                	BI_NEContractHeaderMethods.verificarPermisos(Trigger.new, Trigger.old);
                }
    		}else if(Trigger.isDelete){
                if (BI){
                	BI_NEContractHeaderMethods.verificarPermisos(Trigger.new, Trigger.old);
                }
    		}
		} else if (Trigger.isAfter) {
		//isAfter
           if (Trigger.isInsert){
                if(BI){
                
                }
            }else if(Trigger.isUpdate){
                if (BI){
                
                }
            }
		}
	}//!isTriggerDisabled
}