trigger CWP_CatItemConfiguration_Before on CWP_CatalogItemConfiguration__c (before Insert, before Update) {

	//GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('CWP_CatalogItemConfiguration__c');
    
    if(!isTriggerDisabled){
    	CWP_CatItemConfig_Util.receiveList(trigger.new, trigger.isInsert);
	}

}