trigger BI_Campanas_Cuenta on BI_Campanas_Cuenta__c (before insert, before delete) {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Trigger on BI_Campanas_Cuenta__c
    
    History:
    
    <Date>			  <Author> 	        <Description>
    13/11/2014        Pablo Oliva     	Initial version
    20/07/2017        Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Campanas_Cuenta__c');
    
    if(!isTriggerDisabled){
        if(trigger.isBefore){
            if(trigger.isInsert){
            	BI_Campanas_CuentaMethods.updateCampanaField(trigger.new, 0);
            }	
            //else if(trigger.isUpdate){ 
            //}
            else if(trigger.isDelete){
            	BI_Campanas_CuentaMethods.updateCampanaField(trigger.old, 1);
            }
        }
        
        	
        //else{ 
            //if(trigger.isInsert){ 
            //}
            //else if(trigger.isUpdate){
            //}
            //else if(trigger.isDelete){
            //}
            //else if(trigger.isUnDelete){
            //}         
        //}
    }
}