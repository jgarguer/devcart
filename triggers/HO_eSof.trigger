trigger HO_eSof on HO_e_SOF__c (before insert, before update, before delete, after update) {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Diego Arenas
    Company:       Salesforce.com
    Description:   Trigger for controlling HO_eSofMethods Methods
    History
    06/03/2018      Iñaki Frial       Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    if (trigger.isBefore){
        if (trigger.isInsert){
           
        }
        if (trigger.isUpdate){                    
        }
        if (trigger.isDelete){
        }
    }
    else { 
        if (trigger.isInsert){ 
        }
        //else if (trigger.isDelete){
        //} 
        else if (trigger.isUpdate){
         
	        HO_eSofMethods.updateServiceUnitsStage(trigger.new, trigger.oldMap);
	        
    	}	    
	}
}