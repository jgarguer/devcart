trigger ADQ_ContenedorProgramacion on Factura__c (before delete) {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        
    Company:       Salesforce.com
    Description:   Trigger on Question
    
    History:
    
    <Date>            <Author>                  <Description>
    09/05/2017        Cristina Rodríguez        Avoid SOQL Queries or DML statements inside FOR Loops
    20/07/2017        Guillermo Muñoz           Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Factura__c');

    if(!isTriggerDisabled){

		Map<String,List<Programacion__c>> mapaFactrura= new Map<String,List<Programacion__c>> ();
		
		for ( Programacion__c progra01 : [SELECT id, Name, Facturado__c,Exportado__c ,Folio_Fiscal__c,Migrado__c, Factura__c 
												FROM Programacion__c
												Where Factura__c IN :Trigger.oldMap.keySet()
													AND  (Folio_Fiscal__c != '' 
														OR
														Facturado__c = TRUE
														OR
														Exportado__c  = TRUE
														)
												]){
			if (!mapaFactrura.containsKey(progra01.Factura__c)){
				mapaFactrura.put (progra01.Factura__c, new List<Programacion__c> ());
			}
			mapaFactrura.get (progra01.Factura__c).add (progra01);
		}
	    
	    list<Factura__c> facturasToDelete = new list<Factura__c>();
		for (Factura__c factura2 : trigger.old){
			if (mapaFactrura.get(factura2.id) != null){
				factura2.addError ('No se puede eliminar el encabezado existen programaciones con folios fiscales o pendientes de facturar por SAP');
			}else {	
				facturasToDelete.add(factura2);
			}		
		}
		try{
			if(!facturasToDelete.isEmpty()){
				List <Database.DeleteResult> lst_toReturn;
				lst_toReturn = database.delete(facturasToDelete, false);
			}
		}Catch (Exception err){}


	    /*******OLD
		// validamos que la factura a eliminar.
		
		Set<String> setIdFactura = new  Set<String> ();
		Map<String,List<Programacion__c>> mapaFactrura= new Map<String,List<Programacion__c>> ();
		for (Factura__c factura : trigger.old){
			setIdFactura.add (factura.id);
			
		}
		for ( Programacion__c progra01 : [SELECT id, Name, Facturado__c,Exportado__c ,Folio_Fiscal__c,Migrado__c, Factura__c 
												FROM Programacion__c
												Where Factura__c IN: setIdFactura
													AND  (Folio_Fiscal__c != '' 
														OR
														Facturado__c = TRUE
														OR
														Exportado__c  = TRUE
														)
												]){
			if (!mapaFactrura.containsKey(progra01.Factura__c)){
				mapaFactrura.put (progra01.Factura__c, new List<Programacion__c> ());
			}
			mapaFactrura.get (progra01.Factura__c).add (progra01);
		}

		for (Factura__c factura2 : trigger.old){
			if (mapaFactrura.get(factura2.id) != null){
				factura2.addError ('No se puede eliminar el encabezado existen programaciones con folios fiscales o pendientes de facturar por SAP');
			}else {
				try{
						delete factura2;
				}Catch (Exception err){}
				
				
			}
			
		}
		*/
	}
}