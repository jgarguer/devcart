trigger Attachment on Attachment (before insert, before update, before delete, after insert, after update, after delete) {
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fco.Javier Sanz
    Company:       Salesforce.com
    Description:   Trigger that controls Attachment methods
    
    History:
    
    <Date>                  <Author>                 <Description>
    03/07/2014              Fco.Javier Sanz          Initial Version
    17/11/2014              Pablo Oliva              Method "preventDelete" added
    04/02/2016              Guillermo Muñoz          Method "preventInsertCHI" added
    10/02/2016              Fernando Arteaga         Add BI encapsulation to execute only from BI_EN
    15/05/2017              Cristina Rodríguez       Solve Multiple Trigger On same sObject 
    20/07/2017              Guillermo Muñoz          Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    22/11/2017              Álvaro López             INC000000123436 - Changed checkAttachmentNumber method call to avoid isDelete null
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    // FAR 10/02/2016 - Add BI encapsulation
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean TGS = userTGS.TGS_Is_TGS__c;

    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Attachment');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore){
            // FAR 10/02/2016 - Add BI encapsulation
            if(BI){
                if (trigger.isInsert){
                    BI_AttachmentMethods.validateImageAttachment(trigger.new);
                    BI_AttachmentMethods.preventInsertCHI(trigger.new);
                    BI_AttachmentMethods.verificarPermisosContract(trigger.new, trigger.old);
                }
                else if(trigger.isUpdate){
                    BI_AttachmentMethods.validateImageAttachment(trigger.new);
                    BI_AttachmentMethods.verificarPermisosContract(trigger.new, trigger.old);
                }
                if(trigger.isDelete){
                    BI_AttachmentMethods.preventDelete(trigger.old);
                    BI_AttachmentMethods.countAttachments(null, trigger.old);
                    BI_AttachmentMethods.createHistorialOppCase(null, trigger.old, 'Delete');
                    BI_AttachmentMethods.verificarPermisosContract(trigger.new, trigger.old);
                }       
            } // END FAR 10/02/2016 - Add BI encapsulation
            // START CRM 15/05/2017 - Added TGS_AttachmentTrigger.trigger
            if(TGS){
                
                if(Trigger.isInsert){
                    TGS_Attachment_Handler.checkAttachmentNumber(Trigger.new);
                    if(TGS && !TGS_CallRoDWs.inFutureContextAttachment){
                        TGS_IntegrationUtils.setDescriptionPosition(Trigger.New); 
                    }    
                }
            }
            // END CRM 15/05/2017 - Added TGS_AttachmentTrigger.trigger
            // START CRM 15/05/2017 - Added Attachment_tgr.trigger
            if(trigger.isDelete){
                TGS_Attachment_Handler.checkAttachmentNumber(Trigger.old);
                BI_COL_Attachment_cls deleteAttachmentCase = new BI_COL_Attachment_cls(trigger.old);
            }
            // END CRM 15/05/2017 - Added Attachment_tgr.trigger

            // if (trigger.isInsert) BI_AttachmentMethods.changeOwnerContractAttach(trigger.new);

            // Álvaro López 03-04-2018 - Added BI_AttachmentMethods.validateExtension method call
            if(trigger.isInsert){
                BI_AttachmentMethods.validateExtension(Trigger.new);
            }
        }
        if (trigger.isAfter){
            // FAR 10/02/2016 - Add BI encapsulation
            if(BI){
                if (trigger.isInsert){
                    BI_AttachmentMethods.sendEmail(trigger.new);
                    BI_AttachmentMethods.countAttachments(trigger.new, null);
                    BI_AttachmentMethods.createHistorialOppCase(trigger.new, null, 'Insert');

                }//else if(trigger.isUpdate){
                    //BI_AttachmentMethods.validateImageAttachment(trigger.old, trigger.new);
                //}
                else if(trigger.isUpdate){
                    BI_AttachmentMethods.createHistorialOppCase(trigger.new, null, 'Update');
                }
            } // END FAR 10/02/2016 - Add BI encapsulation
            // START CRM 15/05/2017 - Added TGS_AttachmentTrigger.trigger
            if(TGS && !TGS_CallRoDWs.inFutureContextAttachment){
                /* Integrates the attachment with RoD and UDO */
                if (Trigger.isDelete){
                    TGS_IntegrationUtils.sendAttachments(Trigger.Old, Trigger.oldMap.keySet());
                }else{
                    TGS_IntegrationUtils.sendAttachments(Trigger.New, Trigger.newMap.keySet());
                }
            }    
            // END CRM 15/05/2017 - Added TGS_AttachmentTrigger.trigger
        }
    }
}