/*------------------------------------------------------------------------
Author:        Humberto Nunes 
Company:       NEAborda
Description:   BI_FVI_Nodo_Cliente__c

History: 

<Date>                  <Author>                <Change Description>
12/07/2016              Humberto Nunes     		Initial version  
22/11/2016				Humberto Nunes 			Llamada al metodo actualizaRelacionadosACliente
23/11/2016				Humberto Nunes 			Llamada al metodo actualizaRelacionadosACliente tanto en el insert como en el update.
18/05/2017				Humberto Nunes			Se omitio la ejecución de actualizaRelacionadosACliente que habia subido de PreProd a Prod...
												Se dejo separado el actualizaNodoEnCliente del validaNodo ya que una modificacion de1 
												11/04/2017        Gawron, Julián          Adding {} to avoid delete null problems
												Agrupo los 2 en  if(trigger.isBefore)
    															 {
        															if(trigger.isUpdate || trigger.isInsert){//JEG
															            BI_FVI_NodoClienteMethods.validaNodo(trigger.new , trigger.old);
															            BI_FVI_NodoClienteMethods.actualizaNodoEnCliente(trigger.new , trigger.old);
															        }//JEG
        														 }
        										Lo que no se ejecutara nunca cuando fuese un DELETE y por ende nunca actualizara el Nodo en el Cliente. 
20/07/2017              Guillermo Muñoz         Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
23/04/2018              Javier Lopez            Added before delete add calls to BI_FVI_NodoClienteMethods.changeAccountTeamMember
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
trigger BI_FVI_Nodo_Cliente on BI_FVI_Nodo_Cliente__c (before insert, before update, after insert, after update, after delete,before delete) 
{

    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_FVI_Nodo_Cliente__c');
    
    if(!isTriggerDisabled){
        if(trigger.isBefore){
            BI_FVI_NodoClienteMethods.validaNodo(trigger.new , trigger.oldMap);
            if (trigger.isInsert){
                BI_FVI_NodoClienteMethods.changeAccountTeamMember(trigger.new, null);
            }
            if(trigger.isdelete)
                BI_FVI_NodoClienteMethods.changeAccountTeamMember(null,trigger.old);
        }
        
        
        if(trigger.isAfter){
        	BI_FVI_NodoClienteMethods.actualizaNodoEnCliente(trigger.new , trigger.old);
            if(trigger.isUpdate){
                BI_FVI_NodoClienteMethods.changeAccountTeamMember(trigger.new,trigger.old);
            }
        }

        // OMITIDO PORQUE SE CONTROLARA DESDE LA TABLET Y CON PERMISOS SOBRE LAS FUNCIONES (USER) DEL DISTRIBUIDOR
        // if(trigger.isAfter && (trigger.isUpdate || trigger.isInsert) )	
        //	BI_FVI_NodoClienteMethods.actualizaRelacionadosACliente(trigger.new , trigger.old);   
    }
}