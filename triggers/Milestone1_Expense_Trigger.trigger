trigger Milestone1_Expense_Trigger on Milestone1_Expense__c (before insert, before update) 
{

	//GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Milestone1_Expense__c');
    
    if(!isTriggerDisabled){
		if(Trigger.isBefore)
		{
			Milestone1_Expense_Trigger_Utility.handleExpenseBeforeTrigger(Trigger.new);
		}
	}
}