trigger Contact on Contact (before insert, before update,before delete,after delete, after insert, after update) {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Pablo Oliva
Company:       Salesforce.com
Description:   Methods executed by the Contact trigger.

History

<Date>            <Author>          <Description>
06/11/2014        Pablo Oliva       Initial version
27/11/2014        Ignacio Sarobe    Method SCP_UtilSyncFields.syncContacts added
10/12/2015		  Fernando Arteaga	BI_EN - CSB Integration
25/05/2016        Julio Laplaza     Communicate Contacts to RoD
29/07/2016        Guillermo Muñoz   System.isFuture() condition added on future methods to avoid exceptions
02/02/2017		  Marta Glez		REING-01-Adaptacion reingenieria de contactos
06/02/2017		  Eduardo Ventura	REING-02-Adaptacion reingenieria de contactos (BYPASS SKIP)
27/06/2017		  Marta Glez		Añadidas trazas paar el desarrollo de Fulltack (FS_CORE)
17/07/2017		  Marta Glez		Añadidas dos lineas pra la integracion con FS peru  (FS_CORE, lineas 114 y 119)
20/07/2017        Guillermo Muñoz   Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
04/09/2017        Guillermo Muñoz   Added BI_ContactMethods.sendConvertedContactToFullStack call
07/09/2017        Borja Maria        Añadidas dos lineas para la integracion con FS peru representanteLegal
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    Boolean FVI = userTGS.BI_FVI_Is_FVI__c;

    /* REING-02-INI: BYPASS MIGRATION SETUP */
    BI_bypass__c userBypass = BI_bypass__c.getInstance();
    
    /*FS_CORE-INI Marta Glez 27/06/2017*/
    /* CUSTOM SETTINGS */
    FS_CORE_Integracion__c ORGConfiguration = FS_CORE_Integracion__c.getInstance();
    /*FS_CORE-FIN Marta Glez 27/06/2017*/
    
    Boolean skip = userBypass.BI_skip_trigger__c && userBypass.BI_migration__c;
    /* REING-02-FIN: BYPASS MIGRATION SETUP */
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Contact');
    
    if(!isTriggerDisabled){
        if(trigger.isBefore && /*REING-02-INI*/!skip/*REING-02-FIN*/){        
            if(trigger.isInsert){
                // FAR 10/12/2015 - BI_EN - CSB Integration
                BI_ContactMethods.checkOnlyOneCSBAdmin(trigger.new);
                BI_ContactMethods.assignCountry(trigger.new);
                SCP_UtilSyncFields.syncContacts(trigger.new);
                UtilSync.ContactSync(Trigger.New);
                BI_ContactMethods.validateId(trigger.old, trigger.new);
            }
            if (trigger.isUpdate){
                BI_ContactMethods.validateId(trigger.old, trigger.new);
                SCP_UtilSyncFields.syncContacts(trigger.new);
                UtilSync.ContactSync(Trigger.New);
                
                
                /* INI FS_CORE 001 marta Glez-27/06/2017 */ if(!System.isFuture() && ORGConfiguration != null && ORGConfiguration.FS_CORE_Peru__c && FS_CORE_Fullstack_CheckRecursive.runOnce()){BI_ContactMethods.updateToFullstack(trigger.old, trigger.new);} /* FIN FS_CORE 001 marta Glez-27/06/2017 */
            }  
            
            if(trigger.isDelete){
                /* INI FS_CORE 001 marta Glez-27/06/2017*/ if(!System.isFuture() && ORGConfiguration != null && ORGConfiguration.FS_CORE_Peru__c && FS_CORE_Fullstack_CheckRecursive.runOnce()) BI_ContactMethods.deleteFromFullstack(trigger.old, trigger.new); /* FIN FS_CORE 001 marta Glez-27/06/2017*/
            }
        }
        else if(trigger.isAfter && BI_COL_FlagforTGRS_cls.flagTriggerTRSContacto && /*REING-02-INI*/!skip/*REING-02-FIN*/)
        {
            BI_COL_FlagforTGRS_cls.flagTriggerTRSContacto=false;
            String accion='';
            String strTipoNovedad=trigger.isDelete?label.BI_COL_LbRetiro:trigger.isUpdate?label.BI_COL_LbActualizacion:label.BI_COL_LbCreacion;
  
            List<String> lstIdContactos = new List<String>();
            
            if(trigger.isInsert){
                /* INI FS_CORE 001  marta Glez-27/06/2017*/ if(!System.isFuture() && ORGConfiguration != null && ORGConfiguration.FS_CORE_Peru__c && FS_CORE_Fullstack_CheckRecursive.runOnce()) 
                    BI_ContactMethods.insertToFullstack(trigger.old, trigger.new); /* FIN FS_CORE 001 marta Glez-27/06/2017*/
                    
                //BI_ContactMethods.sendConvertedContactToFullStack(Trigger.new);
            }
        

            if(Trigger.size<1000) 
            {
                List<Contact> listContact=trigger.isDelete?trigger.old:trigger.isUpdate?trigger.new:trigger.new;
                accion=trigger.isInsert?'Insertar':trigger.isUpdate?'Actualizar':'Eliminar';
                if(!trigger.isDelete){
                    for(Contact contacto : Trigger.new){
                        lstIdContactos.add(contacto.Id);
                    }
                }
                else{
                    for(Contact contacto : Trigger.old){
                        lstIdContactos.add(contacto.Id);
                    }
                }
                //Crear contactos servicio web telefonica
                if(lstIdContactos!=null && lstIdContactos.size()>0 && !system.isFuture()) //&& !Test.isRunningTest() )
                {
                    system.debug('----->ingresa al llamado del controlador SW trigger.isDelete: '+trigger.isDelete+' lstIdContactos.size(): '+lstIdContactos.size());
                    /*Álvaro López 30/11/2017 -  Added !TGS condition*/
                    if (!TGS) BI_COL_CreateContactWS_cls.crearContactoTelefonica(trigger.new,trigger.old,accion); //JRM 28/11/2017
                }
                //REING-01-INI
               	//if((listContact[0].BI_Tipo_de_contacto__c!=null && listContact[0].BI_Tipo_de_contacto__c.indexOf('Administrador Canal Online') != -1 && !system.isFuture())
              	if(((listContact[0].BI_Tipo_de_contacto__c!=null && listContact[0].BI_Tipo_de_contacto__c.indexOf('Administrador Canal Online') != -1) || listContact[0].FS_CORE_Acceso_a_Portal_Platino__c == true )&& !system.isFuture())
                    //REING-01-FIN
                {
                    String idContacto=listContact[0].id;
                    BI_COL_ChannelOnline_cls.contactCanalOnline(idContacto,strTipoNovedad);
                }
            }
        }
        
        // Communicate CONTACTs to Remedy
        if(Trigger.isAfter && Trigger.size < 1000 && /*REING-02-INI*/!skip/*REING-02-FIN*/)
        {
            if(Trigger.isInsert && !System.isFuture())
            {
                BIIN_UNICA_ContactMethods.createRoDContact(trigger.new);
            }
            else if(Trigger.isUpdate && !System.isFuture())
            {
                BIIN_UNICA_ContactMethods.updateRoDContact(trigger.new, trigger.old);
            }
        }
    }
}