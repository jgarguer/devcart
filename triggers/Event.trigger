trigger Event on Event (before insert, before update, after insert, after update) {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Diego Arenas
    Company:       Salesforce.com
    Description:   Trigger para controlar los métodos llamados de Event
    
    History:
    
    <Date>            <Author>           <Description>
    11/02/2014        Diego Arenas       Añadir la llamada a UtilTriggers.rellenaDescripcionEvent en before insert/update
    14/07/2014        Ana Escrich        Añadir la llamada a PCA_EventMethods.PCA_EventAfter en after insert
    14/11/2014        Pablo Oliva        Method "validateContact" added
    29/01/2015        Pablo Oliva        Method "preventEventForActionPlan" added
    12/02/2015        Micah BUrgos       Bypass added
    30/09/2015        Guillermo Muñoz    Method "checkRecurrenceEvent" added
    21/01/2016        Micah Burgos       Change !bypass.BI_migration__c to !bypass.BI_skip_trigger__c. BI_migration__c is used for ValidationRules and some code validations and BI_skip_trigger__c disable the trigger.    
    16/01/2017        Alvaro García      Add methods BI_EventMethods.updateEvent_Dates and BI_EventMethods.updateBid_Event
    29/05/2017        Humberto Nunes     Llenado del Nodo en el Evento... 
    20/07/2017      Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
    Boolean TGS = userTGS.TGS_Is_TGS__c;
    Boolean BI = userTGS.TGS_Is_BI_EN__c;
    
    BI_bypass__c bypass = BI_bypass__c.getInstance();
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('Event');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore && !bypass.BI_skip_trigger__c){
            if (trigger.isInsert){
                
                BI_EventMethods.preventEventForActionPlan(trigger.new);
                
                /*Se carga el campo description__c. Este trigger en ningún momento interrumpe la carga de Eventos.*/
                //Certa_SCP.UtilTriggers.rellenaDescripcionEvent(trigger.New);
                //PCA_EventMethods.PCA_Event(Trigger.new);
                for(Event item : trigger.new){
                    item.IsVisibleInSelfService = true;
                }
                
                BI_EventMethods.validateContact(trigger.new);
                BI_EventMethods.checkRecurrenceEvent(trigger.new);
                // 29/05/2017
                BI_EventMethods.fillNodoInEvent(trigger.new);
            }
            if (trigger.isUpdate){
                if(!bypass.BI_migration__c){
                    BI_EventMethods.check_HasClosedPlanDeAccion(trigger.new);

                    if (BI) {
                       BI_EventMethods.updateEvent_Dates(trigger.new, trigger.old); 
                    }
                    
                }
                BI_EventMethods.validateContact(trigger.new);
                
                PCA_EventMethods.sendEmail(Trigger.new, Trigger.old);
                /*Se carga el campo description__c. Este trigger en ningún momento interrumpe la carga de Eventos.*/
                //Certa_SCP.UtilTriggers.rellenaDescripcionEvent(trigger.New);
            }
        //if (trigger.isDelete){
        //}
        }
        else if(!bypass.BI_skip_trigger__c) { 
            //if (trigger.isInsert){ 
            //   PCA_EventMethods.sendEmail(Trigger.new);
            //}
            //else if (trigger.isDelete){
            //} 
            if (trigger.isUpdate){
                if (BI) {
                    BI_EventMethods.updateBid_Event(trigger.new, trigger.old);
                }
                BI_EventMethods.updateAccOfertaVisita(trigger.new, trigger.oldMap);
              //PCA_EventMethods.sendEmail(Trigger.new, Trigger.old);
            }
            if(trigger.isInsert) {
                BI_EventMethods.updateAccOfertaVisita(trigger.new, null);
            }
        }
    }
}