trigger SetStartEndDateContractHeader on NE__Contract__c (after insert, after update) {
    /*

    //Deprecated 
    //Added to BI_NEContractMethods
    //Trigger NEContract

    //GMN 20/07/2017 - Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('NE__Contract__c');
    
    if(!isTriggerDisabled){

        list<String> chToSearch = new list<String>();
        for(NE__Contract__c contract : Trigger.new)
            chToSearch.add(contract.NE__Contract_Header__c);
        
        map<String,NE__Contract_Header__c> mapOfCh  =   new map<String,NE__Contract_Header__c>([SELECT Id, NE__MaxVersion__c, Max_Active_Version_Start_Date__c, Max_Active_Version_End_Date__c 
                                                                                             FROM NE__Contract_Header__c
                                                                                             WHERE Id =: chToSearch]); 
        
        list<NE__Contract_Header__c> contractHeadersToUpdate = new list<NE__Contract_Header__c>();
        for(NE__Contract__c contract : Trigger.new) {
            
            NE__Contract_Header__c ch = mapOfCh.get(contract.NE__Contract_Header__c);
            
            if(Trigger.isInsert || (Trigger.isUpdate && contract.NE__Version__c == ch.NE__MaxVersion__c)) {
                ch.Max_Active_Version_Start_Date__c = contract.NE__Start_Date__c;
                ch.Max_Active_Version_End_Date__c = contract.NE__End_Date__c;
                contractHeadersToUpdate.add(ch);
            }
            
        }
        
        if(contractHeadersToUpdate.size() > 0)
        {
            Set<NE__Contract_Header__c> hSet   = new Set<NE__Contract_Header__c>();
            hSet.addAll(contractHeadersToUpdate);            
            
            list<NE__Contract_Header__c> cHToUpd =   new list<NE__Contract_Header__c>();
            cHToUpd.addAll(hSet);  
            
            update contractHeadersToUpdate;    
        }  
    }
    */
}