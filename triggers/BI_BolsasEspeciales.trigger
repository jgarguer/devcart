trigger BI_BolsasEspeciales on BI_Bolsas_especiales__c (before insert, before update, before delete) {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by the BI_Facturaccion trigger.
    
    History

    <Date>            <Author>          <Description>
    09/03/2015      Ignacio Llorca      Initial version
    20/07/2017      Guillermo Muñoz     Added BI_MigrationHelper.isTriggerDisabled functionality to disable the trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //GMN 20/07/2017
    Boolean isTriggerDisabled = BI_MigrationHelper.isTriggerDisabled('BI_Bolsas_especiales__c');
    
    if(!isTriggerDisabled){
        if (trigger.isBefore){
            if (trigger.isInsert){
            	BI_BolsaEspecialMethods.blockEdition(trigger.new);
            } 
            if (trigger.isUpdate){
            	BI_BolsaEspecialMethods.blockEdition(trigger.new);
            }
            if (trigger.isDelete){
            	BI_BolsaEspecialMethods.blockDelete(trigger.old);
            }
        }/*else {  
            if (trigger.isInsert){ 
                
            }   
            else if (trigger.isUpdate){
            	
            } 
            else if (trigger.isDelete){
            } 
            
        }*/
    }
}