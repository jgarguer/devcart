@isTest
private class BI_Duplicate_ProjectOnOpportunity_TEST
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián 
    Company:       Accenture    
    History:
    <Date>                  <Author>                    <Description>
    11/12/2017              Gawron, Julián             Initial Version D-000523
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@testSetup
	static void generaData(){
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
       BI_TestUtils.throw_exception = false;
            
       List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);

       BI_MigrationHelper.skipAllTriggers();

       List<Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);
       List <Opportunity> lst_opp = BI_Dataload.loadOpportunities(lst_acc[0].Id);

       BI_MigrationHelper.cleanSkippedTriggers();
              
       Milestone1_Project__c proj = new Milestone1_Project__c();       
       proj.Name='Project 1';
       proj.BI_Oportunidad_asociada__c = lst_opp[0].Id;
       //proj.BI_O4_Template__c = true;
       
       insert proj;
       
       Milestone1_Milestone__c rec = new Milestone1_Milestone__c();
        rec.Name = proj.Name + '' + Datetime.now().getTime();
        rec.Project__c = proj.id;
        rec.Complete__c = false; 
        rec.Kickoff__c = date.today();
        rec.Deadline__c = date.today()+1;
        rec.Description__c = 'Description for ' + rec.Name;
        rec.Expense_Budget__c = 20;
        rec.Hours_Budget__c = 50;
        rec.BI_Country__c=lst_pais[0];        
       insert rec;

       Milestone1_Milestone__c rec1 = new Milestone1_Milestone__c();
        rec1.Name = proj.Name + '2' + Datetime.now().getTime();
        rec1.Project__c = proj.id;
        rec1.Complete__c = false; 
        rec1.Kickoff__c = date.today();
        rec1.Deadline__c = date.today()+1;
        rec1.Description__c = 'Description for ' + rec.Name;
        rec1.Expense_Budget__c = 20;
        rec1.Hours_Budget__c = 50;
        rec1.BI_Country__c=lst_pais[0];
        rec1.Parent_Milestone__c = rec.Id;       
       insert rec1;

       List <Milestone1_Task__c> tsk_lst = new List <Milestone1_Task__c>();
       Set <Id> set_mtask = new set <Id>();
       for(Integer i=0;i< BI_Dataload.MAX_LOOP;i++){ //JEG Add MAX_LOOP        
           Milestone1_Task__c newTask = new Milestone1_Task__c();
           newTask.Assigned_To__c = UserInfo.getUserId();
           newTask.Project_Milestone__c = rec.Id;
           newTask.Description__c = 'Test Description';
           newTask.Name = 'test name' + string.valueof(i);
           newTask.Task_Stage__c = 'In Progress';
           tsk_lst.add(newTask);
       }
       insert tsk_lst;
       
       for(Milestone1_Task__c mtask1:tsk_lst){
           set_mtask.add(mtask1.Id);
       }
       List <Task> tsk = [SELECT Id FROM Task WHERE WhatId IN :set_mtask];


	}

	@isTest
	static void getuser_TEST()
	{
		String user = BI_Duplicate_ProjectOnOpportunity.getuser();
		System.assertNotEquals(null, user);

		BI_Duplicate_ProjectOnOpportunity.fetchLookUpValues('searchKeyWord', 'ObjectName');

		//public static void saveProject(Milestone1_Project__c con){
		BI_Duplicate_ProjectOnOpportunity.fetchLookUpValuesByTemplate('searchKeyWord', 'ObjectName');	
	}

	@isTest
	static void getProjectbyNameAndEdit_TEST()
	{

		Opportunity laOpp = [Select Id from Opportunity limit 1];
		Test.startTest();
		BI_Duplicate_ProjectOnOpportunity.getProjectbyNameAndEdit('Project 1', 'Proyect 2', laOpp.Id);
		Test.stopTest();
		List<Milestone1_Project__c> proyects = [Select Id from Milestone1_Project__c];
		System.assertNotEquals(1, proyects.size());
	}

    @isTest
	static void saveProject_TEST()
	{
       Opportunity laOpp = [Select Id from Opportunity limit 1];
	   Milestone1_Project__c proj = new Milestone1_Project__c();       
       proj.Name='Project 2';
       proj.BI_Oportunidad_asociada__c = laOpp.Id;   
	   BI_Duplicate_ProjectOnOpportunity.saveProject(proj);
	}
}