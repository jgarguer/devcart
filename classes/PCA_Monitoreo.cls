public without sharing class PCA_Monitoreo extends PCA_HomeController{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Micah Burgos
Company:       Aborda
Description:   Controller of service tracking page

History:

<Date>            <Author>              <Description>
26/06/2014        Micah Burgos       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public String tokenApex{get;set;}
    public BI_ImageHome__c ImgBack {get;set;}
    public Attachment AttImg {get;set;}
    public BI_ImageHome__c RelationImage {get;set;}
    public BI_ImageHome__c RelationImageMovi {get;set;}
    public BI_ImageHome__c RelationImageTel {get;set;}
    public Boolean RenderTGS{get;set;}
    //public Boolean RenderBIEN{get;set;}
    public Map <String,BI_Service_Tracking_URL_PP__c> map_stu {get{return loadStu();}set;}
    
    //public String repId { get ; set ; }
    // INI - Everis - Evo lightning CWP - Monitornig - 15/03/2017
    public List<BI_Service_Tracking_URL_PP__c> lst_st{get;set;}
    public Map <String,BI_Service_Tracking_URL_PP__c> mapa;
    
    public Map<String, String> mapaBIENImg{get;set;}
    public Map<String, String> mapaBIENLabel{get;set;}
    public Map<String, String> mapaBIENDescription{get;set;}
    public Map<String, String> mapaBIENUrl{get;set;}
    
    public Map<BI_Service_Tracking_URL_PP__c,String> mapPortalsURL {get;set;}
    public Map<BI_Service_Tracking_URL_PP__c,String> mapPortalsNAME {get;set;}
    public Map<BI_Service_Tracking_URL_PP__c,String> mapPortalsDISPLAYNAME {get;set;}
    public Map<BI_Service_Tracking_URL_PP__c,String> mapPortalsDESC {get;set;}
    public Map<BI_Service_Tracking_URL_PP__c,String> mapPortalsLOGO {get;set;}
    public static Map<String, String> AppValues{get;set;}
    public static List<String> AppNames{get;set;}
    // FIN - Everis - Evo lightning CWP - Monitornig - 15/03/2017
     
        
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Constructor
    History:
    
    <Date>            <Author>              <Description>
    26/06/2014        Micah Burgos       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/   
    public PCA_Monitoreo(){AppValues = new Map<String,String>();}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   method that check permissions
    History:
    
    <Date>            <Author>              <Description>
    26/06/2014        Micah Burgos       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    
     public PageReference checkPermissions (){
        try{
            //List<String> x = CWP_Monitoreo_WebService.getApplications('mnc.cdcop01@cspad-premad.wh.telefonica', 'salesforce-ws-user', 'sa1e870rceW8');
            
            //invokeApps();
            
            PageReference page = enviarALoginComm();  
            if(page == null){
                //getUser();
                getTGSUser();
                if(!RenderTGS)  getMapaBIEN();
                else invokeApps();
                loadInfo();
                loadStu();
            }
            return page;
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_ServiceTracking.checkPermissions', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis Murcia
    Company:       Everis
    Description:   Method that returns the map of apps (getter and setter)
    History:
    
    <Date>            <Author>              <Description>
    24/03/2017        Miguel Girón          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    public Map<String, BI_Service_Tracking_URL_PP__c> getmapa(){
        return loadStu();
    }
    
    public void setmapa(Map<String, BI_Service_Tracking_URL_PP__c> mapa){
        this.mapa = mapa;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   Return the image and description of the global page
    
    History:
    
    <Date>                    <Author>               <Description>
    17/12/2014                Antonio Moruno         Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfo(){
        try{
            Id ActAccId = BI_AccountHelper.getCurrentAccountId();
            Account SegAccount = [Select Id, Name,BI_Country__c,BI_Segment__c FROM Account Where Id=:ActAccId];
            List<BI_ImageHome__c> RelImg = new List<BI_ImageHome__c>();
            if(SegAccount.BI_Segment__c!=null){
                RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'Monitoreo' AND  RecordType.Name= 'General' AND BI_Segment__c =: SegAccount.BI_Segment__c LIMIT 1];
                
                if(!RelImg.isEmpty()) {
                    if(RelImg[0].BI_Segment__c!=null){
                        
                        if(RelImg[0].BI_Segment__c=='Empresas'){
                            RelationImageTel = RelImg[0];
                        }else if(RelImg[0].BI_Segment__c=='Negocios'){
                            RelationImageMovi = RelImg[0];
                        }
                    }
                }
            }else{
                RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'Monitoreo' AND  RecordType.Name= 'General'  AND BI_Segment__c='Empresas' LIMIT 1];
                if(!RelImg.isEmpty()){
                    RelationImageTel = RelImg[0];
                }else{
                    RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'Monitoreo' AND  RecordType.Name= 'General'  AND BI_Segment__c='Negocios' LIMIT 1];
                    if(!RelImg.isEmpty()){
                        RelationImageMovi = RelImg[0];
                    }
                    
                }
            }
            if(RelationImageTel!=null){
                
                ImgBack = RelationImageTel;
                
                Attachment AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImageTel.Id Limit 1];
                if(AttTarget!=null){
                    AttImg = AttTarget;
                }
            }else if(RelationImageMovi!=null){
                
                ImgBack = RelationImageMovi;
                
                Attachment AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImageMovi.Id Limit 1];
                if(AttTarget!=null){
                    AttImg = AttTarget;
                }
            }}catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_Monitoreo.loadInfo', 'Portal Platino', Exc, 'Class');
            
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda
    Description:   Method that load the url of the country of the curren user from 'Service Tracking URL | PP' object.
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    30/12/2015                      Guillermo Muñoz             Initial version
    14/03/2017                      Everis                      Logic for Retrieving Apps modified to include TGS profiles and Oracle permissions
    24/03/2017                      Everis                      Logic for Showing the new Apps for TGS Users (obsolete version commented)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Map <String,BI_Service_Tracking_URL_PP__c> loadStu(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            lst_st = new List<BI_Service_Tracking_URL_PP__c>();
            mapa = new Map <String,BI_Service_Tracking_URL_PP__c>();
            system.debug('entrando método loadStu');
            // INI - Everis - 14/03/2017 - Evo visibilidad modificada para incluir perfiles TGS y permisos Oracle
            // Everis - 31/03/2017 - Se distingue a usuarios TGS mediante Custom Setting
            TGS_User_Org__c p = TGS_User_Org__c.getInstance();
                        System.debug(' TGS_User_Org__c p-->'+p );
            //if(p!=null && p.Profile_Name__c.contains('TGS')){
            if(p!=null && p.TGS_Is_TGS__c){
                List<String> privateApps= new List<String>(); 
                //Solo se llama a la invocación si el perfil es TGS - Everis - 31/03/2017
                //invokeApps();
                system.debug('lista resultado1:'+AppValues.isEmpty());
                //lst_st=[SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c FROM BI_Service_Tracking_URL_PP__c WHERE (CWP_TGS_Users__c = true AND CWP_isPublic__c = true) OR CWP_App_Name__c IN :AppNames];
                if(!AppValues.keySet().isEmpty() && AppValues  != null )
                    lst_st=[SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c /* INI CWP 001 */, CWP_App_DisplayName__c /* FIN CWP 001 */ FROM BI_Service_Tracking_URL_PP__c WHERE (CWP_TGS_Users__c = true AND CWP_isPublic__c = true) OR CWP_App_Name__c IN :AppValues.keySet()];
                //lst_st=[SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c FROM BI_Service_Tracking_URL_PP__c WHERE CWP_App_Name__c IN :AppNames ];
                else
                   lst_st=[SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c /* INI CWP 001 */, CWP_App_DisplayName__c /* FIN CWP 001 */ FROM BI_Service_Tracking_URL_PP__c WHERE CWP_TGS_Users__c = true AND CWP_isPublic__c = true];
                //SOLO SI SON TGS SE PROCESARÁN ESTOS MAPAS - 03/04/2017 - Everis
                mapPortalsURL = new Map<BI_Service_Tracking_URL_PP__c,String>();
                mapPortalsNAME = new Map<BI_Service_Tracking_URL_PP__c,String>();
                /* INI CWP 001 */ mapPortalsDISPLAYNAME = new Map<BI_Service_Tracking_URL_PP__c,String>(); /* FIN CWP 001 */
                mapPortalsDESC = new Map<BI_Service_Tracking_URL_PP__c,String>();
                mapPortalsLOGO = new Map<BI_Service_Tracking_URL_PP__c,String>();
                system.debug('lista resultado:'+lst_st);
                 
                if(!lst_st.isEmpty()){
                    for(BI_Service_Tracking_URL_PP__c stu : lst_st){
                        mapa.put(stu.CWP_App_Name__c, stu);   
                        //Añadir página de error
                        //mapPortalsURL.put(stu,stu.BI_URL__c);
                        if(AppValues.containsKey(stu.CWP_App_Name__c) && AppValues.get(stu.CWP_App_Name__c) != null){
                            mapPortalsURL.put(stu,AppValues.get(stu.CWP_App_Name__c));
                        }else{
                            if(stu.BI_URL__c != null)   mapPortalsURL.put(stu, stu.BI_URL__c);
                            else    mapPortalsURL.put(stu, Label.TGS_CWP_No_URL);
                        }
                        IF(stu.CWP_App_Name__c==null)   mapPortalsNAME.put(stu,Label.TGS_CWP_NO_Name);
                        else mapPortalsNAME.put (stu,stu.CWP_App_Name__c);
                        
                        /* INI CWP 001 */
                        IF(stu.CWP_App_DisplayName__c==null)   mapPortalsDISPLAYNAME.put(stu,Label.TGS_CWP_NO_DisplayName);
                        else mapPortalsDISPLAYNAME.put (stu, stu.CWP_App_DisplayName__c);
                        /* FIN CWP 001 */
                        
                        IF(stu.CWP_Description__c==null)    mapPortalsDESC.put(stu,Label.TGS_CWP_NO_Descr);
                        else    mapPortalsDESC.put(stu,stu.CWP_Description__c);
                        
                        mapPortalsLOGO.put(stu,stu.CWP_Logo__c);
                        
                    }
                }else{
                    /* INI CWP 001 (NAME -> DISPLAYNAME) */ mapPortalsDISPLAYNAME /* FIN CWP 001 */ .put(new BI_Service_Tracking_URL_PP__c(CWP_App_Name__c = 'Vacio' /* INI CWP 001 */ , CWP_App_DisplayName__c = 'Vacio' /* FIN CWP 001 */), 'No existen aplicaciones para mostrar');
                }
            }else{
                lst_st = [SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c /* INI CWP 001 */ , CWP_App_DisplayName__c /* FIN CWP 001 */ FROM BI_Service_Tracking_URL_PP__c WHERE BI_Country__c =: usuarioCustomerPortal.BI_Cliente__r.BI_Country__c];
                
                System.debug('lst_st--<'+lst_st+'>');
                if(!lst_st.isEmpty()){
                    for(BI_Service_Tracking_URL_PP__c stu:lst_st){
                        mapa.put(stu.CWP_App_Name__c, stu);
                    }
                }
            }
            //Query Original: BI_Service_Tracking_URL_PP__c [] lst_stu = [SELECT BI_URL__c,BI_Section__c FROM BI_Service_Tracking_URL_PP__c WHERE BI_Country__c =: usuarioCustomerPortal.BI_Cliente__r.BI_Country__c];
            // FIN - Everis - 14/03/2017 - Evo visibilidad modificada para incluir perfiles TGS y permisos Oracle
            System.debug('devuelvo-->>>'+mapa);
            
            return mapa;
        }catch (Exception exc){
            BI_LogHelper.generate_BILog('PCA_Monitoreo.loadStu', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Girón
    Company:       Everis
    Description:   Method that performs the Callout to Oracle WS to get the visible Apps for the User Monitoring
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    14/03/2017                      Miguel Girón                Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public static void invokeApps(){
        if(!Cache.Session.contains('local.CWPexcelExport.AppValues')){
            try{
                system.debug('entrando ws');  
                AppValues  = new Map<String,String>();
                //List<String> x = CWP_Monitoreo_WebService.getApplications('mnc.cdcop01@cspad-premad.wh.telefonica', 'salesforce-ws-user', 'sa1e870rceW8');
                List<List<String>> x = new List<List<String>>();
                User SSOUser = [SELECT FederationIdentifier FROM User WHERE Id = :UserInfo.getUserId()];
                String varAux= (String)SSOUser.FederationIdentifier;
                if(Test.isRunningTest()){
                    List<String> y = new List<String>();
                    List<String> TestApp = new List<String>();
                    y.add('ok');
                    TestApp.add('SalesForce');
                    TestApp.add('www.mockURL.com');
                    x.add(y);
                    x.add(TestApp);
                }else{
                    //if(varAux.contains('@cspad-premad.wh.telefonica')) 
                    x = CWP_Monitoreo_WebService.getApplications(SSOUser.FederationIdentifier);
                    system.debug('lista final:'+x);   
                }
                if(x[0][0].toLowerCase().equals('ok')){
                    for(Integer i = 1; i < x.size(); i++){
                        AppValues.put(x[i][0],x[i][1]);
                    }
                    Cache.Session.put('local.CWPexcelExport.AppValues', AppValues);
                }else{
                    BI_LogHelper.generateLog('PCA_Monitoreo.invokeApps', 'Portal Platino', x[0][0] + ': ' + x[0][1], 'Oracle Callout');
                    System.debug('Error la llamada: '+ '\n'+'codigo de error '+x[0][0] + '\n'+'descripción '+x[0][1]);
                }
            }catch(Exception e){
                BI_LogHelper.generate_BILog('PCA_Monitoreo.invokeApps', 'Portal Platino', e, 'Class');
            }
        }else{
            AppValues = new Map<String, String>();
            AppValues = (Map<String,String>)Cache.Session.get('local.CWPexcelExport.AppValues');
        }              
        System.debug('DBG '+AppNames);
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Pina
    Company:       Everis
    Description:   Method to load Monitoring configuration. Lightning.

    History: 

    <Date>                          <Author>                    <Change Description>
    15/03/2017                      Alberto Pina                Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    @auraEnabled
    public static List<BI_Service_Tracking_URL_PP__c> getAplis(){
       try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            List<BI_Service_Tracking_URL_PP__c> lst_st = new List<BI_Service_Tracking_URL_PP__c>();
            system.debug('entrando método getAplis');    
           
            TGS_User_Org__c p = TGS_User_Org__c.getInstance();
            
            System.debug(' TGS_User_Org__c p-->'+p );
            if(p!=null && p.TGS_Is_TGS__c){
               if(!AppValues.keySet().isEmpty() && AppValues  != null )
                    lst_st=[SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c /* INI CWP 001 */ , CWP_App_DisplayName__c  /* FIN CWP 001 */ FROM BI_Service_Tracking_URL_PP__c WHERE (CWP_TGS_Users__c = true AND CWP_isPublic__c = true) OR CWP_App_Name__c IN :AppValues.keySet()];
                    //lst_st=[SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c FROM BI_Service_Tracking_URL_PP__c WHERE CWP_App_Name__c IN :AppNames ];
                else
                   lst_st=[SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c /* INI CWP 001 */ , CWP_App_DisplayName__c  /* FIN CWP 001 */ FROM BI_Service_Tracking_URL_PP__c WHERE CWP_TGS_Users__c = true AND CWP_isPublic__c = true];
                
                Map<BI_Service_Tracking_URL_PP__c,String> mapPortalsURL = new Map<BI_Service_Tracking_URL_PP__c,String>();
                Map<BI_Service_Tracking_URL_PP__c,String> mapPortalsNAME = new Map<BI_Service_Tracking_URL_PP__c,String>();
                /* INI CWP 001 */ Map<BI_Service_Tracking_URL_PP__c,String> mapPortalsDISPLAYNAME = new Map<BI_Service_Tracking_URL_PP__c,String>(); /* FIN CWP 001 */
                Map<BI_Service_Tracking_URL_PP__c,String> mapPortalsDESC = new Map<BI_Service_Tracking_URL_PP__c,String>();
                Map<BI_Service_Tracking_URL_PP__c,String> mapPortalsLOGO = new Map<BI_Service_Tracking_URL_PP__c,String>();
                system.debug('lista resultado:'+lst_st);
                 
                if(!lst_st.isEmpty()){
                    for(BI_Service_Tracking_URL_PP__c stu : lst_st){
                        //mapa.put(stu.CWP_App_Name__c, stu);   
                        //Añadir página de error
                        //mapPortalsURL.put(stu,stu.BI_URL__c);
                        if(AppValues.containsKey(stu.CWP_App_Name__c) && AppValues.get(stu.CWP_App_Name__c) != null){
                            mapPortalsURL.put(stu,AppValues.get(stu.CWP_App_Name__c));
                        }else{
                            if(stu.BI_URL__c != null)   mapPortalsURL.put(stu, stu.BI_URL__c);
                            else    mapPortalsURL.put(stu, Label.TGS_CWP_No_URL);
                        }
                        IF(stu.CWP_App_Name__c == null)   mapPortalsNAME.put(stu,Label.TGS_CWP_NO_Name);
                        else mapPortalsNAME.put (stu,stu.CWP_App_Name__c);
                        
                        /* INI CWP 001 */
                        IF(stu.CWP_App_DisplayName__c == null)   mapPortalsDISPLAYNAME.put(stu, Label.TGS_CWP_NO_DisplayName);
                        else mapPortalsDISPLAYNAME.put (stu, stu.CWP_App_DisplayName__c);
                        /* FIN CWP 001 */
                        
                        IF(stu.CWP_Description__c == null)    mapPortalsDESC.put(stu,Label.TGS_CWP_NO_Descr);
                        else    mapPortalsDESC.put(stu,stu.CWP_Description__c);
                        
                        mapPortalsLOGO.put(stu,stu.CWP_Logo__c);
                        
                    }
                }else{
                    /* INI CWP 001 (NAME -> DISPLAYNAME) */ mapPortalsDISPLAYNAME /* FIN CWP 001 */ .put(new BI_Service_Tracking_URL_PP__c(CWP_App_Name__c = 'Vacio' /* INI CWP 001 */ , CWP_App_DisplayName__c = 'Vacio' /* FIN CWP 001 */), 'No existen aplicaciones para mostrar');
                }
            }/*else{
                //lst_st = [SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c FROM BI_Service_Tracking_URL_PP__c WHERE BI_Country__c =: usuarioCustomerPortal.BI_Cliente__r.BI_Country__c];
            }*/
            return lst_st;
        }catch (Exception exc){
            BI_LogHelper.generate_BILog('PCA_Monitoreo.loadStu', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Guillermo Muñoz
Company:       Aborda
Description:   Method that return the url of 'Visión Consolidada' section.

History: 

<Date>                          <Author>                    <Change Description>
30/12/2015                      Guillermo Muñoz             Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
     public String getUrlConsolidatedView(){
        
        String url = 'KO';
        try{
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            
            if(usuarioCustomerPortal.BI_SW_User__c != null && usuarioCustomerPortal.BI_SW_Pwd__c != null){
                if(map_stu != null && !map_stu.isEmpty()){
                    if(map_stu.containsKey('Consolidated View')){
                        url = map_stu.get('Consolidated View').BI_URL__c;
                        url = url.replace('{user}', usuarioCustomerPortal.BI_SW_User__c);
                        url = url.replace('{pass}', usuarioCustomerPortal.BI_SW_Pwd__c);
                    }
                }
            }
        }catch (Exception exc){
            BI_LogHelper.generate_BILog('PCA_Monitoreo.getUrlConsolidatedView', 'Portal Platino', Exc, 'Class');
        }
        return url;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Guillermo Muñoz
Company:       Aborda
Description:   Method that return the url of 'Top 10 Desempeño' section.

History: 

<Date>                          <Author>                    <Change Description>
30/12/2015                      Guillermo Muñoz             Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String getUrltopPerformance(){
        
        String url = 'KO';
        
        try{
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            
            if(usuarioCustomerPortal.BI_SW_User__c != null && usuarioCustomerPortal.BI_SW_Pwd__c != null){
                if(map_stu != null && !map_stu.isEmpty()){
                    
                    if(map_stu.containsKey('Top Performance')){
                        url = map_stu.get('Top Performance').BI_URL__c;
                        url = url.replace('{user}', usuarioCustomerPortal.BI_SW_User__c);
                        url = url.replace('{pass}', usuarioCustomerPortal.BI_SW_Pwd__c);
                    }
                }
            }
        }catch (Exception exc){
            BI_LogHelper.generate_BILog('PCA_Monitoreo.getUrltopPerformance', 'Portal Platino', Exc, 'Class');
        }
        return url;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Guillermo Muñoz
Company:       Aborda
Description:   Method that return the url of 'Calidad de Res VoIP' section

History: 

<Date>                          <Author>                    <Change Description>
30/12/2015                      Guillermo Muñoz             Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String getUrlnetworkQuality(){
        
        String url = 'KO';
        
        try{
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            
            if(usuarioCustomerPortal.BI_SW_User__c != null && usuarioCustomerPortal.BI_SW_Pwd__c != null){
                if(map_stu != null && !map_stu.isEmpty()){
                    
                    if(map_stu.containsKey('Network Quality')){
                        url = map_stu.get('Network Quality').BI_URL__c;
                        url = url.replace('{user}', usuarioCustomerPortal.BI_SW_User__c);
                        url = url.replace('{pass}', usuarioCustomerPortal.BI_SW_Pwd__c);
                    }
                }
            }
        }catch (Exception exc){
            BI_LogHelper.generate_BILog('PCA_Monitoreo.getUrlnetworkQuality', 'Portal Platino', Exc, 'Class');
        }
        return url;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Guillermo Muñoz
Company:       Aborda
Description:   Method that return the url of 'Gestión de tráfico' section

History: 

<Date>                          <Author>                    <Change Description>
30/12/2015                      Guillermo Muñoz             Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String getUrltrafficManagement(){
        
        String url = 'KO';
        
        try{
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            
            if(usuarioCustomerPortal.BI_SW_User__c != null && usuarioCustomerPortal.BI_SW_Pwd__c != null){
                if(map_stu != null && !map_stu.isEmpty()){
                    
                    if(map_stu.containsKey('Traffic Management')){
                        url = map_stu.get('Traffic Management').BI_URL__c;
                        url = url.replace('{user}', usuarioCustomerPortal.BI_SW_User__c);
                        url = url.replace('{pass}', usuarioCustomerPortal.BI_SW_Pwd__c);
                    }
                }
            }
        }catch (Exception exc){
            BI_LogHelper.generate_BILog('PCA_Monitoreo.getUrltrafficManagement', 'Portal Platino', Exc, 'Class');
        }
        return url;
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        everis
Company:       everis
Description:   Aux method to get Profile by User Id

History: 

<Date>                          <Author>                    <Change Description>
28/03/2017                      everis                       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*public Profile getProfile()
    {   
        Profile p = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        return p;
    }*/
    public void getTGSUser(){
        if(Test.isRunningTest() && PCA_Monitoreo_TEST.TGSUSer)  RenderTGS = true;
        else    RenderTGS = TGS_User_Org__c.getInstance().TGS_Is_TGS__c;
        //RenderBIEN = TGS_User_Org__c.getInstance().TGS_Is_BI_EN__c;
    }
    public void getMapaBIEN(){
        mapaBIENImg = new Map<String, String>();
        mapaBIENLabel = new Map<String, String>();
        mapaBIENDescription = new Map<String, String>();
        mapaBIENUrl = new Map<String, String>();
        String[] ImageURL = new List<String>();
        String[] Labels = new List<String>();
        String[] Description = new List<String>();
        AppNames = new List<String>{'VisionConsolidada','Top10Desempeno','CalidadDeRed','GestionDeTrafico'};
        StaticResource sr = [SELECT Id, Name, NamespacePrefix, SystemModStamp FROM StaticResource WHERE Name = 'PCA_Template_Resources'];
        String BaseURL = '';
        if(sr != null){
           //Resource URL
           BaseURL = '/empresasplatino/resource/' + sr.SystemModStamp.getTime() + '/' + (sr.NamespacePrefix != null && sr.NamespacePrefix != '' ? sr.NamespacePrefix + '__' : '') + 'PCA_Template_Resources/platino_zoomin/Imagens/Icones/';
           mapaBIENImg.put(AppNames[0],BaseURL+'Buscar.png');
           mapaBIENImg.put(AppNames[1],BaseURL+'top10.png');
           mapaBIENImg.put(AppNames[2],BaseURL+'Analise.png');
           mapaBIENImg.put(AppNames[3],BaseURL+'Gestion.png');
           mapaBIENLabel.put(AppNames[0],Label.PCA_Vision_Consolidada);
           mapaBIENLabel.put(AppNames[1],Label.PCA_Top_10_Desempeno);
           mapaBIENLabel.put(AppNames[2],Label.PCA_Calidad_de_Red);
           mapaBIENLabel.put(AppNames[3],Label.PCA_Gestion_de_trafico);
           mapaBIENDescription.put(AppNames[0],'Consulte el estado de su red de forma centralizada.');
           mapaBIENDescription.put(AppNames[1],'Identifique los casos mas relevantes.');
           mapaBIENDescription.put(AppNames[2],'Supervise sus rutas y el tráfico de voz.');
           mapaBIENDescription.put(AppNames[3],'Conozca el tráfico cursado en tiempo real.'); 
           mapaBIENUrl.put(AppNames[0],'https://www.clientesestrategicos.tdatabrasil.net.br'); //:447/Padesempenho_network.aspx?cod_usuario='+usuarioSW+'&token='+tokenSW+'&idioma=ES'
           mapaBIENUrl.put(AppNames[1],'https://www.clientesestrategicos.tdatabrasil.net.br'); //:447/Pages/desempenho_top10.aspx?cod_usuario='+usuarioSW+'&token='+tokenSW+'&idioma=ES'
           mapaBIENUrl.put(AppNames[2],'https://www.clientesestrategicos.tdatabrasil.net.br');
           mapaBIENUrl.put(AppNames[3],'https://www.clientesestrategicos.tdatabrasil.net.br');
        }
    }
}