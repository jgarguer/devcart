public class BI_SCP_BigDeals_Triggers {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:			Gustavo Kupcevich
    Company:		Certa
    Description:	
    
    History

    <Date>				<Author>			<Description>
    29/10/2015			Gustavo Kupcevich	Initial version
    20/02/2017          Alvaro Garcia       Create a static map to avoid querys
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static Map<string, Certa_SCP__Limite_de_Big_deal_por_pais__c> limite_Big_Deal;

    public static void insertOpportunityBefore(List<Opportunity> added) {
        try {
			commonBeforeUpsert(added);
        }
        catch(Exception e) {
            BI_LogHelper.generate_BILog('BI_SCP_BigDeals_Triggers.insertOpportunityBefore', 'Big_Deal', e, 'Trigger');
        }
	}
	
	public static void insertOpportunityAfter(List<Opportunity> added) {
        try {

        	setSurveysFields(added);
        }
        catch(Exception e) {
            BI_LogHelper.generate_BILog('BI_SCP_BigDeals_Triggers.insertOpportunityAfter', 'Big_Deal', e, 'Trigger');
        }
	}
	
	public static void updateOpportunityBefore(List<Opportunity> changed, Map<Id, Opportunity> original) {
        try{
        	commonBeforeUpsert(changed);
        }
        catch(Exception e) {
            BI_LogHelper.generate_BILog('BI_SCP_BigDeals_Triggers.updateOpportunityBefore', 'Big_Deal', e, 'Trigger');
        }
	}
	
    public static void updateOpportunityAfter(List<Opportunity> changed, Map<Id, Opportunity> original) {
        try {

            setSurveysFields(changed);
        }
        catch(Exception e) {
            BI_LogHelper.generate_BILog('BI_SCP_BigDeals_Triggers.updateOpportunityAfter', 'Big_Deal', e, 'Trigger');
        }
	}
    
	public static void deleteOpportunity(List<Opportunity> deleted) {
        try {
            List<Id> BigDealOpId = new List<Id>();
            for(Opportunity op : deleted) {
                if(op.Certa_SCP__BigDeals__c) {
                    BigDealOpId.add(op.id);
                }
            }
            
            List<Certa_SCP__Big_deals_Survey__c> surveys = [SELECT Id FROM Certa_SCP__Big_deals_Survey__c WHERE Certa_SCP__Oportunidad__c IN :BigDealOpId];
            delete surveys;
        }
        catch(Exception e) {
            BI_LogHelper.generate_BILog('BI_SCP_BigDeals_Triggers.deleteOpportunity', 'Big_Deal', e, 'Trigger');
        }
	}
    
    public static void updateAccountAfter(List<Account> changed, Map<Id, Account> original) {
        try {
            List<Id> accountChanged = new List<Id>();
            for (Account ac : changed) {
                if(ac.owner.Name != original.get(ac.Id).owner.Name || ac.BI_Riesgo__c != original.get(ac.Id).BI_Riesgo__c) {
                    accountChanged.add(ac.Id);
                }
            }
            if (accountChanged.size() != 0) {
                List<Opportunity> ops = [SELECT Id, Certa_SCP__BigDeals__c, Certa_SCP__Manual_Big_Deal__c FROM Opportunity WHERE AccountId IN :accountChanged AND Certa_SCP__BigDeals__c = true];

                setSurveysFields(ops);
            }
        }
        catch(Exception e) {
            BI_LogHelper.generate_BILog('BI_SCP_BigDeals_Triggers.updateAccountAfter', 'Big_Deal', e, 'Trigger');
        }
	}
	//------------------------------------------------------------------------------
    
    private static void setSurveysFields(List<Opportunity> changed) {
        List<Id> opportunityIds = new List<Id>();
        
        for (Opportunity op :changed) {
            if (op.Certa_SCP__BigDeals__c) {
                opportunityIds.add(op.id);
            }
        } 

        List<OpportunityCompetitor> allCompetitors  = new List<OpportunityCompetitor>();
        Map<Id, List<OpportunityCompetitor>> competitorsMap = new Map<Id, List<OpportunityCompetitor>>();
        List<Opportunity> bigDealOpps = new List<Opportunity>();
        Map<Id, Certa_SCP__Big_deals_Survey__c> surveyMap = new Map<Id, Certa_SCP__Big_deals_Survey__c>();

        if(!opportunityIds.isEmpty()){
            
            allCompetitors = [SELECT CompetitorName, OpportunityId FROM OpportunityCompetitor WHERE IsDeleted = false AND OpportunityId IN :opportunityIds];
            
            for(OpportunityCompetitor competitor : allCompetitors) {
                if(competitorsMap.get(competitor.OpportunityId) == null) {
                    competitorsMap.put(competitor.OpportunityId, new List<OpportunityCompetitor>());
                }
                competitorsMap.get(competitor.OpportunityId).add(competitor);
            }
            
            bigDealOpps = [select id, CurrencyIsoCode, BI_Duracion_del_contrato_Meses__c, BI_Ingresos_estimados_ano_en_curso_EAI__c, BI_Id_Interno_de_la_Oportunidad__c, BI_Fecha_de_entrega_de_la_oferta__c, BI_Country__c, account.owner.Name, account.BI_Riesgo__c,
                                            CloseDate, BI_Fecha_de_instalacion__c, amount, BI_Net_annual_value_NAV__c, BI_ingreso_por_unica_vez__c, BI_Recurrente_bruto_mensual__c, BI_ARG_CAPEX_total__c
                                            from Opportunity where id in :opportunityIds];
            
            
            For (Certa_SCP__Big_deals_Survey__c survey :[select id, Certa_SCP__NAV_o_ingresos_proximos_12_meses__c, Certa_SCP__Duracion_del_proyecto__c,  Certa_SCP__Oportunidad__c, Certa_SCP__Competencia__c, Certa_SCP__Pais__c, Certa_SCP__Responsable_comercial__c, Certa_SCP__Codigo_en_Sales_Force_del_proyecto__c, 
                                                        Certa_SCP__Es_moroso_con_el_Grupo__c, Certa_SCP__fecha_de_entrega__c, Certa_SCP__Fecha_estimada_del_fallo__c, Certa_SCP__Fecha_comienzo_de_contrato__c,
                                                        Certa_SCP__FCV_o_valor_total_de_contrato__c, Certa_SCP__Valor_estimado_one_shot__c, Certa_SCP__Valor_estimado_ingresos_recurrentes__c, Certa_SCP__Capex_total__c
                                                        from Certa_SCP__Big_deals_Survey__c where Certa_SCP__Oportunidad__c in :opportunityIds]) {
                surveyMap.put(survey.Certa_SCP__Oportunidad__c, survey);
            }
        }
        
        List<Certa_SCP__Big_deals_Survey__c> surveysToUpsert = new List<Certa_SCP__Big_deals_Survey__c>();
        for (Opportunity op :bigDealOpps) {
            Certa_SCP__Big_deals_Survey__c survey = surveyMap.get(op.id);
            decimal conversionRate = GetConversionRate(op.CurrencyIsoCode);
            final integer million = 1000000;
            if (survey == null) {
                //Create Survey
                survey = new Certa_SCP__Big_deals_Survey__c();
                survey.Certa_SCP__Oportunidad__c = op.Id;
                survey.CurrencyIsoCode = 'USD';
            }
            //Set Fields
            //General_information
            survey.Certa_SCP__Pais__c = op.BI_Country__c;
            system.debug(survey.Certa_SCP__Pais__c);
            survey.Certa_SCP__Responsable_comercial__c = op.account.owner.Name;
            survey.Certa_SCP__Codigo_en_Sales_Force_del_proyecto__c = op.BI_Id_Interno_de_la_Oportunidad__c;
            if (op.account.BI_Riesgo__c != null) {
                survey.Certa_SCP__Es_moroso_con_el_Grupo__c = (op.account.BI_Riesgo__c.startsWith('1')) ? 'No' : 'Si';
            } else {
                survey.Certa_SCP__Es_moroso_con_el_Grupo__c = 'NS/NC';
            }
            
            //offer_schedule
            /*
            if (op.BI_Fecha_de_entrega_de_la_oferta__c != null) {
                survey.Certa_SCP__fecha_de_entrega__c = Date.newInstance(op.BI_Fecha_de_entrega_de_la_oferta__c.year(), op.BI_Fecha_de_entrega_de_la_oferta__c.month(), op.BI_Fecha_de_entrega_de_la_oferta__c.day());
            }
            else {
                survey.Certa_SCP__fecha_de_entrega__c = null;
            }
            */
            survey.Certa_SCP__Fecha_estimada_del_fallo__c = op.CloseDate;
            survey.Certa_SCP__Fecha_comienzo_de_contrato__c = op.BI_Fecha_de_instalacion__c;
            
            //Economical_information
            if(op.BI_Ingresos_estimados_ano_en_curso_EAI__c != null){
                survey.Certa_SCP__NAV_o_ingresos_proximos_12_meses__c = op.BI_Ingresos_estimados_ano_en_curso_EAI__c * conversionRate / million;
            }else{
                survey.Certa_SCP__NAV_o_ingresos_proximos_12_meses__c = 0;
            }
            
            if(op.BI_Duracion_del_contrato_Meses__c != null){
                survey.Certa_SCP__Duracion_del_proyecto__c = op.BI_Duracion_del_contrato_Meses__c;
            }else{
                survey.Certa_SCP__Duracion_del_proyecto__c = 0;
            }
            //=======
            if(op.amount != null) {
            	survey.Certa_SCP__Full_contract_value__c = op.amount * conversionRate / million;
            }
            else {
                survey.Certa_SCP__Full_contract_value__c = 0;
            }
            if(op.BI_Net_annual_value_NAV__c != null) {
                survey.Certa_SCP__NAV_12_meses_naturales__c = op.BI_Net_annual_value_NAV__c * conversionRate / million;
            }
            else {
                survey.Certa_SCP__NAV_12_meses_naturales__c = 0;
            }
            if(op.BI_ingreso_por_unica_vez__c != null) {
            	survey.Certa_SCP__Valor_estimado_one_shot__c = op.BI_ingreso_por_unica_vez__c * conversionRate / million;
            }
            else {
                survey.Certa_SCP__Valor_estimado_one_shot__c = 0;
            }
            if(op.BI_Recurrente_bruto_mensual__c != null) {
            	survey.Certa_SCP__Valor_estimado_ingresos_recurrentes__c = op.BI_Recurrente_bruto_mensual__c * conversionRate / million;
            }
            else {
                survey.Certa_SCP__Valor_estimado_ingresos_recurrentes__c = 0;
            }
            if(op.BI_ARG_CAPEX_total__c != null) {
            	survey.Certa_SCP__Capex_total__c = op.BI_ARG_CAPEX_total__c * conversionRate / million;
            }
            else {
                survey.Certa_SCP__Capex_total__c = 0;
            }
            
            
            List<OpportunityCompetitor> competitors = competitorsMap.get(op.Id);
            
            if(survey.Certa_SCP__Competencia__c == null || survey.Certa_SCP__Competencia__c == ''){
                
                survey.Certa_SCP__Competencia__c = '';
                if(competitors != null) {//if there isn't at least one competitor, the list will not exist
                    for(OpportunityCompetitor comp : competitors) {
                        survey.Certa_SCP__Competencia__c += comp.CompetitorName + '\n';
                    }
                }
            }
            

            
            surveysToUpsert.add(survey);
        }
        
        upsert surveysToUpsert;    
    }
    
    private static void commonBeforeUpsert(List<Opportunity> modified) {
        Map<string, Certa_SCP__Limite_de_Big_deal_por_pais__c> countryLimits = getCountryLimits();
		for(Opportunity op : modified) {
			if(op.Certa_SCP__Manual_Big_Deal__c) {
                op.Certa_SCP__BigDeals__c = true;
            } else {    
				Certa_SCP__Limite_de_Big_deal_por_pais__c lim = countryLimits.get(op.BI_Country__c);
				if(lim != null) {
					op.Certa_SCP__BigDeals__c = shouldBeBigDeal(op, lim);
				}
                else {
                    op.Certa_SCP__BigDeals__c = false;
                }
            } 
		}
    }
    
	private static Map<string, Certa_SCP__Limite_de_Big_deal_por_pais__c> getCountryLimits() {
        if (limite_Big_Deal == null) {
    		limite_Big_Deal = new Map<string, Certa_SCP__Limite_de_Big_deal_por_pais__c>();
    		for(Certa_SCP__Limite_de_Big_deal_por_pais__c lim : [SELECT Name, Certa_SCP__FCV__c, Certa_SCP__CAPEX__c, Certa_SCP__NAV__c FROM Certa_SCP__Limite_de_Big_deal_por_pais__c]) {
    			limite_Big_Deal.put(lim.Name, lim);
    		}	
        }
        return limite_Big_Deal;
	}
	
	private static boolean shouldBeBigDeal(Opportunity op, Certa_SCP__Limite_de_Big_deal_por_pais__c bigDealLimit) {
		final integer million = 1000000;
		decimal conversionRate = GetConversionRate(op.CurrencyIsoCode);
		return (op.Amount != null && op.Amount * conversionRate / million >= bigDealLimit.Certa_SCP__FCV__c) || 
                (op.BI_Net_annual_value_NAV__c != null && op.BI_Net_annual_value_NAV__c * conversionRate / million >= bigDealLimit.Certa_SCP__NAV__c) || 
                (op.BI_ARG_CAPEX_total__c != null && op.BI_ARG_CAPEX_total__c * conversionRate / million >= bigDealLimit.Certa_SCP__CAPEX__c);
	}
    //04/12/2015        Micah Burgos       Commented SOQL query for use a static var from BI_EN.   
    private static decimal GetConversionRate(string isoCode){
        if(conversionRates == null) {
            conversionRates = new map<string, decimal>();
            //List<CurrencyType> cts = [SELECT ConversionRate, IsoCode FROM CurrencyType WHERE isActive = true];
            List<CurrencyType> cts = BI_CurrencyHelper.LST_CURRENCYTYPE;
            system.debug('BI_SCP_BigDeals_Triggers.GetConversionRate.cts: ' + cts);
            integer usdIndex = 0;
            //In case of USD being inactive change here
            while(usdIndex < cts.size() && cts[usdIndex].IsoCode != 'USD') {
                usdIndex++;
            }
            for (CurrencyType ct : cts) {
                conversionRates.put(ct.IsoCode, cts[usdIndex].ConversionRate / ct.ConversionRate);
            }
        }
        return conversionRates.get(isoCode);
    }
    
    private static map<string, decimal> conversionRates;
}