/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-04-29      Daniel ALexander Lopez (DL)     Cloned Trigger      
*************************************************************************************/

public with sharing class BI_COL_Shipping_Opportunity_Trs_ctr {
  
  
    public static String quitaDecimal(Decimal numero){
      String retorno='';
      
      retorno=''+numero;
      if(retorno.contains('.00'))
      retorno=retorno.substring(0, retorno.lastIndexOf('.'));      
      else if(retorno.contains('.'))
      retorno=retorno.replace('.', '');
      
      return retorno;
    }

}