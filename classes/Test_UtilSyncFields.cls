@isTest
private class Test_UtilSyncFields {

    static testMethod void myUnitTest() {
                
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        Certa_SCP__Region__c region = new Certa_SCP__Region__c(Name = 'Argentina');
        insert region;
    
        String region1 =  'Argentina';
        
        String region2 = 'Chile';
        List<Account> accountList = new List<Account>();
        
        for (Integer i=0;i<50;i++) {
            //Test null fields
            Account a1 = new Account();
            a1.Name = 'Test 1' + i;
            a1.BI_Segment__c = 'test'; // 28/09/2017
            a1.BI_Territory__c = 'test'; // 28/09/2017
            a1.BI_Subsegment_Regional__c = 'test'; // 28/09/2017           
            accountList.add(a1);
            
            //Test country known
            Account a2 = new Account();
            a2.Name = 'Test 2' + i;
            a2.BI_Ambito__c = 'Privado';
            a2.BI_Sector__c = 'Finance';
            a2.BI_Subsector__c = 'Banca';
            a2.BI_Segment__c = 'Empresas';
            a2.BI_Territory__c = 'test'; // 28/09/2017
            a2.BI_Subsegment_Regional__c = 'test'; // 28/09/2017  
            a2.BI_Subsegment_Local__c = 'Multinacionales';            
            a2.BI_Country__c = region1;
            accountList.add(a2);
                        
            //Test country unknown
            Account a3 = new Account();
            a3.Name = 'Test 3' + i;
            a3.BI_Ambito__c = 'Privado';
            a3.BI_Sector__c = 'Finance';
            a3.BI_Subsector__c = 'Banca';
            a3.BI_Segment__c = 'Empresas';
            a3.BI_Territory__c = 'test'; // 28/09/2017
            a3.BI_Subsegment_Regional__c = 'test'; // 28/09/2017  
            a3.BI_Subsegment_Local__c = 'Multinacionales';            
            a3.BI_Country__c = region2;
            accountList.add(a3);
        }
        
        insert accountList;
        
        List<Account> accountNullList = [select id, Name, Certa_SCP__Sector__c, industry, Certa_SCP__Micro_Industry__c, Certa_SCP__Region__c, Certa_SCP__Region__r.Name from Account where name like 'Test 1%'];
        system.assert(accountNullList.size()==50);
        
        For (Account a :accountNullList) {
            system.assert(a.Certa_SCP__Sector__c == null && a.industry == null && a.Certa_SCP__Micro_Industry__c == null && a.Certa_SCP__Region__c == null);
        }
        
        List<Account> accountKnowList = [select id, Name, Certa_SCP__Sector__c, industry, Certa_SCP__Micro_Industry__c, Certa_SCP__Region__c, Certa_SCP__Region__r.Name from Account where name like 'Test 2%'];
        system.assert(accountKnowList.size()==50);
        
        For (Account a :accountKnowList) {
            system.assertEquals('Privado',a.Certa_SCP__Sector__c);
            system.assertEquals('Finance', a.industry);
            system.assertEquals('Banca', a.Certa_SCP__Micro_Industry__c);
            system.assertEquals(region.Name, a.Certa_SCP__Region__r.Name);
        }
        
        List<Account> accountUnknowList = [select id, Name, Certa_SCP__Sector__c, industry, Certa_SCP__Micro_Industry__c, Certa_SCP__Region__c, Certa_SCP__Region__r.Name from Account where name like 'Test 3%'];
        system.assert(accountUnknowList.size()==50);
        
        For (Account a :accountUnknowList) {
            system.assertEquals('Privado',a.Certa_SCP__Sector__c);
            system.assertEquals('Finance', a.industry);
            system.assertEquals('Banca', a.Certa_SCP__Micro_Industry__c);
            system.assertEquals(null, a.Certa_SCP__Region__c);
        }

    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}