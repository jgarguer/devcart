public class BI2_COL_RequiredContactSearch_ctr {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Morales Rodríguez, Jose María Martín Castaño
    Company:       Deloitte
    Description:   Clase asociada al Pop Up de búsqueda del Contacto Final de BIIN_CreacionIncidencia

    History:

    <Date>           <Author>                                                   <Description>
    10/12/2015       Ignacio Morales Rodríguez, Jose María Martín               Initial version
    17/06/2016       José Luis González Beltrán                                 Adapt to UNICA
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public String siteName {get;set;}
    public String siteId {get;set;}
    public String query {get;set;}
    public String query2 {get;set;}
    public String query3 {get;set;}
    public String query4 {get;set;}
	public String scriptvar {get;set;}
    public List<Case> lcases {get;set;}
    public Case caso {get;set;}
    public boolean BoolDeshabilitar {get;set;}
    public integer queryLength {get;set;}
    public integer queryLength2 {get;set;}
    public integer queryLength3 {get;set;}
    public boolean MostrarError {get;set;}
    private transient HttpRequest req;
    public HttpRequest getHttpRequest () {
        return req;
    }
		//Nuevos Contactos
	public boolean newContact {get;set;}
	public boolean formNuevoContacto {get;set;}
	public Case casoCrearContact {get;set;}
    private transient HttpResponse res;
    public HttpResponse getHttpResponse () {
        return res;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jose María Martín Castaño
        Company:       Deloitte
        Description:   Constructor, donde se definen las variables globales y se toman los valores heredados de nombre, apellido y contacto de la página padre
                        de creación.
        History:

        <Date>            <Author>                      <Description>
        12/12/2015      Jose María Martín Castaño       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI2_COL_RequiredContactSearch_ctr ()
    {
        newContact=false;
        formNuevoContacto=false;

        if(Test.isRunningTest()) {
            siteName = '';
            siteId   = '';
        }
        MostrarError = false;
        query = ApexPages.currentPage().getParameters().get('namefield');
        query2 = ApexPages.currentPage().getParameters().get('surnamefield');
        query3 = ApexPages.currentPage().getParameters().get('emailfield');
        query4 = ApexPages.currentPage().getParameters().get('accountfield');
        system.debug('QUERY---->' + query3);
        queryLength = query.length();
        queryLength2 = query2.length();
        queryLength3 = query3.length();
        if((queryLength >= 3) || (queryLength2 >= 3) || (queryLength3 >= 3))
        {
            BoolDeshabilitar= false;
        }
        else
        {
            BoolDeshabilitar= true;
        }
        lcases = new List<Case>();
        caso = new Case();
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jose María Martín Castaño
        Company:       Deloitte
        Description:   Función que obtiene la lista de contactos.

        History:

        <Date>            <Author>                          <Description>
        12/12/2015       Jose María Martín Castaño          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void getContact() {
        MostrarError = false;
        newContact=false;
        formNuevoContacto=false;
        BIIN_Obtener_Token_BAO ot=new BIIN_Obtener_Token_BAO();
        BIIN_Obtener_Contacto_WS cfs=new BIIN_Obtener_Contacto_WS();
        String authorizationToken = ''; // ot.obtencionToken();

        BIIN_Obtener_Contacto_WS.ContactSalida salida = new BIIN_Obtener_Contacto_WS.ContactSalida();
        salida = cfs.getContact(authorizationToken, query, query2, query3, query4);
        lcases = new List<Case>(); // limpiamos la tabla
        system.debug('la salida es: '+salida);
        if(salida != null && salida.outputs != null && !salida.outputs.output.isEmpty() && salida.outputs.output[0].code==null)
        {
            if(salida.outputs.output[0].code==null)
            {
                for(Integer i=0;i<salida.outputs.output.size();i++)
                {
                    caso.BI2_Apellido_Contacto_Final__c = salida.outputs.output[i].lastName;
                    caso.BI2_Nombre_Contacto_Final__c = salida.outputs.output[i].firstName;
                    caso.BI2_Email_Contacto_Final__c = salida.outputs.output[i].internetEmail;
                    caso.BI_ECU_Telefono_fijo__c = salida.outputs.output[i].phone;
                    caso.BI_ECU_Telefono_movil__c = salida.outputs.output[i].mobile;

                    lcases.add(caso);
                    caso = new Case();
                }
            }
            else
            {
                MostrarError = true;
								newContact=true;
                caso.BI2_Apellido_Contacto_Final__c = salida.outputs.output[0].message;
            }
        }
        else
        {
					newContact=true;
					MostrarError = true;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ignacio Morales Rodríguez
        Company:       Deloitte
        Description:   Función que cuenta el número de caracteres de los campos de búsqueda para habilitar el botón Ir.

        History:

        <Date>            <Author>                          <Description>
        12/12/2015       Ignacio Morales Rodríguez          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
     public void HabilitarIr()
     {
        queryLength = query.length();
        queryLength2 = query2.length();
        queryLength3 = query3.length();
        if((queryLength >= 3) || (queryLength2 >= 3) || (queryLength3 >= 3) )
        {
            BoolDeshabilitar= false;
        }
        else
        {
            BoolDeshabilitar= true;
        }
        system.debug('HA ENTRADO EN HabilitarIr---->' + BoolDeshabilitar + queryLength );
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ignacio Morales Rodríguez
        Company:       Deloitte
        Description:   Función que cuenta el número de caracteres de los campos de búsqueda para habilitar el botón Ir.

        History:

        <Date>            <Author>                          <Description>
        12/12/2015       Ignacio Morales Rodríguez          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public void mostrarNuevoContacto()
	{
        caso.BI2_Nombre_Contacto_Final__c   = query;
        caso.BI2_Apellido_Contacto_Final__c = query2;
        caso.BI2_Email_Contacto_Final__c    = query3;
        caso.BI_ECU_Telefono_fijo__c        = null;
        caso.BI_ECU_Telefono_movil__c       = null;
		formNuevoContacto=true;
		newContact=false;
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ignacio Morales Rodríguez
        Company:       Deloitte
        Description:   Función que cuenta el número de caracteres de los campos de búsqueda para habilitar el botón Ir.

        History:

        <Date>            <Author>                          <Description>
        12/12/2015       Ignacio Morales Rodríguez          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference  cancelarNuevoContacto() {
        formNuevoContacto=false;
        newContact=false;
        return null;
    }

    public void crearContancoRoD() {
        if(validateEmail(caso.BI2_Email_Contacto_Final__c)) {
            BIIN_Obtener_Contacto_WS cfs = new BIIN_Obtener_Contacto_WS();
    		String strResponse = cfs.crearContacto(caso.BI2_Nombre_Contacto_Final__c, caso.BI2_Apellido_Contacto_Final__c, caso.BI2_Email_Contacto_Final__c, caso.BI_ECU_Telefono_fijo__c, caso.BI_ECU_Telefono_movil__c, query4);
    		
            if(strResponse == null) {
                scriptvar = '<script>ClickBloquearForm();</script>';
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, strResponse));
            }
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Email no válido'));
        }
    }

    //validar Email
    public Boolean validateEmail(String email) {
        Boolean res = true;
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
        if (!MyMatcher.matches())
            res = false;
        return res;
    }
}