public class TGS_BillingProfile_Handler {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta García
    Company:       Deloitte
    Description:   Fill field LastModifiedFD for Account LE and BU
    
    History

    <Date>            <Author>                      <Description>
    28/10/2015        Marta García                  Initial version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  public static void fillLastModifiedFD (set<id> newBP){
    
        List<Account> listAccount = new List<Account>();
       
        Account accountBU;
        Account accountLE;
        
        for(NE__Billing_Profile__c BillingProfile : [SELECT iD, TGS_Billing_Type__c, NE__Account__c FROM NE__Billing_Profile__c WHERE ID IN :newBP AND TGS_Billing_Type__c=:'Distributed']){
            
            if (BillingProfile.NE__Account__c!=null){           
                accountLE = new Account(Id = BillingProfile.NE__Account__c);
                accountLE.TGS_Last_Modified_FD__c = Date.today(); 
                listaccount.add(accountLE);
            }
        }
        update listaccount;
     
    }
}