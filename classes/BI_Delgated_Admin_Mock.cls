public  with sharing class BI_Delgated_Admin_Mock {
	public static DOM.Document createMockSession() {
		DOM.Document doc = new DOM.Document();
		Dom.XMLNode Envelope = doc.createRootElement('Envelope','urn:enterprise.soap.sforce.com','soapenv');
		Dom.XMLNode Body = Envelope.addChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/','soapenv');
		Dom.XMLNode loginResponse = Body.addChildElement('loginResponse','urn:enterprise.soap.sforce.com',null);
		Dom.XMLNode result = loginResponse.addChildElement('result','urn:enterprise.soap.sforce.com',null);
		Dom.XMLNode sessionId = result.addChildElement('sessionId','urn:enterprise.soap.sforce.com',null);
		sessionId.addTextNode('sessionId78541228711588841544a');
		return doc;
	}
	public static DOM.Document createMockInsert() {
		DOM.Document doc = new DOM.Document();
		Dom.XMLNode Envelope = doc.createRootElement('Envelope','urn:enterprise.soap.sforce.com','soapenv');
		Dom.XMLNode Body = Envelope.addChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/','soapenv');
		Dom.XMLNode loginResponse = Body.addChildElement('createResponse','urn:enterprise.soap.sforce.com',null);
		Dom.XMLNode result = loginResponse.addChildElement('result','urn:enterprise.soap.sforce.com',null);
		Dom.XMLNode sessionId = result.addChildElement('success','urn:enterprise.soap.sforce.com',null);
		sessionId.addTextNode('true');
		return doc;
	}
	public static DOM.Document createMockUpdate() {
		DOM.Document doc = new DOM.Document();
		Dom.XMLNode Envelope = doc.createRootElement('Envelope','urn:enterprise.soap.sforce.com','soapenv');
		Dom.XMLNode Body = Envelope.addChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/','soapenv');
		Dom.XMLNode loginResponse = Body.addChildElement('updateResponse','urn:enterprise.soap.sforce.com',null);
		Dom.XMLNode result = loginResponse.addChildElement('result','urn:enterprise.soap.sforce.com',null);
		Dom.XMLNode sessionId = result.addChildElement('success','urn:enterprise.soap.sforce.com',null);
		sessionId.addTextNode('true');
		return doc;
	}
}