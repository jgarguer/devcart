@isTest
private class CWP_AC_QuoteHistoryCTRL_TEST {
	
    @testSetup
    static void testSetup(){
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Spain'});
        Opportunity opp = new Opportunity(Name = 'TestquoteHistoryOPp',
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                                          AccountId = lst_acc[0].Id,
                                          BI_Ciclo_ventas__c = Label.BI_Completo,
                                          BI_Licitacion__c = 'No',
                                          BI_Country__c = 'Spain');
        Insert opp;
        
        NE__Order__c order = new NE__Order__c(NE__OrderStatus__c = 'Pending',
                                              NE__AccountId__c = lst_acc[0].Id,
                                              NE__OptyId__c = opp.Id,
                                              RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Quote' LIMIT 1].Id
                                             );
        Insert order;
    }
    @isTest
    static void getQuotes_test(){
        List<Account> lst_acc = [SELECT Id FROM Account LIMIT 1];
        Map<String, List<Object>> map_return =  CWP_AC_QuoteHistory_Ctrl.getQuotes(UserInfo.getUserId(), lst_acc[0].Id, 10);
        System.assert(map_return!=null);
    }
    @isTest
    static void getQuoteName_test(){
        String OrName = CWP_AC_QuoteHistory_Ctrl.getQuote([SELECT Id FROM NE__Order__c LIMIT 1].Id);
        System.assert(OrName != null);
    }
}