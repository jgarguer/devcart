public without sharing class CWP_ExportTicketsOrders_ctrl{
    
    
    
    public String headerPhoto{get;set;}
    //public List<Case> listToShow {get;set;}
    public Boolean areTickets{get;set;}
    public String fileName{get;set;}
    public static String closedDate{get;set;}/*Álvaro López 09/04/2018 - Added ClosedDate*/
    public List<List<Case>> listOfListToShow{get;set;}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    *
    * Author:        Belén Coronado
    *
    * Company:       everis
    *
    * Description:   Visualforce Page Constructor 
    * History:
    * <Date>                          <Author>                                <Code>              <Change Description>
    * 23/05/2017                      Belén Coronado                                              Initial version
    */
    public CWP_ExportTicketsOrders_ctrl(){
        //INI HEADER
        List<Document> lstDocument = [Select Id,Name,LastModifiedById from Document where Name = 'CWP_TelfonicaLogo' limit 1];
        string strOrgId = UserInfo.getOrganizationId();
        string orgInst = URL.getCurrentRequestUrl().toExternalForm();
        orgInst = orgInst.substring(0, orgInst.indexOf('.')) + '.content.force.com';
        if(!lstDocument.isEmpty()){
            headerPhoto= URL.getSalesforceBaseUrl().getProtocol() + '://c.' + orgInst + '/servlet/servlet.ImageServer?id=' + lstDocument[0].Id + '&oid=' + strOrgId;
            headerPhoto= '/servlet/servlet.ImageServer?id=' + lstDocument[0].Id + '&oid=' + strOrgId;
            system.debug('url header: '+headerPhoto);
        }
        //FIN HEADER
        
        areTickets = (Boolean)Cache.Session.get('local.CWPexcelExport.areTickets');
        fileName = 'Service Tracking_';
        if(areTickets){
            fileName += 'Tickets_' + String.valueOf(Date.Today());
        }else{
            fileName += 'Orders_' + String.valueOf(Date.Today());
        }
        
        //List<Case> listToShow = (List<Case>)Cache.Session.get('local.CWPexcelExport.cachedListaCasosToExport');
        String query = (String)Cache.Session.get('local.CWPexcelExport.queryToExport');
        system.debug('queryToExport: ' + query);
        List<Case> listToShow = Database.query(query);
        listToShow = getKeyValueField(listToShow, areTickets);
        listOfListToShow = new List<List<Case>>();
        Integer listMaxSize = 1000;
        if(Test.isRunningTest()){
            listMaxSize = 1;
        }
        if(listToShow.size() > listMaxSize){
            Integer numIterations = (Integer)(listToShow.size()/listMaxSize) + 1;
            Integer index = 0;
            for(Integer i=0; i<numIterations; i++){
                Integer startIndex;
                Integer endIndex;
                if(index>=0 && index<listToShow.size()){
                    startIndex=index;
                    endIndex=index + listMaxSize;
                }
                system.debug('startIndex: ' + index);
                List<Case> auxList = new List<Case>();
                for(Integer j=startIndex; j<endIndex; j++){
                    if(j<listToShow.size()){
                        auxList.add(listToShow.get(j));
                    }
                }
                system.debug('auxList.size(): ' + auxList.size());
                listOfListToShow.add(auxList);
                index = endIndex;
            }           
        }else{
            listOfListToShow.add(listToShow);
        }
        system.debug('lista de listas size: ' + listOfListToShow.size());
        for(List<Case> i:listOfListToShow){
            system.debug('cada lista tiene: ' + i.size() + ' elementos');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    *
    * Author:        Belén Coronado
    *
    * Company:       everis
    *
    * Description:   Method to get keyValue field and format Open Date field
    * History:
    * <Date>                          <Author>                                <Code>              <Change Description>
    * 09/05/2017                      Belén Coronado                                              Initial version
    */
    private static List<Case> getKeyValueField(List<Case> cases, Boolean areTickets){
        if(!areTickets){
            Map<Id, Id> orAndOiIdMap = new Map<Id, Id>();
            Set<Id> orIds = new Set<Id>();
            for(Case i:cases){
                i.CWP_textDate__c = i.CreatedDate.format('YYYY-MM-dd');
                /*Álvaro López 09/04/2018 - Added ClosedDate*/
                if(i.ClosedDate != null){
                    i.CWP_textCloseDate__c = i.ClosedDate.format('YYYY-MM-dd');
                }
                if(i.Order__c != null){
                    orIds.add(i.Order__c);
                }
            }
            Set<Id> oiSet = new Set<Id>();
            for(NE__OrderItem__c i:[SELECT Id, NE__OrderId__c FROM NE__OrderItem__c WHERE NE__OrderId__c IN: orIds AND NE__CatalogItem__r.NE__Type__c = 'Product']){
                oiSet.add(i.Id);
                if(!orAndOiIdMap.containskey(i.NE__OrderId__c)){
                    orAndOiIdMap.put(i.NE__OrderId__c, i.Id);
                }
            }
            
            List<NE__Order_Item_Attribute__c> oItAtt = [SELECT Id, NE__Order_Item__c, Name, NE__Value__c FROM NE__Order_Item_Attribute__c 
                WHERE NE__Order_Item__c IN: oiSet AND NE__FamPropId__r.CWP_KeyValue__c=true AND NE__Value__c != null ORDER BY NE__Value__c ];
            Map<Id, NE__Order_Item_Attribute__c> oiOiAttMap = new Map<Id, NE__Order_Item_Attribute__c>();
            for(NE__Order_Item_Attribute__c att:oItAtt){
                oiOiAttMap.put(att.NE__Order_Item__c, att);
            }
            for(Case c:cases){
                if(c.Order__c != null && orAndOiIdMap.get(c.Order__c) != null && oiOiAttMap.get(orAndOiIdMap.get(c.Order__c)) != null){
                    c.CWP_KeyValue__c = oiOiAttMap.get(orAndOiIdMap.get(c.Order__c)).NE__Value__c;
                }
            }
        }else{
            Set<Id> oiSetId = new Set<Id>();
            for(Case i:cases){
                i.CWP_textDate__c = i.CreatedDate.format('YYYY-MM-dd');
                /*Álvaro López 16/04/2018 - Added ClosedDate*/
                System.debug('CLOSEDDATE: ' + i.ClosedDate);
                if(i.ClosedDate != null){
                    i.CWP_textCloseDate__c = i.ClosedDate.format('YYYY-MM-dd');
                    System.debug('CLOSEDDATE format: ' + i.CWP_textCloseDate__c);
                }
                if(i.TGS_Customer_Services__c != null){
                    oiSetId.add(i.TGS_Customer_Services__c);
                }
            }
            List<NE__Order_Item_Attribute__c> oItAtt = [SELECT Id, NE__Order_Item__c, Name, NE__Value__c FROM NE__Order_Item_Attribute__c 
                WHERE NE__Order_Item__c IN: oiSetId AND NE__FamPropId__r.CWP_KeyValue__c=true AND NE__Value__c != null ORDER BY NE__Value__c ];
            
            Map<Id, NE__Order_Item_Attribute__c> oiOiAttMap = new Map<Id, NE__Order_Item_Attribute__c>();
            for(NE__Order_Item_Attribute__c att:oItAtt){
                oiOiAttMap.put(att.NE__Order_Item__c, att);
            }
            
            for(Case c:cases){
                if(c.TGS_Customer_Services__c != null && oiOiAttMap.get(c.TGS_Customer_Services__c) != null){
                    c.CWP_KeyValue__c = oiOiAttMap.get(c.TGS_Customer_Services__c).NE__Value__c;
                }
            }
        }
        
        
        return cases;
    }
}