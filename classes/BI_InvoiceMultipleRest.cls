@RestResource(urlMapping='/accountresources/v1/invoices')
global class BI_InvoiceMultipleRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Invoices Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    21/08/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains information stored in the server for invoices.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.InvoicesListType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    21/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.InvoicesListType getMultipleInvoices() {
		
		BI_RestWrapper.InvoicesListType response;
		
		try{
			
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
		
			if(RestContext.request.params.get('accountId') != null || RestContext.request.params.get('country') != null || 
			   RestContext.request.params.get('status') != null || RestContext.request.params.get('endCreationDate') != null || 
			   RestContext.request.params.get('startCreationDate') != null || RestContext.request.params.get('limit') != null || 
			   RestContext.request.params.get('offset') != null)
			{
				
				if((RestContext.request.params.get('limit') == null || 
				   (RestContext.request.params.get('limit') != null && 
				    RestContext.request.params.get('limit').isNumeric() && 
				    Integer.valueOf(RestContext.request.params.get('limit')) < 50000 && 
				    Integer.valueOf(RestContext.request.params.get('limit')) > 0
				   )
				  ) 
				  
				  &&
				  
				  (RestContext.request.params.get('offset') == null || 
				   (RestContext.request.params.get('offset') != null && 
				    RestContext.request.params.get('offset').isNumeric() && 
				    Integer.valueOf(RestContext.request.params.get('offset')) < 2001 && 
				    Integer.valueOf(RestContext.request.params.get('offset')) > 0
				   )
				  ))
				{
					
					Datetime sdate;
					Datetime edate;
					
					if(RestContext.request.params.get('startCreationDate') != null)
						sdate = BI_RestHelper.stringToDateTime(RestContext.request.params.get('startCreationDate'));
						
					if(RestContext.request.params.get('endCreationDate') != null)
						edate = BI_RestHelper.stringToDateTime(RestContext.request.params.get('endCreationDate'));
						
					if((RestContext.request.params.get('startCreationDate') != null && sdate == null) ||
					   (RestContext.request.params.get('endCreationDate') != null && edate == null))
					{
						
						RestContext.response.statuscode = 400;//BAD_REQUEST
						RestContext.response.headers.put('errorMessage', Label.BI_InvalidDate);
						
					}else{
						
						if(RestContext.request.headers.get('countryISO') == null){
								
							RestContext.response.statuscode = 400;//BAD_REQUEST
							RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
							
						}else{
						
							//MULTIPLE OPPORTUNITIES
							response = BI_RestHelper.getMultipleInvoices(RestContext.request.params.get('accountId'), RestContext.request.params.get('country'), 
																	 	 RestContext.request.params.get('status'), RestContext.request.params.get('endCreationDate'), 
																	 	 RestContext.request.params.get('startCreationDate'), RestContext.request.params.get('limit'),
																	 	 RestContext.request.params.get('offset'), RestContext.request.headers.get('countryISO'),
																	 	 RestContext.request.headers.get('systemName'));
																	 	 
							RestContext.response.statuscode = (response == null)?404:200;//404 NOT_FOUND, 200 OK
								
						}
						
					}
					
				}else{
					
					RestContext.response.statuscode = 400;//BAD_REQUEST
					RestContext.response.headers.put('errorMessage', Label.BI_IncorrectLimitOffset);
					
				}
				
			}else{
				
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingRequiredParameters);
				
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_InvoiceMultipleRest.getMultipleInvoices', 'BI_EN', exc, 'Web Service');
			
		}
		
		return response;
		
	}
	
}