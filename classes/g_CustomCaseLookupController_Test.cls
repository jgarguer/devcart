@isTest

private class g_CustomCaseLookupController_Test {


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       NEAborda
	    Description:   Test Method that manage the code coverage from g_CustomCaseLookupController
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    08/07/2016                      Guillermo Muñoz             Overwrite of the initial version of Genesys
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void searcha_Test() {

		BI_TestUtils.throw_exception = false;
        
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);

        //Insert Account and Contact
		List <Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1, new List <String> {Label.BI_Peru});
		List <Contact> lst_con = BI_DataLoad.loadContacts(1, lst_acc);
		
		//Insert Case
		Case cas = new Case(
            RecordTypeId = TGS_RecordTypes_Util.getRecordTypeId(Case.sObjectType, 'BI2_caso_comercial'),
            Reason = 'Test',
            BI2_PER_Tipologia__c = 'Test',
            BI_Line_of_Business__c = 'Test',
            TGS_Disputed_number_of_lines__c = 120,
            BI2_PER_Territorio_caso__c = 'Test',
            BI_Product_Service__c = ' Test',
            BI2_per_subtipologia__c = 'Test',     
            AccountId = lst_acc[0].Id,
            ContactId = lst_con[0].Id,
            BI_Country__c = Label.BI_Peru,
            Subject = 'test_subject1'
        );
        insert cas;
		
		Case myCase1 = [SELECT Id, CaseNumber FROM Case WHERE Subject = 'test_subject1'];

		Test.startTest();
		g_CustomCaseLookupController lookup = new g_CustomCaseLookupController();
		lookup.searchString = myCase1.CaseNumber ;
		lookup.Subject = 'test';    
		
		PageReference pr = lookup.search();
		lookup.getFormTag();
		lookup.getTextBox();
		
		System.debug('*** results = ' + lookup.case_results.size());
		System.assert(lookup.case_results.size() != 0);
		Test.stopTest();
	}


	//GMN 08/07/2016 - Deprecated for errors in the validation rules
	/*static testMethod void searcha_Test() {
		//Create Account test data
		g_CustomCaseLookupController lookup = new g_CustomCaseLookupController();
		//Account newACC = new Account(Name = 'testAccount', Phone = '5555');
		BI_Dataload.loadAccountsPaisStringRef(2, new List <String> {Label.BI_Peru});
		//Account acc = [SELECT Id, Name, Phone FROM Account WHERE Phone = '5555']; 
		//Create Contact test data
		Contact newCon = new Contact(FirstName = 'John', LastName = 'Doer', Phone = '4444', MobilePhone = '3333', Email = 'john.doer@somewhere.com');
		newCon.AccountId = acc.Id;  //account id 
		insert newCon; 
		Contact con = [SELECT Id FROM Contact WHERE Phone = '4444'];  
		//Create Case test data
		Case test_case = new Case(Subject = 'test_subject1', AccountID = acc.Id, ContactId = con.Id);
		insert test_case;
		test_case = new Case(Subject = 'test_subject2', AccountID = acc.Id, ContactId = con.Id);
		insert test_case;
		
		Case myCase1 = [SELECT Id, CaseNumber FROM Case WHERE Subject = 'test_subject1'];
		Case myCase2 = [SELECT Id FROM Case WHERE Subject = 'test_subject2'];
				
		lookup.searchString = myCase1.CaseNumber ;
		lookup.Subject = 'test';    
		
		PageReference pr = lookup.search();
		
		System.debug('*** results = ' + lookup.case_results.size());
		System.assert(lookup.case_results.size() != 0);
	} */
}