global with sharing class OrderSummaryCancelButton {
	
	webservice static String cancelButton(String ordId) {
		
		String result;
		RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'NE__Order__c' AND Name = 'Order' limit 1];
		NE__Order__c ord = [SELECT RecordTypeId, (SELECT Id FROM NE__Order_Items__r WHERE NE__Status__c != 'No procesado' AND NE__Status__c != 'Pending'), NE__OptyId__c, NE__OrderStatus__c FROM NE__Order__c WHERE Id =: ordId];
		
		if(ord.RecordTypeId == rt.Id && ord.NE__Order_Items__r.size() == 0) {
			Opportunity opp = [SELECT Id, StageName FROM Opportunity WHERE Id =: ord.NE__OptyId__c];
			opp.StageName = 'F1 - Cancelled | Suspended';
			ord.NE__OrderStatus__c = 'Cancelled';
			
			NE__Order__c ordToUpd = [SELECT RecordTypeId, (SELECT Id,NE__Status__c FROM NE__Order_Items__r), NE__OptyId__c, NE__OrderStatus__c FROM NE__Order__c WHERE Id =: ordId];
			for(NE__OrderItem__c oiIt:ordToUpd.NE__Order_Items__r)
				oiIt.NE__Status__c = 'No procesado'; 
				
			update ordToUpd.NE__Order_Items__r;
			
			update opp;
			update ord;
		} else {
			result = 'Not possible';
		}
		
		return result;
	}

}