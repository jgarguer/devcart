/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       12-May-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
public with sharing class BI_COL_CreateCheckDigitNIT_cls 
{
    public static void fnValidateDigit( List<Account> lstCuenta ) 
    {
        for( Account oCuenta : lstCuenta )
        {
            if( oCuenta.BI_COL_TipoIdentificador__c == 'NIT' )
            {
                oCuenta.BI_COL_Digito_verificacion__c = BI_COL_CreateCheckDigitNIT_cls.CalculardigitoVerificaion( oCuenta.BI_No_Identificador_fiscal__c );
            }
        }
        system.debug('\n\n##lstCuenta: '+ lstCuenta +'\n\n');
    }

    public static String CalculardigitoVerificaion(String Numero)
    {
        List<Integer> cof = new List<Integer>();
           try {
               
               cof.add(3);
            cof.add(7);
            cof.add(13);
            cof.add(17);
            cof.add(19);
            cof.add(23);
            cof.add(29);
            cof.add(37);
            cof.add(41);
            cof.add(43);
            cof.add(47);
            cof.add(53);
            cof.add(59);
            cof.add(67);
            cof.add(71);    
               
           }catch(Exception e)
           {
              System.debug('Error al ingresar una caracter especial');
              return '-1';
           }
            
            Integer  residuo;
            Integer  sumatoria = 0;
            Integer  cnt = 0;
            Integer  dv = 0;
            Integer  j=0;
    
               // # Convierte número en string y lo itera de atras hacia adelante
               for(Integer i=8 ; i>=0;i--)
            {
               try{
                    if(!Test.isRunningTest())
                    {
                      sumatoria += Integer.valueof(Numero.subString(i,i+1)) * cof[j];
                      j++;                      
                    }
                    
               }catch(Exception e)
               {
                   System.debug('Error en caracter especial '+e.getMessage());
               }
               
            }

             system.debug('sumatoria -->'+sumatoria );
               residuo = math.mod(sumatoria ,11);
               if(residuo >1)
               {
                 dv = 11-residuo ;
               }else{
                 dv = residuo ;
               }

        return ''+dv;
    }
}