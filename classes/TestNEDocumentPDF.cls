@isTest(SeeAllData=true)

private class TestNEDocumentPDF {
     
     static testMethod void myUnitTest() {
	// JLA_PickList_Refactorizacion_Necesaria
	// 21/12/2017 Jesus Arcones - Add BI_TestUtils.throw_exception and BI_MigrationHelper.skipAllTriggers to avoid limits DML and SOQL
	BI_TestUtils.throw_exception = false;
            User us    =    [SELECT id FROM User WHERE IsActive = true AND (Profile.name = 'Administrador del sistema' OR Profile.name = 'System Administrator') LIMIT 1];
            System.runAs(us){
                NE__Document_Component_Header__c dch = new NE__Document_Component_Header__c (NE__Name__c='RIC_PRP-Cotizaciontest_RIC');
                insert dch;
               
                NE__Document_Component_Header__c dchh = new NE__Document_Component_Header__c (NE__Name__c='header');
                insert dchh;
                NE__Document_Component_Header__c dchf = new NE__Document_Component_Header__c (NE__Name__c='footer');
                insert dchf;
                   
                NE__Document_Component__c dchrr = new NE__Document_Component__c (NE__Published__c = '{!test}{!#RIC_TESTTTT_RIC#}',NE__workspace__c='ssssaaa',NE__Document_Component_Header__c=dchh.Id);                                                      
                insert dchrr;   
             
                NE__Document_Component__c dchrr2 = new NE__Document_Component__c (NE__Published__c = '{!test}{!#RIC_TESTTTT_RIC#}',NE__workspace__c='ssssaaa',NE__Document_Component_Header__c=dchf.Id);                                                      
                insert dchrr2;     
                
                NE__Document_Component__c dc = new NE__Document_Component__c (NE__Published__c = '{!test}{!#RIC_TESTTTT_RIC#}{!#RIC_PRP-Cotizaciontest_RIC#}',NE__workspace__c='ssssaaa',NE__Document_Component_Header__c=dch.Id,NE__Header_Document__c=dchh.Id,NE__Footer_Document__c=dchh.Id);                                                      
                insert dc;     
               
		BI_MigrationHelper.skipAllTriggers();
                List<String> lst_pais = new List<String>{'Guatemala'};
                List<Account> accountobj = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
                List<Opportunity> opp = BI_DataLoad.loadOpportunities(accountobj[0].Id);
                opp[0].BI_Duracion_del_contrato_Meses__c = 36;
                update opp[0];
                
                BI_MigrationHelper.cleanSkippedTriggers();
		Test.startTest();

                NE__Order__c oldord = new NE__Order__c();
                oldord.NE__OptyId__c = opp[0].Id;
                oldord.NE__OpportunityId__c = opp[0].Id;
                oldord.NE__OrderStatus__c = 'Active';
                insert oldord;  
             
                NE__OrderItem__c ordIt              =   new NE__OrderItem__c();
                ordIt.NE__OrderId__c                =   oldord.id;
                ordIt.NE__RecurringChargeCode__c    =   'test';
                ordIt.NE__RecurringChargeOv__c      =   10;
                ordIt.NE__OneTimeFeeOv__c           =   10;
                ordIt.NE__Qty__c                    =   1;
                insert ordIt;   
                
                NE__Order_Item_Attribute__c oiaT = new NE__Order_Item_Attribute__c (NE__Order_Item__c = ordIt.id,NE__Value__c='test', Name='test');
                insert oiaT;
                
                List<NE__Document_Component__c> dclist = new  List<NE__Document_Component__c>();
                dclist.add(dc);
                Apexpages.Currentpage().getParameters().put('Id',dc.Id);
                Apexpages.Currentpage().getParameters().put('parentId',accountobj[0].Id);
                Apexpages.Currentpage().getParameters().put('mapName','TESTMAP');
                Apexpages.Currentpage().getParameters().put('DOC','true');
                NEDocumentPDF dcPDF0;
                try{
                    dcPDF0 = new NEDocumentPDF();
                }
                catch(Exception e){}
                
                try{
                    dcPDF0.docCompon=dclist;
                    dcPDF0.generateXMLNEW('RIC_PRP-Cotizaciontest_RIC','TESTMAP',accountobj[0].Id,null,null);
                }
                catch(Exception e){}
                try{
                    dcPDF0.substituteComponent('{!#RIC_PRP-Cotizaciontest_RIC#}');
                    dcPDF0.substituteComponent('$$$$$');
                }
                catch(Exception e){}
                
                dcPDF0.isNull('');
                try{
                    dcPDF0.getExternalMapVariablenew1('<ListOfVariable > <Variable > <AccountName >A J INGENIEROS S A</AccountName> <Variable0 ><Nome >prod2</Nome><Variable1 ><Nomenew >prod2</Nomenew> <Variable2 ><Nomenewl >prod2</Nomenewl> </Variable2></Variable1> </Variable0></Variable> </ListOfVariable>');
                }
                catch(Exception e){}
                Map<String,Object> placeholdersnew = new Map<String,Object>();
                try{
                    placeholdersnew=dcPDF0.getExternalMapVariablenew('<ListOfVariable > <Variable > <AccountName >A J INGENIEROS S A</AccountName> <Variable0 ><Nome >prod2</Nome><Variable1 ><Nomenew >prod2</Nomenew> <Variable2 ><Nomenewl >prod2</Nomenewl> </Variable2></Variable1> </Variable0></Variable> </ListOfVariable>');
                }
                catch(Exception e){}
                try{
                    dcPDF0.substitutePlaceholder('{!Today2}{!Today}{!Nome}{!Nomenew}{!Nomenewl}{!AccountName}',placeholdersnew);
                }
                catch(Exception e){}
                
                 try{
                    placeholdersnew=dcPDF0.getExternalMapVariablenew('<ListOfVariable > <Variable > <AccountName >A J INGENIEROS S A</AccountName> <Variable0 ><Nome >0.00</Nome><Variable1 ><Nomenew >prod2</Nomenew> <Variable2 ><Nomenewl >prod2</Nomenewl> </Variable2></Variable1> </Variable0></Variable> </ListOfVariable>');
                }
                catch(Exception e){}
                try{
                    dcPDF0.substitutePlaceholder('{!Today2}{!Today}{!Nome}{!Nomenew}{!Nomenewl}{!AccountName}',placeholdersnew);
                }
                catch(Exception e){}
                
                try{
                    dcPDF0.previewnew(null,null,null,null,null,null);
                }
                catch(Exception e){}
                
                try{
                    dcPDF0.substituteComponentric(null);
                }
                catch(Exception e){}
                
                try{
                    dcPDF0.substituteComponent(null);
                }
                catch(Exception e){}

                Apexpages.Currentpage().getParameters().put('Id',oldord.Id);
                Apexpages.Currentpage().getParameters().put('mapName',null);
                
                try{
                    NEDocumentPDF dcPDF = new NEDocumentPDF();
                }
                catch(Exception e){}
		Test.stopTest();
            }
     }
     
}