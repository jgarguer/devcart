/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis 
    Company:       Everis
    Description:   Controller of the CWP_MyServices component located in "My Service / Services" portal. 
    Test Class:    CWP_myServicesController_TEST.cls
    
    History:
     
    <Date>                  <Author>                <Change Description>
                             Everis                   Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

public without sharing class CWP_myServiceController {
    private static User currentUser;
    @AuraEnabled
    public static map<string, boolean> getButtonVisibility(){
        system.debug('llamada a la funcion que saca los permisos por cuenta');
        
        currentUser = CWP_LHelper.getCurrentUser(currentUser);
        
        system.debug('id del contacto   ' + currentUser.ContactId);
        list<BI_Contact_Customer_Portal__c> contactPermission = [Select
                                                                 id,
                                                                 BI_Perfil__c, 
                                                                 BI_Cliente__c, 
                                                                 BI_Permisos_PP__c,  
                                                                 BI_Permisos_PP__r.BI_MarketPlace__c,  
                                                                 BI_Permisos_PP__r.BI_Facturacion_y_cobranzas__c,  
                                                                 BI_Permisos_PP__r.BI_Servicios_Instalados__c,
                                                                 BI_Permisos_PP__r.BI_Documentacion__c,  
                                                                 BI_Permisos_PP__r.BI_Pestana_Cobranzas__c,  
                                                                 BI_Contacto__c, 
                                                                 BI_User__c, 
                                                                 BI_Activo__c from BI_Contact_Customer_Portal__c where BI_User__c = : currentUser.Id and BI_Cliente__c = :currentUser.Contact.BI_Cuenta_activa_en_portal__c limit 1];
        system.debug('permisos del contacto ' + contactPermission);
        map<string, boolean> retMap = new map<string, boolean>{
            'showService' => false,
            'showDocumentation' => false,
            'showBilling' => false,
            'showMarket' => false
        };
        if(!contactPermission.isEmpty()){
            retMap = new map<string, boolean>{
                'showService' => contactPermission[0].BI_Permisos_PP__r.BI_Servicios_Instalados__c,
                'showDocumentation' => contactPermission[0].BI_Permisos_PP__r.BI_Documentacion__c,
                'showBilling' => contactPermission[0].BI_Permisos_PP__r.BI_Facturacion_y_cobranzas__c,
                'showMarket' => contactPermission[0].BI_Permisos_PP__r.BI_MarketPlace__c
            };
        } 
        system.debug('mapa de vuelta    ' + retMap);
        return retMap;
    }
    
    @AuraEnabled
    public static map<string, string> getMainImage(){
        currentUser = CWP_LHelper.getCurrentUser(null);
        System.debug('@@$'+currentUser);
        map<string, string> retMap = new map<string, string>{
            'title' => label.PCA_My_Service_menu,
            'informationMenu' => label.PCA_MyServiceText,
            'imgId' => null
        };
        Account currentAccount = CWP_LHelper.getCurrentAccount(currentUser.Contact.BI_Cuenta_activa_en_portal__c);
        system.debug('llamada a la funcion que recoge la imagen del fondo  ' + currentUser);
        map<string, string> rcvMap = CWP_LHelper.getPortalInfo('My Service', currentUser, currentAccount);
        if(!rcvMap.isEmpty()){
            retMap.clear();
            retMap.putAll(rcvMap);
        }
        
        system.debug('mapa de datos que van de vuelta' + retMap);
        return retMap;
    }
    
    @AuraEnabled
    public static Boolean getUser(){
        Boolean esTGS=false; 
        Id pId = UserInfo.getProfileId();
        Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
        if (p.size()>0){
            String pName = p[0].Name;
            esTGS= pName.startsWith('TGS');
        }
        return esTGS;
    }
    
     @auraEnabled
    public static map<string,string> getPermissionSet(){
        map<string,string> mapVisibility = CWP_ProfileHelper.getPermissionSet();
        
        List<User> listPortalUser = [Select id, Pais__c, ContactId, ProfileId, Profile.Name from User where id=:UserInfo.getUserId() limit 1];
        User portalUser = new User(); 
        if(listPortalUser!= null && listPortalUser.size()>0) {
            portalUser = listPortalUser.get(0); 
        }
    
        
        User UserActual = [Select Name, ContactId,Contact.BI_Cuenta_activa_en_portal__c, AccountId FROM User where Id =:UserInfo.getUserId()];
        
        Account SegAccount = [Select Id, Name,BI_Country__c, BI_Segment__c, CWP_OneDriveAddress__c FROM Account Where Id=:UserActual.Contact.BI_Cuenta_activa_en_portal__c];
          /*---------------------
        Start Redirect Documentation Change
        -----------------------*/
        system.debug('link a one drive: ' + SegAccount.CWP_OneDriveAddress__c);
        String oneDriveLink;
        if(portalUser.Profile.Name.containsIgnoreCase('TGS') && mapVisibility.get('pca_documentmanagement') != null && mapVisibility.get('pca_documentmanagement') == 'true'){
            if(SegAccount.CWP_OneDriveAddress__c != null){
                oneDriveLink = SegAccount.CWP_OneDriveAddress__c;
                mapVisibility.put('oneDriveLink', oneDriveLink);
            }else{
                mapVisibility.put('pca_documentmanagement', 'false');
            }
        }
        
        /*---------------------
        End Redirect Documentation Change
        -----------------------*/
        return mapVisibility;
    }
    
}