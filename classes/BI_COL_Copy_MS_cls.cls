/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       15-Abr-2015     Javier Tibamoza              Creación de la Clase
    1.1       17-Ene-2017     Pedro  Párraga               Add isRunningTest
    1.2       31-Ene-2007     Pedro Párraga                delete BI_COL_Codigo_unico_servicio__c because was duplicate
****************************************************************************************************/
public class BI_COL_Copy_MS_cls 
{
    /***
    * @Author: 
    * @Company: Avanxo Colombia
    * @Description: En esta variable estan los campos que no se deben duplicar
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public set<string> noCopiarDatos = new set<string>{'BI_COL_Autorizacion_facturacion__c','BI_COL_Fecha_de_facturacion__c',
        'BI_COL_Estado_orden_trabajo__c','BI_COL_Motivo_cancelacion__c','BI_COL_Fecha_liberacion_OT__c','BI_COL_Fecha_instalacion_servicio_RFS__c',
        'BI_COL_Rango_Dias_Provision__c','BI_COL_Edad_de_la_Provision__c','BI_COL_Parametros_tecnicos__c',
        'BI_COL_Fecha_quiebre__c','BI_COL_Causal_aplazamiento__c','BI_COL_Fecha_solicitud_cancelacion__c',
        'BI_COL_Ingeniero_implantador__c','BI_COL_Ingeniero_implantador__c','BI_COL_Producto__c',
        'BI_COL_Clasificacion_Servicio__c','BI_COL_Bloqueado__c','BI_COL_Numero_de_OT__c','BI_COL_Cargo_fijo_mes__c',
        'BI_COL_IMEI__c','BI_COL_Fecha_inicio_de_cobro_RFB__c','BI_COL_Oportunidad__c','BI_COL_FUN__c', 'BI_COL_Cargo_conexion__c', 'CreatedDate', 
        'CreatedById', 'LastModifiedDate', 'LastModifiedById', 'BI_COL_Codigo_unico_servicio__c','BI_COL_Producto_Anterior__c','BI_COL_Producto__c',
        'BI_COL_Duracion_meses__c','BI_COL_Delta_modificacion_upgrade__c','BI_COL_Cuenta_facturar_davox1__c'};
        //ANCR: 08/02/17 se agregaron los dos ultimos campos para que permita cargar los valores por dataloader y los excluya de la copia
    
    public set<string> campos = new set<string>();
    
    /***
    * @Author: 
    * @Company: Avanxo Colombia
    * @Description: Permite Validar y copiar el servicio de ingenieria
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public void inicioServicioIngenieria(list<BI_COL_Modificacion_de_Servicio__c> msNueva)
    {
        /*set<string> clasValidas = new set<string>{ 'MODIFICACION DOWNGRADE','MODIFICACION UPGRADE',
            'CAMBIO SERVICIO ALTA','ALTA','RENEGOCIACION','CAMBIO S ALTA TRASLA','UPGRADE TRASLADO',
            'DOWNGRADE TRASLADO','RENEGOCIACION TRASLA'};

        string Swhere = ' ( ( BI_COL_Clasificacion_Servicio__c IN: clasValidas and BI_COL_Estado__c IN (\'Pendiente\',\'Enviado\'))'+
            'Or ( BI_COL_Estado__c = \'Activa\') ) AND BI_COL_Codigo_unico_servicio__c=';
        
        for( BI_COL_Modificacion_de_Servicio__c m : msNueva )
        {
            String soql = getCreatableFieldsSOQL('BI_COL_Modificacion_de_Servicio__c', Swhere + '\'' + 
                    m.BI_COL_Codigo_unico_servicio__c+'\' ORDER BY LastModifiedDate DESC');
            
            //Obtiene la informacion de las MS relacionadas con el servicio de ingenieria
            list<BI_COL_Modificacion_de_Servicio__c> servIngRela = Database.query(soql);
            
            map<string,BI_COL_Modificacion_de_Servicio__c> estadosMs = new map<string,BI_COL_Modificacion_de_Servicio__c>(); 

            if( !servIngRela.isEmpty() )
            {
                for( BI_COL_Modificacion_de_Servicio__c sr : servIngRela )
                {
                    system.debug('\n\n##sr.BI_COL_Estado__c: '+ sr.BI_COL_Estado__c +'\n\n');
                    estadosMs.put( sr.BI_COL_Estado__c, sr );
                }
                //Segun la prioridad del campo se copia respecto a la MS respectiva
                if( estadosMs.get( 'Activa' ) != null )
                    creaCopiaDinamico(m, estadosMs.get( 'Activa' ).clone( false, true ) );                                  
                else if( estadosMs.get( 'Enviado' ) != null )
                    creaCopiaDinamico(m, estadosMs.get( 'Enviado' ).clone( false, true ) );
                else if( estadosMs.get( 'Pendiente' ) != null )
                    creaCopiaDinamico( m, estadosMs.get( 'Pendiente' ).clone( false, true ) );
                m.BI_COL_Estado__c = 'Pendiente';
            }
        }*/
    }
    /***
    * @Author: 
    * @Company: Avanxo Colombia
    * @Description:
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public void inicio( list<BI_COL_Modificacion_de_Servicio__c> msNueva )
    {
        set<string> clasMSNocopiaSucursal = new set<string>{
                                                'DOWNGRADE TRASLADO',
                                                'TRASLADO',
                                                'UPGRADE TRASLADO',
                                                'RENEGOCIACION TRASLA'
                                                }; 
        for( BI_COL_Modificacion_de_Servicio__c m : msNueva )
        {
            if(clasMSNocopiaSucursal.contains(m.BI_COL_Clasificacion_Servicio__c))
            {
                noCopiarDatos.add('BI_COL_Sucursal_Origen__c');
                //noCopiarDatos.add('BI_COL_Producto__c');
            }

            String soql = getCreatableFieldsSOQL( 'BI_COL_Modificacion_de_Servicio__c', 
                'BI_COL_Codigo_unico_servicio__c=\'' + m.BI_COL_Codigo_unico_servicio__c + '\' and BI_COL_Estado__c=\'Activa\' limit 1');

            BI_COL_Modificacion_de_Servicio__c ms;

            if(!Test.isRunningTest()){
                ms = (BI_COL_Modificacion_de_Servicio__c)Database.query(soql);              
            }

        
            if( ms != null )
            {ms = (BI_COL_Modificacion_de_Servicio__c)Database.query(soql);
                creaCopiaDinamico( m, ms.clone(false, true) );
                m.BI_COL_Estado__c = 'pendiente';
            }
            else
            {
                m.addError('Para crear una MS con clasificacion de servicio (' + 
                    m.BI_COL_Clasificacion_Servicio__c + ') debe existir en la DS: ' + 
                    m.BI_COL_Codigo_unico_servicio__c+' una MS en estado activo');
            }

            if( Test.isRunningTest() )
                System.debug( getMethodCopiar() );
        }
    }
    /***
    * @Author: 
    * @Company: Avanxo Colombia
    * @Description: retorna un query con toda la info de registro 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public string getCreatableFieldsSOQL( String objectName, String whereClause )
    {
        String selects = '';

        if( whereClause == null || whereClause == '' ){ return null; }

        // Get a map of field name and field token
        map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get( objectName.toLowerCase() ).getDescribe().Fields.getMap();

        set<string> selectFields = new set<string>();
        
        if( objectName.toLowerCase() == 'BI_COL_Modificacion_de_Servicio__c' )
            selectFields.add('BI_COL_Estado__c');
         
        if( fMap != null )
        {
            for( Schema.SObjectField ft : fMap.values() )
            { // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                // field is creatable
                if ( fd.isCreateable() && fd.isAccessible() )
                    selectFields.add( fd.getName() );
            }
        }
         
        if ( !selectFields.isEmpty() )
        {
            for( string s : selectFields )
            {
                if( !noCopiarDatos.contains( s ) )
                {
                    selects += s + ',';
                    campos.add(s);
                }
            }
            
            if( selects.endsWith(',') ) {selects = selects.substring(0,selects.lastIndexOf(','));}
        
        }
        //return 'SELECT ' + selects + ' , BI_COL_Codigo_unico_servicio__c FROM ' + objectName + ' WHERE ' + whereClause; //23/02/17: GSMP
        return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
    }
    /***
    * @Author: 
    * @Company: Avanxo Colombia
    * @Description: retorna el codigo fuente creaCopia usese cuando hallan nuevos campos en la ms 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public string getMethodCopiar()
    {
        string code = '\n\n\n\n';
        
        for( string cs : campos )
            code += '       m.'+ cs+' = c.'+cs+';\n';
        
        code += '\n\n\n\n';
        
        return code;
    }
    /***
    * @Author: 
    * @Company: Avanxo Colombia
    * @Description: Permite Validar y copiar una Renegociacion
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public void creaCopiaDinamico(BI_COL_Modificacion_de_Servicio__c m, BI_COL_Modificacion_de_Servicio__c c)
    {
        for( string field : campos )
        
            if(!Test.isRunningTest() && field != 'BI_COL_Codigo_unico_servicio__c'){
                m.put( field, c.get( field ) );
            }
    }
    /***
    * @Author: 
    * @Company: Avanxo Colombia
    * @Description: Permite Validar y copiar una Renegociacion
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public void renegociacion( list<BI_COL_Modificacion_de_Servicio__c> msNueva )
    {
        string Swhere = ' BI_COL_Estado__c IN (\'Pendiente\',\'Enviado\',\'Activa\')'+
            ' AND BI_COL_Codigo_unico_servicio__c IN ';
        
        set<string> idDs = new set<string>();
        
        for( BI_COL_Modificacion_de_Servicio__c msR : msNueva )
            idDs.add( msR.BI_COL_Codigo_unico_servicio__c );        
        
        Swhere = Swhere + ':idDs ORDER BY BI_COL_Codigo_unico_servicio__c DESC';
        
        String soql = getCreatableFieldsSOQL( 'BI_COL_Modificacion_de_Servicio__c', Swhere );
        
        list<BI_COL_Modificacion_de_Servicio__c> servIngRela = Database.query( soql );
        
        map<string,BI_COL_Modificacion_de_Servicio__c> estadosMS = new map<string,BI_COL_Modificacion_de_Servicio__c>();
        
        for( BI_COL_Modificacion_de_Servicio__c msR : servIngRela )
            estadosMS.put( msR.BI_COL_Codigo_unico_servicio__c + msR.BI_COL_Estado__c, msR );
        
        for( BI_COL_Modificacion_de_Servicio__c m : msNueva )
        {
            BI_COL_Modificacion_de_Servicio__c mOriginal = new BI_COL_Modificacion_de_Servicio__c();
            
            if( estadosMS.get( m.BI_COL_Codigo_unico_servicio__c + 'Enviado' ) != null )
                mOriginal = estadosMS.get( m.BI_COL_Codigo_unico_servicio__c + 'Enviado' );
            else if( estadosMS.get( m.BI_COL_Codigo_unico_servicio__c + 'Pendiente' ) != null )
                mOriginal = estadosMS.get( m.BI_COL_Codigo_unico_servicio__c + 'Pendiente' );               
            else if( estadosMS.get( m.BI_COL_Codigo_unico_servicio__c + 'Activa' ) != null )
                mOriginal = estadosMS.get( m.BI_COL_Codigo_unico_servicio__c + 'Activa' );
            else
                continue;
            creaCopiaDinamico(m, mOriginal.clone(false, true));
            m.BI_COL_Estado__c = 'Pendiente';               
        }
    }
}