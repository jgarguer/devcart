global class BI_UpdateEntitlementsAcc_JOB implements Schedulable {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Scheduled class that updates Account fields.
    Test Class:    BI_Account_JOB_TEST

    History: 
    
     <Date>                     <Author>                <Change Description>
    17/02/2015                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global Id Id_LastAcc;
    global List<SlaProcess> lst_Procesos;

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that executes the job, and reassing Entitlements

    History: 
    
     <Date>                     <Author>                <Change Description>
    17/02/2015                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global void execute(SchedulableContext sch) {
        try{
            system.abortJob(sch.getTriggerId());
    		
            List<Account> lst_accToUpd = new List <Account>();
			List <Account> lst_acc = new List <Account>();
			Integer limite =(Test.isRunningTest())?1:1000;

            

            if (Id_LastAcc==null){
                lst_acc = [SELECT Id, (SELECT ID, SlaProcessId FROM Entitlements WHERE SLaProcessId IN :lst_Procesos) FROM Account WHERE BI_Country__c = :Label.BI_Chile ORDER BY Id ASC limit :limite ];
            }else{
            	lst_acc = [SELECT Id, (SELECT ID, SLaProcessId FROM Entitlements WHERE SlaProcessId IN :lst_Procesos) FROM Account WHERE Id > :Id_LastAcc AND BI_Country__c  = :Label.BI_Chile ORDER BY Id ASC limit :limite ];
            }

            System.debug('ACC QUERY [lst_acc]: '+ lst_acc);

            if(!lst_acc.isEmpty() && lst_Procesos != null && !lst_Procesos.isEmpty()){
	            //List<SlaProcess> lst_SLAProccess = [SELECT CreatedById,CreatedDate,Description,Id,IsActive,IsDeleted,IsVersionDefault,LastModifiedById,LastModifiedDate,Name,NameNorm,StartDateField,
	            //                                        SystemModstamp,VersionMaster,VersionNotes,VersionNumber FROM SlaProcess WHERE Name Like '%Chile%' AND IsVersionDefault = true AND IsActive = true ];

	            List <BusinessHours> lst_hours = [SELECT FridayEndTime,FridayStartTime,Id,MondayEndTime,MondayStartTime,Name,SaturdayEndTime,SaturdayStartTime,
	                              SundayEndTime,SundayStartTime,ThursdayEndTime,ThursdayStartTime,TuesdayEndTime,
	                              TuesdayStartTime,WednesdayEndTime,WednesdayStartTime FROM BusinessHours WHERE Name like 'Horario oficina CHI' AND IsActive = true limit 1];


	            List<Entitlement> lst_Entitlement = new List<Entitlement>();

                for(Account acc :lst_acc){
                    if(acc.Entitlements.isEmpty()){
                        for(SlaProcess Proceso :lst_Procesos){
                            Entitlement ent = new Entitlement(
                                                   Name = Proceso.Name,
                                                   Type = 'Web Support',
                                                   SlaProcessId = Proceso.Id,
                                                   AccountId = acc.Id,
                                                   StartDate = Date.today(),
                                                   BusinessHoursId = lst_hours[0].Id
                                                   );
                           	lst_Entitlement.add(ent);
                        }
                    }else{
                        Set<Id> set_id = new Set<Id>();

                        for(Entitlement ent :acc.Entitlements){
                            set_id.add(ent.SlaProcessId);
                        }
                        

                        for(SlaProcess Proceso :lst_Procesos){
                            if(!set_id.contains(Proceso.Id)){
                                Entitlement ent = new Entitlement(
                                                   Name = Proceso.Name,
                                                   Type = 'Web Support',
                                                   SlaProcessId = Proceso.Id,
                                                   AccountId = acc.Id,
                                                   StartDate = Date.today(),
                                                   BusinessHoursId = lst_hours[0].Id
                                                   );
                                lst_Entitlement.add(ent);
                            }
                        }

                    }

                   	Id_LastAcc = acc.Id;
                }

                insert lst_Entitlement;


				//Rerun Job
                System.debug('Limite: ' + limite + 'List size: ' + lst_acc.size() );
                System.debug('RECALL: Id_LastAcc = ' + Id_LastAcc);
                System.debug('RECALL: lst_Procesos = ' + lst_Procesos);

                if(lst_acc.size() == limite){
                    BI_Jobs.updateEntitlementsAcc(Id_LastAcc, lst_Procesos, false);
                }else{
                    BI_Jobs.updateEntitlementsAcc(Id_LastAcc, lst_Procesos, true);
                }
                
            }else{
                System.debug('1 =[' + lst_acc + '] 2 =[' + lst_Procesos + '] 3 =[' + lst_Procesos + ']');
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_Account_JOB.Schedulable', 'BI_EN', Exc, 'Schedulable Job');
        }
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Constructor method
    
    IN:            Account Id
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    17/02/2015        Micah Burgos       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI_UpdateEntitlementsAcc_JOB(Id idAcc, List<SlaProcess> lst_news_SLAProceso){
        Id_LastAcc = idAcc;
        lst_Procesos = lst_news_SLAProceso;

        system.debug('CONSTRUCTOR: Id_LastAcc = ' + Id_LastAcc);
        system.debug('CONSTRUCTOR: lst_Procesos = ' + lst_Procesos);

    }
}