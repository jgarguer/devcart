/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción
*           -----   ----------      ---------------------------     ---------------
* @version   1.0    2015-07-27      Daniel ALexander Lopez (DL)     Create Class
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
          20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/

@isTest
private class BI_COL_SucursalesValidacion_tst {
  public static list<Opportunity>                               lstOppAsociadas = new list<Opportunity>();
  public static Opportunity                                       objOppty;

  // public static List<BI_COL_FUN_Demo_ctr.wrappMs>               lstWrapper = new List<BI_COL_FUN_Demo_ctr.wrappMs>();

  public static Account                                           objCuenta;

  public static BI_COL_Anexos__c                                  objAnexos;

  public static List<BI_COL_Modificacion_de_Servicio__c>          lstModSer;
  public static BI_COL_Modificacion_de_Servicio__c                objModSer;

  public static List<BI_COL_Descripcion_de_servicio__c>           lstDesSer;
  public static BI_COL_Descripcion_de_servicio__c                 objDesSer;

  public static List<RecordType>                                  rtBI_FUN;

  public static BI_Sede__c                                        direccion;
  public static List<BI_Sede__c>                                  LstdireccionesNuevas = new List<BI_Sede__c>();
  public static List<BI_Sede__c>                                  LstdireccionesViejas = new List<BI_Sede__c>();

  public static BI_Col_Ciudades__c                                ciudad;
  public static list <UserRole> lstRoles;
  public static User objUsuario;
  public static Contact                               objContacto;
  public static BI_Col_Ciudades__c                    objCiudad;
  public static BI_Sede__c                            objSede;
  public static BI_Punto_de_instalacion__c            objPuntosInsta;

  public static void crearData() {

    BI_bypass__c objBibypass = new BI_bypass__c();
    objBibypass.BI_migration__c = true;
    insert objBibypass;

    objCuenta                                       = new Account();
    objCuenta.Name                                  = 'prueba';
    objCuenta.BI_Country__c                         = 'Colombia';
    objCuenta.TGS_Region__c                         = 'América';
    objCuenta.BI_Tipo_de_identificador_fiscal__c    = 'NIT';
    objCuenta.CurrencyIsoCode                       = 'GTQ';
            objCuenta.BI_Segment__c                         = 'test';
        objCuenta.BI_Subsegment_Regional__c             = 'test';
        objCuenta.BI_Territory__c                       = 'test';
    insert objCuenta;

    System.debug('\n\n\n ======= CUENTA ======\n ' + objCuenta );

    for (Integer i = 0; i < 10; i++) {
      objOppty                                      = new Opportunity();
      objOppty.Name                                 = 'TEST AVANXO OPPTY' + i;
      objOppty.AccountId                            = objCuenta.Id;
      //objOppty.TGS_Region__c                      = 'América';
      objOppty.BI_Country__c                        = 'Colombia';
      objOppty.CloseDate                            = System.today().addDays(i);
      objOppty.StageName                            = 'F6 - Prospecting';
      objOppty.BI_Ciclo_ventas__c                   = Label.BI_Completo;
      lstOppAsociadas.add(objOppty);
    }

    System.debug('\n\n\n ======= lstOppyAsociadas ======\n ' + lstOppAsociadas );

    insert lstOppAsociadas;

    rtBI_FUN = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];

    objAnexos               = new BI_COL_Anexos__c();
    objAnexos.Name          = 'FUN-0041414';
    objAnexos.RecordTypeId  = rtBI_FUN[0].Id;
    insert objAnexos;

    System.debug('\n\n\n ======== objAnexos ' + objAnexos + '\n ====> objAnexos.Id ' + objAnexos.Id);



    System.debug('\n\n\n ======= objDesSer ' + lstModSer);



    ciudad                                               = new BI_Col_Ciudades__c();
    ciudad.Name                                          = 'Test';
    ciudad.CurrencyIsoCode                               = 'COP';
    insert ciudad;

    for (integer i = 0; i < 5; i++) {
      direccion                                            = new BI_Sede__c();
      direccion.BI_COL_Ciudad_Departamento__c = ciudad.Id;
      direccion.BI_Direccion__c = 'Test Street 123 Number 321';
      direccion.BI_Localidad__c = 'Test Local';
      direccion.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor3EstadoCallejero;
      direccion.BI_COL_Sucursal_en_uso__c = 'Pendiente';
      direccion.BI_Country__c = 'Colombia';
      direccion.Name = 'Test Street 123 Number 321, Test Local Colombia';
      direccion.BI_Codigo_postal__c = '12356';
      LstdireccionesNuevas.add(direccion);
      LstdireccionesViejas.add(direccion);
    }


    direccion                                            = new BI_Sede__c();
    direccion.BI_COL_Ciudad_Departamento__c = ciudad.Id;
    direccion.BI_Direccion__c = 'Test Street 123 Number 321';
    direccion.BI_Localidad__c = 'Test Local';
    direccion.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor3EstadoCallejero;
    direccion.BI_COL_Sucursal_en_uso__c = 'Pendiente';
    direccion.BI_Country__c = 'Colombia';
    direccion.Name = 'Test Street 123 Number 321, Test Local Colombia';
    direccion.BI_Codigo_postal__c = '12356';
    insert direccion;
    // insert lstModSer;

    LstdireccionesNuevas.add(direccion);
    LstdireccionesViejas.add(direccion);
    //   insert LstdireccionesNuevas;
    //  insert LstdireccionesViejas;

    BI_Punto_de_instalacion__c sedeInstalacion       = new BI_Punto_de_instalacion__c();
    sedeInstalacion.BI_Sede__c               = direccion.Id;
    sedeInstalacion.BI_Cliente__c              = objCuenta.Id;
    sedeInstalacion.Name                 = 'Sede Pruebas';
    insert sedeInstalacion;

    objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
    objDesSer.BI_COL_Oportunidad__c                     = lstOppAsociadas[0].Id;
    objDesSer.CurrencyIsoCode                           = 'COP';
    objDesSer.BI_COL_Sede_Destino__c            = sedeInstalacion.Id;
    insert objDesSer;

    //Contactos
    objContacto                                     = new Contact();
    objContacto.LastName                            = 'Test';
    objContacto.BI_Country__c                       = 'Colombia';
    objContacto.CurrencyIsoCode                     = 'COP';
    objContacto.AccountId                           = objCuenta.Id;
    objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
    //REING-INI
	objContacto.MobilePhone          				= '1236547890';
	objContacto.Phone								= '11111111';
	objContacto.Email								= 'test@test.com';		
    objContacto.BI_Tipo_de_documento__c 			= 'Otros';
    objContacto.BI_Numero_de_documento__c 			= '00000000X';
    objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
    //REING_FIN 
    
    Insert objContacto;

    //Ciudad
    objCiudad = new BI_Col_Ciudades__c ();
    objCiudad.Name                      = 'Test City';
    objCiudad.BI_COL_Pais__c            = 'Test Country';
    objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
    insert objCiudad;
    System.debug('Datos Ciudad ===> ' + objSede);

    //Direccion
    objSede                                 = new BI_Sede__c();
    objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
    objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
    objSede.BI_Localidad__c                 = 'Test Local';
    objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
    objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
    objSede.BI_Country__c                   = 'Colombia';
    objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
    objSede.BI_Codigo_postal__c             = '12356';
    insert objSede;
    System.debug('Datos Sedes ===> ' + objSede);

    //Sede
    objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
    objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
    objPuntosInsta.BI_Sede__c          = objSede.Id;
    objPuntosInsta.BI_Contacto__c      = objContacto.id;
    objPuntosInsta.Name      = 'QA Erroro';
    insert objPuntosInsta;
    System.debug('Datos Sucursales ===> ' + objPuntosInsta);

    objModSer                                       = new BI_COL_Modificacion_de_Servicio__c();
    objModSer.BI_COL_FUN__c                         = objAnexos.Id;
    objModSer.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
    objModSer.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
    objModSer.BI_COL_Oportunidad__c                 = objOppty.Id;
    objModSer.BI_COL_Bloqueado__c                   = false;
    objModSer.BI_COL_Estado__c                      = 'Activa';//label.BI_COL_lblActiva;
    objModSer.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
    objModSer.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
    insert objModSer;

    //for(Integer i = 0; i < 10; i++)
    //{

    // objModSer                                        = new BI_COL_Modificacion_de_Servicio__c();
    // objModSer.BI_COL_FUN__c                          = objAnexos.Id;
    // objModSer.BI_COL_Codigo_unico_servicio__c        = objDesSer.Id;
    // objModSer.BI_COL_Clasificacion_Servicio__c       = 'ALTA';
    // objModSer.BI_COL_Sucursal_de_Facturacion__c    = sedeInstalacion.id;
    // objModSer.BI_COL_Sede_Destino__c           = sedeInstalacion.id;
    // objModSer.BI_COL_Sucursal_Origen__c        = sedeInstalacion.id;
    // //objModSer.BI_COL_Direccion_IP__c            = i+'';
    // insert objModSer;
    //  //  lstModSer.add(objModSer);
    ////}

    update direccion;

    System.debug('\n\n\n ======= lstModSer ' + lstModSer);

  }


  public static testMethod void test_method_one() {
    objUsuario = BI_COL_CreateData_tst.getCreateUSer();
    System.runAs(objUsuario) {
      crearData();
      Test.startTest();
      BI_COL_SucursalesValidacion_cls sucursalvalidacion = new BI_COL_SucursalesValidacion_cls(true);
      sucursalvalidacion.modificaEstadoCallejero(LstdireccionesNuevas, LstdireccionesViejas);
      sucursalvalidacion = new BI_COL_SucursalesValidacion_cls(false);
      sucursalvalidacion.lstSucursal = LstdireccionesNuevas;
      sucursalvalidacion.validaClientesSucursal();

      Test.stopTest();
    }
  }

}