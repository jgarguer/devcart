@isTest public class BI2_CaseMethods_TEST {

    //JMF: TODO: Use DataLoader 

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Álvaro López
        Company:       Aborda
        Description:   Test setup for this class
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        12/07/2016                      Álvaro López                Added Entitlement data
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @testSetup public static void testSetup() {
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);
        
        insert new BI2_Stop_Status__c(Name='Pending');

        List<QueueSObject> lst_colas = [SELECT Id, Queue.Id, Queue.DeveloperName FROM QueueSObject WHERE SObjectType='Case' Limit 1]; //JEG Limitamos la query
        List<Account> lst_clients = new List<Account>();
        for(Integer iter = 0; iter < 2; iter++) {
            lst_clients.add(new Account(Name='TestAccount_0'+iter, BI_Country__c = Label.BI_Peru,BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test'));
        }
        lst_clients[0].BI2_asesor__c=UserInfo.getUserId();
        lst_clients[1].BI2_cuadrilla__c = lst_colas[0].Queue.DeveloperName;
        insert lst_clients;


        List<Contact> lst_contacts = new List<Contact>();
        for(Integer iter = 0; iter < lst_clients.size(); iter++) {
            lst_contacts.add(new Contact(LastName='TestContact_0' + iter, AccountId=lst_clients[iter].Id, email='Test@testest.test'));
        }
        insert lst_contacts;

        BI_Mapeo_criterios_asignacion__c mca1 = new BI_Mapeo_criterios_asignacion__c(
            Name = 'PER-Tipología',
            BI_Nombre_API_Casos__c = 'BI2_per_tipologia__c',
            BI_Nombre_API_Criterios_de_asignacion__c = 'BI2_PER_Tipologia__c',
            BI_Country__c = 'Peru'
        );
        insert mca1;

        BI_Mapeo_criterios_asignacion__c mca2 = new BI_Mapeo_criterios_asignacion__c(
            Name = 'PER-Subtipología',
            BI_Nombre_API_Casos__c = 'BI2_per_subtipologia__c',
            BI_Nombre_API_Criterios_de_asignacion__c = 'BI2_PER_SubTipologia__c',
            BI_Country__c = 'Peru'
        );
        insert mca2;

        BI_Proceso_de_asignacion__c proc = new BI_Proceso_de_asignacion__c(
            Name = 'Test'
        );
        insert proc;

        BI_Criterio_de_asignacion__c cda = new BI_Criterio_de_asignacion__c(
            BI_Country__c = Label.BI_Peru,
            BI2_PER_Motivo_del_caso__c = 'Test',
            BI2_PER_Tipologia__c = 'Test',
            BI2_PER_Linea_de_Negocio__c = 'Test',
            BI2_PER_Limite_inferior__c = 60,
            BI2_PER_Territorio__c = 'Test',
            BI_Proceso_de_asignacion__c = proc.Id
        );
        insert cda;

        SlaProcess sla = [SELECT Id FROM SlaProcess WHERE isActive = true LIMIT 1];

        Entitlement ent = new Entitlement(
            Name = 'Test',
            AccountId = lst_clients[0].Id,
            SlaProcessId = sla.Id,
            StartDate = Date.today()
        );
        insert ent;

    }

    @isTest public static void test_updateContactNoOfCasesOnCaseInsert() {
        BI_TestUtils.throw_exception = false;
        RecordType rtCasoPadre = [SELECT Id FROM RecordType WHERE DeveloperName='BI2_Caso_Padre'];
        List<Contact> lst_contacts = [SELECT Id, BI2_N_Casos_Ultimo_Mes__c, (SELECT Id FROM Cases) FROM Contact WHERE Name LIKE 'TestContact_0%'];
        
        System.assertEquals(true, (lst_contacts[0].BI2_N_Casos_Ultimo_Mes__c==null || lst_contacts[0].BI2_N_Casos_Ultimo_Mes__c==0));
        
        Case c = new Case(Status='Assigned', RecordTypeId=rtCasoPadre.Id, Subject='TestSubject', ContactId = lst_contacts[0].Id);
        
        Test.startTest();
        insert c;
        
        lst_contacts = [SELECT Id, BI2_N_Casos_Ultimo_Mes__c, (SELECT Id FROM Cases) FROM Contact WHERE Id =:lst_contacts[0].Id];
        System.debug('+*+++*+ lst_contacts: ' + lst_contacts[0]);
        System.debug('+*+++*+ lst_contacts.Cases: ' + lst_contacts[0].Cases);
        //List<BI_Log__c> logs = [SELECT Id FROM BI_Log__c];
        //System.assertNotEquals(0, logs.size());
        System.assertEquals(lst_contacts[0].Cases.size(), lst_contacts[0].BI2_N_Casos_Ultimo_Mes__c);
        
        Test.stopTest();
    }
    
    @isTest public static void test_updateOwner() {
        BI_TestUtils.throw_exception = false;
        RecordType rtCasoPadre = [SELECT Id FROM RecordType WHERE DeveloperName='BI2_caso_comercial'];
        List<Account> lst_accounts = [SELECT Id FROM Account WHERE Name LIKE 'TestAccount_0%'];
        List<Contact> lst_contacts = [SELECT Id, BI2_Numero_de_Casos_Abiertos__c, (SELECT Id FROM Cases WHERE Status='Assigned') FROM Contact WHERE Name LIKE 'TestContact_0%'];

        Integer casInitSize= lst_contacts[0].Cases.size();
        Case cas = new Case(Status='Assigned',
         RecordTypeId=rtCasoPadre.Id, Subject='TestSubject',
          BI2_PER_Tipologia__c = 'Test',
            BI_Product_Service__c = ' Test',
            BI2_per_subtipologia__c = 'Test' , 
            BI_Line_of_Business__c = 'Test',
            TGS_Disputed_number_of_lines__c = 120,
            BI2_PER_Territorio_caso__c = 'Test',
          ContactId = lst_contacts[0].Id,
           AccountId=lst_accounts[0].Id);
        
        Test.startTest();
        
        insert cas;
      
        lst_contacts = [SELECT Id, BI2_Numero_de_Casos_Abiertos__c FROM Contact WHERE Name LIKE 'TestContact_0%'];
        System.assertEquals(casInitSize+1, lst_contacts[0].BI2_Numero_de_Casos_Abiertos__c);

        delete cas;
        lst_contacts = [SELECT Id, BI2_Numero_de_Casos_Abiertos__c FROM Contact WHERE Name LIKE 'TestContact_0%'];
        System.assertEquals(casInitSize, lst_contacts[0].BI2_Numero_de_Casos_Abiertos__c);

        undelete cas;
        lst_contacts = [SELECT Id, BI2_Numero_de_Casos_Abiertos__c FROM Contact WHERE Name LIKE 'TestContact_0%'];
        System.assertEquals(casInitSize+1, lst_contacts[0].BI2_Numero_de_Casos_Abiertos__c);

        Test.stoptest();

    }

    /*@isTest public static void test_noCloseCaseWithOpenChildCases() {
        BI_testUtils.throw_exception=false;
        //User usu = [SELECT Id FROM User WHERE profile.name='BI_Standard_PER' AND isActive=true AND UserRole.name='PER Asesores' LIMIT 1];
        List<User> lst_usr;
        System.runas(new User(Id=UserInfo.getUserId())) {
        	lst_usr = BI_DataLoad.loadUsers(1, [SELECT ID FROM PROFILE WHERE Name = 'BI_Standard_PER'][0].Id, Label.BI_Administrador);
        }
        User usu = lst_usr[0];

        Id rtCasoCom, rtCasoPad;
        for(RecordType rt : [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI2_caso_comercial', 'BI2_Caso_Padre')]) {
            if(rt.DeveloperName == 'BI2_Caso_Padre') {
                rtCasoPad = rt.Id;
                
            } else if(rt.DeveloperName == 'BI2_caso_comercial') {
                rtCasoCom = rt.Id;
                
            }
        }
        List<Contact> lst_contacts = [SELECT Id, BI2_N_Casos_Ultimo_Mes__c, (SELECT Id FROM Cases) FROM Contact WHERE Name LIKE 'TestContact_0%'];
        //lst_contacts[0].ownerId=usu.id;
        //update lst_contacts;

        //System.runAs(usu){
            Case cas_padre = new Case(Status='Assigned', RecordTypeId=rtCasoPad, Subject='TestSubject 01', ContactId = lst_contacts[0].Id);
            insert cas_padre;
            System.debug('cas_padre: ' + cas_padre);
            Case cas_hijo = new Case(Status='Assigned', RecordTypeId=rtCasoCom, Subject='TestSubject 01', ContactId = lst_contacts[0].Id, ParentId = cas_padre.id);
            insert cas_hijo;
           
            BI_bypass__c bypass = new BI_bypass__c(BI_skip_trigger__c = true);
            insert bypass;

            cas_padre.Status = 'In Progress';
            update cas_padre;   // Too Many queries

            delete bypass;

            //System.assertEquals(false, true);
            System.debug('cas_padre v2: ' + cas_padre);

            Case casoOld = cas_padre;
            cas_padre.Status = 'Resolved';
            Test.startTest();
            try {
                update cas_padre;
                System.debug('cas_padre v3: ' + cas_padre);

                System.assertEquals(cas_padre.recordTypeId, rtCasoPad);
                System.assertEquals(cas_padre.status,'Resolved');
                throw new BI_Exception('No se realizó la validación correctamente.');
            } catch(DmlException ex) {
              System.debug(LoggingLevel.ERROR, 'Is this expected or unrelated?: ' + ex);
            }
            Test.stopTest();
        //}
    }/**/

    @istest public static void test_setEscalado() {
        BI_TestUtils.throw_exception = false;
        RecordType rtCasoPadre = [SELECT Id FROM RecordType WHERE DeveloperName='BI2_Caso_Padre'];
        List<Contact> lst_contacts = [SELECT Id, BI2_N_Casos_Ultimo_Mes__c FROM Contact WHERE Name LIKE 'TestContact_0%'];
        List<Group> lst_grps = new List<Group> {
            new Group(Type='Queue',DeveloperName='BI_PER_GRP_N2_1', Name='PER_EMP_Gestor comercial'),
            new Group(Type='Queue',DeveloperName='BI_PER_GRP_N3_1', Name='TestN3')
        };
        insert lst_grps;
        System.runAs(new User(Id=UserInfo.getUserId())) {
            insert new List<GroupMember> {
                new GroupMember(GroupId=lst_grps[0].Id, UserOrGroupId=UserInfo.getUserId()),
                new GroupMember(GroupId=lst_grps[1].Id, UserOrGroupId=UserInfo.getUserId())
            };
        }
        System.runAs(new User(Id=UserInfo.getUserId())) {
            List<QueueSObject> lst_queues = new List<QueueSObject> {
                new QueueSObject(QueueId = lst_grps[0].Id, SObjectType='Case'),
                new QueueSObject(QueueId = lst_grps[1].Id, SObjectType='Case')
            };
            insert lst_queues;
        }

        List<Case> lst_cases = new List<Case> {
            new Case(Status='Assigned', RecordTypeId=rtCasoPadre.Id, Subject='TestSubject', ContactId = lst_contacts[0].Id, BI2_PER_escalado_N2__c=false),
            new Case(Status='Assigned', RecordTypeId=rtCasoPadre.Id, Subject='TestSubject', ContactId = lst_contacts[0].Id, BI2_PER_escalado_N3__c=false)
        };
        insert lst_cases;

        lst_cases[0].OwnerId = lst_grps[0].Id;
        lst_cases[1].OwnerId = lst_grps[1].Id;
        
        Test.startTest();
        
        update lst_cases;
        
        Test.stopTest();

        lst_cases = [SELECT Id, OwnerId, BI2_PER_escalado_N3__c, BI2_PER_escalado_N2__c FROM Case WHERE Id IN (:lst_cases.get(0).Id, :lst_cases.get(1).Id)];

        System.assertEquals(lst_grps[0].Id, lst_cases[0].OwnerId);
        System.assertEquals(lst_grps[1].Id, lst_cases[1].OwnerId);

        System.debug('Uh, oh: lst_cases[0]: ' + lst_cases[0]);
        System.debug('Uh, oh: lst_cases[1]: ' + lst_cases[1]);

        System.assertEquals(true, lst_cases[0].BI2_PER_escalado_N2__c);
        System.assertEquals(true, lst_cases[1].BI2_PER_escalado_N3__c);
    }

    @isTest public static void test_setMilestoneEndDate() {
        BI_TestUtils.throw_exception = false;
        RecordType rtCasoPadre = [SELECT Id FROM RecordType WHERE DeveloperName='BI2_Caso_Padre'];
        List<Contact> lst_contacts = [SELECT Id, BI2_N_Casos_Ultimo_Mes__c, AccountId FROM Contact WHERE Name LIKE 'TestContact_0%'];
        

       
        System.runAs(new User(Id=UserInfo.getUserId())) {
            List<MilestoneType> lst_milestonetypes = new List<MilestoneType> {
                new MilestoneType(Name='Test Milestone Type', RecurrenceType='none'),
                new MilestoneType(Name='Paso hasta Old El Paso', RecurrenceType='none'),
                new MilestoneType(Name='Fecha de envio de cotizacion', RecurrenceType='none')
                               
            };
            insert lst_milestonetypes;
        }

        Entitlement ent = new Entitlement(AccountId=lst_contacts[0].AccountId, Name='Test', Type='Asistencia Telefónica');
        insert ent;

        List<Case> lst_cases = new List<Case> {
            new Case(Status='Assigned', RecordTypeId=rtCasoPadre.Id, Subject='TestSubject', ContactId=lst_contacts[0].Id, EntitlementId=ent.Id),
            new Case(Status='Assigned', RecordTypeId=rtCasoPadre.Id, Subject='TestSubject', ContactId=lst_contacts[0].Id, EntitlementId=ent.Id),

            new Case(Status='Assigned', RecordTypeId=rtCasoPadre.Id, Subject='TestSubject', ContactId=lst_contacts[0].Id, EntitlementId=ent.Id),
            new Case(Status='Assigned', RecordTypeId=rtCasoPadre.Id, Subject='TestSubject', ContactId=lst_contacts[0].Id, EntitlementId=ent.Id)


          

        };
        insert lst_cases;


        lst_cases = [SELECT Id, BI_Country__c, BI2_PER_fecha_envio_cotizacion__c, BI2_PER_fecha_escalado_N3__c FROM Case];
    
        System.assertEquals(4, lst_cases.size());

        lst_cases[0].BI_Country__c=Label.BI_Peru;
        lst_cases[0].BI2_PER_fecha_envio_cotizacion__c=Datetime.now();

        lst_cases[1].BI_Country__c=Label.BI_Peru;
        lst_cases[1].BI2_PER_fecha_escalado_N3__c=Datetime.now();

        lst_cases[2].BI_Country__c=Label.BI_Peru;
        lst_cases[2].BI2_PER_fecha_envio_cotizacion__c=Datetime.now();

        lst_cases[3].BI_Country__c=Label.BI_Peru;
        lst_cases[3].BI2_PER_fecha_escalado_N3__c=Datetime.now();
        
        delete [select Id from BI_Log__c];
        
		Test.startTest();
        
        update lst_cases;
        
        system.assert(true, [select Id from BI_Log__c].isEmpty());
        
        Test.stopTest();
    }

    @isTest public static void test_BI_CaseMethods_caseAssignment() {
        //insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=true);
        BI_TestUtils.throw_exception = false;
        BI_MigrationHelper.skipAllTriggers();
        
        insert new TGS_Endpoints__c(Name='rod_ticketing', TGS_Endpoint__c='http://www.example.org');

        RecordType rtCasoPadre = [SELECT Id FROM RecordType WHERE DeveloperName='BI2_Caso_Padre'];
        List<Contact> lst_contacts = [SELECT Id, BI2_N_Casos_Ultimo_Mes__c FROM Contact WHERE Name LIKE 'TestContact_0%'];
        List<Group> lst_grps = new List<Group> {
            new Group(Type='Queue',DeveloperName='BI_PER_GRP_N2_1', Name='PER_EMP_Gestor comercial')
        };
        insert lst_grps;
        System.runAs(new User(Id=UserInfo.getUserId())) {
            insert new List<GroupMember> {
                new GroupMember(GroupId=lst_grps[0].Id, UserOrGroupId=UserInfo.getUserId())
            };
        }
        System.runAs(new User(Id=UserInfo.getUserId())) {
            List<QueueSObject> lst_queues = new List<QueueSObject> {
                new QueueSObject(QueueId = lst_grps[0].Id, SObjectType='Case')
            };
            insert lst_queues;
        }

        List<Case> lst_cases = new List<Case> {
            new Case(Status='Assigned', RecordTypeId=rtCasoPadre.Id, Subject='TestSubject', ContactId = lst_contacts[0].Id, BI2_PER_escalado_N2__c=false)
        };/*,
            new Case(Status='Assigned', RecordTypeId=rtCasoPadre.Id, Subject='TestSubject', ContactId = lst_contacts[0].Id, BI2_PER_escalado_N3__c=false)*/
        insert lst_cases;
		
        for(Case cas : lst_cases) {
            cas.Status = 'In Progress';	// This _should_ be the second BI_Asignacion_de_casos__c entry
        }
        update lst_cases;
        BI_MigrationHelper.cleanSkippedTriggers();
        for(Case cas : lst_cases) {
            cas.Status = 'Assigned'; // This _should_ be the third
        }
        update lst_cases;
        
        List<BI_Asignacion_de_casos__c> lst_registro_asignacion = [SELECT Id, BI_Caso__c, BI_ID_Propietario_asignado__c, BI_Propietario_asignado__c, BI_Usuario_asignado__c FROM BI_Asignacion_de_casos__c WHERE CreatedDate=TODAY];
        System.assertEquals(true, lst_registro_asignacion.size() >=1);
        System.debug(lst_registro_asignacion);

        BI_GlobalVariables.stopTriggereHelpCase = false;
        Test.startTest();
        lst_cases[0].OwnerId = lst_grps[0].Id;
        //lst_cases[0].TGS_Assignee__c = UserInfo.getUserId();
        update lst_cases;
        Test.stopTest();

        lst_registro_asignacion = [SELECT Id, BI_Caso__c, BI_ID_Propietario_asignado__c, BI_Propietario_asignado__c, BI_Usuario_asignado__c FROM BI_Asignacion_de_casos__c WHERE CreatedDate=TODAY];
        
        System.assertEquals(true, lst_registro_asignacion.size() >=2);
        System.debug(lst_registro_asignacion);
    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       Salesforce.com
     Description:   Manage coverage BI2_CaseMethods.fillRequiredFields

     History: 
    
     <Date>                     <Author>                    <Change Description>
     13/11/2015                 Jose Miguel Fierro          Initial Version
     13/04/2016                 Antonio Pardo               CustomSetting to fill BusinessHours in Case
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest public static void test_fillRequiredFields() {
        BI_TestUtils.throw_exception = false;

        RecordType rtCasoPadre = [SELECT Id FROM RecordType WHERE DeveloperName='BI2_Caso_Padre'];

        //Create Cases
        Case casVEN = new Case(
            RecordTypeId = rtCasoPadre.Id, 
            Origin='Email to Case PER', 
            Subject='WASD',
            BI_Country__c = 'Venezuela' 
            );
         Case casARG = new Case(
            RecordTypeId = rtCasoPadre.Id, 
            Origin='Email to Case PER', 
            Subject='WASD',
            BI_Country__c = 'Argentina' 
            );

        //Horarios a asignar Perú y Venezuela, por defecto ARG
        List<BusinessHours> lst_bh = [SELECT Id, Name FROM BusinessHours WHERE Name IN ('Horario oficina VEN' , 'Horario oficina PER')];
        //CUSTOM SETTING
        BI_Horarios_oficina_pp__c csVEN = new BI_Horarios_oficina_pp__c(Name= 'Horario oficina VEN', BI_Pais__c = 'Venezuela');
        BI_Horarios_oficina_pp__c csPER = new BI_Horarios_oficina_pp__c(Name= 'Horario oficina PER', BI_Pais__c = 'Peru');
        Insert csVEN;
        Insert csPER;

        System.assertEquals(null, casVEN.BI_COL_Fecha_Radicacion__c);
        
        Test.startTest();
        insert casVEN;
        Insert casARG;

        List<Case> lst_cases = [SELECT Id, BI_COL_Fecha_Radicacion__c, BusinessHours.Name FROM Case WHERE Subject = 'WASD' ORDER BY Id];
        System.debug('Resultado: ' + lst_cases);
        System.assertNotEquals(null, lst_cases[0].BI_COL_Fecha_Radicacion__c);
        //Comprobando que se asignan los horarios al caso dependiendo del país del caso
        System.assertEquals(lst_cases[0].BusinessHours.Name, 'Horario oficina VEN');
        System.assertEquals(lst_cases[1].BusinessHours.Name, 'Horario oficina ARG');
        Test.stoptest();
    }

    @isTest public static void test_clearFieldsOnUpdate() {
        BI_TestUtils.throw_exception = false;

        RecordType rtCasoPadre = [SELECT Id FROM RecordType WHERE DeveloperName='BI2_Caso_comercial'];

        Contact con = [SELECT Id FROM Contact WHERE Name LIKE 'TestContact_0%' Limit 1];

        List<Group> lst_grps = new List<Group> {
            new Group(Type='Queue',DeveloperName='BI_PER_GRP_N3_1', Name='TestN3')
        };
        insert lst_grps;
        System.runAs(new User(Id=UserInfo.getUserId())) {
            List<QueueSObject> lst_queues = new List<QueueSObject> {
                new QueueSObject(QueueId = lst_grps[0].Id, SObjectType='Case')
            };
            insert lst_queues;
        }

        Case cas = new Case(RecordTypeId = rtCasoPadre.Id,
         Subject='WASD',
         BI2_PER_Tipologia__c = 'Test',
         BI_Line_of_Business__c = 'Test',
         TGS_Disputed_number_of_lines__c = 120,
         BI2_PER_Territorio_caso__c = 'Test',
         BI_Product_Service__c = ' Test',
         BI2_per_subtipologia__c = 'Test', 
         OwnerId = lst_grps[0].Id,
         ContactId = con.Id,
        TGS_Assignee__c=UserInfo.getUserId());
        insert cas;

        Test.startTest();
        cas.OwnerId = UserInfo.getUserId();
        update cas;
        Test.stopTest();


        List<Case> lst_cases = [SELECT Id, TGS_Assignee__c FROM Case WHERE Subject = 'WASD'];
        System.debug('Resultado: ' + lst_cases);
        System.assertEquals(null, lst_cases[0].TGS_Assignee__c);
    }

    @isTest public static void test_assignEntitlements() {
      BI_TestUtils.throw_exception = false;
        Map<Id, String> MAP_RT_NAME = new Map<Id, String>();
        BI2_CaseMethods caseMethods = new BI2_CaseMethods();
        RecordType recort = new RecordType(DeveloperName='BI2_caso_comercial', sObjectType = 'Case');
        RecordType recort2 = new RecordType(DeveloperName='BI2_caso_comercial', sObjectType = 'Case');
        //RecordType rtCasoCom = [SELECT Id FROM RecordType WHERE sObjectType = 'Case' and DeveloperName='BI2_caso_comercial'];
        List<Case> casosnews = new List<Case>();
        List<Case> casosOlds = new List<Case>();
        casosnews.add(new Case(RecordTypeId=recort.Id, BI_Line_of_Business__c= 'Datos' ));
        casosOlds.add(new Case(RecordTypeId=recort2.Id, BI_Line_of_Business__c= 'Otros' ));
        
        Test.startTest();
        
        BI2_CaseMethods.assignEntitlements(casosnews, casosOlds);
        
        system.assert(true, [select Id from BI_Log__c].isEmpty());
        
        Test.stopTest();
    }
	
    @isTest public static void test_exceptions() {
        NETriggerHelper.setTriggerFired('TestException.generate_BILog');
        BI_TestUtils.throw_exception = true;
        
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);

        Test.startTest();
        
        //BI2_CaseMethods.calculateBusinessHoursAges(null, null);
        BI2_CaseMethods.calculateCaseContactOpenCases(null);
        BI2_CaseMethods.deleteCaseUpdateContact(null);
        BI2_CaseMethods.insertCaseUpdateContact(null);
        BI2_CaseMethods.noCloseCaseWithOpenChildCases(null, null);
        BI2_CaseMethods.setEscalado(null, null);
        //BI2_CaseMethods.setLastStatusChangeOnInsert(null);
        BI2_CaseMethods.setMilestoneEndDate(null, null);
        BI2_CaseMethods.undeleteCaseUpdateContact(null);
        BI2_CaseMethods.updateCaseUpdateContact(null, null);
        BI2_CaseMethods.updateContactNoOfCasesOnCaseInsert(null);
        BI2_CaseMethods.updateOwner(null, null);
        BI2_CaseMethods.fillRequiredFields(null);
        BI2_CaseMethods.clearFieldsOnUpdate(null, null);
        BI2_CaseMethods.assignEntitlements(null, null);
        
        system.assertEquals(false, [select Id from BI_Log__c].isEmpty());
        
        Test.stopTest();

    }
    
    static testMethod void fillRelatedCases_TEST() {
    	
    	BI_TestUtils.throw_exception = false;
        
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);
        
        Id rtCasoCom, rtCasoPad;
        for(RecordType rt : [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI2_caso_comercial', 'BI2_Caso_Padre')]) {
            if(rt.DeveloperName == 'BI2_Caso_Padre') {
                rtCasoPad = rt.Id;
            } else if(rt.DeveloperName == 'BI2_caso_comercial') {
                rtCasoCom = rt.Id;
            }
        }

        Contact con = [SELECT Id FROM Contact WHERE Name LIKE 'TestContact_0%' Limit 1];
        
        Case cas_padre = new Case(Status='Assigned',
                                RecordTypeId=rtCasoPad,
                                Subject='TestSubject 01',
                                BI2_PER_Tipologia__c = 'Test',
                                BI_Product_Service__c = ' Test',
                                BI2_per_subtipologia__c = 'Test' , 
                                BI_Line_of_Business__c = 'Test',
                                TGS_Disputed_number_of_lines__c = 120,
                                BI2_PER_Territorio_caso__c = 'Test');
        insert cas_padre;
        
        Test.startTest();
        
        Case cas_hijo = new Case(Status='Assigned',
         RecordTypeId=rtCasoCom,
                                Subject='TestSubject 01',
                                BI2_PER_Tipologia__c = 'Test',
                                BI_Product_Service__c = ' Test',
                                BI2_per_subtipologia__c = 'Test' , 
                                BI_Line_of_Business__c = 'Test',
                                TGS_Disputed_number_of_lines__c = 120,
                                BI2_PER_Territorio_caso__c = 'Test',
                                ParentId = cas_padre.id,
					            ContactId = con.Id,
                                BI_Confidencial__c = false);
        insert cas_hijo;
        
        cas_padre = [select Id, BI2_Casos_Hijos__c from Case where Id = :cas_padre.Id]; 
        
        system.assert(cas_padre.BI2_Casos_Hijos__c != null && cas_padre.BI2_Casos_Hijos__c != '');
           
        Test.stopTest();
    }
    
	static testMethod void testBusinessHoursBucketer() {
		
		BI_TestUtils.throw_exception = false;

        RecordType rtCasoPadre = [SELECT Id FROM RecordType WHERE DeveloperName='BI2_Caso_comercial'];
        Contact con = [SELECT Id FROM Contact WHERE Name LIKE 'TestContact_0%' Limit 1];
        
        BI_MigrationHelper.skipAllTriggers();
        BI2_Stop_Status__c ss = new BI2_Stop_Status__c(Name = 'On Hold');
        insert ss;
        BI_MigrationHelper.cleanSkippedTriggers();

        Case c = new Case();
        c.Status = 'New';
        c.BI2_Last_Status_Change__c = System.Now();
        c.RecordTypeId = rtCasoPadre.Id;
        c.BI2_PER_Tipologia__c = 'Test';
        c.BI_Product_Service__c = ' Test';
        c.BI2_per_subtipologia__c = 'Test'; 
        c.BI_Line_of_Business__c = 'Test';
        c.TGS_Disputed_number_of_lines__c = 120;
        c.BI2_PER_Territorio_caso__c = 'Test';
        c.ContactId = con.Id;
        insert c;

        Test.startTest();
        c.Status = 'On Hold';
        update c;
        c.Status = 'New';
        update c;

    	Case updatedCase = [select BI2_Time_With_Customer__c, BI2_Time_With_Support__c, BI2_Case_Age_In_Business_Hours__c from Case where Id=:c.Id];
    	System.assert(updatedCase.BI2_Time_With_Customer__c!=null);
        System.assert(updatedCase.BI2_Time_With_Support__c!=null);
        System.assert(updatedCase.BI2_Case_Age_In_Business_Hours__c!=null);

        c.Status = 'Closed';
        c.TGS_Status_reason__c = 'test';
        c.TGS_Resolution__c = 'test';
        
        update c;
        updatedCase = [select BI2_Time_With_Customer__c, BI2_Time_With_Support__c, BI2_Case_Age_In_Business_Hours__c from Case where Id=:c.Id];

        System.assert(updatedCase.BI2_Time_With_Customer__c!=null);
        System.assert(updatedCase.BI2_Time_With_Support__c!=null);
        System.assert(updatedCase.BI2_Case_Age_In_Business_Hours__c!=null);
        
        Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that this
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        16/03/2016                      Guillermo Muñoz             Overwrited for allow to include new countries 
        12/07/2016                      Álvaro López                Cover the new @future method "assignEntitlementsFuture" making the test asynchronous.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void assignEntitlementsTEST1() {
    	
    	BI_TestUtils.throw_exception = false;
        
        //insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);

        Map <String, RecordType> map_rt = new Map <String, RecordType>();
        for(RecordType rt : [SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName IN ('BI2_caso_comercial', 'BI2_Caso_Padre') AND sObjectType = 'Case']){
            map_rt.put(rt.DeveloperName, rt);
        }

        Account acc = [SELECT Id FROM Account WHERE Name = 'TestAccount_00'LIMIT 1];
        Contact con = [SELECT Id FROM Contact WHERE Name LIKE 'TestContact_0%' Limit 1];


        /////////////////Insert////////////////////
        Test.startTest();
        Case cas = new Case(
            RecordTypeId = map_rt.get('BI2_caso_comercial').Id,
            Reason = 'Test',
            BI2_PER_Tipologia__c = 'Test',
            BI_Product_Service__c = ' Test',
            BI2_per_subtipologia__c = 'Test' , 
            BI_Line_of_Business__c = 'Test',
            TGS_Disputed_number_of_lines__c = 120,
            BI2_PER_Territorio_caso__c = 'Test',
            ContactId = con.Id,
            AccountId = acc.Id,
            BI_Country__c = Label.BI_Peru
        );
        insert cas;

        Test.stopTest();
        Case toAssert = [SELECT EntitlementId FROM Case WHERE Id =: cas.Id LIMIT 1];

    	System.assertEquals(toAssert.EntitlementId, [SELECT Id FROM Entitlement WHERE Name = 'Test' LIMIT 1].Id);


    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Álvaro López
        Company:       Aborda
        Description:   Cover the new @future method "assignEntitlementsFuture" making the test asynchronous.
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        12/07/2016                      Álvaro López                Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void assignEntitlementsTEST2() {
        
        BI_TestUtils.throw_exception = false;
        
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);

        Map <String, RecordType> map_rt = new Map <String, RecordType>();
        for(RecordType rt : [SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName IN ('BI2_caso_comercial', 'BI2_Caso_Padre') AND sObjectType = 'Case']){
            map_rt.put(rt.DeveloperName, rt);
        }

        Account acc = [SELECT Id FROM Account WHERE Name = 'TestAccount_00'LIMIT 1];

        /////////////////Update////////////////////
        Test.startTest();
        Case cas2 = new Case(
            RecordTypeId = map_rt.get('BI2_Caso_Padre').Id,
            Reason = 'Test',
            BI2_PER_Tipologia__c = 'Test',
            BI_Product_Service__c = ' Test',
            BI2_per_subtipologia__c = 'Test' , 
            BI_Line_of_Business__c = 'Test',
            TGS_Disputed_number_of_lines__c = 120,
            BI2_PER_Territorio_caso__c = 'Test',
            AccountId = acc.Id,
            BI_Country__c = Label.BI_Peru
        );
        insert cas2;

        cas2.RecordTypeId = map_rt.get('BI2_caso_comercial').Id;
        update cas2;
        Test.stopTest();

        Case toAssert = [SELECT EntitlementId FROM Case WHERE Id =: cas2.Id LIMIT 1];

        System.assertEquals(toAssert.EntitlementId, [SELECT Id FROM Entitlement WHERE Name = 'Test' LIMIT 1].Id);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Álvaro López
        Company:       Aborda
        Description:   Cover the new @future method "assignEntitlementsFuture" making the test asynchronous.
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        12/07/2016                      Álvaro López                Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void assignEntitlementsTEST3() {
        
        BI_TestUtils.throw_exception = false;
        
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);

        Map <String, RecordType> map_rt = new Map <String, RecordType>();
        for(RecordType rt : [SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName IN ('BI2_caso_comercial', 'BI2_Caso_Padre') AND sObjectType = 'Case']){
            map_rt.put(rt.DeveloperName, rt);
        }

        Account acc = [SELECT Id FROM Account WHERE Name = 'TestAccount_00'LIMIT 1];
        Contact con = [SELECT Id FROM Contact WHERE Name LIKE 'TestContact_0%' Limit 1];

        ///////////////Cambio de criterios para aumentar la cobertura///////////

        Test.startTest();
         //con solo limite superior
        BI_Criterio_de_asignacion__c cda = [SELECT Id FROM BI_Criterio_de_asignacion__c LIMIT 1];

        cda.BI2_PER_Limite_inferior__c = null;
        cda.BI2_PER_Limite_superior__c = 60;
        update cda;

        Case cas = new Case(
            RecordTypeId = map_rt.get('BI2_caso_comercial').Id,
            Reason = 'Test',
            BI2_PER_Tipologia__c = 'Test',
            BI_Product_Service__c = ' Test',
            BI2_per_subtipologia__c = 'Test' , 
            BI_Line_of_Business__c = 'Test',
            TGS_Disputed_number_of_lines__c = 120,
            BI2_PER_Territorio_caso__c = 'Test',
            ContactId = con.Id,
            AccountId = acc.Id,
            BI_Country__c = Label.BI_Peru
        );
        insert cas;

        cas.TGS_Disputed_number_of_lines__c = 40;
        update cas;
        Test.stopTest();
        Case toAssert = [SELECT EntitlementId FROM Case WHERE Id =: cas.Id LIMIT 1];
        System.assertEquals(toAssert.EntitlementId, [SELECT Id FROM Entitlement WHERE Name = 'Test' LIMIT 1].Id);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Álvaro López
        Company:       Aborda
        Description:   Cover the new @future method "assignEntitlementsFuture" making the test asynchronous.
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        12/07/2016                      Álvaro López                Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void assignEntitlementsTEST4() {
        
        BI_TestUtils.throw_exception = false;
        
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);

        Map <String, RecordType> map_rt = new Map <String, RecordType>();
        for(RecordType rt : [SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName IN ('BI2_caso_comercial', 'BI2_Caso_Padre') AND sObjectType = 'Case']){
            map_rt.put(rt.DeveloperName, rt);
        }

        Account acc = [SELECT Id FROM Account WHERE Name = 'TestAccount_00'LIMIT 1];
        Contact con = [SELECT Id FROM Contact WHERE Name LIKE 'TestContact_0%' Limit 1];

        Test.startTest();
        //sin limite superior ni inferior
        BI_Criterio_de_asignacion__c cda = [SELECT Id FROM BI_Criterio_de_asignacion__c LIMIT 1];

        cda.BI2_PER_Limite_inferior__c = null;
        cda.BI2_PER_Limite_superior__c = null;
        update cda;

         Case cas = new Case(
            RecordTypeId = map_rt.get('BI2_caso_comercial').Id,
            Reason = 'Test',
            BI2_PER_Tipologia__c = 'Test',
            BI_Product_Service__c = ' Test',
            BI2_per_subtipologia__c = 'Test' , 
            BI_Line_of_Business__c = 'Test',
            TGS_Disputed_number_of_lines__c = 120,
            BI2_PER_Territorio_caso__c = 'Test',
            ContactId = con.Id,
            AccountId = acc.Id,
            BI_Country__c = Label.BI_Peru
        );
        insert cas;

        cas.TGS_Disputed_number_of_lines__c = 30;
        update cas;
        Test.stopTest();

        Case toAssert = [SELECT EntitlementId FROM Case WHERE Id =: cas.Id LIMIT 1];
        System.assertEquals(toAssert.EntitlementId, [SELECT Id FROM Entitlement WHERE Name = 'Test' LIMIT 1].Id);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Álvaro López
        Company:       Aborda
        Description:   Cover the new @future method "assignEntitlementsFuture" making the test asynchronous.
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        12/07/2016                      Álvaro López                Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void assignEntitlementsTEST5() {
        
        BI_TestUtils.throw_exception = false;
        
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);

        Map <String, RecordType> map_rt = new Map <String, RecordType>();
        for(RecordType rt : [SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName IN ('BI2_caso_comercial', 'BI2_Caso_Padre') AND sObjectType = 'Case']){
            map_rt.put(rt.DeveloperName, rt);
        }

        Account acc = [SELECT Id FROM Account WHERE Name = 'TestAccount_00'LIMIT 1];
        Contact con = [SELECT Id FROM Contact WHERE Name LIKE 'TestContact_0%' Limit 1];

        //con limite superior e inferior
        Test.startTest();
        BI_Criterio_de_asignacion__c cda = [SELECT Id FROM BI_Criterio_de_asignacion__c LIMIT 1];
        
        cda.BI2_PER_Limite_inferior__c = 10;
        cda.BI2_PER_Limite_superior__c = 20;
        update cda;

         Case cas = new Case(
            RecordTypeId = map_rt.get('BI2_caso_comercial').Id,
            Reason = 'Test',
           BI2_PER_Tipologia__c = 'Test',
            BI_Product_Service__c = ' Test',
            BI2_per_subtipologia__c = 'Test' , 
            BI_Line_of_Business__c = 'Test',
            TGS_Disputed_number_of_lines__c = 120,
            BI2_PER_Territorio_caso__c = 'Test',
            ContactId = con.Id,
            AccountId = acc.Id,
            BI_Country__c = Label.BI_Peru
        );
        insert cas;

        cas.TGS_Disputed_number_of_lines__c = 15;
        update cas;
        Test.stopTest();

        Case toAssert = [SELECT EntitlementId FROM Case WHERE Id =: cas.Id LIMIT 1];
        System.assertEquals(toAssert.EntitlementId, [SELECT Id FROM Entitlement WHERE Name = 'Test' LIMIT 1].Id);
    }

}