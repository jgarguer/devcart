@isTest
private class COE_CaseMethods_TEST {

	static Id RT_EHELP = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'Caso_eHelp');
	static Id RT_ITSD  = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'BI_Caso_ITSD');
	static{
		BI_TestUtils.throw_exception = false;
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jaime Regidor
        Company:       Accenture
        Description:   Test Method that manage the code coverage of COE_CaseMethods.isCoEEntitlementCase
        
        IN:            
        OUT:           
        
        History:   
        <Date>                  <Author>                <Change Description>
        14/02/2017              Jaime Regidor         Iniitial Version.        
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void isCoEEntitlementCase_TEST() {
		//Con caso ehelp
		Contact contacto = new Contact(LastName='test', Email = 'test@test.com');
		insert contacto;
		Case ehelp = new Case(RecordTypeId = RT_EHELP, Contact = contacto, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = 'Nuevo');
		System.assertEquals(COE_CaseMethods.isCoEEntitlementCase(ehelp), true);

		//Con caso ITSD
		Contact contacto2 = new Contact(LastName='test2', Email = 'test2@test.com');
		insert contacto2;
		Case itsd = new Case(RecordTypeId = RT_ITSD, Contact = contacto, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = 'Nuevo');
		System.assertEquals(COE_CaseMethods.isCoEEntitlementCase(itsd), true);
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jaime Regidor
        Company:       Accenture
        Description:   Test Method that manage the code coverage of COE_CaseMethods.assignCoEEntitlements
        
        IN:            
        OUT:           
        
        History:   
        <Date>                  <Author>                <Change Description>
        14/02/2017              Jaime Regidor           Iniitial Version. 
        27/09/2017              Jaime Regidor           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c       
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void assignCoEEntitlements_TEST() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);

        Group cola = new Group(Type='Queue',DeveloperName='Test_GRP_N3_1', Name='TestN3');
        insert cola;
        BI_Proceso_de_asignacion__c proceso = new BI_Proceso_de_asignacion__c(Name = 'procesotest');
        insert proceso;
        BI_Criterio_de_asignacion__c criterio = new BI_Criterio_de_asignacion__c(BI_Country__c = 'eHelp', CoE_eHelp_Owner__c = 'BI_Nivel_1;BI_Nivel_2', BI_Proceso_de_asignacion__c = proceso.Id, CoE_eHelp_Type__c = Label.BI_CaseType_Question, CoE_eHelp_Priority__c = 'Low', CoE_eHelp_Request_type__c = null);
        insert criterio;
        Account cuenta = new Account(Name = 'cuenta', BI_Country__c = 'Chile', TGS_Region__c = 'América', BI_Tipo_de_Identificador_fiscal__c = 'RUT', BI_No_Identificador_fiscal__c = '69615880', BI_Segment__c = 'test', BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test'); // 28/09/2017
        insert cuenta;
        Entitlement entitlement = new Entitlement(Name = 'CoE Support', AccountId = cuenta.Id);
        insert entitlement;

        BI_MigrationHelper.disableBypass(mapa);

		Contact contacto2 = new Contact(LastName='test', Email = 'test@test.com');
		insert contacto2;
		Case ehelp = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = 'Nuevo', AccountId = cuenta.Id);
		insert ehelp;

		Test.startTest();

		ehelp.RecordTypeId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'Caso Interno');
		ehelp.Status = 'prueba';
		update ehelp;
		ehelp.RecordTypeId = RT_ITSD;		
		update ehelp;

		Case ehelp3 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Typing, AccountId = cuenta.Id);
		insert ehelp3;
		ehelp3.Status = Label.CoE_Analyzing;
		update ehelp3;

		
		Test.stopTest();

		Set<Id> set_cuentaIds = new Set<Id>();
		set_cuentaIds.add(cuenta.Id);
		COE_CaseMethods.getEntitlements(set_cuentaIds);

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jaime Regidor
        Company:       Accenture
        Description:   Test Method that manage the code coverage of COE_CaseMethods.assignCoEEntitlements
        
        IN:            
        OUT:           
        
        History:   
        <Date>                  <Author>                <Change Description>
        14/02/2017              Jaime Regidor           Iniitial Version.
        27/09/2017              Jaime Regidor           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c        
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void assignCoEEntitlements_TEST2() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);

		Contact contacto2 = new Contact(LastName='test', Email = 'test@test.com');
		insert contacto2;

		Account cuenta = new Account(Name = 'cuenta', BI_Country__c = 'Chile', TGS_Region__c = 'América', BI_Tipo_de_Identificador_fiscal__c = 'RUT', BI_No_Identificador_fiscal__c = '69615880', BI_Segment__c = 'test', BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test'); // 28/09/2017
        insert cuenta;

        BI_MigrationHelper.disableBypass(mapa);

        Case ehelp2 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.Coe_New, AccountId = cuenta.Id);
		insert ehelp2;

		Test.startTest();
		ehelp2.Status = Label.CoE_Analyzing;
		update ehelp2;		
		Case ehelp4 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Analyzing, AccountId = cuenta.Id);
		insert ehelp4;
		ehelp4.Status = Label.CoE_Being_processed;
		update ehelp4;
		Test.stopTest();
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jaime Regidor
        Company:       Accenture
        Description:   Test Method that manage the code coverage of COE_CaseMethods.assignCoEEntitlements
        
        IN:            
        OUT:           
        
        History:   
        <Date>                  <Author>                <Change Description>
        14/02/2017              Jaime Regidor           Iniitial Version.   
        27/09/2017              Jaime Regidor           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void assignCoEEntitlements_TEST3() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);

		Contact contacto2 = new Contact(LastName='test', Email = 'test@test.com');
		insert contacto2;

		Account cuenta = new Account(Name = 'cuenta', BI_Country__c = 'Chile', TGS_Region__c = 'América', BI_Tipo_de_Identificador_fiscal__c = 'RUT', BI_No_Identificador_fiscal__c = '69615880', BI_Segment__c = 'test', BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test'); // 28/09/2017
        insert cuenta;

        BI_MigrationHelper.disableBypass(mapa);

		Case ehelp5 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Being_processed, AccountId = cuenta.Id);
		insert ehelp5;

		Test.startTest();
		ehelp5.Status = Label.CoE_Information_pending;
		update ehelp5;		
		Case ehelp6 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Confirmation_pending, AccountId = cuenta.Id);
		insert ehelp6;
		ehelp6.Status = Label.CoE_Closed;
		update ehelp6;
		Test.stopTest();
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jaime Regidor
        Company:       Accenture
        Description:   Test Method that manage the code coverage of COE_CaseMethods.assignCoEEntitlements
        
        IN:            
        OUT:           
        
        History:   
        <Date>                  <Author>                <Change Description>
        14/02/2017              Jaime Regidor           Iniitial Version. 
        27/09/2017              Jaime Regidor           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c       
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void assignCoEEntitlements_TEST4() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);

		Contact contacto2 = new Contact(LastName='test', Email = 'test@test.com');
		insert contacto2;

		Account cuenta = new Account(Name = 'cuenta', BI_Country__c = 'Chile', TGS_Region__c = 'América', BI_Tipo_de_Identificador_fiscal__c = 'RUT', BI_No_Identificador_fiscal__c = '69615880', BI_Segment__c = 'test', BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test'); // 28/09/2017
        insert cuenta;

        BI_MigrationHelper.disableBypass(mapa);

		Case ehelp7 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Reopened, AccountId = cuenta.Id);
		insert ehelp7;

		Test.startTest();
		ehelp7.Status = Label.CoE_Analyzing;
		update ehelp7;
		Case ehelp8 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Production_pending, AccountId = cuenta.Id);
		insert ehelp8;
		ehelp8.Status = Label.CoE_Closed;
		update ehelp8;
		Test.stopTest();
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jaime Regidor
        Company:       Accenture
        Description:   Test Method that manage the code coverage of COE_CaseMethods.assignCoEEntitlements
        
        IN:            
        OUT:           
        
        History:   
        <Date>                  <Author>                <Change Description>
        14/02/2017              Jaime Regidor           Iniitial Version. 
        27/09/2017              Jaime Regidor           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c       
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void assignCoEEntitlements_TEST5() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);

		Contact contacto2 = new Contact(LastName='test', Email = 'test@test.com');
		insert contacto2;

		Account cuenta = new Account(Name = 'cuenta', BI_Country__c = 'Chile', TGS_Region__c = 'América', BI_Tipo_de_Identificador_fiscal__c = 'RUT', BI_No_Identificador_fiscal__c = '69615880', BI_Segment__c = 'test', BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test'); // 28/09/2017
        insert cuenta;

        BI_MigrationHelper.disableBypass(mapa);

		Case ehelp7 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Reopened_Closed, AccountId = cuenta.Id);
		insert ehelp7;

		Test.startTest();
		ehelp7.Status = Label.CoE_Closed;
		update ehelp7;
		Case ehelp8 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Closed, AccountId = cuenta.Id);
		insert ehelp8;
		ehelp8.Status = Label.CoE_Reopened_Closed;
		update ehelp8;
		Test.stopTest();
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jaime Regidor
        Company:       Accenture
        Description:   Test Method that manage the code coverage of COE_CaseMethods.assignCoEEntitlements
        
        IN:            
        OUT:           
        
        History:   
        <Date>                  <Author>                <Change Description>
        14/02/2017              Jaime Regidor           Iniitial Version.
        27/09/2017              Jaime Regidor           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c        
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void assignCoEEntitlements_TEST6() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);

		Contact contacto2 = new Contact(LastName='test', Email = 'test@test.com');
		insert contacto2;

		Account cuenta = new Account(Name = 'cuenta', BI_Country__c = 'Chile', TGS_Region__c = 'América', BI_Tipo_de_Identificador_fiscal__c = 'RUT', BI_No_Identificador_fiscal__c = '69615880', BI_Segment__c = 'test', BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test'); // 28/09/2017
        insert cuenta;

        BI_MigrationHelper.disableBypass(mapa);

		Case ehelp7 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Information_pending, AccountId = cuenta.Id, CoE_Stop_Milestone1__c = true, CoE_Stop_Milestone2__c = false);
		insert ehelp7;

		Test.startTest();
		ehelp7.Status = Label.CoE_Analyzing;
		update ehelp7;
		Case ehelp8 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Information_pending, AccountId = cuenta.Id, CoE_Stop_Milestone1__c = true, CoE_Stop_Milestone2__c = true);
		insert ehelp8;
		ehelp8.Status = Label.CoE_Being_processed;
		update ehelp8;
		Test.stopTest();
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jaime Regidor
        Company:       Accenture
        Description:   Test Method that manage the code coverage of COE_CaseMethods.progressCaseMilestones
        
        IN:            
        OUT:           
        
        History:   
        <Date>                  <Author>                <Change Description>
        14/02/2017              Jaime Regidor           Iniitial Version.   
        27/09/2017              Jaime Regidor           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c
        10/10/2017              Gawron, Julian          Added BI_MigrationHelper.skipAllTriggers to avoid limits              
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void progressCaseMilestones_TEST() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_MigrationHelper.skipAllTriggers();
        Account cuenta = new Account(Name = 'cuenta', BI_Country__c = 'Chile', TGS_Region__c = 'América', BI_Tipo_de_Identificador_fiscal__c = 'RUT', BI_No_Identificador_fiscal__c = '69615880', BI_Segment__c = 'test', BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test'); // 28/09/2017
        insert cuenta;

        Entitlement entitlement = new Entitlement(Name = 'CoE Support', AccountId = cuenta.Id);
        insert entitlement;

        Contact contacto2 = new Contact(LastName='test', Email = 'test@test.com');
		insert contacto2;
		BI_MigrationHelper.cleanSkippedTriggers();
		
		Case ehelp = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Information_pending, AccountId = cuenta.Id, CoE_Stop_Milestone1__c = false, CoE_Stop_Milestone2__c = false, EntitlementId = entitlement.Id);
		insert eHelp;
		eHelp.CoE_Stop_Milestone1__c = true;
		update eHelp;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jaime Regidor
        Company:       Accenture
        Description:   Test Method that manage the code coverage of COE_CaseMethods.progressCaseMilestones
        
        IN:            
        OUT:           
        
        History:   
        <Date>                  <Author>                <Change Description>
        14/02/2017              Jaime Regidor           Iniitial Version. 
        27/09/2017              Jaime Regidor           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c       
        10/10/2017              Gawron, Julian          Added BI_MigrationHelper.skipAllTriggers to avoid limits  
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void progressCaseMilestones_TEST2() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_MigrationHelper.skipAllTriggers();
        Account cuenta = new Account(Name = 'cuenta', BI_Country__c = 'Chile', TGS_Region__c = 'América', BI_Tipo_de_Identificador_fiscal__c = 'RUT', BI_No_Identificador_fiscal__c = '69615880', BI_Segment__c = 'test', BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test'); // 28/09/2017
        insert cuenta;

        Entitlement entitlement = new Entitlement(Name = 'CoE Support', AccountId = cuenta.Id);
        insert entitlement;

        Contact contacto2 = new Contact(LastName='test', Email = 'test@test.com');
		insert contacto2;

		BI_MigrationHelper.cleanSkippedTriggers();
		Case ehelp2 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Information_pending, AccountId = cuenta.Id, CoE_Stop_Milestone1__c = false, CoE_Stop_Milestone2__c = false, EntitlementId = entitlement.Id);
		insert eHelp2;
		eHelp2.CoE_Stop_Milestone2__c = true;
		update eHelp2;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jaime Regidor
        Company:       Accenture
        Description:   Test Method that manage the code coverage of COE_CaseMethods.progressCaseMilestones
        
        IN:            
        OUT:           
        
        History:   
        <Date>                  <Author>                <Change Description>
        14/02/2017              Jaime Regidor           Iniitial Version.  
        27/09/2017              Jaime Regidor           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c
        05/10/2017              Guillermo Muñoz         Added BI_MigrationHelper.skipAllTriggers to avoid limits    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void progressCaseMilestones_TEST3() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_MigrationHelper.skipAllTriggers();

        Account cuenta = new Account(Name = 'cuenta', BI_Country__c = 'Chile', TGS_Region__c = 'América', BI_Tipo_de_Identificador_fiscal__c = 'RUT', BI_No_Identificador_fiscal__c = '69615880', BI_Segment__c = 'test', BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test'); // 28/09/2017
        insert cuenta;

        SlaProcess sla = [SELECT Id FROM SlaProcess WHERE isActive = true LIMIT 1];
    
        Entitlement ent = new Entitlement(
            Name = 'Test',
            AccountId = cuenta.Id,
            SlaProcessId = sla.Id,
            StartDate = Date.today()
        );
        insert ent;

        Contact contacto2 = new Contact(LastName='test', Email = 'test@test.com');
		insert contacto2;

		BI_MigrationHelper.cleanSkippedTriggers();
		
        Case ehelp3 = new Case(RecordTypeId = RT_EHELP, Contact = contacto2, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = Label.CoE_Information_pending, AccountId = cuenta.Id, CoE_Stop_Milestone1__c = false, CoE_Stop_Milestone3__c = false, EntitlementId = ent.Id);
		insert eHelp3;

		System.debug('milestone :' + [SELECT Id FROM CaseMilestone WHERE CaseId = :eHelp3.Id]);
		
		Test.startTest();
		eHelp3.CoE_Stop_Milestone3__c = true;
		eHelp3.Status = 'Closed';
		update eHelp3;
		Test.stopTest();
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jaime Regidor
        Company:       Accenture
        Description:   Test Method that manage the code coverage of COE_CaseMethods.validateStatusChange
        
        IN:            
        OUT:           
        
        History:   
        <Date>                  <Author>                <Change Description>
        14/02/2017              Jaime Regidor         Iniitial Version.        
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void validateStatusChange_TEST() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		Account cuenta = new Account(Name = 'cuenta', BI_Country__c = 'Chile', TGS_Region__c = 'América', BI_Tipo_de_Identificador_fiscal__c = 'RUT', BI_No_Identificador_fiscal__c = '69615880');
        insert cuenta;
		Case ehelp = new Case(RecordTypeId = RT_EHELP, Type = Label.BI_CaseType_Question, Priority = 'Low', Status = 'prueba', AccountId = cuenta.Id);
		insert ehelp;
		System.debug('inserto ehelp');
		eHelp.Status = 'Cancelled';
		update ehelp;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Geraldine Montes 
        Company:       Accenture
        Description:   Test Method that manage the code coverage of COE_CaseMethods.RegularizarDatosMS
        
        IN:            
        OUT:           
        
        History:   
        <Date>                  <Author>                <Change Description>
        06/12/2017             Geraldine Montes           Iniitial Version.        
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void RegularizarDatosMS_TEST() {
        
        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, true, true);
        
        Account objAccount = new Account();
        objAccount.Name = 'Account Test';
        objAccount.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'TGS_Legal_Entity' LIMIT 1].Id;
        objAccount.BI_Country__c = 'Colombia';
        objAccount.BI_Tipo_de_identificador_fiscal__c = 'CC';
        objAccount.BI_No_Identificador_fiscal__c = '1049610678';
        insert objAccount;

        Opportunity objOpp = new Opportunity();
        objOpp.Name = 'Opportunity Test';
        objOpp.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'BI_Ciclo_completo' LIMIT 1].Id;
        objOpp.StageName = 'F5 - Solution Definition';
        objOpp.CloseDate = System.today().addDays(1);
        objOpp.BI_Country__c = 'Colombia';
        objOpp.BI_Opportunity_Type__c = 'Digital';
        objOpp.BI_Duracion_del_contrato_Meses__c = 3;
        objOpp.BI_Plazo_estimado_de_provision_dias__c = 12;
        insert objOpp;

        NE__Order__c objOR = new NE__Order__c();
        objOR.NE__OptyId__c = objOpp.Id;
        insert objOR;

        NE__Order__c objORDummy = new NE__Order__c();
        objORDummy.NE__OptyId__c = objOpp.Id;
        insert objORDummy;

        //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 'Catalogo padre';
        producto.TGS_CWP_Tier_2__c = 'Catalogo hijo';
        insert producto;

        NE__Catalog_Category__c testCatalogCategory = new NE__Catalog_Category__c();
        testCatalogCategory.Name = 'Test Category';
        testCatalogCategory.NE__CatalogId__c = catalogo.Id;      
        insert testCatalogCategory;

        NE__Catalog_Category__c testCatalogSubCategory = new NE__Catalog_Category__c();
        testCatalogSubCategory.Name = 'Test SubCategory';
        testCatalogSubCategory.NE__CatalogId__c = catalogo.Id;
        testCatalogSubCategory.NE__Parent_Category_Name__c = testCatalogCategory.Id;        
    	insert testCatalogSubCategory;

        NE__Catalog_Item__c ParentCatalogItem = new NE__Catalog_Item__c();
        ParentCatalogItem.NE__Type__c = 'Root';
        ParentCatalogItem.NE__Catalog_Category_Name__c = testCatalogSubCategory.Id;
        ParentCatalogItem.NE__Catalog_Id__c = catalogo.Id;
        ParentCatalogItem.NE__ProductId__c = producto.Id;
        insert ParentCatalogItem;

        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Product', catalogCategoryChildren.id, catalogo.id, producto.id);
        catalogoItem.NE__Parent_Catalog_Item__c = ParentCatalogItem.Id;
        insert catalogoItem; 

        list<NE__OrderItem__c> lstOI = new list<NE__OrderItem__c>();
        NE__OrderItem__c objOI = new NE__OrderItem__c();
        objOI.NE__OrderId__c = objOR.Id;
        objOI.NE__Account__c = objAccount.Id;
        objOI.NE__Qty__c = 1;
        objOI.BI_COL_ModificacionServicio__c = null;
        objOI.NE__Root_Order_Item__c = null;
        objOI.NE__CatalogItem__c = catalogoItem.id;
        objOI.NE__ProdId__c = producto.id;
        objOI.NE__Billing_Account__c = objAccount.Id;
        objOI.NE__One_Time_Cost__c = 0;
        objOI.NE__OneTimeFeeOv__c = 0;
        objOI.NE__RecurringChargeOv__c = 0;
        objOI.NE__RecurringChargeFrequency__c = 'Monthly';
        objOI.NE__Service_Account__c = objAccount.Id;
        objOI.NE__Status__c = 'Pending';
        objOI.NE__SmartPartNumber__c = 'P - 188956';
        objOI.Configuration_SubType__c = 'Standard';
        objOI.NE__BaseOneTimeFee__c = 0;
        objOI.NE__BaseRecurringCharge__c = 0;
        objOI.NE__Parent_Order_Item__c = null; 
        objOI.CurrencyIsoCode = 'COP';
        objOI.NE__Action__c = 'Add';
        objOI.NE__Catalog__c = catalogo.Id;
        lstOI.add(objOI);
        insert lstOI;

        list<NE__Order_Item_Attribute__c> lstOIA = new list<NE__Order_Item_Attribute__c>();
        NE__Order_Item_Attribute__c objOIAttribute = new NE__Order_Item_Attribute__c();
        objOIAttribute.NE__Order_Item__c = lstOI[0].Id;
        objOIAttribute.Name = 'Test';
        objOIAttribute.NE__Value__c = 'Test';
        lstOIA.add(objOIAttribute);
        

        NE__Order_Item_Attribute__c objOIAttribute1 = new NE__Order_Item_Attribute__c();
        objOIAttribute1.NE__Order_Item__c = lstOI[0].Id;
        objOIAttribute1.Name = 'Test';
        objOIAttribute1.NE__Value__c = 'Test';
        lstOIA.add(objOIAttribute1);
        insert lstOIA;

        NE__OrderItem__c objOINew = lstOI[0].clone(false, true, false, false);
        insert objOINew;
		

        BI_COL_Descripcion_de_servicio__c objDS = new BI_COL_Descripcion_de_servicio__c();
        objDS.BI_COL_Oportunidad__c = objOpp.Id;
        objDS.BI_COL_Producto_Telefonica__c = lstOI[0].Id;
        insert objDS;

        list<BI_COL_Modificacion_de_Servicio__c> lstMS = new list<BI_COL_Modificacion_de_Servicio__c>();
        BI_COL_Modificacion_de_Servicio__c objMS = new BI_COL_Modificacion_de_Servicio__c();
        objMS.BI_COL_Producto__c = lstOI[0].Id;
        objMS.BI_COL_Oportunidad__c = objOpp.Id;
        objMS.BI_COL_Codigo_unico_servicio__c = objDS.Id;
     	objMS.BI_COL_Cargo_fijo_mes__c = 10;
     	objMS.BI_COL_Cargo_conexion__c  = 10;
        lstMS.add(objMS);
        BI_COL_Modificacion_de_Servicio__c objMS1 = new BI_COL_Modificacion_de_Servicio__c();
        objMS1.BI_COL_Producto__c = lstOI[0].Id;
        objMS1.BI_COL_Oportunidad__c = objOpp.Id;
        objMS1.BI_COL_Codigo_unico_servicio__c = objDS.Id;
     	objMS1.BI_COL_Cargo_fijo_mes__c = 20;
     	objMS1.BI_COL_Cargo_conexion__c  = 20;
       	lstMS.add(objMS1);
       	BI_COL_Modificacion_de_Servicio__c objMS2 = new BI_COL_Modificacion_de_Servicio__c();
        objMS2.BI_COL_Producto__c = lstOI[0].Id;
        objMS2.BI_COL_Oportunidad__c = objOpp.Id;
        objMS2.BI_COL_Codigo_unico_servicio__c = objDS.Id;
     	objMS2.BI_COL_Cargo_fijo_mes__c = 30;
     	objMS2.BI_COL_Cargo_conexion__c  = 30;
       	lstMS.add(objMS2);
       	BI_COL_Modificacion_de_Servicio__c objMS3 = new BI_COL_Modificacion_de_Servicio__c();
        objMS3.BI_COL_Producto__c = lstOI[0].Id;
        objMS3.BI_COL_Oportunidad__c = objOpp.Id;
        objMS3.BI_COL_Codigo_unico_servicio__c = objDS.Id;
     	objMS3.BI_COL_Cargo_fijo_mes__c = 40;
     	objMS3.BI_COL_Cargo_conexion__c  = 40;
       	lstMS.add(objMS3);
        insert lstMS;
        BI_COL_Modificacion_de_Servicio__c objMSNew;
        for(Integer i = 0; i < lstMS.size(); i++)
        {
        	objMSNew = lstMS[i].clone(false, true, false, false);
        }
        insert objMSNew;
        NE__Product__c objProduct = new NE__Product__c();
        objProduct.Name = 'Producto de prueba';
        objProduct.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'NE__Product__c' AND DeveloperName = 'Standard' LIMIT 1].Id;
        insert objProduct;

        list<NE__DocumentationCharacterist__c> lstDC = new list<NE__DocumentationCharacterist__c>();
        NE__DocumentationCharacterist__c objDC = new NE__DocumentationCharacterist__c();
        objDC.NE__Product__c = objProduct.Id;
        objDC.Name = lstOI[0].Id;
        objDC.NE__Value__c = ' ';
        objDC.NE__Visible__c = true;
        lstDC.add(objDC);

        NE__DocumentationCharacterist__c objDC1 = new NE__DocumentationCharacterist__c();
        objDC1.NE__Product__c = objProduct.Id;
        objDC1.Name = lstOI[0].Id;
        objDC1.NE__Value__c = 'EXITO 1';
        objDC1.NE__Visible__c = false;
        lstDC.add(objDC1);
        insert lstDC;

        COE_CaseMethods.RegularizarDatosMS objClass = new COE_CaseMethods.RegularizarDatosMS();
        objClass.MAX_RECORDS_FAT = 10;
        objClass.MAX_RECORDS_CHI = 10;
        objClass.ID_PRODUCTO_DUMMY = objProduct.Id;
        objClass.map_Id_OIUpdate = new Map<String,NE__DocumentationCharacterist__c>();
        objClass.map_Id_OIUpdate.put(lstDC[0].name, lstDC[0]);

        Test.startTest();
        Database.SaveResult[] results = Database.insert(lstOI, false);
        System.enqueueJob(objClass);        
        Test.stopTest();

    }
	
}