/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Clonación de información de cartera de clientes.

History:
<Date>							<Author>						<Change Description>
10/04/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class FS_CORE_Fullstack_ATM {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------*
    Author:					Eduardo Ventura
	Company:				Everis España
	Description:			Sincronización entre objetos AccountTeamMember.
	
	History:
	
	<Date>					<Author>					<Description>
	22/03/2017				Eduardo Ventura				Versión inicial.
	27/03/2017				Eduardo Ventura				Filtrado de funciones que han de sincronizar. [FS_CORE 001]
	29/03/2017				Eduardo Ventura				Integración de usuarios asignados por layout. [FS_CORE 002]
	31/03/2017				Eduardo Ventura				Duplicación de registros para ejecución de invocaciones solicitadas por Amdocs. [FS_CORE 003]
	---------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void process (Account account){
        
        System.debug('\n\n\n\t\t\t\t\t IN ATM Process con '+ account+'\n\n\n');
        /* Variable Definition */
        List <AccountTeamMember> accountTeamMember_default = [SELECT Id, UserId, tolabel(TeamMemberRole), AccountId FROM AccountTeamMember WHERE AccountId =: account.Id];
        List <FS_CORE_TeamMemberToSincronize__c> accountTeamMember_custom = [SELECT Id, FS_CORE_SalesforceId__c, FS_CORE_IdUser__c, FS_CORE_FuncionUser__c, FS_CORE_TipoRegistro__c, FS_CORE_IdAccount__c FROM FS_CORE_TeamMemberToSincronize__c WHERE FS_CORE_IdAccount__c =: account.Id];
        
        /* Configuración personalizada */
        FS_CORE_Integracion__c ORGConfiguration = FS_CORE_Integracion__c.getInstance();
        
        /* Process Salesforce Identification */
        Set<String> records_default = new Set<String>();
        Set<String> records_custom = new Set<String>();
        	/* Aux */
        	Map <String, FS_CORE_TeamMemberToSincronize__c> accountTeamMembersAux = new  Map <String, FS_CORE_TeamMemberToSincronize__c>();
        
        for(AccountTeamMember item : accountTeamMember_default){records_default.add(item.Id);}
        for(FS_CORE_TeamMemberToSincronize__c item : accountTeamMember_custom){records_custom.add(item.FS_CORE_SalesforceId__c); accountTeamMembersAux.put(item.FS_CORE_SalesforceId__c, item);}
        
        /* Stage #1: Default > Custom */
        FS_CORE_TeamMemberToSincronize__c element;
        List <FS_CORE_TeamMemberToSincronize__c> toUpsert = new List <FS_CORE_TeamMemberToSincronize__c>();
        for(AccountTeamMember item : accountTeamMember_default){
            /* INI FS_CORE 001 */ if(!records_custom.contains(item.Id) /* Filters -> */&& ORGConfiguration.FS_CORE_AccountTeamMember__c.contains(item.TeamMemberRole)){ /* FIN FS_CORE 001 */
                element = new FS_CORE_TeamMemberToSincronize__c();
                
                element.FS_CORE_SalesforceId__c = item.Id;
                element.FS_CORE_IdUser__c = item.UserId;
                element.FS_CORE_FuncionUser__c = item.TeamMemberRole;
                element.FS_CORE_IdAccount__c = item.AccountId;
                element.FS_CORE_TipoRegistro__c = 'Actual';
                toUpsert.add(element);
            } else if(records_custom.contains(item.Id) && item.TeamMemberRole != accountTeamMembersAux.get(item.Id).FS_CORE_FuncionUser__c){
                element = accountTeamMembersAux.get(item.Id);
                
                /* INI FS_CORE 003 */
                element = accountTeamMembersAux.get(item.Id);
                if(element.FS_CORE_TipoRegistro__c == 'Sincronizado') toUpsert.add(new FS_CORE_TeamMemberToSincronize__c(FS_CORE_SalesforceId__c = element.Id, FS_CORE_IdUser__c = element.FS_CORE_IdUser__c, FS_CORE_FuncionUser__c = element.FS_CORE_FuncionUser__c, FS_CORE_IdAccount__c = element.FS_CORE_IdAccount__c, FS_CORE_TipoRegistro__c = 'Anterior'));
                /* FIN FS_CORE 003 */
                
                element.FS_CORE_FuncionUser__c = item.TeamMemberRole;
                element.FS_CORE_TipoRegistro__c = 'Actual';
                toUpsert.add(element);
            }
        }
        
        /* Stage #2: Custom > Default */
        List <FS_CORE_TeamMemberToSincronize__c> toDelete = new List <FS_CORE_TeamMemberToSincronize__c>();
        List <FS_CORE_TeamMemberToSincronize__c> toUpdate = new List <FS_CORE_TeamMemberToSincronize__c>();
        for(FS_CORE_TeamMemberToSincronize__c item : accountTeamMember_custom){
            if(item.FS_CORE_SalesforceId__c != 'owner' && item.FS_CORE_SalesforceId__c != 'asesor' && item.FS_CORE_SalesforceId__c != 'service manager'){
                System.debug('>>> '+records_default.contains(item.FS_CORE_SalesforceId__c)+' '+item.FS_CORE_TipoRegistro__c);
                if(!records_default.contains(item.FS_CORE_SalesforceId__c) && item.FS_CORE_TipoRegistro__c == 'Sincronizado'){
                    item.FS_CORE_TipoRegistro__c = 'Anterior';
                    toUpdate.add(item);
                }else if (!records_default.contains(item.FS_CORE_SalesforceId__c) && item.FS_CORE_TipoRegistro__c == 'Actual') toDelete.add(item);
            }
        }
        
        /* Stage #3: Layout Fields <> Custom */
            /* INSERT */ if(!records_custom.contains('owner')){element = new FS_CORE_TeamMemberToSincronize__c(FS_CORE_SalesforceId__c = 'owner', FS_CORE_IdUser__c = account.OwnerId, FS_CORE_FuncionUser__c = 'Propietario del Cliente', FS_CORE_IdAccount__c = account.Id, FS_CORE_TipoRegistro__c = 'Actual'); toUpsert.add(element);}
        	/* UPDATE */ else if(accountTeamMembersAux.get('owner').FS_CORE_IdUser__c != account.OwnerId){element = accountTeamMembersAux.get('owner'); /* INI FS_CORE 003 */ if(element.FS_CORE_TipoRegistro__c == 'Sincronizado') toUpsert.add(new FS_CORE_TeamMemberToSincronize__c(FS_CORE_SalesforceId__c = element.Id, FS_CORE_IdUser__c = element.FS_CORE_IdUser__c, FS_CORE_FuncionUser__c = element.FS_CORE_FuncionUser__c, FS_CORE_IdAccount__c = element.FS_CORE_IdAccount__c, FS_CORE_TipoRegistro__c = 'Anterior')); /* FIN FS_CORE 003 */ element.FS_CORE_IdUser__c = account.OwnerId; element.FS_CORE_TipoRegistro__c = 'Actual'; toUpdate.add(element);}
        
            /* INSERT */ if(!records_custom.contains('asesor') && !String.isEmpty(account.BI2_asesor__c)){element = new FS_CORE_TeamMemberToSincronize__c(FS_CORE_SalesforceId__c = 'asesor', FS_CORE_IdUser__c = account.BI2_asesor__c, FS_CORE_FuncionUser__c = 'Asesor', FS_CORE_IdAccount__c = account.Id, FS_CORE_TipoRegistro__c = 'Actual'); toUpsert.add(element);}
        	/* UPDATE */ else if(!String.isEmpty(account.BI2_asesor__c)){if(accountTeamMembersAux.get('asesor').FS_CORE_IdUser__c != account.BI2_asesor__c){element = accountTeamMembersAux.get('asesor'); /* INI FS_CORE 003 */ if(element.FS_CORE_TipoRegistro__c == 'Sincronizado') toUpsert.add(new FS_CORE_TeamMemberToSincronize__c(FS_CORE_SalesforceId__c = element.Id, FS_CORE_IdUser__c = element.FS_CORE_IdUser__c, FS_CORE_FuncionUser__c = element.FS_CORE_FuncionUser__c, FS_CORE_IdAccount__c = element.FS_CORE_IdAccount__c, FS_CORE_TipoRegistro__c = 'Anterior')); /* FIN FS_CORE 003 */ element.FS_CORE_IdUser__c = account.BI2_asesor__c; element.FS_CORE_TipoRegistro__c = 'Actual'; toUpdate.add(element);}}
        	/* DELETE */ else try{element = accountTeamMembersAux.get('asesor'); if(accountTeamMembersAux.get('asesor').FS_CORE_TipoRegistro__c == 'Sincronizado'){element.FS_CORE_TipoRegistro__c = 'Anterior'; toUpdate.add(element);}else toDelete.add(element);}catch(Exception exc){/*El registro no existe*/}
        
        	/* INSERT */ if(!records_custom.contains('service manager') && !String.isEmpty(account.BI2_service_manager__c)){element = new FS_CORE_TeamMemberToSincronize__c(FS_CORE_SalesforceId__c = 'service manager', FS_CORE_IdUser__c = account.BI2_service_manager__c, FS_CORE_FuncionUser__c = 'Service Manager', FS_CORE_IdAccount__c = account.Id, FS_CORE_TipoRegistro__c = 'Actual'); toUpsert.add(element);}
        	/* UPDATE */ else if(!String.isEmpty(account.BI2_service_manager__c)){if(accountTeamMembersAux.get('service manager').FS_CORE_IdUser__c != account.BI2_service_manager__c){element = accountTeamMembersAux.get('service manager'); /* INI FS_CORE 003 */element = accountTeamMembersAux.get('service manager'); if(element.FS_CORE_TipoRegistro__c == 'Sincronizado') toUpsert.add(new FS_CORE_TeamMemberToSincronize__c(FS_CORE_SalesforceId__c = element.Id, FS_CORE_IdUser__c = element.FS_CORE_IdUser__c, FS_CORE_FuncionUser__c = element.FS_CORE_FuncionUser__c, FS_CORE_IdAccount__c = element.FS_CORE_IdAccount__c, FS_CORE_TipoRegistro__c = 'Anterior')); /* FIN FS_CORE 003 */ element.FS_CORE_IdUser__c = account.BI2_service_manager__c; element.FS_CORE_TipoRegistro__c = 'Actual'; toUpdate.add(element);}}
        	/* DELETE */ else try{element = accountTeamMembersAux.get('service manager'); if(accountTeamMembersAux.get('service manager').FS_CORE_TipoRegistro__c == 'Sincronizado'){element.FS_CORE_TipoRegistro__c = 'Anterior'; toUpdate.add(element);}else toDelete.add(element);}catch(Exception exc){/*El registro no existe*/}
       
        /* Database submit */
                     	
       
        toUpsert.addAll(toUpdate);
        
        if(!toUpsert.isEmpty()) upsert toUpsert;
        if(!toDelete.isEmpty()) delete toDelete;
        //if(!toUpdate.isEmpty()) update toUpdate;
        
    }
}