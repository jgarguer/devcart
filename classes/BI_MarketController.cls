public with sharing class BI_MarketController
{
    public NE.NewConfigurationController            newConf;
    /*public String                                   contextCategoryId                {get;set;}  //It's the Id of the category in context*/
    public String                                   promoIdContext                  {get;set;}  //It sets the id of the promotion, when displayed in detail
    public String                                   itemIdContext                   {get;set;}  //It sets the id of the item, when displayed in detail  
    public List<NE__Catalog_Category__c>            listOfCatCategories             {get;set;}  //List of the catalog categories children of the context category
    public Map<String,NE.Promotion>                 mapOfCatalogPromo               {get;set;}  //Map of the catalog's promotions visible in the page
    public Map<String,NE.PromotionItem>             mapOfCatalogPromoItems          {get;set;}  //Map of the promotion's promotion items visible in the page
    //public Map<String,List<NE.Item>>                mapOfCategoryItems              {get;set;}  //Map of all the items visible in the page for a single category
    public List<NE.Item>                            listOfItems                     {get;set;}  //MF 1.0 - Used in the page in order to get the number of the items of the selected category
    //public Map<String,NE__Catalog_Item__c>          mapOfCatalogItems               {get;set;}  //Map of all the catalog items for the selected catalog //Fa esplodere il view state e messa transient non prende le info dei prodotti
    public String                                   wannaSelectCatalog              {get;set;}  //If it's set to true, it allows to select a catalog (true is the default value, when a new category is selected, this value is setted to false)
    public NE__Catalog_Category__c                  selectedCatCategory             {get;set;}  //The selected category
    public Boolean showCompareMarket               {get;set;}  //If it's set to true, it allows to render the CompareMarket page
    /* ENEL */
    public Account                                  contextAccount                  {get;set;}  //The customer
    public Contact                                  contextContact                  {get;set;}  //The contact related to the customer business
    public NE__Order__c                             temporaryOrder                  {get;set;}  //Order to display account data
    public List<NE__DocumentationCharacterist__c>   listOfCharacteristics           {get;set;}  //The Charactestics of the context product
    public String                                   commProdContext                 {get;set;}  //It sets the id of the commercial product, when displayed in detail
    public NE__Product__c                           contextCommercialProduct        {get;set;}  //The commercial product detail
    public String                                   ordDate                         {get;set;}  //String that contains the last modified date of the context order
    
    public Boolean                                  automaticCheckOut               {get;set;} //if the catalog is empty then the value is true
    public String                                   redirectFromSummary             {get;set;}
    /* MF 2015-07-15 */ 
    public Boolean                                  isMobileAgent                   {get;set;}  //It allows to show the redirect to home icon only if the profile of the context user is EE Mobile
    public String                                   catName                         {get;set;}
    public String                                   paramsess                       {get;set;}
    

    public String                                   marketCompareUrl
    {                                                                                           //String needed to redirect the compare page into the iframe included in the B2W_Market.page
        get
        {      
            String marketUrlString    =    newConf.compareUrl;
            system.debug('marketUrlString: '+marketUrlString);
            String toReturn = marketUrlString.replace('compare','BI_CompareMobile');
            toReturn = toReturn.replace('ne__','');
            return toReturn;
        }
        set; 
    }
    
    public Boolean                               listViewVariable                {get;set;}   //Change catalog view, true if is list, false if is carousel

    public Integer numberOfCompare  {get;set;}
    

  
    /*** CONSTRUCTOR ***/ 
    public BI_MarketController(NE.NewConfigurationController ncc)
    {
        try
        {
            newConf                 = ncc;
            itemIdContext           = '';
            promoIdContext          = '';
            wannaSelectCatalog      = 'false';
            showCompareMarket       = false;
            contextAccount          = new Account();
            contextContact          = new Contact();
            ordDate                 = '';
            automaticCheckOut       = false;
            numberOfCompare         =   0;
            //newConf.activateLiteCatalog=true;
            
            
            //ListView
            
            
            system.debug('**MF** constructor1: '+newConf.catitemsmap.size());
            system.debug('**MF** constructor1: '+newConf.headersList);
            system.debug('newConf.ord: '+newConf.ord);///
            system.debug('newConf.ord.LastModifiedDate: '+newConf.ord.LastModifiedDate);///
            //Retrieve the date of the last edit of the order
            if (newConf.ord!=null&&newConf.ord.LastModifiedDate!=null)
                ordDate             = newConf.ord.LastModifiedDate.format();
            
            //GC
            newconf.upsertBlockedBySimpleCart   =   true;
            
            reloadCatalogVars();    
            retrieveAccountData();  

            //GC
            newconf.upsertBlockedBySimpleCart    =    false;            
            
            system.debug('**MF** constructor2: '+newConf.catitemsmap.size());                           
        }
        catch(Exception ecc) { system.debug('Error constructor: '+ecc.getMessage()+ ' at line: '+ecc.getLineNumber()+' stack trace: '+ecc.getStackTraceString()); }
    }    
    
    /** ENEL - When an item is selected, the method retrieves the Characteristics (and the Information note & App image fields from the Commercial Product)**/
    public void retrieveCharacteristics()
    {
        try
        {
          contextCommercialProduct    = new NE__Product__c();
            listOfCharacteristics       = new List<NE__DocumentationCharacterist__c>();
            if (commProdContext!=null && commProdContext!='')
            {
                contextCommercialProduct = [SELECT Name, Id,  
                                                (SELECT Name, Id, NE__Product__c, NE__Value__c, NE__Ranking__c,NE__Visible__c 
                                                 FROM NE__Documentation_Characteristics__r
                                                 WHERE NE__Visible__c = true) 
                                            FROM NE__Product__c
                                            WHERE Id=: commProdContext
                                            LIMIT 1];/*[Mf-Demo]App_image_1__c, Information_note_App1__c, Information_note_App2__c, Information_note_App3__c,App_image_2__c, App_image_3__c, App_image_4__c, App_image_5__c, */
                System.debug('contextCommercialProduct.NE__Documentation_Characteristics__r: '+contextCommercialProduct.NE__Documentation_Characteristics__r);
                System.debug('contextCommercialProduct.NE__Documentation_Characteristics__r.size(): '+contextCommercialProduct.NE__Documentation_Characteristics__r.size());                
                if (contextCommercialProduct.NE__Documentation_Characteristics__r!=null && contextCommercialProduct.NE__Documentation_Characteristics__r.size()>0)
                    listOfCharacteristics.addAll(contextCommercialProduct.NE__Documentation_Characteristics__r);
            }
        }
        catch(Exception ecc) { system.debug('Error retrieveCharacteristics: '+ecc.getMessage()+ ' at line: '+ecc.getLineNumber()+' stack trace: '+ecc.getStackTraceString()); }
    }
    
    /** ENEL - Redirect after the configuration */
    public void newComplexMarketSave()
    {
        try
        {
           
           redirectFromSummary = newConf.complexProdController.saveAndCloseConfiguration().getUrl();
            //String redirectToOppy=ApexPages.currentPage().getParameters().get('oppId');
           System.debug('redirectFromSummary url: '+redirectFromSummary );
           //PageReference page = newConf.complexProdController.saveAndCloseConfiguration();
           /*if(newConf.pageParameters.containsKey('ordId'))
                    newConf.pageParameters.put('orderId',newConf.pageParameters.remove('ordId'));
                page.getParameters().putAll(newConf.pageParameters);
                if(!newConf.pageParameters.containsKey('orderId'))
                    page.getParameters().put('orderId',newConf.ord.id);*/
                
             //redirectFromSummary=page.getUrl();
                 //redirectFromSummary='/'+redirectToOppy;
           
        }
        catch(Exception ecc)  {  system.debug('Error newComplexMarketSave: '+ecc.getMessage()+ ' at line: '+ecc.getLineNumber()+' stack trace: '+ecc.getStackTraceString()); }
    }      
            
    /** New checkout method **/
    /** MF 2015-07-15 */
    public Pagereference newCheckout()
    {
        try
        {
        
            // Before calling the checkout 
            /*
            NE__Order__c ordtemp = [SELECT Id,LastModifiedDate, (SELECT Id FROM NE__Order_Items__r) FROM NE__Order__c WHERE Id =: newConf.ord.Id];
            
            // Delete the order items            
            if (ordtemp!=null && ordtemp.NE__Order_Items__r!=null && ordtemp.NE__Order_Items__r.size()>0)    
                delete ordtemp.NE__Order_Items__r;*/
            System.debug('checkout');    
            Pagereference optyDetail = newConf.checkout();
            //Pagereference optyDetail = newConf.save();
            System.debug('optyDetail checkout');
            
            optyDetail.getParameters().put('orderId',newConf.ord.id);
           
            
            system.debug('First if'+optyDetail);
            system.debug('optyDetail.getParameters: '+optyDetail.getParameters().get('CUIT'));
             system.debug('newConf.pageParameters: '+newConf.pageParameters.get('CUIT') );           
            if(optyDetail.getParameters().get('CUIT') != null || newConf.pageParameters.get('CUIT') != null)  {              
                optyDetail    =   Page.BI_NEMobileOptyConfirm;
                optyDetail.getParameters().putAll(optyDetail.getParameters());
                optyDetail.getParameters().putAll(newConf.pageParameters);
                optyDetail.getParameters().put('orderId',newConf.ord.id); 
                system.debug('After if'+optyDetail);
            }
            
            
            /**/
            //System.debug('Redirect Precedente '+optyDetail.getUrl());
            //String redirectToChangePrice=optyDetail.getUrl(); /**/
            //redirectToChangePrice.replace('NEPageRedirect','LightOptySummary'); /**/
            //redirectToChangePrice.replace('accId','id'); /**/
            //PageReference changePrice = new PageReference(redirectToChangePrice); /**/
            //System.debug('Redirect Finale '+changePrice.getUrl());/**/
            
            
            //return changePrice; 
            
            NE__Order__c ordine = [SELECT id, BI_From_Mobile__c from NE__Order__c where id=:newConf.ord.id ];
            ordine.BI_From_Mobile__c=true;
            update ordine;
            
            
      
            return optyDetail;
        }
        catch(Exception ecc) {  system.debug('Error newCheckout: '+ecc.getMessage()+ ' at line: '+ecc.getLineNumber()+' stack trace: '+ecc.getStackTraceString());            return null;  } 
    }
    
    /** It gets the Account information **/
    public void retrieveAccountData()
    {
        try
        {
            //GC
            newConf.loadSession();            
            
            String accId        = newConf.accId;
            String servAccId    = newConf.servAccId;
            String billAccId    = newConf.billAccId;
            temporaryOrder      = new NE__Order__c();
            /*[Mf-Demo] Installation_Province__c,Mobile_Phone__c FirstName__c,LastName_Company__c */
            temporaryOrder      = [SELECT NE__AccountId__r.Type FROM NE__Order__c WHERE Id =: newConf.orderId AND (NE__AccountId__c =: accId OR NE__AccountId__c =: servAccId OR NE__AccountId__c =: billAccId) LIMIT 1];
            /*Schema.SObjectType obj                      = Schema.getGlobalDescribe().get('Account');    
            Map<String, Schema.SObjectField> fieldMap   = obj.getDescribe().fields.getMap();                                //retrieve all the fields of the object
            List<String> fieldNames                     = new List<String>(fieldMap.keySet());                              //put in the map the fields' name
            fieldNames.sort();                                                                                              //sort the list
            String query = 'SELECT ';                                                                                       //prepare the query string
            for (Integer i=0; i<fieldNames.size(); i++){                                                                    //add each field's name to the query string, add a comma when needed
                query += fieldNames.get(i);
                if (i!=fieldNames.size()-1)
                    query += ',';       
            }
            query += ', (SELECT Name,FirstName,LastName,Cellulare1__c,Type__c FROM Contacts WHERE (AccountId =: accId OR AccountId =: servAccId OR AccountId =: billAccId) AND Type__c = \'Rappresentante Legale\' ORDER BY CreatedDate DESC LIMIT 1) FROM Account WHERE Id =: accId OR Id =: servAccId OR Id =: billAccId LIMIT 1';                 
            contextAccount = Database.query(query);                                                                          //database.query();
            if (contextAccount.Contacts!=null && contextAccount.Contacts.size()>0)
                contextContact = contextAccount.Contacts.get(0); */
                
            System.debug('*PN* orderID: '+newConf.orderId);                                                                         
            system.debug('*MF* contextAccount: '+contextAccount);  
        }
        catch(Exception ecc) {  system.debug('Error retrieveAccountData: '+ecc.getMessage()+ ' at line: '+ecc.getLineNumber()+' stack trace: '+ecc.getStackTraceString());}
        
    }
    
    /** After the selectCatalog of NewConfigurationController, it reloads the maps **/
    public void reloadCatalogVars()
    {
        try
        {
            //GC
            newConf.loadSession();            

            system.debug('**MF** 1: '+newConf.catitemsmap.size()+'    '+wannaSelectCatalog);               
            //SELECTCATALOG         --> In order to repopulate some transient maps
            system.debug('cart.size before: '+newConf.cart.size());
            system.debug('*MF* newConf.selectedHeader: '+newConf.selectedHeader);
            if (wannaSelectCatalog == 'true')
            {
                System.debug('@@@@@@@@@@@@@ Sono nell if');
                newConf.changeComm=true;
                newConf.selectCatalog(); 
            }
            else
            {
                newConf.changeComm          =   false;
                newConf.isChangeCategory    =   true;
                newConf.offSet              =   '1';
                //newConf.getCatalog();
                system.debug('**MF** getCatalog: '+newConf.catitemsmap.size());    
                newConf.getCatalogItemList();
                system.debug('**MF** getCatalogItemList: '+newConf.catitemsmap.size());    
            }
            system.debug('**MF** 2: '+newConf.catitemsmap.size());     
            system.debug('cart.size after: '+newConf.cart.size());
            system.debug('*MF* newConf.catalogName: '+newConf.catalogName);
                
            //CATALOG CATEGORIES    --> CatalogCategories children of the selected Category
            listOfCatCategories     = new List<NE__Catalog_Category__c>();          
            system.debug('*MF* newConf.selectedCategory: '+newConf.selectedCategory);
            
            selectedCatCategory     = new NE__Catalog_Category__c();    
            if (newConf.selectedCategory != null && newConf.selectedCategory != '') 
                selectedCatCategory     = [SELECT Id,Name,NE__Thumbnail_Image__c, NE__Description__c, NE__Hidden__c FROM NE__Catalog_Category__c WHERE Id =: newConf.selectedCategory ORDER BY Name];    //MF 1.0 - Added WHERE condition     
            system.debug('*MF* selectedCatCategory r: '+selectedCatCategory);
                        
            if (selectedCatCategory != null)
                newConf.generateNavigationHistory(selectedCatCategory.Id);
            system.debug('**MF** 3: '+newConf.catitemsmap.size());     
            
            system.debug('*MF* newConf.catCategories: '+newConf.catCategories);
            if (newConf.catCategories != null)
                system.debug('*MF* newConf.catCategories.get(selectedCatCategory.Id): '+newConf.catCategories.get(selectedCatCategory.Id));
                
            if (selectedCatCategory != null && 
                newConf.catCategories != null && 
                newConf.catCategories.get(selectedCatCategory.Id) != null && 
                newConf.catCategories.get(selectedCatCategory.Id).NE__Catalog_Categories__r != null && 
                newConf.catCategories.get(selectedCatCategory.Id).NE__Catalog_Categories__r.size()>0)
                listOfCatCategories = newConf.catCategories.get(newConf.selectedCategory).NE__Catalog_Categories__r;
                    
                for(NE__Catalog_Category__c catCategory : listOfCatCategories)
                    catCategory.NE__Hidden__c = newConf.catCategories.get(catCategory.id).NE__Hidden__c;
            
            system.debug('**MF** 4: '+newConf.catitemsmap.size());                 
            system.debug('*MF* listOfCatCategories: '+listOfCatCategories);                     
            
            //CATEGORY ITEMS        --> Items children of the selected Category                              
            //mapOfCategoryItems      = new Map<String,List<NE.Item>>();
            //mapOfCategoryItems      = newConf.categoryItems;
            
            //MF 1.0
            listOfItems             = new List<NE.Item>();
            if (selectedCatCategory!=null && selectedCatCategory.Id!=null && newConf.categoryItems.get(selectedCatCategory.Id)!=null)    
                listOfItems.addAll(newConf.categoryItems.get(selectedCatCategory.Id));
            //system.debug('*MF* mapOfCategoryItems: '+mapOfCategoryItems);   
            system.debug('**MF** 5: '+newConf.catitemsmap.size());     
                            
            //ITEMS
            itemIdContext           = '';
            //mapOfCatalogItems       = new Map<String,NE__Catalog_Item__c>();
            //mapOfCatalogItems       = newConf.catalogItems;                 
            //system.debug('*MF* mapOfCatalogItems: '+mapOfCatalogItems);                             
            
            //PROMOTIONS
            promoIdContext          = ''; 
            mapOfCatalogPromo       = new Map<String,NE.Promotion>();
            mapOfCatalogPromo       = newConf.catalogPromo;                     
            system.debug('*MF* mapOfCatalogPromo: '+mapOfCatalogPromo);
            mapOfCatalogPromoItems  = new Map<String,NE.PromotionItem>();
            mapOfCatalogPromoItems  = newConf.catalogPromoItems;               
            system.debug('*MF* mapOfCatalogPromoItems: '+mapOfCatalogPromoItems);   
            
            system.debug('**MF** 6: '+newConf.catitemsmap.size());     
            
            system.debug('cart.size after all: '+newConf.cart.size());
            system.debug('NavHistory :'+newConf.navHistoryBarList);
            
        }    catch(Exception ecc)   {  system.debug('Error reloadCatalogVars: '+ecc.getMessage()+ ' at line: '+ecc.getLineNumber()+' stack trace: '+ecc.getStackTraceString());}
        
    }
    
     public void SelectCatalogCustomItem()
    {
            try
            {   
                //GC
                newConf.loadSession();                
                
                system.debug('Nuovo Catalogo'+newConf.selectedHeader);
                newConf.selectCatalog();

                 system.debug('OI**MF** 2: '+newConf.catitemsmap.size());     
            system.debug('cart.size after: '+newConf.cart.size());
            system.debug('*MF* newConf.catalogName: '+newConf.catalogName);
                
            //CATALOG CATEGORIES    --> CatalogCategories children of the selected Category
            listOfCatCategories     = new List<NE__Catalog_Category__c>();          
            system.debug('*MF* newConf.selectedCategory: '+newConf.selectedCategory);
            
            selectedCatCategory     = new NE__Catalog_Category__c();    
            if (newConf.selectedCategory != null && newConf.selectedCategory != '') 
                selectedCatCategory     = [SELECT Id,Name,NE__Thumbnail_Image__c, NE__Description__c, NE__Hidden__c FROM NE__Catalog_Category__c WHERE Id =: newConf.selectedCategory ORDER BY Name];    //MF 1.0 - Added WHERE condition     
            system.debug('*MF* selectedCatCategory s: '+selectedCatCategory);
                        
            if (selectedCatCategory != null)
                newConf.generateNavigationHistory(selectedCatCategory.Id);
            system.debug('**MF** 3: '+newConf.catitemsmap.size());     
            
            system.debug('*MF* newConf.catCategories: '+newConf.catCategories);
            if (newConf.catCategories != null)
                system.debug('*MF* newConf.catCategories.get(selectedCatCategory.Id): '+newConf.catCategories.get(selectedCatCategory.Id));
                
            if (selectedCatCategory != null && newConf.catCategories != null && newConf.catCategories.get(selectedCatCategory.Id) != null && newConf.catCategories.get(selectedCatCategory.Id).NE__Catalog_Categories__r != null && newConf.catCategories.get(selectedCatCategory.Id).NE__Catalog_Categories__r.size()>0)
                    listOfCatCategories = newConf.catCategories.get(newConf.selectedCategory).NE__Catalog_Categories__r;
                    
                for(NE__Catalog_Category__c catCategory : listOfCatCategories)
                    catCategory.NE__Hidden__c = newConf.catCategories.get(catCategory.id).NE__Hidden__c;
            
            system.debug('**MF** 4: '+newConf.catitemsmap.size());                 
            system.debug('*MF* listOfCatCategories: '+listOfCatCategories);                     
            
            //CATEGORY ITEMS        --> Items children of the selected Category                              
            //mapOfCategoryItems      = new Map<String,List<NE.Item>>();
            //mapOfCategoryItems      = newConf.categoryItems;
            
            //MF 1.0
            listOfItems             = new List<NE.Item>();
            if (selectedCatCategory!=null && selectedCatCategory.Id!=null && newConf.categoryItems.get(selectedCatCategory.Id)!=null)    
                listOfItems.addAll(newConf.categoryItems.get(selectedCatCategory.Id));
            //system.debug('*MF* mapOfCategoryItems: '+mapOfCategoryItems);   
            //system.debug('**MF** 5: '+newConf.catitemsmap.size());     
                            
            //ITEMS
            itemIdContext           = '';
            //mapOfCatalogItems       = new Map<String,NE__Catalog_Item__c>();
            //mapOfCatalogItems       = newConf.catalogItems;                 
            //system.debug('*MF* mapOfCatalogItems: '+mapOfCatalogItems);                             
            
            //PROMOTIONS
            promoIdContext          = ''; 
            mapOfCatalogPromo       = new Map<String,NE.Promotion>();
            mapOfCatalogPromo       = newConf.catalogPromo;                     
            system.debug('*MF* mapOfCatalogPromo: '+mapOfCatalogPromo);
            mapOfCatalogPromoItems  = new Map<String,NE.PromotionItem>();
            mapOfCatalogPromoItems  = newConf.catalogPromoItems;               
            system.debug('*MF* mapOfCatalogPromoItems: '+mapOfCatalogPromoItems);   
            
            system.debug('**MF** 6: '+newConf.catitemsmap.size());     
            
            system.debug('cart.size after all: '+newConf.cart.size());

            system.debug('NuoveCategorie: '+listOfCatCategories);
            }     catch(Exception ecc)  { system.debug('Error ChanCatalog '+ecc.getMessage()+ ' at line: '+ecc.getLineNumber()+' stack trace: '+ecc.getStackTraceString()); }         
    
    }
    
    /** Same method as configureProduct() of newConf, but it redirects to B2W_MarketConfigurator **/
    public PageReference configureComplexProduct()
    {  
        //GC
        newConf.loadSession();        
        
        if (newConf.catXmlAttachment == null)
            newConf.reloadCartData();

        String prefix = Site.getPathPrefix();
        if (prefix == null || prefix == '')
            prefix = '';
        system.debug('itemToConfigure: '+newConf.itemToConfigure);
        newConf.actualIitemId           =   '';
        newConf.actualPromoId           =   '';        
        system.debug('calling configuration: '+newConf.itemToConfigure);

        newConf.complexProdController = new NE.ProductConfiguratorController(newConf,newConf.itemToConfigure);

        PageReference confPage;  
        //IF commentato perchè quando non entra nell'if da attempt to do reference a null object
        //if(newConf.cartConfiguration.cartConfigurationObject.NE__CartType__c == 'ComplexCatalogStyle')  
            confPage    = Page.BI_MarketConfigurator;

        confPage.getParameters().put('rootItemVId',newConf.itemToConfigure); 
        confPage.getParameters().putAll(newConf.pageParameters);      
        confPage.setRedirect(false);
        //mapOfCatalogItems.clear();
        return confPage;
    }
    
    //public void reloadMap(){
    //  mapOfCatalogItems = newConf.catalogItems;     
    //
    //
    //}
    
    // Clone of the saveConfiguration method for the complex products configuration /
    public Pagereference complexMarketSave()
    {
        //GC
        newConf.loadSession();         
        
        //CARICATO VARIABILE
        String productStatusKey = newConf.complexProdController.rootCatItemVId+'_status_'+newConf.complexProdController.rootItem.catalogitem.NE__ProductId__r.name;
 
        newConf.complexProdController.saveConfiguration(); 
        //CARICATO VARIABILE        
        
        //05/07/2015 check if the catalog is empty

        automaticCheckOut   =   true;
        for(list<NE.Item> listOfItems:newConf.categoryItems.values())
        {
            for(NE.Item itemToCheck:listOfItems)
            {
                system.debug('itemToCheck: '+itemToCheck.catalogItem.NE__ProductId__r.name +' Eligible: '+itemToCheck.eligible+' visible: '+itemToCheck.visible);

                if(itemToCheck.eligible == true && itemToCheck.visible == 'true')
                    automaticCheckOut   =   false;
            }
        }          
        // MF 2015-07-14 - If the Configuration is not valid, don't do the redirect /
  //CAMBIATO VARIABILE
  String totalStatus = newConf.confStatusMap.get(productStatusKey);
  
        if (totalStatus=='Valid'){
            PageReference cartPage = Page.BI_Market;
            cartPage.getParameters().putAll(newConf.pageParameters);
            cartPage.setRedirect(false);        

            return cartPage;
        }else{return null; }
    }
    
    public void addItemsToCampareCustom ()
    {
        try
        {
            //GC
            newConf.loadSession(); 
            
            newConf.addItemsToCart();
            numberOfCompare = newConf.compareList.size();
            System.debug('Size : '+newConf.compareList.size());
        }       

       catch(Exception ecc){  system.debug('Error ChanCatalog '+ecc.getMessage()+ ' at line: '+ecc.getLineNumber()+' stack trace: '+ecc.getStackTraceString());  }        

    }
    
    /** Clone of the cancel method for the complex products configuration **/
    public Pagereference complexMarketCancel()
    {
        //GC
        newConf.loadSession();         

        PageReference cartPage = Page.BI_Market;
        cartPage.setRedirect(false);        
        newConf.complexProdController.cancel(); 
        return cartPage;
    }
    
    /** It starts the configuration **/
    public void startConfigurator()
    {
        //GC
        newConf.loadSession();         
        
        if(newConf.rootCatItemvId != null && newConf.rootCatItemVId != '')
            newConf.complexProdController = new NE.ProductConfiguratorController(newConf,newConf.rootCatItemVId);
    }
    
    /* TEST COMPARE IN MARKETCONFIGURATOR PAGE*/
    public void addItemsToCompareComplex ()
    {
        try
        {
            //GC
            newConf.loadSession();
            
            newConf.complexProdController.manageConfiguration();
            numberOfCompare = newConf.compareList.size();
            System.debug('Size : '+newConf.compareList.size());
        }       
        catch(Exception ecc) {  system.debug('Error ChanCatalog '+ecc.getMessage()+ ' at line: '+ecc.getLineNumber()+' stack trace: '+ecc.getStackTraceString()); }    
    }              
}