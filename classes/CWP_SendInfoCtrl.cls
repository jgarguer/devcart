public without sharing class CWP_SendInfoCtrl {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   
    Author:        Carlos Bastidas
    Company:       Everis.com
    Description:   Method to load user information 
    
    History:
    
    <Date>            <Author>              <Description>
    17/03/2017        Carlos Bastidas       Initial version
    17/03/2017        Carlos Bastidas       Method that searches user information. It retrieves user data from the database by using a parameter. 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @AuraEnabled
    public static User getloadInfo (String idParam) {
        User us = null;
        if(idParam != null && !String.isEmpty(idParam)) {
            System.debug('Param: '+idParam);
            for (User usTemp:[SELECT FullPhotoUrl, Email, Name FROM User WHERE Id =:idParam or ContactId =:idParam or  ProfileId =:idParam limit 1]){
               us = usTemp;
            }
        }
        System.debug('User: '+us);
        return us;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   
    Author:        Carlos Bastidas
    Company:       Everis.com
    Description:   Method to call UserInfo.getUserEmail() method.
    
    History:
    
    <Date>            <Author>              <Description>
    17/03/2017        Carlos Bastidas       Initial version
    17/03/2017        Carlos Bastidas       Method that calls UserInfo.getUserEmail() method.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @AuraEnabled
    public static String getMail () {
        return UserInfo.getUserEmail();
    }
    
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   
    Author:        Carlos Bastidas
    Company:       Everis.com
    Description:   Method to send message to contact
    
    History:
    
    <Date>            <Author>              <Description>
    17/03/2017       Carlos Bastidas       Initial version
    17/03/2017        Carlos Bastidas       Method that sends message entered in the CWP_SendInfoModal modal to the contact. The message will be send from logged user to contact.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
     @AuraEnabled
    public static void getSend(String emailTo, String emailFrom, String emailSubject, String emailBody){
       try{
           System.debug('emailTo--'+emailTo);
           System.debug('emailFrom--'+emailFrom);
           System.debug('emailSubject--'+emailSubject);
           System.debug('emailBody--'+emailBody);
           	
            Messaging.SingleEmailMessage mail3 = new Messaging.SingleEmailMessage();
            mail3.setToAddresses(new String[] {emailTo});
            mail3.setSaveAsActivity(true);
            mail3.setReplyTo(emailTo);
            mail3.setSubject(emailSubject);
            mail3.setPlainTextBody(emailBody);
            system.debug('### enviarEmail: ' + mail3);
            //List<Messaging.SendEmailResult> results =  Messaging.sendEmail(new Messaging.Email[] { mail3 });
            BI_SendMails_Helper.sendMails(new Messaging.SingleEmailMessage[] { mail3 }, false, 'PCA');
            //system.debug('### enviarEmail results: ' + results);
        }catch (exception e){
            BI_LogHelper.generateLog('PCA_EquipoTelefonica_PopUpCtrl.sendEmail', 'Portal Platino', e.getMessage(), 'Class');
        }
       
    }   
    
    
}