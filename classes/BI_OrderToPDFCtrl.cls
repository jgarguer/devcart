public with sharing class BI_OrderToPDFCtrl {
    /************************ CONTROLLORE COMMENTATO ****************************
    public OrderToPDFCtrl(ApexPages.StandardController controller) {

    }
    ********************************************************************************/
 

    public String Page                                      {get;set;}
    public String OpenPageURL                               {get;set;}
    public String quoteid                                   {get;set;}
    public String  orderId                                  {get;set;}//id dell'order
    public String  AccountId                                {get;set;}//id dell'account
    public NE__Order__c orderPdf                            {get;set;}
    public Account acc                                      {get;set;}
    public NE__Billing_Profile__c billProf                  {get;set;}
    public List  <NE__Order_Item_Attribute__c> orderItMap;
    public List <NE__OrderItem__c> ordItem                  {get;set;}
    public NE__Document_Component__c DC                     {get;set;}
    public NE__Document_Component__c DCitem                 {get;set;}
    public NE__Document_Component__c DCattribute            {get;set;}
    public String content                                   {get;set;} 
    public List<Contact > contactList                       {get;set;}  
    public String AccountIdx                                {get;set;} 
    
    Set<string> rightvalues = new Set<string>();
    public String[] stories                                 {get;set;}
    public String stories2                                  {get;set;}
    public String bodyMail                                  {get;set;}
    
    //*********metodo saveIT
        public String idpdf {get;set;}
    //******************
    
    ///******** 
        public String documentTitle{get;set;}
        public String URL;
    
    //******************************     CONTROLLOER NUOVO *******************************
    public BI_OrderToPDFCtrl(ApexPages.StandardController controller) {
        orderId=ApexPages.currentPage().getParameters().get('Id');
        orderPdf = [select id,Name, NE__AccountId__c  from NE__Order__c where id=:orderId];
        documentTitle='Contract_N.'+orderPDF.Name+'.pdf';
        //bodyMail='Testo di default';
        
        //Cambio Documento a seconda dell'ordine
        URL=NEA_B2W_GENERAR_DOCUMENTOS.CapturarCategorizacionProductos(orderId);
        
    }
    //**********************************************************************************
   

  /****************************  ************************************************  
    public void OrderToPdfController()
    {
      //  try
       // {
            System.debug('test ');   
           orderId             =       ApexPages.currentPage().getParameters().get('Id');  
     //   }
      //  catch (System.CalloutException e)
     //   {
       //     System.debug('ERROR:' + e);
      //  }    
    }
    ****************************************************************************************/
    
    //SOSTITUISCO WorkOrderId con Id
    public List<SelectOption> getStoryItems()
    {
        orderId             =       ApexPages.currentPage().getParameters().get('Id');
        List<SelectOption> options = new List<SelectOption>();
        try{
            orderPdf = [select id,Name, NE__AccountId__c  from NE__Order__c where id=:orderId];
            contactList = [Select  email From Contact where AccountId =: orderPdf.NE__AccountId__c  and email !=:null];
            for(Contact a: contactList)
            {
                options.add(new SelectOption(a.email,a.email));
            }
        }
        catch(Exception e) {}
        return options;
    }
  
    public String[] getStories() 
    {
        return stories;
    }
  
     public void setStories(String[] stories) 
    {
        this.stories= stories;
    } 
      //SOSTITUISCO WorkOrderId con Id
  /************************** SELECTCLICK SOSTITUITA***********************************************************
    public PageReference selectclick(){
        try
        {
            for(String s : stories){
                rightvalues.add(s);
                stories2= s+',';
                System.debug('Address '+s);   
            }

            //orderId =  Page;
            orderId             =       ApexPages.currentPage().getParameters().get('Id');
        
            Messaging.EmailFileAttachment pdfAttc = new Messaging.EmailFileAttachment();
    
            DC = [SELECT NE__Header_Name__c, Id, Name FROM NE__Document_Component__c WHERE NE__Header_Name__c = 'Adesion contract' LIMIT 1];
            String dcId = '';
            if (DC != null && DC.Id != null)
                dcId = DC.Id;
            //NE__
            PageReference p = new PageReference('/apex/NE__DocumentComponentPDF?PDF=true&mapName=EEContractDataMapclone&Id='+dcId+'&parentId='+orderId+'&dateformat=dd-MM-yyyy');
            *//*PageReference p = Page.DocumentComponentPDF;
            p.getParameters().put('PDF','true');
            p.getParameters().put('mapName','MapOrderItems');
            p.getParameters().put('Id',dcId);
            p.getParameters().put('parentId',orderId);*//*
            pdfAttc.Body = p.getContentAsPDF();
        
            string csvname= 'Document.pdf';
            pdfAttc.setFileName(csvname);
    
            Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
            String[] toAddresses;
            NE__Order__c noffer = [Select Name FROM NE__Order__c WHERE Id=:orderId limit 1];
            String nameatt = 'Document_N.'+noffer.Name+'.pdf';
            String subject =nameatt;
            email.setSubject(subject);
            System.debug('testx '+stories);
            String[] toAddresses2=stories2.split(',');
            if(toAddresses2.size()>0)
            {
                for(Integer i=0;i<toAddresses2.size();i++)
                {
                    stories.add(toAddresses2[i]);
                }      
                email.setToAddresses( stories);
            }
    
            email.setToAddresses( stories);    
            //email.setPlainTextBody('Document PDF ');
            email.setPlainTextBody(bodyMail);
            email.setFileAttachments(new Messaging.EmailFileAttachment[]{pdfAttc});
    
            try{
                System.debug('Email '+email);   
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }
            catch(Exception e){}
            return null;
        }
        catch (System.CalloutException e)
        {
            System.debug('ERROR:' + e);
            return null;
        }    
    }
    *********************************************************************************************/
    //****************************NUOVA SELECTCLICK************************************
    public PageReference selectclick(){
        
        try
        {
            for(String s : stories){
                rightvalues.add(s);
                stories2= s+',';
                System.debug('Address '+s);   
            }

            //orderId =  Page;
            //orderId             =       ApexPages.currentPage().getParameters().get('Id');
        
            Messaging.EmailFileAttachment pdfAttc = new Messaging.EmailFileAttachment();
            
            //PageReference p = new PageReference('/apex/NE__DocumentComponentPDF?PDF=true&mapName=EEContractDataMapclone&nameDCH=DocOfertaSM&dateFormat=dd-MM-yyyy&parentId='+orderId);
            PageReference p = new PageReference(URL);
            pdfAttc.Body = p.getContentAsPDF();
            
            
            
    
            Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
            String[] toAddresses;
            //NE__Order__c noffer = [Select Name FROM NE__Order__c WHERE Id=:orderId limit 1];
            //NE__Order__c noffer=orderPdf;
            String nameatt = 'Document_N.'+orderPDF.Name+'.pdf'; //
            String subject =nameatt;
            //string csvname= 'Contract_N.'+orderPDF.Name+'.pdf';  //
            //pdfAttc.setFileName(csvname);
            pdfAttc.setFileName(documentTitle+'.pdf');
            if(documentTitle.endsWithIgnoreCase('.pdf'))
                pdfAttc.setFileName(documentTitle);
            ////if(documentTitle!=null && documentTitle!='')
            ////    pdfAttc.setFileName(documentTitle+'.pdf');
            email.setSubject(subject);
            System.debug('testx '+stories);
            String[] toAddresses2=stories2.split(',');
            if(toAddresses2.size()>0)
            {
                for(Integer i=0;i<toAddresses2.size();i++)
                {
                    stories.add(toAddresses2[i]);
                }      
                email.setToAddresses( stories);
            }
    
            email.setToAddresses( stories);    
            //email.setPlainTextBody('Document PDF ');
            email.setPlainTextBody(bodyMail);
            email.setFileAttachments(new Messaging.EmailFileAttachment[]{pdfAttc});
    
            try{
                System.debug('Email '+email);   
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }
            catch(Exception e){
                System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@Eccezione '+e.getMessage());
            }
            return null;
            
            
            
            
            
            
        }catch(System.CalloutException e)
        {
            System.debug('ERROR:' + e);
            return null;
        }    
        
        
    }
    
    //****************************************************************************
    
    
    
/************************************* REDIRECT SOSTITUITO****************************************
    public PageReference redirect()
    {
        DC = [SELECT NE__Header_Name__c, Id, Name FROM NE__Document_Component__c WHERE NE__Header_Name__c = 'Adesion contract' LIMIT 1];
        String dcId = '';
        if (DC != null && DC.Id != null)
            dcId = DC.Id;
        
        *//*PageReference p = Page.NE__DocumentComponentPDF;
        p.getParameters().put('PDF','true');
        p.getParameters().put('mapName','MapOrderItems');
        p.getParameters().put('Id',dcId);
        p.getParameters().put('parentId',orderId);*//*          
        OpenPageURL = '/apex/NE__DocumentComponentPDF?PDF=true&mapName=EEContractDataMapclone&Id='+dcId+'&parentId='+orderId+'&dateformat=dd-MM-yyyy';
        return null;
    }
    *****************************************************************************************************/
    
    //************************************** NUOVO REDIRECT ******************************
    public PageReference redirect()
    {             
        String orderId             =       ApexPages.currentPage().getParameters().get('Id'); 
        
        //PageReference TestPDF = new PageReference('/apex/NE__DocumentComponentPDF?PDF=true&mapName=EEContractDataMapclone&ID=a0L11000005u8M6&parentId='+orderId+'&dateformat=dd-MM-yyyy');
        /**/
        //OpenPageURL = '/apex/NE__DocumentComponentPDF?PDF=true&mapName=EEContractDataMapclone&ID=a0L11000005u8M6&parentId='+orderId+'&dateformat=dd-MM-yyyy';
        //OpenPageURL = '/apex/NE__DocumentComponentPDF?PDF=true&mapName=EEContractDataMapclone&nameDCH=DocOfertaSM&dateFormat=dd-MM-yyyy&parentId='+orderId;
        OpenPageURL = URL;
        
        //NE__Order__c oConfiguration = [SELECT Id, Name FROM NE__Order__c WHERE Id =: orderId];
        
        /*Attachment attcheck =  new Attachment();
        attcheck.name      = 'Contract_' + oConfiguration.Name + '.pdf';
        attcheck.body      =  TestPDF.getContentAsPDF();
        attcheck.parentid  =  orderId;
        attcheck.ContentType = 'application/pdf';
        insert attcheck ;

        idpdf = '/'+attcheck .Id;*/

        return null;  
    }
    //***********************************************************************************************
    
    /*****************************SAVE IT MODIFICATO ******************************************
    public PageReference SaveIt()
    {             
        orderId             =       ApexPages.currentPage().getParameters().get('WorkOrderId'); 
        NE__Order__c noffer = [Select Name FROM NE__Order__c WHERE Id=:orderId limit 1];
        String nameatt = 'Document_N.'+noffer.Name+'.pdf';
        Attachment att = new Attachment(name =nameatt);
        Attachment att2 = new Attachment(name =nameatt); 
       
        DC = [SELECT NE__Header_Name__c, Id, Name FROM NE__Document_Component__c WHERE NE__Header_Name__c = 'Adesion contract' LIMIT 1];
        String dcId = '';
        if (DC != null && DC.Id != null)
            dcId = DC.Id;                                       
        
        PageReference TestPDF = new PageReference('/apex/NE__DocumentComponentPDF?PDF=true&mapName=EEContractDataMapclone&Id='+dcId+'&parentId='+orderId+'&dateformat=dd-MM-yyyy');
       
        List <Attachment> attcheck;
        if(orderId!=null)
            attcheck = [Select Name, parentid,body FROM Attachment WHERE parentid=:orderId and Name=:nameatt limit 1];

        TestPDF.getParameters().put('Id',dcId);
        TestPDF.getParameters().put('parentId',orderId);
        TestPDF.getParameters().put('mapName','EEContractDataMapclone');
                
        try{
            if(orderId!=null){    
                if( attcheck.size()==0)
                {    
                    //att.body = TestPDF.getContentAsPDF();
                    att.Body = TestPDF.getContent();
                    att.parentid = orderId;
                    insert att;
                    quoteid= '/'+orderId;
                }
                else
                {
                    attcheck[0].body =TestPDF.getContentAsPDF();
                    update attcheck[0];
                    quoteid= '/'+orderId;
                }
            }
        }
        catch(Exception e){}
        return null;  
    }
    
    *************************************************************************************************/
    
    //******************************* NUOVO SAVEIT ***********************************
    public PageReference SaveIt()
    {             
       // String orderId             =       ApexPages.currentPage().getParameters().get('Id'); 
        
        //PageReference TestPDF = new PageReference('/apex/NE__DocumentComponentPDF?PDF=true&mapName=EEContractDataMapclone&nameDCH=DocOfertaSM&dateFormat=dd-MM-yyyy&parentId='+orderId);
        PageReference TestPDF = new PageReference(URL);
        
        NE__Order__c oConfiguration = [SELECT Id, Name FROM NE__Order__c WHERE Id =: orderId];
        
        Attachment attcheck =  new Attachment();
        //attcheck.name      = 'Contract_' + oConfiguration.Name + '.pdf';
        attcheck.name=documentTitle+'.pdf';
        if(documentTitle.endsWithIgnoreCase('.pdf'))
            attcheck.name=documentTitle;
        attcheck.body      =  TestPDF.getContentAsPDF();
        attcheck.parentid  =  orderId;
        attcheck.ContentType = 'application/pdf';
        insert attcheck ;

        idpdf = '/'+attcheck .Id;

        return null;  
    }
    
    //******************************************************************************
@future(callout=true)
public static void sendAttach(String link,String userSessionId){
        try{
            String addr = URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/sendPDFEmail';
            System.debug('DEBUG url sendPDFEmail'+' '+addr);
            Map<String,String> postBody = new Map<String,String>();
    
            HttpRequest req = new HttpRequest();
            req.setEndpoint( addr );
            req.setMethod('POST');
            req.setHeader('Authorization', 'OAuth ' + userSessionId);
            req.setHeader('Content-Type','application/json');
            
            postBody.put('URL',link);
    
            String reqBody = JSON.serialize(postBody);
            req.setBody(reqBody);
            Http http = new Http();
            HttpResponse response = http.send(req);
            System.debug('DEBUG RESPONSE HTTP'+' '+response);
        }catch(Exception ecc){
            System.debug('Errore sendAttach: '+ecc.getMessage()+ ' at line: '+ecc.getLineNumber()+' stack trace: '+ecc.getStackTraceString());
            //GestioneLog.creaLog('Multe','sendAttach',ecc.getLineNumber(),ecc.getMessage(),ecc.getStackTraceString(),ecc.getTypeName(),'','','');
        }
    }
    
}