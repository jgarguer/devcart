public with sharing class BI_FacturasMethods {

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Methods executed by BI_Facturas Triggers 
    
    History

    <Date>            <Author>          <Description>
    05/08/2014        Antonio Moruno    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/		
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Method that insert region value of account in Facturas object region field.
    
    IN:            ApexTrigger.Old, ApexTrigger.New
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    05/08/2014        		Antonio Moruno      	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    static
    {
        for(RecordType rt :[ SELECT Id, DeveloperName FROM RecordType WHERE sObjectType in ('BI_Registro_Datos_Desempeno__c','BI_FVI_Nodos_Objetivos__c') ])
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
    }

	public static void createRegion_Facturas(List<BI_Facturas__c> newsFact,List<BI_Facturacion__c> newsFaction,List<BI_Cobranza__c> newsCobran){ 
		try{
			if(newsFact != null){
				
				Set<Id> set_Ids = new Set<Id>();
				for(BI_Facturas__c Fact :newsFact){
					set_Ids.add(Fact.BI_Nombre_del_cliente__c);
				}
				system.debug('set_Ids ' +set_Ids);
				List<BI_Facturas__c> listFacts = new List<BI_Facturas__c>();
				List<Account> AccFact = [Select Name, BI_Country__c From Account Where Id IN :set_Ids]; 
			//
				for(BI_Facturas__c Fact :newsFact){
					for(Account CuentaFact :AccFact){
						if(Fact.BI_Nombre_del_cliente__c == CuentaFact.Id){
							//The BI_Country field is not write-able -()
							//Fact.BI_Country__c = CuentaFact.BI_Country__c;
							listFacts.add(Fact);
						}
						 
						  
					}
				}
				
			}
			
			else if(newsFaction != null){
				
				Set<Id> set_Ids = new Set<Id>();
				for(BI_Facturacion__c Faction :newsFaction){
					set_Ids.add(Faction.BI_Cliente__c);
				}
				system.debug('set_Ids ' +set_Ids);
				List<BI_Facturacion__c> listFacts = new List<BI_Facturacion__c>();
				List<Account> AccFact = [Select Name, BI_Country__c From Account Where Id IN :set_Ids]; 
				
				for(BI_Facturacion__c Faction :newsFaction){
					for(Account CuentaFaction :AccFact){
						if(Faction.BI_Cliente__c == CuentaFaction.Id){
							if(Faction.BI_Country__c == null){
								// the BI_Facturacion__c is not write-able
								//Faction.BI_Country__c = CuentaFaction.BI_Country__c;
								listFacts.add(Faction);
							}
						}
						 
						  
					}
				}
			}
			else if(newsCobran != null){
				
				Set<Id> set_Ids = new Set<Id>();
				for(BI_Cobranza__c Cobran :newsCobran){
					set_Ids.add(Cobran.BI_clientes__c);
				}
				system.debug('set_Ids ' +set_Ids);
				List<BI_Cobranza__c> listCobran = new List<BI_Cobranza__c>();
				List<Account> AccFact = [Select Name, BI_Country__c From Account Where Id IN :set_Ids]; 
				
				for(BI_Cobranza__c Cobran :newsCobran){
					for(Account CuentaCobran :AccFact){
						if(Cobran.BI_clientes__c == CuentaCobran.Id){
							// BI_Cobranza__c is not write-able
							//Cobran.BI_Country__c = CuentaCobran.BI_Country__c;
							listCobran.add(Cobran);
						}
						 
						  
					}
				}
				
			}
		}
		catch (exception Exc)
		{
			BI_LogHelper.generate_BILog('BI_FacturasMethods', 'BI_EN', Exc, 'Trigger');
		}
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes
    Company:       New Energy Aborda
    Description:   Campo para crear los registros de desempeños en la carga de facturas.
    
    IN:            ApexTrigger.Old, ApexTrigger.New
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    05/06/2016        		Humberto Nunes	     	Initial version
    26/07/2016       		Humberto Nunes	     	Se cambio el campo BI_FVI_Valor__c por BI_FVI_Cargo_Fijo_Total__c
    02/09/2016				Humnerto Nunes			Se Filtro para que solo creara registros para Pais=Peru, SubSegmentoRegional contenga FVI y monto > 0 
    09/11/2016				Humberto Nunes			Se cambio "IC" por "Ingresos Comerciales"		
    15/03/2018              Humberto Nunes          Se agrego el Tipo de Registro 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void CreateRegistroDesemp(List<BI_Facturas__c> lstNewRecords)
	{
		try 
		{
			Set<Id> set_Ids = new Set<Id>();
			for(BI_Facturas__c Fact :lstNewRecords)
				set_Ids.add(Fact.BI_Nombre_del_cliente__c);

			system.debug('set_Ids ' +set_Ids);

			Map<Id,Account> map_acc = new Map<Id,Account>();
			map_acc = new Map<Id,Account>([SELECT Id, BI_Country__c, BI_Subsegment_Local__c FROM Account WHERE Id in :set_Ids]); 


			List<BI_Registro_Datos_Desempeno__c> lstRegDatosDesemp = new List<BI_Registro_Datos_Desempeno__c>();

			for(BI_Facturas__c objFact : lstNewRecords)
			{
				if(	objFact.BI_FVI_FechaCobro__c != null && 
					objFact.BI_FVI_Cargo_Fijo_Total__c>0 &&  
					map_acc.get(objFact.BI_Nombre_del_cliente__c).BI_Country__c == 'Peru' && 
					map_acc.get(objFact.BI_Nombre_del_cliente__c).BI_Subsegment_Local__c.contains('FVI') ) 
				{
					BI_Registro_Datos_Desempeno__c objRegDatosDesemp = new BI_Registro_Datos_Desempeno__c();
					// Datetime dtConvertDT = objFact.BI_FVI_FechaCobro__c;	
					objRegDatosDesemp.BI_FVI_Fecha__c = objFact.BI_FVI_FechaCobro__c; // date.newinstance(dtConvertDT.year(), dtConvertDT.month(), dtConvertDT.day());
					objRegDatosDesemp.BI_FVI_Id_Cliente__c = objFact.BI_Nombre_del_cliente__c;
					objRegDatosDesemp.BI_Tipo__c = 'Ingresos Comerciales'; // ANTES 'IC'
					objRegDatosDesemp.BI_FVI_Valor__c = objFact.BI_FVI_Cargo_Fijo_Total__c;   // objFact.BI_Importe_Factura__c; // decimal.valueof(objFact.BI_Importe_Factura__c);
					objRegDatosDesemp.BI_FVI_IdObjeto__c = objFact.Id;
                    objRegDatosDesemp.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018
					lstRegDatosDesemp.add(objRegDatosDesemp);  
				}
			}

			insert lstRegDatosDesemp;
			system.debug('lstRegDatosDesempNew----->>' +lstRegDatosDesemp);			 
		}
		catch (exception Exc)
		{
			BI_LogHelper.generate_BILog('BI_FacturasMethods.CreateRegistroDesemp', 'BI_EN', Exc, 'Trigger');
		}
	}

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes
    Company:       Accenture
    Description:   Crear los registros de desempeños en la carga de facturas para Nodos Objetivos Jerarquicos.

    History: 
    
    <Date>                  <Author>                <Change Description>
    14/02/2018       		Humberto Nunes	     	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void Create_RDD_NCJ_FAC (List<BI_Facturas__c> lstNewRecords)
    {      
        system.debug('Create_RDD_NCJ_FAC Execution :::' + BI_FVI_SingleExecution.hasAlreadyExecuted('Create_RDD_NCJ_FAC'));
        if(!BI_FVI_SingleExecution.hasAlreadyExecuted('Create_RDD_NCJ_FAC')) // PREVIENE LA DOBLE EJECUCION DEL METODO
        {
            BI_FVI_SingleExecution.setAlreadyExecuted('Create_RDD_NCJ_FAC'); // MARCA LA BANDERA PARA QUE NO SE VUELVA A EJECUTAR OTRA VEZ EN ESTA LLAMADA DEL TRIGGER
            
            set<Id> setIdAccs = new Set<Id>();
            Date max_date = null;
            Date min_date = null;        
            set<Id> setIdOwners = new set<Id>(); 

            // OBTIENE LAS CUENTAS ASOCIADAS A LAS FACTURAS Y LAS FECHAS MINIMA Y MAXIMA PARA OBTENER LOS NOD
			for(BI_Facturas__c objFact : lstNewRecords)
            {           
            	setIdAccs.add(objFact.BI_Nombre_del_cliente__c);
                // setIdOwners.add(lstNewRecords[i].OwnerId); // EVENTUALMENTE PODRIA SER EL OWNER DE LA FACTURA
                if(max_date == null)
                {
                    max_date = objFact.BI_FVI_FechaCobro__c;
                    min_date = objFact.BI_FVI_FechaCobro__c;
                }
                else
                {
                    if(objFact.BI_FVI_FechaCobro__c > max_date) max_date = objFact.BI_FVI_FechaCobro__c;
                    if(objFact.BI_FVI_FechaCobro__c < min_date) min_date = objFact.BI_FVI_FechaCobro__c;
                }
            }
            
            // COMENTAR ESTO SI SE DESCOMENTA LA LINEA QUE OBTIENE LOS PROPIETARIOS DE LAS FACTURAS
            system.debug('Create_RDD_NCJ_FAC: ' + setIdAccs);
            Map<Id,Account> map_acc = new Map<Id,Account>([SELECT Id, OwnerId FROM Account WHERE Id in :setIdAccs]); 
            for (Account acc : map_acc.values())
            	setIdOwners.add(acc.OwnerId);

            if(!setIdOwners.isEmpty()) 
            {
                // SE BUSCAN TODOS LOS NODOS OBJETIVOS JERARQUICOS: (RecosrdType.DeveloperName = "BI_FVI_Objetivos_Jerarquicos")
                // .- ACTIVOS PARA LAS FECHA DE CIERRE min_date Y max_date
                // .- QUE PERTENEZCAN A LOS PROPIETARIOS DE LAS OPP CERRADAS
                // .- QUE SEAN DE TIPO BI_Tipo__c = "Ingresos Comerciales"
                List<BI_FVI_Nodos_Objetivos__c> lst_no = [SELECT Id,
                                                                 BI_FVI_Id_Nodo__c,
                                                                 BI_FVI_Id_Nodo__r.OwnerId,
                                                                 BI_FVI_Objetivo_Comercial__r.BI_FVI_Unidad_Medida__c,
                                                                 BI_FVI_Objetivo_Comercial__r.BI_OBJ_Campo__c,
                                                                 BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c,
                                                                 BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c
                                                          FROM   BI_FVI_Nodos_Objetivos__c
                                                          WHERE  BI_FVI_IdPeriodo__r.BI_FVI_Activo__c = true 
                                                          AND    BI_FVI_Id_Nodo__r.OwnerId in :setIdOwners
                                                          AND    BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c >=: min_date
                                                          AND    BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c <=: max_date
                                                          AND    BI_FVI_Objetivo_Comercial__r.BI_FVI_Activo__c = true                                                      
                                                          AND    BI_FVI_Objetivo_Comercial__r.BI_Tipo__c = 'Ingresos Comerciales'
                                                          AND    RecordTypeId = :MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Jerarquicos')
                                                         ];

                system.debug('Create_RDD_NCJ_FAC lst_no: ' + lst_no);

                List<BI_Registro_Datos_Desempeno__c> lstRegDatosDesemp = new List<BI_Registro_Datos_Desempeno__c>();

                // SE PASEA POR TODAS LAS FACTURAS PARA DETECTAR LAS QUE AFECTEN NOD
				for(BI_Facturas__c objFact : lstNewRecords)
                {           
                    // CREAR LOS RDD CON EL CAMPO BI_OBJ_Campo__c Y RELLENAR EL VALOR (DIVISA O NO)
                    for(BI_FVI_Nodos_Objetivos__c no: lst_no)
                    {
                        // PARA CADA NODO-OBJETIVO CUYO PROPIETARIO DEL NODO SEA IGUAL AL PROPIETARIO DE LA CUENTA ASOCIADA A LA FACTURA SE CREA UN RDD Y ASOCIADO AL NODO-OBJETIVO.
                        if (map_acc.get(objFact.BI_Nombre_del_cliente__c).OwnerId == no.BI_FVI_Id_Nodo__r.OwnerId && no.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c >= objFact.BI_FVI_FechaCobro__c && no.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c <= objFact.BI_FVI_FechaCobro__c)
                        {
                            BI_Registro_Datos_Desempeno__c objRegDatosDesemp = new BI_Registro_Datos_Desempeno__c();
                            objRegDatosDesemp.BI_FVI_Fecha__c = objFact.BI_FVI_FechaCobro__c; 
                            objRegDatosDesemp.BI_FVI_Id_Cliente__c = objFact.BI_Nombre_del_cliente__c;
                            objRegDatosDesemp.BI_Tipo__c = 'Ingresos Comerciales'; 
                            objRegDatosDesemp.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Desempeno_Nodos_Jerarquico');
							objRegDatosDesemp.BI_OBJ_Valor_Moneda__c = objFact.BI_FVI_Cargo_Fijo_Total__c;   // BI_FVI_Valor__c  objFact.BI_Importe_Factura__c; // decimal.valueof(objFact.BI_Importe_Factura__c);
							objRegDatosDesemp.BI_FVI_IdObjeto__c = objFact.Id;
                            objRegDatosDesemp.CurrencyIsoCode = objFact.CurrencyIsoCode;

                            objRegDatosDesemp.BI_OBJ_NodoObjetivo__c = no.Id;
                            lstRegDatosDesemp.add(objRegDatosDesemp);
                        }
                    }    
                }

                insert lstRegDatosDesemp;
                system.debug('lstRegDatosDesempNew----->>' +lstRegDatosDesemp);          

                // CREAR LOS NOD: SE CREAN EN BI_FVI_DesempenoMethods.generaNodo_Objetivo_Desempeno

            }
        }    
     }



}