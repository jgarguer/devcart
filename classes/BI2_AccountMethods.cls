public without sharing class BI2_AccountMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       Salesforce.com
     Description:   Methods executed by Account Triggers 
     Test Class:    (Pending)
    
     History:
     
     <Date>                  <Author>                <Change Description>
     02/09/2015              Jose Miguel Fierro       Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       Salesforce.com
     Description:   Method to assign BI2_cuadrilla_id__c

     History: 
    
     <Date>                     <Author>                <Change Description>
     02/09/2015                 Jose Miguel Fierro       Initial Version
	 29/10/2015					Jose Miguel Fierro		 Added try-catch and country filters
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void actualizarIdCola(List<Account> news, List<Account> olds) {
        try {
            // If User country is not X, do not continue
            List<User> u_me = [SELECT Id, Pais__c FROM User WHERE Id = :UserInfo.getUserId() AND Pais__c =: Label.BI_Peru];
            if(u_me.size() > 0) {
                
                Set<String> set_noms_cuadrillas = new Set<String>();
                for(Account acc : news) {
                    System.debug('##Acc: '+acc.BI2_cuadrilla__c);
                    if(acc.BI2_cuadrilla__c != null && acc.BI2_cuadrilla__c != '') {
                        System.debug('Paso por aqui tete');
                        set_noms_cuadrillas.add(acc.BI2_cuadrilla__c);
                    } else {
                        acc.BI2_cuadrilla_id__c = '';
                    }
                }
        
                if(set_noms_cuadrillas.size() > 0) {
                    List<QueueSObject> lst_colas = [SELECT Id, Queue.Id, Queue.DeveloperName FROM QueueSObject WHERE SObjectType='Case' AND Queue.DeveloperName IN: set_noms_cuadrillas];
        
                    for(Account acc : news) {
                        Boolean found = false;
                        for(QueueSObject qso : lst_colas) {
                            if(acc.BI2_cuadrilla__c == qso.Queue.DeveloperName) {
                                acc.BI2_cuadrilla_id__c = qso.Queue.Id;
                                found = true;
                            }
                        }
                        if(!found) {
                            acc.BI2_cuadrilla_id__c = '';
                        }
                    }
                }
            }
        } catch(Exception exc) {
            BI_LogHelper.generate_BILog('BI2_AccountMethods.actualizarIdCola', 'BI_EN', exc, 'Trigger');
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that create Entitlements for Venezuela Accounts
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        15/03/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void createAnsVEN (List <Account> news, List <Account> olds){

        try{

            Map <Id,Account> toProcess = new Map <Id,Account>();

            if(olds == null){
                for(Account acc : news){
                    if((acc.BI_Segment__c == 'Empresas' || (acc.BI_Segment__c == 'Negocios' && acc.BI_Subsegment_Regional__c == 'Top')) && acc.BI_Country__c == Label.BI_Venezuela){
                        toProcess.put(acc.Id, acc);
                    }
                }
            }
            else{
                for(Integer i = 0; i<news.size();i++){

                    if(
                    news[i].BI_Country__c == Label.BI_Venezuela &&
                    (olds[i].BI_Segment__c != 'Empresas' && !(olds[i].BI_Segment__c == 'Negocios' && olds[i].BI_Subsegment_Regional__c == 'Top')) &&
                    ((news[i].BI_Subsegment_Regional__c == 'Top' && news[i].BI_Segment__c == 'Negocios')||
                    (news[i].BI_Segment__c == 'Empresas'))
                    ){
                        toProcess.put(news[i].Id, news[i]);
                    }
                }
            }

            if(!toProcess.isEmpty()){
                System.debug('********entro a procesar');
                List <Entitlement> lst_ent = [SELECT Id, AccountId FROM Entitlement WHERE AccountId IN: toProcess.keySet()];
                Set <Id> set_ent = new Set <Id>();
                for(Entitlement ent : lst_ent){

                    set_ent.add(ent.AccountId);
                }

                List <SlaProcess> lst_sla = [SELECT Id ,Name, BusinessHoursId FROM SlaProcess WHERE Name LIKE '%Venezuela%' AND isActive = true];

                if(!lst_sla.isEmpty()){
                    System.debug('********entro a crear');
                    List <Entitlement> lst_ent_toInsert = new List <Entitlement>();
                    for(Id idAcc : toProcess.keySet()){

                        if(!set_ent.contains(idAcc)){
                            for(SlaProcess sla : lst_sla){
                                Entitlement ent = new Entitlement(
                                    Name = sla.Name,
                                    AccountId = idAcc,
                                    SlaProcessId = sla.Id,
                                    BusinessHoursId = sla.BusinessHoursId,
                                    StartDate = Date.today()
                                );
                                lst_ent_toInsert.add(ent);
                                System.debug('********creo----->' + ent);
                            }
                        }
                    }
                    if(!lst_ent_toInsert.isEmpty()){
                        insert lst_ent_toInsert;
                    }
                }
            }
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI2_AccountMethods.createAnsVEN', 'BI_EN', exc, 'Trigger');
        }
    }

}