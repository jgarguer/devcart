public class BI2_COL_PCA_CreacionIncidencia_ctr extends PCA_HomeController{
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Carlos Carvajal
    Company:       Avanxo
    Description:   Pop up para crea un nuevo incidetnet tecnico

    History:

    <Date>            <Author>            <Description>
    01/03/2017        Carlos Carvajal           Initial version
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Contact objContact{get;set;}
    public Case newCase {get; set;}
    public String Tipo {get;set;}
    public boolean haveError  {get;set;}
    public String EqTel {get;set;}
    public Id IdAttach{get;set;}
    public String prev{get;set;}
    public boolean adjuntos {get; set;}
    public String siteName {get; set;}
    public String siteFullName {get; set;}
    public Id siteInput {get; set;}
    public String CIname {get; set;}
    public Id CIinput {get; set;}
    public string StringWSCreacionPortal {get; set;}
    public BIIN_Creacion_Ticket_WS.CamposOutsiders co;
    public String country {get; set;}
    public BI_Configuracion_Caso_PP__c ccpp {get; set;}
    public string caseType     {get; set;}
    public Map <String, Id> map_rt {get; set;}
    public boolean isTech    {get;set;}
    public boolean hasChosen    {get;set;}
    public User objUser {get;set;}
    public boolean blnCrearContacto {get;set;}
    public boolean blnContactoConsultado {get;set;}
    //Variables d ajuntar documentos
   public Transient Attachment myDoc {get;set;}
	 public  Transient List<Attachment> lstmyDoc	        {get;set;}
	 public List<String> listaAdjuntos_Name {get;set;}
   public  List<BIIN_Creacion_Ticket_WS.Adjunto> lstAdjuntosRoD;
	 public integer contAdjuntos 	{get;set;}
	 public boolean limiteAdjuntos 	{get;set;}
	 public boolean noError			{get;set;}
   public Transient Blob adjunto1 {get; set;}
   public String nombre1 {get; set;}
    /*public Attachment myDoc {
        get {
            if(myDoc == null) {
                myDoc = new Attachment();
            }
            myDoc.ParentId = newCase != null ? newCase.Id : null;
            return myDoc;
        }
        set;
    }*-/


  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Get type of the Case

    History:

    <Date>            <Author>            <Description>
    26/06/2014        Ana Escrich           Initial version
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  public List<Schema.FieldSetMember> getFields() {
    try{
      if(BI_TestUtils.isRunningTest()){
        throw new BI_Exception('test');
      }

      if(isTech){
            return SObjectType.Case.FieldSets.BIIN_NewCasoTecnico.getFields();
      }else{
            return SObjectType.Case.FieldSets.PCA_NewCaseComercial.getFields();
      }
    }catch (exception Exc){
       BI_LogHelper.generate_BILog('BI2_COL_PCA_CreacionIncidencia_ctr.getFields', 'Portal Platino', Exc, 'Class');
       return null;
    }
  }

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
      Author:        Ana Escrich
      Company:       Salesforce.com
      Description:   check permissions of current user

      History:

      <Date>            <Author>            <Description>
      26/06/2014        Ana Escrich           Initial version
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI2_COL_PCA_CreacionIncidencia_ctr.checkPermissions', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Constructor

    History:

    <Date>            <Author>            <Description>
    05/07/2014        Antonio Moruno        Initial version
    09/10/2015        Jose Miguel Fierro    Added BI_EN II Filter
    23/02/2016        Guillermo Muñoz       Changed for allow to include new countries
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI2_COL_PCA_CreacionIncidencia_ctr() {
        objUser = [Select ContactId, Contact.Account.Name, Pais__c from User where Id =: UserInfo.getUserId()][0];
        //objUser = [Select ContactId, Contact.Account.Name, Pais__c from User where Id ='00526000002eGxr'];
        ccpp = loadCC();
        map_rt = loadRecordTypes();
        isTech = true;
        blnCrearContacto = false;
        blnContactoConsultado = false;
    }

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info of Reclamos

    History:

    <Date>            <Author>            <Description>
    05/07/2014        Antonio Moruno       Initial version
    09/10/2015        Jose Miguel Fierro   Added BI_EN II Filter by country
    23/02/2016        Guillermo Muñoz      Changed for allow to include new countries
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  public void loadInfo (){
    try{
      if(BI_TestUtils.isRunningTest()){
        throw new BI_Exception('test');
      }
      newCase = new case();
      List <String> lst_rts = ccpp.BI_Tipos_de_registro__c.split(';');
      if(lst_rts.size()==1){
        caseType = lst_rts[0];
        //no se filtra si existe el tipo de registro en el mapa ya que si no existe queremos que se lance una excepción.
        newCase.RecordTypeId = map_rt.get(lst_rts[0]);
      }
      EqTel = (apexpages.currentpage().getparameters().get('EqTel')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('EqTel')):null;
      newCase.AccountId = BI_AccountHelper.getCurrentAccountId();
      newCase.ParentId = (apexpages.currentpage().getparameters().get('Id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('Id')):null;

      if(EqTel == 'true') {
        newCase.Reason = 'Administrative Request';
        newCase.Subject = 'Prueba [TBD]';
      }
      haveError = true;
      system.debug('newCaseConstructor: '+ newCase);

      if(caseType != null){
        if(caseType == Label.BI_Caso_comercial_2 || caseType == Label.BI_Caso_agrupador){
          isTech = false;
          hasChosen = true;
        }
        //**************Meter aqui futuros tipos de registro**************//
      }

    }catch (exception Exc){
      Id idlog = BI_LogHelper.generate_BILog('BI2_COL_PCA_CreacionIncidencia_ctr.loadInfo', 'Portal Platino', Exc, 'Class');
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.BI_Error_visualforce + idlog));
    }
  }

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        Antonio Moruno
  Company:       Salesforce.com
  Description:   Change Case type

  History:

  <Date>            <Author>            <Description>
  04/07/2014        Antonio Moruno       Initial version
  09/10/2015        Jose Miguel Fierro   Added BI_EN II Filter
  23/02/2016        Guillermo Muñoz      Changed for allow to include new countries
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void changeCaseType() {
        try {
            if(BI_TestUtils.isRunningTest()) {
                throw new BI_Exception('test');
            }

            if(caseType == Label.BI_Caso_comercial_2 || caseType == Label.BI_Caso_agrupador) {
                isTech = false;
                hasChosen = true;
            } else if (caseType==Label.BIIN_TituloSolicInciden) {
                isTech = true;
                hasChosen = true;
                newCase.RecordTypeId  = [SELECT Id, Name FROM RecordType WHERE Name =: Label.BIIN_TituloSolicInciden].Id;
                system.debug(caseType + isTech +' LoadInfo<<<<<<<<<<caseType');
                User myUser= [Select ContactId, Contact.Account.Name from User where Id=:userinfo.getuserId()][0];
                objContact =  [SELECT Name, FirstName, LastName, Email, Phone, MobilePhone, AccountId, Account.Name, Account.OwnerId, Id FROM Contact WHERE id =: objUser.ContactId][0];

                newCase.ContactId   = objContact.Id;
                newCase.AccountId   = objContact.AccountId;
                newCase.OwnerId     = objContact.Account.OwnerId;
                newCase.Reason      = Label.BIIN_CaseReason;

                newCase.BI2_Nombre_Contacto_Final__c    = objContact.FirstName;
                newCase.BI2_Apellido_Contacto_Final__c  = objContact.LastName;
                newCase.BI2_Email_Contacto_Final__c     = objContact.Email;
                newCase.BI_ECU_Telefono_fijo__c         = objContact.Phone;
                newCase.BI_ECU_Telefono_movil__c        = objContact.MobilePhone;
                newCase.BI2_COL_Contacto_de_cierre__c   = (objContact.FirstName + ' ' + objContact.LastName).trim();

                /*newCase.BI2_Nombre_Contacto_Final__c = objContact.FirstName;
                newCase.BI2_Apellido_Contacto_Final__c = objContact.LastName;
                newCase.BI2_Email_Contacto_Final__c = objContact.Email;*/
                //validarUserBackground();
            } else {
                hasChosen = false;
            }

            System.debug(caseType + isTech + ' cambiar tipo funcion changeCaseType <<<<<<<<<< caseType');
        } catch (exception Exc) {
            BI_LogHelper.generate_BILog('BI2_COL_PCA_CreacionIncidencia_ctr.changeCaseType', 'Portal Platino', Exc, 'Class');
        }
    }

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        Antonio Moruno
  Company:       Salesforce.com
  Description:   get reclamo items

  History:

  <Date>            <Author>            <Description>
  04/07/2014        Antonio Moruno       Initial version
  09/10/2015        Jose Miguel Fierro   Added BI_EN II Filter
  23/02/2016        Guillermo Muñoz      Changed for allow to include new countries
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public List<SelectOption> getItems() {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            List<SelectOption> options = new List<SelectOption>();
            List <String> lst_rts = ccpp.BI_Tipos_de_registro__c.split(';');
            if(!lst_rts.isEmpty()) {
                options.add(new SelectOption('-- Seleccionar --', '-- Seleccionar --'));
                for(String str : lst_rts) {
                    if(str == Label.BI_Caso_agrupador){
                        options.add(new SelectOption(str,'Caso agrupador'));
                    } else if(str == Label.BI_Caso_comercial_2) {
                        options.add(new SelectOption(str,Label.BI2_Caso_Comercial));
                    } else if(str==Label.BIIN_TituloSolicInciden) {
                        options.add(new SelectOption(str,Label.BIIN_TituloSolicInciden));
                    }
                }
            }
            else{
                options.add(new SelectOption('fallo','fallo'));
            }

            return options;
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI2_COL_PCA_CreacionIncidencia_ctr.getItems', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda
    Description:   Method that loads the configuration of the new case filtered by country

    History:

    <Date>                          <Author>                    <Change Description>
    23/02/2016                      Guillermo Muñoz             Initial version
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI_Configuracion_Caso_PP__c loadCC(){
        BI_Configuracion_Caso_PP__c aux = new BI_Configuracion_Caso_PP__c();

        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            aux = [SELECT BI_Confidencial__c, BI_COL_Fecha_Radicacion__c, BI_Type__c, BI_Tipos_de_registro__c, BI_Country__c, BI_Origen__c FROM BI_Configuracion_Caso_PP__c WHERE BI_Country__c =: objUser.Pais__c LIMIT 1];
        }catch (Exception exc){
            BI_LogHelper.generate_BILog('BI2_COL_PCA_CreacionIncidencia_ctr.loadCC', 'Portal Platino', Exc, 'Class');
        }

        return aux;
    }

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda
    Description:   Method that return a map with all cases record types

    History:

    <Date>                          <Author>                    <Change Description>
    23/02/2016                      Guillermo Muñoz             Initial version
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Map <String, Id> loadRecordTypes() {
        Map <String,Id> mapa = new Map <String,Id>();
        RecordType [] lst_rt = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Case'];
        if(!lst_rt.isEmpty()) {
            for(RecordType rt : lst_rt) {
                mapa.put(rt.DeveloperName, rt.Id);
            }
        }
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Se ha producido un error de configuración, contacte con su administrador'));
        }
        return mapa;
    }

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        Antonio Moruno
  Company:       Salesforce.com
  Description:   Save Case

  History:

  <Date>            <Author>            <Description>
  04/07/2014        Antonio Moruno       Initial version
  09/10/2015        Jose Miguel Fierro   Added BI_EN II Filter
  13/01/2015        Jose Miguel Fierro   Added contact assignation if case's recordtype = Caso Agrupador
  20/10/2015        Jose Miguel Fierro   Save cases as non-confidential
  23/02/2016        Guillermo Muñoz      Changed for allow to include new countries
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void saveCase(){
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'saveCase' + '   <<<<<<<<<<\n-=#=-\n');
        system.debug('caseType: '+caseType);
        system.debug('Label.BIIN_TituloSolicInciden: '+Label.BIIN_TituloSolicInciden);
        adjuntos=false;
        try{
            if(BI_TestUtils.isRunningTest()) {
                throw new BI_Exception('test');
            }

            if(newCase.RecordTypeId == null) {
                newCase.RecordTypeId = map_rt.get(caseType);
            }
            system.debug('NOMBRE APELLIDO --->' +  newCase.BI2_Nombre_Contacto_Final__c + '---------------- ' +  newCase.BI2_Apellido_Contacto_Final__c + '----ID-----' + newCase.OwnerId );
            if (caseType==Label.BIIN_TituloSolicInciden) {
                newCase.BI_Confidencial__c = false;
                newCase.Status='In Progress';
                if(newCase.BI_COL_Fecha_Radicacion__c == null){
                    newCase.BI_COL_Fecha_Radicacion__c = Datetime.now();
                }
            }
            //////////////////////Asignación de valores de la configuración//////////////////////
            newCase.BI_Country__c                   = ccpp.BI_Country__c;
            newCase.Origin                          = ccpp.BI_Origen__c;
            newCase.Type                            = ccpp.BI_Type__c;
            newCase.BI_Confidencial__c              = ccpp.BI_Confidencial__c;
            if(ccpp.BI_COL_Fecha_Radicacion__c){
                newCase.BI_Col_Fecha_Radicacion__c  = Datetime.now();
            }
            if(caseType == Label.BIIN_TituloSolicInciden) {
                System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Es Incidencia tecnica' + '   <<<<<<<<<<\n-=#=-\n');
                newCase.Origin                        = Label.BI2_COL_Origen_por_defecto_caso_tecnico;
                newCase.TGS_Impact__c                 = Label.BI2_COL_Impacto_por_defecto_caso_tecnico;
                newCase.TGS_Urgency__c                = Label.BI2_COL_Urgencia_por_defecto_caso_tecnico;
                newCase.BIIN_Site__c                  = siteName;
                newCase.TGS_Site_details_contacts__c  = siteFullName;

                if(!blnContactoConsultado) {
                    System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Contacto aun no consultado' + '   <<<<<<<<<<\n-=#=-\n');
                    System.debug('\n\n blnCrearContacto '+ blnCrearContacto +'  hasChosen= '+hasChosen+'  \n\n');
                    consultarContactoRoD();
                    System.debug('\n\n blnCrearContacto '+ blnCrearContacto +'  hasChosen= '+hasChosen+'  \n\n');
                    if(blnCrearContacto) {
                        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Contacto no existe en RoD, se deben confirmar los datos' + '   <<<<<<<<<<\n-=#=-\n');
                        haveError = true;
                    } else {
                        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Contacto existe en RoD, se prosigue con el proceso de guardado' + '   <<<<<<<<<<\n-=#=-\n');
                        haveError = PCA_CaseHelper.saveCase(newCase);
                        System.debug('\n\n-=#=-\n' + 'haveError' + ': ' + haveError + '\n-=#=-\n');
                    }
                } else  {
                    System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Contacto previamente consultado' + '   <<<<<<<<<<\n-=#=-\n');
                    //isTech=false;
                    if(blnCrearContacto) actualizarDatosContacto();
                    newCase.BI2_COL_Contacto_de_cierre__c = (newCase.BI2_Nombre_Contacto_Final__c + ' ' + newCase.BI2_Apellido_Contacto_Final__c).trim();
                    haveError = PCA_CaseHelper.saveCase(newCase);
                    System.debug('\n\n-=#=-\n' + 'haveError' + ': ' + haveError + '\n-=#=-\n');
                }
            } else {
                System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'NO es Incidencia tecnica' + '   <<<<<<<<<<\n-=#=-\n');
                haveError = PCA_CaseHelper.saveCase(newCase);
                System.debug('\n\n-=#=-\n' + 'haveError' + ': ' + haveError + '\n-=#=-\n');
            }
            /////////////////////////////////////////////////////////////////////////////////////

            System.debug('\n\n-=#=-\n' + 'newCase' + ': ' + newCase + '\n-=#=-\n');
        } catch(DMLException objDMLException) {
            System.debug('\n\n-=#=-\n' + 'saveCase - objDMLException.getMessage()' + ': ' + objDMLException.getMessage() + '\n-=#=-\n');
            //ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.WARNING, objDMLException));
            ApexPages.addMessages(objDMLException);
            BI_LogHelper.generate_BILog('BI2_COL_PCA_CreacionIncidencia_ctr.saveCase', 'Portal Platino', objDMLException, 'Class');
        }
    }

    public void saveCaseAdjunto() {
        try{
            if(BI_TestUtils.isRunningTest()) {
                    throw new BI_Exception('test');
            }

            if(newCase.RecordTypeId == null) {
                    newCase.RecordTypeId = map_rt.get(caseType);
            }
            system.debug('NOMBRE APELLIDO --->' +  newCase.BI2_Nombre_Contacto_Final__c + '---------------- ' +  newCase.BI2_Apellido_Contacto_Final__c + '----ID-----' + newCase.OwnerId );
            if (caseType==Label.BIIN_TituloSolicInciden) {
                    newCase.BI_Confidencial__c = false;
                    newCase.Status='In Progress';
                    if(newCase.BI_COL_Fecha_Radicacion__c == null){
                            newCase.BI_COL_Fecha_Radicacion__c = Datetime.now();
                    }
            }
            //////////////////////Asignación de valores de la configuración//////////////////////
            newCase.BI_Country__c                   = ccpp.BI_Country__c;
            newCase.Origin                          = ccpp.BI_Origen__c;
            newCase.Type                            = ccpp.BI_Type__c;
            newCase.BI_Confidencial__c              = ccpp.BI_Confidencial__c;
            if(ccpp.BI_COL_Fecha_Radicacion__c){
                    newCase.BI_Col_Fecha_Radicacion__c  = Datetime.now();
            }
            if(caseType == Label.BIIN_TituloSolicInciden)
            {
                    newCase.Origin                        = Label.BI2_COL_Origen_por_defecto_caso_tecnico;
                    newCase.TGS_Impact__c                 = Label.BI2_COL_Impacto_por_defecto_caso_tecnico;
                    newCase.TGS_Urgency__c                = Label.BI2_COL_Urgencia_por_defecto_caso_tecnico;
                    newCase.BIIN_Site__c                  = siteName;
                    newCase.TGS_Site_details_contacts__c  = siteFullName;
                    System.debug('\n\n blnCrearContacto '+ blnContactoConsultado +'  hasChosen= '+hasChosen+'  \n\n');
                    if(!blnContactoConsultado) {
                        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Contacto aun no consultado' + '   <<<<<<<<<<\n-=#=-\n');
                        System.debug('\n\n blnCrearContacto '+ blnCrearContacto +'  hasChosen= '+hasChosen+'  \n\n');
                        consultarContactoRoD();
                        System.debug('\n\n blnCrearContacto '+ blnCrearContacto +'  hasChosen= '+hasChosen+'  \n\n');
                        if(blnCrearContacto) {
                            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Contacto no existe en RoD, se deben confirmar los datos' + '   <<<<<<<<<<\n-=#=-\n');
                            haveError = true;
                        } else {
                            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Contacto existe en RoD, se prosigue con el proceso de adjuntar archivos' + '   <<<<<<<<<<\n-=#=-\n');
                            newCase.BI2_COL_Contacto_de_cierre__c = (newCase.BI2_Nombre_Contacto_Final__c + ' ' + newCase.BI2_Apellido_Contacto_Final__c).trim();
                            haveError = PCA_CaseHelper.saveCase(newCase);
                            isTech=false;
                            adjuntos=true;
                            hasChosen=false;
                            lstmyDoc=new List<Attachment>();
                            listaAdjuntos_Name= new List<String>();
                            contAdjuntos = 0;
                        }
                    } else {
                        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Contacto previamente consultado' + '   <<<<<<<<<<\n-=#=-\n');
                        if(blnCrearContacto) actualizarDatosContacto();
                        newCase.BI2_COL_Contacto_de_cierre__c = (newCase.BI2_Nombre_Contacto_Final__c + ' ' + newCase.BI2_Apellido_Contacto_Final__c).trim();
                        haveError = PCA_CaseHelper.saveCase(newCase);
                        isTech=false;
                        adjuntos=true;
                        hasChosen=false;
                        lstmyDoc=new List<Attachment>();
                        listaAdjuntos_Name= new List<String>();
                        contAdjuntos = 0;
                    }
            }
            /////////////////////////////////////////////////////////////////////////////////////

            System.debug('\n\n-=#=-\n' + 'newCase' + ': ' + newCase + '\n-=#=-\n');
        } catch(DMLException objDMLException) {
            System.debug('\n\n-=#=-\n' + 'saveCase - objDMLException.getMessage()' + ': ' + objDMLException.getMessage() + '\n-=#=-\n');
            //ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.WARNING, objDMLException));
            ApexPages.addMessages(objDMLException);
            BI_LogHelper.generate_BILog('BI2_COL_PCA_CreacionIncidencia_ctr.saveCase', 'Portal Platino', objDMLException, 'Class');
        }
    }

    public void consultarContactoRoD() {
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'consultarContactoRoD' + '   <<<<<<<<<<\n-=#=-\n');

        if (caseType==Label.BIIN_TituloSolicInciden) {//Hacemos la llamada al webService de creación, controlaremos que tengamos adjuntos o no
            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Incidencia tecnica' + '   <<<<<<<<<<\n-=#=-\n');
            BIIN_Obtener_Token_BAO ot = new BIIN_Obtener_Token_BAO();
            String authorizationToken   = '';

            // Consulta del contacto
            BIIN_Obtener_Contacto_WS cfs = new BIIN_Obtener_Contacto_WS();
            BIIN_Obtener_Contacto_WS.ContactSalida objContactSalida = new BIIN_Obtener_Contacto_WS.ContactSalida();
            objContactSalida = cfs.getContact(authorizationToken, '', '', newCase.BI2_Email_Contacto_Final__c, newCase.AccountId);

            System.debug('\n\n-=#=-\n' + 'objContactSalida' + ': ' + objContactSalida + '\n-=#=-\n');

            if(objContactSalida != null && objContactSalida.outputs != null && !objContactSalida.outputs.output.isEmpty() && objContactSalida.outputs.output[0].code == null) {
                if(objContactSalida.outputs.output[0].code == null) {
                    for(Integer i = 0; i < objContactSalida.outputs.output.size(); i++) {
                        newCase.BI2_Apellido_Contacto_Final__c = objContactSalida.outputs.output[i].lastName;
                        newCase.BI2_Nombre_Contacto_Final__c = objContactSalida.outputs.output[i].firstName;
                        newCase.BI2_Email_Contacto_Final__c = objContactSalida.outputs.output[i].internetEmail;
                        newCase.BI_ECU_Telefono_fijo__c = objContactSalida.outputs.output[i].phone;
                        newCase.BI_ECU_Telefono_movil__c = objContactSalida.outputs.output[i].mobile;
                    }
                } else {
                    blnCrearContacto = true;
                    haveError=true;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.WARNING, Label.BI2_COL_PCA_Mensaje_contacto_no_existe_RoD));
                }
            } else {
                blnCrearContacto = true;
                haveError=true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.WARNING, Label.BI2_COL_PCA_Mensaje_contacto_no_existe_RoD));
            }
        } else {
            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'NO es incidencia tecnica' + '   <<<<<<<<<<\n-=#=-\n');
        }

				blnContactoConsultado = true;
    }

    //************************************************************************
    //*Llamada al WS de creacion del ticket
    //************************************************************************
    public PageReference crearTicketRoD() {
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'crearTicketRoD' + '   <<<<<<<<<<\n-=#=-\n');

        if (caseType==Label.BIIN_TituloSolicInciden) {//Hacemos la llamada al webService de creación, controlaremos que tengamos adjuntos o no
            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Incidencia tecnica' + '   <<<<<<<<<<\n-=#=-\n');
            BIIN_Obtener_Token_BAO ot = new BIIN_Obtener_Token_BAO();
            String authorizationToken = '';

            // Creación del ticket
            BIIN_Creacion_Ticket_WS ct = new BIIN_Creacion_Ticket_WS();
            BIIN_Creacion_Ticket_WS.CamposOutsiders co = new BIIN_Creacion_Ticket_WS.CamposOutsiders();
            Integer nIntentos   = 0;
            Boolean exito       = false;

            newCase.BIIN_Site__c = siteName;
            newCase.TGS_Site_details_contacts__c = siteFullName;
            System.debug('\n\n-=#=-\n' + 'newCase' + ': ' + newCase + '\n-=#=-\n');

            do {
                exito = ct.crearTicket(authorizationToken, newCase, lstAdjuntosRoD, co);
                System.debug('\n\n-=#=-\n' + 'exito' + ': ' + exito + '\n-=#=-\n');
                nIntentos++;
            } while((nIntentos<3) && (exito==false));

            if(Test.isRunningTest()) {
                exito = false;
            }

            String StringWSCreacion = ct.StringWS;
            System.debug('\n\n-=#=-\n' + 'StringWSCreacion' + ': ' + StringWSCreacion + '\n-=#=-\n');

            if(!exito) {
                //lanzar la tarea porque no hemos podido contactar con el bao
                newCase.Status = 'Confirmation pending';
                newCase.BIIN_Descripcion_error_integracion__c = 'Es necesario relanzar manualmente el ticket,pulse el botón Relanzar';
                StringWSCreacion = 'PENDING';
                //MOSTRAR EL POPUP
                try {
                    upsert newCase;
                } catch(DmlException e) {
                    System.debug('\n\n-=#=-\n' + 'e.getMessage()' + ': ' + e.getMessage() + '\n-=#=-\n');
                    system.debug('Error al insertar el estado del caso: '+e);
                }

                Task tarea = new Task();
                tarea.WhatId = newCase.Id;
                tarea.WhoId = newCase.ContactId;
                tarea.Subject = newCase.Subject;
                //tarea.OwnerId = [SELECT BI2_asesor__c FROM Account WHERE Id =: caso.AccountId].Id; //El campo Custom es el asesor asociado al Cliente (Userinfo.getUserId();)
                tarea.Description = newCase.Description;
                //tarea.Status = caso.Status;

                System.debug('\n\n-=#=-\n' + 'Task' + ': ' + newCase.Subject + newCase.Description + newCase.Status + '\n-=#=-\n');
                insert tarea;
            } else {
                System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Exito!!!' + '   <<<<<<<<<<\n-=#=-\n');
            }
        } else {
            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'NO es incidencia tecnica' + '   <<<<<<<<<<\n-=#=-\n');
        }

        return null;
    }

    public void GuardarUbicacion() {
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'GuardarUbicacion' + '   <<<<<<<<<<\n-=#=-\n');
        newCase.BIIN_Site__c                    = Apexpages.currentPage().getParameters().get('UbicacionController');
        newCase.TGS_Site_details_contacts__c    = Apexpages.currentPage().getParameters().get('UbicacionFullController');

        siteName      = newCase.BIIN_Site__c;
        siteFullName  = newCase.TGS_Site_details_contacts__c;

        System.debug('\n\n-=#=-\n' + 'Ubicación' + ': ' + siteName + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'Ubicación Full' + ': ' + siteFullName + '\n-=#=-\n');
    }

    public PageReference onclickCerrar() {
		System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'onclickCerrar' + '   <<<<<<<<<<\n-=#=-\n');

        System.debug('\n\n-=#=-\n' + 'listaAdjuntos_Name' + ': ' + listaAdjuntos_Name + '\n-=#=-\n');
        lstmyDoc=[Select Name,Body,ID from Attachment where ParentId =:newCase.id];
        System.debug('\n\n-=#=-\n' + 'lstmyDoc' + ': ' + lstmyDoc + '\n-=#=-\n');
        try {
            if(!lstmyDoc.isEmpty()) {
                System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Tiene adjuntos' + '   <<<<<<<<<<\n-=#=-\n');

                //insert lstmyDoc;

                lstAdjuntosRoD = new List<BIIN_Creacion_Ticket_WS.Adjunto>();
                for(Attachment att : lstmyDoc) {
                    System.debug('Adjunto nombre: ' + att);
                    BIIN_Creacion_Ticket_WS.Adjunto a = new BIIN_Creacion_Ticket_WS.Adjunto(att.Name, att.Body, att.Id);
                    System.debug('Adjunto nombre: ' + a);

                    lstAdjuntosRoD.add(a);
                }
            } else {
                System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'No tiene adjuntos' + '   <<<<<<<<<<\n-=#=-\n');
            }

            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Acá llama a la creación en RoD' + '   <<<<<<<<<<\n-=#=-\n');
            crearTicketRoD();
            return new PageReference('javascript:parent.jQuery.fancybox.close(), window.parent.location.reload()');
        } catch(Exception ex) {
            System.debug('\n\n-=#=-\n' + 'GuardarTicketAdjuntoSF - DmlException' + ': ' + ex.getMessage() + '\n-=#=-\n');
            ApexPages.addMessages(ex);
            return null;
        }
    }

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Antonio Moruno
	Company:       Salesforce.com
	Description:   Attach files in Reclamos

	History:

	<Date>            <Author>          	<Description>
	25/06/2014        Antonio Moruno       Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public void attachFile() {
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'attachFile' + '   <<<<<<<<<<\n-=#=-\n');
		try
		{
			// if(BI_TestUtils.isRunningTest()){
			// 				throw new BI_Exception('test');
			// 		}
			//List<Case> cases = [select Id from Case where Id = :caseI Order by CreatedDate];
       //if(myDoc == null) {
         //       myDoc = new Attachment();
           // }
        //myDoc.ParentId = newCase != null ? newCase.Id : null;

      System.debug('\n\n-=#=-\n' + 'adjunto1' + ': ' + adjunto1 + '\n-=#=-\n');
      System.debug('\n\n-=#=-\n' + 'newCase' + ': ' + newCase + '\n-=#=-\n');

			//if(myDoc.Name!=null && newCase !=null)
      if(nombre1 !=null && newCase !=null)
			{
        lstmyDoc=lstmyDoc==null?new List<Attachment>():lstmyDoc;
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Tiene caso y adjunto' + '   <<<<<<<<<<\n-=#=-\n');
				system.debug('\n\n '+ contAdjuntos +'  DENTRODEIF---myDoc---->' + myDoc + '   newCase.ID '+newCase+'  \n\n');
				if(contAdjuntos < 3)
				{ //metido por Alex (sólo borrar el if en sí, no lo de dentro)
					//myDoc.ParentId = newCase.ID;
          //system.debug('****listaAdjuntos-2: '+listaAdjuntos);
          system.debug('****listaAdjuntos_Name-2: '+listaAdjuntos_Name);
          //listaAdjuntos.add(myDoc);
          listaAdjuntos_Name.add(nombre1);
          contAdjuntos = contAdjuntos + 1;//metido por Alex
          system.debug('\n\n '+ contAdjuntos +'  DENTRODEIF---myDoc---->' + myDoc + '   newCase.ID '+newCase+'  \n\n');
          haveError=false;
          noError=true;
          myDoc=new Attachment(ParentId=newCase.id,body=adjunto1,Name=nombre1);
          system.debug('****myDoc: '+myDoc);
          //lstmyDoc.add(myDoc);
          insert myDoc;
					myDoc = new Attachment();
          //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'my error msg');
					//ApexPages.addMessage(myMsg);
				} //llave de cierre del if metido por Alex
				else limiteAdjuntos = true; //metido por alex
			}
			else
			{
                System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'NO Tiene caso o adjunto' + '   <<<<<<<<<<\n-=#=-\n');
				haveError=true;
				noError=false;
				//ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'El archivo elegido no es válido');
			}
		}
		catch (exception e) {
			haveError = true;
			noError=false;
            System.debug('\n\n-=#=-\n' + 'e.getMessage()' + ': ' + e.getMessage() + '\n-=#=-\n');
            System.debug('\n\n-=#=-\n' + 'e.getStackTraceString()' + ': ' + e.getStackTraceString() + '\n-=#=-\n');
			//BI_LogHelper.generate_BILog('PCA_Reclamo_NewAttachmentCtrl.attachFile', 'Portal Platino', Exc, 'Class');
            ApexPages.addMessages(e);
		}
	}

    public void actualizarDatosContacto() {
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'actualizarDatosContacto' + '   <<<<<<<<<<\n-=#=-\n');
        if(newCase != null) {
            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'actualizarDatosContacto - Actualiza Usuario' + '   <<<<<<<<<<\n-=#=-\n');
            objUser.FirstName   = newCase.BI2_Nombre_Contacto_Final__c;
            objUser.LastName    = newCase.BI2_Apellido_Contacto_Final__c;
            objUser.Email       = newCase.BI2_Email_Contacto_Final__c;
            objUser.Phone       = newCase.BI_ECU_Telefono_fijo__c;
            objUser.MobilePhone = newCase.BI_ECU_Telefono_movil__c;
            update objUser;

            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'actualizarDatosContacto - Actualiza Contacto' + '   <<<<<<<<<<\n-=#=-\n');
            objContact.FirstName   = newCase.BI2_Nombre_Contacto_Final__c;
            objContact.LastName    = newCase.BI2_Apellido_Contacto_Final__c;
            objContact.Email       = newCase.BI2_Email_Contacto_Final__c;
            objContact.Phone       = newCase.BI_ECU_Telefono_fijo__c;
            objContact.MobilePhone = newCase.BI_ECU_Telefono_movil__c;
            update objContact;
        }
    }
}