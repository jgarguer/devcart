@isTest
private class BI_Clone_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Ignacio Llorca
	Company:		Salesforce.com
	Description:	Test class to manage the coverage code for BI_AccountHelper class 
	History:
	<Date>			<Author>				<Change Description>
	12/09/2014		Ignacio Llorca			Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static{ BI_TestUtils.throw_exception = false; }

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Ignacio Llorca
	Company:		Salesforce.com
	Description:	Test method to manage the code coverage for BI_Clone.obtainOppClone
	History:
	<Date>			<Author>				<Change Description>
	12/09/2014		Ignacio Llorca			Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void obtainOppCloneTest() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
		userTGS.TGS_Is_BI_EN__c = true;
		insert userTGS;

		List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
		List<Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);
		List<Opportunity> lst_opp = BI_DataLoad.loadOpportunitiesforButton(1, lst_acc[0].Id);

		Test.startTest();
		Opportunity oppClone = BI_Clone.obtainOppClone(lst_opp[0].Id);
		system.assert(lst_opp[0].Name == oppClone.Name);
		oppClone = BI_Clone.obtainOppClone(null);
		Test.stopTest();
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:			Ignacio Llorca
	Company:		Salesforce.com
	Description:	Test method to manage the code coverage for BI_Clone.obtainOliCloneTest
	History:
	<Date>			<Author>				<Change Description>
	12/09/2014		Ignacio Llorca			Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void obtainOliCloneTest() {
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
		userTGS.TGS_Is_BI_EN__c = true;
		insert userTGS;

		List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
		List<Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);
		List<Opportunity> lst_opp = BI_DataLoad.loadOpportunitiesforButton(1, lst_acc[0].Id);
		List<OpportunityLineItem> lst_oli = new List <OpportunityLineItem>();

		Product2 prod = new Product2(Name = 'Test Prod', Family = 'Test family', CurrencyIsoCode = 'EUR');
		insert prod;

		Id pricebookId = Test.getStandardPricebookId();
		PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, CurrencyIsoCode = 'EUR', 
                                                          Product2Id = prod.Id,	UnitPrice = 10000, IsActive = true);
		insert standardPrice;

		Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true, CurrencyIsoCode = 'EUR');
		insert customPB;

		PricebookEntry customPrice = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id,
														UnitPrice = 12000, CurrencyIsoCode = 'EUR', IsActive = true);
		insert customPrice;

		for (Integer i = 0; i<200; i++){    	
			OpportunityLineItem l_item = new OpportunityLineItem(OpportunityId=lst_opp[0].Id, UnitPrice=100,
																Quantity=1, PricebookEntryId = customPrice.Id);
			lst_oli.add(l_item);
		}
		insert lst_oli;

		Test.startTest();
		List <OpportunityLineItem> oliClone = BI_Clone.obtainOliClone(lst_opp[0].Id);
		system.assertEquals(oliClone.size(),[SELECT ID FROM OpportunityLineItem].size());
		oliClone = BI_Clone.obtainOliClone(null);
		Test.stopTest();
	}
}