/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Miguel Cabrera
 Company:       New Energy Aborda
 Description:   Test class for BI_O4_GatesRequiredController class
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 28/08/2016          Miguel Cabrera             Initial Version
 20/09/2017          Angel F. Santaliestra      Add the mandatory fields on account creation: BI_Subsegment_Regional__c,BI_Territory__c
 25/09/2017          Jaime Regidor              Fields BI_Subsector__c and BI_Sector__c added
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_GatesRequiredController_TEST {
	
  static{
    BI_TestUtils.throw_exception = false;
  }
	
	@isTest static void test_method_one() {
		
		BI_TestUtils.throw_exception = false;
		RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' Limit 1];
		User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();
		List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'test',
                                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Segment__c = 'Empresas',
                                  BI_Subsector__c = 'test', //25/09/2017
                                  BI_Sector__c = 'test', //25/09/2017
                                  BI_Subsegment_local__c = 'Top 1',
                                  BI_Subsegment_Regional__c = 'test',
                                  BI_Territory__c = 'test',
                            	    RecordTypeId = rt1.Id
                                  );    
        lst_acc1.add(acc1);
        System.runAs(usu){
        insert lst_acc1;
        }

		List<Account> lst_acc = new List<Account>();       
        Account acc = new Account(Name = 'test',
                                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Segment__c = 'Empresas',
                                  BI_Subsector__c = 'test', //25/09/2017
                                  BI_Sector__c = 'test', //25/09/2017
                                  BI_Subsegment_local__c = 'Top 1',
                                  BI_Subsegment_Regional__c = 'test',
                                  BI_Territory__c = 'test',
                                  ParentId = lst_acc1[0].Id
                                  );    
        lst_acc.add(acc);
        System.runAs(usu){
        insert lst_acc;
        }

        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

        Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                                          AccountId = lst_acc[0].Id,
                                          BI_Ciclo_ventas__c = Label.BI_Completo, 
                                          RecordTypeId = rt.Id
                                          );    
        insert opp;

		Quote proposal = new Quote();
		proposal.Name = 'Test';
		proposal.OpportunityId = opp.Id;

		Test.startTest();
		insert proposal;

		ApexPages.StandardController standardC = new ApexPages.StandardController(proposal); 
		BI_O4_GatesRequiredController controller = new BI_O4_GatesRequiredController(standardC);

     	controller.setGatesRequired();
     	controller.cancel();
     	System.assertNotEquals(null, proposal);

		standardC = new ApexPages.StandardController(proposal); 
		controller = new BI_O4_GatesRequiredController(standardC);
		controller.quote = null;
		try
		{
     		controller.setGatesRequired();
		}
		catch(Exception e)
		{
			System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
		}
     	
     	Test.stopTest();
	}	
	
}