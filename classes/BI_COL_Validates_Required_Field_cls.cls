/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       29-Abr-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
public with sharing class BI_COL_Validates_Required_Field_cls 
{
    /**
    * Método que valida los campos obligatorios de las MS segun su código de referencia
    * @param lista MS
    */
    public static void fnValidates_Required_Field_MS( list<BI_COL_Modificacion_de_Servicio__c> lstMSNew )
    {
        String msg = 'Por favor revise que los siguientes campos contengan algún valor: <br />';
        list<string> lstIds = new list<string>();
        string sIds = '';
        map<string,BI_COL_Modificacion_de_Servicio__c> mapMS = new map<string,BI_COL_Modificacion_de_Servicio__c>();
        
        for( BI_COL_Modificacion_de_Servicio__c obj : lstMSNew )
        {
            lstIds.add( obj.Id );
            sIds += ( sIds == '' ? '(\'' + obj.Id + '\'' : ',\'' + obj.Id +'\'');
            mapMS.put( obj.Id, obj );
        }
        sIds += ')';
        //Se tiene que hacer este quiery para traer el valor del campo BI_COL_LegadoID__c
        List<BI_COL_Modificacion_de_Servicio__c> lstMS = [
            select  Id, Name, BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c
            from    BI_COL_Modificacion_de_Servicio__c
            where   Id IN: lstIds ];
        //Consultar todos los campos que apliquen parea la descripcion de referencia del producto en la MS
        List<BI_COL_CamposObligatorios__c> lstCfgCampos = [
            select  BI_COL_Campo_Api__c, BI_COL_Nombre_Etiqueta__c, BI_COL_Objeto__c, BI_COL_Codigo_Referencia__c, Name 
            from    BI_COL_CamposObligatorios__c
            where   BI_COL_Objeto__c = 'BI_COL_Modificacion_de_Servicio__c'];
        
        map<string,string> mCosRef = new map<string,string>();
        
        for( BI_COL_Modificacion_de_Servicio__c objMS : lstMS )
        {
            if( !mCosRef.containsKey( objMS.BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c ) )
                mCosRef.put( objMS.BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c, objMS.BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c );
        }
        //Construcción del SQL
        String sqlSelect = 'select Id ';
        String sqlBody = ' from BI_COL_Modificacion_de_Servicio__c where Id IN '+ sIds;
        String sqlFieldsNull = '';
        String fields = '';
        boolean blExecute = false;

        for( BI_COL_CamposObligatorios__c objValidacion : lstCfgCampos )
        {
            if(  mCosRef.containsKey( objValidacion.BI_COL_Codigo_Referencia__c ) )
            {
                //Armar select de la consulta
                sqlSelect += ' , ' + objValidacion.BI_COL_Campo_Api__c;
                fields += ( fields == '' ) ?  objValidacion.BI_COL_Campo_Api__c : ' , ' + objValidacion.BI_COL_Campo_Api__c;
                //Validar Campos que tengan valores nulos
                sqlFieldsNull += ( sqlFieldsNull == '' ? ' and ( ' + objValidacion.BI_COL_Campo_Api__c + ' = null ' : ' OR ' + objValidacion.BI_COL_Campo_Api__c + ' = null ' ) ;
                blExecute = true;
            }
        }
        sqlFieldsNull += ')';
        //Armar complemento de la consulta
        sqlSelect += sqlBody + ' ' + ( sqlFieldsNull != ')' ? sqlFieldsNull : '' );
        system.debug('\n\n##sqlSelect: '+ sqlSelect +'\n\n');
        //Ejecuta el query
        list<BI_COL_Modificacion_de_Servicio__c> lstMSValidaNulo = ( blExecute ?  Database.query( sqlSelect ) : null ) ;
        //Si no existe registros con campos nulos se para la ejecución
        if( lstMSValidaNulo == null || lstMSValidaNulo.size() == 0 )
            return;
        else 
        {
            for( BI_COL_Modificacion_de_Servicio__c obj : lstMSValidaNulo )
                if( mapMS.containsKey( obj.Id ) )
                    obj.addError( msg + ' ' + fields +'.');
        }               
    }
}