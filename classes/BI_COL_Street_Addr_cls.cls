public class BI_COL_Street_Addr_cls 
{
	public static List<BI_Sede__c> lstSucFinalActualizar=new List<BI_Sede__c>();

	@Future(callout=true)
	public static void EjecutaCallejero( List<Id> lstId){
		//Busca la Sucursal
		String strCodDane;
		String strCodDep;
		String strMunicipio;
		String strLocalidad;
		String strBarrio;
		String strCadenaTotal;
		
		List<BI_Sede__c> lstOs = BuscaDireccion(lstId);
		
		try{
			//Variable de Respuesta
			String strRta;
			//Inicializa la Clase del WS
			BI_COL_Callejero_wsdl.WebServiceSoap ws_c = new BI_COL_Callejero_wsdl.WebServiceSoap();
						
			//Valida que no sea null
			if(lstOs != null){
				
				//crear lista de ID de las ciudades involucradas
				set<Id> stIdCiudades=new set<Id>();
				
				for(BI_Sede__c direccion: lstOs){
					
					if(!stIdCiudades.contains(direccion.BI_COL_Ciudad_Departamento__c))
					{
						stIdCiudades.add(direccion.BI_COL_Ciudad_Departamento__c);
					}
				}
				
				//Busca la Ciudad y el Departamente
				map<Id,BI_Col_Ciudades__c> mapaCiudad = BI_COL_Street_Addr_cls.BuscaCiudadDepartamente(stIdCiudades);
				BI_COL_Callejero_wsdl.ParametrosGeo param = null;
				BI_COL_Callejero_wsdl.ArrayOfParametrosGeo lstparam = new BI_COL_Callejero_wsdl.ArrayOfParametrosGeo();
				lstparam.ParametrosGeo = new List <BI_COL_Callejero_wsdl.ParametrosGeo>();
				//Valida que no sea null
				if(mapaCiudad != null){
					//Departamente
					for(BI_Sede__c Direccion : lstOs){
						
						BI_Col_Ciudades__c ciudad = mapaCiudad.get(Direccion.BI_COL_Ciudad_Departamento__c);
						
						if(ciudad!=null){
							
							strCodDane = ciudad.BI_COL_Codigo_DANE__c;
							//Evalua
							System.debug('\n\n##SIZE##' + strCodDane.length() + '\n\n');
							if( strCodDane.length() == 7 )
								strCodDane = '0' + strCodDane;
							strCodDep = strCodDane.substring( 0, 2 );
							System.debug('\n\n##DEP##' + strCodDep + '\n\n');
							//Municipio
							strMunicipio = strCodDane.substring( 2, 5 );
							System.debug('\n\n##MUN##' + strMunicipio + '\n\n');
							//Localidad
							strLocalidad = strCodDane.substring( 5, 8 );
							System.debug('\n\n##LOC##' + strLocalidad + '\n\n');
							//Barrio
							
							
							if( Direccion.BI_Localidad__c <> null )
								strBarrio = '11|' + Direccion.BI_Localidad__c;
							else
								strBarrio = '11|PENDIENTE';
								
							param=new BI_COL_Callejero_wsdl.ParametrosGeo();
							param.id=param.direccion=Direccion.Id;
							param.departamento=strCodDep;
							param.municipio=strMunicipio;
							param.localidad=strLocalidad;
							param.barrio=strBarrio;//--->falta
							param.direccion=BI_COL_Utility_cls.quitarCaractees(Direccion.BI_Direccion__c);//--->falta
							System.debug('\n\n##oooo----->'+lstparam);
							lstparam.ParametrosGeo.add(param);
							
						}
					} 
					
					//llenar información del request
					System.debug('\n\n##eeeee----->'+lstparam);
					try{
						strRta = ws_c.Interfaz1(lstparam);
					}catch(Exception e){
						System.debug('..==.. Error en la conexión con el srvicio web de Callejero ');
						//---> FALTA POR ACTIVAR creaLogTransaccion(''+e,lstOs,'CALLEJERO');
					}
					System.debug( '\n\n ##strRta##' + strRta + '\n\n' );
					//Actualiza la Direccion de Sucursal
					
					BI_COL_Street_Addr_cls.ActualizaSucursal(lstId, lstOs, strRta);
				}
			}
		}
		catch( System.exception ex ){
			System.debug( '\n\n ##Error##' + ex + '\n\n' );
		}
	}

	public static void ActualizaSucursalError( BI_Sede__c oS, String ex ){
		oS.BI_DireccionNormalizada__c = 'Error: ' + ex;
		oS.BI_COL_Estado_callejero__c = 'Error Conexión';
		update oS;
	}

	public static map<Id,BI_Col_Ciudades__c> BuscaCiudadDepartamente( set<Id> strId ){
		//Se busca el objeto Ciudad por Id
		List<BI_Col_Ciudades__c> lstC = [Select Id, BI_COL_Codigo_Depto__c, BI_COL_Codigo_ciudad__c, BI_COL_Codigo_Zona__c, BI_COL_Codigo_DANE__c
								from    BI_Col_Ciudades__c
								where   Id IN: strId];
		//Valida si tiene registros
		map<Id,BI_Col_Ciudades__c>mapaCiudad=new map<Id,BI_Col_Ciudades__c>();
		if( lstC.size() > 0 ){
			
			for(BI_Col_Ciudades__c ciudad: lstC){
				mapaCiudad.put(ciudad.Id, ciudad);  
			}
			return mapaCiudad;
			
		}else{
			return null;
		}
	}

	public static List<BI_Sede__c> BuscaDireccion( List<Id> lstID ){
		//Consulta la sucursal actual
		System.debug('\n\n lstID: '+lstID+' \n\n');
		List<BI_Sede__c> lstS = [Select BI_Apartamento__c,BI_Codigo_de_direccion__c,BI_Codigo_postal__c,BI_COL_BW_maximo2__c,
										BI_COL_BW_maximo__c,BI_COL_Ciudad_Departamento__c,BI_COL_Complemento_1__c,
										BI_COL_Complemento_2__c,BI_COL_Conectividad_basica__c,BI_COL_Direccion_split__c,
										BI_COL_Estado_callejero__c,BI_COL_Estado_Cobertura2__c,BI_COL_Estado_Cobertura__c,
										BI_COL_Fecha_consulta2__c,BI_COL_Fecha_consulta__c,BI_COL_Gestion_callejero__c,
										BI_COL_Latitud__c,BI_COL_Linea_basica__c,BI_COL_Longitud__c,BI_COL_Numero_de_puertos2__c,
										BI_COL_Numero_de_puertos__c,BI_COL_Placa__c,BI_COL_Sucursal_en_uso__c,BI_COL_Tipo_de_sede__c,
										BI_COL_Usuario_consulta2__c,BI_COL_Usuario_consulta__c,
										BI_DireccionNormalizada__c,BI_Direccion__c,BI_Distrito__c,BI_Filtro_de_busqueda__c,
										BI_ID_de_la_sede__c,BI_interno_id_direccion__c,BI_Localidad__c,BI_Longitud__c,
										BI_Longitud__Latitude__s,BI_Longitud__Longitude__s,BI_Numero__c,BI_n_id_direccion__c,
										BI_Country__c,BI_Piso__c,BI_Provincia__c,BI_sistema_legado__c,Estado__c,Id,Name,
										LastModifiedById,TEMPEXT_ID__c,
										BI_COL_Ciudad_Departamento__r.Name,BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c,
										BI_COL_Ciudad_Departamento__r.BI_COL_Regional__c
									from    BI_Sede__c
									where   Id IN: lstID];
		//Valida si tiene registros
		if( lstS.size() > 0 )
			return lstS;
		else
			return null;
	}
	/**
	*   Actualiza la dirección de la Sucursal
	*   @param: Objeto Sucursal__c
	*   @param: String Dirección
	*   @return: Void
	*/
	public static void ActualizaSucursal( List<Id>idSuc, List<BI_Sede__c> lstOs, String strX){
		try{
			//Objeto BI_COL_Opcion_callejero__c
			BI_COL_Opcion_callejero__c oOc;
			//Direccion Estandarizada
			oLstOpcion lstOp = new oLstOpcion(strX);
			set<Id> stIdSucActualizar = new set<Id>();
			//Elimino las opciones de Sucursales
			List<BI_COL_Opcion_callejero__c> lstOpE = [Select id from BI_COL_Opcion_callejero__c where BI_COL_Direccion__c IN:idSuc];
			List<BI_COL_Opcion_callejero__c> lstNuevoOpcionCallejero = new List<BI_COL_Opcion_callejero__c>();
			List<BI_Sede__c>lstSucursalesActualizar=new List<BI_Sede__c>();
			List<BI_Sede__c>lstSucursalesDuplicadas=new List<BI_Sede__c>();
			List<BI_COL_Opcion_callejero__c> lstOpcionesNuevas=new List<BI_COL_Opcion_callejero__c>();
			
			BI_Sede__c sucursal=null;
			//OA: creando un mapa de sucursales para poder obtenerlas una a una facilmente por medio de su ID 
			map<Id,BI_Sede__c> mapaSucursales=new map<Id,BI_Sede__c>(); 
					
			for(BI_Sede__c suc : lstOs){
				suc.BI_COL_Estado_callejero__c = label.BI_COL_LbValor1EstadoCallejero;//'Pendiente';
				mapaSucursales.put(suc.Id,suc);
			}
			 
			if(lstOpE.size()>0)
			{
				delete lstOpE;
			}
			System.debug(':: LISTA DE OPCIONES: ' + lstOp.lstOpcion.size());            
			//Valida si tiene Registros
			if(lstOp.lstOpcion.size() > 0)
			{
				System.debug('\n\n\n:: LISTA DE OPCIONES: ' + lstOp.lstOpcion.size());
				
				for(integer i=0;i<lstOp.lstOpcion.size();i++){
					System.debug('\n\n\n:: imprimiendo valor del OptionRecibido-->'+lstOp.lstOpcion[i]);
					//Actualizar registro de la sucursal
					String direccion = lstOp.lstOpcion[i].strDireccion+' '+lstOp.lstOpcion[i].strPlaca+' '+lstOp.lstOpcion[i].strComplemento1;
				//  oS.Direcci_n_Sucursal__c = direccion.replaceAll('\\*', ' ');
				//  oS.Gesti_n_callejero__c = true;
					
					System.debug('\n\n ---Dirección------['+direccion+']'+'\n\n.-.-. mapaSucursales '+mapaSucursales);
					
					List<String> Lat=new List<String>();
					List<String> Lon =new List<String>();
					
					sucursal=mapaSucursales.get(lstOp.lstOpcion[i].strId);
					sucursal.BI_Direccion__c = direccion;
					sucursal.BI_COL_Gestion_callejero__c = true;
					
					
					if( lstOp.lstOpcion[i].strComplemento2 != '' )
					{
						System.debug(':: GESTIONADO POR CALLEJERO: ' + lstOp.lstOpcion[i]);
						List<String> LonLat = lstOp.lstOpcion[i].strComplemento2.split(' ');
						Lat = LonLat[0].split( ':' );
						Lon = LonLat[1].split( ':' );
						
						sucursal.BI_COL_Latitud__c = Lat[1];
						sucursal.BI_COL_Longitud__c = Lon[1];
					}               
						
					sucursal.BI_COL_Direccion_split__c= lstOp.lstOpcion[i].strDirSplit;
					sucursal.BI_COL_Complemento_1__c = lstOp.lstOpcion[i].strComplemento1;
					sucursal.BI_COL_Complemento_2__c = lstOp.lstOpcion[i].strComplemento2;
					sucursal.BI_COL_Placa__c = lstOp.lstOpcion[i].strPlaca;
					//sucursal.Estado_callejero__c='Validado por callejero';
					
					if(sucursal.BI_COL_Estado_callejero__c.equals(label.BI_COL_LbValor3EstadoCallejero)||sucursal.BI_COL_Estado_callejero__c.equals(label.BI_COL_LbValor2EstadoCallejero)){
						sucursal.BI_COL_Estado_callejero__c = label.BI_COL_LbValor2EstadoCallejero;//'Pendiente con opción';
						lstOpcionesNuevas.add(crearOpcion(lstOp.lstOpcion[i]));
						lstSucursalesDuplicadas.add(sucursal);
					}else{
						sucursal.BI_COL_Estado_callejero__c = label.BI_COL_LbValor3EstadoCallejero;//'Validado por callejero';
					}
					
					if(!stIdSucActualizar.contains(sucursal.Id)){
						lstSucursalesActualizar.add(sucursal);
						stIdSucActualizar.add(sucursal.Id);
					}
				
				
				}
				//Actualiza las sucursales que fueron afectadas
				System.debug('Listado retornado de opciones para crear--->'+lstOpcionesNuevas);
				// Insertar las opciones que se hayan creado 
				if(lstOpcionesNuevas.size()>0)
					insert lstOpcionesNuevas;
				BI_COL_FlagforTGRS_cls.flagTriggers = true;
				
				update lstSucFinalActualizar;
				lstSucFinalActualizar.clear();
				lstSucursalesActualizar=valildarSucursalDuplicada(lstSucursalesActualizar);
				System.debug('\n\n lstSucFinalActualizar'+lstSucFinalActualizar+'\n\n');
				//update lstSucFinalActualizar;
				System.debug('\n\n lstSucFinalActualizar'+lstSucFinalActualizar+'\n\n');
				update lstSucursalesActualizar;
				System.debug('\n\n DESPUES DE LA ACTUALIZACION \n\n');
			//  update oS;
			}
			
		}
		catch( System.exception ex ){
			System.debug( '\n\n##Error##' + ex+ '\n\n' );
		}
	}
	
	
	public static List<BI_Sede__c> valildarSucursalDuplicada(List<BI_Sede__c> lstSucursales){
		
		map <String, List<BI_Sede__c>> mapaClientesSucursales=new map <String, List<BI_Sede__c>>();
		set <String> setSucEncontradas=new set<String>();       
		set<String> stDirecciones=new set<String>();
		set<Id> stIdSucursales=new set<Id>();
		List<BI_Sede__c> lstSucursalesEnviar=new List<BI_Sede__c>();
		System.debug('\n\n Validar duplicada ingresa por aca \n\n');        
		// Sacar listado de ID de clientes involucrados
		
		for(BI_Sede__c suc:lstSucursales){
			
			if(!stDirecciones.contains(suc.BI_Direccion__c)){
				String dir=suc.BI_Direccion__c;
				dir=dir.substring(dir.length()-1,dir.length())==' '?dir.substring(0,dir.length()-1):dir;
				stDirecciones.add(dir);  
			}
			if(!stIdSucursales.contains(suc.Id)){
				stIdSucursales.add(suc.Id); 
			}
		}
		System.debug('\n\n stIdSucursales: '+stIdSucursales+' stDirecciones: '+stDirecciones+' label.BI_COL_LbValor3EstadoCallejero: '+label.BI_COL_LbValor3EstadoCallejero+' \n\n');
		// Listar las sucursales de los clientes involucrados 
		List<BI_Sede__c> lstSuc = [Select  Id, BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c
										from    BI_Sede__c
										where   Id NOT IN: stIdSucursales 
										and     BI_Direccion__c IN: stDirecciones                                      
										and     BI_COL_Estado_callejero__c =: label.BI_COL_LbValor3EstadoCallejero];//'Validado por callejero'
		 
		 system.debug('---lista sucursales encontradas--->'+lstSuc);
		 for(BI_Sede__c suc : lstSuc){
			
			setSucEncontradas.add(suc.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c);
		 }
		 
		for(BI_Sede__c suc2 : lstSucursales)
		{
			if(setSucEncontradas.contains(suc2.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c))
			{
				suc2.BI_COL_Estado_callejero__c = label.BI_COL_LbValor4EstadoCallejero;//'Dirección duplicada';
				suc2.BI_Direccion__c = Label.MensajeErrorDireccion;
			}
			 /*else{
				lstSucursalesEnviar.add(suc2);
			 }
			 lstSucFinalActualizar.add(suc2);*/
		 }

			system.debug('---lista para enviar a TRS--->'+lstSucursalesEnviar);
			return lstSucursales;
	}
	
	
	public static BI_COL_Opcion_callejero__c crearOpcion(oOpcion opcion)
	{
		
			system.debug('Entrando al método de creción de opción');
			BI_COL_Opcion_callejero__c oOc = new BI_COL_Opcion_callejero__c();
			oOc.BI_COL_CodBar__c = opcion.strCodBar;
			oOc.BI_COL_CodDir__c = opcion.strCODIR;
			oOc.BI_COL_Complemento1__c = opcion.strComplemento1;
			oOc.BI_COL_Complemento2__c = opcion.strComplemento2;
			//Valida que strComplemento2 dif vacio
			if( opcion.strComplemento2 != '' )
			{
				List<String> LonLat = opcion.strComplemento2.split(' ');
				List<String> Lat = LonLat[0].split( ':' );
				List<String> Lon = LonLat[1].split( ':' );
				oOc.BI_COL_Latitud__c = Lat[1];
				oOc.BI_COL_Longitud__c = Lon[1];
			}
			oOc.BI_COL_DesBar__c = opcion.strDesBar;
			//oOc.BI_COL_Direccion__c = opcion.strDireccion+' '+opcion.strPlaca+' '+opcion.strComplemento1;
			oOc.BI_COL_DirNueva__c = opcion.strDirNueva;
			oOc.BI_COL_DirSplit__c = opcion.strDirSplit;
			oOc.BI_COL_EstadoRef__c = opcion.strEstadoRef;
			oOc.BI_COL_NSE__c = opcion.strNSE;
			oOc.BI_COL_Placa__c = opcion.strPlaca;
			oOc.BI_COL_Direccion__c = opcion.strId; 
			oOc.BI_COL_Zona__c = opcion.strZona;
			
			return oOc; 
				
	}
	

	public class oLstOpcion {
		
		public List<oOpcion> lstOpcion {get;set;}
		
		public oLstOpcion( String strX ){
			system.debug( '\n\n .-.- strX '+strX );
			oOpcion objOpcion;
			this.lstOpcion = new List<oOpcion>();
			//Carga el XML a Document
			Dom.Document doc = new Dom.Document();
			doc.load( strX );
			//Lee el primer Nodo
			Dom.XMLNode WS = doc.getRootElement();
			//Lista de los Nodos
			List<dom.XMLNode> lsxmlValues = WS.getChildElements();
			//Recorre el Nodo Callejero
			for( dom.XMLNode dxm : lsxmlValues ){
				//Nodos de Interfaz2                
				List<dom.XMLNode> NInterfaz2 = dxm.getChildElements();
				//Recorreo el Nodo de Interfaz2
				for( dom.XMLNode dxmv : NInterfaz2 ){
					//Carga el Objeto
					objOpcion = new oOpcion( dxmv, dxm.getAttribute('id',null) );
					//Agrega el Objeto a la Lista
					this.lstOpcion.add( objOpcion );
				}
			}
		}
	}

	public class oOpcion {
		
		public String strId {get;set;} //OA: variable para identificar la sucursal a tratar
		public String strDireccion {get;set;}
		public String strDirSplit {get;set;}
		public String strPlaca {get;set;}
		public String strComplemento1 {get;set;}
		public String strComplemento2 {get;set;}
		public String strCodBar {get;set;}
		public String strDesBar {get;set;}
		public String strZona {get;set;}
		public String strEstadoRef {get;set;}
		public String strNSE {get;set;}
		public String strDirNueva {get;set;}
		public String strCODIR {get;set;}
		
		public oOpcion( dom.XMLNode dxmv, String strIdSucursal ){
			this.strId=strIdSucursal;
			//Nodo Opcion
			List<dom.XMLNode> NOpcion = dxmv.getChildElements();
			//Recorre el Nodo Opcion
			for( dom.XMLNode dxmvv : NOpcion ){
				if( dxmvv.getName() == 'Id' )
					this.strId = dxmvv.getText();
				//Nodo Direccion
				if( dxmvv.getName() == 'Direccion' )
					this.strDireccion = dxmvv.getText();
				//Nodo DirSplit
				if( dxmvv.getName() == 'DirSplit' )
					this.strDirSplit = dxmvv.getText();
				//Nodo Placa 
				if( dxmvv.getName() == 'Placa' )
					this.strPlaca = dxmvv.getText();
				//Nodo Complemento1
				if( dxmvv.getName() == 'Complemento1' )
					this.strComplemento1 = dxmvv.getText();
				//Nodo Complemento2
				if( dxmvv.getName() == 'Complemento2' )
					this.strComplemento2 = dxmvv.getText();
				//Nodo CodBar
				if( dxmvv.getName() == 'CodBar' )
					this.strCodBar = dxmvv.getText();
				//Nodo DesBar
				if( dxmvv.getName() == 'DesBar' )
					this.strDesBar = dxmvv.getText();
				//Nodo Zona
				if( dxmvv.getName() == 'Zona' )
					this.strZona = dxmvv.getText();
				//Nodo EstadoRef
				if( dxmvv.getName() == 'EstadoRef' )
					this.strEstadoRef = dxmvv.getText();
				//Nodo NSE
				if( dxmvv.getName() == 'NSE' )
					this.strNSE = dxmvv.getText();
				//Nodo DirNueva
				if( dxmvv.getName() == 'DirNueva' )
					this.strDirNueva = dxmvv.getText();
				//Nodo CODIR
				if( dxmvv.getName() == 'CODIR' )
					this.strCODIR = dxmvv.getText();
			}
			system.debug( '\n\n .-.-. strDireccion '+strDireccion );
		}
	}
	

	/***
	* @Author: Carlos Fernando Carvajal 
	* @Company: Avanxo
	* @Description: Fincion que recorre la lista de sedes y los coloca en una lista de ID
	* @History: 
	* Date      |   Author  |   Description
	* 24/06/2015    Carlos Carvajal Creacion
	***/
	public static list<ID> fnGetIDsDireccion(list<BI_Sede__c> lstNuevo)
	{
		List<ID> idDirecciones=new List<ID>();

		System.debug('\n\n lstNuevo======>>>>'+lstNuevo);
		
		for(BI_Sede__c sede:lstNuevo)
		{
			if(sede.BI_COL_Estado_callejero__c=='Pendiente' && sede.BI_Country__c=='Colombia')
			{
				idDirecciones.add(sede.id);
			}
		}
		System.debug(' \n\n Paso Linea 679' + idDirecciones);
		return idDirecciones;
	}

	public static void validateDelete(list<BI_Sede__c> lstOld)
	{
		List<ID> idDir=new list<ID>();
		map<String,Integer> mapConteo=new map<String,Integer>();
		for(BI_Sede__c sed:lstOld){ idDir.add(sed.id);      }
		List<AggregateResult> result  = [select count(Id) total,BI_Sede__c sede from BI_Punto_de_instalacion__c where BI_Sede__c in:idDir group by BI_Sede__c ];
		for (AggregateResult ar : result)
		{
			mapConteo.put(String.valueOF(ar.get('sede')),Integer.valueOf(ar.get('total')));
		}
        try
        {
		result[0].get('total');
        }catch(Exception e ){
            system.debug('NO PERMITIO BORRAR POR  '+e.getMessage());
        }
		for(BI_Sede__c sed:lstOld)
		{
			Integer contar=mapConteo.containsKey(sed.id)?mapConteo.get(sed.id):0;
			if(sed.BI_COL_Estado_callejero__c=='Validado por callejero' && contar>0)
			{
				sed.addError('<b style="color:#FF0000">No es posible eliminar la direccion que se encuentra validada por callejero y esta asociado a una sede<b>',false);
			}
		}
	}
}