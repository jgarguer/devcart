/**
* Avanxo Colombia
* @author 			Manuel Medina href=<mmedina@avanxo.com>
* Project:			Telefonica
* Description:		Clase tipo WebServiceMock del servicio ContratoNovedadesWSDL.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2015-07-31		Manuel Medina (MM)      Definicion de la clase implments WebServiceMock
*************************************************************************************************************/
@isTest
global class BI_COL_ChannelOnline_mws implements WebServiceMock {

	global Map<String, Object> mapWSResponseBySOAPAction									= new Map<String, Object>();

	global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){
		
		ws_CanalOnlineModel.mRespNovedadEmpresa_element objmRespNovedadEmpresa_element = new 	ws_CanalOnlineModel.mRespNovedadEmpresa_element();	
		objmRespNovedadEmpresa_element.Resultado 											= new ws_CanalOnlineModel.Resultado();		
		objmRespNovedadEmpresa_element.Resultado.Exito														= true;
		objmRespNovedadEmpresa_element.Resultado.Mensaje														= 'TSTResponse';
		objmRespNovedadEmpresa_element.Resultado.Codigo														= 'mReqNovedadEmpresa';
		objmRespNovedadEmpresa_element.Resultado.Severidad													= 'confirm';
		
		ws_CanalOnlineModel.Resultado wsRESmReqNovedadContacto								= new ws_CanalOnlineModel.Resultado();
		wsRESmReqNovedadContacto.Codigo														= 'mReqNovedadContacto';
		wsRESmReqNovedadContacto.Exito														= true;
		wsRESmReqNovedadContacto.Mensaje													= 'TSTResponse';
		wsRESmReqNovedadContacto.Severidad													= 'confirm';

		ws_CanalOnlineModel.mRespNovedadContacto_element objMRespNovedadContacto_element = new  ws_CanalOnlineModel.mRespNovedadContacto_element();
		objMRespNovedadContacto_element.Resultado = wsRESmReqNovedadContacto;

		
		mapWSResponseBySOAPAction.put( 'mReqNovedadEmpresa', objmRespNovedadEmpresa_element );
		mapWSResponseBySOAPAction.put( 'mReqNovedadContacto', objMRespNovedadContacto_element );
		
		response.put( 'response_x', mapWSResponseBySOAPAction.get( requestName ) );
	}
}