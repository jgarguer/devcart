public class BIIN_UNICA_AccountMethods 
{
/*----------------------------------------------------------------------------------------------------------------------
  Author:        José Luis González
  Company:       HPE
  Description:   
                 
  IN:            ApexTrigger.new
  OUT:           Void
      
  History: 
  
  <Date>              <Author>                            <Change Description>
  22/06/2016          José Luis González                  Initial versión
  06/07/2016          José Luis González                  Check accounts list size
----------------------------------------------------------------------------------------------------------------------*/

    /*------------------------------------------------------------------------------------------------------------------
    Author:        José Luis González
    Company:       HPE
    Description:   
                   
    IN:            ApexTrigger.new
    OUT:           Void
        
    History: 
    
    <Date>              <Author>                            <Change Description>
    22/06/2016          José Luis González                  UNICA integrate Legal Entity with RoD
    ------------------------------------------------------------------------------------------------------------------*/
    public static void createLegalEntity(List<Account> lst_accounts_new)
    {
        if(!lst_accounts_new.isEmpty())
        {
            // UNICA call to create Legal Entity (POST)
            for (Account acc : lst_accounts_new) 
            {
                if(isLegalEntityModified(acc, null) && !Test.isRunningTest()) 
                {
                	BIIN_UNICA_AccountHandler.afterInsert(JSON.serialize(acc));
                }
            }
        }
    }
    /*------------------------------------------------------------------------------------------------------------------
    Author:        José Luis González
    Company:       HPE
    Description:   
                   
    IN:            ApexTrigger.new, ApexTrigger.old
    OUT:           Void
        
    History: 
    
    <Date>              <Author>                            <Change Description>
    22/06/2016          José Luis González                  UNICA integrate Legal Entity with RoD
    ------------------------------------------------------------------------------------------------------------------*/
    public static void updateLegalEntity(List<Account> lst_accounts_new, List<Account> lst_accounts_old)
    {
        if(!lst_accounts_new.isEmpty())
        {
            // UNICA call to update Legal Entity (PUT)
            for (Integer i = 0; i < lst_accounts_new.size(); i++)
            {
                Account newAcc = lst_accounts_new[i];
                Account oldAcc = lst_accounts_old[i] != null ? lst_accounts_old[i] : new Account();

                if (isLegalEntityModified(newAcc, oldAcc) && !Test.isRunningTest()) 
                {
                    String newAccStr = JSON.serialize(newAcc);
                    String oldAccStr = JSON.serialize(oldAcc);

                    BIIN_UNICA_AccountHandler.afterUpdate(newAccStr, oldAccStr);
                }
            }
        }
    }

    private static Boolean isLegalEntityModified(Account acc, Account oldAcc) 
    {
        if(!areKeyfieldsModified(acc, oldAcc))
        {
            return false;
        }
        
        if (!isAccountLegalEntity(acc))
        {
            return false;
        }
        
        return true;
    }

    private static Boolean isAccountLegalEntity(Account acc)
    {
        Boolean isAccountLegalEntity = false;
        // check account is legal entity & belongs to 'Empresas'
        if ((acc.BI_RecordType_DevName__c == 'TGS_Legal_Entity_Inactive' 
                || acc.BI_RecordType_DevName__c == 'TGS_Legal_Entity') 
                && acc.BI_Segment__c == 'Empresas'
                && acc.BI_Country__c == Label.BI_Peru)
        {
            isAccountLegalEntity = true;
        }

        System.debug(' BIIN_UNICA_AccountHandler.isAccountLegalEntity | isAccountLegalEntity: ' + isAccountLegalEntity);
        return isAccountLegalEntity;
    }

    private static Boolean areKeyfieldsModified(Account acc, Account oldAcc) 
    {
        System.debug(' BIIN_UNICA_AccountHandler.areKeyfieldsModified | acc: ' + acc);
        System.debug(' BIIN_UNICA_AccountHandler.areKeyfieldsModified | oldAcc: ' + oldAcc);
        if(oldAcc == null)
        {
            return true;
        }
        Boolean areKeyfieldsModified = false;
        // check modifications of legal entity key fields
        if(acc.Name != oldAcc.Name 
                || acc.BI_Validador_Fiscal__c != oldAcc.BI_Validador_Fiscal__c 
                || acc.BI_Segment__c  != oldAcc.BI_Segment__c 
                || acc.BI_Numero_de_sedes__c != oldAcc.BI_Numero_de_sedes__c
                || acc.BI_Activo__c != oldAcc.BI_Activo__c)
        {
            areKeyfieldsModified = true;
        }

        System.debug(' BIIN_UNICA_AccountHandler.areKeyfieldsModified | areKeyfieldsModified: ' + areKeyfieldsModified);
        return areKeyfieldsModified;
    }
}