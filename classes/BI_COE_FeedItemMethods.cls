public with sharing class BI_COE_FeedItemMethods {
    
    public static void sendEmailCOE(List<FeedItem> news){

        try {

            System.debug('### 1');

            List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject from EmailTemplate where DeveloperName='CoE_Caso_eHelp_Comentado'];
            Profile adminId = [SELECT Id FROM Profile WHERE Name = :Label.BI_Administrador_del_sistema];

            if(!templates.isEmpty()){
                Set<Id> casesIdSet = new Set<Id>();
                for(FeedItem fi : news) {
                    casesIdSet.add(fi.ParentId);
                }
                List<Case> casesList = [SELECT Id, CoE_Support_Contact__c, ParentId, CaseNumber,Priority,CreatedDate, Account.Name FROM Case WHERE Id IN :casesIdSet];

                List<Messaging.SingleEmailMessage> list_emails = new List<Messaging.SingleEmailMessage>();

                for(Case cas : casesList) {
                    if(UserInfo.getProfileId() != adminId.Id) {
                        if(!String.isBlank(cas.CoE_Support_Contact__c)) {
                            Messaging.SingleEmailMessage singleEmail = new Messaging.SingleEmailMessage();
                            
                            string body = templates[0].HTMLValue;
                            body = body.replace('{!Case.CaseNumber}', '\''+cas.CaseNumber+'\'');
                            body = body.replace('{!Case.Priority}', '\''+cas.Priority+'\'');
                            body = body.replace('{!Case.CreatedDate}', '\''+cas.CreatedDate+'\'');
                            String fullCaseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + cas.id;
                            body = body.replace('{!Case.Link}', '\''+fullCaseURL+'\'');
                            singleEmail.setHTMLBody(body);

                            singleEmail.saveAsActivity=false;
                            singleEmail.setTemplateId(templates[0].Id);
                            singleEmail.setWhatId(cas.ParentId);
                            singleEmail.setTargetObjectId(cas.CoE_Support_Contact__c);
                            
                            list_emails.add(singleEmail);
                        }
                    }
                }

                if(!list_emails.isEmpty()){
                    Messaging.sendEmail(list_emails);
                }
            }
        }
        catch (exception Exc){
            BI_LogHelper.generate_BILog('feedItemsMethods.sendEmailCOE', 'BI_EN', Exc, 'Trigger');
        }
    }



    public static void reopenCase(List<FeedItem> news) {

        System.debug('### 2');

        try {
            List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject from EmailTemplate where DeveloperName='CoE_Caso_eHelp_Reabierto'];
            Map<Id,FeedItem> map_caseFi = new Map<Id,FeedItem>();
            List<Case> lst_casesToUpdate = new List<Case>();

            List<Contact> lst_Client_LATAM = [SELECT Id, Account.Name FROM Contact WHERE Account.Name = 'Support_CoE_Team_LATAM_2017'];
            Profile adminId = [SELECT Id FROM Profile WHERE Name = :Label.BI_Administrador_del_sistema];

            if(!templates.isEmpty()){
                Set<Id> casesIdSet = new Set<Id>();
                for(FeedItem fi : news) {
                    casesIdSet.add(fi.ParentId);
                    map_caseFi.put(fi.ParentId, fi);
                    
                }
                List<Case> casesList = [SELECT Id, CoE_Support_Contact__c, ParentId, CaseNumber,Priority,CreatedDate, Account.Name, Status FROM Case WHERE Id IN :casesIdSet AND (Status=:'Closed' OR Status=:'Cancelled')];

                List<Messaging.SingleEmailMessage> list_emails = new List<Messaging.SingleEmailMessage>();

                for(Case cas : casesList) {

                    if(UserInfo.getProfileId() != adminId.Id) {

                            if(!String.isBlank(cas.CoE_Support_Contact__c)) {

                                for(Contact contacto : lst_Client_LATAM) {

                                    Messaging.SingleEmailMessage singleEmail = new Messaging.SingleEmailMessage();

                                    string body = templates[0].HTMLValue;
                                    body = body.replace('{!Case.CaseNumber}', '\''+cas.CaseNumber+'\'');
                                    body = body.replace('{!Case.Priority}', '\''+cas.Priority+'\'');
                                    body = body.replace('{!Case.CreatedDate}', '\''+cas.CreatedDate+'\'');
                                    String fullCaseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + cas.id;
                                    body = body.replace('{!Case.Link}', '\''+fullCaseURL+'\'');
                                    singleEmail.setHTMLBody(body);

                                    singleEmail.saveAsActivity=false;
                                    singleEmail.setTemplateId(templates[0].Id);
                                    singleEmail.setWhatId(cas.ParentId);
                                    singleEmail.setTargetObjectId(contacto.Id);
                                    
                                    list_emails.add(singleEmail);
                                           
    
                                }
                                System.debug('estoy aqui');
                                cas.Status = 'Reopened - Closed';
                                lst_casesToUpdate.add(cas);
                            }
                    }
                    System.debug('estoy aquiiiiii'+cas.Status );
                    
                }

                
                if(!list_emails.isEmpty()){
                    Messaging.sendEmail(list_emails);
                }

                update lst_casesToUpdate;

            }
        }
        catch (exception Exc){
            BI_LogHelper.generate_BILog('feedItemsMethods.sendEmailCOE', 'BI_EN', Exc, 'Trigger');
        }

    }
    
}