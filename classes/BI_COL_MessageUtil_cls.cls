/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           		Descripción
*           -----   ----------      --------------------            		---------------
* @version   1.0    2015-04-15      Manuel Esthiben Mendez Devia (MEMD)     Cloned Controller
*************************************************************************************************************/
public with sharing class BI_COL_MessageUtil_cls 
{ 

    /**Contiene los tipos de mensajes*/
    public static final String ERROR='ERROR';
    public static final String INFO='INFO';
    public static final String WARNING='WARNING';
    public static final String FATAL='FATAL';
    public static final String CONFIRM='CONFIRM';
    
    public static void mostrarMensaje( String Severidad, String Mensaje )
    {
        ApexPages.Severity Severity;
        if( Severidad.equalsIgnoreCase('ERROR') )
            Severity = ApexPages.Severity.ERROR;
        if( Severidad.equalsIgnoreCase('WARNING') )
            Severity = ApexPages.Severity.WARNING;
        if( Severidad.equalsIgnoreCase('INFO') )
            Severity = ApexPages.Severity.INFO;
        if( Severidad.equalsIgnoreCase('FATAL') )
          Severity = ApexPages.Severity.FATAL;
        if( Severidad.equalsIgnoreCase('CONFIRM') )
            Severity = ApexPages.Severity.CONFIRM;
            ApexPages.addMessage( new ApexPages.Message( Severity, Mensaje) );
    }

}