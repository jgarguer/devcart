@isTest public class BI2_AccountMethods_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       Salesforce.com
     Description:   Test class for BI2_AccountMethods
     
     History:
     
     <Date>                  <Author>                <Change Description>
     02/09/2015              Jose Miguel Fierro      Initial Version
     09/05/2016				 Guillermo Muñoz		 BI_TestUtils.throw_exception = false added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest public static void test1() {
    	
    	BI_TestUtils.throw_exception = false;
    	User user = [select Id from User where Id = :UserInfo.getUserId()];
    	user.Pais__c =  Label.BI_Peru;
    	update user;
    	
    	system.runAs(user){
    	
	        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);
	        List<Group> lst_grps = new List<Group> {
				new Group(Type='Queue',DeveloperName='Test_GRP_N3_1', Name='TestN3')
			};
			insert lst_grps;
			System.runAs(new User(Id=UserInfo.getUserId())) {
				insert new List<GroupMember> {
					new GroupMember(GroupId=lst_grps[0].Id, UserOrGroupId=UserInfo.getUserId())
				};
			}
			System.runAs(new User(Id=UserInfo.getUserId())) {
				List<QueueSObject> lst_queues = new List<QueueSObject> {
					new QueueSObject(QueueId = lst_grps[0].Id, SObjectType='Case')
				};
				insert lst_queues;
			}
	        List<Account> lst_accs = new List<Account>();
	        
	        lst_accs.add(new Account(Name='Test Account', BI2_cuadrilla__c='TestN3', BI_Country__c= Label.BI_Peru, BI_Segment__c = 'Mayoristas', BI_Subsegment_regional__c = 'Mayoristas',BI_Territory__c = 'test'));
	        lst_accs.add(new Account(Name='Test Account',BI_Segment__c = 'Mayoristas', BI_Subsegment_regional__c = 'Mayoristas',BI_Territory__c = 'test'));
	       	
	        System.debug(lst_accs);
	        insert lst_accs;
	        System.debug([SELECT Name, BI2_cuadrilla__c, BI_Country__c FROM Account WHERE Id IN:lst_accs]);
	        lst_accs.get(0).BI_Country__c= Label.BI_Peru;
	        lst_accs.get(0).BI2_cuadrilla__c='TestN3';
	        update lst_accs;
	        system.assert([select Id from BI_Log__c limit 1].isEmpty());
    	}
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that manage the code coverage of BI2_AccountMethods.createAnsVEN
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        17/03/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void createAnsVEN_TEST(){

    	BI_TestUtils.throw_exception = false;
        
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);

    	List <SlaProcess> lst_sla = [SELECT Id ,Name FROM SlaProcess WHERE Name LIKE '%Venezuela%' AND isActive = true];

    	//Inserción de cliente con segmento Empresas
    	Account acc = new Account(
    		Name = 'Test1',
            BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
            BI_Activo__c = Label.BI_Si,
            BI_Country__c = Label.BI_Venezuela,
            BI_Segment__c = 'Empresas',
            BI_Subsegment_regional__c = 'Mayoristas',
            BI_Subsector__c = 'Aéreo de Pasajeros',
            BI_Sector__c = 'Financiero',
            BI_Territory__c = 'test'
    	);
    	insert acc;

    	System.assertEquals([SELECT Id FROM Entitlement WHERE AccountId =: acc.Id].size(), lst_sla.size());

    	//Inserción de cliente con segmento Negocios y subsegmento Top
    	Account acc2 = new Account(
    		Name = 'Test2',
            BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
            BI_Activo__c = Label.BI_Si,
            BI_Country__c = Label.BI_Venezuela,
            BI_Segment__c = 'Negocios',
            BI_Subsegment_regional__c = 'Top',
            BI_Subsector__c = 'Aéreo de Pasajeros',
            BI_Sector__c = 'Financiero',
            BI_Territory__c = 'test'
    	);
    	insert acc2;

    	System.assertEquals([SELECT Id FROM Entitlement WHERE AccountId =: acc2.Id].size(), lst_sla.size());

    	//Inserción de cuentas que no cumpla los criterios de creación de ANS y modificación a segmento Negocios y subsegmento Top
    	Account acc3 = new Account(
    		Name = 'Test3',
            BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
            BI_Activo__c = Label.BI_Si,
            BI_Country__c = Label.BI_Venezuela,
            BI_Segment__c = 'Negocios',
            BI_Subsegment_regional__c = 'Premium',
            BI_Subsector__c = 'Aéreo de Pasajeros',
            BI_Sector__c = 'Financiero',
            BI_Territory__c = 'test'
    	);
    	insert acc3;

    	System.assertEquals([SELECT Id FROM Entitlement WHERE AccountId =: acc3.Id].isEmpty(),true);

    	acc3.BI_Subsegment_regional__c = 'Top';
    	update acc3;
    	System.assertEquals([SELECT Id FROM Entitlement WHERE AccountId =: acc3.Id].size(), lst_sla.size());

    	//Inserción de cuentas que no cumpla los criterios de creación de ANS y modificación a segmento Empresas
    	Account acc4 = new Account(
    		Name = 'Test4',
            BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
            BI_Activo__c = Label.BI_Si,
            BI_Country__c = Label.BI_Venezuela,
            BI_Segment__c = 'Negocios',
            BI_Subsegment_regional__c = 'Premium',
            BI_Subsector__c = 'Aéreo de Pasajeros',
            BI_Sector__c = 'Financiero',
            BI_Territory__c = 'test'

    	);
    	insert acc4;

    	System.assertEquals([SELECT Id FROM Entitlement WHERE AccountId =: acc4.Id].isEmpty(),true);

    	acc4.BI_Segment__c = 'Empresas';
    	acc4.BI_Subsegment_regional__c = 'Mayoristas';
    	update acc4;
    	System.assertEquals([SELECT Id FROM Entitlement WHERE AccountId =: acc4.Id].size(), lst_sla.size());
    }
    
}