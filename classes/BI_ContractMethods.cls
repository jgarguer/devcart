public class BI_ContractMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Methods exceuted by Contract Triggers
    
    History:
    
    <Date>            <Author>          <Description>
    22/09/2014        Pablo Oliva       Initial version
    17/05/2016        Humberto Nunes    Adding functionality for FVI (moverNCP).
    27/09/2016        Miguel Cabrera    Agregada condicion en método FiltroOpp => Op.StageName != 'F2 - Contract Negotiation'
    26/10/2016       Patricia Castillo  FiltroOpp->Replace label F2 - Contract Negotiation with F2 - Negotiation, and add filter to avoid validation for Argentina
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that assigns the currency of the products to the contract related
    
    IN:           List <Contract>
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    22/09/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void assignCurrency(List<Contract> news){
       try{ 

            List<Contract> lst_contracts = new List<Contract>();
            Set<Id> set_opp = new Set<Id>();
            
            for(Contract contract:news){
                if(contract.BI_Oportunidad__c != null){
                    set_opp.add(contract.BI_Oportunidad__c);
                    lst_contracts.add(contract);
                }
            }
            
            if(!set_opp.isEmpty()){
                Set <String> set_status = new Set <String> {Label.BI_LabelActive, Label.BI_LabelActiveRapido, Label.BI_OpportunityStageF1Ganada};
                Map<Id, Opportunity> map_opp = new Map<Id, Opportunity>([select Id, (select Id from NE__Orders__r where NE__OrderStatus__c IN :set_status) from Opportunity where Id IN :set_opp]);
                
                if(!map_opp.keySet().isEmpty()){
                    
                    Set<Id> set_orders = new Set<Id>();
                    
                    for(Id idOpp:map_opp.keySet()){
                        
                        if(!map_opp.get(idOpp).NE__Orders__r.isEmpty())
                            set_orders.add(map_opp.get(idOpp).NE__Orders__r[0].Id);
                        
                    }
                    
                    if(!set_orders.isEmpty()){
                        
                        Map<Id, NE__Order__c> map_orders = new Map<Id, NE__Order__c>([select Id, (select Id, CurrencyIsoCode from NE__Order_Items__r) from NE__Order__c where Id IN :set_orders]);
                        
                        if(!map_orders.keySet().isEmpty()){
                            
                            for(Contract cont:lst_contracts){
                                
                                Set<String> set_currency = new Set<String>();
                                
                                if(map_opp.get(cont.BI_Oportunidad__c) != null){
                                    
                                    if(!map_opp.get(cont.BI_Oportunidad__c).NE__Orders__r.isEmpty()){
                                        
                                        if(map_orders.get(map_opp.get(cont.BI_Oportunidad__c).NE__Orders__r[0].Id) != null){
                                            
                                            for(NE__OrderItem__c item:map_orders.get(map_opp.get(cont.BI_Oportunidad__c).NE__Orders__r[0].Id).NE__Order_Items__r)
                                                set_currency.add(item.CurrencyIsoCode);
                                            
                                        }
                                        
                                    }
                                    
                                }
                                
                                if(!set_currency.isEmpty()){
    
                                    cont.BI_Divisas_del_contrato__c = '';
    
                                    Schema.DescribeFieldResult fieldResult = Contract.BI_Divisas_del_contrato__c.getDescribe();
                                    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
                                    Map<String, String> map_currencyPL = new Map<String, String>();
    
                                    for(Schema.PicklistEntry f : ple)
                                        map_currencyPL.put(f.getValue().substring(0,3), f.getValue());
                                    
                                    for(String curren:set_currency){
    
                                        if(map_currencyPL.get(curren) != null)
                                            cont.BI_Divisas_del_contrato__c += map_currencyPL.get(curren)+';';
                                        else
                                            cont.BI_Divisas_del_contrato__c += curren+';';
    
                                    }
                                     
                                    cont.BI_Divisas_del_contrato__c = cont.BI_Divisas_del_contrato__c.substring(0, cont.BI_Divisas_del_contrato__c.length() - 1);
                                    
                                }
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_ContractMethods.assignCurrency', 'BI_EN', Exc, 'Trigger');
        } 
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes 
    Company:       NE Aborda
    Description:   Method that assigns accounts to BI_FVI_Nodo_Cliente__c
    
    IN:           List <Contract>
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    17/05/2016        Humberto Nunes    Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void moverNCP(List<Contract> tnew , List<Contract> told)
    {
        try
        { 
            // SE OBTIENE LOS CLIENTES CUYO CONTRATO HA PASADO A FIRMADO ACTIVO
            Set <Id> set_AccId = new Set <Id>();
            for (Contract item:tnew)
                if(item.Status == 'Signed Activated')
                    set_AccId.add(item.AccountId); 
            
            // SE BUSCAN TODOS LOS NODOCLIENTES QUE CONTENGAN LOS CLIENTES ENCONTRADOS Y QUE ESTEN EN UN NAV PARA CAMBIARLOS AL NCP
            List<BI_FVI_Nodo_Cliente__c> lst_nodosclientes = [SELECT Id, BI_FVI_IdNodo__c, BI_FVI_IdNodo__r.BI_FVI_NodoPadre__c FROM BI_FVI_Nodo_Cliente__c WHERE BI_FVI_IdCliente__c IN: set_AccId AND (BI_FVI_IdNodo__r.RecordType.DeveloperName = 'BI_FVI_NAV' OR BI_FVI_IdNodo__r.RecordType.DeveloperName = 'BI_FVI_NAT')];
            for(BI_FVI_Nodo_Cliente__c nc: lst_nodosclientes)
                nc.BI_FVI_IdNodo__c = nc.BI_FVI_IdNodo__r.BI_FVI_NodoPadre__c;
            
            update lst_nodosclientes;
        }
        catch (exception Exc)
        {
           BI_LogHelper.generate_BILog('BI_ContractMethods.moverNCP', 'BI_EN', Exc, 'Trigger');
        } 
    }   


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes 
    Company:       NE Aborda
    Description:   Method that assigns accounts to BI_FVI_Nodo_Cliente__c
    
    IN:           List <Contract>
    OUT:            
    
    History:
    
    <Date>            <Author>          <Description>
    02/06/2016        Humberto Nunes    Initial version.
    27/09/2016        Miguel Cabrera    Agregada condicion Op.StageName != 'F2 - Contract Negotiation'
    17/10/2016        Fernando Arteaga  Replace stage values with labels
    26/10/2016       Patricia Castillo  Replace label F2 - Contract Negotiation with F2 - Negotiation, and add filter to avoid validation for Argentina
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void FiltroOpp(List<Contract> tnew)
    {
        try
        { 
             // RECREA EL FILTRO EXISTENTE PARA EL CAMPO OPORTUNIDAD Y NO SE APLICA A LOS CONTRATOS CON TIPO DE REGISTRO BI_FVI_CONTRATO
            List<Id> listIdOpp = new list<Id>();
            for (Contract item:tnew)
                listIdOpp.add(item.BI_Oportunidad__c);

            List<Opportunity> lstOpp = [SELECT Id, StageName, BI_Country__c FROM Opportunity WHERE Id in: listIdOpp];
            List<RecordType> lst_rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_FVI_Contrato'];
            if (!lst_rt.isEmpty())
                for (Contract item:tnew)
                    for (Opportunity Op:lstOpp)
                         if (Op.BI_Country__c != Label.BI_Argentina && item.BI_Oportunidad__c == Op.id && Op.StageName != Label.BI_F1Ganada && Op.StageName != Label.BI_F2Negociacion && item.RecordTypeId!=lst_rt[0].Id )
                            item.addError('Solo es posible crear contratos a Oportunidades a partir de etapa "F2 - Gestión de Contratos".');
        }
        catch (exception Exc)
        {
           BI_LogHelper.generate_BILog('BI_ContractMethods.moverNCP', 'BI_EN', Exc, 'Trigger');
        } 

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes 
    Company:       NE Aborda
    Description:   Method that call the @Future event for construct the contract.
    
    IN:           List <Contract>
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    07/06/2016        Humberto Nunes    Initial version.
    03/08/2016        Humberto Nunes    Se incluyo la validacion para IsFuture   
    06/08/2016        Se dejo solo la llamada a  BI_ContractMethods.CallZytrustFuture
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void zytrust (List<Contract> tnew)
    {
        
        Set<Id> idsContract = new Set<Id>();
        for(Contract c: tnew)
            if (c.BI_FVI_Solicitar_Contrato__c == true || c.BI_FVI_Solicitar_Contrato_con_huella__c == true)
                idsContract.add(c.Id);

        if (!idsContract.isEmpty() ) 
            if (!System.isFuture()) BI_ContractMethods.CallZytrustFuture(idsContract);                
    }

    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes 
    Company:       NE Aborda
    Description:   Method that Call WS for construct the Digital Contract
    
    IN:           Set<Id> IdsContract
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    07/06/2016        Humberto Nunes    Initial version.
    03/08/2016        Humberto Nunes    Se creo la version Future y NoFuture
    06/08/2016        Humberto Nunes    Se elimino la version NoFuture
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @Future (callout=true)
    public static void CallZytrustFuture (Set<Id> idsContract)
    {        
        BI_FVI_Contrato_Zytrust_Call.callTestZytrust(idsContract);
    } 

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes 
    Company:       NE Aborda
    Description:   Method Create Record on BI_Registro_Datos_Desempeno__c when the contract has signed activated
    
    IN:            List<Contract> tnew, List<Contract> told
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    07/06/2016        Humberto Nunes    Initial version.
    22/11/2016        Humberto Nunes    Se Cambio 'AC' POR 'Actos Comerciales'.
    09/03/2017        Humberto Nunes    Se Omite todo el calculo ya que ahora se realiza EN LA CLASE BI_OpportunityMethods.FillActosComerciales_Call
    23/03/2017        Humberto Nunes    Se Reactiva el calculo pero se hace dependiente de que el campo BI_FVI_GenerarDesempeno__c este Activo. 
    15/03/2018        Humberto Nunes    Se agrego el Tipo de Registro 
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    static
    {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'BI_Registro_Datos_Desempeno__c'])
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
    }

    public static void CreateRegistroDesemp(List<Contract> tnew, List<Contract> told)
    {
        try 
        {
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');

            List<BI_Registro_Datos_Desempeno__c> lstRegDatosDesemp = new List<BI_Registro_Datos_Desempeno__c>();

            for(Integer i=0; i<tnew.size(); i++)
            {
                if(tnew[i].BI_FVI_GenerarDesempeno__c == true && tnew[i].BI_FVI_GenerarDesempeno__c != told[i].BI_FVI_GenerarDesempeno__c) 
                {
                    BI_Registro_Datos_Desempeno__c objRegDatosDesemp = new BI_Registro_Datos_Desempeno__c();
                    Datetime dtConvertDT = tnew[i].LastModifiedDate;    
                    objRegDatosDesemp.BI_FVI_Fecha__c = date.newinstance(dtConvertDT.year(), dtConvertDT.month(), dtConvertDT.day());
                    objRegDatosDesemp.BI_FVI_Id_Cliente__c = tnew[i].AccountId;
                    objRegDatosDesemp.BI_Tipo__c = 'Actos Comerciales'; // ANTES 'AC'; 22/11/2016
                    objRegDatosDesemp.BI_FVI_Valor__c = tnew[i].BI_FVI_Actos_Comerciales__c; // decimal.valueof(objFact.BI_Importe_Factura__c);
                    objRegDatosDesemp.BI_FVI_IdObjeto__c = tnew[i].Id;
                    objRegDatosDesemp.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018
                    lstRegDatosDesemp.add(objRegDatosDesemp);
                }
            }

            insert lstRegDatosDesemp;
            system.debug('lstRegDatosDesempNew----->>' +lstRegDatosDesemp);          
        }
        catch (exception Exc)
        {
            BI_LogHelper.generate_BILog('BI_ContractMethods.CreateRegistroDesemp', 'BI_EN', Exc, 'Trigger');
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Patricia Castillo 
    Company:       NE Aborda
    Description:   Method to validate if user who modifies record is allowed to do it (only for BI_EN Argentina contracts)

    
    IN:            List<Contract> news, Map<Id,Contract> map_olds
    OUT:           
    
    History:
    
    <Date>              <Author>                <Description>
    05/10/2016          Patricia Castillo       Initial version. (D321)
    05/10/2016          Gawron, Julián          Fix condition. (D321)
    28/03/2017          Gawron, Julián          Refactoring conditions (D321)  
    07-04-2017          Humberto Nunes          Se omitio En el Metodo BI_ContractMethods.validateUserModifierARG los Contratos de FVI
    08-05-2017          Jaime Regidor           Fix condition. (D321) 
    17-10-2017          Paloma Lastra           Ever01 - Administrador del sistema y Super Usuario podrán modificar contrato
    16-04-2018          Alejo Jimenez           Excluir de validación a propietario de la oportunidad eHelp 03561702
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
     public static void validateUserModifierARG(Map<Id,Contract> news, Map<Id,Contract> map_olds) {
        
        Set<Id> set_IdAcc = new Set <Id>();
        List<Contract> toProccess = new List<Contract>();
        
        //OAJF - 16/04/2018
        Map<Id,Contract> lstAuxContracts = new Map<Id,Contract>([select Id,RecordType.DeveloperName,BI_Oportunidad__r.OwnerId from Contract where id in :news.keySet()]);

        for (Id ctrId : news.keySet()) {
            set_IdAcc.add(news.get(ctrId).AccountId);
        }
        Map<Id,Account> map_Acc = new Map<Id,Account>([select id, BI_Country__c,(SELECT UserId from AccountTeamMembers where TeamMemberRole = 'Contract Administrator') from account where id in :set_IdAcc]);
        
        
        for (Id ctrNewId : news.keySet()) {
            if(map_Acc.get(news.get(ctrNewId).AccountId).BI_Country__c == Label.BI_Argentina && lstAuxContracts.get(ctrNewId).RecordType.DeveloperName!='BI_FVI_Contrato')
            {
                toProccess.add(news.get(ctrNewId));
            }
        }
        
        if(!toProccess.isEmpty()) {

            User usu = [SELECT Id, BI_Permisos__c, UserRole.DeveloperName from User where Id=:UserInfo.getUserId() limit 1];
            if(map_olds != null) {
                for(Contract cntr : toProccess) {
                    Account acc = map_Acc.get(cntr.AccountId);
                    Boolean isCA = false;
                    if(!acc.AccountTeamMembers.isEmpty()){
                        for (AccountTeamMember atm : acc.AccountTeamMembers) {
                            if(atm.UserId == UserInfo.getUserId()) {
                                isCA = true;
                                break;
                            }
                        }
                    }
                     
                    Boolean ModifyContracts = (//Con permiso para modificar contratos
                        isCA 
                        || cntr.OwnerId == usu.Id 
                        || usu.BI_Permisos__c == 'Administrador de Contrato'
                        
                        //Ever01-INI
                        || usu.BI_Permisos__c == 'Administrador del sistema'
                        || usu.BI_Permisos__c == 'Super Usuario'
                        //Ever01-FIN                      
                        
                        || usu.UserRole.DeveloperName == 'ARG_EMPRESAS_ADMINISTRADOR_CONTRATOS' 
                        || usu.UserRole.DeveloperName == 'ARG_EMPRESAS_JEFE_ADMINISTRADOR_CONTRATOS'

                        //OAJF - 16/04/2018
                        || lstAuxContracts.get(cntr.Id).BI_Oportunidad__r.OwnerId == usu.Id);

                    if(ModifyContracts || usu.BI_Permisos__c == 'Ingeniería/Preventa'){

                        if (!ModifyContracts && usu.BI_Permisos__c == 'Ingeniería/Preventa') {
                            // es Ingeniero de preventa y no es propietario ni administrador
                            // Se enumeran todos los campos, menos BI_Fecha_inicio_servicio__c y BI_Fecha_fin_de_implementacion__c, que este Permiso puede modificar


                            //Mapa de todos los campos del Contrato JRM
                             Map<String, Schema.SObjectField> map_fields = Contract.sObjectType.getDescribe().fields.getMap();
                      
                              for(Schema.SObjectField field : map_fields.values()){

                                for(Contract ctr : toProccess){

                                    String fieldName = field.getDescribe().getName();
                                    
                                    if(fieldName != 'BI_Fecha_fin_de_implementacion__c' && fieldName != 'BI_Fecha_inicio_servicio__c'  ){
                                        if(news.get(ctr.Id).get(fieldName) != map_olds.get(ctr.Id).get(fieldName)){
                                            ctr.addError('Sólo puedes modificar los campos  "Fecha de inicio servicio/facturación" y "Fecha fin de implementación del Contrato"');
                                            break;
                                       } 
                                    }
                                }
                            }
                        }
                    } else {
                        cntr.addError('No tienes permisos para realizar esta operación');
                    }
                }
            }
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Patricia Castillo 
    Company:       NE Aborda
    Description:   Method to fill BI_ARG_Proveedor_Internacional__c field value from related Opty before insert contract for Argentina 

    
    IN:            List<Contract> news
    OUT:           
    
    History:
    
    <Date>            <Author>              <Description>
    06/10/2016        Patricia Castillo     Initial version. (D321)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static void updateMultipicklistFromRelatedOptyARG(List<Contract> news){
        Set<Id> Ids_opty=new Set<Id>();
        for(Contract ctr_optys : news) {
            Ids_opty.add(ctr_optys.BI_Oportunidad__c);
        }

        Map<Id,Opportunity> optys = new Map<Id,Opportunity>([SELECT Id, BI_Country__c, BI_ARG_Proveedor_Internacional__c from Opportunity where ID in :Ids_opty]);

        if(!optys.isEmpty()){
            for(Contract ctrs : news) {
                if(optys.get(ctrs.BI_Oportunidad__c).BI_Country__c == Label.BI_Argentina) {
                    ctrs.BI_ARG_Proveedor_Internacional__c = optys.get(ctrs.BI_Oportunidad__c).BI_ARG_Proveedor_Internacional__c; 
                }
            }
        }
    }

}