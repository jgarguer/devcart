@isTest
public with sharing class BI_SedeMethods_TEST {

	    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        ??????
        Company:       ??????
        Description:   ?????? 
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        ??/??/????                   	????????					Initial version
        25/06/2016						Humberto Nunes 				Se ejecuta el proceso como administrador de sistema 
                                                                    Se modifico la ultima sede para que entrara por la casuistica sobre campo "BI_Tipo_de_sede__c"
        20/09/2017        Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	@TestSetup
	public static void loadData_COL(){

		String en_US = 'en_US';
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        String region = Label.BI_Argentina;
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name = 'Administrador del sistema' limit 1];
        User user = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = en_US,
                          localesidkey = en_US,
                          ProfileId = prof.Id,
                          BI_Permisos__c = 'Sucursales',
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com',
                          Pais__c = Label.BI_Argentina);
        
        insert user;    
           
        System.runAs(user)
        {
			//Cuentas
			Account objCuenta 								= new Account();
			objCuenta.Name                                  = 'prueba2';
			objCuenta.BI_Country__c                         = 'Colombia';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Fraude__c                          = false;
			objCuenta.BI_Segment__c                         = 'test';
			objCuenta.BI_Subsegment_Regional__c             = 'test';
			objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			system.debug('datos Cuenta ' + objCuenta);

			//Ciudad
			BI_Col_Ciudades__c objCiudad = new BI_Col_Ciudades__c ();
			objCiudad.Name 						= 'Test City';
			objCiudad.BI_COL_Pais__c 			= 'Test Country';
			objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
			insert objCiudad;

			//Contactos
			Contact objContacto                             = new Contact();
			objContacto.LastName                         	= 'Test2';
			objContacto.BI_Country__c                   	= 'Colombia';
			objContacto.CurrencyIsoCode                 	= 'COP';
			objContacto.AccountId                       	= objCuenta.Id;
			objContacto.BI_Tipo_de_contacto__c   			= 'Administrador Canal Online';
			objContacto.Phone         						= '1234567';
			objContacto.MobilePhone       					= '3104785925';
			objContacto.Email        						= 'pruebas@pruebas12.com';
			objContacto.BI_COL_Direccion_oficina__c   		= 'Dirprueba';
			objContacto.BI_COL_Ciudad_Depto_contacto__c  	= objCiudad.Id;
			objContacto.BI_Tipo_de_contacto__c    			= 'Autorizado';
            //REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c		= True;
            //REING-FIN
			Insert objContacto;

			//Direccion COL
			BI_Sede__c objSede 								= new BI_Sede__c();
			objSede.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
			objSede.BI_Direccion__c 				= 'Test Street 1223 Number 3221';
			objSede.BI_Localidad__c 				= 'Test Local';
			objSede.BI_COL_Estado_callejero__c 		= System.Label.BI_COL_LbValor3EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c 		= 'Libre';
			objSede.BI_Country__c 					= 'Colombia';
			objSede.Name 							= 'Test Street 1223 Number 3221, Test Local Colombia';
			objSede.BI_Codigo_postal__c 			= '123562';
			objSede.BI_Provincia__c = 'pruebaProviencia';
			objSede.BI_Apartamento__c = 'apto';
			objSede.BI_Piso__c = 'pisoTest';
			objSede.BI_Distrito__c = 'TestDistrito';

			insert objSede;

			System.debug('Datos Sedes ===> ' + objSede);

			//Sede COL
			BI_Punto_de_instalacion__c objPuntosInsta 	= new BI_Punto_de_instalacion__c ();
			objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
			objPuntosInsta.BI_Sede__c          = objSede.Id;
			objPuntosInsta.BI_Contacto__c      = objContacto.id;
			objPuntosInsta.Name 				= 'Nombre sede prueba2';
			objPuntosInsta.BI_Tipo_de_sede__c 	= 'Comercial Principal';
			insert objPuntosInsta;
			System.debug('Datos Sucursales ===> ' + objPuntosInsta);

		}
	}	


   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        ??????
        Company:       ??????
        Description:   ?????? 
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        ??/??/????                   	????????					Initial version
        25/06/2016						Humberto Nunes 				Se agrego BI_TestUtils.throw_exception 
        															Se Adiciono el Mapa mapNews para hacer la llamada  al metodo updateUsershippingAddress
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	public static void updatePuntoInstalacionTRS_TEST() {
		BI_TestUtils.throw_exception = false;

		map<id,BI_Sede__c> mapNews = new map<id,BI_Sede__c>([select id,BI_Localidad__c,BI_Country__c,BI_Longitud__Latitude__s,BI_Longitud__Longitude__s,BI_Distrito__c,BI_Provincia__c,BI_Codigo_postal__c,BI_Direccion__c,BI_Numero__c,BI_Piso__c,BI_Apartamento__c from BI_Sede__c ]);
		BI_SedeMethods.updateUsershippingAddress (mapNews);

		Profile profile_COL = [SELECT ID FROM Profile WHERE Name = 'BI_Standard_COL' limit 1];
		List<User> lst_usr = BI_DataLoad.loadUsers(1, profile_COL.Id, 'Jefe de Ventas' );		
	
		system.runAs(lst_usr[0]){
			BI_bypass__c objBibypass = new BI_bypass__c(BI_skip_trigger__c=false,BI_migration__c=false,BI_stop_job__c=false,BI_skip_assert__c=false);	
			insert objBibypass;

			List<BI_Sede__c> lst_sede = [SELECT Id, (SELECT Id FROM Puntos_de_instalacion__r) FROM  BI_Sede__c limit 100];
			System.assert(!lst_sede.isEmpty());

			for(BI_Sede__c sede :lst_sede){
				sede.BI_Codigo_postal__c = '11111';
			}
			BI_SedeMethods.updatePuntoInstalacionTRS(lst_sede);
		}	

		BI_TestUtils.throw_exception = true;		
	}

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        ??????
        Company:       ??????
        Description:   ?????? 
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        ??/??/????                   	????????					Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	public static void updatePuntoInstalacionTRS_catchTEST() {
		BI_TestUtils.throw_exception = true;

		BI_SedeMethods.updatePuntoInstalacionTRS(null);
		List<BI_Log__c> lst_log = [SELECT Id FROM BI_Log__c WHERE BI_Asunto__c = 'BI_SedeMethods.updatePuntoInstalacionTRS'];
		System.assert(!lst_log.isEmpty());
	}



    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        ??????
        Company:       ??????
        Description:   ?????? 
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        ??/??/????                   	????????					Initial version
        25/06/2016						Humberto Nunes 				Se ejecuta el proceso como administrador de sistema 
                                                                    Se elimino la variable lst_biSede y lstSedes
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void createData()
	{
		list <UserRole> lstRoles;
		User objUsuario = new User();
		List<String> lstIdSede = new List<String>();
		
		list<BI_Log__c> lstLog = new list <BI_Log__c>();

		String en_US = 'en_US';
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        String region = Label.BI_Argentina;
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name = 'Administrador del sistema' limit 1];
        User user = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = en_US,
                          localesidkey = en_US,
                          ProfileId = prof.Id,
                          BI_Permisos__c = 'Sucursales',
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com',
                          Pais__c = Label.BI_Argentina);
        
        insert user;    
           
        System.runAs(user)
        {
			Account objAccount											= new Account();
			objAccount.BI_Segment__c                         			= 'Empresas';
			objAccount.BI_Subsegment_Regional__c             			= 'Mayoristas';
			objAccount.BI_Subsector__c                       			= 'Banca';
			objAccount.BI_Sector__c                          			= 'Industria';  
			objAccount.Name												= 'TSTAccount';
			objAccount.BI_Segment__c                         = 'test';
			objAccount.BI_Subsegment_Regional__c             = 'test';
			objAccount.BI_Territory__c                       = 'test';
			insert objAccount;

			BI_Col_Ciudades__c objCity									= new BI_Col_Ciudades__c();
			objCity.BI_COL_Codigo_ciudad__c								= 'BOG';
			objCity.BI_COl_ID_Colombia__c								= 'COL';
			objCity.BI_COL_Pais__c										= 'Colombia';
			insert objCity;

			BI_Sede__c objAddress										= new BI_Sede__c();
			objAddress.BI_COL_Ciudad_Departamento__c = objCity.Id;
			objAddress.BI_Direccion__c = 'Test Street 123 Number 321';
			objAddress.BI_Localidad__c = 'Test Local';
			//objAddress.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor3EstadoCallejero;
			objAddress.BI_COL_Sucursal_en_uso__c = 'Libre';
			objAddress.BI_Country__c = 'Colombia';
			objAddress.Name = 'Test Street 123 Number 321, Test Local Colombia';
			objAddress.BI_Codigo_postal__c = '12356';
			objAddress.BI_COL_Estado_callejero__c = 'Validado por callejero';
			insert objAddress;

			BI_Sede__c objAddress2										= new BI_Sede__c();
			objAddress2.BI_COL_Ciudad_Departamento__c = objCity.Id;
			objAddress2.BI_Direccion__c = 'Test Street 123 Number 321';
			objAddress2.BI_Localidad__c = 'Test Local';
			objAddress2.BI_COL_Estado_callejero__c = 'Validado por callejero';
			objAddress2.BI_COL_Sucursal_en_uso__c = 'Libre';
			objAddress2.BI_Country__c = 'Colombia';
			objAddress2.Name = 'Test Street 123 Number 321, Test Local Colombia';
			objAddress2.BI_Codigo_postal__c = '1235';
			insert objAddress2;

			BI_Log__c objLog											= new BI_Log__c();
			objLog.BI_COL_Informacion_recibida__c						= 'OK, Respuesta Procesada';
			//objLog.BI_COL_Contacto__c 									= lstContacts[0].Id;
			objLog.BI_COL_Estado__c										= 'FALLIDO';
			objLog.BI_COL_Interfaz__c									= 'NOTIFICACIONES';
			lstLog.add(objLog);	
			insert lstLog;	

			Contact objContact = new Contact();
			objContact.FirstName = 'pruebaContac';
			objContact.LastName	= 'pruebaContac';
			objContact.MobilePhone = '3681269853';
			objContact.BI_Activo__c = true;
			objContact.BI_Contacto_mercadeo__c = false;
			objContact.BI_No_recibir_llamadas__c = false;
			objContact.BI_Numero_de_documento__c = '1085763264';
			objContact.BI_Proveedor__c = false;
			objContact.BI_Regalos_empresariales__c = false;
			objContact.BI_Representante_legal__c = false;
			objContact.Estado__c = '';
			objContact.Idioma__c = '';
			objContact.Invitar_a_Eventos_de_Relacionamiento__c		= false;
			objContact.BI_Filtro_de_busqueda__c						= 'all';
			objContact.TEMPEXT_ID__c								= objAccount.Id;
			objContact.Certa_SCP__active__c							= true;
			objContact.TGS_Circulo_Allegados__c						= false;
			objContact.TGS_No_Enviar_Newsletter__c					= false;
			objContact.TGS_Surveys__c								= false;
			objContact.TGS_Enviar_Email_Billing_Inquiry__c			= true;
			objContact.TGS_Enviar_Email_Change__c					= true;
			objContact.TGS_Enviar_Email_Complaint__c				= true;
			objContact.TGS_Enviar_Email_Incident__c					= true;
			objContact.TGS_Enviar_Email_Order_Management_Case__c	= true;
			objContact.TGS_Enviar_Email_Query__c					= true;
			objContact.BI_COL_Contacto_principal_holding__c			= false;
			objContact.BI_COL_Potestad_para_firmar__c				= false;
			objContact.BI_COL_Principal__c							= false;
			objContact.BI_COL_Fotocopia_Cedula__c					= false;
			objContact.Phone										= '68544912';
			objContact.Email										= 'testContact1@test.com';
			objContact.BI_Tipo_de_contacto__c						= 'TST';
			objContact.BI_COL_Ciudad_Depto_contacto__c				= objCity.Id;
			objContact.BI_COL_Direccion_oficina__c					= 'K 54';
            //REING-INI
            objContact.BI_Tipo_de_documento__c 					= 'Otros';
            objContact.BI_Numero_de_documento__c 					= '00000000X';
            objContact.FS_CORE_Acceso_a_Portal_Platino__c			= True;
            //REING-FIN
			objContact.AccountId = objAccount.id;
			insert objContact;


			Map<String, List<Object>> mapSedeInfo = new Map<String, List<Object>>
			{
				'Sede1'	=> new List<Object>{
					'prueba', objContact.Id, objAccount.Id, objContact.Id,
					'Principal', objAddress.Id, 'Si'
				},
				'Sede2'	=> new List<Object>{
					'prueba2', objContact.Id, objAccount.Id, objContact.Id,
				 	'Principal', objAddress.Id, 'No'
				},
				'Sede3'	=> new List<Object>{
					'prueba3', objContact.Id, objAccount.Id, objContact.Id,
					'Principal', objAddress2.Id, 'Si'
				}
			};

			for( String strKeySede : mapSedeInfo.keySet() )
			{
				BI_Punto_de_instalacion__c objSede					= new BI_Punto_de_instalacion__c();
				objSede.Name										= 'Nombre Sede' + String.valueOf( mapSedeInfo.get( strKeySede ).get( 0 ) );
				objSede.BI_Contacto__c								= String.valueOf( mapSedeInfo.get( strKeySede ).get( 1 ) );
				objSede.BI_Cliente__c								= String.valueOf( mapSedeInfo.get( strKeySede ).get( 2 ) );
				objSede.BI_COL_Contacto_instalacion__c				= String.valueOf( mapSedeInfo.get( strKeySede ).get( 3 ) );
				objSede.BI_COL_Tipo_sede__c							= String.valueOf( mapSedeInfo.get( strKeySede ).get( 4 ) );
				objSede.BI_Sede__c									= String.valueOf( mapSedeInfo.get( strKeySede ).get( 5 ) );
				objSede.BI_COL_Banda_ancha__c						= String.valueOf( mapSedeInfo.get( strKeySede ).get( 6 ) );
			}	

		}		
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Alvaro Sevilla
        Company:       Aborda
        Description:   Nuevo proceso para probar el borrado de una sede. 
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        ??/??/????                   	Alvaro Sevilla              Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	public static  void Delete_TEST()
	{
		list<BI_Sede__c> lstSede = new list<BI_Sede__c>();
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name = 'Administrador del sistema' limit 1];
        User user = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof.Id,
                          BI_Permisos__c = 'Sucursales',
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'userTEST@testorg.com',
                          Pais__c = 'Colombia');
        
        insert user;    
           
        System.runAs(user)
        {
			Test.startTest();
			
			BI_Col_Ciudades__c objCity			= new BI_Col_Ciudades__c();
			objCity.BI_COL_Codigo_ciudad__c		= 'BOG';
			objCity.BI_COl_ID_Colombia__c		= 'COL';
			objCity.BI_COL_Pais__c				= 'Colombia';
			insert objCity;

			BI_Sede__c objAddress = new BI_Sede__c();
			objAddress.BI_COL_Ciudad_Departamento__c = objCity.Id;
			objAddress.BI_Direccion__c = 'Street 125';
			objAddress.BI_Localidad__c = 'Test Local';
			objAddress.BI_COL_Sucursal_en_uso__c = 'Libre';
			objAddress.BI_Country__c = 'Colombia';
			objAddress.Name = 'Test Local Colombia';
			objAddress.BI_Codigo_postal__c = '11356';
			objAddress.BI_COL_Estado_callejero__c = 'Validado por callejero';
			lstSede.add(objAddress);

			insert lstSede;
			lstSede[0].BI_Direccion__c = 'Street 1555';
			update lstSede;
			delete lstSede;		

			Test.stopTest();
		}		
	}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Eduardo ventura
	Company:       Everis España
	Description:   Actualización del estado de normalización de la dirección para Chile

	History:
	<Date>						<Author>						<Change Description>
	12/06/2017					Eduardo Ventura					Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest
    public static void updateStatus(){
        /* Variable Configuration */
        /* Sede */
        BI_Sede__c sede = new BI_Sede__c(Estado__c = 'Validado', Name = 'Test-Address', BI_Direccion__c ='Manoteras',
                                         BI_Numero__c ='13', BI_Provincia__c ='Santiago', BI_Country__c ='Chile',
                                         BI_Codigo_postal__c = '28050', BI_Distrito__c = 'Chile',
                                         BI_Localidad__c = 'Santiago', BI_Piso__c = '2', BI_Apartamento__c ='A',
                                         FS_CHI_Bloque__c = '10', FS_CHI_Tipo_Complejo_Habitacional__c = 'A', FS_CHI_Nombre_Complejo_Habitacional__c = 'A',
                                         FS_CHI_Numero_de_Casilla__c = '10');
        
        insert sede;
        
        /* Process Data */
        System.Test.startTest();
        
        /* Update Info */
        sede.Name = 'MOD';
        sede.Estado__c = 'Validado';
        update sede;
        
        System.assertEquals('Pendiente de validar', [SELECT Estado__c FROM BI_Sede__c LIMIT 1][0].Estado__c);
        System.Test.stopTest();
    }
}