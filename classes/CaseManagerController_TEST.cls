@isTest 
private class CaseManagerController_TEST
{
  static testMethod void myUnitTest()
    {
        Id holdingAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Holding').getRecordTypeId();
        Account acc = new Account(Name='Account', RecordTypeId=holdingAccountRecordTypeId);
      insert acc;
      RecordType RTProd = [SELECT Id FROM RecordType WHERE (SobjectType = 'NE__Product__c' OR SobjectType = 'Product__c') AND Name = 'Standard'];
      NE__Product__c prod1 = new NE__Product__c(Name='Product 1', RecordTypeId=RTProd.Id);
      insert prod1;
      NE__Order__c ord1 = new NE__Order__c(NE__AccountId__c=acc.Id);
      insert ord1;
      NE__Order__c ord2 = new NE__Order__c(NE__AccountId__c=acc.Id);
      insert ord2;
      NE__Order__c ord3 = new NE__Order__c(NE__AccountId__c=acc.Id);
      insert ord3;
      NE__Order__c ord4 = new NE__Order__c(NE__AccountId__c=acc.Id);
      insert ord4;
      NE__Order__c ord5 = new NE__Order__c(NE__AccountId__c=acc.Id);
      insert ord5;
        Id caseOrderManagementRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RTYPE_ORDER_MNGMNT).getRecordTypeId();
      Case case1 = new Case(Subject=Constants.CASE_RTYPE_ORDER_MNGMNT, AccountId=acc.Id, Order__c=ord1.Id, Status='Assigned', TGS_Service__c=prod1.Name, Type='New', RecordTypeId=caseOrderManagementRecordTypeId);
        insert case1;
        Case case2 = new Case(Subject=Constants.CASE_RTYPE_ORDER_MNGMNT, AccountId=acc.Id, Order__c=ord2.Id, Status='Assigned', TGS_Service__c=prod1.Name, Type='New', RecordTypeId=caseOrderManagementRecordTypeId);
      insert case2;
        Case case3 = new Case(Subject=Constants.CASE_RTYPE_ORDER_MNGMNT, AccountId=acc.Id, Order__c=ord3.Id, Status='Assigned', TGS_Service__c=prod1.Name, Type='New', RecordTypeId=caseOrderManagementRecordTypeId);
      insert case3;
        Case case4 = new Case(Subject=Constants.CASE_RTYPE_ORDER_MNGMNT, AccountId=acc.Id, Order__c=ord4.Id, Status='Assigned', TGS_Service__c=prod1.Name, Type='New', RecordTypeId=caseOrderManagementRecordTypeId);
      insert case4;
        Case case5 = new Case(Subject=Constants.CASE_RTYPE_ORDER_MNGMNT, AccountId=acc.Id, Order__c=ord5.Id, Status='Assigned', TGS_Service__c=prod1.Name, Type='New', RecordTypeId=caseOrderManagementRecordTypeId);
    insert case5;
        TGS_Status_machine__c sm = new TGS_Status_machine__c(TGS_Prior_Status__c='Assigned', TGS_Prior_Status_reason__c='', TGS_Service__c=prod1.Id, TGS_Status__c='In Progress', TGS_Status_reason__c='In Provision');
        insert sm;
        
        CaseManagerController caseMngr = new CaseManagerController();
        
        caseMngr.filterObject.AccountId = acc.Id;
        caseMngr.filterObject.Order__c = ord1.Id;
        caseMngr.filterStatus = 'Assigned';
        caseMngr.addFilter();
        caseMngr.filterObject = new Case();
    caseMngr.filterStatus = '';
    caseMngr.addFilter();
        
        caseMngr.goToNextPage();
        caseMngr.goToPreviousPage();
        
        caseMngr.goToChangeStatusSection();
        
        CaseManagerController.UpdateStatus updStatus = new CaseManagerController.UpdateStatus();
        updStatus.priorStatus = 'Assigned';
        updStatus.priorStatusReason = '';
        updStatus.newStatus = 'In Progress';
        updStatus.newStatusReason = 'In Provision';
        
        caseMngr.listStatusToChange.add(updStatus);        
        caseMngr.updateCases();
    }
}