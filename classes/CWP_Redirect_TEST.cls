/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Óscar González Andrés 
    Company:       Salesforce.com
    Description:   Methods executed by User Triggers 
    Test Class:    CWP_Redirect
    
    History:
     
    <Date>                  <Author>                <Change Description>
    28/02/2017              Óscar González          Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class CWP_Redirect_TEST {
    
    /*******************************
    *** Inicializamos valores    ***        
    *******************************/
    @testSetup 
    private static void dataModelSetup() {               
                
        //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
                
        set <string> setStrings = new set <string>{
            'Account', 
            'Case',
            'NE__Order__c',
            'NE__OrderItem__c'
        };
                
        //ACCOUNT
        map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            //rtMapBydevName.put(i.DeveloperName, i.id);
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;
        Account accCustomerCountry;
        Account accLegalEntity;
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;
        }          
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        insert contactTest;
        Contact contactTest2 = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest2');
        insert contactTest2;  
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            User usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        
        //USER
        UserRole rolUsuario2;
        User unUsuario2;
        System.RunAs(new User(id=userInfo.getUserId())){
            //rolUsuario2 = CWP_TestDataFactory.createRole('rol 0');
            //rolUsuario2 = CWP_TestDataFactory.createRole('rol 2');
            //insert rolUsuario2;
            Profile perfil2 = CWP_TestDataFactory.getProfile('Customer Community Plus User');
            unUsuario2 = CWP_TestDataFactory.createUser('nombre2', null, perfil2.id);
            unUsuario2.contactId = contactTest2.id;
            insert unUsuario2;
        }     
        
        
                       
    }
    
    @isTest
    private static void myUnitTest1() {
                
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        
        System.runAs(usuario){                          
            Test.startTest();                        
            PageReference pageRef = Page.CWP_Redirect_MyServices;
            Test.setCurrentPage(pageRef); 
            //CWP_Redirect c = new CWP_Redirect();         
            CWP_Redirect.dotestpage('doc');
            CWP_Redirect.dotestpage('myService');                   
            Test.stopTest();
        }
                 
    }
    
    @isTest
    private static void myUnitTest2() {     
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre2'];             
        
        System.runAs(usuario){                          
            Test.startTest();                        
            PageReference pageRef = Page.CWP_Redirect_MyServices;
            Test.setCurrentPage(pageRef); 
            //CWP_Redirect c = new CWP_Redirect();         
            CWP_Redirect.dotestpage('doc');
            CWP_Redirect.dotestpage('myService');                  
            Test.stopTest();
        }
                 
    }
}