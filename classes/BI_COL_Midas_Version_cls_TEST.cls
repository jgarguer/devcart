@isTest
private class BI_COL_Midas_Version_cls_TEST {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Test method that Manage the code coverage from BI_COL_Midas_version_cls.validateStatus()
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        02/09/2016                      Guillermo Muñoz             Initial version
        17/01/2017                      Pedro Párraga               Add Method getUSD() and exception to test Method getUSD() and validateStatus()
        27/02/2017                      GSPM                        Modify Class
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*@isTest static void validateStatus_TEST(){

        BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        User me = [SELECT Id FROM User WHERE Id =: Userinfo.getUserId() Limit 1];

        //Insert de usuario de colombia
        User usu = new User(
            alias = 'UsuTEST',
            email = 'UserTest_Test@testorg.com',
            emailencodingkey = 'UTF-8',
            lastname = 'Testing' ,
            languagelocalekey='en_US',
            localesidkey = 'en_US',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'BI_Standard_COL' LIMIT 1].Id,
            BI_Permisos__c = 'Jefe de ventas',
            timezonesidkey = Label.BI_TimeZoneLA,
            username = 'ur23@testorg.com',
            BI_CodigoUsuario__c = '123541235we',
            Pais__c = Label.BI_Colombia
        );
        System.runAs(me){
            insert usu;
        }

        //Insert de cliente
        List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Colombia});

        //Insert de oportunidad
        Opportunity opp = new Opportunity(
            Name = 'Test 1',
            CloseDate = Date.today(),
            StageName = Label.BI_F5DefSolucion,
            AccountId = lst_acc[0].Id,
            BI_Country__c = Label.BI_Colombia
        );
        insert opp;

        //Asignación de la oportunidad al usuario de colombia
        opp.OwnerId = usu.Id;
        update opp;

        //Insert de prefactibilidad
        BI_COL_PreFactibilidad__c prefact = new BI_COL_PreFactibilidad__c(
            BI_COL_Proyecto__c = opp.Id,
            BI_COL_Estado_prefactibilidad__c = 'Aprobada',
            BI_COL_Fecha_estado__c = Date.today(),
            BI_COL_Fecha_solicitud__c = Date.today().addDays(2),
            BI_COL_Registro_aprobado__c = 'Aprobada'
        );
        insert prefact;

        ////Insert de midas
        //BI_COL_MIDAS__c midas = new BI_COL_MIDAS__c(
        //  BI_COL_Proyecto__c = opp.Id,
        //  BI_COL_Fecha_solicitud__c = Date.today(),           
        //  BI_COL_Tipo_Solicitud__c = 'Venta Nueva',
        //  BI_COL_Payback__c = 34,
        //  BI_COL_Cobro_Unico__c = 100,
        //  BI_COL_Duracion__c = 12,
        //  BI_COL_Estado_de_la_aprobacion__c = 'Pendiente Aprobacion'
        //);
        //insert midas;

        ////Insert de midas2
        //BI_COL_MIDAS__c midas2 = new BI_COL_MIDAS__c(
        //    BI_COL_Proyecto__c = opp.Id,
        //    BI_COL_Fecha_solicitud__c = Date.today(),           
        //    BI_COL_Tipo_Solicitud__c = 'Venta Nueva',
        //    BI_COL_Payback__c = 34,
        //    BI_COL_Cobro_Unico__c = 100,
        //    BI_COL_Duracion__c = 12,
        //    BI_COL_Estado_de_la_aprobacion__c = 'Pendiente Aprobacion'
        //);
        //insert midas2;

        // //Insert de midas3
        //BI_COL_MIDAS__c midas3 = new BI_COL_MIDAS__c(
        //    BI_COL_Proyecto__c = opp.Id,
        //    BI_COL_Fecha_solicitud__c = Date.today(),           
        //    BI_COL_Tipo_Solicitud__c = 'Venta Nueva',
        //    BI_COL_Payback__c = 34,
        //    BI_COL_Cobro_Unico__c = 100,
        //    BI_COL_Duracion__c = 12,
        //    BI_COL_Estado_de_la_aprobacion__c = 'Pendiente Aprobacion'
        //);
        //insert midas3;

        //Insert de midas4
        BI_COL_MIDAS__c midas4 = new BI_COL_MIDAS__c(
            BI_COL_Proyecto__c = opp.Id,
            BI_COL_Fecha_solicitud__c = Date.today(),           
            BI_COL_Tipo_Solicitud__c = 'Venta Nueva',
            BI_COL_Payback__c = 34,
            BI_COL_Cobro_Unico__c = 100,
            BI_COL_Duracion__c = 12,
            BI_COL_Estado_de_la_aprobacion__c = 'Pendiente Aprobacion'
        );
        insert midas4;

        Test.startTest();              
        
        midas4.BI_COL_Estado_de_la_aprobacion__c = 'Aprobado';
        update midas4;

        //ProcessDefinition objPD = new ProcessDefinition();
        //objPD.Name = 'ProcessTest';
        //objPD.State = 'Obsolete';
        //objPD.Type = 'Approval';
        //insert objPD;

        //ProcessInstance testInstance = new ProcessInstance();
        //testInstance.targetObjectId = midas4.id;
        //testInstance.ProcessDefinitionId = objPD.id;
        //insert testInstance;
        //ProcessInstanceWorkitem testP = new ProcessInstanceWorkitem(ActorId = UserInfo.getUserId(), 
        //                                                                ProcessInstance = testInstance);

        //ProcessInstance objGI = new ProcessInstance();
        //objGI.Status = 'Approved';
        //ProcessDefinitionId
        //TargetObjectId = midas4.Id;
        //CurrentNodeId
        //insert objGI;

        //Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        //    req.setComments('Approve.');
        //    req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        //    req.setObjectId(midas4.Id);

        //    //Submit the approval request
        //    Approval.ProcessResult result = Approval.process(req);


        //System.runAs(usu){
        //  //Cambio de Aprobado a Rechazado con permisos diferentes de Ingeniería/Preventa
           
        //  midas.BI_COL_Estado_de_la_aprobacion__c = 'Rechazado - Inactivo';
        //  //try{
        //      update midas;
        //      //throw new BI_Exception('No se realizó la validación correctamente');
        //  //}catch(DMLException exc){}
        //}

        //Cambio de los permisos a Ingeniería/Preventa
        //usu.BI_Permisos__c = Label.BI_Ingenieria_preventa;
        //System.runAs(me){
        //  update usu;
        //}

      //  System.runAs(usu){
      //    //Cambio de Aprobado a Rechazado con permisos de Ingeniería/Preventa pero modificando algún campo más
      //      midas2.BI_COL_Estado_de_la_aprobacion__c = 'Rechazado - Inactivo';
      //      midas2.BI_COL_Payback__c = 0;
      //      //try{
      //          update midas2;
      //      //  throw new BI_Exception('No se realizó la validación correctamente');
      //      //}catch(DMLException exc){}

      //      //Cambio de Aprobado a Rechazado con permisos de Ingeniería/Preventa pero sin mudificar ningún campo más
      //    midas3.BI_COL_Estado_de_la_aprobacion__c = 'Aprobado - Inactivo';
      //    midas3.BI_COL_Payback__c = 34;
      //    update midas3;

            ////Cambio de un registro en estado Rechazado - Inactivo
      //    midas4.BI_COL_Payback__c = 0;
      //      List<BI_COL_MIDAS__c> cmi = new List<BI_COL_MIDAS__c>();
      //      cmi.add(midas4);
      //      //BI_COL_Midas_Version_cls cm = new BI_COL_Midas_Version_cls();
      //      //BI_COL_Midas_Version_cls.midastgr(cmi);
      //    //try{
      //        update midas4;
      //    //  throw new BI_Exception('No se realizó la validación correctamente');
      //    //}catch(DMLException exc){}
      //  }

        Test.stopTest();

    }*/

    //@isTest static void getUSD(){
    //    BI_COL_Midas_Version_cls.getUSD();

    //}

    //@isTest static void excepcion(){
    //    BI_COL_Midas_Version_cls.validateStatus(null, null);
    //}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       NEAborda
        Description:   Test method that Manage the code coverage from BI_COL_Midas_version_cls.SendEmail()
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        28/02/2017                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void SendEmail_TEST(){

        BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        User me = [SELECT Id FROM User WHERE Id =: Userinfo.getUserId() Limit 1];

        //Insert de usuario de colombia
        User usu = new User(
            alias = 'UsuTEST',
            email = 'UserTest_Test@testorg.com',
            emailencodingkey = 'UTF-8',
            lastname = 'Testing' ,
            languagelocalekey='en_US',
            localesidkey = 'en_US',
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'BI_Standard_COL' LIMIT 1].Id,
            BI_Permisos__c = 'Jefe de ventas',
            timezonesidkey = Label.BI_TimeZoneLA,
            username = 'ur23@testorg.com',
            BI_CodigoUsuario__c = '123541235we',
            Pais__c = Label.BI_Colombia
        );
        System.runAs(me){
            insert usu;
        }

        //Insert de cliente
        List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Colombia});

        //Insert de oportunidad
        Opportunity opp = new Opportunity(
            Name = 'Test 1',
            CloseDate = Date.today(),
            StageName = Label.BI_F5DefSolucion,
            AccountId = lst_acc[0].Id,
            BI_Country__c = Label.BI_Colombia
        );
        insert opp;

        //Asignación de la oportunidad al usuario de colombia
        opp.OwnerId = usu.Id;
        update opp;

        //Insert de prefactibilidad
        BI_COL_PreFactibilidad__c prefact = new BI_COL_PreFactibilidad__c(
            BI_COL_Proyecto__c = opp.Id,
            BI_COL_Estado_prefactibilidad__c = 'Aprobada',
            BI_COL_Fecha_estado__c = Date.today(),
            BI_COL_Fecha_solicitud__c = Date.today().addDays(2),
            BI_COL_Registro_aprobado__c = 'Aprobada'
        );
        insert prefact;

        //Insert de midas
        BI_COL_MIDAS__c midas = new BI_COL_MIDAS__c(
            BI_COL_Proyecto__c = opp.Id,
            BI_COL_Fecha_solicitud__c = Date.today(),           
            BI_COL_Tipo_Solicitud__c = 'Venta Nueva',
            BI_COL_Payback__c = 34,
            BI_COL_Cobro_Unico__c = 100,
            BI_COL_Duracion__c = 12,
            BI_COL_Estado_de_la_aprobacion__c = 'Pendiente Aprobacion'
        );
        insert midas;

        Test.startTest();              
        
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();

        req1.setComments('Enviando a aprobación');
        req1.setObjectId(midas.id);
         
        // Hacemos que el propietario del MIDAS envía el registroa a aprobación
        req1.setSubmitterId(usu.Id);
         
        // Lo envíamos al proceso de aprobación requerido y hacemos que se salte los criterios de entrada
        req1.setProcessDefinitionNameOrId('BI_COL_Aprobacion_MIDAS2');
        req1.setSkipEntryCriteria(true);
         
        // Envíamos el midas al proceso de aprobación
        Approval.ProcessResult result = Approval.process(req1);
         
        // Comprobamos el estado de la solicitud de aprobación
        System.assert(result.isSuccess());

        List<Id> newWorkItemIds = result.getNewWorkitemIds();

        //Preparamos el trigger para que detecte el cambio de estado de la solicitud de aprobación
        NETriggerHelper.setTriggerFiredTest('BI_COL_Midas_Version_cls.SendEmail', false);


        //Aprobamos la solicitud y eso producirá el cambio de estado del midas
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();

        req2.setComments('Aprobando');

        req2.setAction('Approve');

        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});

        req2.setWorkitemId(newWorkItemIds.get(0));

        Approval.ProcessResult result2 =  Approval.process(req2);

        System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());

        Test.stopTest(); 
    }   
}