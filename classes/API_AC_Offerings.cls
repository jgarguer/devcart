@RestResource(urlMapping = '/offerings')
global class API_AC_Offerings {
	private static final String COUNTRYPARAM	= 'category.id';
	private static final String CITYPARAM		= 'category.name';
	private static final String CONTRACTPARAM	= 'name';
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:			Angel Felipe Santaliestra Pasias
    Company:		Accenture
    Description:	Method that manage the GET API call.
    
    History:
	<Date>			<Author>					<Description>
    20/04/2018		Angel F Santaliestra		Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static void doGet() {
		System.debug('API_AC_Offerings##doGet');
		getOfferings();
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:			Angel Felipe Santaliestra Pasias
    Company:		Accenture
    Description:	Method that retrieve the offerings for the country-city-contract value required by the request.
    
    History:
	<Date>			<Author>					<Description>
    20/04/2018		Angel F Santaliestra		Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static void getOfferings() {
		System.debug('API_AC_Offerings##getOfferings');
		RestRequest request = RestContext.request;
		
		String country = request.params.get(COUNTRYPARAM);
		String city = request.params.get(CITYPARAM);
		Decimal contractTerm = Decimal.valueOf(request.params.get(CONTRACTPARAM));

		System.debug('URL Params ==> country : ' + country + ',city : ' + city + ',contractTerm : ' + contractTerm);

		List<WrpOfferingType> listOfferingType = getData(country,city,contractTerm);
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(listOfferingType));
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:			Angel Felipe Santaliestra Pasias
    Company:		Accenture
    Description:	Method that retrieve the information from the database required by the request.
    
    History:
	<Date>			<Author>					<Description>
    20/04/2018		Angel F Santaliestra		Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static List<WrpOfferingType> getData(String country,String city,Decimal contractTerm) {
		System.debug('API_AC_Offerings##getData >=> Params ==> country : ' + country + ',city : ' + city + ',contractTerm : ' + contractTerm);
        List<CWP_AC_ServicesCost__c> listServiceCosts = [	SELECT	CWP_AC_Country__c,
																	CWP_AC_City__c,
																	CWP_AC_Contract_Term__c,
																	CWP_AC_ServiceName__c,
																	CWP_AC_Bandwidth__c,
																	CWP_AC_Router__c
															FROM CWP_AC_ServicesCost__c
															WHERE CWP_AC_Country__c			= :country
																AND CWP_AC_City__c			= :city
																AND CWP_AC_Contract_Term__c	= :contractTerm];
		
		System.debug('SOQL Query ==> listServiceCosts : ' + listServiceCosts);

		List<WrpOfferingType> listCategoryTypes = new List<WrpOfferingType>();
		for (CWP_AC_ServicesCost__c item : listServiceCosts)
			listCategoryTypes.add(new WrpOfferingType(item.CWP_AC_ServiceName__c,'' + item.CWP_AC_Bandwidth__c,item.CWP_AC_Router__c));
		return listCategoryTypes;
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:			Angel Felipe Santaliestra Pasias
    Company:		Accenture
    Description:	Wrapper of the response.
    
    History:
	<Date>			<Author>					<Description>
    20/04/2018		Angel F Santaliestra		Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global class WrpOfferingType {
		String name;				// Aqui se guardaria el servicio. MPLS o INTERNET o ETHERNET
		String description;			// Aqui ira el bandwidth.
		String frameworkAgreement;	// Aqui ira el router. Yes o No.
		/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    	Author:			Angel Felipe Santaliestra Pasias
    	Company:		Accenture
    	Description:	Constructor.
    	
		History:
		<Date>			<Author>					<Description>
    	20/04/2018		Angel F Santaliestra		Initial version.
    	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
		public WrpOfferingType(String name,String bandwidth,String router) {
			this.name				= name;
			this.description		= bandwidth;
			this.frameworkAgreement	= router;
		}
	}
}