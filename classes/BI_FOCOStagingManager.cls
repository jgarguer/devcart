global with sharing class BI_FOCOStagingManager  implements Schedulable{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Bejoy Babu
    Company:       Salesforce.com
    Description:   Manages and orchestrates the load of Registro Datos FOCO table from staging table.
    
    History:
    
    <Date>            <Author>              <Description>
    09/26/2014        Bejoy Babu            Initial version
    11/12/2015		  Micah Burgos	        Add field BatchDelete_Size_c__c for quick delete.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public String country;
    Map<String, BI_FOCO_Pais__c> mcs;

	public BI_FOCOStagingManager(String c) {

		country = c;
		mcs =  BI_FOCO_Pais__c.getAll();

		
	}

	global void execute(SchedulableContext sc) {
		processStageToMain();
	}

	public void processStageToMain(){

		System.debug('-------------------------------------------BI_FOCOStagingManager.processStageToMain(): Start: '+country);
					
		

			//delete records from the main table
			BI_FOCOSBatchDelete batchDel = new BI_FOCOSBatchDelete(country, 'BI_Registro_Datos_FOCO__c');
			Id delBatchId = Database.executeBatch(batchDel, (Integer)mcs.get(country).BI_BatchDelete_Size__c);


			/* MOVED THIS TO THE finish() method of the BI_FOCOSBatchDelete, so that if the BI_FOCOSBatchDelete finishes *only* on the BI_Registro_Datos_FOCO__c entity
			   , it will kick off the BI_FOCOBatchInsert */
			//insert records into main table from stage
			//BI_FOCOBatchInsert batchIns = new BI_FOCOBatchInsert(country);
			//Id insBatchId = Database.executeBatch(batchIns, (Integer)mcs.get(country).Batch_Size__c);
			//System.scheduleBatch(batchIns, 'BI_FOCOBatchInsert_' + DateTime.now().getTime(), 10, (Integer)mcs.get(country).Batch_Size__c);
		
		System.debug('-------------------------------------------BI_FOCOStagingManager.processStageToMain(): End: '+country);
		
		}
		
	
}