/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-04-23      Manuel Esthiben Mendez Devia (MEMD)     Cloned Class
*************************************************************************************************************/
public with sharing class BI_COL_ChangeMassiveBranch_ctr 
{

	public BI_Punto_de_instalacion__c sucursal{get;set;}
	public BI_Punto_de_instalacion__c sucursalNueva{get;set;}
	public String idSucursal{get;set;}
	public BI_COL_Modificacion_de_Servicio__c ms{get;set;}
	public map<Id, BI_Punto_de_instalacion__c> idSucursalesCliente =new map<Id,BI_Punto_de_instalacion__c> (); 
	public boolean afectaMs{get;set;}	
	public boolean afectaDs{get;set;}
	public String mensaje{get;set;} 
	public ws_wwwTelefonicaComNotificacionessalesfo.cuentaType lstCuentasDavox=new ws_wwwTelefonicaComNotificacionessalesfo.cuentaType();
	public ws_wwwTelefonicaComNotificacionessalesfo.peticionType lstMsDrs=new ws_wwwTelefonicaComNotificacionessalesfo.peticionType();
	
	public List<BI_COL_Modificacion_de_Servicio__c> lstMS{
		get{
			List<BI_COL_Modificacion_de_Servicio__c> lstMS=new List<BI_COL_Modificacion_de_Servicio__c>();
			lstMS=[select Name, BI_COL_Codigo_unico_servicio__c,BI_COL_Producto__r.Name,BI_COL_Descripcion_Referencia__c,
							BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Sucursal_de_Facturacion__c, BI_COL_Codigo_unico_servicio__r.BI_COL_Sede_Destino__c, 
							BI_COL_Sucursal_Origen__c, /*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c 
					from BI_COL_Modificacion_de_Servicio__c 
					where BI_COL_Sucursal_de_Facturacion__c=:sucursal.Id
					or BI_COL_Sucursal_de_Facturacion__c=:sucursal.Id
					or BI_COL_Sucursal_Origen__c=:sucursal.Id]; //10/11/2016 GSPM: Se comentó el campo antiguo y se adiciono nuevo campo davox1
			return lstMS; 
		}set;}	
		
	public List<BI_COL_Descripcion_de_servicio__c> lstDS{
		get{
			List<BI_COL_Descripcion_de_servicio__c> lstDS=new List<BI_COL_Descripcion_de_servicio__c>();
			lstDS=[select BI_COL_Sede_Destino__c, BI_COL_Sede_Origen__c,BI_COL_MS_de_Parque__r.BI_COL_Codigo_unico_servicio__c,BI_COL_MS_de_Parque__r.BI_COL_Descripcion_Referencia__c, BI_COL_Producto_Telefonica__r.Name, Name /*Descripcion_Referencia1__c*/ from BI_COL_Descripcion_de_servicio__c where BI_COL_Sede_Destino__c=:sucursal.Id or BI_COL_Sede_Origen__c=:sucursal.Id];
			return lstDS;
		}set;}
		
	
	public BI_COL_ChangeMassiveBranch_ctr(ApexPages.Standardcontroller controller){
		ms=new BI_COL_Modificacion_de_Servicio__c();
		sucursalNueva=new BI_Punto_de_instalacion__c();
		afectaMs=false;
		afectaDs=false;
		consultaObj();
		mensaje=label.BI_COL_lbNohayResgistros;
	}
	
	/********* método encargado de hacer la consulta de los registros afectados y la sucursal que se desea cambiar *********/
	public void consultaObj(){
		idSucursal=ApexPages.currentPage().getParameters().get('Id');
		sucursal=[select Id,Name, BI_Cliente__c,BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c, BI_Sede__c from BI_Punto_de_instalacion__c where Id=:idSucursal limit 1];
		List<BI_Punto_de_instalacion__c> lstSucCliente=[select Id,BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c, BI_Sede__c from BI_Punto_de_instalacion__c where BI_Cliente__c=:sucursal.BI_Cliente__c and Id !=: sucursal.Id];
		
		for(BI_Punto_de_instalacion__c suc: lstSucCliente){
			idSucursalesCliente.put(suc.Id, suc);
		}
		
		List<BI_COL_Descripcion_de_servicio__c> lstDS=new List<BI_COL_Descripcion_de_servicio__c>();
		lstDS= [select BI_COL_Sede_Destino__c, BI_COL_Sede_Origen__c,BI_COL_MS_de_Parque__r.BI_COL_Codigo_unico_servicio__c,
						BI_COL_MS_de_Parque__r.BI_COL_Descripcion_Referencia__c, BI_COL_Producto_Telefonica__r.Name, 
						Name /*Descripcion_Referencia1__c */from BI_COL_Descripcion_de_servicio__c where BI_COL_Sede_Destino__c=:sucursal.Id or BI_COL_Sede_Origen__c=:sucursal.Id];
		if(lstDS.size()>0)
			afectaDs=true;
				
		List<BI_COL_Modificacion_de_Servicio__c> lstMS=new List<BI_COL_Modificacion_de_Servicio__c>();
		lstMS=[select Id, Name, BI_COL_Codigo_unico_servicio__c,BI_COL_Producto__r.Name,BI_COL_Codigo_unico_servicio__r.Name,
					 BI_COL_Sucursal_de_Facturacion__c,  BI_COL_Codigo_unico_servicio__r.BI_COL_Sede_Destino__c, BI_COL_Sucursal_Origen__c, 
					 /*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c 
				from BI_COL_Modificacion_de_Servicio__c 
				where BI_COL_Sucursal_de_Facturacion__c=:sucursal.Id 
				or BI_COL_Sucursal_de_Facturacion__c=:sucursal.Id or BI_COL_Sucursal_Origen__c=:sucursal.Id]; //10/11/2016 GSPM: Se comentó el campo antiguo y se adiciono nuevo campo davox1
		if(lstMS.size()>0)
			afectaMs=true;
	}
	
	/**** método encargado de hacer la actualización de los registros afectados *********/
	public void actualizaObjetos(){
		List<BI_COL_Modificacion_de_Servicio__c> nuevaLstMs=new List<BI_COL_Modificacion_de_Servicio__c>();
		List<BI_COL_Modificacion_de_Servicio__c> tmpMs=new List<BI_COL_Modificacion_de_Servicio__c>();
		List<BI_COL_Descripcion_de_servicio__c> tmpDs=new List<BI_COL_Descripcion_de_servicio__c>();
		List<BI_COL_Descripcion_de_servicio__c> nuevaLstDs=new List<BI_COL_Descripcion_de_servicio__c>();
		BI_Punto_de_instalacion__c nuevaSucursal=[select id, BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c from BI_Punto_de_instalacion__c where id=:ms.BI_COL_Sucursal_de_Facturacion__c];
		
		
		//mensaje='';
		if(idSucursalesCliente.containsKey(ms.BI_COL_Sucursal_de_Facturacion__c)){
			
			//system.debug('nuevaSucursal.Ciudad__r.Codigo_DANE__c=='+nuevaSucursal.Ciudad__r.Codigo_DANE__c+'---------------------\n'+idSucursalesCliente.get(ms.BI_COL_Sucursal_de_Facturacion__c).Ciudad__r.Codigo_DANE__c);
			
			if(sucursal.BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c==idSucursalesCliente.get(ms.BI_COL_Sucursal_de_Facturacion__c).BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c){
			
				sucursalNueva=[select Id, Name, BI_Sede__c, BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c from BI_Punto_de_instalacion__c where Id=:ms.BI_COL_Sucursal_de_Facturacion__c limit 1];
				
				String [] cuentaDavoxCliente=new String[]{};
				ws_wwwTelefonicaComNotificacionessalesfo.dsr_msType msDs=null;
				try{
					lstMsDrs.dsr_ms=new ws_wwwTelefonicaComNotificacionessalesfo.dsr_msType[]{};
					for(BI_COL_Modificacion_de_Servicio__c msTmp: lstMS){
						if(sucursal.Id==msTmp.BI_COL_Sucursal_de_Facturacion__c){
							msTmp.BI_COL_Sucursal_de_Facturacion__c=sucursalNueva.Id;
							//cuentaDavoxCliente.add(''+msTmp.BI_COL_Cuenta_facturar_davox__c);//10/11/2016 GSPM: Se comentó el campo antiguo 
							cuentaDavoxCliente.add(''+msTmp.BI_COL_Cuenta_facturar_davox1__c); //10/11/2016 GSPM: Adiciono nuevo campo davox1
						}
						if(sucursal.Id==msTmp.BI_COL_Sucursal_Origen__c)
							msTmp.BI_COL_Sucursal_Origen__c=sucursalNueva.Id;
						if(sucursal.Id==msTmp.BI_COL_Sucursal_de_Facturacion__c)
							msTmp.BI_COL_Sucursal_de_Facturacion__c=sucursalNueva.Id;
						nuevaLstMs.add(msTmp);
						
						msDs=new ws_wwwTelefonicaComNotificacionessalesfo.dsr_msType();
						msDs.ms=msTmp.Name;
						msDs.dsr=msTmp.BI_COL_Codigo_unico_servicio__r.Name;
						lstMsDrs.dsr_ms.add(msDs);
						system.debug('-----bandera 1------');
							
					}			
					
					
					//enviar informacion de las MS afectadas a Davox y TRS
					enviarInfoCambio();
					
							
					update(nuevaLstMs);
					tmpMs=nuevaLstMs;
					lstCuentasDavox.cuenta=cuentaDavoxCliente;
					afectaMs=false;
					
					system.debug('-----bandera 2------'+lstCuentasDavox.cuenta);
					
					for(BI_COL_Descripcion_de_servicio__c dsTmp: lstDS){				
						
						if(sucursal.Id==dsTmp.BI_COL_Sede_Origen__c){
							dsTmp.BI_COL_Sede_Origen__c=sucursalNueva.Id;
						}
						if(sucursal.Id==dsTmp.BI_COL_Sede_Destino__c){
							dsTmp.BI_COL_Sede_Destino__c=sucursalNueva.Id;
						}
						nuevaLstDs.add(dsTmp);
					}
					update(nuevaLstDs);
					tmpDs=nuevaLstDs;
					afectaDs=false;
					mensaje+='\n'+label.BI_COL_LbRegistroDsyMs;
					
					
				}catch(Exception e){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error Actualizando los registros\n'+e.getMessage() +'\n '+e.getCause() ));
					mensaje+='\n'+label.BI_COL_LbErrorActualDsyMs;  
				}
			
				//se crea el log siempre y cuando las variables esten en falso y exista el registro de la nueva sucursal
				if(!afectaMs && !afectaDS && sucursalNueva!=null)
					creaLogTransaccion(sucursalNueva,tmpMs,tmpDs);
			}else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'La sucursal seleccionada no pertenece a la misma ciudad de la sucursal anterior'));
			}	
		}else{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'La sucursal seleccionada no pertenece al cliente'));
		}
		
	}
	
	/***** método encargado de enviar la información del cambio de sucursal a davox y TRS *******/
    public void enviarInfoCambio(){
        system.debug('-------enviando info de Ms modificadas ----->');
        try{
            ws_wwwTelefonicaComNotificacionessalesfo notificaciones=new ws_wwwTelefonicaComNotificacionessalesfo();
	         ws_wwwTelefonicaComNotificacionessalesfo.respuesta rta=new ws_wwwTelefonicaComNotificacionessalesfo.respuesta();
	         ws_wwwTelefonicaComNotificacionessalesfo.NotificacionesSalesForceSOAP xy = new ws_wwwTelefonicaComNotificacionessalesfo.NotificacionesSalesForceSOAP();
	         
	         rta=xy.CambiarSucursal(''+sucursal.BI_Cliente__c,
	                lstCuentasDavox,
	                lstMsDrs,
	                ''+sucursalNueva.Id,
	                ''+sucursal.Id,
	                ''+sucursalNueva.BI_Sede__c, 
	                ''+userInfo.getUserId(),
	                sucursalNueva.BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c, 
	                'Actualizar');
	        system.debug('-------respuesta info de Ms modificadas ----->'+rta);
        }catch(Exception e){
            system.debug('\n\n------error enviando info DavoxTRS--->'+e);
            BI_Log__c log=null;
            log=new BI_Log__c(/*Name='Cambio Sucursal'*/BI_COL_Informacion_recibida__c='Información de cambio de sucursal no pudo ser enviada correctamente: '+e,BI_COL_Sede__c=sucursal.id);
            insert log; 
        }       
    }
		
	/**** Se agrega método de creación de log de transacciones para la sucursal antigua ****/
    public void creaLogTransaccion(BI_Punto_de_instalacion__c sucursalNueva,List<BI_COL_Modificacion_de_Servicio__c> tmpMs, List<BI_COL_Descripcion_de_servicio__c> tmpDs){
    	List<BI_Log__c> lstLogTran=new List<BI_Log__c>();
    	BI_Log__c log=null; 
		
		for(BI_COL_Modificacion_de_Servicio__c modServ:tmpMs){
			log=new BI_Log__c(/*Name='Cambio Sucursal',*/BI_Descripcion__c='Nueva Dirección: '+sucursalNueva.BI_Sede__c+'\nCodigo Dane: '+sucursalNueva.BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c+'\nID Salesforce: '+sucursalNueva.Id, 
										BI_COL_Informacion_recibida__c='La MS '+modServ.Name+' ha cambiado en enlace de la sucursal: '+this.sucursal.Name+' por la sucursal: '+sucursalNueva.Name,BI_COL_Sede__c=sucursal.id);
			lstLogTran.add(log);
		}
		
		for(BI_COL_Descripcion_de_servicio__c desServ:tmpDs){
			log=new BI_Log__c(/*Name='Cambio Sucursal',*/BI_Descripcion__c='Nueva Dirección: '+sucursalNueva.BI_Sede__c+'\nCodigo Dane: '+sucursalNueva.BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c+'\nID Salesforce: '+sucursalNueva.Id, 
										BI_COL_Informacion_recibida__c='La DS '+desServ.Name+' ha cambiado en enlace de la sucursal: '+this.sucursal.Name+' por la sucursal: '+sucursalNueva.Name,BI_COL_Sede__c=sucursal.id);
			lstLogTran.add(log);
		}
		
		insert lstLogTran;
    }

}