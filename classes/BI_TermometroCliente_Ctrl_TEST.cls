@isTest
private class BI_TermometroCliente_Ctrl_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Raul Aguera
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_TermometroCliente_Ctrl class
    
    History: 
    <Date>                  <Author>                <Change Description>
    09/04/2015              Raul Aguera             Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Raul Aguera
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_TermometroCliente_Ctrl
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    09/04/2015              Raul Aguera             Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void TestPageTermometroCliente() {
        
        List<RecordType> lst_rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_CHI_Termometro' AND SobjectType = 'BI_ISC2__c'];
                
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller( new BI_Plan_de_accion__c() );
        BI_TermometroCliente_Ctrl psController = new BI_TermometroCliente_Ctrl(controller);

        system.debug( '## termometro: ' + psController.termometro_recordTypeId );
        system.assertEquals( psController.termometro_recordTypeId, lst_rt.get(0).Id );
    }
}