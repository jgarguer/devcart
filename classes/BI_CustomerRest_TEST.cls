@isTest
private class BI_CustomerRest_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_CustomerRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    16/09/2014              Pablo Oliva             Initial Version
    27/05/2016              Guillermo Muñoz         BI_TestUtils.throw_exception set to false to avoid test errors
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static{
        BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CustomerRest.getCustomer()
    History:
    
    <Date>              <Author>                <Description>
    16/09/2014          Pablo Oliva             Initial version
    25/03/2015			Juan Santisi			Refactored Region__c: USe custom setting BI_Code_ISO__c (Name, ISO, CurrencyISO)
    27/05/2016          Guillermo Muñoz         Changed BI_TestUtils.throw_exception values caused by changes in the global value
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getCustomerTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
      
		Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');
         
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        
        List <Contact> lst_con = BI_DataLoadRest.loadContacts(200, lst_acc);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/customerinfo/v1/customers/'+lst_acc[0].BI_Id_del_cliente__c;  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        BI_TestUtils.throw_exception = true;

        //INTERNAL_SERVER_ERROR
        BI_CustomerRest.getCustomer();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        //BAD REQUEST
        BI_TestUtils.throw_exception = false;
        BI_CustomerRest.getCustomer();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //NOT FOUND
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        req.requestURI = '/customerinfo/v1/customers/idContact';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;
        
        BI_CustomerRest.getCustomer();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/customerinfo/v1/customers/'+lst_acc[0].BI_Id_del_cliente__c;
        RestContext.request = req;
        BI_CustomerRest.getCustomer();
        system.assertEquals(200,RestContext.response.statuscode);
        
        lst_acc[0].BI_Activo__c = Label.BI_No;
        update lst_acc[0];
        BI_CustomerRest.getCustomer();
        system.assertEquals(200,RestContext.response.statuscode);
        
        lst_acc[0].BI_Activo__c = 'Pendiente de aprobación';
        update lst_acc[0];
        BI_CustomerRest.getCustomer();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CustomerRest.createCustomer()
    History:
    
    <Date>              <Author>                <Description>
    16/09/2014          Pablo Oliva             Initial version
    25/03/2015			Juan Santisi			Refactored Region__c: Use Custom Setting BI_Code_ISO__c
    27/05/2016          Guillermo Muñoz         Changed BI_TestUtils.throw_exception values caused by changes in the global value
    08/09/2016          Guillermo Muñoz         Chenged assertion for the creation of the BI_Log__c trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createCustomerTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	
    	Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');
        
    	RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/customerinfo/v1/customers';  
        req.httpMethod = 'POST';
        
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        BI_TestUtils.throw_exception = true;

        //BAD REQUEST
        BI_CustomerRest.createCustomer();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //INTERNAL_SERVER_ERROR
        Blob body = Blob.valueof('types": "Administrativo", "status": "inactive", "name": "Test value", "legalId": { "docType": "Otros", "docNumber": "1234567", "country": "aa" }, "customers": ["bb"], "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "address": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null }], "additionalData": null, "accounts": null }');
        req.requestBody = body;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_CustomerRest.createCustomer();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals(2, lst_log.size());//GMN 08/09/2016 - Se pone 2 porque al tener el throw exception a true también salta el log del trigger de BI_log__C
        
        //BAD_REQUEST missing fields
        body = Blob.valueof('{"accountDetails": { "subsegment": null, "statusReason": null, "segment": null, "satisfaction": null, "riskRank": null, "paymentMethods": null, "name": "Prueba Integra 3", "legalId": { "docType": "Test", "docNumber": "20000000007", "country": "0"}, "fraud": false, "description": null, "customerId": null, "contacts": null, "contactingModes": [], "address": [], "additionalData": null, "accountStatus": null, "accountInfoType": "Potencial" }}');
        req.requestBody = body;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_CustomerRest.createCustomer();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //OK
        body = Blob.valueof('{"accountDetails": { "subsegment": null, "statusReason": null, "segment": null, "satisfaction": null, "riskRank": null, "paymentMethods": null, "name": "Prueba Integra 3", "legalId": { "docType": "Test", "docNumber": "20000000007", "country": "0"}, "fraud": false, "description": null, "customerId": null, "contacts": null, "contactingModes": [], "address": [], "additionalData": [ { "Key": "customerId", "value": "543210" } ], "accountStatus": null, "accountInfoType": "Potencial" }}');
        req.requestBody = body;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_CustomerRest.createCustomer();
        system.assertEquals(201,RestContext.response.statuscode);
        
        Test.stopTest();
    	
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CustomerRest.updateCustomer()
    History:
    
    <Date>              <Author>                <Description>
    16/09/2014          Pablo Oliva             Initial version
    15/03/2015			Juan Santisi			Refactored Region__c: 
    27/05/2016          Guillermo Muñoz         Changed BI_TestUtils.throw_exception values caused by changes in the global value
    08/09/2016          Guillermo Muñoz         Chenged assertion for the creation of the BI_Log__c trigger
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void updateCustomerTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	
    	Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
    
    	RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/customerinfo/v1/customers/'+lst_acc[0].BI_Id_del_cliente__c;  
        req.httpMethod = 'PUT';
        
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

        BI_TestUtils.throw_exception = true;
        
        //BAD REQUEST
        BI_CustomerRest.updateCustomer();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //INTERNAL_SERVER_ERROR
        Blob body = Blob.valueof('types": "Administrativo", "status": "inactive", "name": "Test value", "legalId": { "docType": "Otros", "docNumber": "1234567", "country": "aa" }, "customers": ["bb"], "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "address": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null }], "additionalData": null, "accounts": null }');
        req.requestBody = body;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_CustomerRest.updateCustomer();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals(2, lst_log.size());//GMN 08/09/2016 - Se pone 2 porque al tener el throw exception a true también salta el log del trigger de BI_log__C
        
        //BAD_REQUEST NOT_FOUND
        body = Blob.valueof('{"accountDetails": { "subsegment": null, "statusReason": null, "segment": null, "satisfaction": null, "riskRank": null, "paymentMethods": null, "name": "Prueba Integra 3", "legalId": { "docType": "Test", "docNumber": "20000000007", "country": "0"}, "fraud": false, "description": null, "customerId": null, "contacts": null, "contactingModes": [], "address": [], "additionalData": null, "accountStatus": null, "accountInfoType": "Potencial" }}');
        req.requestBody = body;
        req.requestURI = '/customerinfo/v1/customers/test';
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_CustomerRest.updateCustomer();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        body = Blob.valueof('{"accountDetails": { "subsegment": null, "statusReason": null, "segment": null, "satisfaction": null, "riskRank": null, "paymentMethods": null, "name": "Prueba Integra 3", "legalId": { "docType": "Test", "docNumber": "20000000007", "country": "0"}, "fraud": false, "description": null, "customerId": null, "contacts": null, "contactingModes": [], "address": [], "additionalData": [ { "Key": "accountId", "value": "543210" } ], "accountStatus": null, "accountInfoType": "Potencial" }}');
        req.requestBody = body;
        req.requestURI = '/customerinfo/v1/customers/'+lst_acc[0].BI_Id_del_cliente__c;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_CustomerRest.updateCustomer();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
    	
    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}