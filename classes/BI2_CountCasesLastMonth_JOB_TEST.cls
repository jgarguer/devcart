@isTest public with sharing class BI2_CountCasesLastMonth_JOB_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Test class for BI2_CountCasesLastMonth_JOB

	 History:

	 <Date>				<Author>				<Description>
	 16/09/2015			Jose Miguel Fierro		Initial version
	 29/12/2016			Gawron, Julián E.		Add System.runAs to force Admin user
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest public static void test_Schedule() {
		BI_TestUtils.throw_exception = false;
        Id idProfile =BI_DataLoad.searchAdminProfile();
            
        List<User> lst_user = BI_DataLoad.loadUsers(1, idProfile, Label.BI_Administrador);
        
        system.runAs(lst_user[0]){
       
		List<RecordType> lst_rts = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI2_Caso_Padre')];

		Integer NUM_CONTACTS = 10;
		Integer NUM_CASES_PER_CONTACT = 2;
		List<Contact> lst_contacts = new List<Contact>();
		for(Integer iter = 0; iter < NUM_CONTACTS; iter++) {
			lst_contacts.add(new Contact(LastName='TestPeterson', BI2_N_Casos_Ultimo_Mes__c=0));
		}
		insert lst_contacts;


		List<Case> lst_cases = new List<Case>();
		for(Integer iter_con = 0; iter_con < lst_contacts.size(); iter_con++) {
			for(Integer iter_cas = 0; iter_cas < NUM_CASES_PER_CONTACT; iter_cas++) {
				lst_cases.add(new Case(Subject='Test Test', ContactId=lst_contacts[iter_con].Id, RecordTypeId = lst_rts[0].Id));
			}
		}
		insert lst_cases;


		Test.startTest();
		String strCron1 = BI2_CountCasesLastMonth_JOB.generateCronStr(Datetime.now().addMinutes(1));
		System.schedule('TEST->BI2_CountCasesLastMonth_JOB ' + strCron1, strCron1, new BI2_CountCasesLastMonth_JOB());
		Test.stopTest();

		lst_contacts = [SELECT Id, LastName, BI2_N_Casos_Ultimo_Mes__c FROM Contact WHERE LastName LIKE 'TestPeterson%'];
		System.assertEquals(NUM_CASES_PER_CONTACT, lst_contacts[0].BI2_N_Casos_Ultimo_Mes__c);

		BI2_CountCasesLastMonth_JOB.generateCronStr(Datetime.now().addSeconds(15));
		}
		system.assert([select Id from BI_Log__c limit 1].isEmpty());
	}
    
    @isTest public static void test_exceptions() {
        BI_TestUtils.throw_exception = true;
        new BI2_CountCasesLastMonth_JOB().execute(null);
        BI2_CountCasesLastMonth_JOB.generateCronStr(null);
        
        system.assertEquals(false, [select Id from BI_Log__c limit 1].isEmpty());
    }
}