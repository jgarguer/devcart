/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for class BI_COL_validationFraud_ctr.
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-08-21      Raul Mora (RM)				    Create class.
					20/09/2017      Angel F. Santaliestra           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private class BI_COL_validationFraud_tst 
{
	
	public static List<Account> lstAcc;
	public static List<User> lstUser;
	public static list <UserRole> lstRoles;
	public static User objUsuario;
	
	public static void createData()
	{
		List<Profile> lstProfile = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1 ];
		//lstUser = [ Select Id FROM User where country != 'Colombia' AND ProfileId =: lstProfile[0].Id And isActive = true  Limit 1 ];
		
		lstAcc = new List<Account>();

		//lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = new User();
        objUsuario.Alias = 'standt';
        objUsuario.Email ='pruebas@test.com';
        objUsuario.EmailEncodingKey = '';
        objUsuario.LastName ='Testing';
        objUsuario.LanguageLocaleKey ='en_US';
        objUsuario.LocaleSidKey ='en_US'; 
        objUsuario.ProfileId = lstProfile.get(0).Id;
        objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        objUsuario.UserName ='pruebas@test.com';
        objUsuario.EmailEncodingKey ='UTF-8';
        objUsuario.UserRoleId = lstRoles.get(0).Id;
        objUsuario.BI_Permisos__c ='Sucursales';
        objUsuario.Pais__c='Colombia';
        insert objUsuario;
        
        lstUser = new list<User>();
        lstUser.add(objUsuario);
		
		System.runAs( lstUser[0] )
		{ 
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass;
			
			Account objCuenta = new Account();
			objCuenta.Name = 'prueba';
			objCuenta.BI_Country__c = 'Colombia';
			objCuenta.TGS_Region__c = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c = 'NIT';
			objCuenta.CurrencyIsoCode = 'GTQ';
			objCuenta.BI_No_Identificador_fiscal__c = 'NIT 123456789';
			objCuenta.BI_Segment__c                         = 'test';
			objCuenta.BI_Subsegment_Regional__c             = 'test';
			objCuenta.BI_Territory__c                       = 'test';
			lstAcc.add( objCuenta );
			
			Account objCuenta2 = new Account();
			objCuenta2.Name = 'prueba2';
			objCuenta2.BI_Country__c = 'Colombia';
			objCuenta2.TGS_Region__c = 'América';
			objCuenta2.BI_Tipo_de_identificador_fiscal__c = 'NIT';
			objCuenta2.CurrencyIsoCode = 'GTQ';
			objCuenta2.BI_No_Identificador_fiscal__c = 'NIT 987654321';
			objCuenta2.BI_Segment__c                         = 'test';
			objCuenta2.BI_Subsegment_Regional__c             = 'test';
			objCuenta2.BI_Territory__c                       = 'test';
			lstAcc.add( objCuenta2 );
			
			insert lstAcc;
		}
	}

    static testMethod void myUnitTest() 
    {
    	createData();
    	
    	List<ID> lstIdAc = new List<ID>();
    	
    	for( Account objAc : lstAcc )
    	{
    		lstIdAc.add( objAc.Id );
    	}
    	
    	Test.startTest();
        	BI_COL_validationFraud_cls.Consultar_Fraude( lstIdAc );
        Test.stopTest();
    }
}