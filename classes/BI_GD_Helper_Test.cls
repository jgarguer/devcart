/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Borja María
Company:        Everis España
Description:    Test para la Clase BI_GD_CalloutManager

History:

<Date>                      <Author>                        <Change Description>
10/08/2017                 Borja María         		    	Versión Inicial.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
//(SeeAllData=true)
public class BI_GD_Helper_Test {
    
    @isTest static void BI_GD_Helper_Test1() {
        
        Contract contrato = new Contract(BI_FVI_Autoriza__c = true,
                                         BI_FVI_Cobertura__c = false,
                                         BI_FVI_Autoriza_representante__c = false,
                                         BI_FVI_desconoce_CPF__c = false,
                                         BI_FVI_Control_Parental__c = false,
                                         BI_FVI_IdFileTxn__c = null,
                                         BI_COL_Formato_Tipo__c = 'Tipo Cliente',
                                         BI_COL_Tipo_contrato__c = 'Condiciones Uniformes',
                                         BI_COL_Presupuesto_contrato__c  =12000,
                                         BI_FVI_No_de_linea__c = '10',
                                         BI_Indefinido__c = 'No',
                                         StartDate = system.Today()); 
        
        Account account = new Account(Name = 'nameAccount',
                                      BI_Segment__c = 'Empresas',
                                      BI_Subsegment_Regional__c = 'Corporate',
                                      BI_Territory__c = 'CABA',
                                      BI_Country__c = 'ARG',
                                      BI_Sector__c = 'Comercio',
                                      Sector__c = 'Private',
                                      BI_Subsector__c= 'Banca',
                                      BI_Tipo_de_identificador_fiscal__c = 'Cliente exterior');
        insert(account);
        contrato.AccountId= account.Id; 
        insert contrato;
        
        DateTime date1= DateTime.now();
        Date date3= Date.today();
        Opportunity oppTest= new Opportunity (BI_Fecha_de_entrega_de_la_oferta__c = date1, 
                                              BI_Duracion_del_contrato_Meses__c= 10,
                                              OwnerId = userInfo.getUserId(),
                                              Name ='Opp1', 
                                              StageName='F2 - Contract Negotiation' , 
                                              BI_No_Identificador_fiscal__c='123', 
                                              // AccountId=contrato.AccountId,
                                              BI_Opportunity_Type__c = 'Móvil',
                                              BI_Plazo_estimado_de_provision_dias__c = 22, 
                                              BI_Licitacion__c = 'No', 
                                              BI_Probabilidad_de_exito__c = '95',
                                              CurrencyIsoCode = 'MXN',
                                              BI_Requiere_contrato__c = true,
                                              BI_ARG_Tipo_de_Contratacion__c = 'Concurso Privado',
                                              BI_ARG_Nro_de_Licitacion__c = 'No',
                                              BI_Fecha_de_vigencia__c =  date3,
                                              Amount = 16,
                                              BI_ARG_Fecha_Sol_Elaboracion_Rta_Pliego__c = Date.today(),
                                              CloseDate = Date.today(),
                                              BI_ARG_Fecha_presentacion_de_sobres__c = Date.today(),
                                              BI_Country__c ='Argentina'); 
        
        
        insert oppTest;
        
        test.startTest();
        BI_GD_Helper.getContractFromId(contrato.Id);
        
        BI_GD_Helper.getAccountFromId(account.Id);
        
        BI_GD_Helper.getOpportunityFromId(oppTest.Id);
        
        BI_GD_Helper.isValidStageForLicitation(oppTest);
        
        BI_GD_Helper.isValidStageForContract(oppTest);
        
        String username = 'jesus.martinez.sanchez@everis.com.fsarg';
        String password = '19102016Jesus';
        String method = 'PUT';            
        //BI_GD_Helper.getTokenOAuth(username, password, method);
        
        contrato.BI_Oportunidad__c = oppTest.Id;
        BI_GD_Helper.getStageNameContract(contrato);
        BI_GD_Helper.getStageNameLicitacion(contrato);
        
        oppTest.StageName = 'F1 - Ganada' ;
        update oppTest;
        contrato.BI_Oportunidad__c = oppTest.Id;
        BI_GD_Helper.getStageNameContract(contrato);
        BI_GD_Helper.getStageNameLicitacion(contrato);
        
        oppTest.StageName = 'F6';
        update oppTest;
        contrato.BI_Oportunidad__c = oppTest.Id;
        BI_GD_Helper.getStageNameContract(contrato);
        BI_GD_Helper.getStageNameLicitacion(contrato);
        
        // BI_GD_Helper.generateLicitacion();
        // BI_GD_Helper.generateContrato();
        Test.stopTest(); 
    }
    
    @isTest static void BI_GD_Helper_Test2() {
        test.startTest();
        try{
            BI_GD_Helper.generateContrato();
        }catch(Exception e){
        }
        Test.stopTest(); 
    }
    @isTest static void BI_GD_Helper_Test3() {
        test.startTest();
        try{
            BI_GD_Helper.generateLicitacion();
        }catch(Exception e){
        }
        Test.stopTest(); 
    }
     @isTest static void BI_GD_Helper_Test4() {
        test.startTest();
        try{
             Contract contrato = new Contract(BI_FVI_Autoriza__c = true,
                                         BI_FVI_Cobertura__c = false,
                                         BI_FVI_Autoriza_representante__c = false,
                                         BI_FVI_desconoce_CPF__c = false,
                                         BI_FVI_Control_Parental__c = false,
                                         BI_FVI_IdFileTxn__c = null,
                                         BI_COL_Formato_Tipo__c = 'Tipo Cliente',
                                         BI_COL_Tipo_contrato__c = 'Condiciones Uniformes',
                                         BI_COL_Presupuesto_contrato__c  =12000,
                                         BI_FVI_No_de_linea__c = '10',
                                         BI_Indefinido__c = 'No',
                                         StartDate = system.Today()); 
            BI_GD_Helper.isMulticurrencyContract(contrato);
        }catch(Exception e){
        }
        Test.stopTest(); 
    }
    
}