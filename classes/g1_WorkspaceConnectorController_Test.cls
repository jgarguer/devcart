@isTest
public class g1_WorkspaceConnectorController_Test {


 static testMethod void findActivity_Test()
 {        
        //TGSOL
        String connID = '010A0291B9B3A01AA';
        Task t = new Task (
          CallObject = connID,
          Type = 'Call',
          Status = 'Completed',
          Subject = 'Testttt',
          Priority = 'Normal',
          ActivityDate = System.today()
        );
        insert t;
        String result;
        result = g1_WorkspaceConnectorController.findActivity(connID);
        System.assert(result != null);
 }
 
 
 static testMethod void findObjectVoice_Test()
 {        
        //TGSOL
        //create account
    	Account acc = new Account(Name = 'testAccount',
                               Phone = '5555',
                               BI_Segment__c = 'test', // 28/09/2017
                               BI_Subsegment_Regional__c = 'test', // 28/09/2017
                               BI_Territory__c = 'test' // 28/09/2017
                               ); 
        insert acc;
        Account newACC = [SELECT Id, Name, Phone FROM Account WHERE Phone = '5555'];
        Map<String,String> fieldsMap1 = new Map<String,String>();
        //create map
    	fieldsMap1.put('SFDC1field','Description');
    	fieldsMap1.put('SFDC1value','');
        fieldsMap1.put('SFDC2field','Description');
    	fieldsMap1.put('SFDC2value','');
        fieldsMap1.put('SFDC3field','Description');
    	fieldsMap1.put('SFDC3value',newACC.Id);

		String result;
        result = g1_WorkspaceConnectorController.findObjectVoice(fieldsMap1);
        System.assert(result != null);
        
        Map<String,String> fieldsMap = new Map<String,String>();        
        //create map
    	fieldsMap.put('SFDC1field','Description');
    	fieldsMap.put('SFDC1value','Test1');
        fieldsMap.put('SFDC2field','Description');
    	fieldsMap.put('SFDC2value','Test2');
        fieldsMap.put('SFDC3field','Description');
    	fieldsMap.put('SFDC3value',newACC.Id);
    	fieldsMap.put('SFDC4field','Description');
    	fieldsMap.put('SFDC4value','Test4');
    	fieldsMap.put('SFDC5field','Id');
    	fieldsMap.put('SFDC5value',newACC.Id); 

        result = g1_WorkspaceConnectorController.findObjectVoice(fieldsMap);
        System.assert(result != null);
 }
 
  static testMethod void updateActivity_Test()
 {        
        //TGSOL
        Task t = new Task (
          Type = 'Call',
          Status = 'Completed',
          Subject = 'Testttt',
          Priority = 'Normal',
          ActivityDate = System.today()
        );
        insert t;
        Task myTask = [SELECT Id FROM Task WHERE Subject = 'Testttt'];
        
        Map<String,String> fieldsMap = new Map<String,String>();
        //create map
    	fieldsMap.put('Call Duration','10');
    	fieldsMap.put('Comments','Test');
        fieldsMap.put('Disposition','Resolved');
		fieldsMap.put('Media Type','');		
        fieldsMap.put('IXN Type','Voice-Outbound');				
    	fieldsMap.put('DATE','0001-01-01 00:00:00');
    	fieldsMap.put('sfdc Object Id','');
        
        String result;
        result = g1_WorkspaceConnectorController.updateActivity(myTask.Id,fieldsMap);
        System.assert(result == 'Success');
 }
   
 static testMethod void findCaseFromNumber_Test()
 {
    String test_subject = '123test321test';
    //error leg
    Case result = g1_WorkspaceConnectorController.findCaseFromNumber('1');
    System.assert(result == null);
     
 	//Create Case test data
 	Case test_case = new Case(Subject = test_subject);
    insert test_case;
    Case myCase = [SELECT CaseNumber FROM Case WHERE Subject = :test_subject];  
 	result = g1_WorkspaceConnectorController.findCaseFromNumber(myCase.CaseNumber);
        System.assert(result!=null);
 }
 
  static testMethod void findContactFromCase_Test()
 {
    String test_subject = '123test321test';
    //error leg
    String result = g1_WorkspaceConnectorController.findContactFromCase('1');
    System.assert(result == null);
    
 	//Create Contact test data
  	Contact con1 = new Contact(FirstName = 'John', LastName = 'Doer', Phone = '4444', MobilePhone = '3333',Email = 'john.doer@somewhere.com');
  	insert con1;
  	con1 = [SELECT Id FROM Contact WHERE Email = 'john.doer@somewhere.com'];
  		 
 	//Create Case test data
 	Case test_case = new Case(Subject = test_subject, ContactId = con1.Id);
    insert test_case;
    Case case1 = [SELECT Id FROM Case WHERE ContactId = :con1.Id];
 	result = g1_WorkspaceConnectorController.findContactFromCase(case1.Id);
        System.assert(result!=null);
 }
    

static testMethod void findObjectFromANI_Test() {
    	Object result = g1_WorkspaceConnectorController.findObjectFromANI('5555');
		System.assert(result == 'not found');
		
	/* From Salesforce documentation: Adding SOSL Queries to Unit Tests
	To ensure that test methods always behave in a predictable way, any Salesforce Object Search Language (SOSL) query that is 
	added to an Apex test method returns an empty set of search results when the test method executes.
	Need to use setFixedSearchResults to set the results.		
	*/    
		//Create Account test data
        Account acc = new Account(Name = 'testAccount',
                                 Phone = '5555',
                                 BI_Segment__c = 'test', // 28/09/2017
                                 BI_Subsegment_Regional__c = 'test', // 28/09/2017
                                 BI_Territory__c = 'test' // 28/09/2017
                                 ); 
        insert acc;
        Account newACC = [SELECT Id, Name, Phone FROM Account WHERE Phone = '5555'];
        Id [] fixedSearchResultsACC= new Id[1];
  		fixedSearchResultsACC[0] = newACC.Id;
  		Test.setFixedSearchResults(fixedSearchResultsACC);
    	result = g1_WorkspaceConnectorController.findObjectFromANI('5555');
    	System.assert(result!=null);
    	
		//Create Contact test data
  		Contact con1 = new Contact(FirstName = 'John', LastName = 'Doer', Phone = '4444', MobilePhone = '3333',Email = 'john.doer@somewhere.com');
  		insert con1;
  		Contact myContact = [SELECT Id FROM Contact WHERE Email = 'john.doer@somewhere.com'];
  		Id [] fixedSearchResults= new Id[1];
  		fixedSearchResults[0] = myContact.Id;
  		Test.setFixedSearchResults(fixedSearchResults);
  		result = g1_WorkspaceConnectorController.findObjectFromANI('4444');       
        System.assert(result!=null);  
        
        //Create Lead test data
  		Lead lead = new Lead(FirstName = 'John', LastName = 'Doer', Phone = '2424', Company = 'ABC');
  		insert lead;
  		Lead myLead = [SELECT Id FROM Lead WHERE Phone = '2424'];
  		Id [] fixedSearchResultsLead= new Id[1];
  		fixedSearchResultsLead[0] = myLead.Id;
  		Test.setFixedSearchResults(fixedSearchResultsLead);
  		result = g1_WorkspaceConnectorController.findObjectFromANI('2424');       
        System.assert(result!=null); 
}

    
static testMethod void findContactFromANI_Test() {
        Object id;

    	//Account acc = new Account(Name = 'testAccount',Phone = '5555'); 
        //insert acc;
        //Account newACC = [SELECT Id, Name, Phone FROM Account WHERE Phone = '5555'];
        
        //Create Contact test data
        Contact con1 = new Contact(FirstName = 'John', LastName = 'Doer', Phone = '4444', MobilePhone = '3333');
    	//con1.AccountId = newACC.Id;
        Contact con2 = new Contact(FirstName = 'Jane', LastName = 'Doer', Phone = '4444');
    	//con2.AccountId = newACC.Id;
        
        //error leg
        id = g1_WorkspaceConnectorController.findContactFromANI('4444');
        System.assert(id == 'not found');
        
        insert con1;
    	String test_phone = '4444';
		Id [] fixedSearchResults = new Id[1];
       	Contact myCon = [SELECT Id FROM Contact WHERE Phone = :test_phone];
       	fixedSearchResults[0] = myCon.Id;
       	Test.setFixedSearchResults(fixedSearchResults);
    	System.debug('*** Id = '+myCon.Id);
           
        id = g1_WorkspaceConnectorController.findContactFromANI('4444');
        System.assert(id != null && id!='not found' && id != 'multiple found');
        System.debug('*** single match');
        
        //insert con2;
        //id = WorkspaceConnectorController.findContactFromANI('4444');
        //System.assert(id == 'multiple found');
    	//System.debug('*** multiple match');
}  


static testMethod void findContactFromEmailAddress_Test() {
        Object id;
                
        //Create Contact test data
        Contact con1 = new Contact(FirstName = 'John', LastName = 'Doer', Phone = '4444', MobilePhone = '3333',Email = 'john.doer@somewhere.com');
        
        //error leg
        id = g1_WorkspaceConnectorController.findContactFromEmailAddress('john.doer@somewhere.com');
        System.assert(id == null);
        
        insert con1;
        id = g1_WorkspaceConnectorController.findContactFromEmailAddress('john.doer@somewhere.com');
        System.assert(id != null);        
}


static testMethod void findContactFromChatAddress_Test() {
        Object id;
                
        //Create Contact test data
        Contact con1 = new Contact(FirstName = 'John', LastName = 'Doer', Phone = '4444', MobilePhone = '3333',Email = 'john.doer@somewhere.com');
        
        //error leg
        id = g1_WorkspaceConnectorController.findContactFromChatAddress('John Doer');
        System.assert(id == null);
        
        insert con1;
        id = g1_WorkspaceConnectorController.findContactFromChatAddress('John Doer');
        System.assert(id != null);        
}

    static testMethod void findContactFromWorkItemAddress_Test() {
        Object id;
                
        //Create Contact test data
        Contact con1 = new Contact(FirstName = 'John', LastName = 'Doer', Phone = '4444', MobilePhone = '3333',Email = 'john.doer@somewhere.com');
        
        //error leg
        id = g1_WorkspaceConnectorController.findContactFromWorkItemAddress('John Doer');
        System.assert(id == null);
        
        insert con1;
        id = g1_WorkspaceConnectorController.findContactFromWorkItemAddress('John Doer');
        System.assert(id != null);        
}
    
    static testMethod void findContactFromOpenMediaAddress_Test() {
        Object id;
                
        //Create Contact test data
        Contact con1 = new Contact(FirstName = 'John', LastName = 'Doer', Phone = '4444', MobilePhone = '3333',Email = 'john.doer@somewhere.com');
        
        //error leg
        id = g1_WorkspaceConnectorController.findContactFromOpenMediaAddress('John Doer');
        System.assert(id == null);
        
        insert con1;
        id = g1_WorkspaceConnectorController.findContactFromOpenMediaAddress('John Doer');
        System.assert(id != null);        
}    

static testMethod void createCase_Test() {
        Map<String,String> fieldsMap = new Map<String,String>();
        //create map  
        fieldsMap.put('IXN Type','Call');
    	fieldsMap.put('Media Type','');
    	fieldsMap.put('DATE','June1');
    	String result = g1_WorkspaceConnectorController.createCase(fieldsMap);
        System.assert(result != 'case not created');
}
    
static testMethod void createActivity_Test() {
        String ID = '';
        String connID = '010A0291B9B3A01A';
        Map<String,String> fieldsMap = new Map<String,String>();
        //create map
        fieldsMap.put('Genesys Call Type','Inbound');
        fieldsMap.put('LOOKUP','4444');
        fieldsMap.put('Call Duration','35');
        fieldsMap.put('DATE','June1');
        fieldsMap.put('Comments','');
        fieldsMap.put('ANI','4444');
    	fieldsMap.put('IXN Type','Phone');
        fieldsMap.put('Media Type','');
        fieldsMap.put('sfdc Object Id',ID);
        fieldsMap.put('CallObject',connID);

        Case c = new Case(Status = 'New', Origin = 'Phone', Subject = '123321TEST');
        insert c;

        fieldsMap.put('sfdcCaseId',c.Id);

    	//Activity created as orhpan
        String noIDResult = g1_WorkspaceConnectorController.createActivity(fieldsMap);
        System.assert(noIDResult != null);
    
        //Create Account test data
        Account acc = new Account(Name = 'testAccount',
                                 Phone = '7654321',
                                 BI_Segment__c = 'test', // 28/09/2017
                                 BI_Subsegment_Regional__c = 'test', // 28/09/2017
                                 BI_Territory__c = 'test' // 28/09/2017
                                 ); 
        insert acc;
    	Account newACC = [SELECT Id, Name, Phone FROM Account WHERE Phone = '7654321'];
        //Create Contact test data
        Contact con = new Contact(FirstName = 'John', LastName = 'Doer', Phone = '4444', MobilePhone = '3333',Email = 'john.doer@somewhere.com');
        con.AccountId = newACC.Id;  //account id
        insert con;
    	
    	//Activity created under Contact using search
    	String contactResult = g1_WorkspaceConnectorController.createActivity(fieldsMap);
        System.assert(contactResult != null);

    	fieldsMap.put('IXN Type','Email');
    	fieldsMap.put('LOOKUP','john.doer@somewhere.com');
        contactResult = g1_WorkspaceConnectorController.createActivity(fieldsMap);
        System.assert(contactResult != null);

    	fieldsMap.put('IXN Type','Chat');
    	fieldsMap.put('LOOKUP','John Doer');
        contactResult = g1_WorkspaceConnectorController.createActivity(fieldsMap);
        System.assert(contactResult != null); 
        
    	fieldsMap.put('IXN Type','OpenMedia');
    	fieldsMap.put('LOOKUP','John Doer');
        contactResult = g1_WorkspaceConnectorController.createActivity(fieldsMap);
        System.assert(contactResult != null);        
    
    	//Activity created under Contact using ID
        Contact newCon = [SELECT Id FROM Contact WHERE Phone = '4444'];
        ID = newCon.Id;
    	System.Debug('sfdc Object Id = '+ID);
        fieldsMap.put('sfdc Object Id',ID);
    	fieldsMap.put('SFDC1field','Description');
    	fieldsMap.put('SFDC1value','Test1');
        fieldsMap.put('SFDC2field','Description');
    	fieldsMap.put('SFDC2value','Test2');
        fieldsMap.put('SFDC3field','Description');
    	fieldsMap.put('SFDC3value','Test3');
    	fieldsMap.put('SFDC4field','Description');
    	fieldsMap.put('SFDC4value','Test4');
    	fieldsMap.put('SFDC5field','Description');
    	fieldsMap.put('SFDC5value','Test5');    
        contactResult = g1_WorkspaceConnectorController.createActivity(fieldsMap);
        System.assert(contactResult != null);
} 

static testMethod void addAttachment_Test(){
    Task t = new Task (
          Type = 'Call',
          Status = 'Completed',
          Subject = 'Testttt',
          Priority = 'Normal',
          ActivityDate = System.today()
        );
        insert t;
        Task myTask = [SELECT Id FROM Task WHERE Subject = 'Testttt'];
        String blobString= 'This is a string';
     	Blob b = Blob.valueOf(blobString);
        String result = g1_WorkspaceConnectorController.addAttachment(myTask.Id,'Test Description','TestName1','txt',b,null);
    System.assert(result != 'error');
}

static testMethod void testConnection_Test(){
	Object result = g1_WorkspaceConnectorController.testConnection();
}

static testMethod void findObject_Test() {
    	Object result = g1_WorkspaceConnectorController.findObject('Phone','5');
		System.assert(result == null);
		
	/* From Salesforce documentation: Adding SOSL Queries to Unit Tests
	To ensure that test methods always behave in a predictable way, any Salesforce Object Search Language (SOSL) query that is 
	added to an Apex test method returns an empty set of search results when the test method executes.
	Need to use setFixedSearchResults to set the results.		
	*/
		//Create Account test data
        Account acc = new Account(Name = 'testAccount',
                                 Phone = '5555',
                                 BI_Segment__c = 'test', // 28/09/2017
                                 BI_Subsegment_Regional__c = 'test', // 28/09/2017
                                 BI_Territory__c = 'test' // 28/09/2017
                                 ); 
        insert acc;
        Account newACC = [SELECT Id, Name, Phone FROM Account WHERE Phone = '5555'];
        Id [] fixedSearchResultsACC= new Id[1];
  		fixedSearchResultsACC[0] = newACC.Id;
  		Test.setFixedSearchResults(fixedSearchResultsACC);
    	result = g1_WorkspaceConnectorController.findObject('Phone','5555');
    	System.assert(result!=null);
    	
    	//Create Contact test data
  		Contact con1 = new Contact(FirstName = 'John', LastName = 'Doer', Phone = '4444', MobilePhone = '3333',Email = 'john.doer@somewhere.com');
  		insert con1;
  		Contact myContact = [SELECT Id FROM Contact WHERE Email = 'john.doer@somewhere.com'];
  		Id [] fixedSearchResults= new Id[1];
  		fixedSearchResults[0] = myContact.Id;
  		Test.setFixedSearchResults(fixedSearchResults);
  		result = g1_WorkspaceConnectorController.findObject('Phone','4444');       
        System.assert(result!=null);
        
        //Create Lead test data
  		Lead lead = new Lead(FirstName = 'John', LastName = 'Doer', Phone = '2424', Company = 'ABC');
  		insert lead;
  		Lead myLead = [SELECT Id FROM Lead WHERE Phone = '2424'];
  		Id [] fixedSearchResultsLead= new Id[1];
  		fixedSearchResultsLead[0] = myLead.Id;
  		Test.setFixedSearchResults(fixedSearchResultsLead);
  		result = g1_WorkspaceConnectorController.findObject('Phone','2424');       
        System.assert(result!=null);
        
        //Create Case test data
  		Case c = new Case(Status = 'New', Origin = 'Phone', Subject = '123321TEST');
  		insert c;
  		Case myCase = [SELECT Id,CaseNumber FROM Case WHERE Subject = '123321TEST'];
  		Id [] fixedSearchResultsCase= new Id[1];
  		fixedSearchResultsCase[0] = myCase.Id;
  		Test.setFixedSearchResults(fixedSearchResultsCase);
  		result = g1_WorkspaceConnectorController.findCaseObject('CaseNumber',myCase.CaseNumber);       
        System.assert(result!=null);
  }

  //PCP - Added test method for findObjectID method
  static testMethod void findObjectID_Test(){
    Account acc = new Account(Name = 'testAccount',
                             Phone = '5555',
                             BI_Segment__c = 'test', // 28/09/2017
                             BI_Subsegment_Regional__c = 'test', // 28/09/2017
                             BI_Territory__c = 'test' // 28/09/2017
                             ); 
    insert acc;
    Account myAcc = [SELECT Id, Name, Phone FROM Account WHERE Name = 'testAccount'];

    Contact cont = new Contact(FirstName = 'John', LastName = 'Doer', Phone = '4444', MobilePhone = '3333',Email = 'john.doer@somewhere.com');
    insert cont;
    Contact myContact = [SELECT Id FROM Contact WHERE Email = 'john.doer@somewhere.com'];

    Case c = new Case(ContactId = myContact.Id, AccountId = myAcc.Id, Status = 'New', Origin = 'Phone', Subject = '123321TEST');
    insert c;
    Case myCase = [SELECT Id,CaseNumber FROM Case WHERE Subject = '123321TEST'];
    
    
    Map<String, String> toQueryCase = new Map<String, String>();
    toQueryCase.put('Case',myCase.CaseNumber);
    g1_WorkspaceConnectorController.findObjectID(toQueryCase);


    Map<String, String> toQueryContact = new Map<String, String>();
    toQueryContact.put('Contact',myContact.Id);
    g1_WorkspaceConnectorController.findObjectID(toQueryContact);

    Map<String, String> toQueryAccount = new Map<String, String>();
    toQueryAccount.put('Account',myAcc.Id);
    g1_WorkspaceConnectorController.findObjectID(toQueryAccount);
  } 

}