@isTest
private class BI_ClonePlanDeAccionController_TEST {
    
    @isTest static void BI_ClonePlanDeAccionController_Test() {
        BI_Dataload.set_ByPass(false,false);
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'CHL'});
        system.assert(!lst_acc.isEmpty());

        List<BI_Plan_de_accion__c> lst_coord = BI_Dataload.loadCustomPlanDeAccion(1, lst_acc[0]);
        system.assert(!lst_coord.isEmpty());

        List<BI_ISC2__c> lst_ISC = BI_Dataload.loadISC(lst_acc);
        system.assert(!lst_ISC.isEmpty());

        List<Id> whatId = new List<Id>();
        for(BI_Plan_de_accion__c coo :lst_coord){
            whatId.add(coo.Id);
            //coo.BI_Estado__c = Label.BI_Finalizado;
            coo.BI_Termometro_ISC__c =lst_ISC[0].Id;
        }

        List<Recordtype> lst_rt_task = [SELECT ID, sObjectType  FROM RecordType WHERE Developername  = 'BI_Acuerdo_Plan_de_accion'];
        List<Task> lst_task = BI_Dataload.loadTaskWithRT(2,lst_rt_task, whatId, Label.BI_TaskStatus_Waiting);
        system.assert(!lst_task.isEmpty());

        List<Recordtype> lst_rt_event = [SELECT ID, sObjectType  FROM RecordType WHERE Developername  = 'BI_Asistentes_Plan_de_accion'];
        List<Event> lst_event = BI_Dataload.loadEventWithRT(1,lst_rt_event, whatId);
        system.assert(!lst_event.isEmpty());



        Test.startTest();

        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(lst_coord[0]);
        BI_ClonePlanDeAccionController constructor = new BI_ClonePlanDeAccionController(stdCtrl);

        system.assert(!constructor.lst_task.isEmpty());
        for(BI_ClonePlanDeAccionController.wrapper wrp :constructor.lst_task){
            wrp.check = true; 
        }

        system.assert(!constructor.lst_event.isEmpty());
        for(BI_ClonePlanDeAccionController.wrapper wrp :constructor.lst_event){
            wrp.check = true; 
        }

        system.assert(!constructor.lst_isc.isEmpty());
        for(BI_ClonePlanDeAccionController.wrapper wrp :constructor.lst_isc){
            wrp.check = true; 
        }
        
        constructor.CloneCustom();

        Test.stopTest();

    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}