/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_OpportunitySisonSOAP class
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    14/02/2017              Pedro Párraga            Initial Creation
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_OpportunitySisonSOAP_TEST {

  private static List <Opportunity> opp;
  private static User usr;
  private static BI_OpportunitySisonSOAP WSSison;
  private static BI_OpportunitySisonSOAP.OpportunityInfoType oinfotype;
  private static BI_OpportunitySisonSOAP.ConfigurationInfoType confinfotype;
  private static BI_OpportunitySisonSOAP.ProductInfoType prodinfotype;
  private static Opportunity opp1;
  private static Account acc16;
  private static NE__Product__c pord;
  private static NE__Catalog_Item__c catIt;
  private static NE__Catalog__c cat;

      public static void dataload(){
        BI_TestUtils.throw_exception = false;

        List<Profile> prof = [select Id from Profile where Name = 'System Administrator' OR Name = 'Administrador del sistema' limit 1];

        usr = new User (alias = 'stttt',
                            email='u@testorg.com',
                            emailencodingkey='UTF-8',
                            lastname='Testing',
                            languagelocalekey='en_US',
                            localesidkey='en_US',
                            ProfileId = prof[0].Id,
                            BI_Permisos__c='Integración CHI',
                            timezonesidkey=Label.BI_TimeZoneLA,
                            username= 'ur23@testorg.com',
                            BI_CodigoUsuario__c = '1000',
                            IsActive = true);
        insert usr;

        usr.BI_Permisos__c='Integración CHI';

        update usr;

        BI_bypass__c cbypass = new BI_bypass__c(SetupOwnerId = usr.Id, 
                                Name = 'SisonCS2',
                                BI_migration__c = true);

         insert cbypass; 

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        List <String> lst_pais = new List <String>();
        lst_pais.add('Chile');    

         acc16 = new Account(Name = 'Chile Test 1',
                                     BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                     BI_Activo__c = Label.BI_Si,
                                     BI_Tipo_de_Identificador_Fiscal__c = 'RUT',
                                     BI_No_Identificador_fiscal__c = '092704505',
                                     BI_Id_del_cliente__c = 'IDclienteCHL_1',
                                     BI_Country__c = Label.BI_Chile,
                                     BI_Segment__c = 'test',
                                     BI_Subsegment_Regional__c = 'test',
                                     BI_Territory__c = 'test');

        //List <Account> lst_acc = new List <Account>();
        //lst_acc.add(acc16);
        insert acc16;

        //System.assert(!lst_acc.isEmpty());
          
        opp1 = new Opportunity(Name = 'Test_opp_uno',
                                           CloseDate = Date.today(),
                                           StageName = Label.BI_F1Ganada,
                                           AccountId = acc16.Id,
                                           BI_Ciclo_ventas__c = Label.BI_Completo,
                                           BI_Country__c = 'Chile',
                                           BI_Opportunity_Type__c = 'Fijo',
                                           BI_CHI_Tasa_de_interes__c =10,
                                           BI_Duracion_del_contrato_Meses__c = 10,
                                           BI_Recurrente_bruto_mensual__c = 100,
                                           BI_Ingreso_por_unica_vez__c = 500,
                                           BI_Fecha_de_cierre_real__c = System.today(),
                                           BI_Recurrente_bruto_mensual_anterior__c = 500
                                           );     
        
          insert opp1;    

          opp1.Amount = 1000;
          opp1.StageName = Label.BI_F1CanceladaSusp;
          opp1.BI_Fecha_de_cierre_real__c = null;
          opp1.BI_Recurrente_bruto_mensual_anterior__c= null;
          update opp1;

          System.debug('AAA: '+opp1.Amount);

        opp = [SELECT Id, BI_Numero_id_oportunidad__c, Account.BI_No_Identificador_fiscal__c, BI_CHI_Bloqueo_SISON__c, BI_Casilla_desarrollo__c FROM Opportunity WHERE Id =: opp1.Id];

        //System.debug('#Debug opp[0].BI_CHI_Bloqueo_SISON__c '+ opp[0].BI_CHI_Bloqueo_SISON__c);

        pord = new NE__Product__c(Offer_SubFamily__c = 'a-test',
                                                Offer_Family__c = 'b-test',
                                               BI_Id_Producto_Gala__c = '123456');
        
        insert pord;        
        
        //System.debug('BI_Buttons_TEST->updateProjectOppTest-> pord: '+ pord);
        


         //System.debug('#Debug content '+ [SELECT Id, Name FROM NE__Catalog__c]);

        /*catIt = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id,
                                                            NE__ProductId__c = pord.Id, 
                                                            NE__Change_Subtype__c = 'test',
                                                            NE__Disconnect_Subtype__c = 'test');
        insert catIt;
        
        NE__Order__c ord = new NE__Order__c(NE__AccountId__c = lst_acc[0].Id,
                                            NE__BillAccId__c = lst_acc[0].Id,
                                            NE__OptyId__c = opp1.Id,
                                            NE__AssetEnterpriseId__c='testEnterprise',
                                            NE__ServAccId__c = lst_acc[0].Id,
                                            NE__OrderStatus__c = Label.BI_NE_OrderStatus_F1_Ganada);
                          
          
        insert ord;


        NE__OrderItem__c oit = new NE__OrderItem__c(NE__OrderId__c = ord.Id,
                                                    NE__Qty__c = 2,
                                                    NE__Asset_Item_Account__c = lst_acc[0].id,
                                                    NE__Account__c = lst_acc[0].id,
                                                    NE__Status__c = Label.BI_LabelActive,
                                                    NE__ProdId__c = pord.Id,
                                                    NE__CatalogItem__c = catIt.Id);
        
        insert oit;


        opp1.BI_CHI_Bloqueo_SISON__c = true;      

        update opp1;*/



  

      }


      public static void dataload_two(){
        cat = new NE__Catalog__c(Name = 'Chile Empresas');
        insert cat;

      catIt = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id,
                                                            NE__ProductId__c = pord.Id, 
                                                            NE__Change_Subtype__c = 'test',
                                                            NE__Disconnect_Subtype__c = 'test');
        insert catIt;
        

      NE__Order__c ord = new NE__Order__c(NE__AccountId__c = acc16.Id,
                                            NE__BillAccId__c = acc16.Id,
                                            NE__OptyId__c = opp[0].Id,
                                            NE__OpportunityId__c = opp[0].Id,
                                            NE__AssetEnterpriseId__c='testEnterprise',
                                            NE__ServAccId__c = acc16.Id,
                                            NE__OrderStatus__c = Label.BI_NE_OrderStatus_F1_Ganada);
                          
          
        insert ord;


        NE__OrderItem__c oit = new NE__OrderItem__c(NE__OrderId__c = ord.Id,
                                                    NE__Qty__c = 2,
                                                    NE__Asset_Item_Account__c = acc16.id,
                                                    NE__Account__c = acc16.id,
                                                    NE__Status__c = Label.BI_LabelActive,
                                                    NE__ProdId__c = pord.Id,
                                                    NE__CatalogItem__c = catIt.Id,
                                                    //NE__OpportunityId__c = opp[0].Id,
                                                    Integration_Id__c = 'test1');
        
        insert oit;


        opp[0].BI_CHI_Bloqueo_SISON__c = true;      

        update opp;
      //System.runAs(usr){ 

      WSSison = new BI_OpportunitySisonSOAP ();

      oinfotype = new BI_OpportunitySisonSOAP.OpportunityInfoType();

      confinfotype = new BI_OpportunitySisonSOAP.ConfigurationInfoType();

      prodinfotype = new BI_OpportunitySisonSOAP.ProductInfoType();  

      prodinfotype.Integration_id = 'test_intProdId';           
      prodinfotype.Divisa = 'EUR';                  
      prodinfotype.Ingreso_por_unica_vez = 1000;        
      prodinfotype.Recurrente_bruto_mensual = 2500;       
      prodinfotype.Ingreso_por_unica_vez_descuento = 100;             
      prodinfotype.Id_del_producto = '123456';            
      prodinfotype.Cantidad = 2;                
      prodinfotype.Recurrente_bruto_mensual_descuento = 100;          
      //prodinfotype.Linea_padre;             
      prodinfotype.AccionSISON = 'CREATE';  


      confinfotype.Integration_id = 'test_intConfId';           
      confinfotype.Divisa = 'EUR';              
      confinfotype.Cuota_total_por_unica_vez = 1000;
      confinfotype.Cargo_total_recurrente = 100;      
      confinfotype.Catalogo = 'Empresas';             
      confinfotype.Modelo_comercial = 'Router';         
      confinfotype.AccionSISON = 'CREATE';          
      confinfotype.Fecha_Hora_creacion_version = date.today().addDays(-5);

      confinfotype.Productos = new List <BI_OpportunitySisonSOAP.ProductInfoType>();

      confinfotype.Productos.add(prodinfotype);

      oinfotype.Fecha_de_cierre = Date.newInstance(1900, 2, 1);
      oinfotype.Etapa = 'F4 - Design Solution'; 
      oinfotype.Duracion_del_contrato_Meses = 12;
      oinfotype.Fecha_entrega_oferta = date.today().addyears(5);
      oinfotype.Fecha_de_vigencia = date.today().addyears(5); 
      oinfotype.Comienzo_estimado_de_facturacion = date.today().addyears(5);
      oinfotype.Ingreso_por_unica_vez = 1000;
      oinfotype.Recurrente_bruto_mensual = 8455.20;
      oinfotype.Venta_bruta_total_FCV = 8455.20;
      oinfotype.Probabilidad_de_exito = '10'; 
      oinfotype.Plazo_estimado_de_provision_dias = 12;
      oinfotype.Id_Oportunidad_SF = opp[0].Account.BI_No_Identificador_fiscal__c + '_' + opp[0].BI_Numero_id_oportunidad__c;
      oinfotype.Tasa_de_interes = 0.0987;
      oinfotype.Tipo_de_renegociacion = 'Completa';  
      oinfotype.Divisa = 'EUR';
      }




    static testMethod void test_method_one() {
      dataload();
      Test.startTest();
      dataload_two();
        oinfotype.Versiones = new List <BI_OpportunitySisonSOAP.ConfigurationInfoType>();
        oinfotype.Versiones.add(confinfotype);
        oinfotype.Recurrente_bruto_mensual = null;

        BI_OpportunitySisonSOAP.updateFromSison(oinfotype);
        Test.stopTest();

    }



    static testMethod void test_method_two() {
    dataload();
    Test.startTest();
    dataload_two();

      oinfotype.Versiones = new List <BI_OpportunitySisonSOAP.ConfigurationInfoType>();
      oinfotype.Versiones.add(confinfotype);


      prodinfotype.AccionSISON = 'UPDATE';   
      confinfotype.AccionSISON = 'UPDATE';   

      BI_OpportunitySisonSOAP.updateFromSison(oinfotype);  
 

       confinfotype.Modelo_comercial = null;
       confinfotype.Catalogo = 'Empresas';   
       prodinfotype.AccionSISON = 'UPDATE';   
       confinfotype.AccionSISON = 'UPDATE';  

       BI_OpportunitySisonSOAP.updateFromSison(oinfotype);

       confinfotype.Modelo_comercial = 'Router'; 
       confinfotype.Fecha_Hora_creacion_version = null;

       BI_OpportunitySisonSOAP.updateFromSison(oinfotype);
       Test.stopTest();
    } 

    static testMethod void test_method_three() {
      dataload();
      Test.startTest();
      dataload_two();
      System.runAs(usr){ 

        oinfotype.Versiones = new List <BI_OpportunitySisonSOAP.ConfigurationInfoType>();
        oinfotype.Versiones.add(confinfotype);
        oinfotype.Tipo_de_oportunidad = 'Fijo';
        oinfotype.Etapa = 'F1 - Closed Lost';
        oinfotype.Motivo_de_perdida = 'TEST1';
        prodinfotype.AccionSISON = null;   
        confinfotype.AccionSISON = null;  

        BI_OpportunitySisonSOAP.updateFromSison(oinfotype);
        oinfotype.Motivo_de_perdida = null;
        BI_OpportunitySisonSOAP.updateFromSison(oinfotype);

      Test.stopTest();

      }
    }

    static testMethod void test_method_four() {
       dataload();
      Test.startTest();
      dataload_two();
      System.runAs(usr){ 

        oinfotype.Versiones = new List <BI_OpportunitySisonSOAP.ConfigurationInfoType>();
        oinfotype.Versiones.add(confinfotype);
        prodinfotype.Divisa = null; 
        confinfotype.Divisa = null;
        prodinfotype.Integration_id = 'test_intProdId';  
        confinfotype.Integration_id = 'test_intConfId';
        prodinfotype.AccionSISON = 'UPDATE';   
        confinfotype.AccionSISON = 'UPDATE';  

        BI_OpportunitySisonSOAP.updateFromSison(oinfotype); 

      Test.stopTest();

      }     
    }
    static testMethod void test_method_eight() {
       dataload();
      Test.startTest();
      dataload_two();
      System.runAs(usr){ 

        oinfotype.Versiones = new List <BI_OpportunitySisonSOAP.ConfigurationInfoType>();
        oinfotype.Versiones.add(confinfotype);
        prodinfotype.Divisa = null; 
        confinfotype.Divisa = null;
        prodinfotype.Integration_id = 'test_intProdId1';  
        confinfotype.Integration_id = 'test_intConfId1';
        prodinfotype.AccionSISON = 'CREATE';   
        confinfotype.AccionSISON = 'CREATE';  

        BI_OpportunitySisonSOAP.updateFromSison(oinfotype); 

      Test.stopTest();

      }     
    }

    static testMethod void test_method_five() {
      dataload();
      Test.startTest();
      dataload_two();
      System.runAs(usr){ 

        oinfotype.Versiones = new List <BI_OpportunitySisonSOAP.ConfigurationInfoType>();
        oinfotype.Versiones.add(confinfotype);
        confinfotype.Cuota_total_por_unica_vez = null;
        oinfotype.Divisa = 'EUR'; 
        confinfotype.Divisa = 'EUR';
        prodinfotype.AccionSISON = 'UPDATE';   
        confinfotype.AccionSISON = 'UPDATE';  

       BI_OpportunitySisonSOAP.updateFromSison(oinfotype);

      Test.stopTest();

      }      
    }

    static testMethod void test_method_six() {
       dataload();
      Test.startTest();
      dataload_two();
      System.runAs(usr){ 

        oinfotype.Versiones = new List <BI_OpportunitySisonSOAP.ConfigurationInfoType>();
        oinfotype.Versiones.add(confinfotype);
        confinfotype.Cargo_total_recurrente = null;
        confinfotype.Cuota_total_por_unica_vez = 1000;
         prodinfotype.AccionSISON = 'UPDATE';   
        confinfotype.AccionSISON = 'UPDATE';  

       BI_OpportunitySisonSOAP.updateFromSison(oinfotype); 

      Test.stopTest();

      }     
    }

    static testMethod void test_method_seven() {
      dataload();
      Test.startTest();
      dataload_two();
      System.runAs(usr){ 

        oinfotype.Versiones = new List <BI_OpportunitySisonSOAP.ConfigurationInfoType>();
        oinfotype.Versiones.add(confinfotype);
        confinfotype.Catalogo = null; 
        confinfotype.Cargo_total_recurrente = 100;  
        prodinfotype.AccionSISON = 'UPDATE';   
        confinfotype.AccionSISON = 'UPDATE';  

        BI_OpportunitySisonSOAP.updateFromSison(oinfotype); 

      Test.stopTest();

      }   

    }

}