/*------------------------------------------------------------  
Author:         Ana Cirac   
Company:        Deloitte
Description:    Class to call ROD WS
History
<Date>          <Author>        <Change Description>
03-Ago-2015     Ana Cirac        Initial Version 
10-Nov-2016     JC Terrón       Added .abbreviate() method on some fields to send to RoD.
------------------------------------------------------------*/
public class TGS_SitesIntegration{

   public static boolean inFutureContextSite = false;
   static final String rodErrorSiteIntegration = 'The Site has not been synchronized, please contact with your system administrator';

   public static Boolean skipAddSearchInformation = false;

    /*------------------------------------------------------------
    Author:         Laura Mendiola 
    Company:        Deloitte
    Description:    Class to integrate Sites creation with RoD
    History
    <Date>          <Author>            <Change Description>
    11-Mar-2015     Ana Cirac            Initial Version
    10-Nov-2016     JC Terrón           Added .abbreviate() method on some fields to send to RoD. Line 48.
    12-Jul-2017     Guillermo Muñoz     Disabled for Ferrovial sites
    20-Nov-2017     Álvaro López        Dynamic integration for Ferrovial sites
    ------------------------------------------------------------*/
    @future(callout=true)
    public static void createSites (Set<Id> sites) {
        
        /* Capture every integration errors in a list*/
        List<TGS_Error_Integrations__c> lErrors = new List<TGS_Error_Integrations__c>();
        /* List with CIs to update their External_Id or TGS_IntegrationErrorDescription */
        List<BI_Punto_de_instalacion__c> listSitesUpdate = new List<BI_Punto_de_instalacion__c>(); 
        /*START Álvaro López 20/11/2017 - Dynamic sending of sites for Ferrovial*/
        Boolean sendToRoD = true;

        List<TGS_Ferrovial_Sites_Integration__c> lst_fsi = new List<TGS_Ferrovial_Sites_Integration__c>();
        lst_fsi = TGS_Ferrovial_Sites_Integration__c.getAll().values();

        Map<String, TGS_Ferrovial_Sites_Integration__c> map_fsi = new Map<String, TGS_Ferrovial_Sites_Integration__c>();

        for(TGS_Ferrovial_Sites_Integration__c fsi : lst_fsi){
            map_fsi.put(fsi.TGS_AccountID_18__c, fsi);
        }     
        
        for (BI_Punto_de_instalacion__c site: [SELECT Id, Name, BI_Codigo_postal__c, Provincia__c,Pais__c, BI_Cliente__r.Parent.Parent.Parent.Name,
                                               BI_Sede__r.BI_Direccion__c,BI_Sede__r.BI_Localidad__c, BI_Sede__r.BI_Distrito__c,BI_Contacto__r.Phone,
                                               BI_Contacto__r.Fax
                                               FROM BI_Punto_de_instalacion__c 
                                               WHERE Id IN :sites]) {

            //Si el site no pertenece a un cliente de Ferrovial
            if(!map_fsi.containsKey(site.BI_Cliente__r.Parent.ParentId) && site.BI_Cliente__r.Parent.Parent.Parent.Name == Label.TGS_Ferrovial){
                sendToRoD = false;
            }
            if(sendToRoD){
            /*END Álvaro López 20/11/2017 - Dynamic sending of sites for Ferrovial*/
                /* Soap Petition */
                TGS_Site_Sync.Port0Soap port0Soap = new TGS_Site_Sync.Port0Soap();
                /* Authentication parameters */
                port0Soap.parameters = getAuthenticationInfo();         
                    
                TGS_Site_Sync.OutputMapping1 responseId =   new TGS_Site_Sync.OutputMapping1(); 
                try{                                      
                    if(!Test.isRunningTest()){   
                        responseId = port0Soap.create0(site.BI_Cliente__r.Parent.Parent.Parent.Name,'','', '', site.Name.abbreviate(50),'', site.BI_Sede__r.BI_Localidad__c.abbreviate(50),site.Pais__c.abbreviate(50),'',site.BI_Contacto__r.Phone,site.BI_Contacto__r.Fax,site.Provincia__c.abbreviate(50),site.BI_Sede__r.BI_Direccion__c.abbreviate(80),'',site.BI_Codigo_postal__c.abbreviate(10),'','New'); 
                    }    
                    System.debug('[TGS_SitesIntegration.createSites] response: ' + responseId);                                      
                    if (responseId.returnCode == 0 ) { //OK Update New Sites with its IntegrationId

                        site.TEMPEXT_ID__c = responseId.Request_ID;                                              
                        listSitesUpdate.add(site); 
                        System.debug('[TGS_SitesIntegration.createSites] site.TEMPEXT_ID: ' + site.TEMPEXT_ID__c);
                    }
                    else { //KO --> Add the ERROR to the list

                        TGS_Error_Integrations__c error = new TGS_Error_Integrations__c();
                        error.TGS_Interface__c = 'ROD'; 
                        error.TGS_Operation__c = 'New'; 
                        error.TGS_RecordId__c = site.Id;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                        error.TGS_RequestInfo__c = 'Message: '+ responseId.ReturnMessage;
                        lErrors.add(error);
                        /* Updates TGS_IntegrationError_Description with RoD Error */
                        System.debug('[TGS_SitesIntegration.createSites] TGS_IntegrationError_Description : ' + responseId.ReturnMessage);
                        site.TGS_IntegrationError_Description__c = rodErrorSiteIntegration;
                        listSitesUpdate.add(site); 
                                              
                    }      
                }catch(System.CalloutException e){

                    /* Updates TGS_IntegrationError_Description with RoD Error */
                    System.debug('[TGS_SitesIntegration.updateSites] TGS_IntegrationError_Description : ' + e.getMessage());
                    site.TGS_IntegrationError_Description__c = rodErrorSiteIntegration;
                    listSitesUpdate.add(site);  lErrors.add(saveTGSerror(site.Id, 'New',e.getMessage()));
                }
            }                                       
       }
                                                   
        /* Updated the Sites with its ExternalId=instanceId and record the errors*/
        inFutureContextSite=true;
        if(!listSitesUpdate.isEmpty()){
            update listSitesUpdate;
        }
        if(!lErrors.isEmpty()){
            insert lErrors;
        }
        inFutureContextSite=false;                                  
    }
    
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    Class to integrate Updates of Sites with RoD
    History
    <Date>          <Author>            <Change Description>
    11-Mar-2015     Ana Cirac            Initial Version
    10-Nov-2016     JC Terrón           Added .abbreviate() method on some fields to send to RoD.Line 132.
    12-Jul-2017     Guillermo Muñoz     Disabled for Ferrovial sites
    20-Nov-2017     Álvaro López        Dynamic integration for Ferrovial sites
    ------------------------------------------------------------*/
    @future(callout=true)
    public static void updateSites (Set<Id> sites,  Map<Id,String> oldSites){

        /* Capture every integration errors in a list*/
        List<TGS_Error_Integrations__c> lErrors = new List<TGS_Error_Integrations__c>();
        /* List with CIs to update their External_Id or TGS_IntegrationErrorDescription */
        List<BI_Punto_de_instalacion__c> listSitesUpdate = new List<BI_Punto_de_instalacion__c>();      
        /* action to send to ROD update or New */
        String action;
        /* Stores the old name of the site */
        String oldSite;
        /*START Álvaro López 20/11/2017 - Dynamic sending of sites for Ferrovial*/
        Boolean sendToRoD = true;

        List<TGS_Ferrovial_Sites_Integration__c> lst_fsi = new List<TGS_Ferrovial_Sites_Integration__c>();
        lst_fsi = TGS_Ferrovial_Sites_Integration__c.getAll().values();

        Map<String, TGS_Ferrovial_Sites_Integration__c> map_fsi = new Map<String, TGS_Ferrovial_Sites_Integration__c>();

        for(TGS_Ferrovial_Sites_Integration__c fsi : lst_fsi){
            map_fsi.put(fsi.TGS_AccountID_18__c, fsi);
        }

        for (BI_Punto_de_instalacion__c site: [SELECT Id, Name, BI_Codigo_postal__c, Provincia__c,Pais__c, BI_Cliente__r.Parent.Parent.Parent.Name,
                                               BI_Sede__r.BI_Direccion__c,BI_Sede__r.BI_Localidad__c, BI_Sede__r.BI_Distrito__c,BI_Contacto__r.Phone,
                                               BI_Contacto__r.Fax, TEMPEXT_ID__c
                                               FROM BI_Punto_de_instalacion__c 
                                               WHERE Id IN :sites]) {

            //Si el site no pertenece a un cliente de Ferrovial
            if(!map_fsi.containsKey(site.BI_Cliente__r.Parent.ParentId) && site.BI_Cliente__r.Parent.Parent.Parent.Name == Label.TGS_Ferrovial){
                sendToRoD = false;
            }
            if(sendToRoD){
            /*END Álvaro López 20/11/2017 - Dynamic sending of sites for Ferrovial*/
                /* Soap Petition */
                TGS_Site_Sync.Port0Soap port0Soap = new TGS_Site_Sync.Port0Soap();
                /* Authentication parameters */
                port0Soap.parameters = getAuthenticationInfo();         
                /* Calculates action( create or update a site) attribute */
                if(String.isNotBlank(site.TEMPEXT_ID__c)){
                    action = 'Update';
                    oldSite = oldSites.get(site.Id);
                }else{
                    action = 'New';
                    oldSite = '';
                }

                TGS_Site_Sync.OutputMapping1  responseId = new TGS_Site_Sync.OutputMapping1(); 
                                                 
                try{                                
                    if(!Test.isRunningTest()){   
                        System.debug('[TGS_SitesIntegration.updateSites]');     
                        //MGC 28-10-16
                        //responseId = port0Soap.Create0(site.BI_Cliente__r.Parent.Parent.Parent.Name,'','', oldSite, site.Name,'',site.BI_Sede__r.BI_Localidad__c,site.Pais__c,'',site.BI_Contacto__r.Phone,site.BI_Contacto__r.Fax, site.Provincia__c,site.BI_Sede__r.BI_Direccion__c,'',site.BI_Codigo_postal__c,'', action);                                                                                                                                      
                        responseId = port0Soap.Create0(site.BI_Cliente__r.Parent.Parent.Parent.Name,'',site.Pais__c, oldSite, site.Name.abbreviate(50),'',site.BI_Sede__r.BI_Localidad__c.abbreviate(50),site.Pais__c.abbreviate(50),'',site.BI_Contacto__r.Phone,site.BI_Contacto__r.Fax, site.Provincia__c.abbreviate(50),site.BI_Sede__r.BI_Direccion__c.abbreviate(80),'',site.BI_Codigo_postal__c.abbreviate(10),'', action);                                                                                                                                      
                    }      
                    System.debug('[TGS_SitesIntegration.updateSites] response: ' + responseId);                                      
                    if (responseId.returnCode == 0) { //OK Update Sites with its IntegrationId
                        site.TEMPEXT_ID__c = responseId.Request_ID;  site.TGS_IntegrationError_Description__c = '';                        
                        /* Updates error integration description to empty */                                                      
                        listSitesUpdate.add(site); 
                        System.debug('[TGS_SitesIntegration.createSites] site.TEMPEXT_ID: ' + site.TEMPEXT_ID__c);
                    }
                    else{
                        TGS_Error_Integrations__c error = new TGS_Error_Integrations__c();
                        error.TGS_Interface__c = 'ROD'; 
                        error.TGS_Operation__c = action; 
                        error.TGS_RecordId__c = site.Id;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                        error.TGS_RequestInfo__c = 'Message: '+ responseId.ReturnMessage;
                        lErrors.add(error);   
                        /* Updates TGS_IntegrationError_Description with RoD Error */
                        System.debug('[TGS_SitesIntegration.updateSites] TGS_IntegrationError_Description : ' + responseId.ReturnMessage);
                        site.TGS_IntegrationError_Description__c = rodErrorSiteIntegration;
                        listSitesUpdate.add(site);                        
                    }                                                   
                }catch(System.CalloutException e){
                    /* Updates TGS_IntegrationError_Description with RoD Error */
                    System.debug('[TGS_SitesIntegration.updateSites] TGS_IntegrationError_Description : ' + e.getMessage());
                    site.TGS_IntegrationError_Description__c = rodErrorSiteIntegration;
                    listSitesUpdate.add(site);  lErrors.add(saveTGSerror(site.Id, action,e.getMessage()));    
                }
            }
       }
        
        /* Updated the Sites with its ExternalId=instanceId and record the errors*/
        inFutureContextSite=true;
        if(!listSitesUpdate.isEmpty()){
            update listSitesUpdate;
        }
        if(!lErrors.isEmpty()){
            insert lErrors;
        }
        inFutureContextSite=false;
    }


 /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    Class to integrate Delete of Sites with RoD
    History
    <Date>          <Author>        <Change Description>
    11-Mar-2015     Ana Cirac        Initial Version
    ------------------------------------------------------------*/
    public static void deleteSites (List<BI_Punto_de_instalacion__c> sites) {

        for (BI_Punto_de_instalacion__c site : sites) {

            sendPetition(site.BI_Cliente__r.Parent.Parent.Parent.Name,'','', '', site.Name,'',
                         site.BI_Sede__r.BI_Localidad__c,site.Pais__c,'',site.BI_Contacto__r.Phone,site.BI_Contacto__r.Fax,
                         site.Provincia__c,site.BI_Sede__r.BI_Direccion__c,'',site.BI_Codigo_postal__c,'', 'Off');                                          
            }
    }

 
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    Class to integrate Sites with RoD
    History
    <Date>          <Author>        <Change Description>
    11-Mar-2015     Ana Cirac        Initial Version
    ------------------------------------------------------------*/
    @future(callout=true)
    private static void sendPetition (String company, String region, String siteGroup,String siteOld, String siteNew,
                               String additional,String city, String country, String locationID, String phone,
                               String fax, String province, String street, String zone, String cp, String description, String accion) {
        
        
            /* Soap Petition */
            TGS_Site_Sync.Port0Soap port0Soap = new TGS_Site_Sync.Port0Soap();
            /* Authentication parameters */
            port0Soap.parameters = getAuthenticationInfo();         
            TGS_Site_Sync.OutputMapping1 responseId = new TGS_Site_Sync.OutputMapping1();   
            if(!Test.isRunningTest()){   
                responseId =   port0Soap.create0(company, region, siteGroup, siteOld,  siteNew, additional, city,  country,  locationID,  phone, fax,  province,  street,  zone,  cp,  description,  accion);   
             }   
                
        

    }
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    Return the AuthenticationInfo for the ROD call
    History
    <Date>          <Author>        <Change Description>
    03-ago-2015     Ana Cirac        Initial Version
    ------------------------------------------------------------*/
    private static TGS_Site_Sync.AuthenticationInfo getAuthenticationInfo(){
        
        TGS_Site_Sync.AuthenticationInfo authen = new TGS_Site_Sync.AuthenticationInfo();
        authen.userName = TGS_CallRoDWs.USERNAME_INTEGRATION;
        authen.password =  TGS_CallRoDWs.PASSWORD_INTEGRATION;
        
        return authen;
        
    }
    
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    Class to save a error integration

    <Date>          <Author>        <Change Description>
    15-Dec-2015     Ana Cirac        Initial Version
    ------------------------------------------------------------*/
    public static TGS_Error_Integrations__c saveTGSerror(String elementId, String Operation, String message){
        
        TGS_Error_Integrations__c error= new TGS_Error_Integrations__c();
        error.TGS_Interface__c = 'ROD'; 
        error.TGS_Operation__c = Operation; /* Añadir Operacion */
        error.TGS_RecordId__c = elementId;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
        error.TGS_RequestInfo__c = 'Message: '+message;
        
        return error;
    }
    


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       New Energy Aborda
     Description:   Adds searchable information to the sites so user can find sites

     History: 
    
     <Date>                     <Author>                    <Change Description>
     08/03/2016                 Jose Miguel Fierro          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void addSearchInformation(List<BI_Punto_de_instalacion__c> news) {
        // Try-catch à la BI_LogHelper.generateLog ?
        
        System.debug('[TGS_SitesIntegration.addSearchInformation] skipAddSearchInformation ' + skipAddSearchInformation);
        // If data was already filled in, no point in doing it again
        if(skipAddSearchInformation) return;

        // Relaciona BU con todos los sites que tengan esa BU
        Map<Id, List<BI_Punto_de_instalacion__c>> mapAcc2LstSites = new Map<Id, List<BI_Punto_de_instalacion__c>>();
        List<BI_Punto_de_instalacion__c> lstUpdate = new List<BI_Punto_de_instalacion__c>();

        for(BI_Punto_de_instalacion__c site : news) {
            if(mapAcc2LstSites.get(site.BI_Cliente__c) == null) {
                mapAcc2LstSites.put(site.BI_Cliente__c, new List<BI_Punto_de_instalacion__c>{ site });
            } else {
                mapAcc2LstSites.get(site.BI_Cliente__c).add(site);
            }
        }
        //TGS_Main_Account_Name__c
        List<Account> lstAccounts = [SELECT Id, Name, TGS_Aux_Holding__c, TGS_Aux_Holding__r.Name, TGS_Aux_Holding__r.TGS_Es_MNC__c,
                        Parent.Parent.ParentId, Parent.Parent.Parent.Name, Parent.Parent.Parent.TGS_Es_MNC__c   
                        FROM Account WHERE Id IN :mapAcc2LstSites.keyset()];
        System.debug('[TGS_SitesIntegration.addSearchInformation] lstAccounts: ' + lstAccounts);

        for(Account acc : lstAccounts) {
            if(acc.Parent.Parent.Parent.TGS_Es_MNC__c || acc.TGS_Aux_Holding__r.TGS_Es_MNC__c) {
                List<BI_Punto_de_instalacion__c> lstPdI = mapAcc2LstSites.get(acc.Id);
                if(lstPdI != null) {
                    for(BI_Punto_de_instalacion__c pdi : lstPdI) {
                        String accName = acc.TGS_Aux_Holding__c != null ? acc.TGS_Aux_Holding__r.Name : acc.Parent.Parent.Parent.Name;
                        if(accName == null)
                            System.debug('[TGS_SitesIntegration.addSearchInformation] Account ' + acc.Id + ' no tiene holding');

                        pdi.TGS_Main_Account_Name__c = accName;
                        lstUpdate.add(pdi);
                    }
                }
            } else {
                System.debug('[TGS_SitesIntegration.addSearchInformation] La cuenta ' + acc.Name + ' no está manejado por TGS');
            }

        }
        System.debug('[TGS_SitesIntegration.addSearchInformation] Sites to update: ' + lstUpdate);
        //if(Trigger.isAfter) update lstUpdate;
    }

}