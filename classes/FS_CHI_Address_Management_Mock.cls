/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Alejandro Labrin
Company:        Everis Centers Temuco
Description:    RFG-NOR-DIR-004 / RFG-NOR-DIR-005 / RFG-NOR-DIR-006: 
                Página Lightning Experience para normalización de direcciones.

History:

<Date>                      <Author>                        <Change Description>
25/11/2016                  Alejandro Labrin                Initial Version
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
global with sharing class FS_CHI_Address_Management_Mock implements HttpCalloutMock{
    /* SALESFORCE VARIABLE DECLARATION */
    /* Empty */
    
    global static HTTPResponse respond (HttpRequest request) {     
        /*DATA DEFINITION*/
        HttpResponse response = new HttpResponse(); 
        
        try{
            /* Response JSON */
            //String jsonResponse = '{"status":"0","reason":"0","matchingAddresses":{"validAddresses":[{"addressType":"string","normalized":"string","addressName":"String","addressNumber":{"value":0},"floor":{"value":0},"apartment":{"value":10},"area":"string","locality":"string","municipality":"string","region":"string","country":"string","postalCode":"string","addressExtension":"string","coordinates":{"longitude":"0","latitude":"0"},"isDangerous":false,"additionalData":[{"key":"additionalComments","value":"string"},{"key":"betweenStreet1","value":"string"},{"key":"betweenStreet2","value":"string"},{"key":"block","value":"0"},{"key":"housingComplexType","value":"string"},{"key":"housingComplexName","value":"string"},{"key":"boxNr","value":"0"},{"key":"isRedZone","value":"Y"},{"key":"isBlacklisted","value":"Y"}]}],"totalResults":1},"doubtAddresses":{"validAddresses":[{"addressType":"string","normalized":"string","addressName":"String","addressNumber":{"value":0},"floor":{"value":0},"apartment":{"value":10},"area":"string","locality":"string","municipality":"string","region":"string","country":"string","postalCode":"string","addressExtension":"string","coordinates":{"longitude":"0","latitude":"0"},"isDangerous":false,"additionalData":[{"key":"additionalComments","value":"string"},{"key":"betweenStreet1","value":"string"},{"key":"betweenStreet2","value":"string"},{"key":"block","value":"0"},{"key":"housingComplexType","value":"string"},{"key":"housingComplexName","value":"string"},{"key":"boxNr","value":"0"},{"key":"isRedZone","value":"Y"},{"key":"isBlacklisted","value":"Y"}]}],"totalResults":1}}';
            String jsonResponse = '{"validateAddressResponse":{"Status":"Validated","Reason":"0","matchingAddresses":{"validAddresses":[{"addressType":"string","normalized":"string","addressName":"String","addressNumber":{"value":0},"floor":{"value":0},"apartment":{"value":10},"area":"string","locality":"string","municipality":"string","region":"string","country":"string","postalCode":"string","addressExtension":"string","coordinates":{"longitude":"0","latitude":"0"},"isDangerous":false,"additionalData":[{"key":"additionalComments","value":"string"},{"key":"betweenStreet1","value":"string"},{"key":"betweenStreet2","value":"string"},{"key":"block","value":"0"},{"key":"housingComplexType","value":"string"},{"key":"housingComplexName","value":"string"},{"key":"boxNr","value":"0"},{"key":"isRedZone","value":"Y"},{"key":"isBlacklisted","value":"Y"}]}],"totalResults":1},"doubtAddresses":{"validAddresses":[{"addressType":"string","normalized":"string","addressName":"String","addressNumber":{"value":0},"floor":{"value":0},"apartment":{"value":10},"area":"string","locality":"string","municipality":"string","region":"string","country":"string","postalCode":"string","addressExtension":"string","coordinates":{"longitude":"0","latitude":"0"},"isDangerous":false,"additionalData":[{"key":"additionalComments","value":"string"},{"key":"betweenStreet1","value":"string"},{"key":"betweenStreet2","value":"string"},{"key":"block","value":"0"},{"key":"housingComplexType","value":"string"},{"key":"housingComplexName","value":"string"},{"key":"boxNr","value":"0"},{"key":"isRedZone","value":"Y"},{"key":"isBlacklisted","value":"Y"}]}],"totalResults":1}}}';
            response.setStatusCode(200); 
            response.setBody(jsonResponse);
        }catch(Exception exc){}
        
        return response;
    }
}