@isTest
private class PCA_RelationManagement_TEST {

    static testMethod void PCA_RelationManagement() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
	    system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
        /*BI_Pickup_Option__c seg1 = new BI_Pickup_Option__c();
        seg1.BI_Tipo__c = 'Segmento';
        seg1.Name = 'Negocios';
        insert seg1;
        system.debug('*$*$*$: '+seg1.Id);*/
        if(rec1!=null){
        	ima1.RecordTypeId =rec1.Id; 
        	ima1.BI_Titulo__c = 'Test Title';
        	ima1.BI_Informacion_Menu__c = 'Test Text';
        	ima1.BI_Segment__c = 'Negocios';
        	ima1.BI_Pestana__c = 'Relationship Management';
        insert ima1;
        }
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	item.BI_Segment__c = 'Negocios';
            	item.BI_Subsegment_Local__c = null;
            	accList.add(item);
            }
            update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
	       Attachment attach=new Attachment(Name='Unit Test Attachment',
	                                        body=bodyBlob,
	                                        parentId=ima1.Id, 
	                                        ContentType='image/png');  
	        insert attach;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
	    	system.runAs(user1){
	    		BI_TestUtils.throw_exception = false;
	    		
	    		PageReference pageRef = new PageReference('PCA_RelationManagement');
             	Test.setCurrentPage(pageRef);

	    		PCA_RelationManagement tmpContr = new PCA_RelationManagement();
	    		PageReference ret_page =  tmpContr.checkPermissions();
	    		tmpContr.loadinfo();
	    		system.assert(ret_page != null);
	    		
	    		
				BI_TestUtils.throw_exception = true;
	    			    		
	    		tmpContr = new PCA_RelationManagement();
	    		tmpContr.checkPermissions();
	
	    	}
	    }
    }
    static testMethod void PCA_RelationManagement2() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
	    system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
        /*BI_Pickup_Option__c seg1 = new BI_Pickup_Option__c();
        seg1.BI_Tipo__c = 'Segmento';
        seg1.Name = 'Empresas';
        insert seg1;*/
        if(rec1!=null){
        	ima1.RecordTypeId =rec1.Id; 
        	ima1.BI_Titulo__c = 'Test Title';
        	ima1.BI_Informacion_Menu__c = 'Test Text';
        	ima1.BI_Segment__c = 'Empresas';
        	ima1.BI_Pestana__c = 'Relationship Management';
        insert ima1;
        }
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	item.BI_Segment__c = 'Empresas';
            	item.BI_Subsegment_Local__c = null;
            	accList.add(item);
            }
            update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
	       Attachment attach=new Attachment(Name='Unit Test Attachment',
	                                        body=bodyBlob,
	                                        parentId=ima1.Id, 
	                                        ContentType='image/png');  
	        insert attach;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
	    	system.runAs(user1){
	    		BI_TestUtils.throw_exception = false;
	    		
	    		PageReference pageRef = new PageReference('PCA_RelationManagement');
             	 Test.setCurrentPage(pageRef);

	    		PCA_RelationManagement tmpContr = new PCA_RelationManagement();
	    		PageReference ret_page =  tmpContr.checkPermissions();
	    		tmpContr.loadinfo();
	    		system.assert(ret_page != null);
	    		
	    		
				BI_TestUtils.throw_exception = true;
	    			    		
	    		tmpContr = new PCA_RelationManagement();
	    		tmpContr.checkPermissions();
	
	    	}
	    }
    }
    static testMethod void PCA_RelationManagement3() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
	    system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
        /*BI_Pickup_Option__c seg1 = new BI_Pickup_Option__c();
        seg1.BI_Tipo__c = 'Segmento';
        seg1.Name = 'Empresas';
        insert seg1;*/
        if(rec1!=null){
        	ima1.RecordTypeId =rec1.Id; 
        	ima1.BI_Titulo__c = 'Test Title';
        	ima1.BI_Informacion_Menu__c = 'Test Text';
        	ima1.BI_Segment__c = 'Empresas';
        	ima1.BI_Pestana__c = 'Relationship Management';
        insert ima1;
        }
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
	       Attachment attach=new Attachment(Name='Unit Test Attachment',
	                                        body=bodyBlob,
	                                        parentId=ima1.Id, 
	                                        ContentType='image/png');  
	        insert attach;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
	    	system.runAs(user1){
	    		BI_TestUtils.throw_exception = false;
	    		
	    		PageReference pageRef = new PageReference('PCA_RelationManagement');
             	 Test.setCurrentPage(pageRef);

	    		PCA_RelationManagement tmpContr = new PCA_RelationManagement();
	    		PageReference ret_page =  tmpContr.checkPermissions();
	    		tmpContr.loadinfo();
	    		system.assert(ret_page != null);
	    		
	    		
				BI_TestUtils.throw_exception = true;
	    			    		
	    		tmpContr = new PCA_RelationManagement();
	    		tmpContr.checkPermissions();
	
	    	}
	    }
    }
    static testMethod void PCA_RelationManagement4() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
	    system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
        /*BI_Pickup_Option__c seg1 = new BI_Pickup_Option__c();
        seg1.BI_Tipo__c = 'Segmento';
        seg1.Name = 'Negocios';
        insert seg1;
        system.debug('*$*$*$: '+seg1.Id);*/
            system.debug('+rec: ' + rec1);
        if(rec1!=null){
        	ima1.RecordTypeId =rec1.Id;
        	ima1.BI_Titulo__c = 'Test Title';
        	ima1.BI_Informacion_Menu__c = 'Test Text';
        	ima1.BI_Segment__c = 'Empresas';
        	ima1.BI_Pestana__c = 'Relationship Management';
        	insert ima1;
        }
            
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	item.BI_Segment__c = null;
            	item.BI_Subsegment_Local__c = null;
            	accList.add(item);
            }
            update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
	       Attachment attach=new Attachment(Name='Unit Test Attachment',
	                                        body=bodyBlob,
	                                        parentId=ima1.Id, 
	                                        ContentType='image/png');  
	        insert attach;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
	    	system.runAs(user1){
	    		BI_TestUtils.throw_exception = false;
	    		
	    		PageReference pageRef = new PageReference('PCA_RelationManagement');
             	Test.setCurrentPage(pageRef);

	    		PCA_RelationManagement tmpContr = new PCA_RelationManagement();
	    		PageReference ret_page =  tmpContr.checkPermissions();
	    		tmpContr.loadinfo();
	    		system.assert(ret_page != null);
	    		
	    		
				BI_TestUtils.throw_exception = true;
	    			    		
	    		tmpContr = new PCA_RelationManagement();
	    		tmpContr.checkPermissions();
	
	    	}
	    }
    }
    static testMethod void PCA_RelationManagement5() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
	    system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
        /*BI_Pickup_Option__c seg1 = new BI_Pickup_Option__c();
        seg1.BI_Tipo__c = 'Segmento';
        seg1.Name = 'Negocios';
        insert seg1;
        system.debug('*$*$*$: '+seg1.Id);*/
            system.debug('+rec: ' + rec1);
        if(rec1!=null){
        	ima1.RecordTypeId =rec1.Id;
        	ima1.BI_Titulo__c = 'Test Title';
        	ima1.BI_Informacion_Menu__c = 'Test Text';
        	ima1.BI_Segment__c = 'Negocios';
        	ima1.BI_Pestana__c = 'Relationship Management';
        	insert ima1;
        }
            
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	item.BI_Segment__c = null;
            	item.BI_Subsegment_Local__c = null;
            	accList.add(item);
            }
            update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
	       Attachment attach=new Attachment(Name='Unit Test Attachment',
	                                        body=bodyBlob,
	                                        parentId=ima1.Id, 
	                                        ContentType='image/png');  
	        insert attach;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
	    	system.runAs(user1){
	    		BI_TestUtils.throw_exception = false;
	    		
	    		PageReference pageRef = new PageReference('PCA_RelationManagement');
             	Test.setCurrentPage(pageRef);

	    		PCA_RelationManagement tmpContr = new PCA_RelationManagement();
	    		PageReference ret_page =  tmpContr.checkPermissions();
	    		tmpContr.loadinfo();
	    		system.assert(ret_page != null);
	    		
	    		
				BI_TestUtils.throw_exception = true;
	    			    		
	    		tmpContr = new PCA_RelationManagement();
	    		tmpContr.checkPermissions();
	
	    	}
	    }
    }
    static testMethod void PCA_RelationManagement_variableCalls() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        PCA_RelationManagement relMan = new PCA_RelationManagement();
        if(relMan.ImgBack != null) {}
        if(relMan.AttImg != null) {}
        if(relMan.RelationImage != null) {}
        if(relMan.AccSegmento != null) {}
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}