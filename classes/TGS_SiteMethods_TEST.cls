@isTest
private class TGS_SiteMethods_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Álvaro López
     Company:       Accenture
     Description:   Methods executed by BI_Punto_de_instalacion_tgr Trigger (TGS)
     Test Class:    TGS_SiteMethods_TEST
     
     History:
      
     <Date>                  <Author>               <Change Description>
     30/01/2018              Álvaro López          	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	@isTest static void setUpdateIsoCode_test() {

		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_TGS__c = true;
        insert userTGS;
        
		Account le = TGS_Dummy_Test_Data.dummyHierarchy();
		Account bu = [SELECT Id, RecordType.DeveloperName, TGS_Aux_Holding__c FROM Account WHERE ParentId = :le.Id LIMIT 1];

		if(bu.RecordType.DeveloperName.equals(Constants.RECORD_TYPE_TGS_BUSINESS_UNIT)){
            BI_Sede__c sede = new BI_Sede__c(Name = 'Test sede', BI_Codigo_postal__c = '12345', BI_Direccion__c = 'Direccion', BI_Localidad__c = 'Localidad', BI_Provincia__c = 'Provincia', TGS_Available_Accounts__c = bu.TGS_Aux_Holding__c, BI_Country__c = 'United States');
            insert sede;
            BI_Punto_de_instalacion__c site = new BI_Punto_de_instalacion__c(Name = 'Test site', BI_Cliente__c = bu.Id, BI_Sede__c = sede.Id);
            TGS_SitesIntegration.inFutureContextSite = true;
            insert site;
            TGS_SitesIntegration.inFutureContextSite = false;

            sede.BI_Country__c = 'Spain';
            Test.startTest();
            update sede;
            Test.stopTest();
        }
	}
	
}