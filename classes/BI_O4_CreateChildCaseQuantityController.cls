/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   BI_O4_CreateChildCaseQuantityController for create child quantity case button
    Test Class:    BI_O4_CreateChildCaseQuantityCtrl_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera          Initial version
    20/10/2016              Fernando Arteaga        Added new fields
    29/11/2016              Alvaro García           Change the subject of the new case
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_CreateChildCaseQuantityController {

	public List<Case> lstCase {get; set;}
    public Integer i = 0;
    private List<User> user {get; set;}
    private String TGS = UserInfo.getProfileId();

    public BI_O4_CreateChildCaseQuantityController(ApexPages.StandardController controller) {
        
        Case caso = (Case) controller.getRecord();
        this.lstCase = [Select Id, OwnerId, BI_O4_DDO__c, RecordTypeId, RecordType.DeveloperName, Subject, Account.Name, Priority, Description, CreatedDate,BI_O4_Complexity__c,BI_O4_Autonomy__c,BI_O4_Cliente_no_existente_nombre__c,
                               BI_Nombre_de_la_Oportunidad__c, BI_O4_Cliente_final_nombre__c, BI_O4_Es_proyecto_especial__c, BI_O4_Proyecto_Especial__c,BI_O4_Duration_of_Contract_months__c,
                               BI_O4_Tipo_gen_rico_Preventa__c, BI_O4_Tipo_de_Facturaci_n__c, BI_O4_Tipo_de_movimiento__c, BI_O4_Periodo_de_contrataci_n_Meses__c,BI_O4_Tipo_oferta__c,
                               BI_O4_Fecha_presentaci_n_oferta__c, BI_O4_Identificativo_Origen__c, BI_O4_Quantity__c, BI_Department__c, BI_Type__c, Status,BI_O4_Estimated_FCV__c,
                               TGS_Service__c, BI_O4_Service__c, BI_O4_Main_Topics__c, BI_O4_Main_Topics_Rechazados__c, BI_O4_Big_Deal__c, BI_Country__c,Origin,
                               BI_O4_Oportunidad_externa__c, BI_O4_Type_of_Offer__c, BI_O4_URL_Cotizadores__c, BI_O4_Leading_Channel__c, BI_O4_Sales_Channel__c
                        From Case
                        Where Id =: caso.Id Limit 1];

        this.user = [Select Profile.Name from User where ProfileId =: TGS limit 1];
    }

    public PageReference createChildCase() {
        
        //Crear tantos casos hijos como indique el valor del campo BI_O4_Quantity__c copiando todos los campos del caso padre.
        // Al terminar se debe actualizar el valor BI_O4_Quantity__c a 1


        if(user[0].Profile.Name != 'BI_O4_TGS_Standard' && user[0].Profile.Name != 'TGS System Administrator'){
            lstCase[0].addError('Solo los usuarios con perfil TGS Standar pueden crear hijos');
            return null;
        }else{
        try{

            List<Case> newCases = new List<Case>();
            RecordType rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno'];
            if(!this.lstCase.isEmpty()){
                for(Case item : lstCase){
                    if(item.BI_O4_Quantity__c != null){
                        if(item.RecordTypeId == rt.Id && item.BI_Department__c == 'Ingeniería preventa' && item.BI_Type__c == 'Proyectos Especiales'){
                            for(i=0; i<item.BI_O4_Quantity__c; i++){
                                Case caso = new Case();
                                caso.AccountId = this.lstCase[0].AccountId;
                                caso.OwnerId = this.lstCase[0].OwnerId;
                                caso.ParentId = this.lstCase[0].Id;
                                caso.RecordTypeId = this.lstCase[0].RecordTypeId;
                                caso.BI_Nombre_de_la_Oportunidad__c = this.lstCase[0].BI_Nombre_de_la_Oportunidad__c;
                                //caso.Subject = this.lstCase[0].BI_O4_Cliente_final_nombre__c + ' ' + this.lstCase[0].Account.Name + ' GEN';
                                caso.Subject = this.lstCase[0].Subject; 
                                caso.Status = this.lstCase[0].Status;
                                caso.Priority = this.lstCase[0].Priority;
                                caso.Description = this.lstCase[0].Description;
                                caso.BI_O4_Es_proyecto_especial__c = this.lstCase[0].BI_O4_Es_proyecto_especial__c;
                                caso.BI_O4_Proyecto_Especial__c = this.lstCase[0].BI_O4_Proyecto_Especial__c;
                                caso.BI_O4_Tipo_gen_rico_Preventa__c = this.lstCase[0].BI_O4_Tipo_gen_rico_Preventa__c;
                                caso.BI_O4_Tipo_de_Facturaci_n__c = this.lstCase[0].BI_O4_Tipo_de_Facturaci_n__c;
                                caso.BI_O4_Tipo_de_movimiento__c = this.lstCase[0].BI_O4_Tipo_de_movimiento__c;
                                caso.BI_O4_Periodo_de_contrataci_n_Meses__c = this.lstCase[0].BI_O4_Periodo_de_contrataci_n_Meses__c;
                                caso.BI_O4_Fecha_presentaci_n_oferta__c = this.lstCase[0].BI_O4_Fecha_presentaci_n_oferta__c;
                                caso.BI_O4_Identificativo_Origen__c = this.lstCase[0].BI_O4_Identificativo_Origen__c;
                                caso.BI_Department__c = this.lstCase[0].BI_Department__c;
                                caso.TGS_Service__c = this.lstCase[0].TGS_Service__c;
                                caso.BI_O4_Service__c = this.lstCase[0].BI_O4_Service__c;
                                caso.BI_O4_Main_Topics__c = this.lstCase[0].BI_O4_Main_Topics__c;
                                caso.BI_O4_Main_Topics_Rechazados__c = this.lstCase[0].BI_O4_Main_Topics_Rechazados__c;
                                caso.BI_O4_Big_Deal__c = this.lstCase[0].BI_O4_Big_Deal__c;
                                caso.BI_Country__c = this.lstCase[0].BI_Country__c; 
                                //caso.CreatedDate = this.lstCase[0].CreatedDate;
                                caso.BI_Type__c = this.lstCase[0].BI_Type__c;
                                caso.BI_O4_Cliente_no_existente_nombre__c = this.lstCase[0].BI_O4_Cliente_no_existente_nombre__c;
                                caso.Origin = this.lstCase[0].Origin;
                                caso.BI_O4_Tipo_oferta__c = this.lstCase[0].BI_O4_Tipo_oferta__c;
                                caso.BI_O4_Estimated_FCV__c = this.lstCase[0].BI_O4_Estimated_FCV__c;
                                caso.BI_O4_Duration_of_Contract_months__c = this.lstCase[0].BI_O4_Duration_of_Contract_months__c;
                                caso.BI_O4_Autonomy__c = this.lstCase[0].BI_O4_Autonomy__c;
                                caso.BI_O4_Complexity__c = this.lstCase[0].BI_O4_Complexity__c;
                                caso.BI_O4_Oportunidad_externa__c = this.lstCase[0].BI_O4_Oportunidad_externa__c;
                                caso.BI_O4_Type_of_Offer__c = this.lstCase[0].BI_O4_Type_of_Offer__c;
                                caso.BI_O4_URL_Cotizadores__c = this.lstCase[0].BI_O4_URL_Cotizadores__c;
                                caso.BI_O4_Leading_Channel__c = this.lstCase[0].BI_O4_Leading_Channel__c;
                                caso.BI_O4_Sales_Channel__c = this.lstCase[0].BI_O4_Sales_Channel__c;
                               
                                newCases.add(caso);
                            }
                            item.BI_O4_Quantity__c = 1;
                        }
                    }
                    else{    
                        
                        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, String.valueOf('El valor del campo Quantity está vacio')));
                        return null;
                    }
                }
                insert newCases;
                update this.lstCase;
            }

            return new PageReference('/' + this.lstCase[0].Id);

        }catch(Exception e){

            BI_LogHelper.generate_BILog('BI_O4_CreateChildCaseQuantityController.createChildCase', 'BI_EN', e, 'Clase');
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, String.valueOf(e.getMessage())));
            return null;
        }
        }


    }

    public PageReference cancel(){

        return new PageReference('/' + this.lstCase[0].Id);
    }
}