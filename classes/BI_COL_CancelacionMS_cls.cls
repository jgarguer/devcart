/************************************************************************************
* Avanxo Colombia
* @author           Jeisson Alexander Rojas Noy href=<jrojas@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                               Descripción        
*           -----   ----------      ---------------------------         ---------------    
* @version   1.0    2015-25-05      Jeisson Alexander Rojas Noy (JR)    Clase encargada de realizar la cancelación de las Modificaciones de Servicios 
*************************************************************************************/
public class BI_COL_CancelacionMS_cls 
{
    //Método encargado de llamar al process builder
    @InvocableMethod
    public static void invokeapexcallout( list<BI_COL_Modificacion_de_Servicio__c> lstModifi_Servic ) 
    {
        List<String> lstIdModifiServic = new List<String>();
        List<BI_Log__c> lstLogs = new List<BI_Log__c>();

        if( !System.isFuture() && !System.isBatch()) //&& usu.Pais__c == 'Colombia')
        {
            System.debug('\n\n########------->Ejecución Trigger cancelacion de la MS \n\n');
            for( BI_COL_Modificacion_de_Servicio__c objMs : lstModifi_Servic)
            {
                lstIdModifiServic.add( objMs.Id );
                BI_Log__c registroBitacora = new BI_Log__c();
                registroBitacora.BI_COL_Modificacion_Servicio__c = objMs.id;
                registroBitacora.BI_COL_Identificador__c = 'CANCELACION MODIFICACION DE SERVICIO';
                registroBitacora.BI_COL_Estado__c = 'Exitoso';
                registroBitacora.BI_COL_Tipo_Transaccion__c = 'CANCELACION OT';
                //registroBitacora.BI_COL_Informacion_Enviada__c;
                //registroBitacora.BI_COL_Informacion_recibida__c;
                //registroBitacora.BI_COL_Producto_Telefonica__c= hm.ID;
                lstLogs.add( registroBitacora );
                lstIdModifiServic.add( objMs.ID );
            }

            BI_COL_CancelacionMS_cls.envioDatosTR( lstIdModifiServic );

            insert lstLogs;
        }
    }

    //
    @Future (callout=true)
    public static void envioDatosTR(List<String> lstIdMS)
    {
        List<BI_COL_Modificacion_de_Servicio__c> lstMS = [select Id, Name, BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Estado_orden_trabajo__c,
                                                        BI_COL_Motivo_cancelacion__c
                                                        from BI_COL_Modificacion_de_Servicio__c 
                                                        where id in :lstIdMS
                                                        //and Producto_Telefonica__r.Viaja_TRS__c=true
                                                        ];
        
        List<BI_Log__c> lstLogTran=new List<BI_Log__c>(); 
        try
        {
            
            //Ejecutar Servicio Web de OT
            //InterfazSISGOTR.EnviarDatosBasicosDS(lstMS);
            Map<String,ID> mapMS=new Map<String,ID>(); 
            ws_TrsMasivo.instalacionCancelada[] instalacion= new ws_TrsMasivo.instalacionCancelada[lstMS.size()];
            
            for(BI_COL_Modificacion_de_Servicio__c ms:lstMS)
            {
                
                ws_TrsMasivo.instalacionCancelada ic=new ws_TrsMasivo.instalacionCancelada();
                //ic.causaCancelacion= ms.Motivo_cancelaci_n__c;
                ic.ds=ms.BI_COL_Codigo_unico_servicio__r.Name;
                ic.ms=ms.Name;
                ic.causaCancelacion=ms.BI_COL_Motivo_cancelacion__c;
                ic.fechaCancelacion = String.valueOf(System.today());//String.valueOf(ms.Fecha_carta_cliente__c);
                //System.debug('\n\n ic.fechaCancelacion: '+ic.fechaCancelacion+'\n\n');
                instalacion.add(ic);
                mapMS.put(ms.Name,ms.ID);
            }
            
            ws_TrsMasivo.serviciosSISGOTSOAP objWS = new ws_TrsMasivo.serviciosSISGOTSOAP();
            objWS.timeout_x=120000;
            ws_TrsMasivo.cancelarInstalacionesResponse[] arrRespuesta = objWS.cancelarInstalacionesSFDC(instalacion);
            System.debug('\n\n arrRespuesta: '+arrRespuesta+'\n\n');

            for(Integer i=0;i<arrRespuesta.size();i++)
            {
                System.debug('\n\n mapMS.get(arrRespuesta[i].ms))'+mapMS.get(arrRespuesta[i].ms)+'\n\n');
                if(arrRespuesta[i].codigoError=='0')
                {
                    lstLogTran.add(BI_COL_InterfaceSISGOTR_cls.GuardarEnLog('SISGOT - CANCELACION','Exitoso', arrRespuesta[i].ms ,arrRespuesta[i].mensajeError ,arrRespuesta[i].ms ,'Cancelaciones',mapMS.get(arrRespuesta[i].ms)));
                }
                else
                {
                    lstLogTran.add(BI_COL_InterfaceSISGOTR_cls.GuardarEnLog('SISGOT - CANCELACION','Fallido', arrRespuesta[i].ms , arrRespuesta[i].mensajeError ,arrRespuesta[i].ms ,'Cancelaciones', mapMS.get(arrRespuesta[i].ms)));
                }
            }

            system.debug('arrRespuesta'+arrRespuesta);

        }
        catch(Exception e)
        {
            System.debug(':: Se presento un error generando las cancelaciones: '+e.getMessage());
            for(BI_COL_Modificacion_de_Servicio__c ms:lstMS)
            {
                lstLogTran.add(BI_COL_InterfaceSISGOTR_cls.GuardarEnLog('SISGOT - CANCELACION','Fallido - Conexion', ms.Name , e.getMessage() ,' ' ,'Cancelaciones', ms.id));
            }
        }
        insert lstLogTran;
    }
}