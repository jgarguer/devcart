//Generated by wsdl2apex

public class BI_ExisteCodigoClienteWrapper {
    public class ExisteCodigoClienteOutMsg {
        public BI_ExisteCodigoClienteWrapper.ExisteCodigoClienteOutVO existeCodigoClienteReturn;
        private String[] existeCodigoClienteReturn_type_info = new String[]{'existeCodigoClienteReturn','http://movistar.cl/CIR/vo/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://movistar.cl/CIR/vo/','true','false'};
        private String[] field_order_type_info = new String[]{'existeCodigoClienteReturn'};
    }
    public class ExisteCodigoClienteInMsg {
        public BI_ExisteCodigoClienteWrapper.ExisteCodigoClienteInVO existeCodigoClienteInVO;
        private String[] existeCodigoClienteInVO_type_info = new String[]{'existeCodigoClienteInVO','http://movistar.cl/CIR/vo/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://movistar.cl/CIR/vo/','true','false'};
        private String[] field_order_type_info = new String[]{'existeCodigoClienteInVO'};
    }
    public class ExisteCodigoClienteOutVO {
        public String existeCodigoClienteReturn;
        public String errorCode;
        public String errorMessage;
        private String[] existeCodigoClienteReturn_type_info = new String[]{'existeCodigoClienteReturn','http://movistar.cl/CIR/vo/',null,'1','1','true'};
        private String[] errorCode_type_info = new String[]{'errorCode','http://movistar.cl/CIR/vo/',null,'1','1','true'};
        private String[] errorMessage_type_info = new String[]{'errorMessage','http://movistar.cl/CIR/vo/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://movistar.cl/CIR/vo/','true','false'};
        private String[] field_order_type_info = new String[]{'existeCodigoClienteReturn','errorCode','errorMessage'};
    }
    public class ExisteCodigoClienteInVO {
        public String rut;
        public String codigoCliente;
        private String[] rut_type_info = new String[]{'rut','http://movistar.cl/CIR/vo/',null,'1','1','true'};
        private String[] codigoCliente_type_info = new String[]{'codigoCliente','http://movistar.cl/CIR/vo/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://movistar.cl/CIR/vo/','true','false'};
        private String[] field_order_type_info = new String[]{'rut','codigoCliente'};
    }
}