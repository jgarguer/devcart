public with sharing class BI_HierarchyNode {
	public Account node;
	public List<BI_HierarchyNode> children;
	public list<Contact> contacts;
	public list<BI_Punto_de_instalacion__c> sites;

	public BI_HierarchyNode(Account acc) {
		node = acc;
		children = new List<BI_HierarchyNode>();
		contacts = new list<Contact>();
		sites = new list<BI_Punto_de_instalacion__c>();
	}
}