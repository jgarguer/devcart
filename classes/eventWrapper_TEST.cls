@isTest
private class eventWrapper_TEST {
	

	@isTest static void eventWrapper_Constructor() {

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		List<String> lst_pais = BI_Dataload.loadPaisFromPickList(1);
		system.assert(!lst_pais.isEmpty());

		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais);
		system.assert(!lst_acc.isEmpty());

		List<String> lst_whatId = new List<String>();
		for(Account acc :lst_acc){
			lst_whatId.add(acc.Id);
		}
		system.assert(!lst_whatId.isEmpty());


		List<Recordtype> lst_rt_event = [SELECT ID, sObjectType  FROM RecordType WHERE Developername  = 'BI_Evento' AND sObjectType= 'Event'];

		List<Event> lst_event = BI_Dataload.loadEventWithRT(2,lst_rt_event, lst_whatId );
		system.assert(!lst_event.isEmpty());

		for(Event eve :lst_event){
			eventWrapper ev = new eventWrapper('string subj', 'string descr', DateTime.now(), DateTime.now().addDays(12), true);

			ev.setEventId(eve.Id);
			ev.setVisible(true);
			ev.setOwner(false);
			ev.setEditable(true);	
		}

	}
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}