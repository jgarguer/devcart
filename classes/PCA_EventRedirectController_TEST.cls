@isTest
private class PCA_EventRedirectController_TEST {

    static testMethod void getloadInfo() {
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        //insert usr;
       
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;                   
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            PageReference pageRef = new PageReference('PCA_EventRedirect');
            Test.setCurrentPage(pageRef);
			DateTime day = datetime.newInstance(2014, 9, 15, 12, 30, 0);
	        DateTime day10 = datetime.newInstance(2014, 9, 15, 13, 30, 0);
			Event ev1 = new Event(subject='Nuevo Evento', description='Test Evento', ownerId = acc[0].ownerId, DurationInMinutes=60,ActivityDateTime=DAY,ActivityDate=Date.today(),EndDateTime=day10,StartDateTime=day);
			insert ev1;            
            system.runAs(user1){           
            ApexPages.currentPage().getParameters().put('id',  ev1.Id);
        	PCA_EventRedirectController constructor = new PCA_EventRedirectController();
        	BI_TestUtils.throw_exception = false;
        	constructor.checkPermissions();            
        	constructor.initialize();
            }
            system.runAs(user1){           
            ApexPages.currentPage().getParameters().put('id',  ev1.Id);
        	PCA_EventRedirectController constructor = new PCA_EventRedirectController();
        	BI_TestUtils.throw_exception = true;
        	constructor.checkPermissions();
        	constructor.initialize();
            }
           /* system.runAs(user1){           
            ApexPages.currentPage().getParameters().put('id',  ev1.Id);
        	PCA_EventRedirectController constructor = new PCA_EventRedirectController();
        	constructor.checkPermissions();
        	constructor.initialize();
            }*/
        }
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}