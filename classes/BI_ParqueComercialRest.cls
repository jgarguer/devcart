@RestResource(urlMapping='/accountresources/v1/assets/*')
global with sharing class BI_ParqueComercialRest
{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Class that loads commercial asset records from CSB related to the accounts
    
    History:
    
    <Date>            <Author>          	<Description>
    09/12/2015        Fernando Arteaga      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Inserts commercial asset records received from CSB, deleting existing ones previously
    
    IN:            
    OUT:           void
    
    History:
    
    <Date>            <Author>          	<Description>
    09/12/2015        Fernando Arteaga     	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @HttpPost
    global static void insertCommercialAssets()
    {
    	Set<String> setAccountIds = new Set<String>();
    	Set<String> setProductIds = new Set<String>();
    	Map<String, Map<String, List<BI_RestWrapper.UsageType>>> mapAccSubscriptions = new Map<String, Map<String, List<BI_RestWrapper.UsageType>>>();
    	Map<String, Id> mapAccountsCSB = new Map<String, Id>();
		Map<String, NE__Asset__c> mapAccIdAsset = new Map<String, NE__Asset__c>();
		Map<String, ANI__c> mapSubIdSubscription = new Map<String, ANI__c>();
		Map<String, NE__Catalog_Item__c> mapCatalogItems = new Map<String, NE__Catalog_Item__c>();
		List<Servicio_Parque__c> listServicios = new List<Servicio_Parque__c>();
		List<ANI__c> listSubscriptions = new List<ANI__c>();
    	List<ANI__c> listSubsWithoutProducts = new List<ANI__c>();
    	Map<Id, ANI__c> mapSubsCreated;
    	Map<Id, Integer> mapParqueNumSubscriptions = new Map<Id, Integer>();
		Id catalogId;
    	System.Savepoint savePoint;
    	Boolean subscriptionHasProducts = false;

    	
    	try
    	{
    		savePoint = Database.setSavepoint();
            Blob blobBody = RestContext.request.requestBody;
            String strUrlUTF8 = blobBody.toString();
            String strBody = EncodingUtil.urlDecode(strUrlUTF8, 'UTF-8');
            System.debug('strBody:' + strBody);
            
			String jsonString = strBody.replaceAll('\"[^\"]*\":null',''); //basic removal of null values
			jsonString = jsonString.replaceAll('\"[^\"]*\":\\[null\\]','');
			jsonString = jsonString.replaceAll('\"[^\"]*\":\"\"',''); //basic removal of ""
			jsonString = jsonString.replaceAll(',{2,}', ','); //remove duplicate/multiple commas
			jsonString = jsonString.replace('{,', '{'); //prevent opening brace from having a comma after it
			jsonString = jsonString.replace(',}', '}'); //prevent closing brace from having a comma before it
			jsonString = jsonString.replace('[,', '['); //prevent opening bracket from having a comma after it
			jsonString = jsonString.replace(',]', ']'); //prevent closing bracket from having a comma before it
			
			System.debug(jsonString);

            BI_RestWrapper.UsageReportType usageType = (BI_RestWrapper.UsageReportType) JSON.deserialize(jsonString, BI_RestWrapper.UsageReportType.class);
			System.debug(LoggingLevel.INFO, 'usageType:' + usageType);
			if (usageType.Usage != null && !usageType.Usage.isEmpty())
			{
				// Fill maps that relate accounts with subscriptions, and subscriptions with services
				for (BI_RestWrapper.UsageType usage :usageType.Usage)
				{
					if (usage.accountId != null)
					{
						setAccountIds.add(usage.accountId);
						setProductIds.add(usage.productId);
						if (!mapAccSubscriptions.containsKey(usage.accountId) && usage.subscriptionId != null)
							mapAccSubscriptions.put(usage.accountId, new Map<String, List<BI_RestWrapper.UsageType>>{usage.subscriptionId => new List<BI_RestWrapper.UsageType>{usage}});
						else if (usage.subscriptionId != null && mapAccSubscriptions.get(usage.accountId).containsKey(usage.subscriptionId))
						{
							mapAccSubscriptions.get(usage.accountId).get(usage.subscriptionId).add(usage);
							System.debug(LoggingLevel.INFO, 'mapAccSubscriptions 1:' + mapAccSubscriptions);
						}
						else if (usage.subscriptionId != null && !mapAccSubscriptions.get(usage.accountId).containsKey(usage.subscriptionId))
						{
							Map<String, List<BI_RestWrapper.UsageType>> mapActual = mapAccSubscriptions.get(usage.accountId);
							mapActual.put(usage.subscriptionId, new List<BI_RestWrapper.UsageType>{usage});
							System.debug(LoggingLevel.INFO, 'mapActual:' + mapActual);
							mapAccSubscriptions.put(usage.accountId, mapActual);
							System.debug(LoggingLevel.INFO, 'mapAccSubscriptions 2:' + mapAccSubscriptions);
						}
					}
				}
				
				System.debug(LoggingLevel.INFO, 'mapAccSubscriptions final:' + mapAccSubscriptions);

				// Get a map of catalog items
				for (NE__Catalog_Item__c catItem: [SELECT Id, Name, BI_CSB_Code__c, NE__ProductId__r.Name, NE__Catalog_Id__c
											  	   FROM NE__Catalog_Item__c
											  	   WHERE BI_CSB_Code__c IN :setProductIds])
				{
					mapCatalogItems.put(catItem.BI_CSB_Code__c, catItem);
				}
				
				catalogId = !mapCatalogItems.isEmpty() ? mapCatalogItems.values()[0].NE__Catalog_Id__c : null;
				
				// Get old records to delete
				List<NE__Asset__c> listParque = [SELECT Id, NE__AccountId__c, NE__AccountId__r.BI_Id_CSB__c
											 	 FROM NE__Asset__c
												 WHERE NE__AccountId__r.BI_Id_CSB__c IN :setAccountIds
												 AND BI_CSB_Is_Digital_Service__c = true
												 FOR UPDATE];
				// Delete records if list is not empty
				if (!listParque.isEmpty())
				{
					for (NE__Asset__c asset :listParque)
					{
						if (!mapAccountsCSB.containsKey(asset.NE__AccountId__r.BI_Id_CSB__c))
							mapAccountsCSB.put(asset.NE__AccountId__r.BI_Id_CSB__c, asset.NE__AccountId__c);
					}
				}
				else // Fill a map of accounts
				{
					for (Account acc: [SELECT Id, BI_Id_CSB__c
									   FROM Account
									   WHERE BI_Id_CSB__c IN :setAccountIds])
						mapAccountsCSB.put(acc.BI_Id_CSB__c, acc.Id);
				}
				
				List<NE__Asset__c> listNewAssets = new List<NE__Asset__c>();
				
				// Insert new Asset records
				for (String accId: mapAccSubscriptions.keySet())
				{
					// Create new assets if the account exists
					if (mapAccountsCSB.get(accId) != null)
					{
						NE__Asset__c newAsset = new NE__Asset__c(NE__AccountId__c = mapAccountsCSB.get(accId),
																 NE__BillAccId__c = mapAccountsCSB.get(accId),
																 NE__CatalogId__c = catalogId,
																 BI_CSB_Is_Digital_Service__c = true,
																 Parque_External_Id__c = 'Parque CSB_' + accId);
						listNewAssets.add(newAsset);
						mapAccIdAsset.put(accId, newAsset);
					}
				}
				
				System.debug(LoggingLevel.INFO, 'listNewAssets:' + listNewAssets);
				
				// Delete old assets
				delete listParque;
				// Insert assets
				insert listNewAssets;

				// Insert new subscriptions related to the assets
				for (String accId: mapAccIdAsset.keySet())
				{
					for (String subscr : mapAccSubscriptions.get(accId).keySet())
					{
						ANI__c newSubsc = new ANI__c(Name = 'Suscripción' + '_' + subscr,
													 Parque__c = mapAccIdAsset.get(accId).Id,
													 Description__c = 'Suscripción' + ' ' + subscr);
													 
						listSubscriptions.add(newSubsc);
						mapSubIdSubscription.put(subscr, newSubsc);
					}
				}
				
				System.debug(LoggingLevel.INFO, 'listSubscriptions:' + listSubscriptions);
				
				if (mapCatalogItems.isEmpty())
				{
					delete listNewAssets;
					RestContext.response.statusCode = 200;
				}
				else
				{
					// Insert subscriptions
					insert listSubscriptions;
					mapSubsCreated = new Map<Id, ANI__c>(listSubscriptions);
					
					// Insert new services related to the subscriptions
					for (String accId: mapAccIdAsset.keySet())
					{
						for (String subscr : mapAccSubscriptions.get(accId).keySet())
						{
							subscriptionHasProducts = false;
							
							for (BI_RestWrapper.UsageType usage : mapAccSubscriptions.get(accId).get(subscr))
							{
								if (mapCatalogItems.get(usage.productId) != null)
								{
									Servicio_Parque__c newServ = new Servicio_Parque__c(ANI__c = mapSubIdSubscription.get(subscr).Id,
																						Parque__c = mapSubIdSubscription.get(subscr).Parque__c,
																						Description__c = mapCatalogItems.get(usage.productId).NE__ProductId__r.Name,
																						Type__c = Label.BI_CSB_Service,
																						Qty__c = usage.consumption.value,
																						BI_CSB_Fecha_de_inicio__c = usage.consumption.validFor.startDateTime,
																						BI_CSB_Fecha_de_finalizacion__c = usage.consumption.validFor.endDateTime);
									listServicios.add(newServ);
									subscriptionHasProducts = true;
								}
							}
							
							if (!subscriptionHasProducts)
							{
								listSubsWithoutProducts.add(mapSubsCreated.get(mapSubIdSubscription.get(subscr).Id));
							}
							else
							{
								if (mapParqueNumSubscriptions.get(mapSubIdSubscription.get(subscr).Parque__c) != null)
								{
									Integer i = mapParqueNumSubscriptions.get(mapSubIdSubscription.get(subscr).Parque__c) + 1;
									mapParqueNumSubscriptions.put(mapSubIdSubscription.get(subscr).Parque__c, i);
								}
								else
									mapParqueNumSubscriptions.put(mapSubIdSubscription.get(subscr).Parque__c, 1);
							}
						}
					}
					
					System.debug(LoggingLevel.INFO, 'listServicios:' + listServicios);
					insert listServicios;
					
					List<NE__Asset__c> listToDelete = new List<NE__Asset__c>();
					if (mapParqueNumSubscriptions.isEmpty())
						listToDelete.addAll(listNewAssets);
					else
					{
						for (NE__Asset__c parque: listNewAssets)
						{
							if (mapParqueNumSubscriptions.get(parque.Id) == null)
								listToDelete.add(parque);
						}
					}
					
					if (!listToDelete.isEmpty())
						delete listToDelete;
						
					delete listSubsWithoutProducts;
					
					RestContext.response.statusCode = 200;
				}
			}
			else
			{
				RestContext.response.statusCode = 400;
				RestContext.response.headers.put('errorMessage', 'Parameter \'Usage\' is mandatory');
			}
    	}
    	catch(Exception exc)
    	{
    		Database.rollback(savePoint);
			System.debug(LoggingLevel.INFO, 'exc:' + exc);
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage', exc.getMessage());
			BI_LogHelper.generate_BILog('BI_ParqueComercialRest.insertCommercialAssets', 'BI_EN', exc, 'Web Service');
    	}
    }
}