public class BIIN_Obtener_Token_BAO {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Jose María Martín Castaño
	    Company:       Deloitte
	    Description:   Clase para recoger el token de auntentificación
	    
	    History:
	    
	    <Date>            	<Author>        							  	<Description>
	    10/12/2015        Jose María Martín Castaño     				 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static final String USER = Label.BIIN_USER_TOKEN;
    public static final String PASS = Label.BIIN_PASS_TOKEN;
    private transient HttpRequest reqToken;
    private transient HttpResponse resToken;
    
    public String obtencionToken() {
        tokenBAOSalidaError salidaTokenError = new tokenBAOSalidaError();
        tokenBAOSalidaOk salidaTokenOk = new tokenBAOSalidaOk();
        tokenBAOEntrada entradaToken = new tokenBAOEntrada();
        Http httpToken = new Http();
        String bodyToken;
        String authorizationToken;
        Boolean redirect = false;
        
        reqToken = new HttpRequest();
        //string urlToken = 'https://br1wbmcapvp01.tdatabrasil.net.br:38080/baocdp/rest/login';
        string urlToken = 'callout:BIIN_WS/login';
        reqToken.setEndpoint( urlToken );
        reqToken.setMethod('POST');
        reqToken.setHeader('Content-Type','application/json');
        
        entradaToken.username=USER;        
        //entradaToken.username='salesforce';
        entradaToken.password=PASS;
        //entradaToken.password='Salesforce';
        bodyToken = JSON.serialize(entradaToken);
        
        reqToken.setBody(bodyToken);
        
        try{//Realizamos la llamada para obtener el token
            resToken = httpToken.send(reqToken);
           	system.debug ( 'TOKEN. la llamada: ' +reqToken.toString() +' Con entrada: '+reqToken.getBody());
            if(resToken.getStatusCode() == 200 ) {//Hemos obtenido el token
                authorizationToken = resToken.getHeader('Authentication-Token');
                system.debug ( 'TOKEN-SUCCESS. El token es: ' +authorizationToken );
            }else{//No hemos obtenido el token
                salidaTokenError = (tokenBAOSalidaError)JSON.deserialize(resToken.getBody(), tokenBAOSalidaError.class);
            	system.debug ( 'TOKEN-FAIL: ' +salidaTokenError.errorMessage);
            }
        }catch( System.Exception e) {//Ha sucedido un error a la hora de llamar al servicio de obtención del token
            authorizationToken=null;
            system.debug ( 'TOKEN-ERROR: ' +e );
        }
        return authorizationToken;
    }

    public class tokenBAOSalidaOk {
        public String login;
    }
    
    public class tokenBAOSalidaError {
        public String errorNumber;
        public String errorMessage;
        public String remediation;
    }
    
    public class tokenBAOEntrada {
        public String username;
        public String password;
    }
}