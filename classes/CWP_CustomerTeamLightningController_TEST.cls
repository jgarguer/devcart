/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis 
    Company:       Everis
    Description:   Test Methods executed to test CWP_CustomerTeamLightningController.cls 
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    15/03/2016              Everis                   Initial Version
    
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class CWP_CustomerTeamLightningController_TEST {
     @testSetup 
    private static void dataModelSetup() {          
       
        //USER
        UserRole rolUsuario;
        User unUsuario;
        User otroUsuario;
        Profile profileTGS;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            profileTGS = CWP_TestDataFactory.getProfile('TGS System Administrator');
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, profileTGS.Id);
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
            //ACCOUNT
            map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(new User(id=userInfo.getUserId())){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
        
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        Contact contactTest1 = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest1');
        
        contactTest.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest;
        contactTest1.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest1;
        
        User usuario;
        
        
         //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 't1';
        producto.TGS_CWP_Tier_2__c = 't2';
        insert producto;
        list<NE__Catalog_Item__c> ciList = new list<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
        ciList.add(catalogoItem);
        
        NE__Catalog_Item__c catalogoItem1 = CWP_TestDataFactory.createCatalogoItem('ProductoTB', catalogCategoryChildren.id, catalogo.id, producto.id);
        catalogoItem1.NE__Technical_Behaviour__c = 'Service with pre-approved changes';
        ciList.add(catalogoItem1);
        insert ciList;
        
        NE__Contract_Header__c newCH = new NE__Contract_Header__c(
            NE__name__c = 'theContract'
        );
        insert newCH;
        
        NE__Contract_Account_Association__c newCAA = new NE__Contract_Account_Association__c(
            NE__Contract_Header__c = newCH.id,
            NE__Account__c = accLegalEntity.id
        );
        insert newCAA;
        
        NE__Contract__c newC =new  NE__Contract__c(
            NE__Contract_Header__c = newCH.id
        );
        insert newC;
        
        NE__Contract_Line_Item__c newCII = new NE__Contract_Line_Item__c(
            NE__Commercial_Product__c = producto.id,
            NE__Contract__c = newC.id
        );
        insert newCII;
        
        
        
        
          //ORDER
        
        NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        insert testOrder;
        //createOrderItem(id accountId, id orderId, id productId, id catalogItem, integer quantity){
        NE__OrderItem__c newOI;
        list<NE__OrderItem__c> oiList = new list<NE__OrderItem__c>();
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
        oiList.add(newOI);
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[1].id, 1);
        newOI.NE__City__c = 'Gondor';
        newOI.NE__city__c = 'Middle-earth';
        oiList.add(newOI);
        insert oiList;
        
        
        //CASE
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        insert newCase;       
        
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');       
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        
         
        CaseComment Comment = new CaseComment(ParentId = newCase.Id, CommentBody = 'new comment');
        insert Comment;
        
         Market_Place_PP__c newMarketplace = new Market_Place_PP__c();
        insert newMarketplace;
     
     }
    
    @isTest static void getIdentTest() {  
        system.debug('test 1');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.getIdent();
            Test.stopTest();
        }       
               
    }
    
    @isTest static void getBIENUserTest() {  
        system.debug('test 2');
        BI_TestUtils.throw_exception=false;  
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.getBIENUser();
            Test.stopTest();
        }       
               
    }
    
    @isTest static void getHeadersTest() {  
        system.debug('test 3');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
           
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.getHeaders();
            Test.stopTest();
        }       
               
    }
    
     @isTest static void getRecordsTest() {  
        system.debug('test 4');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.getRecords('','');
            Test.stopTest();
        }       
               
    }
    
    @isTest static void getPaginationTest() {  
        system.debug('test 5');
         
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.getPagination();
            Test.stopTest();
        }       
               
    }
    
    
    @isTest static void defineRecordsTest() {  
        system.debug('test 6');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.defineRecords();
            Test.stopTest();
        }       
               
    }
   
    
    @isTest static void getNextTest() {  
        system.debug('test 7');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest(); 
            CWP_CustomerTeamLightningController.index = 1; 
             CWP_CustomerTeamLightningController.pageSize = 5;                     
            CWP_CustomerTeamLightningController.getNext(0, 2, 5);
            Test.stopTest();
        }       
               
    }
    
    @isTest static void getBackTest() {  
        system.debug('test 8');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();      
            CWP_CustomerTeamLightningController.index = 5;   
            CWP_CustomerTeamLightningController.pageSize = 5;               
            CWP_CustomerTeamLightningController.getBack(2, 1, 5);
            Test.stopTest();
        }       
               
    }
    
     @isTest static void getAttrTest() {  
        system.debug('test 9');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.getAttr(0, 5, 1);
            Test.stopTest();
        }       
               
    }
    
    @isTest static void getHasNextTest() {  
        system.debug('test 10');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.getHasNext();
            Test.stopTest();
        }       
               
    }
    
    @isTest static void getHasBackTest() {  
        system.debug('test 11');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.getHasBack();
            Test.stopTest();
        }       
               
    }
    
     @isTest static void getIndiceTest() {  
        system.debug('test 12');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.getIndice();
            Test.stopTest();
        }       
               
    }
    
    @isTest static void getNumRecordsTest() {  
        system.debug('test 13');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1']; 
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.getNumRecords();
            Test.stopTest();
        }       
               
    }
    
    @isTest static void getPagesTest() {  
        system.debug('test 14');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();     
            CWP_CustomerTeamLightningController.index = 1;
             CWP_CustomerTeamLightningController.pageSize = 5;
             CWP_CustomerTeamLightningController.totalRegs = 5;                   
            CWP_CustomerTeamLightningController.getPages();
            Test.stopTest();
        }       
               
    }
    @isTest static void loadRecordTypesTest() {  
        system.debug('test 15');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.loadRecordTypes();
            Test.stopTest();
        }       
               
    }
    
    
     @isTest static void loadCCTest() {  
        system.debug('test 16');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.loadCC();
            Test.stopTest();
        }       
               
    }
    
     @isTest static void readFieldSetTest() {  
        system.debug('test 17');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.readFieldSet('PCA_MainTableContacts','Contact');
            Test.stopTest();
        }       
               
    }
    
    @isTest static void getVistaTest() {  
        system.debug('test 18');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.getVista(5);
            Test.stopTest();
        }       
               
    }
    
        
 @isTest static void getTGSUserTest() {  
        system.debug('test 19');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.getTGSUser();
            Test.stopTest();
        }       
               
    }
    
    
     @isTest static void setCaseTest() {  
        system.debug('test 20');
        BI_TestUtils.throw_exception=false; 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.setCase('Assigned', 'Assigned', 'subject', 'description', 'test value', 'EqTel');
            Test.stopTest();
        }       
               
    }
    
   
   @isTest static void defineSearchTest() {  
        system.debug('test 21');
        BI_TestUtils.throw_exception=false;
         List<String> reason= new List<String>{'Administrative Request', 'Attention Complaint', 'Billing Complaint' , 'Commercial Request' , 'Provision Complaint'};
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_CustomerTeamLightningController.defineSearch(reason);
            CWP_CustomerTeamLightningController.RecordsWrapper rWrap= 
                (CWP_CustomerTeamLightningController.RecordsWrapper)JSON.deserialize(CWP_CustomerTeamLightningController.getRecordsView(), CWP_CustomerTeamLightningController.RecordsWrapper.class);
            CWP_CustomerTeamLightningController.getNewTableController(JSON.serialize(rWrap));
            rWrap.paginationFlag='backward';
            CWP_CustomerTeamLightningController.getNewTableController(JSON.serialize(rWrap));
            rWrap.paginationFlag='10';
            CWP_CustomerTeamLightningController.getNewTableController(JSON.serialize(rWrap));
            Test.stopTest();
        }       
               
    }
    
    
}