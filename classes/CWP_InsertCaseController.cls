public class CWP_InsertCaseController{
    public string caseNumber{get; set;}
    public integer result{get; set;}
    public string tipoCaso{get; set;}
    public string operacion{get; set;}
    public string message{get; set;}
    public customAtt currentAtt {get;set;}
    private List<Attachment> attachments {get;set;}
    public map<string,id> wiMap{get;set;} 
    public boolean allToRoD {get;set;} 
    public boolean onlyAttach {get;set;}
    public Set<Id> casesIdSet{get;set;}
    public Set<Id> workInfosIdSet{get;set;} 
    public Integer attachmentSize{get;set;}
    public Integer attachmentInserted{get;set;}
    
    private boolean isForComment;
    private boolean isForAttachment;
    private final set<String> ticketDevelopernameSet = new set<string>{
        'TGS_Billing_Inquiry', 'TGS_Change', 'TGS_Complaint', 'TGS_Incident', 'TGS_Problem', 'TGS_Query'
            };
                private final map<string, string> successMessageMap = new map<string, string>{
                    'TGS_Incident' =>label.CWP_IncidentCreated,
                        'TGS_Change' => label.CWP_ModificationCreated,
                        'TGS_Billing_Inquiry' => label.CWP_BillingInsertSuccess,
                        'TGS_Query' =>label.CWP_QuerySuccess,
                        'TGS_Complaint' =>label.CWP_complaintSuccess,
                        'Order_Management_Case' =>label.CWP_RequestSuccess,
                        'CWP_OrderFromCWP' =>label.CWP_RequestSuccess,
                        Constants.CASE_STATUS_CLOSED=>Label.TGS_CWP_AcceptMessage,
                        Constants.CASE_STATUS_ASSIGNED=>Label.TGS_CWP_ReopenMessage,
                        Constants.CASE_STATUS_CANCELLED=>Label.CWP_TicketCancelled,
                        'comment'=>label.CWP_CommentInsertSuccess,
                        'attachment'=>label.CWP_AttachmentSuccess
                        
                        };
                            private final map<string, string> failMessageMap = new map<string, string>{
                                'TGS_Incident'=>label.CWP_IncidentError,
                                    'TGS_Change'=>label.CWP_ModificationError,
                                    'TGS_Billing_Inquiry'=>label.CWP_BillingInsertFail,
                                    'TGS_Query'=>label.CWP_QueryFail,
                                    'TGS_Complaint'=>label.CWP_ComplaintFail,
                                    'Order_Management_Case'=>label.CWP_RequestFail,
                                    'CWP_OrderFromCWP'=>label.CWP_RequestFail,
                                    'update'=>Label.CWP_UpdateCaseFail,
                                    'comment'=>label.CWP_CommentInsertFail,
                                    'attachment'=>label.CWP_AttachmentFail
                                    
                                    };
                                        /*private final set<String> orderDevelopernameSet = new set<string>{
'Order_Management_Case', 'CWP_OrderFromCWP'
};*/
                                        
                                        public CWP_InsertCaseController(){
                                            currentAtt = new customAtt();
                                            isForComment=false;
                                            isForAttachment=false;
                                            result=0; 
                                            operacion = 'none'; 
                                            message = '';
                                            string why = ApexPages.currentPage().getParameters().get('why');
                                            if(!string.isBlank(why) && 'comment'.equalsIgnoreCase(why)){
                                                isForComment=true;
                                            }
                                            if(!string.isBlank(why) && 'attachment'.equalsIgnoreCase(why)){
                                                isForAttachment=true;
                                            }
                                            allToRoD = false;
                                            onlyAttach = false;
                                            attachmentSize = 0;
                                            attachmentInserted = 0;
                                        }
    
    public void insertCase(){
        result = 0;
        /*Case caseToInsert= (Case)Cache.Session.get('local.CWPexcelExport.caseToInsert');
String caseId = (String)Cache.Session.get('local.CWPexcelExport.caseId');
String theComment= (String)Cache.Session.get('local.CWPexcelExport.newComment');*/
        
        
        //Case caseToInsert= (Case)ApexPages.currentPage().getParameters().get('local.CWPexcelExport.caseToInsert');
        Case caseToInsert;
        String caseId = (String)ApexPages.currentPage().getParameters().get('cancelticketid');
        String casestatus = (String)ApexPages.currentPage().getParameters().get('statusticketid');
        String attachmentListB64 = (String)ApexPages.currentPage().getParameters().get('attachmentListB64');
        List<Attachment> attachmentList;
        system.debug('caseId: ' + caseId);
        system.debug('statusticketid: ' + casestatus);
        system.debug('attachmentListB64: ' + attachmentListB64);
        
        if(caseId!=null && caseId != ''){
            caseToInsert= [SELECT Id, caseNumber, RecordType.DeveloperName FROM Case WHERE id =: caseId];
            if(casestatus!=null && casestatus != ''){
                caseToInsert.Status=casestatus;               
            }
        }else if((caseId ==null || caseId == '') && (casestatus!=null && casestatus != '')){
            caseToInsert = (Case)JSON.deserialize(casestatus, Case.class);
        }
        
        if(attachmentListB64!=null && attachmentListB64 != ''){
            system.debug('deserializando attachmentList: '+attachmentListB64);
            attachmentList = (List<Attachment>) JSON.deserialize(attachmentListB64, List<Attachment>.class);
            attachmentSize = attachmentList.size();
        }
        
        String theComment= (String)ApexPages.currentPage().getParameters().get('comment');
        /*= (List<Attachment>)Cache.Session.get('local.CWPexcelExport.attList');*/
        system.debug('lista de adjuntos: ' + attachmentList);
        
        if(caseId!=null && isForAttachment){
            if(attachmentList != null && !attachmentList.isEmpty()){
                onlyAttach = true;
                operacion='attachment';
                try{
                    result = 1;
                    crearWorkInfo(caseId, attachmentList);
                    //message = label.CWP_AttachmentSuccess;
                    message = successMessageMap.get(operacion);
                }catch(exception e){
                    result = -1;
                    //message = label.CWP_AttachmentFail;
                    message = failMessageMap.get(operacion);
                }
                system.debug('mensaje    ' + message);
                system.debug('operacion    ' + operacion);
                
            }
        }else if((caseToInsert == null || (caseToInsert!=null && caseToInsert.id==null)) && !isForComment){
            system.debug('caso de la inserción/creacion de workInfo');
            operacion = 'insert';
            try{
                result=1;
                system.debug('Se inserta el CASE');
                // Reglas de asignación estándar para Reclamación de facturación y Queja
                CWP_Assignment_Rule classAssignmentRuleList = new CWP_Assignment_Rule();
                AssignmentRule AssignmentRuleList = classAssignmentRuleList.assignmentRule();
                Database.DMLOptions dmlOpts = new Database.DMLOptions();
                dmlOpts.assignmentRuleHeader.useDefaultRule= true;
                caseToInsert.setOptions(dmlOpts);
                // TGS_CallRodWs.inFutureContext= true; hace que no haga la integración con RoD desde el trigger de caso. 
                // Esto será necesario únicamente en los casos en que se quiera insertar el caso y el adjunto a la vez, que deberemos hacer la integración con RoD de forma secuencial
                if(attachmentList != null && !attachmentList.isEmpty()){
                    TGS_CallRodWs.inFutureContext= true;
                    allToRoD = true;
                }
                insert caseToInsert;
                if(attachmentList != null && !attachmentList.isEmpty()){
                    TGS_CallRodWs.inFutureContext= false;
                }
                system.debug('Se busca el CASE');
                Case theCase = [SELECT Id, caseNumber, RecordType.DeveloperName FROM Case WHERE id =: caseToInsert.id]; 
                caseNumber = theCase.caseNumber;
                if(ticketDevelopernameSet.contains(theCase.RecordType.DeveloperName)){
                    tipoCaso = 'ticket';
                }else{
                    tipoCaso = 'order';
                }
                system.debug('Se procede a actualizar los adjuntos');
                /* Inserto Adjuntos */
                if(attachmentList != null && !attachmentList.isEmpty()){
                    crearWorkInfo(theCase.Id, attachmentList);
                }
                message = successMessageMap.get(theCase.RecordType.DeveloperName);
                message +=' '+ theCase.caseNumber;
            }catch(exception e){
                result=-1;
                caseNumber = '' ;
                String thisRecordType =[SELECT Id, DeveloperName FROM RecordType WHERE Id=: caseToInsert.RecordTypeId].DeveloperName; 
                message = failMessageMap.get(thisRecordType);
            }
        }else if((caseToInsert != null && caseToInsert.id!=null) && !isForComment){
            operacion = 'update';
            system.debug('caso de la actualizacion');
            try{
                update caseToInsert;
                /*if(caseToInsert.status ==Constants.CASE_STATUS_CLOSED){
message = Label.TGS_CWP_AcceptMessage;
}
if(caseToInsert.status == Constants.CASE_STATUS_ASSIGNED){
message = Label.TGS_CWP_ReopenMessage;
}
if(caseToInsert.status==Constants.CASE_STATUS_CANCELLED){
message = Label.CWP_TicketCancelled;
}*/
                message = successMessageMap.get(caseToInsert.status);
                result = 1;
            }catch(exception e){
                system.debug('fallo al actualizar el caso    ' +caseToInsert);
                result = -1;
                message = failMessageMap.geT(operacion);
            }
        }else if(isForComment){
            operacion ='comment';
            TGS_Work_Info__c theNewComment = new TGS_Work_Info__c(
                TGS_Case__c =caseToInsert.id,
                TGS_Description__c=theComment,
                TGS_Public__c = true);
            system.debug('creacion de nuevo comentario    '+ theNewComment);
            try{
                insert theNewComment;
                message = successMessageMap.get(operacion);
                //message = 'you comment has been saved';
                result = 1;
            }catch(exception e){
                result = -1;
                //message = 'there has been a problem and your comment could not be saved';
                message = failMessageMap.get(operacion);
                system.debug('fallo al crear nuevo comentario    ' +theNewComment);
            }
        }
    }
    
    /**
* Method:         crearWorkinfo
* Param:          id es el identificador del objeto creado
* Description:    Crea un SObject TGS_WorkInfo__c relacionado con el Case creado y lo inserta en la base de datos.
*                 También inserta todos los Attachment que se encuentran 'listAttachments' y que se relacionan
*                 mediante ParentId con el SObject TGS_WorkInfo__c creado.
*/
    public void crearWorkinfo(Id id, List<Attachment> attachmentList){
        system.debug('crearWorkinfo: allToRod : ' + allToRoD + ' onlyAttach: ' + onlyAttach);
        Attachment newAttachmentLocal;
        list<TGS_Work_Info__c> wfList = new list<TGS_Work_Info__c>();        
        wiMap = new Map<string,id>();        
        
        for(Attachment ad:attachmentList){
            String description = ad.Name;
            if(ad.Description != null && ad.Description != ''){
                description = EncodingUtil.urlDecode(ad.Description,'UTF-8');
            }
            wfList.add(new TGS_Work_Info__c(
                TGS_Description__c = description,
                TGS_Case__c = id,                   
                TGS_Public__c = true,
                TGS_Contains_attachments__c = true
            ));
        }
        
        // allToRoD == true => Debo hacer la integración con RoD del caso y el workInfo de forma secuencial
        // onlyAttach == true => Debo hacer la ingetración con RoD del WI y el attachment a la vez
        if(allToRoD || onlyAttach){
            TGS_CallRodWs.inFutureContextWI = true;
        }
        insert wfList;
        if(allToRoD || onlyAttach){
            TGS_CallRodWs.inFutureContextWI = false;
            casesIdSet = new Set<Id>();
            casesIdSet.add(id);
            workInfosIdSet = new Set<Id>();
            for(TGS_Work_Info__c wi : wfList){
                workInfosIdSet.add(wi.Id);
            }
        }
        system.debug('workInfosIdSet: ' + workInfosIdSet);
        integer wiCont = 0;
        String regExp = '[^a-zA-Z0-9]';
        List<String> attNameWOPoints = new List<String>();
        String attNameFormated ;
        for(Attachment ad:attachmentList){
            attNameWOPoints = ad.name.split('\\.');
            attNameFormated = attNameWOPoints.get(0).replaceAll(regExp, '') + '.' + attNameWOPoints.get(1);
            system.debug('NameTransformed:'+attNameFormated);
            wiMap.put(attNameFormated,wfList[wiCont].Id);
            wiCont ++;          
        }        
        
        /*
system.debug('se ha creado un nuevo workInfo para crearle los adjuntos  ' + wfList);
list<Attachment> listToInsert = new list<Attachment>();

//Map<String,List<String>> cachedStringsMap=(Map<String,List<String>>) Cache.Session.get('local.CWPexcelExport.adjuntosMap');
List<String> cachedStringsList=(List<String>) Cache.Session.get('local.CWPexcelExport.adjuntosList');
//system.debug('cachedStringsMap  ' + cachedStringsMap);
system.debug('cachedStringsList  ' + cachedStringsList);
List<String> nameStrings;
integer cont = 0;

for (Attachment ad:attachmentList ){
newAttachmentLocal = new Attachment();
newAttachmentLocal.Name = ad.Name;
newAttachmentLocal.Description = ad.Description;
//newAttachment.Body = ad.Body;
string name=ad.Name.left(ad.Name.indexOf('.'));
//nameStrings=cachedStringsMap.get(name);

//nameStrings=cachedStringsList.get(name);            
//String attachmenStringBody='';
//for (String s : nameStrings) {
//attachmenStringBody+= (String) Cache.Session.get('local.CWPexcelExport.'+s);
//}
//system.debug('attachmenStringBody - PRE: '+ attachmenStringBody);
//newAttachmentLocal.Body = EncodingUtil.base64Decode(attachmenStringBody);
//system.debug('attachmenStringBody base64 DECODE - POST: '+ newAttachmentLocal.Body);

newAttachmentLocal.OwnerId = UserInfo.getUserId();
newAttachmentLocal.ParentId = wfList[cont].Id;
cont++;
listToInsert.add(newAttachmentLocal);
}
try {
if (listToInsert.size() > 0) {
TGS_CallRoDWs.inFutureContextAttachment = true;
Database.SaveResult[] results = Database.insert(listToInsert);
TGS_CallRoDWs.inFutureContextAttachment = false;
attachmentList.clear();
//attachListToShow.clear();
//cachedStringsMap=new Map<String,List<String>>();
cachedStringsList =new List<String>();
//Cache.Session.put('local.CWPexcelExport.adjuntosMap',cachedStringsMap);
Cache.Session.put('local.CWPexcelExport.adjuntosList',cachedStringsList);
}
} catch (DmlException e) {
System.debug('Error al subir los adjuntos del workinfo del case '+id + ': ' + e);
}
*/
        
    }
    /*AddAttachments to list (Direct DML due to ViewState*/
    public void addAttachment(){
        system.debug('parentId'+wiMap);
        system.debug('name'+currentAtt.name);
        String regExp = '[^a-zA-Z0-9]';
        List<String> attNameWOPoints = currentAtt.name.split('\\.');
        String attNameFormated = attNameWOPoints.get(0).replaceAll(regExp, '') + '.' + attNameWOPoints.get(1);
        try{
            if(allToRoD || onlyAttach){
                TGS_CallRoDWs.inFutureContextAttachment = true;
            }
            insert(
                new Attachment (
                    Name=currentAtt.name,
                    ParentId=wiMap.get(attNameFormated),
                    body = EncodingUtil.Base64Decode(currentAtt.body),
                    ContentType = currentAtt.contentType
                )
            );
            if(allToRoD || onlyAttach){
                TGS_CallRoDWs.inFutureContextAttachment = false;
            }
            /*attachmentInserted = attachmentInserted +1;
            system.debug('attachmentInserted: ' + attachmentInserted + ' de: ' + attachmentSize);
            if(attachmentInserted == attachmentSize){
                integraRoD();
            }*/
            currentAtt = new customAtt();
        }catch(exception e){
            result = -1;
        }
    }
    
    public void integraRoD(){
        if(allToRoD && !onlyAttach){
            system.debug('tengo adjuntos: llamo integración RoD síncrona');
            CWP_CallRoDWSFromCWP.invokeWebServiceRODFromCWP(casesIdSet, null, null, false);
            system.debug('llamada síncrona finalizada. llamo @ future');
            CWP_CallRoDWSFromCWP.invokeRoDWSFromCWP(workInfosIdSet);
        }else if(onlyAttach && !allToRoD){
            system.debug('lanzo future para integrar workinfos con sus adjuntos: ' + workInfosIdSet);
            CWP_CallRoDWSFromCWP.invokeRoDWSFromCWP(workInfosIdSet);
        }
            
    }
    public void insertAttachments(){
        
    }
    public class customAtt {
        public string name {get;set;}
        public string body {get;set;}
        public string description {get;set;}
        public string contentType {get;set;}
    }
}