@isTest
private class BI_ClientesIndicadores_JOB_TEST {
	
    @isTest
    public static void createFeedItem(){
        
        CollaborationGroup test = new CollaborationGroup( Name = 'chatterGroup', CollaborationType = 'Public');
        insert test;
        
        String msg = 'Message with a report Id #informe#';
        
        Map<String, String> map_group_message = new Map<String, String>();
        map_group_message.put(test.Id, '0000001');
        
        BI_ClientesIndicadores_JOB testJob = new BI_ClientesIndicadores_JOB(map_group_message, '00O26000000QrJnEAK'); //Schedule apex class name
        testJob.execute(null);

    }
}