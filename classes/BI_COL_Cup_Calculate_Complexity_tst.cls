/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           		Descripción
*           -----   ----------      --------------------            		---------------
* @version   1.0    2015-07-24      Manuel Esthiben Mendez Devia (MEMD)     Create Test Class
*            1.1    2016-12-27      Gawron, Julian                          Add @testSetup to async use
                    20/09/2017      Angel F. Santaliestra                   Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
***********************************************************************************************************/
@isTest
private class BI_COL_Cup_Calculate_Complexity_tst
{

	Public static list <UserRole>                 lstRoles;
    Public static User                            objUsuario;
    public static BI_COL_PreFactibilidad__c objPrefactibilidad;
	public static BI_COL_MatrizP__c lstConfMatrizP;
	public static Account  objCuenta;
	public static Opportunity objOpp;


	@testSetup public static void crearData()
	{
	   list <Profile>  lstPerfil                      = new list <Profile>();
        lstPerfil                       = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
        system.debug('datos Perfil '+lstPerfil);
                
        //usuarios
       //List<User> lstUsuarios = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

       //lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);
        System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
            insert objUsuario;
        }
        
        System.runAs(objUsuario)
        {   

			objCuenta 										= new Account();
	        objCuenta.Name 									= 'prueba';
	        objCuenta.BI_Country__c 						= 'Colombia';
	        objCuenta.TGS_Region__c 						= 'América';
	        objCuenta.BI_Tipo_de_identificador_fiscal__c 	= 'NIT';
	        objCuenta.CurrencyIsoCode 						= 'GTQ';
	        objCuenta.BI_No_Identificador_fiscal__c='800789456';
            objCuenta.BI_Segment__c                         = 'test';
            objCuenta.BI_Subsegment_Regional__c             = 'test';
            objCuenta.BI_Territory__c                       = 'test';
	        insert objCuenta;

	        objOpp 											= new Opportunity();
	        objOpp.Name 									= 'prueba opp';
	        objOpp.AccountId 	 							= objCuenta.Id;
	        objOpp.BI_Country__c 							= 'Colombia';
	        objOpp.CloseDate 						 		= System.today().addDays(+5);
	        objOpp.StageName 								= 'F6 - Prospecting';
	        objOpp.CurrencyIsoCode							= 'GTQ';
	        objOpp.Certa_SCP__contract_duration_months__c 	= 12;
	        objOpp.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
	        //objOpp.BI_COL_Autoconsumo__c 					= true;
	        insert objOpp;

	        objPrefactibilidad									= new BI_COL_PreFactibilidad__c();
	        objPrefactibilidad.BI_COL_Proyecto__c				= objOpp.Id;
	        objPrefactibilidad.BI_COL_Descripcion_negocio__c	= 'PruebaTest';
	        objPrefactibilidad.BI_COL_Fecha_solicitud__c		= Date.newInstance(2015, 07, 30);
	        objPrefactibilidad.BI_COL_Probabilidad_cierre__c	='50%';
	        objPrefactibilidad.BI_COL_Monto_contrato_12_meses__c ='Menos de $300 M';
	        objPrefactibilidad.BI_COL_Estado_oferta__c			='Elaboración';
	        objPrefactibilidad.BI_COL_Full_Outsourcing__c  = '1';
	        objPrefactibilidad.BI_COL_Hosting__c          = '1';
	        objPrefactibilidad.BI_COL_Housing__c          = '1';   
	        objPrefactibilidad.BI_COL_Servicio_HelpDesk__c = '1';            
	        insert objPrefactibilidad;
        }

	}

	static testmethod void myTestMethod()
	{

		//crearData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_Cup_Calculate_Complexity_pag;
        Test.setCurrentPage(pageRef);
        objPrefactibilidad = [Select Id from BI_COL_PreFactibilidad__c limit 1];
        ApexPages.currentPage().getParameters().put('idprecomp',objPrefactibilidad.Id);
        BI_COL_Cup_Calculate_Complexity_ctr cup_calculate = new BI_COL_Cup_Calculate_Complexity_ctr();
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Full_Outsourcing__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 150;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosIT';
        lstConfMatrizP.ValorLista__c = '1';
        
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosIT';
        lstConfMatrizP.ValorLista__c = '2';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


		lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosIT';
        lstConfMatrizP.ValorLista__c = '4';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


         lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosIT';
        lstConfMatrizP.ValorLista__c = null;  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        
        System.debug('cup_calculate.lstConfMatrizP-->'+cup_calculate.lstConfMatrizP);
        
        cup_calculate.calcularComplejidadyPrefactibilidad();
        Test.stopTest();

	}


	static testmethod void myTestMethod2()
	{

		//crearData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_Cup_Calculate_Complexity_pag;
        Test.setCurrentPage(pageRef);
        objPrefactibilidad = [Select Id from BI_COL_PreFactibilidad__c limit 1];
        ApexPages.currentPage().getParameters().put('idprecomp',objPrefactibilidad.Id);
        BI_COL_Cup_Calculate_Complexity_ctr cup_calculate = new BI_COL_Cup_Calculate_Complexity_ctr();
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Full_Outsourcing__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 150;
        lstConfMatrizP.PuntoACalcular__c = 'Puntos';
        lstConfMatrizP.ValorLista__c = '1';
        
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'Puntos';
        lstConfMatrizP.ValorLista__c = '2';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


		lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'Puntos';
        lstConfMatrizP.ValorLista__c = '4';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


         lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'Puntos';
        lstConfMatrizP.ValorLista__c = null;  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        System.debug('cup_calculate.lstConfMatrizP-->'+cup_calculate.lstConfMatrizP);
        
        cup_calculate.calcularComplejidadyPrefactibilidad();
        Test.stopTest();

	}


	static testmethod void myTestMethod3()
	{

		//crearData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_Cup_Calculate_Complexity_pag;
        Test.setCurrentPage(pageRef);
        objPrefactibilidad = [Select Id from BI_COL_PreFactibilidad__c limit 1];
        ApexPages.currentPage().getParameters().put('idprecomp',objPrefactibilidad.Id);
        BI_COL_Cup_Calculate_Complexity_ctr cup_calculate = new BI_COL_Cup_Calculate_Complexity_ctr();
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Full_Outsourcing__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 150;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosUC';
        lstConfMatrizP.ValorLista__c = '1';
        
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosUC';
        lstConfMatrizP.ValorLista__c = '2';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


		lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosUC';
        lstConfMatrizP.ValorLista__c = '4';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


         lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosUC';
        lstConfMatrizP.ValorLista__c = null;  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        System.debug('cup_calculate.lstConfMatrizP-->'+cup_calculate.lstConfMatrizP);
        
        cup_calculate.calcularComplejidadyPrefactibilidad();
        Test.stopTest();

	}


	static testmethod void myTestMethod4()
	{

		//crearData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_Cup_Calculate_Complexity_pag;
        Test.setCurrentPage(pageRef);
        objPrefactibilidad = [Select Id from BI_COL_PreFactibilidad__c limit 1];
        ApexPages.currentPage().getParameters().put('idprecomp',objPrefactibilidad.Id);
        BI_COL_Cup_Calculate_Complexity_ctr cup_calculate = new BI_COL_Cup_Calculate_Complexity_ctr();
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Full_Outsourcing__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 150;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosMovil';
        lstConfMatrizP.ValorLista__c = '1';
        
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosMovil';
        lstConfMatrizP.ValorLista__c = '2';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                

		lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosMovil';
        lstConfMatrizP.ValorLista__c = '4';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


         lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = true;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosMovil';
        lstConfMatrizP.ValorLista__c = null;  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        System.debug('cup_calculate.lstConfMatrizP-->'+cup_calculate.lstConfMatrizP);
        
        cup_calculate.calcularComplejidadyPrefactibilidad();
        Test.stopTest();

	}

	static testmethod void myTestMethod5()
	{
		//crearData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_Cup_Calculate_Complexity_pag;
        Test.setCurrentPage(pageRef);
        objPrefactibilidad = [Select Id from BI_COL_PreFactibilidad__c limit 1];
        ApexPages.currentPage().getParameters().put('idprecomp',objPrefactibilidad.Id);
        BI_COL_Cup_Calculate_Complexity_ctr cup_calculate = new BI_COL_Cup_Calculate_Complexity_ctr();
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Full_Outsourcing__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 150;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosIT';
        lstConfMatrizP.ValorLista__c = '1';
        
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosIT';
        lstConfMatrizP.ValorLista__c = '2';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


		lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosIT';
        lstConfMatrizP.ValorLista__c = '4';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


         lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosIT';
        lstConfMatrizP.ValorLista__c = null;  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        
        System.debug('cup_calculate.lstConfMatrizP-->'+cup_calculate.lstConfMatrizP);
        
        cup_calculate.calcularComplejidadyPrefactibilidad();
        Test.stopTest();

	}


	static testmethod void myTestMethod6()
	{

		//crearData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_Cup_Calculate_Complexity_pag;
        Test.setCurrentPage(pageRef);
        objPrefactibilidad = [Select Id from BI_COL_PreFactibilidad__c limit 1];
        ApexPages.currentPage().getParameters().put('idprecomp',objPrefactibilidad.Id);
        BI_COL_Cup_Calculate_Complexity_ctr cup_calculate = new BI_COL_Cup_Calculate_Complexity_ctr();
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Full_Outsourcing__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 150;
        lstConfMatrizP.PuntoACalcular__c = 'Puntos';
        lstConfMatrizP.ValorLista__c = '1';
        
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'Puntos';
        lstConfMatrizP.ValorLista__c = '2';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


		lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'Puntos';
        lstConfMatrizP.ValorLista__c = '4';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


         lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'Puntos';
        lstConfMatrizP.ValorLista__c = null;  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        System.debug('cup_calculate.lstConfMatrizP-->'+cup_calculate.lstConfMatrizP);
        
        cup_calculate.calcularComplejidadyPrefactibilidad();
        Test.stopTest();

	}


	static testmethod void myTestMethod7()
	{

		//crearData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_Cup_Calculate_Complexity_pag;
        Test.setCurrentPage(pageRef);
        objPrefactibilidad = [Select Id from BI_COL_PreFactibilidad__c limit 1];
        ApexPages.currentPage().getParameters().put('idprecomp',objPrefactibilidad.Id);
        BI_COL_Cup_Calculate_Complexity_ctr cup_calculate = new BI_COL_Cup_Calculate_Complexity_ctr();
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Full_Outsourcing__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 150;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosUC';
        lstConfMatrizP.ValorLista__c = '1';
        
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosUC';
        lstConfMatrizP.ValorLista__c = '2';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


		lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosUC';
        lstConfMatrizP.ValorLista__c = '4';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                


         lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosUC';
        lstConfMatrizP.ValorLista__c = null;  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        System.debug('cup_calculate.lstConfMatrizP-->'+cup_calculate.lstConfMatrizP);
        
        cup_calculate.calcularComplejidadyPrefactibilidad();
        Test.stopTest();

	}


	static testmethod void myTestMethod8()
	{

		//crearData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_Cup_Calculate_Complexity_pag;
        Test.setCurrentPage(pageRef);
        objPrefactibilidad = [Select Id from BI_COL_PreFactibilidad__c limit 1];
        ApexPages.currentPage().getParameters().put('idprecomp',objPrefactibilidad.Id);
        BI_COL_Cup_Calculate_Complexity_ctr cup_calculate = new BI_COL_Cup_Calculate_Complexity_ctr();
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Full_Outsourcing__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 150;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosMovil';
        lstConfMatrizP.ValorLista__c = '1';
        
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        
        lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosMovil';
        lstConfMatrizP.ValorLista__c = '2';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
                

		lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosMovil';
        lstConfMatrizP.ValorLista__c = '4';  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);

         lstConfMatrizP = new BI_COL_MatrizP__c();
        
        lstConfMatrizP.CalcularPref__c = false;
        lstConfMatrizP.Campo__c = 'BI_COL_Servicio_HelpDesk__c';
        lstConfMatrizP.FactorComplejidad__c = 1;
        lstConfMatrizP.PuntajePrefactibilidad__c = 120;
        lstConfMatrizP.PuntoACalcular__c = 'PuntosMovil';
        lstConfMatrizP.ValorLista__c = null;  
             
        cup_calculate.lstConfMatrizP.add(lstConfMatrizP);
        
        
        System.debug('cup_calculate.lstConfMatrizP-->'+cup_calculate.lstConfMatrizP);
        
        cup_calculate.calcularComplejidadyPrefactibilidad();
        Test.stopTest();

	}

	static testmethod void myTestMethod9()
	{

		//crearData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_Cup_Calculate_Complexity_pag;
        Test.setCurrentPage(pageRef);
        BI_COL_Cup_Calculate_Complexity_ctr cup_calculate = new BI_COL_Cup_Calculate_Complexity_ctr();
        Test.stopTest();

	}

}