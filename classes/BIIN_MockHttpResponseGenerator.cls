@isTest

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Jose María Martín Castaño
	    Company:       Deloitte
	    Description:   Generador de Mock para Clases test BIIN
	    
	    History:
	    
	    <Date>            	<Author>        							  	<Description>
	    10/12/2015       Jose María Martín Castaño			    		 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

global class BIIN_MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        String condicion=req.getHeader('Authentication-Token');
        system.debug('condicion: '+condicion);
        system.debug('req '+req);
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        if(condicion=='OBTENER_ANEXO_1'){
        	res.setBody('[{"name":"str_json","value":"<value>{\\"outputs\\" : {\\"output\\":[ {\\"Incident Number\\" : \\"INC000000025522\\",\\"z2AF Work Log01\\" : \\"iVBORw0KGg\\"}]}}</value>"}]');
        }else if(condicion=='OBTENER_ANEXO_2'){
        	res.setBody('[{"name":"str_json","value":"<value>{\\"outputs\\" : {\\"output\\":[ {\\"Incident Number\\" : \\"INC000000025522\\",\\"z2AF Work Log02\\" : \\"iVBORw0KGg\\"}]}}</value>"}]');
        }else if(condicion=='OBTENER_ANEXO_3'){
        	res.setBody('[{"name":"str_json","value":"<value>{\\"outputs\\" : {\\"output\\":[ {\\"Incident Number\\" : \\"INC000000025522\\",\\"z2AF Work Log03\\" : \\"iVBORw0KGg\\"}]}}</value>"}]');
        }else if(condicion=='OBTENER_TICKET'){
        	res.setBody('[{"name":"str_json","value":"<var2 type=\\"scalar\\">{\\"outputs\\":{\\"output\\":[{\\"Incident Number\\" : \\"INC000000022352\\",\\"First Name\\" : \\"Nombre-Vacio\\",\\"Last Name\\" : \\"Apellidos-Vacio\\",\\"Internet E-mail\\" : \\"email-Vacio@acme.com\\",\\"Status\\" : \\"6\\",\\"Status_Reason\\" : \\"\\",\\"Service Type\\" : \\"0\\",\\"Impact\\" : \\"2000\\",\\"Urgency\\" : \\"3000\\",\\"Priority\\" : \\"2\\",\\"Submit Date\\" : \\"1448464345\\",\\"Last Modified Date\\" : \\"1448465457\\",\\"Last Resolved Date\\" : \\"1448465455\\",\\"Closed Date\\" : \\"1448465455\\",\\"SLM Status\\" : \\"1\\",\\"Description\\" : \\"Subject 25/11 16:11\\",\\"Reported Source\\" : \\"2000\\",\\"HPD_CI\\" : \\"\\",\\"HPD_CI_ReconID\\" : \\"\\",\\"HPD_CI_FormName\\" : \\"\\",\\"Detailed Decription\\" : \\"desc\\",\\"Direct Contact Company\\" : \\"\\",\\"Direct Contact First Name\\" : \\"\\",\\"Direct Contact Middle Initial\\" : \\"\\",\\"Direct Contact Last Name\\" : \\"\\",\\"Direct Contact Internet E-mail\\" : \\"\\",\\"Product Categorization Tier 1\\" : \\"\\",\\"Product Categorization Tier 2\\" : \\"\\",\\"Product Categorization Tier 3\\" : \\"\\",\\"Resolution\\" : \\"\\",\\"Resolution Category\\" : \\"\\",\\"Resolution Category Tier 2\\" : \\"\\",\\"Resolution Category Tier 3\\" : \\"\\",\\"Company\\" : \\"ACME\\",\\"Site\\" : \\"Lima ACME\\",\\"Categorization Tier 1\\" : \\"\\",\\"Categorization Tier 2\\" : \\"\\",\\"Categorization Tier 3\\" : \\"\\",\\"Reported Date\\" : \\"1448464345\\",\\"Assigned Group\\" : \\"ACME GROUP 1\\",\\"BAO_CaseNumberID\\" : \\"00456516\\",\\"BAO_ParentID\\" : \\"\\",\\"Company ID\\" : \\"PER_RUC_12121212122\\",\\"worklogs\\":[{\\"Work Log ID\\" : \\"WLG000000023973\\",\\"Detailed Description\\" : \\"\\",\\"Work Log Type\\" : \\"8000\\",\\"Work Log Date\\" : \\"1448465457\\",\\"z2AF Work Log01\\" : \\"\\",\\"z2AF Work Log02\\" : \\"\\",\\"z2AF Work Log03\\" : \\"\\",\\"View Access\\" : \\"1\\",\\"Secure Work Log\\" : \\"0\\"},{\\"Work Log ID\\" : \\"WLG000000023972\\",\\"Detailed Description\\" : \\"Status has been changed to Cancelled\\",\\"Work Log Type\\" : \\"8000\\",\\"Work Log Date\\" : \\"1448465455\\",\\"z2AF Work Log01\\" : \\"\\",\\"z2AF Work Log02\\" : \\"\\",\\"z2AF Work Log03\\" : \\"\\",\\"View Access\\" : \\"0\\",\\"Secure Work Log\\" : \\"0\\"},{\\"Work Log ID\\" : \\"WLG000000023965\\",\\"Detailed Description\\" : \\"Nota sobre los tres adjuntos que se han incluido en el WI\\",\\"Work Log Type\\" : \\"8000\\",\\"Work Log Date\\" : \\"1448464585\\",\\"z2AF Work Log01\\" : \\"Attach_Prueba_1.txt\\",\\"z2AF Work Log02\\" : \\"Attach_Prueba_2.txt\\",\\"z2AF Work Log03\\" : \\"Adjunto3.txt\\",\\"View Access\\" : \\"1\\",\\"Secure Work Log\\" : \\"0\\"}],\\"slas\\":[{\\"SVTTitle\\" : \\"SLA_BAO_TNA\\",\\"SVTDueDate\\" : \\"1449184345\\",\\"MeasurementStatus\\" : \\"4\\",\\"GoalCategoryChar\\" : \\"Incident Response Time\\",\\"GoalTimeHr\\" : \\"200.00\\",\\"GoalTimeMin\\" : \\"0.00\\",\\"UpTime\\" : \\"1110.00\\",\\"MetMissedAmount\\" : \\"718890.00\\",\\"WarningDateTimefrmINT\\" : \\"1448824345\\"}]}]}}</var2>"}]');
        }else if(condicion=='CONSULTA_INCIDENCIA'){
            res.setBody('[{"name":"str_json","value":"<json>{\\"outputs\\":{\\"output\\":[{\\"IncidentNumber\\":\\"INC000000020273\\",\\"Company\\":\\"ACME\\",\\"Status\\":\\"4\\",\\"Description\\":\\"pruebas10\\",\\"ReportedDate\\":\\"1446652561\\",\\"Priority\\":\\"0\\",\\"LastModifiedDate\\":\\"1446653868\\",\\"SubmitDate\\":\\"1446652562\\",\\"LastName\\":\\"Apellidos-Vacio\\",\\"FirstName\\":\\"Nombre-Vacio\\",\\"Site\\":\\"LimaACME\\",\\"HPDCI\\":\\"\\",\\"AssignedGroup\\":\\"ACMEGROUP1\\",\\"SLMStatus\\":\\"1\\",\\"Resolution\\":\\"Resolución00489551\\",\\"DetailedDecription\\":\\"asdf\\",\\"LastResolvedDate\\":\\"1446653868\\",\\"Impact\\":\\"1000\\",\\"Urgency\\":\\"1000\\",\\"ServiceType\\":\\"0\\",\\"ReportedSource\\":\\"2000\\",\\"StatusReason\\":\\"16000\\",\\"ProductCategorizationTier1\\":\\"\\",\\"ProductCategorizationTier2\\":\\"\\",\\"ProductCategorizationTier3\\":\\"\\",\\"ResolutionCategory\\":\\"\\",\\"ResolutionCategoryTier2\\":\\"\\",\\"ResolutionCategoryTier3\\":\\"\\",\\"CategorizationTier1\\":\\"\\",\\"CategorizationTier2\\":\\"\\",\\"CategorizationTier3\\":\\"\\",\\"InternetE-mail\\":\\"email-Vacio@acme.com\\",\\"BAOFechaUltimaInfoTrabajo\\":\\"1446653868\\",\\"BAOParentID\\":\\"\\",\\"BAOExternalID\\":\\"\\",\\"BAOCaseNumberID\\":\\"00489551\\",\\"BAOCaseNumberID\\":\\"\\"}]}}</json>"}]');
        }else if(condicion=='OBTENER_CONTACTO'){
            res.setBody('[{"name":"str_json","value":"<value>{\\"outputs\\" : {\\"output\\":[ {\\"Remedy Login ID\\" : \\"\\",\\"First Name\\" : \\"Nombre-Vacio\\",\\"Last Name\\" : \\"Apellidos-Vacio\\",\\"Middle Initial\\" : \\"\\",\\"Internet E-mail\\" : \\"email-Vacio@acme.com\\",\\"Company\\" : \\"ACME\\",\\"Country\\" : \\"Peru\\",\\"code\\" : \\"000\\",\\"message\\" : \\"Error\\"}]}}</value>"}]');
        }else if(condicion=='CREACION_TICKET'){
            res.setBody('[{"name":"str_json","value":"<value>{\\"code\\":\\"000\\",\\"message\\":\\"Registro insertado correctamente\\",\\"ticket\\":[{ \\"ID Recibo\\":\\"247\\", \\"Case Number ID\\":\\"00456591\\", \\"ParentID\\":\\"\\", \\"ID Sistema\\":\\"Salesforce\\", \\"Country\\":\\"Peru\\", \\"Company ID\\":\\"PER_RUC_12121212122\\", \\"Company\\":\\"ACME\\", \\"Site\\":\\"\\", \\"First Name\\":\\"Nombre-Vacio\\", \\"Last Name\\":\\"Apellidos-Vacio\\", \\"Internet Email\\":\\"email-Vacio@acme.com\\", \\"Service Type\\":\\"0\\", \\"Impact\\":\\"1000\\", \\"Urgency\\":\\"4000\\", \\"Description\\":\\"f\\", \\"Detailed Description\\":\\"dfh\\", \\"Reported Source\\":\\"2000\\", \\"Reported Date\\":\\"\\", \\"Direct Contact Company\\":\\"\\", \\"Direct Contact First Name\\":\\"\\", \\"Direct Contact Last Name\\":\\"\\", \\"Direct Contact Email\\":\\"\\", \\"Categorization Tier 1\\":\\"\\", \\"Categorization Tier 2\\":\\"\\", \\"Categorization Tier 3\\":\\"\\", \\"Product Categorization Tier 1\\":\\"\\", \\"Product Categorization Tier 2\\":\\"\\", \\"Product Categorization Tier 3\\":\\"\\"}]}</value>"}]');
        }else if(condicion=='ACTUALIZAR_TICKET'){
            res.setBody(' [{"name":"str_json","value":"<value>{\\"code\\":\\"000\\", \\"message\\":\\"Cambio de estado realizado correctamente\\", \\"ticket\\": [{\\"ID Sistema\\":\\"Salesforce\\", \\"Country\\":\\"Peru\\", \\"Company ID\\":\\"PER_RUC_12121212122\\", \\"Incident Number\\":\\"INC000000022406\\", \\"Status\\":\\"6\\", \\"Status Reason\\":\\"27000\\", \\"Work Log Notes\\":\\"cancelo el ticket\\", \\"Work Log Type\\":\\"\\", \\"Work Log Date\\":\\"1448625560\\"}]}</value>"}]');
        }else if(condicion=='CREAR_WORKINFO'){
            res.setBody('[{"name":"str_json","value":"<value>{\\"WorklogID\\":\\"WLG000000024126\\",\\"message\\":\\"La informacion de trabajo se ha introducido correctamente\\",\\"code\\":\\"000\\"}</value>"}]');
        }else if(condicion=='OBTENER_SITE'){
            res.setBody('[{"name":"str_json","value":"<json>{\\"outputs\\" : { \\"output\\" :[ {\\"Site\\" : \\"LIMA-MIRAFLORES-AREQUIPA 1111\\",\\"Company\\" : \\"ACME\\"},{\\"Site\\" : \\"Lima ACME\\",\\"Company\\" : \\"ACME\\"}]}}</json>"}]');
        }else if(condicion=='OBTENER_CI'){
            res.setBody('[{"name":"str_json","value":"<json>{\\"outputs\\" : { \\"output\\" :[ {\\"Name\\" : \\"PRUEBAS2_LIMA\\",\\"Reconciliation Identity\\" : \\"OI-DFD871A5A0E644288303E3A12EC8E565\\",\\"Class Id\\" : \\"BMC_PERSON\\",\\"Category\\" : \\"\\",\\"Type\\" : \\"\\",\\"Company\\" : \\"ACME\\",\\"Item\\" : \\"\\",\\"Country\\" : \\"Peru\\"},{\\"Name\\" : \\"PRUEBAS2_LIMA\\",\\"Reconciliation Identity\\" : \\"OI-DFD871A5A0E644288303E3A12EC8E565\\",\\"Class Id\\" : \\"BMC_ASSETBASE\\",\\"Category\\" : \\"\\",\\"Type\\" : \\"\\",\\"Company\\" : \\"ACME\\",\\"Item\\" : \\"\\",\\"Country\\" : \\"Peru\\"}]}}</json>"}]');
        }
        else{
            res.setBody('[{"name":"nombre","value":"valor"}]');
        }
        res.setStatusCode(200);
        res.setStatus('OK');
        return res;
    }
}