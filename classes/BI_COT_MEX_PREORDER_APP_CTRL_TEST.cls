@isTest
private class BI_COT_MEX_PREORDER_APP_CTRL_TEST {

 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alejandro Pantoja	
    Company:       Accenture NEA
    Description:   Test class to manage the coverage code for COT_MEX_PREORDER_APP_CTRL class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    13/02/2017               Alejandro Pantoja          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 

    Description:   Test method to created the necesary data for the test.
   
    History:
    
    <Date>                  <Author>                <Change Description>
    13/02/2017               Alejandro Pantoja          Initial Version   
     ----------------------------------------------------------------------------------------------------------------------------------------*/


	@TestSetup
  	public static void crearData()
	{
		/*Creacion de estructura de Bu y LE*/
		Tgs_Dummy_Test_Data.dummyHierarchy();
		String record=[Select Id, DeveloperName,Name from RecordType where DeveloperName ='TGS_Business_Unit' limit 1].Id;
		Account accBU =[Select Id, Name from Account where RecordTypeId = :record LIMIT 1];

		/*Creacion de Sede y Punto de isntalacion*/
		//TGS_CaseMethods_TEST.validateMandatorySiteMWAN_TEST();

		  NE__OrderItem__c tr_CI_Order = TGS_Dummy_Test_Data.dummyConfigurationOrderCCAndBU();
            NE__Order__c tr_Order = [
                    SELECT Id, Site__c, NE__AccountId__c, NE__ServAccId__c, NE__BillAccId__c, NE__ServAccId__r.TGS_Aux_Holding__r.Name
                    FROM   NE__Order__c
                    WHERE  Id = :tr_CI_Order.NE__OrderId__c
            ];

		BI_Sede__c tr_Address = new BI_Sede__c (
                Name = 'TEST', 
                BI_Direccion__c = 'TEST',
                BI_Localidad__c = 'TEST',
                BI_Provincia__c = 'TEST',
                BI_Country__c   = 'Spain',
                BI_Codigo_postal__c = '28040'
            );
            insert tr_Address;
            BI_Punto_de_instalacion__c tr_Site = new BI_Punto_de_instalacion__c(
                BI_Cliente__c = tr_Order.NE__ServAccId__c,
                BI_Sede__c    = tr_Address.Id
            );
            insert tr_Site;

            Date myTt = date.today();
        Opportunity opp=new Opportunity(Name='opp',AccountId=accBU.id,StageName='Prospecting',CloseDate=myTt);
        insert opp;

          BI_COT_MEX_PuntoDeInstalacion_AccOpp__c sede = new  BI_COT_MEX_PuntoDeInstalacion_AccOpp__c();
            sede.BI_COT_MEX_Opportunity__c=opp.Id;
            sede.BI_COT_MEX_Sede__c=tr_Site.Id;
            insert sede;

	}


 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 

    Description:   Test method to manage the code coverage for COT_MEX_PREORDER_APP_CTRL.getInstallPoints 
   
    History:
    
    <Date>                  <Author>                <Change Description>
    13/02/2017               Alejandro Pantoja          Initial Version   
     ----------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void getInstallPoints_tst(){
		String record=[Select Id, DeveloperName,Name from RecordType where DeveloperName ='TGS_Business_Unit' limit 1].Id;
		Account accBU =[Select Id, Name from Account where RecordTypeId = :record LIMIT 1];
        Test.startTest();
		List<BI_Punto_de_instalacion__c> lst_PI =  BI_COT_MEX_PREORDER_APP_CTRL.getInstallPoints(accBU.Id);
        Test.stopTest();
		//System.assertNotEquals(false, List_asss.isEmpty(), 'No se ha creado SP');
	}
	 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 

    Description:   Test method to manage the code coverage for COT_MEX_PREORDER_APP_CTR.procesarSedes
   
    History:
    
    <Date>                  <Author>                <Change Description>
    13/02/2017               Alejandro Pantoja          Initial Version   
     ----------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	static void procesarSedes_tst(){
		String record=[Select Id, DeveloperName,Name from RecordType where DeveloperName ='TGS_Business_Unit' limit 1].Id;
		Account accBU =[Select Id, Name from Account where RecordTypeId = :record LIMIT 1];
		List<BI_Punto_de_instalacion__c> lst_PI =[Select Id,Name from BI_Punto_de_instalacion__c];
		String oppId=[Select Id,Name from Opportunity limit 1].Id;
        Test.startTest();
		 BI_COT_MEX_PREORDER_APP_CTRL.procesarSedes(lst_PI,accBU.Id+'',oppId);
        Test.stopTest();
	}
	 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 

    Description:   Test method to manage the code coverage for COT_MEX_PREORDER_APP_CTRL.getListSelected
   
    History:
    
    <Date>                  <Author>                <Change Description>
    13/02/2017               Alejandro Pantoja          Initial Version   
     ----------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	static void getListSelected_tst(){
		String record=[Select Id, DeveloperName,Name from RecordType where DeveloperName ='TGS_Business_Unit' limit 1].Id;
		Account accBU =[Select Id, Name from Account where RecordTypeId = :record LIMIT 1];
		List<BI_Punto_de_instalacion__c> lst_PI =[Select Id,Name from BI_Punto_de_instalacion__c];
		String oppId=[Select Id,Name from Opportunity limit 1].Id;
        Test.startTest();
		BI_COT_MEX_PREORDER_APP_CTRL.getListSelected(accBU.Id+'',oppId);
        Test.stopTest();
	}
	 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 

    Description:   Test method to manage the code coverage for COT_MEX_PREORDER_APP_CTRL.getAccname   
    History:
    
    <Date>                  <Author>                <Change Description>
    13/02/2017               Alejandro Pantoja          Initial Version   
     ----------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	static void getAccname_tst(){
		String record=[Select Id, DeveloperName,Name from RecordType where DeveloperName ='TGS_Business_Unit' limit 1].Id;
		Account accBU =[Select Id, Name from Account where RecordTypeId = :record LIMIT 1];
        Test.startTest();
		String aux=BI_COT_MEX_PREORDER_APP_CTRL.getAccname(accBU.Id+'');
        Test.stopTest();
	}
	 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 

    Description:   Test method to manage the code coverage for COT_MEX_PREORDER_APP_CTRL.getOppname
   
    History:
    
    <Date>                  <Author>                <Change Description>
    13/02/2017               Alejandro Pantoja          Initial Version   
     ----------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	static void getOppname_tst(){
		String oppId=[Select Id,Name from Opportunity limit 1].Id;
        Test.startTest();
		String aux=BI_COT_MEX_PREORDER_APP_CTRL.getOppname(oppId);
        Test.stopTest();
	} 
	
}