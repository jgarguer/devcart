/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:          Implementación Salesforce
* Customer:         TELEFONICA
* Description:         
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-07-24      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_Btn_Generate_MS_ctr
*		     1.1    23-02-2017      Jaime Regidor           Adding Campaign Values, Adding Catalog Country
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
					19/09/2017      Angel F. Santaliestra			Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest
private class BI_COL_Btn_Generate_MS_tst
{
	Public static User 											objUsuario;
	public static BI_COL_Btn_Generate_MS_ctr                	objGenerMS;
	public static Account                                   	objCuenta;
	public static Account                                   	objCuenta2;
	public static Contact                                   	objContacto;
	public static Campaign                                  	objCampana;
	public static Contract                                  	objContrato;
	public static BI_COL_Anexos__c                          	objAnexos;
	public static Opportunity                               	objOportunidad;
	public static BI_Col_Ciudades__c                        	objCiudad;
	public static BI_Sede__c                                	objSede;
	public static BI_Punto_de_instalacion__c                	objPuntosInsta;
	public static BI_COL_Descripcion_de_servicio__c         	objDesSer;
	public static BI_COL_Modificacion_de_Servicio__c        	objMS;
	public static BI_COL_Cobro_Parcial__c                   	objCobroParcial;
	public static BI_COL_Btn_Generate_MS_ctr.WrapperMS			objWrapperMS;
	
	public static List <Profile>                            	lstPerfil;
	public static List <User>                               	lstUsuarios;
	public static List <UserRole>                           	lstRoles;
	public static List<Campaign>                            	lstCampana;
	public static List<BI_COL_manage_cons__c >              	lstManege;
	public static List<BI_COL_Modificacion_de_Servicio__c>  	lstMS;
	public static List<BI_COL_Descripcion_de_servicio__c>   	lstDS;
	public static List<recordType>                          	lstRecTyp;
	public static List<BI_COL_Btn_Generate_MS_ctr.WrapperMS>	lstWrapperMS;
	public static void crearData ()
	{

			 
			BI_bypass__c objBibypass = new BI_bypass__c();
            objBibypass.BI_migration__c=true;
            insert objBibypass;

			////Roles
			//lstRoles                = new list <UserRole>();
			//lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
			//system.debug('datos Rol '+lstRoles);
			
			//Ciudad
			objCiudad = new BI_Col_Ciudades__c ();
			objCiudad.Name                      = 'Test City';
			objCiudad.BI_COL_Pais__c            = 'Test Country';
			objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
			insert objCiudad;
			System.debug('Datos Ciudad ===> '+objSede);
			
			//Cuentas
			objCuenta                                       = new Account();      
			objCuenta.Name                                  = 'prueba';
			objCuenta.BI_Country__c                         = 'Colombia';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Fraude__c                          = false;
			objCuenta.BillingCity 							= objCiudad.Id;
			objCuenta.BillingCountry 						= 'prueba';
			objCuenta.BillingPostalCode						= 'prueba';
			objCuenta.BillingState							= 'prueba';
			objCuenta.BillingStreet							= 'prueba';
			objCuenta.BI_Segment__c                         = 'test';
        	objCuenta.BI_Subsegment_Regional__c             = 'test';
       		objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			system.debug('datos Cuenta '+objCuenta);
			
			objCuenta2                                       = new Account();
			objCuenta2.Name                                  = 'prueba';
			objCuenta2.BI_Country__c                         = 'Colombia';
			objCuenta2.TGS_Region__c                         = 'América';
			objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta2.CurrencyIsoCode                       = 'COP';
			objCuenta2.BI_Fraude__c                          = true;
			objCuenta2.BI_Segment__c                        = 'test';
        	objCuenta2.BI_Subsegment_Regional__c            = 'test';
        	objCuenta2.BI_Territory__c                      = 'test';
			insert objCuenta2;
			
			//Contactos
			objContacto                                     = new Contact();
			objContacto.LastName                            = 'Test';
			objContacto.BI_Country__c                       = 'Colombia';
			objContacto.CurrencyIsoCode                     = 'COP'; 
			objContacto.AccountId                           = objCuenta.Id;
			objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
        	//REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
            //REING_FIN
			Insert objContacto;
			
			//catalogo
			NE__Catalog__c objCatalogo                      = new NE__Catalog__c();
			objCatalogo.Name                                = 'prueba Catalogo';
			objCatalogo.BI_country__c             			= 'Colombia';
			insert objCatalogo; 
			
			//Campaña
			objCampana                                      = new Campaign();
			objCampana.Name                                 = 'Campaña Prueba';
			objCampana.BI_Catalogo__c                       = objCatalogo.Id;
			objCampana.BI_COL_Codigo_campana__c             = 'prueba1';
			objCampana.BI_Country__c                        = 'Colombia';
      		objCampana.BI_Opportunity_Type__c               = 'Fijo';
      		objCampana.BI_SIMP_Opportunity_Type__c          = 'Alta';
			insert objCampana;
			lstCampana                                      = new List<Campaign>();
			lstCampana                                      = [select Id, Name from Campaign where Id =: objCampana.Id];
			system.debug('datos Cuenta '+lstCampana);
			
			//Tipo de registro
			lstRecTyp                                       = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
			system.debug('datos de FUN '+lstRecTyp);
			
			//Contrato
			objContrato                                     = new Contract ();
			objContrato.AccountId                           = objCuenta.Id;
			objContrato.StartDate                           =  System.today().addDays(+5);
			objContrato.BI_COL_Formato_Tipo__c              = 'Contrato Marco';
			objContrato.ContractTerm                        = 12;
			objContrato.BI_COL_Presupuesto_contrato__c      = 1000000;
			objContrato.StartDate                           = System.today().addDays(+5);
			insert objContrato;
			System.debug('datos Contratos ===>'+objContrato);
			
			//FUN
			objAnexos                                       = new BI_COL_Anexos__c();
			objAnexos.Name                                  = 'FUN-0041414';
			objAnexos.RecordTypeId                          = lstRecTyp[0].Id;
			objAnexos.BI_COL_Contrato__c                    = objContrato.Id;
			insert objAnexos;
			System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
			
			//Configuracion Personalizada
			BI_COL_manage_cons__c objConPer                 = new BI_COL_manage_cons__c();
			objConPer.Name                                  = 'Viabilidades';
			objConPer.BI_COL_Numero_Viabilidad__c           = 46;
			objConPer.BI_COL_ConsProyecto__c                = 1;
			objConPer.BI_COL_ValorConsecutivo__c            =1;
			lstManege                                       = new list<BI_COL_manage_cons__c >();        
			lstManege.add(objConPer);
			insert lstManege;
			System.debug('======= configuracion personalizada ======= '+lstManege);
			
			//Oportunidad
			objOportunidad                                          = new Opportunity();
			objOportunidad.Name                                     = 'prueba opp';
			objOportunidad.AccountId                                = objCuenta.Id;
			objOportunidad.BI_Country__c                            = 'Colombia';
			objOportunidad.CloseDate                                = System.today().addDays(+5);
			objOportunidad.StageName                                = 'F5 - Solution Definition';
			objOportunidad.CurrencyIsoCode                          = 'COP';
			objOportunidad.Certa_SCP__contract_duration_months__c   = 12;
			objOportunidad.BI_Plazo_estimado_de_provision_dias__c   = 0 ;
			//objOportunidad.BI_COL_Autoconsumo__c					= true;
			//objOportunidad.OwnerId                                  = lstUsuarios[0].id;
			insert objOportunidad;
			system.debug('Datos Oportunidad'+objOportunidad);
			list<Opportunity> lstOportunidad    = new list<Opportunity>();
			lstOportunidad                      = [select Id from Opportunity where Id =: objOportunidad.Id];
			system.debug('datos ListaOportunida '+lstOportunidad);
			
			
			//Direccion
			objSede                                 = new BI_Sede__c();
			objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
			objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
			objSede.BI_Localidad__c                 = 'Test Local';
			objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
			objSede.BI_Country__c                   = 'Colombia';
			objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
			objSede.BI_Codigo_postal__c             = '12356';
			insert objSede;
			System.debug('Datos Sedes ===> '+objSede);
			
			//Sede
			objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
			objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
			objPuntosInsta.BI_Sede__c          = objSede.Id;
			objPuntosInsta.BI_Contacto__c      = objContacto.id;
			objPuntosInsta.Name 				='Prueba Costantina';
			insert objPuntosInsta;
			System.debug('Datos Sucursales ===> '+objPuntosInsta);
			
			//DS
			objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
			objDesSer.CurrencyIsoCode                           = 'COP';
			insert objDesSer;
			System.debug('Data DS ====> '+ objDesSer);
			System.debug('Data DS ====> '+ objDesSer.Name);
			lstDS   = [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
			System.debug('Data lista DS ====> '+ lstDS);
			
			//MS
			objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
			objMS.BI_COL_FUN__c                         = objAnexos.Id;
			objMS.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
			objMS.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
			objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c                   = false;
			objMS.BI_COL_Estado__c                      = 'Activa';//label.BI_COL_lblActiva;
			objMS.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
			insert objMS;
			system.debug('Data objMs ===>'+objMs);
			lstMS = [select Name, BI_COL_Codigo_unico_servicio__c,BI_COL_Producto__r.Name,BI_COL_Descripcion_Referencia__c,
			                BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Sucursal_de_Facturacion__c, BI_COL_Codigo_unico_servicio__r.BI_COL_Sede_Destino__c, 
			                BI_COL_Sucursal_Origen__c, /*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c from BI_COL_Modificacion_de_Servicio__c
			        ]; //10/11/2016 ANRC: Se comentó el campo antiguo y se adiciono nuevo campo davox1
			System.debug('datos lista MS ===> '+lstMS);

	}
	
	static testMethod void test1()
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			// TO DO: implement unit test
			crearData();

			objAnexos.BI_COL_Fecha_de_venta__c = System.today().addDays(+5);
			update objAnexos;

			ApexPages.StandardController  controler           = new ApexPages.StandardController(objAnexos);
			PageReference                 pageRef 	          = Page.BI_COL_Btn_Generate_MS_pag;
			Test.setCurrentPage(pageRef);
			Test.startTest();
				lstWrapperMS									= new List<BI_COL_Btn_Generate_MS_ctr.WrapperMS> ();
				objGenerMS = new BI_COL_Btn_Generate_MS_ctr (controler);
				objGenerMS.fun = [select id, name, BI_COL_Contrato__r.Account.Id from BI_COL_Anexos__c Limit 1];
				system.debug('@-->>objGenerMS.fun.BI_COL_Contrato__r.Account.Id '+objGenerMS.fun.BI_COL_Contrato__r.Account.Id );
				List<BI_COL_Modificacion_de_Servicio__c> lstMSAutoconsumoConsulta = objGenerMS.getMSSolicitudServicioAutoconsumo( objGenerMS.fun.BI_COL_Contrato__r.Account.Id );
				system.debug('@-->>lstMSAutoconsumoConsulta'+lstMSAutoconsumoConsulta);
				
				objWrapperMS = new BI_COL_Btn_Generate_MS_ctr.WrapperMS(objMs, true);

				objGenerMS.idOppty = objOportunidad.Id;
				objGenerMS.getMSSolicitudServicio (objCuenta.Id);
				objGenerMS.idOppty = objOportunidad.Id;
				objGenerMS.getMSSolicitudServicioAutoconsumo (objCuenta.Id);
				lstWrapperMS.add(objWrapperMS);
				objGenerMS.lstMS = lstWrapperMS;
				objGenerMS.action_generarMS();
				objGenerMS.reiniciarConsultaMS(lstMS);
				objGenerMS.reiniciarConsultaMSAc(lstMS);
				objGenerMS.action_seleccionarTodos();
				objGenerMS.getPageNumber();
				objGenerMS.getPageNumberAc();
				objGenerMS.getPageSize();
				objGenerMS.getPageSizeAc();
				objGenerMS.getPreviousButtonEnabled();
				objGenerMS.getPreviousButtonEnabledAc();
				objGenerMS.getNextButtonDisabled();
				objGenerMS.getNextButtonDisabledAc();
				objGenerMS.getTotalPageNumber();
				objGenerMS.getTotalPageNumberAc();
				objGenerMS.ViewData();
				objGenerMS.ViewDataAc();
				objGenerMS.idOppty = objOportunidad.Id;
				objGenerMS.action_filtrarMSbyOppty();
				objGenerMS.nextBtnClick();
				objGenerMS.nextBtnClickAc();
				objGenerMS.previousBtnClickAc();
				objGenerMS.previousBtnClickAc();
				objGenerMS.getLstMS();
				objGenerMS.getLstMSAutoconsumo();
				objGenerMS.previousBtnClick();



				objGenerMS = new BI_COL_Btn_Generate_MS_ctr (controler);
				
			test.stopTest();
		}
		
	}

	static testMethod void test2()
	{

		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			// TO DO: implement unit test
			crearData();

			objAnexos.BI_COL_Fecha_de_venta__c = System.today().addDays(+5);
			update objAnexos;

			ApexPages.StandardController  controler           = new ApexPages.StandardController(objAnexos);
			PageReference                 pageRef 	          = Page.BI_COL_Btn_Generate_MS_pag;
			Test.setCurrentPage(pageRef);

			lstWrapperMS									= new List<BI_COL_Btn_Generate_MS_ctr.WrapperMS> ();

			Test.startTest();

				objGenerMS = new BI_COL_Btn_Generate_MS_ctr (controler);
				objWrapperMS = new BI_COL_Btn_Generate_MS_ctr.WrapperMS(objMs, true);


				objGenerMS.idOppty = objOportunidad.Id;
				objGenerMS.getMSSolicitudServicio (objCuenta.Id);
				objGenerMS.idOppty = objOportunidad.Id;
				objGenerMS.getMSSolicitudServicioAutoconsumo (objCuenta.Id);
				lstWrapperMS.add(objWrapperMS);
				objGenerMS.lstMSAutoconsumo = lstWrapperMS;
				//objGenerMS.action_generarMS();
				objGenerMS.reiniciarConsultaMS(lstMS);
				objGenerMS.reiniciarConsultaMSAc(lstMS);
				objGenerMS.action_seleccionarTodos();
				objGenerMS.getPageNumber();
				objGenerMS.getPageNumberAc();
				objGenerMS.getPageSize();
				objGenerMS.getPageSizeAc();
				objGenerMS.getPreviousButtonEnabled();
				objGenerMS.getPreviousButtonEnabledAc();
				objGenerMS.getNextButtonDisabled();
				objGenerMS.getNextButtonDisabledAc();
				objGenerMS.getTotalPageNumber();
				objGenerMS.getTotalPageNumberAc();
				objGenerMS.ViewData();
				objGenerMS.ViewDataAc();
				objGenerMS.idOppty = objOportunidad.Id;
				objGenerMS.action_filtrarMSbyOppty();
				objGenerMS.nextBtnClick();
				objGenerMS.nextBtnClickAc();
				objGenerMS.previousBtnClickAc();
				objGenerMS.previousBtnClickAc();
				objGenerMS.getLstMS();
				objGenerMS.getLstMSAutoconsumo();
				objGenerMS.previousBtnClick();


				objGenerMS = new BI_COL_Btn_Generate_MS_ctr (controler);
				
			test.stopTest();
		}
		
	}

	static testMethod void test3()
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			// TO DO: implement unit test
			crearData();

			List<BI_COL_Anexos__c> lstAnexos = [Select Id, Name, RecordTypeId, BI_COL_Contrato__r.Account.Id, BI_COL_Fecha_de_venta__c from BI_COL_Anexos__c];
			lstWrapperMS					 = new List<BI_COL_Btn_Generate_MS_ctr.WrapperMS> ();

			objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
			objMS.BI_COL_FUN__c                         = lstAnexos.get(0).Id;
			objMS.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
			objMS.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
			objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c                   = false;
			objMS.BI_COL_Estado__c                      = 'Pendiente';//label.BI_COL_lblActiva;
			objMS.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
			//objMs.BI_COL_Autoconsumo__c					= false;
			insert objMS;

			ApexPages.StandardController  controler           = new ApexPages.StandardController(lstAnexos.get(0));
			PageReference                 pageRef 	          = Page.BI_COL_Btn_Generate_MS_pag;
			Test.setCurrentPage(pageRef);

			Test.startTest();

				objGenerMS = new BI_COL_Btn_Generate_MS_ctr (controler);
				objWrapperMS = new BI_COL_Btn_Generate_MS_ctr.WrapperMS(objMs, true);
				
				lstWrapperMS.add(objWrapperMS);
				system.debug('\n-->>lstWrapperMS-->'+lstWrapperMS);
				objGenerMS.lstMSAutoconsumo  = lstWrapperMS;

				objGenerMS.getTotalPageNumberAc();
			test.stopTest();
		}
		
	}

	static testMethod void test4()
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			// TO DO: implement unit test
			crearData();

			List<BI_COL_Anexos__c> lstAnexos = [Select Id, Name, RecordTypeId, BI_COL_Contrato__r.Account.Id, BI_COL_Fecha_de_venta__c from BI_COL_Anexos__c];

			objOportunidad.BI_COL_Autoconsumo__c		= true;
			//System.runAs(lstUsuarios[0]) //Fix test (03/11/2015)
			//{
				update objOportunidad;
			//}

			//objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
			objMS.BI_COL_FUN__c                         = lstAnexos.get(0).Id;
			objMS.BI_COL_Estado__c                      = 'Pendiente';//label.BI_COL_lblActiva;
	//		objMs.BI_COL_Autoconsumo__c					= true;
			update objMS;

			ApexPages.StandardController  controler           = new ApexPages.StandardController(lstAnexos.get(0));
			PageReference                 pageRef 	          = Page.BI_COL_Btn_Generate_MS_pag;
			Test.setCurrentPage(pageRef);

			Test.startTest();

				objGenerMS = new BI_COL_Btn_Generate_MS_ctr (controler);
				
			test.stopTest();
		}
		
	}

	static testMethod void test5()
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			// TO DO: implement unit test
			crearData();

			ApexPages.StandardController  controler           = new ApexPages.StandardController(objSede);
			PageReference                 pageRef 	          = Page.BI_COL_Btn_Generate_MS_pag;
			Test.setCurrentPage(pageRef);

			objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
			objMS.BI_COL_FUN__c                         = objAnexos.Id;
			objMS.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
			objMS.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
			objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c                   = false;
			objMS.BI_COL_Estado__c                      = 'Pendiente';//label.BI_COL_lblActiva;
			objMS.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
			//objMs.BI_COL_Autoconsumo__c					= false;
			objAnexos.BI_COL_Fecha_de_venta__c = System.today().addDays(+5);
			insert objMS;
			List<BI_COL_Btn_Generate_MS_ctr.WrapperMS> lstWrapperMS1					 = new List<BI_COL_Btn_Generate_MS_ctr.WrapperMS> ();
			lstWrapperMS					 = new List<BI_COL_Btn_Generate_MS_ctr.WrapperMS> ();

			Test.startTest();

				
				objGenerMS = new BI_COL_Btn_Generate_MS_ctr (controler);
				BI_COL_Btn_Generate_MS_ctr.WrapperMS objWrapperMS1 = new BI_COL_Btn_Generate_MS_ctr.WrapperMS(objMs, true);
				objWrapperMS = new BI_COL_Btn_Generate_MS_ctr.WrapperMS(objMs, true);
				
				lstWrapperMS.add(objWrapperMS);
				system.debug('\n-->>lstWrapperMS-->lstMS'+lstWrapperMS);
				objGenerMS.lstMS  = lstWrapperMS;
				
				lstWrapperMS1.add(objWrapperMS1);
				system.debug('\n-->>lstWrapperMS-->lstMSAutoconsumo'+lstWrapperMS1);
				objGenerMS.lstMSAutoconsumo  = lstWrapperMS1;
				objGenerMS.action_seleccionarTodosAc();
				objGenerMS.idFUN  = null;

				
			test.stopTest();
		}
		
	}

	static testMethod void test6()
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			crearData();
			lstRecTyp                                       = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
			system.debug('datos de FUN '+lstRecTyp);

			//Ciudad
			objCiudad = new BI_Col_Ciudades__c ();
			objCiudad.Name                      = 'Test City';
			objCiudad.BI_COL_Pais__c            = 'Test Country';
			objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
			insert objCiudad;
			System.debug('Datos Ciudad ===> '+objSede);
			
			//Cuentas
			objCuenta                                       = new Account();      
			objCuenta.Name                                  = 'prueba';
			objCuenta.BI_Country__c                         = 'Colombia';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Fraude__c                          = false;
			objCuenta.BillingCity 							= objCiudad.Id;
			objCuenta.BillingCountry 						= 'prueba';
			objCuenta.BillingPostalCode						= 'prueba';
			objCuenta.BillingState							= 'prueba';
			objCuenta.BillingStreet							= 'prueba';
			objCuenta.BI_Segment__c                        = 'test';
        	objCuenta.BI_Subsegment_Regional__c            = 'test';
        	objCuenta.BI_Territory__c                      = 'test';
			insert objCuenta;
			system.debug('datos Cuenta '+objCuenta);

			//Contrato
			objContrato                                     = new Contract ();
			objContrato.AccountId                           = objCuenta.Id;
			objContrato.StartDate                           =  System.today().addDays(+5);
			objContrato.BI_COL_Formato_Tipo__c              = 'Contrato Marco';
			objContrato.ContractTerm                        = 12;
			objContrato.BI_COL_Presupuesto_contrato__c      = 1000000;
			objContrato.BI_COL_Duracion_Indefinida__c		= true;
			objContrato.BI_COL_Cuantia_Indeterminada__c		= true;
			objContrato.BI_Indefinido__c					= 'Si';
			objContrato.StartDate                           = System.today().addDays(+5);
			insert objContrato;
			System.debug('datos Contratos ===>'+objContrato);

			//FUN
			objAnexos                                       = new BI_COL_Anexos__c();
			objAnexos.Name                                  = 'FUN-0041414';
			objAnexos.RecordTypeId                          = lstRecTyp[0].Id;
			objAnexos.BI_COL_Contrato__c                    = objContrato.Id;
			insert objAnexos;
			System.debug('\n\n\n salida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
			ApexPages.StandardController  controler           = new ApexPages.StandardController(objAnexos);
			PageReference                 pageRef 	          = Page.BI_COL_Btn_Generate_MS_pag;
			Test.setCurrentPage(pageRef);
			
			lstWrapperMS					 = new List<BI_COL_Btn_Generate_MS_ctr.WrapperMS> ();

			Test.startTest();
				BI_COL_Btn_Generate_MS_ctr.WrapperMS objWrapperMS1 = new BI_COL_Btn_Generate_MS_ctr.WrapperMS(objMs, true);
				objWrapperMS = new BI_COL_Btn_Generate_MS_ctr.WrapperMS(objMs, true);
				lstWrapperMS.add(objWrapperMS);

				objGenerMS = new BI_COL_Btn_Generate_MS_ctr (controler);
				objGenerMS.lstMSSel = lstMS;
				objGenerMS.fun = [Select id, name, RecordTypeId, BI_COL_Contrato__r.Account.Id  from BI_COL_Anexos__c Limit 1];
				objGenerMS.lstMSAutoconsumo = lstWrapperMS;
				objGenerMS.action_generarMS();
							
			test.stopTest();
		}
		
	}

	static testMethod void test7()
	{

		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
        	crearData();
			lstRecTyp                                       = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
			system.debug('datos de FUN '+lstRecTyp);

			//Ciudad
			objCiudad = new BI_Col_Ciudades__c ();
			objCiudad.Name                      = 'Test City';
			objCiudad.BI_COL_Pais__c            = 'Test Country';
			objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
			insert objCiudad;
			System.debug('Datos Ciudad ===> '+objSede);
			
			//Cuentas
			objCuenta                                       = new Account();      
			objCuenta.Name                                  = 'prueba';
			objCuenta.BI_Country__c                         = 'Colombia';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Fraude__c                          = false;
			objCuenta.BillingCity 							= objCiudad.Id;
			objCuenta.BillingCountry 						= 'prueba';
			objCuenta.BillingPostalCode						= 'prueba';
			objCuenta.BillingState							= 'prueba';
			objCuenta.BillingStreet							= 'prueba';
			objCuenta.BI_Segment__c                        = 'test';
        	objCuenta.BI_Subsegment_Regional__c            = 'test';
        	objCuenta.BI_Territory__c                      = 'test';
			insert objCuenta;
			system.debug('datos Cuenta '+objCuenta);

			//Contrato
			objContrato                                     = new Contract ();
			objContrato.AccountId                           = objCuenta.Id;
			objContrato.StartDate                           =  System.today().addDays(+5);
			objContrato.BI_COL_Formato_Tipo__c              = 'Contrato Marco';
			objContrato.ContractTerm                        = 12;
			objContrato.BI_COL_Presupuesto_contrato__c      = 1000000;
			objContrato.BI_COL_Duracion_Indefinida__c		= true;
			objContrato.BI_COL_Cuantia_Indeterminada__c		= true;
			objContrato.BI_Indefinido__c					= 'Si';
			objContrato.StartDate                           = System.today().addDays(+5);
			insert objContrato;
			System.debug('datos Contratos ===>'+objContrato);

			//FUN
			objAnexos                                       = new BI_COL_Anexos__c();
			objAnexos.Name                                  = 'FUN-0041414';
			objAnexos.RecordTypeId                          = lstRecTyp[0].Id;
			objAnexos.BI_COL_Contrato__c                    = objContrato.Id;
			insert objAnexos;
			System.debug('\n\n\n salida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
			ApexPages.StandardController  controler           = new ApexPages.StandardController(objAnexos);
			PageReference                 pageRef 	          = Page.BI_COL_Btn_Generate_MS_pag;
			Test.setCurrentPage(pageRef);
			
			lstWrapperMS					 = new List<BI_COL_Btn_Generate_MS_ctr.WrapperMS> ();

			Test.startTest();
				BI_COL_Btn_Generate_MS_ctr.WrapperMS objWrapperMS1 = new BI_COL_Btn_Generate_MS_ctr.WrapperMS(objMs, true);
				objWrapperMS = new BI_COL_Btn_Generate_MS_ctr.WrapperMS(objMs, true);
				lstWrapperMS.add(objWrapperMS);

				objGenerMS = new BI_COL_Btn_Generate_MS_ctr (controler);
				//objGenerMS.lstMSSel = lstMS;
				objGenerMS.fun = [Select id, name, RecordTypeId, BI_COL_Contrato__r.Account.Id  from BI_COL_Anexos__c Limit 1];
				objGenerMS.lstMSAutoconsumo = lstWrapperMS;
				objGenerMS.lstMSConsulta = lstMS;
				objGenerMS.action_filtrarMSbyOppty();
							
			test.stopTest();
		}		
	}
}