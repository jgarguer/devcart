/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Controller extension for BI_O4_OpportunityQuotes page
 Test Class:    BI_O4_OpportunityQuotesController_TEST
 
 History:
  
 <Date>              <Author>                   <Change Description>
 26/09/2016          Fernando Arteaga           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_OpportunityQuotesController
{
	public Opportunity opp {get; set;}
	
	public BI_O4_OpportunityQuotesController(ApexPages.standardController controller)
	{
		opp = (Opportunity) controller.getRecord();
	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Gets quotes related to the opportunity
 
 History:
  
 <Date>              <Author>                   <Change Description>
 26/09/2016          Fernando Arteaga           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public List<NE__Order__c> getQuotes()
	{
		try
		{
			if (BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
				
			return [SELECT Id, Name, NE__Version__c, NE__OrderStatus__c, CreatedDate
					FROM NE__Order__c
					WHERE NE__OptyId__c = :opp.Id
					AND RecordType.DeveloperName = 'Quote'
					ORDER BY NE__Version__c DESC];
		}
		catch (Exception exc)
		{
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_OpportunityQuotesController.getQuotes', 'BI_EN', exc, 'Clase');
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, exc.getMessage()));
			return new List<NE__Order__c>();
		}
	}
}