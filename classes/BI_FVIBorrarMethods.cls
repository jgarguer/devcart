public with sharing class BI_FVIBorrarMethods 
{

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        ---------
    Company:       ---------
    Description:   Method that delete the oi that have FVI Borrar status
    
    History:
    
    <Date>            <Author>             <Description>
    --/--/----        -----------------    Initial version
    17/04/2017        Guillermo Muñoz      Added childs oi delete
    19/05/2017		  Guillermo Muñoz	   Moved setTriggerFired to avoid errors
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void Borrar(List <NE__OrderItem__c> news) 
	{
		boolean wasFired  =  NETriggerHelper.getTriggerFired('BI_FVIBorrar');
		system.debug('*wasFired ' + wasFired + ' !wasFired ' + !wasFired);
		if(!wasFired)
		{
			//NETriggerHelper.setTriggerFired('BI_FVIBorrar');
			System.debug('BIFVIBorrar entering in code');
			List<NE__OrderItem__c> OIToDeleteList = new List<NE__OrderItem__c>();
			List<id> idsToFindAttributes = new List<id>();

			// retrieving if we have to delete or not
			for(NE__OrderItem__c oiNew : [SELECT id,NE__Status__c FROM NE__OrderItem__c WHERE id IN :news])
			{
				if(oiNew.NE__Status__c == 'FVI Borrar')
				{
					idsToFindAttributes.add(oiNew.Id);
					OIToDeleteList.add(oiNew);
				}
			}

			if(!OIToDeleteList.isEmpty()){

				NETriggerHelper.setTriggerFired('BI_FVIBorrar');
				
				//retrieving the Order Item Attributes form out OI to delete
				List<NE__Order_Item_Attribute__c> lst_attributes = new List <NE__Order_Item_Attribute__c>(); 

				List <NE__OrderItem__c> lst_oi = [SELECT Id, (SELECT Id FROM NE__Order_Item_Attributes__r) FROM NE__OrderItem__c WHERE NE__Parent_Order_Item__c IN: OIToDeleteList OR Id IN: OIToDeleteList];

				if(!lst_oi.isEmpty()){
					for(NE__OrderItem__c oi : lst_oi){

						if(!oi.NE__Order_Item_Attributes__r.isEmpty()){
							
							lst_attributes.addAll(oi.NE__Order_Item_Attributes__r);
						}
					}
				}

				try
				{
					if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');
					System.debug('BI_FVIBorrar deleting ' + lst_oi.size() + ' OrderItems and ' + lst_attributes.size() + ' Attributes' );
					// deleting the objects   
					if(!lst_oi.isEmpty()){
						delete lst_oi;
					}
					if(!lst_attributes.isEmpty()){
						delete lst_attributes;
					}
				}
				catch(Exception e)
				{
					System.debug('BI_FVIBorrar error deleting objects. ' + e.getMessage());
				}
			}
       }
	}
}