public class TGS_Lookup_Handler {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta García
    Company:       Deloitte
    Description:   Add error for lookup field in Case
    
    History

    <Date>            <Author>                      <Description>
    18/11/2015        Marta García                  Initial version
    11/02/2016        Fernando Arteaga              Delete no needed queries
    19/06/2017        Álvaro López                  General optimization for big quantity of CIs and attributes
--------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    public static void  addErrorLookup (List<Case> newCase, Map<Id,Case> Cases, List<Case> oldCase){
        if(Constants.firstRunLookup){
            Constants.firstRunLookup=false;
             
            String attribute_class;
            String attribute_id;
            String caseNumber;
            String IntegrationID;
            Id rtIdO = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
            List<Id> IdOrder = new  List<Id>();
            Map<String, String> MapAttribute = new Map<String,String>();
            Set<String> SetNameAttribute = new Set<String>(); //Álvaro López 19/06/2017 - Changed list to set
            // FAR 11/02/2016
            //List<String> ListIntegrationID = new List<String>();
            Set<String> setIntegrationID = new Set<String>();
            // End FAR
            //List<NE__OrderItem__c> ListCis = new List<NE__OrderItem__c>();
            Map<Id, NE__OrderItem__c> map_cis = new Map<Id, NE__OrderItem__c>();
            Set<String> setIntegrationIDcancelled = new Set<String>();
            Set<String> setIntegrationIDdeleted = new Set<String>();
            Id recordtypelookup = TGS_RecordTypes_Util.getRecordTypeId(NE__DynamicPropertyDefinition__c.SObjectType, 'Dynamic_Lookup');
            Map<id, String> MapCaseError = new  Map<id, String>();
            Map<id,String> MapStatusReasonOld = new Map<id,String>();
            Map<id,String> MapStatusOld = new Map<id,String>();
             
            for(Case c: newCase){
                 
                IdOrder.add(c.Order__c);
            }
             
            for(Case co: oldCase){
                 
                MapStatusReasonOld.put(co.id,co.TGS_Status_reason__c);
                MapStatusOld.put(co.id,co.Status);
                 
            }
             
            /* START Álvaro López 19/06/2017 - General optimization */

            // FAR 11/02/2016 - Add fields to the query --> avoids executing another query since we have all the order items (line 83-88)
            //for(NE__OrderItem__c cis : [SELECT iD, NE__OrderId__r.RecordTypeId, NE__OrderId__r.Case__r.id, NE__OrderId__c, NE__OrderId__r.Case__r.Type, (SELECT Name, NE__Value__c, NE__FamPropId__r.NE__PropId__r.RecordTypeId FROM NE__Order_Item_Attributes__r) FROM NE__OrderItem__c WHERE NE__OrderId__r.RecordTypeId =: rtIdO AND NE__OrderId__c in :IdOrder ]){
            for(NE__OrderItem__c cis : [SELECT iD, NE__OrderId__r.RecordTypeId, NE__OrderId__r.Case__r.id, NE__OrderId__c, NE__OrderId__r.Case__r.Type, Name, NE__OrderId__r.Case__r.Status, Integration_Id__c, NE__OrderId__r.NE__Type__c, TGS_Service_status__c, (SELECT Name, NE__Value__c, NE__FamPropId__r.NE__PropId__r.RecordTypeId, NE__Order_Item__c FROM NE__Order_Item_Attributes__r) FROM NE__OrderItem__c WHERE NE__OrderId__r.RecordTypeId =: rtIdO AND NE__OrderId__c in :IdOrder ]){
                 
                map_cis.put(cis.Id, cis);
                 
                for(NE__Order_Item_Attribute__c attributes: cis.NE__Order_Item_Attributes__r){
                     
                    MapAttribute.put(attributes.Name, attributes.NE__Value__c); // todos los atributos con su valor
                     
                    if(attributes.Name.endsWith('_id') || attributes.Name.endsWith('_class')){
                         
                        SetNameAttribute.add(attributes.Name); // atributos _id y _class
                         
                         
                         
                        if(attributes.Name.endsWith('_id')){
                            IntegrationID = attributes.Name;
                            if(String.IsNOtBlank(MapAttribute.get(IntegrationID))){
                                 
                                // FAR 11/02/2016
                                //ListIntegrationID.add(MapAttribute.get(IntegrationID)); // todos los Integration id, el valor de los atributos _id
                                setIntegrationID.add(MapAttribute.get(IntegrationID)); // todos los Integration id, el valor de los atributos _id
                            }
                        }
                    }
                     
                     
                }
            }
             
             /* FAR 11/02/2016 - The queries are identical, and no needed since we have all the order items from the query in line 51
             List<NE__OrderItem__c> ciscancelled = [Select Name, NE__OrderId__r.Case__r.Status, NE__OrderId__r.Case__r.Type, Integration_Id__c, NE__OrderId__r.NE__Type__c, TGS_Service_status__c
                                                    From NE__OrderItem__c
                                                    Where Integration_Id__c in : ListIntegrationID];
             List<NE__OrderItem__c> cisdeleted = [Select Name, NE__OrderId__r.Case__r.Status, NE__OrderId__r.Case__r.Type, Integration_Id__c, NE__OrderId__r.NE__Type__c, TGS_Service_status__c
                                                  From NE__OrderItem__c
                                                  Where Integration_Id__c in : ListIntegrationID];
             
             for(NE__OrderItem__c cicancelled : ciscancelled){
                 
                 if(cicancelled.NE__OrderId__r.Case__r.Status == Constants.CASE_STATUS_CANCELLED && cicancelled.NE__OrderId__r.Case__r.Type == Constants.TYPE_NEW && cicancelled.NE__OrderId__r.NE__Type__c == 'Asset'){
                     
                     ListIntegrationIDcancelled.add(cicancelled.Integration_Id__c); //IntegrationID de case cancelled y registration
                 }
                 
             }
             
             for(NE__OrderItem__c cideleted : cisdeleted){
                 
                 if(cideleted.TGS_Service_status__c == Constants.CI_TGS_SERVICE_STATUS_DELETED && cideleted.NE__OrderId__r.NE__Type__c == 'Asset'){
                     
                     ListIntegrationIDdeleted.add(cideleted.Integration_Id__c); //IntegrationID de case deleted
                 }
                 
             }  
             */
            // FAR 11/02/2016 - Iterate over each ci and, if setIntegrationID contains it, put the integration_id into the proper set
            if (!setIntegrationID.isEmpty())
            {
                for (NE__OrderItem__c oi: map_cis.values())
                {
                    if (setIntegrationID.contains(oi.Integration_Id__c) &&
                        oi.NE__OrderId__r.Case__r.Status == Constants.CASE_STATUS_CANCELLED &&
                        oi.NE__OrderId__r.Case__r.Type == Constants.TYPE_NEW &&
                        oi.NE__OrderId__r.NE__Type__c == 'Asset')
                        setIntegrationIDcancelled.add(oi.Integration_Id__c); //IntegrationID de case cancelled y registration
                        
                    if (setIntegrationID.contains(oi.Integration_Id__c) &&
                        oi.TGS_Service_status__c == Constants.CI_TGS_SERVICE_STATUS_DELETED &&
                        oi.NE__OrderId__r.NE__Type__c == 'Asset')
                        setIntegrationIDdeleted.add(oi.Integration_Id__c); //IntegrationID de case deleted
                }
            }
            // END FAR 11/02/2016
             
            Map<Id, NE__Order_Item_Attribute__c> map_atts = new Map<Id, NE__Order_Item_Attribute__c>();
             
            for(NE__OrderItem__c cis :  map_cis.values()){
                map_atts.putAll(cis.NE__Order_Item_Attributes__r);
                map_cis.put(cis.Id, cis);
            }
            for(NE__Order_Item_Attribute__c att : map_atts.values()){
                if(map_atts.containsKey(att.Id)){
                     
                    //Para los atributos lookup de OrderItem  y OrderItemAttribute
                    if( ( SetNameAttribute.contains(map_atts.get(att.Id).Name +'_class') || SetNameAttribute.contains(map_atts.get(att.Id).Name +'_id')) && (map_atts.get(att.Id).NE__FamPropId__c != null) && map_atts.get(att.Id).NE__FamPropId__r.NE__PropId__r.RecordTypeId.equals(recordtypelookup) && String.isNotBlank(MapAttribute.get(map_atts.get(att.Id).Name))){
                        System.debug('############');
                         
                         
                         
                        attribute_class = MapAttribute.get(map_atts.get(att.Id).Name +'_class');
                        attribute_id = MapAttribute.get(map_atts.get(att.Id).Name +'_id');
                        system.debug('Attribute_id '+attribute_id);
                        system.debug(attribute_class+'  '+String.isBlank(attribute_class));
                        //system.debug(Cases.get(cis.NE__OrderId__r.Case__r.id).Status);
                        //system.debug(cis.NE__OrderId__r.Case__r.Type);
                        if(map_cis.containsKey(att.NE__Order_Item__c)){
                            system.debug(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__c);//null
                            system.debug(map_cis.get(att.NE__Order_Item__c).NE__OrderId__c);//id
                            system.debug(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r);
                            if(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__c!=null){
                                if((Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).Status==Constants.CASE_STATUS_CLOSED) && (String.isBlank(attribute_class) || String.isBlank(attribute_id)) && (map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.Type == Constants.TYPE_CHANGE || map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.Type == Constants.TYPE_DISCONNECT)){
                                     
                                     
                                     
                                    Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).addError('To progress this order, the CI associated in the attribute ' + map_atts.get(att.Id).Name.remove('_id').remove('_class') + ' must be an asset. Check your order and try again '); 
                                     
                                } 
                                 
                                system.debug(Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).Status==Constants.CASE_STATUS_RESOLVED);
                                system.debug(String.isBlank(attribute_class) || String.isBlank(attribute_id));
                                system.debug(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.Type == Constants.TYPE_NEW);
                                 
                                 
                                if((Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).Status==Constants.CASE_STATUS_RESOLVED) && (String.isBlank(attribute_class) || String.isBlank(attribute_id)) && (map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.Type == Constants.TYPE_NEW)){
                                     
                                     
                                     
                                    Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).addError('To progress this order, the CI associated in the attribute ' + map_atts.get(att.Id).Name.remove('_id').remove('_class') + ' must be an asset. Check your order and try again '); 
                                     
                                }  
                                 
                                // FAR 11/02/2016
                                //if(ListIntegrationIDcancelled.contains(attribute_id)){
                                if(setIntegrationIDcancelled.contains(attribute_id)){
                                     
                                     
                                    Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).addError('This order can not be progress because lookup '+map_atts.get(att.Id).Name.remove('_id').remove('_class')+' relationship is related with Asset in status cancelled' ); 
                                     
                                }  
                                system.debug('status old '+ MapStatusOld.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id));
                                system.debug('status reason old '+MapStatusReasonOld.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id));
                                system.debug('status new '+Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).Status);
                                system.debug('status reason new '+Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).TGS_Status_reason__c);
                                 
                                system.debug(setIntegrationIDdeleted.contains(attribute_id));
                                system.debug(Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).Status!=  MapStatusOld.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id));
                                system.debug(Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).TGS_Status_reason__c!= MapStatusReasonOld.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id));
                                 
                                // FAR 11/02/2016
                                //if(ListIntegrationIDdeleted.contains(attribute_id) && (Cases.get(cis.NE__OrderId__r.Case__r.id).Status!=  MapStatusOld.get(cis.NE__OrderId__r.Case__r.id) || Cases.get(cis.NE__OrderId__r.Case__r.id).TGS_Status_reason__c!= MapStatusReasonOld.get(cis.NE__OrderId__r.Case__r.id))){
                                if(setIntegrationIDdeleted.contains(attribute_id) && (Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).Status!=  MapStatusOld.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id) || Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).TGS_Status_reason__c!= MapStatusReasonOld.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id))){
                                     
                                     
                                    Cases.get(map_cis.get(att.NE__Order_Item__c).NE__OrderId__r.Case__r.id).addError('This order can not be progress because lookup '+map_atts.get(att.Id).Name.remove('_id').remove('_class')+' relationship is related with Asset in status deleted' ); 
                                     
                                }  
                            }    
                        }
                    }
                }  
            }
        } 
        /* END Álvaro López 19/06/2017 - General optimization */             
    }
}