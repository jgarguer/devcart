public class BI_COL_SendingEmailPrefeasibility_cls 
{
    
    public static void envioMail_mtd(BI_COL_PreFactibilidad__c regPC)
   {
        string strMailCC='', strMailReplyTo='', strSenderName='', strSubject='', strMessage='', strRestpuesta='', srtRiesgos='', strFirma='';
        string []strMailPARA=null;
        boolean blnIsHTML = false;
        integer intCont = 1;
        list<String> lstemail= new list<String> ();
        //
        string envioEMail= label.BI_COL_LbDestinatariosEmailTelefonica;//'prefactibilidades.tc@telefonica.com.conf;novedadespreventa.co@telefonica.com;paula.galindo@telefonica.com';
        
        System.debug('Lista de email----->>>>'+envioEMail);
        System.debug('Lista de etiquetas----->>>>'+label.BI_COL_LbDestinatariosEmailTelefonica);

        System.debug('BI_COL_PreFactibilidad__c----->>>>'+regPC);
        //Prefactibilidad= [SELECT BI_COL_Proyecto__c,Id,Name FROM BI_COL_PreFactibilidad__c where id =: regPC.id ];
        Opportunity regOp = [select Id, Name, AccountId, Account.Name, BI_Numero_id_oportunidad__c, OwnerId from Opportunity where Id =: regPC.BI_COL_Proyecto__c limit 1];
        System.debug('Lista oportunidad----->>>>'+regOp);
        User regUs = [select Id, Name, Email from User where Id =:regOp.OwnerId limit 1];
        User regUsLogeado = [select Id, Name, Email from User where Id =:userInfo.getUserId() limit 1];
        
        /* Por validar sis e crea o no el objeto
        list<MatrizRiesgo__c> lstMR = [select Id, Name, FechaRegistro__c, Estado__c, FaseAfectada__c, FaseIdentificacion__c, Denominacion__c, 
                                              ImpactoAlcance__c, Descripcion__c 
                                       from   MatrizRiesgo__c 
                                       where  PrefactibilidadComplejidad__c=:regPC.Id];*/
                
        //strFirma = regUs.Name+'\n';
        //strFirma = regUs.Email+'\n';
        //strFirma = strFirma+'Líder Prefactibilidad | Dirección de Ingeniería y Atención Clientes\n';
        //strFirma = strFirma+'Transv. 60 (Av. Suba) Nº 114A - 55 \n';
        //strFirma = strFirma+'paula.galindo@telefonica.com | Tel:(571)5935399 Ext.5989 | cel:(57)3174422405';
        lstemail = envioEMail.split(';');
        
        System.debug('Lista email full----->>>>'+lstemail);
        strMailPARA = new String[]{regUs.Email,regUsLogeado.Email}; 
        

        for(String EM : lstemail){
            System.debug('Lista email full22----->>>>'+lstemail);
            strMailPARA.add(EM);
        }
        
        if(regPC.BI_COL_Destinatarios_email__c!=null && regPC.BI_COL_Destinatarios_email__c.trim()!=''){
            
            if(!regPC.BI_COL_Destinatarios_email__c.contains(';') && !regPC.BI_COL_Destinatarios_email__c.contains(',') && !regPC.BI_COL_Destinatarios_email__c.equals('')){
                strMailPARA.add(regPC.BI_COL_Destinatarios_email__c.trim());
            }
            
            if(regPC.BI_COL_Destinatarios_email__c.contains(';')||regPC.BI_COL_Destinatarios_email__c.contains(',')){
    
                    regPC.BI_COL_Destinatarios_email__c=regPC.BI_COL_Destinatarios_email__c.replace(',',';');
    
                    string []strCC=regPC.BI_COL_Destinatarios_email__c.split(';');
    
                    for(integer i=0;i<strCC.size();i++){
                        
                        strMailPARA.add(strCC[i].trim());   
                
                    }
                    
            }
        }
        //strMailPARA.add('cfcarvajal@avanxo.com');
        strMailReplyTo = envioEMail;//'prefactibilidades.tc@telefonica.com';
        strSenderName = envioEMail; //'prefactibilidades.tc@telefonica.com';
        strMailCC = envioEMail;
        if(regPC.BI_COL_Destinatarios_email__c != null){
            strMailCC = strMailCC+';'+regPC.BI_COL_Destinatarios_email__c;      
        }
        
        //System.debug('\n\n strMailCC: '+strMailCC+'\n\n');
        strSubject = 'Prefactibilidad oportunidad'+' '+regOp.BI_Numero_id_oportunidad__c+'-'+regOp.Name+'-'+regOp.Account.Name+'-'+regPC.Name;
        //-----Cuerpo de mensaje a enviar una vez se aprueba flujo en PC
        if(regPC.BI_COL_Enviar_email__c == true)
        {
            /*if(lstMR.size() > 0)
            {
                for(MatrizRiesgo__c mr : lstMR)
                {
                    srtRiesgos = srtRiesgos+'FASE DENOMINACIÓN:'+' '+mr.Denominacion__c+'\n';
                    srtRiesgos = srtRiesgos+'IMPACTO DESCRIPCIÓN:'+' '+mr.ImpactoAlcance__c+'\n';
                    srtRiesgos = srtRiesgos+'DESCRIPCIÓN:'+' '+mr.Descripcion__c+'\n\n';
                }
            }*/
            
            strMessage = 'Buen día'+' '+regUs.Name+'<br></br><br></br>';
            strMessage = strMessage+'Confirmo que la oportunidad'+' '+regOp.Name+' '+'para el cliente'+' '+regOp.Account.Name+' '+'es'+' '+regPC.BI_COL_Registro_aprobado__c+' ';
            strMessage = strMessage+'con complejidad'+' '+regPC.BI_COL_Complejidad__c+', '+'con los siguientes riesgos:<br></br><br></br>';
            strMessage = strMessage+srtRiesgos+'<br></br>';
            strMessage = strMessage+'Observaciones:'+' '+RegPC.BI_COL_Agregar_observaciones__c+'<br></br><br></br>';
            
            strMessage = strMessage+'Descripción inicial del proyecto por parte del Ejecutivo:'+' '+RegPC.BI_COL_Descripcion_negocio__c+'<br></br><br></br>';
            
            
            strMessage = strMessage+'Para mayor información dirijase a Salesforce:<br></br>';
            //strMessage = strMessage+'https://na5.salesforce.com/'+regPC.Id+'<br></br><br></br>';
            //strMessage = strMessage+'www.salesforce.com';
            strMessage = strMessage+'Gracias <br></br><br></br>';
            strMessage = strMessage+strFirma;
        }
        
        //----- Cuerpo de mensaje a enviar cuando PC inicia aprobación
        if(regPC.BI_COL_Enviar_email_cial__c == true)
        {
            strMessage = 'Buen día área de Prefactibilidad:<br></br><br></br>';
            strMessage = strMessage+'Se ha enviado una solicitud de aprobación sobre el registro de prefactibilidad'+' '+regPC.Name+'<br></br>';
            strMessage = strMessage+'correspondiente al cliente'+' '+regOp.Account.Name+' '+'con el proyecto <br></br>';
            strMessage = strMessage+regOp.Name+' '+'de Id'+' '+regOp.BI_Numero_id_oportunidad__c+', descripción del negocio'+' '+regPC.BI_COL_Descripcion_negocio__c+'<br></br><br></br>';
            strMessage = strMessage+'Por favor ingrese a Salesforce para la gestión dicha solicitud.<br></br>';
            //strMessage = strMessage+'https://na5.salesforce.com/'+regPC.Id+'<br></br><br></br>';
            //strMessage = strMessage+'www.salesforce.com';
            strMessage = strMessage+'Gracias<br></br><br></br>';
            strMessage = strMessage+strFirma;
        }
        
        //strRestpuesta = EmailSender_cls.SendEmail_ws(strMailPARA, strMailCC, strMailReplyTo, strSenderName, strSubject, strMessage, blnIsHTML);
        System.debug('\n\n strMailPARA: '+strMailPARA+'\n\n');
        System.debug('\n\n strSubject: '+strSubject+'\n\n');
        System.debug('\n\n strMessage: '+strMessage+'\n\n');
        
        strMessage=strMessage.replace( '\n' , '<br/>' );
        
        BI_COL_EmailUseful_cls emailUtil= new BI_COL_EmailUseful_cls(strMailPARA);
        emailUtil.htmlBody(strMessage)
                .senderDisplayName(strSenderName)
                .subject(strSubject)
                .useSignature(false);       
        
        if(emailUtil.sendEmail()) 
        {
            System.debug('\n\n Envio Correcto '+strMailPARA+'\n\n');
            regPC.BI_COL_Enviar_email__c = false;
            regPC.BI_COL_Enviar_email_cial__c   = false;
        }
        

    }
    
         public static void processAprobal( BI_COL_PreFactibilidad__c prefact)    
            {
                if(!Test.isRunningTest())
                {
                List<Approval.ProcessRequest> reqs = new List<Approval.ProcessRequest>(); //listado de peticiones a aprobar 
                Approval.ProcessSubmitRequest aprobacion=new Approval.ProcessSubmitRequest(); //solicitud de aprobación
                aprobacion.setComments('Registro enviado para aprobación');
                aprobacion.setObjectId(trigger.new[0].id);
                reqs.add(aprobacion);
                try{
                    
                        List<Approval.ProcessResult> result = Approval.process(reqs);
                    
                }
                catch(Exception e)
                    {
                    System.debug('------------no es posible enviar a aprobación-->'+e);
                    }
              
            }
      }

  // @InvocableMethod
  //public static void invokeapexcallout(list<BI_COL_PreFactibilidad__c> lstEmail2) 
  //{
  ////public static void invokeapexcallout(List<BI_COL_PreFactibilidad__c> regPC) 
    
  //  System.debug('lista EMAIL----------->>>>'+ lstEmail2);
  //  //System.debug('regPC----------->>>>'+ regPC);
  //  //if(!System.isFuture() && !System.isBatch())
  //  //{
  //  //  System.debug('------->Ejecución por process Builder \n\n');           
      
  //  list<BI_COL_PreFactibilidad__c> lstemail2Pre = new list<BI_COL_PreFactibilidad__c> (); 
  //    for(BI_COL_PreFactibilidad__c elem : lstEmail2)
  //    {
  //      lstemail2Pre.add(elem);
  //      System.debug('\n lstemail2Pre=====>>>'+lstemail2Pre);
  //    }
  //      BI_COL_SendingEmailPrefeasibility_cls.envioMail_mtd(lstemail2Pre);
         
  //  //}
  //}

}