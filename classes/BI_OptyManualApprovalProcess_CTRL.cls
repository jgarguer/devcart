/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Francisco Ayllon
    Company:       Salesforce.com
    Description:   Controller for VisualForce page BI_OptyManualApprovalProcess

    History:

    <Date>            <Author>          <Description>
    23/09/2015        Francisco Ayllon  Initial version
    09/10/2015        Francisco Ayllon  Fix bugs
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

public with sharing class BI_OptyManualApprovalProcess_CTRL {
    public Opportunity Oppty {get; set;}
    public Contact con {get;set;} //Lookup to get the user  
   
    public Boolean error {get;set;}
    public Boolean manual {get;set;}
    public Boolean isvalidOppty {get;set;}
    public List<String> listProcessedErrors {get;set;}
    public String buttonsName {get;set;}
    public String header {get;set;}

    public String approvalStep {get;set;}

    public BI_OptyManualApprovalProcess_CTRL(ApexPages.StandardController controller) {
        
        listProcessedErrors = new List<String>();
        error = false; //Initialize variable
        approvalStep = ''; //Initialize variable
        con = new Contact(); //Instance the object to reference the Lookup on the VF

        Oppty = (Opportunity)controller.getRecord();
            
        Oppty = [select Id, BI_ApprovalStep__c, BI_Enviar_a_aprobacion__c, BI_Manual_Approver__c from Opportunity where Id = :Oppty.Id limit 1];

        system.debug('OpptyId: '+Oppty.Id);
        system.debug('BI_Enviar_a_aprobacion__c: '+String.valueOf(Oppty.BI_Enviar_a_aprobacion__c));
        system.debug('BI_Manual_Approver__c: '+String.valueOf(Oppty.BI_Manual_Approver__c));
        if(String.isNotBlank(Oppty.BI_ApprovalStep__c)&&Oppty.BI_Enviar_a_aprobacion__c==true){
            approvalStep = Oppty.BI_ApprovalStep__c;
            system.debug('BI_ApprovalStep__c: '+Oppty.BI_ApprovalStep__c);
            isvalidOppty = true;
        }
        else{
            isvalidOppty = false;
            system.debug('BI_ApprovalStep__c: MUST BE NULL, CHECK ON WORKBENCH');
        }
        
        system.debug('isvalidOppty: '+String.valueOf(isvalidOppty));  
    }

    public void submitForApproval() {
        try{
            system.debug('LOOKUP VALUE: '+con.OwnerId);
            // Create an approval request for the Opportunity
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Aprobación enviada desde VisualForce: BI_OptyManualApprovalProcess');
            req1.setObjectId(Oppty.Id);
            if(Oppty.BI_Manual_Approver__c==true) req1.setNextApproverIds(new Id[] {con.OwnerId});

            // Submit the approval request for the Opportunity            
            Approval.ProcessResult result = Approval.process(req1);
            
            system.debug('APPROVAL PROCESS: '+result.getInstanceId()+' PARA OPTY CON ID: '+result.getEntityId()+' CON STATUS: '+result.getInstanceStatus());

            //Processing errors if neccesary
            if(!result.isSuccess()){
                error = true;
                for(Database.Error currentError: result.getErrors()){
                    String errorMsg = 'Error: '+currentError.getStatusCode()+', '+currentError.getMessage();
                    List<String> fieldsError = currentError.getFields();
                    if(!fieldsError.isEmpty()){
                        if(fieldsError.size()>1){
                            errorMsg+='. Relacionado con los campos: ';
                            for(String currentFieldError: currentError.getFields()){
                                errorMsg+=', '+currentFieldError;
                            }                       
                        }
                        else{
                            errorMsg+='. Relacionado con el campo: '+currentError.getFields()[0];
                        }                   
                    }
                    listProcessedErrors.add(errorMsg);
                }  
            }
            else{            
                Oppty.BI_ApprovalStep__c = null;
                Oppty.BI_Enviar_a_aprobacion__c = false;
            }

            BI_ApprovalHelper AP = new BI_ApprovalHelper(Oppty);
            AP.updateOppty();       
        }
        catch(Exception e){
            BI_LogHelper.generate_BILog('BI_ApprovalStatus_CTRL.submitForApproval', 'BI_EN', e, 'Trigger');
            error=true;
            if(e.getMessage().contains('ALREADY_IN_PROCESS, Cannot submit object already in process.')){
                listProcessedErrors.add('Oportunidad ya enviada a aprobación, no se puede enviar de nuevo.');
            }
            else if(e.getMessage().contains('MANAGER_NOT_DEFINED, Manager undefined.')){
                listProcessedErrors.add('Gestor no definido, no se puede enviar a aprobación.');
            }
            //Could be that the criteria doesn't met or that the user who is sending the approval doesn't have permission to do it
            else if(e.getMessage().contains('NO_APPLICABLE_PROCESS, No se ha encontrado ningún proceso aplicable.')){
                listProcessedErrors.add('No tienes permisos para enviar éste registro a aprobación o bien no existe ningún proceso que coincida con los criterios de entrada.');
            }
            else{
                listProcessedErrors.add(e.getMessage());
            }            
        }
    }

    public PageReference isManual(){
        manual = Oppty.BI_Manual_Approver__c;
        if(Oppty.BI_Manual_Approver__c==true){
            header = 'Elegir aprobador';
            buttonsName = 'Cancelar';
            return null;
        }
        else{
            header = 'Error';
            buttonsName = 'Volver';
            submitForApproval();
            return redirect();           
        }
    }

    public PageReference redirect(){
        if(error==false){
            return new ApexPages.Pagereference('/apex/BI_Opportunity_HiddenSubmitButton?id='+Oppty.Id);
        }
        else{
            return null;
        }
    }
}