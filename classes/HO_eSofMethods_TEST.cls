@isTest
private class HO_eSofMethods_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Iñaki Frial
    Company:       Accenture
    Description:   Test class to manage the coverage code for HO_eSofMethods class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    06/03/2018              Iñaki Frial             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static testMethod void updateServiceUnitsStage() {
			HO_Service_Unit__c serUnit1 = new HO_Service_Unit__c(
			HO_Status__c='Pending Service Processing'
		);
		insert serUnit1;
		HO_e_SOF__c esof1 = new HO_e_SOF__c(
			HO_Service_Unit__c= serUnit1.id,
			HO_Status__c='Accepted'
		);
		insert esof1;
		esof1.HO_Status__c='Cancelled';
		update esof1;
		
	}
}