@isTest
private class BI_ListaWrapperWSDL_TEST {

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   Author:        Fernando González
   Company:       Salesforce.com
   Description:   Test class to manage the coverage code for net classes:
   							BI_ServicioCodigoSiscelWrapper ,BI_ServicioCodigoSiscelWSDL 
   							BI_PrecioSimWrapper ,BI_PrecioSimWSDL
   							BI_PrecioModeloWrapper ,BI_PrecioModeloWSDL	
   							BI_ExisteCodigoClienteWrapper ,BI_ExisteCodigoClienteWSDL
   							BI_ListaCodigoClienteWrapper ,BI_ListaCodigoClienteWSDL 
   					   		BI_ListaPlanesClienteWrapper ,BI_ListaPlanesClienteWSDL 
   					   		BI_InformacionContactoWrapper ,BI_InformacionContactoWSDL
   					   		BI_CodigoAsociadoTelefonoWrapper ,BI_CodigoAsociadoTelefonoWSDL.		 
   
   History: 
   
   <Date>                  <Author>                <Change Description>
   20/06/2014              Ignacio Llorca          Initial Version
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   Author:        Ignacio Llorca
   Company:       Salesforce.com
   Description:   Test method to manage the code coverage for generateLog
   
   History: 
   
   <Date>                   <Author>                <Change Description>
   20/06/2014               Ignacio Llorca          Initial Version        
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void WrapperWSDLTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	
    	BI_ServicioCodigoSiscelWrapper.ServicioAsociadoSiscelInMsg codigoSiscel = new BI_ServicioCodigoSiscelWrapper.ServicioAsociadoSiscelInMsg();
        BI_ServicioCodigoSiscelWrapper.ServicioAsociadoSiscelInVO codigoSiscel2 = new BI_ServicioCodigoSiscelWrapper.ServicioAsociadoSiscelInVO();
        BI_ServicioCodigoSiscelWrapper.ServicioAsociadoSiscelOutMsg codigoSiscel3 = new BI_ServicioCodigoSiscelWrapper.ServicioAsociadoSiscelOutMsg();
        BI_ServicioCodigoSiscelWrapper.ServicioCodigoSiscelOutVO codigoSiscel4 = new BI_ServicioCodigoSiscelWrapper.ServicioCodigoSiscelOutVO();
        
        BI_ServicioCodigoSiscelWSDL.ServicioCodigoSiscelWS siscelWSDL = new BI_ServicioCodigoSiscelWSDL.ServicioCodigoSiscelWS();
    	siscelWSDL.servicioAsociadoSiscel(codigoSiscel2);
    	siscelWSDL.inputHttpHeaders_x = null;
        siscelWSDL.outputHttpHeaders_x = null;
        siscelWSDL.clientCertName_x = null;
        siscelWSDL.clientCert_x = null;
        siscelWSDL.clientCertPasswd_x = null;
        siscelWSDL.timeout_x = null;
    	
    	BI_PrecioModeloWrapper.PrecioModeloInMsg precioModelo1 = new BI_PrecioModeloWrapper.PrecioModeloInMsg();
    	BI_PrecioModeloWrapper.PrecioModeloInVO precioModelo2 = new BI_PrecioModeloWrapper.PrecioModeloInVO();
    	BI_PrecioModeloWrapper.PrecioModeloOutMsg precioModelo3 = new BI_PrecioModeloWrapper.PrecioModeloOutMsg();
    	BI_PrecioModeloWrapper.PrecioModeloOutVO precioModelo4 = new BI_PrecioModeloWrapper.PrecioModeloOutVO();
    	
    	BI_PrecioModeloWSDL.PrecioModeloWS precioModeloWSDL = new BI_PrecioModeloWSDL.PrecioModeloWS();
    	precioModeloWSDL.precioModelo(precioModelo2);
    	precioModeloWSDL.inputHttpHeaders_x = null;
        precioModeloWSDL.outputHttpHeaders_x = null;
        precioModeloWSDL.clientCertName_x = null;
        precioModeloWSDL.clientCert_x = null;
        precioModeloWSDL.clientCertPasswd_x = null;
        precioModeloWSDL.timeout_x = null;
    	
    	BI_ExisteCodigoClienteWrapper.ExisteCodigoClienteInMsg existeCodigo1 = new BI_ExisteCodigoClienteWrapper.ExisteCodigoClienteInMsg();
    	BI_ExisteCodigoClienteWrapper.ExisteCodigoClienteInVO existeCodigo2 = new BI_ExisteCodigoClienteWrapper.ExisteCodigoClienteInVO();
    	BI_ExisteCodigoClienteWrapper.ExisteCodigoClienteOutMsg existeCodigo3 = new BI_ExisteCodigoClienteWrapper.ExisteCodigoClienteOutMsg();
    	BI_ExisteCodigoClienteWrapper.ExisteCodigoClienteOutVO existeCodigo4 = new BI_ExisteCodigoClienteWrapper.ExisteCodigoClienteOutVO();
    	
    	BI_ExisteCodigoClienteWSDL.ExisteCodigoClienteWS existeCodigoWSDL = new BI_ExisteCodigoClienteWSDL.ExisteCodigoClienteWS();
    	existeCodigoWSDL.existeCodigoCliente(existeCodigo2);
    	existeCodigoWSDL.inputHttpHeaders_x = null;
        existeCodigoWSDL.outputHttpHeaders_x = null;
        existeCodigoWSDL.clientCertName_x = null;
        existeCodigoWSDL.clientCert_x = null;
        existeCodigoWSDL.clientCertPasswd_x = null;
        existeCodigoWSDL.timeout_x = null;
    	
        BI_ListaCodigoClienteWrapper.CodigoVO codigoWrapper1 = new BI_ListaCodigoClienteWrapper.CodigoVO();
        BI_ListaCodigoClienteWrapper.ListaCodigoClienteInVO codigoWrapper2 = new BI_ListaCodigoClienteWrapper.ListaCodigoClienteInVO();
        BI_ListaCodigoClienteWrapper.ListaCodigoClienteOutMsg codigoWrapper3 = new BI_ListaCodigoClienteWrapper.ListaCodigoClienteOutMsg();
        BI_ListaCodigoClienteWrapper.ListaCodigoClienteInMsg codigoWrapper4 = new BI_ListaCodigoClienteWrapper.ListaCodigoClienteInMsg();
        BI_ListaCodigoClienteWrapper.ListaCodigoClienteOutVO codigoWrapper5 = new BI_ListaCodigoClienteWrapper.ListaCodigoClienteOutVO();
        
        BI_ListaCodigoClienteWSDL.ListaCodigoClienteWS codigoWSDL = new BI_ListaCodigoClienteWSDL.ListaCodigoClienteWS();
        codigoWSDL.obtenerListaCodCliente(codigoWrapper2);
        codigoWSDL.inputHttpHeaders_x = null;
        codigoWSDL.outputHttpHeaders_x = null;
        codigoWSDL.clientCertName_x = null;
        codigoWSDL.clientCert_x = null;
        codigoWSDL.clientCertPasswd_x = null;
        codigoWSDL.timeout_x = null;
        
        BI_ListaPlanesClienteWrapper.ListaPlanesClienteOutMsg planesWrapper1 = new BI_ListaPlanesClienteWrapper.ListaPlanesClienteOutMsg();
        BI_ListaPlanesClienteWrapper.ListaPlanesClienteInVO planesWrapper2 = new BI_ListaPlanesClienteWrapper.ListaPlanesClienteInVO();
        BI_ListaPlanesClienteWrapper.ListaPlanesClienteOutVO planesWrapper3 = new BI_ListaPlanesClienteWrapper.ListaPlanesClienteOutVO();
        BI_ListaPlanesClienteWrapper.ListaPlanesClienteInMsg planesWrapper4 = new BI_ListaPlanesClienteWrapper.ListaPlanesClienteInMsg();
        BI_ListaPlanesClienteWrapper.PlanVO planesWrapper5 = new BI_ListaPlanesClienteWrapper.PlanVO();
        
        BI_ListaPlanesClienteWSDL.ListaPlanesClienteWS planesWSDL = new BI_ListaPlanesClienteWSDL.ListaPlanesClienteWS();
        planesWSDL.listaPlanesCliente(planesWrapper2);
        planesWSDL.inputHttpHeaders_x = null;
        planesWSDL.outputHttpHeaders_x = null;
        planesWSDL.clientCertName_x = null;
        planesWSDL.clientCert_x = null;
        planesWSDL.clientCertPasswd_x = null;
        planesWSDL.timeout_x = null;
        
        BI_InformacionContactoWrapper.InformacionContactoInMsg infoContacto1 = new BI_InformacionContactoWrapper.InformacionContactoInMsg();
        BI_InformacionContactoWrapper.InformacionContactoInVO infoContacto2 = new BI_InformacionContactoWrapper.InformacionContactoInVO();
        BI_InformacionContactoWrapper.InformacionContactoOutMsg infoContacto3 = new BI_InformacionContactoWrapper.InformacionContactoOutMsg();
        BI_InformacionContactoWrapper.InformacionContactoOutVO infoContacto4 = new BI_InformacionContactoWrapper.InformacionContactoOutVO();
        
        BI_InformacionContactoWSDL.InformacionContactoWS infoContactoWSDL = new BI_InformacionContactoWSDL.InformacionContactoWS();
        infoContactoWSDL.informacionContacto(infoContacto2);
        infoContactoWSDL.inputHttpHeaders_x = null;
        infoContactoWSDL.outputHttpHeaders_x = null;
        infoContactoWSDL.clientCertName_x = null;
        infoContactoWSDL.clientCert_x = null;
        infoContactoWSDL.clientCertPasswd_x = null;
        infoContactoWSDL.timeout_x = null;
        
        BI_CodigoAsociadoTelefonoWrapper.CodigoAsociadoTelefonoInMsg codigoTelef1 = new  BI_CodigoAsociadoTelefonoWrapper.CodigoAsociadoTelefonoInMsg();
		BI_CodigoAsociadoTelefonoWrapper.CodigoAsociadoTelefonoInVO codigoTelef2 = new BI_CodigoAsociadoTelefonoWrapper.CodigoAsociadoTelefonoInVO();
		BI_CodigoAsociadoTelefonoWrapper.CodigoAsociadoTelefonoOutMsg codigoTelef3 = new BI_CodigoAsociadoTelefonoWrapper.CodigoAsociadoTelefonoOutMsg();
		BI_CodigoAsociadoTelefonoWrapper.CodigoAsociadoTelefonoOutVO codigoTelef4 = new BI_CodigoAsociadoTelefonoWrapper.CodigoAsociadoTelefonoOutVO();
		
        BI_CodigoAsociadoTelefonoWSDL.CodigoAsociadoTelefonoWS codigoTelefWSDL = new  BI_CodigoAsociadoTelefonoWSDL.CodigoAsociadoTelefonoWS();
        codigoTelefWSDL.codigoAsociadoTelefono(codigoTelef2);
       	codigoTelefWSDL.inputHttpHeaders_x = null;
        codigoTelefWSDL.outputHttpHeaders_x = null;
        codigoTelefWSDL.clientCertName_x = null;
        codigoTelefWSDL.clientCert_x = null;
        codigoTelefWSDL.clientCertPasswd_x = null;
        codigoTelefWSDL.timeout_x = null;
        
        List<BI_Log__c> lst_log = [select Id from BI_Log__c];
        system.assert(lst_log.isEmpty());
        
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}