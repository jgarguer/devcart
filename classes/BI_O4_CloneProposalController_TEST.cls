/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Test class for BI_O4_CloneProposalController class
 Test Class:    
 
 History:
	
 <Date>              <Author>                   <Change Description>
 28/08/2016          Fernando Arteaga           Initial Version
 20/09/2016					 Ricardo Pereira						Added exceptions()
20/09/2017                       Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
 25/09/2017		     Jaime Regidor		   	    Fields BI_Subsector__c and BI_Sector__c added
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_CloneProposalController_TEST {
	
	static{
		BI_TestUtils.throw_exception = false;
	}
	
	@isTest static void test_method_one() {

		RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' Limit 1];

		List<Account> lst_acc1 = new List<Account>();       
				Account acc1 = new Account(Name = 'test',
											BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
											BI_Activo__c = Label.BI_Si,
											BI_Segment__c = 'Empresas',
											BI_Subsector__c = 'test', //25/09/2017
											BI_Sector__c = 'test', //25/09/2017
											BI_Subsegment_local__c = 'Top 1',
											RecordTypeId = rt1.Id,
										    BI_Subsegment_Regional__c = 'test',
												BI_Territory__c = 'test'
											);    
				lst_acc1.add(acc1);
				insert lst_acc1;

		List<Account> lst_acc = new List<Account>();       
				Account acc = new Account(Name = 'test',
											BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
											BI_Activo__c = Label.BI_Si,
											BI_Segment__c = 'Empresas',
											BI_Subsector__c = 'test', //25/09/2017
											BI_Sector__c = 'test', //25/09/2017
											BI_Subsegment_local__c = 'Top 1',
											ParentId = lst_acc1[0].Id,
											BI_Subsegment_Regional__c = 'test',
										    BI_Territory__c = 'test'
											);    
				lst_acc.add(acc);

				User currentUser = BI_DataLoadAgendamientos.loadValidAdministratorUser();
   				System.runAs(currentUser){
				insert lst_acc;
				}

				RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

				Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
																					CloseDate = Date.today(),
																					StageName = Label.BI_F6Preoportunidad,
																					AccountId = lst_acc[0].Id,
																					BI_Ciclo_ventas__c = Label.BI_Completo, 
																					RecordTypeId = rt.Id
																					);    
				insert opp;

		Quote proposal = new Quote();
		proposal.Name = 'Test';
		proposal.OpportunityId = opp.Id;

		Test.startTest();
		insert proposal;

		ApexPages.StandardController standardC = new ApexPages.StandardController(proposal); 
		BI_O4_CloneProposalController controller = new BI_O4_CloneProposalController(standardC);

			controller.cloneProposal();
			controller.cancel();
			System.assertNotEquals(null, proposal.Id);
			Test.stopTest();
	}	

	@isTest static void exceptions() {

		BI_TestUtils.throw_exception = true;

		test_method_one();
	}

}