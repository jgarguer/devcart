@isTest(seeAllData = false)
public class BIIN_Obtener_Contacto_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José María Martín
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_Obtener_Contacto_WS
    
    <Date>             <Author>                        <Change Description>
    11/2015            José María Martín               Initial Version
    12/05/2016         José Luis González Beltrán      Adapt TEST to UNICA interface modifications in tested class
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
    private final static String LE_NAME = 'TestAccount_Obtener_Contacto_TEST';

    @testSetup static void dataSetup() 
    {
        Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert account;
    }

    @isTest static void test_BIIN_Obtener_Contacto_TEST() {
        String authorizationToken = 'OBTENER_CONTACTO';
        
        Test.startTest(); 

        BI_TestUtils.throw_exception = false;
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerContacto'));  
        BIIN_Obtener_Contacto_WS ceController = new BIIN_Obtener_Contacto_WS();
        Account account = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
        ceController.getContact(authorizationToken, 'nombre', 'apellido', 'email', account.Id);
            
        Test.stopTest();
    }

    @isTest static void test_BIIN_Obtener_Contacto_TEST_FAIL() {
        String authorizationToken = 'OBTENER_CONTACTO';
        
        Test.startTest(); 

        BI_TestUtils.throw_exception = true;
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerContacto'));  
        BIIN_Obtener_Contacto_WS ceController = new BIIN_Obtener_Contacto_WS();
        Account account = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
        ceController.getContact(authorizationToken, 'nombre', 'apellido', 'email', account.Id);
            
        Test.stopTest();
    }

    @isTest static void test_BIIN_Obtener_Contacto_Create_TEST() {
        String authorizationToken = 'OBTENER_CONTACTO';
        
        Test.startTest(); 

        BI_TestUtils.throw_exception = false;
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionContacto'));  
        BIIN_Obtener_Contacto_WS ceController = new BIIN_Obtener_Contacto_WS();
        Account account = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
        ceController.crearContacto('nombre', 'apellido', 'email', 'telefono', 'movil', account.Id);
            
        Test.stopTest();
    }

    @isTest static void test_BIIN_Obtener_Contacto_Create_TEST_FAIL() {
        String authorizationToken = 'OBTENER_CONTACTO';
        
        Test.startTest(); 
        
        BI_TestUtils.throw_exception = true;
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionContacto'));  
        BIIN_Obtener_Contacto_WS ceController = new BIIN_Obtener_Contacto_WS();
        Account account = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
        ceController.crearContacto('nombre', 'apellido', 'email', 'telefono', 'movil', account.Id);
            
        Test.stopTest();
    }
}