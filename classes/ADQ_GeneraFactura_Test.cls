@isTest
private class ADQ_GeneraFactura_Test {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julio Ortega
    Company:       Adquira México
    Description:   Apex TestClass Da Cobertura a clase ADQ_GeneraFactura_Recurrente
    
    History:
    
    <Date>            <Author>              <Description>
    09/26/2014        Julio Ortega         Initial version
    20/07/2015		  Julio Ortega 		   Generar Facturas Recurrente y Cargos Únicos  unificado
    13/06/2017        Guillermo Muñoz      Deprecated references to Raz_n_social__c (Optimización)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	@isTest static void ADQ_GeneraFactura_Recurrente_test(){
		//JG Migracion a UATFIX
		
		List<NE__OrderItem__c> lisinertNEorder = new List<NE__OrderItem__c> ();
		List<NE__OrderItem__c> listaOriginal = new List<NE__OrderItem__c> ();
		Map<String,Pedido_PE__c> mapPedidoPE = new Map<String,Pedido_PE__c> ();
		//List<wrapperProductos> lsWrapperProductos = new List<wrapperProductos>  ();
		ADQ_GeneraFactura_Recurrente testClass01 = new ADQ_GeneraFactura_Recurrente ();
		//ADQ_EncabezadoFacturacion testClass02 =  new ADQ_EncabezadoFacturacion ();

			
		Product2 prod = new Product2(Name = 'Test Prod', 
           							 Family = 'Test family', CurrencyIsoCode = 'EUR');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, CurrencyIsoCode = 'EUR', Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
                
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true, CurrencyIsoCode = 'EUR');
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id,
                                                    UnitPrice = 12000, CurrencyIsoCode = 'EUR', IsActive = true);
        insert customPrice;

			
			Account acc01  = new Account ();
				acc01.Name = 'Cuenta de prueba JMO';
				acc01.BI_Segment__c = 'test';
				acc01.BI_Subsegment_Regional__c = 'test';
				acc01.BI_Territory__c = 'test';
				//acc01.Raz_n_social__c = 'Cuenta de prueba SA CV';
				acc01.BI_Country__c = Label.BI_Mexico;
			insert acc01;

		Test.setCurrentPageReference(new PageReference('Page.ADQ_DefinicionFactura'));
		System.currentPageReference().getParameters().put('accid', acc01.Id);
			
			Opportunity opp01 = new Opportunity ();	
				opp01.BI_Id_de_la_Oportunidad__c = '0067000000ZA5XF=1';
				opp01.Name = 'ENLACE INTERNET DEDICADO PLANTA';
				opp01.AccountId = acc01.Id;
				opp01.BI_Country__c = Label.BI_Mexico;
				opp01.BI_Ciclo_ventas__c = 'Completo';
				opp01.CloseDate = date.today();
				opp01.StageName = 'F1 - Ganada';
				opp01.CurrencyIsoCode = 'MXN';
				opp01.BI_Duracion_del_contrato_Meses__c = 36;
				opp01.BI_Ingreso_por_unica_vez__c = 0.0;
				opp01.BI_Recurrente_bruto_mensual__c = 13390.00;
				opp01.BI_Opportunity_Type__c = 'Fijo';
				opp01.Amount = 482040;
			insert opp01;	
			

			NE__Order__c NEORDER =  new NE__Order__c(); 
				NEORDER.NE__OptyId__c = opp01.Id;
				NEORDER.NE__AccountId__c = acc01.Id;
				insert NEORDER;
			
			
			NE__OrderItem__c NEInsert2  = new NE__OrderItem__c ();
				NEInsert2.NE__OrderId__c = NEORDER.id; 
				NEInsert2.CurrencyIsoCode = 'USD';
				NEInsert2.NE__OneTimeFeeOv__c = 10;
				NEInsert2.NE__RecurringChargeOv__c = 100;
				NEInsert2.Fecha_de_reasignaci_n_a_factura__c = date.today();
				NEInsert2.NE__Qty__c = 1;
			insert NEInsert2;
			

		Opportunity opp02 = new Opportunity ();	
				opp02.BI_Id_de_la_Oportunidad__c = '0067000000ZA5XF-2';
				opp02.Name = 'ENLACE INTERNET DEDICADO PLANTA';
				opp02.AccountId = acc01.Id;
				opp02.BI_Country__c = Label.BI_Mexico;
				opp02.BI_Ciclo_ventas__c = 'Completo';
				opp02.CloseDate = date.today();
				opp02.StageName = 'F1 - Ganada';
				opp02.CurrencyIsoCode = 'MXN';
				opp02.BI_Duracion_del_contrato_Meses__c = 1;
				opp02.BI_Ingreso_por_unica_vez__c = 0.0;
				opp02.BI_Recurrente_bruto_mensual__c = 13390.00;
				opp02.BI_Opportunity_Type__c = 'Fijo';
				opp02.Amount = 482040;
			insert opp02;	
			

			NE__Order__c Norder2 =  new NE__Order__c(); 
				Norder2.NE__OptyId__c = opp02.Id;
				Norder2.NE__AccountId__c = acc01.Id;
				insert Norder2;
			
			
			NE__OrderItem__c NEInsert3  = new NE__OrderItem__c ();
				NEInsert3.NE__OrderId__c = Norder2.id; 
				NEInsert3.CurrencyIsoCode = 'USD';
				NEInsert3.NE__RecurringChargeOv__c = 0;
				NEInsert3.NE__OneTimeFeeOv__c = 100;
				NEInsert3.NE__Qty__c = 1;
				NEInsert3.Fecha_de_reasignaci_n_a_factura__c = date.today();
			insert NEInsert3;


			lisinertNEorder.add (NEInsert2);
			lisinertNEorder.add (NEInsert3);
			listaOriginal.add(NEInsert2);
			listaOriginal.add(NEInsert3);
			
			
			Factura__c Factura = new Factura__c ();
				Factura.Activo__c = true;
				Factura.Cliente__c = acc01.Id;
				Factura.Fecha_Inicio__c = date.newinstance(2015, 2, 17);
				Factura.Fecha_Fin__c = Date.today() + 20;
				Factura.Inicio_de_Facturaci_n__c = Date.today();
				Factura.IVA__c = '16%';
				Factura.Sociedad_Facturadora__c = 'GTM';
				Factura.Requiere_Anexo__c = 'SI';
				Factura.CurrencyIsoCode = 'USD';
				Factura.Tipo_de_Factura__c = 'Anticipado'; 
				Factura.Periodos_para_Facturar__c = '1';
				//RecordType tiporegistro = [SELECT Id FROM RecordType where  SobjectType = 'Factura__c' and DeveloperName  ='Recurrente' limit 1];
				Factura.RecordTypeId =Schema.SObjectType.Factura__c.getRecordTypeInfosByName().get('Recurrente').getRecordTypeId();
			insert Factura;
			
				Factura.Sociedad_Facturadora__c = 'PCS';
			//update  Factura;
			
			Map<String,Programacion__c> mapProgramacion  = new Map<String,Programacion__c> ();
			Programacion__c programacion = new Programacion__c ();
				programacion.Factura__c = Factura.Id;
				programacion.CurrencyIsoCode = 'MXN';
				programacion.Sociedad_Facturadora_Real__c = 'GTM';
				programacion.Facturado__c = false;
				programacion.Fecha__c = Date.today();
				programacion.Inicio_del_per_odo__c = date.newinstance(2015, 2, 17);
				programacion.Fin_del_per_odo__c = Date.today().addMonths(1);
				programacion.Fecha_de_inicio_de_cobro__c = Date.today().addDays(2);
				programacion.Numero_de_factura_SD__c = null;
				programacion.Folio_fiscal__c = null;
				
				
			insert programacion;
			
			mapProgramacion.put(programacion.Id,programacion);
			
			Pedido_PE__c  p = new Pedido_PE__c ();
                p.Producto_en_Sitio__c = NEInsert2.Id;
                p.CurrencyIsoCode = NEInsert2.CurrencyIsoCode;
                p.Precio_original_EQS__c = Double.valueOf(''+NEInsert2.NE__RecurringChargeOv__c);
                p.Precio_original__c = 3999.98;
                p.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                p.Precio_convertido__c = 8990.998;
                p.Inicio_del_per_odo__c = mapProgramacion.get(programacion.Id).Inicio_del_per_odo__c;
                p.Fin_del_per_odo__c = mapProgramacion.get(programacion.Id).Fin_del_per_odo__c;
    			p.Lleva_IEPS__c = false; 
                p.IEPS__c = (p.Lleva_IEPS__c)? Double.valueOf(''+p.Precio_convertido__c)*0.03:0;
	        insert p;


	        
	         Factura_OLI__c f = new Factura_OLI__c();
                f.Factura__c = Factura.Id;
               // f.Commercial_Product__c = ne01.NE__ProdId__c; // relacionamos con el producto comercial
                f.Cantidad__c = 1;
                f.CurrencyIsoCode = 'USD';
                f.Descuento__c = 0.2 * 100;
                f.Plazo__c = 36;  
                f.Precio_Unitario__c = 800.80;
                f.Configuration_Item__c = NEInsert3.Id;
                
          	insert f;
                
                
             Factura__c Factura2 = new Factura__c ();
				Factura2.Activo__c = true;
				Factura2.Cliente__c = acc01.Id;
				Factura2.Fecha_Inicio__c = date.newinstance(2015, 2, 1);
				Factura2.Fecha_Fin__c = Date.today() + 20;
				Factura2.Inicio_de_Facturaci_n__c = Date.today();
				Factura2.IVA__c = '16%';
				Factura2.Sociedad_Facturadora__c = 'GTM';
				Factura2.Requiere_Anexo__c = 'SI';
				Factura2.CurrencyIsoCode = 'USD';
				Factura2.Tipo_de_Factura__c = 'Anticipado'; 
				Factura.Periodos_para_Facturar__c = '1';
				//RecordType tiporegistro = [SELECT Id FROM RecordType where  SobjectType = 'Factura__c' and DeveloperName  ='Recurrente' limit 1];
				Factura2.RecordTypeId =Schema.SObjectType.Factura__c.getRecordTypeInfosByName().get('Recurrente').getRecordTypeId();
			insert Factura2;
			
				
			//Map<String,Programacion__c> mapProgramacion  = new Map<String,Programacion__c> ();
			Programacion__c programacion2 = new Programacion__c ();
				programacion2.Factura__c = Factura.Id;
				programacion2.CurrencyIsoCode = 'MXN';
				programacion2.Sociedad_Facturadora_Real__c = 'GTM';
				programacion2.Facturado__c = false;
				programacion2.Fecha__c = Date.today();
				programacion2.Inicio_del_per_odo__c =date.newinstance(2015, 2, 1);
				programacion2.Fin_del_per_odo__c = Date.today().addMonths(1);
				programacion2.Fecha_de_inicio_de_cobro__c = Date.today().addDays(2);
				programacion2.Numero_de_factura_SD__c = null;
				programacion2.Folio_fiscal__c = null;
				
				
			insert programacion2;

			Programacion__c programacion3 = new Programacion__c ();
				programacion3.Factura__c = Factura.Id;
				programacion3.CurrencyIsoCode = 'MXN';
				programacion3.Sociedad_Facturadora_Real__c = 'GTM';
				programacion3.Facturado__c = false;
				programacion3.Fecha__c = Date.today();
				programacion3.Inicio_del_per_odo__c =date.newinstance(2015, 1, 1);
				programacion3.Fin_del_per_odo__c = date.newinstance(2015, 1, 31);
				programacion3.Fecha_de_inicio_de_cobro__c = date.newinstance(2015, 1, 1);
				programacion3.Numero_de_factura_SD__c = null;
				programacion3.Folio_fiscal__c = null;
			
				
			insert programacion3;
			
			mapProgramacion.put(programacion2.Id,programacion2);
			mapProgramacion.put(programacion3.Id,programacion3);
			
			Pedido_PE__c  PE2 = new Pedido_PE__c ();
                PE2.Producto_en_Sitio__c = NEInsert2.Id;
                PE2.CurrencyIsoCode = NEInsert2.CurrencyIsoCode;
                PE2.Precio_original_EQS__c = Double.valueOf(''+NEInsert2.NE__RecurringChargeOv__c);
                PE2.Precio_original__c = 3999.98;
                PE2.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                PE2.Precio_convertido__c = 8990.998;
                PE2.Inicio_del_per_odo__c = mapProgramacion.get(programacion.Id).Inicio_del_per_odo__c;
                PE2.Fin_del_per_odo__c = mapProgramacion.get(programacion.Id).Fin_del_per_odo__c;
    			PE2.Lleva_IEPS__c = false; 
                PE2.IEPS__c = (PE2.Lleva_IEPS__c)? Double.valueOf(''+PE2.Precio_convertido__c)*0.03:0;
	        insert PE2;   

	        Pedido_PE__c  PE3 = new Pedido_PE__c ();
	            PE3.Producto_en_Sitio__c = NEInsert3.Id;
                PE3.CurrencyIsoCode = NEInsert3.CurrencyIsoCode;
                PE3.Precio_original_EQS__c = Double.valueOf(''+NEInsert3.NE__RecurringChargeOv__c);
                PE3.Precio_original__c = 3999.98;
                PE3.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                PE3.Precio_convertido__c = 8990.998;
                PE3.Inicio_del_per_odo__c = mapProgramacion.get(programacion.Id).Inicio_del_per_odo__c;
                PE3.Fin_del_per_odo__c = mapProgramacion.get(programacion.Id).Fin_del_per_odo__c;
    			PE3.Lleva_IEPS__c = false; 
                PE3.IEPS__c = (PE3.Lleva_IEPS__c)? Double.valueOf(''+PE3.Precio_convertido__c)*0.03:0;
	        insert PE3;   
                
	        
	        mapPedidoPE.put (p.Id, p);
			testClass01.factura = Factura;

			
			//testClass01.cliente = acc01.Id;
			//try{
			// Varibeles 
			testClass01.idCta = ''+acc01.Id + '';
			testClass01.Tipo_de_Factura = 'Anticipado';
			testClass01.idCuenta = ''+acc01.Id + '';
			
			Boolean seleccionada = true;
			ADQ_GeneraFactura_Recurrente.wrapperProductos newWrapper3 = new ADQ_GeneraFactura_Recurrente.wrapperProductos(NEInsert3, seleccionada  );
			ADQ_GeneraFactura_Recurrente.wrapperProductos newWrapper2 = new ADQ_GeneraFactura_Recurrente.wrapperProductos(NEInsert2, seleccionada  );
			List<ADQ_GeneraFactura_Recurrente.wrapperProductos> lsWrapperProductos = new List<ADQ_GeneraFactura_Recurrente.wrapperProductos> ();  
			
			
			testClass01.buscarProductos ();	
			testClass01.queryProductosIEPS();
			testClass01.cancelar();
			testClass01.continuar();
			testClass01.crearFactura();
			testClass01.crearFacturaCU();
			
			testClass01.updateNEOrderItem (lisinertNEorder);
			
			Integer plazo24 =24;
			String monedaUSD = 'USD';
			testClass01.CrearProgramaciones(plazo24,  monedaUSD);
			
			Integer plazo1 =1;
			String monedaMXN = 'MXN';
			testClass01.CrearProgramaciones(plazo1,  monedaMXN); 
			
			testClass01.queryPrgramacionesCreadas();
			testClass01.queryPedidosPECreadas();
			//testClass01.creaProgramacionCurrencyType (mapProgramacion);
			
			testClass01.factura = Factura;
			
				testClass01.crearPedidosPE(lisinertNEorder,  mapProgramacion );
				//testClass01.crearPedidosPE(listaOriginal,  mapProgramacion );		
			 
			
			
			
			testClass01.crearFacturaSENS (lisinertNEorder);
			
			testClass01.creaPedio (mapPedidoPE );
			
			testClass01.generaListaFacturasOLI  (lisinertNEorder);
			
			Double importe = 300.90;
			//Programacion__c prog = new Programacion__c ();
			testClass01.calculaFraccionImporte(importe, programacion);


			 //testClass01.cubre ();
			
			//NE__OrderItem__c neOrderITEMParam  = new NE__OrderItem__c ();
			
			
			//testClass02.inicio();
			//}catch(Exception err){
				
			//}
			
	}
	

}