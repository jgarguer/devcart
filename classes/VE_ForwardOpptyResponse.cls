public class VE_ForwardOpptyResponse {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Suarez
    Company:       Salesforce.com
    Description:   Model class for the Forward Opportunity response.
    
    History:
    
    <Date>            <Author>          <Description>
    02/02/2016        Javier Suarez      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public Opportunity o {get; set;}
    public String m {get; set;}
    
}