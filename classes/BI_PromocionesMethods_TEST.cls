@isTest
private class BI_PromocionesMethods_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Ignacio Llorca
	Company:       Salesforce.com
	Description:   Test class to manage the coverage code for BI_PromocionesMethods class 
	
	History: 
	
	<Date> 					<Author> 				<Change Description>
	15/04/2014      		Ignacio Llorca     		Initial Version
	21/04/2014				Pablo Oliva				Methods 3 and 4 added
	22/04/2014				Pablo Oliva				Method 5 added
	05/06/2014				Pablo Oliva				General: A new user is created for inserting campaigns
	21/10/2014				Pablo Oliva				Number of records decreased in order to avoid problems with limits
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	    
   	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
   	Author:        Ignacio Llorca
   	Company:       Salesforce.com
   	Description:   Test method to manage the code coverage for preventUpdatePromotion. 
   
   	History: 
   
   	<Date> 					<Author> 				<Change Description>
   	29/04/2014      		Ignacio Llorca     		Initial Version        
   	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void preventUpdatePromotion() {
    	/////////////////////////////////////////////////////
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    							Integer K = 50;
    	////////////////////////////////////////////////////
    
        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
         
        List<Contact> lst_con = BI_DataLoad.loadContacts(1, lst_acc);
       
        List<Lead> lst_ld = BI_DataLoad.loadLeads(1);
        
        List <Campaign> lst_camp = new List<Campaign>();
        String sysAdmin = 'System Administrator';
        String en_US = 'en_US';
        
        Profile prof = [select Id from Profile where Name = :sysAdmin OR Name = :Label.BI_Administrador limit 1];
        
		User user = new User(alias = 'testu',
						  email = 'tu@testorg.com',
						  emailencodingkey = 'UTF-8',
						  lastname = 'Test',
						  languagelocalekey = en_US,
						  localesidkey = en_US,
						  ProfileId = prof.Id,
						  BI_Permisos__c = Label.BI_Administrador,
						  timezonesidkey = Label.BI_TimeZoneLA,
						  username = 'usertestuser@testorg.com');
		
		insert user;
		
		system.runAs(user){
			lst_camp = BI_DataLoad.loadCampaignsCatalog(K);
		}
       
        List<CampaignMember> lst_cm = BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);
        
        List <BI_Promociones__c> lst_promociones =BI_DataLoad.loadPromociones(2, lst_camp);
       
    	for (Campaign camp:lst_camp){
    		camp.Status = Label.BI_EnAprobacion;
     	} 
    	
    	Test.StartTest();
    	
    	update lst_camp;
    	
		/*for(BI_Promociones__c p2list:lst_promociones)
		    p2list.BI_Comentarios__c = 'Test Coment';*/
		
		try{
			update lst_promociones;	
    		system.assert(false);
    	} catch (DmlException e){
    		system.assert(e.getMessage().contains(Label.BI_Trigger_Promociones_preventUpdateDelete));
    	}
    	
    	Test.StopTest();
           
    }
    
        
   	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
   	Author:        Ignacio Llorca
   	Company:       Salesforce.com
   	Description:   Test method to manage the code coverage for preventDeletePromotion.
   
   	History: 
   
   	<Date> 					<Author> 				<Change Description>
   	29/04/2014      		Ignacio Llorca     		Initial Version        
   	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void preventDeletePromotion() {
    
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
         
        List<Contact> lst_con = BI_DataLoad.loadContacts(1, lst_acc);
       
        List<Lead> lst_ld = BI_DataLoad.loadLeads(1);
        
        List <Campaign> lst_camp = new List<Campaign>();
        
        Id idProfile = BI_DataLoad.searchAdminProfile();
        
        String en_US = 'en_US';
        
        String aprovedStatus = Label.BI_EnAprobacion;
                              
		User user = new User(alias = 'testu',
						  email = 'tu@testorg.com',
						  emailencodingkey = 'UTF-8',
						  lastname = 'Test',
						  languagelocalekey = en_US,
						  localesidkey = en_US,
						  ProfileId = idProfile,
						  BI_Permisos__c = Label.BI_Administrador,
						  timezonesidkey = Label.BI_TimeZoneLA,
						  username = 'usertestuser@testorg.com');
		
		insert user;
		
		system.runAs(user){
			lst_camp = BI_DataLoad.loadCampaignsCatalog(5);
		}
       
        List<CampaignMember> lst_cm = BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);
        
        List <BI_Promociones__c> lst_promociones =BI_DataLoad.loadPromociones(5, lst_camp);
        
        for (Campaign camp:lst_camp){
    		camp.Status = aprovedStatus;
    	}
    	
    	Test.StartTest();
    	
    	update lst_camp;
        
        try{
    		delete lst_promociones;
    		system.assert(false);
    	} catch (DmlException e){
    		system.assert (e.getMessage().contains(Label.BI_Trigger_Promociones_preventUpdateDelete));
    	}
    	
    	Test.StopTest();
    }
    
        
   	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
   	Author:        Ignacio Llorca
   	Company:       Salesforce.com
   	Description:   Test method to manage the code coverage for preventUndelete.
   
   	History: 
   
   	<Date> 					<Author> 				<Change Description>
   	29/04/2014      		Ignacio Llorca     		Initial Version        
   	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void preventUndelete() {
    	////////////////////////////////////////////////
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    						Integer K = 10;
    	///////////////////////////////////////////////
    
        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
         
        List<Contact> lst_con = BI_DataLoad.loadContacts(1, lst_acc);
       
        List<Lead> lst_ld = BI_DataLoad.loadLeads(1);
        
        List <Campaign> lst_camp = new List<Campaign>();
        
        Id idProfile = BI_DataLoad.searchAdminProfile();
            
        String en_US = 'en_US';
        
                          
		User user = new User(alias = 'testu',
						  email = 'tu@testorg.com',
						  emailencodingkey = 'UTF-8',
						  lastname = 'Test',
						  languagelocalekey = en_US,
						  localesidkey = en_US,
						  ProfileId = idProfile,
						  BI_Permisos__c = Label.BI_Administrador,
						  timezonesidkey = Label.BI_TimeZoneLA,
						  username = 'usertestuser@testorg.com');
		
		insert user;
		
		system.runAs(user){
			lst_camp = BI_DataLoad.loadCampaignsCatalog(K);
		}
       
        List<CampaignMember> lst_cm = BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);
        
        List <BI_Promociones__c> lst_promociones =BI_DataLoad.loadPromociones(2, lst_camp);
       
        Test.StartTest();
        
        delete lst_promociones;
        
        for (Campaign camp:lst_camp){
        	camp.Status =  Label.BI_EnCurso;
        }
        update lst_camp;
       
    	try{
    		undelete lst_promociones;
    		system.assert(false);
    	} catch (DmlException e){
    		system.assert(e.getMessage().contains(Label.BI_Trigger_Promociones_preventUndelete));
    	}
    	
    	Test.StopTest();
        
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for changeCurrencyAndValidateEconomic method.

    History: 
    
     <Date>                     <Author>                <Change Description>
    24/11/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void changeCurrencyAndValidateEconomic_TEST(){
    /////////////////////////////////////////////////////
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
                  Integer K = 50;
      ////////////////////////////////////////////////////
    
        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
         
        List<Contact> lst_con = BI_DataLoad.loadContacts(1, lst_acc);
       
        List<Lead> lst_ld = BI_DataLoad.loadLeads(1);
        
        List <Campaign> lst_camp = new List<Campaign>();
        String sysAdmin = 'System Administrator';
        String en_US = 'en_US';
        
        Profile prof = [select Id from Profile where Name = :sysAdmin OR Name = :Label.BI_Administrador limit 1];
        
    User user = new User(alias = 'testu',
              email = 'tu@testorg.com',
              emailencodingkey = 'UTF-8',
              lastname = 'Test',
              languagelocalekey = en_US,
              localesidkey = en_US,
              ProfileId = prof.Id,
              BI_Permisos__c = Label.BI_Administrador,
              timezonesidkey = Label.BI_TimeZoneLA,
              username = 'usertestuser@testorg.com');
    
    insert user;
    
    system.runAs(user){
      NE__Catalog__c cat = new NE__Catalog__c (Name = 'test catalog', BI_Country__c = 'Argentina');
      insert cat;
        
      lst_camp = BI_DataLoad.loadCampaigns_CatalogID(K,cat.Id);

      ///////
      List<BI_Promociones__c> lst_promociones = new List<BI_Promociones__c>();
          List<NE__Promotion__c> lst_promo = new List<NE__Promotion__c>();
          
          for(Campaign camp :lst_camp){
            for(Integer j = 0; j < 2; j++){
                                
                NE__Promotion__c pro = new NE__Promotion__c (Name = 'test'+ string.valueof(j),
                                                             NE__Start_Date__c = Datetime.now(),
                                                             NE__Catalog_Id__c = camp.BI_Catalogo__c);
                lst_promo.add(pro); 

                //system.debug('camp.BI_Catalogo__c ' + camp.BI_Catalogo__c);
            
            }
          }
          insert lst_promo;
          //system.debug('lst_promo' + lst_promo);

         List<NE__Catalog_Item__c> lst_nci = BI_DataLoad.loadNECatalogItems(lst_promo.size(), cat.Id, lst_promo);


          for(Campaign camp :lst_camp){
            //system.debug('Campaign --> ' + camp.Name);
            for(NE__Catalog_Item__c NE_CatItem :lst_nci){
  /*            System.debug('camp.BI_Catalogo__c --> ' + camp.BI_Catalogo__c);
              System.debug('NE_CatItem.NE__Catalog_Id__c --> ' + NE_CatItem.NE__Catalog_Id__c);
              System.debug('NE_CatItem.NE__Catalog_Category_Name__c --> ' + NE_CatItem.NE__Product_Category_Name__c);
              System.debug('NE_CatItem.NE_CatItem.NE__Type__c --> ' + NE_CatItem.NE__Type__c);*/

                  BI_Promociones__c promo = new BI_Promociones__c (
                      BI_Campanas__c = camp.Id, 
                      BI_Ingreso_por_unica_vez__c = Math.random() * 1000,
                      Recurrente_bruto_mensual__c = Math.random() * 1000,
                      BI_Catalogo_Producto__c = camp.BI_Catalogo__c,
                      BI_Producto__c = NE_CatItem.Id
                  );
                lst_promociones.add(promo);
            }   
        }

      //system.debug('TO INS: lst_promociones' + lst_promociones);
          Test.startTest();
          insert lst_promociones;

          Set<Id> set_Prom_Id = new Set<Id>();
          for(BI_Promociones__c prom :lst_promociones){
            set_Prom_Id.add(prom.Id);
          }
          system.assert(!set_Prom_Id.isEmpty());
          lst_promociones = [SELECT CurrencyIsoCode, BI_Producto__c FROM BI_Promociones__c WHERE Id IN :set_Prom_Id];
      Map<Id,NE__Catalog_Item__c> map_nci = new  Map<Id,NE__Catalog_Item__c>([SELECT Id, CurrencyIsoCode FROM NE__Catalog_Item__c WHERE Id IN :lst_nci]);

          for(BI_Promociones__c prom :lst_promociones){
            system.assertEquals(prom.CurrencyIsoCode , map_nci.get(prom.BI_Producto__c).CurrencyIsoCode);
          }

          Test.stopTest();
    }
  }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}