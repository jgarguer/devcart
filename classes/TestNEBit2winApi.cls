@isTest (SeeAllData=true)
private class TestNEBit2winApi {

    
        
          
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Alvaro García
        Company:       Accenture  
        Description:   Test method that manage the code coverage from NEBit2WinApi.doCalloutFromFuture
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        22/11/2016                      Alvaro García               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @istest static void doCalloutFromFuture_TEST(){

        BI_TestUtils.throw_exception = false;

        TGS_User_Org__c [] lst_userTGS = [SELECT Id,TGS_Is_BI_EN__c,TGS_Is_TGS__c,SetupOwnerId FROM TGS_User_Org__c WHERE SetupOwnerId =: UserInfo.getUserId()];
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        if(lst_userTGS.isEmpty()){
            userTGS.SetupOwnerId = UserInfo.getUserId();
            userTGS.TGS_Is_TGS__c = false;
        userTGS.TGS_Is_BI_EN__c = true;
            insert userTGS;
        }
        else{
            userTGS.SetupOwnerId = lst_userTGS[0].SetupOwnerId;
        userTGS.TGS_Is_TGS__c = false;
            userTGS.TGS_Is_BI_EN__c = true;
        update userTGS;
        }

        NEBit2winApi.doCalloutFromFuture('test');
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Alvaro García
        Company:       Accenture  
        Description:   Test method that manage the code coverage from NEBit2WinApi.submit
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        22/11/2016                      Alvaro García               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void submit_TEST(){

        BI_TestUtils.throw_exception = false;

        TGS_User_Org__c [] lst_userTGS = [SELECT Id,TGS_Is_BI_EN__c,TGS_Is_TGS__c,SetupOwnerId FROM TGS_User_Org__c WHERE SetupOwnerId =: UserInfo.getUserId()];
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        if(lst_userTGS.isEmpty()){
            userTGS.SetupOwnerId = UserInfo.getUserId();
            userTGS.TGS_Is_TGS__c = false;
        userTGS.TGS_Is_BI_EN__c = true;
            insert userTGS;
        }
        else{
            userTGS.SetupOwnerId = lst_userTGS[0].SetupOwnerId;
        userTGS.TGS_Is_TGS__c = false;
            userTGS.TGS_Is_BI_EN__c = true;
        update userTGS;
        }

        NE__Order__c ord = new NE__Order__c(
            NE__OrderStatus__c = 'Active'
        );
        insert ord;
        NE__OrderItem__c oi = new NE__OrderItem__c (
            NE__Status__c = 'Active',
            NE__OrderId__c = ord.Id,
            NE__Qty__c = 2
        );
        insert oi;

        NEBit2winApi.submit(ord.Id, 'Test');
        NEBit2winApi.submit(ord.Id, 'Active');
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Alvaro García
        Company:       Accenture  
        Description:   Test method that manage the code coverage from NEBit2WinApi.cloneOpty
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        22/11/2016                      Alvaro García               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void cloneOpty_TEST(){

        BI_TestUtils.throw_exception = false;

        TGS_User_Org__c [] lst_userTGS = [SELECT Id,TGS_Is_BI_EN__c,TGS_Is_TGS__c,SetupOwnerId FROM TGS_User_Org__c WHERE SetupOwnerId =: UserInfo.getUserId()];
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        if(lst_userTGS.isEmpty()){
            userTGS.SetupOwnerId = UserInfo.getUserId();
            userTGS.TGS_Is_TGS__c = false;
            userTGS.TGS_Is_BI_EN__c = false;
            insert userTGS;
        }
        else{
            userTGS.SetupOwnerId = lst_userTGS[0].SetupOwnerId;
        userTGS.TGS_Is_TGS__c = false;
            userTGS.TGS_Is_BI_EN__c = false;
        update userTGS;
        }

        NETriggerHelper.setTriggerFired('TGS_Order_Handler.NeCheckOrder');

        List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Argentina});

        List <Opportunity> lst_opp = new List <Opportunity>();

        Opportunity opp = new Opportunity(
            Name = 'tesssst',
            CloseDate = Date.today().addDays(2),
            StageName = Label.BI_DefinicionSolucion,
            AccountId = lst_acc[0].Id,
            BI_Opportunity_Type__c = 'Móvil',
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            BI_Licitacion__c = 'No'
        );
        lst_opp.add(opp);
        Opportunity opp2 = new Opportunity(
            Name = 'tesssst2',
            CloseDate = Date.today().addDays(2),
            StageName = Label.BI_DefinicionSolucion,
            AccountId = lst_acc[0].Id,
            BI_Opportunity_Type__c = 'Móvil',
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            BI_Licitacion__c = 'No'
        );
        lst_opp.add(opp2);

        insert lst_opp;

        NE__Catalog__c cat = new NE__Catalog__c(Name = 'teset');
        insert cat;

        NE__Catalog_Item__c ci = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id,
            NE__Technical_Behaviour_Opty__c = 'siii'
        );
        insert ci;

        NE__Order__c ord = new NE__Order__c(
            NE__OrderStatus__c = 'Active',
            NE__OptyId__c = opp.Id,
            RecordTypeId = Schema.SObjectType.NE__Order__c.getRecordTypeInfosByName().get('Opty').getRecordTypeId()
        );
        insert ord;

        NE__OrderItem__c oi = new NE__OrderItem__c (
            NE__Status__c = 'Active',
            NE__OrderId__c = ord.Id,
            NE__Qty__c = 2,
            NE__CatalogItem__c = ci.Id
        );
        insert oi;

        Test.startTest();
        NEBit2winApi.cloneOpty(opp.Id, opp2.Id, 'New Version');
        NEBit2winApi.cloneOpty(opp.Id, opp2.Id, 'Renewal');
        NEBit2winApi.cloneOpty(opp.Id, opp2.Id, 'Clone');
        Test.stopTest();
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Alvaro García
        Company:       Accenture  
        Description:   Test method that manage the code coverage from NEBit2WinApi.resetOpty
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        22/11/2016                      Alvaro García               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void resetOpty_TEST(){

        BI_TestUtils.throw_exception = false;

        TGS_User_Org__c [] lst_userTGS = [SELECT Id,TGS_Is_BI_EN__c,TGS_Is_TGS__c,SetupOwnerId FROM TGS_User_Org__c WHERE SetupOwnerId =: UserInfo.getUserId()];
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        if(lst_userTGS.isEmpty()){
            userTGS.SetupOwnerId = UserInfo.getUserId();
            userTGS.TGS_Is_TGS__c = false;
        userTGS.TGS_Is_BI_EN__c = true;
            insert userTGS;
        }
        else{
            userTGS.SetupOwnerId = lst_userTGS[0].SetupOwnerId;
        userTGS.TGS_Is_TGS__c = false;
            userTGS.TGS_Is_BI_EN__c = true;
        update userTGS;
        }
        /*  Pedro Pachas
            Evitar lanzar Triggers, ya que se realizan pruebas para reiniciar los CIs de las Oportunidades
        */
        Map<Integer,BI_bypass__c> mapOfBypass = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, true, true);
        
        

        List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Colombia});

        List <Opportunity> lst_opp = new List <Opportunity>();

        Opportunity opp = new Opportunity(
            Name = 'tesssst',
            CloseDate = Date.today().addDays(2),
            StageName = Label.BI_DefinicionSolucion,
            AccountId = lst_acc[0].Id,
            BI_Opportunity_Type__c = 'Móvil',
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Colombia,
            BI_Licitacion__c = 'No'
        );
        insert opp;

        NE__Catalog__c cat = new NE__Catalog__c(Name = 'teset');
        insert cat;

        NE__Catalog_Item__c ci = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id,
            NE__Technical_Behaviour_Opty__c = 'Factibilidad técnica',
            NE__BaseRecurringCharge__c = 12,
            NE__Base_OneTime_Fee__c = 12
        );
        insert ci;

        NE__Order__c ord = new NE__Order__c(
            NE__OrderStatus__c = 'Active',
            NE__OptyId__c = opp.Id,
            RecordTypeId = Schema.SObjectType.NE__Order__c.getRecordTypeInfosByName().get('Opty').getRecordTypeId()
        );
        insert ord;

        NE__OrderItem__c oi = new NE__OrderItem__c (
            NE__Status__c = 'Active',
            NE__OrderId__c = ord.Id,
            NE__Qty__c = 2,
            NE__CatalogItem__c = ci.Id,
            NE__OneTimeFeeOv__c = 0,
            NE__RecurringChargeOv__c = 0,
            NE__BaseOneTimeFee__c = 12,
            NE__BaseRecurringCharge__c = 12
        );
        insert oi;

        BI_MigrationHelper.disableBypass(mapOfBypass);

        Test.startTest();
        NEBit2winApi.resetOpty(opp.Id);
        ci.NE__Base_OneTime_Fee__c = 0;
        ci.NE__BaseRecurringCharge__c = 0;
        update ci;
        NEBit2winApi.resetOpty(opp.Id);
        Test.stopTest();
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Alvaro García
        Company:       Accenture  
        Description:   Test method that manage the code coverage from NEBit2WinApi.findActiveOptyVersion
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        22/11/2016                      Alvaro García               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void findActiveOptyVersion_TEST(){

        BI_TestUtils.throw_exception = false;

        TGS_User_Org__c [] lst_userTGS = [SELECT Id,TGS_Is_BI_EN__c,TGS_Is_TGS__c,SetupOwnerId FROM TGS_User_Org__c WHERE SetupOwnerId =: UserInfo.getUserId()];
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        if(lst_userTGS.isEmpty()){
            userTGS.SetupOwnerId = UserInfo.getUserId();
            userTGS.TGS_Is_TGS__c = false;
        userTGS.TGS_Is_BI_EN__c = true;
            insert userTGS;
        }
        else{
            userTGS.SetupOwnerId = lst_userTGS[0].SetupOwnerId;
        userTGS.TGS_Is_TGS__c = false;
            userTGS.TGS_Is_BI_EN__c = true;
        update userTGS;
        }

        List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Colombia});

        List <Opportunity> lst_opp = new List <Opportunity>();

        Opportunity opp = new Opportunity(
            Name = 'tesssst',
            CloseDate = Date.today().addDays(2),
            StageName = Label.BI_DefinicionSolucion,
            AccountId = lst_acc[0].Id,
            BI_Opportunity_Type__c = 'Móvil',
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Colombia,
            BI_Licitacion__c = 'No'
        );
        insert opp;

        NE__Order__c ord = new NE__Order__c(
            NE__OrderStatus__c = 'Active',
            NE__OptyId__c = opp.Id,
            RecordTypeId = Schema.SObjectType.NE__Order__c.getRecordTypeInfosByName().get('Opty').getRecordTypeId()
        );
        insert ord;

        Test.startTest();
        userTGS.TGS_Is_BI_EN__c = true;
        update userTGS;
        NEBit2winApi.findActiveOptyVersion(opp.Id);
        Test.stopTest();

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Alvaro García
        Company:       Accenture  
        Description:   Test method that manage the code coverage from NEBit2WinApi.cloneOptyconfiguration
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        22/11/2016                      Alvaro García               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @istest static void cloneOptyconfiguration_TEST (){

        BI_TestUtils.throw_exception = false;

        TGS_User_Org__c [] lst_userTGS = [SELECT Id,TGS_Is_BI_EN__c,TGS_Is_TGS__c,SetupOwnerId FROM TGS_User_Org__c WHERE SetupOwnerId =: UserInfo.getUserId()];
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        if(lst_userTGS.isEmpty()){
            userTGS.SetupOwnerId = UserInfo.getUserId();
            userTGS.TGS_Is_TGS__c = false;
            userTGS.TGS_Is_BI_EN__c = false;
            insert userTGS;
        }
        else{
            userTGS.SetupOwnerId = lst_userTGS[0].SetupOwnerId;
        userTGS.TGS_Is_TGS__c = false;
            userTGS.TGS_Is_BI_EN__c = false;
        update userTGS;
        }

        NETriggerHelper.setTriggerFired('TGS_Order_Handler.NeCheckOrder');

        List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Colombia});

        List <Opportunity> lst_opp = new List <Opportunity>();

        Opportunity opp = new Opportunity(
            Name = 'tesssst',
            CloseDate = Date.today().addDays(2),
            StageName = Label.BI_DefinicionSolucion,
            AccountId = lst_acc[0].Id,
            BI_Opportunity_Type__c = 'Móvil',
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Colombia,
            BI_Licitacion__c = 'No',
            NE__HaveActiveLineItem__c = true
        );
        insert opp;

        NE__Catalog__c cat = new NE__Catalog__c(Name = 'teset');
        insert cat;

        NE__Catalog_Item__c ci = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id,
            NE__Technical_Behaviour_Opty__c = 'Factibilidad técnica',
            NE__BaseRecurringCharge__c = 12,
            NE__Base_OneTime_Fee__c = 12
        );
        insert ci;

        NE__Order__c ord = new NE__Order__c(
            NE__OrderStatus__c = 'Active',
            NE__OptyId__c = opp.Id,
            RecordTypeId = Schema.SObjectType.NE__Order__c.getRecordTypeInfosByName().get('Opty').getRecordTypeId()
        );
        insert ord;

        NE__OrderItem__c oi = new NE__OrderItem__c (
            NE__Status__c = 'Active',
            NE__OrderId__c = ord.Id,
            NE__Qty__c = 2,
            NE__CatalogItem__c = ci.Id,
            NE__OneTimeFeeOv__c = 0,
            NE__RecurringChargeOv__c = 0,
            NE__BaseOneTimeFee__c = 12,
            NE__BaseRecurringCharge__c = 12
        );
        insert oi;

        Test.startTest();
        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), false, true, false, false);
        NEBit2winApi.cloneOptyconfiguration(ord.Id, 'Test', 'New Version');
        NEBit2winApi.cloneOptyconfiguration(ord.Id, 'Test', 'Renewal');
        NEBit2winApi.cloneOptyconfiguration(ord.Id, 'Test', 'Clone');
        NEBit2winApi.cloneOptyconfiguration(ord.Id, 'Test', 'Opty2');
        ord.RecordTypeId = Schema.SObjectType.NE__Order__c.getRecordTypeInfosByName().get('Quote').getRecordTypeId();
        update ord;
        NEBit2winApi.cloneOptyconfiguration(ord.Id, 'Test', 'Quote');
        opp.NE__HaveActiveLineItem__c = false;
        update opp;
        NEBit2winApi.cloneOptyconfiguration(ord.Id, 'Test', 'New Version');
        BI_MigrationHelper.disableBypass(mapa);
        Test.stopTest();

    }
}