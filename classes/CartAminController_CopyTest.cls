/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
@isTest(SeeAllData=true)
private class CartAminController_CopyTest {

    static testMethod void unitTest1() {
        try{
              Profile p = [SELECT Id FROM Profile WHERE Name='System administrator' OR Name='Amministratore del sistema' OR Name='Administrador del sistema' limit 1]; 
              
            NE__IntegrationAdministration__c ia = new NE__IntegrationAdministration__c (NE__SearchBarFields__c='<Workspace><ListOfFields><Field name="ispromo__c" type="BOOLEAN" object="Product" apiname="Product__c" label="IsPromo" itemquery="ProductId__r.ispromo__c" promoquery="Catalog_Item__r.ProductId__r.ispromo__c"><Domain value=""></Domain><Domain value="true"></Domain><Domain value="false"></Domain></Field><Field name="active__c" type="BOOLEAN" object="Item" apiname="Catalog_Item__c" label="Active" itemquery="active__c" promoquery="Catalog_Item__r.active__c"><Domain value=""></Domain><Domain value="true"></Domain><Domain value="false"></Domain></Field><Field name="brand__c" type="PICKLIST" object="Item" apiname="Catalog_Item__c" label="Brand" itemquery="brand__c" promoquery="Catalog_Item__r.brand__c"><Domain value=""></Domain><Domain value="Brand1"></Domain><Domain value="Brand2"></Domain></Field></ListOfFields></Workspace>', 
                                                                                NE__AssetSummaryItems__c='<Workspace><ListOfFields><Field name="ispromo__c" type="BOOLEAN" object="Product" apiname="Product__c" label="IsPromo" itemquery="ProductId__r.ispromo__c" promoquery="Catalog_Item__r.ProductId__r.ispromo__c"><Domain value=""></Domain><Domain value="true"></Domain><Domain value="false"></Domain></Field><Field name="active__c" type="BOOLEAN" object="Item" apiname="Catalog_Item__c" label="Active" itemquery="active__c" promoquery="Catalog_Item__r.active__c"><Domain value=""></Domain><Domain value="true"></Domain><Domain value="false"></Domain></Field><Field name="brand__c" type="PICKLIST" object="Item" apiname="Catalog_Item__c" label="Brand" itemquery="brand__c" promoquery="Catalog_Item__r.brand__c"><Domain value=""></Domain><Domain value="Brand1"></Domain><Domain value="Brand2"></Domain></Field><Field name="onetimefeeov__c" type="CURRENCY" object="Order Item" apiname="OrderItem__c" label="OneTimeFeeOv" itemquery="ProdId__ronetimefeeov__c" promoquery="ProductId__r.onetimefeeov__c"></Field></ListOfFields></Workspace>', 
                                                                                NE__OptySummaryItems__c='<Workspace><ListOfFields><Field name="ispromo__c" type="BOOLEAN" object="Product" apiname="Product__c" label="IsPromo" itemquery="ProductId__r.ispromo__c" promoquery="Catalog_Item__r.ProductId__r.ispromo__c"><Domain value=""></Domain><Domain value="true"></Domain><Domain value="false"></Domain></Field><Field name="active__c" type="BOOLEAN" object="Item" apiname="Catalog_Item__c" label="Active" itemquery="active__c" promoquery="Catalog_Item__r.active__c"><Domain value=""></Domain><Domain value="true"></Domain><Domain value="false"></Domain></Field><Field name="brand__c" type="PICKLIST" object="Item" apiname="Catalog_Item__c" label="Brand" itemquery="brand__c" promoquery="Catalog_Item__r.brand__c"><Domain value=""></Domain><Domain value="Brand1"></Domain><Domain value="Brand2"></Domain></Field><Field name="onetimefeeov__c" type="CURRENCY" object="Order Item" apiname="OrderItem__c" label="OneTimeFeeOv" itemquery="ProdId__ronetimefeeov__c" promoquery="ProductId__r.onetimefeeov__c"></Field></ListOfFields></Workspace>', 
                                                                                NE__OrderSummaryItems__c='<Workspace><ListOfFields><Field name="ispromo__c" type="BOOLEAN" object="Product" apiname="Product__c" label="IsPromo" itemquery="ProductId__r.ispromo__c" promoquery="Catalog_Item__r.ProductId__r.ispromo__c"><Domain value=""></Domain><Domain value="true"></Domain><Domain value="false"></Domain></Field><Field name="active__c" type="BOOLEAN" object="Item" apiname="Catalog_Item__c" label="Active" itemquery="active__c" promoquery="Catalog_Item__r.active__c"><Domain value=""></Domain><Domain value="true"></Domain><Domain value="false"></Domain></Field><Field name="brand__c" type="PICKLIST" object="Item" apiname="Catalog_Item__c" label="Brand" itemquery="brand__c" promoquery="Catalog_Item__r.brand__c"><Domain value=""></Domain><Domain value="Brand1"></Domain><Domain value="Brand2"></Domain></Field><Field name="onetimefeeov__c" type="CURRENCY" object="Order Item" apiname="OrderItem__c" label="OneTimeFeeOv" itemquery="ProdId__ronetimefeeov__c" promoquery="ProductId__r.onetimefeeov__c"></Field></ListOfFields></Workspace>', 
                                                                                NE__QuoteSummaryItems__c='<Workspace><ListOfFields><Field name="ispromo__c" type="BOOLEAN" object="Product" apiname="Product__c" label="IsPromo" itemquery="ProductId__r.ispromo__c" promoquery="Catalog_Item__r.ProductId__r.ispromo__c"><Domain value=""></Domain><Domain value="true"></Domain><Domain value="false"></Domain></Field><Field name="active__c" type="BOOLEAN" object="Item" apiname="Catalog_Item__c" label="Active" itemquery="active__c" promoquery="Catalog_Item__r.active__c"><Domain value=""></Domain><Domain value="true"></Domain><Domain value="false"></Domain></Field><Field name="brand__c" type="PICKLIST" object="Item" apiname="Catalog_Item__c" label="Brand" itemquery="brand__c" promoquery="Catalog_Item__r.brand__c"><Domain value=""></Domain><Domain value="Brand1"></Domain><Domain value="Brand2"></Domain></Field><Field name="onetimefeeov__c" type="CURRENCY" object="Order Item" apiname="OrderItem__c" label="OneTimeFeeOv" itemquery="ProdId__ronetimefeeov__c" promoquery="ProductId__r.onetimefeeov__c"></Field></ListOfFields></Workspace>', 
                                                                                NE__Value_received__c='vr', NE__Value_interpreted_as__c='Sent', NE__Username__c='un', NE__AbilitatedProfiles__c= p.Id, NE__Password__c='pw', NE__Name__c='name', NE__ItemsForPage__c=5, NE__EndPoint__c='ep', NE__DVECall__c='Punctual', NE__CertificateName__c='cn', NE__CatalogVisibility__c='Both', NE__AlternativePassword__c='apw');
            insert ia;
    
            ApexPages.standardController intcontr = new ApexPages.standardController(ia);      
            CartAdminController_Copy noe = new CartAdminController_Copy(intcontr);
            
            List<SelectOption> selectedOptions          =   noe.selectedOptions;
            List<SelectOption> selectedOptionsItems     =   noe.selectedOptionsItems;
            List<SelectOption> selectedFieldOptions     =   noe.selectedFieldOptions;
            List<SelectOption> unSelectedOptions        =   noe.unSelectedOptions;
            List<SelectOption> unSelectedOptionsItems   =   noe.unSelectedOptionsItems;
            List<SelectOption> unSelectedFieldOptions   =   noe.unSelectedFieldOptions;   
            List<SelectOption> selectedFieldOptionsItemsAsset   =   noe.selectedFieldOptionsItemsAsset;
            List<SelectOption> selectedFieldOptionsItemsOpty    =   noe.selectedFieldOptionsItemsOpty;
            List<SelectOption> selectedFieldOptionsItemsOrder   =   noe.selectedFieldOptionsItemsOrder;
            List<SelectOption> selectedFieldOptionsItemsQuote   =   noe.selectedFieldOptionsItemsQuote;
            List<SelectOption> unSelectedFieldOptionsItemsAsset =   noe.unSelectedFieldOptionsItemsAsset;
            List<SelectOption> unSelectedFieldOptionsItemsOpty  =   noe.unSelectedFieldOptionsItemsOpty;
            List<SelectOption> unSelectedFieldOptionsItemsOrder =   noe.unSelectedFieldOptionsItemsOrder;
            List<SelectOption> unSelectedFieldOptionsItemsQuote =   noe.unSelectedFieldOptionsItemsQuote;
            
            noe.selected            =   new List<String>();
            noe.selectedSearch      =   new List<String>();
            noe.selectedItems       =   new List<String>();
            noe.selectedAssetItems  =   new List<String>();
            noe.selectedOptyItems   =   new List<String>();
            noe.selectedOrderItems  =   new List<String>();
            noe.selectedQuoteItems  =   new List<String>();
            
            noe.selected.add(noe.unSelectedOptions.get(0).getValue());
            noe.selectedSearch.add(noe.unSelectedFieldOptions.get(0).getValue());
            noe.selectedItems.add(noe.unSelectedOptionsItems.get(0).getValue());
            noe.selectedAssetItems.add(noe.unSelectedFieldOptionsItemsAsset.get(0).getValue());
            noe.selectedOptyItems.add(noe.unSelectedFieldOptionsItemsOpty.get(0).getValue());
            noe.selectedOrderItems.add(noe.unSelectedFieldOptionsItemsOrder.get(0).getValue());
            noe.selectedQuoteItems.add(noe.unSelectedFieldOptionsItemsQuote.get(0).getValue());
            
            noe.unselected          =   new List<String>();
            noe.unselectedSearch    =   new List<String>();
            noe.unselectedItems     =   new List<String>();
            noe.unselectedAssetItems =  new List<String>();
            noe.unselectedOptyItems =   new List<String>();
            noe.unselectedOrderItems =  new List<String>();
            noe.unselectedQuoteItems =  new List<String>();
            
            noe.unselected.add(noe.selectedOptions.get(0).getValue());
            noe.unselectedSearch.add(noe.selectedFieldOptions.get(0).getValue());
            noe.unselectedItems.add(noe.selectedOptionsItems.get(0).getValue());
            noe.unselectedAssetItems.add(noe.selectedFieldOptionsItemsAsset.get(1).getValue());
            noe.unselectedOptyItems.add(noe.selectedFieldOptionsItemsOpty.get(1).getValue());
            noe.unselectedOrderItems.add(noe.selectedFieldOptionsItemsOrder.get(1).getValue());
            noe.unselectedQuoteItems.add(noe.selectedFieldOptionsItemsQuote.get(1).getValue());
            //get(1) so that selected item can be moved on
                    
            noe.doSelect();
            noe.doSelectField();
            noe.doSelectItems();
            noe.doSelectFieldItemsAsset();
            noe.doSelectFieldItemsOpty();
            noe.doSelectFieldItemsOrder();
            noe.doSelectFieldItemsQuote();
            
            noe.DoFieldUpAsset();
            noe.DoFieldUpOpty();
            noe.DoFieldUpOrder();
            noe.DoFieldUpQuote();
            noe.DoFieldDownAsset();
            noe.DoFieldDownOpty();
            noe.DoFieldDownOrder();
            noe.DoFieldDownQuote();
            
            noe.doUnSelect();
            noe.doUnSelectField();
            noe.doUnSelectItems();
            noe.doUnSelectFieldItemsAsset();
            noe.doUnSelectFieldItemsOpty();
            noe.doUnSelectFieldItemsOrder();
            noe.doUnSelectFieldItemsQuote();
            
            noe.saveConf();
            noe.saveFieldConf();
            noe.saveSummaryConfAssetItems();
            noe.saveSummaryConfOptyItems();
            noe.saveSummaryConfOrderItems();
            noe.saveSummaryConfQuoteItems();
        }
        catch(Exception e){
            System.debug(e);
        }
    }
    
    static testMethod void unitTest2() {
        try{
            // SearchBarFields__c == null, (Asset/Opty/Order/Quote)SummaryItems == null       
            NE__IntegrationAdministration__c ia = new NE__IntegrationAdministration__c (NE__Value_received__c='vr', NE__Value_interpreted_as__c='Sent', NE__Username__c='un', NE__AbilitatedProfiles__c='ap', NE__Password__c='pw', NE__Name__c='name', NE__ItemsForPage__c=5, NE__EndPoint__c='ep', NE__DVECall__c='Punctual', NE__CertificateName__c='cn', NE__CatalogVisibility__c='Both', NE__AlternativePassword__c='apw');
            insert ia;
            
            //[select id from IntegrationAdministration__c where DVECall__c != null AND CatalogVisibility__c != null LIMIT 1];
            ApexPages.standardController intcontr = new ApexPages.standardController(ia);      
            CartAdminController_Copy noe = new CartAdminController_Copy(intcontr);
            
            List<SelectOption> unSelectedOptions        =   noe.unSelectedOptions;
            List<SelectOption> unSelectedOptionsItems   =   noe.unSelectedOptionsItems;
            List<SelectOption> unSelectedFieldOptions   =   noe.unSelectedFieldOptions;
            List<SelectOption> unSelectedFieldOptionsItemsAsset =   noe.unSelectedFieldOptionsItemsAsset;
            List<SelectOption> unSelectedFieldOptionsItemsOpty  =   noe.unSelectedFieldOptionsItemsOpty;
            List<SelectOption> unSelectedFieldOptionsItemsOrder =   noe.unSelectedFieldOptionsItemsOrder;
            List<SelectOption> unSelectedFieldOptionsItemsQuote =   noe.unSelectedFieldOptionsItemsQuote;
            
            noe.selected            =   new List<String>();
            noe.selectedSearch      =   new List<String>();
            noe.selectedItems       =   new List<String>();
            noe.selectedAssetItems  =   new List<String>();
            noe.selectedOptyItems   =   new List<String>();
            noe.selectedOrderItems  =   new List<String>();
            noe.selectedQuoteItems  =   new List<String>();
            
            noe.selected.add(noe.unSelectedOptions.get(0).getValue());
            noe.selectedSearch.add(noe.unSelectedFieldOptions.get(0).getValue());
            noe.selectedItems.add(noe.unSelectedOptionsItems.get(0).getValue());
            noe.selectedAssetItems.add(noe.unSelectedFieldOptionsItemsAsset.get(0).getValue());
            noe.selectedOptyItems.add(noe.unSelectedFieldOptionsItemsOpty.get(0).getValue());
            noe.selectedOrderItems.add(noe.unSelectedFieldOptionsItemsOrder.get(0).getValue());
            noe.selectedQuoteItems.add(noe.unSelectedFieldOptionsItemsQuote.get(0).getValue());
            
            noe.saveConf();
            noe.saveFieldConf();
            noe.saveSummaryConfAssetItems();
            noe.saveSummaryConfOptyItems();
            noe.saveSummaryConfOrderItems();
            noe.saveSummaryConfQuoteItems();
        }
        catch(Exception e){
            System.debug(e);
        }
    }
}