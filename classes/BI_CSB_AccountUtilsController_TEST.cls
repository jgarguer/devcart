@isTest
private class BI_CSB_AccountUtilsController_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_AccountRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    17/11/2015        		Fernando Arteaga  		Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_AccountRest.getAccount()
    History:
    
    <Date>              <Author>                <Description>
    17/11/2015          Fernando Arteaga        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static testMethod void myUnitTest()
    {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	/*  
    	Commented out.  Left as reference....  
        List <Region__c> lst_region = BI_DataLoad.loadRegions().values();  
        */

		List<Profile> prof = [select Id from Profile where Name = 'BI_Administrator' limit 1];

        User usertest = new User(alias = 'cctest',
                          email = 'cctest@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof[0].Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'cctest@testorg.com');
        
        insert usertest;
        
        System.runAs(usertest)
        {
	        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
	        
	        List<String>lst_region = new List<String>();
	       
	        lst_region.add('Argentina');
	        
	        //retrieve the country
	        BI_Code_ISO__c region = biCodeIso.get('ARG');
	       
	        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(2, lst_region, false);
	        List <Contact> lst_con = BI_DataLoadRest.loadContacts(1, lst_acc);
	        lst_acc[0].BI_Activo__c = Label.BI_No;
	        lst_acc[0].BI_COL_Estado__c = Label.BI_CSB_Activo;
	        update lst_acc[0];
	        
	        Test.startTest();
	        
			BI_CSB_AccountUtilsController controller = new BI_CSB_AccountUtilsController();
			
			List<SelectOption> items = controller.getItems();
			controller.seleccionar = 'Suspender';
			PageReference p = controller.actionSave();
			
			Blob blobBody = Blob.valueOf(lst_acc[0].Id + '\r\n' + lst_acc[1].Id);
			controller.csvFileBody = blobBody;
			
			controller.importCSVFile();
			//String value = controller.procesar;
			//controller.habilitarProcesar();
			controller.Process();
	
			controller.csvFileBody = null;
			try
			{
				controller.importCSVFile();
			}
	        catch(Exception e)
	        {
	        	System.assert(e.getMessage().contains('An error has occured while importing data. Please make sure input csv file is correct'));
	        }
	        
	        Test.stopTest();
        }
    }
}