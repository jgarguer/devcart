@RestResource(urlMapping='/customeraccounts/v1/accounts/*/invoices')
global class BI_InvoicesForAccountRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Account's related invoices Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Retrieves the list of invoices associated to the specific account
    
    IN:            Void
    OUT:           BI_RestWrapper.InvoicesListInfoType structure
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.InvoicesListInfoType getInvoices() {
		
		BI_RestWrapper.InvoicesListInfoType invListInfoType;
		
		try{
			
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
		
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
			
				//MULTIPLE INVOICES
				invListInfoType = BI_RestHelper.getInvoicesForAccount(RestContext.request.requestURI.split('/')[4], RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
			
				RestContext.response.statuscode = (invListInfoType == null)?404:200;//404 NOT_FOUND, 200 OK				
			
			}
				
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_InvoicesForAccountRest.getInvoices', 'BI_EN', exc, 'Web Service');
			
		}
		
		return invListInfoType;
		
	}
	
}