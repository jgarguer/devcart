/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-04-07      Manuel Esthiben Mendez Devia (MEMD)     Cloned Class
* @version   1.1    2015-04-27      Javier Tibamoza Cubillos                Se agregó el mnetodo fnSendUser que se llama desde un proccess builder.
		    2017-07-21	    Marta Gonzalez			    Adaptacion canal Online a la reingenieria //REING-21/07/2017    
*************************************************************************************************************/
global class BI_COL_ChannelOnline_cls
{
    webservice static String insertarContacto( 
        String nombres, String apellidos, 
        String tipoDocumento, String numeroDocumento, 
        String cargoEnEmpresa, String email,
        String direccion, String pais, 
        String departamento, String ciudad, 
        String telefono, String movil, 
        String perfil, String nitEmpresa, 
        String estado, String tipoContacto )
    {
        String strRespuesta = '<respuesta>';
        try
        {
            if( estado.equals('Activo') || estado.equals('Inactivo') )
            {
                //Buscamos el id de la ciudad asociada a los parámetros de ciudad y departamento
                List<BI_Col_Ciudades__c> listCiudad = [
                    SELECT Id
                    FROM BI_Col_Ciudades__c
                    WHERE Name = :ciudad
                    AND BI_COL_Departamento__c =: departamento
                    LIMIT 1 ];
                
                if( listCiudad.size() > 0 )
                {
                    //Buscamos el id de la cuenta asociada al nit recibido como parámetro
                    List<Account> listAccount = [
                        SELECT Id
                        FROM Account
                        WHERE BI_No_Identificador_fiscal__c =: nitEmpresa
                        LIMIT 1 ];
                    
                    if( listAccount.size() > 0 )
                    {
                        Contact objContact = new Contact(); 
                        objContact.FirstName = nombres; 
                        objContact.LastName = apellidos;
                        objContact.BI_Tipo_de_documento__c = tipoDocumento; 
                        objContact.BI_Numero_de_documento__c = numeroDocumento;
                        objContact.Title = cargoEnEmpresa;
                        objContact.Email = email;
                        objContact.BI_COL_Direccion_oficina__c = direccion;
                        objContact.BI_Country__c = pais;
                        objContact.Phone = telefono;
                        objContact.MobilePhone = movil;
                        objContact.BI_COL_Perfil__c = perfil;
                        objContact.BI_Activo__c = estado.equals('Activo')?true:false;
                        objContact.AccountId = listAccount[0].Id;
                        objContact.Department = listCiudad[0].Id;
                        objContact.AssistantPhone = telefono;
                        objContact.BI_Tipo_de_contacto__c = tipoContacto;
                        
                        insert objContact;
                        
                        strRespuesta += '<resultado>true</resultado>';
                        strRespuesta += '<idcontacto>' + objContact.Id + '</idcontacto>';
                        strRespuesta += '<descripcion>El contacto se ha creado satisfactoriamente</descripcion>';
                    }
                    else
                    {
                        strRespuesta += '<resultado>false</resultado>';
                        strRespuesta += '<descripcion>El NIT dado no tiene empresa asociada</descripcion>';
                    }
                }
                else
                {
                    strRespuesta += '<resultado>false</resultado>';
                    strRespuesta += '<descripcion>Los valores de ciudad y departamento no se encontraron</descripcion>';
                }
            }
            else
            {
                strRespuesta += '<resultado>false</resultado>';
                strRespuesta += '<descripcion>El estado deber ser Activo o Inactivo</descripcion>';
            }
        }
        catch( Exception error )
        {
            strRespuesta += '<resultado>false</resultado>';
            strRespuesta += '<descripcion>No se pudo insertar el nuevo contacto: ' + error.getMessage() +'</descripcion>';
        }

        strRespuesta += '</respuesta>';
        
        return strRespuesta;
    }
    /**
    * @description: 
    */
    webservice static String administrarContacto(
        String idSalesforceContact, String nombres,
        String apellidos, String tipoDocumento, 
        String numeroDocumento, String cargoEnEmpresa, 
        String email, String direccion, 
        String pais, String departamento, 
        String ciudad, String telefono, 
        String movil, String perfil,
        String nitEmpresa, String estado, 
        String tipoContacto)
    {
        String strRespuesta = '<respuesta>';
        //, BI_COL_Estado__c
        //Seleccionamos los contactos que se identifiquen con el id recibido como parámetro (uno nada más)
        List<Contact> listContact = [
            SELECT  Id, FirstName, LastName, BI_Tipo_de_documento__c, BI_Numero_de_documento__c, Title,
                    Email, BI_COL_Direccion_oficina__c, Department, Phone, MobilePhone, AccountId,
                    BI_COL_Perfil__c, BI_Activo__c
            FROM    Contact
            WHERE   Id =: idSalesforceContact and BI_Country__c='Colombia'
            LIMIT 1 ];
        //Verificamos si el query arrojó resultados
        if( listContact.size() > 0 )
        {           
            //Obtenemos el objeto
            Contact objContact = listContact[0];
            //Asignamos valores recibidos a los campos
            objContact.FirstName = nombres;
            objContact.LastName = apellidos;
            objContact.BI_Tipo_de_documento__c = tipoDocumento;
            objContact.BI_Numero_de_documento__c = numeroDocumento;
            objContact.Title = cargoEnEmpresa;
            objContact.Email = email;
            objContact.BI_COL_Direccion_oficina__c = direccion;
            objContact.BI_Country__c = pais;
            objContact.Phone = telefono;
            objContact.AssistantPhone = telefono;
            objContact.MobilePhone = movil;
            objContact.BI_COL_Perfil__c = perfil;
            objContact.BI_Tipo_de_contacto__c = tipoContacto;
            
            //if( estado.equals('Activo') || estado.equals('Inactivo') )
            objContact.BI_Activo__c = estado.equals('Activo')?true:false;
            //Buscamos el id de la ciudad asociada a los parámetros de ciudad y departamento
            List<BI_Col_Ciudades__c> listCiudad = [
                SELECT Id
                FROM BI_Col_Ciudades__c
                WHERE Name = :ciudad
                AND BI_COL_Departamento__c =: departamento
                LIMIT 1 ];
            //Actualizamos el id de la ciudad del contacto
            if( listCiudad.size() > 0 )
                objContact.Department = listCiudad[0].Id;
            //Buscamos el id de la cuenta asociada al nit recibido como parámetro
            List<Account> listAccount = [
                SELECT  Id
                FROM    Account
                WHERE   BI_No_Identificador_fiscal__c =: nitEmpresa
                LIMIT 1 ];
            
            if( listAccount.size() > 0 )
            {
                //Actualizamos el id de la cuenta del contacto
                objContact.AccountId = listAccount[0].Id;
                
                try
                {
                    update objContact;
                    strRespuesta += '<resultado>true</resultado>';
                    strRespuesta += '<descripcion>Se actualizaron los datos del contacto (id = '
                        + idSalesforceContact
                        + ') satisfactoriamente</descripcion>';
                }
                catch( Exception error )
                {
                    strRespuesta += '<resultado>false</resultado>';
                    strRespuesta += '<descripcion>Ocurrió un error al intentar actualizar en la base de datos. Error: '
                        + error.getMessage() + '</descripcion>';
                }
            }
            else
            {
                strRespuesta += '<resultado>false</resultado>';
                    strRespuesta += '<descripcion>No se encontró una empresa con el NIT: '
                        + nitEmpresa + '</descripcion>';
            }
        }
        else
        {
            strRespuesta += '<resultado>false</resultado>';
            strRespuesta += '<descripcion>No se encontraron contactos con el id enviado como parametro</descripcion>';
        }
        
        strRespuesta += '</respuesta>';
    
        return strRespuesta;
    }
    /**
    * @description: 
    */
    webservice static String listarEmpresasService( String userEmail )
    {
        String respuesta = '\n<respuesta>\n';
        
        //Obtenemos la lista de cuentas asociadas al usuario identificado por el email
        List<Account> listAccount = [
            SELECT  Id, Name, BI_COL_Segmento_Telefonica__c,  
                    BI_Subsegment_Regional__c, BI_No_Identificador_fiscal__c, Parent.BI_No_Identificador_fiscal__c, ParentId,
                (   SELECT  UserId,user.firstname,user.email, user.lastname,TeamMemberRole 
                    FROM    AccountTeamMembers 
                    WHERE   TeamMemberRole = 'Service Manager'), 
                
                (   SELECT  Id, FirstName, LastName, Phone, MobilePhone, BI_Tipo_de_documento__c,BI_Numero_de_documento__c,
                            Title, TGS_Address__r.BI_COL_Ciudad_Departamento__r.Name, 
                            TGS_Address__r.BI_COL_Ciudad_Departamento__r.BI_COL_Departamento__c,
                            BI_COL_Direccion_oficina__c, Email, BI_Country__c, AccountId, BI_Tipo_de_contacto__c,FS_CORE_Acceso_a_Portal_Platino__c,
                            Department //REING-21/07/2017    
                    FROM    Account.Contacts
    /*REING-01-INI*/WHERE   BI_Tipo_de_contacto__c = 'Administrador Canal Online' OR FS_CORE_Acceso_a_Portal_Platino__c = true )/*REING-01-FIN*/
            FROM    Account
            WHERE   Owner.email =: userEmail ];
        //Verifciamos que el query haya arrojado resultados
        if( listAccount.size() > 0 )
        {
            //Abrimos el tag de service
            respuesta += '\t<service>\n';
            List<AccountTeamMember> objAccountTeamMembers = listAccount[0].AccountTeamMembers;
            //Obtenemos la información del Service Manager (es uno sólo)
            respuesta += '\t\t<nombres>' + objAccountTeamMembers[0].user.firstname + '</nombres>\n';
            respuesta += '\t\t<apellidos>' + objAccountTeamMembers[0].user.LastName + '</apellidos>\n';
            respuesta += '\t\t<tipodocumento>CC</tipodocumento>';
            //respuesta += '\t\t<numerodocumento>' + objAccountTeamMembers[0].user.Cedula__c + '</numerodocumento>\n';
            respuesta += '\t\t<correoElectronico>' + objAccountTeamMembers[0].user.Email + '</correoElectronico>\n';
            //Cerramos el tag de service
            respuesta += '\t</service>\n';
            //Abrimos el tag de empresas
            respuesta += '\t<empresas>\n';
            //Recorremos la lista de cuentas
            for( Account objAccount : listAccount )
            {
                //Abrimos el tag de empresa
                respuesta += '\t\t<empresa>\n';
                //Obtenemos la información de la empresa
                respuesta += '\t\t\t<IDSalesforce>' + objAccount.Id + '</IDSalesforce>\n';
                respuesta += '\t\t\t<NITEmpresa>' + objAccount.BI_No_Identificador_fiscal__c + '</NITEmpresa>\n';
                respuesta += '\t\t\t<NITPadre>' + objAccount.Parent.BI_No_Identificador_fiscal__c + '</NITPadre>';
                String strAccountName = objAccount.Name;
                strAccountName = strAccountName.replace('&', '&amp;');
                strAccountName = strAccountName.replace('<', '');
                respuesta += '\t\t\t<nombre>' + strAccountName + '</nombre>\n';
                respuesta += '\t\t\t<segmento>' + objAccount.BI_COL_Segmento_Telefonica__c + '</segmento>\n';
                respuesta += '\t\t\t<subsegmento>' + objAccount.BI_Subsegment_Regional__c + '</subsegmento>\n';
                //Abrimos el tag de contactos
                respuesta += '\t\t\t<contactos>';
                
                if( objAccount.Contacts.size() > 0 )
                {
                    respuesta += '\n';
                    
                    for( Contact objContact : objAccount.Contacts )
                    {
                        //Abrimos el tag de contactos
                        respuesta += '\t\t\t\t<contacto>\n';
                        //Obtenemos la información del contacto
                        respuesta += '\t\t\t\t\t<Nombres>' + objContact.FirstName + '</Nombres>\n';
                        respuesta += '\t\t\t\t\t<Apellidos>' + objContact.LastName + '</Apellidos>\n';
                        respuesta += '\t\t\t\t\t<TipoDocumento>' + objContact.BI_Tipo_de_documento__c + '</TipoDocumento>\n';
                        respuesta += '\t\t\t\t\t<numeroDocumento>' + objContact.BI_Numero_de_documento__c + '</numeroDocumento>\n';
                        respuesta += '\t\t\t\t\t<CargoEnEmpresa>' + objContact.Title + '</CargoEnEmpresa>\n';
                        respuesta += '\t\t\t\t\t<correoElectronico>' + objContact.Email + '</correoElectronico>\n';
                        respuesta += '\t\t\t\t\t<direccion>' + objContact.BI_COL_Direccion_oficina__c + '</direccion>\n';
                        respuesta += '\t\t\t\t\t<departamento>' + objContact.TGS_Address__r.BI_COL_Ciudad_Departamento__r.BI_COL_Departamento__c + '</departamento>\n';
                        respuesta += '\t\t\t\t\t<ciudad>' + objContact.TGS_Address__r.BI_COL_Ciudad_Departamento__r.Name + '</ciudad>\n';
                        respuesta += '\t\t\t\t\t<telefono>' + objContact.TGS_Address__r.BI_COL_Ciudad_Departamento__r.Name + '</telefono>\n';
                        respuesta += '\t\t\t\t\t<movil>' + objContact.MobilePhone + '</movil>\n';
                        respuesta += '\t\t\t\t\t<admonHolding>' + objAccount.BI_No_Identificador_fiscal__c + '</admonHolding>\n';
                        //Cerramos el tag de contacto
                        respuesta += '\t\t\t\t</contacto>\n';
                    }
                    //Cerramos el tag de contactos
                    respuesta += '\t\t\t</contactos>\n';
                }
                else
                {
                    respuesta += 'null';
                    //Cerramos el tag de contactos
                    respuesta += '</contactos>\n';
                }
                //Cerramos el tag de empresa
                respuesta += '\t\t</empresa>\n';
            }
            //Cerramos el tag de empresas
            respuesta += '\t</empresas>\n';
        }
        //Terminamos el XML
        respuesta += '</respuesta>';
        
        return respuesta;
    }
     
    @future (callout = true)
    global static void contactCanalOnline( String strIdContact, String strTipoNovedad )
    {
        //Obtenemos la lista de contactos asociados al Id (sólo 1)
        List<Contact> listContact =
        [
            SELECT  Id, FirstName, LastName, Phone, MobilePhone, BI_Tipo_de_documento__c, BI_Numero_de_documento__c,
                    Title, Department, TGS_Address__r.BI_COL_Ciudad_Departamento__r.Name,
                    TGS_Address__r.BI_COL_Ciudad_Departamento__r.BI_COL_Departamento__c, BI_COL_Direccion_oficina__c, Email,
                    BI_Country__c, AccountId, BI_Tipo_de_contacto__c, BI_COL_Principal__c,FS_CORE_Acceso_a_Portal_Platino__c //REING-21/07/2017    
            FROM    Contact
            WHERE   Id =: strIdContact AND BI_Country__c='Colombia'
            LIMIT 1 ];

        if( listContact.size() > 0 )
          
        //REING-21/07/2017    
        {   
            system.debug('@@@Antes de entrar ' +listContact[0].BI_Tipo_de_contacto__c);
            if (listContact[0].FS_CORE_Acceso_a_Portal_Platino__c)
        	{
            	if(listContact[0].BI_Tipo_de_contacto__c!=null)
            	{
                	listContact[0].BI_Tipo_de_contacto__c=listContact[0].BI_Tipo_de_contacto__c+';Administrador Canal Online';
                    system.debug('@@@Despues de entrar si no es NULO ' +listContact[0].BI_Tipo_de_contacto__c);
           		 }
                
                else{
                    listContact[0].BI_Tipo_de_contacto__c= 'Administrador Canal Online';
                    system.debug('@@@Despues de entrar si es NULO ' +listContact[0].BI_Tipo_de_contacto__c);
                	}
                   
       		 }
		//REING-21/07/2017  
			
            //Asignamos a una variable el objeto Contacto
            Contact objContact = listContact[0];
            //Traemos la información necesaria de la cuenta a la cual pertenece el contacto
            List<Account> listAccount =
            [
                SELECT  Id, Name,  BI_COL_Segmento_Telefonica__c,  BI_Subsegment_Regional__c,  
                        BI_No_Identificador_fiscal__c, Parent.BI_No_Identificador_fiscal__c, ParentId,
                    (   SELECT  UserId,user.firstname, user.lastname,TeamMemberRole,user.Email,user.BI_DNI_Per__c 
                        FROM    AccountTeamMembers 
                        WHERE   TeamMemberRole ='Service Manager')
                FROM    Account
                WHERE   Id =: objContact.AccountId ];

            if( listAccount.size() > 0 )
            {
                //Obtenemos el objeto Account
                Account objAccount = listAccount[0];
                //Creamos un objeto ws_CanalOnlineModel.Usuario
                ws_CanalOnlineModel.Usuario objUsuario = new ws_CanalOnlineModel.Usuario();
                //Asignamos valor a los atributos del objeto usuario
                objUsuario.TipoIdentificacion = objContact.BI_Tipo_de_documento__c;
                objUsuario.NumeroIdentificacion = objContact.BI_Numero_de_documento__c;
                objUsuario.Nombres = objContact.FirstName;
                objUsuario.Apellidos = objContact.LastName;
                objUsuario.Cargo = objContact.Title;
                objUsuario.Ciudad = objContact.TGS_Address__r.BI_COL_Ciudad_Departamento__r.Name;
                objUsuario.Departamento = objContact.TGS_Address__r.BI_COL_Ciudad_Departamento__r.BI_COL_Departamento__c; 
                objUsuario.Direccion = objContact.BI_COL_Direccion_oficina__c;
                objUsuario.EMail = objContact.Email;
                objUsuario.Pais = objContact.BI_Country__c;
                objUsuario.TelefonoCelular = objContact.MobilePhone;
                objUsuario.TelefonoFijo = objContact.Phone;
                objUsuario.UsuarioId = objContact.Id;
                //Creamos un objeto ws_CanalOnlineModel.Empresa Empresa
                ws_CanalOnlineModel.Empresa objEmpresa =
                    new ws_CanalOnlineModel.Empresa();
                //Asignamos valor a los atributos del objeto empresa
                objEmpresa.EmpresaId = objAccount.Id;
                objEmpresa.NIT = objAccount.BI_No_Identificador_fiscal__c;
                objEmpresa.NITPadre = objAccount.Parent.BI_No_Identificador_fiscal__c;
                objEmpresa.Nombre = objAccount.Name;
                objEmpresa.NombreCorto = '';//objAccount.Nombre_Original__c;
                objEmpresa.Segmento = objAccount.BI_COL_Segmento_Telefonica__c;
                objEmpresa.Subsegmento = objAccount.BI_Subsegment_Regional__c;
                //Creamos un objeto ws_CanalOnlineModel.Usuario
                ws_CanalOnlineModel.Service objService = new ws_CanalOnlineModel.Service();
                List<AccountTeamMember> objAccountTeamMembers = listAccount[0].AccountTeamMembers;
                //Asignamos valor a los atributos del objeto usuario
                if(!objAccountTeamMembers.isEmpty())
                {
                    objService.TipoIdentificacion = 'CC';
                    objService.NumeroIdentificacion = objAccountTeamMembers[0].user.BI_DNI_Per__c;
                    objService.Nombres = objAccountTeamMembers[0].user.FirstName;
                    objService.Apellidos = objAccountTeamMembers[0].user.LastName;
                    objService.EMail = objAccountTeamMembers[0].user.Email;
                    objService.ServiceId = objAccountTeamMembers[0].user.Id;
                }       
                //Creamos un objeto ws_CanalOnlineModel.reqNovedadContacto
                ws_CanalOnlineModel.reqNovedadContacto objReqNovedadContacto =
                    new ws_CanalOnlineModel.reqNovedadContacto();   
                    
                //Asignamos valor a los atributos del objeto
                objReqNovedadContacto.Contacto = objUsuario;
                objReqNovedadContacto.Empresa = objEmpresa;
                objReqNovedadContacto.TipoContacto = 'Administrador';
                objReqNovedadContacto.TipoNovedad = strTipoNovedad;
                objReqNovedadContacto.Service = objService;
                //Creamos un objeto Empresas
                ws_CanalOnlineModel.Empresas objEmpresas = new ws_CanalOnlineModel.Empresas();
                
                //Verificamos si el contacto es el administrador del holding
                if( objContact.BI_COL_Principal__c )
                {
                    List<Account> listAccountHolding =
                    [
                        SELECT  Id, Name, BI_COL_Segmento_Telefonica__c,  BI_Subsegment_Regional__c,
                                BI_No_Identificador_fiscal__c, Parent.BI_No_Identificador_fiscal__c, 
                                ParentId,
                            (   SELECT  UserId, user.firstname, user.lastname, TeamMemberRole 
                                FROM    AccountTeamMembers 
                                WHERE   TeamMemberRole ='Service Manager')
                        FROM    Account
                        WHERE   Parent.BI_No_Identificador_fiscal__c =: objAccount.BI_No_Identificador_fiscal__c
                        OR      (Parent.BI_No_Identificador_fiscal__c =: objAccount.Parent.BI_No_Identificador_fiscal__c
                        AND     Parent.BI_No_Identificador_fiscal__c != null) ];
                    
                    if( listAccountHolding.size() > 0 )
                    {
                        //Creamos un arreglo de Empresa
                        ws_CanalOnlineModel.Empresa[] arrEmpresa =
                            new ws_CanalOnlineModel.Empresa[listAccountHolding.size()];
                        
                        for( Integer i = 0; i < arrEmpresa.size(); i++ )
                        {
                            //Obtenemos el objeto account del holding
                            Account objAccountH = listAccountHolding[i];
                            //Creamos un objeto ws_CanalOnlineModel.Empresa EmpresaH
                            ws_CanalOnlineModel.Empresa objEmpresaH =
                                new ws_CanalOnlineModel.Empresa();
                            //Asignamos valor a los atributos del objeto empresa
                            objEmpresaH.EmpresaId = objAccountH.Id;
                            objEmpresaH.NIT = objAccountH.BI_No_Identificador_fiscal__c;
                            objEmpresaH.NITPadre = objAccountH.Parent.BI_No_Identificador_fiscal__c;
                            objEmpresaH.Nombre = objAccountH.Name;
                            //objEmpresaH.NombreCorto = objAccountH.Nombre_Original__c;
                            objEmpresaH.Segmento = objAccountH.BI_COL_Segmento_Telefonica__c;
                            objEmpresaH.Subsegmento = objAccountH.BI_Subsegment_Regional__c;
                            //Agregamos el objeto EmpresaH al arreglo
                            arrEmpresa[i] = objEmpresaH;
                        }
                        //Asignamos el arreglo de Empresa al objeto Empresas
                        objEmpresas.Empresa = arrEmpresa;
                    }
                }
                //Consumimos el web service
                ws_CanalOnlineModel.COCorpNovedadesEndpoint objWS =
                    new ws_CanalOnlineModel.COCorpNovedadesEndpoint();
                objWS.timeout_x = 60000;
                
                try
                {
                    ws_CanalOnlineModel.Resultado respuesta =
                        objWS.novedadContacto( objEmpresas, objReqNovedadContacto );
                    
                }
                catch( Exception error )
                {
                    System.debug('\n\n Lina 547 Error en el consumo del ws de canal online :' + error);
                }
                
                //Creamos un objeto ws_CanalOnlineModel.reqNovedadEmpresa
                ws_CanalOnlineModel.reqNovedadEmpresa objReqNovedadEmpresa =
                    new ws_CanalOnlineModel.reqNovedadEmpresa();    
                    
                //Asignamos valor a los atributos del objeto
                objReqNovedadEmpresa.Service = objService;
                objReqNovedadEmpresa.Empresa = objEmpresa;
                objReqNovedadEmpresa.TipoNovedad = strTipoNovedad;
                
                try
                {
                    ws_CanalOnlineModel.Resultado respuesta =
                        objWS.novedadEmpresa(objEmpresas, objReqNovedadEmpresa);
                    
                }
                catch( Exception error )
                {
                    System.debug('\n\nError en el consumo del ws de canal online :' + error);
                }
            }
        }
    }
    /**
    * @description: 
    */
    @future (callout = true)
    global static void accountCanalOnline( String strIdAccount, String strTipoNovedad )
    {
        //Traemos la información necesaria de la cuenta asociada al id recibido como parámetro
        List<Account> listAccount;

        if( strIdAccount != null && strIdAccount != '' )
        {
            listAccount =
            [
                SELECT Id, Name, BI_COL_Segmento_Telefonica__c, BI_Subsegment_Regional__c,BI_Segment__c,
                    (   SELECT  UserId,user.firstname, user.lastname,user.Email,TeamMemberRole 
                        FROM  AccountTeamMembers 
                        WHERE TeamMemberRole ='Service Manager'),
                    BI_No_Identificador_fiscal__c, Parent.BI_No_Identificador_fiscal__c, ParentId
                FROM    Account
                WHERE   Id =: strIdAccount AND BI_Country__c='Colombia'
                LIMIT 1 ];
        }

        if( listAccount.size() > 0 )
        {
            //Asignamos a una variable el objeto Account
            Account objAccount = listAccount[0];
            //Creamos un objeto ws_CanalOnlineModel.Usuarioseg
            ws_CanalOnlineModel.Service objService = new ws_CanalOnlineModel.Service();
             List<AccountTeamMember> objAccountTeamMembers = listAccount[0].AccountTeamMembers;
            //Asignamos valor a los atributos del objeto usuario
            objService.TipoIdentificacion = 'CC';
            //objService.NumeroIdentificacion = objAccountTeamMembers[0].user.Cedula__c;
            if( !objAccountTeamMembers.isEmpty() )
            {
                objService.Nombres = objAccountTeamMembers[0].user.FirstName;
                objService.Apellidos = objAccountTeamMembers[0].user.LastName;
                objService.EMail = objAccountTeamMembers[0].user.Email;
                objService.ServiceId = objAccountTeamMembers[0].UserId;
            } 
            //Creamos un objeto ws_CanalOnlineModel.Empresa Empresa
            ws_CanalOnlineModel.Empresa objEmpresa =
                new ws_CanalOnlineModel.Empresa();
            //Asignamos valor a los atributos del objeto empresa
            objEmpresa.EmpresaId = objAccount.Id;
            objEmpresa.NIT = objAccount.BI_No_Identificador_fiscal__c;
            objEmpresa.NITPadre = objAccount.Parent.BI_No_Identificador_fiscal__c;
            objEmpresa.Nombre = objAccount.Name;
            objEmpresa.Segmento = objAccount.BI_COL_Segmento_Telefonica__c;
            objEmpresa.Subsegmento = objAccount.BI_Subsegment_Regional__c;
            //Creamos un objeto ws_CanalOnlineModel.reqNovedadEmpresa
            ws_CanalOnlineModel.reqNovedadEmpresa objReqNovedadEmpresa =
                new ws_CanalOnlineModel.reqNovedadEmpresa();    
            //Asignamos valor a los atributos del objeto
            objReqNovedadEmpresa.Service = objService;
            objReqNovedadEmpresa.Empresa = objEmpresa;
            objReqNovedadEmpresa.TipoNovedad = strTipoNovedad;
            //Creamos un objeto Empresas
            ws_CanalOnlineModel.Empresas objEmpresas = new ws_CanalOnlineModel.Empresas();

            List<Account> listAccountHolding = new List<Account>();
            string sNoIdentificadorFiscalParent, sNoIdentificadorFiscalAccount;

            if( objAccount.Parent != null )
            {
                sNoIdentificadorFiscalParent = ( objAccount.Parent.BI_No_Identificador_fiscal__c != null ? 
                    objAccount.Parent.BI_No_Identificador_fiscal__c : null );
            }

            sNoIdentificadorFiscalAccount = ( objAccount.BI_No_Identificador_fiscal__c != null ?
                    objAccount.BI_No_Identificador_fiscal__c : null );

            if( objAccount != null && sNoIdentificadorFiscalAccount != null ||  sNoIdentificadorFiscalParent != null )
            {
                listAccountHolding =
                [
                    SELECT  Id, Name, BI_COL_Segmento_Telefonica__c, BI_Subsegment_Regional__c, 
                            BI_No_Identificador_fiscal__c,BI_Segment__c, Parent.BI_No_Identificador_fiscal__c, ParentId,
                            (   SELECT  UserId, user.firstname, user.lastname, TeamMemberRole 
                                FROM  AccountTeamMembers 
                                WHERE TeamMemberRole ='Service Manager')
                    FROM    Account
                    WHERE   Parent.BI_No_Identificador_fiscal__c =: sNoIdentificadorFiscalAccount
                    OR      (Parent.BI_No_Identificador_fiscal__c =: sNoIdentificadorFiscalParent
                    AND     Parent.BI_No_Identificador_fiscal__c != null) ];   
            }
            
            if( listAccountHolding.size() > 0 )
            {
                //Creamos un arreglo de Empresa
                ws_CanalOnlineModel.Empresa[] arrEmpresa =
                    new ws_CanalOnlineModel.Empresa[listAccountHolding.size()];

                for( Integer i = 0; i < arrEmpresa.size(); i++ )
                {
                    //Creamos un objeto ws_CanalOnlineModel.Empresa EmpresaH
                    ws_CanalOnlineModel.Empresa objEmpresaH =
                        new ws_CanalOnlineModel.Empresa();
                    //Asignamos valor a los atributos del objeto empresa
                    objEmpresaH.EmpresaId = listAccountHolding[i].Id;
                    objEmpresaH.NIT = listAccountHolding[i].BI_No_Identificador_fiscal__c;
                    objEmpresaH.NITPadre = listAccountHolding[i].Parent.BI_No_Identificador_fiscal__c;
                    objEmpresaH.Nombre = listAccountHolding[i].Name;
                    //objEmpresaH.NombreCorto = listAccountHolding[i].Nombre_Original__c;
                    objEmpresaH.Segmento = listAccountHolding[i].BI_COL_Segmento_Telefonica__c;
                    objEmpresaH.Subsegmento = listAccountHolding[i].BI_Subsegment_Regional__c;
                    //Agregamos el objeto EmpresaH al arreglo
                    arrEmpresa[i] = objEmpresaH;
                }
                //Asignamos el arreglo de Empresa al objeto Empresas
                objEmpresas.Empresa = arrEmpresa;
            }
            //Consumimos el web service
            ws_CanalOnlineModel.COCorpNovedadesEndpoint objWS =
                new ws_CanalOnlineModel.COCorpNovedadesEndpoint();

            objWS.timeout_x = 60000;
            
            try
            {
            	if(!test.isRunningTest()){ //OA: 05-10-2015 Ajuste de emergencia para evitar un error por MIXED_DML_OPERATION
	                ws_CanalOnlineModel.Resultado respuesta =
	                    objWS.novedadEmpresa( objEmpresas, objReqNovedadEmpresa );
	
	                fnCreateLog( String.valueOf( respuesta ), listAccount[0].id, 
	                    'Canal Online',String.valueOf( objEmpresas ), 'Exitoso', 'CANAL ONLINE' );
            	}
            }
            catch( Exception error )
            {
                fnCreateLog( error.getMessage(), listAccount[0].id, 
                    'Canal Online',String.valueOf( objEmpresas ), 'Fallido', 'CANAL ONLINE' );
                System.debug('\n\n##Error en el consumo del ws de canal online :' + error + '\n\n');
            }
        }
    }

    public static void fnCreateLog(string sRespuesta, string sId, string sInterfaz, string sInfoEnviada, string sEstado, string sTipoTransaccion )
    {
        BI_Log__c log = new BI_Log__c(); 
        log.BI_COL_Informacion_recibida__c = sRespuesta;
        log.BI_COL_Cuenta__c = sId;
        log.BI_COL_Interfaz__c = sInterfaz;
        log.BI_COL_Informacion_Enviada__c = sInfoEnviada; 
        log.BI_COL_Estado__c = sEstado;
        log.BI_COL_Tipo_Transaccion__c = sTipoTransaccion;
        insert log;
    }

    @InvocableMethod( Label = 'Enviar Usuario Canal Online' )
    public static void fnSendUser( list<User> lstUser )
    {
        List<AccountTeamMember> lstTeamMember = [
            SELECT  Id, TeamMemberRole
            FROM    AccountTeamMember
            WHERE   UserId =: lstUser[0].Id
            AND     TeamMemberRole = 'Service Manager'
            LIMIT 1 ];

            if( lstTeamMember.size() > 0 && !System.isFuture() )
                BI_COL_ChannelOnline_cls.userCanalOnline( lstUser[0].Id, 'Retiro' );
    }

    @future ( callout = true )
    global static void userCanalOnline(String strIdUser, String strTipoNovedad)
    {
        //Traemos la información necesaria de la cuenta asociada al id recibido como parámetro
        List<User> listUser = [
            SELECT  Id, FirstName, LastName, BI_User_Rut__c, Email 
            FROM    User
            WHERE   Id =: strIdUser
            LIMIT 1 ];

        if( listUser.size() > 0 )
        {
            //Asignamos a una variable el objeto Account
            User objUser = listUser[0];
            //Creamos un objeto ws_CanalOnlineModel.Usuario
            ws_CanalOnlineModel.Service objService = new ws_CanalOnlineModel.Service();
            //Asignamos valor a los atributos del objeto usuario
            objService.TipoIdentificacion = 'CC';
            objService.NumeroIdentificacion = objUser.BI_User_Rut__c;
            objService.Nombres = objUser.FirstName;
            objService.Apellidos = objUser.LastName;
            objService.EMail = objUser.Email;
            objService.ServiceId = objUser.Id;
            //Creamos un objeto ws_CanalOnlineModel.reqNovedadEmpresa
            ws_CanalOnlineModel.reqNovedadEmpresa objReqNovedadEmpresa =
                new ws_CanalOnlineModel.reqNovedadEmpresa();    
            //Asignamos valor a los atributos del objeto
            objReqNovedadEmpresa.Service = objService;
            //objReqNovedadEmpresa.Empresa = null;
            objReqNovedadEmpresa.TipoNovedad = strTipoNovedad;
            //Consumimos el web service
            ws_CanalOnlineModel.COCorpNovedadesEndpoint objWS =
                new ws_CanalOnlineModel.COCorpNovedadesEndpoint();
            try
            {
                ws_CanalOnlineModel.Resultado respuesta =
                    objWS.novedadEmpresa( null, objReqNovedadEmpresa );
            }
            catch(Exception error)
            {
                System.debug('\n\nError en el consumo del ws de canal online :' + error);
            }
        }
    }
}