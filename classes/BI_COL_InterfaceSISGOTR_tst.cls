/****************************************************************************************************
    Información general
    -------------------
    author: OJCB
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description:

    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       19-08- 2015     OJCB              			Creación de la Clase
    1.1    	  23-02-2017      Jaime Regidor           		Adding Campaign Values, Adding Catalog Country
    1.2		  27-02-2017	  Jaime Regidor					Adding Contact Values
			  13/03/2017		Marta Gonzalez(Everis)		REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
			  20/09/2017      Angel F. Santaliestra         Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
****************************************************************************************************/
@isTest
private class BI_COL_InterfaceSISGOTR_tst {

	public static BI_COL_ChangeMassiveBranch_ctr objCMassBranch;
	public static BI_COL_Modificacion_de_Servicio__c objMS;
	public static Account objCuenta;
	public static Account objCuenta2;
	public static Contact objContacto;
	public static Opportunity objOportunidad;
	public static BI_COL_Anexos__c objAnexos;
	public static BI_COL_Descripcion_de_servicio__c objDesSer;
	public static Contract objContrato;
	public static Campaign objCampana;
	public static BI_Col_Ciudades__c objCiudad;
	public static BI_Sede__c objSede;
	public static User objUsuario;
	public static BI_Punto_de_instalacion__c objPuntosInsta;
	public static BI_Log__c objBiLog;

	public static list <Profile> lstPerfil;
	public static list <UserRole> lstRoles;
	public static list <Campaign> lstCampana;
	public static list<BI_COL_manage_cons__c> lstManege;
	public static List<RecordType> lstRecTyp;
	public static List<User> lstUsuarios;
	public static List<BI_COL_Modificacion_de_Servicio__c> lstMS;
	public static List<BI_COL_Descripcion_de_servicio__c> lstDS;
	public static List<BI_COL_SessionContract_ctr.wrapperServicios> lstWraperServ;
	public static list<BI_Log__c> lstLog = new list <BI_Log__c>();
	public static BI_COL_Configuracion_campos_interfases__c objConfCamposInterfases1;
	public static void crearData() {
		//Roles
		lstRoles                = new list <UserRole>();
		lstRoles                = [Select id, Name from UserRole where Name = 'Telefónica Global'];
		system.debug('datos Rol ' + lstRoles);

		//Cuentas
		objCuenta 										= new Account();
		objCuenta.Name                                  = 'prueba';
		objCuenta.BI_Country__c                         = 'Colombia';
		objCuenta.TGS_Region__c                         = 'América';
		objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta.CurrencyIsoCode                       = 'COP';
		objCuenta.BI_Fraude__c                          = false;
		objCuenta.BI_Segment__c                         = 'test';
		objCuenta.BI_Subsegment_Regional__c             = 'test';
		objCuenta.BI_Territory__c                       = 'test';
		insert objCuenta;
		system.debug('datos Cuenta ' + objCuenta);

		objCuenta2                                       = new Account();
		objCuenta2.Name                                  = 'prueba';
		objCuenta2.BI_Country__c                         = 'Colombia';
		objCuenta2.TGS_Region__c                         = 'América';
		objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta2.CurrencyIsoCode                       = 'COP';
		objCuenta2.BI_Fraude__c                          = true;
		objCuenta2.BI_Segment__c                         = 'test';
		objCuenta2.BI_Subsegment_Regional__c             = 'test';
		objCuenta2.BI_Territory__c                       = 'test';
		insert objCuenta2;

		//Ciudad
		objCiudad = new BI_Col_Ciudades__c ();
		objCiudad.Name                      = 'Test City';
		objCiudad.BI_COL_Pais__c            = 'Test Country';
		objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
		insert objCiudad;

		//Contactos
		objContacto                                     = new Contact();
		objContacto.LastName                            = 'Test';
        objContacto.FirstName 							= 'firstname';
		objContacto.BI_Country__c                       = 'Colombia';
		objContacto.CurrencyIsoCode                     = 'COP';
		objContacto.AccountId                           = objCuenta.Id;
		objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
		objContacto.Phone                               = '12345678';
		objContacto.MobilePhone                         = '3104785925';
		objContacto.Email                               = 'pruebas@pruebas1.com';
		objContacto.BI_COL_Direccion_oficina__c         = 'Dirprueba 34550';
		objContacto.BI_COL_Ciudad_Depto_contacto__c     = objCiudad.Id;
		//REING-INI
        //objContacto.BI_Tipo_de_contacto__c          = 'Autorizado';
        objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
        objContacto.BI_Tipo_de_contacto__c          = 'General';
        //REING_FIN
		objContacto.BI_Tipo_de_documento__c				= 'RUT';	 //JRM
		objContacto.BI_Numero_de_documento__c			= '123456789'; //JRM
		Insert objContacto;

		//catalogo
		NE__Catalog__c objCatalogo 						= new NE__Catalog__c();
		objCatalogo.Name 								= 'prueba Catalogo';
		objCatalogo.BI_country__c                       = 'Colombia';
		insert objCatalogo;

		//Campaña
		objCampana 										= new Campaign();
		objCampana.Name 								= 'Campaña Prueba';
		objCampana.BI_Catalogo__c 						= objCatalogo.Id;
		objCampana.BI_COL_Codigo_campana__c 			= 'prueba1';
		objCampana.BI_Country__c 						= 'Colombia';
        objCampana.BI_Opportunity_Type__c 				= 'Fijo';
        objCampana.BI_SIMP_Opportunity_Type__c 			= 'Alta';
		insert objCampana;
		lstCampana 										= new List<Campaign>();
		lstCampana 										= [select Id, Name from Campaign where Id = : objCampana.Id];
		system.debug('datos Cuenta ' + lstCampana);

		//Tipo de registro
		lstRecTyp 										= [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
		system.debug('datos de FUN ' + lstRecTyp);

		//Contrato
		objContrato 									= new Contract ();
		objContrato.AccountId 							= objCuenta.Id;
		objContrato.StartDate 							=  System.today().addDays(+5);
		objContrato.BI_COL_Formato_Tipo__c 				= 'Contrato Marco';
		objContrato.BI_COL_Tipo_contrato__c             = 'Contrato Telefónica';
		objContrato.ContractTerm 						= 12;
		objContrato.BI_COL_Presupuesto_contrato__c 		= 1000000;
		objContrato.StartDate							= System.today().addDays(+5);
		objContrato.BI_Indefinido__c 					= 'Si';
		objContrato.BI_COL_Cuantia_Indeterminada__c 	= true;
		objContrato.BI_COL_Duracion_Indefinida__c 		= true;
		insert objContrato;
		System.debug('datos Contratos ===>' + objContrato);

		//FUN
		objAnexos               						= new BI_COL_Anexos__c();
		objAnexos.Name          						= 'FUN-0041414';
		objAnexos.RecordTypeId  						= lstRecTyp[0].Id;
		objAnexos.BI_COL_Contrato__c   					= objContrato.Id;
		insert objAnexos;
		System.debug('\n\n\n Sosalida objAnexos ' + objAnexos + '\n ====> objAnexos.Id ' + objAnexos.Id);

		//Configuracion Personalizada
		BI_COL_manage_cons__c objConPer 				= new BI_COL_manage_cons__c();
		objConPer.Name                  				= 'Viabilidades';
		objConPer.BI_COL_Numero_Viabilidad__c 			= 46;
		objConPer.BI_COL_ConsProyecto__c     			= 1;
		objConPer.BI_COL_ValorConsecutivo__c  			= 1;
		lstManege                      					= new list<BI_COL_manage_cons__c >();
		lstManege.add(objConPer);
		insert lstManege;
		System.debug('======= configuracion personalizada ======= ' + lstManege);

		//Oportunidad
		objOportunidad                                      	= new Opportunity();
		objOportunidad.Name                                   	= 'prueba opp';
		objOportunidad.AccountId                              	= objCuenta.Id;
		objOportunidad.BI_Country__c                          	= 'Colombia';
		objOportunidad.CloseDate                              	= System.today().addDays(+5);
		objOportunidad.StageName                              	= 'F6 - Prospecting';
		objOportunidad.CurrencyIsoCode                        	= 'COP';
		objOportunidad.Certa_SCP__contract_duration_months__c 	= 12;
		objOportunidad.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
		//objOportunidad.OwnerId                                	= lstUsuarios[0].id;
		insert objOportunidad;
		system.debug('Datos Oportunidad' + objOportunidad);
		list<Opportunity> lstOportunidad 	= new list<Opportunity>();
		lstOportunidad 						= [select Id from Opportunity where Id = : objOportunidad.Id];
		system.debug('datos ListaOportunida ' + lstOportunidad);

		//Ciudad
		objCiudad = new BI_Col_Ciudades__c ();
		objCiudad.Name 						= 'Test City';
		objCiudad.BI_COL_Pais__c 			= 'Test Country';
		objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
		insert objCiudad;
		System.debug('Datos Ciudad ===> ' + objSede);

		//Direccion
		objSede 								= new BI_Sede__c();
		objSede.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
		objSede.BI_Direccion__c 				= 'Test Street 123 Number 321';
		objSede.BI_Localidad__c 				= 'Test Local';
		objSede.BI_COL_Estado_callejero__c 		= System.Label.BI_COL_LbValor3EstadoCallejero;
		objSede.BI_COL_Sucursal_en_uso__c 		= 'Libre';
		objSede.BI_Country__c 					= 'Colombia';
		objSede.Name 							= 'Test Street 123 Number 321, Test Local Colombia';
		objSede.BI_Codigo_postal__c 			= '12356';
		insert objSede;
		System.debug('Datos Sedes ===> ' + objSede);

		//Sede
		objPuntosInsta 						= new BI_Punto_de_instalacion__c ();
		objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
		objPuntosInsta.BI_Sede__c          = objSede.Id;
		objPuntosInsta.BI_Contacto__c      = objContacto.id;
		objPuntosInsta.Name  				= 'Nombre caso';
		insert objPuntosInsta;
		System.debug('Datos Sucursales ===> ' + objPuntosInsta);

		//DS
		objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
		objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
		objDesSer.CurrencyIsoCode               			= 'COP';
		insert objDesSer;
		System.debug('Data DS ====> ' + objDesSer);
		System.debug('Data DS ====> ' + objDesSer.Name);
		lstDS 	= [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
		System.debug('Data lista DS ====> ' + lstDS);

		//MS
		objMS                         				= new BI_COL_Modificacion_de_Servicio__c();
		objMS.BI_COL_FUN__c           				= objAnexos.Id;
		objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
		objMS.BI_COL_Clasificacion_Servicio__c  	= 'ALTA';
		objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
		objMS.BI_COL_Bloqueado__c 					= false;
		objMS.BI_COL_Estado__c 						= 'Activa';//label.BI_COL_lblActiva;
		objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
		objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
		insert objMS;
		//MS
		objMS                         				= new BI_COL_Modificacion_de_Servicio__c();
		objMS.BI_COL_FUN__c           				= objAnexos.Id;
		objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
		objMS.BI_COL_Clasificacion_Servicio__c  	= 'SERVICIO INGENIERIA'; //JEG
		objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
		objMS.BI_COL_Bloqueado__c 					= false;
		objMS.BI_COL_Estado__c 						= 'Inactiva';//label.BI_COL_lblActiva;
		objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
		objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
		insert objMS; 
		//MS
		objMS                         				= new BI_COL_Modificacion_de_Servicio__c();
		objMS.BI_COL_FUN__c           				= objAnexos.Id;
		objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
		objMS.BI_COL_Clasificacion_Servicio__c  	= 'RENEGOCIACION'; //JEG
		objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
		objMS.BI_COL_Bloqueado__c 					= false;
		objMS.BI_COL_Estado__c 						= 'Inactiva';//label.BI_COL_lblActiva;
		objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
		objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
		//insert objMS; //JEG
		system.debug('Data objMs ===>' + objMs);
		lstMS = [select Name, BI_COL_Codigo_unico_servicio__c, BI_COL_Producto__r.Name, BI_COL_Descripcion_Referencia__c,
		         BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Sucursal_de_Facturacion__c, BI_COL_Codigo_unico_servicio__r.BI_COL_Sede_Destino__c,
		         BI_COL_Sucursal_Origen__c, /*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c from BI_COL_Modificacion_de_Servicio__c
		        ]; //10/11/2016 GSPM: Se comentó el campo antiguo y se adiciono nuevo campo davox1
		System.debug('datos lista MS ===> ' + lstMS);

		objBiLog											= new BI_Log__c();
		objBiLog.BI_COL_Informacion_recibida__c						= 'OK, Respuesta Procesada';
		//objBiLog.BI_COL_Contacto__c 									= lstContacts[0].Id;
		objBiLog.BI_COL_Estado__c										= 'FALLIDO';
		objBiLog.BI_COL_Interfaz__c									= 'NOTIFICACIONES';
		objBiLog.BI_COL_Informacion_Enviada__c						= 'INFO Enviada';
		lstLog.add(objBiLog);
		insert lstLog;

		BI_COL_Configuracion_campos_interfases__c objConfCamposInterfases = new BI_COL_Configuracion_campos_interfases__c();
		objConfCamposInterfases.BI_COL_Adicion__c = false;
		objConfCamposInterfases.BI_COL_Altas__c = false;
		objConfCamposInterfases.BI_COL_Bajas__c = false;
		objConfCamposInterfases.BI_COL_Calificador_Texto__c = null;
		objConfCamposInterfases.BI_COL_Cambio_Cuenta__c = false;
		objConfCamposInterfases.BI_COL_Cambio_Plan__c = false;
		objConfCamposInterfases.BI_COL_Cambio_servicio_alta__c = false;
		objConfCamposInterfases.BI_COL_Configuracion_Interfaz__c = null;
		objConfCamposInterfases.BI_COL_Convertir_mayuscula__c = false;
		objConfCamposInterfases.BI_COL_Decimales__c = null;
		objConfCamposInterfases.BI_COL_Descripcion__c = 'Envio de Instalaciones';
		objConfCamposInterfases.BI_COL_Direccion__c = 'Saliente';
		objConfCamposInterfases.BI_COL_Downgrade__c = false;
		objConfCamposInterfases.BI_COL_Formato_Campo__c = null;
		objConfCamposInterfases.BI_COL_Funcion_Especial__c = null;
		objConfCamposInterfases.BI_COL_Identificador__c = false;
		objConfCamposInterfases.BI_COL_ID_Colombia__c = 'a3Qg0000000ExpJEAS';
		objConfCamposInterfases.BI_COL_Inactivacion__c = false;
		objConfCamposInterfases.BI_COL_Ingreso__c = false;
		objConfCamposInterfases.BI_COL_Interfaz__c = 'SISGOT-INSTALACION';
		objConfCamposInterfases.BI_COL_Linea__c = null;
		objConfCamposInterfases.BI_COL_Longitud__c = null;
		objConfCamposInterfases.BI_COL_Nombre_campo__c = null;
		objConfCamposInterfases.BI_COL_Objeto__c = 'BI_COL_Modificacion_de_Servicio__c';
		objConfCamposInterfases.BI_COL_Observaciones__c = null;
		objConfCamposInterfases.BI_COL_Orden__c = null;
		objConfCamposInterfases.BI_COL_Permite_nulos__c = false;
		objConfCamposInterfases.BI_COL_Renegociacion__c = false;
		objConfCamposInterfases.BI_COL_Retiro__c = false;
		objConfCamposInterfases.BI_COL_Segmento__c = null;
		objConfCamposInterfases.BI_COL_Separador_Decimal__c = 'Õ';
		objConfCamposInterfases.BI_COL_Separador_Interno__c = '.';
		objConfCamposInterfases.BI_COL_Separador__c = 'Õ';
		objConfCamposInterfases.BI_COL_Servicios_de_IngenierIa__c = false;
		objConfCamposInterfases.BI_COL_Susp_Temporal_Facturacion__c = false;
		objConfCamposInterfases.BI_COL_Tipo_campo__c = null;
		objConfCamposInterfases.BI_COL_Tipo_transaccion__c = 'INSTALACIONES';
		objConfCamposInterfases.BI_COL_Traslado__c = false;
		objConfCamposInterfases.BI_COL_Upgrade__c = false;
		objConfCamposInterfases.BI_COL_Valor_predeterminado__c = null;
		objConfCamposInterfases.Name = 'Cabecera';
		insert objConfCamposInterfases;
		objConfCamposInterfases1 = new BI_COL_Configuracion_campos_interfases__c();
		objConfCamposInterfases1.BI_COL_Adicion__c = false;
		objConfCamposInterfases1.BI_COL_Altas__c = false;
		objConfCamposInterfases1.BI_COL_Bajas__c = false;
		objConfCamposInterfases1.BI_COL_Calificador_Texto__c = null;
		objConfCamposInterfases1.BI_COL_Cambio_Cuenta__c = false;
		objConfCamposInterfases1.BI_COL_Cambio_Plan__c = false;
		objConfCamposInterfases1.BI_COL_Cambio_servicio_alta__c = false;
		objConfCamposInterfases1.BI_COL_Configuracion_Interfaz__c = objConfCamposInterfases.id;
		objConfCamposInterfases1.BI_COL_Convertir_mayuscula__c = false;
		objConfCamposInterfases1.BI_COL_Decimales__c = null;
		objConfCamposInterfases1.BI_COL_Descripcion__c = 'Envio de Instalaciones';
		objConfCamposInterfases1.BI_COL_Direccion__c = 'Saliente';
		objConfCamposInterfases1.BI_COL_Downgrade__c = false;
		objConfCamposInterfases1.BI_COL_Formato_Campo__c = null;
		objConfCamposInterfases1.BI_COL_Funcion_Especial__c = null;
		objConfCamposInterfases1.BI_COL_Identificador__c = false;
		objConfCamposInterfases1.BI_COL_ID_Colombia__c = 'a3Qg0000000ExsKEAS';
		objConfCamposInterfases1.BI_COL_Inactivacion__c = false;
		objConfCamposInterfases1.BI_COL_Ingreso__c = false;
		objConfCamposInterfases1.BI_COL_Interfaz__c = 'SISGOT-INSTALACION';
		objConfCamposInterfases1.BI_COL_Linea__c = null;
		objConfCamposInterfases1.BI_COL_Longitud__c = null;
		objConfCamposInterfases1.BI_COL_Nombre_campo__c = 'BI_COL_Sucursal_Origen__r.BI_COL_Contacto_instalacion__r.Email';
		objConfCamposInterfases1.BI_COL_Objeto__c = 'BI_COL_Modificacion_de_Servicio__c';
		objConfCamposInterfases1.BI_COL_Observaciones__c = null;
		objConfCamposInterfases1.BI_COL_Orden__c = null;
		objConfCamposInterfases1.BI_COL_Permite_nulos__c = false;
		objConfCamposInterfases1.BI_COL_Renegociacion__c = false;
		objConfCamposInterfases1.BI_COL_Retiro__c = false;
		objConfCamposInterfases1.BI_COL_Segmento__c = null;
		objConfCamposInterfases1.BI_COL_Separador_Decimal__c = '.';
		objConfCamposInterfases1.BI_COL_Separador_Interno__c = ',';
		objConfCamposInterfases1.BI_COL_Separador__c = '~';
		objConfCamposInterfases1.BI_COL_Servicios_de_IngenierIa__c = false;
		objConfCamposInterfases1.BI_COL_Susp_Temporal_Facturacion__c = false;
		objConfCamposInterfases1.BI_COL_Tipo_campo__c = null;
		objConfCamposInterfases1.BI_COL_Tipo_transaccion__c = 'INSTALACIONES';
		objConfCamposInterfases1.BI_COL_Traslado__c = false;
		objConfCamposInterfases1.BI_COL_Upgrade__c = false;
		objConfCamposInterfases1.BI_COL_Valor_predeterminado__c = null;
		objConfCamposInterfases1.Name = 'Cabecera';
		insert objConfCamposInterfases1;
		System.debug('objConfCamposInterfases1=>' + objConfCamposInterfases1);


	}
	static testMethod void myUnitTest() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();
			Test.startTest();
			Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws() );
			//System.debug(BI_COL_InterfaceSISGOTR_cls.EnviarDatosBasicosDS(lstMS ));
			System.debug(BI_COL_InterfaceSISGOTR_cls.getConfInfz( 'DAVOXPLUS', 'SERV. CORPORATIVO - NOVEDADES', 'Saliente' ));
			System.debug(BI_COL_InterfaceSISGOTR_cls.configCampos('' + objConfCamposInterfases1.BI_COL_Configuracion_Interfaz__c ));
			System.debug(BI_COL_InterfaceSISGOTR_cls.interpretarRespuesta( 'la respuesta, interpretela, separada, por, comas'));
			System.debug( BI_COL_InterfaceSISGOTR_cls.EnviarDatosBasicosDS(lstMS));
			ws_TrsMasivo.InstalacionResponseDataType[] respuestas;
			Map<String, String> mapInfoEnviar = new  Map<String, String>();
			mapInfoEnviar.put('' + objDesSer.Id, '1Õ2Õ3Õ4Õ5Õ6Õ');
			BI_COL_InterfaceSISGOTR_cls.fnEnviarInfoSisgot( respuestas, mapInfoEnviar );
			Test.stopTest();
		}
	}

	static testMethod void myUnitTest2() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();
			Test.startTest();
			Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws() );
			//System.debug(BI_COL_InterfaceSISGOTR_cls.EnviarDatosBasicosDS(lstMS ));
			System.debug(BI_COL_InterfaceSISGOTR_cls.getConfInfz( 'DAVOXPLUS', 'SERV. CORPORATIVO - NOVEDADES', 'Saliente' ));
			System.debug(BI_COL_InterfaceSISGOTR_cls.configCampos('' + objConfCamposInterfases1.BI_COL_Configuracion_Interfaz__c ));
			System.debug(BI_COL_InterfaceSISGOTR_cls.interpretarRespuesta( 'la respuesta, interpretela, separada, por, comas'));
			System.debug( BI_COL_InterfaceSISGOTR_cls.EnviarDatosBasicosDS(lstMS));
			List<ws_TrsMasivo.InstalacionResponseDataType> respuestas = new List<ws_TrsMasivo.InstalacionResponseDataType>();
			ws_TrsMasivo.InstalacionResponseDataType obj = new ws_TrsMasivo.InstalacionResponseDataType();
			obj.codigo = '1';
			obj.error = 'error';
			respuestas.add(obj);
			Map<String, String> mapInfoEnviar = new  Map<String, String>();
			mapInfoEnviar.put(lstMS[0].Name, '1Õ2Õ3Õ4Õ5Õ6Õ');
			BI_COL_InterfaceSISGOTR_cls.fnEnviarInfoSisgot( respuestas, mapInfoEnviar );
			Test.stopTest();
		}
	}
}