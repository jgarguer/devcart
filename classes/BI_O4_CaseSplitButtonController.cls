/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Class BI_O4_CaseSplitButtonController for case split button
    Test Class:    BI_O4_CaseSplitButtonController_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera         Initial version
    20/10/2016              Fernando Arteaga       Added fields BI_O4_Oportunidad_externa__c, BI_O4_Type_of_Offer__c, BI_O4_URL_Cotizadores__c, BI_O4_Leading_Channel__c, BI_O4_Sales_Channel__c
    29/11/2016              Alvaro García          Change the subject of the new case
    06/06/2017              Álvaro López           Changed CreatedDate from parent case CreatedDate to today date (eHelp 02636646)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_CaseSplitButtonController {

    public List<Case> lstCase {get; set;}
    public Integer i = 0;
    private Boolean tieneTh = false;
    private Boolean tieneOp = false;
    private List<User> user {get; set;}
    private String TGS = UserInfo.getProfileId();

    public BI_O4_CaseSplitButtonController(ApexPages.StandardController controller) {
        
        Case caso = (Case) controller.getRecord();
        this.lstCase = [Select Id, OwnerId, BI_O4_DDO__c, RecordTypeId, RecordType.DeveloperName, Subject, Account.Name, Priority, Description, CreatedDate, BI_O4_Complexity__c, BI_O4_Autonomy__c,BI_O4_Estimated_FCV__c,
                               BI_Nombre_de_la_Oportunidad__c, BI_O4_Cliente_final_nombre__c, BI_O4_Es_proyecto_especial__c, BI_O4_Proyecto_Especial__c,BI_O4_Duration_of_Contract_months__c,
                               BI_O4_Tipo_gen_rico_Preventa__c, BI_O4_Tipo_de_Facturaci_n__c, BI_O4_Tipo_de_movimiento__c, BI_O4_Periodo_de_contrataci_n_Meses__c,BI_O4_Tipo_oferta__c,Origin,
                               BI_O4_Fecha_presentaci_n_oferta__c, BI_O4_Identificativo_Origen__c, BI_O4_Quantity__c, BI_Department__c, BI_Type__c, Status, BI_O4_Type_of_Support_Request__c,
                               TGS_Service__c, BI_O4_Service__c, BI_O4_Main_Topics__c, BI_O4_Main_Topics_Rechazados__c, BI_O4_Big_Deal__c, BI_Country__c,BI_O4_Cliente_no_existente_nombre__c,
                               BI_O4_Oportunidad_externa__c, BI_O4_Type_of_Offer__c, BI_O4_URL_Cotizadores__c, BI_O4_Leading_Channel__c, BI_O4_Sales_Channel__c
                        From Case
                        Where Id =: caso.Id Limit 1];

        this.user = [Select Profile.Name from User where ProfileId =: TGS limit 1];


    }   

    public PageReference splitCase() {


        if(user[0].Profile.Name != 'BI_O4_TGS_Standard' && user[0].Profile.Name != 'TGS System Administrator'){
            lstCase[0].addError('Solo los usuarios con perfil TGS Standar pueden hacer Split');
            return null;
        }else{

        try{

            Id idCaso;
            if(!this.lstCase.isEmpty()){
                for(Case caso : this.lstCase){
                    idCaso = caso.Id;
                }  
            }

            List<Case> lstCasosHijos = [SELECT Id, ParentId, BI_O4_Type_of_Support_Request__c FROM Case WHERE ParentId =: idCaso];
            for(Case caso : lstCasosHijos){
                if(caso.BI_O4_Type_of_Support_Request__c != null && caso.BI_O4_Type_of_Support_Request__c == 'Operational'){
                    tieneOp = true;
                }
                if(caso.BI_O4_Type_of_Support_Request__c != null && caso.BI_O4_Type_of_Support_Request__c == 'Technical'){
                    tieneTh = true;
                }
            }

            List<Case> newCases = new List<Case>();
            RecordType rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno'];
            if(!this.lstCase.isEmpty()){
                for(Case item : lstCase){
                    if(item.BI_O4_Type_of_Support_Request__c != null){
                        if(item.RecordTypeId == rt.Id && item.BI_Department__c == 'Ingeniería preventa' && item.BI_Type__c == 'Proyectos Especiales'){
                            //Debe crear 2 casos hijos asociados. Uno con valor de Type of Support Request=Technical y el otro Type of Support Request= Operational
                            if(item.BI_O4_Type_of_Support_Request__c == 'Both'){
                                if(!tieneOp){
                                    Case casoBoth = new Case();
                                    casoBoth.OwnerId = this.lstCase[0].OwnerId;
                                    casoBoth.AccountId = this.lstCase[0].AccountId;
                                    casoBoth.ParentId = this.lstCase[0].Id;
                                    casoBoth.RecordTypeId = this.lstCase[0].RecordTypeId;
                                    casoBoth.BI_Nombre_de_la_Oportunidad__c = this.lstCase[0].BI_Nombre_de_la_Oportunidad__c;
                                    //casoBoth.Subject = this.lstCase[0].BI_O4_Cliente_final_nombre__c + ' ' + this.lstCase[0].Account.Name + ' GEN';
                                    casoBoth.Subject = this.lstCase[0].Subject;
                                    casoBoth.Status = this.lstCase[0].Status;
                                    casoBoth.Priority = this.lstCase[0].Priority;
                                    casoBoth.Description = this.lstCase[0].Description;
                                    casoBoth.BI_O4_Es_proyecto_especial__c = this.lstCase[0].BI_O4_Es_proyecto_especial__c;
                                    casoBoth.BI_O4_Proyecto_Especial__c = this.lstCase[0].BI_O4_Proyecto_Especial__c;
                                    casoBoth.BI_O4_Tipo_gen_rico_Preventa__c = this.lstCase[0].BI_O4_Tipo_gen_rico_Preventa__c;
                                    casoBoth.BI_O4_Tipo_de_Facturaci_n__c = this.lstCase[0].BI_O4_Tipo_de_Facturaci_n__c;
                                    casoBoth.BI_O4_Tipo_de_movimiento__c = this.lstCase[0].BI_O4_Tipo_de_movimiento__c;
                                    casoBoth.BI_O4_Periodo_de_contrataci_n_Meses__c = this.lstCase[0].BI_O4_Periodo_de_contrataci_n_Meses__c;
                                    casoBoth.BI_O4_Fecha_presentaci_n_oferta__c = this.lstCase[0].BI_O4_Fecha_presentaci_n_oferta__c;
                                    casoBoth.BI_O4_Identificativo_Origen__c = this.lstCase[0].BI_O4_Identificativo_Origen__c;
                                    casoBoth.BI_O4_Type_of_Support_Request__c = 'Operational';
                                    casoBoth.BI_Department__c = this.lstCase[0].BI_Department__c;
                                    casoBoth.TGS_Service__c = this.lstCase[0].TGS_Service__c;
                                    casoBoth.BI_O4_Service__c = this.lstCase[0].BI_O4_Service__c;
                                    casoBoth.BI_O4_Main_Topics__c = this.lstCase[0].BI_O4_Main_Topics__c;
                                    casoBoth.BI_O4_Main_Topics_Rechazados__c = this.lstCase[0].BI_O4_Main_Topics_Rechazados__c;
                                    casoBoth.BI_O4_Big_Deal__c = this.lstCase[0].BI_O4_Big_Deal__c;
                                    casoBoth.BI_Country__c = this.lstCase[0].BI_Country__c;
                                    casoBoth.CreatedDate = System.now(); /*Álvaro López 06/06/2017 - Changed CreatedDate from parent case CreatedDate to today date (eHelp 02636646)*/
                                    casoBoth.BI_Type__c = this.lstCase[0].BI_Type__c;
                                    casoBoth.BI_O4_Cliente_no_existente_nombre__c = this.lstCase[0].BI_O4_Cliente_no_existente_nombre__c;
                                    casoBoth.Origin = this.lstCase[0].Origin;
                                    casoBoth.BI_O4_Tipo_oferta__c = this.lstCase[0].BI_O4_Tipo_oferta__c;
                                    casoBoth.BI_O4_Estimated_FCV__c = this.lstCase[0].BI_O4_Estimated_FCV__c;
                                    casoBoth.BI_O4_Duration_of_Contract_months__c = this.lstCase[0].BI_O4_Duration_of_Contract_months__c;
                                    casoBoth.BI_O4_Autonomy__c = this.lstCase[0].BI_O4_Autonomy__c;
                                    casoBoth.BI_O4_Complexity__c = this.lstCase[0].BI_O4_Complexity__c;
                                    casoBoth.BI_O4_Big_Deal__c = this.lstCase[0].BI_O4_Big_Deal__c;
                                    casoBoth.BI_O4_Oportunidad_externa__c = this.lstCase[0].BI_O4_Oportunidad_externa__c;
                                    casoBoth.BI_O4_Type_of_Offer__c = this.lstCase[0].BI_O4_Type_of_Offer__c;
                                    casoBoth.BI_O4_URL_Cotizadores__c = this.lstCase[0].BI_O4_URL_Cotizadores__c;
                                    casoBoth.BI_O4_Leading_Channel__c = this.lstCase[0].BI_O4_Leading_Channel__c;
                                    casoBoth.BI_O4_Sales_Channel__c = this.lstCase[0].BI_O4_Sales_Channel__c;

                                    newCases.add(casoBoth);
                                }
                                if(!tieneTh){
                                    Case casoBoth1 = new Case();
                                    casoBoth1.OwnerId = this.lstCase[0].OwnerId;
                                    casoBoth1.AccountId = this.lstCase[0].AccountId;
                                    casoBoth1.ParentId = this.lstCase[0].Id;
                                    casoBoth1.RecordTypeId = this.lstCase[0].RecordTypeId;
                                    casoBoth1.BI_Nombre_de_la_Oportunidad__c = this.lstCase[0].BI_Nombre_de_la_Oportunidad__c;
                                    //casoBoth1.Subject = this.lstCase[0].BI_O4_Cliente_final_nombre__c + ' ' + this.lstCase[0].Account.Name + ' GEN';
                                    casoBoth1.Subject = this.lstCase[0].Subject;
                                    casoBoth1.Status = this.lstCase[0].Status;
                                    casoBoth1.Priority = this.lstCase[0].Priority;
                                    casoBoth1.Description = this.lstCase[0].Description;
                                    casoBoth1.BI_O4_Es_proyecto_especial__c = this.lstCase[0].BI_O4_Es_proyecto_especial__c;
                                    casoBoth1.BI_O4_Proyecto_Especial__c = this.lstCase[0].BI_O4_Proyecto_Especial__c;
                                    casoBoth1.BI_O4_Tipo_gen_rico_Preventa__c = this.lstCase[0].BI_O4_Tipo_gen_rico_Preventa__c;
                                    casoBoth1.BI_O4_Tipo_de_Facturaci_n__c = this.lstCase[0].BI_O4_Tipo_de_Facturaci_n__c;
                                    casoBoth1.BI_O4_Tipo_de_movimiento__c = this.lstCase[0].BI_O4_Tipo_de_movimiento__c;
                                    casoBoth1.BI_O4_Periodo_de_contrataci_n_Meses__c = this.lstCase[0].BI_O4_Periodo_de_contrataci_n_Meses__c;
                                    casoBoth1.BI_O4_Fecha_presentaci_n_oferta__c = this.lstCase[0].BI_O4_Fecha_presentaci_n_oferta__c;
                                    casoBoth1.BI_O4_Identificativo_Origen__c = this.lstCase[0].BI_O4_Identificativo_Origen__c;
                                    casoBoth1.BI_O4_Type_of_Support_Request__c = 'Technical';
                                    casoBoth1.BI_Department__c = this.lstCase[0].BI_Department__c;
                                    casoBoth1.TGS_Service__c = this.lstCase[0].TGS_Service__c;
                                    casoBoth1.BI_O4_Service__c = this.lstCase[0].BI_O4_Service__c;
                                    casoBoth1.BI_O4_Main_Topics__c = this.lstCase[0].BI_O4_Main_Topics__c;
                                    casoBoth1.BI_O4_Main_Topics_Rechazados__c = this.lstCase[0].BI_O4_Main_Topics_Rechazados__c;
                                    casoBoth1.BI_O4_Big_Deal__c = this.lstCase[0].BI_O4_Big_Deal__c;
                                    casoBoth1.BI_Country__c = this.lstCase[0].BI_Country__c;
                                    casoBoth1.CreatedDate = System.now(); /*Álvaro López 07/06/2017 - Changed CreatedDate from parent case CreatedDate to today date (eHelp 02636646)*/
                                    casoBoth1.BI_Type__c = this.lstCase[0].BI_Type__c;
                                    casoBoth1.BI_O4_Cliente_no_existente_nombre__c = this.lstCase[0].BI_O4_Cliente_no_existente_nombre__c;
                                    casoBoth1.Origin = this.lstCase[0].Origin;
                                    casoBoth1.BI_O4_Tipo_oferta__c = this.lstCase[0].BI_O4_Tipo_oferta__c;
                                    casoBoth1.BI_O4_Estimated_FCV__c = this.lstCase[0].BI_O4_Estimated_FCV__c;
                                    casoBoth1.BI_O4_Duration_of_Contract_months__c = this.lstCase[0].BI_O4_Duration_of_Contract_months__c;
                                    casoBoth1.BI_O4_Autonomy__c = this.lstCase[0].BI_O4_Autonomy__c;
                                    casoBoth1.BI_O4_Complexity__c = this.lstCase[0].BI_O4_Complexity__c;
                                    casoBoth1.BI_O4_Big_Deal__c = this.lstCase[0].BI_O4_Big_Deal__c;
                                    casoBoth1.BI_O4_Oportunidad_externa__c = this.lstCase[0].BI_O4_Oportunidad_externa__c;
                                    casoBoth1.BI_O4_Type_of_Offer__c = this.lstCase[0].BI_O4_Type_of_Offer__c;
                                    casoBoth1.BI_O4_URL_Cotizadores__c = this.lstCase[0].BI_O4_URL_Cotizadores__c;
                                    casoBoth1.BI_O4_Leading_Channel__c = this.lstCase[0].BI_O4_Leading_Channel__c;
                                    casoBoth1.BI_O4_Sales_Channel__c = this.lstCase[0].BI_O4_Sales_Channel__c;
                                    
                                    newCases.add(casoBoth1);
                                }
                            }
                           
                            //Debe crear un caso hijo asociado con valor de Type of Support Request=Technical
                            else if(item.BI_O4_Type_of_Support_Request__c == 'Technical' && !tieneTh){

                                Case caso1 = new Case();
                                caso1.OwnerId = this.lstCase[0].OwnerId;
                                caso1.AccountId = this.lstCase[0].AccountId;
                                caso1.ParentId = this.lstCase[0].Id;
                                caso1.RecordTypeId = this.lstCase[0].RecordTypeId;
                                caso1.BI_Nombre_de_la_Oportunidad__c = this.lstCase[0].BI_Nombre_de_la_Oportunidad__c;
                                //caso1.Subject = this.lstCase[0].BI_O4_Cliente_final_nombre__c + ' ' + this.lstCase[0].Account.Name + ' GEN';
                                caso1.Subject = this.lstCase[0].Subject;
                                caso1.Status = this.lstCase[0].Status;
                                caso1.Priority = this.lstCase[0].Priority;
                                caso1.Description = this.lstCase[0].Description;
                                caso1.BI_O4_Es_proyecto_especial__c = this.lstCase[0].BI_O4_Es_proyecto_especial__c;
                                caso1.BI_O4_Proyecto_Especial__c = this.lstCase[0].BI_O4_Proyecto_Especial__c;
                                caso1.BI_O4_Tipo_gen_rico_Preventa__c = this.lstCase[0].BI_O4_Tipo_gen_rico_Preventa__c;
                                caso1.BI_O4_Tipo_de_Facturaci_n__c = this.lstCase[0].BI_O4_Tipo_de_Facturaci_n__c;
                                caso1.BI_O4_Tipo_de_movimiento__c = this.lstCase[0].BI_O4_Tipo_de_movimiento__c;
                                caso1.BI_O4_Periodo_de_contrataci_n_Meses__c = this.lstCase[0].BI_O4_Periodo_de_contrataci_n_Meses__c;
                                caso1.BI_O4_Fecha_presentaci_n_oferta__c = this.lstCase[0].BI_O4_Fecha_presentaci_n_oferta__c;
                                caso1.BI_O4_Identificativo_Origen__c = this.lstCase[0].BI_O4_Identificativo_Origen__c;
                                caso1.BI_O4_Type_of_Support_Request__c = 'Technical';
                                caso1.BI_Department__c = this.lstCase[0].BI_Department__c;
                                caso1.TGS_Service__c = this.lstCase[0].TGS_Service__c;
                                caso1.BI_O4_Service__c = this.lstCase[0].BI_O4_Service__c;
                                caso1.BI_O4_Main_Topics__c = this.lstCase[0].BI_O4_Main_Topics__c;
                                caso1.BI_O4_Main_Topics_Rechazados__c = this.lstCase[0].BI_O4_Main_Topics_Rechazados__c;
                                caso1.BI_O4_Big_Deal__c = this.lstCase[0].BI_O4_Big_Deal__c;
                                caso1.BI_Country__c = this.lstCase[0].BI_Country__c;
                                caso1.CreatedDate = System.now(); /*Álvaro López 07/06/2017 - Changed CreatedDate from parent case CreatedDate to today date (eHelp 02636646)*/
                                caso1.BI_Type__c = this.lstCase[0].BI_Type__c;
                                caso1.BI_O4_Cliente_no_existente_nombre__c = this.lstCase[0].BI_O4_Cliente_no_existente_nombre__c;
                                caso1.Origin = this.lstCase[0].Origin;
                                caso1.BI_O4_Tipo_oferta__c = this.lstCase[0].BI_O4_Tipo_oferta__c;
                                caso1.BI_O4_Estimated_FCV__c = this.lstCase[0].BI_O4_Estimated_FCV__c;
                                caso1.BI_O4_Duration_of_Contract_months__c = this.lstCase[0].BI_O4_Duration_of_Contract_months__c;
                                caso1.BI_O4_Autonomy__c = this.lstCase[0].BI_O4_Autonomy__c;
                                caso1.BI_O4_Complexity__c = this.lstCase[0].BI_O4_Complexity__c;
                                caso1.BI_O4_Big_Deal__c = this.lstCase[0].BI_O4_Big_Deal__c;
                                caso1.BI_O4_Oportunidad_externa__c = this.lstCase[0].BI_O4_Oportunidad_externa__c;
                                caso1.BI_O4_Type_of_Offer__c = this.lstCase[0].BI_O4_Type_of_Offer__c;
                                caso1.BI_O4_URL_Cotizadores__c = this.lstCase[0].BI_O4_URL_Cotizadores__c;
                                caso1.BI_O4_Leading_Channel__c = this.lstCase[0].BI_O4_Leading_Channel__c;
                                caso1.BI_O4_Sales_Channel__c = this.lstCase[0].BI_O4_Sales_Channel__c;
                                newCases.add(caso1);

                            }
                            //Debe crear un caso hijo asociado con valor de Type of Support Request=Operational
                            else if(item.BI_O4_Type_of_Support_Request__c == 'Operational' && !tieneOp){

                                Case caso2 = new Case();
                                caso2.OwnerId = this.lstCase[0].OwnerId;
                                caso2.AccountId = this.lstCase[0].AccountId;
                                caso2.ParentId = this.lstCase[0].Id;
                                caso2.RecordTypeId = this.lstCase[0].RecordTypeId;
                                caso2.BI_Nombre_de_la_Oportunidad__c = this.lstCase[0].BI_Nombre_de_la_Oportunidad__c;
                                //caso2.Subject = this.lstCase[0].BI_O4_Cliente_final_nombre__c + ' ' + this.lstCase[0].Account.Name + ' GEN';
                                caso2.Subject = this.lstCase[0].Subject;
                                caso2.Status = this.lstCase[0].Status;
                                caso2.Priority = this.lstCase[0].Priority;
                                caso2.Description = this.lstCase[0].Description;
                                caso2.BI_O4_Es_proyecto_especial__c = this.lstCase[0].BI_O4_Es_proyecto_especial__c;
                                caso2.BI_O4_Proyecto_Especial__c = this.lstCase[0].BI_O4_Proyecto_Especial__c;
                                caso2.BI_O4_Tipo_gen_rico_Preventa__c = this.lstCase[0].BI_O4_Tipo_gen_rico_Preventa__c;
                                caso2.BI_O4_Tipo_de_Facturaci_n__c = this.lstCase[0].BI_O4_Tipo_de_Facturaci_n__c;
                                caso2.BI_O4_Tipo_de_movimiento__c = this.lstCase[0].BI_O4_Tipo_de_movimiento__c;
                                caso2.BI_O4_Periodo_de_contrataci_n_Meses__c = this.lstCase[0].BI_O4_Periodo_de_contrataci_n_Meses__c;
                                caso2.BI_O4_Fecha_presentaci_n_oferta__c = this.lstCase[0].BI_O4_Fecha_presentaci_n_oferta__c;
                                caso2.BI_O4_Identificativo_Origen__c = this.lstCase[0].BI_O4_Identificativo_Origen__c;
                                caso2.BI_O4_Type_of_Support_Request__c = 'Operational';
                                caso2.BI_Department__c = this.lstCase[0].BI_Department__c;
                                caso2.TGS_Service__c = this.lstCase[0].TGS_Service__c;
                                caso2.BI_O4_Service__c = this.lstCase[0].BI_O4_Service__c;
                                caso2.BI_O4_Main_Topics__c = this.lstCase[0].BI_O4_Main_Topics__c;
                                caso2.BI_O4_Main_Topics_Rechazados__c = this.lstCase[0].BI_O4_Main_Topics_Rechazados__c;
                                caso2.BI_O4_Big_Deal__c = this.lstCase[0].BI_O4_Big_Deal__c;
                                caso2.BI_Country__c = this.lstCase[0].BI_Country__c;
                                caso2.CreatedDate = System.now(); /*Álvaro López 07/06/2017 - Changed CreatedDate from parent case CreatedDate to today date (eHelp 02636646)*/
                                caso2.BI_Type__c = this.lstCase[0].BI_Type__c;
                                caso2.BI_O4_Cliente_no_existente_nombre__c = this.lstCase[0].BI_O4_Cliente_no_existente_nombre__c;
                                caso2.Origin = this.lstCase[0].Origin;
                                caso2.BI_O4_Tipo_oferta__c = this.lstCase[0].BI_O4_Tipo_oferta__c;
                                caso2.BI_O4_Estimated_FCV__c = this.lstCase[0].BI_O4_Estimated_FCV__c;
                                caso2.BI_O4_Duration_of_Contract_months__c = this.lstCase[0].BI_O4_Duration_of_Contract_months__c;
                                caso2.BI_O4_Autonomy__c = this.lstCase[0].BI_O4_Autonomy__c;
                                caso2.BI_O4_Complexity__c = this.lstCase[0].BI_O4_Complexity__c;
                                caso2.BI_O4_Big_Deal__c = this.lstCase[0].BI_O4_Big_Deal__c;
                                caso2.BI_O4_Oportunidad_externa__c = this.lstCase[0].BI_O4_Oportunidad_externa__c;
                                caso2.BI_O4_Type_of_Offer__c = this.lstCase[0].BI_O4_Type_of_Offer__c;
                                caso2.BI_O4_URL_Cotizadores__c = this.lstCase[0].BI_O4_URL_Cotizadores__c;
                                caso2.BI_O4_Leading_Channel__c = this.lstCase[0].BI_O4_Leading_Channel__c;
                                caso2.BI_O4_Sales_Channel__c = this.lstCase[0].BI_O4_Sales_Channel__c;
                                newCases.add(caso2);
                            }
                            else{

                                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, String.valueOf(Label.BI_O4_SplitCasesCreated)));
                                return null;
                            }
                        }
                    }
                    else{

                        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, String.valueOf(Label.BI_O4SplitCasesNull)));
                        return null;
                    }
                }
                insert newCases;
            }

            return new PageReference('/' + this.lstCase[0].Id);

        }catch(Exception e){

            BI_LogHelper.generate_BILog('BI_O4_CaseSplitButtonController.splitCase', 'BI_EN', e, 'Clase');
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, String.valueOf(e.getMessage())));
            return null;
        }   


        }

        
    }

    public PageReference cancel(){

        return new PageReference('/' + this.lstCase[0].Id);
    }
}