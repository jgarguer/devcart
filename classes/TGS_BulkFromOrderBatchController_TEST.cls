@isTest
public class TGS_BulkFromOrderBatchController_TEST {

    static testMethod void myUnitTest() 
    {
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c = 'TGS';
        insert u;
        
     System.runAs(u) {  
        Id recordtypeAccId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_LEGAL_ENTITY);
        Account acc = new Account(Name='Account', RecordTypeId=recordtypeAccId);
         insert acc;
     
         Id rtBUId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_BUSINESS_UNIT);
        Account acc2	=	new Account(name = 'ThisIsATest2', ParentId = acc.Id, RecordTypeId=rtBUId, TGS_Aux_Legal_Entity__c = acc.Id);
    	insert acc2;
        System.debug('BU: ' + acc2.Id);
    	
        Id rtCCId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_COST_CENTER);
         Account acc3	=	new Account(name = 'ThisIsATest3', ParentId = acc2.Id, RecordTypeId= rtCCId);
    	insert acc3;

        BI_Sede__c sede = new BI_Sede__c(Name = 'Test sede', BI_Codigo_postal__c = '12345', BI_Direccion__c = 'Direccion', BI_Localidad__c = 'Localidad', BI_Provincia__c = 'Provincia', TGS_Available_Accounts__c = acc.TGS_Aux_Holding__c);
        insert sede;

        List<BI_Punto_de_instalacion__c> lst_sites = new List<BI_Punto_de_instalacion__c>();
        for(Integer i=0; i<1001; i++){
            BI_Punto_de_instalacion__c site = new BI_Punto_de_instalacion__c(BI_Cliente__c = acc2.Id, Name = 'Test site '+ i, BI_Sede__c = sede.Id);
            lst_sites.add(site);
        }
        insert lst_sites;
       
	   	NE__Catalog_Header__c catHead = new NE__Catalog_Header__c(Name='Test Catalog', NE__Name__c='Test Catalog');
	    insert catHead;
	   
        NE__Catalog__c	cat = new NE__Catalog__c(NE__Catalog_Header__c=catHead.Id, NE__StartDate__c=Datetime.now(), NE__Active__c = true);
	    insert cat;
        
        NE__Catalog_Category__c catCat = new NE__Catalog_Category__c(Name='Category', NE__CatalogId__c=cat.Id);
	    insert catCat;
        
        RecordType RTProd = [SELECT Id FROM RecordType WHERE (SobjectType = 'NE__Product__c' OR SobjectType = 'Product__c') AND Name = 'Standard'];
        NE__Product__c prod1 = new NE__Product__c(Name='Test Product', RecordTypeId=RTProd.Id);
        insert prod1;
        NE__Product__c prod2 = new NE__Product__c(Name='Test Product 2', RecordTypeId=RTProd.Id);
        insert prod2;
        NE__Product__c prod3 = new NE__Product__c(Name='Test Product 3', RecordTypeId=RTProd.Id);
        insert prod3;
        
        NE__Family__c family	=	new NE__Family__c(name = 'FamilyTest');
	    insert family;
        
        NE__DynamicPropertyDefinition__c prop	=	new NE__DynamicPropertyDefinition__c(name = 'TestProperty',NE__Type__c ='Enumerated');
    	insert prop;
        
        NE__ProductFamily__c prodFamily	=	new NE__ProductFamily__c(NE__ProdId__c = prod1.id, NE__FamilyId__c = family.id);
    	insert prodFamily;
        
        NE__ProductFamilyProperty__c pfp1 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=family.Id, NE__PropId__c=prop.Id);
    	insert pfp1;
        
        NE__Contract_Header__c contractHead = new NE__Contract_Header__c(NE__Name__c='Contract Header');
        insert contractHead;
        NE__Order__c ord = new NE__Order__c(NE__AccountId__c=acc.Id, NE__BillAccId__c=acc3.Id, NE__ServAccId__c=acc2.Id, NE__CatalogId__c=cat.Id, NE__OrderStatus__c='Pending', NE__ConfigurationStatus__c='Valid',NE__Contract_Header__c=contractHead.Id, NE__Order_date__c=Datetime.now().AddDays(1),NE__Type__c='New');
        insert ord;
        
        NE__Catalog_Item__c catItem1 = new NE__Catalog_Item__c(NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, NE__Recurring_Charge_Frequency__c='Monthly');
	    insert catItem1;
        
        NE__OrderItem__c ordit1 = new NE__OrderItem__c(NE__OrderId__c=ord.Id, NE__ProdId__c=prod1.Id,NE__CatalogItem__c=catItem1.Id,NE__RecurringChargeFrequency__c =null, NE__Qty__c=1,NE__Action__c='Add',NE__Root_Order_Item__c=null, NE__Status__c ='In progress');
    	insert ordit1;
        NE__OrderItem__c ordit1SL = new NE__OrderItem__c(NE__OrderId__c=ord.Id,NE__Parent_Order_Item__c= ordit1.Id, NE__ProdId__c=prod1.Id,NE__CatalogItem__c=catItem1.Id,NE__RecurringChargeFrequency__c =null, NE__Qty__c=1,NE__Action__c='Add',NE__Root_Order_Item__c= ordit1.Id);
    	insert ordit1SL;
        NE__OrderItem__c ordit1TL = new NE__OrderItem__c(NE__OrderId__c=ord.Id,NE__Parent_Order_Item__c= ordit1SL.Id, NE__ProdId__c=prod1.Id,NE__CatalogItem__c=catItem1.Id,NE__RecurringChargeFrequency__c =null, NE__Qty__c=1,NE__Action__c='Add');
    	insert ordit1TL;
        
        NE__ProductFamilyProperty__c productFamilyProp	=	new NE__ProductFamilyProperty__c(NE__PropId__c = prop.id,NE__FamilyId__c = family.id, NE__Required__c='true');
    	insert productFamilyProp;
        NE__Order_Item_Attribute__c attr1 = new NE__Order_Item_Attribute__c(Name='Test Attribute 1', NE__Order_Item__c=ordit1.Id, NE__FamPropId__c=productFamilyProp.Id, NE__Value__c='Premier Monthly Rate',NE__Action__c='Add');
		insert attr1;
        
        String fifteenDigitId = prod1.Id;
        Id eighteenDigitId = fifteenDigitId;        
                
        NE__Lov__c lovList = new NE__Lov__c(NE__Type__c ='FieldsRequiredForService', NE__Value2__c=eighteenDigitId , Text_Area_Value__c='fam1:att1;fam2:att2' );
        insert lovList;
        
		String orderId = System.currentPagereference().getParameters().put('orderId', ord.Id); 
				  
		TGS_BulkFromOrderBatchController contr = new TGS_BulkFromOrderBatchController();
				
		List<TGS_BulkFromOrderBatchController.StructureWrapper> swrapp = contr.getStructure(orderId);

		List<TGS_BulkFromOrderBatchController.RowWrapper> compData = contr.getList(orderId,swrapp);
		
		contr.generateDataGrid();

		Bit2WinHUB__Bulk_Import_Request__c bir =new Bit2WinHUB__Bulk_Import_Request__c();
		insert bir; 
         
		Pagereference bulkImportBP = Page.Bit2WinHUB__BulkImportBatchProgress;
        bulkImportBP.getParameters().put('birId',bir.Id);
        Test.setCurrentPageReference(bulkImportBP);

		String head_ConfigurationID = 'Configuration ID';
        String head_Account = 'Account';
        String head_Catalog = 'Catalog';
        String head_Category = 'Category';
        String head_ConfItemRoot = 'Configuration Item Level 1';
        String head_ConfItemChild1 = 'Configuration Item Level 2';
        String head_ConfItemChild2 = 'Configuration Item Level 3';
        String head_Qty = 'Quantity';
        String head_Family = 'Family';
        String head_DynPropDef = 'Dynamic Property Definition';
        String head_Value = 'Value';
        String head_ID = 'ID';
        String head_Configuration = 'Configuration ';


		String csvFile = head_ConfigurationID+';'+head_Account+';'+head_Catalog+';'+head_Category+';'+head_ConfItemRoot+';'+head_Category+';'+head_ConfItemChild1
                            +';'+head_Category+';'+head_ConfItemChild2+';'+head_Qty+';'+head_Family+';'+head_DynPropDef+';'+head_Value+';'+head_ID
                            +';'+'Configuration HoldingId__c;Configuration NE__ServAccId__c;Configuration Cost_Center__c;Configuration Configuration Site__c;Configuration Authorized_User__c;Configuration NE__AccountId__c;Configuration NE__Contract_Account__c';
        
        csvFile += 'ORD-001;'+acc.Name+';'+catHead.Name+';;;;;;;;;;;;;\n';
        csvFile	+= ';;;'+catCat.Name+';'+prod1.Name+';;;;;3;'+family.Name+';'+prop.Name+';This is a value;;'+acc.Id+';'+acc2.Id+';'+acc3.Id+';;'+u.Id+';'+acc.Id+';'+contractHead.Id+'\n';
        csvFile += 'ORD-003;'+acc.Name+';'+catHead.Name+';'+catCat.Name+';'+catItem1.NE__ProductId__r.Name+';'+catCat.Name+';'+catItem1.NE__ProductId__r.Name+';'+catCat.Name+';'+catItem1.NE__ProductId__r.Name+';1;;;;;;;;;;;;\n';
		
		test.startTest();
		contr.echoVal();
		contr.importFile(csvFile);
		test.stopTest();

	 }
    }
    
   
}