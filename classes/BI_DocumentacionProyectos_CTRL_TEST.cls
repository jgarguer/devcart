@isTest
private class BI_DocumentacionProyectos_CTRL_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Class to manage coverage of BI_DocumentacionProyectos_CTRL

    History: 
    
     <Date>                     <Author>                <Change Description>
    14/11/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	@isTest static void BI_DocumentacionProyectos_CTRL_TEST() {

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
		List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
		Opportunity opp = new Opportunity(Name = 'Test_opp',
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                                          AccountId = lst_acc[0].Id,
                                          BI_Ciclo_ventas__c = Label.BI_Completo,
                                          BI_Country__c = lst_pais[0]);
		insert opp;

		Milestone1_Project__c proj = new Milestone1_Project__c ();   		
			       		proj.Name = 'TEST PROJ ' + Datetime.now().getTime();
			        	proj.BI_Oportunidad_asociada__c = opp.Id;
			        	proj.BI_Etapa_del_proyecto__c = Label.BI_Kickoff_inicio;
			        	proj.BI_Country__c = lst_pais[0];
			        	proj.BI_Complejidad_del_proyecto__c=Label.BI_Complejo;
		insert proj;

		list<BI_Configuracion_de_documentacion__c> lst_confDoc = new list<BI_Configuracion_de_documentacion__c>();
		BI_Configuracion_de_documentacion__c confDoc_1 = new BI_Configuracion_de_documentacion__c(
				BI_Etapa_del_proyecto__c	 = Label.BI_Kickoff_inicio,
				BI_Nombre_del_documento__c	 = 'test_doc',
				BI_Complejidad_del_proyecto__c = Label.BI_Complejo,
				BI_Objeto__c	 = 'Proyecto',
				BI_Country__c =  lst_pais[0]
				);

		lst_confDoc.add(confDoc_1);

		BI_Configuracion_de_documentacion__c confDoc_2 = new BI_Configuracion_de_documentacion__c(
				BI_Etapa_del_proyecto__c	 = Label.BI_Kickoff_inicio,
				BI_Nombre_del_documento__c	 = 'doc',
				BI_Complejidad_del_proyecto__c = Label.BI_Complejo,
				BI_Objeto__c	 = 'Proyecto',
				BI_Country__c =  lst_pais[0]
				);
		lst_confDoc.add(confDoc_2);

		insert lst_confDoc;

		Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        Attachment attach=new Attachment(Name='Unit Test Attachment',
        								body=bodyBlob,
        								parentId=proj.Id, 
        								Description = 'test_doc',
        								ContentType='image/png');  
        
        insert attach; 

        Test.startTest();

        BI_DocumentacionProyectos_CTRL clase = new BI_DocumentacionProyectos_CTRL( new Apexpages.StandardController(proj) );
	        system.assert(!clase.listConfigurationWrapper.isEmpty());
			system.assert(!clase.listConfigurationWrapper2.isEmpty());
        Test.stopTest();
	}
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}