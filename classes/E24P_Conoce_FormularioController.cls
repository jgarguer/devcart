public class E24P_Conoce_FormularioController {
    
    @AuraEnabled
    public static E24P_Account_Plan__c doLoadAccountPlan(Id acc_planId) {
 E24P_Account_Plan__c accountPlan = [SELECT  Id, OwnerId, IsDeleted, Name, CurrencyIsoCode, CreatedDate, CreatedById, LastModifiedDate, 
                                     LastModifiedById, SystemModstamp, LastViewedDate, LastReferencedDate, ConnectionReceivedId, ConnectionSentId, 
                                     E24P_Cliente__c, E24P_Cifra_de_negocio_de_tu_cliente__c, E24P_Gasto_real_TIC__c, E24P_Cantidad_de_empleados__c,
                                     E24P_Empleados_crece_o_decrece__c, E24P_Indica_sucursales_agencias__c, E24P_Sucursales_crece_decrece__c, 
                                     E24P_Presencia_internacional_cliente__c, E24P_Principales_empresas_competidoras_1__c, 
                                     E24P_Principales_empresas_competidoras_2__c, E24P_Principales_empresas_competidoras_3__c, 
                                     E24P_Canales_utilizados_por_cliente__c, E24P_Contacto_con_afinidad_1__r.Name, 
                                     E24P_Contacto_con_afinidad_1__r.Id, E24P_Contacto_con_afinidad_1__r.Title, E24P_Contacto_con_afinidad_2__r.Name,
                                     E24P_Contacto_con_afinidad_2__r.Id, E24P_Contacto_con_afinidad_2__r.Title, E24P_Contacto_con_afinidad_3__r.Name,
                                     E24P_Contacto_con_afinidad_3__r.Id, E24P_Contacto_con_afinidad_3__r.Title, E24P_Contacto_con_afinidad_4__r.Name,
                                     E24P_Contacto_con_afinidad_4__r.Id, E24P_Contacto_con_afinidad_4__r.Title, E24P_Contacto_con_afinidad_5__r.Name,
                                     E24P_Contacto_con_afinidad_5__r.Id, E24P_Contacto_con_afinidad_5__r.Title, E24P_Sector__c, E24P_Subsector__c, 
                                     E24P_Cliente_no_informa_del_dato__c, E24P_Cantidad_de_sedes__c, E24P_Rango_de_empleados__c, 
                                     E24P_Agenda_periodica_ejecutiva__c, E24P_Gasto_total_TIC_Telefonica__c, E24P_Agenda_periodica_operacional__c, 
                                     E24P_Top_5_de_proovedores__c, E24P_Buen_vinculo_entre_TEF_y_cliente__c, 
                                     E24P_Quien_prospecta_la_venta_tecnologia__c, E24P_Estas_en_el_futuro_de_tu_cliente__c, 
                                     E24P_planes_estrategicos_de_tu_cliente__c, E24P_Gasto_TIC_promedio_de_la_industria__c 
                                          FROM E24P_Account_Plan__c WHERE Id = :acc_planId LIMIT 1];                          
        return accountPlan;
    } 
    
    @AuraEnabled
    public static Map<String, Map<String, String>> doGetPicklist()
    {
        Schema.DescribeFieldResult fieldResultSucursales = E24P_Account_Plan__c.E24P_Sucursales_crece_decrece__c.getDescribe();
        List<Schema.PicklistEntry> pleSucursales = fieldResultSucursales.getPicklistValues();
        Map<String, String> sucursales = new Map<String, String>();
        for(Schema.PicklistEntry f : pleSucursales) {
            sucursales.put(f.getLabel(), f.getValue());
        }       
        Map<String, Map<String, String>> result = new Map<String, Map<String, String>>();
        result.put('sucursales', sucursales);
        
        Schema.DescribeFieldResult fieldResultEmpleados = E24P_Account_Plan__c.E24P_Empleados_crece_o_decrece__c.getDescribe();
        List<Schema.PicklistEntry> pleEmpleados = fieldResultEmpleados.getPicklistValues();
        Map<String, String> empleados = new Map<String, String>();
        for(Schema.PicklistEntry f : pleEmpleados) {
            empleados.put(f.getLabel(), f.getValue());
        }       
        result.put('empleados', empleados);
        
        Schema.DescribeFieldResult fieldResultPresencia = E24P_Account_Plan__c.E24P_Presencia_internacional_cliente__c.getDescribe();
        List<Schema.PicklistEntry> plePresencia = fieldResultPresencia.getPicklistValues();
        Map<String, String> presencia = new Map<String, String>();
        for(Schema.PicklistEntry f : plePresencia) {
            presencia.put(f.getLabel(), f.getValue());
        }       
        result.put('presencia', presencia);
        
        Schema.DescribeFieldResult fieldResultCanales = E24P_Account_Plan__c.E24P_Canales_utilizados_por_cliente__c.getDescribe();
        List<Schema.PicklistEntry> pleCanales = fieldResultCanales.getPicklistValues();
        Map<String, String> canales = new Map<String, String>();
        for(Schema.PicklistEntry f : pleCanales) {
        canales.put(f.getLabel(), f.getValue());
        }       
        result.put('canales', canales);
        
                Schema.DescribeFieldResult fieldResultEjecutiva = E24P_Account_Plan__c.E24P_Agenda_periodica_ejecutiva__c.getDescribe();
        List<Schema.PicklistEntry> pleEjecutiva = fieldResultEjecutiva.getPicklistValues();
        Map<String, String> ejecutiva = new Map<String, String>();
        for(Schema.PicklistEntry f : pleEjecutiva) {
        ejecutiva.put(f.getLabel(), f.getValue());
        }       
        result.put('ejecutiva', ejecutiva);
        
        Schema.DescribeFieldResult fieldResultOperacional = E24P_Account_Plan__c.E24P_Agenda_periodica_operacional__c.getDescribe();
        List<Schema.PicklistEntry> pleOperacional = fieldResultOperacional.getPicklistValues();
        Map<String, String> operacional = new Map<String, String>();
        for(Schema.PicklistEntry f : pleOperacional) {
        operacional.put(f.getLabel(), f.getValue());
        }       
        result.put('operacional', operacional);
        
        Schema.DescribeFieldResult fieldResultVenta = E24P_Account_Plan__c.E24P_Quien_prospecta_la_venta_tecnologia__c.getDescribe();
        List<Schema.PicklistEntry> pleVenta = fieldResultVenta.getPicklistValues();
        Map<String, String> venta = new Map<String, String>();
        for(Schema.PicklistEntry f : pleVenta) {
        venta.put(f.getLabel(), f.getValue());
        }       
        result.put('venta', venta);
        
                Schema.DescribeFieldResult fieldResultProveedores = E24P_Account_Plan__c.E24P_Top_5_de_proovedores__c.getDescribe();
        List<Schema.PicklistEntry> pleProveedores = fieldResultProveedores.getPicklistValues();
        Map<String, String> proveedores = new Map<String, String>();
        for(Schema.PicklistEntry f : pleProveedores) {
        proveedores.put(f.getLabel(), f.getValue());
        }       
        result.put('proveedores', proveedores);
        
                Schema.DescribeFieldResult fieldResultVinculo = E24P_Account_Plan__c.E24P_Buen_vinculo_entre_TEF_y_cliente__c.getDescribe();
        List<Schema.PicklistEntry> pleVinculo = fieldResultVinculo.getPicklistValues();
        Map<String, String> vinculo = new Map<String, String>();
        for(Schema.PicklistEntry f : pleVinculo) {
        vinculo.put(f.getLabel(), f.getValue());
        }       
        result.put('vinculo', vinculo);
        
                Schema.DescribeFieldResult fieldResultFuturo = E24P_Account_Plan__c.E24P_Estas_en_el_futuro_de_tu_cliente__c.getDescribe();
        List<Schema.PicklistEntry> pleFuturo = fieldResultFuturo.getPicklistValues();
        Map<String, String> futuro = new Map<String, String>();
        for(Schema.PicklistEntry f : pleFuturo) {
        futuro.put(f.getLabel(), f.getValue());
        }       
        result.put('futuro', futuro);
        
                Schema.DescribeFieldResult fieldResultPlanes = E24P_Account_Plan__c.E24P_planes_estrategicos_de_tu_cliente__c.getDescribe();
        List<Schema.PicklistEntry> plePlanes = fieldResultPlanes.getPicklistValues();
        Map<String, String> planes = new Map<String, String>();
        for(Schema.PicklistEntry f : plePlanes) {
        planes.put(f.getLabel(), f.getValue());
        }       
        result.put('planes', planes);
        
        return result;  
    }	
    
    @AuraEnabled
    public static boolean doSave(E24P_Account_Plan__c acc_plan) {
        boolean result = false;
        try {
            upsert(acc_plan);
            result = true;
        } catch (Exception e) {
            throw new AuraHandledException('Se ha producido un error al guardar la lista');
        }
        return result;
    }
}