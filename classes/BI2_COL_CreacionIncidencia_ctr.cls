/**
**************************************************************************************************************
* @company          Avanxo Colombia
* @author           Antonio Torres href=<atorres@avanxo.com>
* @project          Telefonica BI_EN Fase 2 Colombia
* @name             BI2_COL_CreacionIncidencia_ctr
* @description      Controller extension for visualforce page "BI2_COL_CreacionIncidencia_pag"
*                   Based on BIIN_CreacionIncidencia_Ctrl.
* @dependencies     None
* @changes (Version)
* --------   ---   ----------   ---------------------------   ------------------------------------------------
*            No.   Date         Author                        Description
* --------   ---   ----------   ---------------------------   ------------------------------------------------
* @version   1.0   2017-02-01   Antonio Torres (AT)           Initial version.
**************************************************************************************************************
**/

public class BI2_COL_CreacionIncidencia_ctr {
    BIIN_Creacion_Ticket_WS.CamposOutsiders co=new BIIN_Creacion_Ticket_WS.CamposOutsiders();
    public Case caso {get;set;}
    //public Attachment attach {get;set;}
    public String  contact_Id {get; set;}
    public String account_Id {get; set;}
    public contact contactoRelated {get; set;}
    public Account accountRelated {get; set;}
    //public boolean MasDeTres {get; set;}
    public boolean displayPopup {get; set;}
    public boolean displayPopup2 {get; set;}
    public boolean displayPopup3 {get; set;}
    public boolean displayPopup4 {get; set;}
    public boolean displayPopUpAdjuntos  {get; set;}
    //public List <Attachment> ListaAdjuntos; //Moved to method
    //public List <String> ListaNombres {get; set;}
    //public integer ContAdj {get; set;}   //Meter en el método, creo que no hace falta como variable global
    public String siteName {get; set;}
    public String siteFullName {get; set;}
    public String emailContacto {get; set;}
    public String nombreContacto {get; set;}
    public String apellidoContacto {get; set;}
    public Id EmailInput {get; set;}
    public String CIname {get; set;}
    public Id CIinput {get; set;}
    public Id siteInput {get; set;}
    public String StringWSCreacion {get; set;}
    public String rutaBmc {get; set;}
    public boolean MostrarPopUpCategOperac {get; set;}
    public Transient List<BIIN_Creacion_Ticket_WS.Adjunto> la; //Transient in order to down the view state size.
    public boolean UpdateCatDistintas;

    public Transient  Blob adjunto1 {get; set;}
    public String nombre1 {get; set;}
    public Transient  Blob adjunto2 {get; set;}
    public String nombre2 {get; set;}
    public Transient  Blob adjunto3 {get; set;}
    public String nombre3 {get; set;}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Constructor, donde se definen las variables globales y se toman los valores heredados de páginas padre como el id contacto, cuenta,
                       el caso padre.

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI2_COL_CreacionIncidencia_ctr(ApexPages.StandardSetController controller){
            caso = new Case();
            system.debug('Categorias Constructor' + caso.BIIN_Categorization_Tier_1__c + caso.BIIN_Categorization_Tier_2__c + caso.BIIN_Categorization_Tier_3__c);
            NamedCredential nc=[SELECT endpoint FROM NamedCredential WHERE developerName='BIIN_RoD' LIMIT 1];
            rutaBmc=nc.endpoint;
            if(Test.isRunningTest()){
                siteName = '';
                emailContacto  = '';
                nombreContacto   = '';
                apellidoContacto   = '';
                EmailInput   = caso.id;
                CIname   = '';
                CIinput   = caso.id;
                siteInput     = caso.id;
            }
            UpdateCatDistintas = false;
            MostrarPopUpCategOperac = false;
            StringWSCreacion = '';
            displayPopup2 = false;
            displayPopup3 = false;
            displayPopup4 = false;
            displayPopUpAdjuntos = false;
        //ContAdj = 0;
        //YaAdjuntado = false;
        //MasDeTres = false;
        caso.OwnerId = Userinfo.getUserId();
        contact_Id = System.currentPagereference().getParameters().get('def_contact_id');
        account_Id = System.currentPagereference().getParameters().get('def_account_id');
        string ParentNumberRelated = System.currentPagereference().getParameters().get('cas28');
        String tipoRegistroId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BIIN_Solicitud_Incidencia_Tecnica' limit 1].Id;
        caso.recordtypeid = tipoRegistroId;
        caso.Type='First instance';
        caso.Reason = 'Incidencia Técnica';
        contactoRelated = new contact();
        accountRelated = new Account();
        if(contact_Id != null){
            contactoRelated = [SELECT Name, Id FROM contact WHERE Id=: contact_Id];
        }
        if(account_Id != null){
         List<Account> lst_acc = [SELECT Name, BI_Country__c, BI_Validador_Fiscal__c, BI_No_Identificador_fiscal__c, Id FROM account WHERE Id=: account_Id];
            //accountRelated = [SELECT Name, BI_Country__c, BI_Validador_Fiscal__c,  Id FROM account WHERE Id=: account_Id];

            if(!lst_acc.isEmpty()){
                accountRelated = lst_acc[0];
                caso.AccountId = account_Id;
                caso.BI_Country__c = accountRelated.BI_Country__c;
            }

        }
        if(contact_Id != null){
            caso.ContactId = contact_Id;
        }
        if(ParentNumberRelated != null){
            caso.ParentId = [SELECT Id FROM case WHERE caseNumber =: ParentNumberRelated ].Id;
            system.debug( 'Id Caso Padre -->' + caso.ParentId );
        }

        //attach = new Attachment();
        //ListaAdjuntos  = new List <Attachment> (); //Moved to method
        //ListaNombres  = new List <String> ();

        //INICIO MODIFICACION: Recoge los parameteros que lleguen de la pagina New Case Custom

        caso.subject = System.currentPagereference().getParameters().get('subject');
        caso.status = System.currentPagereference().getParameters().get('status');
        //caso.caseorigin = System.currentPagereference().getParameters().get('caseorigin');
        caso.priority = System.currentPagereference().getParameters().get('priority');

        //FIN MODIFICACION

        //validarUserBackground();

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función que comprueba que, las tres categorías que el usuario ha seleccionado coinciden con algún registro de
                       la tabla BIIN_Tabla_Cat_Operacionales__c (Custom Setting) y en caso de no coincidir, active el popup de error

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void CambioCatOperacional(){
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'CambioCatOperacional' + '   <<<<<<<<<<\n-=#=-\n');
        string Categoria1 = Apexpages.currentPage().getParameters().get('Categoria1');
        string Categoria2 = Apexpages.currentPage().getParameters().get('Categoria2');
        string Categoria3 = Apexpages.currentPage().getParameters().get('Categoria3');
        System.debug('\n\n-=#=-\n' + 'Categorias Operacionales Seleccionadas' + ': ' + Categoria1 +'-'+ Categoria2 +'-'+ Categoria3 + '\n-=#=-\n');
        Integer i=[SELECT count() FROM BIIN_Tabla_Cat_Operacionales__c where Cat_Operacional1__c=: Categoria1 and  Cat_Operacional2__c=: Categoria2 and Cat_Operacional3__c=: Categoria3];
        System.debug(' \n\n I = '+ i +' \n\n');
        if(i<1){
            UpdateCatDistintas = true;
            MostrarPopUpCategOperac = true;
        } else {
            UpdateCatDistintas = false;
        }
        System.debug('\n\n-=#=-\n' + 'UpdateCatDistintas' + ': ' + UpdateCatDistintas + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'MostrarPopUpCategOperac' + ': ' + MostrarPopUpCategOperac + '\n-=#=-\n');
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función que crea el ticket, después de haber sido insertado en Salesforce, en Remedy. De no tener éxito, asigna una tarea pendiente

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void crearTicketRoD(){
        BIIN_Obtener_Token_BAO ot=new BIIN_Obtener_Token_BAO();
        BIIN_Creacion_Ticket_WS ct=new BIIN_Creacion_Ticket_WS();
        String authorizationToken = ''; // ot.obtencionToken();
        Integer nIntentos=0;
        Boolean exito=false;
        system.debug('CASO' + caso);
        do
        {
            exito=ct.crearTicket(authorizationToken, caso, la,co);
            nIntentos++;
        }
        while((nIntentos<3)&&(exito==false));

        if(Test.isRunningTest())
        {
            exito = false;
        }

        StringWSCreacion = ct.StringWS;
        system.debug('BooleanWSCreacion----->' + StringWSCreacion);
        if(!exito)
        {
            //lanzar la tarea porque no hemos podido contactar con el bao
            caso.Status='Confirmation pending';
            caso.BIIN_Descripcion_error_integracion__c = 'Es necesario relanzar manualmente el ticket,pulse el botón Relanzar';
            StringWSCreacion = 'PENDING';
            //MOSTRAR EL POPUP
            try
            {
                update caso;
            }
            catch(DmlException e)
            {
                system.debug('Error al insertar el estado del caso: '+e);
            }

            Task tarea = new Task();
            tarea.WhatId = caso.Id;
            tarea.WhoId = caso.ContactId;
            tarea.Subject = caso.Subject;
            //tarea.OwnerId = [SELECT BI2_asesor__c FROM Account WHERE Id =: caso.AccountId].Id; //El campo Custom es el asesor asociado al Cliente (Userinfo.getUserId();)
            tarea.Description = caso.Description;
            //tarea.Status = caso.Status;
            system.debug('TASK -->'+ caso.Subject + caso.Description +caso.Status);
            insert tarea;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función que se ejecuta después de guardar el ticket en SF y llama a la creación de Remedy

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference onClickGuardar() {
        loadAccount();
        if(caso.BI_COL_Fecha_Radicacion__c <= Datetime.now()) {
            crearTicketRoD();
        } else {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,''+'La fecha de solicitud no puede ser superior a la actual'));
        }
        return null;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función para insertar ticket en SF previa verificación de fecha de solicitud correcta.

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference insertarSolicitudTecnica() {
		loadAccount();
        if(caso.BIIN_Categorization_Tier_2__c == '__') caso.BIIN_Categorization_Tier_2__c  = null;
        if(caso.BIIN_Categorization_Tier_3__c == '__') caso.BIIN_Categorization_Tier_3__c  = null;
        if(caso.BI_Product_Service__c == '__') caso.BI_Product_Service__c  = null;

        caso.BIIN_Site__c                  = siteName;
        caso.TGS_Site_details_contacts__c  = siteFullName;

        if(caso.BI_COL_Fecha_Radicacion__c > Datetime.now())
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,''+'La fecha de solicitud no puede ser superior a la actual'));
        }


        if(caso.AccountId != null && caso.BI_Country__c == null)
        {
            List<Account> lst_acc = [SELECT Name, BI_Country__c, BI_Validador_Fiscal__c,  Id FROM account WHERE Id=: caso.AccountId];
            //accountRelated = [SELECT Name, BI_Country__c, BI_Validador_Fiscal__c,  Id FROM account WHERE Id=: account_Id];

            if(!lst_acc.isEmpty())
            {
                caso.BI_Country__c = lst_acc[0].BI_Country__c;
            }
        }

        if(caso.BI_COL_Fecha_Radicacion__c == null)
        {
            caso.BI_COL_Fecha_Radicacion__c = Datetime.now();
        }

        if(UpdateCatDistintas == true || Test.isRunningTest())
        {
            system.debug('Ha entrado en Cat no válidas ---> CatNull');
            caso.BIIN_Categorization_Tier_1__c = null;
            caso.BIIN_Categorization_Tier_2__c = null;
            caso.BIIN_Categorization_Tier_3__c = null;
        }

        try
        {
            caso.Status = 'Being processed';
            upsert caso;

            system.debug('Inserción Caso Salesforce: ' + caso);
            system.debug('Adjunto1: ' + adjunto1);
            system.debug('Adjunto2: ' + adjunto2);
            system.debug('Adjunto3: ' + adjunto3);

            Boolean hasAttachments = GuardarTicketAdjuntoSF();
            if(hasAttachments)
            {
                // If case has attachments, show PopUps, Can´t use JS onComplete OR onClick in file´s transaction.
                System.debug('ENTRA EN ADJUNTOS');

                // By default before calling Remedy
                StringWSCreacion = 'ADJUNTOS';

                // Call Remedy
                onClickGuardarFuturo(JSON.serialize(caso), JSON.serialize(la));

                MostrarPopUpEstado();
            }

            return null;
        }
        catch(Exception e)
        {
            //Apexpages.addMessages(e);
            displayPopUp = false;
            return null;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Save attachments in future call

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @Future(callout=true)
    public static void onClickGuardarFuturo(String casoStr, String attachmentsStr) {
        Case caso = (Case) JSON.deserialize(casoStr, Case.class);
        List<BIIN_Creacion_Ticket_WS.Adjunto> attachments = (List<BIIN_Creacion_Ticket_WS.Adjunto>) JSON.deserialize(attachmentsStr, List<BIIN_Creacion_Ticket_WS.Adjunto>.class);

        if(caso.BI_COL_Fecha_Radicacion__c > Datetime.now())
        {
            return;
        }

        BIIN_Creacion_Ticket_WS ct = new BIIN_Creacion_Ticket_WS();
        Integer nIntentos = 0;
        Boolean exito = false;

        do
        {
            exito = ct.crearTicket(null, caso, attachments, null);
            nIntentos++;
        }
        while((nIntentos<3) && (exito==false));

        if(Test.isRunningTest())
        {
            exito = false;
        }

        System.debug(' BI2_COL_CreacionIncidencia_ctr.onClickGuardarFuturo | exito: ' + exito);

        if(!exito)
        {
            // lanzar la tarea porque no hemos podido contactar con el bao
            BI2_COL_CreacionIncidencia_ctr.setTicketError(caso, 'Confirmation pending', 'Es necesario relanzar manualmente el ticket, pulse el botón Relanzar.');
        }

        System.debug(' BI2_COL_CreacionIncidencia_ctr.onClickGuardarFuturo | ct.StringWS: ' + ct.StringWS);

        if (ct.StringWS.contains('SVC1021'))
        {
            BI2_COL_CreacionIncidencia_ctr.setTicketError(caso, 'Error de validación', ct.StringWS);
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Save ticket error in the case

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void setTicketError(Case caso, String status, String errorMessage) {
        caso.Status = status;
        caso.BIIN_Descripcion_error_integracion__c = errorMessage;

        try {
            update caso;
        } catch(DmlException e) {
            System.debug('Error al insertar el estado del caso: ' + e);
        }

        Task tarea = new Task();
        tarea.WhatId = caso.Id;
        tarea.WhoId = caso.ContactId;
        tarea.Subject = caso.Subject;
        tarea.Description = caso.Description;
        insert tarea;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función para guardar el  ticket con adjuntos en Remedy, previamente guardado en Salesforce.

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Boolean GuardarTicketAdjuntoSF() {
        system.debug('GuardarTicketAdjuntoSF --- ENTRA');
        List <Attachment> ListaAdjuntos = new List<Attachment>();
        la = new List<BIIN_Creacion_Ticket_WS.Adjunto>();

        if (adjunto1 != null) ListaAdjuntos.add(new Attachment(Name = nombre1, Body = adjunto1));
        if (adjunto2 != null) ListaAdjuntos.add(new Attachment(Name = nombre2, Body = adjunto2));
        if (adjunto3 != null) ListaAdjuntos.add(new Attachment(Name = nombre3, Body = adjunto3));

        try {
            if(!ListaAdjuntos.isEmpty()) {
                for (Attachment att: ListaAdjuntos) {
                    att.ParentId = caso.Id;
                }

                insert ListaAdjuntos;

                System.debug(' LIST1 > '+ ListaAdjuntos);

                for (Attachment att: ListaAdjuntos) {
                    System.debug('Adjunto nombre: ' + att);
                    BIIN_Creacion_Ticket_WS.Adjunto a = new BIIN_Creacion_Ticket_WS.Adjunto(att.Name,att.Body,att.Id);
                    System.debug('Adjunto nombre: ' + a);

                    la.add(a);
                }

                ListaAdjuntos.clear(); //Reset adjunto variable to down view size.
                adjunto1 = null;
                adjunto2 = null;
                adjunto3 = null;

                system.debug(' LIST2 > '+ la);

                return true;
            }

            return false; //Case don´t has attachments
        }
        catch(DmlException ex) {
            system.debug('GuardarTicketAdjuntoSF.DmlException --- ex: ' + ex);
            ApexPages.addMessages(ex);
            MostrarPopUpEstado();
            return true;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función para almacenar la descripción del error que ha sucedido en la integración .

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void MostrarPopUpEstado() {
        System.debug('MostrarPopUpEstado -->  StringWSCreacion= ' + StringWSCreacion);

        displayPopup = false;

        if(caso.BI_COL_Fecha_Radicacion__c <= Datetime.now()) {
            if(StringWSCreacion == 'OK') {
                displayPopUp2 = true;
                caso.Status = 'Being processed';
                caso.BIIN_Descripcion_error_integracion__c = 'Petición en curso. Mientras el ticket no se haya procesado podrá consultar su ticket como Solicitud Incidencia Técnica. En breves momentos recibirá una notificación y podrá consultar su ticket como una Incidencia Técnica.';
                update caso;
            } else if(StringWSCreacion == 'PENDING') {
                displayPopUp4 = true;
            } else if(StringWSCreacion == 'ADJUNTOS') {
                displayPopUpAdjuntos = true;
                caso.Status = 'Being processed';
                caso.BIIN_Descripcion_error_integracion__c = 'Petición en curso. Mientras el ticket no se haya procesado podrá consultar su ticket como Solicitud Incidencia Técnica.';
                update caso;
            } else {
                displayPopUp3 = true;
                caso.Status = 'Error de validación';
                caso.BIIN_Descripcion_error_integracion__c = StringWSCreacion;
                update caso;
            }
        } else {
             Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,''+'La fecha de solicitud no puede ser superior a la actual'));
        }
     }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función que lleva al listado de casos (Cuando el ticket ha sido correctamente guardado en ambos entornos).

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public pageReference OKdetalle() {
        return new PageReference('/500/o');
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función asociada al botón Cancelar para cancelar la creación del ticket y volver al menú de casos.

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    Public PageReference Cancel(){
        return new PageReference ('/500/o');
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Funciones que guardan los datos del contacto final, la Ubicación y CI mientras el usuario navega por la página.

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void GuardarDatos(){
        caso.BI2_Nombre_Contacto_Final__c = Apexpages.currentPage().getParameters().get('NombreContactoFinal');
        caso.BI2_Apellido_Contacto_Final__c = Apexpages.currentPage().getParameters().get('ApellidoContactoFinal');
        caso.BI2_Email_Contacto_Final__c = Apexpages.currentPage().getParameters().get('EmailContactoFinal');
        caso.BI_ECU_Telefono_fijo__c = Apexpages.currentPage().getParameters().get('TelefonoFijoContactoFinal');
        caso.BI_ECU_Telefono_movil__c = Apexpages.currentPage().getParameters().get('TelefonoMovilContactoFinal');
    }

    public void GuardarUbicacion() {
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'GuardarUbicacion' + '   <<<<<<<<<<\n-=#=-\n');
        caso.BIIN_Site__c                    = Apexpages.currentPage().getParameters().get('UbicacionController');
        caso.TGS_Site_details_contacts__c    = Apexpages.currentPage().getParameters().get('UbicacionFullController');

        siteName      = caso.BIIN_Site__c;
        siteFullName  = caso.TGS_Site_details_contacts__c;

        System.debug('\n\n-=#=-\n' + 'Ubicación' + ': ' + siteName + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'Ubicación Full' + ': ' + siteFullName + '\n-=#=-\n');
    }

    public void GuardarCI(){
        caso.BIIN_Id_Producto__c = Apexpages.currentPage().getParameters().get('CIController');
        system.debug('CI -->' + caso.BIIN_Id_Producto__c);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función que vacía los campos de contacto final, CI e Ubicación al cambiar de Cliente.

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference ReiniciarCampos(){
        caso.BIIN_Site__c = null;
        caso.TGS_Site_details_contacts__c = null;
        caso.BI2_Nombre_Contacto_Final__c = null;
        caso.BI2_Apellido_Contacto_Final__c = null;
        caso.BI2_Email_Contacto_Final__c = null;
        caso.BI_ECU_Telefono_fijo__c = null;
        caso.BI_ECU_Telefono_movil__c = null;
        caso.BIIN_Id_Producto__c  = null;
        caso.BI_Confidencial__c=true;
        AccountRelated.BI_No_Identificador_fiscal__c='';
       return null;
   }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función que vacía los campos  CI e Ubicación al cambiar la línea de negocio.

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void ValidarCIUbicacion(){
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'ValidarCIUbicacion' + '   <<<<<<<<<<\n-=#=-\n');
        //caso.BIIN_Site__c = '';
        caso.BIIN_Id_Producto__c  = '';
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función que cierra la ventana de adjuntos y de error de categorías operacionales.

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void closePopup3() {
        displayPopup = false;
        if(StringWSCreacion.contains('SVC1021')) {
            displayPopUp3 = false;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función que abre la ventana de adjuntos.

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void showPopup3() {
        System.debug('\n\n-=#=-\n' + 'caso' + ': ' + caso + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'Ubicación --> caso.BIIN_Site__c' + ': ' + caso.BIIN_Site__c + '\n-=#=-\n');
        displayPopup = true;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función que abre el popup de error de categorías operacionales.

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void ClosePopUpCategOperac()
        {
        MostrarPopUpCategOperac = false;
        UpdateCatDistintas = true;
        caso.BIIN_Categorization_Tier_1__c = '';
        caso.BIIN_Categorization_Tier_2__c = '';
        caso.BIIN_Categorization_Tier_3__c = '';
        system.debug('Categorias ' + caso.BIIN_Categorization_Tier_1__c + caso.BIIN_Categorization_Tier_2__c + caso.BIIN_Categorization_Tier_3__c);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Método que actualiza la cuenta relacionada al caso

        History:

        <Date>         <Author>                     <Description>
        2017-02-01     Antonio Torres               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference loadAccount() {
        String strAccountId = Apexpages.currentPage().getParameters().get('strAccountId');

        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'loadAccount' + '   <<<<<<<<<<\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'strAccountId' + ': ' + strAccountId + '\n-=#=-\n');
		System.debug('\n\n-=#=-\n' + 'strAccountId' + ': ' + caso.AccountId + '\n-=#=-\n');

        if(strAccountId != null) {
            System.debug('\n\n-=#=-\n' + 'caso.AccountId' + ': ' + caso.AccountId + '\n-=#=-\n');

            List<Account> lstAccounts = [SELECT Name
                                               ,BI_Country__c
                                               ,BI_Validador_Fiscal__c
                                               ,BI_No_Identificador_fiscal__c
                                               ,Id
                                           FROM Account
                                          WHERE Id =: strAccountId];

            if(!lstAccounts.isEmpty()) {
                System.debug('\n\n-=#=-\n' + 'lstAccounts' + ': ' + lstAccounts + '\n-=#=-\n');
                accountRelated = lstAccounts[0];
                caso.BI_Country__c = accountRelated.BI_Country__c;
            } else {
                System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'lstAccounts EMPTY' + '   <<<<<<<<<<\n-=#=-\n');
            }
        } else {
            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'caso.AccountId NULO' + '   <<<<<<<<<<\n-=#=-\n');
        }

        return null;
    }
}