@isTest (SeeAllData=true)
private class TestChangeRequestConfItem {

    static testMethod void myUnitTest() {
        // Insert account
        Account acc = new Account(Name = 'Test Account',
                                 BI_Segment__c = 'test', //28/09/2017
                                 BI_Subsegment_Regional__c = 'test', //28/09/2017
                                 BI_Territory__c = 'test' //28/09/2017
                                 );
        insert acc;
        // Insert orders
        NE__Order__c ord1 = new NE__Order__c(NE__AccountId__c = acc.Id);
        NE__Order__c ord2 = new NE__Order__c(NE__AccountId__c = acc.Id);
        NE__Order__c ord3 = new NE__Order__c(NE__AccountId__c = acc.Id);
        //insert ord1;
        //insert ord2;
        //insert ord3;
        
        list<NE__Order__c> ordtoins = new list<NE__Order__c>();
        ordtoins.add(ord1);
        ordtoins.add(ord2);
        ordtoins.add(ord3);    
        insert ordtoins;   
        
        // Insert order items
        NE__OrderItem__c ordIt1 = new NE__OrderItem__c(NE__OrderId__c = ord1.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        NE__OrderItem__c ordIt2 = new NE__OrderItem__c(NE__OrderId__c = ord2.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        NE__OrderItem__c ordIt3 = new NE__OrderItem__c(NE__OrderId__c = ord3.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        //insert ordIt1;
        //insert ordIt2;
        //insert ordIt3;
        
        list<NE__OrderItem__c> oitoins = new list<NE__OrderItem__c>();
        oitoins.add(ordIt1);
        oitoins.add(ordIt2);
        oitoins.add(ordIt3);
        insert oitoins; 
        
        // Type = ChangeOrder, Opportunity created
        list<String> listOrdItemsId1 = new list<String>();
        listOrdItemsId1.add(ordIt1.Id);
        listOrdItemsId1.add(ordIt2.Id);
        
        
        test.startTest();
        String result = ChangeRequestConfItem.changeRequest(listOrdItemsId1,'ChangeOrder');
        //Comented by Micah Burgos because in version 3.7 field NE__OrderId__c of NE__Order__c isn´t updatable. Exception on ChangeRequestConfItem.changeRequest() line 98
        //PickList Deployment to Production [02/06/2015]
        //Opportunity opty = [SELECT Id FROM Opportunity WHERE AccountId =: acc.Id limit 1];
        //NE__Order__c newOrd = [SELECT NE__OptyId__c FROM NE__Order__c WHERE Id =: result];
        //system.assertEquals(opty.Id, newOrd.NE__OptyId__c);
        
    }
    
    static testMethod void myUnitTest2() {
        // Insert account
        Account acc = new Account(Name = 'Test Account',
                                 BI_Segment__c = 'test', //28/09/2017
                                 BI_Subsegment_Regional__c = 'test', //28/09/2017
                                 BI_Territory__c = 'test' //28/09/2017
                                 );
        insert acc;
        // Insert orders
        NE__Order__c ord1 = new NE__Order__c(NE__AccountId__c = acc.Id);
        NE__Order__c ord2 = new NE__Order__c(NE__AccountId__c = acc.Id);
        NE__Order__c ord3 = new NE__Order__c(NE__AccountId__c = acc.Id);
        //insert ord1;
        //insert ord2;
        //insert ord3;
        
        list<NE__Order__c> ordtoins = new list<NE__Order__c>();
        ordtoins.add(ord1);
        ordtoins.add(ord2);
        ordtoins.add(ord3);    
        insert ordtoins;   
        
        test.startTest();   
        // Insert order items
        NE__OrderItem__c ordIt1 = new NE__OrderItem__c(NE__OrderId__c = ord1.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        NE__OrderItem__c ordIt2 = new NE__OrderItem__c(NE__OrderId__c = ord2.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        NE__OrderItem__c ordIt3 = new NE__OrderItem__c(NE__OrderId__c = ord3.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        //insert ordIt1;
        //insert ordIt2;
        //insert ordIt3;
        
        list<NE__OrderItem__c> oitoins = new list<NE__OrderItem__c>();
        oitoins.add(ordIt1);
        oitoins.add(ordIt2);
        oitoins.add(ordIt3);
        insert oitoins; 
        
        // Type = ChangeOrder, Opportunity created
        list<String> listOrdItemsId1 = new list<String>();
        listOrdItemsId1.add(ordIt1.Id);
        listOrdItemsId1.add(ordIt2.Id);
        
        
                    
        // Type = Disconnection, CatalogHeader created and result = order.Id
        list<String> listOrdItemsId2 = new list<String>();
        listOrdItemsId2.add(ordIt3.Id);       
        String result = ChangeRequestConfItem.changeRequest(listOrdItemsId2,'Disconnection');
        //Comented by Micah Burgos because in version 3.7 field NE__OrderId__c of NE__Order__c isn´t updatable. Exception on ChangeRequestConfItem.changeRequest() line 98
        //PickList Deployment to Production [02/06/2015]
        //NE__Order_Header__c oh = [SELECT NE__OrderId__c FROM NE__Order_Header__c WHERE NE__AccountId__c =: acc.Id limit 1];
        //system.assertEquals(oh.NE__OrderId__c, result);     
    }
    
    
    static testMethod void myUnitTest3() {
        // Insert account
        Account acc = new Account(Name = 'Test Account',
                                 BI_Segment__c = 'test', //28/09/2017
                                 BI_Subsegment_Regional__c = 'test', //28/09/2017
                                 BI_Territory__c = 'test' //28/09/2017
                                 );
        insert acc;
        // Insert orders
        NE__Order__c ord1 = new NE__Order__c(NE__AccountId__c = acc.Id);
        NE__Order__c ord2 = new NE__Order__c(NE__AccountId__c = acc.Id);
        NE__Order__c ord3 = new NE__Order__c(NE__AccountId__c = acc.Id);
        //insert ord1;
        //insert ord2;
        //insert ord3;
        
        list<NE__Order__c> ordtoins = new list<NE__Order__c>();
        ordtoins.add(ord1);
        ordtoins.add(ord2);
        ordtoins.add(ord3);    
        insert ordtoins;   
        test.startTest();   
        // Insert order items
        NE__OrderItem__c ordIt1 = new NE__OrderItem__c(NE__OrderId__c = ord1.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        NE__OrderItem__c ordIt2 = new NE__OrderItem__c(NE__OrderId__c = ord2.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        NE__OrderItem__c ordIt3 = new NE__OrderItem__c(NE__OrderId__c = ord3.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        //insert ordIt1;
        //insert ordIt2;
        //insert ordIt3;
        
        list<NE__OrderItem__c> oitoins = new list<NE__OrderItem__c>();
        oitoins.add(ordIt1);
        oitoins.add(ordIt2);
        oitoins.add(ordIt3);
        
        insert oitoins; 
        
        // Type = ChangeOrder, Opportunity created
        list<String> listOrdItemsId1 = new list<String>();
        listOrdItemsId1.add(ordIt1.Id);
        listOrdItemsId1.add(ordIt2.Id);     
        
        // Type = Disconnection, CatalogHeader created and result = order.Id
        list<String> listOrdItemsId2 = new list<String>();
        listOrdItemsId2.add(ordIt3.Id);
        
        // Order item is already in progress        
        String result = ChangeRequestConfItem.changeRequest(listOrdItemsId2,'ChangeOrder');
        //system.assertEquals('Error', result);
        
        
        // Order item is not Active
        NE__OrderItem__c ordIt4 = new NE__OrderItem__c(NE__OrderId__c = ord1.Id, NE__Status__c = 'Pending', NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id);
        insert ordIt4;
        
        list<String> listOrdItemsId3 = new list<String>();
        listOrdItemsId3.add(ordIt4.Id);
        
        result = ChangeRequestConfItem.changeRequest(listOrdItemsId3,'ChangeOrder');
        system.assertEquals('Not active', result);          
    }
    
    static testMethod void myUnitTest4() {
        // Insert account
        Account acc = new Account(Name = 'Test Account',
                                 BI_Segment__c = 'test', //28/09/2017
                                 BI_Subsegment_Regional__c = 'test', //28/09/2017
                                 BI_Territory__c = 'test' //28/09/2017
                                 );
        insert acc;
        // Insert orders
        NE__Order__c ord1 = new NE__Order__c(NE__AccountId__c = acc.Id);
        NE__Order__c ord2 = new NE__Order__c(NE__AccountId__c = acc.Id);
        NE__Order__c ord3 = new NE__Order__c(NE__AccountId__c = acc.Id);
        //insert ord1;
        //insert ord2;
        //insert ord3;
        
        list<NE__Order__c> ordtoins = new list<NE__Order__c>();
        ordtoins.add(ord1);
        ordtoins.add(ord2);
        ordtoins.add(ord3);    
        insert ordtoins;   
        
        // Insert order items
        NE__OrderItem__c ordIt1 = new NE__OrderItem__c(NE__OrderId__c = ord1.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        NE__OrderItem__c ordIt2 = new NE__OrderItem__c(NE__OrderId__c = ord2.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        NE__OrderItem__c ordIt3 = new NE__OrderItem__c(NE__OrderId__c = ord3.Id, NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id, NE__Status__c = 'Active');
        //insert ordIt1;
        //insert ordIt2;
        //insert ordIt3;
        
        list<NE__OrderItem__c> oitoins = new list<NE__OrderItem__c>();
        oitoins.add(ordIt1);
        oitoins.add(ordIt2);
        oitoins.add(ordIt3);
        
        insert oitoins; 
        
        // Type = ChangeOrder, Opportunity created
        list<String> listOrdItemsId1 = new list<String>();
        listOrdItemsId1.add(ordIt1.Id);
        listOrdItemsId1.add(ordIt2.Id);     
        
        // Order item is not Active
        NE__OrderItem__c ordIt4 = new NE__OrderItem__c(NE__OrderId__c = ord1.Id, NE__Status__c = 'Pending', NE__Qty__c = 1, CurrencyIsoCode = 'ARS', NE__Account__c = acc.Id);
        insert ordIt4;
        
        list<String> listOrdItemsId3 = new list<String>();
        listOrdItemsId3.add(ordIt4.Id);
        
        String result = ChangeRequestConfItem.changeRequest(listOrdItemsId3,'ChangeOrder');
        system.assertEquals('Not active', result);          
    }    
    
}