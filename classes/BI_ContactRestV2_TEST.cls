/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Oscar Ena
Company:       Accenture - New Energy Aborda
Description:   TEST Class for BI_ContactRestV2 coverage

History:

<Date>            <Author>          <Description>
02/06/2017        Oscar Ena        Initial version
20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_ContactRestV2_TEST {

	public static final String EMAIL = 'email';
	public static final String PHONE = 'phone';
	public static final String FAX = 'fax';
	public static final String COMMUNICATION_LANGUAGE = 'communicationLanguage';
	public static final String DEPARTMENT = 'department';
		
	@isTest static void test_createContact() {

		Account oAccount = createAccount();
		Contact oContact = createContact(oAccount);

		FS_TGS_RestWrapperFullStack.ContactRequestType wContact = getContactWrapperObject(oContact);

		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();

		//Test with an account that doesn't exist
		restReq.requestURI = '/contactManagement/v1/customers/WRONG_ACCOUNT/contacts/CONT_123456789';
		restReq.requestBody = Blob.valueOf(JSON.serialize(wContact));
		RestContext.request = restReq;

		BI_ContactRestV2.createContact();
		FS_TGS_RestWrapperFullStack.GenericResponse oGenericResponse = (FS_TGS_RestWrapperFullStack.GenericResponse)Json.deserialize(RestContext.response.responseBody.tostring(), FS_TGS_RestWrapperFullStack.GenericResponse.Class);
		System.assertEquals(404, RestContext.response.statusCode);
		System.assertEquals('SVC 1006', oGenericresponse.error);
		System.assertEquals('Resource WRONG_ACCOUNT does not exist', oGenericresponse.message);
		

		//Test with a valid account;
		restReq.requestURI = '/contactManagement/v1/customers/ACC_123456789/contacts/CONT_123456789';
		restReq.requestBody = Blob.valueOf(JSON.serialize(wContact));
		RestContext.request = restReq;

		BI_ContactRestV2.createContact();

		List<Contact> qContact = [SELECT id FROM Contact];

		System.assert(qContact.size()==1);
		
	}
	
	@isTest static void test_updateContact() {

		Account oAccount = createAccount();
		Contact oContact = createContact(oAccount);

		insert oContact;

		oContact.Email = 'testput@test.com';

		FS_TGS_RestWrapperFullStack.ContactRequestType wContact = getContactWrapperObject(oContact);

		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();

		//Test with a contact that doesn't exist
		restReq.requestURI = '/contactManagement/v1/customers/ACC_123456789/contacts/WRONG_CONTACT';
		restReq.requestBody = Blob.valueOf(JSON.serialize(wContact));
		RestContext.request = restReq;

		BI_ContactRestV2.updateContact();
		FS_TGS_RestWrapperFullStack.GenericResponse oGenericResponse = (FS_TGS_RestWrapperFullStack.GenericResponse)Json.deserialize(RestContext.response.responseBody.tostring(), FS_TGS_RestWrapperFullStack.GenericResponse.Class);
		System.assertEquals(404, RestContext.response.statusCode);
		System.assertEquals('SVC 1006', oGenericresponse.error);
		System.assertEquals('Resource WRONG_CONTACT does not exist', oGenericresponse.message);

		//Test with a contact with wrong parent account
		restReq.requestURI = '/contactManagement/v1/customers/WRONG_ACCOUNT/contacts/CONT_123456789';
		restReq.requestBody = Blob.valueOf(JSON.serialize(wContact));
		RestContext.request = restReq;

		BI_ContactRestV2.updateContact();
		oGenericResponse = (FS_TGS_RestWrapperFullStack.GenericResponse)Json.deserialize(RestContext.response.responseBody.tostring(), FS_TGS_RestWrapperFullStack.GenericResponse.Class);
		System.assertEquals('SVC 0001', oGenericresponse.error);
		System.assertEquals('Generic Client Error: Customer With ID WRONG_ACCOUNT Is not the parent of Contact With ID CONT_123456789', oGenericresponse.message);

		//Test with a valid contact and account;
		restReq.requestURI = '/contactManagement/v1/customers/ACC_123456789/contacts/CONT_123456789';
		restReq.requestBody = Blob.valueOf(JSON.serialize(wContact));
		RestContext.request = restReq;

		BI_ContactRestV2.updateContact();

		List<Contact> qContact = [SELECT id, Email FROM Contact];

		System.assertEquals('testput@test.com', qContact.get(0).Email);
	}

	@isTest static void test_exception_createContact(){

		try {

			Account oAccount = createAccount();
			Contact oContact = createContact(oAccount);

			FS_TGS_RestWrapperFullStack.ContactRequestType wContact = getContactWrapperObject(oContact);

			RestRequest restReq = new RestRequest();
			//Force Exception without inicializing response
			//RestContext.response = new RestResponse();
	
			restReq.requestURI = '/contactManagement/v1/customers/ACC_123456789/contacts/CONT_123456789';
			restReq.requestBody = Blob.valueOf(JSON.serialize(wContact));
			RestContext.request = restReq;

			BI_ContactRestV2.createContact();

		} catch (Exception e){

			System.assert(String.isNotBlank(e.getMessage()));

		}


	}

	@isTest static void test_exception_updateContact(){

		try {

			Account oAccount = createAccount();
			Contact oContact = createContact(oAccount);

			FS_TGS_RestWrapperFullStack.ContactRequestType wContact = getContactWrapperObject(oContact);

			RestRequest restReq = new RestRequest();
			//Force Exception without inicializing response
			//RestContext.response = new RestResponse();
	
			restReq.requestURI = '/contactManagement/v1/customers/ACC_123456789/contacts/CONT_123456789';
			restReq.requestBody = Blob.valueOf(JSON.serialize(wContact));
			RestContext.request = restReq;

			BI_ContactRestV2.updateContact();

		} catch (Exception e){

			System.assert(String.isNotBlank(e.getMessage()));

		}


	}


	public static Account createAccount(){

		Account oAccount = new Account();

		oAccount.Name = 'TEST';
		oAccount.BI_Identificador_Externo__c = 'ACC_123456789';
		oAccount.RecordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
            oAccount.BI_Segment__c                         = 'test';
            oAccount.BI_Subsegment_Regional__c             = 'test';
            oAccount.BI_Territory__c                       = 'test';
		insert oAccount;
		return oAccount;
	}

	public static Contact createContact(Account oAccount){

		Contact oContact = new Contact();

		oContact.AccountId = oAccount.id;
		oContact.BirthDate = Date.valueof('2016-02-08');
		oContact.TGS_Language__c = 'ES';
		oContact.Email = 'test@test.com';
		oContact.Phone = '976123456';
		oContact.Fax = '976654321';
		oContact.Department = 'TEST DEPARMENT';
		oContact.FirstName = 'FIRST_NAME';
		oContact.LastName = 'LAST_NAME';
		oContact.Salutation = 'Mr';
		oContact.BI_Genero__c = 'Masculino';
		oContact.Bi_Activo__c = true;
		oContact.FS_TGS_Id_Contacto_FullStack__c = 'CONT_123456789';
	
		return oContact;

	}

	public static FS_TGS_RestWrapperFullStack.ContactRequestType getContactWrapperObject(Contact oContact){

		FS_TGS_RestWrapperFullStack.ContactRequestType wContact = BI_RestHelper.inicializeContactRequestWraperObject(); 

		wContact.personal.birthdate = String.valueof(oContact.BirthDate);
	    wContact.personal.gender = oContact.BI_Genero__c;
	    wContact.name.givenName = oContact.FirstName; 
	    wContact.name.familyName = oContact.LastName;
	    wContact.name.fullName = oContact.Salutation+' '+oContact.Name; 
	    wContact.name.title = oContact.Salutation;
	    wContact.status = oContact.BI_Activo__c ? FS_TGS_RestWrapperFullStack.ContactStatusType.active : FS_TGS_RestWrapperFullStack.ContactStatusType.inactive;

		FS_TGS_RestWrapperFullStack.KeyValueType wKeyValueEmail = BI_RestHelper.getKeyValueWrapperObject(EMAIL, oContact.Email);
		FS_TGS_RestWrapperFullStack.ContactingModeType wContactingModeEmail = BI_RestHelper.getContactingModeWrapperObject(EMAIL, wKeyValueEmail);
		wContact.contactMedium.add(wContactingModeEmail);

		FS_TGS_RestWrapperFullStack.KeyValueType wKeyValuePhone = BI_RestHelper.getKeyValueWrapperObject(PHONE, oContact.Phone);
		FS_TGS_RestWrapperFullStack.ContactingModeType wContactingModePhone = BI_RestHelper.getContactingModeWrapperObject(PHONE, wKeyValuePhone);
		wContact.contactMedium.add(wContactingModePhone);

		FS_TGS_RestWrapperFullStack.KeyValueType wKeyValueFax = BI_RestHelper.getKeyValueWrapperObject(FAX, oContact.Fax);
		FS_TGS_RestWrapperFullStack.ContactingModeType wContactingModeFax = BI_RestHelper.getContactingModeWrapperObject(FAX, wKeyValueFax);
		wContact.contactMedium.add(wContactingModeFax);


		FS_TGS_RestWrapperFullStack.KeyValueType wKeyValueLanguage = BI_RestHelper.getKeyValueWrapperObject(COMMUNICATION_LANGUAGE, oContact.TGS_Language__c);
		wContact.additionalData.add(wKeyValueLanguage);

		FS_TGS_RestWrapperFullStack.KeyValueType wKeyValueDepartment = BI_RestHelper.getKeyValueWrapperObject(DEPARTMENT, oContact.Department);
		wContact.additionalData.add(wKeyValueDepartment);

		return wContact;
	}

	



	
}