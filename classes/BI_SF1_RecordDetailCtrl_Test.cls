@isTest
private class BI_SF1_RecordDetailCtrl_Test {
    @isTest static void getLayoutConfig_test(){
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Argentina'});
        BI_SF1_RecordDetail_Ctrl.getLayoutConfig(lst_acc[0].Id);
        Contact cnt = new Contact(LastName='Prueba',
								AccountId=lst_acc[0].Id,
								Email='prueba@prueba.prueba',
								CurrencyIsoCode='EUR');
		insert cnt;

        Map<String, Object> map_return = BI_SF1_RecordDetail_Ctrl.getLayoutConfig(cnt.Id);
        System.assert(map_return!=null);
    }
}