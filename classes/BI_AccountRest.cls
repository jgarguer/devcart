@RestResource(urlMapping='/customeraccounts/v1/accounts/*')
global class BI_AccountRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Account Rest WebServices.
    
    History:
    
    <Date>            <Author>          	<Description>
    14/08/2014        Pablo Oliva       	Initial version
    10/11/2015	      Fernando Arteaga		BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains basic information stored in the server for a specific account.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.AccountType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          	<Description>
    14/08/2014        Pablo Oliva       	Initial version.
    10/11/2015	      Fernando Arteaga		BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	//global static BI_RestWrapper.AccountType getAccount() {
	global static BI_RestWrapper.AccountInfoType getAccount() {
		
		//BI_RestWrapper.AccountType response;
		BI_RestWrapper.AccountInfoType response;
		
		try{
			/* SERVER ERROR [TEST]: */
            if(BI_TestUtils.throw_exception) throw new BI_Exception('test');
            
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
				
				//ONE ACCOUNT
				response = BI_RestHelper.getAccount(RestContext.request.requestURI.split('/')[4], RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
				
				RestContext.response.statuscode = (response == null)?404:200;//404 NOT_FOUND, 200 OK	
				
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_AccountRest.getAccount', 'BI_EN', exc, 'Web Service');
			
		}
		
		return response;
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Creates a new client in the server receiving the request.
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          	<Description>
    14/08/2014        Pablo Oliva       	Initial version.
    10/11/2015	      Fernando Arteaga		BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPost
	global static void createAccount() {
		
		try{
			
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
				
				RestRequest req = RestContext.request;
		
				Blob blob_json = req.requestBody; 
			
				String string_json = blob_json.toString();
				
				BI_RestWrapper.AccountRequestType accReqType = (BI_RestWrapper.AccountRequestType) JSON.deserialize(string_json, BI_RestWrapper.AccountRequestType.class);
			
				String accountExtId;
				
				//if(accReqType.accountDetails != null && accReqType.accountDetails.additionalData != null){
				if(accReqType.account != null){
					
				//for(BI_RestWrapper.KeyvalueType keyValue:accReqType.accountDetails.additionalData){
				//
				//	if(keyValue.Key == 'accountId'){
				//		accountExtId = keyValue.value;
				//		break;
				//	}
				//
				//}
					
					accountExtId = accReqType.account.accountId;
					
					if(accountExtId == null){
						
						RestContext.response.statuscode = 400;//BAD_REQUEST
						RestContext.response.headers.put('errorMessage', Label.BI_MissingRequiredFields+' accountId');
						
					}else{
			
						Account acc = new Account();
				
						//if(accReqType.accountDetails != null){
							
						acc.BI_Id_del_cliente__c = accountExtId;
						if(RestContext.request.headers.get('systemName') != null)
							acc.BI_Sistema_legado__c = RestContext.request.headers.get('systemName');
							
						//List<Region__c> lst_region = [select Id from Region__c where BI_Codigo_ISO__c = :RestContext.request.headers.get('countryISO') limit 1];
						BI_Code_ISO__c biCodeIso = [select BI_Country_Currency_ISO_Code__c, BI_Country_ISO_Code__c, Name from BI_Code_ISO__c where BI_Country_ISO_Code__c =: RestContext.request.headers.get('countryISO') ];	
						String region = biCodeIso.Name;
						
						if(String.isNotEmpty(region))
							acc.BI_Country__c = region;
						else{
							RestContext.response.statuscode = 400;//BAD_REQUEST
							RestContext.response.headers.put('errorMessage', Label.BI_CountryNotValid);
						}
						
						//acc = BI_RestHelper.generateAccount(acc, accReqType.accountDetails);
						acc = BI_RestHelper.generateAccount(acc, accReqType.account);
				
						//}
				
						insert acc;
				
						RestContext.response.statuscode = 201;//CREATED
						RestContext.response.headers.put('Location','https://'+System.URL.getSalesforceBaseURL().getHost()+'/services/apexrest/customeraccounts/v1/accounts/'+acc.BI_Id_del_cliente__c);
						
					}
					
				}else{
					
					RestContext.response.statuscode = 400;//BAD_REQUEST
					RestContext.response.headers.put('errorMessage', Label.BI_MissingRequiredFields+' accountId');
					
				}
				
			}
			
			
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_AccountRest.createAccount', 'BI_EN', exc, 'Web Service');
			
		}
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Modifies an existing client in the server receiving the request.
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          	<Description>
    14/08/2014        Pablo Oliva       	Initial version.
    10/11/2015	      Fernando Arteaga		BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPut
	global static void updateAccount() {
		
		try{
				
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
				
				String idAcc = RestContext.request.requestURI.split('/')[4] + ' '+RestContext.request.headers.get('countryISO');
				if(RestContext.request.headers.get('systemName') != null)
					 idAcc += ' '+RestContext.request.headers.get('systemName');
					 
				List<Account> lst_acc = [select Id from Account where BI_Validador_Id_de_legado__c = :idAcc limit 1];
		
				if(!lst_acc.isEmpty()){
					
					RestRequest req = RestContext.request;
			
					Blob blob_json = req.requestBody; 
				
					String string_json = blob_json.toString(); 
					
					BI_RestWrapper.AccountRequestType accReqType = (BI_RestWrapper.AccountRequestType) JSON.deserialize(string_json, BI_RestWrapper.AccountRequestType.class);
				
					Account acc = lst_acc[0];
					
					//if(accReqType.accountDetails != null)
						//acc = BI_RestHelper.generateAccount(acc, accReqType.accountDetails);
					if(accReqType.account != null)
						acc = BI_RestHelper.generateAccount(acc, accReqType.account);
					
					update acc;
					
					RestContext.response.statuscode = 200;//OK
					
					
				}else{
					
					RestContext.response.statuscode = 404;//NOT_FOUND
					
				}		
				
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_AccountRest.updateAccount', 'BI_EN', exc, 'Web Service');
			
		}
		
	}

}