public class BI_COL_SucursalesValidacion_cls {
	
	public List<BI_Sede__c> lstSucursal{get;set;}
	public boolean isDelete;
	public boolean validacionOK=true;
	public String idsSucursalesError='';
	public String idsSucursalesOk='';
	
	public BI_COL_SucursalesValidacion_cls(boolean accion){
		this.isDelete=accion;
	}
	
	
	//método encargado de iniciar valores de los campos estado callejero y sucursal uso 
	public void modificaEstadoCallejero(List<BI_Sede__c> lstNew, List<BI_Sede__c> lstOld)
	{
		System.debug('\n\n Ingresa a modificaEstadoCallejero tamaño lista lstNew='+lstNew.size()+'  lstNew[i].Estado_callejero__c'+lstNew[0].BI_COL_Estado_callejero__c+'\n\n');
		for(Integer i=0;i<lstNew.size();i++)
		{
			//lstNew[i].BI_COL_Sucursal_en_uso__c='Libre';
			System.debug('\n\n lstNew[i].Direcci_n_Sucursal__c= '+lstNew[i].BI_Direccion__c+' lstOld[i].Direcci_n_Sucursal__c='+lstOld[i].BI_Direccion__c+'  lstOld[i].Estado_callejero__c= '+lstOld[i].BI_COL_Estado_callejero__c+'\n\n');
			if(lstOld[i].BI_COL_Estado_callejero__c!='Pendiente con opción' && (lstNew[i].BI_Direccion__c!=lstOld[i].BI_Direccion__c) || (lstNew[i].BI_COL_Ciudad_Departamento__c!=lstOld[i].BI_COL_Ciudad_Departamento__c))
			{
				System.debug('\n\n Ingresa al IF de los estados Linea 31 \n\n');
				lstNew[i].BI_COL_Estado_callejero__c='Pendiente';
				//lstNew[i].BI_COL_Sucursal_en_uso__c='Pendiente';
			}
		}
		System.debug('\n\n A LA SALIDA ====>>> lstNew[i].Estado_callejero__c'+lstNew[0].BI_COL_Estado_callejero__c+'\n\n');
	}
	
	// Método encargado de hacer la validación de registros asociados a las sucursales afectadas 
	
	public void validaClientesSucursal()
	{
		set<Id> lstIdSucursal=new set<Id>();
		set <Id> setIdError=new set <Id>();
		map<Id,BI_Sede__c> mapaSucursales=new map<Id,BI_Sede__c>();
		validacionOK=true;
		
		if(lstSucursal!=null)
		{
			// Se crea un listado de los Id de las sucursales afectadas se tienen en cuenta solo las que estan en estado de pendiente o las que se van a eliminar
			for(BI_Sede__c sucursal:lstSucursal)
			{
				system.debug('-----------Sucursal_en_uso__c----->'+sucursal.BI_COL_Sucursal_en_uso__c);
				if(sucursal.id!=null && (sucursal.BI_COL_Sucursal_en_uso__c=='Pendiente' || isDelete))
				{
					lstIdSucursal.add(sucursal.id);
					mapaSucursales.put(sucursal.id,sucursal);
				}
			}	
		
			if(lstIdSucursal.size()>0)
			{
				system.debug('-----------Id sucursales----->'+lstIdSucursal);
				// Validación sobre el objeto Modificación de Servicio en los 3 campos lupa hacia el objeto Sucursal
				idsSucursalesError+='Modificaciones de Servicio afectadas:\n';
				List <BI_COL_Modificacion_de_Servicio__c> lstMS=[select BI_COL_Sucursal_de_Facturacion__r.BI_Sede__c,  BI_COL_Sede_Destino__r.BI_Sede__c, BI_COL_Sucursal_Origen__r.BI_Sede__c from BI_COL_Modificacion_de_Servicio__c where BI_COL_Sucursal_de_Facturacion__r.BI_Sede__c IN: lstIdSucursal or BI_COL_Sede_Destino__r.BI_Sede__c IN: lstIdSucursal or BI_COL_Sucursal_Origen__r.BI_Sede__c IN: lstIdSucursal limit 1000];
				for(BI_COL_Modificacion_de_Servicio__c sucursal: lstMS)
				{
					
					if(sucursal.BI_COL_Sucursal_de_Facturacion__c!=null && !setIdError.contains(sucursal.BI_COL_Sucursal_de_Facturacion__c) && lstIdSucursal.contains(sucursal.BI_COL_Sucursal_de_Facturacion__c) )
					{
						setIdError.add(sucursal.BI_COL_Sucursal_de_Facturacion__c);
					}
					if(sucursal.BI_COL_Sede_Destino__c!=null && !setIdError.contains(sucursal.BI_COL_Sede_Destino__c) && lstIdSucursal.contains(sucursal.BI_COL_Sede_Destino__c))
					{
						setIdError.add(sucursal.BI_COL_Sede_Destino__c);
					}
					if(sucursal.BI_COL_Sucursal_Origen__c!=null && !setIdError.contains(sucursal.BI_COL_Sucursal_Origen__c) && lstIdSucursal.contains(sucursal.BI_COL_Sucursal_Origen__c))
					{
						setIdError.add(sucursal.BI_COL_Sucursal_Origen__c);
					}
					system.debug('\n\n----encuentra MS asociadas---->');
				}
				
				idsSucursalesError+='\nDS Reestructurada afectadas:\n';
				// Validación sobre el objeto DS Restructurada en los 2 campos lupa hacia el objeto Sucursal 
				List<BI_COL_Descripcion_de_servicio__c> lstDsr=[select BI_COL_Sede_Destino__r.BI_Sede__c, BI_COL_Sede_Origen__r.BI_Sede__c from BI_COL_Descripcion_de_servicio__c where BI_COL_Sede_Destino__r.BI_Sede__c IN: lstIdSucursal or BI_COL_Sede_Origen__r.BI_Sede__c IN: lstIdSucursal limit 1000];
				for(BI_COL_Descripcion_de_servicio__c dsr: lstDsr)
				{
					
					if(dsr.BI_COL_Sede_Destino__r.BI_Sede__c!=null && !setIdError.contains(dsr.BI_COL_Sede_Destino__r.BI_Sede__c) && lstIdSucursal.contains(dsr.BI_COL_Sede_Destino__r.BI_Sede__c))						
						setIdError.add(dsr.BI_COL_Sede_Destino__r.BI_Sede__c);
					
					if(dsr.BI_COL_Sede_Origen__r.BI_Sede__c!=null && !setIdError.contains(dsr.BI_COL_Sede_Origen__r.BI_Sede__c) && lstIdSucursal.contains(dsr.BI_COL_Sede_Origen__r.BI_Sede__c))
						setIdError.add(dsr.BI_COL_Sede_Origen__r.BI_Sede__c);
				}
				 System.debug('\n\n setIdError=====>>>>'+setIdError);
				if(setIdError.size()>0)
					validacionOk=false;
				BI_Sede__c suc=null;
				String sucErrorMsm='';
				
				// agregando el mensaje de error a las sucursales que fueron encontradas con registros dependientes 
				for(Id isSucursal:setIdError){					
					suc=mapaSucursales.get(isSucursal);
					system.debug('Encuentra sucursal con error--- se asigna mensaje-->'+suc);					
					system.debug('Encuentra sucursal con error---->'+Label.BI_COL_ErrorModificacionSucursal);
					if(!isDelete){
						sucErrorMsm+='<br/>'+suc.Name;
						suc.BI_COL_Sucursal_en_uso__c='En uso';
					}
					System.debug('\n\n BI_COL_Sucursal_en_uso__c=====>>>'+suc.BI_COL_Sucursal_en_uso__c);
					if(!test.isRunningTest())
					{
						system.debug('Test dentro --->'+suc);
						system.debug('Test dentro con error---->'+Label.BI_COL_ErrorModificacionSucursal);
					//	if(suc.Estado_callejero__c!='Pendiente')
							suc.addError(Label.BI_COL_ErrorModificacionSucursal);
					}	
													
				}
				
				// preparando mensaje de envio para notificar el cambio masivo de las sucursales afectadas 
				if(lstSucursal.size()>1){
					String strMensaje='<br/>Buen día: <br/><br/> El Siguiente es el estado de actualización de los registros de sucursales que fueron modificados en su dirección o ciudad:<br/><br/> ';
					string strAsunto='Sucursales Modificadas';
					User us=[select Id, Email,Name from User where Id=: Userinfo.getUserId() limit 1];
					String[] strMailPARA = new String[]{us.Email}; 
					List<BI_Sede__c> lstSucOk=new List<BI_Sede__c>();
					for(BI_Sede__c sucu:mapasucursales.values()){
						if((sucu.BI_COL_Sucursal_en_uso__c == null || !sucu.BI_COL_Sucursal_en_uso__c.equals('En uso' )) && !sucu.Estado__c.equals('Pendiente')) {
							lstSucOk.add(sucu);
							strMensaje+=sucu.Name+' (Modificado Satisfactoriamente)<br/>'; 
						}
					}
					system.debug('-----msmErrores sucursal-->'+sucErrorMsm);
					if(sucErrorMsm.length()>0)
						strMensaje+='<br/>Las siguientes sucursales no pudieron ser actualizadas porque tienen solicitudes en Vuelo:<br/>'+sucErrorMsm;
					strMensaje+='<br/><br/>La modificación ha sido realizada por el usuario: '+us.Name;
					//if(lstSucOk.size()>0 || sucErrorMsm.length()>0)
					//	enviarCorreo(strMensaje, strAsunto, strMailPARA, lstSucOk);
				}
				
			}
		}
	}
	/*
	public void enviarCorreo(String strMensaje,String strAsunto, String[] strMailPARA, List<BI_Sede__c> lstSucOk){
		String strSenderName='';
		Log_Transaccion__c log=null;
		EmailUtil_cls emailUtil= new EmailUtil_cls(strMailPARA);
		emailUtil.htmlBody(strMensaje)
				.senderDisplayName(strSenderName)
				.subject(strAsunto)
				.useSignature(false);		
		
		if(!emailUtil.sendEmail()) 
		{
			System.debug('\n\n Envio Fallido '+strMailPARA+'\n\n');
			List<Log_Transaccion__c> lstLogTran=new List<Log_Transaccion__c>();
			for(BI_Sede__c suc: lstSucOk){
				log=new Log_Transaccion__c(Name='Envio de Correo',Respuesta__c='No pudo ser enviado el correo con la información del cambio de sucursal',Sucursal__c=suc.id);
				lstLogTran.add(log);
			}
			insert lstLogTran;
		}else{
			System.debug('\n\n Envio Correcto '+strMailPARA+'\n\n');
		}
	}
	
	private static testMethod void test1(){
		Account cliente = new Account(Name ='NEXTANT SUCURSAL COLOMBIA',
									Tipo_Id__c= 'NIT',
									Identificaci_n__c = '000000001-1',
									Tipo__c = 'Activo',
									Aplica_LD__c = true,
									Cartera__c = 'OK',
									Naturaleza__c = 'JURIDICA',
									Marcaci_n_del_cliente__c = '3',
									Segmento_Telef_nica__c = 'EMPRESAS',
									Sector_Telef_nica__c = 'CORPORATIVO',
									Subsegmento_Telef_nica__c = 'FINANCIERO',
									Subsector_Telef_nica__c = 'BANCA',
									Actividad_Telef_nica__c = 'BANCA COMERCIAL PRIVADA',
									Description = 'Nextant',
									Gerencia_Comercial__c = 'JOSE LUIS RODRIGUEZ GARCIA',
									Mercado_Objetivo__c = true,
									Jefatura_Comercial__c = 'FEDERICO PELAEZ',
									Pais__c = 'Colombia',
									Phone = '13450280');
									
		insert cliente;
		
		Ciudad__c ciudad = new Ciudad__c(Name= 'BOGOTA DC / CUNDINAMARCA',
								Codigo_DANE__c = '11001000',
								Regional__c = 'Centro Sur');
								
		insert ciudad;
		
		BI_Sede__c sucursal = new BI_Sede__c(Cliente__r = cliente,
										Cliente__c = cliente.Id,
										Name = 'Sucursal Prueba',
										Tipo_Sucursal__c = 'PRINCIPAL',
										BI_COL_Estado_callejero__c='Validado por callejero',
										BI_Direccion__c='calle 142 # 22-09',
										Ciudad__c = ciudad.Id);
	 	insert sucursal;
	 	BI_Sede__c sucursal2 = new BI_Sede__c(Cliente__r = cliente,
										Cliente__c = cliente.Id,
										Name = 'Sucursal Prueba',
										Tipo_Sucursal__c = 'PRINCIPAL',
										BI_COL_Estado_callejero__c='Validado por callejero',
										BI_DireccionNormalizada__c='calle 140 # 22-09',
										Ciudad__c = ciudad.Id);
	 	insert sucursal2;
	 	
	 	Opportunity oppty = new Opportunity(
											Name = 'Oportunidad prueba ',
											AccountId = cliente.id,
											Prioridad__c = '3',
											CloseDate = System.today().addDays(7),
											StageName = 'F5 - En comentarios o En Estructuración');
		insert oppty;
		
		Anchos_de_Banda__c anchoBanda = new Anchos_de_Banda__c(Name = 'DS1',
														Ancho_de_Banda__c = 'DS1');
		insert anchoBanda;
		
		Producto_Telefonica__c producto = new Producto_Telefonica__c(
												Tipo_registro_DS__c = 'DS-Conectividad',
												Segmento__c = 'EMPRESAS',
												Producto__c = 'MPLS',
												Linea__c = 'BANDA ANCHA',
												Familia__c = 'Datos',
												Descripcion_Referencia__c = 'VPN IP MPLS DATOS',
												Cod_Desc_Referencia__c = 'EQCENO79',
												Anchos_de_banda__c = anchoBanda.Id,
												Activo__c = true);
												
		producto.Acceso_internet__c='No';     
	    producto.Activo__c=true;     
	    producto.Administracion__c='No';    
	    producto.Agrupacion__c='Agrupacion__c';    
	    producto.Aplicacion_homologada_sobre_Red_celular__c='Si';     
	    producto.AutoAtendant__c=true;  
	    producto.Backup__c='Si';    
	    producto.Banda_espectral__c='C';     
	    producto.Cantidad_servicios__c=0;    
	    producto.Producto__c = 'INTERNET'; 
	    producto.Descripcion_Referencia__c = 'INTERNET SEGURO';
	    producto.Acceso_internet__c = 'Si';
	    producto.Segmento__c = 'EMPRESAS';
	    producto.Tipo_registro_DS__c = 'DS-Internet Dedicado';
	    producto.Cod_Desc_Referencia__c = 'INT12345';
	    producto.Linea__c = 'DATOS E INTERNET';
	    producto.SubFamilia__c = 'ACCESO A INTERNET';
	    producto.Familia__c = 'Internet';
	    producto.Tipo_anexo__c = 'Anexo de TV';	
												
		insert producto;
	 	
	 	BI_COL_Descripcion_de_servicio__c ds = new BI_COL_Descripcion_de_servicio__c(	Oportunidad__c = oppty.id,
											Producto_Telefonica__c = producto.id,
											Sucursal_Origen__c = sucursal2.id);
		insert ds;
											
		BI_COL_Modificacion_de_Servicio__c ms = [select Id,Observaciones__c,Estado__c from BI_COL_Modificacion_de_Servicio__c where   Descripcion_Servicio_R__c=:ds.Id limit 1];
	 	ms.Sucursal_Origen__c=sucursal.Id;
	 	ms.BI_COL_Sede_Destino__c=sucursal2.Id;
	 	ms.Sucursal_facturacion__c=sucursal.Id;
	 	update ms;
	 	
	 	List<BI_Sede__c> lstSuc=new List<BI_Sede__c>();
	 	sucursal.BI_DireccionNormalizada__c='calle 142 # 22-02';
	 	lstSuc.add(sucursal);
	 	sucursal2.BI_DireccionNormalizada__c='calle 147# 22-02';
	 	lstSuc.add(sucursal2);
	 	
	 	update lstSuc;
	 	delete sucursal;
	 	
	}	*/

}