global class BI_GenerateCaseCSV_Batch implements Database.Batchable<sObject> {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Micah Burgos
	    Company:       Aborda
	    Description:   Batch class that generate a csv doc with the cases of the 'Recarterización'
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/02/2016                      Micah Burgos             	Initial version
	    25/02/2016						Guillermo Muñoz				End of the develop
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	String query;
	Set<Id> set_all_acc;

	final String DOCUMENT_NAME;
	final Id DOC_ID;
	final Id DOC2_ID;
	final String RECARTERIZACION_ID;
	//final Id FOLDER_ID;
	
	global BI_GenerateCaseCSV_Batch(Id rec_param) {

		RECARTERIZACION_ID = rec_param;

		//SEARCH FOLDER
		//FOLDER_ID = [Select Id from Folder where DeveloperName = 'BI_Ficheros_CSV_de_Recarterizaciones'].Id;

		////GENERATE DOCUMENT
		//Document doc = new Document ();
		//doc.Name = 'Recarterización casos ['+String.valueOf(System.now())+'].csv'; 
		//DOCUMENT_NAME = doc.Name;
		//doc.Description  = 'Apex Script';
		//doc.FolderId     = FOLDER_ID; 

		Attachment doc = new Attachment();
		doc.Name = 'Recarterización casos ['+String.valueOf(System.now())+'].csv';
		doc.ParentId = rec_param; 


		//Header CSV
		String csvDocHeader = 'CaseId,OldOwner,NewOwner,AccountId,RecarterizacionId,IsProcessed'+'\n';
		doc.Body  = Blob.valueOf(csvDocHeader);  
		insert doc;

		Attachment doc2 = new Attachment();
		doc2.Name = 'Borrar equipo de casos ['+String.valueOf(System.now())+'].csv';
		doc2.ParentId = rec_param; 


		//Header CSV
		String csvDoc2Header = 'Id,MemberId,ParentId,TeamRoleId,TeamTemplateMemberId,Propietario,RecarterizacionId'+'\n';
		doc2.Body  = Blob.valueOf(csvDoc2Header);  
		insert doc2;

		DOC_ID = doc.Id;
		DOC2_ID = doc2.Id;


		//SEARCH ACCOUNTS FROM BI_Linea_de_Recarterizacion__c
		List<BI_Linea_de_Recarterizacion__c> lst_rec =  [SELECT Id,BI_AccountId__c, BI_Recarterizacion__c, BI_Cases_RecordTypes__c, BI_Casos_abiertos__c, BI_Casos_cerrados__c, BI_Open_Opp__c FROM BI_Linea_de_Recarterizacion__c WHERE BI_Recarterizacion__c = :rec_param];


		set_all_acc = new Set<Id>();
		
		Set<Id> set_ParentsID = new Set<Id>();

		//GENERIC FILTERS FOR ALL OPP 
		Boolean onlyOpenCases = true;
		Boolean onlyClosedCases = true;

		for(BI_Linea_de_Recarterizacion__c rec :lst_rec){
			set_all_acc.add(rec.BI_AccountId__c);

			if(rec.BI_Casos_abiertos__c){
				onlyClosedCases = false;
			}

			if(rec.BI_Casos_cerrados__c){
				onlyOpenCases = false;
			}
		}

		query = 'SELECT ID, OwnerId, AccountId, RecordType.DeveloperName, IsClosed, (SELECT Id, MemberId, ParentId, TeamRoleId, TeamTemplateMemberId FROM TeamMembers) FROM Case WHERE AccountId IN :set_all_acc ';

		if(onlyOpenCases){
			query += ' AND IsClosed = false';
		}
		if(onlyClosedCases){
			query += ' AND IsClosed = true';
		}

		query += ' order by AccountId';

		System.debug('********QUERY: ' + query);
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		List<Attachment> lst_doc = [SELECT ID, Body FROM Attachment WHERE Id = :DOC_ID OR Id =: DOC2_ID ORDER BY CreatedDate DESC];

   		System.debug('******** DOC ID: ' + lst_doc[0].Id);

   		String csvDoc_2 = lst_doc[0].Body.toString();
   		String csvDoc2_2 = lst_doc[1].Body.toString();

   		List<Case> lst_cases = scope;	
		Set<Id> set_accID = new Set<Id>();
		for(Case cas :lst_cases){
			set_accID.add(cas.AccountId);
		}

		//Crear mapa de recarterizaciones 
		List<BI_Linea_de_Recarterizacion__c> lst_rec2 = [SELECT Id, BI_AccountId__c,BI_Borrar_Equipo_de_Cuenta__c,BI_Cases_RecordTypes__c,BI_NewOwner__c,BI_OldOwner__c,BI_Status__c,BI_Num_OppProcessed__c,BI_Borrar_Equipo_de_Casos__c,BI_Casos_abiertos__c,BI_Casos_cerrados__c
   													FROM BI_Linea_de_Recarterizacion__c WHERE BI_AccountId__c IN :set_accID AND BI_Recarterizacion__c = :RECARTERIZACION_ID];
   		Map<String, BI_Linea_de_Recarterizacion__c> map_rec = new Map<String, BI_Linea_de_Recarterizacion__c>();

   		for(BI_Linea_de_Recarterizacion__c rec2 : lst_rec2){
			map_rec.put(rec2.BI_AccountId__c, rec2);
   		}

   		for(Case cas :lst_cases){
   			System.debug('+++++++++++++++++   --->' + map_rec.get(cas.AccountId).BI_Cases_RecordTypes__c);
   			if(map_rec.get(cas.AccountId).BI_Cases_RecordTypes__c != null){
	   			if(
	   				(map_rec.get(cas.AccountId).BI_Casos_abiertos__c && map_rec.get(cas.AccountId).BI_Cases_RecordTypes__c.contains(cas.RecordType.DeveloperName) && !cas.IsClosed )||

					(map_rec.get(cas.AccountId).BI_Casos_cerrados__c && map_rec.get(cas.AccountId).BI_Cases_RecordTypes__c.contains(cas.RecordType.DeveloperName) && cas.IsClosed )
				){
					csvDoc_2 += cas.Id+','+cas.OwnerId+','+map_rec.get(cas.AccountId).BI_NewOwner__c+','+cas.AccountId+','+map_rec.get(cas.AccountId).Id+',true\n';
					map_rec.get(cas.AccountId).BI_Processed_cas__c = true;
					map_rec.get(cas.AccountId).BI_Num_casProcessed__c = (map_rec.get(cas.AccountId).BI_Num_casProcessed__c == null)?1: map_rec.get(cas.AccountId).BI_Num_casProcessed__c +1;
					//Id,MemberId,ParentId,TeamRoleId,TeamTemplateMemberId,Propietario,RecarterizacionId
					if(!cas.TeamMembers.isEmpty() && map_rec.get(cas.AccountId).BI_Borrar_Equipo_de_Casos__c){
						for(CaseTeamMember ctm : cas.TeamMembers){

							if(ctm.MemberId == cas.OwnerId){
								csvDoc2_2 += ctm.Id + ',' + ctm.MemberId + ',' + ctm.ParentId + ',' + ctm.TeamRoleId + ',' + ctm.TeamTemplateMemberId + ',true,' + RECARTERIZACION_ID + '\n';
							}
							else{
								csvDoc2_2 += ctm.Id + ',' + ctm.MemberId + ',' + ctm.ParentId + ',' + ctm.TeamRoleId + ',' + ctm.TeamTemplateMemberId + ',,' + RECARTERIZACION_ID + '\n';
							}
						}
					}
				}
   			}else if(
   				(map_rec.get(cas.AccountId).BI_Casos_abiertos__c && !cas.IsClosed )||

				(map_rec.get(cas.AccountId).BI_Casos_cerrados__c && cas.IsClosed )
			){
				csvDoc_2 += cas.Id+','+cas.OwnerId+','+map_rec.get(cas.AccountId).BI_NewOwner__c+','+cas.AccountId+','+map_rec.get(cas.AccountId).Id+',true\n';
				map_rec.get(cas.AccountId).BI_Processed_cas__c = true;
				map_rec.get(cas.AccountId).BI_Num_casProcessed__c = (map_rec.get(cas.AccountId).BI_Num_casProcessed__c == null)?1: map_rec.get(cas.AccountId).BI_Num_casProcessed__c +1;
				//Id,MemberId,ParentId,TeamRoleId,TeamTemplateMemberId,Propietario,RecarterizacionId
				if(!cas.TeamMembers.isEmpty() && map_rec.get(cas.AccountId).BI_Borrar_Equipo_de_Casos__c){
					for(CaseTeamMember ctm : cas.TeamMembers){

						if(ctm.MemberId == cas.OwnerId){
								csvDoc2_2 += ctm.Id + ',' + ctm.MemberId + ',' + ctm.ParentId + ',' + ctm.TeamRoleId + ',' + ctm.TeamTemplateMemberId + ',true,' + RECARTERIZACION_ID + '\n';
						}
						else{
							csvDoc2_2 += ctm.Id + ',' + ctm.MemberId + ',' + ctm.ParentId + ',' + ctm.TeamRoleId + ',' + ctm.TeamTemplateMemberId + ',,' + RECARTERIZACION_ID + '\n';
						}
					}
				}
			}
		}			
		
		lst_doc[0].Body = Blob.valueOf(csvDoc_2); 
		lst_doc[1].Body = Blob.valueOf(csvDoc2_2);

		update lst_doc;

		update map_rec.values();

	
	}
	
	global void finish(Database.BatchableContext BC) {
		//List<> lst_rec_3 = [SELECT  WHERE ID = :RECARTERIZACION_ID];

		BI_Recarterizacion__c recarterizacion = new BI_Recarterizacion__c(
			Id = RECARTERIZACION_ID,
			BI_End_batch_case__c = true
		);

		update recarterizacion;


	}
	
}