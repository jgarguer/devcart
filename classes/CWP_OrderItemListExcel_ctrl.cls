/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for visualforce pages in charge of exporting results table as excel or pdf
    Test Class:    CWP_OrderITemListExcel_ctrl_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    23/02/2017              Everis                    Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class CWP_OrderItemListExcel_ctrl {
    public List<CWP_Installed_Services_Controller.tableRecord> listToShow{get; set;}
    
    public String serviceUnit{get;set;} 
     
    public String service1{get;set;}
    public String service2{get;set;}
    public String service3{get;set;}
    
    public Boolean showProdCategor {get;Set;}
    public Map<String, String> appliedFilters {get;set;}
    public Map<String, String> mapLabel {get;set;}
    
    
    public list<string> filterByAttribute{get; set;}
    
    public List<String> filterByFieldOrderedList{get;set;}
    
    public String orientation = System.currentPagereference().getParameters().get('orientationValue');
    public String headerPhoto{get;set;}
    /*
    Developer Everis
    Function: constructor
    */
    public CWP_OrderItemListExcel_ctrl(){
        
        //INI HEADER
        List<Document> lstDocument = [Select Id,Name,LastModifiedById from Document where Name = 'CWP_TelfonicaLogo' limit 1];
        string strOrgId = UserInfo.getOrganizationId();
        //string orgInst = URL.getSalesforceBaseUrl().getHost();
        string orgInst = URL.getCurrentRequestUrl().toExternalForm();
        orgInst = orgInst.substring(0, orgInst.indexOf('.')) + '.content.force.com';
        if(!lstDocument.isEmpty()){
        headerPhoto= URL.getSalesforceBaseUrl().getProtocol() + '://c.' + orgInst + '/servlet/servlet.ImageServer?id=' + lstDocument[0].Id + '&oid=' + strOrgId;
        headerPhoto= '/servlet/servlet.ImageServer?id=' + lstDocument[0].Id + '&oid=' + strOrgId;
        system.debug('url header: '+headerPhoto);
            }
        //FIN HEADER
        
        
        list<CWP_Installed_Services_Controller.tableRecord> theList = new list<CWP_Installed_Services_Controller.tableRecord>();
        theList = (list<CWP_Installed_Services_Controller.tableRecord>)Cache.Session.get('local.CWPexcelExport.cachedListaResultados');
        filterByAttribute=(list<String>)Cache.Session.get('local.CWPexcelExport.cachedfilterByAttribute');
        filterByFieldOrderedList=(list<String>)Cache.Session.get('local.CWPexcelExport.cachedfilterByFieldOrderedList');
        
        
        system.debug('traza recuperada por cache    ' + theList);
        listToShow = new list<CWP_Installed_Services_Controller.tableRecord>();
        listToShow.addAll(theList);
        System.debug('@ '+listToShow);
        
        // Elementos provenientes de My Service
        String aux=listToShow[0].identifier;
        list<NE__OrderItem__c> list1 = [SELECT Id,NE__CatalogItem__r.NE__ProductId__r.Name,NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name, NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name FROM NE__OrderItem__c WHERE Id =: aux];
        
        if(!list1.isEmpty()){
            showProdCategor = true;
            serviceUnit=list1[0].NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name;
        
            service1=list1[0].NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name ;
            service2=list1[0].NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name;
            service3=list1[0].NE__CatalogItem__r.NE__ProductId__r.Name;
        }else{
            showProdCategor = false;
            //Elementos provenientes de Service Tracking
            // El nombre del archivo indicará si son tickets u orders y la fecha de descarga
            serviceUnit = 'Service Tracking_';
            if(theList.get(0).technicalBehaviour.equalsIgnoreCase('Orders')){
                service3 = 'Orders';
            }else{
                service3 = 'Tickets';
            }
            service3 += '_' + String.valueOf(Date.Today());
            
            // Filtros aplicados
            mapLabel = getMapLabel();
            appliedFilters = (Map<String, String>)Cache.Session.get('local.CWPexcelExport.cachedFilterApplied');
            system.debug('appliedFilters: ' + appliedFilters);
        }
        
        
            
       
        
       /* list<CWP_Installed_Services_Controller.tableRecord> theList = new list<CWP_Installed_Services_Controller.tableRecord>();

       
        list<NE__OrderItem__c> list1 = [SELECT Id, NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name FROM NE__OrderItem__c WHERE Id =: listToShow[0].identifier];
        */
    }
    
    /*
    Developer Everis
    Function: method used to get the orientation of the page desired for the pdf
    */
    public String getOrientation(){
        system.debug('la orientación seleccionada es: ' + orientation);
        return orientation;
    }
    
    /*
    Developer Everis
    Function: method used to build a map with the labels of the header of the table
    */
    public Map<String, String> getMapLabel(){
        Map<String, String> mapToRet = new Map<String, String>();
        mapToRet.put('city', Label.TGS_CWP_CITY);
        mapToRet.put('country', Label.TGS_CWP_COUNT);
        mapToRet.put('keyValue', Label.CWP_KeyValue);
        mapToRet.put('lvl1', Label.TGS_CWP_T1);
        mapToRet.put('lvl2', Label.TGS_CWP_T2);
        mapToRet.put('lvl3', Label.TGS_CWP_T3);
        mapToRet.put('status', Label.TGS_CWP_STAT);
        mapToRet.put('type', Label.CWP_Type);
        return mapToRet;
    }
  
    
   
}