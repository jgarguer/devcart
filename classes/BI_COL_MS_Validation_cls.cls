/****************************************************************************************************
	Información general
	-------------------
	author: Javier Tibamoza Cubillos
	company: Avanxo Colombia
	Project: Implementación Salesforce
	Customer: TELEFONICA
	Description: 
	
	Information about changes (versions)
	-------------------------------------
	Number    Dates           Author                       Description
	------    --------        --------------------------   -----------
	1.0       15-Abr-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
public class BI_COL_MS_Validation_cls
{
	public static Map<ID,BI_COL_Descripcion_de_servicio__c> mapidXnuevaDS;
	public static Map<id,String> mapidXnueva;
	public static Map<id,String> mapidDR;
	public static BI_COL_Modificacion_de_Servicio__c msEstA = new BI_COL_Modificacion_de_Servicio__c();
	public static Integer tamLista;
	/***
	* @Author: 
	* @Company: 
	* @Description: 
	* @History: 
	* Date		|	Author	|	Description
	* 
	***/
	public static void validation( List<BI_COL_Modificacion_de_Servicio__c> lstModificacion, Boolean isInsert )
	{
		List<ID> lstIdDS = new List<ID>();
		Map<String,String> msActaulizar = new Map<String,String>(); 
		List<Id> lstIdMS = new List<ID>();
		
		for( BI_COL_Modificacion_de_Servicio__c MStemp : lstModificacion )
		{
			lstIdDS.add( MStemp.BI_COL_Codigo_unico_servicio__c );
			lstIdMS.add( MStemp.id );
		}
		//JATC Descripcion_Referencia__c PILAS averiguar cual es el campo en BI_EN -- BI_COL_Producto__r.Descripcion_Referencia__c, //BI_COL_Producto__r.Descripcion_Referencia__c, 
		List<BI_COL_Descripcion_de_servicio__c> lstDS=[
			select 	Id, Name, BI_COL_Codigo_paquete__c, BI_COL_Oportunidad__r.AccountId,
					( 	select 	Id, BI_COL_Estado__c, BI_COL_Clasificacion_Servicio__c, BI_COL_Cargo_fijo_mes__c,
								BI_COL_Delta_modificacion_upgrade__c, BI_COL_Producto__c
						from 	BI_COL_Codigo_unico_servicio__r 
						where 	BI_COL_Estado__c Not in ('Inactiva') and ID NOT IN:lstIdMS ),
					BI_COL_Producto_Telefonica__c, 
					BI_COL_Sede_Origen__c 
			from 	BI_COL_Descripcion_de_servicio__c 
			where 	id =: lstIdDS ];
										
		//Ds Inactivas
		//JATC Descripcion_Referencia__c PILAS averiguar cual es el campo en BI_EN //BI_COL_Producto__r.Descripcion_Referencia__c, //BI_COL_Producto__r.Descripcion_Referencia__c,
		map< Id, BI_COL_Descripcion_de_servicio__c >mapDsInactiva = new map< Id, BI_COL_Descripcion_de_servicio__c >([
			select 	Id, Name, BI_COL_Codigo_paquete__c,
					( select 	Id, BI_COL_Estado__c, BI_COL_Clasificacion_Servicio__c, 
								BI_COL_Cargo_fijo_mes__c, BI_COL_Delta_modificacion_upgrade__c, 
								BI_COL_Producto__c
					  from 		BI_COL_Codigo_unico_servicio__r 
					  where 	ID NOT IN:lstIdMS),
					BI_COL_Producto_Telefonica__c, 
					BI_COL_Sede_Origen__c 
			from 	BI_COL_Descripcion_de_servicio__c 
			where 	Id =: lstIdDS 
			and 	BI_COL_Estado_de_servicio__c = 'Inactiva' ] );
		
		mapidXnuevaDS = new Map< id, BI_COL_Descripcion_de_servicio__c >();
		mapidXnueva = new Map< id, String >();
		mapidDR = new Map< id, String >();
		set<string> idfr = new set<string>();
		
		for( BI_COL_Descripcion_de_servicio__c dsTemp : lstDS )
		{	
			mapidXnuevaDS.put(dsTemp.id,dsTemp);
			
			List<BI_COL_Modificacion_de_Servicio__c> listmodServ = dsTemp.BI_COL_Codigo_unico_servicio__r;

			for( BI_COL_Modificacion_de_Servicio__c temp : listmodServ )
			{
				mapidXnueva.put( temp.id, temp.BI_COL_Producto__c );
				//mapidDR.put( temp.id, temp.BI_COL_Producto__r.Descripcion_Referencia__c ); JATC PILAS averiguar cual es el campo en BI_EN
			}

			idfr.add( dsTemp.BI_COL_Oportunidad__r.AccountId );
		}
		
		for( BI_COL_Modificacion_de_Servicio__c mod : lstModificacion )
		{
			
			if( mod.BI_COL_Estado__c == 'Pendiente' || mod.BI_COL_Estado__c =='Enviado' || mod.BI_COL_Estado__c == 'Activa' )
				msEstado( mod );
				
			if( mod.BI_COL_Clasificacion_Servicio__c != null )
			{
				msValidaciones( mod );
				ValidarClasificacionServicio( mod );
			}
			else
				mod.addError('La MS no tiene clasificacion de servicio: '+ mod.Name);
		}
	}
	/***
	* @Author: 
	* @Company: 
	* @Description: 
	* @History: 
	* Date		|	Author	|	Description
	* 
	***/
	public static void msEstado( BI_COL_Modificacion_de_Servicio__c mod )
	{
		BI_COL_Descripcion_de_servicio__c dsTemp = mapidXnuevaDS.get( mod.BI_COL_Codigo_unico_servicio__c );
		
		if( dsTemp != null )
		{
			integer cont=0, contActiva=0,contSol=0; 
			
			integer contServIng=0, contActivaServIng=0,	contSolServIng=0;
			
			integer contReneg=0, contActivaReneg=0,	contSolReneg=0;
			
			tamLista = 0;
			
			if( dsTemp.BI_COL_Codigo_unico_servicio__r != null )
			{
				tamLista = dsTemp.BI_COL_Codigo_unico_servicio__r.size();
			}
			
			if( mod.BI_COL_Clasificacion_Servicio__c != 'SERVICIO INGENIERIA' && mod.BI_COL_Clasificacion_Servicio__c != 'RENEGOCIACION' )
			{
				if( mod.BI_COL_Estado__c == 'Pendiente')
					cont++;
				else if(mod.BI_COL_Estado__c == 'Enviado')
					contSol++;
				else if(mod.BI_COL_Estado__c == 'Activa')
					contActiva++;
			}
			else if( mod.BI_COL_Clasificacion_Servicio__c == 'RENEGOCIACION' )
			{
				if( mod.BI_COL_Estado__c == 'Pendiente' )
					contReneg++;
				else if( mod.BI_COL_Estado__c == 'Enviado' )
					contSolReneg++;
				else if( mod.BI_COL_Estado__c == 'Activa' )
					contActivaReneg++;
			}
			else
			{
				if( mod.BI_COL_Estado__c == 'Pendiente' )
					contServIng++;
				else if( mod.BI_COL_Estado__c == 'Enviado' )
					contSolServIng++;
				else if( mod.BI_COL_Estado__c == 'Activa' )
					contActivaServIng++;
			}
			
			if( dsTemp.BI_COL_Codigo_unico_servicio__r.size() > 0 )
			{
				for( BI_COL_Modificacion_de_Servicio__c modtemp : dsTemp.BI_COL_Codigo_unico_servicio__r )
				{
					if( modtemp.BI_COL_Clasificacion_Servicio__c != 'SERVICIO INGENIERIA' && modtemp.BI_COL_Clasificacion_Servicio__c != 'RENEGOCIACION')
					{
						if( modtemp.BI_COL_Estado__c =='Pendiente' && mod.Id != modtemp.Id )
							cont++;
						if( modtemp.BI_COL_Estado__c == 'Enviado' && mod.Id != modtemp.Id )//15022012 LVALDES: mod por estado solicitud 
							contSol++;
						if( modtemp.BI_COL_Estado__c == 'Activa' && mod.Id != modtemp.Id )
						{
							msEstA = modtemp;
							contActiva++;
						}
					}
					else if( modtemp.BI_COL_Clasificacion_Servicio__c == 'RENEGOCIACION')
					{
						
						if( modtemp.BI_COL_Estado__c =='Pendiente' && mod.Id != modtemp.Id )
							contReneg++;
						if( modtemp.BI_COL_Estado__c =='Enviado' && mod.Id != modtemp.Id ) 
							contSolReneg++;
						if( modtemp.BI_COL_Estado__c =='Activa' && mod.Id != modtemp.Id )
							contActivaReneg++;
					}
					else
					{
						if( modtemp.BI_COL_Estado__c == 'Pendiente' && mod.Id != modtemp.Id )
							contServIng++;
						if( modtemp.BI_COL_Estado__c=='Enviado' && mod.Id != modtemp.Id) 
							contSolServIng++;
						if( modtemp.BI_COL_Estado__c=='Activa' && mod.Id != modtemp.Id )
							contActivaServIng++;
					}
				}
			}

			//public PageReference test()
			//{

			//	PageReference p = null;BI_COL_SelectClassificationServices_pag

				
			//	p = new PageReference('apex/error');
			//	p.getParameters().put('error', 'noParam');
			    
			//}

			if( mod.BI_COL_Clasificacion_Servicio__c !='SERVICIO INGENIERIA' && mod.BI_COL_Clasificacion_Servicio__c !='RENEGOCIACION')
			{
				if( cont > 1 )
					mod.addError('No se puede generar m�s de una Novedad en estado Pendiente, verifique las MS asociadas');
				if( contActiva > 1 )
					mod.addError('No se puede generar m�s de una Novedad en estado Activa, verifique las MS asociadas');
				if( contSol > 1 )

					mod.addError('No se puede generar m�s de una Novedad en estado Enviado, verifique las MS asociadas');
			}
			else if( mod.BI_COL_Clasificacion_Servicio__c == 'RENEGOCIACION' )
					//ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'Valide campos obligatorios.' ) );	
			{
				if( contReneg  > 1 )
					mod.addError('No se puede generar m�s de una Novedad en estado Pendiente, verifique las MS asociadas, con '+mod.BI_COL_Clasificacion_Servicio__c);
				if( contActivaReneg > 1 )
					mod.adderror('No se puede generar m�s de una Novedad en estado Activa, verifique las MS asociadas con '+mod.BI_COL_Clasificacion_Servicio__c);
				if( contSolReneg > 1 )
					mod.addError('No se puede generar m�s de una Novedad en estado Enviado, verifique las MS asociadas con '+mod.BI_COL_Clasificacion_Servicio__c);
			}
			else
			{
				if( contServIng > 1 )
					mod.addError('No se puede generar m�s de una Novedad en estado Pendiente, verifique las MS asociadas, con servicio de ingenieria');
				if( contActivaServIng > 1 )
					mod.addError('No se puede generar m�s de una Novedad en estado Activa, verifique las MS asociadas con servicio de ingenieria');
				if( contSolServIng > 1 )
					mod.addError('No se puede generar m�s de una Novedad en estado Enviado, verifique las MS asociadas con servicio de ingenieria');
			}
		}
	}
	/***
	* @Author: 
	* @Company: 
	* @Description: 
	* @History: 
	* Date		|	Author	|	Description
	* 
	***/
	public static void msValidaciones( BI_COL_Modificacion_de_Servicio__c mod )
	{
		BI_COL_Descripcion_de_servicio__c dsTemp = mapidXnuevaDS.get( mod.BI_COL_Codigo_unico_servicio__c );
		
		if( mod.BI_COL_Clasificacion_Servicio__c =='MODIFICACION - UPGRADE' || 
			mod.BI_COL_Clasificacion_Servicio__c =='MODIFICACION - DOWNGRADE'||
			mod.BI_COL_Clasificacion_Servicio__c =='CAMBIO SERVICIO - ALTA')
		{
			if( mapidXnueva.containsKey( mod.BI_COL_Producto__c ) )
			{
				mod.addError('Para este tipo de modificaci�n, el producto asociado a la DS debe tener un identificador distinto al inicial');
				return;
			}
			
			if( mod.BI_COL_Clasificacion_Servicio__c == 'MODIFICACION - UPGRADE' || mod.BI_COL_Clasificacion_Servicio__c == 'MODIFICACION - DOWNGRADE')
			{
				//JATC Descripcion_Referencia__c PILAS averiguar cual es el campo en BI_EN
				/*
				if(mapidDR.containsKey(mod.BI_COL_Producto__r.Descripcion_Referencia__c))
				{
					mod.addError('La descripcion referencia del producto es diferente');
					return;	
				}
				*/
			}
		}
	}
	/***
	* @Author: 
	* @Company: 
	* @Description: 
	* @History: 
	* Date		|	Author	|	Description
	* 
	***/
	public static void ValidarClasificacionServicio( BI_COL_Modificacion_de_Servicio__c mod )
	{
		if( mod.BI_COL_Clasificacion_Servicio__c == 'SUSPENSION TEMPORAL' )
		{
			integer total;
			
			/*if( mod.BI_COL_Fecha_inicio_suspension_temporal__c < mod.BI_COL_Fecha_fin_suspension_temporal__c )
			{
				Integer dias = Integer.valueOf( BI_COL_ValidacionFechas__c.getAll().get('Diferencia_Dias_De_Suspension').get( 'BI_COL_Dias__c' ) );
				total = mod.BI_COL_Fecha_inicio_suspension_temporal__c.daysBetween( mod.BI_COL_Fecha_fin_suspension_temporal__c );
				
				if( total > dias )
					mod.adderror('La diferencia entre la fecha inicio suspension temporal y fecha fin suspension temporal no debe ser mayor a ' + dias + ' dias');				
				else
					mod.adderror('La fecha inicio suspension temporal no debe ser mayor a la fecha fin suspension temporal');				
			}*/

			if( mapidXnuevaDS.get( mod.BI_COL_Codigo_unico_servicio__c ) != null )
			{
				
				List<BI_COL_Modificacion_de_Servicio__c> listmdnew = mapidXnuevaDS.get( mod.BI_COL_Codigo_unico_servicio__c).BI_COL_Codigo_unico_servicio__r;

				for( BI_COL_Modificacion_de_Servicio__c tempmd : listmdnew )
				{
					if( tempmd.BI_COL_Estado__c == 'Suspensión voluntaria' && tempmd.BI_COL_Clasificacion_Servicio__c == 'SUSPENSION TEMPORAL')
						mod.adderror('No se pueden generar mas MS pues el servicio esta suspendido');
				}
			}
		}
	}
	/***
	* @Author: 
	* @Company: 
	* @Description: 
	* @History: 
	* Date		|	Author	|	Description
	* 
	***/
	public static void cargoFijoMes( List<BI_COL_Modificacion_de_Servicio__c> lstNew, List<BI_COL_Modificacion_de_Servicio__c> lstOld, set<String> idprod )
	{	/*
		//JATC Pilas!!!!!! en el query van estos campos
		//, Cod_Desc_Referencia__c, Viaja_TRS__c, Demo_y_o__c, Precio_Lista_CFM_Servicio_Ocasional__c 
		map<Id,Producto_Telefonica__c> prduct = new map<Id,Producto_Telefonica__c>([
			SELECT 	Id
			FROM 	NE__OrderItem__c 
			WHERE 	Id IN :idprod]);
		//INICIO CambioRFS 0
		//Se almacenan los id de la DS de las MS que cambia el RFS
		set<id> rfsDsId = new set<id>();
		//almacenan ids ms para que no busque en estas MS
		set<id> rfsMSId = new set<id>();
		set<String> noViajanTrs = new set<String>();
		noViajanTrs.add('CAMBIO CUENTA');
		noViajanTrs.add('RENEGOCIACION');
		noViajanTrs.add('COBRO UNICO');
		noViajanTrs.add('CESION CONTRATO');
		//FIN CambioRFS 01
		for( Integer i = 0; i < lstNew.size(); i++ )
		{
			string codRef = '';
		
			if( prduct.get( lstNew[i].Producto_Telefonica__c) != null )
			{
				codRef = prduct.get(lstNew[i].Producto_Telefonica__c).Cod_Desc_Referencia__c;
				//INICIO CambioRFS 01
				NE__OrderItem__c producto = prduct.get( lstNew[i].Producto_Telefonica__c );	
				
				if( producto.Viaja_TRS__c == false || noViajanTrs.contains( lstNew[i].BI_COL_Clasificacion_Servicio__c ) )
				{
					if( lstNew[i].BI_COL_Fecha_instalacion_servicio_RFS__c != null && 
						lstOld[i].BI_COL_Fecha_instalacion_servicio_RFS__c != lstNew[i].BI_COL_Fecha_instalacion_servicio_RFS__c )
					{
						lstNew[i].BI_COL_Estado__c = 'Activa';
						lstNew[i].BI_COL_Estado_orden_trabajo__c = 'Ejecutado';
						//Para buscar dentro las ms relacionadas
						rfsDsId.add( lstNew[i].BI_COL_Codigo_unico_servicio__c );
					}
				}
				//FIN CambioRFS 01
			}
			//INICIO CambioRFS 02
			//para No buscar dentro las Ms que estoy cambiando
			rfsMsId.add( lstNew[i].Id );
			//FIN CambioRFS 02
			BI_COL_EMAIL_PROTECTION__c emailProtection = BI_COL_EMAIL_PROTECTION__c.getInstance( codRef );
			
			if( emailProtection != null && codRef != null )
			{
				if( lstNew[i].Cantidad__c == null || lstNew[i].Cantidad__c == 0 )
					lstNew[i].addError('Por favor Ingrese una cantidad');
			}
			
			if( prduct.get(lstNew[i].Producto_Telefonica__c) != null )
			{
				String texto=''+prduct.get(lstNew[i].Producto_Telefonica__c).Demo_y_o__c;
				
				if(!texto.containsIgnoreCase('servicio ocasional'))
				{
					if( ( lstNew[i].Cargo_Fijo_Mes__c != lstOld[i].Cargo_Fijo_Mes__c || lstNew[i].Cantidad__c != lstOld[i].Cantidad__c) &&
					 	( emailProtection!=null && codRef != null) && (lstNew[i].Cantidad__c != null && lstNew[i].Cantidad__c != 0)  &&
						( lstNew[i].Precio_lista_Cargo_fijo_mes__c != null && lstNew[i].Precio_lista_Cargo_fijo_mes__c != 0 ) )
					{
						double CFM = lstNew[i].Cantidad__c * lstNew[i].Precio_lista_Cargo_fijo_mes__c;
						if( lstNew[i].Cargo_Fijo_Mes__c < CFM )
							lstNew[i].addError(' El cargo fijo mes no puede ser menor a '+ CFM+ ' Valide el Precio de lista Cargo fijo mes y la cantidad');				
					}
				}
				else
				{
					if( ( lstNew[i].BI_COL_Cargo_fijo_mes__c != lstOld[i].BI_COL_Cargo_fijo_mes__c || lstNew[i].Cantidad__c != lstOld[i].Cantidad__c) && lstNew[i].Cantidad__c != null )
					{
						double CFM = lstNew[i].BI_COL_Cantidad__c * prduct.get( lstNew[i].BI_COL_Producto__c ).Precio_Lista_CFM_Servicio_Ocasional__c;

						if( lstNew[i].BI_COL_Cargo_fijo_mes__c < CFM )
							lstNew[i].addError(' El cargo fijo mes no puede ser menor a '+ CFM + ' Valide el Precio de lista Cargo fijo mes y la cantidad');				
					}
				}
			}
		}
		//INICIO CambioRFS 03
		//TRAE ms relacionadas CambioRFS para actualizar el estado
		list<BI_COL_Modificacion_de_Servicio__c> relacionadas = new list<BI_COL_Modificacion_de_Servicio__c>([
			select 	Id, BI_COL_Estado__c, BI_COL_Codigo_unico_servicio__c 
			from 	BI_COL_Modificacion_de_Servicio__c 
			where 	BI_COL_Estado__c = 'Activa' 
			and 	BI_COL_Codigo_unico_servicio__c IN :rfsDsId and id NOT IN: rfsMsId ] );

		for ( BI_COL_Modificacion_de_Servicio__c mr :relacionadas )
			mr.BI_COL_Estado__c = 'Inactiva';
			
		try
		{
			update relacionadas;		
		}
		catch(Exception e)
		{

		}
		//FIN CambioRFS 03
		*/
	}
	/***
	* @Author: 
	* @Company: 
	* @Description: 
	* @History: 
	* Date		|	Author	|	Description
	* 
	***/
	public static void servicioIngenieria( List<BI_COL_Modificacion_de_Servicio__c> lisModSNew )
	{
		map<String, BI_COL_Modificacion_de_Servicio__c> dsRelacionadas = new map<String, BI_COL_Modificacion_de_Servicio__c>();

		for( Integer i = 0; i < lisModSNew.size(); i++ )
		{
			//Obtengo las Ms con SERVICIO INGENIERIA
			if( lisModSNew[i].BI_COL_Clasificacion_Servicio__c == 'SERVICIO INGENIERIA' || lisModSNew[i].BI_COL_Clasificacion_Servicio__c == 'RENEGOCIACION' )
				dsRelacionadas.put( lisModSNew[i].BI_COL_Codigo_unico_servicio__c, lisModSNew[i] );
		}
		//si hay SERVICIO INGENIERIA obtengo las MS-DS Relacionadas
		if( !dsRelacionadas.isEmpty() )
		{
			set<string> clasValidas = new set<string>{'MODIFICACION DOWNGRADE','MODIFICACION UPGRADE','CAMBIO SERVICIO ALTA','ALTA',
				'RENEGOCIACION','CAMBIO S ALTA TRASLA','UPGRADE TRASLADO','DOWNGRADE TRASLADO','RENEGOCIACION TRASLA'};

			list<BI_COL_Modificacion_de_Servicio__c> msR = new list<BI_COL_Modificacion_de_Servicio__c>([
				select 	Id, BI_COL_Codigo_unico_servicio__c 
				from 	BI_COL_Modificacion_de_Servicio__c 
				where 	((BI_COL_Clasificacion_Servicio__c IN :clasValidas and
						BI_COL_Estado__c IN ('Pendiente','Enviado'))
				Or 		BI_COL_Estado__c = 'Activa') 
				and 	BI_COL_Codigo_unico_servicio__c IN :dsRelacionadas.keySet()]);
			
			if( msR != null && msR.size() > 0 )
			{
				//Almacena las id ds si tiene una relacion valida
				set<String> relacionValida = new set<String>();
				for( BI_COL_Modificacion_de_Servicio__c relac : msR )
					relacionValida.add( relac.BI_COL_Codigo_unico_servicio__c );			
				//Si no encuentra una relacion valida genera error
				for( string val : dsRelacionadas.keySet() )
				{
					//JATC Pilas!!!!!!!!!! Id quemado
					if( ( relacionValida.contains( val ) == null || relacionValida.size() == 0 ) && UserInfo.getProfileId()!='00e30000000lpYCAAY')
						dsRelacionadas.get(val).addError('La Operaci�n Comercial Servicio de Ingenier�a solo se podr� '+
							'utilizar si existe en una DSR con MS en estado Pendiente o Enviado  con las siguientes operaciones'+
							' Comerciales (Alta o Cambio Servicio Alta o Modificaci�n Upgrade o Modificaci�n Downgrade o Renegociaci�n)');								
				}
			}	
		}
	}
}