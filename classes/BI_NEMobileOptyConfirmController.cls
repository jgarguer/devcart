public class BI_NEMobileOptyConfirmController 
{
    public NE__Order__c configurationCreated{get;set;}
    public Account selectedAccount{get;set;}
    public String optyIdURL{get;set;}
    
    public BI_NEMobileOptyConfirmController()  {
        String accId        =   apexPages.currentPage().getParameters().get('accId');
        String cuit         =   apexPages.currentPage().getParameters().get('cuit');
        String accType      =   apexPages.currentPage().getParameters().get('accType');
        
        system.debug('apexPages.currentPage().getParameters(): '+apexPages.currentPage().getParameters());

        if(accType  ==  'DUMMY') {
            selectedAccount =   new Account();   
            selectedAccount.NE__Vat__c  =   cuit;
        } else {
            selectedAccount =   [SELECT name, NE__Vat__c,ShippingCity,ShippingCountry,ShippingLatitude,ShippingLongitude,ShippingPostalCode,ShippingState,ShippingStreet 
                                 FROM account 
                                 WHERE id =: accId];
        }      
    }
    
    public void saveAndCreateOpty() {
        upsert selectedAccount;
        
        RecordType recType  =   [SELECT Id FROM RecordType WHERE (SobjectType = 'Order__c' OR SobjectType = 'NE__Order__c') AND Name = 'Opty' limit 1];        
          
        String confId               =   apexPages.currentPage().getParameters().get('orderId');
        NE__Order__c configuration  =   [SELECT id, NE__Version__c, NE__OptyId__c, NE__OpportunityId__c FROM NE__Order__c WHERE id =: confId];  
        
        String optyId               =   BI_OpportunityMethods.createQuickOpp(selectedAccount.id, configuration.id);
        system.debug('optyId new test:'+optyId );
        
        configuration.RecordTypeId          =   recType.id;
        configuration.NE__Version__c        =   1;
        configuration.NE__OptyId__c         =   optyId;
        configuration.NE__OpportunityId__c  =   optyId;
        
        update configuration;           optyIdURL   =   '/'+optyId;
    }   
}