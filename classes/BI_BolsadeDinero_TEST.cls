@isTest
private class BI_BolsadeDinero_TEST {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_Bolsa_de_Dinero__c and BI_Transaccion_bolsa_dinero classes
    
    History: 
    <Date> 					<Author> 				<Change Description>
    16/04/2014      		Ignacio Llorca    		Initial Version		
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Pablo Oliva
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_Bolsa_de_Dinero__c and BI_Transaccion_bolsa_dinero
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	16/04/2014      		Ignacio Llorca    		Initial Version	
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/		    

	static testMethod void bolsadeDinerotest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;


    	List <Recordtype> lst_rt = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Libre' AND sObjectType = 'BI_Bolsa_de_Dinero__c'];
	     
	    List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List <Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);	

		List <BI_Bolsa_de_Dinero__c> lst_bolsa = new List <BI_Bolsa_de_Dinero__c>();     

        for (Integer i=0;i<200;i++){
	        BI_Bolsa_de_Dinero__c bolsa = new BI_Bolsa_de_Dinero__c(BI_Cliente__c = lst_acc[0].Id,
	        														BI_Monto_Inicial__c = 1000000,
	        														CurrencyIsoCode = 'EUR',
	        														RecordtypeId = lst_rt[0].Id);

	        lst_bolsa.add(bolsa);
    	}    	
    	insert lst_bolsa;

    	Test.startTest();

        BI_BolsadeDineroMethods.blockEdition(null, null);

    	List <BI_Transaccion_de_bolsa_de_dinero__c> lst_trans = new List <BI_Transaccion_de_bolsa_de_dinero__c>();
        List <BI_Transaccion_de_bolsa_de_dinero__c> lst_trans2 = new List <BI_Transaccion_de_bolsa_de_dinero__c>();
    	for (Integer i=0;i<200;i++){
    		BI_Transaccion_de_bolsa_de_dinero__c trans = new BI_Transaccion_de_bolsa_de_dinero__c(BI_Movimiento__c = 100,
    																							  BI_Codigo_de_bolsa_de_dinero__c = lst_bolsa[0].Id);
    		lst_trans.add(trans);
    	}
    	insert lst_trans;

        for (Integer i=0;i<200;i++){
            BI_Transaccion_de_bolsa_de_dinero__c trans2 = new BI_Transaccion_de_bolsa_de_dinero__c(BI_Movimiento__c = 100,
                                                                                                  BI_Codigo_de_bolsa_de_dinero__c = lst_bolsa[0].Id);
            lst_trans2.add(trans2);
        }
        insert lst_trans2;

        delete lst_trans2;

    	for (Integer i=0;i<50;i++){
    		lst_trans[i].BI_Movimiento__c = 50;
    	}
    	update lst_trans;

    	for (Integer i=0;i<50;i++){
    		lst_trans[i].BI_Movimiento__c = 1000000;
    	}
    	try{
    		update lst_trans;
    	}catch(exception e){
    		system.assert(e.getMessage().contains('El saldo disponible en la bolsa de dinero no puede ser negativo'));
    	}
    	for(BI_Bolsa_de_Dinero__c bolsa:lst_bolsa){
    		bolsa.BI_Estado_del_proceso__c = 'Cancelado';
    	}
    	update lst_bolsa;

    	for(BI_Bolsa_de_Dinero__c bolsa:lst_bolsa){
    		bolsa.BI_Saldo_Disponible__c = 2000000;
    	}

    	try{
    		update lst_bolsa;
    	}catch(exception e){
    		system.assert(e.getMessage().contains('No se puede modificar la bolsa de dinero en estado Cancelada'));
    	}

    	for (Integer i=0;i<50;i++){
    		lst_trans[i].BI_Movimiento__c = 50;
    	}
    	try{
    		update lst_trans;
    	}catch(exception e){
    		system.assert(e.getMessage().contains('No se puede modificar la transacción en el estado actual de la bolsa de dinero'));
    	}
        try{
            delete lst_trans;
        }catch(exception e){
            system.assert(e.getMessage().contains('No se puede borrar la transacción en el estado actual de la bolsa de dinero'));
        }
    	

    	Test.stopTest();
        
    }
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}