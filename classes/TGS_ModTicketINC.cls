/*------------------------------------------------------------------------------
Author:         Ana Cirac 
Company:        Deloitte
Description:    Class to create/update INCIDENTS
History
<Date>          <Author>            <Change Description>
24-Feb-2015     Ana Cirac           Initial Version
23-Sep-2015     Ana Cirac           Add parameter infoMwan to upsertINC
01-Mar-2016     Miguel Cabrera      Add Backup_Line field to mWan Info
28-Mar-2016     Jose Miguel Fierro  Merged from downstream + spacing fixes
21-Oct-2016     Juan Carlos Terrón  Deleted IF(logInContact) sentence since it's already in the called method. Lines  202,361. 
13-Jun-2017		Ramon Pernas		Create incident class, Add TGS_Resolution_Category_Tier_1, TGS_Resolution_Category_Tier_2, TGS_Resolution_Category_Tier_3,
									TGS_Fecha_Aparicion, TGS_Fecha_Parada_Reloj, TGS_Fecha_Arranque_Reloj, TGS_Tiempo_Afectacion from RoD
------------------------------------------------------------------------------*/
global with sharing class TGS_ModTicketINC {
    
    //RPM 13/06/2017
    global with sharing class incident{
        webservice String TGS_Resolution_Category_Tier_1;
        webservice String TGS_Resolution_Category_Tier_2;
        webservice String TGS_Resolution_Category_Tier_3;
        webservice String TGS_Fecha_Aparicion;
        webservice String TGS_Fecha_Parada_Reloj;
        webservice String TGS_Fecha_Arranque_Reloj; 
        webservice String TGS_Tiempo_Afectacion;
        webservice String Ticket_ID;
        webservice String CaseNumber;
        webservice String Operation;
        webservice String Service_Type;
        webservice String Impact;
        webservice String Urgency;
        webservice String Subject;
        webservice String Description;
        webservice String Reported_Source;
        webservice String AssigneeName;
        webservice String LogInContact;
        webservice String Company;
        webservice String Direct_Contact_Site;
        webservice String Direct_Contact_Department; 
        webservice String CI_ID; 
        webservice String Categorization_Tier_1; 
        webservice String Categorization_Tier_2; 
        webservice String Categorization_Tier_3; 
        webservice String Product_Categorization_Tier_1; 
        webservice String Product_Categorization_Tier_2; 
        webservice String Product_Categorization_Tier_3; 
        webservice String Direct_Contact_Organization; 
        webservice String Owner_Group;
        webservice String Status; 
        webservice String Status_Reason;
        webservice String Priority;
        webservice String Resolution;
    }
    //END RPM
    
    global with sharing class mWanInfo{ 
        webservice String Customer_Contacted; 
        webservice String Electrical_Power; 
        webservice String CPE_Status;
        webservice String CPE_Reset;
        webservice String Cabling_Checked; 
        webservice String Working_Before; 
        webservice DateTime Date_CameUp;
        webservice String Change_Pending; 
        webservice String Allowed_Intrusive_Test; 
        webservice String Repetitive_Issue; 
        webservice String Site_Details;
        webservice String Incident_Summary;
        webservice String Backup_Line;
    }
    
    
    /* A new INC is created or updated */
    //RPM 13/06/2017 Modify incident Object
    webservice static TGS_ModTicket.Result upsertINC(incident incidencia, mWanInfo infoMwan){
        
        Savepoint savePointCheckout = Database.setSavepoint();  
        /* Result of the request */
        TGS_ModTicket.Result resul = new TGS_ModTicket.Result();
        
        system.debug('######' + incidencia.Operation);
        
        try{
            if (incidencia.Operation.equals(TGS_ModTicket.ROD_SF_CREATE_INCIDENT)){ 
                /*CREATES A INC*/
                resul = createINC(incidencia, infoMwan);
            }else if(incidencia.Operation.equals(TGS_ModTicket.ROD_SF_MODIFY_INCIDENT)){
                /*UPDATES A INC*/
                resul = updateINC(incidencia, infoMwan);
            }else{
                
                throw new IntegrationException('Invalid Operation');
            }
        } catch(Exception e) {
            resul.Operation_Status = 'ERROR'; 
            resul.Error_Msj = e.getMessage();
            resul.CaseNumber = incidencia.CaseNumber;
            system.debug('Exception found: '+e+' At line: '+e.getLineNumber());
            system.debug('CaseNumber: '+resul.CaseNumber);
            DataBase.rollback(savePointCheckout);
            //add a tgs_error_integrations_c object
            TGS_Error_Integrations__c error= new TGS_Error_Integrations__c();
            error.TGS_Interface__c = 'ROD'; 
            error.TGS_Operation__c = incidencia.Operation;
            error.TGS_RecordId__c = incidencia.CaseNumber;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
            error.TGS_RequestInfo__c = 'Message: '+e.getMessage();
            insert error;
        }  
        return resul;
    }
    
    /*------------------------------------------------------------ 
	Author:         Ana Cirac 
	Company:        Deloitte
	Description:    Web Service that consumes RoD to create INCIDENTS
	History
	<Date>          <Author>        <Change Description>
	24-Feb-2015     Ana Cirac        Initial Version
	13-Jun-2017		Ramon Pernas	 Create incident class, Add TGS_Resolution_Category_Tier_1, TGS_Resolution_Category_Tier_2, TGS_Resolution_Category_Tier_3,
									 TGS_Fecha_Aparicion, TGS_Fecha_Parada_Reloj, TGS_Fecha_Arranque_Reloj, TGS_Tiempo_Afectacion from RoD
	------------------------------------------------------------*/

    static TGS_ModTicket.Result createINC(incident incidencia, mWanInfo infoMwan){
        TGS_ModTicket.Result resul = new TGS_ModTicket.Result();     
        resul.operation_status = 'OK'; 
        /* New Case to create */
        Case caseNew = new Case();  
        caseNew.TGS_Ticket_Id__c = incidencia.Ticket_ID;
        /* Technical_Assignee */
        caseNew.TGS_Technical_Assignee__c = incidencia.AssigneeName;
        Id rtId = null;
        if (!String.isNotBlank(incidencia.Service_Type)){      
            
            throw new IntegrationException('Service_Type is mandatory');     
            
        }else if (!TGS_ModTicket.userServerRestoration.contains(incidencia.Service_Type) && !incidencia.Service_Type.equals(TGS_ModTicket.QUERY_TICKET)){
            throw new IntegrationException('Invalid Service_Type'); 
        }else if(TGS_ModTicket.userServerRestoration.contains(incidencia.Service_Type)){
            //rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Incident').getRecordTypeId();
            rtId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType,'TGS_Incident');
            incidencia.Service_Type = TGS_ModTicket.USER_SERVICE_RESTORATION;  
            
        }else if(incidencia.Service_Type.equals(TGS_ModTicket.QUERY_TICKET)){
            //rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Query').getRecordTypeId();
            rtId =  TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType,'TGS_Query');
            
        }
        caseNew.RecordTypeId = rtId;
        caseNew.Type = incidencia.Service_Type;
        /*Owner Group*/
        if(!String.isNotBlank(incidencia.Owner_Group)){        
            
            throw new IntegrationException('Owner Group is mandatory'); 
        }else {
            /* attributes caseNew.ownerId is assigned */
            TGS_ModTicket.validateOwner(caseNew,incidencia.Owner_Group);
        }
        
        /* TGS_Ticket_Site__c */
        String site = TGS_ModTicket.ValidateSite(incidencia.Direct_Contact_Site);
        if(site.equals('-1')){
            
            throw new IntegrationException('Invalid Direct Contact Site');  
        }else if (String.isNotBlank(site)){ 
            
            caseNew.TGS_Ticket_Site__c =site;
            
        }
        /* Impact */
        Set<String> listImpact= TGS_ModTicket.getPickListValues('Case','TGS_Impact__c');
        
        if (!String.isNotBlank(incidencia.Impact)){         
            
            throw new IntegrationException('Impact is mandatory');  
            
        }else if(!listImpact.contains(incidencia.Impact)) {
            
            throw new IntegrationException('The Impact ' +incidencia.Impact+ ' does not exist');   
            
        }else{
            caseNew.TGS_Impact__c = incidencia.Impact;
        }
        /* Valid values of Urgency */
        Set<String> lstUrgency = TGS_ModTicket.getPickListValues('Case','TGS_Urgency__c');
        system.debug(lstUrgency);
        if (!String.isNotBlank(incidencia.Urgency)){       
            
            throw new IntegrationException('Urgency is mandatory'); 
            
        }else if(!lstUrgency.contains(incidencia.Urgency)) {
            
            throw new IntegrationException('The Urgency ' +incidencia.Urgency+ ' does not exist'); 
            
        }else{
            
            caseNew.TGS_Urgency__c = incidencia.Urgency;
        }   
        /* Subject */
        if (!String.isNotBlank(incidencia.Subject)){       
            
            throw new IntegrationException('Description is mandatory'); 
            
        }else{
            caseNew.Subject = incidencia.Subject;
        }
        /* Description */
        if (!String.isNotBlank(incidencia.Description)){       
            
            throw new IntegrationException('Detailed Description is mandatory');    
            
        }else{
            caseNew.Description = incidencia.Description;
        }
        /* Origin*/
        if (!String.isNotBlank(incidencia.Reported_Source)){       
            
            throw new IntegrationException('Reported Source is mandatory'); 
            
        }else if (!TGS_ModTicket.validReportedSources.contains(incidencia.Reported_Source)){
            
            throw new IntegrationException('The Reported Source ' +incidencia.Reported_Source+ ' is not valid');   
            
        }else {
            
            caseNew.origin = TGS_ModTicket.validateReportedSource(incidencia.Reported_Source);  
            
        }
        
        /* Account */
        if(!TGS_ModTicket.validateAccount(caseNew,incidencia.Direct_Contact_Organization,incidencia.Direct_Contact_Department,incidencia.Company)){
            
            throw new IntegrationException('A valid Account is mandatory');
        }
        /* Attribute Assignee*/
        
        //JCT 21/10/2016 Deleted IF(logInContact) sentence since it's already in the called method.
        TGS_ModTicket.validateContact(caseNew,incidencia.logInContact);
        
        
        /* Status */
        if (!String.isNotBlank(incidencia.Status)) {
            throw new IntegrationException('Status is mandatory');
        }else {
            caseNew.Status = 'Assigned';
        }
        
        //RPM 13/06/2017 
        caseNew.TGS_Resolution_Category_Tier_1__c = incidencia.TGS_Resolution_Category_Tier_1;
        
        caseNew.TGS_Resolution_Category_Tier_2__c = incidencia.TGS_Resolution_Category_Tier_2;
        
        caseNew.TGS_Resolution_Category_Tier_3__c = incidencia.TGS_Resolution_Category_Tier_3;
        
        caseNew.TGS_Fecha_Aparicion__c = incidencia.TGS_Fecha_Aparicion;
        
        caseNew.TGS_Fecha_Parada_Reloj__c = incidencia.TGS_Fecha_Parada_Reloj;
        
        caseNew.TGS_Fecha_Arranque_Reloj__c = incidencia.TGS_Fecha_Arranque_Reloj;
        
        caseNew.TGS_Tiempo_Afectacion__c = incidencia.TGS_Tiempo_Afectacion;

        //END RPM                                 
        
        /* Validates Not Mandatory attributes  */
        TGS_ModTicket.validateNotMandatoryAtt(caseNew, incidencia.Status_Reason, incidencia.Priority, incidencia.Categorization_Tier_1, incidencia.Categorization_Tier_2,
                                              incidencia.Categorization_Tier_3, incidencia.Product_Categorization_Tier_1, incidencia.Product_Categorization_Tier_2, incidencia.Product_Categorization_Tier_3);
        /* Validates and Sets mWan info */
        TGS_ModTicket.updateInfoMwan(caseNew,infoMwan);
        
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule= false;
        caseNew.setOptions(dmo);
        /* validate and assigns CI_ID*/
        TGS_ModTicket.validateCI(caseNew,incidencia.CI_ID);
        /* Inserts the case */
        insert caseNew;  
        /* Updates the resolution */
        TGS_ModTicket.setResolution(incidencia.Resolution,caseNew.id); 
        
        /* Updates the case Status */ 
        if (incidencia.Status.equals('In Progress')){
            
            caseNew.Status = incidencia.Status;
            
            
            update caseNew;
        }
        /* Calculates the CaseNumber of the case to retun it */
        String caseNum = [SELECT Id, CaseNumber FROM Case WHERE Id=:caseNew.Id].CaseNumber;        
        resul.CaseNumber = caseNum;
        return resul;
    }
    
    /*------------------------------------------------------------ 
	Author:         Ana Cirac 
	Company:        Deloitte
	Description:    Web Service that consumes RoD to update INCIDENTS
	History
	<Date>          <Author>        <Change Description>
	24-Feb-2015     Ana Cirac        Initial Version
	------------------------------------------------------------*/
    //RPM 13/06/2017 Add  test object 
    static TGS_ModTicket.Result updateINC(incident incidencia, mWanInfo infoMwan){
        System.debug('STATUS RECIBIDO: ' + incidencia.Status);                            
        TGS_ModTicket.Result resul = new TGS_ModTicket.Result();     
        resul.operation_status = 'OK';     
        resul.CaseNumber = incidencia.CaseNumber;     
        if (!String.isNotBlank(incidencia.CaseNumber)){
            throw new IntegrationException('CaseNumber Mandatory');
        }   
        List<Case> lisCaseUpdate = [SELECT ID, Status, CaseNumber, Account.RecordType.DeveloperName FROM Case WHERE CaseNumber=:incidencia.CaseNumber];
        
        if (lisCaseUpdate.size()==0){
            
            throw new IntegrationException('Invalid Case Number');
            
        }
        
        /*Case to update */
        Case caseUpdate  = lisCaseUpdate.get(0);    
        
        
        if (String.isNotBlank(incidencia.Service_Type)){       
            Id rtId = null;
            if (!TGS_ModTicket.userServerRestoration.contains(incidencia.Service_Type) && !incidencia.Service_Type.equals(TGS_ModTicket.QUERY_TICKET)){
                throw new IntegrationException('Invalid Service_Type'); 
            }else if(TGS_ModTicket.userServerRestoration.contains(incidencia.Service_Type)){
                rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Incident').getRecordTypeId();
                incidencia.Service_Type = TGS_ModTicket.USER_SERVICE_RESTORATION; 
                
            }else if(incidencia.Service_Type.equals(TGS_ModTicket.QUERY_TICKET)){
                rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Query').getRecordTypeId();                
            }
            caseUpdate.RecordTypeId = rtId;
            caseUpdate.Type = incidencia.Service_Type;
            
        } 
        /* Technical_Assignee */
        caseUpdate.TGS_Technical_Assignee__c = incidencia.AssigneeName;
        
        /*ownerId */
        if (String.isNotBlank(incidencia.Owner_Group)){    
            
            TGS_ModTicket.validateOwner(caseUpdate,incidencia.Owner_Group);
        }
        if (String.isNotBlank(incidencia.Direct_Contact_Site)){    
            /* TGS_Ticket_Site__c */
            String site = TGS_ModTicket.ValidateSite(incidencia.Direct_Contact_Site);
            if(site.equals('-1')){
                
                throw new IntegrationException('Invalid Direct Contact Site');  
            }else if (String.isNotBlank(site)){ 
                
                caseUpdate.TGS_Ticket_Site__c =site;
                
            }else{
                caseUpdate.TGS_Ticket_Site__c = null;
            }
        }
        /* Impact */
        if (String.isNotBlank(incidencia.Impact)){      
            Set<String> listImpact= TGS_ModTicket.getPickListValues('Case','TGS_Impact__c');    
            if(!listImpact.contains(incidencia.Impact)) {
                
                throw new IntegrationException('The Impact ' +incidencia.Impact+ ' does not exist');   
                
            }else{
                caseUpdate.TGS_Impact__c = incidencia.Impact;
            }
        }
        /* Urgency */
        if (String.isNotBlank(incidencia.Urgency)){
            /* Valid Urgency */     
            Set<String> lstUrgency = TGS_ModTicket.getPickListValues('Case','TGS_Urgency__c');
            if(!lstUrgency.contains(incidencia.Urgency)) {
                
                throw new IntegrationException('The Urgency ' +incidencia.Urgency+ ' does not exist'); 
                
            }else{
                
                caseUpdate.TGS_Urgency__c = incidencia.Urgency;
            }   
        }  
        /* Subject */
        if (String.isNotBlank(incidencia.Subject)){        
            caseUpdate.Subject = incidencia.Subject;
        }
        /* Description */
        if (String.isNotBlank(incidencia.Description)){        
            caseUpdate.Description = incidencia.Description;
        }
        /* Origin*/
        if (String.isNotBlank(incidencia.Reported_Source)){        
            /* valid Reported Sources */
            if (!TGS_ModTicket.validReportedSources.contains(incidencia.Reported_Source)){
                
                throw new IntegrationException('The Reported Source ' +incidencia.Reported_Source+ ' is not valid');   
            }else {
                caseUpdate.origin = TGS_ModTicket.validateReportedSource(incidencia.Reported_Source); 
            }
        }   
        
        /* Account */
        TGS_ModTicket.updateAccounts(caseUpdate, incidencia.Direct_Contact_Department, incidencia.Direct_Contact_Organization, incidencia.Company);
        /*
	if (String.isNotBlank(logInAssignee)){
	TGS_ModTicket.validateAssignee(caseUpdate,logInAssignee);
	}
	*/
        System.debug('LogInContact: '+ incidencia.LogInContact);
        //JCT 21/10/2016 Deleted IF(logInContact) sentence since it's already in the called method.
        TGS_ModTicket.validateContact(caseUpdate, incidencia.LogInContact);
        
        /* contactId */
        /* try{
	Contact contact = [SELECT Id,Name FROM Contact WHERE Email=:Email];
	caseUpdate.ContactId= contact.Id;
	}catch(Exception e){
	throw new IntegrationException('Invalid Email');    
	}*/
        
        /* Status */
        System.debug('Status: '+ incidencia.Status);
        if (String.isNotBlank(incidencia.Status)){     
            
            if(!TGS_ModTicket.validStatus.contains(incidencia.Status)){
                
                throw new IntegrationException('The Status ' + incidencia.Status + ' does not exist');   
            }else{
                caseUpdate.Status = incidencia.Status;                    
                /*TGS_Acting_as__c */
                //caseUpdate.TGS_Acting_as__c =  TGS_ModTicket.SA;
                
            }
        }
        
        //RPM 13/06/2017 
        caseUpdate.TGS_Resolution_Category_Tier_1__c = incidencia.TGS_Resolution_Category_Tier_1;
        
        caseUpdate.TGS_Resolution_Category_Tier_2__c = incidencia.TGS_Resolution_Category_Tier_2;
        
        caseUpdate.TGS_Resolution_Category_Tier_3__c = incidencia.TGS_Resolution_Category_Tier_3;
        
        caseUpdate.TGS_Fecha_Aparicion__c = incidencia.TGS_Fecha_Aparicion;
        
        caseUpdate.TGS_Fecha_Parada_Reloj__c = incidencia.TGS_Fecha_Parada_Reloj;
        
        caseUpdate.TGS_Fecha_Arranque_Reloj__c = incidencia.TGS_Fecha_Arranque_Reloj;
        
        caseUpdate.TGS_Tiempo_Afectacion__c = incidencia.TGS_Tiempo_Afectacion;

        //END RPM  
        
        /* Validates Not Mandatory attributes  */
        TGS_ModTicket.validateNotMandatoryAtt(caseUpdate, incidencia.Status_Reason, incidencia.Priority, incidencia.Categorization_Tier_1, incidencia.Categorization_Tier_2,
                                              incidencia.Categorization_Tier_3, incidencia.Product_Categorization_Tier_1, incidencia.Product_Categorization_Tier_2, incidencia.Product_Categorization_Tier_3);
        /* Validates and Sets mWan info */
        TGS_ModTicket.updateInfoMwan(caseUpdate,infoMwan);
        /* validates and assigns CI_ID*/
        TGS_ModTicket.validateCI(caseUpdate,incidencia.CI_ID);           
        /* Updates the case */
        update caseUpdate;
        /* Updates the resolution */
        TGS_ModTicket.setResolution(incidencia.Resolution,caseUpdate.id); 
        
        return resul;
    }
}