/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Test class for BI_O4_AsociarAccountToCipShareJOB JOB
    Test Class:      
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera          Initial version
    20/09/2017                       Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c 
    25/09/2017              Jaime Regidor           Fields BI_Subsector__c and BI_Sector__c added 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_AsociarAccountToCipShareJOB_TEST {

  static{
    BI_TestUtils.throw_exception = false;
  }
	
	@isTest static void AsociarAccountToCipShareJOB() {

	RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];	

    Account acc1 = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_local__c = 'Top 1',
                  RecordTypeId = rt1.Id,
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test'
                  );    
    insert acc1;


    AccountTeamMember at = new AccountTeamMember(AccountId = acc1.Id, UserId = UserInfo.getUserId());
    insert at;

    BI_Plan_de_accion__c pa = new BI_Plan_de_accion__c(BI_O4_MNCs_Account__c = acc1.Id);
    insert pa;

    User user = [SELECT id FROM User WHERE Id !=: UserInfo.getUserId() AND ProfileId =: UserInfo.getProfileId() AND isActive = true limit 1];

    BI_Plan_de_accion__Share pas = new BI_Plan_de_accion__Share(ParentId = pa.Id, UserOrGroupId = user.Id, AccessLevel = 'Edit');
    


		Test.startTest();
		insert pas;
		SchedulableContext sc = null;
		BI_O4_AsociarAccountToCipShareJOB aaj = new BI_O4_AsociarAccountToCipShareJOB();
		aaj.execute(sc);
		Test.stopTest();
	}
}