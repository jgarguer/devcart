public class BI_DataLoadRest {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Methods executed to load data for Rest classes.
    
    History:
    
    <Date>            <Author>              <Description>
    08/09/2014        Pablo Oliva           Initial version
    12/12/2016        Adrián Caro           reduce externalId number
    19/12/2016        Gawron, Julian        Add variable to limit iterations
    20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    //final static List<User> LST_USER_INFO;
    
    // Variable to limit iteration on creation
    public static Integer MAX_LOOP = 2;
    public static boolean SKIP_PARAMETER = true;
    
    static{
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'Account' ]){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }

        //LST_USER_INFO = [SELECT Id, Pais__c, BI_Permisos__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that inserts x records of Account with the external id
    
    IN:            Integer i: Number of records created
                   List<Region__c> lst_region: List of Region__c
    OUT:           List<Account>: Generated records list
    
    History:
    
    <Date>            <Author>              <Description>
    14/08/2014        Pablo Oliva           Initial version
    15/03/2015        Juan Santisi          Refactored method signature to List<String> lst_pais_iso
    10/11/2015        Fernando Arteaga      BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
    16/11/2016        Adrian Caro           Reduce externalId number
    19/12/2016        Gawron, Julián        Add MAX_LOOP
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<Account> loadAccountsExternalId(Integer i, List<String> lst_pais, Boolean holding){
        String pais = lst_pais[0];
        List<Account> lst_acc = new List<Account>();
        Integer externalId = 5;
        

        if(SKIP_PARAMETER && i > MAX_LOOP){  //JEG 19/12/2016
          i = MAX_LOOP;
        }
        
        for(Integer j = 0; j < i; j++){
            
            externalId += j;
            
            Account acc = new Account(Name = 'test'+ String.valueOf(j),
                                      BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                      BI_Activo__c = Label.BI_Si,
                                      BI_Id_del_cliente__c = String.valueOf(externalId),
                                      BI_Id_CSB__c = String.valueOf(externalId), // FAR 23/11/2015
                                      BI_Country__c = pais,
                                      BillingCountry = 'country',
                                      BillingState = 'state',
                                      BillingCity = 'city',
                                      BillingStreet = 'street',
                                      BillingPostalCode = '12345',
                                      ShippingCountry = 'country',
                                      ShippingState = 'state',
                                      ShippingCity = 'city',
                                      ShippingStreet = 'street',
                                      ShippingPostalCode = '12345',
                                      Fax = '12345',
                                      WebSite = 'www.test.com',
                                      Phone = '123456789',
                                      BI_Riesgo__c = '1 - test', 
                                      BI_Segment__c = 'test',
                                      BI_Subsegment_Regional__c = 'test',
                                      BI_Territory__c = 'test'
                                      );
            
            if(holding)
                //acc.BI_Holding__c = Label.BI_Si; this filed now is a Workflow Update based on RecordType.
                 acc.RecordTypeId = MAP_NAME_RT.get('TGS_Customer_Country');
            else{
                //acc.BI_Holding__c = Label.BI_No; this filed now is a Workflow Update based on RecordType.
                acc.RecordTypeId = MAP_NAME_RT.get('TGS_Legal_Entity');
                acc.BI_Tipo_de_Identificador_Fiscal__c = 'RFC';
                acc.BI_No_Identificador_fiscal__c = 'VECB38032'+j+'XXX';
            }
                                      
            lst_acc.add(acc);
        }
        
        insert lst_acc;
        
        return lst_acc;
        
    }
   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that inserts x records of Contact
    
    IN:            Integer i: Number of records created
                   List<Account> lst_acc: List of related accounts
    OUT:           List<Contact>: Generated records list
    
    History:
    
    <Date>            <Author>              <Description>
    08/09/2014        Pablo Oliva           Initial version
    19/03/2015        Juan Santisi          Refactored BI_Paises_ref__c
    19/12/2016        Gawron, Julián        Add MAX_LOOP
    31/01/2017        Marta Gonzalez        REING-INI-001 - REING-FIN-001: Updating mandatory contact data.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<Contact> loadContacts(Integer i, List<Account> lst_acc){
        
        List<Contact> lst_contact = new List<Contact>();

        if(SKIP_PARAMETER && i > MAX_LOOP){  //JEG 19/12/2016
          i = MAX_LOOP;
        }
        
        for(Account item:lst_acc){
            
            for(Integer j = 0; j < i; j++){
                Contact con = new Contact(LastName = 'test'+ String.valueOf(j),
                                          AccountId = item.Id,
                                          Email = 'test'+ String.valueOf(j)+'@gmail.com',
                                          Fax = '123456789',
                                          Phone = '123456789',
                                          MobilePhone = '123456789',
                                          BI_Id_del_contacto__c = 'test'+ String.valueOf(j),
                                          BI_Country__c = item.BI_Country__c,
                                          BI_Activo__c = true,
                                          MailingCity = 'city',
                                          BI_Tipo_de_documento__c = 'Otros',
                                          BI_Numero_de_documento__c = String.valueOf(j),
                                          /*REING-INI-001*/
                                          FS_CORE_Acceso_a_Portal_CSB__c = True);
                                          /*REING-FIN-001*/
                
                lst_contact.add(con);
            }
        }
        
        insert lst_contact;
        
        return lst_contact;
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that inserts a pickup option record
    
    IN:            List<Region__c> lst_region: List of Region__c
    OUT:           BI_Pickup_Option__c: The generated record
    
    History:
    
    <Date>            <Author>              <Description>
    15/09/2014        Pablo Oliva           Initial version
    19/03/2015        Juan Santisi          Refactored BI_Paises_ref__c and Method signature List<String> 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 /*   public static BI_Pickup_Option__c loadPickUpOptions(List<String> lst_pais){
        /*
        BI_Pickup_Option__c pickupOption_region = new BI_Pickup_Option__c(Name = lst_region[0].Name,
                                                                         Refactored:JS BI_Paises_ref__c = lst_region[0].Id,
                                                                          BI_Tipo__c = 'País');
         BI_Pickup_Option__c pickupOption = new BI_Pickup_Option__c(Name = 'Test',
                                                                   BI_Parent__c = pickupOption_region.Id,
                                                                   BI_Country__c = pais,
                                                                   BI_Tipo__c = 'Tipo de Identificador Fiscal'); 
         */
     /*   String pais = lst_pais[0];
        BI_Pickup_Option__c pickupOption_region = new BI_Pickup_Option__c(Name = pais,
                                                                          BI_Country__c = pais,
                                                                          BI_Tipo__c = 'País');                                                               
        insert pickupOption_region;
        
        BI_Pickup_Option__c pickupOption = new BI_Pickup_Option__c (Name = 'RUC',BI_Parent__c = pickupOption_region.Id, BI_Tipo__c = 'Tipo de Identificador Fiscal', BI_Country__c = pais);
        insert pickupOption;
        
        
        return pickupOption;
        
    }*/
   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that inserts x records of Account with the external id
    
    IN:            Integer i: Number of records created
                   List<Region__c> lst_region: List of Region__c
                   BI_Pickup_Option__c pickupOption: Value for BI_Tipo_de_Identificador_Fiscal__c
                   Account customer: Related customer
    OUT:           List<Account>: Generated records list
    
    History:
    
    <Date>            <Author>              <Description>
    16/09/2014        Pablo Oliva           Initial version
    19/12/2016        Gawron, Julián        Add MAX_LOOP
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<Account> loadAccountsForCustomer(Account customer, List<String> lst_pais, Integer i){
            
        List<Account> lst_acc = new List<Account>();
        Integer externalId = 10;

        if(SKIP_PARAMETER && i > MAX_LOOP){  //JEG 19/12/2016
          i = MAX_LOOP;
        }

        for(Integer j = 0; j < i; j++){
            
            externalId += j;
            
            Account acc = new Account(Name = 'test'+ String.valueOf(j),
                                      BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                      BI_Activo__c = Label.BI_Si,
                                      BI_Id_del_cliente__c = String.valueOf(externalId),
                                      BI_Identificador_Externo__c = String.valueOf(externalId),
                                      BI_Country__c = lst_pais[0],
                                      BillingCountry = 'country',
                                      BillingState = 'state',
                                      BillingCity = 'city',
                                      BillingStreet = 'street',
                                      BillingPostalCode = '12345',
                                      ShippingCountry = 'country',
                                      ShippingState = 'state',
                                      ShippingCity = 'city',
                                      ShippingStreet = 'street',
                                      ShippingPostalCode = '12345',
                                      Fax = '12345',
                                      WebSite = 'www.test.com',
                                      Phone = '123456789',
                                      BI_Riesgo__c = '1 - test',
                                      //BI_Holding__c = Label.BI_No, this filed now is a Workflow Update based on RecordType.
                                      RecordTypeId = MAP_NAME_RT.get('TGS_Legal_Entity'),
                                      BI_Tipo_de_Identificador_Fiscal__c = 'RFC',                                         
                                      ParentId = customer.Id,
                                      BI_Segment__c = 'test',
                                      BI_Subsegment_Regional__c = 'test',
                                      BI_Territory__c = 'test'
                                      );
            if(j<10)
                acc.BI_No_Identificador_fiscal__c = 'VECB39039'+j+'XXX';
            else if(j>=10 && j<100)
                acc.BI_No_Identificador_fiscal__c = 'VECB9803'+j+'XXX';
            else
                acc.BI_No_Identificador_fiscal__c = 'VECB380'+j+'XXX';
                                    
            lst_acc.add(acc);
        }

        insert lst_acc;

        return lst_acc;
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that inserts x records of BI_Facturas__c with the external id
    
    IN:            Integer i: Number of records created
                   List<Region__c> lst_region: List of Region__c
                   Account acc: Related account
                   
    OUT:           List<BI_Facturas__c>: Generated list
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Pablo Oliva           Initial version
    21/03/2015        Juan Santisi          Refactored method signature List<String> lst_pais (Pass a string )
    19/12/2016        Gawron, Julián        Add MAX_LOOP
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<BI_Facturas__c> loadInvoices(Integer i, List<String> lst_pais, Account acc){
           
        List<BI_Facturas__c> lst_inv = new List<BI_Facturas__c>();
        Integer externalId = 10;

        if(SKIP_PARAMETER && i > MAX_LOOP){  //JEG 19/12/2016
          i = MAX_LOOP;
        }

        for(Integer j = 0; j < i; j++){
            
            externalId += j;
            
            BI_Facturas__c inv = new BI_Facturas__c(Name = 'test'+ String.valueOf(j),
                                                    BI_Cliente_facturacion__c = 'test',
                                                    BI_Cobrada__c = Label.BI_No,
                                                    BI_Nombre_del_cliente__c = acc.Id,
                                                    BI_Id_sistema_legado__c = String.valueOf(externalId),
                                                    BI_Tipo_de_facturacion__c = 'Test',
                                                    BI_Fecha_de_factura__c = Datetime.now(),
                                                    BI_Importe_s_tax__c = 1000,
                                                    BI_Tax__c = 0);
                                      
            lst_inv.add(inv);
        }
        
        insert lst_inv;
        
        return lst_inv;
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that inserts x records of NE__OrderItem__c for a related account
    
    IN:            Integer i: Number of records created
                   Account acc: Related account
    OUT:           List<NE__OrderItem__c>: Generated records list
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Pablo Oliva           Initial version
    19/12/2016        Gawron, Julián        Add MAX_LOOP
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<NE__Order__c> loadOrders(Integer i, Account acc, Opportunity opp){
            
        List<RecordType> lst_rt = [select Id from RecordType where SobjectType = 'NE__Order__c' AND DeveloperName = 'Order' limit 1];
        
        List<NE__Order__c> lst_orders = new List<NE__Order__c>();
        
        Integer externalId = 10;
        
        if(SKIP_PARAMETER && i > MAX_LOOP){  //JEG 19/12/2016
          i = MAX_LOOP;
        }

        for(Integer j = 0; j < i; j++){
            
            externalId += j;

            
            NE__Order__c ne_order = new NE__Order__c(RecordTypeId = lst_rt[0].Id,
                                                     BI_Id_legado__c = String.valueOf(externalId),
                                                     NE__OptyId__c = opp.Id,
                                                     NE__OrderStatus__c = 'Pending',
                                                     NE__AccountId__c = acc.Id);

            System.debug('BI_DataLoadRest: '+ ne_order);                                           
            lst_orders.add(ne_order);
        }
        
        insert lst_orders;

            
        
        return lst_orders;
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that inserts 50 records of NE__OrderItem__c for each NE__Order__c
    
    IN:            Account acc: Related account
                   NE__Order__c orderNE: Related order
    OUT:           List<NE__OrderItem__c>: Generated records list
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Pablo Oliva           Initial version
    19/12/2016        Gawron, Julián        Add MAX_LOOP
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<NE__OrderItem__c> loadOrderItem(Account acc, List<NE__Order__c> lst_order){
          
        List<NE__OrderItem__c> lst_oi = new List<NE__OrderItem__c>();

        Integer CantidadItems = 2;//JEG 19/12/2016
        if(SKIP_PARAMETER){  
          CantidadItems = MAX_LOOP;
        }
        
        for(NE__Order__c orderNE:lst_order){
            
            for(Integer i=0; i < CantidadItems; i++){
                NE__OrderItem__c oit = new NE__OrderItem__c(NE__OrderId__c = orderNE.Id,
                                                            NE__Qty__c = 2,
                                                            NE__Asset_Item_Account__c = acc.Id,
                                                            NE__Account__c = acc.Id,
                                                            NE__Status__c = 'Active');
                                                            
                lst_oi.add(oit);
            }
            
        }
        
        insert lst_oi;
        
        return lst_oi;
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva 
    Company:       Salesforce.com
    Description:   Method that creates opportunities with regions
    
    IN:            idAccount: The related account id
                   region: Related region
    OUT:           List of the opportunities created
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Pablo Oliva           Initial version
    21/03/2015        Juan Santisi          Refactored Method Signature. Use a the Country Code and lookup Pais
    19/12/2016        Gawron, Julián        Add MAX_LOOP
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<Opportunity> loadOpportunitiesWithPais(Integer i, Id idAccount, String pais, List <NE__Contract_Account_Association__c> lst_caa){
        
        List<Opportunity> lst_opp = new List<Opportunity>();

        if(SKIP_PARAMETER && i > MAX_LOOP){  //JEG 19/12/2016
          i = MAX_LOOP;
        }

        for(Integer j = 0; j < i; j++){
            Opportunity opp = new Opportunity(Name = 'Test'+j,
                                              CloseDate = Date.today(),
                                              StageName = 'F1 - Closed Won',
                                              AccountId = idAccount,
                                              BI_Ciclo_ventas__c = 'Completo',
                                              BI_Country__c = pais,
                                              BI_Presupuesto_Licitacion__c = 1,
                                              Amount = 1,
                                              NE__One_Time_Fee__c = 1,
                                              BI_Recurrente_bruto_mensual__c = 1,
                                              BI_Recurrente_bruto_mensual_anterior__c = 1,
                                              BI_Venta_Bruta_Total_FCV_estimado__c = 1,
                                              Generate_Acuerdo_Marco__c = true,
                                              BI_Requiere_contrato__c = true,
                                              BI_Ultrarapida__c = true,
                                              BI_Opportunity_Type__c = 'Producto Standard',
                                              BI_Acuerdo_Marco__c = (lst_caa != null)?lst_caa[j].Id:null);
            
            lst_opp.add(opp);
        }
        
        insert lst_opp;
        
        return lst_opp;
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva 
    Company:       Salesforce.com
    Description:   Method that creates x Case records (Technical)
    
    IN:            idAccount: The related account id
                   region: Related region
                   integer i: number of records
    OUT:           List of the tickets created
    
    History:
    
    <Date>            <Author>              <Description>
    18/09/2014        Pablo Oliva           Initial version
    15/03/2015        Juan Santisi          Refactored method signature (Integer, Id, String region) && BI_Paises_ref__c
    16/11/2016        Adrian Caro           Reduce externalId number
    19/12/2016        Gawron, Julián        Add MAX_LOOP
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<Case> loadTickets(Integer i, Id idAccount, String region){
        
        List<Case> lst_tickets = new List<Case>();
        
        List<RecordType> lst_rt = [select Id from RecordType where DeveloperName = 'BI_CasoTecnico' AND SobjectType = 'Case' limit 1];

        BI_Code_ISO__c reg = [select BI_Country_ISO_Code__c from BI_Code_ISO__c where name =:region limit 1];
        
        Integer externalId = 10;

        if(SKIP_PARAMETER && i > MAX_LOOP){  //JEG 19/12/2016
          i = MAX_LOOP;
        }
        
        for(Integer j = 0; j < i; j++){
            
            externalId += j;
            
            Case ticket = new Case(Type = 'Problem',
                                   AccountId = idAccount,
                                   Subject = 'test',
                                   Status = 'New',
                                   RecordTypeId = lst_rt[0].Id,
                                   Priority = 'Medium',
                                   Origin = 'Remedy',
                                   BI_Assigned_Group__c = 'test',
                                   BI_Assigned_Support_Company__c = 'test',
                                   BI_Categorization_Tier_1__c = 'test',
                                   BI_Categorization_Tier_2__c = 'test',
                                   BI_Categorization_Tier_3__c = 'test',
                                   BI_CI_Name__c = 'test',
                                   BI_Lookup_Keyword__c = 'test',
                                   BI_Closure_Product_Category_Tier1__c = 'test',
                                   BI_Closure_Product_Category_Tier2__c = 'test2',
                                   BI_Closure_Product_Category_Tier3__c = 'test3',
                                   BI_Product_Categorization_Tier_1__c = 'test',
                                   BI_Product_Categorization_Tier_2__c = 'test',
                                   BI_Product_Categorization_Tier_3__c = 'test',
                                   BI_Create_Request__c = 'test',
                                   BI_Company__c = 'test',
                                   BI_Resolution_Category__c = 'test',
                                   BI_Resolution_Method__c = 'test',
                                   BI_Country__c = region,
                                   BI_Id_del_caso_Legado__c = String.valueOf(externalId),
                                   BI_Otro_Tipo__c = 'test',
                                   BI_Validador_Id_de_legado__c = String.valueOf(externalId) + ' ' + reg.BI_Country_ISO_Code__c);
            
            lst_tickets.add(ticket);
        }
        
        insert lst_tickets;
        
        return lst_tickets;
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva 
    Company:       Salesforce.com
    Description:   Method that creates x BI_Comentario__c records
    
    IN:            List<Case> lst_tickets: Parent Tickets
                   List<User> lst_user: user autor
                   integer i: number of records
    OUT:           List of the comments created
    
    History:
    
    <Date>            <Author>              <Description>
    22/09/2014        Pablo Oliva           Initial version
    19/12/2016        Gawron, Julián        Add MAX_LOOP
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<BI_Comentario__c> loadComments(Integer i, List<Case> lst_tickets, List<User> lst_user){
       
        List <BI_Comentario__c> lst_com = new List<BI_Comentario__c>();
        
        Integer externalId = 10;

        if(SKIP_PARAMETER && i > MAX_LOOP){  //JEG 19/12/2016
          i = MAX_LOOP;
        }
        
        for(Integer j = 0; j < i; j++){
            
            externalId += j;
            
            BI_Comentario__c comm = new BI_Comentario__c(BI_Id_del_comentario_Legado__c = String.valueOf(externalId),
                                                         BI_Autor__c = lst_user[0].Id,
                                                         BI_Comentario__c = 'test',
                                                         BI_Caso__c = lst_tickets[0].Id);
            
            lst_com.add(comm);
            
        }
        
        insert lst_com;
        
        return lst_com;
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva 
    Company:       Salesforce.com
    Description:   Method that creates x Contract records
    
    IN:            Account acc: related account
                   integer i: number of records
    OUT:           List of the contracts created
    
    History:
    
    <Date>            <Author>              <Description>
    22/09/2014        Pablo Oliva           Initial version
    19/12/2016        Gawron, Julián        Add MAX_LOOP
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<Contract> loadContracts(Integer i, Account acc, List<Opportunity> lst_opp){
       
        List <Contract> lst_con = new List<Contract>();

        if(SKIP_PARAMETER && i > MAX_LOOP){  //JEG 19/12/2016
          i = MAX_LOOP;
        }
        
        for(Integer j = 0; j < i; j++){
            
            Contract contract = new Contract(AccountId = acc.Id,
                                             Status = 'Draft',
                                             BI_Oportunidad__c = lst_opp[j].Id,
                                             OwnerId = acc.OwnerId);
            
            lst_con.add(contract);
            
        }
        
        insert lst_con;
        
        return lst_con;
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva 
    Company:       Salesforce.com
    Description:   Method that creates x NE__Contract__c records
    
    IN:            Account acc: related account
                   integer i: number of records
    OUT:           List with the contract created
    
    History:
    
    <Date>            <Author>              <Description>
    13/10/2014        Pablo Oliva           Initial version
    19/12/2016        Gawron, Julián        Add MAX_LOOP
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<NE__Contract__c> loadContractsNE(Integer i, Account acc){
       
        List <NE__Contract__c> lst_con = new List<NE__Contract__c>();
        List <NE__Contract_Header__c> lst_head = new List<NE__Contract_Header__c>();
        List <NE__Product__c> lst_prod = new List<NE__Product__c>();
        List <NE__Contract_Line_Item__c> lst_cli = new List<NE__Contract_Line_Item__c>();

        if(SKIP_PARAMETER && i > MAX_LOOP){  //JEG 19/12/2016
          i = MAX_LOOP;
        }
        
        for(Integer j=0; j<i; j++){
            NE__Contract_Header__c header = new NE__Contract_Header__c(NE__Actual_Qty__c = 3,
                                                                       NE__Delta_Qty__c = 2,
                                                                       NE__Name__c ='Header'+String.valueOf(j));
                                                                       
            lst_head.add(header);
            
            NE__Product__c prod = new NE__Product__c(Name = 'Prod');
       
            lst_prod.add(prod);
            
        }
        
        insert lst_head;
        
        insert lst_prod;
        
        for(Integer k=0; k<i; k++){
            NE__Contract__c contract = new NE__Contract__c(NE__Contract_Header__c = lst_head[k].Id,
                                                           NE__Status__c = 'Active');
           
            lst_con.add(contract);
            
        }
            
        insert lst_con;
        
        for(Integer l=0; l<i; l++){
            
            NE__Contract_Line_Item__c line = new NE__Contract_Line_Item__c(NE__Contract__c = lst_con[l].Id,
                                                                           NE__Commercial_Product__c = lst_prod[l].Id,
                                                                           NE__Allow_Qty_Override__c = false);
            
            lst_cli.add(line);
            
        }
        
        insert lst_cli;
        
        return lst_con;
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva 
    Company:       Salesforce.com
    Description:   Method that creates x NE__Contract__c records
    
    IN:            Account acc: related account
                   List<NE__Contract_Header__c> related NE__Contract_Header__c records
    OUT:           List with the contract account association created
    
    History:
    
    <Date>            <Author>              <Description>
    13/10/2014        Pablo Oliva           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<NE__Contract_Account_Association__c> loadContractAccountAssociations(Account acc, List<NE__Contract__c> lst_con){
       
        List <NE__Contract_Account_Association__c> lst_caa = new List<NE__Contract_Account_Association__c>();
        
        for(NE__Contract__c con: lst_con){
            
            NE__Contract_Account_Association__c caa = new NE__Contract_Account_Association__c(NE__Account__c = acc.Id,
                                                                                              NE__Contract_Header__c = con.NE__Contract_Header__c);
       
            lst_caa.add(caa);
            
        }
        
        insert lst_caa;
        
        return lst_caa;
        
    }
 
}