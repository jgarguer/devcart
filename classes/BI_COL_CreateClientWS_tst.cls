/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-04-07      Daniel ALexander Lopez (DL)     Cloned Test Class      
* @version   1.0    2015-04-07      GSPM                            Test Class edited
*            1.1    23-02-2017      Jaime Regidor                   Adding Campaign Values, Adding Catalog Country
                    20/09/2017      Angel F. Santaliestra           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/

@isTest
global class BI_COL_CreateClientWS_tst {
    
  Public static User                                    objUsuario;
  public static Account                                 objCuenta;
  public static Account                                 objCuenta2;
  public static Contact                                 objContacto;
  public static Campaign                                objCampana;
  public static Contract                                objContrato;
  public static BI_COL_Anexos__c                        objAnexos;
  public static Opportunity                             objOportunidad;
  public static BI_Col_Ciudades__c                      objCiudad;
  public static BI_Sede__c                              objSede;
  public static BI_Punto_de_instalacion__c                objPuntosInsta;
  public static BI_COL_Descripcion_de_servicio__c         objDesSer;
  public static BI_COL_Modificacion_de_Servicio__c        objMS;
  public static BI_COL_Generate_OT_ctr.WrapperMS          objWrapperMS;
  public static BI_Log__c objlog;
  
  public static List <Profile>              lstPerfil;
  public static List <User>               lstUsuarios;
  public static List <UserRole>               lstRoles;
  public static List<Campaign>              lstCampana;
  public static List<BI_COL_manage_cons__c >        lstManege;
  public static List<BI_COL_Modificacion_de_Servicio__c>  lstMS;
  public static List<BI_COL_Descripcion_de_servicio__c> lstDS;
  public static List<recordType>              lstRecTyp;
  public static List<BI_COL_Generate_OT_ctr.WrapperMS>    lstWrapperMS;
  public static List<BI_COL_Anexos__c>            lstAnexos;
  public static list<Account> lstcuentas;
  
  public static ID idAccountDeleted;

    public static void CrearData() 
  {

    ////perfiles
    //lstPerfil                   = new List <Profile>();
    //lstPerfil                   = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1 ];
    //system.debug('datos Perfil '+lstPerfil);
        
    ////usuarios
    ////lstUsuarios = [Select Id FROM User where Pais__c = 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

    ////Roles
    //  lstRoles                = new List <UserRole>();
    //  lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
    //  system.debug('datos Rol '+lstRoles);

    ////ObjUsuario
    //    objUsuario = new User();
    //    objUsuario.Alias = 'standt';
    //    objUsuario.Email ='pruebas@test.com';
    //    objUsuario.EmailEncodingKey = '';
    //    objUsuario.LastName ='Testing';
    //    objUsuario.LanguageLocaleKey ='en_US';
    //    objUsuario.LocaleSidKey ='en_US'; 
    //    objUsuario.ProfileId = lstPerfil.get(0).Id;
    //    objUsuario.TimeZoneSidKey ='America/Los_Angeles';
    //    objUsuario.UserName ='pruebas@test.com';
    //    objUsuario.EmailEncodingKey ='UTF-8';
    //    objUsuario.UserRoleId = lstRoles.get(0).Id;
    //    objUsuario.BI_Permisos__c ='Sucursales';
    //    objUsuario.Pais__c='Colombia';
    //    insert objUsuario;
        
    //    lstUsuarios = new list<User>();
    //    lstUsuarios.add(objUsuario);

    //System.runAs(lstUsuarios[0])
    //{ 
      BI_bypass__c objBibypass = new BI_bypass__c();
      objBibypass.BI_migration__c=true;
      insert objBibypass; 
      
      //Roles
      lstRoles                = new List <UserRole>();
      lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
      system.debug('datos Rol '+lstRoles);
      
      //Cuentas
      objCuenta                     = new Account();      
      objCuenta.Name                                  = 'prueba';
      objCuenta.BI_Country__c                         = 'Colombia';
      objCuenta.TGS_Region__c                         = 'América';
      objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
      objCuenta.CurrencyIsoCode                       = 'COP';
      objCuenta.BI_Fraude__c                          = false;
      objCuenta.AccountNumber             = '123456';
      objCuenta.BI_No_Identificador_fiscal__c     = '1234566';
      objCuenta.Phone                 = '1233421';
      objCuenta.BI_Segment__c                         = 'test';
      objCuenta.BI_Subsegment_Regional__c             = 'test';
      objCuenta.BI_Territory__c                       = 'test';
      insert objCuenta;
      system.debug('datos Cuenta '+objCuenta);
      
      Account objCuentaEliminar = new Account();      
      objCuentaEliminar.Name = 'prueba2';
      objCuentaEliminar.BI_Country__c = 'Colombia';
      objCuentaEliminar.TGS_Region__c = 'América';
      objCuentaEliminar.BI_Tipo_de_identificador_fiscal__c = '';
      objCuentaEliminar.CurrencyIsoCode = 'COP';
      objCuentaEliminar.BI_Fraude__c = false;
      objCuentaEliminar.AccountNumber = '654321';
      objCuentaEliminar.BI_No_Identificador_fiscal__c = '6654321';
      objCuentaEliminar.Phone = '1233421';
      objCuenta.BI_Segment__c                         = 'test';
      objCuenta.BI_Subsegment_Regional__c             = 'test';
      objCuenta.BI_Territory__c                       = 'test';
      insert objCuentaEliminar;
      
      idAccountDeleted = objCuentaEliminar.Id;
      
      delete objCuentaEliminar;

      //Ciudad
      objCiudad = new BI_Col_Ciudades__c ();
      objCiudad.Name            = 'Test City';
      objCiudad.BI_COL_Pais__c      = 'Test Country';
      objCiudad.BI_COL_Codigo_DANE__c   = 'TestCDa';
      insert objCiudad;
      System.debug('Datos Ciudad ===> '+objSede);
      
      //Contactos
      objContacto                                     = new Contact();
      objContacto.LastName                          = 'Test';
      objContacto.BI_Country__c                     = 'Colombia';
      objContacto.CurrencyIsoCode                   = 'COP'; 
      objContacto.AccountId                         = objCuenta.Id;
      objContacto.BI_Tipo_de_contacto__c        = 'Administrador Canal Online';
      objContacto.Phone                 = '1234567';
      objContacto.MobilePhone             = '3104785925';
      objContacto.Email               = 'pruebas@pruebas1.com';
      objContacto.BI_COL_Direccion_oficina__c     = 'Dirprueba';
      objContacto.BI_COL_Ciudad_Depto_contacto__c   = objCiudad.Id;
      //REING-INI
      //objContacto.BI_Tipo_de_contacto__c          = 'Autorizado';
      objContacto.BI_Tipo_de_documento__c 			= 'Otros';
      objContacto.BI_Numero_de_documento__c 			= '00000000X';
	  objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
      objContacto.BI_Tipo_de_contacto__c          = 'General';
      objContacto.BI_Representante_legal__c  = true;
      objContacto.FS_CORE_Referencias_Funcionales__c         = 'Administrativo';
      //REING_FIN

      Insert objContacto;
      
      //catalogo
      NE__Catalog__c objCatalogo            = new NE__Catalog__c();
      objCatalogo.Name                      = 'prueba Catalogo';
      objCatalogo.BI_country__c             = 'Colombia';
      insert objCatalogo; 
      
      //Campaña
      objCampana                    = new Campaign();
      objCampana.Name                 = 'Campaña Prueba';
      objCampana.BI_Catalogo__c             = objCatalogo.Id;
      objCampana.BI_COL_Codigo_campana__c       = 'prueba1';
      objCampana.BI_Country__c                        = 'Colombia';
      objCampana.BI_Opportunity_Type__c               = 'Fijo';
      objCampana.BI_SIMP_Opportunity_Type__c          = 'Alta';
      insert objCampana;
      lstCampana                    = new List<Campaign>();
      lstCampana                    = [select Id, Name from Campaign where Id =: objCampana.Id];
      system.debug('datos Cuenta '+lstCampana);
      
      //Tipo de registro
      lstRecTyp                     = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
      system.debug('datos de FUN '+lstRecTyp);
      
      //Contrato
      objContrato                   = new Contract ();
      objContrato.AccountId               = objCuenta.Id;
      objContrato.StartDate               =  System.today().addDays(+5);
      objContrato.BI_COL_Formato_Tipo__c        = 'Condiciones Generales';
      objContrato.BI_COL_Presupuesto_contrato__c    = 1300000;
      objContrato.StartDate             = System.today().addDays(+5);
      objContrato.BI_Indefinido__c          = 'No';
      objContrato.BI_COL_Monto_ejecutado__c     = 5;
      objContrato.ContractTerm            = 12;
      objContrato.BI_COL_Duracion_Indefinida__c   = true;
      objContrato.Name                = 'prueba';
      objContrato.BI_COL_Cuantia_Indeterminada__c   = true;
    
      insert objContrato;
      Contract objCont = [select ContractNumber, BI_COL_Saldo_contrato__c from Contract limit 1];

      System.debug('datos Contratos ===>'+objContrato);
      
      //FUN
      objAnexos                           = new BI_COL_Anexos__c();
      objAnexos.Name                      = 'FUN-0041414';
      objAnexos.RecordTypeId              = lstRecTyp[0].Id;
      objAnexos.BI_COL_Contrato__c            = objContrato.Id;
      objAnexos.BI_COL_Estado__c            = 'Activo';

      insert objAnexos;
      System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
      
      //Configuracion Personalizada
      BI_COL_manage_cons__c objConPer         = new BI_COL_manage_cons__c();
      objConPer.Name                          = 'Viabilidades';
      objConPer.BI_COL_Numero_Viabilidad__c       = 46;
      objConPer.BI_COL_ConsProyecto__c          = 1;
      objConPer.BI_COL_ValorConsecutivo__c        =1;
      lstManege                               = new List<BI_COL_manage_cons__c >();        
      lstManege.add(objConPer);
      insert lstManege;
      System.debug('======= configuracion personalizada ======= '+lstManege);
      
      //Oportunidad
      objOportunidad                                        = new Opportunity();
      objOportunidad.Name                                     = 'prueba opp';
      objOportunidad.AccountId                                = objCuenta.Id;
      objOportunidad.BI_Country__c                            = 'Colombia';
      objOportunidad.CloseDate                                = System.today().addDays(+5);
      objOportunidad.StageName                                = 'F5 - Solution Definition';
      objOportunidad.CurrencyIsoCode                          = 'COP';
      objOportunidad.Certa_SCP__contract_duration_months__c   = 12;
      objOportunidad.BI_Plazo_estimado_de_provision_dias__c   = 0 ;
      insert objOportunidad;
      system.debug('Datos Oportunidad'+objOportunidad);
      List<Opportunity> lstOportunidad  = new List<Opportunity>();
      lstOportunidad            = [select Id from Opportunity where Id =: objOportunidad.Id];
      system.debug('datos ListaOportunida '+lstOportunidad);
      
      
      //Direccion
      objSede                 = new BI_Sede__c();
      objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
      objSede.BI_Direccion__c         = 'Test Street 123 Number 321';
      objSede.BI_Localidad__c         = 'Test Local';
      objSede.BI_COL_Estado_callejero__c    = System.Label.BI_COL_LbValor3EstadoCallejero;
      objSede.BI_COL_Sucursal_en_uso__c     = 'Libre';
      objSede.BI_Country__c           = 'Colombia';
      objSede.Name              = 'Test Street 123 Number 321, Test Local Colombia';
      objSede.BI_Codigo_postal__c       = '12356';
      insert objSede;
      System.debug('Datos Sedes ===> '+objSede);
      
      //Sede
      objPuntosInsta            = new BI_Punto_de_instalacion__c ();
      objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
      objPuntosInsta.BI_Sede__c          = objSede.Id;
      objPuntosInsta.BI_Contacto__c      = objContacto.id;
      objPuntosInsta.Name                 ='Prueba test';
      insert objPuntosInsta;
      System.debug('Datos Sucursales ===> '+objPuntosInsta);
      
      //DS
      objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
      objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
      objDesSer.CurrencyIsoCode                     = 'COP';
      insert objDesSer;
      System.debug('Data DS ====> '+ objDesSer);
      System.debug('Data DS ====> '+ objDesSer.Name);
      lstDS   = [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
      System.debug('Data lista DS ====> '+ lstDS);
      
      //MS
      objMS                                 = new BI_COL_Modificacion_de_Servicio__c();
      objMS.BI_COL_FUN__c                   = objAnexos.Id;
      objMS.BI_COL_Codigo_unico_servicio__c     = objDesSer.Id;
      objMS.BI_COL_Oportunidad__c         = objOportunidad.Id;
      objMS.BI_COL_Bloqueado__c           = false;
      objMS.BI_COL_Estado__c            = 'Activa';//label.BI_COL_lblActiva;
      objMS.BI_COL_Sucursal_de_Facturacion__c   = objPuntosInsta.Id;
      objMs.BI_COL_Sucursal_Origen__c       = objPuntosInsta.Id;
      objMs.BI_COL_Medio_Preferido__c       = 'Cobre';
      objMS.BI_COL_Clasificacion_Servicio__c    = 'Alta demo';
      objMS.BI_COL_TRM__c             = 5;
      objMS.BI_COL_Oportunidad__c         = objOportunidad.id;
      insert objMS;
      system.debug('Data objMs ===>'+objMs);
      BI_COL_Modificacion_de_Servicio__c objr = [select Id, BI_COL_Monto_ejecutado__c from BI_COL_Modificacion_de_Servicio__c where id =: objMS.id];
      system.debug('\n@-->Data consulta ===>'+objr);  
    
    //}
  }



    @isTest static void myUnitTest(){
      objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
          crearData();
         Test.startTest();        
          ws_wwwTelefonicaComNotificacionessalesfo.respuesta resp      = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta();
          resp.descripcionError='descripcion error..';
          resp.idError='0';
          ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] lstResp = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta[]{resp};
          resp.idError='1';
          lstResp = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta[]{resp};
          Test.setMock(WebServiceMock.class, new BI_COL_CreateContactWS_mws());
          BI_COL_CreateClientWS_cls.crearClienteTelefonica(new List<Id>{objCuenta.id}, 'Insertar');
          
          Test.stopTest();
        }
    }
    
    static testMethod void myUnitTest2()
  {
    objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
          crearData();
          Test.startTest();        
            ws_wwwTelefonicaComNotificacionessalesfo.respuesta resp      = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta();
            resp.descripcionError='descripcion error..';
            resp.idError='0';
            ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] lstResp = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta[]{resp};
            resp.idError='1';
            lstResp = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta[]{resp};
            Test.setMock(WebServiceMock.class, new BI_COL_CreateContactWS_mws());
            BI_COL_CreateClientWS_cls.crearClienteTelefonica(new List<Id>{ objCuenta.id }, 'Eliminar');        
            BI_COL_CreateClientWS_cls.creaLogTransaccionError( 'Error prueba', new List<Account>{ objCuenta }, 'Inf Enviada' );
          Test.stopTest();
        }
  }

     global class MockResponseGenerator1 implements WebServiceMock {
      
      global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) 
      {    
       
        ws_wwwTelefonicaComNotificacionessalesfo.respuesta resp      = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta();

        response.put( 'response_x', resp);
      }
    }
    
}