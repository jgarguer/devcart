/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro Sevilla Durango
    Company:       NEAborda
    Description:   Test Class for to cover and to test the class named BI_FVI_CasosContratosFVI
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    02/06/2016          Alvaro Sevilla Durango       Initial Version     
    20/09/2017          Angel F. Santaliestra       Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_FVI_CasosContratosFVI_TEST {
	
	@isTest static void BI_FVI_CrearCasosInternos_TEST() {

		Test.startTest();
		BI_TestUtils.throw_exception = false;

		List<Contract> lstContrato = new List<Contract>();
		RecordType TR = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_FVI_Contrato'];

		Account objCuenta 								= new Account();
		objCuenta.Name                                  = 'prueba2';
		objCuenta.BI_Country__c                         = 'Colombia';
		objCuenta.TGS_Region__c                         = 'América';
		objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta.CurrencyIsoCode                       = 'COP';
		objCuenta.BI_Fraude__c                          = false;
		objCuenta.BI_Segment__c                         = 'test';
		objCuenta.BI_Subsegment_Regional__c             = 'test';
		objCuenta.BI_Territory__c                       = 'test';
		insert objCuenta;


        NE__Lov__c lov1 = new NE__Lov__c();
        lov1.Name = 'N_DIAS_BARBECHO_SALTO_TEMPORAL';
        lov1.NE__Active__c = true;
        lov1.NE__Type__c = 'ENCUESTA ISC';
        lov1.NE__Value1__c = 'https://telefonicab2b--canindev--c.cs86.visual.force.com/apex/TakeSurvey?id=a147E0000007GGNQA2&';
        lov1.Country__c = 'Peru';
        Insert lov1;
		
		Contract objContrato = new Contract();
		objContrato.RecordTypeId = TR.Id;
		objContrato.AccountId = objCuenta.Id;
		objContrato.Status = 'Draft';
		objContrato.BI_FVI_Actos_Comerciales__c = 5;
		lstContrato.add(objContrato);
		insert lstContrato;

		lstContrato[0].Status = 'Completed Pending Sign';
		update lstContrato;
		lstContrato[0].Status = 'Presented Signed';
		update lstContrato;
		lstContrato[0].Status = 'Signed Activated';
		update lstContrato;
		Test.stopTest();

	}
	
	@isTest static void Excepciones_TEST() {

		Test.startTest();
		BI_TestUtils.throw_exception = true;

		List<Contract> lstContrato = new List<Contract>();
		RecordType TR = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_FVI_Contrato'];

		Account objCuenta 								= new Account();
		objCuenta.Name                                  = 'prueba2';
		objCuenta.BI_Country__c                         = 'Colombia';
		objCuenta.TGS_Region__c                         = 'América';
		objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta.CurrencyIsoCode                       = 'COP';
		objCuenta.BI_Fraude__c                          = false;
		objCuenta.BI_Segment__c                         = 'test';
		objCuenta.BI_Subsegment_Regional__c             = 'test';
		objCuenta.BI_Territory__c                       = 'test';
		insert objCuenta;
		
		Contract objContrato = new Contract();
		objContrato.RecordTypeId = TR.Id;
		objContrato.AccountId = objCuenta.Id;
		objContrato.Status = 'Draft';
		objContrato.BI_FVI_Actos_Comerciales__c = 5;
		lstContrato.add(objContrato);
		insert lstContrato;
		BI_FVI_CasosContratosFVI.BI_FVI_CrearCasosInternos(lstContrato, lstContrato);

		Test.stopTest();

	}
	
}