/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CWP_pageHeaderController_TEST {
    
         @testSetup 
    private static void dataModelSetup() {          
       
        //USER
        UserRole rolUsuario;
        User unUsuario;
        User otroUsuario;
        Profile profileTGS;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            profileTGS = CWP_TestDataFactory.getProfile('TGS System Administrator');
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, profileTGS.Id);
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
            //ACCOUNT
            map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
        
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        Contact contactTest1 = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest1');
        
        contactTest.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest;
        contactTest1.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest1;
        
        User usuario;
        User usuarioNoTGS;
              
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');       
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
            
        }
        
     
     }
    
    @isTest static void CWP_doInit() {
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        
        System.runAs(usuario){
        Map<String, String> selectOptionList = CWP_pageHeaderController.CWP_doInit();
        System.assert(selectOptionList != null);
        System.assert(selectOptionList.size() > 0);
        }
    }
    
    @isTest static void redirectionChatter() {
        User usuario = [SELECT id, Contact.BI_Cuenta_activa_en_portal__c FROM User WHERE FirstName =: 'nombre1'];  
        BI_TestUtils.throw_exception=false; 

        BI_Contact_Customer_Portal__c ccportal = new BI_Contact_Customer_Portal__c();
        ccportal.BI_User__c = usuario.Id;
        ccportal.BI_Cliente__c = usuario.Contact.BI_Cuenta_activa_en_portal__c;
        insert ccportal;
        System.runAs(usuario){
            Boolean responseChatter = CWP_pageHeaderController.redirectToChatter();
            System.assert(responseChatter != null);
        }
    }
    
    @isTest static void getPermissionSet() {
        User usuario = [SELECT id, Contact.BI_Cuenta_activa_en_portal__c FROM User WHERE FirstName =: 'nombre1'];  
        BI_TestUtils.throw_exception=false; 

        BI_Contact_Customer_Portal__c ccportal = new BI_Contact_Customer_Portal__c();
        ccportal.BI_User__c = usuario.Id;
        ccportal.BI_Cliente__c = usuario.Contact.BI_Cuenta_activa_en_portal__c;
        insert ccportal;
        System.runAs(usuario){
            Map<String, String> selectOptionList = CWP_pageHeaderController.getPermissionSet();
            System.assert(selectOptionList != null);
            System.assert(selectOptionList.size() > 0);
        }
    }
    
    @isTest static void updateUser() {
        User usuario = [SELECT id,Contact.BI_Cuenta_activa_en_portal__c, AccountId FROM User WHERE FirstName =: 'nombre1'];  
        
        System.runAs(usuario){
            CWP_pageHeaderController.updateUser(usuario.Contact.BI_Cuenta_activa_en_portal__c);
        }
    }
}