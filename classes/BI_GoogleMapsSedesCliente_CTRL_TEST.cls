@isTest
private class BI_GoogleMapsSedesCliente_CTRL_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_GoogleMapsSedesCliente_CTRL class 
    
    History: 
    
    <Date> 					<Author> 				<Change Description>
    17/06/2014				Pablo Oliva				Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Pablo Oliva
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_GoogleMapsSedesCliente_CTRL
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	17/06/2014				Pablo Oliva				Initial version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void BI_GoogleMapsSedesCliente_CTRLTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
		List <Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);
		
		String testStr = 'Test';
		
		BI_Sede__c headquarters = new BI_Sede__c(BI_Codigo_postal__c = '11111',		 
										 BI_Direccion__c = testStr,		 
										 BI_ID_de_la_sede__c = '1',	 
										 BI_Localidad__c = testStr,		 
										 BI_Provincia__c = testStr);
										 
		insert headquarters;
		
		BI_Punto_de_instalacion__c installPoint = new BI_Punto_de_instalacion__c(BI_Cliente__c = lst_acc[0].Id,
																	   BI_Sede__c = headquarters.Id);
																	   
		insert installPoint;
		
		Test.startTest();
		
		ApexPages.Standardcontroller ctr = new ApexPages.Standardcontroller(lst_acc[0]);
		BI_GoogleMapsSedesCliente_CTRL gmsc = new BI_GoogleMapsSedesCliente_CTRL(ctr);
		
		system.assert(!gmsc.lst_address.isEmpty());
		
		Test.stopTest();
        
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}