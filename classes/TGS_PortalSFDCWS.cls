/*------------------------------------------------------------
Author:         Ana Cirac 
Company:        Deloitte
Description:    The Portal gets Information about Accounts              
History
<Date>         <Author>             <Change Description>
12-feb-2015    Ana Cirac            Initial Version
31-mar-2015    Alejandro García     Addition of the TGS Prefix
26-may-2017    Javier Martínez      Se cambia el método getProfiles() para que sólo devuelva el perfil TGS Customer Community ya que los otros no tiene disponibilidad de licencias.
17-oct-2017    Guillermo Muñoz      Added Multitenant variable in Company structure
20-feb-2018		Antonio Pardo		Added TIWS_Gestion_TIWS__c field to the query
------------------------------------------------------------*/

global with sharing class TGS_PortalSFDCWS {


   global class Company{ 
    
        webservice String NameCompany;
        webservice String IdCompany;
        webservice String Abbreviature;
        webservice String SigmaId;
        webservice Boolean Multitenant;
        webservice Boolean isTIWS;
      	webservice String identificadorExterno;
    }
        
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    The Portal gets companies           
    History
    <Date>          <Author>        <Change Description>
    12-feb-2015     Ana Cirac        Initial Version
    17-oct-2017     Guillermo Muñoz     Added TGS_Multitenant__c field to the query
	20-feb-2018		Antonio Pardo		Added TIWS_Gestion_TIWS__c field to the query
	16-mar-2018		Antonio Pardo		Added BI_Identificador_Externo__c field to the query
    ------------------------------------------------------------*/
    webservice static List<Company> getCompanies(){
        
        List<Company> listResult = new List<Company>();
        
        List<Account> accounts = [SELECT Id, RecordType.DeveloperName, Name, TGS_IntegrationID__c, TGS_Abbreviation__c, 
                                    TGS_Portal_Sigma_WH_ID__c, TGS_Es_MNC__c, TGS_Multitenant__c, TIWS_Gestion_TIWS__c, BI_Identificador_Externo__c
                                    FROM Account 
                                    WHERE RecordType.DeveloperName='TGS_Holding' AND (TGS_Es_MNC__c=true OR TIWS_Gestion_TIWS__c=true)AND BI_Activo__c = 'Sí'];
        for (Account account: accounts) {
           
            Company result = new Company();
            result.NameCompany = account.Name;
            result.IdCompany = account.TGS_IntegrationID__c;
            result.Abbreviature = account.TGS_Abbreviation__c;
            result.SigmaId = account.TGS_Portal_Sigma_WH_ID__c;
            result.Multitenant = account.TGS_Multitenant__c;
            result.isTIWS = account.TIWS_Gestion_TIWS__c;
            result.identificadorExterno = account.BI_Identificador_Externo__c;
            
            listResult.add(result);
               
        }
        
        return listResult;
    }
    
    global class Country{ 
    
        webservice String NameCountry;
        webservice String IdCountry;
        webservice String IdCompany;
        webservice String identificadorExterno;
        
    }
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    The Portal gets Legal Entities          
    History
    <Date>          <Author>        <Change Description>
    12-feb-2015     Ana Cirac        Initial Version
	20-feb-2018		Antonio Pardo		Added TIWS_Gestion_TIWS__c field to the query
	16-mar-2018		Antonio Pardo		Added BI_Identificador_Externo__c field to the query
    ------------------------------------------------------------*/
    webservice static List<Country> getCountries(){
        
        List<Country> listResult = new List<Country>();
        List<Account> accounts = [SELECT RecordType.DeveloperName, BI_Identificador_Externo__c ,Name, BI_Country__c, TGS_ISD_Code__c, Parent.Name, TGS_Es_MNC__c FROM Account WHERE RecordType.DeveloperName = 'TGS_Customer_Country' and (TGS_Es_MNC__c=true OR TIWS_Gestion_TIWS__c=true) AND BI_Activo__c = 'Sí'];
        
        for (Account account: accounts) 
        {
            Country result = new Country();
            result.NameCountry = account.BI_Country__c;
            result.IdCompany = account.Parent.Name;
            result.IdCountry = account.TGS_ISD_Code__c;
            result.identificadorExterno = account.BI_Identificador_Externo__c;
            
            listResult.add(result);                
        }
        
        return listResult;
    }
    
    global class LegalEntity {     
    
        webservice String NameLE;
        webservice String IdLE;
        webservice String IdCompany;
        webservice String Country;
        webservice String identificadorExterno;
    }
    
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    The Portal gets Legal Entities          
    History
    <Date>          <Author>        <Change Description>
    12-feb-2015     Ana Cirac        Initial Version
    18-feb-2016     Miguel Cabrera   Where condition added for active LE only
	20-feb-2018		Antonio Pardo		Added TIWS_Gestion_TIWS__c field to the query
    2-mar-2018      Antonio Pardo   Added BI_Identificador_Externo__c
    ------------------------------------------------------------*/
    webservice static List<LegalEntity> getLegalEntities(){
        
        List<LegalEntity> listResult = new List<LegalEntity>();
        List<Account> accounts = [SELECT Id,BI_Identificador_Externo__c, RecordType.DeveloperName, Name, TGS_IntegrationID__c, Parent.Name, Parent.BI_Country__c, 
                                    Parent.TGS_ISD_Code__c,  Parent.Parent.Name, TGS_Es_MNC__c
                                    FROM Account 
                                    WHERE RecordType.DeveloperName='TGS_Legal_Entity' AND (TGS_Es_MNC__c=true OR TIWS_Gestion_TIWS__c=true) AND BI_Activo__c = 'Sí'];
        
        for (Account account: accounts){
            
            LegalEntity result = new LegalEntity();
            result.NameLE = account.Name;
            result.IdLE = account.TGS_IntegrationID__c;
            result.IdCompany = account.Parent.Parent.Name;
            result.Country = account.Parent.BI_Country__c;
            result.identificadorExterno = account.BI_Identificador_Externo__c;
            
            listResult.add(result);
               
        }
        
        return listResult;
    }
    
    global class BusinessUnit{ 
    
        webservice String NameBU;
        webservice String IdBU;
        webservice String IdCompany;
        webservice String IdLE;
        
    }
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    The Portal gets Business Units          
    History
    <Date>          <Author>        <Change Description>
    12-feb-2015     Ana Cirac        Initial Version
	20-feb-2018		Antonio Pardo		Added TIWS_Gestion_TIWS__c field to the query
    ------------------------------------------------------------*/
    webservice static List<BusinessUnit> getBusinessUnits(){
        
        List<BusinessUnit> listResult = new List<BusinessUnit>();
        
        List<Account> accounts = [SELECT Id, RecordType.DeveloperName, Name, TGS_IntegrationID__c, Parent.TGS_IntegrationID__c, 
                                    Parent.Parent.Parent.TGS_IntegrationID__c, Parent.Parent.Parent.Name, TGS_Es_MNC__c 
                                    FROM Account 
                                    WHERE RecordType.DeveloperName='TGS_Business_Unit' AND (TGS_Es_MNC__c=true OR TIWS_Gestion_TIWS__c=true) AND BI_Activo__c = 'Sí'];
        for (Account account: accounts) {
           
            BusinessUnit result = new BusinessUnit();
            result.NameBU = account.Name;
            result.IdBU = account.TGS_IntegrationID__c;
            //result.IdCompany = account.Parent.Parent.Parent.TGS_IntegrationID__c;
            result.IdCompany = account.Parent.Parent.Parent.Name;
            result.IdLE = account.Parent.TGS_IntegrationID__c;
            
            listResult.add(result);
               
        }
        
        return listResult;
    }
    
    global class CostCenter{ 
    
        webservice String NameCC;
        webservice String IdCC;
        webservice String IdCompany;
        webservice String IdBU;
        
    }
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    The Portal gets Cost Centers            
    History
    <Date>          <Author>        <Change Description>
    12-feb-2015     Ana Cirac        Initial Version
	20-feb-2018		Antonio Pardo		Added TIWS_Gestion_TIWS__c field to the query
    ------------------------------------------------------------*/
    webservice static List<CostCenter> getCostCenters(){
        
        List<CostCenter> listResult = new List<CostCenter>();
        
        List<Account> accounts = [SELECT Id, RecordType.DeveloperName, Name, TGS_IntegrationID__c, Parent.TGS_IntegrationID__c, 
                                    Parent.Parent.Parent.Parent.TGS_IntegrationID__c, Parent.Parent.Parent.Parent.Name,TGS_Es_MNC__c 
                                    FROM Account 
                                    WHERE RecordType.DeveloperName='TGS_Cost_Center' AND (TGS_Es_MNC__c=true OR TIWS_Gestion_TIWS__c=true) AND BI_Activo__c = 'Sí'];
        for (Account account: accounts) {
           
            CostCenter result = new CostCenter();
            result.NameCC = account.Name;
            result.IdCC = account.TGS_IntegrationID__c;
            result.IdCompany = account.Parent.Parent.Parent.Parent.Name;
            result.IdBU = account.Parent.TGS_IntegrationID__c;
            
            listResult.add(result);
               
        }
        
        return listResult;
    }
    
    global class Sites{ 
    
        webservice String NameSite;
        webservice String IdSite;
        webservice String IdCompany;
        webservice String IdBU;
        webservice String NameLE;
        webservice String NameCountry;
        
    }
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    The Portal gets Sites           
    History
    <Date>          <Author>        <Change Description>
    12-feb-2015     Ana Cirac        Initial Version
	20-feb-2018		Antonio Pardo		Added TIWS_Gestion_TIWS__c field to the query
    ------------------------------------------------------------*/
    webservice static List<Sites> getSites(){
        
        List<Sites> listResult = new List<Sites>();
        
        List<BI_Punto_de_Instalacion__c> sites = [SELECT Id, Name, TEMPEXT_ID__c, BI_Cliente__r.TGS_IntegrationID__c, BI_Cliente__r.Parent.Parent.Parent.Name, BI_Cliente__r.Parent.Name, BI_Cliente__r.Parent.Parent.BI_Country__c FROM BI_Punto_de_Instalacion__c WHERE (BI_Cliente__r.TGS_Es_MNC__c=true OR BI_Cliente__r.TIWS_Gestion_TIWS__c=true)];
        for (BI_Punto_de_Instalacion__c site: sites){
            Sites result = new Sites();
            result.NameSite = site.Name;
            result.IdSite = site.TEMPEXT_ID__c;       
            result.IdCompany = site.BI_Cliente__r.Parent.Parent.Parent.Name;
            result.IdBU = site.BI_Cliente__r.TGS_IntegrationID__c;
            result.NameLE = site.BI_Cliente__r.Parent.Name;
            result.NameCountry = site.BI_Cliente__r.Parent.Parent.BI_Country__c;
            listResult.add(result);
        }
        
        return listResult;
    }
    
    global class Profiles{ 
    
        webservice String NameProfile;
        webservice String IdSFProfile;
        webservice String UserType;
        
    }
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    The Portal gets Profiles whose userTypes are "SD" 
                    or "AU"
    History
    <Date>          <Author>        <Change Description>
    12-feb-2015     Ana Cirac        Initial Version
    18-feb-2016     Miguel Cabrera   AU Profile 'TGS_Customer_community_plus_login' added 
	20-feb-2018		Antonio Pardo	AU Profile 'TGS Carrier Customer Community' added
    ------------------------------------------------------------*/
    webservice static List<Profiles> getProfiles(){
        
        List<Profiles> listResult = new List<Profiles>();
        
        List<Profile> profiles = [SELECT Id,Name,UserType FROM Profile WHERE Name LIKE 'TGS%'];
        Profiles result = new Profiles();

        result.UserType= 'SD';
        result.NameProfile = Constants.PROFILE_DEFAULT_TICKET_MASTER;
        result.IdSFProfile = '1';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = Constants.PROFILE_DEFAULT_TICKET_USER;
        result.IdSFProfile = '2';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = Constants.PROFILE_ORDER_HANDLING;
        result.IdSFProfile = '3';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = Constants.PROFILE_PROVISION;
        result.IdSFProfile = '4';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = Constants.PROFILE_CUSTOMER_CARE;
        result.IdSFProfile = '5';
        listResult.add(result);



                /*
        result = new Profiles();
        result.UserType= 'SD';

        result.NameProfile = 'Billing Inquiry Ticket Master';
        result.IdSFProfile = '3';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';

        result.NameProfile = 'Billing Inquiry Ticket User';
        result.IdSFProfile = '4';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';

        result.NameProfile = 'Complaint Ticket Master';
        result.IdSFProfile = '5';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Complaint Ticket User';
        result.IdSFProfile = '6';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Billing + Complaint Ticket Master';
        result.IdSFProfile = '7';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Billing + Complaint Ticket User';
        result.IdSFProfile = '8';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Ordering Master';
        result.IdSFProfile = '9';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Ordering User';
        result.IdSFProfile = '10';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Billing + Ordering Ticket Master';
        result.IdSFProfile = '11';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Billing + Ordering Ticket User';
        result.IdSFProfile = '12';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Complaint + Ordering Ticket Master';
        result.IdSFProfile = '13';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Complaint + Ordering Ticket User';
        result.IdSFProfile = '14';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Ordering - Permission OH / Contract deployment -IT';
        result.IdSFProfile = '15';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Ordering - Provision';
        result.IdSFProfile = '16';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Billing + Complaint + Ordering Ticket Master';
        result.IdSFProfile = '17';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Billing + Complaint + Ordering Ticket User';
        result.IdSFProfile = '18';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Incident + Change + Problem Ticket Master';
        result.IdSFProfile = '19';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Incident + Change + Problem Ticket User';
        result.IdSFProfile = '20';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Incident Ticket User';
        result.IdSFProfile = '21';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Quality Survey Supervisor';
        result.IdSFProfile = '22';
        listResult.add(result);
        
        result = new Profiles();
        result.UserType= 'SD';
        result.NameProfile = 'Broadcast Submitter(IT)';
        result.IdSFProfile = '23';
        listResult.add(result);
        */
        for (Profile profile: Profiles){
            
            result = new Profiles();
            //26/05/2017: Javier Martínez: Se cambia para que sólo devuelva el perfil TGS Customer Community ya que los otros no tiene disponibilidad de licencias.
            //if (profile.Name.contains('TGS Customer Community') || profile.Name.contains('TGS_Customer_Community_Plus_Login')){
            if (profile.Name.equals('TGS Customer Community') || profile.Name.equals('TGS Carrier Customer Community')){
                 result.NameProfile = profile.Name;
                 result.IdSFProfile = profile.Id;                   
                 result.UserType = 'AU';
                 listResult.add(result);
            }
        }
        
        return listResult;
    }
    
    global class Queue{ 
    
        webservice String NameQueue;
        webservice String IdSFQueue;
        
    }
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    The Portal gets Groups whose Type=Queue and
                    DeveloperName starts with 'TGS'
    History
    <Date>          <Author>        <Change Description>
    12-feb-2015     Ana Cirac        Initial Version
    ------------------------------------------------------------*/
    webservice static List<Queue> getQueues(){
        
        List<Queue> listResult = new List<Queue>();
        
        List<Group> queues = [SELECT Id,Name,DeveloperName FROM Group WHERE Type = 'Queue' AND DeveloperName LIKE 'TGS%'];
        for (Group queue: queues){
            Queue result = new Queue();
            result.NameQueue = queue.Name;
            result.IdSFQueue = queue.Id;

            listResult.add(result);
        }
        
        return listResult;
    }
    
    global class BSMProfile{ 
    
        webservice String NameBSM;
        webservice String IdGroup;
        webservice String IdCompany;
        
    }
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    The Portal gets Groups whose Type=Queue and
                    DeveloperName starts with 'TGS'
    History
    <Date>          <Author>        <Change Description>
    12-feb-2015     Ana Cirac        Initial Version
    ------------------------------------------------------------*/
    webservice static List<BSMProfile> getBSMProfiles(){
        
        List<BSMProfile> listResult = new List<BSMProfile>();
        
        List<TGS_BSM_Profile__c> bsmProfiles = [SELECT Id,Name, TGS_Company__r.Name FROM TGS_BSM_Profile__c];
        for(TGS_BSM_Profile__c bsmProfile:bsmProfiles){
            BSMProfile result = new BSMProfile();
            result.NameBSM = bsmProfile.Name;
            result.IdGroup = bsmProfile.Id;
            result.IdCompany = bsmProfile.TGS_Company__r.Name;
            listResult.add(result);
        }

        return listResult;
    }
    
    global class MailNotification{ 
    
        webservice String NotificationOption;
        
    }
    /*------------------------------------------------------------
    Author:         Ana Cirac 
    Company:        Deloitte
    Description:    The Portal gets Groups whose Type=Queue and
                    DeveloperName starts with 'TGS'
    History
    <Date>          <Author>        <Change Description>
    12-feb-2015     Ana Cirac        Initial Version
    28-May-2015     Ana Cirac        Notifications
    ------------------------------------------------------------*/
    webservice static List<MailNotification> getMailNotifications(){
        
        List<MailNotification> listResult = new List<MailNotification>();
        MailNotification  mailNot = new MailNotification();
        mailNot.NotificationOption = Constants.SEND_ALL_NOTIFICATIONS;
        listResult.add(mailNot);
        MailNotification  mailNot2= new MailNotification();
        mailNot2.NotificationOption = Constants.SEND_BILLING_INQUIRY;
        listResult.add(mailNot2);
        MailNotification  mailNot3= new MailNotification();
        mailNot3.NotificationOption = Constants.SEND_CHANGE;
        listResult.add(mailNot3);
        MailNotification  mailNot4 = new MailNotification();
        mailNot4.NotificationOption = Constants.SEND_COMPLAINT;
        listResult.add(mailNot4);
        MailNotification  mailNot5 =  new MailNotification();
        mailNot5.NotificationOption = Constants.SEND_INCIDENT;
        listResult.add(mailNot5);
     
        MailNotification  mailNot6 = new MailNotification();
        mailNot6.NotificationOption = Constants.SEND_ORDER_MANAGMENT_CASE;
        listResult.add(mailNot6);
     
        MailNotification  mailNot7 = new MailNotification();
        mailNot7.NotificationOption = Constants.SEND_QUERY;
        listResult.add(mailNot7);


        
        return listResult;
    }
    

}