public without sharing class BI_CampaignMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by Campaign Triggers 
    Test Class:    BI_CampaignMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    15/04/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
           
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if the Campaign has changed from Inactive to Active and calls the method createTaskPreOpp to
                   create tasks assigned to Campaign Members.
    
    IN:            ApexTrigger.New, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    15/04/2014              Ignacio Llorca          Initial Version         
    07/12/2015              Guillermo Muñoz         Prevent the email send in Test class 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void assignTaskpreOpp (List<Campaign> news, List<Campaign> olds){
       try{ 
       
            integer i = 0;
            Set<Id> set_campaigns = new Set<Id>();
            Set<Id> set_campaignsEmails = new Set<Id>(); //New var because it´s a pointer.
            
            for(Campaign cmp:news){
                if (cmp.IsActive && olds[i].IsActive == false ){//Falta condición de no encolado para una que ya está encolada.
                        set_campaigns.add(cmp.Id);
                        set_campaignsEmails.add(cmp.Id); //New var because it´s a pointer.
                        //set_campaignsOwners.add(cmp.LastModifiedById);
                }
                i++;
            }
            
            if(!set_campaigns.isempty()){
                System.enqueueJob(new BI_CampaignMember_Queueable(set_campaigns, 2, set_campaignsEmails));

                List<EmailTemplate> lst_tmpl = [SELECT DeveloperName,Id,Name FROM EmailTemplate WHERE DeveloperName = 'BI_Comienzo_Activacion_Campana'];
                if(!lst_tmpl.isEmpty()){
                    Contact dummyContact = new Contact(LastName = 'noreply', Email = 'noreply@telefonica.com');
                    List<Messaging.SingleEmailMessage> lst_mails = new List<Messaging.SingleEmailMessage>();
                    
                    insert dummyContact;

                    List<Campaign> lst_campEmail = [SELECT Id, OwnerId ,Owner.Email, LastModifiedById, LastModifiedBy.Email FROM Campaign WHERE Id IN :set_campaignsEmails];

                    for(Campaign camp :lst_campEmail){
                        
                        Messaging.SingleEmailMessage mailOwner = new Messaging.SingleEmailMessage();

                            mailOwner.setTargetObjectId(dummyContact.Id);
                                                        
                            mailOwner.setTemplateId(lst_tmpl[0].Id);
                            mailOwner.setWhatId(camp.Id);  

                            mailOwner.setReplyTo('noreply@telefonica.com');
                            mailOwner.setSenderDisplayName('Telefónica BI_EN');
                            mailOwner.setSaveAsActivity(false);

                            List<String> lst_toBcc = new List<String>();
                            lst_toBcc.add(camp.LastModifiedBy.Email);
                            if(camp.LastModifiedById != camp.OwnerId){
                                lst_toBcc.add(camp.Owner.Email);
                            }
                            mailOwner.setToAddresses(lst_toBcc);
                            
                            lst_mails.add(mailOwner);
                    }
                    system.debug('lst_mails' +lst_mails);
                    try{
                        if(!Test.isRunningTest()){
                            Messaging.sendEmail(lst_mails);
                        }
                    }catch(exception Exc){
                        BI_LogHelper.generate_BILog('BI_CampaignMemberMethods.createTaskPreOpp', 'BI_EN', Exc, 'Trigger');
                    }
                    
                    delete dummyContact;
                }else{
                    BI_LogHelper.generateLog('BI_CampaignMemberMethods.createTaskPreOpp.sendEmail', 'BI_EN', 'No se encuentra plantilla de email: BI_Comienzo_Activacion_Campana', 'Trigger');
                }
                
                

            }
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_CampaignMethods.assignTaskpreOpp', 'BI_EN', Exc, 'Trigger');
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if the Campaign has changed from Active to Inactive and if 
                   BI_Cancela_tareas_cierre__c field is checked, changes the Task Status to 'Cancelada'.
                       
    IN:            ApexTrigger.New, ApexTrigger.Old
    OUT:           Void
  
    History:
   
    <Date>              <Author>                <Change Description>
    16/04/2014          Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void cancelTask (List<Campaign> news, List<Campaign> olds){
       try{ 
            integer i = 0;
            Set<Id> set_camp = new Set<Id>();
            
            for(Campaign camp:news){
                if (camp.IsActive==false && olds[i].IsActive && camp.BI_Cancela_tareas_cierre__c  == Label.BI_Si){
                    set_camp.add(camp.Id);
                }
                i++;
            }
            if (!set_camp.isempty()){
                Task[] tsk = [SELECT Id, Status FROM Task WHERE BI_Campana_contacto__c=false AND (WhatId IN :set_camp OR BI_CampaignId__c IN :set_camp)];
                
                for(Task tskset:tsk){
                    tskset.Status = Label.BI_TaskStatus_Cancelled;
                }
                
                update tsk; 
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_CampaignMethods.cancelTask', 'BI_EN', Exc, 'Trigger');
        }    
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if the Campaign has changed from Active to Inactive and if 
                   BI_Cancelacion_preoportunidades__c field is checked, changes the Opportunity StageName to 'Cancelada'.
                           
    IN:            ApexTrigger.New, ApexTrigger.Old
    OUT:           Void
      
    History:
     
    <Date>              <Author>                <Change Description>
    16/04/2014          Ignacio Llorca          Initial Version    
    08/05/2015          Micah Burgos            New Batchable class to more Opportunities    
    07/12/2015          Guillermo Muñoz         Prevent the email send in Test class 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void cancelpreOpp (List<Campaign> news, List<Campaign> olds){
        Integer i = 0;
        Set<Id> set_camp = new Set<Id>(); 
        Set<Id> set_campaignsEmails = new Set<Id>(); //Two set because it´s a pointer
        
        for(Campaign camp:news){
            
            if (camp.IsActive==false && olds[i].IsActive && camp.BI_Cancela_oportunidad_cierre__c  == Label.BI_Si){
                set_camp.add(camp.Id);
                set_campaignsEmails.add(camp.Id);
            }
            i++;
        }

        if (!set_camp.isempty() ){ //&& !Test.isRunningTest()
             System.enqueueJob(new BI_Campaign_CancelOpp_Queueable(set_camp, 2,set_campaignsEmails));

            List<EmailTemplate> lst_tmpl = [SELECT DeveloperName,Id,Name FROM EmailTemplate WHERE DeveloperName = 'BI_Start_Cancel_Preopp_campaign'];
            if(!lst_tmpl.isEmpty()){
                Contact dummyContact = new Contact(LastName = 'noreply', Email = 'noreply@telefonica.com');
                List<Messaging.SingleEmailMessage> lst_mails = new List<Messaging.SingleEmailMessage>();
                
                insert dummyContact;

                List<Campaign> lst_campEmail = [SELECT Id, OwnerId ,Owner.Email, LastModifiedById, LastModifiedBy.Email FROM Campaign WHERE Id IN :set_campaignsEmails];

                for(Campaign camp :lst_campEmail){
                    
                    Messaging.SingleEmailMessage mailOwner = new Messaging.SingleEmailMessage();

                        mailOwner.setTargetObjectId(dummyContact.Id);
                                                    
                        mailOwner.setTemplateId(lst_tmpl[0].Id);
                        mailOwner.setWhatId(camp.Id);  

                        mailOwner.setReplyTo('noreply@telefonica.com');
                        mailOwner.setSenderDisplayName('Telefónica BI_EN');
                        mailOwner.setSaveAsActivity(false);

                        List<String> lst_toBcc = new List<String>();
                        lst_toBcc.add(camp.LastModifiedBy.Email);
                        if(camp.LastModifiedById != camp.OwnerId){
                            lst_toBcc.add(camp.Owner.Email);
                        }
                        mailOwner.setToAddresses(lst_toBcc);
                            
                        lst_mails.add(mailOwner);
                }
                system.debug('lst_mails2' +lst_mails);
                try{
                    if(!Test.isRunningTest()){
                        Messaging.sendEmail(lst_mails);
                    }
                }catch(exception Exc){
                    BI_LogHelper.generate_BILog('BI_CampaignMemberMethods.cancelpreOpp', 'BI_EN', Exc, 'Trigger');
                }
                
                delete dummyContact;
            }else{
                BI_LogHelper.generateLog('BI_CampaignMemberMethods.cancelpreOpp.sendEmail', 'BI_EN', 'No se encuentra plantilla de email: BI_Start_Cancel_Preopp_campaign', 'Trigger');
            }
        }

/*        try{
            String F6Opp = Label.BI_F6Preoportunidad;
            String F5Opp = Label.BI_DefinicionSolucion;
            String F4Opp = Label.BI_DesarrolloOferta;
            String F3Opp = Label.BI_F3OfertaPresentada;
            String F2Opp = Label.BI_F2Negociacion;
            String F1Opp = Label.BI_F1CanceladaSusp;
            
            integer i = 0;
            Set<Id> set_camp = new Set<Id>();
            
            for(Campaign camp:news){
                
                if (camp.IsActive==false && olds[i].IsActive && camp.BI_Cancela_oportunidad_cierre__c  == Label.BI_Si){
                    set_camp.add(camp.Id);
                }
                i++;
            }
            if (!set_camp.isempty()){ 
                Opportunity[] opp = [SELECT Id, StageName FROM Opportunity WHERE CampaignId IN :set_camp];
                
                for(Opportunity item:opp){
                   if (item.StageName == F6Opp || item.StageName == F5Opp || item.StageName == F4Opp ||
                       item.StageName == F3Opp || item.StageName == F2Opp)
                   {
                       item.StageName = F1Opp;
                   }
                }
                
                update opp;
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_CampaignMethods.cancelpreOpp', 'BI_EN', Exc, 'Trigger');
        }*/
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that creates three CampaignMemberStatus records, 'Pendiente de aprobación', 'Aprobado', 'Cancelado' 
                  
    IN:            ApexTrigger.New
    OUT:           Void
      
    History:
     
    <Date>              <Author>                <Change Description>
    16/04/2014          Ignacio Llorca          Initial Version    
    22/04/2014          Ignacio Llorca          CampaignMemberStatus will only be created if the Campaign changes from Inactive to Active     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void createCampaignMemberStatus (List<Campaign> news, List<Campaign> olds){
       try{
            integer i = 0;
            
            Set<Id> idCamp = new Set<Id>();
            for(Campaign cam:news)
                idCamp.add(cam.Id);
                
            List <CampaignMemberStatus> cms0 = new List <CampaignMemberStatus>();
            
            Map<Id, Set<String>> map_cms = new Map<Id, Set<String>>();
            
            for(CampaignMemberStatus cms:[select Id, CampaignId, Label from CampaignMemberStatus where CampaignId IN :idCamp]){
                if(map_cms.containsKey(cms.CampaignId))
                    map_cms.get(cms.CampaignId).add(cms.Label);
                else
                    map_cms.put(cms.CampaignId, new Set<String>{cms.Label});
            }
            
            system.debug('map_cms: '+ map_cms);

            for (Campaign camp:news){
            
                if (camp.IsActive && olds[i].IsActive==false){
                    
                    Set<String> labels = (map_cms.get(camp.Id) == null)?new Set<String>():map_cms.get(camp.Id);
                    Integer cont = labels.size();
                    
                    system.debug('*** labels: ' + labels);
                    system.debug('*** cont: ' + cont);

                    if(!labels.contains(Label.BI_PendienteAprobacion)){
                        
                        cont++;
            
                        CampaignMemberStatus cms = new CampaignMemberStatus(CampaignId = camp.Id,
                                                                            Label = Label.BI_PendienteAprobacion,
                                                                            sortorder = cont);
                                                                            
                        cms0.add(cms);
                        system.debug('1 - '+ camp.id + ' Status: '+ Label.BI_PendienteAprobacion);
                                                                        
                    }
                    
                    if(!labels.contains(Label.BI_Aprobado)){
                        
                        cont++;
                    
                        CampaignMemberStatus cms1 = new CampaignMemberStatus(CampaignId = camp.Id,
                                                                             Label = Label.BI_Aprobado,
                                                                             sortorder = cont);
                                                                             
                        cms0.add(cms1);
                        system.debug('2 - '+ camp.id + ' Status: '+ Label.BI_Aprobado);
                                                                             
                    }
                    
                    if(!labels.contains(Label.BI_Rechazado)){
                        
                        cont++;
                                                                        
                        CampaignMemberStatus cms2 = new CampaignMemberStatus(CampaignId = camp.Id,
                                                                             Label = Label.BI_Rechazado,
                                                                             sortorder = cont);
                        
                        cms0.add(cms2);
                        system.debug('3 - '+ camp.id + ' Status: '+ Label.BI_Rechazado);
                        
                    }
                
                }
                i++;
            }
            system.debug('*** cms0: ' + cms0);
            insert cms0;
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_CampaignMethods.createCampaignMemberStatus', 'BI_EN', Exc, 'Trigger');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Salesforce.com
    Description:   Method that checks if the Campaign has changed from Active to Inactive and if 
                   BI_Cancela_tareas_cierre_contactos__c field is checked, changes the Task Status to 'Cancelada'.
                       
    IN:            ApexTrigger.New, ApexTrigger.Old
    OUT:           Void
  
    History:
   
    <Date>              <Author>                <Change Description>
    17/09/2015          Guillermo Muñoz         Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void cancelContactTask(List<Campaign> news, List<Campaign> olds){
        
        try{
            Set <Id> idCampaigns = new Set<Id>();
            List<Task> tasks = new List<Task>();
            for(Integer i = 0; i<news.size(); i++){
                if(!news[i].IsActive && olds[i].IsActive && news[i].BI_Cancela_Tareas_Cierre_Contactos__c == Label.BI_si){
                    idCampaigns.add(news[i].Id);
                }
            }
            if(!idCampaigns.isEmpty()){
                tasks = [Select Id, status, createdDate, lastModifiedDate from Task where BI_Campana_Contacto__c = true AND (BI_CampaignId__c IN : idCampaigns OR whatId in : idCampaigns)];
            }
            
            if(!tasks.isEmpty()){
                for(Task tsk : tasks){
                    if(tsk.createdDate==tsk.lastModifiedDate){
                        tsk.Status=Label.BI_TaskStatus_Cancelled;
                    }
                }
            }
            
            update tasks;           
        }
        catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_CampaignMethods.cancelContactTask', 'BI_EN', Exc, 'Trigger');
        }
    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián
    Company:       Accenture
    Description:   Method to send Emails to Owners of Tasks and Opp related to a new Campaign.
                   Used in BI_CampaignMember_Queueable
    
    IN:            Ids of campaign
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    10/11/2016              Gawron, Julián          Initial Version D295
    1/12/2016               Pedro Parraga           Change in sending the message
    13/02/2017              Gawron, Julián          Changing template
    27/02/2017              Gawron, Julián          Fixing template (adding <li>)
    03/03/2017              Gawron, Julián          Fix whoId     eHelp 02388950
    20/04/2017              Gawron, Julián          Fix eHelp 02494737
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   public static void sendNotificationNewTasksToOwner (Set<Id> campaingIds){

      try{
        if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');


        
      System.debug('sendNotificationNewTasksToOwner entra');


        
        //Data created from Contacts
        //JEG adding BI_Asunto_de_las_tareas__c
        //JEG adding BI_Id_Interno_de_la_Oportunidad__c
        //JEG adding WhoId
        Map<Id,Campaign> map_camp = new Map<Id,Campaign>(); //Para recuperar datos de la campaña
        Map<Id, User> map_user = new Map<Id, User>();
        Map<Id, Map<Id, Map<Id, SObject>>> map_user_camp_obj = new Map<Id, Map<Id, Map<Id, SObject>>>();
        // set_ContactsId map_contactId_AccountName para recuperar nombres de clientes desde Tareas
        Set<Id>           set_ContactsId  = new Set<Id>();
        Map<Id, String>   map_contactId_AccountName = new Map<Id, String>();

        for(Campaign c : [SELECT 
                        Id, Name, BI_Asunto_de_las_tareas__c,                                                   
                        (Select Id, Name,  BI_Id_Interno_de_la_Oportunidad__c,                                  
                           Account.Name, OwnerId, Owner.Name, Owner.Email from Opportunities), 
                        (Select Id, Subject, Account.Name, OwnerId, Owner.Name, Owner.Email, WhoId from Tasks)
                                FROM Campaign 
                                where id in:campaingIds
                                AND BI_Notificar_tareas__c = True]){
           System.debug('sendNotificationNewTasksToOwner camp: ' + c.Name);
            System.debug('sendNotificationNewTasksToOwner tiene opp: ' + c.Opportunities.size());
            System.debug('sendNotificationNewTasksToOwner tiene tasks: ' + c.Tasks.size());
             map_camp.put(c.Id, c);
            if(c.Opportunities.size() > 0 || c.Tasks.size() > 0 ) //Si hay datos, guardamos la campaña
            {   
                for(Opportunity op : c.Opportunities){
                    //Agregar cada línea si corresponde
                    //Si este usuario tiene oportunidades, en esta campaña, la agregamos
                    //Si este usuario no tiene oportunidades, en esta campaña, la creamos
                    if(map_user_camp_obj.containsKey(op.OwnerId)){ // tiene este usuario
                        if(map_user_camp_obj.get(op.OwnerId).containsKey(c.Id)){ // si tiene la campaña
                            map_user_camp_obj.get(op.OwnerId).get(c.Id).put(op.Id, op);//Agregamos la oportunidad
                        }else{ // si no tiene la campaña
                            map_user_camp_obj.get(op.OwnerId).put(c.Id, new Map<Id, SObject>{op.Id => op});
                           // map_user_camp_obj.get(op.OwnerId).get(c.Id).put(op.Id, op);//Agregamos la oportunidad
                        }
                    }else{//Si no tiene este usuario
                        map_user.put(op.OwnerId, new User(Id=op.OwnerId, Email=op.Owner.Email));
                        map_user_camp_obj.put(
                            op.OwnerId, new Map<Id, Map<Id, SObject>>
                              {c.Id => new Map<Id, SObject> {op.Id => op}});
                    }
                    System.debug('sendNotificationNewTasksToOwner opp listada:' + op.Name);
                }
                for(Task ta : c.Tasks){
                    if(map_user_camp_obj.containsKey(ta.OwnerId)){ // tiene este usuario
                        if(map_user_camp_obj.get(ta.OwnerId).containsKey(c.Id)){ // si tiene la campaña
                            map_user_camp_obj.get(ta.OwnerId).get(c.Id).put(ta.Id, ta);//Agregamos la tarea
                        }else{ // si no tiene la campaña
                            map_user_camp_obj.get(ta.OwnerId).put(c.Id, new Map<Id, SObject>{ta.Id => ta});
                           // map_user_camp_obj.get(ta.OwnerId).get(c.Id).put(ta.Id, ta);//Agregamos la tarea
                        }
                    }else{//Si no tiene este usuario
                        map_user.put(ta.OwnerId, new User(Id=ta.OwnerId, Email=ta.Owner.Email));
                        map_user_camp_obj.put(
                            ta.OwnerId, new Map<Id, Map<Id, SObject>>
                              {c.Id => new Map<Id, SObject> {ta.Id => ta}});
                    }

                    set_ContactsId.add(ta.WhoId); //Set para recuperar los datos del cliente desde el Contact
                    System.debug('sendNotificationNewTasksToOwner task listada:' + ta.Subject);


                }   
            }
        }

        //Datos creados a partir de Leads (todavía no hay un campo de búsqueda para esto en Campañas);
        //Es necesario el map_camp values porque tiene las campañas con BI_Notificar_tareas__c true
        //si los whatId son null las task son de leads
            for(Task t : [Select Id, BI_CampaignId__c, OwnerId, Owner.Email, Owner.Name, Subject, WhoId //JEG adding whoid
                    from Task
                    where BI_CampaignId__c in :map_camp.keySet()
                    and WhatId = null]){
                System.debug('sendNotificationNewTasksToOwner task lead a listar:' + t.Subject);
                 if(map_user_camp_obj.containsKey(t.OwnerId)){ //Existe el usuario
                    if(map_user_camp_obj.get(t.OwnerId).containsKey(t.BI_CampaignId__c)){ // si tiene la campaña
                            map_user_camp_obj.get(t.OwnerId).get(t.BI_CampaignId__c).put(t.Id, t);//Agregamos la tarea
                        }else{ // si no tiene la campaña
                            map_user_camp_obj.get(t.OwnerId).put(t.BI_CampaignId__c, new Map<Id, SObject>{t.Id => t});
                           // map_user_camp_obj.get(t.OwnerId).get(t.BI_CampaignId__c).put(t.Id, t);//Agregamos la tarea
                        }
                 }else{//no existe el usuario
                        map_user.put(t.OwnerId, new User(Id=t.OwnerId, Email=t.Owner.Email));
                        /*
                        map_user_camp_obj.put(t.OwnerId, new Map<Id, Map<Id, SObject>>());
                        map_user_camp_obj.get(t.OwnerId).put(t.BI_CampaignId__c, new Map<Id, SObject>());
                        map_user_camp_obj.get(t.OwnerId).get(t.BI_CampaignId__c).put(t.Id, t);//Agregamos la tarea
                        */
                       map_user_camp_obj.put(t.OwnerId, new Map<Id, Map<Id, SObject>>{t.BI_CampaignId__c =>
                             new Map<Id, SObject>{t.Id => t }});
                 }
            }
        
        //Recuperamos los datos de cliente
        System.debug('sendNotificationNewTasksToOwner contacts' + set_ContactsId);
        if(!set_ContactsId.isEmpty()){
          //  List<Contact> lst_contact = [Select id, Account.Name, Owner.Id from Contact where Id in : set_ContactsId ];
            for(Contact c : [Select id, Account.Name, Owner.Id from Contact where Id in : set_ContactsId ]){
                map_contactId_AccountName.put(c.Id, c.Account.Name);
            }
        }





        //Construimos los mensajes
        List<Messaging.SingleEmailMessage> lst_mails = new List<Messaging.SingleEmailMessage>();

    /////////////////////////////////////////////////////////////////////////////////////
    //modificacion de la demanda

        for(Id user_id : map_user_camp_obj.keySet()){
 
            List<String>      lst_nombresCamp = new List<String>(); 
            List<Opportunity> lst_optys = new List<Opportunity>();  
            List<Task>        lst_tasks = new List<Task>();         
            Set<String>       set_nombresTask = new Set<String>();  //juntará BI_Asunto_de_Tareas de todas las campañas.


            for(Id camp_id : map_user_camp_obj.get(user_id).keySet()){

                //acumulamos los datos en cada lista
                for(Id obj_id : map_user_camp_obj.get(user_id).get(camp_id).keySet()){
                    if(map_user_camp_obj.get(user_id).get(camp_id).get(obj_id) instanceof Opportunity)
                        lst_optys.add((Opportunity)map_user_camp_obj.get(user_id).get(camp_id).get(obj_id));
                    else{
                        Task t = (Task)map_user_camp_obj.get(user_id).get(camp_id).get(obj_id);
                        lst_tasks.add(t);
                       // set_ContactsId.add(t.WhoId);
                        set_nombresTask.add(map_camp.get(camp_id).BI_Asunto_de_las_tareas__c);  
                    }
                }
                //Si hay datos en las listas, guardamos la campaña
                if(!lst_optys.isEmpty() || !lst_tasks.isEmpty()){
                    lst_nombresCamp.add(map_camp.get(camp_id).Name);
                }

            }
            System.debug('sendNotificationNewTasksToOwner lst_tasks ' + lst_tasks);
            System.debug('sendNotificationNewTasksToOwner lst_optys ' + lst_optys);

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String str_Asunto;
            String str_CampanasBody;
            if(lst_nombresCamp.size() > 1 ){ // si hay más de una campaña
                String losNombres = String.join(lst_nombresCamp, ', ');
                str_Asunto = 'Campañas ' + losNombres;
                str_CampanasBody = 'las campañas ' + losNombres;
            }else{
                str_Asunto = 'Campaña ' + lst_nombresCamp.get(0);
                str_CampanasBody = 'la campaña ' + lst_nombresCamp.get(0); //JEG
            }

             String msg_html = '<div> '   +
             '<p>Estimado usuarios,<br> ' +
                 'En el marco de ' + str_CampanasBody + ' se informa que se le han asignado las siguientes Oportunidades y/o Tareas: </p>';

             //Insertar Oportunidades
             if(!lst_optys.isEmpty()){
                msg_html += '<p><u>Oportunidades</u></p>';
                msg_html += '<ul>';
                for(Opportunity o : lst_optys){
                    msg_html += ' <li>' + o.Account.Name + ' ' + o.BI_Id_Interno_de_la_Oportunidad__c + ' ' //JEG 01/03/2017
                    + '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() 
                    +'/'+ o.Id + '" target="_blank">Link</a></li>'; //JEG
                }
                msg_html += '</ul>';
             }
             //Insertar Tareas
             if(!lst_tasks.isEmpty()){
                msg_html += '<p><u>Tareas ' + String.join(lst_nombresCamp, ', ') +'</u></p>';
                msg_html += '<ul>';
                for(Task t : lst_tasks){
                    //El nombre de la cuenta se obtiene del contacto asociado. Si es un lead no lo tiene, se agrega 'Sin cliente asociado'
                    String accName = (map_contactId_AccountName.get(t.WhoId) == null ) ? 'Sin cliente asociado' : map_contactId_AccountName.get(t.WhoId);
                    msg_html += '<li>' + accName + ' - <a href="'
                        + URL.getSalesforceBaseUrl().toExternalForm()
                        +'/' + t.Id + '" target="_blank">Link</a></li>';                 
                }
                msg_html += '</ul>';
             }
             msg_html += '</div>';
             
             //Configuramos el mail
             mail.setTargetObjectId(user_id);                
             mail.setReplyTo('noreply@telefonica.com');
             mail.setSenderDisplayName('Telefónica BI_EN');
             mail.setSubject(str_Asunto);
             mail.setHtmlBody(msg_html);
             mail.setSaveAsActivity(false);

             System.debug(msg_html);
             //Agregamos el mail a la lista 
             lst_mails.add(mail);

            //fixing heapProblem
              // implement logic to reduce
               //Si el Heap es mayor que 2/3 del limite, envia y limipia la lista.
               if(Limits.getHeapSize() > (Limits.getLimitHeapSize() - Limits.getLimitHeapSize() / 3 )){
                 Messaging.sendEmail(lst_mails);
                 lst_mails.clear();
               }


        }//fin del if

        if(!Test.isRunningTest()){
                Messaging.sendEmail(lst_mails);
            }else{
                System.debug('sendNotificationNewTasksToOwner mailsAEnviar:' + lst_mails);
            }


    }catch(exception Exc){
                BI_LogHelper.generate_BILog('BI_CampaignMethods.sendNotificationNewTasksToOwner', 'BI_EN', Exc, 'Trigger - Queueable');
    }

    }//fin de sendNotificationNewTasksToOwner

}