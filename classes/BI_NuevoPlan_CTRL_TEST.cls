/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_NuevoPlan_CTRL class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    22/05/2015              Ignacio Llorca            Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_NuevoPlan_CTRL_TEST {

    static testMethod void nuevoPlanTest() {  
        
        
        List<String> lst_region = new List<String>();
        
        lst_region.add('Chile');        
                                                 
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        List <Opportunity> lst_opp = new List<Opportunity>();
        

        
        for(Opportunity opp:BI_DataLoadRest.loadOpportunitiesWithPais(1, lst_acc[0].Id, lst_region[0], null)){
            
            opp.BI_Opportunity_Type__c = 'Móvil';
            
            lst_opp.add(opp);
        }
        
        update lst_opp;
        
        ApexPages.Standardcontroller ctr = new ApexPages.Standardcontroller(lst_opp[0]);
        ApexPages.currentPage().getParameters().put('id', lst_opp[0].Id);
        
        BI_NuevoPlan_CTRL class_ctrl = new BI_NuevoPlan_CTRL(ctr);
        
        class_ctrl.crearPlan();
        
    }
}