@isTest
public with sharing class BI_G4C_BI_Sede_Methods_TEST {
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Julio Alberto Asenjo García
	    Company:       everis
	    Description:   Test class for BI_G4C_Sede_Methods	    
	    History: 
	    
	    <Date>                          <Author>                    		 <Change Description>
	    20/06/2016                      Julio Alberto Asenjo García          Initial version
        20/09/2017                  Angel F. Santaliestra                   Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
        
    
   static testMethod void copiaSede() {
            Account a = new Account(Name='pruebaTest',BI_Segment__c = 'test',
                            BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
            insert a;   
            BI_Sede__c sede = new BI_Sede__c(Name ='Test sede', BI_Codigo_postal__c = '12345', BI_COL_Latitud__c='0', BI_COL_Longitud__c='0', BI_Direccion__c = 'Direccion', BI_Localidad__c = 'Localidad', BI_Provincia__c = 'Provincia');
            insert sede;
            BI_Punto_de_instalacion__c site = new BI_Punto_de_instalacion__c(Name = 'Test site', BI_Cliente__c = a.Id, BI_Sede__c = sede.Id, BI_Tipo_de_sede__c='Comercial Principal' );
            insert site;
            List<BI_Punto_de_instalacion__c> sitelist = new List<BI_Punto_de_instalacion__c>();
            sitelist.add(site);
            BI_G4C_BI_Sede_Methods.shippingAddressToDirComercial (sitelist);
        }
        static testMethod void copiaSede2() {
            Account a = new Account(Name='pruebaTest',BI_Segment__c = 'test',
                            BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
            insert a;   
            BI_Sede__c sede = new BI_Sede__c(Name ='Test sede', BI_Codigo_postal__c = '12345', BI_COL_Latitud__c='0', BI_COL_Longitud__c='0', BI_Direccion__c = 'Direccion', BI_Localidad__c = 'Localidad', BI_Provincia__c = 'Provincia');
            insert sede;
            update sede;

        }     
}