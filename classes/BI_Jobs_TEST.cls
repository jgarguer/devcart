@isTest 
private class BI_Jobs_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage "catch" code for BI_Jobs class. Try coverage has been manage on BI_Buttons_TEST.sendExternalCommunication()
                    
    History: 
    
    <Date>                  <Author>                <Change Description>
    29/09/2014              Micah Burgos            Initial Version
    19/01/2017              Pedro Párraga           Add Methods     updateUsers_Test(), updateEntitlementsAcc_Test(), start_checkNews_SLAProcess_JOB_Test(), linea_JOB_Test() and BackOffice_JOB_Test()
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for createExternalCommunicationJOB method
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    29/09/2014              Micah Burgos            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*static testMethod void createExternalCommunicationJOB_catchTest1() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_Jobs.createExternalCommunicationJOB(null);
    }*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for createExternalCommunicationJOB method
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    29/09/2014              Micah Burgos            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*static testMethod void createExternalCommunicationJOB_catchTest2() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_Jobs.createExternalCommunicationJOB(null,null);
    }*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for generateStrProg method
            
    History:  
    
    <Date>                  <Author>                <Change Description>
    29/09/2014              Micah Burgos            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    static testMethod void generateStrProg_catchTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        system.assertEquals(null , BI_Jobs.generateStrProg(null)); 
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for generateStrNameProg method
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    29/09/2014              Micah Burgos            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    static testMethod void generateStrNameProg_catchTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        system.assertEquals(null , BI_Jobs.generateStrNameProg(null,null));
    }

    static testMethod void updateCases() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_Jobs.updateCases();
    }
    
    static testMethod void updateCasesException()
    {
        try
        {
           BI_Jobs.updateCases(null, null); 
        } catch (Exception e)
        {
           System.assert(e.getMessage().contains('Attempt to de-reference a null object')); 
        }
        
    }

    static testMethod void updateCurrencyUFTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_Configuracion__c conf = new BI_Configuracion__c (Name = 'url_currency_uf',
                                                            BI_Valor__c = 'http://www.sii.cl/pagina/valores/uf/uf@anyo@.htm');
        insert conf;
         BI_Configuracion__c conf2 = new BI_Configuracion__c (Name = 'split_currency_uf',
                                                            BI_Valor__c = '<td style=\'text-align:right;\'>');
        insert conf2;

        BI_Configuracion__c conf3 = new BI_Configuracion__c (Name = 'day_currency_uf',
                                                            BI_Valor__c = '<th style=\'text-align:center;\'>@day@</th>');
        insert conf3;

          BI_Configuracion__c conf4 = new BI_Configuracion__c (Name = 'client_secret_currency_uf',
                                                            BI_Valor__c = '1444009479913099833');
        insert conf4;

         BI_Configuracion__c conf5 = new BI_Configuracion__c (Name = 'uname_currency_uf',
                                                            BI_Valor__c = 'integracion.chile@telefonica.com.prd');
        insert conf5;

           BI_Configuracion__c conf6 = new BI_Configuracion__c (Name = 'upass_currency_uf',
                                                            BI_Valor__c = 'Aborda15tyG6mz6CTT4QaYGZwTdhX6vK');
        insert conf6;

        BI_Configuracion__c conf7 = new BI_Configuracion__c (Name = 'client_id_currency_uf',
                                                            BI_Valor__c = '3MVG9A_f29uWoVQtvM2zZHhBXk3RfIMlFEv.zHVySIbfVPvCM5w2ok2nTEehyS4viSGqXqkiNbc48ijBPw_hl');
        insert conf7;

        BI_Jobs.updateCurrencyUF();
        BI_Currency_UF_JOB currencyjob = new BI_Currency_UF_JOB();
        BI_Currency_UF_JOB.executeFuture();
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for updateUsers() method
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/01/2017              Pedro Párraga            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @isTest static void updateUsers_Test(){ 
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS; 

        Test.startTest();  
        
        BI_Jobs.updateUsers(UserInfo.getProfileId(), true);  
        BI_Jobs.updateUsers(UserInfo.getProfileId(), false); 
        BI_Jobs.updateUsers(null, false); 
        BI_Jobs.updateUsers(null, null);   

        Test.stopTest(); 
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for updateEntitlementsAcc() method
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/01/2017              Pedro Párraga            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void updateEntitlementsAcc_Test(){
      List<SlaProcess>  lst_news_SLAProceso = [SELECT CreatedById,CreatedDate,Description,Id,IsActive,IsDeleted,IsVersionDefault,LastModifiedById,LastModifiedDate,Name,NameNorm,StartDateField,
                                                    SystemModstamp,VersionMaster,VersionNotes,VersionNumber FROM SlaProcess WHERE Name Like '%Chile%' AND IsVersionDefault = true AND IsActive = true ];
        
        
        Test.startTest();  

        BI_Jobs.updateEntitlementsAcc(UserInfo.getProfileId(), lst_news_SLAProceso, false);
        BI_Jobs.updateEntitlementsAcc(null, null, null);

       Test.stopTest(); 
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for start_checkNews_SLAProcess_JOB() method
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/01/2017              Pedro Párraga            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void start_checkNews_SLAProcess_JOB_Test(){
      List<SlaProcess>  lst_news_SLAProceso = [SELECT CreatedById,CreatedDate,Description,Id,IsActive,IsDeleted,IsVersionDefault,LastModifiedById,LastModifiedDate,Name,NameNorm,StartDateField,
                                                    SystemModstamp,VersionMaster,VersionNotes,VersionNumber FROM SlaProcess WHERE Name Like '%Chile%' AND IsVersionDefault = true AND IsActive = true ];

      Map<Id, SlaProcess> mapsla = new Map<Id, SlaProcess>();

        for(SlaProcess sla : lst_news_SLAProceso){
            mapsla.put(sla.Id, sla);
        }
    
    Test.startTest(); 

        BI_Jobs.start_checkNews_SLAProcess_JOB(mapsla);
        BI_Jobs.start_checkNews_SLAProcess_JOB(null);

    Test.stopTest(); 

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for Linea_JOB() method
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/01/2017              Pedro Párraga            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void linea_JOB_Test(){
        String email = 'example@email.com';
        List<Id> lst = new List<Id>();
        Integer posi = 0;
        Map<Id,String> result = new Map<Id,String>();
        Map<Id,String> mline = new Map<Id,String>();
        Integer option = 0;

        lst.add(UserInfo.getProfileId());

        Test.startTest(); 

        BI_Jobs.Linea_JOB(email, lst, posi, result, mline, option);

        Test.stopTest(); 
   
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BackOffice_JOB() method
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/01/2017              Pedro Párraga            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void BackOffice_JOB_Test(){
    

        String email = 'example@email.com';
        List<Id> lst = new List<Id>();
        Integer posi = 0;
        Map<Id,String> result = new Map<Id,String>();
        Map<Id,String> mline = new Map<Id,String>();
        Integer option = 0;

    Test.startTest(); 

        BI_Jobs.BackOffice_JOB(email, lst, posi, result, option);

    Test.stopTest(); 
    }


/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}