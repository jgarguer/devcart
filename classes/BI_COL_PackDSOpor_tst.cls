/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:          
* Description:         
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-07-24      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_PackDSOpor_ctr
*			 1.1	2015-08-19		Jeisson Rojas (JR)		Cambio realizado a la Query de Usuarios del campo Country por Pais__c
*                   20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest
private class BI_COL_PackDSOpor_tst 
{
	
	public static BI_COL_PackDSOpor_ctr objPackDSOpor;
	public static Account   objCuenta;
	public static Opportunity objOportunidad;
	public static BI_COL_Descripcion_de_servicio__c objDesSer;
	public static BI_COL_Viabilidad_Tecnica__c objViabiTec;
	
	public static list <Profile> lstPerfil;
	public static list <UserRole> lstRoles;
	public static List<User> lstUsuarios;
	public static BI_COL_PackDSOpor_ctr.Wrapempaquetar objWrap;
	public static List<BI_COL_PackDSOpor_ctr.Wrapempaquetar> lstWraper;
	public static List<List<BI_COL_PackDSOpor_ctr.Wrapempaquetar>> lstWrapList;
	public static User objUsuario;
	
	public static void crearData()
	{
		
		/*lstPerfil             = new list <Profile>();
		lstPerfil               = [Select id,Name from Profile where Name = 'Admon Ventas Master'];
		system.debug('datos Perfil '+lstPerfil);

		lstUsuarios = new List<User>();
		lstUsuarios                       = [Select Id FROM User where country != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];
		system.debug('datos Usuario '+lstUsuarios);*/

		lstPerfil             = new list <Profile>();
		lstPerfil               = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1 ];
		system.debug('datos Perfil '+lstPerfil);
		
		
		lstRoles                = new list <UserRole>();
		lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
		system.debug('datos Rol '+lstRoles);
		
		
		lstUsuarios = new List<User>();
		lstUsuarios                       = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];
		system.debug('datos Usuario '+lstUsuarios);
		
		
		objCuenta = new Account();
		objCuenta                                         = new Account();
		objCuenta.Name                                  = 'prueba';
		objCuenta.BI_Country__c                         = 'Argentina';
		objCuenta.TGS_Region__c                         = 'América';
		objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta.CurrencyIsoCode                       = 'COP';
		objCuenta.BI_Segment__c                         = 'test';
        objCuenta.BI_Subsegment_Regional__c             = 'test';
        objCuenta.BI_Territory__c                       = 'test';
		insert objCuenta;
		system.debug('datos Cuenta '+objCuenta);
		
		
		objOportunidad                                      = new Opportunity();
		objOportunidad.Name                                   = 'prueba opp';
		objOportunidad.AccountId                              = objCuenta.Id;
		objOportunidad.BI_Country__c                          = 'Argentina';
		objOportunidad.CloseDate                              = System.today().addDays(+5);
		objOportunidad.StageName                              = 'F5 - Solution Definition';
		objOportunidad.CurrencyIsoCode                        = 'COP';
		objOportunidad.Certa_SCP__contract_duration_months__c = 12;
		objOportunidad.BI_Plazo_estimado_de_provision_dias__c = 0 ;
		objOportunidad.OwnerId                                = lstUsuarios[0].id;
		insert objOportunidad;
		system.debug('Datos Oportunidad  '+objOportunidad);
		
		
		objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
		objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
		objDesSer.CurrencyIsoCode               = 'COP';
		insert objDesSer;
		System.debug('\n\n\n Sosalida objDesSer '+ objDesSer);

		objViabiTec = new BI_COL_Viabilidad_Tecnica__c();
		objViabiTec.BI_COL_Descripcion_de_servicio_DSR__c = objDesSer.Id;
		objViabiTec.BI_COL_Numero_viabilidad__c = 10;
		//objViabiTec.Name = 'prueba Viabilidad';
		insert objViabiTec;
		System.debug('Datos objViabiTec '+ objViabiTec);

	}
	
	static testMethod void test_method_one() 
	{
		// TO DO: implement unit test
		crearData();
		Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
		ApexPages.StandardController  sc                = new ApexPages.StandardController(objCuenta);
		PageReference                 pageRef           = Page.BI_COL_PackDSOpor_pag;
		Test.setCurrentPage(pageRef);
		system.debug('objOportunidad '+objCuenta.Id);
		
		List<BI_COL_Descripcion_de_servicio__c> lstDS =new List<BI_COL_Descripcion_de_servicio__c>();
		
		List<Account> lstCuentas = new List<Account>();
		lstWraper = new List<BI_COL_PackDSOpor_ctr.Wrapempaquetar> ();
		List<BI_COL_PackDSOpor_ctr.Wrapempaquetar> lstWraper2 = new List<BI_COL_PackDSOpor_ctr.Wrapempaquetar> ();
		lstWrapList = new List<List<BI_COL_PackDSOpor_ctr.Wrapempaquetar>> ();
		BI_COL_Empaquetamiento__c objEmpaquetamiento = new BI_COL_Empaquetamiento__c();
		List<BI_COL_Empaquetamiento__c> lstObjEmpaq = new List<BI_COL_Empaquetamiento__c>();
		
		Test.startTest();
		pageRef.getParameters().put('id',objCuenta.Id);
		objPackDSOpor = new BI_COL_PackDSOpor_ctr(sc);
		objWrap = new BI_COL_PackDSOpor_ctr.Wrapempaquetar ();
		BI_COL_manage_cons__c objMangeCons = new BI_COL_manage_cons__c();
		//objDesSer = new BI_COL_Descripcion_de_servicio__c();
		//objPackDSOpor.constructor('prueba', lstObjEmpaq, lstDS);
		
		
		for ( Integer i = 0; i < 5 ; I ++)
		{
			objDesSer = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
			objDesSer.CurrencyIsoCode               = 'COP';
			objDesSer.BI_COL_Ancho_de_Banda_Canal_Cliente__c = 'prueba '+i;
			
			System.debug('ds '+ i +' '+objDesSer);
			lstDS.add(objDesSer);
			
			objCuenta = new Account();
			objCuenta.Name                                  = 'prueba'+i;
			objCuenta.BI_Country__c                         = 'Argentina';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Segment__c                         = 'test';
	        objCuenta.BI_Subsegment_Regional__c             = 'test';
	        objCuenta.BI_Territory__c                       = 'test';
			lstCuentas.add(objCuenta);
		}
		
		insert lstDS;
		insert lstCuentas;
		
		
		for ( Integer i = 0; i < 5 ; I ++)
		{
			objWrap = new BI_COL_PackDSOpor_ctr.Wrapempaquetar ();
			
			objWrap.seleccion = false;
			objWrap.paquete='Sin Empaquetar'+i;
			objWrap.NombrePaquete='Sin Empaquetar';
			objWrap.idDs = lstDS.get(i).Id;			
			
			lstWraper.add(objWrap);
			lstWraper2.add(objWrap);

			objEmpaquetamiento.BI_COL_Segmento__c = 'prueba'+i;
			
			lstObjEmpaq.add(objEmpaquetamiento);
		}
		

		//lstDS.add(objDesSer);
		lstWrapList.add(lstWraper);
		lstWrapList.add(lstWraper2);
		
		
		System.debug('Datos lista DS===> '+lstDS);
		System.debug('Datos lista Cuenta===> '+lstCuentas);
		System.debug('Datos Wrapp ===>'+objWrap);
		System.debug('Datos lista Wrapp ===>'+lstWraper);
		System.debug('Datos lista Wrapp2 ===>'+lstWraper2);
		System.debug('Datos lista de lista Wrapp ===>'+lstWrapList);
		
		objPackDSOpor.lstDs = lstDS;
		objPackDSOpor.lstMostrar = lstWraper;
		objPackDSOpor.lstPrincipal = lstWrapList;

		objPackDSOpor.constructor('prueba',lstObjEmpaq,lstDs);
		objPackDSOpor.siguiente();
		objPackDSOpor.empaquetar();
		objPackDSOpor.opcEmpaquetar = label.BI_COL_lblNuevo_Paquete;
		objPackDSOpor.empaquetar();
		
		
		objPackDSOpor.opcEmpaquetar = 'prueba';
		objMangeCons.Name = 'ConsPaquetes';
		objMangeCons.BI_COL_Numero_Viabilidad__c = objViabiTec.BI_COL_Numero_viabilidad__c;
		insert objMangeCons;
		System.debug('datos objmangecons'+objMangeCons);
		objPackDSOpor.lstDsEmpaquetar = lstDS;
		objPackDSOpor.empaquetar();
		
		/*objPackDSOpor.opcEmpaquetar = 'prueba';		
		objMangeCons = new BI_COL_manage_cons__c();
		objMangeCons.Name = 'ConsPaquetes';
		objMangeCons.BI_COL_Numero_Viabilidad__c = objViabiTec.BI_COL_Numero_viabilidad__c;
		insert objMangeCons;
		System.debug('datos objmangecons'+objMangeCons);
		
		objPackDSOpor.empaquetar();*/
		
		test.stopTest();
		
	}
	
	static testMethod void test_method_two() 
	{
		// TO DO: implement unit test
		Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
		crearData();
		ApexPages.StandardController  Sc                = new ApexPages.StandardController(objCuenta);
		PageReference                 pageRef           = Page.BI_COL_PackDSOpor_pag;
		Test.setCurrentPage(pageRef);
		
		System.debug('id cuenta ===>'+objCuenta.Id);
		
		objPackDSOpor = new BI_COL_PackDSOpor_ctr(Sc);
		
		List<BI_COL_Descripcion_de_servicio__c> lstDesSer = new List<BI_COL_Descripcion_de_servicio__c>();
		lstWraper = new List<BI_COL_PackDSOpor_ctr.Wrapempaquetar> ();
		lstWrapList = new List<List<BI_COL_PackDSOpor_ctr.Wrapempaquetar>> ();
		List<BI_COL_Empaquetamiento__c> lstObjEmpaq = new List<BI_COL_Empaquetamiento__c>();
		
		Test.startTest();
		
		pageRef.getParameters().put('id',string.valueOf(objCuenta.Id));
		//pageRef.getParameters().put('id',objCuenta.Id);
		
		objPackDSOpor.lstDs = lstDesSer;
		objPackDSOpor.lstMostrar = lstWraper;
		objPackDSOpor.lstPrincipal = null;

		objPackDSOpor.constructor('prueba',lstObjEmpaq,lstDesSer);

		Test.stopTest();
	}

	static testMethod void test_method_three() 
	{
		// TO DO: implement unit test
		Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
		crearData();
		ApexPages.StandardController  sc                = new ApexPages.StandardController(objCuenta);
		PageReference                 pageRef           = Page.BI_COL_PackDSOpor_pag;
		Test.setCurrentPage(pageRef);
		system.debug('objOportunidad '+objCuenta.Id);
		
		List<BI_COL_Descripcion_de_servicio__c> lstDS =new List<BI_COL_Descripcion_de_servicio__c>();
		
		List<Account> lstCuentas = new List<Account>();
		lstWraper = new List<BI_COL_PackDSOpor_ctr.Wrapempaquetar> ();
		List<BI_COL_PackDSOpor_ctr.Wrapempaquetar> lstWraper2 = new List<BI_COL_PackDSOpor_ctr.Wrapempaquetar> ();
		lstWrapList = new List<List<BI_COL_PackDSOpor_ctr.Wrapempaquetar>> ();
		BI_COL_Empaquetamiento__c objEmpaquetamiento = new BI_COL_Empaquetamiento__c();
		List<BI_COL_Empaquetamiento__c> lstObjEmpaq = new List<BI_COL_Empaquetamiento__c>();
		list<Profile> lstPerfil2 = new list<Profile>();
		List<User> lstUsuarios2 = new List<User>();



		
		Test.startTest();
		pageRef.getParameters().put('id',objCuenta.Id);
		objPackDSOpor = new BI_COL_PackDSOpor_ctr(sc);
		objWrap = new BI_COL_PackDSOpor_ctr.Wrapempaquetar ();


		//objDesSer = new BI_COL_Descripcion_de_servicio__c();
		//objPackDSOpor.constructor('prueba', lstObjEmpaq, lstDS);
		
		
		for ( Integer i = 0; i < 5 ; I ++)
		{
			objDesSer = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
			objDesSer.CurrencyIsoCode               = 'COP';
			objDesSer.BI_COL_Ancho_de_Banda_Canal_Cliente__c = 'prueba '+i;
			
			System.debug('ds '+ i +' '+objDesSer);
			lstDS.add(objDesSer);
			
			objCuenta = new Account();
			objCuenta.Name                                  = 'prueba'+i;
			objCuenta.BI_Country__c                         = 'Argentina';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Segment__c                         = 'test';
	        objCuenta.BI_Subsegment_Regional__c             = 'test';
	        objCuenta.BI_Territory__c                       = 'test';
			lstCuentas.add(objCuenta);
		}
		
		insert lstDS;
		insert lstCuentas;
		
		
		for ( Integer i = 0; i < 5 ; I ++)
		{
			objWrap = new BI_COL_PackDSOpor_ctr.Wrapempaquetar ();
			
			objWrap.seleccion = false;
			objWrap.paquete='Sin Empaquetar'+i;
			objWrap.NombrePaquete='Sin Empaquetar';
			objWrap.idDs = lstDS.get(i).Id;
			objWrap.linea = 'prueba linea';
			objWrap.producto = 'prueba prod';
			objWrap.descripcionRef = 'prueba descripcion';
			
			lstWraper.add(objWrap);
			lstWraper2.add(objWrap);

			objEmpaquetamiento.BI_COL_Segmento__c = 'prueba'+i;
			
			lstObjEmpaq.add(objEmpaquetamiento);
		}
		

		//lstDS.add(objDesSer);
		lstWrapList.add(lstWraper);
		lstWrapList.add(lstWraper2);
		
		
		System.debug('Datos lista DS===> '+lstDS);
		System.debug('Datos lista Cuenta===> '+lstCuentas);
		System.debug('Datos Wrapp ===>'+objWrap);
		System.debug('Datos lista Wrapp ===>'+lstWraper);
		System.debug('Datos lista Wrapp2 ===>'+lstWraper2);
		System.debug('Datos lista de lista Wrapp ===>'+lstWrapList);
		
		objPackDSOpor.lstDs = lstDs;
		objPackDSOpor.lstMostrar = lstWraper;
		objPackDSOpor.lstPrincipal = lstWrapList;
		objPackDSOpor.iterador = 3;

		objPackDSOpor.constructor('prueba',lstObjEmpaq,lstDs);
		objPackDSOpor.lstPrincipal = lstWrapList;
		objPackDSOpor.iterador = 1;
		objPackDSOpor.anterior();
		
		test.stopTest();
		
	}

	static testMethod void test_method_four() 
	{
		// TO DO: implement unit test
		Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
		crearData();
		ApexPages.StandardController  sc                = new ApexPages.StandardController(objCuenta);
		PageReference                 pageRef           = Page.BI_COL_PackDSOpor_pag;
		Test.setCurrentPage(pageRef);
		system.debug('objOportunidad '+objCuenta.Id);
		
		List<BI_COL_Descripcion_de_servicio__c> lstDS =new List<BI_COL_Descripcion_de_servicio__c>();
		
		List<Account> lstCuentas = new List<Account>();
		lstWraper = new List<BI_COL_PackDSOpor_ctr.Wrapempaquetar> ();
		List<BI_COL_PackDSOpor_ctr.Wrapempaquetar> lstWraper2 = new List<BI_COL_PackDSOpor_ctr.Wrapempaquetar> ();
		lstWrapList = new List<List<BI_COL_PackDSOpor_ctr.Wrapempaquetar>> ();
		BI_COL_Empaquetamiento__c objEmpaquetamiento = new BI_COL_Empaquetamiento__c();
		List<BI_COL_Empaquetamiento__c> lstObjEmpaq = new List<BI_COL_Empaquetamiento__c>();
		
		Test.startTest();
		pageRef.getParameters().put('id',objCuenta.Id);
		objPackDSOpor = new BI_COL_PackDSOpor_ctr(sc);
		objWrap = new BI_COL_PackDSOpor_ctr.Wrapempaquetar ();
		BI_COL_manage_cons__c objMangeCons = new BI_COL_manage_cons__c();
		objMangeCons.Name = 'ConsPaquetes';
		objMangeCons.BI_COL_Numero_Viabilidad__c = objViabiTec.BI_COL_Numero_viabilidad__c;
		insert objMangeCons;
		
		objPackDSOpor.opcEmpaquetar = 'prueba';
		
		
		for ( Integer i = 0; i < 5 ; I ++)
		{
			objDesSer = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
			objDesSer.CurrencyIsoCode               = 'COP';
			objDesSer.BI_COL_Ancho_de_Banda_Canal_Cliente__c = 'prueba '+i;
			
			System.debug('ds '+ i +' '+objDesSer);
			lstDS.add(objDesSer);
			
			objCuenta = new Account();
			objCuenta.Name                                  = 'prueba'+i;
			objCuenta.BI_Country__c                         = 'Argentina';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Segment__c                         = 'test';
	        objCuenta.BI_Subsegment_Regional__c             = 'test';
	        objCuenta.BI_Territory__c                       = 'test';
			lstCuentas.add(objCuenta);
		}
		
		insert lstDS;
		insert lstCuentas;
		
		
		//for ( Integer i = 0; i < 5 ; I ++)
		//{
			objWrap = new BI_COL_PackDSOpor_ctr.Wrapempaquetar ();
			
			objWrap.seleccion = true;
			objWrap.paquete='Sin Empaquetar'+0;
			objWrap.NombrePaquete='Sin Empaquetar';
			objWrap.idDs = lstDS.get(0).Id;	
			objWrap.disable = false;
			
			lstWraper.add(objWrap);
			//lstWraper2.add(objWrap);

			objEmpaquetamiento.BI_COL_Segmento__c = 'prueba'+0;
			
			lstObjEmpaq.add(objEmpaquetamiento);
		//}
		

		//lstDS.add(objDesSer);
		lstWrapList.add(lstWraper);
		//lstWrapList.add(lstWraper2);
		
		
		System.debug('Datos lista DS===> '+lstDS);
		System.debug('Datos lista Cuenta===> '+lstCuentas);
		System.debug('Datos Wrapp ===>'+objWrap);
		System.debug('Datos lista Wrapp ===>'+lstWraper);
		System.debug('Datos lista Wrapp2 ===>'+lstWraper2);
		System.debug('Datos lista de lista Wrapp ===>'+lstWrapList);
		
		Map<Id,BI_COL_Descripcion_de_servicio__c> mapIdXnuevaDS = new Map<Id,BI_COL_Descripcion_de_servicio__c>();
		mapIdXnuevaDS.put(lstDS.get(0).Id,objDesSer);

		objPackDSOpor.constructor('prueba',lstObjEmpaq,lstDs);		
		objPackDSOpor.mapIdXnuevaDS = mapIdXnuevaDS;
		objPackDSOpor.lstDs = lstDS;
		objPackDSOpor.lstMostrar = lstWraper;
		objPackDSOpor.lstPrincipal = lstWrapList;

		objPackDSOpor.empaquetar();
		//(JR) Metodos que no se encontraban cubiertos
		objPackDSOpor.getHabilitarPaquete();
		objPackDSOpor.nextBtnClick();
		objPackDSOpor.previousBtnClick();
		objPackDSOpor.getPageNumber();
		objPackDSOpor.getTotalPageNumber();
		objPackDSOpor.getPreviousButtonEnabled();
		objPackDSOpor.getNextButtonDisabled();
	}
}