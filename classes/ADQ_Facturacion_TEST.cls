@isTest
private class ADQ_Facturacion_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        
    History:
    
    <Date>            <Author>              <Description>
    ??/??/????        ??                   Initial version
    28/12/2016		  Adrián Caro		   Validation rule error solved
    13/06/2017		  Guillermo Muñoz	   Deprecated references to Raz_n_social__c (Optimización)
    25/09/2017		  Jaime Regidor		   Fields BI_Subsector__c and BI_Sector__c added

    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
		
	@isTest static void test_ADQ_Facturacion() {
		// Implement test code
			Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
			Account acc01  = new Account ();
				acc01.Name = 'Cuenta de prueba JMO';
				acc01.BI_Subsegment_Regional__c = 'test';
				acc01.BI_Territory__c = 'test';
				//acc01.Raz_n_social__c = 'Cuenta de prueba SA CV';
				acc01.BI_Country__c = Label.Bi_Mexico;
				acc01.BI_Segment__c = 'Empresas';
				acc01.BI_Subsector__c = 'test'; //25/09/2017
				acc01.BI_Sector__c = 'test'; //25/09/2017
				acc01.RecordTypeId = rtId;
			insert acc01;
			
			Opportunity opp01 = new Opportunity ();	
				//opp01.BI_Id_de_la_Oportunidad__c = '0067000000ZA5XF';
				opp01.Name = 'ENLACE INTERNET DEDICADO PLANTA';
				opp01.AccountId = acc01.Id;
				opp01.BI_Country__c = Label.Bi_Mexico;	
				opp01.BI_Ciclo_ventas__c = 'Completo';
				opp01.CloseDate = date.today();
				opp01.StageName = 'F1 - Ganada';
				opp01.CurrencyIsoCode = 'USD';
				opp01.BI_Duracion_del_contrato_Meses__c = 36;
				opp01.BI_Ingreso_por_unica_vez__c = 0.0;
				opp01.BI_Recurrente_bruto_mensual__c = 13390.00;
				opp01.BI_Opportunity_Type__c = 'Fijo';
				opp01.Amount = 482040;
			insert opp01;	
			
			NE__Order__c NEORDER =  new NE__Order__c(); 
				NEORDER.NE__OptyId__c = opp01.Id;
				NEORDER.NE__AccountId__c = acc01.Id;
				insert NEORDER;
			
						
			NE__OrderItem__c NEInsert2  = new NE__OrderItem__c ();
				NEInsert2.NE__OrderId__c = NEORDER.id; 
				NEInsert2.CurrencyIsoCode = 'USD';
				NEInsert2.NE__OneTimeFeeOv__c = 0;
				NEInsert2.NE__RecurringChargeOv__c = 100;
				NEInsert2.Fecha_de_reasignaci_n_a_factura__c = date.today();
				NEInsert2.NE__Qty__c = 1;
				insert NEInsert2;

			NE__OrderItem__c NEInsert3  = new NE__OrderItem__c ();
				NEInsert3.NE__OrderId__c = NEORDER.id; 
				NEInsert3.CurrencyIsoCode = 'USD';
				NEInsert3.NE__OneTimeFeeOv__c = 100;
				NEInsert3.NE__RecurringChargeOv__c = 0;
				NEInsert3.NE__Qty__c = 1;
				insert NEInsert3;
			
			Map<string,CurrencyTypeFacturacion__c>  conversionrates = new  Map<string,CurrencyTypeFacturacion__c> ();

			CurrencyTypeFacturacion__c corporatecurrency = new  CurrencyTypeFacturacion__c ();
				corporatecurrency.IsoCode__c = 'USD';
				corporatecurrency.ConversionRate__c = 0.058823529411;
				corporatecurrency.IsActive__c = true;
			insert corporatecurrency;
			conversionrates.put(corporatecurrency.Id, corporatecurrency);

			
			
			List<Factura__c> lsInsertFactura = new List<Factura__c>();
			Factura__c Factura = new Factura__c ();
				Factura.Activo__c = true;
				Factura.Cliente__c = acc01.Id;
				Factura.Fecha_Inicio__c = Date.today();
				Factura.Fecha_Fin__c = Date.today() + 20;
				Factura.Inicio_de_Facturaci_n__c = Date.today();
				Factura.IVA__c = '16%';
				Factura.Sociedad_Facturadora__c = 'GTM';
				Factura.Requiere_Anexo__c = 'SI';
				Factura.CurrencyIsoCode = 'USD';
				RecordType tiporegistro = [SELECT Id FROM RecordType where  SobjectType = 'Factura__c' and DeveloperName  ='Recurrente'];
				Factura.RecordTypeId = tiporegistro.Id;
			insert Factura;
			
				Factura.Sociedad_Facturadora__c = 'PCS';
			update  Factura;

			NEInsert2.Factura__c = Factura.Id;
			update NEInsert2;

			NEInsert3.Factura__c = Factura.Id;
			update NEInsert3;

			lsInsertFactura.add(Factura);
			
			Programacion__c programacion = new Programacion__c ();
				programacion.Factura__c = Factura.Id;
				programacion.CurrencyIsoCode = 'USD';
				programacion.Sociedad_Facturadora_Real__c = 'GTM';
				programacion.Facturado__c = false;
				programacion.Fecha__c = date.newInstance(2009 ,12, 25);
				programacion.Inicio_del_per_odo__c = Date.today();
				//programacion.Fin_del_per_odo__c = Date.today();
				programacion.Fin_del_per_odo__c = Date.newInstance(date.today().year(), date.today().month(), Date.daysInMonth(date.today().year(), date.today().month()));
				programacion.Fecha_de_inicio_de_cobro__c = Date.today();
				programacion.Numero_de_factura_SD__c = null;
				programacion.Folio_fiscal__c = null;
			insert programacion;

	 		  	

			Pedido_PE__c  pediodPE01 = new Pedido_PE__c ();
                pediodPE01.Producto_en_Sitio__c = NEInsert2.Id;
                pediodPE01.CurrencyIsoCode = NEInsert2.CurrencyIsoCode;
                pediodPE01.Precio_original_EQS__c = Double.valueOf(''+NEInsert2.NE__RecurringChargeOv__c);
                pediodPE01.Precio_original__c = 3999.98;
                pediodPE01.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                pediodPE01.Precio_convertido__c = 8990.998;
                pediodPE01.Inicio_del_per_odo__c = date.newInstance(2015, 5, 24);
                pediodPE01.Fin_del_per_odo__c = date.newInstance(2015, 5, 31);
    			pediodPE01.Lleva_IEPS__c = false; 
                pediodPE01.IEPS__c = (pediodPE01.Lleva_IEPS__c)? Double.valueOf(''+pediodPE01.Precio_convertido__c)*0.03:0;
                pediodPE01.Programacion__c = programacion.Id;
	        insert pediodPE01;


	        Pedido_PE__c  pediodPE04 = new Pedido_PE__c ();
                pediodPE04.Producto_en_Sitio__c = NEInsert3.Id;
                pediodPE04.CurrencyIsoCode = NEInsert2.CurrencyIsoCode;
                pediodPE04.Precio_original_EQS__c = Double.valueOf(''+NEInsert3.NE__RecurringChargeOv__c);
                pediodPE04.Precio_original__c = 3999.98;
                pediodPE04.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                pediodPE04.Precio_convertido__c = 8990.998;
                pediodPE04.Inicio_del_per_odo__c = date.newInstance(2015, 5, 24);
                pediodPE04.Fin_del_per_odo__c = date.newInstance(2015, 5, 31);
    			pediodPE04.Lleva_IEPS__c = false; 
                pediodPE04.IEPS__c = (pediodPE04.Lleva_IEPS__c)? Double.valueOf(''+pediodPE04.Precio_convertido__c)*0.03:0;
                pediodPE04.Programacion__c = programacion.Id;
	        insert pediodPE04;

	        Facturas_SenS__c sens =  new Facturas_SenS__c ();
	        	sens.Factura__c = Factura.Id;
	        	sens.Equipos_en_Sitio__c = NEInsert2.Id;
	        	sens.Fecha_de_inicio__c = date.newInstance(2015, 5, 24);

	        insert sens;	




	        List<Pedido_PE__c> listaPedidoPE  = new  List<Pedido_PE__c> ();
	        
			

			Map<String, NotadeCredito__c> mapNotasDeCredito = new  Map<String, NotadeCredito__c> ();				
			NotadeCredito__c ncp = new NotadeCredito__c();
				ncp.Encabezado_de_Facturacion__c = Factura.Id;
				ncp.TipoNota__c = 'Total';
				ncp.Responsable__c = 'Gustavo Arditti';
				ncp.Factura__c = programacion.Id;
				ncp.Fecha_Solicitud__c = Date.today();
				ncp.Motivo_Pedido_del__c = 'M19-Refacturación';
				ncp.Tipo_de_IVA__c = '16%'; 
				ncp.CurrencyIsoCode = 'USD';
				ncp.Condiciones_de_Pago__c = '30 Días';
				ncp.Refacturada__c = false;
				ncp.Facturado__c = true;
			insert ncp;

			mapNotasDeCredito.put(ncp.Id,ncp);
			
			
			NotaCredito_OLI__c NColi = new NotaCredito_OLI__c ();
				NColi.NotaCreditoId__c = ncp.Id;
				NColi.ProgramacionId__c = programacion.Id;
				NColi.Lleva_IEPS__c = false;
				NColi.Importe__c = 2000;
				
			insert 	NColi;
						
			Programacion__c programacionMiscelanea = new Programacion__c ();
				programacionMiscelanea.Factura__c = Factura.Id;
				programacionMiscelanea.CurrencyIsoCode = 'USD';
				programacionMiscelanea.Sociedad_Facturadora_Real__c = 'GTM';
				programacionMiscelanea.Facturado__c = false;
				programacionMiscelanea.Fecha__c = Date.today();
				programacionMiscelanea.Inicio_del_per_odo__c = Date.today();
				programacionMiscelanea.Fin_del_per_odo__c = Date.today().addMonths(1);
				programacionMiscelanea.Fecha_de_inicio_de_cobro__c = Date.today().addDays(2);
				programacionMiscelanea.Numero_de_factura_SD__c = null;
				programacionMiscelanea.Folio_fiscal__c = null;
				programacionMiscelanea.Miscelanea__c = true;
				
			try{
				insert programacionMiscelanea;
			}catch (Exception err){
					
			}
			
						
			Pedido_PE__c  p = new Pedido_PE__c ();
                p.Producto_en_Sitio__c = NEInsert2.Id;
                p.CurrencyIsoCode = NEInsert2.CurrencyIsoCode;
                p.Precio_original_EQS__c = Double.valueOf(''+NEInsert2.NE__RecurringChargeOv__c);
                p.Precio_original__c = 3999.98;
                p.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                p.Precio_convertido__c = 8990.998;
                p.Inicio_del_per_odo__c = date.newInstance(2015, 5, 24);
                p.Fin_del_per_odo__c = date.newInstance(2015, 5, 31);
    			p.Lleva_IEPS__c = false; 
                p.IEPS__c = (p.Lleva_IEPS__c)? Double.valueOf(''+p.Precio_convertido__c)*0.03:0;
                p.Programacion__c = programacionMiscelanea.Id;
	        insert p;
	        //listaPedidosPE.add(p);

	        Pedido_PE__c  p2 = new Pedido_PE__c ();
                p2.Producto_en_Sitio__c = NEInsert2.Id;
                p2.CurrencyIsoCode = NEInsert2.CurrencyIsoCode;
                p2.Precio_original_EQS__c = Double.valueOf(''+NEInsert2.NE__RecurringChargeOv__c);
                p2.Precio_original__c = 3999.98;
                p2.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                p2.Precio_convertido__c = 8990.998;
                p2.Inicio_del_per_odo__c = date.newInstance(2015, 5, 24);
                p2.Fin_del_per_odo__c = date.newInstance(2015, 5, 31);
    			p2.Lleva_IEPS__c = false; 
                p2.IEPS__c = (p.Lleva_IEPS__c)? Double.valueOf(''+p.Precio_convertido__c)*0.03:0;
                p2.Programacion__c = programacionMiscelanea.Id;
	        insert p2;
	       
	        
	         Factura_OLI__c f = new Factura_OLI__c();
                f.Factura__c = Factura.Id;
               // f.Commercial_Product__c = ne01.NE__ProdId__c; // relacionamos con el producto comercial
                f.Cantidad__c = 1;
                f.CurrencyIsoCode = 'USD';
                f.Descuento__c = 0.2 * 100;
                f.Plazo__c = 36;  
                f.Precio_Unitario__c = 800.80;
                f.Configuration_Item__c = NEInsert2.Id;
                
          	insert f;
		
		//ApexPages.currentPage().getParameters().put('pid', pro.Id);
		pageReference pr = page.ADQ_Facturacion;
		Test.startTest();
		Test.setCurrentPage(pr); 
		apexpages.currentPage().getParameters().put('pid',programacion.Id);
		Double val1= 98723;
		Double val2= 5643;
		ADQ_Facturacion anexo = new ADQ_Facturacion();

		ADQ_Facturacion.EquipoWrapper equipoWrappero1= new  ADQ_Facturacion.EquipoWrapper ('MPLS remta', val1, val2, val2, val2,   conversionrates,  corporatecurrency);
		List<ADQ_Facturacion.EquipoWrapper>	listaWrapperProductos = new List<ADQ_Facturacion.EquipoWrapper> ();
		listaWrapperProductos.add(equipoWrappero1);
		

		anexo.programacion  =  programacion;
		anexo.factura = Factura;
		anexo.getHoy();
		anexo.getPeriodo ();
		anexo.getTotalConLetra ();
		anexo.generaAnexos ();
		//anexo.getListaWrapperProductos();
		//anexo.getTotalConLetra();
		equipoWrappero1.formatCurrency (17);
		equipoWrappero1.round(val1,val2);
		anexo.getListaWrapperProductos();
		equipoWrappero1.truncate (val1, 2);
		Test.stopTest();
	}
	

	
}