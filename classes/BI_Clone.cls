public class BI_Clone {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Methods for cloning sObjects
    
    History:
    
    <Date>            <Author>          <Description>
    22/05/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Clones the opportunity
    
    IN:            Opportunity Id
    OUT:           Opportunity cloned
    
    History:
    
    <Date>            <Author>          <Description>
    22/05/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Opportunity obtainOppClone(Id idopp){
        try{

            BI_GlobalVariables.isCloned = true;

            Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap();
            List<String> lst_fields = new List<String>();
    
            for(String str : objectFields.keySet())
                lst_fields.add(str);
            
            String query = 'SELECT ' + lst_fields[0]; 
        
            for (Integer i = 1 ; i < lst_fields.size() ; i++)
                query += ', ' + lst_fields[i];
        
            query += ' FROM Opportunity WHERE ID = \'' + idopp + '\' limit 1';
            
            List<Opportunity> lst_opp = Database.query(query); 
            Opportunity opp2 = lst_opp[0].clone(false, false, false, false);
            return opp2;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_Clone.obtainOppClone', 'BI_EN', Exc, 'Trigger');
           return null;
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Clones the opportunity
    
    IN:            Opportunity Id
    OUT:           Opportunity cloned
    
    History:
    
    <Date>            <Author>          <Description>
    22/05/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<OpportunityLineItem> obtainOliClone(Id idoppL){
        try{

            BI_GlobalVariables.isCloned = true;
            
            Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get('OpportunityLineItem').getDescribe().fields.getMap();
            List<String> lst_fields = new List<String>();
    
            for(String str : objectFields.keySet())
                lst_fields.add(str);
            
            String query = 'SELECT ' + lst_fields[0]; 
        
            for (Integer i = 1 ; i < lst_fields.size() ; i++)
                query += ', ' + lst_fields[i];
        
            query += ' FROM OpportunityLineItem WHERE OpportunityId = \'' + idoppL + '\'';
            
            List<OpportunityLineItem> lst_oli = Database.query(query); 
            
            List<OpportunityLineItem> lst_oli_clone = lst_oli.deepClone(false, false, false);
            
            return lst_oli_clone;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_Clone.obtainOliClone', 'BI_EN', Exc, 'Trigger');
           return null;
        }
    }

}