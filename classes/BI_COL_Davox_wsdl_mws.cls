/**
* Avanxo Colombia
* @author           OJCB
* Project:          Telefonica
* Description:      Clase tipo WebServiceMock del servicio NotificacionesSalesForceWSDL.
*
* Changes (Version)
* -------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    ---------------
* @version  1.0     2015-08-24      OJCB                    Definicion de la clase implments WebServiceMock
*************************************************************************************************************/

@isTest
global class BI_COL_Davox_wsdl_mws implements WebServiceMock 
{

    global Map<String, Object> mapWSResponseBySOAPAction    = new Map<String, Object>();

    global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){
        
        String  strRespuesta='Respuesta';
        String strXmlResp = '';

            strXmlResp = '<WS>';
            strXmlResp += '<USER>USER</USER>';
            strXmlResp += '<PASSWORD>PASSWORD</PASSWORD>';
            strXmlResp += '<DATEPROCESS>2015-08-24</DATEPROCESS>';
            strXmlResp += '<PACKCODE>ASD654AS</PACKCODE>';
            strXmlResp += '<'+Label.BI_COL_LbSeparator+'>Õ</'+Label.BI_COL_LbSeparator+'>';
            strXmlResp += '<TYPE>CONSULTA_CUENTA</TYPE>';
            strXmlResp += '<COUNT>1</COUNT>';
            strXmlResp += '<'+label.BI_COL_LbValues+'>';     
            strXmlResp += '<'+label.BI_COL_LbValue+'>value_testÕvalue_test2Õvalue_test3Õvalue_test4ÕSCÕVEÕvalue_test7Õvalue_test8Õvalue_test9Õvalue_test10Õvalue_test11Õvalue_test12 </'+label.BI_COL_LbValue+'>';
            strXmlResp +=  '</'+label.BI_COL_LbValues+'>';      
            strXmlResp += '</WS>';
            system.debug('@-->strXmlResp'+strXmlResp); 
        BI_COL_Davox_wsdl.findResponse_element objfindResponse_element = new BI_COL_Davox_wsdl.findResponse_element();
        objfindResponse_element.findReturn = strXmlResp;//'Prueba objfindResponse_element';
        
        BI_COL_Davox_wsdl.addResponse_element objaddResponse_element = new BI_COL_Davox_wsdl.addResponse_element();
        //objaddResponse_element.addReturn = 'Prueba objaddResponse_element';
        objaddResponse_element.addReturn = strXmlResp;

        mapWSResponseBySOAPAction.put( 'find', objfindResponse_element );
        mapWSResponseBySOAPAction.put( 'add', objaddResponse_element );

                
        response.put( 'response_x', mapWSResponseBySOAPAction.get( requestName ) );
    }
}