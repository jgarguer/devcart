/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Ignacio G Schuhmacher
Company:       Accenture - New Energy Aborda
Description:   TEST Class for BI_AddressRest coverage

History:

<Date>         		 <Author>          		<Description>
05/06/2017     Ignacio G Schuhmacher        Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
public with sharing class BI_AddressRest_TEST {
	static testMethod void test_updateSite() {


		System.debug('test_updateSite');
		BI_TestUtils.throw_exception = false;
		FS_TGS_RestWrapperFullStack.SiteRequestType site = new FS_TGS_RestWrapperFullStack.SiteRequestType();
		site.address = new FS_TGS_RestWrapperFullStack.AddressInfoType();
		site.name = 'UpdateSitesTest_1';
		site.status = FS_TGS_RestWrapperFullStack.SiteStatusType.active;
		site.address.addressName='UpdateSitesTest_1';	
		site.address.addressNumber = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.addressNumber.value='6'; 
		site.address.country='Spain'; 
		site.address.region='Madrid'; 
		site.address.locality='Madrid'; 
		site.address.floor = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.floor.value='4'; 
		site.address.apartment = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.apartment.value='B'; 
		site.address.postalCode='50007'; 
		createAddress();

		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();
		restReq.requestURI = '/SiteManagement/v1/sites/111222333';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		BI_AddressRest.updateSite();
		BI_Sede__C updatedAddress = [SELECT Id, Name,BI_Direccion__c,BI_Numero__c,BI_Country__c,BI_Provincia__c,BI_Localidad__c,BI_Piso__c,
								BI_Apartamento__c,BI_Codigo_postal__c,BI_ID_de_la_sede__c  
								from BI_Sede__c where BI_ID_de_la_sede__c = :'111222333' limit 1];

		system.assertEquals(updatedAddress.Name, site.address.addressName);
		system.assertEquals(updatedAddress.BI_Direccion__c, site.address.addressName);
		system.assertEquals(updatedAddress.BI_Numero__c, site.address.addressNumber.value);
		system.assertEquals(updatedAddress.BI_Country__c, site.address.country);
		system.assertEquals(updatedAddress.BI_Provincia__c, site.address.region);
		system.assertEquals(updatedAddress.BI_Localidad__c, site.address.locality);
		system.assertEquals(updatedAddress.BI_Piso__c, site.address.floor.value);
		system.assertEquals(updatedAddress.BI_Apartamento__c, site.address.apartment.value);
		system.assertEquals(updatedAddress.BI_Codigo_postal__c, site.address.postalCode);

		
	}

	static testMethod void test_updateSiteBadSiteInUri() {
		System.debug('test_updateSiteBadSiteInUri');
		BI_TestUtils.throw_exception = false;
		FS_TGS_RestWrapperFullStack.SiteRequestType site = new FS_TGS_RestWrapperFullStack.SiteRequestType();
		site.address = new FS_TGS_RestWrapperFullStack.AddressInfoType();
		site.name = 'UpdateSitesTest_1';
		site.status = FS_TGS_RestWrapperFullStack.SiteStatusType.active;
		site.address.addressName='UpdateSitesTest_1';	
		site.address.addressNumber = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.addressNumber.value='6'; 
		site.address.country='France'; 
		site.address.region='Madrid'; 
		site.address.locality='Madrid'; 
		site.address.floor = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.floor.value='4'; 
		site.address.apartment = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.apartment.value='B'; 
		site.address.postalCode='50007'; 
		createAddress();

		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();
		restReq.requestURI = '/SiteManagement/v1/sites/111222334';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		BI_AddressRest.updateSite();
		BI_Sede__C updatedAddress = [SELECT Id, Name,BI_Direccion__c,BI_Numero__c,BI_Country__c,BI_Provincia__c,BI_Localidad__c,BI_Piso__c,
								BI_Apartamento__c,BI_Codigo_postal__c,BI_ID_de_la_sede__c  
								from BI_Sede__c where BI_ID_de_la_sede__c = :'111222333' limit 1];


		system.assertNotEquals(updatedAddress.Name, site.address.addressName);
		system.assertNotEquals(updatedAddress.BI_Direccion__c, site.address.addressName);
		system.assertNotEquals(updatedAddress.BI_Numero__c, site.address.addressNumber.value);
		system.assertNotEquals(updatedAddress.BI_Country__c, site.address.country);
		system.assertNotEquals(updatedAddress.BI_Provincia__c, site.address.region);
		system.assertNotEquals(updatedAddress.BI_Localidad__c, site.address.locality);
		system.assertNotEquals(updatedAddress.BI_Piso__c, site.address.floor.value);
		system.assertNotEquals(updatedAddress.BI_Apartamento__c, site.address.apartment.value);
		system.assertNotEquals(updatedAddress.BI_Codigo_postal__c, site.address.postalCode);

		
	}

	static testMethod void test_updateSiteWithOutSiteInUri() {
		System.debug('test_updateSiteWithOutSiteInUri');
		BI_TestUtils.throw_exception = false;
		FS_TGS_RestWrapperFullStack.SiteRequestType site = new FS_TGS_RestWrapperFullStack.SiteRequestType();
		site.address = new FS_TGS_RestWrapperFullStack.AddressInfoType();
		site.name = 'UpdateSitesTest_1';
		site.status = FS_TGS_RestWrapperFullStack.SiteStatusType.active;
		site.address.addressName='UpdateSitesTest_1';	
		site.address.addressNumber = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.addressNumber.value='6'; 
		site.address.country='France'; 
		site.address.region='Madrid'; 
		site.address.locality='Madrid'; 
		site.address.floor = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.floor.value='4'; 
		site.address.apartment = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.apartment.value='B'; 
		site.address.postalCode='50007'; 
		createAddress();

		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();
		restReq.requestURI = '/SiteManagement/v1/sites/';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		BI_AddressRest.updateSite();
		BI_Sede__C updatedAddress = [SELECT Id, Name,BI_Direccion__c,BI_Numero__c,BI_Country__c,BI_Provincia__c,BI_Localidad__c,BI_Piso__c,
								BI_Apartamento__c,BI_Codigo_postal__c,BI_ID_de_la_sede__c  
								from BI_Sede__c where BI_ID_de_la_sede__c = :'111222333' limit 1];

		
	}

	static testMethod void test_updateSiteWithOutBody() {
		System.debug('test_updateSiteWithOutSiteInUri');
		BI_TestUtils.throw_exception = false;
		FS_TGS_RestWrapperFullStack.SiteRequestType site = new FS_TGS_RestWrapperFullStack.SiteRequestType();
		site.address = new FS_TGS_RestWrapperFullStack.AddressInfoType();
		site.name = 'UpdateSitesTest_1';
		site.status = FS_TGS_RestWrapperFullStack.SiteStatusType.active;
		site.address.addressName='UpdateSitesTest_1';	
		site.address.addressNumber = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.addressNumber.value='6'; 
		site.address.country='France'; 
		site.address.region='Madrid'; 
		site.address.locality='Madrid'; 
		site.address.floor = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.floor.value='4'; 
		site.address.apartment = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.apartment.value='B'; 
		site.address.postalCode='50007'; 
		createAddress();

		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();
		restReq.requestURI = '/SiteManagement/v1/sites/111222333';
		//restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		BI_AddressRest.updateSite();
		BI_Sede__C updatedAddress = [SELECT Id, Name,BI_Direccion__c,BI_Numero__c,BI_Country__c,BI_Provincia__c,BI_Localidad__c,BI_Piso__c,
								BI_Apartamento__c,BI_Codigo_postal__c,BI_ID_de_la_sede__c  
								from BI_Sede__c where BI_ID_de_la_sede__c = :'111222333' limit 1];

		
	}

	public static void createAddress(){
		BI_Sede__c address = new BI_Sede__c();
		address.Name = 'UpdateSitesTest';
		address.BI_Direccion__c = 'UpdateSitesTest';
		address.BI_Numero__c = '1';
		address.BI_Country__c = 'Spain';
		address.BI_Provincia__c = 'Zaragoza';
		address.BI_Localidad__c = 'Zaragoza';
		address.BI_Piso__c = '5';
		address.BI_Apartamento__c = 'A';
		address.BI_Codigo_postal__c = '50006';
		address.BI_ID_de_la_sede__c = '111222333';
		insert address;
	}
}