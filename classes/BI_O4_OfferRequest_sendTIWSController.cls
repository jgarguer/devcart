public with sharing class BI_O4_OfferRequest_sendTIWSController {

	public BI_O4_Offer_Request__c offReq;
	public BI_Log__c error {get; set;}
	public String baseURL {get {return URL.getSalesforceBaseUrl().toExternalForm();}}

	public static final String TIWS_NAME = Label.BI_O4_OfferRequest;

	public BI_O4_OfferRequest_sendTIWSController(ApexPages.StandardController ctrl) {
		//offReq = (BI_O4_Offer_Request__c) ctrl.getRecord();
	}

	public PageReference run(){

		try {
			if(BI_TestUtils.isRunningTest()){throw new BI_Exception('test');}
			String idOffReq = ApexPages.currentPage().getParameters().get('id');

			if(idOffReq == null) {
				return null;
			}
			offReq = [SELECT Id, Name, BI_O4_Offer_Req_Submitted_Date__c FROM BI_O4_Offer_Request__c WHERE Id = :idOffReq];

			List<PartnerNetworkRecordConnection> lstRecords = new List<PartnerNetworkRecordConnection>();
			for(PartnerNetworkConnection conn : [SELECT Id, ConnectionStatus, ConnectionName FROM PartnerNetworkConnection
						WHERE ConnectionStatus = 'Accepted' AND ConnectionName = :TIWS_NAME]) {
				
				lstRecords.add(new PartnerNetworkRecordConnection(ConnectionId = conn.Id, RelatedRecords='Attachment', LocalRecordId = offReq.Id));
			}
			insert lstRecords;

			if(offReq.BI_O4_Offer_Req_Submitted_Date__c == null) {
				offReq.BI_O4_Offer_Req_Submitted_Date__c = DateTime.now();
				update offReq;
			}

			PageReference pRef = new PageReference('/' + idOffReq);
			pRef.setRedirect(true);
			return pRef;
		} catch (Exception exc) {
			Id idErr = BI_LogHelper.generate_BILog('BI_O4_OfferRequest_sendTIWSController.run', 'BI_EN O4', exc, 'Apex Class');
			error = [SELECT Id, Name FROM BI_Log__c WHERE Id =: idErr];
			return null;
		}
	}
}