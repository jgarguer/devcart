/****************************************************************************************************
	Información general
	-------------------
	author: Javier Tibamoza Cubillos
	company: Avanxo Colombia
	Project: Implementación Salesforce
	Customer: TELEFONICA
	Description: 
	
	Information about changes (versions)
	-------------------------------------
	Number    Dates           Author                       Description
	------    --------        --------------------------   -----------
	1.0       05-Jun-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
public with sharing class BI_COL_ValidatorCaseServiceManager_cls 
{
	public static void fnValidate( Case objCase ) 
	{
		Id IdCurrentUser = UserInfo.getUserId();

		List<User> lCurrentUser = [ select Pais__c from User Where Id=: IdCurrentUser ];

		if( lCurrentUser[0].Pais__c == 'Colombia' && IdCurrentUser != objCase.OwnerId && objCase.BI_COL_CasoPM__c)
		{
			Map<Id,Id> mIdUser = new Map<Id,Id>();
		
			List<AccountTeamMember> lATM = [
				select 	AccountId, Id, TeamMemberRole, UserId 
				from 	AccountTeamMember
				where 	AccountId =: objCase.AccountId ];

			for( AccountTeamMember oATM : lATM )
			{
				if( oATM.TeamMemberRole == 'Service Manager' )
				{
					mIdUser.put( oATM.UserId,oATM.UserId );
				}
			}

			if( mIdUser != null )
			{
				if( !mIdUser.containsKey( IdCurrentUser ) )
				{
					objCase.addError(Label.BI_COL_lblErrorValidatorServiceManager);
				}
			}	
		}
	}
}