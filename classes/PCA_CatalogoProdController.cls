public with sharing class PCA_CatalogoProdController extends PCA_HomeController{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Class to invoke catalog 
    
    History:
    
    <Date>            <Author>              <Description>
    26/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    
    public List<Market_Place_PP__c> marketPlaceFamilies     {get;set;}
    
    public List<BI_Catalogo_PP__c> products                 {get;set;}
    
    public PCA_CatalogoProdController(){}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest()){
              throw new BI_Exception('test');
            }
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            system.debug('page: '+page);
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_CatalogoProdController.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info of catalogo
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfo (){
        
        try{
            if(BI_TestUtils.isRunningTest()){
              throw new BI_Exception('test');
            }
            system.debug('loadInfo');
            Id accId = BI_AccountHelper.getCurrentAccountId();
            Id ProdId = (apexpages.currentpage().getparameters().get('id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('id')):null;
            system.debug('ProdId: ' +ProdId);
            if(ProdId!=null){
                products=[SELECT BI_Familia_del_producto__r.Name,Name,BI_Imagen_del_producto__c,BI_Link_icono_1__c,BI_Link_icono_2__c,BI_Link_icono_3__c,BI_Link_icono_4__c,BI_Texto_descriptivo_P1__c,BI_Texto_descriptivo_P2__c,BI_Texto_de_Introduccion__c,BI_Texto_icono_1__c,BI_Texto_icono_2__c,BI_Texto_icono_3__c,BI_Texto_icono_4__c,BI_Titulo_de_seccion_P1__c,BI_Titulo_de_seccion_P2__c,BI_Titulo_icono_1__c,BI_Titulo_icono_2__c,BI_Titulo_icono_3__c,BI_Titulo_icono_4__c FROM BI_Catalogo_PP__c where Id =: ProdId];
                for(BI_Catalogo_PP__c item: products){
                    if(item.BI_Imagen_del_producto__c==null){
                        item.BI_Imagen_del_producto__c = 'url';
                    }
                }
                
            }
            
            /*if(getAccountCountry(accId)!=null){
                marketPlaceFamilies = [SELECT BI_Descripcion__c,BI_Link_icono__c,BI_Link_imagen__c,BI_Country__c,BI_Posicion__c,BI_Subtitulo__c,BI_Texto__c,BI_Titulo__c,Id,Name FROM Market_Place_PP__c where BI_Country__c = :getAccountCountry(accId) limit 4];
                List<Id> familyIds = new List<Id>();
                for(Market_Place_PP__c item : marketPlaceFamilies){
                    familyIds.add(item.Id);
                }
                products =[SELECT BI_Familia_del_producto__c, BI_Texto_de_Introduccion__c, Id, Name FROM BI_Catalogo_PP__c where BI_Familia_del_producto__c in :familyIds];
            }*/
            
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_CatalogoProdController.loadInfo', 'Portal Platino', Exc, 'Class');
        }
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Create task to the customer executive
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    07/12/2015        Guillermo Muñoz      Prevent the email send in Test class 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void createTask (){
        try{
            if(BI_TestUtils.isRunningTest()){
              throw new BI_Exception('test');
            }
            Id ProdId = (apexpages.currentpage().getparameters().get('id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('id')):null;
            Task newTask = new Task();
            newTask.Subject = Label.BI_Contratacion+' '+products[0].Name;
            Id accId = BI_AccountHelper.getCurrentAccountId();
            newTask.OwnerId = BI_AccountHelper.getCurrentAccount(accId).Owner.Id;
            system.debug('OwnerID**: '+newTask.OwnerId);
            List<BI_Contact_Customer_Portal__c> CCP = [SELECT BI_Contacto__c FROM BI_Contact_Customer_Portal__c where BI_User__c =: Userinfo.getUserId() AND BI_Cliente__c=: BI_AccountHelper.getCurrentAccountId() LIMIT 1];
            
            if(!CCP.IsEmpty()){
                
                newTask.WhoId = CCP[0].BI_Contacto__c;
                system.debug('WhoId:' + newTask.WhoId);
            }
            system.debug('CCP** :' +CCP);
            if(ProdId!=null){
                newTask.WhatId = ProdId;
            } 
            newTask.Status = Label.BI_NoIniciada;
            newTask.Priority = 'Normal';
            //newTask.IsReminderSet = true;
            system.debug('newTask****: ' + newTask);
            Database.Saveresult resul= Database.insert(newTask,false);
            system.debug('result****: ' + resul);
            List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject, DeveloperName, Body from EmailTemplate where DeveloperName='PCA_comunicacion_nueva_tarea'];
            //List<User> users = [select Id, Email__c from User where Id= :newTask.OwnerId];
            
            if(!templates.isEmpty()){
                List<Messaging.SingleEmailMessage> emailSend = new List<Messaging.SingleEmailMessage>();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                //String[] emailAddresses = new String[]{};
                //system.debug('EMAIL**: '+users[0].Email__c);
                //emailAddresses.add(users[0].Email__c);         
                //mail.setToAddresses(emailAddresses);
                mail.settargetObjectId(newTask.OwnerId); 
                string body = templates[0].Body;
                system.debug('body: '+body);
                body = body.replace('{!Task.Subject}', newTask.Subject);
                body = body.replace('{!Task.Status}', newTask.Status);
                body = body.replace('{!Task.Priority}', newTask.Priority);
                //body = body.replace('{!Event.Subject}', item.Subject);
                //mail.setPlainTextBody(body);
                system.debug('mail: '+mail);
                mail.setSubject(templates[0].Subject);
                mail.setPlainTextBody(body);
                emailSend.add(mail);
                if(!Test.isRunningTest()){
                    Messaging.sendEmail(emailSend);
                }
            }
            //createchattermsg(newTask.OwnerId, products[0].Name);    
            ConnectApi.ChatterMessage Msg = ConnectAPI.ChatterMessages.sendMessage('El usuario '+UserInfo.getName() +' ha pedido información acerca del producto '+products[0].Name+'. ', newTask.OwnerId);
            System.debug('**Mensaje Chatter: '+Msg );
        }
        catch (exception Exc){
            system.debug('error');
            BI_LogHelper.generate_BILog('PCA_CatalogoProdController.createTask', 'Portal Platino', Exc, 'Class');
        }
    }
    /*@future(callout=true)
    public static void createchattermsg(Id OwnerId, String ProductName){   
        Id userId = UserInfo.getUserId();    
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();    
        req.setEndpoint('https://'+System.URL.getSalesforceBaseUrl().getHost()+'/services/data/v33.0/connect/communities/0DBw00000000pEgGAI/chatter/users/'+UserInfo.getUserId()+'/messages');
        //req.setHeader('Authorization', 'OAuth ' + UserInfo.getsessionId());
        req.setMethod('POST');
        req.setBody('{"body":"El usuario '+UserInfo.getName() +'" ha pedido información acerca del producto '+ProductName+'. ","recipients":["'+OwnerId+'","'+OwnerId+'"]}');        
        try {
            //res = http.send(req);
            system.debug('res**: '+res);
        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
    }   */ 
    
}