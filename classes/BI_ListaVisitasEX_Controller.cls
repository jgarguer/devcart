/*--------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Mendivil Azagra
Company:       Accenture Zaragoza
Description:   Controller for the VFPage  BI_ListaVisitasEX

History: 

<Date>						<Author>                    				<Code>				<Change Description>
13/12/2017                  Antonio Mendivil Azagra(CoE Zaragoza)         					Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------*/
public class BI_ListaVisitasEX_Controller {
     List<Event> listevt;
	 Boolean isASC=true ;
   	 	 
	 public BI_ListaVisitasEX_Controller(ApexPages.StandardController Controller){
         
     } 
     public BI_ListaVisitasEX_Controller(){
     }

    public PageReference redirectToEvent(){
        //igual no es necesario este metodo... esperar a que se termine la VF
         PageReference pageRef = new PageReference('https://telefonicab2b--coedev.cs20.my.salesforce.com');
       	 pageRef.setRedirect(true);
         return pageRef;
    }
  public Event[] getEventByAccount(){
        listevt=[SELECT id, Location,BI_G4C_esVisita__c, EndDateTime, Description,StartDateTime, Subject, WhatId,BI_FVI_Estado__c,
                BI_FVI_Resultado__c,Who.Name, BI_G4C_Caducada__c,BI_Identificador_Interno__c,Owner.Name,BI_Correo_electronico_enviado__c
                from Event where  whatid=:Apexpages.currentPage().getParameters().get('id') and BI_G4C_esVisita__c=TRUE order by StartDateTime];
          System.debug('Events ::'+listevt);
        return null;
   } 
     public List<Event> getResults(){    
        return listevt;
    }
    public Event[] getEventByAccountOrderBy(){
        
        ID accid=Apexpages.currentPage().getParameters().get('id');
        String fieldToOrder=apexPages.currentPage().getParameters().get('fil');
        System.debug(fieldToOrder);
       String query='SELECT id, Location, EndDateTime,BI_G4C_esVisita__c, Description,StartDateTime, Subject, WhatId,BI_FVI_Estado__c, '+
                'BI_FVI_Resultado__c,Who.Name, BI_G4C_Caducada__c,BI_Identificador_Interno__c,Owner.Name,BI_Correo_electronico_enviado__c '+
                'from Event where whatid=:accid and BI_G4C_esVisita__c=TRUE order by ';
        
        String campo;
        if(fieldToOrder.equals('BI_Identificador_Interno__c')){
            campo=' BI_Identificador_Interno__c';
        }else if(fieldToOrder.equals('Contacto')){
             campo=' Who.Name';
        }else if(fieldToOrder.equals('FechaHora')){
             campo=' StartDateTime';
        }else if(fieldToOrder.equals('Asignado')){
             campo=' Owner.Name';
        }else if(fieldToOrder.equals('Estado')){
              campo=' BI_FVI_Estado__c';
        } else if(fieldToOrder.equals('enviado')){
             campo=' BI_Correo_electronico_enviado__c';
        }else if(fieldToOrder.equals('subject')){
            campo=' Subject';
        }else if(fieldToOrder.equals('isvisita')){
              campo=' BI_G4C_esVisita__c';
        }
        query+=campo;
        
        if(isASC){
             query+=' ASC';
             isASC=false;
        }else{
            query+=' DESC';
            isASC=true;
        } 
        
        System.debug('Query::- '+query);
        listevt=database.query(query);
        System.debug('Events ::'+listevt);
        return null;
    } 
    
}