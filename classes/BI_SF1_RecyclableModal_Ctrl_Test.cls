@isTest
private class BI_SF1_RecyclableModal_Ctrl_Test {
	
	@isTest static void eliminar_Test() {
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Argentina'});
		BI_SF1_RecyclableModal_Ctrl.eliminar(lst_acc[0].Id,'Account');

		List<Account> deleted_accs = [Select Id FROM Account WHERE Id =:lst_acc[0].Id];
		System.assert(deleted_accs.isEmpty());
	}
	
}