/***********************************************************************************************
* Avanxo Colombia
* @author           Andrea Nayibe Castañeda href=<acastaneda@avanxo.com>
* Proyecto:         Telefonica
* Descripción:     Clase relacionada al trigger BI_Attachment_tgr.

* Control de Cambios (Versión)
* -----------------------------------------------------------------------------------------------
*             No.     Fecha                Autor                   Descripción
*           -----   ----------      --------------------    -------------------------------------
* @version   1.0    2015-10-09      Andrea Castañeda (AC)   Clase del Trigger BI_Attachment_tgr
* @version   1.1    2017-08-08      Javier Martinez Sanz    Se añade la línea 58 y se retoca el
*                                                           if de la línea 59 para permitir el 
*                                                           borrado de los casos técnicos de
*                                                           posventa Fase 2
**************************************************************************************************/
public class BI_COL_Attachment_cls 
{
	 public BI_COL_Attachment_cls(List<Attachment> lstAttOld) 
	 {
		  deleteAttachment(lstAttOld);
	 }

	 /**
    * @Method:      deleteAttachment
    * @param:       List<Attachment> lstAttOld 
    * @Description: [AC]: No permite eliminar archivos de Casos
	**/
	 
	public void deleteAttachment(List<Attachment> lstAttOld)
	{
		  Schema.DescribeSObjectResult caseObj =   Case.sObjectType.getDescribe();
		  Schema.DescribeSObjectResult funObj  =   BI_COL_Anexos__c.sObjectType.getDescribe();

		  String      strPrefijoObjCase       =   caseObj.getKeyPrefix();      // Obtener el prefijo del objeto Casos para compararlo con el parentID
		  String      strPrefijoObjFun        =   funObj.getKeyPrefix();      // Obtener el prefijo del objeto FUN para compararlo con el parentID
		  Set<Id>     setIdFun                =   new Set<Id>();
		  Set<String> strUsers                =   new Set<String>();  
		  Set<String> strNoBorrar             =   new Set<String>{strPrefijoObjCase,strPrefijoObjFun};
		            
          List<Attachment> lstAttId = [SELECT ParentId FROM Attachment WHERE Id IN :lstAttOld];
          Set<Id> st_caseId = new Set<Id>();
          for(Attachment at : lstAttId){
              st_caseId.add(at.ParentId);
          }
          List<case> lstCaseId = [SELECT Id FROM case WHERE Id IN :st_caseId AND recordtype.DeveloperName='BIIN_Incidencia_Tecnica'];
    	  Set<Id> setCaseId = new Set<Id>();
          for(Case c :lstCaseId){
            setCaseId.add(c.Id);
          }
            
        
		  List<User> lstUserPermisos = [ SELECT Id, BI_Permisos__c, Pais__c 
													 FROM User 
													 WHERE Pais__c = 'Colombia' and id=:UserInfo.getUserId()];

		  for(User elem : lstUserPermisos)
		  {
				String strId = ''+elem;
				strUsers.add(strId);
		  }
 
		  for (Attachment objAtt: lstAttOld)
		  {
				if(objAtt.ParentId!=null && lstUserPermisos!=null && lstUserPermisos.size()>0)
				{
				 String strInicioId = String.valueOf(objAtt.ParentId).substring(0, 3);
				 System.debug(strUsers+'>>>>>'+UserInfo.getProfileId());
                    
					if(!setCaseId.contains(objAtt.ParentId) && strNoBorrar.contains(strInicioId) && lstUserPermisos.get(0).BI_Permisos__c!='Jefe de Ventas')
					{
						objAtt.addError('No se pueden eliminar archivos');
					}else{
		 
 					  if(strInicioId.equals(strPrefijoObjFun) && !setIdFun.contains(objAtt.ParentId))
					  {
							System.debug('Comparando los prefijos:'+strPrefijoObjFun+' y '+strInicioId);
							setIdFun.add(objAtt.ParentId);
					  }
					}
				}
		   }

		   noDeleteAttachment(setIdFun,lstAttOld);

	 }

	 /**
    * @Method:      noDeleteAttachment
    * @param:       Set<Id> setIdsFun, List<Attachment> lstAttOld2
    * @Description: [AC]: No elimina Attachment de caso porque no coincide con la etapa de la oporunidad
    *							  F1 - Cancelled | Suspended
	**/

	public void noDeleteAttachment(Set<Id> setIdsFun, List<Attachment> lstAttOld2)
	{
		 Map<Id,BI_COL_Anexos__c> mapFun     =   new Map<Id,BI_COL_Anexos__c>();     

		if(setIdsFun.size()>0)
		{
		
			List<BI_COL_Anexos__c> lstFun = [SELECT Id,BI_COL_Contrato__r.BI_Oportunidad__r.StageName 
															FROM BI_COL_Anexos__c 
															WHERE Id IN: setIdsFun];
		
			for(BI_COL_Anexos__c fun : lstFun)
			{
				mapFun.put(fun.Id,fun);
			}
			
			BI_COL_Anexos__c tmp;
			
			for(Attachment objAtts: lstAttOld2)
			{
				tmp = mapFun.get(objAtts.ParentId);
				
				if(tmp.BI_COL_Contrato__r.BI_Oportunidad__r.StageName == 'F1 - Cancelled | Suspended')
				{
					objAtts.addError('El registro no se puede eliminar debido a que la oportunidad a la que pertenece esta en estado F1 - Cerrada/Legalizada');
				}
			}
		}
	}
}