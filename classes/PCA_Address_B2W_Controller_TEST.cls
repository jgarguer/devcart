@isTest
private class PCA_Address_B2W_Controller_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Description:   Test method to manage the code coverage for PCA_Address_B2W_Controller_TEST.init
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getloadInfo() {
        User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem=TGS_Dummy_Test_data.dummyConfiguration();
            Id testCatalogItemId = testOrderItem.NE__CatalogItem__c;
            NE__Catalog_Item__c testCatalogItem=[SELECT NE__Technical_Behaviour__c FROM NE__Catalog_Item__c WHERE Id=:testCatalogItemId][0];
            testCatalogItem.NE__Technical_Behaviour__c='Attachment Required';
            update testCatalogItem;
            Id testOrderId=testOrderItem.NE__OrderId__c;
            NE__Order__c testOrder=[SELECT Id, Case__c FROM NE__Order__c WHERE Id=:testOrderId][0];
            Id testCaseId= testOrder.Case__c;
            
            Test.startTest();
            ApexPages.currentPage().getParameters().put('cId', testCaseId);
            ApexPages.currentPage().getParameters().put('orderId', testOrderId);
            PCA_Address_B2W_Controller controller = new PCA_Address_B2W_Controller();
            System.assertEquals(testCaseId, controller.myCase.Id);
            controller.nextStep();
            
            Delete testOrder;
            Delete controller.myCase;
            controller.nextStep();
            Test.stopTest();
        }
    }
    
    

    
     
}