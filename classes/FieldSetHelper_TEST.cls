@isTest
private class FieldSetHelper_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test Class to cover FieldSetHelper class.

    History: 
    
     <Date>                     <Author>                <Change Description>
    06/10/2014                  Micah Burgos             Initial Version
    29/12/2015					Guillermo Muñoz			Coverage of FieldSetHelperSearchWithCondition method added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    @isTest static void FieldSetHelper_Order_TEST() {

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	BI_TestUtils.throw_exception = false;
    	List <String> lst_region = BI_DataLoad.loadPaisFromPickList(1);
		
		List<Account> accounts = BI_DataLoad.loadAccountsPaisStringRef(1, lst_region);
	
		Opportunity opp = new Opportunity(Name = 'Test',
                                          CloseDate = Date.today(),
                                          StageName =  Label.BI_F6Preoportunidad,
                                          AccountId = accounts[0].Id,
                                          BI_Country__c = lst_region[0],
                                          BI_Ciclo_ventas__c = Label.BI_Completo);
	                                              
		insert opp;
			                                              
		NE__Order__c orderC = new NE__Order__c(NE__OptyId__c=opp.Id, 
											   NE__OrderStatus__c = Label.BI_LabelActive);
		insert orderC;

		FieldSetHelper.FieldSetContainer container = FieldSetHelper.FieldSetHelperSearch('PCA_DetailPedido', 'NE__Order__c', 'NE__OrderStatus__c;', 'Id;' + orderC.Id,'asc');
		system.assert(container != null);

		container = FieldSetHelper.FieldSetHelperSearchChildren('PCA_DetailPedido', 'NE__Order__c', 'NE__OrderStatus__c;', 'Id;' + orderC.Id,'asc', null);
		system.assert(container != null);

		list<FieldSetHelper.FieldSetResult> result = FieldSetHelper.FieldSetHelperSimple('PCA_DetailPedido','NE__Order__c', orderC.Id);
		system.assert(result != null);
		system.assert(!result.isEmpty());
    }

    @isTest static void  defineSearchFields_TEST() {
    	BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	FieldSetHelper.defineSearchFields('PCA_MainTableFacturas','BI_Facturas__c');
    	
    	system.assert([select Id from BI_Log__c limit 1].isEmpty());
    }
	    
	@isTest static void FieldSetHelper_Case_TEST() {
		BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		//Simulate Data:
		Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), false, true, false, false);
		List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1);
		List <Account> lst_acc =BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
		List <Contact> lst_con =BI_DataLoad.loadContacts(1, lst_acc);

        BI_Facturacion__c fact1 = new BI_Facturacion__c();
        fact1.BI_Cliente__c = lst_acc[0].Id;
        insert fact1;

       	Case cas = new Case();
       	cas.BI_Facturacion__c = fact1.Id;
       	cas.Type = 'Reclamo';
       	cas.Subject = 'Caso test';
       	cas.BI_Otro_Tipo__c = 'Test';
       	cas.Status = 'Assigned';
       	cas.BI2_PER_Tipologia__c = 'Test';
        cas.BI_Product_Service__c = ' Test';
        cas.BI2_per_subtipologia__c = 'Test'; 
        cas.BI_Line_of_Business__c = 'Test';
        cas.TGS_Disputed_number_of_lines__c = 120;
        cas.BI2_PER_Territorio_caso__c = 'Test';
       	cas.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI2_Caso_comercial' LIMIT 1].Id;
       	cas.BI_Confidencial__c=false;
		cas.ContactId = lst_con[0].Id;
        insert cas;

        Case cas2 = new Case();
       	cas2.BI_Facturacion__c = fact1.Id;
       	cas2.Type = 'Reclamo';
       	cas2.Subject = 'Caso test';
       	cas2.BI_Otro_Tipo__c = 'Test';
       	cas2.Status = 'Assigned';
       	cas2.BI2_PER_Tipologia__c = 'Test';
        cas2.BI_Product_Service__c = ' Test';
        cas2.BI2_per_subtipologia__c = 'Test'; 
        cas2.BI_Line_of_Business__c = 'Test';
        cas2.TGS_Disputed_number_of_lines__c = 120;
        cas2.BI2_PER_Territorio_caso__c = 'Test';
       	cas2.RecordTypeId = cas.RecordTypeId;
       	cas2.BI_Confidencial__c=false;
       	cas2.ParentId = cas.Id;
		cas2.ContactId = lst_con[0].Id;
        insert cas2;
        BI_MigrationHelper.disableBypass(mapa);
	    //End simulate data
	    
	    Test.startTest();
		
		FieldSetHelper.FieldSetContainer container = FieldSetHelper.FieldSetHelperSearch('BI2_MainTableCase', 'Case', 'Subject;Test::Status;Asignado;Status::Status;Assigned;Estado', 'BI_Facturacion__c;' + fact1.Id,'asc');
		system.assert(container != null);

		container = FieldSetHelper.FieldSetHelperSearchChildren('BI2_MainTableCase', 'Case', 'Subject;Test::BI_COL_Fecha_Radicacion__c', 'BI_Facturacion__c;' + fact1.Id,'asc', 'ParentId;' + cas.Id);
		system.assert(container != null);

		list<FieldSetHelper.FieldSetResult> result = FieldSetHelper.FieldSetHelperSimple('PCA_DetailCaseComercial','Case', cas.Id);
		system.assert(result != null);
		system.assert(!result.isEmpty());
		
		Test.stopTest();

	}

	@isTest static void FieldSetHelper_Fact_TEST() {
		BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		//Simulate Data: 
			List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1);
			List <Account> lst_acc =BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);

	        BI_Facturacion__c fact1 = new BI_Facturacion__c();
	        fact1.BI_Cliente__c = lst_acc[0].Id;
	        insert fact1;
	    //End simulate data

		FieldSetHelper.FieldSetContainer container = FieldSetHelper.FieldSetHelperSearch('PCA_MainTableFacturacion', 'BI_Facturacion__c', '', 'BI_Cliente__c;' + lst_acc[0].Id,'asc');
		system.assert(container != null);

		container = FieldSetHelper.FieldSetHelperSearchChildren('PCA_MainTableFacturacion', 'BI_Facturacion__c', '', 'BI_Cliente__c;' + lst_acc[0].Id,'asc', null);
		system.assert(container != null);

		container = FieldSetHelper.FieldSetHelperSearchWithCondition('PCA_MainTableFacturacion', 'BI_Facturacion__c', 'BI_Cliente__c = \'' + lst_acc[0].Id +'\'' , null);
		system.assert(container != null);

		list<FieldSetHelper.FieldSetResult> result = FieldSetHelper.FieldSetHelperSimple('PCA_MainTableFacturacion', 'BI_Facturacion__c', fact1.Id);
		system.assert(result != null);
		system.assert(!result.isEmpty());

	}

	@isTest static void FieldSetHelper_Opp_TEST() {
		BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		list<String> lst_reg = BI_DataLoad.loadPaisFromPickList(1);
		list<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_reg);
		list<Opportunity> lst_opp = BI_DataLoad.loadOpportunitiesforButton(1, lst_acc[0].Id);
		
		lst_opp[0].BI_recurrente_bruto_mensual__c = 1;
		update lst_opp;

		FieldSetHelper.FieldSetContainer container = FieldSetHelper.FieldSetHelperSearch('PCA_DetailOportunidad', 'Opportunity', '', 'AccountId;' + lst_acc[0].Id,'asc');
		system.assert(container != null);

		container = FieldSetHelper.FieldSetHelperSearchChildren('PCA_DetailOportunidad', 'Opportunity', '', 'AccountId;' + lst_acc[0].Id,'asc', null);
		system.assert(container != null);

		container = FieldSetHelper.FieldSetHelperSearchWithCondition('PCA_DetailOportunidad', 'Opportunity', 'AccountId = \'' + lst_acc[0].Id +'\'' , 'CreatedDate;asc');
		system.assert(container != null);

		list<FieldSetHelper.FieldSetResult> result = FieldSetHelper.FieldSetHelperSimple('PCA_DetailOportunidad','Opportunity', lst_opp[0].Id);
		system.assert(result != null);
		system.assert(!result.isEmpty()); 
	}

	@isTest static void textAreaFields_TEST() {
		BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		Set<String> result = FieldSetHelper.textAreaFields('PCA_DetailCaseComercial', 'Case');
		system.assert(result != null);
		system.assert(!result.isEmpty());
	}
	
	@isTest static void FieldSetHelper_catch_TEST() {
		//Methods
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_TestUtils.throw_exception = true;

		FieldSetHelper.FieldSetContainer container = FieldSetHelper.FieldSetHelperSearch(null,null,null,null,null);
		system.assert(container == null);
		container = FieldSetHelper.FieldSetHelperSearchChildren(null,null,null,null,null, null);
		system.assert(container == null);
		list<FieldSetHelper.FieldSetResult> result = FieldSetHelper.FieldSetHelperSimple(null,null,null);
		system.assert(result == null);
		container = FieldSetHelper.FieldSetHelperSearchWithCondition(null, null, null, null);
		
		FieldSetHelper.defineSearchFields(null,null);
		FieldSetHelper.textAreaFields(null,null);

		//Wrapper Class: 
		FieldSetHelper.FieldSetResult resul = new FieldSetHelper.FieldSetResult(null, null);
		FieldSetHelper.FieldSetRecord record= new FieldSetHelper.FieldSetRecord(null, null);
		FieldSetHelper.FieldSetContainer cont = new FieldSetHelper.FieldSetContainer(null, null);
		cont = new FieldSetHelper.FieldSetContainer(null, null, null, null);
		
		system.assertEquals(false, [select Id from BI_Log__c limit 1].isEmpty());
	}
	/*  quitar
	 @isTest public static void test_exceptions() {
        BI_TestUtils.throw_exception = true;
        FieldSetHelper FSHelper = new FieldSetHelper();
        List<String> heads=new List<String>();
        Map<String, String> apiheaders=new Map<String, String> ();
        List<String> apis=new List<String> ();
        FieldSetHelper.FieldSetRecord FieldSetRecord;
       //List<FieldSetRecord> regs=new List <FieldSetRecord>();
             




	FSHelper.FieldSetContainer(heads, apiheaders, apis, regs);

	}
	*/
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}