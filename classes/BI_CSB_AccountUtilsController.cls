public with sharing class BI_CSB_AccountUtilsController {

    public Blob csvFileBody{get;set;}
    public string csvAsString{get;set;}
    public String[] csvFileLines{get;set;}
    public List<Account> acclist{get;set;}
    public List<Account> acclistNotFound{get;set;}
    public List<Account> acclistInvalid{get;set;}
    public List<Account> acclistToProcess{get;set;}
    public List<Account> acclistErrors {get; set;}
    public Map<Id, String> mapErrors {get; set;}
   // public List<account> acclistActivos{get;set;}
    public Map <String,String> mapCuent;
    public Set<String> setIds {get;set;}
    public Boolean mostrar{get;set;}
    public Boolean seleccionado{get;set;}
    public Boolean archivoCargado{get;set;}
    public Boolean finished {get; set;}
    // FAR 11/04/2016
    public static Boolean fireTriggerAccountSuspension = false;
    
    public static void setFireTriggerAccountSuspension(Boolean value)
    {
    	fireTriggerAccountSuspension = value;
    }
    
    public String seleccionar {
        get
        {
            seleccionado=true;
            return seleccionar;
        }
        set;
    }
    
    private String estado {get; set;}

    public BI_CSB_AccountUtilsController()  {
        csvFileLines = new String[]{};
        acclist = New List<Account>(); 
        acclistToProcess = New List<Account>(); 
        acclistNotFound = New List<Account>(); 
        acclistInvalid = New List<Account>();
        acclistErrors = New List<Account>();
        mapErrors = new Map<Id, String>(); 
        setIds=New Set<String>();
        mostrar=false;
        seleccionado=false;
        archivoCargado=false;
        estado = null;
        seleccionar = null;
        finished = false;
    }


    public PageReference actionSave() {
       seleccionado=true;
       habilitarProcesar();
       limpiar();
        return null;
    }


    public List<SelectOption> getItems() {

        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('Suspender','Suspender'));
        options.add(new SelectOption('Reactivar','Reactivar'));
        
       return options;
    }

    public void importCSVFile()
    {
        try
        {
            csvAsString=csvFileBody.toString().replaceAll('"', '');
            csvFIleLines=csvAsString.split('\r\n');
    
            //hacemos el for para cada registro del archivo
    
            for(String strAccountId : csvFIleLines){
    
                //System.debug('strAccountId:' + strAccountId);
                //Id accId = Id.valueOf(strAccountId);
                System.debug('strAccountId:' + strAccountId);
                if (strAccountId != null && !setIds.contains(strAccountId))
                {
    
                    setIds.add(strAccountId);
                    Account accObject= new Account(Id = strAccountId.substring(0,15));
    
                    //String [] csvRecordData= csvFIleLines[i].split(',');
    
                    //accObject.Id = strAccountId;
                    // accObject.Name=csvRecordData[1];
                    //accObject.BI_Activo__c=csvRecordData[2];
                    //accObject.BI_COL_Estado__c=csvRecordData[2];
    
                    //accObject.BI_No_Identificador_fiscal__c=csvRecordData[3];
                    acclist.add(accObject);
                }
    
            }
    
            estado = seleccionar == 'Suspender' ? Label.BI_CSB_Activo : Label.BI_CSB_Suspendido;
            archivoCargado=true;
    
            Map<Id,Account> mapacuenta=new Map<Id,Account>([Select Id,Name,BI_COL_Estado__c from Account where Id IN :setIds ]);
    
            System.debug('mapacuenta:' + mapacuenta);
            for(Account item : acclist){
    
                System.debug('item.Id:' + item.Id);
                Account cuenta=mapacuenta.get(item.Id); //obtengo el dato de la select
    
                if(mapacuenta.containsKey(item.Id) && mapacuenta.get(item.Id).BI_COL_Estado__c==estado){
    
                //lista cuentas si
                acclistToprocess.add(cuenta);
    
    
                }else if(mapacuenta.containsKey(item.Id) && mapacuenta.get(item.Id).BI_COL_Estado__c!=estado){
    
                acclistInvalid.add(cuenta);
    
                }else if(!mapacuenta.containsKey(item.Id)){
    
                acclistNotFound.add(item);
    
                }
            }
            
            mapErrors.clear();
            acclistErrors.clear();
        }
        catch(Exception e)
        {
            ApexPages.Message  errorMessage= new ApexPages.Message(ApexPages.Severity.Error, 'An error has occured while importing data. Please make sure input csv file is correct');
            ApexPages.addMessage(errorMessage);
        }
    }

   public void Process(){

    for(Account item :acclistToProcess)
    {
        if(seleccionar.equals('Suspender')){
            item.BI_COL_Estado__c= Label.BI_CSB_Pendiente_Suspension;

            }else if (seleccionar.equals('Reactivar'))

        item.BI_COL_Estado__c= Label.BI_CSB_Pendiente_Reactivacion;
    }
    
    Set<Integer> setIndexes = new Set<Integer>();

    // FAR 11/04/2016
    setFireTriggerAccountSuspension(true);
    
    List<Database.SaveResult> listSR = Database.update(acclistToProcess, false);
    for (Integer i = 0; i < listSR.size(); i++)
    {
        if (!listSR[i].isSuccess())
        {
            acclistErrors.add(acclistToProcess[i]);
            setIndexes.add(i);
            mapErrors.put(acclistToProcess[i].Id, listSR[i].getErrors()[0].getMessage());
        }
    }
    
    for (Integer i : setIndexes)
        acclistToProcess.remove(i);

    acclistNotFound = null;
    acclistInvalid = null;
    
    finished = true;
 }

    public void limpiar(){
        acclistToProcess.clear();
        acclistInvalid.clear();
        acclistNotFound.clear();
    }

    public void habilitarProcesar(){
        archivoCargado=false;
    }

}