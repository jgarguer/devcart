@isTest
private class BI_MigrationHelper_TEST {


	static{
		BI_TestUtils.throw_exception=false;
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the coverage of BI_MigrationHelper.enableBypass and BI_MigrationHelper.disableBypass()
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    10/12/2015                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void enableDisableBypass() {
		

		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
        BI_Dataload.set_ByPass(false,false);
        BI_TestUtils.throw_exception=false;

        List <User> lst_usu = BI_Dataload.loadUsers(1, [SELECT Id FROM Profile WHERE Name =: Label.BI_Administrador_Sistema LIMIT 1].Id, 'Administrador del sistema');

        Map<Integer,BI_bypass__c> lastInstance = BI_MigrationHelper.enableBypass(lst_usu[0].Id, true, true, true, true);
        BI_MigrationHelper.disableBypass(lastInstance);

        List <BI_bypass__c> lst_bypass = [SELECT Id FROM BI_bypass__c WHERE SetupOwnerId =: lst_usu[0].Id];
        System.assertEquals(lst_bypass.isEmpty(),true);

        BI_bypass__c cs = new BI_bypass__c(
					SetupOwnerId = lst_usu[0].Id,
					BI_migration__c = false,
					BI_skip_assert__c = false,
					BI_stop_job__c = false,
					BI_skip_trigger__c = false
		);
		insert cs;

		lastInstance = BI_MigrationHelper.enableBypass(lst_usu[0].Id, true, true, true, true);
        BI_MigrationHelper.disableBypass(lastInstance);

        lst_bypass = [SELECT Id,BI_migration__c,BI_skip_assert__c,BI_skip_trigger__c,BI_stop_job__c,SetupOwnerId FROM BI_bypass__c WHERE SetupOwnerId =: lst_usu[0].Id];
        System.assertEquals(lst_bypass.size(),1);
        System.assertEquals(lst_bypass[0].BI_migration__c,false);
        System.assertEquals(lst_bypass[0].BI_skip_assert__c,false);
        System.assertEquals(lst_bypass[0].BI_skip_trigger__c,false);
        System.assertEquals(lst_bypass[0].BI_stop_job__c,false);

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the coverage of all exceptions of BI_MigrationHelper class
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    10/12/2015                      Guillermo Muñoz             Initial version
	    02/03/2016                      Guillermo Muñoz             Changed by new develop
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void exceptions(){

        BI_TestUtils.throw_exception=true;

        BI_MigrationHelper.enableBypass(null, true, true, true, true);
        BI_MigrationHelper.disableBypass(null);
        //BI_MigrationHelper.bypassDeleteDML(null, true, true, true, true, null, true);
        BI_MigrationHelper.bypassInsertUpdateDML(null, true, true, true, true, null, null, true);
        BI_MigrationHelper.updateCaseWithoutAssignationsAndTrigger(null);

        BI_TestUtils.throw_exception=false;

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the code coverage of BI_MigrationHelper.bypassInsertUpdateDML
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    02/03/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	@isTest static void bypassInsertUpdateDMLWithoutInstance(){
		///Si este test falla en alguna ocasión es probable que sea porque la casuística creada no genera una excepción
		///ya que para testear este desarrollo es necesaria una casuistica que haga que salte alguna regla de validación
		///para comprobar que la habilitación del bypass funciona de forma correcta.
		List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{Label.BI_Argentina});
		List <Opportunity> lst_opp = new List <Opportunity>();
		List <Database.SaveResult> lst_dbsr;
        User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();//este método carga un usuario sin instancia en el bypass
        
        Opportunity opp = new Opportunity(Name = 'Test',
        	CloseDate = Date.today().addDays(-3),//casuística que provoca la excepción
        	BI_Requiere_contrato__c = true,
        	Generate_Acuerdo_Marco__c = false,
        	AccountId = lst_acc[0].id,
        	StageName = Label.BI_F2Negociacion,
        	BI_Ultrarapida__c = true,
        	BI_Duracion_del_contrato_Meses__c = 3,
        	BI_Recurrente_bruto_mensual__c = 100.12,
        	BI_Ingreso_por_unica_vez__c = 12.1,
        	BI_Recurrente_bruto_mensual_anterior__c = 1,
        	BI_Opportunity_Type__c = 'Producto Standard',
        	Amount = 23,
        	BI_Country__c = Label.BI_Argentina
        );
        lst_opp.add(opp);

        System.assertEquals([SELECT Id FROM BI_bypass__c WHERE SetupOwnerId =: usu.Id].isEmpty(),true);

        System.runAs(usu){
        	/////////////////////////////INSERT OPERATION///////////////////////////////////
	        try{    

	        	insert lst_opp;
	        	throw new BI_Exception('No salta ninguna validacion, cambiar los criterios para que salte cualquier validación');
	        }
	        catch(DMLException e){}

	        lst_dbsr = BI_MigrationHelper.bypassInsertUpdateDML(usu.Id, true, true, false, false, lst_opp, 'insert', true);

	        System.assertEquals([SELECT Id FROM BI_bypass__c WHERE SetupOwnerId =: usu.Id].isEmpty(),true);

	        System.assertNotEquals(lst_dbsr, null);

	        lst_dbsr = null;

	        ////////////////////////////UPDATE OPERATION///////////////////////////////////
	        try{

	        	update lst_opp;
	        	throw new BI_Exception('No salta ninguna validacion, cambiar los criterios para que salte cualquier validación');
	        }
	        catch(DMLException e){}

	        lst_dbsr = BI_MigrationHelper.bypassInsertUpdateDML(usu.Id, true, true, false, false, lst_opp, 'update', true);

	        System.assertEquals([SELECT Id FROM BI_bypass__c WHERE SetupOwnerId =: usu.Id].isEmpty(),true);

	        System.assertNotEquals(lst_dbsr, null);
    	}
	}
	/*

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the code coverage of BI_MigrationHelper.bypassInsertUpdateDML
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    02/03/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	/*
	@isTest static void bypassInsertUpdateDMLWithInstance(){
		///Si este test falla en alguna ocasión es probable que sea porque la casuística creada no genera una excepción
		///ya que para testear este desarrollo es necesaria una casuistica que haga que salte alguna regla de validación
		///para comprobar que la habilitación del bypass funciona de forma correcta.
		List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{Label.BI_Argentina});
		List <Opportunity> lst_opp = new List <Opportunity>();
		List <Database.SaveResult> lst_dbsr;
        User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();//este método carga un usuario sin instancia en el bypass
        Opportunity opp = new Opportunity(Name = 'Test',
        	CloseDate = Date.today().addDays(-3),//casuística que provoca la excepción
        	BI_Requiere_contrato__c = true,
        	Generate_Acuerdo_Marco__c = false,
        	AccountId = lst_acc[0].id,
        	StageName = Label.BI_F2Negociacion,
        	BI_Ultrarapida__c = true,
        	BI_Duracion_del_contrato_Meses__c = 3,
        	BI_Recurrente_bruto_mensual__c = 100.12,
        	BI_Ingreso_por_unica_vez__c = 12.1,
        	BI_Recurrente_bruto_mensual_anterior__c = 1,
        	BI_Opportunity_Type__c = 'Producto Standard',
        	Amount = 23,
        	BI_Country__c = Label.BI_Argentina
        );
        lst_opp.add(opp);

        BI_bypass__c cs = new BI_bypass__c(
			SetupOwnerId = usu.Id,
			BI_migration__c = false,
			BI_skip_assert__c = false,
			BI_stop_job__c = false,
			BI_skip_trigger__c = false
		);
		insert cs;

        System.assertEquals([SELECT Id FROM BI_bypass__c WHERE SetupOwnerId =: usu.Id AND BI_migration__c = false AND BI_skip_assert__c = false AND BI_stop_job__c = false AND BI_skip_trigger__c = false].isEmpty(),false);
        System.runAs(usu){
        	/////////////////////////////INSERT OPERATION///////////////////////////////////
	        try{                       
	        	insert lst_opp;
	        	throw new BI_Exception('No salta ninguna validacion, cambiar los criterios para que salte cualquier validación');
	        }
	        catch(DMLException e){}

	        lst_dbsr = BI_MigrationHelper.bypassInsertUpdateDML(usu.Id, true, true, false, false, lst_opp, 'insert', true);

	       System.assertEquals([SELECT Id FROM BI_bypass__c WHERE SetupOwnerId =: usu.Id AND BI_migration__c = false AND BI_skip_assert__c = false AND BI_stop_job__c = false AND BI_skip_trigger__c = false].isEmpty(),false);

	        System.assertNotEquals(lst_dbsr, null);
	        lst_dbsr = null;

	        ////////////////////////////UPDATE OPERATION///////////////////////////////////
	        try{
	        	update lst_opp;
	        	throw new BI_Exception('No salta ninguna validacion, cambiar los criterios para que salte cualquier validación');
	        }
	        catch(DMLException e){}

	        lst_dbsr = BI_MigrationHelper.bypassInsertUpdateDML(usu.Id, true, true, false, false, lst_opp, 'update', true);

	        System.assertEquals([SELECT Id FROM BI_bypass__c WHERE SetupOwnerId =: usu.Id AND BI_migration__c = false AND BI_skip_assert__c = false AND BI_stop_job__c = false AND BI_skip_trigger__c = false].isEmpty(),false);


	        System.assertNotEquals(lst_dbsr, null);
    	}
	}
	*/

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the code coverage of BI_MigrationHelper.updateCaseWithoutAssignationsAndTrigger
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    03/01/2017                      Guillermo Muñoz             Initial version
	    23/05/2017		  				Álvaro López				Added Internal Case RecordType Label to avoid null object when try to getRecordTypeId in other language.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void updateCaseWithoutAssignationsAndTrigger_TEST(){

		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{Label.BI_Argentina});
		List <Case> lst_cas = new List <Case>();

        User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();

        String rt_internalCase = Label.BI_O4_Caso_Interno;
        Id rt_casoInterno = Schema.SObjectType.Case.getRecordTypeInfosByName().get(rt_internalCase).getRecordTypeId();

        //////////Insertamos un caso////////////
        Case cas = new Case(
            Subject = 'Test case',
            AccountId = lst_acc[0].Id,
            RecordTypeId = rt_casoInterno,
            BI_Country__c = lst_acc[0].BI_Country__c,
            Status = 'New',
            BI_Department__c = 'Calidad',
            BI_Type__c = 'Comercial'
        );
        insert cas;
        lst_cas.add(cas);

        Test.startTest();

        //lanzamos el método con un usuario sin instancia previa en el bypass
        System.runAs(usu){

        	BI_MigrationHelper.updateCaseWithoutAssignationsAndTrigger(lst_cas);
        }

        List <BI_bypass__c> lst_bypass = [SELECT Id FROM BI_bypass__c WHERE SetupOwnerId =: usu.Id];

        //Comprobamos que sigue sin tener instancia en el bypass porque el método la ha borrado
        system.assert(lst_bypass.isEmpty());

        //creamos una instancia en el bypass para el usuario
        BI_bypass__c bypass = new BI_bypass__c(
        	SetupOwnerId = usu.id,
        	BI_skip_trigger__c = false,
        	BI_Skip_case_assignations__c = false
        );
        insert bypass;

        //lanzamos el método con un usuario con instancia previa en el bypass
        System.runAs(usu){

        	BI_MigrationHelper.updateCaseWithoutAssignationsAndTrigger(lst_cas);
        }

        lst_bypass = [SELECT Id FROM BI_bypass__c WHERE SetupOwnerId =: usu.Id AND BI_skip_trigger__c = false AND BI_Skip_case_assignations__c = false];

        //Comprobamos que la instancia que tiene en el bypass es la misma que insertamos antes porque el mñetodo la ha actualizado
        System.assert(!lst_bypass.isEmpty());

        Test.stopTest();

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Accenture
	    Description:   Test method that manage the code coverage of all methods related to BI_MigrationHelper.set_skippedTriggers vartiable.
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    19/07/2017                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void setTriggerSkipped_TEST(){

		Boolean aux;

		//Desactivamos y comprobamos que se ha desactivado el trigger de Oportunidades
		aux = BI_MigrationHelper.setSkippedTrigger('Opportunity');
		System.assert(aux);
		aux = BI_MigrationHelper.isTriggerDisabled('Opportunity');
		System.assert(aux);

		//Comprobamos que el trigger de casos (por ejemplo) no se ha desactivado
		aux = BI_MigrationHelper.isTriggerDisabled('Case');
		System.assertEquals(aux, false);

		//Intentamos desactivar un trigger de un objeto que no existe y comprobamos que no se ha realizado
		aux = BI_MigrationHelper.setSkippedTrigger('Bad object');
		System.assertEquals(aux, false);

		//Desactivamos todos los triggers y comprobamos que se han sustituido todos los valores que hubiera por el valor 'all'
		BI_MigrationHelper.skipAllTriggers();
		System.assertEquals(BI_MigrationHelper.set_skippedTriggers.size(), 1);
		System.assert(BI_MigrationHelper.set_skippedTriggers.contains('all'));

		//Comprobamos que al desactivar todo los triggers el trigger casos (por ejemplo) se ha desactivado
		aux = BI_MigrationHelper.isTriggerDisabled('Case');
		System.assert(aux);

		//Comprobamos que con todos los triggers desactivaods, si intentamos comprobar si los triggers de un objeto que no existe están activados nos devuelve false
		aux = BI_MigrationHelper.isTriggerDisabled('Bad object');
		System.assertEquals(aux, false);

		//desactivamos el trigger de oportunidades, comprobamos que el valor 'all' se ha borrado y comprobamos que el trigger de Casos (por ejemplo) ya no está desactivado
		aux = BI_MigrationHelper.setSkippedTrigger('Opportunity');
		System.assert(aux);
		System.assertEquals(BI_MigrationHelper.set_skippedTriggers.contains('all'), false);
		aux = BI_MigrationHelper.isTriggerDisabled('Case');
		System.assertEquals(aux, false);

		//Volvemos a activar todos los triggers y comprobamos que la lista de triggers desactivados esta vacía
		BI_MigrationHelper.cleanSkippedTriggers();
		System.assert(BI_MigrationHelper.set_skippedTriggers.isEmpty());
	}
	
}