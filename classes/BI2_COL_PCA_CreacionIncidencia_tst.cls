@isTest
private class BI2_COL_PCA_CreacionIncidencia_tst {
    static testMethod void BI2_COL_PCA_CreacionIncidencia_ctr_TEST() {

        BI_TestUtils.throw_exception = false;

        User me = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];

        List<Account> lst_acc = BI_DataLoad.loadAccounts(1, new List <String>{'Colombia'});
        System.assertEquals(lst_acc.isEmpty(), false);

        List<RecordType> lst_rt = [SELECT  Id, DeveloperName FROM Recordtype WHERE SObjectType = 'Case'];
        System.assertEquals(lst_rt.isEmpty(), false);

        Map <String, RecordType> map_rt = new Map <String, RecordType>();
        for(RecordType rt : lst_rt){
            map_rt.put(rt.DeveloperName, rt);
        }

        System.debug('\n\n-=#=-\n' + 'map_rt' + ': ' + map_rt + '\n-=#=-\n');

        insert new BI_bypass__c(BI_migration__c = true);

        List<Contact> lst_con = BI_DataLoad.loadContacts(1, lst_acc);

        System.debug('\n\n-=#=-\n' + 'lst_con - salida' + ': ' + lst_con + '\n-=#=-\n');
        //System.assertEquals(lst_con.isEmpty(), false);

        Id profId = [SELECT Id FROM Profile WHERE Name =: Label.BI_Customer_Portal_Profile].Id;

        User usu = BI_DataLoad.loadSeveralPortalUsers(lst_con[0].Id, profId);
        usu.Pais__c = 'Colombia';
        System.runAs(me){
            update usu;
        }

        BI_Configuracion_Caso_PP__c ccpp = new BI_Configuracion_Caso_PP__c(
            BI_Confidencial__c = false,
            BI_COL_Fecha_Radicacion__c = true,
            BI_Type__c = 'First instance',
            BI_Tipos_de_registro__c = Label.BI_Caso_agrupador,
            BI_Country__c = lst_acc[0].BI_Country__c,
            BI_Origen__c = Label.BI_OrigenCasoPortalCliente
        );
        insert ccpp;


        System.runAs(usu){
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new TGS_MockHttpResponseGenerator());

            /////////////////////Caso Agrupador//////////////////////
            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'TEST - Caso Agrupador' + '   <<<<<<<<<<\n-=#=-\n');
            BI2_COL_PCA_CreacionIncidencia_ctr obj = new BI2_COL_PCA_CreacionIncidencia_ctr();
            obj.checkPermissions();
            obj.loadInfo();
            obj.changeCaseType();
            obj.getItems();
            obj.getFields();
            obj.newCase.Reason = 'Test';
            obj.newCase.Subject = 'Test';
            obj.newCase.Description = 'Test';
            obj.saveCase();

            /////////////////////Caso Comercial//////////////////////
            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'TEST - Caso Comercial' + '   <<<<<<<<<<\n-=#=-\n');
            ccpp.BI_Tipos_de_registro__c = Label.BI_Caso_comercial_2;
            update ccpp;
            BI2_COL_PCA_CreacionIncidencia_ctr obj2 = new BI2_COL_PCA_CreacionIncidencia_ctr();
            obj2.checkPermissions();
            obj2.loadInfo();
            obj2.changeCaseType();
            obj2.getItems();
            obj2.getFields();
            obj2.newCase.Reason = 'Test';
            obj2.newCase.Subject = 'Test';
            obj2.newCase.Description = 'Test';
            obj2.saveCase();

            ////////////////////Incidencia Técnica//////////////////////
            System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'TEST - Incidencia Tecnica' + '   <<<<<<<<<<\n-=#=-\n');
            ccpp.BI_Tipos_de_registro__c = Label.BIIN_TituloSolicInciden;
            update ccpp;
            BI2_COL_PCA_CreacionIncidencia_ctr obj3 = new BI2_COL_PCA_CreacionIncidencia_ctr();
            String Tipo = obj3.Tipo;
            Id IdAttach = obj3.IdAttach;
            String prev = obj3.prev;
            Id siteInput = obj3.siteInput;
            String CIname = obj3.CIname;
            Id CIinput = obj3.CIinput;
            String StringWSCreacionPortal = obj3.StringWSCreacionPortal;
            String country = obj3.country;
            User objUser = obj3.objUser;
            obj3.checkPermissions();
            obj3.loadInfo();
            obj3.changeCaseType();
            obj3.getItems();
            obj3.getFields();
            obj3.newCase.Reason = 'Test';
            obj3.newCase.Subject = 'Test';
            obj3.newCase.Description = 'Test';
            obj3.GuardarUbicacion();
            ////////////////////Guardar Caso Sin Consultar//////////////////////
            obj3.saveCase();
            ////////////////////Guardar Caso despues de COnsultar//////////////////////
            obj3.consultarContactoRoD();
            obj3.saveCase();

            PCA_CaseHelper.saveCase(obj3.newCase);
            obj3.adjuntos = false;
            obj3.crearTicketRoD();

            obj3.newCase.BI_COL_Fecha_Radicacion__c=null;
            ////////////////////Guardar Caso Sin Consultar//////////////////////
            obj3.blnContactoConsultado=false;
            obj3.saveCaseAdjunto();

            obj3.attachFile();
            ////////////////////Guardar Caso despues de COnsultar//////////////////////
            obj3.consultarContactoRoD();
            ////////////////////Adjuntar un archivo//////////////////////
            obj3.nombre1 = 'archivo uno';
            obj3.adjunto1 =  Blob.valueOf('archivo uno');
            obj3.saveCaseAdjunto();
            obj3.attachFile();
            ////////////////////Cerrar//////////////////////
            obj3.onclickCerrar();      
            obj3.blnCrearContacto = false;
        }
        List <Case> lst_case = [SELECT BI_Confidencial__c, BI_COL_Fecha_Radicacion__c, Type, RecordType.DeveloperName, BI_Country__c, Origin FROM Case];
        System.assertEquals(lst_case.size(), 3);

        Test.stopTest();

    }

    static testMethod void PCA_ShowProfileController_catch_TEST() {
        BI_TestUtils.throw_exception = true;

        BI2_COL_PCA_CreacionIncidencia_ctr obj = new BI2_COL_PCA_CreacionIncidencia_ctr();
        obj.checkPermissions();
        obj.loadInfo();
        obj.changeCaseType();
        obj.getItems();
        obj.getFields();    
        obj.blnCrearContacto = false;
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerContacto'));
        System.debug('###====='+obj.blnCrearContacto);
        obj.consultarContactoRoD();
        System.debug('###====='+obj.blnCrearContacto);
        //obj.saveCase();
        //obj.saveCaseAdjunto();
        //obj.GuardarUbicacion();
        obj.attachFile();
    }

}