@isTest
public class PCA_Chatter_Controller_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
      Author:        Jose Miguel Fierro
      Company:       New Enery Aborda
      Description:   Coverage for PCA_Chatter_Controller.<Constructor>
    
      History:
      <Date>            <Author>              <Description>
      01/02/2016		  Jose Miguel Fierro    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    //@isTest(SeeAllData=true)
    @isTest
    public static void test_Constructor() {
        // JMF: getPortalUsers returns a single user instead of a list
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        
        List<CollaborationGroup> lstCGs = new List<CollaborationGroup>();
        
        System.runAs(new User(Id=UserInfo.getUserId())) {
            try {
                lstCGs.add(new CollaborationGroup(Name='DUMMY TEST GROUP', CollaborationType='Private', CanHaveGuests=true, OwnerId=UserInfo.getUserId()));
                insert lstCGs;
            } catch (DMLException exc) {
                System.debug('[PCA_Chatter_Controller_TEST test_Constructor]: Exception: ' + exc);
                lstCGs[0].CollaborationType='Private';
                insert lstCGs;	// If there is an error here, give up
            }
            //lstCGs.addAll([SELECT Id, Name, CanHaveGuests FROM CollaborationGroup WHERE Name = 'Test ACME']);
            
            //System.debug('[PCA_Chatter_Controller_TEST test_Constructor]: lstCGs: ' + lstCGs);
            //System.debug('[PCA_Chatter_Controller_TEST test_Constructor]: lstCGs: ' + [SELECT Id, Name, CanHaveGuests FROM CollaborationGroup WHERE Name Like '%Test%']);
        }
        /*System.runAs(new User(Id=UserInfo.getUserId())) {
            List<CollaborationGroupMember> lstCGMs = new List<CollaborationGroupMember>();
            lstCGMs.add(new CollaborationGroupMember(CollaborationGroupId=lstCGs[0].Id, MemberId=userTest.Id));
            insert lstCGMs;
        }*/
        
        
        //System.runAs(userTest) {
            PageReference pageRef = new PageReference('PCA_Chatter2');
            System.debug('[PCA_Chatter_Controller_TEST test_Constructor]: lstCGs[0].Name = ' + lstCGs[0].Name);
            pageRef.getParameters().put('GroupName', lstCGs[0].Name);
        	Test.setCurrentPage(pageRef);
			PCA_Chatter_Controller tmpContr = new PCA_Chatter_Controller();
        	tmpContr.createTableRecordList(new List<ContentVersion>{new ContentVersion()});
        //}
    }
}