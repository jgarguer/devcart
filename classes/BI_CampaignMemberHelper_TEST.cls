@isTest 
private class BI_CampaignMemberHelper_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_CampaignMemberHelper class 
    
    History: 
    
    <Date> 					<Author> 				<Change Description>
    22/09/2014				Micah Burgos			Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Micah Burgos
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for createCMS method
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	22/09/2014				Micah Burgos			Initial version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
    static testMethod void createCMS_Test() {
        list<Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(1, BI_DataLoad.obtainCampaignRecordType());//1 x 3
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        CampaignMemberStatus ret = BI_CampaignMemberHelper.createCMS('label_test', lst_camp[0].Id, 0);

        CampaignMemberStatus expected = new CampaignMemberStatus(Label = 'label_test',
                                                                SortOrder = 0,
                                                                CampaignId = lst_camp[0].Id);

        system.assertEquals(ret,expected);


    	ret = BI_CampaignMemberHelper.createCMS(null, null, null);
        expected = new CampaignMemberStatus(Label = null,
                                            SortOrder = null,
                                            CampaignId = null);
		system.assertEquals(expected, ret);
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Micah Burgos
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for createCampaignMemberStatus method
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	22/09/2014				Micah Burgos			Initial version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
    static testMethod void createCampaignMemberStatus_Test() {
        //try
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
         list<Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(1, BI_DataLoad.obtainCampaignRecordType());//1 x 3
         
         List<CampaignMemberStatus> list_cms = [select Id,CampaignId,Label,SortOrder from CampaignMemberStatus];
         system.assert(list_cms.isEmpty());

         set<Id> set_campIds = new set<Id>();
         for(Campaign camp :lst_camp){
            set_campIds.add(camp.Id);
         }
         BI_CampaignMemberHelper.createCampaignMemberStatus(set_campIds);
         list_cms = [select Id,CampaignId,Label,SortOrder from CampaignMemberStatus where CampaignId IN :set_campIds];
         system.assert(!list_cms.isEmpty());

        //catch
    	BI_CampaignMemberHelper.createCampaignMemberStatus(null);
    }
    
   	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Micah Burgos
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for createTaskLead method
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	22/09/2014				Micah Burgos			Initial version
    22/06/2018              Pablo de Andrés         Añadidos parámetros ownerId y asuntoTask
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
    static testMethod void createTaskLead_Test() { //String TaskName, Date closeDate, Id assignTo, Id idRelated, Id idCampaign, Id ownerId, String asuntoTask
    	Task ret = BI_CampaignMemberHelper.createTaskLead(null, null, null, null, null, null, null);
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        system.assert(ret != null);
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Micah Burgos
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for createTaskLead method
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	22/09/2014				Micah Burgos			Initial version
    06/11/2014              Micah Burgos            Commented because is Deprecated
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
/*    static testMethod void createTaskContact_Test() {
    	Task ret = BI_CampaignMemberHelper.createTaskContact(null, null, null, null);
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    }*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Micah Burgos
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for createTaskLead method
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	22/09/2014				Micah Burgos			Initial version
    06/11/2014              Micah Burgos            Commented because is Deprecated
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
/*    static testMethod void createTaskContact_TryTest() {
    	
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	List<Campaign> lst_camp = BI_DataLoad.loadCampaigns(1);
    	
    	List <Region__c> lst_pais =BI_DataLoad.loadPais(1);
              
        List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);
               
        List <Contact> lst_con =BI_DataLoad.loadContacts(1, lst_acc);
        
        BI_DataLoad.searchAdminProfile();
    	
    	User usertest = new User(alias = 'testu',
						  email = 'tu@testorg.com',
						  emailencodingkey = 'UTF-8',
						  lastname = 'Test',
						  languagelocalekey = 'en_US',
						  localesidkey = 'en_US',
						  BI_Permisos__c = Label.BI_Administrador,
						  ProfileId = BI_DataLoad.searchAdminProfile(),
						  timezonesidkey = Label.BI_TimeZoneLA,
						  username = 'usertestuser@testorg.com');
		
		insert usertest;
		
		system.runAs(usertest){
		
			Test.startTest();
			    	
	    	list<Task> lst_tsk = new list<Task>();
	    	lst_tsk.add(BI_CampaignMemberHelper.createTaskContact(lst_camp[0].id, date.today(),usertest.id,lst_con[0].id));
	    	
	    	system.assertEquals(lst_tsk.isEmpty(), false);
	    	
	    	Test.stopTest();
		}
    }*/
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}