@isTest
private class BI_Delegated_Admin_Controlller_TEST {
	
	@isTest static void callInitFunctions() {
		// Implement test code
		List<UserRole> lst_Role = BI_Delegated_Admin_Controlller.getRoles('prima');
		List<Profile> lst_pf = BI_Delegated_Admin_Controlller.getProfiles('dos');
		List<User> lst_man = BI_Delegated_Admin_Controlller.getManagers(lst_Role[0].Name,'Argentina');
		List<BI_Delegated_Admin_Controlller.PicklistValue> lst_Permisos = BI_Delegated_Admin_Controlller.getPermisosValues('dos');
		List<BI_Delegated_Admin_Controlller.PicklistValue> lst_Country = BI_Delegated_Admin_Controlller.getPaisesValues('una');
		List<BI_Delegated_Admin_Controlller.PicklistValue> lst_Currency = BI_Delegated_Admin_Controlller.getDivisas();
		List<BI_Delegated_Admin_Controlller.PicklistValue> lst_Len = BI_Delegated_Admin_Controlller.getLeguage();
		List<BI_Delegated_Admin_Controlller.PicklistValue> lst_Config= BI_Delegated_Admin_Controlller.regionalConfig();
		List<BI_Delegated_Admin_Controlller.PicklistValue> lst_Tim = BI_Delegated_Admin_Controlller.timeZone();
		List<BI_Delegated_Admin_Controlller.PicklistValue> lst_Seg = BI_Delegated_Admin_Controlller.SegmentoRegional();
		List<BI_Delegated_Admin_Controlller.PicklistValue> lst_Code = BI_Delegated_Admin_Controlller.emailCode();
		BI_Delegated_Admin_Controlller.leftLic('Colombia');
		BI_Delegated_Admin_Controlller.leftLic('Test');
		List<String> lst_Field = BI_Delegated_Admin_Controlller.getFieldsSF();
		List<PermissionSet> lst_Per = BI_Delegated_Admin_Controlller.getPermissionsSetsAll();
		List<PermissionSet> lst_Per2 = BI_Delegated_Admin_Controlller.getPermissionSetUser('asdasd');
		USer flags = BI_Delegated_Admin_Controlller.checkUserNameMethod('RestIsTes');
		List<Group> lst_grp = BI_Delegated_Admin_Controlller.getQueues();
		lst_grp = BI_Delegated_Admin_Controlller.getQueuesUser('asdasd');
		List<String> lst_test = new List<String>();
		lst_test.add(UserInfo.getUserName());
		BI_Delegated_Admin_Controlller.getManagersCsv(lst_test);
	}
	
	@isTest static void createUser_TEST() {
		// Implement test code
		list<BI_Licencias_Pais__c> lst_lic = new List<BI_Licencias_Pais__c>();
		BI_Licencias_Pais__c lic1 = new BI_Licencias_Pais__c(); 
		lic1.Name = 'Argentina';
		lic1.BI_Numero_Licencias_Usadas__c = 0;
		lic1.BI_Numero_Total_Licencias__c =1111;
		lst_lic.add(lic1);
		BI_Licencias_Pais__c lic2 = new BI_Licencias_Pais__c();
		lic2.Name = 'Colombia';
		lic2.BI_Numero_Licencias_Usadas__c= 11;
		lic2.BI_Numero_Total_Licencias__c=11;
		lst_lic.add(lic2);
		insert lst_lic;
		//No existen licencias para ese pais
		//String result = createUser('jl@jl.com','jl@jl.com',null,null,null,null,'Lope','jla',null,null,'IdRole','IdProf','Peru','Permiso','jk@jk.com',null,null,'es','EUR','jla','XXXXXX','UTF-8',true,true,null,null,null,null);

		String result = BI_Delegated_Admin_Controlller.createUser(null,null,null,null,null,null,null,null,null,null,null,null,null,'Peru',null,NULL,NULL,null,null,null,null,null,null,true,true,null,null,null,null,false,false,'Peru');
		System.debug('LLLLL result = ' + result);
		System.assertEquals(result,'No existen licencias para este tipo de usuario, por favor dirigete al CoE para más información');

		//Numero límite de licencias alcanzado
		result = BI_Delegated_Admin_Controlller.createUser(null,null,null,null,null,null,null,null,null,null,null,null,null,'Colombia',null,NULL,NULL,null,null,null,null,null,null,true,true,null,null,null,null,false,false,'Colombia');
		System.debug('Segunda'+result);
		System.assertEquals(result,'No quedan licencias disponibles, póngase en contacto con el CoE');
		//All fields filled to pass more code in test(It's no relevance)
		result = BI_Delegated_Admin_Controlller.createUser('null','null','|null|*|null|*|null|*|null|','null','null','null','null','null','null','null','null','null','null','Argentina','null','NULL','NULL','null','null','null','null','null','null',true,true,'null','null','null','null',false,false,'Argentina');
		System.debug('Tercera'+result);
		System.assertEquals(result,'OK');

	}
	
	@isTest static void updateUser_TEST() {
		// Implement test code
		list<BI_Licencias_Pais__c> lst_lic = new List<BI_Licencias_Pais__c>();
		BI_Licencias_Pais__c lic1 = new BI_Licencias_Pais__c();
		lic1.Name = 'Argentina';
		lic1.BI_Numero_Licencias_Usadas__c = 0;
		lic1.BI_Numero_Total_Licencias__c =1111;
		lst_lic.add(lic1);
		BI_Licencias_Pais__c lic2 = new BI_Licencias_Pais__c();
		lic2.Name = 'Colombia';
		lic2.BI_Numero_Licencias_Usadas__c= 11;
		lic2.BI_Numero_Total_Licencias__c=11;
		lst_lic.add(lic2);
		BI_Licencias_Pais__c lic23 = new BI_Licencias_Pais__c();
		lic23.Name = 'B2B';
		lic23.BI_Numero_Licencias_Usadas__c= 11;
		lic23.BI_Numero_Total_Licencias__c=12;
		lst_lic.add(lic23);
		insert lst_lic;
		//No existen licencias para ese pais
		//String result = createUser('jl@jl.com','jl@jl.com',null,null,null,null,'Lope','jla',null,null,'IdRole','IdProf','Peru','Permiso','jk@jk.com',null,null,'es','EUR','jla','XXXXXX','UTF-8',true,true,null,null,null,null);
		List<Profile> lst_prof = [Select Id from Profile where Name ='BI_Standard_ARG'];
		User aux = BI_DataLoad.loadUsers(1,lst_prof[0].Id,Label.BI_Administrador)[0];
		aux.Pais__c='Costa Rica';
		aux.BI_Clasificacion_de_Usuario__c='B2B';
		update aux;
		String result = BI_Delegated_Admin_Controlller.updateUser(null,aux.UserName,null,null,null,null,null,null,null,null,null,null,null,'Peru',null,NULL,NULL,null,null,null,null,null,null,true,true,null,null,null,null,false,false,'Peru');
		System.assertEquals(result,'No existen licencias para ese tipo de usuario');
		//Numero límite de licencias alcanzado
		result = BI_Delegated_Admin_Controlller.updateUser(null,aux.UserName,null,null,null,null,null,null,null,null,null,null,null,'Colombia',null,NULL,NULL,null,null,null,null,null,null,true,true,null,null,null,null,false,false,'Colombia');
		System.debug('Segunda'+result);
		System.assertEquals(result,'Número total de licencias alcazadas');
		result = BI_Delegated_Admin_Controlller.updateUser(null,aux.UserName,null,null,null,null,null,null,null,null,null,null,null,'Argentina',null,NULL,NULL,null,null,null,null,null,null,true,true,null,null,null,null,false,false,'Argentina');
		System.debug('Tercera'+result);
		System.assertEquals(result,'OK');
		result = BI_Delegated_Admin_Controlller.updateUser(null,aux.UserName,null,null,null,null,null,null,null,null,null,null,null,'Argentina',null,NULL,NULL,null,null,null,null,null,null,false,false,null,null,null,null,false,false,'Argentina');
		aux.isActive=false;
		aux.BI_Clasificacion_de_Usuario__c='Colombia';
		//Avoid DML Exception
		System.runAs(new User(Id=UserInfo.getUserId())){
			update aux;
		}
		result = BI_Delegated_Admin_Controlller.updateUser(null,aux.UserName,null,null,null,null,null,null,null,null,null,null,null,'Argentina',null,NULL,NULL,null,null,null,null,null,null,false,false,null,null,null,null,false,false,'Argentina');
		System.debug('Cuarta');
		System.assertEquals(result,'No se puede actualizar un Usuario desactivado');
		result = BI_Delegated_Admin_Controlller.updateUser(null,aux.UserName,null,null,null,null,null,null,null,null,null,null,null,'Argentina',null,NULL,NULL,null,null,null,null,null,null,true,false,null,null,null,null,false,false,'Argentina');
		System.debug('Quinta');
		System.assertEquals(result,'OK');
		//Fill the field to up the coberage of the class, it has no need to have sense all are controlled by fron in lightning
		result = BI_Delegated_Admin_Controlller.updateUser('null',aux.UserName,'|null|*|null|*|null|*|null|',null,'null','null','null','null','null','null','null','null','null','Colombia','null',aux.UserName,'NULL','null','null','null','null','null','null',true,false,'null','null','null','null',false,false,'Colombia');
		System.debug('Sexta');
		System.assertEquals(result,'OK');
	}
	@isTest static void updateQueue_TEST() {
		List<Profile> lst_prof = [Select Id from Profile where Name ='BI_Standard_ARG'];
		User aux = BI_DataLoad.loadUsers(1,lst_prof[0].Id,Label.BI_Administrador)[0];
		List<Group> ls_gr = [select Id from Group where Type='Queue' limit 2];//solo cogemos 2 registros para hacer la prueba
		GroupMember gpm = new GroupMember(GroupId=ls_gr[0].Id,UserOrGroupId=aux.Id);
		insert gpm;
		List<String> send = new List<String>();
		send.add(ls_gr[1].Id);
		String result = BI_Delegated_Admin_Controlller.setQueue(send,aux.UserName);
	}
}