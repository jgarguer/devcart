@isTest
public class CWP_LookupSObjectController_TEST {
        @isTest
        public static void getCurrentValueTestKO(){
            CWP_LookupSObjectController.getCurrentValue(null, null);
        }    
        @isTest
        public static void getCurrentValueTestKO2(){
            CWP_LookupSObjectController.getCurrentValue(null, '1');
        } 
        @isTest
        public static void getCurrentValueTestKO3(){
            CWP_LookupSObjectController.getCurrentValue('1', '1');
        } 
        @isTest
        public static void getCurrentValueTestOK(){
            set <string> setStrings = new set <string>{
            'Account', 
                'Case',
                'NE__Order__c',
                'NE__OrderItem__c'
            };
                    
            map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
            map<string, id> rtMapByName = new map<string, id>();
            for(RecordType i : rtMap.values()){
                //rtMapBydevName.put(i.DeveloperName, i.id);
                rtMapByName.put(i.DeveloperName, i.id);
            }
            
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' OR Name = 'Usuario estándar']; 
            User u = new User(Alias = 'standt', Email='standardusertesteveris@testorgeveris.com', 
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standardusertesteveris@testorgeveris.com');
            
            Account accHolding;
            Account accCustomerCountry;
            Account accLegalEntity;
            System.runAs(new User(id=userInfo.getUserId())){        
                accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
                insert accHolding;
                accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
                insert accCustomerCountry;
                accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
                accLegalEntity.ParentId = accCustomerCountry.id;
                accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
                insert accLegalEntity;
            }
            
            NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
            insert catalogo;
            NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
            insert catalogCategoryParent;
            NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
            insert catalogCategoryChildren;
            NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
            producto.TGS_CWP_Tier_1__c = 't1';
            producto.TGS_CWP_Tier_2__c = 't2';
            insert producto;
            
            Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'Order_Management_Case'); 
            
            NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
            insert testOrder;
            
            list<NE__Catalog_Item__c> ciList = new list<NE__Catalog_Item__c>();
            NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
            ciList.add(catalogoItem);
            
            NE__Catalog_Item__c catalogoItem1 = CWP_TestDataFactory.createCatalogoItem('ProductoTB', catalogCategoryChildren.id, catalogo.id, producto.id);
            catalogoItem1.NE__Technical_Behaviour__c = 'Service with pre-approved changes';
            ciList.add(catalogoItem1);
            insert ciList;
            
            NE__OrderItem__c oi = CWP_TestDataFactory.createOrderItem(u.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
            insert oi;
            
            system.debug('etrujill '+'accLegalEntity:'+ accLegalEntity.id);
            system.debug('etrujill '+'rtMapByName:'+ rtMapByName.get('TGS_Incident'));
            system.debug('etrujill '+'oi:'+ oi.id);
            
            Case newcase = CWP_TestDataFactory.createCase(accLegalEntity.id,oi.id,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
            insert newcase;
            system.debug('etrujill '+'Case:'+ String.valueOf(newcase.id));
            CWP_LookupSObjectController.getCurrentValue('Case', newcase.id);
        } 
        @isTest
        public static void lookupOK(){
            CWP_LookupSObjectController.lookup('aux', 'Account', '', '');
        }
        @isTest
        public static void lookupOK2(){ 
            CWP_LookupSObjectController.lookup('aux', 'Account', '', 'SOQL');
        }
        @isTest
        public static void lookupOK3(){ 
            CWP_LookupSObjectController.lookup('aux', 'Account', 'Name LIKE \'%aux%\'', 'SOQL');
        }
        @isTest
        public static void lookupOK4(){ 
            CWP_LookupSObjectController.lookup('aux', 'Account', 'Name LIKE \'%aux%\'', '');
        }
        @isTest
        public static void result(){ 
            CWP_LookupSObjectController.Result res = new CWP_LookupSObjectController.Result('0012500000dVH54AAG','0012500000dVH54AAG');
        }

    
}