/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:          
* Description:         
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-07-24      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_DinamicTable_cls
*                   20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest
private class BI_COL_DinamicTable_tst
{

    Public static User                                                  objUsuario = new User();
    public static BI_COL_DinamicTable_cls                       objDinamicTable;
    public static Account                                       objCuenta;

    public static List <Profile>                                lstPerfil;
    public static List <User>                                   lstUsuarios;
    public static List <UserRole>                               lstRoles;
    
    public static void crearData()
    {

        //perfiles
        lstPerfil                       = new list <Profile>();
        lstPerfil                       = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
        system.debug('datos Perfil '+lstPerfil);
        
        //lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = new User();
        objUsuario.Alias = 'standt';
        objUsuario.Email ='pruebas@test.com';
        objUsuario.EmailEncodingKey = '';
        objUsuario.LastName ='Testing';
        objUsuario.LanguageLocaleKey ='en_US';
        objUsuario.LocaleSidKey ='en_US'; 
        objUsuario.ProfileId = lstPerfil.get(0).Id;
        objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        objUsuario.UserName ='pruebas@test.com';
        objUsuario.EmailEncodingKey ='UTF-8';
        objUsuario.UserRoleId = lstRoles.get(0).Id;
        objUsuario.BI_Permisos__c ='Sucursales';
        objUsuario.Pais__c='Colombia';
        insert objUsuario;
        
        lstUsuarios = new list<User>();
        lstUsuarios.add(objUsuario);

        System.runAs(lstUsuarios[0])
        {
            BI_bypass__c objBibypass = new BI_bypass__c();
            objBibypass.BI_migration__c=true;
            insert objBibypass;
            
            //Roles
            lstRoles                = new list <UserRole>();
            lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
            system.debug('datos Rol '+lstRoles);
            
            //Cuentas
            objCuenta                                       = new Account();      
            objCuenta.Name                                  = 'prueba';
            objCuenta.BI_Country__c                         = 'Colombia';
            objCuenta.TGS_Region__c                         = 'América';
            objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
            objCuenta.CurrencyIsoCode                       = 'COP';
            objCuenta.BI_Fraude__c                          = false;
                        objCuenta.BI_Segment__c                         = 'test';
            objCuenta.BI_Subsegment_Regional__c             = 'test';
            objCuenta.BI_Territory__c                       = 'test';
            insert objCuenta;
            system.debug('datos Cuenta '+objCuenta);

        }
        
    }   
    
    static testMethod void test_method_one()
    {
        // TO DO: implement unit test
        crearData();

        objDinamicTable = new BI_COL_DinamicTable_cls();

        List<Account> lstCuenta = new List<Account>();

        List<String> lstString      = new List<String>();
        List<String> lstStringLabel = new List<String>();

        String strA = 'prueba list String';
        String strLabel = 'prueba list String Label';

        lstCuenta.add(objCuenta);
        lstString.add(strA);
        lstStringLabel.add(strLabel);

        
        objDinamicTable.fn_DynamicComponentTable(lstCuenta, lstString, lstStringLabel );
        
    }
}