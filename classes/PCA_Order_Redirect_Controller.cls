public class PCA_Order_Redirect_Controller
{
    public String catalogItemId{get;set;}
    public NE.NewConfigurationController configuration;
    public String quantity{get; set;}
    public boolean isRegistration {get; set;}
    public boolean isExtBwBool{get;set;}
    public String site {get;set;}
    
    // TEMPLATE
    public List<BI_Multimarca_PP__c> footerLinks {get;set;}
    public List<BI_Multimarca_PP__c> SocialNetwork {get;set;}
    public String AccSegmento{get;set;}
    public map<string,string> mapVisibility {get; private set;}
    public list<Account> ActAccs {get;set;}
    public list <User> UserActual {get;set;}
    public list<Account> ActAcc {get;set;}
    public String AccName{get;set;}
    public String ValueOption {get;set;}
    public User portalUser{get;set;}
    public String profilePhotoURL {get{
        User owner = [SELECT SmallPhotoUrl FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1]; 
        return owner.SmallPhotoUrl;
    } set;}
    public User usuarioPortal{
        get{
            try{
                if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
                if(usuarioPortal==null){
                usuarioPortal = [Select id, FullPhotoUrl, name, CompanyName, ContactId, MobilePhone from User where id=:UserInfo.getUserId()];
                //31/05/2017              Cristina Rodriguez      Commented Usuario_SolarWinds__c, Usuario_BO__c, Sector__c, pass_BO__c, token_SolarWinds__c (User) from SELECT
                }
                return usuarioPortal;
            }catch (exception Exc){
                 BI_LogHelper.generate_BILog('PCA_TemplateController.usuarioPortal', 'Portal Platino', Exc, 'Class');
                 return null;
            }
        }
        set;
    }
    public String idioma {get{
        return UserInfo.getLanguage();
    } set;}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Description:  Controller of the PCA_Order_Redirect view and the PCA_Product_Configurator class
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Load accounts of Portal user 
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Micah Burgos          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public PCA_Order_Redirect_Controller(){
        mapVisibility = PCA_ProfileHelper.getPermissionSet();
        
        List<User> listPortalUser = [Select id, Pais__c, ContactId from User where id=:UserInfo.getUserId() limit 1];
        if(listPortalUser!= null && listPortalUser.size()>0) {
            this.portalUser = listPortalUser.get(0); 
        }
        ActAccs = BI_AccountHelper.getAccounts();
        
        system.debug('[PCA_Order_Redirect_Controller Constructor] ActAccs' +ActAccs);
        
        Id ActAccId = BI_AccountHelper.getCurrentAccountId();
        if (ActAccId!=null){
            UserActual = [Select Name, ContactId,Contact.BI_Cuenta_activa_en_portal__c, AccountId FROM User where Id =:UserInfo.getUserId()];
            if(ActAccs.size() == 1){
                String TotalName = ActAccs[0].Name;
                if(TotalName.length() > 32){
                    AccName = TotalName.substring(0,32);
                    AccName = AccName + '...';
                }else{
                    AccName = TotalName;
                }
            }
            ValueOption = ActAccId;
            Account SegAccount = [Select Id, Name,BI_Country__c, BI_Segment__c FROM Account Where Id=:ActAccId];
            if(SegAccount!=null){
                AccSegmento =  SegAccount.BI_Segment__c;
                
                System.debug('[PCA_Order_Redirect_Controller Constructor] AccSegmento**'+AccSegmento);
            }
            buildFooter(SegAccount);
        }else{
            ValueOption = ' '; 
        }
        
        system.debug('[PCA_Order_Redirect_Controller Constructor] mapVisibility' +mapVisibility);
        system.debug('[PCA_Order_Redirect_Controller Constructor] ActAcc' +ActAcc);
        system.debug('[PCA_Order_Redirect_Controller Constructor] ActAccs' +ActAccs);
    }
    
    /**
     * Method:          Constructor
     * Description:     Obtiene los datos de la página anterior.
     */
    public PCA_Order_Redirect_Controller(NE.NewConfigurationController configurationExt)
    {
        //Parameter to put in the cookie
        String isExtBwStr=ApexPages.currentPage().getParameters().get('isExtBw');
        isExtBwBool=boolean.valueof(isExtBwStr);
        System.debug('[PCA_Order_Redirect_Controller constructor]  isExtBwBool: ' + isExtBwBool);
        
        site=ApexPages.currentPage().getParameters().get('site');
        if(site == null){
            site = '';
        }
        System.debug('[PCA_Order_Redirect_Controller constructor]  site: ' + site);
        
        
        catalogItemId   =   ApexPages.currentPage().getParameters().get('cid');
        if(catalogItemId == null){
            catalogItemId = '';
        }
        configuration   =   configurationExt;
        quantity=ApexPages.currentPage().getParameters().get('quantity');
        if(quantity == null){
            quantity = '';
        }
        
        if(ApexPages.currentPage().getParameters().get('cType') != null && ApexPages.currentPage().getParameters().get('cType').equals('New')){
            isRegistration = true;
        }
        else{
            isRegistration = false;
        }
        
        if(isRegistration){
            configuration.orderRecordType = 'New';
        }
        else{
            configuration.orderRecordType = 'Change';
        }
        
        System.debug('[PCA_Order_Redirect_Controller constructor]  catalodItemId: ' + catalogItemId);
        System.debug('[PCA_Order_Redirect_Controller constructor] configuration: ' + configuration);
        System.debug('[PCA_Order_Redirect_Controller constructor] quantity: ' + quantity);
        System.debug('[PCA_Order_Redirect_Controller constructor] configuration.orderRecordType: ' + configuration.orderRecordType);
     }
    
    /**
     * Method:          redirectToConfigurator
     * Description:     Método llamado tras cargar los datos de PCA_Order_Redirect.
     *                  Rellena los valores necesarios del objeto NE.NewConfigurationController y
     *                  redirige la página hacia la clase PCA_Product_Configurator.
     */
    public Pagereference redirectToConfigurator()
    {
        String XML = '<Workspace action="add" isAddAndConfigure="false"><ListOfPromotions /><ListOfProducts><Item id="' + catalogItemId + '" vid="" quantity="'+quantity+'" /></ListOfProducts></Workspace>';    
        if(isRegistration){
            ApexPages.currentPage().getParameters().put('cType','New');
        }
        else{
            ApexPages.currentPage().getParameters().put('cType','Change');
        }
        configuration.selectedItemsXml = XML;  
        
        System.debug('[PCA_Order_Redirect_Controller.redirectToConfigurator] configuration.selectedItemsXml: '+configuration.selectedItemsXml);
        
        //GC avoid add to cart for registration orders
        if(isRegistration)
        {
            //GC 07/10/2015 change the category before the add
            NE__Catalog_Item__c catalogItemToadd    =   [SELECT NE__Catalog_Category_Name__c FROM NE__Catalog_Item__c WHERE id =: catalogItemId];
            System.debug('[PCA_Order_Redirect_Controller.redirectToConfigurator] catalogItemToadd: '+catalogItemToadd);
            
            //Avoid Pagination in order to find the object
            Decimal bkItemsForPage  =   configuration.cartConfiguration.cartConfigurationObject.NE__ItemsForPage__c;
            configuration.cartConfiguration.cartConfigurationObject.NE__ItemsForPage__c =   200;
            
            System.debug('[PCA_Order_Redirect_Controller.redirectToConfigurator] catalogItemToadd.NE__Catalog_Category_Name__c: '+catalogItemToadd.NE__Catalog_Category_Name__c);
            
            //Change the category before adding the product
            configuration.selectedCategory  =   catalogItemToadd.NE__Catalog_Category_Name__c;
            configuration.offSet            =   '1';
            configuration.isChangeCategory  =   true;
            configuration.getCatalogItemList();
            //end of GC 07/10/2015
            
            configuration.addItemsToCart();      
        
            //GC 07/10/2015 restore items for page BK
            configuration.cartConfiguration.cartConfigurationObject.NE__ItemsForPage__c =   bkItemsForPage;
            
            System.debug('[PCA_Order_Redirect_Controller.redirectToConfigurator] configuration.Cart: '+configuration.Cart);
            
            for(NE.Item it:configuration.Cart)
            {
                configuration.itemToConfigure = it.id;
            }
            
            System.debug('[PCA_Order_Redirect_Controller.redirectToConfigurator] configuration.itemToConfigure: '+configuration.itemToConfigure);
        }
        else
        {        
            String rootItemVId            =    ApexPages.currentPage().getParameters().get('rootItemVId');      
            System.debug('[PCA_Order_Redirect_Controller.redirectToConfigurator] rootItemVId: '+rootItemVId);
            configuration.itemToConfigure =    rootItemVId;
        }
        if(!Test.isRunningTest()){
            configuration.configureProduct();
        }
        
        system.debug('[PCA_Order_Redirect_Controller.redirectToConfigurator] configuration: '+configuration);
        
        PageReference confPage  =   Page.PCA_Product_Configurator;
        confPage.getParameters().put('rootItemVId',configuration.itemToConfigure); 
        confPage.getParameters().putAll(configuration.pageParameters);
        confPage.getParameters().put('fromSummary','true');  //Intentar no cambiar
        if(isRegistration){
            confPage.getParameters().put('cType','New');
        }
        else{
            confPage.getParameters().put('cType','Change');
        }
        confPage.setRedirect(false);   
        
        system.debug('[PCA_Order_Redirect_Controller.redirectToConfigurator] confPage: ' + confPage);
        
        return confPage;
    }
    
    //Template
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   Create Footer for Platino
    
    History:
    
    <Date>            <Author>              <Description>
    29/10/2014        Antonio Moruno        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public void buildFooter(Account Acc){   
        If(Acc.BI_Segment__c != null){
            footerLinks = [SELECT Name, BI_Country__c, BI_Segment__c, BI_Posicion__c, BI_URL__c,BI_Activo__c FROM BI_Multimarca_PP__c WHERE BI_Activo__c=true AND BI_Segment__c =: Acc.BI_Segment__c AND BI_Country__c =:Acc.BI_Country__c AND RecordType.DeveloperName ='BI_Pie_de_pagina' order by BI_Posicion__c];
            SocialNetwork = [SELECT Name, BI_Country__c, BI_Segment__c, BI_Posicion__c, BI_URL__c,BI_Activo__c,BI_Facebook__c,BI_Google__c,BI_Instagram__c,BI_Tuenti__c,BI_Twitter__c,BI_Yammer__c FROM BI_Multimarca_PP__c WHERE BI_Activo__c=true AND BI_Segment__c =: Acc.BI_Segment__c AND BI_Country__c =:Acc.BI_Country__c AND RecordType.DeveloperName='BI_Redes_sociales'];
        }else{  
            footerLinks = [SELECT Name, BI_Country__c, BI_Segment__c, BI_Posicion__c, BI_URL__c,BI_Activo__c FROM BI_Multimarca_PP__c WHERE BI_Activo__c=true AND BI_Segment__c = 'Empresas' AND BI_Country__c =:Acc.BI_Country__c AND RecordType.DeveloperName='BI_Pie_de_pagina' order by BI_Posicion__c];
            SocialNetwork = [SELECT Name, BI_Country__c, BI_Segment__c, BI_Posicion__c, BI_URL__c,BI_Activo__c,BI_Facebook__c,BI_Google__c,BI_Instagram__c,BI_Tuenti__c,BI_Twitter__c,BI_Yammer__c FROM BI_Multimarca_PP__c WHERE BI_Activo__c=true AND BI_Segment__c = 'Empresas' AND BI_Country__c =:Acc.BI_Country__c AND RecordType.DeveloperName='BI_Redes_sociales'];
        }
        
        System.debug('[PCA_Order_Redirect_Controller.buildFooter] footerLinks: ' + footerLinks);
        System.debug('[PCA_Order_Redirect_Controller.buildFooter] SocialNetwork: ' + SocialNetwork);
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jara Minguillon
    Company:       Deloitte
    Description:   Changes user language to english
    
    History:
    
    <Date>            <Author>              <Description>
    16/03/2015        Jara Minguillon          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    public PageReference changeToEN() {
        User thisUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        thisUser.LanguageLocaleKey = 'en_US';
        update thisUser;
        PageReference pr = ApexPages.currentPage();
        pr.setRedirect(true);
        return pr;
    }
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jara Minguillon
    Company:       Deloitte
    Description:   Changes user language to spanish
    
    History:
    
    <Date>            <Author>              <Description>
    16/03/2015        Jara Minguillon          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference changeToES() {
        User thisUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        thisUser.LanguageLocaleKey = 'es';
        update thisUser;
        PageReference pr = ApexPages.currentPage();
        pr.setRedirect(true);
        return pr;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jara Minguillon
    Company:       Deloitte
    Description:   Changes user language to french
    
    History:
    
    <Date>            <Author>              <Description>
    22/04/2015        Jara Minguillon          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference changeToFR() {
        User thisUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        thisUser.LanguageLocaleKey = 'fr';
        update thisUser;
        PageReference pr = ApexPages.currentPage();
        pr.setRedirect(true);
        return pr;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jara Minguillon
    Company:       Deloitte
    Description:   Changes user language to portuguese
    
    History:
    
    <Date>            <Author>              <Description>
    22/04/2015        Jara Minguillon          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference changeToPT() {
        User thisUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        thisUser.LanguageLocaleKey = 'pt_PT';
        update thisUser;
        PageReference pr = ApexPages.currentPage();
        pr.setRedirect(true);
        return pr;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jara Minguillon
    Company:       Deloitte
    Description:   Changes user language to deutch
    
    History:
    
    <Date>            <Author>              <Description>
    22/04/2015        Jara Minguillon          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference changeToDE() {
        User thisUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        thisUser.LanguageLocaleKey = 'de';
        update thisUser;
        PageReference pr = ApexPages.currentPage();
        pr.setRedirect(true);
        return pr;
    }
}