/**
* Avanxo Colombia
* @author           Dolly Fierro href=<dfierro@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Test Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-08-12      Dolly Fierro (DF)                       New Test Class
*            1.1    23-02-2017      Jaime Regidor                           Adding Campaign Values, Adding Catalog Country 
*                   20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest 
private class BI_COL_InterfacesSisgot_tst
{
    
    public static list<Profile> lstPerfil;
    public static list <UserRole> lstRoles;
    public static list<User> lstUsuarios;
    public static list <Campaign> lstCampana;
    public static List<RecordType> lstRecTyp;
    public static list<BI_COL_Modificacion_de_Servicio__c> lstMSSel;
    
    public static User objUsuario;
    public static Account objCuenta;
    public static Opportunity objOportunidad;
    public static Campaign objCampana;
    public static BI_COL_cup_Selectds_ctr objCupSelectds;
    public static BI_COL_Modificacion_de_Servicio__c objMS;
    public static BI_COL_Anexos__c objAnexos;
    public static BI_COL_Descripcion_de_servicio__c objDesSer;
    
    
    public static void crearData()
    {
        //lstPerfil                                                     = new list <Profile>();
        //lstPerfil                                                 = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
        //system.debug('datos Perfil '+lstPerfil);
        
        //lstRoles                                                  = new list <UserRole>();
        //lstRoles                                                  = [Select id,Name from UserRole where Name = 'Telefónica Global'];
        //system.debug('datos Rol '+lstRoles);
        
        
        //objUsuario                                                    = new User();
        //objUsuario.Alias                                          = 'standt';
        //objUsuario.Email                                          ='pruebas@test.com';
        //objUsuario.EmailEncodingKey                               = '';
        //objUsuario.LastName                                           ='Testing';
        //objUsuario.LanguageLocaleKey                              ='en_US';
        //objUsuario.LocaleSidKey                                       ='en_US'; 
        //objUsuario.ProfileId                                      = lstPerfil.get(0).Id;
        //objUsuario.TimeZoneSidKey                                 ='America/Los_Angeles';
        //objUsuario.UserName                                           ='pruebas@test.com';
        //objUsuario.EmailEncodingKey                                   ='UTF-8';
        //objUsuario.UserRoleId                                         = lstRoles.get(0).Id;
        //objUsuario.BI_Permisos__c                                     ='Sucursales';
        //objUsuario.Pais__c                                            ='Colombia';
        ////objUsuario.Division_Allocation__c='COP';
        
        //lstUsuarios                                               = new List<User>();
        //lstUsuarios.add(objUsuario);
        //insert lstUsuarios;
        //system.debug('datos Usuario '+lstUsuarios);
        
         
        NE__Catalog__c objCatalogo                                  = new NE__Catalog__c();
        objCatalogo.Name                                            = 'prueba Catalogo';
        objCatalogo.BI_country__c                                   = 'Colombia';
        insert objCatalogo; 
        
        objCampana = new Campaign();
        objCampana.Name = 'Campaña Prueba';
        objCampana.BI_Catalogo__c = objCatalogo.Id;
        objCampana.BI_COL_Codigo_campana__c = 'prueba1';
        objCampana.BI_Country__c = 'Colombia';
        objCampana.BI_Opportunity_Type__c = 'Fijo';
        objCampana.BI_SIMP_Opportunity_Type__c = 'Alta';
        insert objCampana;
        lstCampana = new List<Campaign>();
        lstCampana = [select Id, Name from Campaign where Id =: objCampana.Id];
        system.debug('datos Cuenta '+lstCampana);
        
        objCuenta                                                   = new Account();
        objCuenta.Name                                              = 'prueba';
        objCuenta.BI_Country__c                                     = 'Colombia';
        objCuenta.TGS_Region__c                                     = 'América';
        objCuenta.BI_Tipo_de_identificador_fiscal__c                = '';
        objCuenta.CurrencyIsoCode                                   = 'COP';
        objCuenta.BI_Segment__c                         = 'test';
        objCuenta.BI_Subsegment_Regional__c             = 'test';
        objCuenta.BI_Territory__c                       = 'test';
        insert objCuenta;
        system.debug('datos Cuenta '+objCuenta);
        
        objOportunidad                                              = new Opportunity();
        objOportunidad.Name                                         = 'prueba opp';
        objOportunidad.AccountId                                    = objCuenta.Id;
        objOportunidad.BI_Country__c                                = 'Colombia';
        objOportunidad.CloseDate                                    = System.today().addDays(+5);
        objOportunidad.StageName                                    = 'F5 - Solution Definition';
        objOportunidad.CurrencyIsoCode                              = 'COP';
        objOportunidad.Certa_SCP__contract_duration_months__c       = 12;
        objOportunidad.BI_Plazo_estimado_de_provision_dias__c       = 0 ;
        //objOportunidad.OwnerId                                      = lstUsuarios[0].id;
        insert objOportunidad;
        system.debug('Datos Oportunidad'+objOportunidad);
        
        lstRecTyp = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
        system.debug('datos de FUN '+lstRecTyp);
        
        objAnexos               = new BI_COL_Anexos__c();
        objAnexos.Name          = 'FUN-0041414';
        objAnexos.RecordTypeId  = lstRecTyp[0].Id;
        insert objAnexos;
        System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
        
        objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
        objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
        objDesSer.CurrencyIsoCode               = 'COP';
        insert objDesSer;
        System.debug('\n\n\n Sosalida objDesSer '+ objDesSer);
        
        objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
        objMS.BI_COL_FUN__c                         = objAnexos.Id;
        objMS.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
        objMS.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
        objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
        objMS.BI_COL_Bloqueado__c                   = false;
        objMS.BI_COL_Estado_orden_trabajo__c        = 'Ejecutado';
        
        
        
        lstMSSel = new List<BI_COL_Modificacion_de_Servicio__c>();
        lstMSSel.add(objMS); 
        
        System.debug('\n\n\n Modificación de Servicio '+ objMS);
    }
    
    static testMethod void myUnitTestOne() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            crearData();
            insert objMS;
            objMS = [SELECT Name, id FROM BI_COL_Modificacion_de_Servicio__c WHERE Id =: objMS.Id ];
            User usr = [SELECT Username FROM User WHERE isActive = true LIMIT 1].get(0);
            
            test.startTest();
                
                System.debug('\n\n ***objMS.name ' + objMS.name);
                Test.setMock(WebServiceMock.class, new BI_COL_InterfacesSisgot_mws());
                BI_COL_InterfacesSisgot_ctr.EnviarAplazamiento(objMS.Name, '2015-08-10', '2015-08-12', 'causal');
                
                for(String strFor: new List<String>{ 'String1', null, 'String2', 'Str\\x7cing3' } )
                {
                    BI_COL_InterfacesSisgot_ctr.processSplit(strFor);
                }
                
                BI_COL_InterfacesSisgot_ctr.ConsultarSegmentoUsuario(usr.Username);
                BI_COL_InterfacesSisgot_ctr.interpretarRespuesta('interpretar,respuesta,interfaces');
                
            test.stopTest();
        }
    }
    static testMethod void myUnitTestTwo() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            crearData();
            insert objMS;
            objMS = [SELECT Name, id FROM BI_COL_Modificacion_de_Servicio__c WHERE Id =: objMS.Id ];
            User usr = [SELECT Username FROM User WHERE isActive = true LIMIT 1].get(0);
            
            test.startTest();
                
            BI_COL_InterfacesSisgot_ctr.GuardarEnLog('SistemaExterno','Estado', 'MensajeEnviado', 'MensajeRecibido', 'CodObjeto', 'TipoTransaccion', '');
                
            test.stopTest();
        }
    }
}