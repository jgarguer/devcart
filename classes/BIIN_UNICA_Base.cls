/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julio Laplaza
    Company:       HPE
    Description:   Base Class for service calls components
      
    History:
      
    <Date>              <Author>                          <Description>
    10/05/2016          Julio Laplaza                     Initial version
    20/06/2016          José Luis González Beltrán        Fix body construction in login request & use cache for validation token
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public virtual class BIIN_UNICA_Base
{
  public static final Integer TIMEOUT = 120000;
  public static final Long TIMEOUT_TOKEN_SECONDS = 29*60;
      
  public Boolean authenticationFailed = false;

  Long tokenTime    = 0;
  String tokenValue = '';

  public String getToken()
  {
    if (Cache.Org.contains('local.BIINtoken.tokenValue')) 
    {
      tokenValue = (String) Cache.Org.get('local.BIINtoken.tokenValue');
    }

    if (Cache.Org.contains('local.BIINtoken.tokenTime')) 
    {
      tokenTime = (Long) Cache.Org.get('local.BIINtoken.tokenTime');
    }

    Long timePassed = ((System.currentTimeMillis() / 1000) - tokenTime);
    Boolean isTokenExpired = timePassed >= TIMEOUT_TOKEN_SECONDS;

    System.debug('TOKEN value: ' + tokenValue);
    System.debug('TOKEN isTokenExpired: ' + isTokenExpired);

    if (isTokenExpired)
    {
      if(!Test.isRunningTest())
      {
        tokenValue = new BIIN_Login_WS().login(Label.BIIN_USER_TOKEN, Label.BIIN_PASS_TOKEN);
      }
      Cache.Org.put('local.BIINtoken.tokenValue', tokenValue);
      tokenTime = System.currentTimeMillis() / 1000;
      Cache.Org.put('local.BIINtoken.tokenTime', tokenTime);
    }

    return tokenValue;
  }

  protected HttpRequest getRequest(String url, String method)
  {
    return getRequest(url, method, null);     
  }

  protected HttpRequest getRequest(String url, String method, Object optionalBodyClass)
  {
    System.debug(' BIIN_UNICA_Base.getRequest | Montamos url: ' + url + ', method: ' + method);

    HttpRequest req = new HttpRequest();
    req.setEndpoint(url);
    req.setMethod(method);

    if (optionalBodyClass != null)
    {
      String body = JSON.serialize(optionalBodyClass);

      body = cleanRequest(body);

      // Adapt to BAO JSON containers
      if(body.indexOf('credentials') == -1)
      {
        if (method == 'PUT')
        {
          body = '{"update":{"container":' + body + '}}';
          req.setHeader('Content-Type', 'application/json');
        }
        else if (method == 'POST')
        {
          body = '{"create":{"container":' + body + '}}';
          req.setHeader('Content-Type', 'application/json');
        }
      }
      else
      {
        body = '{"login":' + body + '}';
        req.setHeader('Content-Type', 'application/json');
      }

      req.setBody(body);
    }

    // Avoid asking for token in login page
    if (url.indexOf('login') == -1)
    {
      req.setHeader('Authentication-Token', getToken());
    }

    String strUserCountry = [SELECT Pais__c FROM User Where Id =: UserInfo.getUserId()].Pais__c;
    strUserCountry = String.isBlank(strUserCountry) ? 'Peru' : strUserCountry;
    
    req.setHeader('UNICA-Application', 'Salesforce');
    req.setHeader('UNICA-ServiceId', strUserCountry);
    req.setHeader('Accept', 'application/json');

    req.setTimeout(TIMEOUT);

    return req;
  }

  protected BIIN_UNICA_Pojos.ResponseObject getResponse(HttpRequest req, Type optionalReturnClass)
  {
    System.debug(' BIIN_UNICA_Base.getResponse | Enviamos: ' + req + ' con cuerpo: ' + req.getBody() + ' para rellenar un objeto: ' + optionalReturnClass);

    try
    {
      // Realizar llamada
      HttpResponse res = new Http().send(req);
      String body = res.getBody();

      System.debug(' BIIN_UNICA_Base.getResponse | Recibimos: ' + res + ' con cuerpo: ' + body + ' para rellenar un objeto: ' + optionalReturnClass);

      // Limpiar respuesta
      body = cleanResponse(body);
      System.debug('Body tras limpieza: ' + body);

      // Return object
      BIIN_UNICA_Pojos.ResponseObject resObject = new BIIN_UNICA_Pojos.ResponseObject(res);

      // Check response
      if (resObject.isResponseOK()) 
      {
        // Deserializar
        if (optionalReturnClass != null)
        {
          resObject.responseObject = JSON.deserialize(body, optionalReturnClass);
        }

        System.debug('BIIN_UNICA_Base.getResponse | Recibimos: ' + resObject.responseStatus + ' con body: ' + body);
      }
      else 
      {
        System.debug('BIIN_UNICA_Base.getResponse | Error en la respuesta del backend: ' + res);

        // Unauthorized ? reset Token.
        if (resObject.responseStatusCode == 401 && !authenticationFailed)
        {
          System.debug('BIIN_UNICA_Base.getResponse | Token invalido: ' + tokenValue);
          
          authenticationFailed = true;
          String nuevoToken = getToken();
          req.setHeader('Authentication-Token', nuevoToken);

          System.debug('BIIN_UNICA_Base.getResponse | Nuevo Token solicitado: ' + tokenValue);

          return getResponse(req, optionalReturnClass);
        }

        // Deserializar el error
        try
        {
          resObject.exceptionObject = ((BIIN_UNICA_Pojos.ResponseObject) JSON.deserialize(body, BIIN_UNICA_Pojos.ResponseObject.class)).exceptionObject;
        }
        catch(Exception serializationEx)
        {
          throw new IntegrationException('Integration problem: Code: ' + res.getStatusCode() + ' Description: ' + body);
        }

        throw new IntegrationException('Backend problem: Code: ' + resObject.exceptionObject.id + ' Description: ' + resObject.exceptionObject.text);
      }

      return resObject;
    }
    catch(Exception ex)
    {
      if(!Test.isRunningTest())
      {
        throw new IntegrationException('Integration problem: ' + ex.getMessage());
      }
      else
      {
        return new BIIN_UNICA_Pojos.ResponseObject();
      }
    }
  }

  public Account getAccount(String accountId)
  {
    Account account = null;

    if(accountId != null)
    {
      try
      {
        Account[] accounts = [SELECT BI_Validador_Fiscal__c, Name FROM Account WHERE id =: accountId];
        account = accounts[0];
      }
      catch(DmlException ex)
      {
        System.debug(' BIIN_UNICA_Base.getAccount | Error al obtener la cuenta' + accountId + ', Error: ' + ex);
      }
    }

    return account;
  }

  private String cleanResponse(String resBody)
  {
    if (String.isNotEmpty(resBody))
    {
        // Avoid "return" word
        Integer index = resBody.indexOf('"return":');
        if (index != -1)
        {
          Integer startObj = index + 9;
          resBody = resBody.substring(startObj, resBody.length() - 2);
        }

        // Avoid reserved words before parsing/deserializing like: 'new', 'Exception'
        resBody = resBody.replaceAll('"(?i)NEW"', '"NEW_STATUS"');
        resBody = resBody.replaceAll('"Exception"', '"exceptionObject"');
      }
      return resBody;
    }

    private String cleanRequest(String reqBody)
    {
      if (String.isNotEmpty(reqBody))
      {
        // Avoid reserved words before parsing/deserializing like: 'new', 'Exception'
        reqBody = reqBody.replaceAll('"NEW_STATUS"', '"NEW"');
      }
      return reqBody;
    }
  }