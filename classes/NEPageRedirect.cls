public with sharing class NEPageRedirect {
    
    public String ordId;
    public PageReference redirect()
    {
        NETriggerHelper.setTriggerFired('duplicarOI');
        ordId                   =   ApexPages.currentPage().getParameters().get('orderId');
        String createOpty       =   ApexPages.currentPage().getParameters().get('createOpty');
        String siteId           =   ApexPages.currentPage().getParameters().get('site');
        String authorizedUserId =   ApexPages.currentPage().getParameters().get('authUser');
        String holdingId        =   ApexPages.currentPage().getParameters().get('holdingId');
        Pagereference red;
        
        try
        {
            NE__Order__c ord    =   [SELECT id, NE__OrderStatus__c, NE__OpportunityId__c, NE__AccountId__c, NE__BillAccId__c, NE__ServAccId__c, NE__OptyId__c, RecordType.name, RecordType.DeveloperName, RecordTypeId, NE__Version__c, Site__c, Authorized_User__c, Business_Unit__c, Cost_Center__c FROM NE__Order__c WHERE id =: ordId];
            String optyId       =   ord.NE__OptyId__c;
            
            if (ord.RecordType.DeveloperName == 'Quote'){
                red =   new PageReference('/'+optyId);
                return red;
            }

            if(createOpty != null)
            {
                RecordType recType  =   [SELECT Id FROM RecordType WHERE (SobjectType = 'Order__c' OR SobjectType = 'NE__Order__c') AND Name = 'Opty' limit 1]; 
                optyId              =   BI_OpportunityMethods.createQuickOpp(ord.NE__AccountId__c, ord.id);
                system.debug('optyId new test:'+optyId );
                ord.RecordTypeId    =   recType.id;
                ord.NE__Version__c  =   1;
                ord.NE__OptyId__c   =   optyId;
                ord.NE__OpportunityId__c    =   optyId;
                ord.NE__OrderStatus__c      =   'F1 - Closed Won';
                
                update ord;             
            }
            
            if(ord.RecordType.name == 'Opty' || createOpty != null)
            {
                List<NE__OrderItem__c> ordit = [SELECT NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name FROM NE__OrderItem__c WHERE NE__OrderId__c =: ordId];
                Boolean isParque = false;
                for(NE__OrderItem__c oi : ordit)
                {
                    if(oi.NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name == 'Parque Modification' || oi.NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name == 'Parque Baja')
                    {
                        isParque = true;
                        break;
                    }
                }
                
                if(isParque)
                {   
                    Opportunity opty = [SELECT Id, BI_Ciclo_ventas__c FROM Opportunity WHERE Id =: ord.NE__OptyId__c];
                    opty.BI_Ciclo_ventas__c = 'Completo';
                    update opty;
                    
                    for(NE__OrderItem__c oi : ordit)
                    {
                        oi.Configuration_Type__c = 'New';
                    }
                    update ordit;
                    red =   new PageReference('/apex/ChangePrice?id='+ord.id);
                }
                else
                    red =   new PageReference('/'+optyId);
            }
            else
            {
                ord.HoldingId__c = holdingId;
                
                if(siteId != null)
                {
                    ord.Site__c = siteId;
                    list<NE__OrderItem__c> ordItToUpd = new list<NE__OrderItem__c>();
                    for(NE__OrderItem__c ordit : [SELECT Installation_point__c FROM NE__OrderItem__c WHERE NE__OrderId__c =: ord.Id])
                    {
                        ordit.Installation_point__c = siteId;
                        ordItToUpd.add(ordit);
                    }
                	
                    if(ordItToUpd.size() > 0)
                        update ordItToUpd;
                }
                    
                
                if(authorizedUserId != null)
                    ord.Authorized_User__c = authorizedUserId;
                
                update ord;
                
                red =   new PageReference('/'+ord.Id);
            }
                
            //GC TEST to run opty
            system.debug('TEST RUN');
            List<NE__OrderItem__c> ordit = [SELECT Id FROM NE__OrderItem__c WHERE NE__OrderId__c =: ordId];
            update ordit;
        }
        catch(Exception e)
        {
            red =   new PageReference('/'+ordId);
        }
        
        return red;
    }

}