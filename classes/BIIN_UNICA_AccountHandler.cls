/*---------------------------------------------------------------------------------------
  Author:        José Luis González
  Company:       HPE
  Description:   Handler to manage Account Trigger events

  <Date>             <Author>                								<Change Description>
  07/04/2016         José Luis González Beltrán     				Initial Version
  04/07/2016         José Luis González Beltrán     				Fix validations in method areKeyfieldsModified()
  15/07/2016         José Luis González Beltrán     				Validate Account Country is Perú
---------------------------------------------------------------------------------------*/
/**
 * Class AccountHandler
 *
 * Trigger Handler for the Account String. This class implements the ITriggerHandler
 * interface to help ensure the trigger code is bulkified and all in one place.
 */
public with sharing class BIIN_UNICA_AccountHandler implements BIIN_ITriggerHandler
{
	// Constructor
	public BIIN_UNICA_AccountHandler()
	{
	}

	public void bulkBefore()
	{
	}
	
	public void bulkAfter()
	{
	}

	public void beforeInsert(String so)
	{
	}
	
	public void beforeUpdate(String oldSo, String so)
	{
	}
	
	public void beforeDelete(String so)
	{	
	}
	
	@future(callout = true)
	public static void afterInsert(String so)
	{
		Account acc = (Account) JSON.deserialize(so, Account.class);

		new BIIN_UNICA_WS_LegalEntity().postLegalEntity(acc);
	}
	
	@future(callout = true)
	public static void afterUpdate(String so, String oldSo)
	{
		// Cast the String to an Account
		Account newAcc = (Account) JSON.deserialize(so, Account.class);
		Account oldAcc = (Account) JSON.deserialize(oldSo, Account.class);

		new BIIN_UNICA_WS_LegalEntity().putLegalEntity(newAcc, oldAcc);
	}
	
	public void afterDelete(String so)
	{
	}
	
	public void andFinally()
	{
	}	
}