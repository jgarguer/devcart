/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Clase de prueba: FS_CHI_Account_Management

History:
<Date>							<Author>						<Change Description>
29/05/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class FS_CHI_Account_Management_Test {
    
    @testSetup 
    private static void init() {
        
    }
    
    @isTest private static void sync() {
        /* Test */
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueb';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueb';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueb';
        insert config_integracion;
        String alfa = JSON.serialize((BI_RestWrapper.CustomersListType) JSON.deserialize('{"customerList":[{"customerId":"14785AAE1","customerDetails":{"correlatorId":"0012600000S3bfn","customerName":"Telefonica","description":"Example.","parentCustomer":"0012600000Q8BQUBB3","customerStatus":"active","segment":"Empresas","subsegment":"Masivo","creditProfiles":[{"creditProfileDate":"2017-01-20T10:53:58.223Z","creditRiskRating":2}],"additionalData":[{"key":"DenominacionComercial","value":"TEF"},{"key":"FaxEmpresa","value":"1-415-555-1212"},{"key":"TelefonoReferencia","value":"917358987"},{"key":"Sector","value":"Comunicacionesyteconología"},{"key":"Holding","value":"Cliente"},{"key":"CuentaGlobal","value":"True"},{"key":"Ambito","value":"Privado"},{"key":"Subsector","value":"GobiernoNacional"},{"key":"Website","value":"www.telefonica.com"},{"key":"FechaActivacion","value":"2017-01-20T10:53:58.223Z"},{"key":"FechaDesactivacion","value":"2017-01-30T10:53:58.223Z"}]},"legalid":[{"country":"Argentina","nationalIDType":"NIF","nationalID":"000000000X"}],"customerAddresses":[{"addressName":"AvenidaManoteras","addressNumber":{"value":"184"},"locality":"Madrid","region":"Madrid","country":"España","postalCode":"28050"}],"contactingModes":[{"contactMode":"email","details":"emailprueba@gmail.com"},{"contactMode":"phone","details":"123456789"},{"contactMode":"fax","details":"123456789"}]}],"totalResults":1}', BI_RestWrapper.CustomersListType.class));
        String beta = JSON.serialize(FS_CHI_Account_Management.Sync('20552003609'));
        String t = JSON.serialize((BI_RestWrapper.CustomersListType) FS_CHI_Account_Management.Sync('20552003609'));
        System.debug('Serialize expected: \n'+alfa+'\nSerialize actual:\n'+beta+'\nSerialize t:\n'+t);
        System.Test.startTest();
        
        try{
            string resultExpected = '{"totalResults":1,"customersList":{"customerList":[{"legalId":[{"nationalIDType":"NIF","nationalID":"000000000X","isPrimary":null,"country":"Argentina"}],"customerId":"14785AAE1","customerDetails":{"subsegment":"Masivo","segment":"Empresas","satisfaction":null,"parentCustomer":"0012600000Q8BQUBB3","name":null,"legalId":null,"description":"Example.","customerStatus":"active","customerRank":null,"customerName":"Telefonica","cstType":null,"creditProfiles":[{"creditScore":null,"creditRiskRating":2,"creditProfileDate":"2017-01-20T10:53:58.223Z"}],"correlatorId":"0012600000S3bfn","contacts":null,"contactingModes":null,"address":null,"additionalData":[{"value":"TEF","key":"DenominacionComercial"},{"value":"1-415-555-1212","key":"FaxEmpresa"},{"value":"917358987","key":"TelefonoReferencia"},{"value":"Comunicacionesyteconología","key":"Sector"},{"value":"Cliente","key":"Holding"},{"value":"True","key":"CuentaGlobal"},{"value":"Privado","key":"Ambito"},{"value":"GobiernoNacional","key":"Subsector"},{"value":"www.telefonica.com","key":"Website"},{"value":"2017-01-20T10:53:58.223Z","key":"FechaActivacion"},{"value":"2017-01-30T10:53:58.223Z","key":"FechaDesactivacion"}]},"customerAddresses":[{"region":"Madrid","postalCode":"28050","normalized":null,"municipality":null,"locality":"Madrid","isDangerous":null,"floor":null,"country":"España","coordinates":null,"area":null,"apartment":null,"addressType":null,"addressNumber":{"value":"184","range":null},"addressName":"AvenidaManoteras","addressExtension":null,"additionalData":null}],"contactingModes":[{"isPreferred":null,"details":"emailprueba@gmail.com","contactMode":"email"},{"isPreferred":null,"details":"123456789","contactMode":"phone"},{"isPreferred":null,"details":"123456789","contactMode":"fax"}]}]}}';
            System.assertEquals(resultExpected.length(),JSON.serialize(FS_CHI_Account_Management.Sync('20552003609')).length()); 
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void sync2() {
        /* Test */
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueb2';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueb2';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueb2';
        insert config_integracion;
        
        System.Test.startTest();
        
        try{
            JSON.serialize(FS_CHI_Account_Management.Sync('234134'));
        }catch(Exception exc){
            System.assert(true);
        }
        
        System.Test.stopTest();
    }
}