@isTest
private class test_MyAccountsController {
	static testMethod void myUnitTest() {
    	
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        Account parentAccount = new Account(Name = 'Test 1',                                     
                                            BI_Segment__c = 'test', //27/09/2017              
                                            BI_Subsegment_Regional__c = 'test', //27/09/2017  
                                            BI_Territory__c = 'test' //27/09/2017             
                                            );
        insert parentAccount;
        
        parentAccount.OwnerId = system.UserInfo.getUserId();
        update parentAccount;
        
        insert new Account(Name = 'Test 2', 
                          parentId = parentAccount.id,
                          BI_Segment__c = 'test2', //27/09/2017
                          BI_Subsegment_Regional__c = 'test2', //27/09/2017
                          BI_Territory__c = 'test2' //27/09/2017
                          );

        insert new Account(Name = 'Test 3',
                          parentId = parentAccount.id,
                          BI_Segment__c = 'test', //27/09/2017
                          BI_Subsegment_Regional__c = 'test', //27/09/2017
                          BI_Territory__c = 'test' //27/09/2017
                          );        
        
        MyAccountsController myController = new MyAccountsController();
        
        //system.assert(myController.accounts.size() == 2);
        
        
	}        
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}