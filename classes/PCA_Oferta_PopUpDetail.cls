public without sharing class PCA_Oferta_PopUpDetail  extends PCA_HomeController
{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Class for show opportunity details 
    
    History:
    
    <Date>            <Author>          	<Description>
    26/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public Id accountId;
	public String firstHeader									{get; set;}
	public list<Attachment> Attach {get;set;}
	public list<string> AttachList {get; set;}
	public List<FieldSetHelper.FieldSetRecord> viewRecords 		{get; set;}
	public FieldSetHelper.FieldSetContainer fieldSetRecords 	{get; set;}
	public List<FieldSetHelper.FieldSetResult> fieldSetRecord	{get; set;}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>          	<Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/		
	public PageReference checkPermissions (){
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			
			PageReference page = enviarALoginComm();
			if(page == null){
				loadInfo();
			}
			return page;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_Oferta_PopUpDetail.checkPermissions', 'Portal Platino',Exc, 'Class');
		   return null;
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Constructor
    
    History:
    
    <Date>            <Author>          	<Description>
    27/06/2014        Alejandro Garcia      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public PCA_Oferta_PopUpDetail()
	{}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   load info of Offer
    
    History:
    
    <Date>            <Author>          	<Description>
    30/06/2014       Antonio Moruno      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/		
	public void loadInfo (){
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			
			firstHeader = '';
			accountId =  BI_AccountHelper.getCurrentAccountId();
			Id OppId = (apexpages.currentpage().getparameters().get('id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('id')):null;
			
			String objectName = 'Opportunity';
			String fieldSetName = 'PCA_DetailOportunidad';
			system.debug('oppId: '+oppId);
			if (OppId != null)
			{
				this.fieldSetRecord = FieldSetHelper.FieldSetHelperSimple(fieldSetName, objectName, OppId);
			}
			
			//////////////////////////////////////////////////////////////////////////
			
			List<NE__Order__c> ActiveOrders = new List<NE__Order__c>();
			ActiveOrders = [SELECT Id, NE__OptyId__c, NE__OrderStatus__c FROM NE__Order__c where NE__OptyId__c = :OppId 
			 			    AND (NE__OrderStatus__c = :Label.BI_LabelActive OR NE__OrderStatus__c = :Label.BI_LabelActiveRapido)];
	
			String PedidoId = '';
			Attach = [SELECT Id, Name FROM Attachment Where ParentId =: OppId];
			
			/*
			AttachList = new list<string>();
			if(!Attach.isEmpty()){
				for(Attachment item : Attach){
					string cc = EncodingUtil.base64Encode(item.Body);
					system.debug('cc: ' + cc);
					AttachList.add('data:application/pdf;base64,' + cc);
				}
	            //string bb = EncodingUtil.convertToHex(listAtt[0].Body);
	            //system.debug('bb: '+bb);
				
			}
			
			*/
			
			
			if (ActiveOrders.size() > 0)
			{
				PedidoId = ActiveOrders[0].Id;
			}
	
			String objectName2 = 'NE__OrderItem__c';
			String fieldSetName2 = 'PCA_RelationTableLineasProductos';
			
			this.viewRecords = new List<FieldSetHelper.FieldSetRecord>();
			
			if (pedidoId != '')
			{
				String pedidoValue = 'NE__OrderId__c;' + pedidoId;
						
				this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(fieldSetName2, objectName2, '', pedidoValue,null);
				
				if (this.fieldSetRecords.regs.size() > 0)
				{
					this.firstHeader = this.fieldSetRecords.regs[0].values[0].field;
				}
			
				for (FieldSetHelper.FieldSetRecord item : this.fieldSetRecords.regs)
				{
					FieldSetHelper.FieldSetRecord iRecord = item; 
					this.viewRecords.add(iRecord);
				}
			}
	
		}catch (exception Exc){
	   		BI_LogHelper.generate_BILog('PCA_Oferta_PopUpDetail.PCA_Oferta_PopUpDetail', 'Portal Platino', Exc, 'Class');
		}
	}
}