/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for testing CWP_HomeController
    Test Class:    CWP_HomeController
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/04/2017              Everis                    Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class CWP_HomeController_TEST {
    
     
    @testSetup 
    private static void dataModelSetup() {  
        
       
        //USER
        UserRole rolUsuario;
        User unUsuario;
        User otroUsuario;
        Profile profileTGS;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            profileTGS = CWP_TestDataFactory.getProfile('TGS System Administrator');
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, profileTGS.Id);
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
            //ACCOUNT
            map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(new User(id=userInfo.getUserId())){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
        
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        Contact contactTest1 = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest1');
        
        contactTest.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest;
        contactTest1.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest1;
        
        User usuario;
        User usuarioNoTGS;
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');       
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        /*System.runAs(usuario){        
            Profile perfil2 = CWP_TestDataFactory.getProfile('BI_Customer Communities');       
            usuarioNoTGS = CWP_TestDataFactory.createUser('nombre3', null, perfil2.id);
            usuarioNoTGS.contactId = contactTest1.id;
            insert usuarioNoTGS;
        }*/
        
        
        BI_ImageHome__c home = new BI_ImageHome__c();
        home.Name='home';
        home.BI_Fecha_Inicio__c=Date.today();
        home.BI_Country__c=null;
        home.BI_Segment__c=accLegalEntity.BI_Segment__c;
        home.BI_Activo__c=true;
        home.CWP_ManagedByTGS__c=true;
        insert home;
        
        BI_ImageHome__c home2 = new BI_ImageHome__c();
        home2.Name='home2';
        home2.BI_Fecha_Inicio__c=Date.newInstance(2016, 01, 01);
        home2.BI_Fecha_Fin__c=null;
        home2.BI_Country__c=null;
        home2.BI_Segment__c=accLegalEntity.BI_Segment__c;
        home2.BI_Activo__c=true;
        home2.CWP_ManagedByTGS__c=true;
        insert home2;
        
        Attachment att= new Attachment();
        att.Name = 'att';
        att.ParentId = home.Id;
       	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        att.Body=bodyBlob;
        insert att;
        
        
               
    }
    
      @isTest
    private static void getImageListTest() {     
        system.debug('test 1');
         
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        BI_ImageHome__c home= [SELECT id FROM BI_ImageHome__c WHERE Name =: 'home'];
      
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_HomeController.getImageList();   
            Test.stopTest();
        }       
        
    } 
    
    /*NO TGS*/
     @isTest
    private static void getImageListTest2() {     
        system.debug('test 1');
         
        User usuario2 = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        BI_ImageHome__c home= [SELECT id FROM BI_ImageHome__c WHERE Name =: 'home'];
       
        System.runAs(usuario2){                          
            Test.startTest();                      
            CWP_HomeController.getImageList();   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void getImageListTest3() {     
        system.debug('test 1');
         
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        BI_ImageHome__c home= [SELECT id FROM BI_ImageHome__c WHERE Name =: 'home2'];
        Attachment att = [SELECT id FROM Attachment WHERE Name =: 'att'];
       
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_HomeController.getImageList();   
            Test.stopTest();
        }       
        
    } 
    
    @isTest
    private static void getUserInfoTest() {     
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        system.debug('test 2 '+usuario);
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_HomeController.getUserInfo();                  
            Test.stopTest();
        }       
        
    } 
    
    @isTest
    private static void getPermissionSetTest() {     
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        system.debug('test 3 '+usuario);
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_HomeController.getPermissionSet();                  
            Test.stopTest();
        }       
        
    } 
    
    @isTest
    private static void getShowServTandMySTest() {     
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        system.debug('test 4 getShowServTandMySTest '+usuario);
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_HomeController.getShowServTandMyS();                  
            Test.stopTest();
        }       
        
    }

    @isTest
    private static void redirectToChatterTest() {     
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        system.debug('test 5 redirectToChatterTest '+usuario);
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_HomeController.redirectToChatter();                  
            Test.stopTest();
        }       
        
    }

}