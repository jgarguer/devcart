@isTest
private class BI_CheckNews_SLAProcess_JOB_TEST
{
	static testMethod void BI_CheckNews_SLAProcess() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
		List<SlaProcess> lst_news_SLAProceso = new List<SlaProcess>();
		lst_news_SLAProceso = [SELECT CreatedById,CreatedDate,Description,Id,IsActive,IsDeleted,IsVersionDefault,LastModifiedById,LastModifiedDate,Name,NameNorm,StartDateField,
                                SystemModstamp,VersionMaster,VersionNotes,VersionNumber FROM SlaProcess WHERE Name Like '%Chile%' AND IsVersionDefault = true AND IsActive = true ];
		
		BI_Jobs.start_checkNews_SLAProcess_JOB(new Map<Id,SlaProcess>(lst_news_SLAProceso));

		List<CronTrigger> lst_cronTrigg = [SELECT CreatedById,CreatedDate,CronExpression,CronJobDetail.Name,EndTime,Id,LastModifiedById,NextFireTime,OwnerId,
		PreviousFireTime,StartTime,State,TimesTriggered,TimeZoneSidKey FROM CronTrigger WHERE CronJobDetail.Name LIKE '%Check New SlaProcess%'];
		System.debug('lst_cronTrigg: ' + lst_cronTrigg);

		System.assert(!lst_cronTrigg.isEmpty());

		BI_Jobs.start_checkNews_SLAProcess_JOB(null);
	}
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}