@isTest  (SeeAllData=true)
private class GenerateOrderTest {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        XXXXXXXXXXXXX
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for GenerateOrder

    History: 

    <Date>                  <Author>                <Change Description>
     XXXXXX                  XXXXXX                 Initial Version
    28/11/2016              Gawron, Julián E.       Put Test.startTest() Test.stopTest(), Change Method Names
    18/02/2017              Guillermo Muñoz         Activate bypass in oneOrderComplexProd to avoid limits

--------------------------------------------------------------------------------------------------------------------------------------------------------*/


    static testMethod void emptyOrderId() 
    {
        try
        {
            Test.startTest();
            GenerateOrder.GenerateOrder('');
            Test.stopTest();
        }
        catch(Exception e){
            System.debug(e);
        }
    }
    
    
    static testMethod void oneOrder() 
    {
        try
        {        
            List <String> lst_pais = new List <String>();
            lst_pais.add('Argentina');
            BI_TestUtils.throw_exception = false; //JEG
            List<Account> accountobj = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Opportunity> opp = BI_DataLoad.loadOpportunities(accountobj[0].Id);
            opp[0].BI_Duracion_del_contrato_Meses__c = 36;
            update opp[0];
            
            RecordType rec2     =   [select Name,Id 
                from RecordType 
                WHERE Name = 'Opty' 
                AND (SObjectType = 'Order__c' OR SObjectType = 'NE__Order__c') LIMIT 1];
            
            NE__Order__c ord = new NE__Order__c(
                NE__AccountId__c    = accountobj [0].id,
                NE__BillAccId__c    = accountobj [0].id,
                NE__ServAccId__c    = accountobj [0].id,
                RecordTypeId        = rec2.id,
                NE__OptyId__c       = opp[0].id,
                NE__Shipping_City__c='city',
                NE__Shipping_Country__c='country',
                NE__Shipping_State_Province__c='SSP',
                NE__Shipping_Street__c='Street',
                NE__Shipping_Zip_Postal_Code__c='ZPC',
                NE__OrderStatus__c  ='Active');

            Test.startTest();

            insert ord;
            
            GenerateOrder.GenerateOrder(opp[0].Id);

            Test.stopTest();
        }
        catch(Exception e){
            System.debug(e);
        }
    }    
    
    
    static testMethod void oneOrderComplexProd() 
    {
       try{   
            
            BI_TestUtils.throw_exception = false;

            Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),true, true, false, false);
            NETriggerHelper.setTriggerFired('NESetDefaultOIFieldsTest');
            NETriggerHelper.setTriggerFired('NECheckOrderItems');

           // List<Region__c> lst_pais = BI_DataLoad.loadPais(1);
            List <String> lst_pais = new List <String>();
            lst_pais.add('Argentina');
            List<Account> accountobj = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Opportunity> opp = BI_DataLoad.loadOpportunities(accountobj[0].Id);
            opp[0].BI_Duracion_del_contrato_Meses__c = 36;
            opp[0].Generate_Acuerdo_Marco__c=true;
            opp[0].BI_Acuerdo_Marco__c=null;
            update opp[0];
            
            NE__Contract_Header__c contrHeader = new NE__Contract_Header__c();
            contrHeader.NE__Name__c='TESTCH';
            insert contrHeader;
            
            NE__Contract__c contract = new NE__Contract__c();
            contract.NE__Contract_Header__c= contrHeader.Id;
            insert contract;
            
            NE__Contract_Account_Association__c contrAcc = new NE__Contract_Account_Association__c();
            contrAcc.NE__Account__c=accountobj[0].Id;
            contrAcc.NE__Contract_Header__c= contrHeader.Id;
            insert contrAcc;
            
            RecordType rec2     =   [select Name,Id from RecordType WHERE Name = 'Opty' AND (SObjectType = 'Order__c' OR SObjectType = 'NE__Order__c') LIMIT 1];
            
            NE__Product__c prod = new NE__Product__c(Name='prodotto');
            insert prod;
            
            NE__Catalog__c cat = new NE__Catalog__c(Name='catalog');
            insert cat;
            
            NE__Catalog_Category__c category = new NE__Catalog_Category__c(name='category',NE__CatalogId__c=cat.id);
            insert category;
            
            NE__Catalog_Item__c catit = new NE__Catalog_Item__c(
                NE__Catalog_Id__c=cat.id, 
                NE__Catalog_Category_Name__c = category.id, 
                NE__ProductId__c=prod.id, 
                NE__Max_Qty__c=1,
                NE__Base_OneTime_Fee__c=15,
                NE__BaseRecurringCharge__c=30);
            insert catit;
             
            NE__Order__c ord2=new NE__Order__c( 
                NE__AccountId__c=accountobj[0].id,
                NE__BillAccId__c=accountobj[0].id,
                NE__ServAccId__c=accountobj[0].id,
                RecordTypeId=rec2.id,
                NE__OptyId__c=opp[0].id,
                NE__Shipping_City__c='city',
                NE__Shipping_Country__c='country',
                NE__Shipping_State_Province__c='SSP',
                NE__Shipping_Street__c='Street',
                NE__Shipping_Zip_Postal_Code__c='ZPC',
                NE__OrderStatus__c='Active',
                NE__CatalogId__c=cat.Id);
            insert ord2;
            
            NE__OrderItem__c oit3=new NE__OrderItem__c(
                NE__OrderId__c=ord2.id,
                NE__ProdId__c=prod.id,
                NE__CatalogItem__c=catit.id,
                NE__Qty__c=1,NE__BaseOneTimeFee__c=5,
                NE__BaseRecurringCharge__c=10,
                NE__OneTimeFeeOv__c=15,
                NE__RecurringChargeOv__c=20);
            insert oit3;

            BI_MigrationHelper.disableBypass(mapa);
            NETriggerHelper.setTriggerFiredTest('NESetDefaultOIFieldsTest', false);
            NETriggerHelper.setTriggerFiredTest('NECheckOrderItems', false);
            
            test.startTest();
            
            GenerateOrder.GenerateOrder(opp[0].Id);
            test.stopTest();
            
        }
        catch(Exception e)
        {
            System.debug(e);
        }
    }
}