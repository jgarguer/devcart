/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-04-09      Manuel Esthiben Mendez Devia (MEMD)     Cloned Controller
*************************************************************************************************************/
public class BI_COL_SessionContract_ctr 
{


	public boolean continuar=true;

	public list<BI_COL_Modificacion_de_Servicio__c> procesables{get;set;}
	public list<BI_COL_Modificacion_de_Servicio__c> inProcesables{get;set;}
	public list<BI_COL_Modificacion_de_Servicio__c> noSeCambian{get;set;}
	public map<string,boolean> dsErrores{get;set;}
	
	public Contract ctr{get;set;}
 
	public boolean pro{get;set;}
	public boolean inpro{get;set;}
	public boolean notc{get;set;}
	public boolean siguiente1{get;set;}
	public boolean DavoxTrsAprob{get;set;}
	public String Busqueda{get; set;}
	
	private set<string> listaIdaCopiar=new set<string>();
	
	public formulario f {get;set;}
	public list<BI_Punto_de_instalacion__c> SucSimilares{get;set;}
	
	private map<string,string> dirSucursales=new map<string,string>();
	private map <string,id> sucursalesSimilares;
	private map<string,string> nuevasSucursales=new map<string,string>();
	
	//variables para manejo serv
	private set<string> listDsr=new set<string>();
	private map<string,Boolean> datosDavox=new map<string,Boolean>();
	private map<string,Boolean> datosTrs=new map<string,Boolean>();
	
	public string tipoCesion{get;set;}
	
	
	private Account vcliente=new Account();
	
	public list<BI_Punto_de_instalacion__c> sucursalesPorCrear{get;set;}
	public BI_Punto_de_instalacion__c sede {get;set;}
	
	
	//Servicios
	public list<wrapperServicios> wrServicios{get;set;}
	public boolean siguienteS1{get;set;}
	public string e{get;set;}
	
	
	public string errorDabox='';
	public string errorTrs='';
	
	private boolean renombrarSucursal=true;
	
	//
	public map<id,BI_COL_Modificacion_de_Servicio__c> msName{get;set;}
	
	
	public Boolean sucpcr{
		get{
			if(sucursalesPorCrear.size()>0){
				return true;
			}
			return false;
		}
	}
	
	public list<string>logTrsDavox{get;set;}

	
	
	public BI_COL_SessionContract_ctr(ApexPages.StandardController stdController) {
		tipoCesion=label.BI_COL_LbCesionContrato;
		
		DavoxTrsAprob=true;
		
		logTrsDavox=new list<string>();
				/*
				stdController.addFields(new List<String>{'Account.Estado_Fraude__c'});
				this.ctr = (Contract)stdController.getRecord();
				*/
				this.ctr =[select id,name,Account.BI_Fraude__c,Account.BI_No_Identificador_fiscal__c,BI_COL_Numero_de_contrato_Cliente__c
				from Contract
				where id=:ApexPages.currentPage().getParameters().get('id')];
				
				
				procesables=new list<BI_COL_Modificacion_de_Servicio__c>();
				inProcesables=new list<BI_COL_Modificacion_de_Servicio__c>();
				noSeCambian=new list<BI_COL_Modificacion_de_Servicio__c>();
				
				f=new formulario();
				
				System.debug(ApexPages.currentPage().getParameters().get('e'));
		e = ApexPages.currentPage().getParameters().get('e');
		if(e=='c'){
			validarMs();
		}
				
		}
	
	
	
	public PageReference Servicios(){
		
		tipoCesion=label.BI_COL_LbCesionServicio;
		
		siguienteS1=true;
		
		if(ctr.id==null){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'El contrato no es valido'));
					pro=inpro=notc=siguienteS1=false;
					return null;
		}
		
		if(ctr.Account.BI_Fraude__c==true){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'El cliente se encuentra reportdado por fraude'));
					pro=inpro=notc=siguienteS1=false;
					return null;
		}
		
		String sqlBusqueda=  'SELECT BI_COL_Clasificacion_Servicio__c,name,BI_COL_Estado__c,BI_COL_Codigo_unico_servicio__c ,Id,BI_COL_Codigo_unico_servicio__r.name '
							+' FROM BI_COL_Modificacion_de_Servicio__c '
							+' where BI_COL_FUN__r.BI_COL_Contrato__c =\''+ctr.id+'\' and BI_COL_Clasificacion_Servicio__c!=\'BAJA\' and BI_COL_Estado__c=\'Activa\'';
		if(Busqueda!=null && Busqueda!='' )
		{
			String dsBuscar='';
			if(Busqueda.contains(','))
			{
				List<String> lstDSB=Busqueda.split(',');
				for(String ls:lstDSB)
				{
					dsBuscar=dsBuscar.trim();
					dsBuscar+='\''+ls+'\',';
				}
				dsBuscar=dsBuscar.subString(0,dsBuscar.length()-1);
			}
			else
			{
				dsBuscar=dsBuscar.trim();
				dsBuscar='\''+Busqueda+'\'';
			}
			sqlBusqueda+=' AND BI_COL_Codigo_unico_servicio__r.Name IN ('+dsBuscar+')'; 
		}
		System.debug('\n\n'+sqlBusqueda+'\n\n');
		sqlBusqueda+=' Limit 1000'; 
		
		//list<Modificacion_servicio__c> ms=new list<Modificacion_servicio__c>([SELECT Clasificaci_n_del_Servicio__c,name,Estado__c,Descripcion_Servicio_R__c ,Id,Descripcion_Servicio_R__r.name FROM Modificacion_servicio__c where FUN__r.Contrato__c =:ctr.id and Clasificaci_n_del_Servicio__c!='BAJA' and Estado__c='Activa']);
		list<BI_COL_Modificacion_de_Servicio__c> ms=Database.query(sqlBusqueda);
		system.debug(ms);
		
		if(ms.isEmpty())
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No existen Ms para transladar:'));
			pro=inpro=notc=siguienteS1=false;
				 return null;
		}
		wrServicios = new list<wrapperServicios>(); 
		
		for(BI_COL_Modificacion_de_Servicio__c mw: ms){
			//if(mw.Estado__c=='Activa'){
			wrServicios.add(new wrapperServicios(false,mw));
			System.debug(mw.name);
			//}
		}
	
	
	return null; 
	}
	
	public PageReference validarServicios()
	{
		pro=inpro=notc=siguiente1=true;
		
		set<string> idServ=new set<string>();
		
		
		for(wrapperServicios wrSs:wrServicios){
			if(wrSs.sel){
				idServ.add(wrSs.ms.id);
			} 
		}
		
		if(idServ.isEmpty()){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Por favor Seleccione al menos un servicio'));
				 return null;
		}
		
		list<BI_COL_Modificacion_de_Servicio__c> ms=new list<BI_COL_Modificacion_de_Servicio__c>([SELECT BI_COL_Codigo_unico_servicio__c ,Id,BI_COL_Codigo_unico_servicio__r.Name FROM BI_COL_Modificacion_de_Servicio__c where id in :idServ]);
		
		set<string> dsId=new set<string>();
		
		for(BI_COL_Modificacion_de_Servicio__c m:ms){
			dsId.add(m.BI_COL_Codigo_unico_servicio__c);      
		}
		
		//---------------------------------------------->>>>>>> relacion producto_telefonica_r para cambiar 

		list<BI_COL_Descripcion_de_servicio__c> dsRelacionadas=new list<BI_COL_Descripcion_de_servicio__c>([SELECT Id,name, (
		select id,BI_COL_Descripcion_Referencia__c,BI_COL_Producto__r.name,name,BI_COL_Estado__c,BI_COL_Clasificacion_Servicio__c,BI_COL_Codigo_unico_servicio__c, BI_COL_Codigo_unico_servicio__r.name,
		BI_COL_Sucursal_de_Facturacion__c,BI_COL_Sucursal_de_Facturacion__r.BI_Sede__c,BI_COL_Sucursal_de_Facturacion__r.BI_Sede__r.BI_COL_Ciudad_Departamento__r.name,
		BI_COL_Sucursal_Origen__c,BI_COL_Sucursal_Origen__r.BI_Sede__c,BI_COL_Sucursal_Origen__r.BI_Sede__r.BI_COL_Ciudad_Departamento__r.name
		from BI_COL_Codigo_unico_servicio__r) FROM BI_COL_Descripcion_de_servicio__c where id=:dsId]);
		
		System.debug('==============> drs '+dsRelacionadas);
				
		set<string> noValidas=new set<string>{label.BI_COL_lblPendiente,label.BI_Enviado,label.BI_COL_LbSuspensionVoluntaria};
		set<string> validas=new set<string>{label.BI_COL_lblActiva};
		
		for(BI_COL_Descripcion_de_servicio__c xds:dsRelacionadas){
			
			list<BI_COL_Modificacion_de_Servicio__c> xms = xds.BI_COL_Codigo_unico_servicio__r;

			System.debug('=========================> Xms\n \n '+xms);

			//Valida ms
			for(BI_COL_Modificacion_de_Servicio__c vms:xms){
				
				if(noValidas.contains(vms.BI_COL_Estado__c)){
					inProcesables.add(vms);
				}else if(validas.contains(vms.BI_COL_Estado__c) && vms.BI_COL_Clasificacion_Servicio__c=='BAJA'){
					noSeCambian.add(vms);
				}else if(validas.contains(vms.BI_COL_Estado__c)){
					procesables.add(vms);
					listaIdaCopiar.add(vms.id);
					
					//Creo Map con identificador direccion+ciudad +vms.Sucursal_Destino__r.Ciudad__r.name +vms.Sucursal_Origen__r.Ciudad__r.name
					//if(vms.BI_COL_Sucursal_de_Facturacion__c!=null && vms.BI_COL_Sucursal_de_Facturacion__r.BI_Sede__c!=null){
					//dirSucursales.put(vms.BI_COL_Sucursal_de_Facturacion__r.BI_Sede__c,vms.BI_COL_Sucursal_de_Facturacion__c);
					//}
					if(vms.BI_COL_Sucursal_Origen__c!=null && vms.BI_COL_Sucursal_Origen__r.BI_Sede__c!=null){
					dirSucursales.put(vms.BI_COL_Sucursal_Origen__r.BI_Sede__c,vms.BI_COL_Sucursal_Origen__c);
					}
					
				}else{
					//inProcesables.add(vms);
				}
			
			}
			
		
		}
		
		if(!inProcesables.isEmpty()){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Se encuentran MS en vuelo no se puede continuar por favor corrija esto.'));
			siguiente1=false;
		}
		
		//---------->> Reemplazar visual ejercicio
		PageReference pr = Page.BI_COL_CusContractSession_pag;
		pr.setRedirect(false);
	
	
	return pr;
	}
	
	public PageReference validarMs(){
		
		
		pro=inpro=notc=siguiente1=true;
		
		if(ctr.id==null){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'El contrato no es valido'));
					pro=inpro=notc=siguiente1=false;
					return null;
		}
		
		if(ctr.Account.BI_Fraude__c==true){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'El cliente se encuentra reportdado por fraude'));
					pro=inpro=notc=siguiente1=false;
					return null;
		}
		
		System.debug('\n\n ----> ctr.id '+ctr.id);
		list<BI_COL_Modificacion_de_Servicio__c> ms=new list<BI_COL_Modificacion_de_Servicio__c>(
				[SELECT BI_COL_Codigo_unico_servicio__c ,Id,BI_COL_Codigo_unico_servicio__r.Name FROM BI_COL_Modificacion_de_Servicio__c where BI_COL_FUN__r.BI_COL_Contrato__c =:ctr.id and BI_COL_Clasificacion_Servicio__c!='BAJA']
				);
		
		system.debug('========= MS ======'+ms);
		
		if(ms.isEmpty()){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,' No existen Ms para transladar'));
			pro=inpro=notc=siguiente1=false;
				 return null;
		}
		
		set<string> dsId=new set<string>();
		
		for(BI_COL_Modificacion_de_Servicio__c m:ms){
			dsId.add(m.BI_COL_Codigo_unico_servicio__c);      
		}
		
		

		list<BI_COL_Descripcion_de_servicio__c> dsRelacionadas=new list<BI_COL_Descripcion_de_servicio__c>([SELECT Id,name, (
		select id,BI_COL_Descripcion_Referencia__c,BI_COL_Producto__r.name,name,BI_COL_Estado__c,BI_COL_Clasificacion_Servicio__c,BI_COL_Codigo_unico_servicio__c, BI_COL_Codigo_unico_servicio__r.name,
		BI_COL_Sucursal_de_Facturacion__c,BI_COL_Sucursal_de_Facturacion__r.BI_Sede__c,BI_COL_Sucursal_de_Facturacion__r.BI_Sede__r.BI_COL_Ciudad_Departamento__r.name,
		BI_COL_Sucursal_Origen__c,BI_COL_Sucursal_Origen__r.BI_Sede__c,BI_COL_Sucursal_Origen__r.BI_Sede__r.BI_COL_Ciudad_Departamento__r.name
		from BI_COL_Codigo_unico_servicio__r) FROM BI_COL_Descripcion_de_servicio__c where id=:dsId]);
		
		set<string> noValidas=new set<string>{label.BI_COL_lblPendiente,label.BI_Enviado,label.BI_COL_LbSuspensionVoluntaria};
		set<string> validas=new set<string>{label.BI_COL_lblActiva};
		
		for(BI_COL_Descripcion_de_servicio__c xds:dsRelacionadas){
			
			list<BI_COL_Modificacion_de_Servicio__c> xms = xds.BI_COL_Codigo_unico_servicio__r;

			//Valida ms
			for(BI_COL_Modificacion_de_Servicio__c vms:xms){
				
				if(noValidas.contains(vms.BI_COL_Estado__c)){
					inProcesables.add(vms);
				}else if(validas.contains(vms.BI_COL_Estado__c) && vms.BI_COL_Clasificacion_Servicio__c=='BAJA'){
					noSeCambian.add(vms);
				}else if(validas.contains(vms.BI_COL_Estado__c)){
					procesables.add(vms);
					listaIdaCopiar.add(vms.id);
					
					//Creo Map con identificador direccion+ciudad +vms.Sucursal_Destino__r.Ciudad__r.name +vms.Sucursal_Origen__r.Ciudad__r.name este Id es para determinar si se crea o no la sucu
					// if(vms.BI_COL_Sucursal_de_Facturacion__c!=null && vms.BI_COL_Sucursal_de_Facturacion__r.BI_Sede__c!=null){
					//dirSucursales.put(vms.BI_COL_Sucursal_de_Facturacion__r.BI_Sede__c,vms.BI_COL_Sucursal_de_Facturacion__c);
					//}
					if(vms.BI_COL_Sucursal_Origen__c!=null && vms.BI_COL_Sucursal_Origen__r.BI_Sede__c!=null){
					dirSucursales.put(vms.BI_COL_Sucursal_Origen__r.BI_Sede__c,vms.BI_COL_Sucursal_Origen__c);
					}
					
				}else{
					//inProcesables.add(vms);
				}
			
			}
			
		
		}
		if(!inProcesables.isEmpty()){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Se encuentran MS en vuelo no se puede continuar por favor corrija esto'));
			siguiente1=false;
		}
		
		return null;
	
	}
	
	public Pagereference siguiente(){
		
		
		
		PageReference pr = Page.BI_COL_CusSessionContractValidation_pag;
		pr.setRedirect(false);
		return pr;
	}
	
	
	public Pagereference siguiente2()
	{
		
		//inProcesables.clear();
		//noSeCambian.clear();
		vcliente=[select id,name from Account where id=:f.ctr.AccountId];
		
		

		PageReference pr = Page.BI_COL_CusSessionContractAllocation_pag;
		pr.setRedirect(false);
		sucursales();
		return pr;
	}
	
	private void sucursales()
	{
		/*string inDirecciones='';

		for(string dir :dirSucursales.keySet())
		{
			if(dir!='' && dir!=null)
			{
				list<string> strdir = dir.split('&$%#&');
				inDirecciones+='\''+String.escapeSingleQuotes(strdir[0])+'\',';   
			}
		}
		
		inDirecciones=inDirecciones.removeEnd(',');*/
		
		
		//Sucursales Disponibles    
		Map<Id, BI_Punto_de_instalacion__c> aSuc = new Map<Id, BI_Punto_de_instalacion__c>([select id,BI_Sede__c,BI_Sede__r.BI_COL_Ciudad_Departamento__r.name from BI_Punto_de_instalacion__c where BI_Cliente__c=:f.ctr.AccountId]);
		System.debug(f.ctr.AccountId);
		
		sucursalesSimilares=new map <string,id>();
		
		for(string s:aSuc.keySet()){
			string sucX=aSuc.get(s).BI_Sede__c;
			string sucIdX=aSuc.get(s).Id;
			if(!sucursalesSimilares.containskey(sucX))
			{
				sucursalesSimilares.put(sucX,sucIdX);     
			}
		}

		
		//Remueve aquellas sucursales que tiene una similar en el cliente
		set<string> idSucAcopiar=new set<string>();
		
 /*Identificadores sucu */  
		//Sucursal Original dirSucursales
		for(string dir :dirSucursales.keySet())
		{
			if(sucursalesSimilares.get(dir)!=null)
			{
				system.debug('\n\n\ncoincidencia:'+dir);
				dirSucursales.remove(dir);
			}
			else
			{
				system.debug('\n\n\nnocoincidencia:'+dir+' ->'+dirSucursales.get(dir));
				idSucAcopiar.add(dirSucursales.get(dir));
			}
		}
		
		sucursalesPorCrear = new list<BI_Punto_de_instalacion__c>();
		

		if(!idSucAcopiar.isEmpty())
		{
			BI_COL_Copy_MS_cls cop =new BI_COL_Copy_MS_cls();
			cop.noCopiarDatos=new set<string>{'Linea_Basica__c','Banda_Ancha__c','Fecha_Consulta__c','Usuario_Consulta__c','Numero_de_Puertos__c','BW_Maximo__c','Estado_Cobertura__c'};
			
			System.debug(idSucAcopiar);
			String sid='(';//string.valueof(idSucAcopiar);
			for(String ids:idSucAcopiar)
			{
				System.debug('\n\n ids = '+ids+'\n\n');
				sid+='\''+ids+'\',';
			}
			sid=sid.subString(0,sid.length()-1)+')';
			//sid=sid.replaceAll(', ', '\',\'').replace('{','(\'').replace('}','\')');
			String querySuc = cop.getCreatableFieldsSOQL('BI_Punto_de_instalacion__c',' id IN '+sid);
			system.debug('========================    sql '+querySuc);
			sucursalesPorCrear = Database.query(querySuc);
		}
		
		
		aSuc=null;
		//Adiciono relaciones campos
		//querySuc=querySuc.replace(' FROM ', ',Ciudad__r.name FROM '); 
		//System.debug(querySuc);
		
		
	}
	
	//Crea suscu y MS
	public Pagereference siguiente3(){
	 // f.contactoFacId=sede.BI_Contacto__c;
		if(f.contactoFacId==null ||  f.contactoFacId==''){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Contacto de Facturación: Debe escribir un valor'));
			f.contactoFac=null;
			return null;  
		}
		
		if(f.contactoInsId==null ||  f.contactoInsId==''){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Contacto de Instalación: Debe escribir un valor'));
			f.contactoInsId=null;
			return null;  
		}
		
		/*
		if(f.contratoDesId==null ||  f.contratoDesId==''){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Contacto de Facturación: Debe escribir un valor'));
			f.contratoDes=null;
			return null;  
		}*/
				
		if(f.cuenta==null ||  f.cuenta==''){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Cuenta Cliente: Debe escribir un valor'));
			f.cuenta=null;
			return null;  
		}


		// Valida que fun contrato oportunidad sean del cliente a ceder*
		integer factSuc=[select count() from BI_Punto_de_instalacion__c where id =:f.myMs.BI_COL_Sucursal_de_Facturacion__c and BI_Cliente__c =:vcliente.id];
		if(factSuc==0){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'La sucursal de facturacion no pertenece al cliente: '+ vcliente.name));
			return null;
		}
		
		integer vfun=[select count() from BI_COL_Anexos__c where id =:f.myMs.BI_COL_FUN__c and BI_COL_Contrato__r.AccountId =:vcliente.id];
		if(vfun==0){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'El FUN no pertenece al cliente '+ vcliente.name));
			return null;
		}
		
		integer vopp=[select count() from Opportunity where id =:f.myMs.BI_COL_Oportunidad__c and AccountId =:vcliente.id and StageName NOT IN ('F2 - Ganada','F1 - Cerrada/Legalizada')];
		if(vopp==0){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'La oportunidad no pertenece al cliente '+ vcliente.name+'o la oportunidad se encuentra F2 - Ganada/F1 - Cerrada/Legalizada'));
			return null;
		}
		if(renombrarSucursal){
		// Fin Valida que fun contrato oportunidad sean del cliente a ceder*
		for (BI_Punto_de_instalacion__c s: sucursalesPorCrear){
			string nvName=s.Name+' '+vcliente.name;
			s.BI_COL_Codigo_sisgot__c='';
			s.Name=nvName.abbreviate(80);
			s.Id=null;
			s.BI_Contacto__c=f.contactoFacId;
			s.BI_COL_Contacto_instalacion__c=f.contactoInsId;
			s.BI_Cliente__c=f.ctr.AccountId;
			System.debug('\n\n\nSUCURSALES \n'+s);
			//s.clone(false,true,false,false);
			
			//Si segenera error evita que se renombre el name
			renombrarSucursal=false;
		}
		}
		
		Savepoint sp = Database.setSavepoint();
		
		try{
		
				insert sucursalesPorCrear;
		
		
		list<string> creadasId =new list<string>(); 
		
		System.debug('=====\n Sucusales por crear '+sucursalesPorCrear);

		for(BI_Punto_de_instalacion__c scn:sucursalesPorCrear){
			creadasId.add(scn.id);
				System.debug('=====\n creadas '+scn);
			//Direcci_n_Sucursal__c+'&$%#&'+aSuc.get(s).Ciudad__r.Name
			//System.debug(scn.Direcci_n_Sucursal__c+'&$%#&'+scn.Ciudad__r.Name);
			//nuevasSucursales.put(scn.Direcci_n_Sucursal__c+'&$%#&'+scn.Ciudad__r.Name,scn.id);  
		}
		
		if(!creadasId.isEmpty()){
			list<BI_Punto_de_instalacion__c> creadas_suc=new list<BI_Punto_de_instalacion__c>([select id,BI_Sede__r.BI_COL_Ciudad_Departamento__r.Name,BI_Sede__c from BI_Punto_de_instalacion__c where id=:creadasId]);
				for(BI_Punto_de_instalacion__c scn:creadas_suc){
					System.debug(scn.BI_Sede__c+'&$%#&'+scn.BI_Sede__r.BI_COL_Ciudad_Departamento__r.Name);
					nuevasSucursales.put(scn.BI_Sede__c,scn.id);
				}
		}
		
		//Libera variables para no ocupar viewstate
		//listDsr=null;
		datosDavox=null;
		datosTrs=null;
		
		//--------------------------->>> CLONAR VISUAL

		duplicar(listaIdaCopiar);
		PageReference pr = Page.BI_COL_CusSessionContractFinalized_pag;
		pr.setRedirect(false);
		return pr;
		
		
		
		
		}catch(exception e){

			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
			Database.RollBack(sp);
			return null;
		
		}

		system.debug('\n\n\n\n\n'+f);
		
		return null;
	}
	
	public Pagereference ValidaDaboxTrs(){
		
		return null;
	}
	
	public Pagereference cancelar(){
		
		PageReference acctPage = new ApexPages.StandardController(ctr).view();
				acctPage.setRedirect(true);
				return acctPage;
	
	}
	
	
	private void duplicar(set<string> idMS){
		
		//omite todos los triggers
		BI_COL_FlagforTGRS_cls.flagTriggers=true;
		map<string,DsrInfo> dsIn=new map<string,DsrInfo>();
		Savepoint sp = Database.setSavepoint();
		string queryId='';
		
		for(string idd:idMS){
			queryId+='\''+idd+'\',';        
		}
		queryId=queryId.subString(0,queryId.length()-1);
		queryId='('+queryId+')';
		
		BI_COL_Copy_MS_cls cop =new BI_COL_Copy_MS_cls();
		cop.noCopiarDatos.clear();
		
		queryId =cop.getCreatableFieldsSOQL('BI_COL_Modificacion_de_Servicio__c','id IN '+queryId);
		System.debug(queryId);
		
		queryId = queryId.replace(' FROM ',',BI_COL_Sede_Destino__r.BI_Sede__r.BI_COL_Ciudad_Departamento__r.Name,BI_COL_Sucursal_Origen__r.BI_Sede__r.BI_COL_Ciudad_Departamento__r.Name,BI_COL_Sede_Destino__r.BI_Sede__c,BI_COL_Sucursal_Origen__r.BI_Sede__c,BI_COL_Codigo_unico_servicio__r.id, BI_COL_Codigo_unico_servicio__r.BI_COL_Estado_de_servicio__c FROM ');
		
		list<BI_COL_Modificacion_de_Servicio__c> msFuente=new list<BI_COL_Modificacion_de_Servicio__c>();
		list<BI_COL_Modificacion_de_Servicio__c> msDestino=new list<BI_COL_Modificacion_de_Servicio__c>();
		
		
		msFuente=Database.query(queryId);
		BI_COL_FlagforTGRS_cls.flagTriggers=true;
		BI_COL_Descripcion_de_servicio__c xds;
		map<Id,BI_COL_Descripcion_de_servicio__c> dsrs=new map<id,BI_COL_Descripcion_de_servicio__c>();
		string funAnterior='';
		
			BI_COL_Modificacion_de_Servicio__c md;
		for(BI_COL_Modificacion_de_Servicio__c mf:msFuente){
			funAnterior=mf.BI_COL_FUN__c+'';
			system.debug('funAnterior:'+funAnterior);
			md= new BI_COL_Modificacion_de_Servicio__c();
			xds= new BI_COL_Descripcion_de_servicio__c();
			md=mf.clone(false,true,false,false);
			md.BI_COL_Estado__c='Pendiente';
			md.BI_COL_Sucursal_de_Facturacion__c=obtenerId(md.BI_COL_Sucursal_de_Facturacion__r.BI_Sede__c);
			md.BI_COL_Sucursal_Origen__c=obtenerId(md.BI_COL_Sucursal_Origen__r.BI_Sede__c);
			//md.BI_COL_Cliente__c='';
			//md.FUN_Anterior__c=md.BI_COL_FUN__c;
			md.BI_COL_FUN__c=f.myMs.BI_COL_FUN__c;
			system.debug('funNuevo:'+f.myMs.BI_COL_FUN__c);
			md.BI_COL_Sucursal_de_Facturacion__c=f.myMs.BI_COL_Sucursal_de_Facturacion__c;
			md.BI_COL_Oportunidad__c=f.myMs.BI_COL_Oportunidad__c;
			//md.BI_COL_Cuenta_facturar_davox__c=f.cuenta; //10/11/2016 ANRC: Se comentó el campo antiguo
			md.BI_COL_Cuenta_facturar_davox1__c=f.cuenta; //10/11/2016 ANRC: Se adiciono nuevo campo davox1
			md.BI_COL_Clasificacion_Servicio__c=tipoCesion;
			md.BI_COL_Estado_orden_trabajo__c=null;
			md.BI_COL_Cargo_conexion__c=0;
			md.BI_COL_Fecha_inicio_de_cobro_RFB__c=null;
			md.BI_COL_Fecha_de_facturacion__c=null;
			md.BI_COL_Autorizacion_facturacion__c=false;
			md.BI_COL_Fecha_liberacion_OT__c=null;
			
			dsIn.put(md.BI_COL_Codigo_unico_servicio__r.id,new DsrInfo(md.BI_COL_Sucursal_de_Facturacion__c,md.BI_COL_Sucursal_Origen__c));
			
			System.debug('\n\n md.BI_COL_Codigo_unico_servicio__r.id'+md.BI_COL_Codigo_unico_servicio__r.id+' \n\n');

			try{
					
					 List<BI_COL_Descripcion_de_servicio__c> lstDS = [
								Select  Id, Name
								From    BI_COL_Descripcion_de_servicio__c 
								Where   Id =: md.BI_COL_Codigo_unico_servicio__r.id Limit 1];

				 System.debug('=======Resultado Consulta ====== '+lstDS);

						xds.id = lstDS[0].id;

			}catch(Exception e)
			{
					System.debug('=======Error ====== '+e.getMessage());
			}
			
					System.debug('=======XDS ====== '+xds);

			dsrs.put(md.BI_COL_Codigo_unico_servicio__r.id,xds);
			
			msDestino.add(md);
			
			System.debug('FUN:'+f.myMs.BI_COL_FUN__c+ ' '+funAnterior);
			
			
			
			//System.debug(md);  
			//System.debug(mf);
		}
		 BI_COL_Anexos__c actuFun;
		if(f.myMs.BI_COL_FUN__c!=funAnterior)
		{
				actuFun = new BI_COL_Anexos__c(id=f.myMs.BI_COL_FUN__c, BI_COL_FUN_Anterior__c=funAnterior);
		}else{
				actuFun = new BI_COL_Anexos__c(id=f.myMs.BI_COL_FUN__c);
		}
		update actuFun;
		
		
		//cambia el Estado_servicio__c para que una regla impida la creacion de ms dentro la ds
		for(string dsr:dsrs.keySet()){
			dsrs.get(dsr).BI_COL_Estado_de_servicio__c='Cesion contrato';
			dsrs.get(dsr).BI_COL_Oportunidad__c=f.myMs.BI_COL_Oportunidad__c;
			dsrs.get(dsr).BI_COL_Bloqueado__c=false;
			dsrs.get(dsr).BI_COL_Sede_Destino__c= dsIn.get(dsr).sDestino;
			dsrs.get(dsr).BI_COL_Sede_Origen__c= dsIn.get(dsr).sOrigen;

		}
		
		try{
				update dsrs.values();
				insert msDestino;
		}catch(Exception e)
		{
			Database.rollback(sp);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
			system.debug('\n ====== MSdestino  '+msDestino);
			system.debug('\n ====== dsrs.values()  '+dsrs.values());
			system.debug('\n ====== Error MsDestino  '+e.getMessage());
		}
		
		//obtengo los id para realizar una consulta y obtener el name de la MS
		list<string> dsrUpd=new list<string>();
		list<string> idMsNew=new list<string>();
			for(BI_COL_Modificacion_de_Servicio__c mn: msDestino){
				idMsNew.add(mn.id);
		}
		
		msName=new map<id,BI_COL_Modificacion_de_Servicio__c>([select name from BI_COL_Modificacion_de_Servicio__c where id=:idMsNew]);
		
		//actualiza la informacion del contrato anterior y bloquea la drs para que no se puedan crear ms
		bloquerDs(dsrs.keySet(), f.myMs.BI_COL_FUN__c, funAnterior,tipoCesion);
		
		System.debug('Nuevo Fun'+f.myMs.BI_COL_FUN__c+' FUN anterior'+funAnterior);
		
		
	
	}
	
	private string obtenerId(string dirS){
		
		string idreturn=null;
		
		if(dirS==null || dirS==''){return idreturn;}
		
		if(sucursalesSimilares.get(dirS)!=null){idreturn = sucursalesSimilares.get(dirS);}

		if(nuevasSucursales.get(dirS)!=null){idreturn = nuevasSucursales.get(dirS);}
	
	
		return idreturn;
	}
	
	
	
	public class formulario{
		
		public BI_COL_Modificacion_de_Servicio__c myMs{get;set;}
		
		public string cuenta {get;set;}
		public string contactoIns {get;set;}
		public string contactoInsId {get;set;}
		
		public string contactoFac {get;set;}
		public string contactoFacId {get;set;}

		//public string contratoDes {get;set;}
		//public string contratoDesId {get;set;}
		
		public Contract ctr{get;set;}
		
		
		public formulario(){
			this.cuenta=null;
			this.myMs=new BI_COL_Modificacion_de_Servicio__c();
			this.ctr=new Contract();
		}
	}
	
	public string getContactoInfo(){
		
		return   '/apex/BI_COL_Filter_pag?id=003&c='+BI_COL_Filter_cls.codificar(' Where AccountId =\''+f.ctr.AccountId+'\'')+'&f='+BI_COL_Filter_cls.codificar(',Name');
	}
	
	public string getContratoInfo(){
		
		return   '/apex/BI_COL_Filter_pag?id=800&c='+BI_COL_Filter_cls.codificar(' ')+'&f='+BI_COL_Filter_cls.codificar(',Numero_de_documento__c');
	}
	
	public string getCuentaInfo(){
		
		System.debug('\n IDCUENTA========>>>>'+f.ctr.AccountId+'\nFORMULARIO===========>>'+f);
		
		return   '/apex/BI_COL_cus_Ses_Contract_Acc_pag?id='+f.ctr.AccountId;
	}

	public list<wrapper>  getValidadosTrsDavox(){
		
		/* ################################### */
		ws_TrsMasivo.CesionRequestDataType[] validDS=new list<ws_TrsMasivo.CesionRequestDataType>();
		ws_TrsMasivo.cesionesRequestArray_element[] listCesion=new list<ws_TrsMasivo.cesionesRequestArray_element>();
		
		string dsDavox='';
		
		for(BI_COL_Modificacion_de_Servicio__c msTrsD:procesables){
			listDsr.add(msTrsD.BI_COL_Codigo_unico_servicio__r.Name);
		}
			
		//TRS estructura
		ws_TrsMasivo.CesionRequestDataType ds=new ws_TrsMasivo.CesionRequestDataType();
		ds.ds=new list<string>();
		
		list<ws_TrsMasivo.CesionRequestDataType>test=new list<ws_TrsMasivo.CesionRequestDataType>();
		
		for(string ds1:listDsr){

			ds.ds.add(ds1);
			system.debug('Para TRS'+ds);
			//validDS.add(ds);
			//Davox Estructura
			dsDavox+=ds1+';';
		}
		
		test.add(ds);
		//listCesion.add(validDS);
		dsDavox=dsDavox.removeEnd('\\x3B');
		dsDavox=ctr.Account.BI_No_Identificador_fiscal__c+'Õ'+ctr.BI_COL_Numero_de_contrato_Cliente__c+'Õ'+dsDavox;
		
		
		//try{
		//  System.debug('\n\n Antes del IF Linea 714 \n\n');
		//  consultarDavox(dsDavox);
		//  //system.debug('Para TRS array'+validDS);
		//  consultarTrs(test);
		//}catch(exception e){
		//  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error servicio:'+ e.getMessage()));
		//  DavoxTrsAprob=false;
		//  System.debug('\n\n --->'+ e.getStackTraceString() +' \n\n');
		//  return null;
			
		//}

		
		
		/* ################################### */
		
		
		

		list<wrapper> procesadosTrsD=new list<wrapper>();
		
		for(BI_COL_Modificacion_de_Servicio__c msTrsD:procesables)
		{
			//wrapper(trs,davox,Modificacion_servicio__c )
			procesadosTrsD.add(new wrapper(datosTrs.get(msTrsD.BI_COL_Codigo_unico_servicio__r.Name), datosDavox.get(msTrsD.BI_COL_Codigo_unico_servicio__r.Name), msTrsD));
			
			//Comentario que no permite continuar la cesion de contrato o servicio
			/*if(datosDavox.get(msTrsD.Descripcion_Servicio_R__r.Name)==false || datosTrs.get(msTrsD.Descripcion_Servicio_R__r.Name)==false){
				//DavoxTrsAprob=false;
			}*/
		}
		
		/*
		if(System.Userinfo.getUserId()==){
		DavoxTrsAprob=true;
		}
		*/
		
		if(!DavoxTrsAprob){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'NO ES POSIBLE REALIZAR LA CESION POR FAVOR REVISAR LAS DSR EN TRS Y/O DAVOX'+errorDabox+errorTrs));
		}
		
		

		return procesadosTrsD;


	}


	public class wrapper{

		public boolean trs{get;set;}
		public boolean davox{get;set;}
		public BI_COL_Modificacion_de_Servicio__c ms{get;set;}
		

		public wrapper(boolean trs,boolean davox,BI_COL_Modificacion_de_Servicio__c ms){
			this.trs=trs;
			this.davox=davox;
			this.ms=ms;
		}
	}
	
	public class wrapperServicios{
		public boolean sel{get;set;}
		public BI_COL_Modificacion_de_Servicio__c ms{get;set;}
		
		public wrapperServicios(boolean sel,BI_COL_Modificacion_de_Servicio__c ms){
			this.sel=sel;
			this.ms=ms;
		}
	
	} 
	
	public class DsrInfo{
		
		public string sDestino{get;set;}
		public string sOrigen{get;set;}
		
		public DsrInfo(string sDestino,string sOrigen){
			this.sDestino=sDestino;
			this.sOrigen=sOrigen;
		}
					
	
	}
	
	@future (callout=true)
		public static void bloquerDs(set<id>ds,id funNew, string funOld, string tipoCesion){
			
			list<BI_COL_Descripcion_de_servicio__c>drs=new list<BI_COL_Descripcion_de_servicio__c>([select id from BI_COL_Descripcion_de_servicio__c where id IN :ds]);
			
			System.debug('==============> drs '+drs);

			for(BI_COL_Descripcion_de_servicio__c nds:drs){
				nds.BI_COL_Estado_de_servicio__c='Cesion contrato';
			}
			update drs;
			
			map<Id,BI_COL_Anexos__c> fun=new map<Id,BI_COL_Anexos__c>([select id,name from BI_COL_Anexos__c where id=:funNew or id=:funOld]);
			
			if(tipoCesion==label.BI_COL_LbCesionContrato){
				fun.get(funNew).BI_COL_Codigo_paquete__c =fun.get(funOld).name;         
			}
			
			fun.get(funNew).BI_COL_Codigo_paquete__c=funOld;
			
			update fun.values();
			
					 
	}
	

}