@isTest 
private class PCA_Reclamo_PopUpNew_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_ReclamosPedidosCtrl class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    30/09/2014              Micah Burgos 	        Initial Version 
    24/02/2016				Guillermo Muñoz			Added configuration object for allow to include new countries
    10/02/2017              Pedro Párraga           Error Resolved
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        -------------------
        Company:       -------------------
        Description:   Method that manage the code coverage of PCA_Reclamo_PopUpNew class
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        24/02/2016                      Guillermo Muñoz             Overwrited
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void PCA_Reclamo_PopUpNew_TEST() {
       
	    BI_TestUtils.throw_exception = false;

	    User me = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];

	    List<Account> lst_acc = BI_DataLoad.loadAccounts(1, new List <String>{'Peru'});
	    System.assertEquals(lst_acc.isEmpty(), false);

		List<RecordType> lst_rt = [SELECT  Id, DeveloperName FROM Recordtype WHERE SObjectType = 'Case'];
		System.assertEquals(lst_rt.isEmpty(), false);

		Map <String, RecordType> map_rt = new Map <String, RecordType>();
		for(RecordType rt : lst_rt){
			map_rt.put(rt.DeveloperName, rt);
		}

		List<Contact> lst_con = BI_DataLoad.loadContacts(1, lst_acc);
		System.assertEquals(lst_con.isEmpty(), false);

		Id profId = [SELECT Id FROM Profile WHERE Name =: Label.BI_Customer_Portal_Profile].Id;

        User usu = BI_DataLoad.loadSeveralPortalUsers(lst_con[0].Id, profId);
        usu.Pais__c = 'Peru';
        System.runAs(me){
        	update usu;      
        }

        BI_Configuracion_Caso_PP__c ccpp = new BI_Configuracion_Caso_PP__c(
        	BI_Confidencial__c = false,
        	BI_COL_Fecha_Radicacion__c = true,
        	BI_Type__c = 'First instance',
        	BI_Tipos_de_registro__c = Label.BI_Caso_agrupador,
        	BI_Country__c = lst_acc[0].BI_Country__c,
        	BI_Origen__c = Label.BI_OrigenCasoPortalCliente
        );
        insert ccpp;


        System.runAs(usu){
        	Test.startTest();
            Test.setMock(HttpCalloutMock.class, new TGS_MockHttpResponseGenerator());

            /////////////////////Caso Agrupador//////////////////////
            PCA_Reclamo_PopUpNew obj = new PCA_Reclamo_PopUpNew();
            obj.checkPermissions();
            obj.loadInfo();
            obj.changeCaseType();
            obj.getItems();
            obj.getFields();
            obj.newCase.Reason = 'Test';
            obj.newCase.Subject = 'Test';
            obj.newCase.Description = 'Test';
            obj.saveCase();

            /////////////////////Caso Comercial//////////////////////
            ccpp.BI_Tipos_de_registro__c = Label.BI_Caso_comercial_2;
            update ccpp;
            PCA_Reclamo_PopUpNew obj2 = new PCA_Reclamo_PopUpNew();
            obj2.checkPermissions();
            obj2.loadInfo();
            obj2.changeCaseType();
            obj2.getItems();
            obj2.getFields();
            obj2.newCase.Reason = 'Test';
            obj2.newCase.Subject = 'Test';
            obj2.newCase.Description = 'Test';
            obj2.saveCase();

            ////////////////////Incidencia Técnica//////////////////////
            ccpp.BI_Tipos_de_registro__c = Label.BIIN_TituloSolicInciden;
            update ccpp;
            PCA_Reclamo_PopUpNew obj3 = new PCA_Reclamo_PopUpNew();
            obj3.checkPermissions();
            obj3.loadInfo();
            obj3.changeCaseType();
            obj3.getItems();
            obj3.getFields();
            obj3.newCase.Reason = 'Test';
            obj3.newCase.Subject = 'Test';
            obj3.newCase.Description = 'Test';
            obj3.saveCase();
            obj3.adjuntos = false;
            obj3.crearTicketRoD();
        }
        List <Case> lst_case = [SELECT BI_Confidencial__c, BI_COL_Fecha_Radicacion__c, Type, RecordType.DeveloperName, BI_Country__c, Origin FROM Case];
        System.assertEquals(lst_case.size(), 2);
        Test.stopTest();
	    	
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        -------------------
        Company:       -------------------
        Description:   Method that manage the code coverage of the exceptions thrown in PCA_
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        24/02/2016                      Guillermo Muñoz             Overwrited PCA_Reclamo_PopUpNew
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void PCA_ShowProfileController_catch_TEST() {
		BI_TestUtils.throw_exception = true;      
		    		
	   	PCA_Reclamo_PopUpNew obj = new PCA_Reclamo_PopUpNew();
        obj.checkPermissions();
        obj.loadInfo();
        obj.changeCaseType();
        obj.getItems();
        obj.getFields();
        obj.saveCase();
    }

}