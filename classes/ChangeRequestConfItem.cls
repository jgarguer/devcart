global with sharing class ChangeRequestConfItem {
    
    webservice static String changeRequest (list<String> orderItemsId, String t) {
        
        list<String> ordersId = new list<String>();     // IDs of orders associated with orderItemsId
        String result = null;
        
        Savepoint savePointCheckout = Database.setSavepoint();  
        
        try {
            list<NE__OrderItem__c> orderItems = [SELECT Id, (SELECT id, processCode__c, Process_in_progress__c, NE__OrderId__c, NE__Status__c FROM NE__Order_Items_Conf__r), NE__Root_Order_Item__c, processCode__c, Process_in_progress__c, NE__OrderId__c, NE__Status__c 
                                                FROM NE__OrderItem__c 
                                                WHERE Id IN :orderItemsId OR NE__Root_Order_Item__c IN: orderItemsId];
            
            map<String, String> rootIds =   new map<String,String>();
            for(NE__OrderItem__c ordIt : orderItems)
            {
                if(ordIt.NE__Root_Order_Item__c != null)
                    rootIds.put(ordIt.NE__Root_Order_Item__c, ordIt.NE__Root_Order_Item__c);
            }
            
            list<NE__OrderItem__c> rootOrderItems   = [SELECT Id, (SELECT id, processCode__c, Process_in_progress__c, NE__OrderId__c, NE__Status__c FROM NE__Order_Items_Conf__r), NE__Root_Order_Item__c, processCode__c, Process_in_progress__c, NE__OrderId__c, NE__Status__c 
                                                        FROM NE__OrderItem__c 
                                                        WHERE Id IN :rootIds.values() OR NE__Root_Order_Item__c IN :rootIds.values()];
            
            orderItems.addAll(rootOrderItems);
            
            Set<NE__OrderItem__c> oiSet   = new Set<NE__OrderItem__c>();
            oiSet.addAll(orderItems);   
            
            orderItems  =   new list<NE__OrderItem__c>();       
            orderItems.addAll(oiSet);
            
            system.debug('*LV orderItems.size() = '+orderItems.size());
            
            for(NE__OrderItem__c ordIt : orderItems) {
                 system.debug('*LV ordIt.NE__Status__c = '+ordIt.NE__Status__c);
                 system.debug('*LV ordIt.id = '+ordIt.id);
                if(ordIt.NE__Status__c == 'Active') {
                    ordersId.add(ordIt.NE__OrderId__c);
                    if(ordIt.Process_in_progress__c != null)
                        // Process already in progress - no update
                        return result = 'Error';
                } else {
                    return result = 'Not active';
                }
            }
            
            list<NE__Order__c> orders = [SELECT Id, processCode__c, NE__AccountId__c, NE__ServAccId__c, NE__BillAccId__c FROM NE__Order__c WHERE Id IN :ordersId];
            
            
            // Update processCode__c field for OrderItem
            String processCode = generateRandomNumber();
            for(NE__Order__c ord : orders) {
                ord.processCode__c = processCode;
            }
            update orders;
            for(NE__OrderItem__c ordIt : orderItems) {
                ordIt.processCode__c = processCode;
            }
            update orderItems;
            
            // Call datamapper
            NE.DataMap.DataMapRequest dReq = new NE.DataMap.DataMapRequest();
            dReq.mapName = 'Order2Order';
            if(Test.IsRunningTest())
                dReq.sourceId = orders[0].Id;
            else
                dReq.sourceId = null;
            Map<String,String> mapOfq = new Map<String,String>();
            list<String> listOfph = new list<String>();
            listOfph.add(processCode);
            
            String customQuery = 'processCode__c =: {!0}';
            mapOfq.put('Order',customQuery);
            mapOfq.put('NoPromoOrderItem',customQuery);
            mapOfq.put('PromoOrderItem',customQuery);
            mapOfq.put('OrderItem',customQuery);
            
            dReq.mapOfCustomQueries = mapOfq;
            dReq.listOfPlaceHolders = listOfph;
            
            NE.DataMap obj = new NE.DataMap();
            NE.DataMap.DataMapResponse dResp = obj.callDataMap(dReq);
            
            
            list<String> parentId = dResp.ParentId.split(';');
            system.debug('*LV parentId = '+parentId);
            list<NE__Order__c> ordDataMap = [SELECT Id, RecordTypeId, NE__Type__c, NE__Configuration_Type__c, NE__Configuration_SubType__c, NE__OrderStatus__c FROM NE__Order__c WHERE Id IN :parentId];
            list<NE__OrderItem__c> ordItemDataMap = [SELECT Id, NE__OrderId__c, Configuration_Type__c FROM NE__OrderItem__c WHERE NE__OrderId__c IN :parentId];
            
            // Move all order items in a single order (firstOrder), all other orders will be deleted
            NE__Order__c firstOrder = ordDataMap.get(0);
            list<NE__Order__c> ordToCanc = new list<NE__Order__c>();
            
            for(NE__Order__c ord : ordDataMap) {
                for(NE__OrderItem__c oi : ordItemDataMap) {
                    oi.NE__OrderId__c = firstOrder.Id;
                    if(t == 'ChangeOrder')
                        oi.Configuration_Type__c = 'Change';
                    else if(t == 'Disconnection')
                        oi.Configuration_Type__c = 'Disconnect';
                }
                if(ord.Id != firstOrder.Id)
                    ordToCanc.add(ord);
            }
            update ordItemDataMap;
            system.debug('*LV newOrdItems = '+ordItemDataMap);
            delete ordToCanc;
            
            // Update Process in progress of old order items with new order's ID
            for(NE__OrderItem__c ordIt : orderItems) {
                ordIt.Process_in_progress__c = firstOrder.Id;
            }
            update orderItems;
            
            
            if(t == 'ChangeOrder') {
                // Update firstOrder with RecordType = 'Opty', and create a new Opportunity
                RecordType rtOpp = [SELECT Id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'BI_Renegociacion' limit 1];
                Opportunity opty = new Opportunity( Name = 'Cambio '+processCode,
                                                    RecordTypeId = rtOpp.Id,
                                                    AccountId = orders[0].NE__AccountId__c,
                                                    BI_Ciclo_ventas__c = 'Completo',
                                                    StageName = 'F6 - Prospecting',
                                                    CurrencyIsoCode = 'ARS',
                                                    BI_Duracion_del_contrato_Meses__c = 12,
                                                    CloseDate = date.today().addMonths(1) );
                insert opty;
                
                RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'NE__Order__c' AND Name = 'Opty' limit 1];
                firstOrder.RecordTypeId = rt.Id;
                firstOrder.NE__Type__c = 'ChangeOrder';
                firstOrder.NE__OpportunityId__c = opty.Id;
                firstOrder.NE__OptyId__c = opty.Id;
                firstOrder.NE__AccountId__c = orders[0].NE__AccountId__c;
                firstOrder.NE__ServAccId__c = orders[0].NE__ServAccId__c;
                firstOrder.NE__BillAccId__c = orders[0].NE__BillAccId__c;
                firstOrder.NE__Configuration_Type__c = 'Change';
                firstOrder.NE__Configuration_SubType__c = '';
                firstOrder.NE__OrderStatus__c = 'Active';
                
                update firstOrder;
                system.debug('*LV new final order (ChangeOrder) = ' + firstOrder);
                
                result = firstOrder.Id;
            
            } else if(t == 'Disconnection') {
                // Update firstOrder with RecordType = 'Order', and create a new Order Header
                RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'NE__Order__c' AND Name = 'Order' limit 1];
                firstOrder.RecordTypeId = rt.Id;
                firstOrder.NE__Type__c = 'Disconnection';
                firstOrder.NE__AccountId__c = orders[0].NE__AccountId__c;
                firstOrder.NE__ServAccId__c = orders[0].NE__ServAccId__c;
                firstOrder.NE__BillAccId__c = orders[0].NE__BillAccId__c;
                firstOrder.NE__Configuration_Type__c = 'Disconnect';
                firstOrder.NE__Configuration_SubType__c = '';
                firstOrder.NE__OrderStatus__c = 'Pending';
                
                update firstOrder;
                system.debug('*LV new final order (Disconnection) = ' + firstOrder);
                
                NE__Order_Header__c oh = new NE__Order_Header__c (  NE__OrderId__c = firstOrder.Id,
                                                                    NE__AccountId__c = firstOrder.NE__AccountId__c,
                                                                    NE__ServAccId__c = firstOrder.NE__ServAccId__c,
                                                                    NE__BillAccId__c = firstOrder.NE__BillAccId__c,
                                                                    NE__OrderStatus__c = firstOrder.NE__OrderStatus__c,
                                                                    NE__Type__c = firstOrder.NE__Type__c );
                insert oh;
                result = firstOrder.Id;
            }
            
        } catch(Exception e) {
            system.debug('Exception found: '+e+' At line: '+e.getLineNumber());
            DataBase.rollback(savePointCheckout);
        }
        
        return result;
    }
    
    
    public static String generateRandomNumber() {
        String randomNumber = generate();

        if (randomNumber.length() < 10) {
        String randomNumber2 = generate();
            randomNumber = randomNumber + randomNumber2.substring(0, 10 - randomNumber.length());
        }
        
        return randomNumber;
    }
    
    
    private static String generate() {
        return String.valueOf(Math.abs(Crypto.getRandomInteger()));
    }

}