public with sharing class BI_ConvertLeadButtonCtrl {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   This class changes the action of lead convert button
    Test Class:	   BI_ConvertLeadButtonCtrl_TEST	
    
    History:
    
    <Date>            <Author>          	<Description>
    05/08/2014        Micah Burgos       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public boolean dir			 { get; set; }
	public Lead LeadtoConvert 	{ get; set; }
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Constructor Method 
    
    IN:            ApexPages.StandardController (Lead)
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    05/08/2014        Micah Burgos      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	public BI_ConvertLeadButtonCtrl(ApexPages.StandardController controller){

		LeadtoConvert = (Lead)controller.getRecord();
		LeadtoConvert = [select Id,  BI_Tipo_de_Identificador_Fiscal__c,	BI_Numero_identificador_fiscal__c from Lead where Id = :LeadtoConvert.Id limit 1];
		
		if(	LeadtoConvert.BI_Tipo_de_Identificador_Fiscal__c != null && LeadtoConvert.BI_Numero_identificador_fiscal__c != null){
				dir = true;
		}else{
				dir=false;		
		}
		
	}
}