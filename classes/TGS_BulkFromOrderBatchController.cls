public class TGS_BulkFromOrderBatchController {

    public  static final integer BLOCKSIZE = 100; 
	//public List<rowWrapper> listAtt {get; set;}
	//public List<rowWrapper> completeData {get; set;}
	public List<rowWrapper> compData {get; set;}
	public String orderId {get; set;}
	public List<rowWrapper> selectedListAtt {get; set;}
	public String listAttJSON {	get;set;}
	public String completeDataListJSON {get;set;}

	//public String csvString {get;set;}
	public String mail;
	public String enteredText1{get;set;}
	public Boolean enableStandardSalesforce {get;set;}

	// 16-05-2017 Manuel Ochoa - Foundation Data filters - START
	
	public NE__Order__c orderObject {get; set;}	
	public string accId {get; set;}
	public string custCtryId {get; set;}
    public id buId {get; set;}
    public id contAccId {get; set;}
    public string contAccName {get; set;}
    public List<SelectOption> varOption1 {get; set;} // Álvaro López - 18/04/2018
    public List<SelectOption> varOption2 {get; set;} // Álvaro López - 18/04/2018

    public List<SelectOption> usersList {get; set;} 
    public List<SelectOption> costCenterOptions {get; set;}
    public List<SelectOption> businessUnitOptions {get; set;}
    //public List<SelectOption> sitesOptions {get; set;}
    private Boolean checkFirstExec = true ; // check for first execution when retrieving Foundation Data

    // 16-05-2017 Manuel Ochoa - Foundation Data filters  - END

	/* public void setCsvString(String n) {

        csvString = n;
    }
      
    public String getCsvString() {
        return csvString;
    }*/



	public TGS_BulkFromOrderBatchController(){
		orderId = System.currentPagereference().getParameters().get('orderId');
		listAttJSON = '[]' ;
       	// 16-05-2017 Manuel Ochoa - Test foundation data
        orderObject=Database.query('SELECT Id,NE__AccountId__c, NE__AccountId__r.Name,NE__AccountId__r.TGS_Aux_Customer_Country__c,NE__CatalogId__r.Name,Business_Unit__C,Site__c,HoldingId__c,TGS_Holding_Name__c,NE__ServAccId__c,NE__ServAccId__r.Name,NE__BillAccId__c,NE__Contract_Account__c, NE__Contract_Account__r.Name,NE__AssetEnterpriseId__c FROM NE__Order__c WHERE Id=:orderId');
		
		custCtryId=orderObject.NE__AccountId__r.TGS_Aux_Customer_Country__c;
		accId=orderObject.NE__AccountId__c;
		buId=orderObject.NE__ServAccId__c;
		contAccID=orderObject.NE__Contract_Account__c;
		contAccName=orderObject.NE__Contract_Account__r.Name;

		List<StructureWrapper> swrapp = getStructure(orderId);
		//completeData = getList(orderId,swrapp); 
		compData = getList(orderId,swrapp); 
		//listAtt = getList(orderId);	
		// get user email 
		mail=UserInfo.getUserEmail();
		system.debug('User email: '+mail);
		getBusinessUnits();
		//getCostCenters();
		//getAuthorizedUsersAndContract(); // 17-06-2017 - Manuel Ochoa
	}


	public List<StructureWrapper> getStructure(String orderId){

		List<StructureWrapper> strWrapperList = new List<StructureWrapper>();


		Map<Id,NE__OrderItem__c> orderItemsFirstLevelMap = new Map<Id,NE__OrderItem__c>([select Id,Name,NE__Parent_Order_Item__c, NE__ProdId__c, NE__CatalogItem__c from NE__OrderItem__c where NE__OrderId__c =:orderId AND NE__Parent_Order_Item__c = null]); 

		Map<Id,NE__OrderItem__c> orderItemsSecondLevelMap = new Map<Id,NE__OrderItem__c>([select Id,Name,NE__Parent_Order_Item__c, NE__ProdId__c, NE__CatalogItem__c from NE__OrderItem__c where NE__OrderId__c =:orderId AND NE__Parent_Order_Item__c IN :orderItemsFirstLevelMap.keySet()]); 

		Map<Id,NE__OrderItem__c> orderItemsThirdLevelMap = new Map<Id,NE__OrderItem__c>([select Id,Name,NE__Parent_Order_Item__c, NE__ProdId__c, NE__CatalogItem__c from NE__OrderItem__c where NE__OrderId__c =:orderId AND NE__Parent_Order_Item__c IN :orderItemsSecondLevelMap.keySet()]); 

		Integer itemIndex=0;

		for (NE__OrderItem__c oiFL : orderItemsFirstLevelMap.values()) {
			
			StructureWrapper strItemFirstLevel = new StructureWrapper();
			
			strItemFirstLevel.orderItemId  = oiFL.Id;
			strItemFirstLevel.catalogItemId = oiFL.NE__CatalogItem__c;
			strItemFirstLevel.productId = oiFL.NE__ProdId__c;
			strItemFirstLevel.itemLevel = 1;
            strItemFirstLevel.itemIndex = ++itemIndex; // 05-09-2017 - ALM4032 - Manuel Ochoa - Fix to process correctly correlative OIs iwth the same family and product name

			strWrapperList.add(strItemFirstLevel);

			for (NE__OrderItem__c oiSL : orderItemsSecondLevelMap.values()) {

				if(oiSL.NE__Parent_Order_Item__c == oiFL.Id){
				
					StructureWrapper strItemSecondLevel = new StructureWrapper();
			
					strItemSecondLevel.orderItemId  = oiSL.Id;
					strItemSecondLevel.catalogItemId = oiSL.NE__CatalogItem__c;
					strItemSecondLevel.productId = oiSL.NE__ProdId__c;
					strItemSecondLevel.itemLevel = 2;
                    strItemSecondLevel.itemIndex = ++itemIndex; // 05-09-2017 - ALM4032 - Manuel Ochoa - Fix to process correctly correlative OIs iwth the same family and product name

					strWrapperList.add(strItemSecondLevel);

					for (NE__OrderItem__c oiTL : orderItemsThirdLevelMap.values()) {
						if(oiTL.NE__Parent_Order_Item__c == oiSL.Id){
				
							StructureWrapper strItemThirdLevel = new StructureWrapper();
			
							strItemThirdLevel.orderItemId  = oiTL.Id;
							strItemThirdLevel.catalogItemId = oiTL.NE__CatalogItem__c;
							strItemThirdLevel.productId = oiTL.NE__ProdId__c;
							strItemThirdLevel.itemLevel = 3;
							strItemThirdLevel.itemIndex = ++itemIndex; // 05-09-2017 - ALM4032 - Manuel Ochoa - Fix to process correctly correlative OIs iwth the same family and product name

							strWrapperList.add(strItemThirdLevel);
						}

					}
				
				}
			
			}

		}

		return 	strWrapperList;
	}



	public List<RowWrapper> getList(String orderId,List<StructureWrapper> strList){


		//List<StructureWrapper> strList =  getStructure(orderId);

		System.debug('strList: ' + strList);


		List<RowWrapper> listAttAux = new List<RowWrapper>();


		// aqui obtenemos de la Order el servicio contratado, pero vamos a hacer las queries partiendo de un producto concreto
		// se miran los order Items y de ah? los catalog Item
		// Adem?s se cogen los datos de la orden para el grid


		// Lista de productos a recuperar sus familias (ordenados) padre - hijo - nieto

		List<id> productIdsList = new List<id>();
		List<id> orderItemsIdsList = new List<id>();


		for (StructureWrapper oItem : strList) {
			productIdsList.add(oItem.productId);
			orderItemsIdsList.add(oItem.orderItemId);
		}
		System.debug('List of all OI Ids (orderItemsIdsList) : ' + orderItemsIdsList);

		// lista para obtener todos los OIAttributtes de la orden
		//INC000000130969 Álvaro López
		List<NE__Order_Item_Attribute__c> oiAttributesList = [Select Id,Name,NE__Order_Item__c, NE__FamPropId__c,NE__Value__c, NE__Order_Item__r.NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name FROM NE__Order_Item_Attribute__c WHERE NE__Order_Item__c IN : orderItemsIdsList];

		System.debug('List of all order item attributes (oiAttributesList) : ' + oiAttributesList);

		Map<Id,NE__Product__c> productsMap = new Map<Id,NE__Product__c>([SELECT Id,Name FROM NE__Product__c WHERE Id IN :productIdsList]);

		// hay que escribir todo el csv en orden 

		Map<Id,NE__ProductFamily__c> productFamilymap = new Map<Id,NE__ProductFamily__c>([select Id,Name,NE__FamilyId__c,NE__FamilyId__r.Name,NE__ProdId__c  from NE__ProductFamily__c where NE__ProdId__c IN :productIdsList]);


		System.debug('productFamilymap: ' + productFamilymap);


		List<id> familyIdList = new List<id>();

		for(NE__ProductFamily__c prodFam : productFamilymap.values()){
				familyIdList.add(prodFam.NE__FamilyId__c);
		}

		Map<Id,NE__Family__c> familyMap = new Map<Id,NE__Family__c>([SELECT Id,Name FROM NE__Family__c WHERE Id IN :familyIdList]);



		// con los ids de las familias cogemos los atributos

		Map<Id,NE__ProductFamilyProperty__c> productFamilyPropertyMap = new Map<Id,NE__ProductFamilyProperty__c>([select Id,Name,NE__PropId__c,NE__PropId__r.Name,NE__PropId__r.NE__Type__c, NE__Required__c,NE__FamilyId__c   from NE__ProductFamilyProperty__c where NE__FamilyId__c IN :familyIdList]);

		System.debug('productFamilyPropertyMap: ' + productFamilyPropertyMap);



		// no trae los valores de la relacion , asi que un paso mas

		List<Id> dynamicPropertyDefinitionIdList = new List<Id>();

		for(NE__ProductFamilyProperty__c prodFamProp: productFamilyPropertyMap.values()){
			dynamicPropertyDefinitionIdList.add(prodFamProp.NE__PropId__c);
		}


		Map<Id,NE__DynamicPropertyDefinition__c> dynamicPropertyDefinitionMap = new Map<Id,NE__DynamicPropertyDefinition__c>([SELECT Id,Name,NE__Type__c FROM  NE__DynamicPropertyDefinition__c where Id IN :dynamicPropertyDefinitionIdList]);

		System.debug('dynamicPropertyDefinitionMap: ' + dynamicPropertyDefinitionMap);

		// cogemos s?lo los ids de los atributos enumerated para coger sus posibles valores

		List<Id> enumeratedPropertiesIdList = new List<Id>();

		for(NE__DynamicPropertyDefinition__c dynPropDef : dynamicPropertyDefinitionMap.values()){
	
				if(dynPropDef.NE__Type__c == 'Enumerated'){
	
					enumeratedPropertiesIdList.add(dynPropDef.Id);
				}
		}

		System.debug('enumeratedPropertiesIdList: ' + enumeratedPropertiesIdList);

		Map<Id,NE__PropertyDomain__c> enumeratedPropertiesMap = new Map<Id,NE__PropertyDomain__c>([select Id,Name,NE__PropId__c,NE__PropId__r.Name  from NE__PropertyDomain__c where NE__PropId__c IN :enumeratedPropertiesIdList]);

		System.debug('enumeratedPropertiesMap: ' + enumeratedPropertiesMap);

		// una vez tenemos todo empezamos a crear la lista que mostraremos

		// empezamos por el producto y de ahi hacia abajo, ya que la lista de los productos marca todo
			
		//List to save product level 1 ids (to where clause in the select lov)
		Set<String> prodIdFLevelSet = new Set<String>();

		for (StructureWrapper oIStr : strList) {
		//for(Id prodId : productIdsList){

			//System.debug('prodId: ' + prodId);	

			for(NE__ProductFamily__c prodFamItem : productFamilymap.values()){

				//System.debug('prodFamItem: ' + prodFamItem.NE__ProdId__c);

				if(prodFamItem.NE__ProdId__c == oIStr.productId ){

						for(NE__ProductFamilyProperty__c prodFamPropItem : productFamilyPropertyMap.values()){

							//System.debug('prodFamPropItem: ' + prodFamPropItem.NE__FamilyId__c);

							if(prodFamPropItem.NE__FamilyId__c == prodFamItem.NE__FamilyId__c ){


								for (NE__DynamicPropertyDefinition__c dynPropDefItem : dynamicPropertyDefinitionMap.values()){

									//System.debug('dynPropDefItem: ' + dynPropDefItem.Id);

									if(dynPropDefItem.Id == prodFamPropItem.NE__PropId__c ){

										//System.debug('Required: ' + productFamilyPropertyMap.get(prodFamPropItem.Id).NE__Required__c );
										//System.debug('prodName: ' + productsMap.get(prodId).Name);
										//System.debug('attName: ' + dynPropDefItem.Name );
										//System.debug('xType: ' +  dynPropDefItem.NE__Type__c );

										RowWrapper item = new RowWrapper();

										if(productFamilyPropertyMap.get(prodFamPropItem.Id).NE__Required__c == 'Yes'){
											item.required = true;
											item.selected = true;
										}else{
											item.required = false;
											item.selected = false;
										}
										 
										item.product = productsMap.get(oIStr.productId).Name;
										item.itemLevel = oIStr.itemLevel;

										if (item.itemLevel== 1){
											prodIdFLevelSet.add(oIStr.productId);
										}

										item.family = familyMap.get(prodFamItem.NE__FamilyId__c).Name;
										item.attribute = dynPropDefItem.Name;
										item.xType = dynPropDefItem.NE__Type__c;
										//to include assetItemEnterpriseId for Change Orders
										item.assetItemEnterpriseId = oIStr.orderItemId;

										// here we have all data. Also if the dyn prop is enumerated we have to inform also the enumerated list
										if(dynPropDefItem.NE__Type__c == 'Enumerated'){

											List<String> auxValues = new List<String>();

											for(NE__PropertyDomain__c enumProp : enumeratedPropertiesMap.values()){
												if(enumProp.NE__PropId__c == dynPropDefItem.Id){
													auxValues.add(enumProp.Name);
												}
											}

											item.enumeratedValues = auxValues;

										}


										// value AAP

										for (NE__Order_Item_Attribute__c oiAtt : oiAttributesList){
											if ((oiAtt.NE__FamPropId__c == prodFamPropItem.Id) && (oiAtt.NE__Order_Item__c == oIStr.orderItemId)&&(oiAtt.NE__Value__c!=null && oiAtt.NE__Value__c!='')){
												//add to list
	
												item.value=oiAtt.NE__Value__c;
												item.selected = true;
											}
											//INC000000130969 Álvaro López
											if(oiAtt.NE__Order_Item__r.NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name != null && item.catalogCategoryName == null){
												item.catalogCategoryName = oiAtt.NE__Order_Item__r.NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name;
											}
										}
                                        
                                        // 05-09-2017 - ALM4032 - Manuel Ochoa - Fix to process correctly correlative OIs iwth the same family and product name
										item.itemIndex=oIStr.itemIndex;
										// 05-09-2017 - ALM4032 - Manuel Ochoa - END
										listAttAux.add(item);

									}
								}


							}
						}
				
				}
			}


		}
		System.debug('prodIdFLevelSet: '+prodIdFLevelSet);

		List<NE__Lov__c> requiredLOVList = new List<NE__Lov__c> ();
		List<NE__Lov__c> requiredLOVListAux = new List<NE__Lov__c> ();

		if (!prodIdFLevelSet.isEmpty()){
			//search required attributes in the LOV
			 requiredLOVList = [SELECT Id,Name, NE__Type__c, NE__Value2__c, NE__Value3__c, Text_Area_Value__c from NE__Lov__c where NE__Type__c ='FieldsRequiredForService'];
		}
	
        System.debug('requiredLOVList: '+requiredLOVList);
        
	for (Integer j=0; j<requiredLOVList.size(); j++){	
		for (String pfl : prodIdFLevelSet){
              System.debug('pfl: '+pfl);
              System.debug('requiredLOVList[j].NE__Value2__c: '+requiredLOVList[j].NE__Value2__c);
			if (pfl == requiredLOVList[j].NE__Value2__c){
				requiredLOVListAux.add(requiredLOVList[j]);
                System.debug('pfl: '+pfl);
                
			}
		}
	}
     
		System.debug('requiredLOVList: '+requiredLOVList);
		System.debug('requiredLOVListAux: '+requiredLOVListAux);

		List<String> lovFamAttList = new List<String>();
		List<String> famAttrList = new List<String>();
		

	    if(!requiredLOVListAux.isEmpty()){
			//lovFamAttList = requiredLOVList[0].NE__Value3__c.split(';');
			//AAP 08/05/2017
			lovFamAttList = requiredLOVListAux[0].Text_Area_Value__c.split(';');
            //AAP 08/05/2017
			System.debug('lovFamAttList: '+lovFamAttList);
			for (Integer t=0; t<lovFamAttList.size();t++){
				famAttrList = lovFamAttList[t].split(':');
				for(RowWrapper lovRW : listAttAux){
					if ((famAttrList[0]==lovRW.family) && (famAttrList[1]==lovRW.attribute)){
						lovRW.required=true;
						lovRW.selected=true;
					}
				}
			}
		}

		System.debug('listAttAux: ' + listAttAux);
		return listAttAux;
	}


	/*public void generateJSON(){
		
		system.debug('GENERATING JSON');

		listAttJSON = JSON.serialize(listAtt);

		selectedListAtt = listAtt;

		system.debug('listAttJSON: ' + listAttJSON);

		

	}*/

	    public void generateDataGrid (){

		list<String> orderIdList = new List<String>();
		orderIdList.add(orderId);
	
        System.debug('******************** ORDER LIST:' + orderIdList);

       	List<StructureWrapper> sw = getStructure(orderIdList[0]);
        System.debug(sw);

        if(!sw.isEmpty()){

            List<CompleteRowData> completeRowDataList = new List<CompleteRowData>();

            for(String ordId : orderIdList){
                CompleteRowData completeData = new CompleteRowData();
                completeData.orderData = getOrderData(ordId);
                //completeData.OIData = getList(ordId,sw);
                completeData.OIData = compData; 

                completeRowDataList.add(completeData);
            }
       		//list of selected attributes
			selectedListAtt = compData;

            completeDataListJSON = JSON.serialize(completeRowDataList);
            
            System.debug(completeDataListJSON);
        }
	
        
    }


	 public OrderDataWrapper getOrderData(String orderId){
 
        OrderDataWrapper orderData = new OrderDataWrapper();

		NE__Bit2WinStandard__c bwstandard = NE__Bit2WinStandard__c.getInstance();
		enableStandardSalesforce = bwstandard.NE__EnableStandardSalesforce__c;

		if(!enableStandardSalesforce){
			String hId = 'HoldingId__c';
			String ccenter= 'Cost_Center__c';
			//String ccenter= 'Cost_Center__c,Cost_Center__r.Name'; // Manuel Ochoa 16-05-2017
			String aUser = 'Authorized_User__c';
			String st = 'Site__c';
			List<NE__Order__c> orderList =Database.query('SELECT Id,NE__AccountId__c, NE__AccountId__r.Name,NE__CatalogId__r.Name,'+hId+',NE__ServAccId__c,NE__ServAccId__r.Name,'+ccenter+',NE__BillAccId__c,'+st+','+aUser+', NE__Contract_Account__c, NE__AssetEnterpriseId__c FROM NE__Order__c WHERE Id=:orderId');

			for (NE__Order__c ord : orderList){
				orderData.configurationId = ord.Id;
				orderData.accountName = ord.NE__AccountId__r.Name;
				orderData.catalogName = ord.NE__CatalogId__r.Name;
				orderData.orderType = 'InOrder';
				orderData.confHoldingId = ord.HoldingId__c;
				orderData.confServAccId = ord.NE__ServAccId__c;
				orderData.confCostCenterId = ord.NE__BillAccId__c;
				orderData.confBussUnitId =ord.NE__ServAccId__c; // Manuel Ochoa 16-05-2017
        		orderData.confBussUnitName =ord.NE__ServAccId__r.Name; // Manuel Ochoa 16-05-2017
				orderData.assetEnterpriseId = ord.NE__AssetEnterpriseId__c;

				//TO ADD MORE FINAL COLUMNS
				 orderData.confAccId = ord.NE__AccountId__c;
				 orderData.confSiteId = ord.Site__c;
				 orderData.confAUId = ord.Authorized_User__c;
				 orderData.confContractId = ord.NE__Contract_Account__c;
			 }
		} else{
			orderData.configurationId = orderId;
			orderData.accountName = 'GenePoint';
			orderData.catalogName = 'HubDevCatalog';
			orderData.orderType = 'InOrder';
			orderData.confHoldingId = '00141000005M31s';
			orderData.confServAccId = '00141000004wRAr';
			orderData.confCostCenterId = '00141000004wRAr'; 
			orderData.assetEnterpriseId = orderId;

			//TO ADD MORE FINAL COLUMNS
			// orderData.confAccId = '00141000005M31s';
			// orderData.confSiteId = '00141000004wRAr';
			// orderData.confAUId = 'AUTHORIZED USER';
			 //orderData.confContractId = '00141000005M31sAAC';
			}
        return orderData;
    }



	public PageReference echoVal(){
			//String param = Apexpages.currentPage().getParameters().get('csvGrid');

		if(!Test.isRunningTest())
		{
			String param = enteredText1;
			System.debug(param);

			return importFile(param);
		}else {
			return null;
		}

	}


	public  PageReference importFile(String csvFile)
    {
		Integer linesNumber=0;
	
	    Savepoint savePointCheckout = Database.setSavepoint();
        try
        {
            if(csvFile != null)
            {
				//Bit2WinHUBUtilities_dev.importFile(csvFile);
				//llamada para que me devuelva el numero de bloques en los que parte el csv (numBlocks)
				//List<List<String>> totalBlocks = Bit2WinHUBUtilities_dev.safeSplit(csvFile, '\n', Bulk_Import_Request_Methods.BLOCKSIZE); //CHANGED
				List<List<String>> totalBlocks = safeSplit(csvFile, '\n', BLOCKSIZE); 

				//calculate number of csv lines
				for (List<String> tb: totalBlocks){
					linesNumber = linesNumber + tb.size();
				}
				linesNumber=linesNumber-totalBlocks.size();

				//generate requestId 
				String requestId = generateIdentifier('req');
				System.debug('****************** REQUEST Id: '+requestId);

				//Creacion de la BIR
				Bit2WinHUB__Bulk_Import_Request__c BIR = new Bit2WinHUB__Bulk_Import_Request__c();
				BIR.Bit2WinHUB__Request_Id__c = requestId;
				//BIR.Total_Blocks__c=totalBlocks.size();
				BIR.Bit2WinHUB__Total_Blocks__c= linesNumber;
				BIR.Bit2WinHUB__Processed_Blocks__c=0;

				insert BIR;

				// Creacion attach con el csv a importar

					Attachment taskAttach       =   new Attachment();
                    taskAttach.Body             =   blob.valueOf(csvFile);  
                    taskAttach.ParentId         =   BIR.Id;
                    taskAttach.Name             =   'CSV Load Request: '+requestId+'.csv';				                    
                    insert taskAttach;

				BIR.Bit2WinHUB__Step__c = 'CreateNTXML';
				update BIR;

				if(!Test.isRunningTest())
				{
					//call CreateNonTechXMLBatch to start batch process
					Id batchInstanceId = Database.executeBatch(new Bit2WinHUB.CreateNonTechXMLBatch(BIR.Id,mail), 1);
				}

				//redirect load status page (BulkImportBatchProgress) 
				return redirect(BIR.Id);
			}else{

				return null;
			}
		}catch(Exception e){
            system.debug('Exception found: '+e+' at line:'+e.getLineNumber()); 
            //importResult = 'Exception found: '+e+' at line: '+e.getLineNumber();
            //importStatus = -1;
            DataBase.rollback(savePointCheckout);

			return null;	
        }
		
	
		return null;
	
	}



	// Generate Request Identifier Methods 

	public static String generateRandomNumber() {
        String randomNumber = generate();

        if (randomNumber.length() < 10) {
        	String randomNumber2 = generate();
            randomNumber = randomNumber + randomNumber2.substring(0, 10 - randomNumber.length());
        }
        
        return randomNumber;
    }
    
    public static String generateIdentifier(String prefix) {
        return prefix + '-' + generateRandomNumber();
    }
    
    public static String generate() {
        return String.valueOf(Math.abs(Crypto.getRandomInteger()));
    }
    

	public PageReference redirect(String birId){
			System.debug('****REDIRECTING****');
            PageReference pageRef = new PageReference('/apex/Bit2WinHUB__BulkImportBatchProgress?BIRId=' + birId);
            return pageRef;
     
	}
	//SAFE SPLIT METHOD
	public static List<List<String>> safeSplit(String inStr, String delim, Integer bSize)
	{
		Boolean moreThanOneBlock = false;
		
		Integer regexFindLimit = 100;

		Integer regexFindCount = 0;
    
		List<List<String>> blockOutput = new List<List<String>>();
		List<String> output = new List<String>();
    
		Matcher m = Pattern.compile(delim).matcher(inStr);
    
		Integer lastEnd = 0;

		List<String> header = new List<String>();

		while(!m.hitEnd())
		{
			while(regexFindCount < regexFindLimit && !m.hitEnd())
			{
				
				if(m.find())
				{
					if(inStr.substring(lastEnd, m.start()) != null && inStr.substring(lastEnd, m.start()) !=''){
						output.add(inStr.substring(lastEnd, m.start())); 
					}	 
					lastEnd = m.end();
				
					if (header.isEmpty()){
						header.add(output[0]);
					}
				}
				else
				{
					if(inStr.substring(lastEnd) != null && inStr.substring(lastEnd) !=''){
						output.add(inStr.substring(lastEnd));
					}
					lastEnd = inStr.length();
				}
            
				regexFindCount++;
			}

			// Note: Using region() to advance instead of substring() saves 
			// drastically on heap size. Nonetheless, we still must reset the 
			// (unmodified) input sequence to avoid a 'Regex too complicated' 
			// error.
			m.reset(inStr);        
			m.region(lastEnd, m.regionEnd());
        
			regexFindCount = 0;
			//System.debug('********************** OUTPUT SIZE: '+output.size());				
			if (output.size() == bSize){

				moreThanOneBlock = true;

				if (blockOutput.size()>0){
					List<String> outputAux = new List<String>();
					outputAux.addAll(header);
					outputAux.addAll(output);
					blockOutput.add(outputAux);
					//System.debug('********************** OUTPUT BLOCKS SIZE: '+blockOutput.size());		
					//System.debug('********************** OUTPUT SIZE: '+output.size());			
					output = new List<String>();
				} else {
					blockOutput.add(output);
					//System.debug('********************** OUTPUT BLOCKS SIZE: '+blockOutput.size());
					//System.debug('********************** OUTPUT SIZE: '+output.size());	
					output = new List<String>();
				}
			}
		}	

		if (output.size() > 0) {
			if(moreThanOneBlock){
				List<String> outputAux = new List<String>();
				outputAux.addAll(header);
				outputAux.addAll(output);
				blockOutput.add(outputAux);
				System.debug('********************** OUTPUT BLOCKS SIZE: '+blockOutput.size());
				System.debug('********************** OUTPUT SIZE: '+output.size());	
			}else{
				blockOutput.add(output);
				System.debug('********************** OUTPUT BLOCKS SIZE: '+blockOutput.size());
				System.debug('********************** OUTPUT SIZE: '+output.size());	
			}
		}
		return blockOutput;
	
	}



	public class RowWrapper{
		
		public Boolean selected {get;set;}
		public Integer itemLevel {get;set;}
        public integer itemIndex {get;set;} // 05-09-2017 - ALM4032 - Manuel Ochoa - Fix to process correctly correlative OIs iwth the same family and product name
		public String product {get;set;}
		public String family {get;set;}
		public String attribute {get;set;}
		public String value {get;set;}
		public Boolean required {get;set;}
		public String xType {get;set;}
		public List<String> enumeratedValues {get;set;}		
		public Id assetItemEnterpriseId {get;set;}
		public String catalogCategoryName {get;set;}//INC000000130969 Álvaro López
	}

	public class StructureWrapper{
		public Id orderItemId {get;set;}
		public Id catalogItemId {get;set;}
		public Id productId {get;set;}
		public integer itemLevel {get;set;}
        public integer itemIndex {get;set;} // 05-09-2017 - ALM4032 - Manuel Ochoa - Fix to process correctly correlative OIs iwth the same family and product name
		public Id assetItemEnterpriseId {get;set;}
	}

	 public class OrderDataWrapper{
		public String configurationId {get;set;}
		public String accountName {get;set;}
		public String catalogName {get;set;}
        public String confHoldingId {get;set;}
        public String confServAccId {get;set;}
        public String confCostCenterId {get;set;}
        public String confCostCenterName {get;set;} // Manuel Ochoa 16-05-2017
        public String confBussUnitId {get;set;} // Manuel Ochoa 16-05-2017
        public String confBussUnitName {get;set;} // Manuel Ochoa 16-05-2017
        public String orderType{get;set;}
        public String assetEnterpriseId{get;set;}
		
		public String confAccId{get;set;}
		public String confSiteId{get;set;}
		public String confAUId{get;set;}
		public String confContractId{get;set;}
		
 	}

	public class CompleteRowData{
        public orderDataWrapper orderData {get;set;}
        public List<RowWrapper> OIData {get;set;}
    }
    
    // 16-05-2017 - Manuel Ochoa Florez 

    public void getAuthorizedUsersAndContract() 
    {
        System.debug('test');
        usersList = new List<SelectOption>();
        if (orderObject != null && orderObject.NE__AccountId__c != null) 
        {
            
            List<User> resultList = [SELECT Id, Name 
                                     FROM User
                                     WHERE Contact.AccountId = :accId
                                        AND IsActive = true
                                     ORDER BY Name];
            if (resultList.size() == 0) {
                usersList.add(new SelectOption('','-No users available-'));
            } else {
                //usersList.add(new SelectOption('','-Select a user-'));
                for (User u : resultList) {
                    usersList.add(new SelectOption(u.Id, u.Name));
                }
            }
        } else {
            usersList.add(new SelectOption('','-No users available-'));
        }
        
        //Get Contract
        List<NE__Contract_Account_Association__c> contractAccounts = [SELECT Id,Name FROM NE__Contract_Account_Association__c WHERE NE__Account__c = :accId];
        if (contractAccounts.size() > 0) {
            contAccId = contractAccounts[0].Id;
            contAccName = contractAccounts[0].Name;
        } else {
            contAccName = null;
            contAccId = null;
        }        
    }

    

    public void getBusinessUnits() {

    	Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_BUSINESS_UNIT);
           
        if(accId != null){        
            businessUnitOptions = new List<SelectOption>();
            //sitesOptions = new List<SelectOption>();
            
			getCustomerCountry();

            List<Account> listBusinessUnit= [SELECT Id, Name, Parent.Parent.BI_Country__c
            								FROM Account 
                                           WHERE TGS_Aux_Legal_Entity__c= :accId AND 
                                                 RecordTypeId= :rtId];
            
            if(!listBusinessUnit.isEmpty()){
            	System.debug('Changing BU list -  AccId: ' + accId);
                if(!checkFirstExec){
            		buId=listBusinessUnit[0].Id;            		
            	}else{
            		checkFirstExec=false;
            		System.debug('First execution: getBusinessUnits');
            	}
	            for (Account businessUnit : listBusinessUnit){
	            	businessUnitOptions.add(new SelectOption(businessUnit.Id, businessUnit.Parent.Parent.BI_Country__c +' - ' +businessUnit.Name));
	            }
         	
	            getAuthorizedUsersAndContract();
	            getCostCenters();
	        }else{
	        	usersList = new List<SelectOption>();
	        	usersList.add(new SelectOption('','-No users available-'));
	        	businessUnitOptions.add(new SelectOption('','-No Business Units available-'));
	        	//costCenterOptions = new List<SelectOption>();
	        	//costCenterOptions.add(new SelectOption('','-No Cost Centers available-'));
	        	// Álvaro López - 18/04/2018
	        	varOption1 = new List<SelectOption>();
	        	varOption1.add(new SelectOption('','-No Sites available-'));
	        }
        }
        
               
    }

    
    public void getCostCenters() {

    	Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_COST_CENTER);        
        
 		if(custCtryId != null){
 			costCenterOptions = new List<SelectOption>();
            
            List<Account> listCostCenter= [SELECT Id, Name, Parent.Parent.Parent.BI_Country__c 
                                           FROM Account 
                                           WHERE Parent.Parent.Parent.id= :custCtryId AND 
                                                 RecordTypeId= :rtId];
            
            if(!listCostCenter.isEmpty()){
	            for (Account costCenter : listCostCenter){
	                costCenterOptions.add(new SelectOption(costCenter.Id, costCenter.Parent.Parent.Parent.BI_Country__c +' - ' +costCenter.Name));
	            }
	        }else{
	        	costCenterOptions.add(new SelectOption('','-No Cost Centers available-'));
	        }
        }else{
        	costCenterOptions = new List<SelectOption>();
        	costCenterOptions.add(new SelectOption('','-No Cost Centers available-'));
        }

        // sites options
        System.debug('Sites list - BuId: ' + buId);
        Integer listMaxSize = 1000;
		if(buId != null){ // Álvaro López - 18/04/2018
			varOption1 = new List<SelectOption>();
       	 	varOption2 = new List<SelectOption>();
	        List<BI_Punto_de_instalacion__c> listSites = [SELECT Id, Name, BI_Cliente__c, TGS_ISO_Code__c, Pais__c
	                                   FROM BI_Punto_de_instalacion__c
	                                   WHERE BI_Cliente__c= :buId];

	        if(!listSites.isEmpty()){
	        	System.debug('Changing Sites list - BuId: ' + buId);
	        	if(listSites.size() > listMaxSize){
	        		Integer numIterations = (Integer)(listSites.size()/listMaxSize) + 1;
            		Integer index = 0;
		        	for (Integer i=0; i<numIterations; i++){
		        		Integer startIndex;
		                Integer endIndex;
		                if(index>=0 && index<listSites.size()){
		                    startIndex=index;
		                    endIndex=index + listMaxSize;
		                }
		                List<SelectOption> auxList = new List<SelectOption>();
		                for(Integer j=startIndex; j<endIndex; j++){
		                    if(j<listSites.size()){
		                    	if(i==0){
		                        	varOption1.add(new SelectOption(listSites.get(j).Id, listSites.get(j).Pais__c +' - ' + listSites.get(j).Name));
		                        }
		                        else{
									varOption2.add(new SelectOption(listSites.get(j).Id, listSites.get(j).Pais__c +' - ' + listSites.get(j).Name));
		                        }
		                        //sitesOptions.add(new SelectOption(listSites.get(j).Id, listSites.get(j).Pais__c +' - ' + listSites.get(j).Name));
		                    }
		                }
		                index = endIndex;
			        	//sitesOptions.add(new SelectOption(site.Id, site.Pais__c +' - ' + site.Name)); 
		        	}
	        	}
	        	else{
	        		for (BI_Punto_de_instalacion__c site : listSites){
		        		varOption1.add(new SelectOption(site.Id, site.Pais__c +' - ' + site.Name));
		        	}
	        	}               	
	    	}else{
	    		varOption1.add(new SelectOption('','-No Sites available-'));
	    	}
    	}else{
    		varOption1 = new List<SelectOption>();
	       	varOption1.add(new SelectOption('','-No Sites available-'));
	    }
        
    }

    private void getCustomerCountry(){
    	
        List<Account> auxAccounts= [SELECT Id, Name, parent.id
        								FROM Account 
                                       WHERE id=:accid];
        if(!auxAccounts.isEmpty()){
        	for (Account auxAccount : auxAccounts){
                custCtryId=auxAccount.parent.id;
                system.debug('Customer County ID: ' +  custCtryId);
                //getCostCenters();
            }
        }
    }    
}