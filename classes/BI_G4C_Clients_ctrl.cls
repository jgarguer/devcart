public with sharing class BI_G4C_Clients_ctrl {   
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García

Company:       everis

Description:   Controller for the G4C Clients Map Lightning Components
History:
<Date>                          <Author>                                <Code>              <Change Description>
20/06/2016                      Julio Alberto Asenjo García                                 Initial version
22/06/2016                      Julio Alberto Asenjo García                                 Cambio en método  getState para corregir funcioanamiento
29/06/2016                      Julio Alberto Asenjo García                                 Cambio en método  getMyAccounts
05/07/2016                      Julio Alberto Asenjo García                                 Cambio de funcionalidad para mostrar los clientes de todos lo susarios y filtrar por ellos.
21/07/2016                      Eduardo Ventura Izquierdo Nuñez         E001                Inicidencia filtros anidados: No era posible filtrar por estado de oportunidad si ya existía un filtro previamente aplicado.
21/07/2016                      Eduardo Ventura Izquierdo Nuñez         E002                Inicidencia información de búsqueda: Se han de mostrar la consulta realizada por el filtro.
29/07/2016                      Eduardo Ventura Izquierdo Nuñez         E003                Inicidencia información de búsqueda: Se ha de mostrar el propietario de la oportunidad en la búsqueda filtrada.
01/09/2016                      Julio Alberto Asenjo García             E004                Cambios para solucionar problemas con los límites de las querys 
12/09/2016						Julio Alberto Asenjo García 								Añadimos filtro por defecto para que al iniciar el usuario sólo vea los clientes de los que es propietario. 
29/11/2016						Carlos Bastidas Hernandez				ESTE-F2-03			Se elimina del where en la query para visualizar clientes no geolocalizados "ShippingLatitude !=null AND ShippingLongitude !=null AND"
02/12/2016						Carlos Bastidas Hernandez				ESTE-F2-04			Se genera metodo que crea las entidades Sede Comercial y Dirección para los clientes no Geolocalizados
15/12/2016						Carlos Bastidas Hernandez				ESTE-F2-05			se modifican las querys para añadir los nuevos filtros
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public Static String campaniaSelect ='';
    public Static List<Id> idsFiltradas;
    public Static List<string> filterData;
    
    public BI_G4C_Clients_ctrl() {
        System.debug('Inicializamos el método');
    }
    
    //Método que devuelve una lista con los Ids de los usuarios que reportan al usuario que ejecuta el código, y al propio usuario. 
    @AuraEnabled
    public static String getUserName() {
        return UserInfo.getUserId();
    }
    
    public static String getListToString(List <Id> hijos){
        String ret='( \'';
        integer numHijos=hijos.size();
        for (Id i :hijos){
            ret+=i;
            numHijos--;
            if (numhijos>0){
                ret+='\', \'';
            } else{
                ret+='\')';
            }
        }
        return ret;
    }
    
    @AuraEnabled
    public static List<Account> getMyAccounts() {
        
        //Obtenemos la lista de todos los usuarios
        //E004 
        //   Limitamos los resultados a 80
        //#ESTE-F2-03
        // se elimina del where en la query para visualizar clientes no geolocalizados "ShippingLatitude !=null AND ShippingLongitude !=null AND"
        List<Account> c=[SELECT id, Owner.Name, BI_G4C_ShippingLatitude__c, BI_G4C_ShippingLongitude__c,BI_G4C_Estado_Coordenadas__c,name,AnnualRevenue,NumberOfEmployees,BI_G4C_Bookmarked__c,BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c,BI_Sector__c,BI_Subsector__c,BI_G4C_visited__c,BI_G4C_senyalizado__c,ShippingStreet,ShippingCity, ShippingState, ShippingPostalCode,Phone,Website,
                         (SELECT StageName, AccountId, Id, CreatedDate from Opportunities ORDER BY CreatedDate DESC LIMIT 1),
                         (SELECT id from events where BI_G4C_Caducada__c=false and BI_FVI_Estado__c!='Finalizada')
                         FROM Account WHERE OwnerId=:UserInfo.getUserId() ORDER BY LastModifiedDate DESC LIMIT 150 ];
        setFilterData('null');
        System.debug('La lista tiene '+c.size()+' elementos');
        return c;
    }
    
    @AuraEnabled
    public static List<Account> getFilterAccounts(String filters,String campania, String listaOwners, String filtroEstado, String newFilter) {
        
        System.debug('newFilter: '+ newFilter);
        /*INI E002*/
        List<Account> lRet =new  List<Account>();
        
        //ESTE-F2-05
        if(filters!='' || newFilter != '') {
            
            if(!Test.isRunningTest()) {
                setFilterData(filters);
            }
            /*FIN E002*/
            string sIds='';
            // #ESTE-F2-05 nueva lista de id para los nuevos filtros 
            List<Id> idsNewFilter = BI_G4C_Clients_ctrl.getNewFilter(newFilter);
            List<Id> ids = new List<Id>();
            
            // #ESTE-F2-05 si los dos filtros tienen resultados retornamos los comunes
            if(!string.isEmpty(filters)) {
                if (newFilter!='todos'){
                    ids = BI_G4C_Clients_ctrl.getAuxFilter(filters);
                    
                    System.debug('$$$$$$La lista 1-mi Filtro'+ids );
                    System.debug('$$$$$$La lista 2-chi Filtro'+idsNewFilter );
                    
                    List<Id> idsAux = new List<Id>();
                    
                    for(Id idAux : ids) {
                        for(Id idNewAux : idsNewFilter) {
                            if(idAux == idNewAux) {
                                idsAux.add(idAux);
                            }
                        }
                    }
                    System.debug('$$$$$$La lista de IDS es'+idsAux );
                    ids = idsAux;
                } else {
                   ids = BI_G4C_Clients_ctrl.getAuxFilter(filters);
                    
                }
            }else {
                // #ESTE-F2-05 si no hay filters asignamos los resultados del nuevo filtro 
                ids = idsNewFilter;
            }
            
            if (ids.size()>0) {
                sIds='id in ';
                sIds+=getListToString(ids);
                //#ESTE-F2-03
                // se elimina el codigo "sIds+=' AND ';"
                String query= 'SELECT id, BI_G4C_ShippingLatitude__c, BI_G4C_ShippingLongitude__c,BI_G4C_Estado_Coordenadas__c,name,BI_G4C_icon_type__c,AnnualRevenue,NumberOfEmployees, BI_G4C_Bookmarked__c,BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c,BI_Sector__c,BI_Subsector__c,BI_G4C_visited__c,BI_G4C_senyalizado__c,ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode,Phone,Website,';
                query+='(';
                query+= getState(campania, listaOwners ,FiltroEstado);
                query+='),';
                query+='(SELECT id FROM events WHERE BI_G4C_Caducada__c=false AND BI_FVI_Estado__c!=\'Finalizada\') ';
                //#ESTE-F2-03
                // se elimina del where en la query para visualizar clientes no geolocalizados "ShippingLatitude !=null AND ShippingLongitude !=null AND"
                query+='FROM Account ';
                
                if(!String.isBlank(sIds) ) {
                    query+=' WHERE '+sIds; 
                }
                
                query+=' ORDER BY Name ASC LIMIT 150';
                /*INI E003*/
                System.debug('La query para las cuentas filtradas 1 es: '+ query);
                lRet=Database.query(query);
                /*FIN E003*/
            }
        } 
        
        return lRet;
    }
    // #ESTE-F2-05 metodo que retorna los id del nuevo filtro deacuerdo a 
    // la seleccion de misClientes,miembrosEquipo,misClientesMiembrosEquipo y todos
    public static List<Id> getNewFilter(String newFilter) {
        
        List<Account> cuentas = null;
        List<Id> retornoIds = new List<Id>();
        
        if('misClientes' == newFilter) {
            cuentas = [SELECT Id FROM Account WHERE OwnerId =:UserInfo.getUserId()  ];
        }
        
        if('miembrosEquipo' == newFilter) {
            List<AccountTeamMember> listAccountId = [SELECT AccountId FROM AccountTeamMember WHERE userId =:UserInfo.getUserId()];
            if(!listAccountId.isEmpty()) {
                List<Id> listIds = new List<Id>();
                for(AccountTeamMember aTeam :listAccountId){
                    if(aTeam.userId != UserInfo.getUserId()) {
                        listIds.add(aTeam.AccountId);
                    }
                }
                cuentas =  [SELECT Id FROM Account  WHERE Id in :listIds LIMIT 1500];
            }
        }
        
        if('misClientesMiembrosEquipo' == newFilter) { 
            cuentas = [SELECT Id FROM Account WHERE OwnerId =:UserInfo.getUserId() LIMIT 1500];
            List<AccountTeamMember> listAccountId = [SELECT AccountId FROM AccountTeamMember WHERE userId =:UserInfo.getUserId()];
            if(!listAccountId.isEmpty()) {
                List<Id> listIds = new List<Id>();
                for(AccountTeamMember aTeam :listAccountId){
                    listIds.add(aTeam.AccountId);
                }
                cuentas.addAll([SELECT Id FROM Account  WHERE Id =:listIds LIMIT 1500]);
            }
            
        }
        
        if('todos' == newFilter) {
            cuentas =  [SELECT Id FROM Account LIMIT 150];
        }
        
        if(cuentas != null && !cuentas.isEmpty()) {
            for (Account acc : cuentas ) {
                retornoIds.add(acc.Id);
            }
        }
        
        return retornoIds;
    }
    
    public static List<Id> getAuxFilter(String filters){
        System.debug('Llamada a getAuxFilters con: '+filters);
        String query ='';
        String aux;
        List<Id> lIds=new List<Id> ();
        //Si sólo filtramos por usuario 
        if(!filters.contains('AND') && filters.contains('Owner')) {
            query ='SELECT Id FROM Account  ';
            if(filters!=''){
                query +=  'WHERE ';
                query += filters;
            }
            query +='ORDER BY LastModifiedDate DESC LIMIT 150';
            List<sObject> preids = Database.query(query);
            for (SObject obs: preids){
                aux=String.valueOf(obs.get('Id'));
                lIds.add(aux);
            }
        }
        
        //Si sólo filtramos por Nombre
        else if(!filters.contains('AND') && filters.contains('LIKE')){
            query ='SELECT Id FROM Account  ';
            if(filters !=''){
                query +=  'WHERE ';
                query += filters;
            }
            query +='ORDER BY LastModifiedDate DESC LIMIT 80';
            List<sObject> preids = Database.query(query);
            for (SObject obs: preids){
                aux=String.valueOf(obs.get('Id'));
                lIds.add(aux);
            }
            
        }//Si filtramos por nombre y por usuario 
        else if( filters.contains('Owner') && filters.contains('LIKE') && !filters.contains('Stage')&& !filters.contains('Campaign')){
            query ='SELECT Id FROM Account  ';
            if(filters!=''){
                query +=  'WHERE ';
                query += filters;
            }
            query +='ORDER BY LastModifiedDate DESC LIMIT 150';
            List<sObject> preids = Database.query(query);
            for (SObject obs: preids){
                aux=String.valueOf(obs.get('Id'));
                lIds.add(aux);
            }
            
        } else {
            query ='SELECT AccountId, CampaignId FROM Opportunity  ';
            if(filters!=''){
                query +=  'WHERE ';
                query += filters;
            }
            //E004 
            //  Limitamos los resultados a 80
            query +='ORDER BY LastModifiedDate DESC LIMIT 150';
            //E004 
            //  Limitamos los resultados a 80
            if (filters.containsIgnoreCase('CampaignId')) { 
                campaniaSelect=filters.substringAfter('CampaignId =\'').left(15);
            }
            system.debug(query);
            List<sObject> preids = Database.query(query);
            for (SObject obs: preids){
                aux=String.valueOf(obs.get('AccountId'));
                lIds.add(aux);
            }
        }
        
        if (filters.contains('Sin estado')) {
            String auxNombre='';
            if(filters.contains('Account.Name LIKE')){
                system.debug('filters.indexOf(Account.Name LIKE) '+ filters.indexOf('Account.Name LIKE'));
                system.debug('filters.indexOf(%\')'+filters.indexOf('%\''));
                auxNombre=' AND ';
                auxNombre+=filters.mid(filters.indexOf('Account.Name LIKE'),filters.indexOf('%\'')+2);
            }
            
            if(filters.contains('OwnerId IN (')) {
                system.debug('filters.indexOf(OwnerId IN) '+ filters.indexOf('OwnerId IN '));
                system.debug('filters.indexOf(%\')'+filters.indexOf('%\''));
                auxNombre+=' AND ';
                auxNombre+=filters.mid(filters.indexOf('OwnerId IN'),filters.indexOf('\')',filters.indexOf('OwnerId IN'))+2);
            }
            
            // #ESTE-F2-05 se elimjina el LIMIT 80 para que funcione con el nuevo filtro
            String query2= 'SELECT Id FROM ACCOUNT WHERE id NOT in (SELECT accountid FROM opportunity) '+auxNombre;
            system.debug('@@@@@@@ '+query2 );
            for (SObject obs: Database.query(query2)){
                aux=String.valueOf(obs.get('Id'));
                lIds.add(aux);
            }            
        }
        System.debug('la sentencia SQL ejecutada es ' + query);
        System.debug('devuelvo ' + lIds);
        return lIds;
    }
    
    @AuraEnabled
    public static String getState(String campania, String listaOwners, String filtroEstado){
        
        system.debug('campania: '+campania+' listaOwners: '+listaOwners+'filtroEstado: '+filtroEstado+'@@@');
        
        String query='SELECT StageName, AccountId, Id, CreatedDate FROM Opportunities ';
        //Construimos String para lista de Owner
        String sListaOwner='';
        if(!String.isEmpty(listaOwners)) {
            sListaOwner=' AND OwnerId IN '+listaOwners+' ';
        }else{
            sListaOwner='';
        }
        //SE GENERA EL WHERE DE LA QUERY
        // Si se ha filtrado por cliente y por campania
        if ((campania!=null && campania!='') && idsFiltradas !=null) {
            system.debug('Opción 1');
            query ='WHERE CampaignId=\''+campania+'\' '+sListaOwner + 'AND AccountId in'+idsFiltradas; 
            //Si se ha filtrado por campania
        }else if (campania!=null && campania!=''){
            system.debug('Opción 2');
            if(!String.isEmpty(listaOwners)){
                query +='WHERE CampaignId=\''+campania+'\' AND AccountId IN (select id from Account WHERE OwnerId IN '+listaOwners+')'; 
            }else{
                query +='WHERE CampaignId=\''+campania+'\''; 
            }    
        }else{
            system.debug('Opción 3');
            if(!String.isEmpty(listaOwners)){
                // query +='WHERE AccountId IN (select id from Account WHERE OwnerId IN '+listaOwners+')'; 
            }    
        }
        
        /*INI E001*/
        if (!String.isEmpty(filtroEstado)){
            if(!query.contains('WHERE')){
                query+=' WHERE ';
                query+=filtroEstado;
            }else query+='AND '+filtroEstado;   
        }
        /*FIN E001*/
        //E004 añadimos límite
        //
        //
        //
        
        //
        query+=' ORDER BY CreatedDate DESC LIMIT 1 ';
        System.debug('$$La query de oportunidades es>>'+query+'<ppp>');
        return query;
    }
    
    @AuraEnabled
    public static String getUsuarios(String filter){
        List<PickListItem> resul = new List<PickListItem>();
        PickListItem pil;
        
        pil = new PickListItem();
        pil.value=UserInfo.getUserId();
        pil.label=UserInfo.getName();
        resul.add(pil);

        List<User> l;

        if(filter == 'miembrosEquipo') {
            l =[SELECT Id,Name FROM User where Id  IN (SELECT OwnerId FROM Account WHERE Id IN :getNewFilter('miembrosEquipo')) ORDER By Name ];
        }
        else if(filter == 'misClientesMiembrosEquipo') {
            l =[SELECT Id,Name FROM User where Id  IN (SELECT OwnerId FROM Account WHERE Id IN :getNewFilter('misClientesMiembrosEquipo')) ORDER By Name ];
        }
        else if(filter == 'todos') {
            l =[SELECT Id,Name FROM User where Id  IN (SELECT OwnerId FROM Account) ORDER By Name ];
        }

        for (User c: l){
            If(c.Id!=UserInfo.getUserId()){
                pil = new PickListItem();
                pil.value=String.valueOf(c.get('Id'));
                pil.label=string.valueof(c.get('Name'));
                resul.add(pil);
            }
        }     
        
        String jsonResul = JSON.serialize(resul);        
        return jsonResul;
    }
    
    @AuraEnabled
    public static String getOptions(List<Account> lAccount){
        List<Id> lIDs=new List<Id>();
        for (Account c : lAccount){
            lIDs.add(c.Id);  
        }
        
        List<PickListItem> resul = new List<PickListItem>();
        PickListItem pil;
        pil = new PickListItem();
        pil.value='';
        pil.label='Ninguna';
        resul.add(pil);
        List<AggregateResult> l;
        if(lIDs.isEmpty()){
            System.debug('Opción 1');
            l=new  List<AggregateResult>();
        }else {
            System.debug('Opción 2  '+ lIDs);
            l=[SELECT CampaignId, Campaign.Name campaignName FROM CampaignMember where Campaign.Status='In Progress' AND Campaign.IsActive=true AND ContactId  IN (SELECT Id FROM Contact WHERE AccountId IN :lIDs)GROUP BY CampaignId, Campaign.Name LIMIT 50000 ];
        }
        System.debug('LA LISTA ESSSS '+l);
        for (AggregateResult c: l){
            pil = new PickListItem();
            pil.value=String.valueOf(c.get('CampaignId'));
            pil.label=string.valueof(c.get('campaignName'));
            resul.add(pil);
        }     
        
        String jsonResul = JSON.serialize(resul);        
        return jsonResul;
        
    }
    
    @AuraEnabled
    public static String getEstadoOportunidades(String campania,List<Account> lAccount){
        System.debug('Llamada a getEstadoOportunidades con campania'+campania+' y la lista de IDs '+lAccount);
        List<Id> lIDs=new List<Id>();
        for (Account c : lAccount){
            lIDs.add(c.Id);  
        }
        List<PickListItem> resul = new List<PickListItem>();
        String stageName='StageName';
        PickListItem pil=new PickListItem();
        pil.value='Sin estado';
        pil.label='Sin Estado';
        resul.add(pil);
        List<AggregateResult>l;
        
        System.debug('Llamada a getEstadoOportunidades con campania '+campania+' y la lista de IDs '+lIDs);
        if(lIDs.isEmpty()){
            //Si no se ha filtrado por usuarios
            if(campania==null || campania ==''){
                l=[SELECT toLabel(StageName) FROM Opportunity GROUP BY Stagename LIMIT 4500];
            }else {
                //Si no se ha filtrado por usuarios Y Filtramos por campaña
                l=[SELECT toLabel(StageName) FROM Opportunity WHERE CampaignId=:campania GROUP BY Stagename LIMIT 4500];                
            }
        }else {
            System.debug('Opción 2  '+ lIDs);
            if(campania==null || campania ==''){
                l=[SELECT toLabel(StageName) FROM Opportunity WHERE AccountId IN :lIDs GROUP BY Stagename LIMIT 4500];
            }else {
                //Filtramos por usuario y por campaña
                l=[SELECT toLabel(StageName) FROM Opportunity WHERE AccountId IN :lIDs AND CampaignId=:campania GROUP BY Stagename LIMIT 4500];                
            }
        }
        for (AggregateResult c: l){                 
            pil = new PickListItem();
            pil.value=String.valueOf(c.get(StageName));
            pil.label=string.valueof(c.get(StageName));
            resul.add(pil);
        } 
        String jsonResul = JSON.serialize(resul);      
        
        
        return jsonResul;
        
    }
    
    
    @AuraEnabled
    public static List<Account> getVisitados(String StringIds,String campania, String listaOwners, String filtroEstado){
        
        List<Id>listIds= (List<Id>)System.JSON.deserialize(StringIds, List<Id>.class);
        List<Account>lRet= new List<Account>();
        List<Id>listIdsAcco=new List<Id>();
        String sIds='';
        for(AggregateResult a : [SELECT AccountId FROM Event WHERE AccountId IN:listIds AND BI_FVI_Estado__c !='planificada' GROUP BY AccountId ]){
            listIdsAcco.add((Id)a.get('AccountId'));
        }
        
        if (listIdsAcco.size()>0){
            sIds='id in ';
            sIds+=getListToString(listIdsAcco);
            //sIds+=' AND ';
            sIds+='  ';
            String query= 'SELECT id, BI_G4C_ShippingLatitude__c, BI_G4C_ShippingLongitude__c,BI_G4C_Estado_Coordenadas__c,name,BI_G4C_icon_type__c,AnnualRevenue,NumberOfEmployees, BI_G4C_Bookmarked__c,BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c,BI_Sector__c,BI_Subsector__c,BI_G4C_visited__c,BI_G4C_senyalizado__c,ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode,Phone,Website,';
            query+='(';
            query+=getState(campania, listaOwners,FiltroEstado);
            query+='),';
            query+='(SELECT id FROM events WHERE BI_G4C_Caducada__c=false AND BI_FVI_Estado__c!=\'Finalizada\') ';
            //#ESTE-F2-03
            // se elimina del where en la query para visualizar clientes no geolocalizados "ShippingLatitude !=null AND ShippingLongitude !=null AND"
            query+='FROM Account ';
            if(!String.isBlank(sIds)) {
                query+=' WHERE '+sIds; 
            }
            query+=' ORDER BY LastModifiedDate DESC LIMIT 150';
            /*INI E003*/
            System.debug('La query para las cuentas filtradas es: '+ query);
            lRet=Database.query(query);
        }
        
        
        return lRet;
        
    }
    
    @AuraEnabled
    public static List<Account> getNoVisitados(String StringIds,String campania, String listaOwners, String filtroEstado){  
        List<Id>listIds= (List<Id>)System.JSON.deserialize(StringIds, List<Id>.class);
        List<Account>lRet= new List<Account>();
        //extraemos todas las cuentas que podemos filtrar y las metemos en un mapa
        
        String sIds='';
        
        
        if (listIds.size()>0){
            sIds='id in ';
            sIds+=getListToString(listIds);
            sIds+=' AND ';
            String query= 'SELECT id, BI_G4C_ShippingLatitude__c, BI_G4C_ShippingLongitude__c,BI_G4C_Estado_Coordenadas__c,name,BI_G4C_icon_type__c,AnnualRevenue,NumberOfEmployees, BI_G4C_Bookmarked__c,BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c,BI_Sector__c,BI_Subsector__c,BI_G4C_visited__c,BI_G4C_senyalizado__c,ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode,Phone,Website,';
            query+='(';
            query+=getState(campania, listaOwners,FiltroEstado);
            query+='),';
            query+='(SELECT id FROM events WHERE BI_G4C_Caducada__c=false AND BI_FVI_Estado__c!=\'Finalizada\') ';
            query+='FROM Account WHERE '+sIds+' BI_G4C_ShippingLatitude__c !=null AND BI_G4C_ShippingLongitude__c !=null ORDER BY LastModifiedDate DESC LIMIT 80';
            
            lRet=Database.query(query);
        }
        
        Map<Id, Account> mapPre = new Map<Id, Account>(lRet);
        List<Id>listIdsAcco=new List<Id>();
        
        //Obtenemos una lista con las cuentas que han sido visitadas
        for(AggregateResult a : [SELECT AccountId FROM Event WHERE AccountId IN:listIds AND BI_FVI_Estado__c !='planificada' GROUP BY AccountId ]){
            listIdsAcco.add((Id)a.get('AccountId'));
        }
        
        for (Id i : listIdsAcco){
            mapPre.remove(i);
        }
        
        
        lRet=mapPre.values();
        return lRet;
        
    }
    
    @AuraEnabled
    public static String setGeolocalization(String eventId, String geo){
        String resul = 'OK';
        try{
            Event e = [select id, BI_G4C_GeoLocalizacion__c from event where id=:eventId];
            if (e != null){
                e.BI_G4C_GeoLocalizacion__c=geo;
                update e;
            }
            
        }catch(Exception ex){
            resul = 'ERROR: '+ex.getMessage();
        }
        return resul;
        
    }
    
    /*INI E002*/
    @AuraEnabled
    public static List<string> getFilterData(){
        return filterData;
    }
    /*FIN E002*/
    
    /*INI E002*/    
    public static void setFilterData(string filterString){
        system.debug(filterString + ' FILTERSTRINGUNO');
        if(filterData == Null){
            filterData = new List<string>();
            filterData.add('No aplicado');
            filterData.add('No aplicado');
            filterData.add('No aplicado');
        }
        
        //Procesado Filtro de Propietarios
        if(filterString.contains('OwnerId')){
            string stringIds = filterString.substringBetween('OwnerId IN (', ')');
            string query = 'SELECT Name FROM User WHERE Id IN('+stringIds+')';
            List<User> userList = Database.query(query);
            
            string returnValue;
            for(User user : userList){
                if(returnValue == Null) returnValue = user.Name;
                else returnValue = returnValue + ',' + user.Name;
            }
            filterData[0] = returnValue;
        }else filterData[0] = UserInfo.getFirstName()+ ' '+ UserInfo.getLastName() ;
        
        //Procesado Filtro de Campañas
        string stringIdsCampaign;
        if(filterString.contains('CampaignId') && filterString.contains('StageName')){
            stringIdsCampaign = filterString.substringBetween('CampaignId =','AND');
        }else if(filterString.contains('CampaignId')) stringIdsCampaign = filterString.substringAfter('CampaignId =');
        else filterData[1] = 'No aplicado';
        
        if(stringIdsCampaign != null){
            string query = '';
            if(!Test.isRunningTest()){
                query = 'SELECT Name FROM Campaign WHERE Id IN('+stringIdsCampaign+')';
            }else {
                query = 'SELECT Name FROM Campaign';
            }
            List<Campaign> campaignList = Database.query(query);
            
            string returnValue;
            for(Campaign campaign : campaignList){
                if(returnValue == Null) returnValue = campaign.Name;
                else returnValue = returnValue + ',' + campaign.Name;
            }
            filterData[1] = returnValue;
        }
        
        //Procesado Filtro de Etapas
        if(filterString.contains('StageName')){
            filterData[2] = filterString.substringBetween('StageName IN(', ')');
        }else filterData[2] = 'No aplicado';
    }
    /*FIN E002*/
    public class PickListItem {
        
        public String myclass = 'optionClass';
        public String labelClass = 'texto';
        public String label {get; set;}
        public String value {get; set;}   
        public String ptype {get; set;} 
        
    }
    
    /*#ESTE-F2-04*/
    @AuraEnabled
    public static Boolean getSaveCommercialAddress(String acc, String calleComercial, String numComercial, String ciudadComercial,String provinciaComercial, String codigoPostalComercial, String paisComercial) {
        List<Account> accDatosList = [SELECT id, Name, 	BI_G4C_ShippingLatitude__c, BI_G4C_ShippingLongitude__c FROM Account WHERE Id=:acc];
        Boolean resul= false; 
        Account accountDatos = accDatosList.get(0);
        /* String jsonReqContent=JSON.serialize(calleComercial+','+numComercial+','+ciudadComercial+','+provinciaComercial+','+codigoPostalComercial+','+paisComercial);
System.debug('Llamamos a getCordinates ');
System.debug('getCordinates@@@'+calleComercial+','+numComercial+','+ciudadComercial+','+provinciaComercial+','+codigoPostalComercial+','+paisComercial);
List<String> coordinates= getCoordinates(jsonReqContent.replace(' ', ''));
System.debug('getCordinates@@@'+coordinates);
if (coordinates!=null && coordinates.size()==2){
*/
        BI_Sede__c address = new BI_Sede__c();
        address.Name = accountDatos.Name;
        address.BI_Direccion__c = calleComercial;
        address.BI_Localidad__c = ciudadComercial;
        address.BI_Provincia__c = provinciaComercial;
        address.BI_Codigo_postal__c = codigoPostalComercial;
        address.BI_Country__c = paisComercial;
        address.BI_Numero__c= numComercial;   
        
        insert address;
        
        BI_Punto_de_instalacion__c puntoInstalacionTipoComercial = new BI_Punto_de_instalacion__c();
        
        puntoInstalacionTipoComercial.BI_Cliente__c = acc;
        puntoInstalacionTipoComercial.BI_Tipo_de_sede__c='Comercial Principal';
        puntoInstalacionTipoComercial.BI_Sede__c = address.Id;
        //falta agregar la Sede Principal + nombre Cliente
        puntoInstalacionTipoComercial.Name = 'Sede Principal ' +accountDatos.Name;
        insert puntoInstalacionTipoComercial;
        
        //accountDatos.ShippingLatitude=coordinates.get(0);
        //accountDatos.ShippingLongitude=coordinates.get(1);
        
        // update accountDatos;
        resul=true;
        /*}*/
        
        return resul;
    }
    
    
    
    
    @AuraEnabled
    public static List<String> getCountryPickList() {
        List<String> countries = new List<String>();
        
        Schema.DescribeFieldResult fieldResult = Opportunity.BI_Country__c.getDescribe();
        
        List<Schema.PicklistEntry> pickListValues = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry value : pickListValues ) {
            countries.add(value.getValue());
        }
        return countries;
    }
}