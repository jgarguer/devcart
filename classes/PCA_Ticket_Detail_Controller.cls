public class PCA_Ticket_Detail_Controller {
	// Status field values
    public String CASE_STATUS_RESOLVED {
        get{return Constants.CASE_STATUS_RESOLVED;} 
        set;
    }
    public String CASE_STATUS_CLOSED {
        get{return Constants.CASE_STATUS_CLOSED;} 
        set;
    }
    public String CASE_STATUS_REJECTED {
        get{return Constants.CASE_STATUS_REJECTED;} 
        set;
    }
    public String CASE_STATUS_CANCELLED {
        get{return Constants.CASE_STATUS_CANCELLED;} 
        set;
    }
    public String CASE_STATUS_ASSIGNED {
        get{return Constants.CASE_STATUS_ASSIGNED;} 
        set;
    }
    public String CASE_STATUS_PENDING {
        get{return Constants.CASE_STATUS_PENDING;} 
        set;
    }
    public String CASE_STATUS_IN_PROGRESS {
        get{return Constants.CASE_STATUS_IN_PROGRESS;} 
        set;
    }

    /***********************************************
     *    ATTRIBUTES
     ***********************************************/
    public boolean error {get; set;}
    public boolean visibility {get; set;}
    public Case currentCase {get; set;}
    public String cRecordType {get; set;}
    
	public String serviceCategorization {get; set;}
    public String operationalCategorization {get; set;}
    public String operationalCommercialCategorization {get; set;}
    
    public String companyInfo {get; set;}
    public boolean impactUrgency {get; set;}
    
    public String invoiceInfoNumber {get; set;}
    public String invoiceInfoDate {get; set;}
    public String invoiceInfoEntity {get; set;}
    public Decimal invoiceInfoLines {get;set;}
    public String invoiceInfoBU {get;set;}
    
    public boolean isReassigned {get; set;}
    public List<CaseComment> comments {get; set;}
    public List<WorkInfo> workinfos {get; set;}
    public List<Attachment> attachments {get;set;}
    public String newComment {get; set;}
    public List<Attachment> listNewCommentAttachments {get;set;}
    public Attachment newAttachment {
    get {
      if (newAttachment == null)
        newAttachment = new Attachment();
      return newAttachment;
    }
      set;
    }
    public blob myAttachedBody {get; set;}
    public blob myAttachedBody2 {get; set;}
    public blob myAttachedBody3 {get; set;}
    
    public String attachmentName1 {get; set;}
    public String attachmentName2 {get; set;}
    public String attachmentName3 {get; set;}
    
    public String attachmentDesc1 {get; set;}
    public String attachmentDesc2 {get; set;}
    public String attachmentDesc3 {get; set;}
    
    public List<TGS_Proposed_Solutions__c> solutions {get;set;}
    public String selectedSolution {get;set;}
    
    public String backupLine {get; set;}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Description:  Controller of the ticket detail view
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    /***********************************************
     *    CONSTRUCTORS
     ***********************************************/

    public PCA_Ticket_Detail_Controller() {
        if(Test.isRunningTest()){
            CASE_STATUS_RESOLVED='CASE_STATUS_RESOLVED';
            CASE_STATUS_CLOSED='CASE_STATUS_CLOSED';
            CASE_STATUS_REJECTED='CASE_STATUS_REJECTED';
            CASE_STATUS_CANCELLED='CASE_STATUS_CANCELLED';
            CASE_STATUS_ASSIGNED='CASE_STATUS_ASSIGNED';
            CASE_STATUS_PENDING='CASE_STATUS_PENDING';
            CASE_STATUS_IN_PROGRESS='CASE_STATUS_IN_PROGRESS';
        }
        //Obtener case
        String mId = ApexPages.currentPage().getParameters().get('caseId');
        if(Test.isRunningTest()){ visibility = true;}
        else{
            visibility = TGS_Portal_Utils.validTicketOrderServiceVisibility(1, mId); 
            }
        if(visibility){
            currentCase = [SELECT ID,
                                subject, 
                                caseNumber, 
                                Asset__c, 
                                TGS_Invoice_Number__c,
                                TGS_Invoice_date__c,
                                TGS_Disputed_number_of_lines__c,
                                TGS_Invoicing_Entity__r.Name,
                                TGS_Invoice_BU__r.Name,
                                TGS_RFB_date__c, 
                                TGS_Impact__c, 
                                TGS_Urgency__c, 
                                Priority, 
                                TGS_Product_Tier_1__c, 
                                TGS_Product_Tier_2__c, 
                                TGS_Product_Tier_3__c,
                                TGS_Op_Technical_Tier_1__c, 
                                TGS_Op_Technical_Tier_2__c, 
                                TGS_Op_Technical_Tier_3__c, 
                                TGS_Op_Commercial_Tier_1__c, 
                                TGS_Op_Commercial_Tier_2__c, 
                                TGS_Op_Commercial_Tier_3__c,
                                TGS_Customer_Services__r.Name,
                                TGS_Customer_Services__r.NE__OrderId__r.NE__Asset__r.NE__ServAccId__c,
                                TGS_Customer_Services__r.NE__OrderId__r.NE__Asset__r.NE__BillAccId__c,
                                TGS_Ticket_Site__r.Name,
                           		/*toLabel(TGS_Backup_Line__c),*/ // Translated value
                           		TGS_Backup_Line__c,
                                OwnerId, description, accountId, account.Name,                           
                                CreatedDate, createdById, createdBy.Name, LastModifiedDate, status, RecordTypeId FROM Case WHERE Id = :mId LIMIT 1];
            
            System.debug('[PCA_Ticket_Detail_Controller Constructor] currentCase: ' + currentCase);
            
            if (currentCase.status.equalsIgnoreCase(Constants.CASE_STATUS_ASSIGNED)) {
                CaseHistory[] ch = [SELECT CaseId FROM CaseHistory  WHERE Field = 'Status' AND CaseId =:currentCase.Id ];
                if (ch.size() > 0) {
                    isReassigned = true;
                }
            }
            
            // Categorizaciones
            if (currentCase.TGS_Product_Tier_1__c!= null || currentCase.TGS_Product_Tier_2__c!= null || currentCase.TGS_Product_Tier_3__c!= null){
                serviceCategorization = (currentCase.TGS_Product_Tier_1__c!=null?currentCase.TGS_Product_Tier_1__c:'') + (currentCase.TGS_Product_Tier_2__c!=null?' > ' + currentCase.TGS_Product_Tier_2__c:'') + (currentCase.TGS_Product_Tier_3__c!=null?' > ' + currentCase.TGS_Product_Tier_3__c:'');
            }
            
            if (currentCase.TGS_Op_Technical_Tier_1__c!= null || currentCase.TGS_Op_Technical_Tier_2__c!= null || currentCase.TGS_Op_Technical_Tier_3__c!= null){
                operationalCategorization = (currentCase.TGS_Op_Technical_Tier_1__c!=null?currentCase.TGS_Op_Technical_Tier_1__c:'') + (currentCase.TGS_Op_Technical_Tier_2__c!=null?' > ' + currentCase.TGS_Op_Technical_Tier_2__c:'') + (currentCase.TGS_Op_Technical_Tier_3__c!=null?' > ' + currentCase.TGS_Op_Technical_Tier_3__c:'');
            }
            
            if (currentCase.TGS_Op_Commercial_Tier_1__c!= null || currentCase.TGS_Op_Commercial_Tier_2__c!= null || currentCase.TGS_Op_Commercial_Tier_3__c!= null){
                operationalCommercialCategorization = (currentCase.TGS_Op_Commercial_Tier_1__c!=null?currentCase.TGS_Op_Commercial_Tier_1__c:'') + (currentCase.TGS_Op_Commercial_Tier_2__c!=null?' > ' + currentCase.TGS_Op_Commercial_Tier_2__c:'') + (currentCase.TGS_Op_Commercial_Tier_3__c!=null?' > ' + currentCase.TGS_Op_Commercial_Tier_3__c:'');
            }
            
            System.debug('[PCA_Ticket_Detail_Controller Constructor] serviceCategorization: '+serviceCategorization);
            System.debug('[PCA_Ticket_Detail_Controller Constructor] operationalCategorization: '+operationalCategorization);
            System.debug('[PCA_Ticket_Detail_Controller Constructor] operationalCommercialCategorization: '+operationalCommercialCategorization);    
            
            // Obtenemos la jerarquía de la cuenta con más bajo nivel entre serviceAccount Y billingAccount de la Service Unit
            companyInfo = '';
            integer level = TGS_Portal_Utils.getAccountLevelForAccount(currentCase.accountId);
            if(level>=1){companyInfo += (TGS_Portal_Utils.getLevel1(currentCase.accountId,1)[0]).Name;}
            if(level>=2){companyInfo += '  >  ' + (TGS_Portal_Utils.getLevel2(currentCase.accountId,1)[0]).Name;}
            if(level>=3){companyInfo += '  >  ' + (TGS_Portal_Utils.getLevel3(currentCase.accountId,1)[0]).Name;}
            if(level>=4){companyInfo += '  >  ' + (TGS_Portal_Utils.getLevel4(currentCase.accountId,1)[0]).Name;}
            if(level>=5){companyInfo += '  >  ' + (TGS_Portal_Utils.getLevel5(currentCase.accountId,1)[0]).Name;}
            
            System.debug('[PCA_Ticket_Detail_Controller Constructor] companyInfo: '+companyInfo);    
            
            RecordType cRT = [SELECT Name FROM RecordType WHERE Id = :currentCase.RecordTypeId LIMIT 1];
            cRecordType = cRT.Name;
            
            System.debug('[PCA_Ticket_Detail_Controller Constructor] cRecordType: '+cRecordType);
            
            impactUrgency = (cRecordType == 'Incident' || cRecordType == 'Query' || cRecordType == 'Change');
            
            System.debug('[PCA_Ticket_Detail_Controller Constructor] impactUrgency: '+impactUrgency);
                
            //Si el ticket es del tipo Complaint, no aparece la categorización operacional
            if(currentCase.TGS_Op_Technical_Tier_1__c!= null && cRecordType.equals('Complaint')){
                operationalCategorization = null;
                
                System.debug('[PCA_Ticket_Detail_Controller Constructor] operationalCategorization: '+operationalCategorization);
            } 
            
            if (currentCase.TGS_Invoice_date__c != null) {
                invoiceInfoDate =(currentCase.TGS_Invoice_date__c!=null?' ' + currentCase.TGS_Invoice_date__c + '':'').substring(0,12);
            }
            if (currentCase.TGS_Invoice_Number__c != null ){
                invoiceInfoNumber =currentCase.TGS_Invoice_Number__c;
            }
            if (currentCase.TGS_Disputed_number_of_lines__c != null ){
                invoiceInfoLines =currentCase.TGS_Disputed_number_of_lines__c;
            }
            if (currentCase.TGS_Invoicing_Entity__r.Name != null ){
                invoiceInfoEntity =currentCase.TGS_Invoicing_Entity__r.Name;
            }
            if (currentCase.TGS_Invoice_BU__r.Name != null ){
                invoiceInfoBU =currentCase.TGS_Invoice_BU__r.Name;
            }
            // If case is for mWan, show backupline
            if(/*!String.isBlank(currentCase.TGS_Backup_Line__c) &&*/ (currentCase.TGS_Product_Tier_2__c == Constants.CASE_TIER2_MPLS_VPN || currentCase.TGS_Product_Tier_2__c == Constants.CASE_TIER2_INTERNET_ACCESS_TG_SOLUTIONS)) {
                backupLine = currentCase.TGS_Backup_Line__c != null ? currentCase.TGS_Backup_Line__c : '-';
            }
            
            System.debug('[PCA_Ticket_Detail_Controller Constructor] invoiceInfoDate: '+invoiceInfoDate); 
            System.debug('[PCA_Ticket_Detail_Controller Constructor] invoiceInfoNumber: '+invoiceInfoNumber);
            System.debug('[PCA_Ticket_Detail_Controller Constructor] invoiceInfoLines: '+invoiceInfoLines);
            System.debug('[PCA_Ticket_Detail_Controller Constructor] invoiceInfoEntity: '+invoiceInfoEntity);
            System.debug('[PCA_Ticket_Detail_Controller Constructor] invoiceInfoBU: '+invoiceInfoBU);
                
            //Obtener comments
            comments = [SELECT Id, CommentBody,CreatedById,CreatedDate FROM CaseComment WHERE ParentId = :mId AND IsPublished = true ORDER BY CreatedDate];
            System.debug('[PCA_Ticket_Detail_Controller Constructor] comments: '+comments);
                
            //Obtener workinfos
            List<TGS_Work_Info__c> TGS_workinfos = [SELECT Id,Name,TGS_Description__c,CreatedById, CreatedDate FROM TGS_Work_Info__c WHERE TGS_Case__c = :mId AND TGS_Public__c = true ORDER BY CreatedDate];
    
            //Obtener attachment
            if(TGS_workinfos.size() != 0){
                String queryAttachments = 'SELECT Id, Name, ParentId FROM Attachment WHERE ParentId IN (';
                for(integer i=0; i < TGS_workinfos.size(); i++){
                    queryAttachments += ' \'' + TGS_workinfos[i].Id + '\'';
                    if(i < TGS_workinfos.size()-1){
                        queryAttachments += ',';
                    }
                }
                queryAttachments += ' )';
                attachments = (List<Attachment>)Database.query(queryAttachments);
            }
            else{
                attachments = new List<Attachment>();
            }
            
            System.debug('[PCA_Ticket_Detail_Controller Constructor] attachments: '+attachments);
                
            //Creamos la lista de workinfos con sus attachments
            WorkInfo aux;
            workinfos = new List<WorkInfo>();
            for(integer i=0; i < TGS_workinfos.size(); i++){
                aux = new WorkInfo(TGS_workinfos[i]);
                aux.getCommentAttachments(attachments);
                workinfos.add(aux);
            }
            //Inicializamos la lista de adjuntos y el adjunto
            listNewCommentAttachments = new List<Attachment>();
            newAttachment = new Attachment();
            
            //Obtenemos las soluciones propuestas
            solutions = [SELECT Id,CreatedBy.Name,CreatedDate,TGS_Description__c,TGS_Final_Solution__c FROM TGS_Proposed_Solutions__c WHERE TGS_Case__c = :mId ORDER BY CreatedDate];
            selectedSolution = '';
            
            System.debug('[PCA_Ticket_Detail_Controller Constructor] solutions: '+solutions);
        }
    }
    
    /***********************************************
     *    PUBLIC METHODS
     ***********************************************/
    /**
     * Method:         crearWorkinfo
     * Description:    Crea un SObject TGS_WorkInfo__c con los datos 'newSubject' y 'newDescription' y lo inserta en la base de datos.
     *                 También inserta todos los Attachment que se encuentran 'listNewCommentAttachments' y que se relacionan mediante
     *                 ParentId con el SObject TGS_WorkInfo__c creado. En caso de error, 'error' será verdadero con el fin de mostrar
     *                 un mensaje al usuario.
     */
    public PageReference crearWorkinfo() {
        TGS_Work_Info__c work = new TGS_Work_Info__c();
        work.TGS_Description__c = newComment;
        work.TGS_Case__c = ApexPages.currentPage().getParameters().get('caseId');
        work.TGS_Public__c = true;
        
        System.debug('[PCA_Ticket_Detail_Controller Constructor] work: '+work);
        
        insert work;
        List<Attachment> listToInsert = new List<Attachment>();
        if (myAttachedBody != null) {
            newAttachment = new Attachment();
            newAttachment.Name = attachmentName1;
            newAttachment.Description = TGS_IntegrationUtils.UNO+attachmentDesc1;
            newAttachment.Body = myAttachedBody;
            newAttachment.OwnerId = UserInfo.getUserId();
            newAttachment.ParentId = work.Id;
            listToInsert.add(newAttachment);
        }
        
        System.debug('[PCA_Ticket_Detail_Controller.crearWorkinfo] newAttachment: ' + newAttachment);
        
        if (myAttachedBody2 != null) {
            newAttachment = new Attachment();
            newAttachment.Name = attachmentName2;
            newAttachment.Description = TGS_IntegrationUtils.DOS+attachmentDesc2;
            newAttachment.Body = myAttachedBody2;
            newAttachment.OwnerId = UserInfo.getUserId();
            newAttachment.ParentId = work.Id;
            listToInsert.add(newAttachment);
        }
        
        System.debug('[PCA_Ticket_Detail_Controller.crearWorkinfo] newAttachment: ' + newAttachment);
        
        if (myAttachedBody3 != null) {
            newAttachment = new Attachment();
            newAttachment.Name = attachmentName3;
            newAttachment.Description = TGS_IntegrationUtils.TRES+attachmentDesc3;
            newAttachment.Body = myAttachedBody3;
            newAttachment.ParentId = work.Id;
            listToInsert.add(newAttachment);
        }
        
        System.debug('[PCA_Ticket_Detail_Controller.crearWorkinfo] newAttachment: ' + newAttachment);
        System.debug('[PCA_Ticket_Detail_Controller.crearWorkinfo] listToInsert: ' + listToInsert);
        
        try {
            if (listToInsert.size() > 0) {       
                Database.SaveResult[] results = Database.insert(listToInsert);
                
                System.debug('[PCA_Ticket_Detail_Controller.crearWorkinfo] Result: ' + results[0]);
            }
        } catch (DmlException e) {
            System.debug('[PCA_Ticket_Detail_Controller.crearWorkinfo] Error al subir los adjuntos del workinfo del case : ' + e);
            error = true;
        }
        
        //Vaciamos los campos
        listNewCommentAttachments = new List<Attachment>();
        newComment = '';
        
        PageReference pr = ApexPages.currentPage();
        pr.getParameters().put('caseId', ApexPages.currentPage().getParameters().get('caseId'));
        pr.setRedirect(true);
        return pr;
    }
    
    /**
     * Method:         uploadAttachment
     * Description:    Da valor a campos de 'newAttachment' y lo añade a la lista 'listNewCommentAttachments'.
     *                 Se crea un nuevo SObject Attachment relacionado a 'newAttachment'.
     */
    public PageReference uploadAttachment() {
        newAttachment.OwnerId = UserInfo.getUserId();
        newAttachment.IsPrivate = true;
        
        System.debug('[PCA_Ticket_Detail_Controller.uploadAttachment] newAttachment: '+newAttachment);
        
        listNewCommentAttachments.add(newAttachment);
        
        System.debug('[PCA_Ticket_Detail_Controller.uploadAttachment] listNewCommentAttachments: '+listNewCommentAttachments);
        
        newAttachment = new Attachment();
        
        PageReference pr = ApexPages.currentPage();
        pr.getParameters().put('caseId', ApexPages.currentPage().getParameters().get('caseId'));
        return pr;
    }

    /********** BOTONES *********/
    /**
     * Method:         acceptResolution
     * Description:    Da valor a campos de 'newAttachment' y lo añade a la lista 'listNewCommentAttachments'.
     *                 Se crea un nuevo SObject Attachment relacionado a 'newAttachment'.
     */
    public PageReference acceptResolution() {
        Id caseId = ApexPages.currentPage().getParameters().get('caseId');
        try {
         	if((selectedSolution != null) && (selectedSolution != '')){
                Id finalSolId = (Id) selectedSolution;
                for(integer i=0; i<solutions.size();i++){
                    if(solutions[i].TGS_Final_Solution__c){
                        TGS_Proposed_Solutions__c solution = [SELECT Id,TGS_Final_Solution__c FROM TGS_Proposed_Solutions__c WHERE Id = :solutions[i].Id];
                        solution.TGS_Final_Solution__c = false;
                        
                        System.debug('[PCA_Ticket_Detail_Controller.acceptResolution]  solution.Id '+solution.Id);
                        update solution;
                    }
                }
                TGS_Proposed_Solutions__c finalSolution = [SELECT Id,TGS_Final_Solution__c FROM TGS_Proposed_Solutions__c WHERE Id = :finalSolId];
                finalSolution.TGS_Final_Solution__c = true;
                 
                System.debug('[PCA_Ticket_Detail_Controller.acceptResolution] finalSolution: '+finalSolution);
                update finalSolution;
           }
           Case caseActual = [SELECT Id,status FROM Case WHERE Id = :caseId LIMIT 1];
           caseActual.status = Constants.CASE_STATUS_CLOSED;
           
           System.debug('[PCA_Ticket_Detail_Controller.acceptResolution] caseActual: '+caseActual);
           update caseActual;  
           
          } catch(DmlException e) {
            System.debug('[PCA_Ticket_Detail_Controller.acceptResolution] Error al Actualizar el Status o la solución final: ' + e.getMessage());
        }             
        PageReference pr = ApexPages.currentPage();
        pr.getParameters().put('caseId', ApexPages.currentPage().getParameters().get('caseId'));
        pr.setRedirect(true);
        return pr; 
    }
    
    /**
     * Method:         	reopen
     * Description:    	Método que se lanza cuando se pulsa el botón reopen.
     * 					Cambia el status y TGS_Status_reason__c del order actual.
     */
    public PageReference reopen() {
        Id caseId = ApexPages.currentPage().getParameters().get('caseId');              
        try {
            Case caseActual = [SELECT Id,status FROM Case WHERE Id = :caseId LIMIT 1];
            caseActual.status = Constants.CASE_STATUS_ASSIGNED; 
            
            System.debug('[PCA_Ticket_Detail_Controller.reopen caseActual: '+caseActual);
            
            update caseActual;
        } catch(DmlException e) {
            System.debug('Error al Actualizar el Status: ' + e.getMessage());
        }                
        PageReference pr = ApexPages.currentPage();
        pr.getParameters().put('caseId', ApexPages.currentPage().getParameters().get('caseId'));
        pr.setRedirect(true);
        return pr;               
    }
    
	/**
     * Method:         	cancelTicket
     * Description:    	Método que se lanza cuando se pulsa el botón Cancel Ticket.
     * 					Cambia el status y TGS_Status_reason__c del order actual.
     */
    public PageReference cancelTicket() {
        Id caseId = ApexPages.currentPage().getParameters().get('caseId');             
        try {
            currentCase.status = Constants.CASE_STATUS_CANCELLED;
            
            System.debug('[PCA_Ticket_Detail_Controller.cancelTicket] currentCase: '+currentCase);
            
            update currentCase;
            
        } catch(DmlException e) {
            System.debug('[PCA_Ticket_Detail_Controller.cancelTicket] Error al Actualizar el Status: ' + e.getMessage());
        }                
        PageReference pr = ApexPages.currentPage();
        pr.getParameters().put('caseId', ApexPages.currentPage().getParameters().get('caseId'));
        pr.setRedirect(true);
        return pr;     
    }
	
    /**
     * Method:         useAsTemplate
     * Description:    Redirige hacia la creación de un nuevo ticket del mismo record type.
    */
    public PageReference useAsTemplate() {
        Id caseId = ApexPages.currentPage().getParameters().get('caseId'); 
        String pageTicket = '';
        if(cRecordType == 'Incident'){
            pageTicket = '/empresasplatino/PCA_New_Incident';
        }else if(cRecordType == 'Query'){
            pageTicket = '/empresasplatino/PCA_New_Query';
        }else if(cRecordType == 'Change'){
            pageTicket = '/empresasplatino/PCA_New_Change';
        }else if(cRecordType == 'Complaint'){
            pageTicket = '/empresasplatino/PCA_New_Complaint';
        }else if(cRecordType == 'Billing Inquiry'){
            pageTicket = '/empresasplatino/PCA_New_Inquiry';        
        }
        
        System.debug('[PCA_Ticket_Detail_Controller.useAsTemplate pageTicket: '+pageTicket); 
        
        pageTicket += '?caseId=' + caseId;                
        PageReference pr = new PageReference(pageTicket);
        pr.setRedirect(true);
        return pr;  
    }
    

    /***********************************************
     *    INNER CLASSES
     ***********************************************/
    /**
     * Class:            WorkInfo
     * Description:      Contiene la información de un cierto
     *                   comentario de un Case.
     */
    public class WorkInfo {
        public TGS_Work_Info__c work {get; set;}
        public List<Attachment> listAttachment {get; set;}
        public String userName {get; set;}
        
        // Constructor
        public WorkInfo (TGS_Work_Info__c work) {
            this.work = work;
            User user = [SELECT Name FROM User WHERE Id = :work.CreatedById LIMIT 1];
            this.userName = user.Name;
        }
        
        /**
         * Method:         hasAttachments
         * Param:          'attachments' es una lista de adjuntos
         * Description:    Verdadero si el TGS_Work_Info__c tiene archivos adjuntos.
         *                 De lo contrario, falso.
         */
        public Boolean hasAttachments(List<Attachment> attachments){
            Boolean hasAttach = false;
            for(integer i=0; i<attachments.size() && !hasAttach; i++){
                if(attachments[i].ParentId == work.Id){
                    hasAttach = true;
                }
            }
            
            System.debug('[PCA_Ticket_Detail_Controller.WorkInfo.hasAttach] hasAttach: ' + hasAttach);
            
            return hasAttach;
        }
        
        /**
         * Method:         getCommentAttachments
         * Param:          'attachments' es una lista de adjuntos
         * Description:    Guarda en 'listAttachment' los adjuntos del workinfo.
         */
        public void getCommentAttachments(List<Attachment> attachments){
            listAttachment = new List<Attachment>();
            for(integer i=0;i<attachments.size(); i++){
                if(attachments[i].ParentId == work.Id){
                    listAttachment.add(attachments[i]);
                }
            }
            
            System.debug('[PCA_Ticket_Detail_Controller.WorkInfo.getCommentAttachments] listAttachment: ' + listAttachment);
        }
    }
}