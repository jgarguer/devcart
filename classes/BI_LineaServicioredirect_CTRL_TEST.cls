@isTest
private class BI_LineaServicioredirect_CTRL_TEST {

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Raul Aguera
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_LineaServicioredirect_CTRL class
    
    History: 
    <Date> 					<Author> 				<Change Description>
    09/04/2015      		Raul Aguera    			Initial Version		
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    static{
        
        BI_TestUtils.throw_exception = false;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Raul Aguera
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_LineaServicioredirect_CTRL
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	09/04/2015      		Raul Aguera    			Initial Version	
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void TestPageLineaServicioRedirect() {

		//create BI_Linea_de_Servicio__c
		list<BI_Linea_de_Servicio__c> lstLineaServicio = BI_DataLoadAgendamientos.loadLineaServicio( 3 );
		
		ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller( lstLineaServicio.get(0) );
		BI_LineaServicioredirect_CTRL psController = new BI_LineaServicioredirect_CTRL(controller);
        
        BI_Linea_de_Servicio__c oLineaServicio = [ select Id, BI_Linea_de_recambio__c, BI_Linea_de_recambio__r.BI_Solicitud__c, BI_Linea_de_venta__c, BI_Linea_de_venta__r.BI_Solicitud__c, BI_Solicitud__c 
        											from BI_Linea_de_Servicio__c where Id = :lstLineaServicio.get(0).Id ];
        
        system.debug( '## oLineaServicio: ' + oLineaServicio );
        system.debug( '##url: ' + psController.url );
        
        string surl = '/apex/BI_LineaServicio?id=' + oLineaServicio.BI_Solicitud__c + '&idServ=' + lstLineaServicio.get(0).Id;
        system.assertEquals( surl, psController.url );
        
    }
    
    static testMethod void TestPageLineaServicioRedirect_null() {
    	
    	//create BI_Linea_de_Servicio__c
		list<BI_Linea_de_Servicio__c> lstLineaServicio = BI_DataLoadAgendamientos.loadLineaServicio( 3 );
		
		for( BI_Linea_de_Servicio__c inLineaServicio : lstLineaServicio ){
			
			inLineaServicio.BI_Solicitud__c = null;
		}
		
		update lstLineaServicio;
		
		ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller( lstLineaServicio.get(0) );
		BI_LineaServicioredirect_CTRL psController = new BI_LineaServicioredirect_CTRL(controller);
		
		BI_Linea_de_Servicio__c oLineaServicio = [ select Id, BI_Linea_de_recambio__c, BI_Linea_de_recambio__r.BI_Solicitud__c, BI_Linea_de_venta__c, BI_Linea_de_venta__r.BI_Solicitud__c, BI_Solicitud__c 
        											from BI_Linea_de_Servicio__c where Id = :lstLineaServicio.get(0).Id ];
        
        system.debug( '## oLineaServicio2: ' + oLineaServicio );
        system.debug( '##url: ' + psController.url );
        
        string surl = '/apex/BI_LineaServicio?id=' + oLineaServicio.BI_Linea_de_venta__r.BI_Solicitud__c + '&idServ=' + lstLineaServicio.get(0).Id;
		system.assertEquals( surl, psController.url );
    }
}