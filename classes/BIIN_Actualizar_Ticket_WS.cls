/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jose María Martín Castaño
        Company:       Deloitte
        Description:   Clase para modificar el estado del ticket a través de WS
        
        History:
               
        <Date>          <Author>                                    <Description>
        10/12/2015      Jose María Martín Castaño                   Initial version
        17/05/2016      Julio Laplaza Soria                         Adaptation to UNICA
        --------------------------------------------------------------------------------------------------------------------------------------------------------
*/

public class BIIN_Actualizar_Ticket_WS extends BIIN_UNICA_Base
{
    public Boolean actualizarTicket(String authorizationToken, Case caso, String WIDescription) 
    {
        // caso.Status viene con valoes 1, 2, 3...6..
        try
        {
            // Prepare URL
            String url = 'callout:BIIN_WS_UNICA/ticketing/v1/tickets/' + caso.BI_Id_del_caso_legado__c + '/status';
            //url = addParameter('limit', '1000') ;
            //url = addParameter('offset', '1000'); 
            
            // Add Basic Data
            BIIN_UNICA_Pojos.TicketStatusChangeType changeType = new BIIN_UNICA_Pojos.TicketStatusChangeType();
            changeType.statusChangeReason = BIIN_UNICA_Utils.translateFromSalesForceToID('Subestado', caso.BI_Status_Reason__c);
            changeType.ticketStatus = BIIN_UNICA_Utils.stringToEnumValueForTicketStatusType(BIIN_UNICA_Utils.translateFromRemedyIDToRemedyValue('Estado', Integer.valueOf(caso.Status)));
            changeType.ticketSubStatus = null;

            // Add additional Data
            List<BIIN_UNICA_Pojos.KeyValueType> additionalData = new List<BIIN_UNICA_Pojos.KeyValueType>();
            
            Account account = getAccount(caso.accountId);
            if (account != null)
            {
                additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('companyId', account.BI_Validador_Fiscal__c));
            }
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('workLogType', ''));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('workLogNotes', WIDescription));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('workLogDate', string.valueof(System.currentTimeMillis()/1000)));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('viewAccess', ''));
            changeType.additionalData = additionalData;

            // Prepare Request
            HttpRequest req = getRequest(url, 'PUT', changeType);
            
            // Call and Get Response
            BIIN_UNICA_Pojos.ResponseObject response = getResponse(req, BIIN_UNICA_Pojos.TicketStatusInfoType.class);
            BIIN_UNICA_Pojos.TicketStatusInfoType statusInfoType = (BIIN_UNICA_Pojos.TicketStatusInfoType) response.responseObject;

            return true;
        }
        catch(Exception e) 
        {
            system.debug ('ERROR: ' + e);
            return false;
        }
    }
}