/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Clase de prueba: FS_CHI_Risk_Management

History:
<Date>							<Author>						<Change Description>
29/05/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class FS_CHI_Risk_Management_Test {
    
    @testSetup 
    private static void init() {
        /* Enable Bypass */
        BI_bypass__c bypass = new BI_bypass__c();
        bypass.BI_migration__c = true;
        bypass.BI_skip_trigger__c = true;
        bypass.BI_stop_job__c = true;
        bypass.BI_skip_assert__c = true;
        bypass.BI_Skip_case_assignations__c = true;
        insert bypass;
        
        /* Account */
        Account account = new Account(Name = 'Test [Integración] - Chile', Description = 'null', BI_Activo__c = 'Sí',
                                     BI_Segment__c = 'Empresas', BI_Subsegment_Local__c = 'Top 1', FS_CHI_Fecha_de_ultima_ev__c = Date.today(),
                                     BI_Riesgo__c = null, FS_CHI_Credit_Score__c = 0, BI_Denominacion_comercial__c = 'Test [Integración] - Chile [Denominación comercial]',
                                     BI_Sector__c = 'Comercio', TGS_Es_MNC__c = false, BI_Ambito__c = 'Público', BI_Subsector__c = 'Droguerías',
                                     Website = 'www.cliente.com', TGS_Fecha_Activacion__c = Date.today(), TGS_Deactivation_Date__c = Date.today(),
                                     FS_CHI_CREDIT_CLASS__c = null, FS_CHI_Limite_de_credito__c = null, BI_Country__c = 'Chile',
                                     BI_Tipo_de_identificador_fiscal__c = 'RUC',BI_No_Identificador_fiscal__c = '20552003609',Phone = '6895623147',
                                     Fax = '123456789', BI_Subsegment_Regional__c = 'Test', FS_CHI_Tipo_Credito__c = 'Otras');
        
        insert account;
    }
    
    @isTest private static void Sync() {
        /* Test */
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba';
        config_integracion.FS_CHI_creditTypeMap__c = '{"Tarjeta Bancaria":"4","Tarjeta Comercial":"5","Cuenta Corriente":"3","Otras":"1"}';
        insert config_integracion;
        System.Test.startTest();
        
        try{
            Account account = [SELECT Id, Name, BI_Denominacion_comercial__c, BI_Country__c, BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c, FS_CHI_Tipo_Credito__c, BI_Subsegment_Regional__c FROM Account LIMIT 1][0];
            FS_CHI_Risk_Management.Sync(account.FS_CHI_Tipo_Credito__c, account.BI_Subsegment_Regional__c, account.BI_No_Identificador_fiscal__c);
            //System.assertEquals(JSON.serialize((BI_RestWrapper.CreditInfoType) JSON.deserialize('{"creditProfiles":[{"creditProfileDate":"15/12/1990","creditScore":"0"}],"creditLimitInfo":{"creditLimit":"1"},"additionalData":[{"key":"portabilityLimit","value":"2"},{"key":"creditClass","value":"A"},{"key":"consumeLimit","value":"3"}]}', BI_RestWrapper.CreditInfoType.class)).length(), JSON.serialize(FS_CHI_Risk_Management.Sync(account.FS_CHI_Tipo_Credito__c, account.BI_Subsegment_Regional__c, account.BI_No_Identificador_fiscal__c)).length());
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    @isTest private static void Sync2() {
        /* Test */
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba2';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba2';
        config_integracion.FS_CHI_creditTypeMap__c = '{"Tarjeta Bancaria":"4","Tarjeta Comercial":"5","Cuenta Corriente":"3","Otras":"1"}';
        insert config_integracion;
        System.Test.startTest();
        
        try{
            Account account = [SELECT Id, Name, BI_Denominacion_comercial__c, BI_Country__c, BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c, FS_CHI_Tipo_Credito__c, BI_Subsegment_Regional__c FROM Account LIMIT 1][0];
            account.FS_CHI_Tipo_Credito__c = 'Cuenta Corriente';
            update account;
            FS_CHI_Risk_Management.Sync(account.FS_CHI_Tipo_Credito__c, account.BI_Subsegment_Regional__c, account.BI_No_Identificador_fiscal__c);
            //System.assertEquals(JSON.serialize((BI_RestWrapper.CreditInfoType) JSON.deserialize('{"creditProfiles":[{"creditProfileDate":"15/12/1990","creditScore":"0"}],"creditLimitInfo":{"creditLimit":"1"},"additionalData":[{"key":"portabilityLimit","value":"2"},{"key":"creditClass","value":"A"},{"key":"consumeLimit","value":"3"}]}', BI_RestWrapper.CreditInfoType.class)).length(), JSON.serialize(FS_CHI_Risk_Management.Sync(account.FS_CHI_Tipo_Credito__c, account.BI_Subsegment_Regional__c, account.BI_No_Identificador_fiscal__c)).length());
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
}