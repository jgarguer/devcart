public with sharing class BI_AccountKpisBatch implements Database.Batchable<sObject>, Database.Stateful {

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Alberto Fernández
Company:       Accenture
Description:   Batchable class to update 'BI_Ultima_Oportunidad_Visita__c', 'BI_Ultima_Oportunidad_Oferta_Digital__c', 'BI_Ultima_Oportunidad_Oferta__c'
			   Account fields. (D-398)
			   Use the boolean 'total' to activate all the methods
			   
			   Example
	            BI_AccountKpisBatch batch = new BI_AccountKpisBatch();
		        batch.todos = true;
				Database.executeBatch(batch);

			   To limit the quantity of Accounts: 
			   	BI_AccountKpisBatch batch = new BI_AccountKpisBatch();
		        batch.query = batch.query + ' and Id = \'001w000001OmtV6AAJ\'';
				Database.executeBatch(batch);
				
Test Class:    BI_AccountKpisBatch_TEST

History:
 
<Date>                  <Author>                <Change Description>
28/02/2017             Alberto Fernández        Initial version
31/05/2017			   Gawron, Julián			Adding total as parameter
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static Map<Id,Account> toActualize = new Map<Id,Account>();
	static Database.SaveResult[] records;
	Integer successRecords = 0;
	Integer failedRecords = 0;
	List<String> lst_errors = new List<String>();

    public String query;
    public boolean  todos;
    public BI_AccountKpisBatch() {
      todos = false; 
      query = 'SELECT Id FROM Account where TGS_Es_MNC__c = false';
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }


	/*EXECUTE method*/
	public void execute(Database.BatchableContext BC, List<Account> accounts) {

		Set<Id> set_AccIds = new Set<Id>();
        
		for(Account acc: accounts) {
			set_AccIds.add(acc.Id);
		}

		System.debug('TAMANIO LISTA QUE RECIBO: ' + accounts.size());

		//IMPORTANTE, no modificar el orden de llamada a los métodos.
		if(todos){
		 updateOfertaVisita(set_AccIds);
		 updateOfertaDigital(set_AccIds);
		 updateOferta(set_AccIds);
		}
		updateOfertaGanada(set_AccIds); /*D-461*/

		// 23/01/2017 se comento la validacion de Bypass para este casos
        Map<Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(Userinfo.getUserId(), true, true, false, false);

        try{
			records = Database.update(toActualize.values(), false);

			//Comprobamos cuales de los registros no han podido actualizarse correctamente
			for (Database.SaveResult sr : records) {			
			    if (!sr.isSuccess()) {  
			    	System.debug('ENTROOO 1');  
			    	string errorDetalles = '';

			        for(Database.Error err : sr.getErrors()) {
			            System.debug('ERROR: Ha ocurrido un error al actualizar una cuenta: ' + err.getStatusCode() + ': ' + err.getMessage() + ' / ' + err.getStatusCode() + ': ' + err.getMessage());
			            errorDetalles += err.getMessage();
			        
				        String textoMSG = '- Nro ID:' + sr.getId() + ' Detalles: ' + errorDetalles;    

	                    lst_errors.add(textoMSG);                   
			        }
			        failedRecords += 1;
			    }
			    else {
			    	System.debug('ENTROOO 2');
			    	successRecords += 1;
			    }
			}
        }

        catch(Exception e){
        	System.debug('BI_AccountKpisBatch EXCEPTION: '+ e);
        }

        finally{
            if(mapa != null){
                BI_MigrationHelper.disableBypass(mapa);
            }
        }
        //fin cambio validacion bypasss

	}


	/*FINISH method*/
	public void finish(Database.BatchableContext BC) {

		System.debug('KPISupdate: Actualizacion de KPIS terminada');
		System.debug('REGISTROS ACTUALIZADOS CORRECTAMENTE: ' + successRecords);
		System.debug('REGISTROS CON FALLO EN ACTUALIZACION: ' + failedRecords);

		//Envio de email avisando de que la actualizacion ha terminado
		Messaging.SingleEmailMessage mailOwner = new Messaging.SingleEmailMessage();
    	String body;
    	System.debug('TAMANIO ERRORES: ' + lst_errors.size());
    	if(lst_errors.size() > 0)
    		body = '<h1>Actualización BI_AccountKpisBatch finalizada.<br>Listado de errores:</h1>' + String.join(lst_errors,'<br>');
    	else 
    		body = '<h1>Actualización BI_AccountKpisBatch finalizada sin errores.</h1>';
        mailOwner.setTargetObjectId(UserInfo.getUserId());
        mailOwner.setHtmlBody(body);
        mailOwner.setSubject('Batch de actualizacion - BI_AccountKpisBatch');

        mailOwner.setSenderDisplayName('BI_AccountKpisBatch');
        mailOwner.setSaveAsActivity(false);
        
        if(!Test.isRunningTest()){   
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mailOwner});
        }
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Fernández
    Company:       Accenture
    Description:   Method that updates the 'Ultima Oportunidad - Visita' field on Account
    
    History:
    
    <Date>            <Author>             <Description>
    28/02/2017        Alberto Fernández		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateOfertaVisita(Set<Id> set_AccIds){

    	Map<Id,Account> toActualizeVisita = new Map<Id,Account>();
		
		AggregateResult[] resultados = [SELECT
				  WhatId, 
				  MAX(BI_FVI_FechaHoraFinVisita__c) fechaFinVisita 
				  FROM Event 
				  WHERE BI_FVI_FechaHoraFinVisita__c != null
				  AND BI_FVI_Estado__c Like 'Cualificad%'
				  AND WhatId IN :set_AccIds
				  AND Subject LIKE '%Visita a Cliente%'
				  GROUP BY WhatId];

	  	for(AggregateResult resultado : resultados) {

	  		Account acc;
	  		
	  		if(!toActualize.containsKey((Id)resultado.get('WhatId'))) {
	  			acc = new Account(
	  				Id = (Id)resultado.get('WhatId'),
		  			BI_Ultima_Oportunidad_Visita__c = ((Datetime)resultado.get('fechaFinVisita')).date()
		  		);

		  		toActualize.put(acc.Id, acc);
	  		}
	  		else {
	  			toActualize.get((Id)resultado.get('WhatId')).BI_Ultima_Oportunidad_Visita__c = ((Datetime)resultado.get('fechaFinVisita')).date();
	  		}
	  		if(!toActualizeVisita.containsKey((Id)resultado.get('WhatId'))) {
	  			acc = new Account(
					Id = (Id)resultado.get('WhatId')
				);
	  			toActualizeVisita.put(acc.Id, acc);
	  		}

	  	}

	  	for(Id accId :set_AccIds) {
	  		if(!toActualizeVisita.containsKey(accId)) {

	  			Account acc = new Account(
	  				Id = accId,
	  				BI_Ultima_Oportunidad_Visita__c = null
	  			);
	  			
	  			if(!toActualize.containsKey(acc.Id)) {	
		  			toActualize.put(acc.Id, acc);
	  			}
	  			else {
	  				toActualize.get(acc.Id).BI_Ultima_Oportunidad_Visita__c = null;
	  			}
	  			toActualizeVisita.put(acc.Id, acc);

	  		}
	  	}

	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Fernández
    Company:       Accenture
    Description:   Method that updates the 'Ultima Oportunidad - Digital' field on Account
    
    History:
    
    <Date>            <Author>             <Description>
    28/02/2017        Alberto Fernández		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void updateOfertaDigital(Set<Id> set_AccIds){

		Map<Id,Account> toActualizeDigital = new Map<Id,Account>();

		Set<String> set_stageOppty = new Set<String>{
								Label.BI_DefinicionSolucion,	//F5 - Solution Definition
								Label.BI_O4_F5_Prequalified,	//F5 - Prequalified
								Label.BI_O4_F4_DesignSolution, 	//F4 - Design Solution
								Label.BI_DesarrolloOferta,		//F4 - Offer Development
								Label.BI_O4_F3_Negotiation,		// 
								Label.BI_F3OfertaPresentada, 	//F3 - Offer Presented
								Label.BI_F2Negociacion, 		//F2 - Negotiation
								Label.BI_O4_F2_ContractNegotiation,//F2 - Contract Negotiation
								Label.BI_O4_F1_Partially_Won, 	//F1 - Partially Won
								Label.BI_F1Ganada,				//F1 - Closed Won
								Label.BI_F1ClosedLost			//
							};

		//juntamos todas las cuentas que cumplen con la condicion y la fecha de su oportunidad más reciente.
		AggregateResult[] resultados =  [Select
    			  NE__OrderId__r.Ne__OptyId__r.AccountId,               
    			  MAX(NE__OrderId__r.Ne__OptyId__r.CreatedDate) FechaOpty
    			  FROM NE__OrderItem__c
    			  WHERE NE__ProdId__r.BI_Rama_Digital__c != null
    			  AND NE__OrderId__r.NE__OrderStatus__c Like 'Activ%'
    			  AND NE__OrderId__r.Ne__OptyId__r.StageName IN :set_stageOppty
    			  AND NE__OrderId__r.Ne__OptyId__r.AccountId IN :set_AccIds
				  group by NE__OrderId__r.Ne__OptyId__r.AccountId];
		//Recorremos los resultados y agregamos la cuentas que si cumplen el criterio
		for(AggregateResult r: resultados){

			Account a;

			if(!toActualize.containsKey((Id)r.get('AccountId'))) {
					a = new Account(
					Id = (Id)r.get('AccountId'),
					BI_Ultima_Oportunidad_Oferta_Digital__c = ((Datetime)r.get('FechaOpty')).date()
				);
				toActualize.put(a.Id, a);
			}

			else {
				toActualize.get((Id)r.get('AccountId')).BI_Ultima_Oportunidad_Oferta_Digital__c = ((Datetime)r.get('FechaOpty')).date();
			}
			if(!toActualizeDigital.containsKey((Id)r.get('AccountId'))) {
				a = new Account(
					Id = (Id)r.get('AccountId')
				);
	  			toActualizeDigital.put(a.Id, a);
	  		}
		}
		//Si el id no está en el mapa a actualizar, las cuentas ya no cumplen la condicion, le ponemos el campo a null
		for(Id accId : set_AccIds){
			if(!toActualizeDigital.containsKey(accId)){

				Account acc = new Account(
					Id = accId,
					BI_Ultima_Oportunidad_Oferta_Digital__c = null
				);

				if(!toActualize.containsKey(acc.Id)) {
		  			toActualize.put(acc.Id, acc);
	  			}
	  			else {
	  				toActualize.get(acc.Id).BI_Ultima_Oportunidad_Oferta_Digital__c = null;
	  			}
	  			toActualizeDigital.put(acc.Id, acc);

			}
		}

	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Fernández
    Company:       Accenture
    Description:   Method that updates the 'Ultima Oportunidad - Oferta' field on Account
    
    History:
    
    <Date>            <Author>             <Description>
    28/02/2017        Alberto Fernández		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void updateOferta(Set<Id> set_AccIds) {

		Map<Id,Account> toActualizeOferta = new Map<Id,Account>();

	  	Set<String> set_stageOppty = new Set<String>{
							Label.BI_DefinicionSolucion,	//F5 - Solution Definition
							Label.BI_O4_F5_Prequalified,	//F5 - Prequalified
							Label.BI_O4_F4_DesignSolution, 	//F4 - Design Solution
							Label.BI_DesarrolloOferta,		//F4 - Offer Development
							Label.BI_O4_F3_Negotiation,		//F3 - Negociation
							Label.BI_F3OfertaPresentada, 	//F3 - Offer Presented
							Label.BI_F2Negociacion, 		//F2 - Negotiation
							Label.BI_O4_F2_ContractNegotiation,//F2 - Contract Negotiation
							Label.BI_O4_F1_Partially_Won, 	//F1 - Partially Won
							Label.BI_F1Ganada,				//F1 - Closed Won
							Label.BI_F1ClosedLost			//F1 - Closed Lost
						};
		
		AggregateResult[] resultados = [SELECT
				  AccountId, 
				  MAX(CreatedDate) fechaCreacion 
				  FROM Opportunity 
				  WHERE CreatedDate != null 
				  AND AccountId IN :set_AccIds
                  AND StageName IN :set_stageOppty
				  GROUP BY AccountId];

	  	for(AggregateResult resultado : resultados) {

	  		Account acc;

	  		if(!toActualize.containsKey((Id)resultado.get('AccountId'))) {
	  				acc = new Account(
		  			Id = (Id)resultado.get('AccountId'),
		  			BI_Ultima_Oportunidad_Oferta__c = ((Datetime)resultado.get('fechaCreacion')).date()
		  		);
		  		toActualize.put(acc.Id, acc);
	  		}

	  		else {
	  			toActualize.get((Id)resultado.get('AccountId')).BI_Ultima_Oportunidad_Oferta__c = ((Datetime)resultado.get('fechaCreacion')).date();
	  		}
	  		if(!toActualizeOferta.containsKey((Id)resultado.get('AccountId'))) {
	  			acc = new Account(
					Id = (Id)resultado.get('AccountId')
				);
	  			toActualizeOferta.put(acc.Id, acc);
	  		}
	  	}

	  	for(Id accId :set_AccIds) {
	  		if(!toActualizeOferta.containsKey(accId)) {
	  			Account acc = new Account(
	  				Id = accId,
	  				BI_Ultima_Oportunidad_Oferta__c = null
	  			);
	  			toActualize.put(acc.Id, acc);
	  			toActualizeOferta.put(acc.Id, acc);
	  		}
	  	}

	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Fernández
    Company:       Accenture
    Description:   Method that updates the 'Ultima Oportunidad - Oferta Ganada' field on Account
    
    History:
    
    <Date>            <Author>             <Description>
    18/04/2017        Alberto Fernández		Initial version. D-461
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void updateOfertaGanada(Set<Id> set_AccIds) {

		Map<Id,Account> toActualizeOferta = new Map<Id,Account>();
		
		AggregateResult[] resultados = [SELECT
				  AccountId, 
				  MAX(CreatedDate) fechaCreacion 
				  FROM Opportunity 
				  WHERE CreatedDate != null 
				  AND AccountId IN :set_AccIds
                  AND StageName = :Label.BI_F1Ganada
				  GROUP BY AccountId];

	  	for(AggregateResult resultado : resultados) {

	  		Account acc;

	  		if(!toActualize.containsKey((Id)resultado.get('AccountId'))) {
	  				acc = new Account(
		  			Id = (Id)resultado.get('AccountId'),
		  			BI_Ultima_Oportunidad_Oferta_Ganada__c = ((Datetime)resultado.get('fechaCreacion')).date()
		  		);
		  		toActualize.put(acc.Id, acc);
	  		}

	  		else {
	  			toActualize.get((Id)resultado.get('AccountId')).BI_Ultima_Oportunidad_Oferta_Ganada__c = ((Datetime)resultado.get('fechaCreacion')).date();
	  		}
	  		if(!toActualizeOferta.containsKey((Id)resultado.get('AccountId'))) {
	  			acc = new Account(
					Id = (Id)resultado.get('AccountId')
				);
	  			toActualizeOferta.put(acc.Id, acc);
	  		}
	  	}

	  	for(Id accId :set_AccIds) {
	  		if(!toActualizeOferta.containsKey(accId)) {
	  			Account acc = new Account(
	  				Id = accId,
	  				BI_Ultima_Oportunidad_Oferta_Ganada__c = null
	  			);
	  			toActualize.put(acc.Id, acc);
	  			toActualizeOferta.put(acc.Id, acc);
	  		}
	  	}

	}

}