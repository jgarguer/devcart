/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							
Company:						Everis España
Description:					

History:
<Date>							<Author>						<Change Description>

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class FS_CORE_Fullstack_Scheduler_Test {
    
  public static String CRON_EXP = '0 0 0 15 3 ? 2022';


   static testmethod void test() {
       
      Test.startTest();
       
      String jobId = System.schedule('Testing FS_CORE_Fullstack_Scheduler', CRON_EXP, new FS_CORE_Fullstack_Scheduler());

      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

      System.assertEquals(CRON_EXP, ct.CronExpression);

      System.assertEquals(0, ct.TimesTriggered);

      System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
       
      Test.stopTest();

   }
       
}