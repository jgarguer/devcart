/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Antonio Pardo
     Company:       Accenture
     Description:   Test for the controller TGS_Carousel_Ctrl

     History:
     <Date>            <Author>             <Description>
 	01/03/2018        	Antonio Pardo        First version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class TGS_Carousel_Ctrl_Test {
	
    @isTest
    static void getCustomMetadata(){
        Map<String, Object> map_images = TGS_Carousel_Ctrl.getImages();
        System.assert(!map_images.isEmpty());
    }
}