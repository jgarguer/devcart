@isTest public class BI2_CancelarViejosPendientes_JOB_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Test class for BI2_CancelarViejosPendientes_JOB

	 History:

	 <Date>				<Author>				<Description>
	 08/10/2015			Jose Miguel Fierro		Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest public static void test_execute() {
        BI_TestUtils.throw_exception = false;
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);
        
        List<RecordType> lst_rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI2_caso_comercial','BI2_Caso_Padre')];
        List<RecordType> lst_rt_dos = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('BI_Caso_Comercial_Abierto') limit 1];
        Id id_ccomer, id_cpadre;
        for(RecordType rt : lst_rt) {
            if(rt.DeveloperName =='BI2_caso_comercial')
                id_ccomer = rt.Id;
            if(rt.DeveloperName == 'BI2_Caso_Padre')
                id_cpadre = rt.Id;
        }

        List<Contact> lst_contacts = new List<Contact>();
        lst_contacts.add(new Contact(LastName='TestContact', email='Test@testest.test'));
        insert lst_contacts;
        
        List<Case> lst_cases = new List<Case>();
        for(Integer iter = 0; iter < 1; iter++) {
        	lst_cases.add(new Case(Status='Assigned',
                                   BI_Line_of_Business__c = 'Datos',
                                   BI_Product_Service__c = 'LAN gestionada',
                                   TGS_Disputed_number_of_lines__c = 1,
                                   BI2_PER_Territorio_caso__c = 'Lima',
                                   BI2_per_tipologia__c = 'Alta / Nuevo servicio' ,
                                   BI2_per_subtipologia__c = 'Alta nueva' , 
                                   RecordTypeId=id_ccomer,
                                   BI_Fecha_ltimo_cambio_de_estado__c=Date.today().addDays(-6),
                                   ContactId = lst_contacts[0].Id));
        }
        lst_cases.add(new Case(Status='Assigned', RecordTypeId= lst_rt_dos[0].Id, BI_Fecha_ltimo_cambio_de_estado__c=Date.today().addDays(-6)));
        insert lst_cases;
        
        Test.startTest();
        lst_cases = [SELECT Id, Status, TGS_Status_Reason__c FROM Case WHERE Status='Assigned'];
        for(Case cas : lst_cases) {
            cas.Status = 'Pending';
        	cas.TGS_Status_Reason__c = 'CL - Customer Posponed';
        }
        update lst_cases;
        
        delete [select Id from BI_Log__c];
        
        String strCron = '20 30 8 10 2 ?';
		System.schedule('BI2_CancelarViejosPendientes_JOB ' + strCron, '20 30 8 10 2 ?', new BI2_CancelarViejosPendientes_JOB());
        Test.stopTest();
        System.debug('ERRORS: ');
        for(BI_Log__c log: [SELECT Id, BI_Tipo_Error__c, BI_Asunto__c, BI_StackTrace__c, BI_Descripcion__c FROM BI_Log__c]) {
            System.debug(logginglevel.ERROR,'LOG: ' + log.BI_Tipo_Error__c + ' en ' + log.BI_Asunto__c + '\nDescr: ' + log.BI_Descripcion__c + '\n' + log.BI_StackTrace__c);
        }
        
        //system.assertEquals(true, [select Id from BI_Log__c limit 1].isEmpty());
        
    }

    @isTest public static void test_exceptions() {
        BI_TestUtils.throw_exception = true;
        new BI2_CancelarViejosPendientes_JOB().execute(null);
        BI2_CancelarViejosPendientes_JOB.generateCronStr(null);
        
        system.assertEquals(false, [select Id from BI_Log__c limit 1].isEmpty());
    }
}