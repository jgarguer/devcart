@isTest
private class PCA_New_Inquiry_Controller_TEST {

    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test class to manage the coverage code for PCA_New_Inquiry_Controller class 

    <Date>                  <Author>                <Inquiry Description>
    02/03/2015              Marta Laliena           Initial Version
    07/02/2017              Pedro Párraga           Increase coverage
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void Test_Method_One() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            ApexPages.currentPage().getParameters().put('sId', testProduct.Id);

            Id Inquiry = [SELECT Id FROM RecordType WHERE Name = 'Billing Inquiry' LIMIT 1].Id;
            Case testCase = new Case(RecordTypeId = Inquiry , Subject = 'Test controller', ContactId = userTest.ContactId,
                                    TGS_Invoice_Date__c = date.today());
            insert testCase;


            Test.startTest();

            ApexPages.currentPage().getParameters().put('caseId', testCase.Id);

            PCA_New_Inquiry_Controller controller = new PCA_New_Inquiry_Controller();
            controller.newAttachment.Name = 'Unit Test Attachment';
            controller.myAttachedBody = Blob.valueOf('Unit Test Attachment Body');
            controller.attachmentName2 = 'Unit Test Attachment';
            controller.attachmentDesc2 = 'Unit Test Description Attachment';
            controller.myAttachedBody2 = Blob.valueOf('Unit Test Attachment Body');
            controller.attachmentName3 = 'Unit Test Attachment';
            controller.attachmentDesc3 = 'Unit Test Description Attachment';
            controller.myAttachedBody3 = Blob.valueOf('Unit Test Attachment Body');

            List<SelectOption> lst_selopp = new List<SelectOption>();
            lst_selopp.add(new SelectOption('Metashield'   , 'Metashield ' ));

            controller.listlevel4 = lst_selopp;
            controller.listlevel5 = lst_selopp;
            controller.siteList = lst_selopp;
            controller.namelevel1 = 'name level 1';
            controller.namelevel2 = 'name level 2';
            controller.namelevel3 = 'name level 3';
            controller.namelevel4 = 'name level 4';
            controller.namelevel5 = 'name level 5';
            controller.nameSite = 'nameSite';
            controller.lookupSU = 'lookupSU';

            controller.buList = lst_selopp;
            controller.businessUnit = 'businessUnit';
            controller.nombreUsuario = UserInfo.getName();
            controller.identificadorUsuario = UserInfo.getUsername();
            controller.telefonoUsuario = '637263726';
            controller.emailUsuario = UserInfo.getUserEmail();

            controller.resumen = 'resumen';
            controller.notas = 'notas';
            controller.primerNivel = 'primerNivel';
            controller.segundoNivel = 'segundoNivel';
            controller.tercerNivel = 'tercerNivel';
            controller.usuarioFinal = userTest;
            controller.ubicacionUsuarioFinal = 'ubicacionUsuarioFinal';
            controller.emailUsuarioFinal = UserInfo.getUserEmail();


            //controller.convertDate();
            controller.crearCase();


            controller.year='4';
            controller.month='0';
            controller.myAttachedBody = Blob.valueOf('Unit Test Attachment Body');
            controller.uploadAttachment();
            controller.cancelar();

            //controller.convertDate();
            controller.crearCase();
            controller.initializeProductTiers();
            controller.loadLookupServices();
            controller.uploadAttachment();
            controller.cancelar();

            Test.stopTest();

            System.assertEquals(2, controller.listCaseAttachments.size());
            System.assertEquals('Unit Test Attachment', controller.listCaseAttachments[0].Name);
        }
    }
    
}