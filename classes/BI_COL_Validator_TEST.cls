@isTest
private class BI_COL_Validator_TEST {
	
	@isTest static void test_method_one() {
		// Implement test code
	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	// Valida el formateo de valores
	public static testMethod void ValidarFormatOtros()
	{
    	String datoCampo = '', strTipoCampo = '', separadorDecimal = '.', strRespuesta = ''; 
    	string strFormato = ''; 
    	double dbDecimales = 2;  
    	boolean ConvertirMayus = true;
    	// Prueba datos lógicos
    	strTipoCampo = 'Lógico';
    	datoCampo = 'true';
    	strFormato = '1 : 0';
    	strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus);
    	strFormato = 'TRUE : FALSE';
    	strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus);
    	strFormato = 'S : N';
    	strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus);
    	strFormato = 'Y : N';
    	strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus);    	
    	datoCampo = 'false';
    	strFormato = '1 : 0';
    	strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus);
    	strFormato = 'TRUE : FALSE';
    	strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus);
    	strFormato = 'S : N';
    	strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus);
    	strFormato = 'Y : N';
    	strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus);
    	// Prueba datos Fecha
    	strTipoCampo = 'Fecha';
    	datoCampo = String.valueOf(system.now());
    	strFormato = 'yyyy/MM/dd';
    	strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus);
    	strFormato = null;
    	strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus);
    	Date mifechs = BI_COL_Validator_cls.fn_formatFechaSFDC('2009/06/08', '/', 'yyyy/MM/dd');
    	Datetime mifech3 = BI_COL_Validator_cls.fn_formatFechaHoraSFDC('01/12/2009 19:05:23', '/', 'dd/MM/yyyy hh:mm:ss');
    	// Prueba dato Numérico
		strTipoCampo = 'Numérico';
    	datoCampo = '1234.56';
		strFormato = '#,###.##';
		strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus);
    	// Prueba datos Texto
    	strTipoCampo = 'Texto';
    	datoCampo = 'hola mundo';
    	ConvertirMayus = true;
    	strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus);
		strRespuesta = BI_COL_Validator_cls.fn_FormatearValor(null, strTipoCampo, strFormato, dbDecimales, separadorDecimal, ConvertirMayus); // Produce la excepción
    }
    
	// Pruebas para la validación de funciones generales
    public static testMethod void ValidarFormatNumber(){
        // Pruebas de valores numéricos
        Decimal dcNumber = 1234.56;
		string strFormato = '#.###,##';
		double dbPosicionesDecimales = 2.0;
		string strSeparadorDecimal = ',', nueva_cadena2 = '';
		nueva_cadena2 = BI_COL_Validator_cls.fn_FormatNumber(dcNumber, strFormato, dbPosicionesDecimales, strSeparadorDecimal);
		strFormato = '#,###.##';
		dbPosicionesDecimales = 4.0;
		strSeparadorDecimal = '.';
		nueva_cadena2 = BI_COL_Validator_cls.fn_FormatNumber(dcNumber, strFormato, dbPosicionesDecimales, strSeparadorDecimal);
		strFormato = '###.##';
		dbPosicionesDecimales = 2.0;
		strSeparadorDecimal = '.';
		nueva_cadena2 = BI_COL_Validator_cls.fn_FormatNumber(dcNumber, strFormato, dbPosicionesDecimales, strSeparadorDecimal);
		strFormato = '###';
		dbPosicionesDecimales = 0.0;
		strSeparadorDecimal = '.';
		nueva_cadena2 = BI_COL_Validator_cls.fn_FormatNumber(dcNumber, strFormato, dbPosicionesDecimales, strSeparadorDecimal);
		strFormato = '###.##';
		dbPosicionesDecimales = 2.0;
		strSeparadorDecimal = '.';
		dcNumber = 1234;
		nueva_cadena2 = BI_COL_Validator_cls.fn_FormatNumber(dcNumber, strFormato, dbPosicionesDecimales, strSeparadorDecimal);
		nueva_cadena2 = BI_COL_Validator_cls.fn_FormatNumber(null, strFormato, dbPosicionesDecimales, strSeparadorDecimal); // Provoca la excepción
    }
	
	public static testMethod void ValidarConversiones(){
		string resultado = '';
		list<id> ListaIds_list = new list<id>();
		set<id> SetIds_list = new set<id>();
		set<String> SetString_list = new set<String>();
		resultado = BI_COL_Validator_cls.fn_QuitarTildes('ÁÉÍÓÚáéíóú');
		resultado = BI_COL_Validator_cls.fn_ConvertirNs('Ññ');
		Boolean NOEnviarADavox = false;
		for(Account Clientes : [select id from Account limit 2]){
			ListaIds_list.add(Clientes.id);
			SetIds_list.add(Clientes.id);
			//NOEnviarADavox = BI_COL_Validator_cls.fn_DS_NoEnviarDAVOXPLUS(Clientes.id);
		}
		ListaIds_list.add('a01000000000000000');
		SetIds_list.add('a01000000000000000');
		SetString_list.add('Cadena1');
		SetString_list.add('Cadena2');
		ListaIds_list.add('a01000000000000001');
		SetIds_list.add('a01000000000000001');
		resultado = BI_COL_Validator_cls.fn_getStringIdsFromList(ListaIds_list);
		resultado = BI_COL_Validator_cls.fn_getStringIdsFromSet(SetIds_list);
		resultado = BI_COL_Validator_cls.fn_getStringFromSet(SetString_list);
		
		resultado = BI_COL_Validator_cls.armadoXML('nroIdentificacion', 'sDeveloper', 'packCode', 'Tipo');
		
		Date miFecha = BI_COL_Validator_cls.fn_formatFechaSFDC('2009/01/18', '/', 'yyyy/MM/dd');
		Datetime miFechaHora = BI_COL_Validator_cls.fn_formatFechaHoraSFDC('2009/01/18 19:05:39', '/', 'yyyy/MM/dd hh:mm:ss');
		BI_COL_Validator_cls.setCasoGeneradoSFDC();
		boolean res = BI_COL_Validator_cls.getCasoGeneradoSFDC();
		miFecha = BI_COL_Validator_cls.fn_formatFechaSFDC(null, '/', 'yyyy/MM/dd');							// Produce la excepción
		miFechaHora = BI_COL_Validator_cls.fn_formatFechaHoraSFDC(null, '/', 'yyyy/MM/dd hh:mm:ss');	// Produce la excepción	
	}
	
	public static testMethod void ValidarFechasGeneradas(){
		// HOY, AYER, MAÑANA, ESTE_AÑO, ESTE_MES, AHORA
		string resultado = '';
		resultado = BI_COL_Validator_cls.fn_FechasPredeterminadas('HOY', 'yyyy/MM/dd');
		resultado = BI_COL_Validator_cls.fn_FechasPredeterminadas('AHORA', 'yyyy/MM/dd hh:mm:ss');
		resultado = BI_COL_Validator_cls.fn_FechasPredeterminadas('AYER', 'yyyy/MM/dd');
		resultado = BI_COL_Validator_cls.fn_FechasPredeterminadas('MAÑANA', 'yyyy/MM/dd');
		resultado = BI_COL_Validator_cls.fn_FechasPredeterminadas('ESTE_AÑO', 'yyyy');
		resultado = BI_COL_Validator_cls.fn_FechasPredeterminadas('ESTE_MES', 'MM');
	}
	
	public static testMethod void convertirLSTtoStrings()
	{ 
		string resultado = '';
		List<String> lstValidador =new List<String>{'a','b','c'};
		resultado = BI_COL_Validator_cls.fn_getStringIdsFromStringList(lstValidador);
		BI_COL_Validator_cls.setCasoActualizado();
		BI_COL_Validator_cls.setCasoNOActualizado();
		BI_COL_Validator_cls.getCasoActualizado();
		BI_Log__c objlog= new BI_Log__c();
		resultado=BI_COL_Validator_cls.fn_ObtenerValor('BI_COL_Oportunidad__r.name', objlog);
		resultado=BI_COL_Validator_cls.fn_ObtenerValor('BI_COL_Oportunidad__c', objlog);
	}
	
}