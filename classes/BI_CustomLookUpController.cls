public class BI_CustomLookUpController {
    @AuraEnabled
    public static List<sObject> fetchLookUpValues(String searchKeyWord, String objectName) {
        System.debug('Object Name --> ' + objectName);
        String searchKey = searchKeyWord + '%';
        String query =	'SELECT Id, Name ' + 
            			'FROM ' + objectName + ' ' +
            			'WHERE Name LIKE: searchKey ' +
            			'ORDER BY createdDate DESC ' +
            			'LIMIT 5';
        List<sObject> listRecords = Database.query(query);
        return listRecords;
    }
}