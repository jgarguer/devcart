public class BI_EventMethods {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Methods executed by the Event trigger.
    History:
    
    <Date>            <Author>          <Description>
    14/11/2014        Pablo Oliva       Initial version
    12/01/2017		  Alvaro García		Add static variables
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	//public static Id calendarId = [SELECT BI_O4_Calendar_Id__c  FROM BI_O4_Public_calendar_id__c WHERE name = 'Agenda DRB Live' LIMIT 1].BI_O4_Calendar_Id__c;
	public static Id calendarId = Label.BI_O4_Public_calendar_id;

	static final map<String, Id> MAP_RT_NAME = new map<String, Id>();
    
    static{
        for(RecordType rt : [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Event']){
            MAP_RT_NAME.put(rt.DeveloperName,rt.Id);
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that validates the fields WhatId and WhoId (only Ecuador)
    
    IN:            ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    14/11/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void validateContact(List<Event> news){
		
		User user = [select Id, Pais__c from User where Id = :UserInfo.getUserId() limit 1];
		
		if(user.Pais__c == Label.BI_Ecuador){
			
			for(Event event:news){
				if(event.WhatId != null && String.valueOf(event.WhatId).startsWith('001') && 
				   (event.WhoId == null || (event.WhoId != null && !String.valueOf(event.WhoId).startsWith('003'))))
				{
					event.addError(Label.BI_ContactoEvento);
				}
			}
			
		}
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   
    
    IN:            ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    29/01/2015        Pablo Oliva       Initial version
    12/01/2017		  Alvaro García		get the recordtype of a static map of recordType
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void preventEventForActionPlan(List<Event> news){
		
		try{
			
			if (BI_TestUtils.isRunningTest()) {
				throw new BI_Exception('test');
    		}

			Schema.DescribeSObjectResult r = BI_Plan_de_accion__c.sObjectType.getDescribe();
			String keyPrefix = r.getKeyPrefix();
			
			List<Event> news2 = new List<Event>();
						
			Boolean error = false;
			
			Set<Id> set_ap = new Set<Id>();
			for(Event ev:news){

				if(String.valueOf(ev.WhatId).startsWith(keyPrefix) && ev.RecordTypeId == MAP_RT_NAME.get('BI_Asistentes_Plan_de_accion')){
					if(set_ap.contains(ev.WhatId)){
						error = true;
						break;
					}else
						set_ap.add(ev.WhatId);
						
					news2.add(ev);
				}
			}
			
			if(error){
				for(Event ev2:news)
					ev2.addError(Label.BI_Eventos_Plan_Accion_Masivo);
			}else{
				if(!set_ap.isEmpty()){
					
					Map<Id, BI_Plan_de_accion__c> map_plans = new Map<Id, BI_Plan_de_accion__c>([select Id, (select Id from Events where RecordTypeId = :MAP_RT_NAME.get('BI_Asistentes_Plan_de_accion')) from BI_Plan_de_accion__c where Id IN :set_ap]);
					
					for(Event ev2:news2){
						if(map_plans.containsKey(ev2.WhatId) && map_plans.get(ev2.WhatId).Events.size() > 0)
							ev2.addError(Label.BI_Eventos_Plan_Accion);
					}
					
				}
			}
		}catch(Exception exc){
			BI_LogHelper.generate_BILog('BI_EventMethods.preventEventForActionPlan', 'BI_EN', Exc, 'Trigger');
		}
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos
	Company:       Aborda
	Description:   Method that stop event update if this has a Plan de acción on State IN (Finalizado, Cancelado)
	
	History: 
	
	<Date>            <Author>                    <Change Description>
	03/02/2014        Micah Burgos                Initial Version
    	12/06/2017		  Gawron, Julián              Fixing System.NullPointerException on WhatId
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void check_HasClosedPlanDeAccion(List<Event> news){
	    try{

	    	if (BI_TestUtils.isRunningTest()) {
				throw new BI_Exception('test');
    		}
    		
		    List<BI_Plan_de_accion__c> lst_ap = new List<BI_Plan_de_accion__c>();
		    Set<Id> set_ap = new Set<Id>();
		    
		    Schema.DescribeSObjectResult r = BI_Plan_de_accion__c.sObjectType.getDescribe();
			String keyPrefix = r.getKeyPrefix();
			
			List<Event> lst_Event = new List<Event>();
		    for(Event evento:news){
		    	if(evento.WhatId != null && String.valueOf(evento.WhatId).startsWith(keyPrefix)) //JEG
		        	set_ap.add(evento.WhatId);
		        	lst_Event.add(evento);
		    }
			    
		    if(!set_ap.isEmpty()){
		      
		    	Map<Id,BI_Plan_de_accion__c> map_ap = new Map<Id,BI_Plan_de_accion__c>([select Id from BI_Plan_de_accion__c where Id IN :set_ap AND BI_Estado__c != :Label.BI_PlanAccion_Estado_En_creacion]);
		    	for(Event evento :lst_Event){
		    		if(map_ap.containsKey(evento.WhatId)){
		    			evento.addError(Label.BI_Msg_NoModificarSiActionPlanCerrado);
		    		}
		    	}	
		    }
		    
	    }catch(Exception exc){
	    	BI_LogHelper.generate_BILog('BI_EventMethods.check_HasClosedPlanDeAccion', 'BI_EN', Exc, 'Trigger');
	    }
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Guillermo Muñoz
	Company:       Aborda
	Description:   Method that update the value of the field BI_Recurrente if the Event have recurrence or not
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	30/09/2015                      Guillermo Muñoz             Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void checkRecurrenceEvent(List<Event> news){

		for(Event evt : news){
			if(evt.RecurrenceActivityId != null){				
				evt.BI_Recurrente__c=true;
			}				
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Alvaro García
	Company:       Aborda
	Description:   Method that update an approval if the confirmed date or request status of the event change
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	16/01/2017                      Alvaro García               Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void updateBid_Event(List<Event> news, List<Event> olds){

    	try{

    		if (BI_TestUtils.isRunningTest()) {
				throw new BI_Exception('test');
    		}

	        List<BI_O4_Approvals__c> updateApproval = new List<BI_O4_Approvals__c>();
	        List<BI_O4_Approvals__c> listApproval = new List<BI_O4_Approvals__c>();
	        List<Event> list_event = new List<Event>();
	        Set<ID> approvalsID = new Set<ID>();

	        for(Integer i = 0; i < news.size(); i++) {

	        	if (((news[i].StartDateTime != olds[i].StartDateTime)
	        			|| (news[i].BI_O4_DRB_Confirmed_Date__c != olds[i].BI_O4_DRB_Confirmed_Date__c)
	        			|| (news[i].BI_O4_DRB_Request_Status__c != olds[i].BI_O4_DRB_Request_Status__c))
	        		&& news[i].OwnerID == calendarId 
	        		&& news[i].BI_O4_Record_type__c == 'Gate3' 
	        		&& news[i].BI_O4_fromBID__c == false) {
	        		
	        		approvalsID.add(news[i].WhatId); //APPROVAL ID
	           		list_event.add(news[i]);
	        	}   
	        }
                                   
	        listApproval = [SELECT Id, BI_O4_Event_Date__c,BI_O4_DRB_confirmed_DateTime__c,BI_O4_DRB_request_status__c, BI_O4_Reasons_for_DRB_slot_rejection__c
	                        FROM BI_O4_Approvals__c WHERE Id IN: approvalsID];                                  
	        
	        for(Event evento : list_event) {

	            for(BI_O4_Approvals__c aprobacion : listApproval ) {
	                
	                if(evento.WhatID == aprobacion.Id) {

	                	aprobacion.BI_O4_Event_Date__c = Date.valueOf(evento.StartDateTime);
	                    
                        if(evento.StartDateTime != aprobacion.BI_O4_DRB_confirmed_DateTime__c && aprobacion.BI_O4_DRB_confirmed_DateTime__c != null) {

                            aprobacion.BI_O4_DRB_confirmed_DateTime__c = evento.StartDateTime;     
                        }
                            
                            
                        else if (aprobacion.BI_O4_DRB_confirmed_DateTime__c != evento.BI_O4_DRB_Confirmed_Date__c|| aprobacion.BI_O4_DRB_confirmed_DateTime__c == null) {     
                        
                            aprobacion.BI_O4_DRB_confirmed_DateTime__c = evento.BI_O4_DRB_Confirmed_Date__c;
                        }
                            
                        if(aprobacion.BI_O4_DRB_request_status__c != evento.BI_O4_DRB_Request_Status__c|| aprobacion.BI_O4_DRB_request_status__c == null) {     
                            
                            aprobacion.BI_O4_DRB_request_status__c = evento.BI_O4_DRB_Request_Status__c;
                            aprobacion.BI_O4_Reasons_for_DRB_slot_rejection__c = evento.BI_O4_Reasons_for_DRB__c;
                        }
                            
                        updateApproval.add(aprobacion);
                        BI_O4_ApprovalsMethods.changedFlag = false;
	                }
	            }
	           
	        }

	     	if (!updateApproval.isEmpty()) {
	     		update updateApproval;
	     	}

	    }catch(Exception exc){
	    	BI_LogHelper.generate_BILog('BI_EventMethods.updateBid_Event', 'BI_EN', Exc, 'Trigger');
	    }
    }

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Alvaro García
	Company:       Aborda
	Description:   Method that check if is possible change the date.
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	16/01/2017                      Alvaro García               Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void updateEvent_Dates(List<Event> news, List<Event> olds){
      
	    try{

    		if (BI_TestUtils.isRunningTest()) {
				throw new BI_Exception('test');
    		}

    		String myProfileName = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1].Name;
    		List<Event> listUpdateEvent=new List<Event>();

    		for(integer i = 0; i < trigger.new.size(); i++)
            {
          
                if((news[i].StartDateTime != olds[i].StartDateTime) 
                	&& news[i].OwnerID ==  calendarId 
                	&& news[i].BI_O4_Record_type__c == 'Gate3' 
                    && news[i].BI_O4_fromBID__c == false 
                    && myProfileName != Label.BI_Administrador_Sistema) { 
                  
                  	news[i].StartDateTime.addError('Can not change Start date manually');

                }
                
                if((news[i].EndDateTime != olds[i].EndDateTime)
                	&& news[i].OwnerID ==  calendarId 
                    && news[i].BI_O4_Record_type__c == 'Gate3' 
                    && news[i].BI_O4_fromBID__c == false 
                    && myProfileName != Label.BI_Administrador_Sistema) { 
                
                	news[i].EndDateTime.addError('Can not change End date manually');

                }
                
                if((news[i].StartDateTime != olds[i].StartDateTime)&& 
                    news[i].OwnerID ==  calendarId &&
                    news[i].BI_O4_Record_type__c == 'Gate3' &&
                    news[i].BI_O4_fromBID__c == false) {          
                        
                    listUpdateEvent.add(news[i]);
                        
                }
            }

	      	Datetime today = System.now();
	        List<Event> lstEvent = [SELECT Id, StartDateTime, EndDateTime, WhatID FROM Event WHERE StartDateTime >= :today];
	      
	        for(Event ev : listUpdateEvent){

	            for(Event exEvent : lstEvent){

	                if(ev.StartDateTime == exEvent.StartDateTime){

	                  ev.addError('The time slot is busy, please select another time slot');
	                }
	            }
	        } 

	    }catch(Exception exc){
	    	BI_LogHelper.generate_BILog('BI_EventMethods.updateEvent_Dates', 'BI_EN', Exc, 'Trigger');
	    }  
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Fernández
    Company:       Accenture
    Description:   Method that calls BI_AccountHelper.updateOfertaVisita() method to update the 'Cobertura de Visita' KPI on Account
    
    History:
    
    <Date>            <Author>             <Description>
    16/02/2017        Alberto Fernández	   Initial version D398
    22/02/2017        Gawron, Julián       Adding Future Methods
    12/06/2017		  Gawron, Julián 	   Fixing System.NullPointerException on WhatId
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateAccOfertaVisita(List<Event> news, Map<Id, Event> olds){

    	try {

    		Set<Id> set_accIds = new Set<Id>();
	    	System.debug('updateAccOfertaVisita entro');
	    	/*Se trata de un INSERT*/
	    	if(olds == null) {
	    		System.debug('updateAccOfertaVisita  es insert');
	    		for(Event newEvent : news) {
	    			if(newEvent.WhatId != null && String.valueOf(newEvent.WhatId).startsWith('001')) { //JEG
	    				set_accIds.add(newEvent.WhatId);
	    			}
		    	}
	    	}

	    	/*Se trata de un UPDATE*/
	    	else if(news != null && olds != null) {
	    		for(Event newEvent : news) {
	    			if(newEvent.WhatId != null && String.valueOf(newEvent.WhatId).startsWith('001')) { //JEG
		    			set_accIds.add(newEvent.WhatId);
	    			}
	    			if(newEvent.WhatId != null && String.valueOf(olds.get(newEvent.Id).WhatId).startsWith('001')) { //JEG
		    			if(newEvent.WhatId != olds.get(newEvent.Id).WhatId) {
							set_accIds.add(olds.get(newEvent.Id).WhatId);
							System.debug('AÑADO OLD: ' + olds.get(newEvent.Id).WhatId);
		    			}
		    		}
	    		}
	    	}
	    	System.debug('updateAccOfertaVisita set_accIds: ' + set_accIds);
	    	
           if(!System.isFuture()){
            BI_AccountHelper.updateOfertaVisita_fut(set_accIds);
            System.debug('entro updateOfertaVisita envio futuro:' + set_accIds);
           }else{
            BI_AccountHelper.updateOfertaVisita(set_accIds);
            System.debug('entro updateOfertaVisita envio no futuro:' + set_accIds);

           }

    	}
    	catch (Exception exc) {
            BI_LogHelper.generate_BILog('BI_EventMethods.updateAccOfertaVisita', 'BI_EN', Exc, 'Trigger');
        }

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes
    Company:       Accenture
    Description:   Method that fill the field "BI_FVI_Nodo_c" on the Event when it is created. 
    
    History:
    
    <Date>            <Author>             <Description>
    29/05/2017        Humberto Nunes	   Initial version D398
    12/06/2017		  Gawron, Julián 	   Fixing System.NullPointerException on WhatId
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void fillNodoInEvent(List<Event> news)
    {
    	try 
    	{
    		Set<Id> set_accIds = new Set<Id>();
    		for(Event newEvent : news) 
    		{
    			if(newEvent.WhatId != null && String.valueOf(newEvent.WhatId).startsWith('001')) //JEG
    			{
    				set_accIds.add(newEvent.WhatId);
    			}
	    	}

	    	List<Account> lstAcc = [SELECT Id, BI_FVI_Nodo__c FROM Account WHERE Id in :set_accIds];

			for (Account acc:lstAcc)
    		{
    			for(Event newEvent : news) 
		    	{
		    		if (newEvent.WhatId == acc.Id) newEvent.BI_FVI_Nodo__c = acc.BI_FVI_Nodo__c;
		    	}
    		}		    	
    	}
    	catch (Exception exc) 
    	{
            BI_LogHelper.generate_BILog('BI_EventMethods.fillNodoInEvent', 'BI_EN', Exc, 'Trigger');
        }  	
    }
}