/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Antonio Pardo
     Company:       Accenture
     Description:   Controller to get images from CWP_Carousel__mdt metadata
                   
     Test Class:    TGS_Carousel_Ctrl_Test
     History:
     
     <Date>            <Author>             <Description>
                        Antonio Pardo        First version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public with sharing class TGS_Carousel_Ctrl {
    
    @AuraEnabled
    public static Map<String, Object> getImages(){
        Map<String, Object> map_return = new Map<String, Object>();
        map_return.put('isoLang', UserInfo.getLanguage());
        map_return.put('lstImages', [SELECT Id, CWP_Image_name__c, CWP_Description_Label__c ,CWP_Resource__c, CWP_Path__c, CWP_Order__c FROM CWP_Carousel__mdt WHERE Label = 'PortalSDN' ORDER BY CWP_Order__c]);
        return map_return;
    }
}