/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Masferrer
Company:       NEAborda
Description:   Class called by the Demanda.trigger

History:

<Date>            <Author>          <Description>
01/03/2014    Antonio Masferrer	    Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_DemandaMethods {


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Antonio Masferrer
	Company:       NEAborda
	Description:   method that assigns the owner

	History: 

	<Date>                          <Author>                    <Change Description>
	01/03/2016                     Antonio Masferrer              Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void asignaPropietario(List<BI_Demanda__c> tnew, List<BI_Demanda__c> told){
		
		//QueueSobject cola_demandas = [SELECT QueueId FROM QueueSobject WHERE Queue.Name = 'Cola demandas'];
		//QueueSobject cola_proyectos = [SELECT QueueId FROM QueueSobject WHERE Queue.Name = 'Cola proyectos'];
		//QueueSobject cola_peticion = [SELECT QueueId FROM QueueSobject WHERE Queue.Name = 'Cola petición'];

		Group grp_dem =[SELECT Id FROM Group WHERE Type = 'Queue' AND Name = 'Cola demandas'];
		Group grp_pro =[SELECT Id FROM Group WHERE Type = 'Queue' AND Name = 'Cola proyectos'];
		Group grp_pet =[SELECT Id FROM Group WHERE Type = 'Queue' AND Name = 'Cola petición'];

		for(Integer i = 0 ; i < tnew.size() ; i++){

			if(told == null || tnew[i].BI_Tipologia__c != told[i].BI_Tipologia__c ){

				if(tnew[i].BI_Tipologia__c == 'Demanda'){
				
					tnew[i].OwnerId = grp_dem.Id;

				}else if(tnew[i].BI_Tipologia__c == 'Proyecto'){
					
					tnew[i].OwnerId = grp_pro.Id;

				}else if(tnew[i].BI_Tipologia__c == 'Petición Ambientes' || tnew[i].BI_Tipologia__c == 'Instalación Paquete'){

					tnew[i].OwnerId = grp_pet.Id;
				}
			}
		}
	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Antonio Masferrer
	Company:       NEAborda
	Description:   method that assigns values for BI_Tipo_de_demanda__c

	History: 

	<Date>                          <Author>                    <Change Description>
	01/03/2016                     Antonio Masferrer              Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void asignaTipoDemanda(List<BI_Demanda__c> tnew, List<BI_Demanda__c> told){
		
		List<BI_Demanda__c> lst_dem = new List<BI_Demanda__c>();

		if(told == null){

			for(BI_Demanda__c dem : tnew){
			
				lst_dem.add(dem);
			}
		}else{

			for(Integer i = 0 ; i < tnew.size() ; i++){
				
				if(tnew[i].BI_Horas_Estimadas__c != told[i].BI_Horas_Estimadas__c || tnew[i].BI_Tipo_de_demanda__c != told[i].BI_Tipo_de_demanda__c){

					lst_dem.add(tnew[i]);
				}
			}
		}

		if(!lst_dem.isEmpty()){

			for(BI_Demanda__c dem : lst_dem){
					
				if(dem.BI_Horas_Estimadas__c <= 4){

					dem.BI_Tipo_de_demanda__c = 'Inmediata';

				}else if(dem.BI_Horas_Estimadas__c > 4 && dem.BI_Horas_Estimadas__c <= 24){

					dem.BI_Tipo_de_demanda__c = 'Minor';				

				}else if(dem.BI_Horas_Estimadas__c > 24){

					dem.BI_Tipo_de_demanda__c = 'Major';
				}
			}
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Antonio Masferrer
	Company:       NEAborda
	Description:   Method that creates BI_Asignacion_demanda__c if BI_Demanda__c.BI_Estado_de_la_demanda__c or BI_Demanda__c.BI_Tipologia__c change

	History: 

	<Date>                          <Author>                    <Change Description>
	01/03/2016                     Antonio Masferrer              Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	
	public static void cambioAsignacion(List<BI_Demanda__c> tnew, List<BI_Demanda__c> told){

		Map<BI_Demanda__c, BI_Demanda__c> mapa_dem_dem = new Map<BI_Demanda__c, BI_Demanda__c>();
		
		try{   

			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			
			for(Integer i = 0 ; i < tnew.size() ; i++){

				if(tnew[i].BI_Estado_de_la_demanda__c != told[i].BI_Estado_de_la_demanda__c || tnew[i].BI_Tipologia__c != told[i].BI_Tipologia__c || tnew[i].OwnerId != told[i].OwnerId){
				
					mapa_dem_dem.put(tnew[i], told[i]);
				}
			}
			
			if(mapa_dem_dem.size() > 0){
				
				List<BI_Asignacion_demanda__c> lst_ad = new List<BI_Asignacion_demanda__c>();

				for(BI_Demanda__c d : mapa_dem_dem.keySet()){
					
					BI_Asignacion_demanda__c ad = new BI_Asignacion_demanda__c();

					ad.BI_Demanda__c = d.Id;
					ad.BI_Fecha_modificacion__c = Date.today();

					ad.BI_Estado_actual__c = d.BI_Estado_de_la_demanda__c;
					ad.BI_Estado_anterior__c = mapa_dem_dem.get(d).BI_Estado_de_la_demanda__c;
					
					ad.BI_Id_propietario_actual__c = d.OwnerId;
					ad.BI_Id_propietario_anterior__c = mapa_dem_dem.get(d).OwnerId;

					ad.BI_Tipo_demanda_actual__c = d.BI_Tipologia__c;
					ad.BI_Tipo_demanda_anterior__c = mapa_dem_dem.get(d).BI_Tipologia__c;

					lst_ad.add(ad);
				}
				
				insert lst_ad;

				Map<BI_Asignacion_demanda__c, Id> mapa_ad_ac = new Map<BI_Asignacion_demanda__c, Id>();
				Map<BI_Asignacion_demanda__c, Id> mapa_ad_an = new Map<BI_Asignacion_demanda__c, Id>();

				for(BI_Asignacion_demanda__c ad : lst_ad){

					Id prop_actual = ad.BI_Id_propietario_actual__c;
					Id prop_anterior = ad.BI_Id_propietario_anterior__c;

					if(prop_actual.getSobjectType().getDescribe().getName() == 'User'){

						mapa_ad_ac.put(ad, prop_actual);
  
					}else if(prop_actual.getSobjectType().getDescribe().getName() == 'Group'){

						ad.BI_Id_propietario_actual__c = 'p/own/Queue/d?id=' + ad.BI_Id_propietario_actual__c;

						if(ad.BI_Tipo_demanda_actual__c == 'Demanda'){

							ad.BI_Enlace_actual__c = 'Cola Demandas';

						}else if(ad.BI_Tipo_demanda_actual__c == 'Proyecto'){

							ad.BI_Enlace_actual__c = 'Cola Proyectos';

						}else if(ad.BI_Tipo_demanda_actual__c == 'Petición Ambientes' || ad.BI_Tipo_demanda_actual__c == 'Instalación Paquete'){

							ad.BI_Enlace_actual__c = 'Cola Petición | Instalación';
						}
					}

					if(prop_anterior.getSobjectType().getDescribe().getName() == 'User'){	
						
						mapa_ad_an.put(ad, prop_anterior);

					}else if(prop_anterior.getSobjectType().getDescribe().getName() == 'Group'){

						ad.BI_Id_propietario_anterior__c = 'p/own/Queue/d?id=' + ad.BI_Id_propietario_anterior__c;

						if(ad.BI_Tipo_demanda_anterior__c == 'Demanda'){

							ad.BI_Enlace_anterior__c = 'Cola Demandas';

						}else if(ad.BI_Tipo_demanda_anterior__c == 'Proyecto'){

							ad.BI_Enlace_anterior__c = 'Cola Proyectos';

						}else if(ad.BI_Tipo_demanda_anterior__c == 'Petición Ambientes' || ad.BI_Tipo_demanda_anterior__c == 'Instalación Paquete'){

							ad.BI_Enlace_anterior__c = 'Cola Petición | Instalación';
						}
					}
				}

				Integer cont = 0;

				if(mapa_ad_ac.size() > 0){

					List<User> lst_usu_ac = [SELECT Name FROM USER WHERE Id IN: mapa_ad_ac.values()];

					for(BI_Asignacion_demanda__c ad : mapa_ad_ac.keySet()){
					
						for(BI_Asignacion_demanda__c lad : lst_ad){
							
							if(ad.Id == lad.Id){

								lad.BI_Enlace_actual__c = lst_usu_ac[cont].Name;
							}
						}
						
						cont++;
					}
				}

				cont = 0;
				
				if(mapa_ad_an.size() > 0){

					List<User> lst_usu_an = [SELECT Name FROM USER WHERE Id IN: mapa_ad_an.values()];
					
					for(BI_Asignacion_demanda__c ad : mapa_ad_an.keySet()){
					
						for(BI_Asignacion_demanda__c lad : lst_ad){
							
							if(ad.Id == lad.Id){

								lad.BI_Enlace_anterior__c = lst_usu_an[cont].Name;
							}
						}
						cont++;
					}
				}
				update lst_ad;
			}
		}catch(Exception e){
			BI_LogHelper.generate_BILog('BI_Demanda.asignaPropietario', 'BI_EN', e, 'Trigger');
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Angel F. Santaliestra Pasias
	Company:       NEAborda
	Description:   Method that calculate the time between stages.

	History: 

	<Date>                          <Author>                    <Change Description>
	31/10/2017				Angel F. Santaliestra Pasias		Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void calculateTimeBetweenStages(List<BI_Demanda__c> triggerNew, List<BI_Demanda__c> triggerOld) {
		Map<Id, BI_Demanda__c> mapDemandsOld = new Map<Id, BI_Demanda__c>(triggerOld);
		BI_Demanda__c inDemandOld;
		Boolean isStageEquals;
		String oldDemandStage;
		Decimal timeInMinutes;
		DateTime oldDateToCompare;
		try{
			for (BI_Demanda__c inDemandNew : triggerNew) {
				inDemandOld = mapDemandsOld.get(inDemandNew.Id);
				isStageEquals = inDemandNew.BI_Estado_de_la_demanda__c == inDemandOld.BI_Estado_de_la_demanda__c;
				if (isStageEquals) {
					// The stage of the demand didn't change, do nothing with this demand.
					continue;
				}
				if (inDemandOld.BI_Fecha_ultima_modificacion_etapa__c == null) {
					// The field BI_Fecha_ultima_modificacion_etapa__c is empty. Use the LastModifiedDate.
					oldDateToCompare = inDemandOld.LastModifiedDate;
				} else {
					oldDateToCompare = inDemandOld.BI_Fecha_ultima_modificacion_etapa__c;
				}
				// The stage of the demand change, update the time of the old stage.
				inDemandNew.BI_Fecha_ultima_modificacion_etapa__c = System.now();
				oldDemandStage = inDemandOld.BI_Estado_de_la_demanda__c;
				timeInMinutes = (inDemandNew.BI_Fecha_ultima_modificacion_etapa__c.getTime() - oldDateToCompare.getTime())/60000;

				inDemandNew.BI_Tiempo_1_Por_revisar__C 			= checkDemand(oldDemandStage, '1- Por revisar', 		inDemandNew.BI_Tiempo_1_Por_revisar__C,			timeInMinutes);
				inDemandNew.BI_Tiempo_2_Congelado__c 			= checkDemand(oldDemandStage, '2- Congelado',			inDemandNew.BI_Tiempo_2_Congelado__c,			timeInMinutes);
				inDemandNew.BI_Tiempo_3_En_analisis__c 			= checkDemand(oldDemandStage, '3- En Análisis',			inDemandNew.BI_Tiempo_3_En_analisis__c,			timeInMinutes);
				inDemandNew.BI_Tiempo_4_Pend_Estimacion__c 		= checkDemand(oldDemandStage, '4- Pend. Estimación',	inDemandNew.BI_Tiempo_4_Pend_Estimacion__c,		timeInMinutes);
				inDemandNew.BI_Tiempo_5_Pend_Planificacion__c 	= checkDemand(oldDemandStage, '5- Pend. Planificación',	inDemandNew.BI_Tiempo_5_Pend_Planificacion__c,	timeInMinutes);
				inDemandNew.BI_Tiempo_6_Planificado__c 			= checkDemand(oldDemandStage, '6- Planificado',			inDemandNew.BI_Tiempo_6_Planificado__c,			timeInMinutes);
				inDemandNew.BI_Tiempo_7_En_curso__c 			= checkDemand(oldDemandStage, '7- En curso',			inDemandNew.BI_Tiempo_7_En_curso__c,			timeInMinutes);
				inDemandNew.BI_Tiempo_7_1_Retrabajo__c 			= checkDemand(oldDemandStage, '7.5 - Retrabajo',		inDemandNew.BI_Tiempo_7_1_Retrabajo__c,			timeInMinutes);
				inDemandNew.BI_Tiempo_8_Certificacion__c 		= checkDemand(oldDemandStage, '8- Certificación',		inDemandNew.BI_Tiempo_8_Certificacion__c,		timeInMinutes);
				inDemandNew.BI_Tiempo_9_Pendiente_de_PAP__c 	= checkDemand(oldDemandStage, '9- Pendiente de PAP',	inDemandNew.BI_Tiempo_9_Pendiente_de_PAP__c,	timeInMinutes);
			}
		}catch(Exception e){
			BI_LogHelper.generate_BILog('BI_Demanda.calculateTimeBetweenStages', 'BI_EN', e, 'Trigger');
		}
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Angel F. Santaliestra Pasias
	Company:       NEAborda
	Description:   Method that checks if is this time the time that must be changed. If not, return the same time that have before.

	History: 

	<Date>                          <Author>                    <Change Description>
	31/10/2017				Angel F. Santaliestra Pasias		Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static Decimal checkDemand(String oldDemandStage, String label, Decimal timeTotal, Decimal timeInMinutes) {
		if (timeTotal == null)
			timeTotal = 0;
		return (oldDemandStage == label) ? (timeTotal + timeInMinutes) : timeTotal;
	}
}