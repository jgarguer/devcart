@RestResource(urlMapping='/customeraccounts/v1/accounts/*/tickets')
global class BI_TicketsForAccountRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Account's related tickets Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    21/08/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Retrieves the list of tickets associated to the specific account
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.TicketsListInfoType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    21/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.TicketsListInfoType getTickets() {
		
		BI_RestWrapper.TicketsListInfoType ticListInfoType; 
		
		try{
			
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
			
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
			
				//MULTIPLE TICKETS
				ticListInfoType = BI_RestHelper.getTicketsForAccount(RestContext.request.requestURI.split('/')[4], RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
			
				RestContext.response.statuscode = (ticListInfoType == null)?404:200;//404 NOT_FOUND, 200 OK				
			
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_TicketsForAccountRest.getTickets', 'BI_EN', exc, 'Web Service');
			
		}
		
		return ticListInfoType;
		
	}
	
}