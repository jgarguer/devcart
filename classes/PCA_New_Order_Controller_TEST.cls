/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Pedro Párraga
        Company:       Accenture  
        Description:   Test method that manage the code coverage from NEBit2WinApi.doCalloutFromFuture
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        07/02/2017                      Pedro Párraga               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
public class PCA_New_Order_Controller_TEST { 
    
   @isTest(SeeAllData=true) static void Test_Method_One() {
    BI_TestUtils.throw_exception = false;
    BI_MigrationHelper.skipAllTriggers();

       User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
       List<RecordType> listRecordTypes = [SELECT Id, Name FROM RecordType WHERE Name LIKE 'Asset'];

        System.runAs(userTest){ 
            Test.startTest();

        NE__Asset__c commAsset = new NE__Asset__c(NE__ServAccId__c = userTest.Contact.Account.Id, NE__BillAccId__c = userTest.Contact.Account.Id);
        insert commAsset;

        NE__Order__c testOrder = new NE__Order__c(RecordTypeId = listRecordTypes[0].Id, NE__Asset__c = commAsset.Id);
        insert testOrder;

        NE__Product__c testProduct = new NE__Product__c(Name='Universal WIFI');
        insert testProduct;

        NE__OrderItem__c testOrderItem = new NE__OrderItem__c(NE__OrderId__c = testOrder.Id, NE__ProdId__c = testProduct.Id, NE__Qty__c=1, NE__Account__c = userTest.Contact.Account.Id, TGS_Service_Status__c = 'Deployed', NE__Service_Account_Asset_Item__c= userTest.Contact.Account.Id, NE__Billing_Account_Asset_Item__c= userTest.Contact.Account.Id); 
                
        insert testOrderItem;

        NE__Order_Item_Attribute__c attribute = new NE__Order_Item_Attribute__c(Name = 'Domain', NE__Value__c = 'test', NE__Order_Item__c = testOrderItem.Id);
       insert attribute;

        NE__Catalog_Header__c testCatalogHeader = new NE__Catalog_Header__c(NE__Name__c='Test Catalog Header');
        insert testCatalogHeader;

        NE__Catalog__c testCatalog = new NE__Catalog__c(Name='Test Catalog', NE__Catalog_Header__c = testCatalogHeader.Id);
        insert testCatalog;

        NE__Catalog_Category__c testCategory = new NE__Catalog_Category__c(Name = 'Test Category', NE__CatalogId__c = testCatalog.Id);
        insert testCategory;

        NE__Catalog_Item__c testCatalogItem = new NE__Catalog_Item__c(NE__Catalog_Category_Name__c = testCategory.Id, NE__ProductId__c = testProduct.Id, NE__Catalog_Id__c = testCatalog.Id, NE__Technical_Behaviour__c = 'Attachment Required');
        insert testCatalogItem;

        NE__CatalogMessage__c testCatalogMessage = new NE__CatalogMessage__c(NE__Text__c = 'test', NE__Type__c = 'req', NE__CatalogId__c = testCatalog.Id, NE__Catalog_Item__c = testCatalogItem.Id, CurrencyIsoCode = 'EUR');
        insert testCatalogMessage;
        BI_MigrationHelper.cleanSkippedTriggers();
            ApexPages.currentPage().getParameters().put('rootItemVId', testOrderItem.Id);
            ApexPages.currentPage().getParameters().put('cId', testCatalogItem.Id);
            ApexPages.currentPage().getParameters().put('company', userTest.Contact.AccountId);
            ApexPages.currentPage().getParameters().put('user', userTest.Contact.AccountId);
            ApexPages.currentPage().getParameters().put('payer', userTest.Contact.AccountId);
            ApexPages.currentPage().getParameters().put('site', 'site');
            PCA_New_Order_Controller controller = new PCA_New_Order_Controller();

            controller.configuration = new NE.NewConfigurationController();
            controller.mySite ='mysite';
            controller.confItem = testOrderItem;
            controller.additionalCSSUrl = 'additionalCSSUrl';
            controller.hiddenInCWPMap = new Map<Id, Boolean>();
            controller.mapKeys = 'mapKeys';
            controller.attDescription = 'attDescription';
            controller.myAttachmentBody = 'myAttachmentBody';
            controller.myAttachmentName = 'myAttachmentName';
            controller.comProd = testProduct;
            controller.quantity = 'quantity';

            controller.isRegistration = true;
            controller.configuration.getCatalog();
            controller.saveAndCloseConfiguration();
            controller.cancel();

            Test.stopTest();
        }
        
    }

   @isTest(SeeAllData=true) static void Test_Method_Two() {

    BI_TestUtils.throw_exception = false;
    BI_MigrationHelper.skipAllTriggers();

       User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
       List<RecordType> listRecordTypes = [SELECT Id, Name FROM RecordType WHERE Name LIKE 'Asset'];

        System.runAs(userTest){ 
            Test.startTest();

        NE__Asset__c commAsset = new NE__Asset__c(NE__ServAccId__c = userTest.Contact.Account.Id, NE__BillAccId__c = userTest.Contact.Account.Id);
        insert commAsset;

        NE__Order__c testOrder = new NE__Order__c(RecordTypeId = listRecordTypes[0].Id, NE__Asset__c = commAsset.Id);
        insert testOrder;

        NE__Product__c testProduct = new NE__Product__c(Name='Universal WIFI');
        insert testProduct;

        NE__OrderItem__c testOrderItem = new NE__OrderItem__c(NE__OrderId__c = testOrder.Id, NE__ProdId__c = testProduct.Id, NE__Qty__c=1, NE__Account__c = userTest.Contact.Account.Id, TGS_Service_Status__c = 'Deployed', NE__Service_Account_Asset_Item__c= userTest.Contact.Account.Id, NE__Billing_Account_Asset_Item__c= userTest.Contact.Account.Id); 
                
        insert testOrderItem;

        NE__Order_Item_Attribute__c attribute = new NE__Order_Item_Attribute__c(Name = 'Domain', NE__Value__c = 'test', NE__Order_Item__c = testOrderItem.Id);
       insert attribute;

        NE__Catalog_Header__c testCatalogHeader = new NE__Catalog_Header__c(NE__Name__c='Test Catalog Header');
        insert testCatalogHeader;

        NE__Catalog__c testCatalog = new NE__Catalog__c(Name='Test Catalog', NE__Catalog_Header__c = testCatalogHeader.Id);
        insert testCatalog;

        NE__Catalog_Category__c testCategory = new NE__Catalog_Category__c(Name = 'Test Category', NE__CatalogId__c = testCatalog.Id);
        insert testCategory;

        NE__Catalog_Item__c testCatalogItem = new NE__Catalog_Item__c(NE__Catalog_Category_Name__c = testCategory.Id, NE__ProductId__c = testProduct.Id, NE__Catalog_Id__c = testCatalog.Id, NE__Technical_Behaviour__c = 'Attachment Required');
        insert testCatalogItem;

        NE__CatalogMessage__c testCatalogMessage = new NE__CatalogMessage__c(NE__Text__c = 'test', NE__Type__c = 'req', NE__CatalogId__c = testCatalog.Id, NE__Catalog_Item__c = testCatalogItem.Id, CurrencyIsoCode = 'EUR');
        insert testCatalogMessage;
        BI_MigrationHelper.cleanSkippedTriggers();
            ApexPages.currentPage().getParameters().put('rootItemVId', testOrderItem.Id);
            ApexPages.currentPage().getParameters().put('company', userTest.Contact.AccountId);
            ApexPages.currentPage().getParameters().put('user', userTest.Contact.AccountId);
            ApexPages.currentPage().getParameters().put('payer', userTest.Contact.AccountId);
            ApexPages.currentPage().getParameters().put('site', 'site');
            PCA_New_Order_Controller controller = new PCA_New_Order_Controller();

            try{
              controller.TGSStartConfigurator();
            }catch(Exception e){}

            Test.stopTest();
        }
        
    }

   @isTest(SeeAllData=true) static void Test_Method_Three() {

    BI_TestUtils.throw_exception = false;
    BI_MigrationHelper.skipAllTriggers();

       User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
       List<RecordType> listRecordTypes = [SELECT Id, Name FROM RecordType WHERE Name LIKE 'Asset'];

        System.runAs(userTest){ 
            Test.startTest();

        NE__Asset__c commAsset = new NE__Asset__c(NE__ServAccId__c = userTest.Contact.Account.Id, NE__BillAccId__c = userTest.Contact.Account.Id);
        insert commAsset;

        NE__Order__c testOrder = new NE__Order__c(RecordTypeId = listRecordTypes[0].Id, NE__Asset__c = commAsset.Id);
        insert testOrder;

        NE__Product__c testProduct = new NE__Product__c(Name='Universal WIFI');
        insert testProduct;

        NE__OrderItem__c testOrderItem = new NE__OrderItem__c(NE__OrderId__c = testOrder.Id, NE__ProdId__c = testProduct.Id, NE__Qty__c=1, NE__Account__c = userTest.Contact.Account.Id, TGS_Service_Status__c = 'Deployed', NE__Service_Account_Asset_Item__c= userTest.Contact.Account.Id, NE__Billing_Account_Asset_Item__c= userTest.Contact.Account.Id); 
                
        insert testOrderItem;

        NE__Order_Item_Attribute__c attribute = new NE__Order_Item_Attribute__c(Name = 'Domain', NE__Value__c = 'test', NE__Order_Item__c = testOrderItem.Id);
       insert attribute;

        NE__Catalog_Header__c testCatalogHeader = new NE__Catalog_Header__c(NE__Name__c='Test Catalog Header');
        insert testCatalogHeader;

        NE__Catalog__c testCatalog = new NE__Catalog__c(Name='Test Catalog', NE__Catalog_Header__c = testCatalogHeader.Id);
        insert testCatalog;

        NE__Catalog_Category__c testCategory = new NE__Catalog_Category__c(Name = 'Test Category', NE__CatalogId__c = testCatalog.Id);
        insert testCategory;

        NE__Catalog_Item__c testCatalogItem = new NE__Catalog_Item__c(NE__Catalog_Category_Name__c = testCategory.Id, NE__ProductId__c = testProduct.Id, NE__Catalog_Id__c = testCatalog.Id);
        insert testCatalogItem;

        NE__CatalogMessage__c testCatalogMessage = new NE__CatalogMessage__c(NE__Text__c = 'test', NE__Type__c = 'req', NE__CatalogId__c = testCatalog.Id, NE__Catalog_Item__c = testCatalogItem.Id, CurrencyIsoCode = 'EUR');
        insert testCatalogMessage;
            BI_MigrationHelper.cleanSkippedTriggers();
            ApexPages.currentPage().getParameters().put('rootItemVId', testOrderItem.Id);
            ApexPages.currentPage().getParameters().put('cId', testCatalogItem.Id);
            ApexPages.currentPage().getParameters().put('company', userTest.Contact.AccountId);
            ApexPages.currentPage().getParameters().put('user', userTest.Contact.AccountId);
            ApexPages.currentPage().getParameters().put('payer', userTest.Contact.AccountId);
            ApexPages.currentPage().getParameters().put('site', 'site');
            PCA_New_Order_Controller controller = new PCA_New_Order_Controller();

            controller.configuration = new NE.NewConfigurationController();
            controller.mySite ='mysite';
            controller.confItem = testOrderItem;
            controller.additionalCSSUrl = 'additionalCSSUrl';
            controller.hiddenInCWPMap = new Map<Id, Boolean>();
            controller.mapKeys = 'mapKeys';
            controller.attDescription = 'attDescription';
            controller.myAttachmentBody = 'myAttachmentBody';
            controller.myAttachmentName = 'myAttachmentName';
            controller.comProd = testProduct;
            controller.quantity = 'quantity';

            controller.isRegistration = true;
            controller.configuration.getCatalog();

            try{
              controller.saveAndCloseConfiguration();
            }catch(Exception e){}

            controller.cancel();
            
            Test.stopTest();
        }
        
    }
}