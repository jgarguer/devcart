@isTest
/******************************************
 * Author: JAvier Lopez Andradas
 * Company: gCTIO
 * Description: Test class for the Forecast development
 * 
 * 
 * 
 * 
 * *****************************************/
public class FO_ForecastTest_TEST {
    /*******************************************
     * Author: Javier Lopez Andradas
     * Company: gCTIO
     * Description: Loads the initial data to do the test
     * 
     * 
     * 
     * ******************************************/
	@testsetup
    private static void loadTestData(){
        List<Account> lst_acc = new List<Account>();
        
        Account cuenta1 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20431271525',
			    CurrencyIsoCode = 'MXN',
  			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
			lst_acc.add(cuenta1);

			Account cuenta2 = new Account(
			    Name = 'CLIENTE_TEST',
			    BI_Country__c = Label.BI_Peru,
			    BI_No_Identificador_fiscal__c = '20538273525',
			    CurrencyIsoCode = 'MXN',
			    BI_Segment__c = 'test',
				BI_Subsegment_Regional__c = 'test',
				BI_Territory__c = 'test',
			    BI_FVI_Tipo_Planta__c = 'No Cliente'
			);
        lst_acc.add(cuenta2);
        //Aqui tengo los ids de las cuentas
        DataBase.SaveResult[] arr_svrAcc = DataBAse.insert(lst_acc);
        Map<String,String> rt_mp = new  Map<String,String>();
        List<RecordType> lst_rt = [Select Id,SObjectType,DeveloperName from RecordType where SObjectType IN('BI_FVI_Nodos__c','Opportunity','BI_Objetivo_Comercial__c','BI_FVI_Nodos_Objetivos__c')];
        for(RecordType rt : lst_rt){
            rt_mp.put(rt.SObjectType+rt.DeveloperName,rt.Id);
        }
        List<BI_FVI_Nodos__c> ls_node = new List<BI_FVI_Nodos__c>();
        BI_FVI_Nodos__c nodo_ncs = new BI_FVI_Nodos__c(
	            Name = 'NCS_TEST', 
	            RecordTypeId = rt_mp.get('BI_FVI_Nodos__cBI_FVI_NCS'),
	            BI_FVI_Pais__c = Label.BI_Peru
        	);
        ls_node.add(nodo_ncs);
        DataBase.SaveResult[] arr_svrNodeP = DataBase.insert(ls_node); 
        BI_FVI_Nodos__c nodo_ncp = new BI_FVI_Nodos__c(
        	Name='NCP_TEST',
            RecordTypeId=rt_mp.get('BI_FVI_Nodos__cBI_FVI_NCP'),
            BI_FVI_NodoPadre__c=arr_svrNodeP[0].getId(),
            BI_FVI_Pais__c = Label.BI_Peru
        );
        System.debug(nodo_ncp);
        ls_node.clear();
        List<BI_FVI_Nodos__c> lst_ndNCP = new List<BI_FVI_Nodos__c>();
        lst_ndNCP.add(nodo_ncp);
       	Database.SaveResult[] arr_svrNode = Database.insert(lst_ndNCP);
        System.debug(arr_svrNode);
        List<BI_FVI_Nodo_Cliente__c> lst_ndc = new List<BI_FVI_Nodo_Cliente__c>();
        for(Database.SaveResult sv: arr_svrAcc){
            BI_FVI_Nodo_Cliente__c ndc = new BI_FVI_Nodo_Cliente__c(
            	BI_FVI_IdCliente__c=sv.getId(),
                BI_FVI_IdNodo__c=arr_svrNode[0].getId()
            );
            lst_ndc.add(ndc);
        }
        insert(lst_ndc);
        List<BI_Periodo__c> lst_per =new List<BI_Periodo__c>();
        Date hoy = Date.today();
        Integer year = hoy.year();
        BI_Periodo__c per_y = new BI_Periodo__c(
        	Name='FY',
            BI_FVI_Activo__c=true,
            FO_Type__c='Año',
            BI_FVI_Fecha_Inicio__c=Date.newInstance(year,1,1),
            BI_FVI_Fecha_Fin__c=Date.newInstance(year,12, 31)
        );
        lst_per.add(per_y);
        //Hacemos siempre el último caso del año, asi sabemos que siempre va a estar activo
        BI_Periodo__c per_q = new BI_Periodo__C(
        	Name='FYQ4',
            BI_FVI_Activo__c=true,
            
            FO_Type__c='Trimestre',
            FO_Secuencia__c=4,
            BI_FVI_Fecha_Inicio__c=Date.newInstance(year,10,1),
            BI_FVI_Fecha_Fin__c=Date.newInstance(year,12, 31)
        );
        lst_per.add(per_q);
        for(Integer i =10;i<=12;i++){
            Integer day= 31;
            //Novembre
            if(i==11)
                day=30;
            BI_Periodo__c per_M= new BI_Periodo__c(
            	Name='FYQM'+i,
                BI_FVI_Activo__c=true,
                FO_Type__c='Mes',
                FO_Secuencia__c=i-9,
                BI_FVI_Fecha_Inicio__c=Date.newInstance(year,i,1),
            	BI_FVI_Fecha_Fin__c=Date.newInstance(year,i, day)
                
            );
            lst_per.add(per_M);
        }
        //AHora van las semanas de diciembre, de 7 en 7 
        for(Integer i=0;i<5;i++){
            BI_Periodo__c per_w = new BI_Periodo__c(
            	Name='FYQM3W'+i,
                FO_Type__c='Semana',
                FO_Secuencia__c=i,
                BI_FVI_Activo__c=true,
                BI_FVI_Fecha_Inicio__c=Date.newInstance(year,12,i*7),
            	BI_FVI_Fecha_Fin__c=Date.newInstance(year,12, i*7+6)
            );
            lst_per.add(per_w);
        }
        BI_Periodo__c per_w5 = new BI_PEriodo__c(
        	Name='FQM3W5',
            FO_Type__c='Semana',
            BI_FVI_Activo__c=true,
            FO_Secuencia__c=5,
            BI_FVI_Fecha_Inicio__c=Date.newInstance(year,12,28),
            BI_FVI_Fecha_Fin__c=Date.newInstance(year,12, 31)
        );
        lst_per.add(per_w5);
        insert(lst_per);
        List<BI_Periodo__c> lst_perUpd = [Select Id,FO_Type__c,BI_FVI_Fecha_Inicio__c from BI_PEriodo__c];
        String Idy ='';
        String QId='';
        String MId='';
        //Sacamos los Id de los que serán jeraquicamente superiores
        for(BI_Periodo__c per: lst_perUpd){
            if(per.FO_Type__c.equals('Año')){
                Idy=per.Id;
            }else if(per.FO_Type__c.equals('Trimestre')){
                QId=per.Id;
            }else if(per.FO_Type__c.equals('Mes')){
                if(per.BI_FVI_Fecha_Inicio__c.month()==12){
                    MId=per.Id;
                }
            }
        }
        //Establecemos la jerarquia y ya que estamos aprvechamos el bucle para crear los Objetivos Peridos
        List<BI_FVI_Nodos_Objetivos__c> lst_noOb = new List<BI_FVI_Nodos_Objetivos__c>();
        List<BI_Objetivo_Comercial__c> mp_obj = new List<BI_Objetivo_Comercial__c>();
        for(BI_PEriodo__c per :lst_perUpd){
            BI_Objetivo_Comercial__c aux = new BI_Objetivo_Comercial__c(
            	BI_Periodo__c=per.Id,
                BI_Objetivo__c=199,
                BI_FVI_Activo__c=true,
                BI_Tipo__c='Cartera',
                recordTypeId=rt_mp.get('BI_Objetivo_Comercial__cBI_FVI_Objetivos_Nodos')
            );
            mp_obj.add(aux);
        }
        insert(mp_obj);
        mp_obj = [Select Id,BI_Periodo__c from BI_Objetivo_Comercial__c ];
        Map<String,String> mp_perObj = new map<String,String>();
        for(BI_Objetivo_Comercial__c objcm :mp_obj){
            mp_perObj.put(objcm.BI_Periodo__c,objcm.Id);
        }
        for(BI_PEriodo__c per :lst_perUpd){
            BI_FVI_Nodos_Objetivos__c nd1Ob=null,nd2Ob=null;
            if(per.FO_Type__c.equals('Trimestre')){
                nd1Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNode[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=300,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
                nd2Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNodeP[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=300,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                    
                );
                per.FO_Periodo_Padre__c=Idy;
            }else if(per.FO_Type__c.equals('Mes')){
                nd1Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNode[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=100,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
                nd2Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNodeP[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=100,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
                per.FO_Periodo_Padre__c=QId;
            }else if(per.FO_Type__c.equals('Semana')){
                nd1Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNode[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=20,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
                nd2Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNodeP[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=20,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
                per.FO_Periodo_Padre__c=Mid;
            }else{
                nd1Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNode[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=300,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
                nd2Ob = new BI_FVI_Nodos_Objetivos__c(
                	BI_FVI_Id_Nodo__c=arr_svrNodeP[0].getId(),
                    BI_FVI_IdPeriodo__c=per.Id,
                    BI_FVI_Objetivo__c=300,
                    BI_FVI_Objetivo_Comercial__c=mp_perObj.get(per.Id),
                    RecordTypeId=rt_mp.get('BI_FVI_Nodos_Objetivos__cBI_OBJ_Nodo_Objetivos_Nodos')
                );
            }
            lst_noOb.add(nd1Ob);
            lst_noOb.add(nd2Ob);
        }
        update(lst_perUpd);
        insert(lst_noOb);
        //Ahora toca las oportunidades, tenemos que ver si se puede meter alguna en f1 directamente o no
        //
        List<Opportunity> lst_opp = new List<Opportunity>();
         Opportunity opp1 = new Opportunity(Name = 'Test',
             CloseDate = Date.newInstance(year, 12, 31),
             BI_Requiere_contrato__c = true,
             Generate_Acuerdo_Marco__c = false,
             AccountId = arr_svrAcc[0].getId(),
             StageName = Label.BI_OpportunityStageF1Ganada,
             BI_Ultrarapida__c = true,
             BI_Duracion_del_contrato_Meses__c = 3,
             BI_Recurrente_bruto_mensual__c = 100,
             BI_Ingreso_por_unica_vez__c = 12,
             BI_Recurrente_bruto_mensual_anterior__c = 1,
             BI_Opportunity_Type__c = 'Producto Standard',
            
             Amount = 23,
             BI_Licitacion__c = 'No', //Manuel Medina
             BI_Country__c = Label.BI_Peru);
        lst_opp.add(opp1);
        Opportunity opptyLocal = new Opportunity(Name = 'Oportunidad Test Local',
            AccountId = arr_svrAcc[0].getId(),
            BI_Country__c = Label.BI_Peru,
            BI_Opportunity_Type__c = 'Digital',
            CloseDate =  Date.newInstance(year, 12, 31),
            StageName = Label.BI_F5DefSolucion,
            BI_Probabilidad_de_exito__c = '25',
            BI_Ingreso_por_unica_vez__c = 50,
            BI_Recurrente_bruto_mensual__c = 10,
            BI_Duracion_del_contrato_Meses__c = 6,
            BI_Licitacion__c='No',
            BI_Plazo_estimado_de_provision_dias__c = 5,
            Amount = 5.8);
        lst_opp.add(opptyLocal);
        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, true, true);
        insert(lst_opp);
        BI_MigrationHelper.disableBypass(mapa);
    }
    /******************************************
     * Author: Javier López Andradas
     * Company:gCTIO
     * Description: to Test the trigger and controller side of the app, the normal flow of
     * 				the application, includes the detail server-controller coverage, depends of
     * 				the data load for the flow.
     * 
     * <date>		<version>		<description>
     * 11/04/2018	1.1				Modification over FO_ModificaFOIController.updateFOI
     * ***************************************/
    @isTest static void testForecast1(){
        BI_FVI_Nodos__c node = [Select Id from BI_FVI_Nodos__c where Name='NCP_TEST' limit 1];
        BI_Periodo__c per = [Select Id from BI_Periodo__c where Name='FQM3W5' limit 1];
        FO_Forecast__c fo = new FO_Forecast__c (
        	FO_Nodo__c=node.Id,
            FO_Periodo__c=per.Id
        );
        insert fo;
        FO_Forecast__c foQ = [Select Id,RecordType.DeveloperName  from FO_Forecast__c limit 1];
        //Comprobamos que se ha puesto el RT correcto en el trigger
        System.assert(foQ.RecordType.DeveloperName.equals('FO_Ejecutivo'));
        List<FO_Forecast_Futuro__c> lst_foFut = [Select Id,FO_PEriodo__c,FO_Periodo__r.Name from FO_Forecast_Futuro__c];
        //Comprobamos que se han añadido los Forecast futuros al FO
        System.debug('ForecastFut:'+lst_foFut);
        System.assert(lst_foFut.size()==5);
        //actualiza losFOI
        FO_ActualizaForecast.actualizaFO(foQ.Id);
        List<FO_Forecast_Item__c> lst_foi = [Select Id from FO_Forecast_Item__c];
        System.assert(lst_foi.size()==1);
        lst_foi.get(0).FO_Tipo_Forecast__c='Commit';
        //Ahora hay que volver a sacar el forecast
        Test.startTest();
        update(lst_foi);
        foQ=[Select Id, FO_Total_Forecast__c, FO_Total_Pipeline__c from FO_Forecast__c limit 1];
        System.assert(foQ.FO_Total_Forecast__c==foQ.FO_Total_Pipeline__c);
        
        FO_ModificaFOIController.updateFOI(lst_foi.get(0).Id,String.valueOf(Date.today().year())+'-12-31','Upside',true,true);
        
        foQ=[Select Id, FO_Total_Forecast__c, FO_Total_Pipeline__c from FO_Forecast__c limit 1];
        System.assert(foQ.FO_Total_Forecast__c==(foQ.FO_Total_Pipeline__c/2));
        String pa = FO_ModificaFOIController.getParent(foQ.Id);
        FO_ModificaFOIController.updateFOI(lst_foi.get(0).Id,String.valueOf(Date.today().year())+'-12-31','Commit',true,true);
        //No tiene padres
        System.assert(pa==null);
        //Enviamos el Forecast del subordinado
        System.assert(FO_DetailRecordPageController.changeStatus(foQ.Id).equals('OK'));
        BI_FVI_Nodos__c nodeP=[Select Id from BI_FVI_Nodos__c where Name='NCS_TEST' limit 1];
        FO_Forecast__c foP = new FO_Forecast__c(
        	FO_Nodo__c=nodeP.Id,
            FO_Periodo__c=per.Id
        );
        insert foP;
        FO_Forecast__c foQP=[Select Id,RecordType.DeveloperName,FO_Numero_Nodos__c from FO_Forecast__c where FO_Nodo__c=:nodeP.Id limit 1];
        
        //Comprobamos que el RT es correcto
        System.assert(foQP.RecordType.DeveloperName.equals('FO_Manager'));
        //Deberia tener un unico nodo hijo
        System.assert(foQP.FO_Numero_Nodos__c==1);
        //Actualiza los forecast hijos
        FO_ActualizaForecast.actualizaFO(foQP.Id);
        foQ = [Select Id,FO_Forecast_Padre__c from FO_Forecast__c where FO_Nodo__c=:node.Id limit 1];
        System.assert(foQ.FO_Forecast_Padre__c.equals(foQP.Id));
        foQP=[Select Id, FO_Total_Forecast__c, FO_Total_Pipeline__c from FO_Forecast__c where FO_Nodo__c=:nodeP.Id  limit 1];
        //Nos hemos subido los valores correctos
        FO_ActualizaForecast.actualizaFO(foQP.Id);
        FO_ModificaFOIController.updateFO(foQ.Id,'Borrador');
		FO_ActualizaForecast.actualizaFO(foQP.Id);
        //Actualizamos un fotFut nos da igual cual
        FO_ForecastChange.changeFore(lst_foFut.get(0).Id,Double.valueOf('2000'),false);
        //Comprobamos que tenemos un fallo
        System.assert(FO_ForecastChange.changeFore(null,Double.valueOf('2000'),false).contains('Error:'));
        //Probamos a eliminar foi
        delete(lst_foi.get(0));
        //Ahora probamos la de la tabla de DEtalles
        List<BI_FVI_Nodos__c> lst_nod = FO_ForecastListControllerApex.getNodos();
        List<BI_Periodo__c> lst_perDet = FO_ForecastListControllerApex.getPeriodos(lst_nod.get(0).Id);
        String perFo = '';
        for(BI_Periodo__c perAux:lst_perDet){
            if(perAux.Name.equals('FQM3W5'))
                perFo=per.Id;
        }
        List<BI_Periodo__c> lst_perFO=FO_ForecastListControllerApex.getPeriodosForecast(perFo);
        List<String> lst_auxPer = new List<String>();
        for(BI_Periodo__c perAux : lst_perFO)
            lst_auxPer.add(perAux.Id);
        List<FO_Forecast__c> lst_foDet = FO_ForecastListControllerApex.getForecasts(perFo, lst_nod.get(0).Id);
        List<FO_Forecast_Futuro__c> lst_foFutDet = FO_ForecastListControllerApex.getAjustes(lst_foDet.get(0).Id);
        FO_ForecastListControllerApex.getOpps(String.valueOf(Date.today().year())+'-01-01', String.valueOf(Date.today().year())+'-12-31', lst_nod.get(0).Id,foQ.ID);
        FO_ForecastListControllerApex.getObjetivo(lst_nod.get(0).Id,String.valueOf(Date.today().year())+'-01-01', String.valueOf(Date.today().year())+'-12-31',new List<String>());
        TEst.stopTest();
    }
    /**************************************************
     * Author:Javier Lopez Andradas
     * Company:gCTIO
     * Description: To coverage standAlone methods
     * 
     * 
     * <date>		<version>		<Description>
     * 06/04/2018	1.1				Added today Period
     * 
     * **********************************************/
    @isTest static void testRetrieveForecast(){
        FO_ForecastListControllerApex.getOppStageName();
        FO_ModificaFOIController.getTipo();
        FO_ModificaFOIController.getEstado();
        FO_Crea_Forecast_cls.getNodes();
        BI_Periodo__c per = new BI_Periodo__c(
        Name='TEST',
        BI_FVI_Fecha_Inicio__c=Date.today(),
        BI_FVI_Fecha_Fin__c=Date.today(),
        FO_Type__c='Semana');
        insert per;
        FO_Crea_Forecast_cls.getPer();
        FO_DetailRecordPageController.returnOppsAtr(false);
        
    }
    @isTEst static void testForecast2(){
        List<BI_FVI_Nodos__c> node = [Select Id,Name from BI_FVI_Nodos__c ];
        BI_Periodo__c per = [Select Id from BI_Periodo__c where Name='FQM3W5' limit 1];
        String IdNodeP ='',IdNode='';
        for(BI_FVI_Nodos__c nod : node){
            if(nod.Name.equals('NCP_TEST')){
                IdNode=nod.Id;
            }else{
                IdNodeP=nod.Id;
            }
        }
        FO_Forecast__c foP = new FO_Forecast__c(
        	FO_Nodo__c=IdNodeP,
            FO_Periodo__c=per.Id
        );
        DataBase.SaveResult svrFOP = DataBAse.insert(foP);
        FO_Forecast__c fo = new FO_Forecast__c(
        	FO_Nodo__c=IdNode,
            FO_Periodo__c=per.Id
        );
        DataBase.saveResult svrFo = Database.insert(fo);
        FO_ActualizaForecast.actualizaFO(svrFo.getId());
        List<FO_Forecast_Item__c>lst_foi = [select Id from FO_Forecast_Item__c];
        FO_ModificaFOIController.getClose(lst_foi.get(0).Id);
        FO_ModificaFOIController.updateFOI(lst_foi.get(0).Id,String.valueOf(Date.today().addYears(1).year())+'-12-31','Upside',true,true);
        
        FO_ModificaFOIController.getState(svrFo.getId());
        //delete(lst_foi.get(0));
        test.startTest();
        FO_ActualizaForecast.actualizaFO(svrFo.getId());
        FO_DetailRecordPageController.changeStatus(svrFo.getId());
        foP.Id=svrFOP.getId();
        foP.FO_Estado__c='Enviado';
        update foP;
        
        FO_DetailRecordPageController.changeStatus(svrFo.getId());
        FO_DetailRecordPageController.changeStatus(foP.Id);
        FO_DetailRecordPageController.checkSubmitted(svrFo.getId());
        FO_DetailRecordPageController.validaSub(svrFo.getId());
        FO_DetailRecordPageController.validaSub(svrFOP.getId());
        FO_DetailRecordPageController.getRT(svrFOP.getId());
        test.stopTest();
            
    }    
    /**********************************************
     * Author:Javier Lopez Andradas
     * Company: gcTIO
     * DEscription: Testing the batch to activate the periods
     * 
     * 
     * ************************************************/
    @isTest static void FO_test_Sch(){
        test.startTest();
        System.schedule('tst_Fore','0 0 0 3 9 ? 2022', new FO_Deactivate_periods_job());
    }
}