/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Alvaro Sevilla
 Company:       Accenture LTDA
 Project:       Subsidios ARG
 Description:Esta clase se creo para incluir metodos a usar en botones javaScript pero cuando
 es necesario se ejecuten saltando las reglas de colaboracion y permisos, esto se logra
 usango without sharing en la definicion de la clase.

 History:

 <Date>               <Author>             <Change Description>
 16/02/2018         Alvaro Sevilla          Initial Version
 ------------------------------------------------------------------------------------------------*/
global without sharing class BI_SUB_Buttons3 {

	 webservice static String cancelarCasoSubsidio(String idCaso){
        String resultado;
        List<Case> lstCaso = new List<Case>();
        try{
        Case caso = new Case();
        caso.Id = idCaso;
        caso.Status = 'Cancelled';
        lstCaso.add(caso);
        //update lstCaso;
        Database.SaveResult[] lstUpCase = Database.update(lstCaso, true);
        for(Database.SaveResult elemGuardado : lstUpCase){
            if(elemGuardado.isSuccess()){
                resultado = 'exito';
            }else{
                for(Database.Error err : elemGuardado.getErrors()){
                    System.debug('Error::::: '+ err.getStatusCode() + ': ' + err.getMessage());
                }
                 resultado = 'error';
            }
        }

        return resultado;

        }catch(Exception e){
            return 'ERROR: '+e;
        }
    }


}