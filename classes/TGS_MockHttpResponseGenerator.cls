@isTest
global class TGS_MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('<?xml version=\'1.0\' encoding=\'UTF-8\'?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><typ:CreateInstanceOutput xmlns:typ="http://atrium.bmc.com/2009/01/instances/types"><instanceId>OI-3ae5637ba6b846e4b222a3e071e66c6c</instanceId><status/><extensions xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/></typ:CreateInstanceOutput></soapenv:Body></soapenv:Envelope>');
        res.setStatusCode(200);
        res.setStatus('OK');
        return res;
    }
}