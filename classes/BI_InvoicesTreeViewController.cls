public with sharing class BI_InvoicesTreeViewController{
     
    public id accId {get;set;}
    public Account accountRecord {get;set;}

    private static Map <String, String> monthNames = new Map <String, String> {'01'=>'Enero', '02'=>'Febrero', '03'=>'Marzo', '04'=>'Abril', '05'=>'Mayo', '06'=>'Junio', 
                                                                         '07'=>'Julio', '08'=>'Agosto', '09'=>'Septiembre', '10'=>'Octubre', '11'=>'Noviembre', '12'=>'Diciembre'};

    public static list<string> initialLevelsToQuery = new list<string>{'Level0', 'Level1', 'Level2', 'Level3'};

    public List<SelectOption> displayTypes {get;set;}
    public String selectedDisplayType {get;set;}
    public List<Level0> topLevelItems {get;set;}

    public User currentUser {get;set;}
    public Boolean isPortalUser {get;set;}

    public BI_InvoicesTreeViewController() {
        try{
            User u = [select LocaleSidKey, AccountId from User where id = :UserInfo.getUserId()];
            currentUser = u;
            isPortalUser = u.AccountId != null;

            if (u.AccountId != null) {
                accId = BI_AccountHelper.getCurrentAccountId();
            } else {
                if (ApexPages.currentpage().getparameters().get('id') != null) {
                    accId = String.escapeSingleQuotes(ApexPages.currentpage().getparameters().get('id'));
                }
            }

            if (accId != null) {
                accountRecord = [select Name, CurrencyIsoCode from Account where id = :accId];
            }
            
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_InvoicesTreeViewController.BI_InvoicesTreeViewController', 'BI_EN', Exc, 'Class');
        }

        generateDisplayTypes();
        selectedDisplayType = 'FCIC';

        fetchInvoices();
    }

    private void generateDisplayTypes() {
        displayTypes = new List<SelectOption>();
        displayTypes.add(new SelectOption('FCIC', 'Recurrente'));
        displayTypes.add(new SelectOption('FCON', 'No recurrente'));
    }

    public void fetchInvoices(){
            topLevelItems = new List<Level0>();

            Map<String, Level0> level0Map = new Map<String, Level0>();
            Map<String, Level1> level1Map = new Map<String, Level1>();
            Map<String, Level2> level2Map = new Map<String, Level2>();
            Map<String, Level3> level3Map = new Map<String, Level3>();

            Map<String, Level0> level0DateMap = new Map<String, Level0>();
            Map<String, Level1> level1DateMap = new Map<String, Level1>();
            Map<String, Level2> level2DateMap = new Map<String, Level2>();
            Map<String, Level3> level3DateMap = new Map<String, Level3>();

            List<BI_Cobranza__c> invoiceList = [Select  Id, Aggregation_Level__c, BI_YearMonth__c, 
                                                    BI_ID_Fiscal__c, BI_Rama__c, BI_Familia_1__c, 
                                                    BI_Familia_2__c, BI_Tipo_Facturacion__c, BI_FCIC_Amount__c, BI_FCON_Amount__c,
                                                    BI_Codigo_concepto__c, BI_Descripcion_concepto__c,
                                                    Aggregation_Type__c
                                             from   BI_Cobranza__c 
                                            where   BI_Clientes__c=:accId and Aggregation_Level__c in:initialLevelsToQuery and Active__c=true and 
                                                    Aggregation_Type__c IN ('1-3M', '4-13M') and
                                                    BI_Tipo_Facturacion__c = :selectedDisplayType
                                         order by   Aggregation_Level__c Asc, BI_YearMonth__c desc, BI_Rama__c Asc, BI_Familia_1__c Asc, BI_Familia_2__c Asc, BI_Tipo_Facturacion__c Asc ];

            for(BI_Cobranza__c invRecord : invoiceList){
    
                String InvDate = getDateKey(invRecord.BI_YearMonth__c);
                String InvDatem1 = getPreviousMonthKey(invRecord.BI_YearMonth__c, -1);
                String InvDatem2 = getPreviousMonthKey(invRecord.BI_YearMonth__c, -2);
                String InvRama = invRecord.BI_Rama__c;
                String InvFam1 = invRecord.BI_Familia_1__c;
                String InvFam2 = invRecord.BI_Familia_2__c;

                if(invRecord.Aggregation_Level__c=='Level0'){
                    Level0 l = new Level0(invRecord, InvDate, level0DateMap.get(InvDatem1), level0DateMap.get(InvDatem2));
                    level0Map.put(InvDate, l);
                    level0DateMap.put(invRecord.BI_YearMonth__c, l);
                }

                if(invRecord.Aggregation_Level__c=='Level1'){
                    Level0 l = level0Map.get(InvDate);

                    if (l != null) {
                        Level1 lOne = new Level1(invRecord, InvRama, level1DateMap.get(InvRama+InvDatem1), level1DateMap.get(InvRama+InvDatem2));
                        level1Map.put(InvDate+InvRama, lOne);
                        level1DateMap.put(InvRama+invRecord.BI_YearMonth__c, lOne);
                    }
                }

                if(invRecord.Aggregation_Level__c=='Level2'){
                    Level1 l = level1Map.get(InvDate+InvRama);

                    if (l != null) {
                        Level2 lTwo = new Level2(invRecord, InvFam1, level2DateMap.get(InvRama+InvFam1+InvDatem1), level2DateMap.get(InvRama+InvFam1+InvDatem2));
                        level2Map.put(InvDate+InvRama+InvFam1, lTwo);
                        level2DateMap.put(InvRama+InvFam1+invRecord.BI_YearMonth__c, lTwo);
                    }
                }

                if(invRecord.Aggregation_Level__c=='Level3'){
                    Level2 l = level2Map.get(InvDate+InvRama+InvFam1);

                    if (l != null) {
                        Level3 lThree = new Level3(invRecord, InvFam2, level3DateMap.get(InvRama+InvFam1+InvFam2+InvDatem1), level3DateMap.get(InvRama+InvFam1+InvFam2+InvDatem2));
                        level3Map.put(InvDate+InvRama+InvFam1+InvFam2, lThree);
                        level3DateMap.put(InvRama+InvFam1+InvFam2+invRecord.BI_YearMonth__c, lThree);
                    }
                }                                                        
            }

        
            for(BI_Cobranza__c invRecord : invoiceList){
    
                String InvDate = getDateKey(invRecord.BI_YearMonth__c);
                String InvDatem1 = getPreviousMonthKey(invRecord.BI_YearMonth__c, -1);
                String InvDatem2 = getPreviousMonthKey(invRecord.BI_YearMonth__c, -2);
                String InvRama = invRecord.BI_Rama__c;
                String InvFam1 = invRecord.BI_Familia_1__c;
                String InvFam2 = invRecord.BI_Familia_2__c;

                if(invRecord.Aggregation_Level__c=='Level0'){

                    Level0 l = new Level0(invRecord, InvDate, level0DateMap.get(InvDatem1), level0DateMap.get(InvDatem2));
                    topLevelItems.add(l);
                    level0Map.put(InvDate, l);
                    level0DateMap.put(invRecord.BI_YearMonth__c, l);
                }

                if(invRecord.Aggregation_Level__c=='Level1'){
                    Level0 l = level0Map.get(InvDate);

                    if (l != null) {
                        Level1 lOne = new Level1(invRecord, InvRama, level1DateMap.get(InvRama+InvDatem1), level1DateMap.get(InvRama+InvDatem2));
                        l.addChild(lOne);
                        level1Map.put(InvDate+InvRama, lOne);
                        level1DateMap.put(InvRama+invRecord.BI_YearMonth__c, lOne);
                    }
                }

                if(invRecord.Aggregation_Level__c=='Level2'){
                    Level1 l = level1Map.get(InvDate+InvRama);

                    if (l != null) {
                        Level2 lTwo = new Level2(invRecord, InvFam1, level2DateMap.get(InvRama+InvFam1+InvDatem1), level2DateMap.get(InvRama+InvFam1+InvDatem2));
                        l.addChild(lTwo);
                        level2Map.put(InvDate+InvRama+InvFam1, lTwo);
                        level2DateMap.put(InvRama+InvFam1+invRecord.BI_YearMonth__c, lTwo);
                    }
                }

                if(invRecord.Aggregation_Level__c=='Level3'){
                    Level2 l = level2Map.get(InvDate+InvRama+InvFam1);

                    if (l != null) {
                        Level3 lThree = new Level3(invRecord, InvFam2, level3DateMap.get(InvRama+InvFam1+InvFam2+InvDatem1), level3DateMap.get(InvRama+InvFam1+InvFam2+InvDatem2));
                        l.addChild(lThree);
                        level3Map.put(InvDate+InvRama+InvFam1+InvFam2, lThree);
                        level3DateMap.put(InvRama+InvFam1+InvFam2+invRecord.BI_YearMonth__c, lThree);
                    }
                }                                                            
            }                                          
    }

    @RemoteAction
    public static List<Level4> getDetails(string accountID, string InvDate, string InvRama, string InvFam1, string InvFam2, string tipoFacturacion) {
        List<Level4> returnList = new List<Level4>();


        list<string> tList = new list<string>();
        if (tipoFacturacion != null) {
            tList.add(tipoFacturacion);
        }

        string prev1 = BI_InvoicesTreeViewController.getPreviousMonthKey(InvDate, -1);
        string prev2 = BI_InvoicesTreeViewController.getPreviousMonthKey(InvDate, -2);

        List<BI_Cobranza__c> tempList = [Select     Aggregation_Level__c, BI_YearMonth__c, 
                                                    BI_ID_Fiscal__c, BI_Rama__c, BI_Familia_1__c, 
                                                    BI_Familia_2__c, BI_FCIC_Amount__c, BI_FCON_Amount__c,
                                                    BI_Codigo_concepto__c, BI_Descripcion_concepto__c,
                                                    BI_Currency_ISO__c
                                             from   BI_Cobranza__c 
                                            where   BI_Clientes__c=:accountID and Aggregation_Level__c = 'Level4' and Active__c=true and 
                                                    BI_Tipo_Facturacion__c IN :tList and
                                                    Aggregation_Type__c IN ('1-3M', '4-13M') and
                                                    BI_Rama__c = :InvRama and
                                                    BI_Familia_1__c = :InvFam1 and
                                                    BI_Familia_2__c = :InvFam2 and
                                                    (BI_YearMonth__c = :InvDate or BI_YearMonth__c = :prev1 or  BI_YearMonth__c = :prev2)
                                         order by   Aggregation_Level__c Asc, BI_YearMonth__c Asc, BI_Rama__c Asc, BI_Familia_1__c Asc, BI_Familia_2__c Asc, BI_Tipo_Facturacion__c Asc ];
                                         

        map<string, Level4> levelMap = new map<string, Level4>();

        for (BI_Cobranza__c bic : tempList) {
            Level4 lFour = new Level4(bic, '', null, null);
            levelMap.put(bic.BI_Codigo_concepto__c+bic.BI_YearMonth__c, lFour);
        }

        for (BI_Cobranza__c bic : tempList) {
            if (bic.BI_YearMonth__c == InvDate) {
                returnList.add(new Level4(bic, '', levelMap.get(bic.BI_Codigo_concepto__c+prev1), levelMap.get(bic.BI_Codigo_concepto__c+prev2)));    
            }
        }

        return returnList;
    }

    private static String getDateKey(String DWHDate){
        try {
            return(monthNames.get(DWHDate.substring(4,6)) + ' - ' + DWHDate.substring(0,4));

        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_InvoicesTreeViewController.getDateKey', 'BI_EN', Exc, 'Class');
           return null;
        } 
    }

    public static String getPreviousMonthKey(string dateString, integer months) {
        try {
            Date d = Date.newinstance(Integer.valueOf(dateString.substring(0,4)), Integer.valueOf(dateString.substring(4,6)), 1);
            d = d.addMonths(months);

            String sMonth;
            if (d.month() > 9) {
                sMonth = String.valueOf(d.month());
            } else {
                sMonth = '0'+String.valueOf(d.month());
            }

            return ''+d.year()+sMonth;

        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_InvoicesTreeViewController.getDateKey', 'BI_EN', Exc, 'Class');
           return null;
        } 
    }

    public String getDynamicFormatString() {
        Decimal value = 1000.10;
        String formattedValue = value.format();
        String thousandSep = formattedValue.substring(1,2);
        String decimalSep = formattedValue.substring(5,6);
        return '{0,number,########000}';
    }

    public virtual class LineItem {
        public List<BI_Cobranza__c> records {get;set;}
        public BI_Cobranza__c record {get;set;}
        public String levelName {get;set;}
    }

    public class Level0 extends LineItem {
        public List<Level1> children {get;set;}
        public Decimal totalFCIC {get;set;}
        public Decimal totalFCON {get;set;}
        public Decimal totalBOTH {get;set;}
        public Level0 minus1Amount {get;set;}
        public Level0 minus2Amount {get;set;}

        public Level0(BI_Cobranza__c r, String lt, Level0 m1, Level0 m2) {
            records = new List<BI_Cobranza__c>();
            records.add(r);
            record = r;
            levelName = lt;
            children = new List<Level1>();
            totalFCIC = r.BI_FCIC_Amount__c;
            totalFCON = r.BI_FCON_Amount__c;
            totalBOTH = (r.BI_FCIC_Amount__c == null ? 0 : r.BI_FCIC_Amount__c) + (r.BI_FCON_Amount__c == null ? 0 : r.BI_FCON_Amount__c);
            minus1Amount = m1;
            minus2Amount = m2;
        }

        public void addChild(Level1 l) {
            children.add(l);
        }
    }

    public class Level1 extends LineItem  {
        public List<Level2> children {get;set;}
        public Decimal totalFCIC {get;set;}
        public Decimal totalFCON {get;set;}
        public Decimal totalBOTH {get;set;}
        public Level1 minus1Amount {get;set;}
        public Level1 minus2Amount {get;set;}

        public Level1(BI_Cobranza__c r, String lt, Level1 m1, Level1 m2) {
            records = new List<BI_Cobranza__c>();
            records.add(r);
            record = r;
            levelName = lt;
            children = new List<Level2>();
            totalFCIC = r.BI_FCIC_Amount__c;
            totalFCON = r.BI_FCON_Amount__c;
            totalBOTH = (r.BI_FCIC_Amount__c == null ? 0 : r.BI_FCIC_Amount__c) + (r.BI_FCON_Amount__c == null ? 0 : r.BI_FCON_Amount__c);
            minus1Amount = m1;
            minus2Amount = m2;
        }

        public void addChild(Level2 l) {
            children.add(l);
        }
    }

    public class Level2 extends LineItem  {
        public List<Level3> children {get;set;}
        public Decimal totalFCIC {get;set;}
        public Decimal totalFCON {get;set;}
        public Decimal totalBOTH {get;set;}
        public Level2 minus1Amount {get;set;}
        public Level2 minus2Amount {get;set;}

        public Level2(BI_Cobranza__c r, String lt, Level2 m1, Level2 m2) {
            records = new List<BI_Cobranza__c>();
            records.add(r);
            record = r;
            levelName = lt;
            children = new List<Level3>();
            totalFCIC = r.BI_FCIC_Amount__c;
            totalFCON = r.BI_FCON_Amount__c;
            totalBOTH = (r.BI_FCIC_Amount__c == null ? 0 : r.BI_FCIC_Amount__c) + (r.BI_FCON_Amount__c == null ? 0 : r.BI_FCON_Amount__c);
            minus1Amount = m1;
            minus2Amount = m2;
        }

        public void addChild(Level3 l) {
            children.add(l);
        }
    }

    public class Level3 extends LineItem  {
        public List<Level4> children {get;set;}
        public String codigoConcepto {get;set;}
        public String descripcionConcepto {get;set;}
        public Decimal totalFCIC {get;set;}
        public Decimal totalFCON {get;set;}
        public Decimal totalBOTH {get;set;}
        public String yearMonth {get;set;}
        public String rama {get;set;}
        public String famOne {get;set;}
        public String famTwo {get;set;}
        public String aggregationType {get;set;}
        public Level3 minus1Amount {get;set;}
        public Level3 minus2Amount {get;set;}

        public Level3(BI_Cobranza__c r, String lt, Level3 m1, Level3 m2) {
            records = new List<BI_Cobranza__c>();
            records.add(r);
            record = r;
            codigoConcepto = r.BI_Codigo_concepto__c;
            descripcionConcepto = r.BI_Descripcion_concepto__c;
            totalFCIC = r.BI_FCIC_Amount__c;
            totalFCON = r.BI_FCON_Amount__c;
            totalBOTH = (r.BI_FCIC_Amount__c == null ? 0 : r.BI_FCIC_Amount__c) + (r.BI_FCON_Amount__c == null ? 0 : r.BI_FCON_Amount__c);
            children = new List<Level4>();
            yearMonth = r.BI_YearMonth__c;
            rama = r.BI_Rama__c;
            famOne = r.BI_Familia_1__c;
            famTwo = r.BI_Familia_2__c;
            aggregationType = r.Aggregation_Type__c;
            minus1Amount = m1;
            minus2Amount = m2;

            levelName = lt;
        }
    }

    public class Level4 extends LineItem  {
        public String codigoConcepto {get;set;}
        public String descripcionConcepto {get;set;}
        public Decimal totalFCIC {get;set;}
        public Decimal totalFCON {get;set;}
        public Decimal totalBOTH {get;set;}
        public Level4 minus1Amount {get;set;}
        public Level4 minus2Amount {get;set;}
        public String currencyType {get;set;}

        public Level4(BI_Cobranza__c r, String lt, Level4 m1, Level4 m2) {
            records = new List<BI_Cobranza__c>();
            records.add(r);
            record = r;
            codigoConcepto = r.BI_Codigo_concepto__c;
            descripcionConcepto = r.BI_Descripcion_concepto__c;
            totalFCIC = r.BI_FCIC_Amount__c;
            totalFCON = r.BI_FCON_Amount__c;
            totalBOTH = (r.BI_FCIC_Amount__c == null ? 0 : r.BI_FCIC_Amount__c) + (r.BI_FCON_Amount__c == null ? 0 : r.BI_FCON_Amount__c);
            minus1Amount = m1;
            minus2Amount = m2;
            currencyType = r.BI_Currency_ISO__c;

            levelName = lt;
        }
    }
}