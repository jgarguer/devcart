public class ADQ_TipoCambioController {

	private List<List<TipoCambio__c>> matrizHistorial;
	private List<List<TipoCambio__c>> matrizCuentasBack;
	private List<TipoCambio__c> lh  = new List<TipoCambio__c>();
	private List<TipoCambio__c> listaHistorial = new List<TipoCambio__c>();
	private List<TipoCambio__c> listaHistorialNext = new List<TipoCambio__c>();
	
	private Map<String, String> mapTiposCambio = new Map<String, String>();
	public String cuentaSeleccionada {get; set;}
	public Boolean isTest = false;

	public String valueUSD {get; set;}
	public String valueEUR {get; set;}

	public TipoCambio__c tipoCamnio = new TipoCambio__c ();

	Integer next = 10, count = 10, totalRegistros = 0, indiceMatriz = 0;
    boolean shownext, showprev, listaGenerada;
    String labelTabla = '';
	 
	public ADQ_TipoCambioController(ApexPages.StandardController stdController){ 
	
	}

	public void insertDivisa (){
		TipoCambio__c tipoCamnioUSD = new TipoCambio__c ();
		TipoCambio__c tipoCamnioEUR = new TipoCambio__c ();
		Double montoUSD = 0;
		Double montoEUR = 0;
		if (valueUSD != '' ){
			montoUSD = Double.valueOf(valueUSD);

			tipoCamnioUSD.CodigoDivisa__c  = 'USD';
			tipoCamnioUSD.ConversionRate__c  = montoUSD;
			tipoCamnioUSD.Usuario__c   = UserInfo.getUserId();
			tipoCamnioUSD.CurrencyIsoCode  =  'USD';
			tipoCamnioUSD.FechaCreacion__c  = String.valueOf(Date.today());
			tipoCamnioUSD.Factor__c  = 1/montoUSD;
			tipoCamnioUSD.API__c = 'APEX';

			insert tipoCamnioUSD;

			//START CRM 09/05/2017 commented not used lines 
			//CurrencyType currencyUSD = new CurrencyType ();
			//	currencyUSD.ConversionRate = montoUSD;
			//	currencyUSD.Id = '01Lw0000000GYeN';
			//END CRM 09/05/2017 commented not used lines

				//insert currencyUSD;

		}

		if (valueEUR != '' ){
			montoEUR = Double.valueOf(valueEUR);

			tipoCamnioEUR.CodigoDivisa__c  = 'EUR';
			tipoCamnioEUR.ConversionRate__c  = montoEUR;
			tipoCamnioEUR.Usuario__c   = UserInfo.getUserId();
			tipoCamnioEUR.CurrencyIsoCode  =  'EUR';
			tipoCamnioEUR.FechaCreacion__c  = String.valueOf( Date.today() );
			tipoCamnioEUR.Factor__c  = 1/montoEUR;
			tipoCamnioEUR.API__c = 'APEX';

			insert tipoCamnioEUR;

			//START CRM 09/05/2017 commented not used lines
			//CurrencyType currencyEUR = new CurrencyType ();
			//	currencyEUR.ConversionRate = montoUSD;
			//	currencyEUR.Id = '01Lw0000000GQFb';
			//END CRM 09/05/2017 commented not used lines

				// currencyEUR;

		}

	}
	
	public void refresh(){
		next = count;
		totalRegistros = 0;
		shownext = false;
		showprev = false;
		generarListas();
	}
	
	public List<TipoCambio__c> getListaHistorialNext(){
		if(listaGenerada != true)
			generarListas();
		return listaHistorialNext;
	}
	
	public void generarListas(){
		try{
			labelTabla = '';
			listaHistorial = new List<TipoCambio__c>();
			listaHistorialNext = new List<TipoCambio__c>();
			matrizHistorial = new List<List<TipoCambio__c>>();
			Integer limiteRegistros = (isTest)?100:5000;
			
			List<TipoCambio__c> listaTiposCambio = new List<TipoCambio__c>();
			for(TipoCambio__c ltc : [Select n.Id, n.Name, n.Factor__c,n.FechaCreacion__c, n.ultima_modificacion__c, n.Usuario__c, n.Usuario__r.Name, n.CurrencyIsoCode, n.ConversionRate__c, n.CodigoDivisa__c From TipoCambio__c n order by n.Name desc limit :limiteRegistros]){
				listaTiposCambio.add(ltc);
			}
			matrizHistorial.add(listaTiposCambio);
			
			if(matrizHistorial.size() > 0)listaHistorial = matrizHistorial[0];
			totalRegistros = listaHistorial.size();
			if(listaHistorial.size() > count){
                for(Integer i=0; i<count; i++){
                	listaHistorialNext.add(listaHistorial[i]);
                	//labelTabla += i + ',';
                }
                shownext = true;
                labelTabla = 'Registros 1 - ' + count + ' de ' + totalRegistros;
            }
            else{
            	listaHistorialNext = listaHistorial;
            	labelTabla = 'Registros 1 - ' + listaHistorial.size();// + ' - size' + ' - matrizSitiosSize: ' + matrizCuentas.size();
            }//
            if(listaHistorialNext.size() > 0)listaGenerada = true;
			
		}
		catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			System.debug('Exception: '+e);
		}
	}
	
	public void Next(){
        try{
            showprev = true;
            listaHistorialNext.clear();
            Integer limit1 = 0;
    		
            if(next+count < listaHistorial.size())
                limit1 = next+count;
            else{
                limit1 = listaHistorial.size();
                
            }
                
            for(Integer i=next; i<limit1; i++){
            	listaHistorialNext.add(listaHistorial[i]);
            }
            shownext = (listaHistorialNext.size() < count)?false:true;

    		Integer x = matrizHistorial[0].size();
            labelTabla = 'Registros ' + (next+1) + ' - ' + limit1 + ' de ' + totalRegistros;
            //labelTabla += (cuentaSeleccionada != null && cuentaSeleccionada != 'Todas')?' de ' + totalSitiosOppSeleccionada + ' - ' + mapCuentas.get(cuentaSeleccionada):'';
            next+=count;
        }
        catch(Exception e){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			System.debug('Exception: '+e);
        }
    }
    
    public void Prev(){
        try{
            shownext = true;
            listaHistorialNext.clear();
            Integer limit1 = 0;   
                 
            if(next-(count+count) > 0)
                limit1 = next-count;
            else{
                limit1 = next-count; 
            }
    
            for(Integer i=next-(count+count); i<limit1; i++)    
            	listaHistorialNext.add(listaHistorial[i]);
            	
            
            
            Integer x = matrizHistorial[0].size();
            labelTabla = 'Registros  ' + ((next-(count+count))+1) + ' - ' + limit1 + '  de  ' + totalRegistros;
            //labelTabla += (cuentaSeleccionada != null && cuentaSeleccionada != 'Todas')?' de ' + totalSitiosOppSeleccionada + ' - ' + mapCuentas.get(cuentaSeleccionada):'';
            next-=count;
            
            showprev = (next == count)?false:true;
        }
        catch(Exception e){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			System.debug('Exception: '+e);
        }               
    }
	
	public String getFechaHoy(){
		Datetime myDT = Datetime.now();
		String myDate = myDT.format('yyyy-MM-dd HH:mm');
		return myDate;
	}
	
	public Integer getTotalRegistros(){return totalRegistros;}
	
	public Boolean getShownext(){return shownext;}
    
    public Boolean getShowprev(){return showprev;}
    
    public Boolean getListaGenerada(){return listaGenerada;}
    
    public String getLabelTabla(){return labelTabla;}
}