public with sharing class PCA_Util_MultiselectController {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Class for show PCA_Util_MultiselectController Component 
    
    History:
    
    <Date>            <Author>          	<Description>
    02/07/2014        Micah Burgos         Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    // SelectOption lists for public consumption
    public SelectOption[] leftOptions { get; set; }
    public SelectOption[] rightOptions { get; set; }
    
    public boolean isDisable			{get;set;}
    // Parse &-separated values and labels from value and 
    // put them in option
    private void setOptions(SelectOption[] options, String value) {
    	try{
	        options.clear();
	        String[] parts = value.split('&');
	        for (Integer i=0; i<parts.size()/2; i++) {
	            options.add(new SelectOption(EncodingUtil.urlDecode(parts[i*2], 'UTF-8'), 
	              EncodingUtil.urlDecode(parts[(i*2)+1], 'UTF-8')));
	        }
    	}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_Util_MultiselectController.setOptions', 'Portal Platino', Exc, 'Class');
		}
    }
    
    // Backing for hidden text field containing the options from the 
    // left list
    public String leftOptionsHidden { get; set {
    	try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
           leftOptionsHidden = value;
           setOptions(leftOptions, value);
    	}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_Util_MultiselectController.leftOptionsHidden', 'Portal Platino', Exc, 'Class');
		} 
        }
    }
    
    // Backing for hidden text field containing the options from the
    // right list
    public String rightOptionsHidden { get; set {
    	try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
           rightOptionsHidden = value;
           setOptions(rightOptions, value);
    	}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_Util_MultiselectController.rightOptionsHidden', 'Portal Platino', Exc, 'Class');
		}
        }
    }

}