/**
* Avanxo Colombia
* @author           Geraldine Sofía Pérez Montes href=<gperez@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class 
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-08-19      GSPM                    Test Class created 
* @version   2.0    2015-08-25      OAJF                    Mejorar cobertura
*            1.1    23-02-2017      Jaime Regidor           Adding Campaign Values, Adding Catalog Country
					13/03/2017		Marta Gonzalez(Everis)	REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
*************************************************************************************************************/

@isTest
private class BI_COL_Generate_OT_tst {
    
    Public static User                                                  objUsuario = new User();
    public static Account                                   objCuenta;
    public static Account                                   objCuenta2;
    public static Contact                                   objContacto;
    public static Campaign                                  objCampana;
    public static Contract                                  objContrato;
    public static BI_COL_Anexos__c                          objAnexos;
    public static Opportunity                               objOportunidad;
    public static BI_Col_Ciudades__c                        objCiudad;
    public static BI_Sede__c                                objSede;
    public static BI_Punto_de_instalacion__c                objPuntosInsta;
    public static BI_COL_Descripcion_de_servicio__c         objDesSer;
    public static BI_COL_Modificacion_de_Servicio__c        objMS;
    public static BI_COL_Generate_OT_ctr.WrapperMS          objWrapperMS;
    
    public static List <Profile>                            lstPerfil;
    public static List <User>                               lstUsuarios;
    public static List <UserRole>                           lstRoles;
    public static List<Campaign>                            lstCampana;
    public static List<BI_COL_manage_cons__c >              lstManege;
    public static List<BI_COL_Modificacion_de_Servicio__c>  lstMS;
    public static List<BI_COL_Descripcion_de_servicio__c>   lstDS;
    public static List<recordType>                          lstRecTyp;
    public static List<BI_COL_Generate_OT_ctr.WrapperMS>    lstWrapperMS;
    public static List<BI_COL_Anexos__c>                    lstAnexos;

    public static boolean todosMarcados {get;set;}
    public static boolean habilitarEnvio {get;set;}
    public static boolean showPage{get; set;}

    public static Decimal decTotalServiciosMs; 
    public static String idFUN;
    public static Integer newPageIndex;
    public static BI_COL_Homologacion_integraciones__c objHomologacion;

    public static void CrearData() 
    {

        ////perfiles
        //lstPerfil                       = new List <Profile>();
        //lstPerfil                       = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
        //system.debug('datos Perfil '+lstPerfil);
                
        ////usuarios
        ////lstUsuarios = [Select Id FROM User where Pais__c = 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

        ////lstRol
        //lstRoles = new list <UserRole>();
        //lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        ////ObjUsuario
        //objUsuario = new User();
        //objUsuario.Alias = 'standt';
        //objUsuario.Email ='pruebas@test.com';
        //objUsuario.EmailEncodingKey = '';
        //objUsuario.LastName ='Testing';
        //objUsuario.LanguageLocaleKey ='en_US';
        //objUsuario.LocaleSidKey ='en_US'; 
        //objUsuario.ProfileId = lstPerfil.get(0).Id;
        //objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        //objUsuario.UserName ='pruebas@test.com';
        //objUsuario.EmailEncodingKey ='UTF-8';
        //objUsuario.UserRoleId = lstRoles.get(0).Id;
        //objUsuario.BI_Permisos__c ='Sucursales';
        //objUsuario.Pais__c='Colombia';
        //insert objUsuario;
        
        //lstUsuarios = new list<User>();
        //lstUsuarios.add(objUsuario);


            BI_bypass__c objBibypass = new BI_bypass__c();
            objBibypass.BI_migration__c=true;
            insert objBibypass;

            //Roles
            lstRoles                = new List <UserRole>();
            lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
            system.debug('datos Rol '+lstRoles);
            
            //Cuentas
            objCuenta                                       = new Account();      
            objCuenta.Name                                  = 'prueba';
            objCuenta.BI_Country__c                         = 'Argentina';
            objCuenta.TGS_Region__c                         = 'América';
            objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
            objCuenta.CurrencyIsoCode                       = 'COP';
            objCuenta.BI_Fraude__c                          = false;
            objCuenta.AccountNumber                         = '123456';
            //objCuenta.BillingAddress                      = 'dirprueba';
            objCuenta.Phone                                 = '1233421';
            insert objCuenta;
            system.debug('datos Cuenta '+objCuenta);

            //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);
            
            //Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
            objContacto.BI_Country__c                       = 'Argentina';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objCuenta.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
            objContacto.Phone                               = '1234567';
            objContacto.MobilePhone                         = '3104785925';
            objContacto.Email                               = 'pruebas@pruebas1.com';
            objContacto.BI_COL_Direccion_oficina__c         = 'Dirprueba';
            objContacto.BI_COL_Ciudad_Depto_contacto__c     = objCiudad.Id;
            //REING-INI
            //objContacto.BI_Tipo_de_contacto__c          = 'Autorizado';
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
       		objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
            objContacto.BI_Tipo_de_contacto__c          = 'General';
       		//REING_FIN

            Insert objContacto;
            
            //catalogo
            NE__Catalog__c objCatalogo                      = new NE__Catalog__c();
            objCatalogo.Name                                = 'prueba Catalogo';
            objCatalogo.BI_country__c                       = 'Argentina';
            insert objCatalogo; 
            
            //Campaña
            objCampana                                      = new Campaign();
            objCampana.Name                                 = 'Campaña Prueba';
            objCampana.BI_Catalogo__c                       = objCatalogo.Id;
            objCampana.BI_COL_Codigo_campana__c             = 'prueba1';
            objCampana.BI_Country__c                        = 'Argentina';
            objCampana.BI_Opportunity_Type__c               = 'Fijo';
            objCampana.BI_SIMP_Opportunity_Type__c          = 'Alta';
            insert objCampana;
            lstCampana                                      = new List<Campaign>();
            lstCampana                                      = [select Id, Name from Campaign where Id =: objCampana.Id];
            system.debug('datos Cuenta '+lstCampana);
            
            //Tipo de registro
            lstRecTyp                                       = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
            system.debug('datos de FUN '+lstRecTyp);
            
            //Contrato 
            objContrato                                     = new Contract ();
            objContrato.AccountId                           = objCuenta.Id;
            objContrato.StartDate                           =  System.today().addDays(+5);
            objContrato.BI_COL_Formato_Tipo__c              = 'Condiciones Generales';
            objContrato.BI_COL_Presupuesto_contrato__c      = 1300000;
            objContrato.StartDate                           = System.today().addDays(+5);
            objContrato.BI_Indefinido__c                    = 'Si';
            objContrato.BI_COL_Monto_ejecutado__c           = 5.00;
            objContrato.ContractTerm                        = 12;
            objContrato.BI_COL_Duracion_Indefinida__c       = true;
            objContrato.Name                                = 'prueba';
            objContrato.BI_COL_Cuantia_Indeterminada__c     = true;
            insert objContrato;
            
            objContrato.Status                              = 'Activated';
            update objContrato;
            Contract objCont = [select Id, ContractNumber, BI_COL_Saldo_contrato__c, Status from Contract limit 1];

            System.debug('datos Contratos ===>'+objCont);
            
            //FUN
            objAnexos                                       = new BI_COL_Anexos__c();
            objAnexos.Name                                  = 'FUN-0041414';
            objAnexos.RecordTypeId                          = lstRecTyp[0].Id;
            objAnexos.BI_COL_Contrato__c                    = objContrato.Id;
            objAnexos.BI_COL_Estado__c                      = 'Activo';
            insert objAnexos;
            System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
            
            //Configuracion Personalizada
            BI_COL_manage_cons__c objConPer                 = new BI_COL_manage_cons__c();
            objConPer.Name                                  = 'Viabilidades';
            objConPer.BI_COL_Numero_Viabilidad__c           = 46;
            objConPer.BI_COL_ConsProyecto__c                = 1;
            objConPer.BI_COL_ValorConsecutivo__c            =1;
            lstManege                                       = new List<BI_COL_manage_cons__c >();        
            lstManege.add(objConPer);
            insert lstManege;
            System.debug('======= configuracion personalizada ======= '+lstManege);
            
            //Oportunidad
            objOportunidad                                          = new Opportunity();
            objOportunidad.Name                                     = 'prueba opp';
            objOportunidad.AccountId                                = objCuenta.Id;
            objOportunidad.BI_Country__c                            = 'Argentina';
            objOportunidad.CloseDate                                = System.today().addDays(+5);
            objOportunidad.StageName                                = 'F1 - Closed Won';//'F5 - Solution Definition';
            objOportunidad.CurrencyIsoCode                          = 'COP';
            objOportunidad.Certa_SCP__contract_duration_months__c   = 12;
            objOportunidad.BI_Plazo_estimado_de_provision_dias__c   = 0 ;
            //objOportunidad.OwnerId                                  = lstUsuarios[0].id;
            insert objOportunidad;
            system.debug('Datos Oportunidad'+objOportunidad);
            List<Opportunity> lstOportunidad    = new List<Opportunity>();
            lstOportunidad                      = [select Id from Opportunity where Id =: objOportunidad.Id];
            system.debug('datos ListaOportunida '+lstOportunidad);
            
            
            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);
            
            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name                ='Petuba sedess';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);
            
            //DS
            objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
            objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
            objDesSer.CurrencyIsoCode                           = 'COP';
            insert objDesSer;
            System.debug('Data DS ====> '+ objDesSer);
            System.debug('Data DS ====> '+ objDesSer.Name);
            lstDS   = [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
            System.debug('Data lista DS ====> '+ lstDS);
            
            objHomologacion = new BI_COL_Homologacion_integraciones__c();
            objHomologacion.BI_COL_Viaja_TRS__c = true;
            insert objHomologacion;

            //MS
            objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
            objMS.BI_COL_FUN__c                         = objAnexos.Id;
            objMS.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
            objMS.BI_COL_Bloqueado__c                   = false;
            objMS.BI_COL_Estado__c                      = 'Activa';
            objMS.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objMs.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            objMs.BI_COL_Medio_Preferido__c             = 'Cobre';
            objMS.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
            objMS.BI_COL_TRM__c                         = 5;
            objMS.BI_COL_Producto_Anterior__c           = objHomologacion.Id; 
            objMS.BI_COL_Estado_orden_trabajo__c        = 'Fallido';
            insert objMS;
            system.debug('Data objMs ===>'+objMs);
            BI_COL_Modificacion_de_Servicio__c objr = [select BI_COL_Monto_ejecutado__c from BI_COL_Modificacion_de_Servicio__c where id =: objMS.id];
            system.debug('\n@-->Data consulta ===>'+objr);  
        
    } 
    
    
    public static testMethod void validarFraude() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.validarFraude();  
            Test.stopTest();
        }
    }

    
    public static testMethod void action_Regresar() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.action_Regresar();    
            Test.stopTest();
        }
    }

    public static testMethod void ActualizarDS() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Set<Id> setId = new Set<Id>();

            setId.add(objDesSer.Id);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr.ActualizarDS(setId); 
            Test.stopTest();
        }
    }

    public static testMethod void validarMsEnvio() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);
            lstMS = [select BI_COL_FUN__c, BI_COL_Codigo_unico_servicio__c, BI_COL_Clasificacion_Servicio__c, BI_COL_Oportunidad__c,
                            BI_COL_Bloqueado__c, BI_COL_Estado__c, BI_COL_Sucursal_de_Facturacion__c, BI_COL_Sucursal_Origen__c 
                            from BI_COL_Modificacion_de_Servicio__c limit 1];
            System.debug('@\n-->Lista de datos MS \n'+lstMS);
            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr.validarMsEnvio(lstMS);   
            Test.stopTest();
        }
    }

    public static testMethod void actualizarPresupuesto() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);
            BI_COL_Modificacion_de_Servicio__c objr = [select BI_COL_FUN__c,BI_COL_Codigo_unico_servicio__c,BI_COL_Monto_ejecutado__c,
                                                                BI_COL_Oportunidad__c,BI_COL_Bloqueado__c,BI_COL_Estado__c,BI_COL_Sucursal_de_Facturacion__c,
                                                                BI_COL_Sucursal_Origen__c,BI_COL_Medio_Preferido__c,BI_COL_Clasificacion_Servicio__c,
                                                                BI_COL_TRM__c, BI_COL_Producto_Anterior__r.BI_COL_Viaja_TRS__c from BI_COL_Modificacion_de_Servicio__c where id =: objMS.id];

            lstWrapperMS = new List<BI_COL_Generate_OT_ctr.WrapperMS> ();

            Decimal decVar = 10;
            
            Test.startTest();
            //System.runAs(lstUsuarios[0])
            //{
                ApexPages.StandardController  objStandardController        = new ApexPages.StandardController(objAnexos);
                BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);

                objWrapperMS = new BI_COL_Generate_OT_ctr.WrapperMS (objr, true);
                lstWrapperMS.add(objWrapperMS);
                objcontroller.lstMS = lstWrapperMS; 
                objcontroller.fun = [SELECT Id, 
                                            BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c, 
                                            BI_COL_Contrato__r.BI_COL_Presupuesto_contrato__c,                                  
                                            BI_COL_Contrato__r.BI_Indefinido__c,
                                            BI_COL_Estado__c 
                                            FROM BI_COL_Anexos__c LIMIT 1];//objAnexos;
                objcontroller.getMSSolicitudServicio(objAnexos.id); 
                objcontroller.lstMsDepuradas = lstMS;
                objcontroller.action_envioOT(); 
                objcontroller.actualizarPresupuesto(0.00);

                objcontroller.actualizarPresupuesto(decVar);    
                objAnexos.BI_COL_Contrato__c = objContrato.Id;
                update objAnexos;
            //}
            Test.stopTest(); 
        }
    }

    public static testMethod void actualizarPresupuesto_2() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            
            objHomologacion.BI_COL_Viaja_TRS__c = false;
            update objHomologacion;
            

            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);
            BI_COL_Modificacion_de_Servicio__c objr = [select BI_COL_FUN__c,BI_COL_Codigo_unico_servicio__c,BI_COL_Monto_ejecutado__c,
                                                                BI_COL_Oportunidad__c,BI_COL_Bloqueado__c,BI_COL_Estado__c,BI_COL_Sucursal_de_Facturacion__c,
                                                                BI_COL_Sucursal_Origen__c,BI_COL_Medio_Preferido__c,BI_COL_Clasificacion_Servicio__c,
                                                                BI_COL_TRM__c, BI_COL_Producto_Anterior__r.BI_COL_Viaja_TRS__c from BI_COL_Modificacion_de_Servicio__c where id =: objMS.id];

            lstWrapperMS = new List<BI_COL_Generate_OT_ctr.WrapperMS> ();

            Decimal decVar = 10;
            
            Test.startTest();
            //System.runAs(lstUsuarios[0])
            //{
                ApexPages.StandardController  objStandardController        = new ApexPages.StandardController(objAnexos);
                BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);

                objWrapperMS = new BI_COL_Generate_OT_ctr.WrapperMS (objr, true);
                lstWrapperMS.add(objWrapperMS);
                objcontroller.lstMS = lstWrapperMS; 
                objcontroller.fun = [SELECT Id, 
                                            BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c, 
                                            BI_COL_Contrato__r.BI_COL_Presupuesto_contrato__c,                                  
                                            BI_COL_Contrato__r.BI_Indefinido__c,
                                            BI_COL_Estado__c 
                                            FROM BI_COL_Anexos__c LIMIT 1];//objAnexos;
                
                objMS.BI_COL_Estado__c = 'Pendiente';
                objMS.BI_COL_FUN__c = objcontroller.fun.Id;
                objMS.BI_COL_Estado_orden_trabajo__c = 'Fallido';
                update objMS;

                //objOportunidad.StageName = 'F1 - Closed Won';
                //update objOportunidad;

                //objcontroller.getMSSolicitudServicio(objAnexos.id);
                objcontroller.lstMsDepuradas = lstMS;
                objcontroller.action_envioOT(); 
            //}
            Test.stopTest(); 
        }
    }

    public static testMethod void envioDatosSISGOT() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);
            List<String> lstIdMS = new List<String>();
            lstIdMS.add(objMS.Id);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;  
            BI_COL_Generate_OT_ctr.envioDatosSISGOT(lstIdMS);   
            Test.stopTest();
        }
    }

    public static testMethod void getMSSolicitudServicio() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.getMSSolicitudServicio(objAnexos.id); 
            Test.stopTest();
        }
    }

    public static testMethod void ViewData() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.ViewData();   
            Test.stopTest();
        }
    }

    public static testMethod void BindData() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.BindData(2);  
            Test.stopTest();
        }
    }

    public static testMethod void WrapperMS() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objWrapperMS = new  BI_COL_Generate_OT_ctr.WrapperMS(objms, true);
            Test.stopTest();
        }
    }

    public static testMethod void Buscar() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);
            objMS.BI_COL_Estado__c                      = 'Pendiente';
            update objMS;

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.Buscar(); 
            Test.stopTest();
        }
    }

    public static testMethod void action_seleccionarTodos() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.action_seleccionarTodos();    
            Test.stopTest();
        }
    }

    public static testMethod void getLstMS() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.getLstMS();   
            Test.stopTest();
        }
    }

    public static testMethod void getPageNumber() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.getPageNumber();  
            Test.stopTest();
        }
    }

    public static testMethod void getPageSize() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.getPageSize();    
            Test.stopTest();
        }
    }

    public static testMethod void getPreviousButtonEnabled() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.getPreviousButtonEnabled();   
            Test.stopTest();
        }
    }

    public static testMethod void getNextButtonDisabled() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.getNextButtonDisabled();  
            Test.stopTest();
        }
    }

    public static testMethod void getTotalPageNumber() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.getTotalPageNumber(); 
            Test.stopTest();
        }
    }

    public static testMethod void nextBtnClick() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.nextBtnClick();   
            Test.stopTest();
        }
    }

    public static testMethod void previousBtnClick() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_Generate_OT_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objAnexos) ;
            BI_COL_Generate_OT_ctr objcontroller = new BI_COL_Generate_OT_ctr(objStandardController);   
            objcontroller.previousBtnClick();   
            Test.stopTest();
        }
    }

}