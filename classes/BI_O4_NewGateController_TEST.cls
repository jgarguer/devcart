/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Test class for BI_O4_NewGateController class
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 28/08/2016          Fernando Arteaga           Initial Version
 07/11/2016          Pedro Párraga              Added BI_DynamicFields__c records
 27/07/2017          Javier Almirón García      Extension of test to cover the population of fields via URL developed in BI_O4_NewGateController.cls
 20/09/2017          Antonio Mendivil           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
 25/09/2017          Jaime Regidor              Fields BI_Subsector__c and BI_Sector__c added  
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_NewGateController_TEST {
	
  static{
    BI_TestUtils.throw_exception = false;
  }
	
	@isTest static void test_method_one() {
		
		BI_TestUtils.throw_exception = false;
		RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' Limit 1];
    User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();

		List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'test',
                                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Segment__c = 'Empresas',
                                  BI_Subsector__c = 'test', //25/09/2017
                                  BI_Sector__c = 'test', //25/09/2017
                                  BI_Subsegment_local__c = 'Top 1',
                            	    RecordTypeId = rt1.Id,
                                  BI_Subsegment_Regional__c = 'test',
                                  BI_Territory__c = 'test'
                                  );    
        lst_acc1.add(acc1);
        System.runAs(usu){
        insert lst_acc1;
        }

		List<Account> lst_acc = new List<Account>();       
        Account acc = new Account(Name = 'test',
                                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Segment__c = 'Empresas',
                                  BI_Subsector__c = 'test', //25/09/2017
                                  BI_Sector__c = 'test', //25/09/2017
                                  BI_Subsegment_local__c = 'Top 1',
                                  ParentId = lst_acc1[0].Id,
                                  BI_Subsegment_Regional__c = 'test',
                                  BI_Territory__c = 'test'
                                  );    
        lst_acc.add(acc);
        System.runAs(usu){
        insert lst_acc;
        }

        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

        Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                                          AccountId = lst_acc[0].Id,
                                          BI_Ciclo_ventas__c = Label.BI_Completo, 
                                          RecordTypeId = rt.Id
                                          );    
        insert opp;

		Quote proposal = new Quote();
		proposal.Name = 'Test';
		proposal.OpportunityId = opp.Id;
    proposal.BI_O4_Proposal_FCV_TEF__c = 10;
    proposal.BI_O4_Proposal_FCV_TGS__c = 10;
    proposal.BI_O4_Proposal_iCOST_TEF__c = 10;
    proposal.BI_O4_Proposal_iCOST_TGS__c = 10;
    proposal.BI_O4_Proposal_iCAPEX_TGS__c = 10;
    proposal.BI_O4_Proposal_iCAPEX_TEF__c = 10;
    proposal.BI_O4_Proposal_NRR_TEF__c = 10;
    proposal.BI_O4_Proposal_NRR_TGS__c = 10;
    proposal.BI_O4_Proposal_NRC_TEF__c = 10;
    proposal.BI_O4_Proposal_NRC_TGS__c = 10;
    proposal.BI_O4_Proposal_MRR_TEF__c = 10;
    proposal.BI_O4_Proposal_MRR_TGS__c = 10;
    proposal.BI_O4_Proposal_MRC_TEF__c = 10;
    proposal.BI_O4_Proposal_MRC_TGS__c = 15;

    List <BI_DynamicFields__c> lst_fields = new List <BI_DynamicFields__c>();

    BI_DynamicFields__c field1 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_Proposal__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_Proposal__c'
    );
    lst_fields.add(field1);

    BI_DynamicFields__c field2 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_Opportunity__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_Opportunity__c'
    );
    lst_fields.add(field2);

    BI_DynamicFields__c field3 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_FCV__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_FCV__c'
    );
    lst_fields.add(field3);

    BI_DynamicFields__c field4 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_FCV_Off_Net__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_FCV_Off_Net__c'
    );
    lst_fields.add(field4);

    BI_DynamicFields__c field5 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_Incremental_Cost__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_Incremental_Cost__c'
    );
    lst_fields.add(field5);

    BI_DynamicFields__c field6 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_iCOST_TGS__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_iCOST_TGS__c'
    );
    lst_fields.add(field6);

    BI_DynamicFields__c field7 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_CAPEX__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_CAPEX__c'
    );
    lst_fields.add(field7);

    BI_DynamicFields__c field8 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_CAPEX_TGS__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_CAPEX_TGS__c'
    );
    lst_fields.add(field8);

    BI_DynamicFields__c field9 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_NRR__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_NRR__c'
    );
    lst_fields.add(field9);

    BI_DynamicFields__c field99 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_NRR_TGS__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_NRR_TGS__c'
    );
    lst_fields.add(field99);

    BI_DynamicFields__c field999 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_NRC_TEF__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_NRC_TEF__c'
    );
    lst_fields.add(field999);

    BI_DynamicFields__c field10 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_NRC_TGS__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_NRC_TGS__c'
    );
    lst_fields.add(field10);

    BI_DynamicFields__c field11 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_MRR_TEF__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_MRR_TEF__c'
    );
    lst_fields.add(field11);

    BI_DynamicFields__c field12 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_MRR_TGS__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_MRR_TGS__c'
    );
    lst_fields.add(field12);

    BI_DynamicFields__c field13 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_MRC_TEF__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_MRC_TEF__c'
    );
    lst_fields.add(field13);

    BI_DynamicFields__c field14 = new BI_DynamicFields__c(
      BI_IdCampo__c = 'Test',
      BI_IdCampo_cf__c = 'CFTest',
      BI_KeyPrefix__c = 'pre',
      BI_NombreAPI__c = 'BI_O4_MRC_TGS__c',
      BI_sObject__c = 'BI_O4_Approvals__c',
      Name = 'a6B_BI_O4_MRC_TGS__c'
    );
    lst_fields.add(field14);

    insert lst_fields;

		Test.startTest();
		insert proposal;
		PageReference p = Page.BI_O4_NewGate;
		p.getParameters().put('gate', '2');
		Test.setCurrentPageReference(p);
		ApexPages.StandardController standardC = new ApexPages.StandardController(proposal); 
		BI_O4_NewGateController controller = new BI_O4_NewGateController(standardC);
     	controller.newApproval();
     	System.assertNotEquals(null, proposal);
      p.getParameters().put('gate', '3');
      controller.newApproval();
     	Test.stopTest();
	}
}