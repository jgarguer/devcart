public with sharing class BI_Linea_de_recarterizacionMethods {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that insert the account Id from an external Id
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/02/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void insertIdFromExternalId (List <BI_Linea_de_Recarterizacion__c> news){

		try{

			if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
			}

			

			List <String> exIdToProcess = new List <String>();
			List <BI_Linea_de_Recarterizacion__c> lineasToProcess = new List <BI_Linea_de_Recarterizacion__c>();

			for(BI_Linea_de_Recarterizacion__c linea : news){

				if(linea.BI_Nombre_API_identificador_clientes__c != null && linea.BI_Account_TEMP__c != null){
					exIdToProcess.add(linea.BI_Account_TEMP__c);
					lineasToProcess.add(linea);
				}
			}

			if(!lineasToProcess.isEmpty()){

				//System.debug('#######' + lineasToProcess[0].BI_Account_TEMP__c);

				//System.debug('#######' + lineasToProcess[1].BI_Account_TEMP__c);
				Map <String, Id> map_vf = new Map <String, Id>();
				Map <String, Id> map_idCli = new Map <String, Id>();

				//System.debug('#######' +[SELECT Id, BI_Validador_Fiscal__c, BI_Id_del_cliente__c FROM Account]);

				Account [] lst_acc = [SELECT Id, BI_Validador_Fiscal__c, BI_Id_del_cliente__c FROM Account WHERE BI_Validador_Fiscal__c IN: exIdToProcess OR BI_Id_del_cliente__c IN: exIdToProcess];
				//System.debug('#######' + lst_acc.size());
				if(!lst_acc.isEmpty()){
					for(Account acc : lst_acc){

						if(acc.BI_Validador_Fiscal__c != null){
							map_vf.put(acc.BI_Validador_Fiscal__c, acc.Id);
						}

						if(acc.BI_Id_del_cliente__c != null){
							map_idCli.put(acc.BI_Id_del_cliente__c, acc.Id);
						}

					}

					for(BI_Linea_de_Recarterizacion__c linea : lineasToProcess){

						if(linea.BI_Nombre_API_identificador_clientes__c == 'BI_Validador_Fiscal__c'){

							if(map_vf.containsKey(linea.BI_Account_TEMP__c)){
								linea.BI_AccountId__c = map_vf.get(linea.BI_Account_TEMP__c);
							}
							else{
								linea.addError('El validador fiscal introducido no es válido');
							}
						}
						else if(linea.BI_Nombre_API_identificador_clientes__c == 'BI_Id_del_cliente__c'){

							if(map_idCli.containsKey(linea.BI_Account_TEMP__c)){
								linea.BI_AccountId__c = map_idCli.get(linea.BI_Account_TEMP__c);
							}
							else{
								linea.addError('El id legado introducido no es válido');
							}
						}

					}

				}			
			}
		}catch(Exception exc){
			BI_LogHelper.generate_BILog('BI_Linea_de_recarterizacionMethods.disableValidations', 'BI_EN', exc, 'Trigger');
		}
	}

}