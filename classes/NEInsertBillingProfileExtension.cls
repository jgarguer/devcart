public with sharing class NEInsertBillingProfileExtension {

    Apexpages.StandardController controller; 
    public NE__Billing_Profile__c billProf;
    public String accName						{get;set;}
    public String accSurname 					{get;set;}
    public Boolean isThereAccountId				{get;set;}
     
    public NEInsertBillingProfileExtension(ApexPages.StandardController c){
        controller = c;
        this.billProf = (NE__Billing_Profile__c)controller.getRecord();
        try{
        	
        	/*	3.0 Micol (23-09-2013): if the param of the accountId is null or empty, set the boleean var to false 
        		in order to render the inputField and allow the selection, else set the boolean to true and retrieve the info from a query */
        	if (ApexPages.currentPage().getParameters().get('accId') != '' && ApexPages.currentPage().getParameters().get('accId') != null)
        	{
        		isThereAccountId = true;
                billProf.NE__Account__c = ApexPages.currentPage().getParameters().get('accId');
                Account acc = [SELECT Id, NE__Name__c, NE__Surname__c FROM Account WHERE Id =: billProf.NE__Account__c];
		        accName = acc.NE__Name__c;
		        accSurname = acc.NE__Surname__c;
        	}
        	else
        	{
        		isThereAccountId = false;
        	}

        }
        catch(Exception e){}
        
    }
              
    public PageReference save() {
		if(Controlli())
		{
     		return controller.save();
		}
     	return null;
             /*if (billProf.Id != null){
              try{
                  Integer cc1 = [Select Count () from Order__c where BillingProfId__c =: billProf.id AND OrderStatus__c = 'Completed'];  
                  if (cc1 > 0){ 
                      XMLDVE xmy = new XMLDVE (billProf);
                      String requestXML = xmy.getX(); //Genera XML per la request 
                      IntegrationAdministration__c integrationAdmin=[Select Id,Name__c,Password__c,Username__c,EndPoint__c,CertificateName__c from IntegrationAdministration__c where Name__c='getFulfillment'];
                      GetFulfillmentEntity.GetFulfilment_GetFulfilmentRequest entityReq = new GetFulfillmentEntity.GetFulfilment_GetFulfilmentRequest();
                      entityReq.inputMessage = requestXML;

                        GetFulfillmentService.GetFulfilmentHttpSoap11Endpoint stub = new GetFulfillmentService.GetFulfilmentHttpSoap11Endpoint();
                        if ( integrationAdmin.Password__c != null || integrationAdmin.Username__c != null)
                            stub.inputHttpHeaders_x = new Map<String, String>();
                        if ( integrationAdmin.Username__c != null)
                            stub.inputHttpHeaders_x.put('UserName',integrationAdmin.Username__c);
                        if ( integrationAdmin.Password__c != null )
                            stub.inputHttpHeaders_x.put('Password',integrationAdmin.Password__c);
                        if( integrationAdmin.CertificateName__c != null)
                        {
                            stub.clientCertName_x = integrationAdmin.CertificateName__c;
                        }
                        stub.endpoint_x = integrationAdmin.EndPoint__c;
                        GetFulfillmentEntity.GetFulfilment_GetFulfilmentResponse entityResp = stub.getFulfilment(entityReq);
                      if(entityResp.errorcode == '0'){
                          return controller.save();
                      }
                      else{
                          return null;       
                     }
                 }
                 else{return controller.save();}
            }
            catch(Exception e){  return controller.save();}
           }
           else
                return controller.save();
                
               //invoke standard Save method
          }      
            
            else
                return null;
         */

    }

    
    public Pagereference saveNew(){      
       	this.save();
       	Pagereference next = System.Page.NEInsertBillingProfile;
       	next.getParameters().put('accId', billProf.NE__Account__c);
       	next.setRedirect(true);
		return next;
               
    } 
    
    public Boolean Controlli(){
        Boolean ris=true;
        //Controlli per il pagamento con carta di credito
        if(billProf.NE__Payment__c=='Credit Card'){
                if(billProf.NE__Card_Type__c==''){
                    billProf.NE__Payment__c.adderror(System.Label.NE.CardType);
                    ris=ris&&false;
                }
                else{
                    //Controllo su CSC
                    String str=String.valueOf(billProf.NE__Security_Code__c);
                    try{
                        Integer cvv=Integer.valueOf(str);
                        if(cvv<0){
                            billProf.NE__Security_Code__c.adderror(System.Label.NE.Error_CSC_Billing_Profile);
                            ris=ris&&false;
                        }
                    }
                    catch(Exception e){
                        billProf.NE__Security_Code__c.adderror(System.Label.NE.Error_CSC_Billing_Profile);
                        ris=ris&&false;
                    }
                    if(billProf.NE__Card_Type__c=='American Express'){
                        if(!(str.length()==4)){
                            billProf.NE__Security_Code__c.adderror(System.Label.NE.Error_AmericanCSC_Billing_Profile);
                            ris=ris&&false;
                        }
                    }
                    else if(!(str.length()==3)){
                        billProf.NE__Security_Code__c.adderror(System.Label.NE.Error_CSC3_Billing_Profile);
                        ris=ris&&false;
                    }
               }
               //Controllo sul mese di scadenza
               String stm=String.valueOf(billProf.NE__Expiration__c);
               try{
                   Integer mms=Integer.valueOf(stm);
                   if(mms<1 || mms>=13){
                       billProf.NE__Expiration__c.adderror(System.Label.NE.Error_Month_Billing_Profile);
                       ris=ris&&false;
                   }
               }
               catch(Exception e3){
                   billProf.NE__Expiration__c.adderror(System.Label.NE.Error_Integer_Value);
                   ris=ris&&false;
               }

               //Controllo sull'anno di scadenza
               String st=String.valueOf(billProf.NE__Expiration_year__c);
               try{
                   Integer aas=Integer.valueOf(st);
               }
               catch(Exception e2){
                   billProf.NE__Expiration_year__c.adderror(System.Label.NE.Error_Integer_Value);
                   ris=ris&&false;
               }
               if(!(st.length()==2)){
                   billProf.NE__Expiration_Year__c.adderror(System.Label.NE.Error_Two_Digit);
                   ris=ris&&false;
               }
               else
                   ris=ris&&true;  
               //Controllo sul numero di carta   
               String nc=String.valueOf(billProf.NE__Card_Number__c);
               try{
                   Long ncc=Long.valueOf(nc);
               }
               catch(Exception e3){
                   billProf.NE__Card_Number__c.adderror(System.Label.NE.Error_Numeric_Value);
                   ris=ris&&false;
               }
               if(!(nc.length()==16)){
                   billProf.NE__Card_Number__c.adderror(System.Label.NE.Error_Sixteen_Digit);
                   ris=ris&&false;
               }
               else
                   ris=ris&&true;
                    
              
            }
            //Controlli pagamento automatic debt
            else if(billProf.NE__Payment__c=='Automatic Debt'){
                String ad=billProf.NE__Iban__c;
                if(ad.length()<5){
                    billProf.NE__Iban__c.adderror(System.Label.NE.Error_Five_Char);
                    ris=ris&&false;
                }
                else
                    ris=ris&&true;
            }
            else
                ris=ris&&true;
            return ris;
    }
    
    public String getLabel(){
        if(billProf.NE__Payment__c == 'Credit Card') return 'Credit Card';
        else if(billProf.NE__Payment__c == 'Iban') return 'Iban';
        else if(billProf.NE__Payment__c == 'Rid') return 'Rid';
        else if(billProf.NE__Payment__c == 'Mark') return 'Mark';
        else return null;
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Paper',System.Label.NE.Paper)); 
        options.add(new SelectOption('Mail',System.Label.NE.Email)); 
        return options; 
    }
    
    public List<SelectOption> getPayment() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Credit Card','Credit Card')); 
        options.add(new SelectOption('Automatic Debt','Automatic Debt')); 
        options.add(new SelectOption('Statement','Statement'));
        return options; 
    }
           
}