public with sharing class BI_Campaign_EditPageCtrl {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   BI_EditPage controller class
    Test Class:    BI_Campaign_EditPageCtrl_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    11/06/2014              Ignacio Llorca          Initial Version
    04/07/2014              Pablo Oliva             BI_Campaign_EditPageCtrl updated
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Constructor method.
       
    History: 
    
    <Date>                  <Author>                <Change Description>
    11/06/2014              Ignacio Llorca          Initial Version 
    04/07/2014              Pablo Oliva             BI_Desarrollo Comercial ---> BI_Inteligencia Comercial       
    23/07/2014              Micah Burgos            BI_Ejecutivo de Cliente (Add)
    10/09/2014              Ignacio Llorca          BI_Ejecutivo de Cliente removed
    06/07/2015              Esthiben Mendez         BI_COL_Aplazamiento (Add)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public boolean dir { get; set; }
    public String url { get; set; }
    public Campaign campaignC { get; set; }
    
    
    public BI_Campaign_EditPageCtrl(ApexPages.StandardController camp){
        try{
            //String SysAdmin = 'System Administrator';
            //String SystemAdmin = Label.BI_Administrador;
            //String LocalAdmin = Label.BI_AdministradorLocal;
            //String ComercialDevelop = Label.BI_DesarrolloComercial;
            //String ComercialIntel = Label.BI_InteligenciaComercial;
            //String ClientEj = Label.BI_EjecutivoCliente;
            
            String urlPrefix = '';
            if(Site.getPrefix() != null){
                urlPrefix = Site.getPrefix();
            }
                
            
            campaignC =(Campaign)camp.getRecord();
            
            //String userPermission = BI_UserMethods.getPermission();
            
            //Profile pro = [SELECT Name FROM Profile where Id = :UserInfo.getProfileId() limit 1];
            
            
            
            User usr_info = [SELECT Id,BI_Permisos__c, Pais__c FROM User WHERE Id = :UserInfo.getUserId()];
             System.debug('#### usr_info:  '+ usr_info );

            if(   
               ////PROFILE
               //pro.Name == SysAdmin       ||
               //pro.Name == SystemAdmin        ||
               //pro.Name == LocalAdmin         ||
               //pro.Name == ComercialDevelop   ||
               //pro.Name == ComercialIntel     
               
               //||
               //PERMISSION

               //ADMINS:
               usr_info.BI_Permisos__c == Label.BI_Administrador_del_sistema ||
               usr_info.BI_Permisos__c == Label.BI_SuperUsuario              ||

               //BIEN:
              (usr_info.Pais__c != 'Colombia' && ( usr_info.BI_Permisos__c == Label.BI_Desarrollo_Comercial      ||
              usr_info.BI_Permisos__c == Label.BI_Inteligencia_Comercial ) ) 

              ||

               //COL
              (usr_info.Pais__c == 'Colombia' && ( usr_info.BI_Permisos__c == Label.BI_COL_Jefe_ventas ) )
            
               //userPermission.Pais__c == Label.BI_COL_Aplazamiento          || [Hector COL - 07/10/2015]

               //userPermission == Label.BI_COL_Desarrollo_comercial
               //userPermission == Label.BI_COL_Ejecutivo_cobro     
               //userPermission == Label.BI_COL_Ejecutivo_cliente   
               //userPermission == Label.BI_COL_Jefe_ventas         
               //userPermission == Label.BI_COL_Admin_colombia
               ){
                
                
                dir = true;
                

                url = urlPrefix + '/' + string.valueof(campaignC.Id) + '/e?retURL=' + urlPrefix +'/'+ string.valueof(campaignC.Id) + '&nooverride=1';
            }else{
                dir=false;
                url = urlPrefix + '/' + string.valueof(campaignC.Id);
                
            }
                        
        }catch (exception Exc){
                BI_LogHelper.generate_BILog('PCA_HomeController.BI_Campaign_EditPageCtrl', 'BI_EN', Exc, 'Trigger');
            }
    }
}