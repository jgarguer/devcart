/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:          
* Description:         
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-07-24      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_LiberationApproval_ctr
* @version   1.1    2015-07-29      Daniel Lopez (DL)       Se actualiza la clase para subir su cobertura.
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
                    20/09/2017      Angel F. Santaliestra   Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest(SeeAllData=false)
private with sharing class BI_COL_LiberationApproval_tst 
{
    //INICIO DL 29/07/2015

    public static Account  objCuenta;
    public static Opportunity objoppty; 
    public static Case objCase;
    public static List<RecordType>  rtCaso;
    public static Contact contacto;
    public static User Usuario;
    Public static list <UserRole>       lstRoles;
    public static List<User>    lstUsuarios;

    //DL Metodo para crear la data necesaria para la prueba.
    
    static void crearData()
    {
            //List<Profile> lstPerfil                         = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
     //       //List<User>    lstUsuarios                       = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

     //       //lstRol
     //   lstRoles = new list <UserRole>();
     //   lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

     //   //ObjUsuario
     //   Usuario = new User();
     //   Usuario.Alias = 'standt';
     //   Usuario.Email ='pruebas@test.com';
     //   Usuario.EmailEncodingKey = '';
     //   Usuario.LastName ='Testing';
     //   Usuario.LanguageLocaleKey ='en_US';
     //   Usuario.LocaleSidKey ='en_US'; 
     //   Usuario.ProfileId = lstPerfil.get(0).Id;
     //   Usuario.TimeZoneSidKey ='America/Los_Angeles';
     //   Usuario.UserName ='pruebas@test.com';
     //   Usuario.EmailEncodingKey ='UTF-8';
     //   Usuario.UserRoleId = lstRoles.get(0).Id;
     //   Usuario.BI_Permisos__c ='Sucursales';
     //   Usuario.Pais__c='Colombia';
     //   insert Usuario;
        
     //   lstUsuarios = new list<User>();
     //   lstUsuarios.add(Usuario);

                BI_bypass__c objBibypass = new BI_bypass__c();
                objBibypass.BI_migration__c=true;
                insert objBibypass;

                objCuenta                                       = new Account();
                objCuenta.Name                                  = 'prueba';
                objCuenta.BI_Country__c                         = 'Colombia';
                objCuenta.TGS_Region__c                         = 'América';
                objCuenta.BI_Tipo_de_identificador_fiscal__c    = 'NIT';
                objCuenta.CurrencyIsoCode                       = 'COP';
                objCuenta.BI_Segment__c                         = 'test';
                objCuenta.BI_Subsegment_Regional__c             = 'test';
                objCuenta.BI_Territory__c                       = 'test';
                insert objCuenta;

                objoppty                                        = new Opportunity();
                objoppty.Name                                   = 'prueba opp';
                objoppty.AccountId                              = objCuenta.Id;
                objoppty.BI_Country__c                          = 'Colombia';
                objoppty.CloseDate                              = System.today().addDays(+5);
                objoppty.StageName                              = 'F5 - Solution Definition';
                objoppty.CurrencyIsoCode                        = 'COP';
                objoppty.Certa_SCP__contract_duration_months__c = 12;
                objoppty.BI_Plazo_estimado_de_provision_dias__c = 0 ;
                //objoppty.OwnerId                                = lstUsuarios[0].id;
                insert objoppty;

                contacto                                        = new Contact();
                contacto.LastName                               = 'Test';
                contacto.BI_Country__c                          = 'Colombia';
                contacto.CurrencyIsoCode                        = 'COP'; 
                contacto.AccountId                              = objCuenta.Id;
                contacto.BI_Tipo_de_contacto__c                 = 'Administrador Canal Online';
                //REING-INI
                contacto.BI_Tipo_de_documento__c 			= 'Otros';
                contacto.BI_Numero_de_documento__c 			= '00000000X';
                contacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
                //REING_FIN
                Insert contacto;

                //usuario                                         = new User();
                //usuario                                         = lstUsuarios[0];
                //usuario.Email                                   = 'Test@teste.com';
                //update usuario; 

                rtCaso = [select id from recordType where SobjectType = 'Case' and DeveloperName = 'BI_Caso_Interno'];

                objCase                                         = new Case();
                objCase.AccountId                               = objCuenta.Id;
                objCase.RecordTypeId                            = rtCaso[0].id;
                objCase.BI_Country__c                           = 'Colombia';
                objCase.BI_Department__c                        = 'Jefatura Integración de Proyectos';
                objCase.BI_Type__c                              = 'Solicitud de Project Manager';
                objCase.Status                                  = 'Nuevo';
                objCase.Priority                                = 'Media';
                objCase.Origin                                  = 'Chat';
                objCase.ContactId                               = contacto.id;
                objCase.Subject                                 = 'Test';
                //objCase.OwnerId                                 = lstUsuarios[0].id;
                objCase.BI_Nombre_de_la_Oportunidad__c          = objoppty.Id;
                insert objCase;
                
    }

    static testmethod void myTestMethod1()
    {

        Usuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(Usuario)
        {
            crearData();
            ApexPages.StandardController  sc                = new ApexPages.StandardController(objCase);
            PageReference                 pageRef           = Page.BI_COL_LiberationApproval_pag;
            Test.setCurrentPage(pageRef);           
            Test.startTest();

                pageRef.getParameters().put('Id',string.valueOf(objCase.Id));
                pageRef.getParameters().put('Accion','Cancelar');
                BI_COL_LiberationApproval_ctr liberation = new BI_COL_LiberationApproval_ctr(sc);
                pageRef.getParameters().put('Accion','Aceptar');
                liberation = new BI_COL_LiberationApproval_ctr(sc);
                liberation = new BI_COL_LiberationApproval_ctr(objCase.Id);
                liberation.actualizarCaso();
                liberation.correoAceptacion();
                liberation.correoCancelacion();
                liberation.volver();

            Test.stopTest();
        }

    }

   //FIN DL 29/07/2015    
    
}