/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-04-20      Daniel ALexander Lopez (DL)     Cloned Class
*					19/09/2017		Angel F. Santaliestra			Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private class BI_COL_Associate_opportunity_tst
{
	Public static list<Profile> 											lstPerfil;
	Public static list <UserRole> 											lstRoles;
	Public static User 													objUsuario = new User();
	public static list<Opportunity> lstOppAsociadas                         = new list<Opportunity>();
    public static List<BI_COL_Associate_opportunity_ctr.WrapperOpt> lstWrapper = new List<BI_COL_Associate_opportunity_ctr.WrapperOpt>();
	public static Account 			objCuenta;
    public static Opportunity		objOppty;


	static void crearData()
    {
    	//List<Profile> lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
    	//list<User> lstUsuarios = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

    	//lstPerfil
		lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];

		//lstRol
		lstRoles = new list <UserRole>();
		lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

		//ObjUsuario
		objUsuario = new User();
		objUsuario.Alias = 'standt';
		objUsuario.Email ='pruebas@test.com';
		objUsuario.EmailEncodingKey = '';
		objUsuario.LastName ='Testing';
		objUsuario.LanguageLocaleKey ='en_US';
		objUsuario.LocaleSidKey ='en_US'; 
		objUsuario.ProfileId = lstPerfil.get(0).Id;
		objUsuario.TimeZoneSidKey ='America/Los_Angeles';
		objUsuario.UserName ='pruebas@test.com';
		objUsuario.EmailEncodingKey ='UTF-8';
		objUsuario.UserRoleId = lstRoles.get(0).Id;
		objUsuario.BI_Permisos__c ='Sucursales';
		objUsuario.Pais__c='Colombia';
		insert objUsuario;

		list<User> lstUsuarios = new list<User>();
		lstUsuarios.add(objUsuario);

    	System.runAs(lstUsuarios[0])
    	{
    		BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass;

    		objCuenta 										= new Account();
	        objCuenta.Name 									= 'prueba';
	        objCuenta.BI_Country__c 						= 'Colombia';
	        objCuenta.TGS_Region__c 						= 'América';
	        objCuenta.BI_Tipo_de_identificador_fiscal__c 	= 'NIT';
	        objCuenta.CurrencyIsoCode 						= 'GTQ';
	        objCuenta.BI_Segment__c 						= 'test';
          	objCuenta.BI_Subsegment_Regional__c 			= 'test';
          	objCuenta.BI_Territory__c 						= 'test';
	        insert objCuenta;

	        System.debug('\n\n\n ======= CUENTA ======\n '+ objCuenta );

	        for(Integer i = 0; i < 10; i++)
	        {
		        objOppty 										= new Opportunity();
		        objOppty.Name 									= 'TEST AVANXO OPPTY'+i;
		        objOppty.AccountId 	 							= objCuenta.Id;
		        objOppty.BI_Country__c 							= 'Colombia';
		        objOppty.CloseDate 						 		= System.today().addDays(i);
		        objOppty.StageName 								= 'F6 - Prospecting';
		        objOppty.BI_Ciclo_ventas__c 					= Label.BI_Completo;
	        	lstOppAsociadas.add(objOppty);
	        }
	        
	        System.debug('\n\n\n ======= lstOppyAsociadas ======\n '+ lstOppAsociadas );

	        insert lstOppAsociadas;
      	}
    }
    

	@isTest
	static void TestMethod1()
	{
		crearData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_Associate_opportunity_pag;
        Test.setCurrentPage(pageRef);
		BI_COL_Associate_opportunity_ctr controller         = new BI_COL_Associate_opportunity_ctr();
		BI_COL_Associate_opportunity_ctr.WrapperOpt wrapper = new BI_COL_Associate_opportunity_ctr.WrapperOpt(true, lstOppAsociadas[0]);
		lstWrapper.add(wrapper);
		controller.asociadas = lstOppAsociadas;
        controller.optSeleccionadas = lstWrapper;
		controller.seleccionar();
		controller.cancelar();
		Test.stopTest();
	}

	@isTest
	static void TestMethod2()
	{
		crearData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_Associate_opportunity_pag;
        Test.setCurrentPage(pageRef);
		BI_COL_Associate_opportunity_ctr controller         = new BI_COL_Associate_opportunity_ctr();		
		controller.seleccionar();
		controller.cancelar();
		Test.stopTest();
	}

	@isTest
	static void TestMethod3()
	{
		crearData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_Associate_opportunity_pag;
        Test.setCurrentPage(pageRef);
		BI_COL_Associate_opportunity_ctr controller         = new BI_COL_Associate_opportunity_ctr();
		BI_COL_Associate_opportunity_ctr.WrapperOpt wrapper = new BI_COL_Associate_opportunity_ctr.WrapperOpt(true, new Opportunity());
		lstWrapper.add(wrapper);
		controller.asociadas = lstOppAsociadas;
        controller.optSeleccionadas = lstWrapper;
		controller.seleccionar();
		controller.cancelar();
		Test.stopTest();
	}
}