@isTest
private class PCA_EditContact_PopUp_TEST {

    static testMethod void getLoadInfo() {
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        //insert usr;
        system.debug('usr: '+usr);
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;            
            Event Evento1;
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            system.debug('con: '+con);
            Contact con2 = new Contact(LastName = 'test'+ String.valueOf(2),
	                                          AccountId = acc[0].Id,
	                                          Email = '',
	                                          HasOptedOutOfEmail = false,
                                              BI_Country__c = lst_pais[0]);
			insert con2;	                                          
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            PageReference pageRef = new PageReference('PCA_EditContact_PopUp');
            Test.setCurrentPage(pageRef);
            
            system.runAs(user1){
            	PCA_EditContact_PopUpCtrl constructor = new PCA_EditContact_PopUpCtrl();
            	ApexPages.currentPage().getParameters().put('id',  con[0].Id);
            	ApexPages.currentPage().getParameters().put('Mode',  'Edit');
                 BI_TestUtils.throw_exception = false;
            	 constructor.checkPermissions();
            	 constructor.loadInfo();
            	 constructor.save();
                 system.assertEquals(con[0].Id, ApexPages.currentPage().getParameters().get('id'));
            }
             system.runAs(user1){
            	PCA_EditContact_PopUpCtrl constructor = new PCA_EditContact_PopUpCtrl();
            	ApexPages.currentPage().getParameters().put('id',  con2.Id);
            	ApexPages.currentPage().getParameters().put('Mode',  'Edit');
                 BI_TestUtils.throw_exception = false;
            	 constructor.checkPermissions();
            	 constructor.loadInfo();
            	 constructor.save();
            }
            system.runAs(user1){
            	PCA_EditContact_PopUpCtrl constructor = new PCA_EditContact_PopUpCtrl();
            	ApexPages.currentPage().getParameters().put('id',  con[0].Id);
            	ApexPages.currentPage().getParameters().put('Mode',  'Edit');
                 BI_TestUtils.throw_exception = true;
            	 constructor.checkPermissions();
            }
            system.runAs(user1){
            	PCA_EditContact_PopUpCtrl constructor = new PCA_EditContact_PopUpCtrl();
            	ApexPages.currentPage().getParameters().put('id',  con[0].Id);
            	ApexPages.currentPage().getParameters().put('Mode',  'Edit');
            	constructor.checkPermissions();
            	BI_TestUtils.throw_exception = true;
            	constructor.loadInfo();
            }
            system.runAs(user1){
            	PCA_EditContact_PopUpCtrl constructor = new PCA_EditContact_PopUpCtrl();
            	ApexPages.currentPage().getParameters().put('id',  con[0].Id);
            	ApexPages.currentPage().getParameters().put('Mode',  'Edit');
            	 constructor.checkPermissions();
            	 constructor.loadInfo();
                 //BI_TestUtils.throw_exception = true;
            	 constructor.save();
            	
            }
        }
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}