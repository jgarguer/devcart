/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:       Javier Almirón García
 Company:       New Energy Aborda
 Description:   Batch Code to update CIs - Demanda 615
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 21/01/2018          Javier Almirón García      Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

public with sharing class  BI_O4_Test_UpdateCIs implements Database.Batchable<sObject>, Database.Stateful {

    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    
    static {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType IN ('Opportunity', 'Case')]){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }
    }
    
    
    static Integer recordsProcessed = 0;
    //List <NE__OrderItem__c> idCis = new List <NE__OrderItem__c>();
    static List <NE__OrderItem__c>  ciUpdate = new List <NE__OrderItem__c>();
    static List<NE__OrderItem__c> lst_padres = new List<NE__OrderItem__c>();
    static List<NE__OrderItem__c> lst_hijos = new List<NE__OrderItem__c>();
    static List<NE__OrderItem__c> lst_toUpdate = new List<NE__OrderItem__c>();
    public String query;
    public String query2;
    public String limite;
    public String country;
    public String queryAuxiliar;
    public String querytotal;
    public boolean metodo; //Metodo = False (0) => Actualizar CIs / Método = True (1) => Actualizar Opps
    //public static boolean skipTrigger = true;
    static Database.SaveResult[] records;
    static Database.SaveResult[] recordsOppCopia;
    static Database.SaveResult[] recordsOppOriginal;
    static Database.SaveResult[] recordsCis;
    static Integer successRecords = 0;
    static Integer failedRecords = 0;
    static List<String> lst_errors = new List<String>();

    //Constructor
    public BI_O4_Test_UpdateCIs (){
        metodo = false;
        query = 
        'SELECT id, NE__OrderItem__c.BI_O4_Parent_Qty__c, NE__OrderId__r.NE__OptyId__c, NE__Qty__c, BI_O4_Parent_Qty__c, NE__OrderId__r.NE__OptyId__r.Account.BI_O4_Holding_Account_Category__c, BI_O4_MRC_Product_Qt_Presup__c, BI_Tasa_RecurringChargeOv__c, NE__Qty__c, BI_Tasa_OneTimeFeeOv__c, BI_O4_Type__c, BI_Ingreso_Recurrente_Anterior_Producto__c from NE__OrderItem__c  where NE__OrderId__r.NE__OptyId__r.RecordTypeId = \'012w0000000hzMW\' and NE__OrderId__r.NE__OptyId__r.StageName =   \'F1 - Closed Won\' and (NE__OrderId__r.NE__OptyId__r.BI_O4_MNC_HQ_Corporate_Opp_Leading__c != \'\' OR (NE__OrderId__r.NE__OptyId__r.Account.BI_O4_Holding_Account_Category__c = \'GA\' OR NE__OrderId__r.NE__OptyId__r.Account.BI_O4_Holding_Account_Category__c = \'GBDA\')) and (NE__OrderId__r.NE__OrderStatus__c != \'Completed\' and NE__OrderId__r.NE__OrderStatus__c != \'Revised\')';
        country = '';
        queryAuxiliar = '';
        query2 = '';
        limite = '';
        
        //query = 'SELECT id, NE__OrderItem__c.BI_O4_Parent_Qty__c, NE__OrderId__r.NE__OptyId__c, NE__OrderId__r.NE__OptyId__r.Account.BI_O4_Holding_Account_Category__c, BI_O4_MRC_Product_Qt_Presup__c, BI_Tasa_RecurringChargeOv__c, NE__Qty__c, BI_Tasa_OneTimeFeeOv__c, BI_O4_Type__c, BI_Ingreso_Recurrente_Anterior_Producto__c from NE__OrderItem__c  where Id = \'a0aw000000HMkbyAAD\' LIMIT 1';
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        querytotal = query + country + queryAuxiliar + query2 +limite;
        System.debug ('ABAB querytotal = ' + querytotal);
        return Database.getQueryLocator(querytotal);
    }

    public void execute(Database.BatchableContext bc, List<NE__OrderItem__c> scope){
        // process each batch of records
        //if ( metodo == false) {updateCIs (scope);}
        if ( metodo == false) {updateOptysTarget (scope);}


    }    

    public void finish(Database.BatchableContext bc){
        System.debug('ABAB ' + recordsProcessed + ' Registros procesados');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, 
            JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob
            WHERE Id = :bc.getJobId()];
             System.debug('ABAB AsyncApexJob job = ' + job);
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////

//Método de update de CIs
    public static void updateCIs (List <NE__OrderItem__c> scope){

        System.debug ('ABAB Entro en método CIs');
        List <NE__OrderItem__c> ciUpdate = new List <NE__OrderItem__c>();

            

            //////////////////////////////////////////////////
            // Informar correctamente el Parent Quantity//////
            //////////////////////////////////////////////////

            for (NE__OrderItem__c ci : scope) {


                List<NE__OrderItem__c> lst_padres = new List<NE__OrderItem__c>();
                List<NE__OrderItem__c> lst_hijos = new List<NE__OrderItem__c>();
                List<NE__OrderItem__c> lst_toUpdate = new List<NE__OrderItem__c>();
                
                if(ci.NE__Parent_Order_Item__c == null){ //Padre en un insert
                
                    lst_padres.add(ci);
                }else if(ci.NE__Parent_Order_Item__c != null){ //Hijo en un insert
                    
                    lst_hijos.add(ci);
               }
            }

            if(!lst_padres.isEmpty()){
                for(NE__OrderItem__c n : lst_padres)
                    n.BI_O4_Parent_Qty__c = n.NE__Qty__c;
            }//Padres be
            if(!lst_hijos.isEmpty()){
                Set<Id> set_IdParents = new Set<Id>();
                Map<Id,NE__OrderItem__c> map_parents;
                for(NE__OrderItem__c n : lst_hijos){
                    set_IdParents.add(n.NE__Parent_Order_Item__c);
                }
                if(!set_IdParents.isEmpty()){
                    Map<Id, NE__OrderItem__c> map_ois = new Map<Id, NE__OrderItem__c>([SELECT Id, NE__Qty__c FROM NE__OrderItem__c WHERE Id IN : set_IdParents]);
                    for(NE__OrderItem__c n : lst_hijos){
                        n.BI_O4_Parent_Qty__c = n.NE__Qty__c * map_ois.get(n.NE__Parent_Order_Item__c).NE__Qty__c ;
                        lst_toUpdate.add(n);
                    }
                }
            }//hijos before
        
             if(!lst_toUpdate.isEmpty()){
               BI_MigrationHelper.skipAllTriggers();
                 update lst_toUpdate;
               BI_MigrationHelper.cleanSkippedTriggers();
             }



            ///////////////////////////
            // Simular Workflows //////
            ///////////////////////////

            for (NE__OrderItem__c ci : scope) {
                System.debug ('ABAB FLAG 1');
                Integer cont = 0;
                //Simulo WF BI_O4_RAV_NBAV
                //ci.BI_O4_MRC_Product_Qt_Presup__c == null  || ci.BI_O4_NRC_Product_Qt_Presup__c == null) && 
                if ((ci.BI_Ingreso_Recurrente_Anterior_Producto__c == 0 || ci.BI_Ingreso_Recurrente_Anterior_Producto__c == null)){
                    System.debug ('ABAB FLAG 2');
        
                    //Simulo Field Update BI_O4_MRC_Presup
                    if(ci.BI_Tasa_RecurringChargeOv__c != null && ci.BI_O4_Parent_Qty__c != null) {
                        System.debug ('ABAB FLAG 3');
                        ci.BI_O4_MRC_Product_Qt_Presup__c = ci.BI_Tasa_RecurringChargeOv__c * ci.BI_O4_Parent_Qty__c;
                        cont += 1;
                        }
                
                    //Simulo Field Update BI_O4_NRC_Presup
                    if (ci.BI_Tasa_OneTimeFeeOv__c != null && ci.BI_O4_Parent_Qty__c != null){
                        System.debug ('ABAB FLAG 4');
                        ci.BI_O4_NRC_Product_Qt_Presup__c = ci.BI_Tasa_OneTimeFeeOv__c * ci.BI_O4_Parent_Qty__c;
                        cont += 1;
                    }
            
                    //Simulo Field Update 
                    if (ci.BI_O4_Type__c == null){
                        System.debug ('ABAB FLAG 5');
                        ci.BI_O4_Type__c = 'New Business';
                        cont += 1;
                    }
                }
        
            //Simulo WF BI_O4_RAV_NBAV2
                if(ci.BI_Ingreso_Recurrente_Anterior_Producto__c > 0){
                    System.debug ('ABAB FLAG 7');
                //Simulo Field Update BI_O4_MRC_Presup
                    if(ci.BI_Tasa_RecurringChargeOv__c != null && ci.BI_O4_Parent_Qty__c != null) {
                        System.debug ('ABAB FLAG 8');
                        ci.BI_O4_MRC_Product_Qt_Presup__c = ci.BI_Tasa_RecurringChargeOv__c * ci.BI_O4_Parent_Qty__c;
                        cont += 1;
                        }
                
                    //Simulo Field Update BI_O4_NRC_Presup
                    if (ci.BI_Tasa_OneTimeFeeOv__c != null && ci.BI_O4_Parent_Qty__c != null){
                        System.debug ('ABAB FLAG 9');
                        ci.BI_O4_NRC_Product_Qt_Presup__c = ci.BI_Tasa_OneTimeFeeOv__c * ci.BI_O4_Parent_Qty__c;
                        cont += 1;
                    }
            
                    //Simulo Field Update BI_O4_NRC_Presup2
                    if (ci.BI_O4_Type__c == null){
                        System.debug ('ABAB FLAG 10');
                        ci.BI_O4_Type__c = 'Renewal';
                        cont += 1;
                    }
                }
    
                // add contact to list to be updated
                if (cont>1){
                    System.debug ('ABAB FLAG 11');
                    ciUpdate.add(ci);
    
                }
                
                // increment the instance member counter
                recordsProcessed = recordsProcessed + 1;
            }
            System.debug ('ABAB FLAG 12');
            //BI_MigrationHelper.skipAllTriggers();
            if (!ciUpdate.isEmpty()){
                System.debug ('ABAB FLAG 13');
                //update ciUpdate;
                try{

                    BI_MigrationHelper.skipAllTriggers();
                    recordsCis = Database.update(ciUpdate, false);
                    System.debug ('ABAB FLAG 14');
                    Integer cont = 0;
                    for(NE__OrderItem__c c : ciUpdate){
                        System.debug ('ABAB FLAG 15');
                        System.debug('ABAB FLAG 15 Records Procesado Nº' + cont + '  = ' + c.Id);
                        cont +=1;

                    }
                    //Comprobamos cuales de los registros no han podido actualizarse correctamente
                    for (Database.SaveResult sr : recordsCis) {    
                        System.debug ('ABAB FLAG 16'); 

                        if (!sr.isSuccess()) {  
                            System.debug('ABABAB MÉTODO UPDATE CIS');  
                            string errorDetalles = '';
        
                            for(Database.Error err : sr.getErrors()) {
                                System.debug('ERROR: Ha ocurrido un error al actualizar una cuenta: ' + err.getStatusCode() + ': ' + err.getMessage() + ' / ' + err.getStatusCode() + ': ' + err.getMessage());
                                errorDetalles += err.getMessage();
                            
                                String textoMSG = '- Nro ID:' + sr.getId() + ' Detalles: ' + errorDetalles;    
        
                                lst_errors.add(textoMSG);                   
                            }
                            failedRecords += 1;
                        }
                        else {
                            System.debug('ENTROOO 2');
                            successRecords += 1;
                        }
                    }
                 }   

                catch(Exception e){
                    System.debug('BI_O4_Test_UodateCIs EXCEPTION: '+ e);
                }
        

            }

            //BI_MigrationHelper.cleanSkippedTriggers();
    }


///////////////////////////////////////////////////////////////////////////////////////////


//Método de update de Optys
    public static void updateOptysTarget (List <NE__OrderItem__c> scope2){

        try {

            Set <String> invoiceUnitSet = new Set <String> ();
            Set <Integer> monthSet = new Set <Integer> ();
            Set <String> regionTargetString = new Set <String> ();
            Set <String> regionTargetString2 = new Set <String> ();
            Set <String> accLeadSet = new Set <String> ();
            List <BI_O4_Target__c> targetList = new List <BI_O4_Target__c>();
            Set <BI_O4_Target__c> targetSetNoDuplicates = new Set <BI_O4_Target__c>();
            Map <String, BI_O4_InvoicingUnit_Opp_Target__c> invUnitCustomSetting = BI_O4_InvoicingUnit_Opp_Target__c.getAll();
            Map <String, BI_O4_AccountLeading_Opp_Target__c> accLeadCustomSetting = BI_O4_AccountLeading_Opp_Target__c.getAll();
            List <BI_O4_Oportunidad_Target__c> oppTarQuery = new List <BI_O4_Oportunidad_Target__c>();
            Set <Id> oppTargetChange = new Set <Id> ();
            Set <Id>  scopeOpp = new Set <Id>();
            List <Opportunity> scopeList = new List <Opportunity> ();

            if (!scope2.isEmpty()){

                for (NE__OrderItem__c oi : scope2){
                    System.debug('ooo FLAG 0 - oi.Id = ' + oi.Id);
                    System.debug('ooo FLAG 0 - oi.NE__OrderId__r.NE__OptyId__c) = ' + oi.NE__OrderId__r.NE__OptyId__c);
                    scopeOpp.add(oi.NE__OrderId__r.NE__OptyId__c);
                }

                System.debug('ooo FLAG 0 - scopeOpp.size() = ' + scopeOpp.size());
            }

            if (!scope2.isEmpty()){

                System.debug('ooo FLAG 1');

                scopeList = [SELECT id,  Name,  RecordTypeId, CloseDate, BI_O4_MNC_HQ_Corporate_Opp_Leading__c, BI_O4_Invoicing_Unit__c, BI_Fecha_de_cierre_real__c, StageName  FROM Opportunity WHERE Id IN: scopeOpp]; 
                System.debug('ooo FLAG 1 - scopeList.size() = ' + scopeList.size());
            }

            List <BI_O4_Oportunidad_Target__c> listToInsert = new List <BI_O4_Oportunidad_Target__c>();
    
            if (scopeList != null && !invUnitCustomSetting.isEmpty() && !accLeadCustomSetting.isEmpty()){
                for (Opportunity opp : scopeList){
                    if( opp.RecordTypeId == MAP_NAME_RT.get('BI_Ciclo_completo')) { 
                        System.debug('ooo FLAG 2');
                        oppTargetChange.add(opp.Id);
                        System.debug('ooo FLAG 2 - oppTargetChange.size() = ' + oppTargetChange.size());
                    }

                }
                        
    
                if (!oppTargetChange.isEmpty()){
                    oppTarQuery = [SELECT Id, Name, BI_O4_Oportunidad__c  from BI_O4_Oportunidad_Target__c where BI_O4_Oportunidad__c IN: oppTargetChange];
                    System.debug('ooo FLAG V8 - OppTarQuery.Size() = ' + oppTarQuery.Size());
                }
    
                if (!oppTarQuery.isEmpty()){
                    delete oppTarQuery;
                }
    
               
    
                targetList = [select Id, Name, BI_O4_Invoicing_Unit__c, BI_O4_Region__c, BI_O4_Date_Target__c, BI_O4_Account_Leading_MNC_Unit__c, BI_O4_Month_Target_Date__c, BI_O4_Global__c FROM BI_O4_Target__c];
                targetSetNoDuplicates.addAll(targetList);

                System.debug('ooo FLAG 3 - targetSetNoDuplicates.Size() = ' + targetSetNoDuplicates.Size());
    
                
    
                if (!targetSetNoDuplicates.isEmpty()){
                    for (Opportunity opp : scopeList){
                        System.debug ('ooo FLAG 4 - opp.Id = ' + opp.Id);
                        System.debug ('ooo FLAG 4 - opp.Name = ' +  opp.Name);
                        System.debug ('ooo FLAG 4- opp.CloseDate  = ' + opp.CloseDate);
                        System.debug ('ooo FLAG 4- opp.StageName  = ' + opp.StageName);
                        System.debug ('ooo FLAG 4- opp.BI_O4_MNC_HQ_Corporate_Opp_Leading__c  = ' + opp.BI_O4_MNC_HQ_Corporate_Opp_Leading__c);
                        System.debug ('ooo FLAG 4- opp.BI_O4_Invoicing_Unit__c  = ' + opp.BI_O4_Invoicing_Unit__c);
                        System.debug ('ooo FLAG 4- opp.BI_Fecha_de_cierre_real__c = ' + opp.BI_Fecha_de_cierre_real__c);

                        for (BI_O4_Target__c target : targetSetNoDuplicates){
                            //if (opp.BI_Fecha_de_cierre_real__c != null) { System.debug('ooo opp.BI_Fecha_de_cierre_real__c.month() = ' + opp.BI_Fecha_de_cierre_real__c.month());}
                            ////if (olds != null ){System.debug('ooo olds.get(opp.Id).StageName = ' + olds.get(opp.Id).StageName);}
                            //if(opp.CloseDate != null) {System.debug ('ooo Flag 10 Opp.Month = ' + opp.CloseDate.month());}
    
                            if (target.BI_O4_Invoicing_Unit__c == null && target.BI_O4_Account_Leading_MNC_Unit__c == null && 
                                ((opp.BI_O4_Invoicing_Unit__c != null && invUnitCustomSetting.get(opp.BI_O4_Invoicing_Unit__c) != null && target.BI_O4_Region__c != null && invUnitCustomSetting.get(opp.BI_O4_Invoicing_Unit__c).Regional_Target__c == target.BI_O4_Region__c)
                                || (opp.BI_O4_MNC_HQ_Corporate_Opp_Leading__c != null && accLeadCustomSetting.get(opp.BI_O4_MNC_HQ_Corporate_Opp_Leading__c) != null && target.BI_O4_Region__c != null && accLeadCustomSetting.get(opp.BI_O4_MNC_HQ_Corporate_Opp_Leading__c).Regional_Target__c == target.BI_O4_Region__c))){
    
                                if(target.BI_O4_Month_Target_Date__c == null){
    
                                    BI_O4_Oportunidad_Target__c oppTargetRegional = new BI_O4_Oportunidad_Target__c ();
                                    oppTargetRegional.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetRegional.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetRegional);
                                
                                } else if (target.BI_O4_Month_Target_Date__c != null && opp.StageName != 'F1 - Closed Won' && opp.CloseDate != null && opp.CloseDate.month() == target.BI_O4_Month_Target_Date__c){
    
                                    BI_O4_Oportunidad_Target__c oppTargetRegionalMes = new BI_O4_Oportunidad_Target__c ();
                                    oppTargetRegionalMes.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetRegionalMes.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetRegionalMes);
                                
                                } else if (target.BI_O4_Month_Target_Date__c != null && (opp.StageName == 'F1 - Closed Won') && (opp.BI_Fecha_de_cierre_real__c.month() != null) && opp.BI_Fecha_de_cierre_real__c.month() == target.BI_O4_Month_Target_Date__c){
    
                                    BI_O4_Oportunidad_Target__c oppTargetRegionalMesF1 = new BI_O4_Oportunidad_Target__c ();
                                    oppTargetRegionalMesF1.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetRegionalMesF1.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetRegionalMesF1);
    
                                }
    
    
                            }
    
                            if (opp.BI_O4_Invoicing_Unit__c != null && invUnitCustomSetting.get(opp.BI_O4_Invoicing_Unit__c) != null && target.BI_O4_Invoicing_Unit__c != null && invUnitCustomSetting.get(opp.BI_O4_Invoicing_Unit__c).BI_O4_Local_Target__c == target.BI_O4_Invoicing_Unit__c){
                                if (target.BI_O4_Month_Target_Date__c == null && target.BI_O4_Region__c == null){
                                    System.debug ('ooo Flag 12');
                                    BI_O4_Oportunidad_Target__c oppTargetInvUnit = new BI_O4_Oportunidad_Target__c ();
                                    oppTargetInvUnit.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetInvUnit.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetInvUnit);
    
                                }else if (target.BI_O4_Month_Target_Date__c == null && target.BI_O4_Region__c != null && invUnitCustomSetting.get(opp.BI_O4_Invoicing_Unit__c).Regional_Target__c == target.BI_O4_Region__c){
                                    System.debug ('ooo Flag 13');
                                    BI_O4_Oportunidad_Target__c oppTargetInvUnitRegional = new BI_O4_Oportunidad_Target__c ();
                                    oppTargetInvUnitRegional.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetInvUnitRegional.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetInvUnitRegional);   
    
                                }else if (target.BI_O4_Month_Target_Date__c != null && target.BI_O4_Region__c == null && (opp.StageName == 'F1 - Closed Won') && (opp.BI_Fecha_de_cierre_real__c.month() != null) && opp.BI_Fecha_de_cierre_real__c.month() == target.BI_O4_Month_Target_Date__c){
                                    System.debug ('ooo Flag 14 F1');
                                    BI_O4_Oportunidad_Target__c oppTargetInvUnitMensualF1= new BI_O4_Oportunidad_Target__c ();
                                    oppTargetInvUnitMensualF1.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetInvUnitMensualF1.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetInvUnitMensualF1); 
    
                                }else if (target.BI_O4_Month_Target_Date__c != null && target.BI_O4_Region__c == null && opp.CloseDate.month() == target.BI_O4_Month_Target_Date__c && opp.StageName != 'F1 - Closed Won'){
                                    System.debug ('ooo Flag 14');
                                    BI_O4_Oportunidad_Target__c oppTargetInvUnitMensual= new BI_O4_Oportunidad_Target__c ();
                                    oppTargetInvUnitMensual.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetInvUnitMensual.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetInvUnitMensual);                          
    
    
                                }else if (target.BI_O4_Region__c != null && (opp.StageName == 'F1 - Closed Won') && (opp.BI_Fecha_de_cierre_real__c.month() != null) && opp.BI_Fecha_de_cierre_real__c.month()== target.BI_O4_Month_Target_Date__c && invUnitCustomSetting.get(opp.BI_O4_Invoicing_Unit__c).Regional_Target__c == target.BI_O4_Region__c){
                                    System.debug ('ooo Flag 15 F1');
                                    BI_O4_Oportunidad_Target__c oppTargetRegionalMensualF1 = new BI_O4_Oportunidad_Target__c ();
                                    oppTargetRegionalMensualF1.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetRegionalMensualF1.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetRegionalMensualF1); 
    
                                    }else if (target.BI_O4_Month_Target_Date__c != null && target.BI_O4_Region__c != null && opp.CloseDate.month() == target.BI_O4_Month_Target_Date__c && invUnitCustomSetting.get(opp.BI_O4_Invoicing_Unit__c).Regional_Target__c == target.BI_O4_Region__c){
                                    System.debug ('ooo Flag 15');
                                    BI_O4_Oportunidad_Target__c oppTargetRegionalMensual = new BI_O4_Oportunidad_Target__c ();
                                    oppTargetRegionalMensual.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetRegionalMensual.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetRegionalMensual);
    
                                }
                            }
    
                            if (opp.BI_O4_MNC_HQ_Corporate_Opp_Leading__c != null && target.BI_O4_Account_Leading_MNC_Unit__c != null && accLeadCustomSetting.get(opp.BI_O4_MNC_HQ_Corporate_Opp_Leading__c).BI_O4_Local_Target__c == target.BI_O4_Account_Leading_MNC_Unit__c){
                                if (target.BI_O4_Month_Target_Date__c == null && target.BI_O4_Region__c == null){
                                    System.debug ('ooo Flag 12.1.');
                                    BI_O4_Oportunidad_Target__c oppTargetAccLead = new BI_O4_Oportunidad_Target__c ();
                                    oppTargetAccLead.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetAccLead.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetAccLead);
    
                                }else if (target.BI_O4_Month_Target_Date__c == null && target.BI_O4_Region__c !=null && accLeadCustomSetting.get(opp.BI_O4_MNC_HQ_Corporate_Opp_Leading__c).Regional_Target__c == target.BI_O4_Region__c){
                                    System.debug ('ooo Flag 13.1.');
                                    BI_O4_Oportunidad_Target__c oppTargetAccLeadRegional = new BI_O4_Oportunidad_Target__c ();
                                    oppTargetAccLeadRegional.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetAccLeadRegional.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetAccLeadRegional);   
    
                                }else if (target.BI_O4_Month_Target_Date__c != null && target.BI_O4_Region__c == null && ( opp.StageName == 'F1 - Closed Won') && (opp.BI_Fecha_de_cierre_real__c.month() != null) && opp.BI_Fecha_de_cierre_real__c.month() == target.BI_O4_Month_Target_Date__c){
                                    System.debug ('ooo Flag 14.1. F1');
                                    BI_O4_Oportunidad_Target__c oppTargetAccLeadMensualF1= new BI_O4_Oportunidad_Target__c ();
                                    oppTargetAccLeadMensualF1.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetAccLeadMensualF1.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetAccLeadMensualF1); 
    
                                
                                }else if (target.BI_O4_Month_Target_Date__c != null && target.BI_O4_Region__c == null && opp.CloseDate.month() == target.BI_O4_Month_Target_Date__c && opp.StageName != 'F1 - Closed Won'){
                                    System.debug ('ooo Flag 14.1.');
                                    BI_O4_Oportunidad_Target__c oppTargetAccLeadMensual= new BI_O4_Oportunidad_Target__c ();
                                    oppTargetAccLeadMensual.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetAccLeadMensual.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetAccLeadMensual); 
                                        
    
                                }else if (target.BI_O4_Region__c != null && (opp.StageName == 'F1 - Closed Won') && (opp.BI_Fecha_de_cierre_real__c.month() != null) && opp.BI_Fecha_de_cierre_real__c.month() == target.BI_O4_Month_Target_Date__c && accLeadCustomSetting.get(opp.BI_O4_MNC_HQ_Corporate_Opp_Leading__c).Regional_Target__c == target.BI_O4_Region__c){
                                    System.debug ('ooo Flag 15.1.');
                                    BI_O4_Oportunidad_Target__c oppTargetRegionalMensualAccLeadF1= new BI_O4_Oportunidad_Target__c ();
                                    oppTargetRegionalMensualAccLeadF1.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetRegionalMensualAccLeadF1.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetRegionalMensualAccLeadF1) ;
    
                                }else if (target.BI_O4_Month_Target_Date__c != null && target.BI_O4_Region__c != null && opp.CloseDate.month() == target.BI_O4_Month_Target_Date__c && accLeadCustomSetting.get(opp.BI_O4_MNC_HQ_Corporate_Opp_Leading__c).Regional_Target__c == target.BI_O4_Region__c){
                                    System.debug ('ooo Flag 16.1.');
                                    BI_O4_Oportunidad_Target__c oppTargetRegionalMensualAccLead= new BI_O4_Oportunidad_Target__c ();
                                    oppTargetRegionalMensualAccLead.BI_O4_Oportunidad__c = opp.Id;
                                    oppTargetRegionalMensualAccLead.BI_O4_Target__c = target.Id;
                                    listToInsert.add(oppTargetRegionalMensualAccLead) ;
                                }
                            }
    
                            if(target.BI_O4_Global__c == TRUE && (target.BI_O4_Month_Target_Date__c == null) && (target.BI_O4_Account_Leading_MNC_Unit__c == null) && (target.BI_O4_Invoicing_Unit__c == null)){
                                System.debug ('ooo Flag  GLOBAL1');
                                BI_O4_Oportunidad_Target__c oppTargetGlobal= new BI_O4_Oportunidad_Target__c ();
                                oppTargetGlobal.BI_O4_Oportunidad__c = opp.Id;
                                oppTargetGlobal.BI_O4_Target__c = target.Id;
                                listToInsert.add(oppTargetGlobal); 
                            }
    
                            if(target.BI_O4_Global__c == TRUE && (target.BI_O4_Month_Target_Date__c != null) && opp.StageName != 'F1 - Closed Won' && target.BI_O4_Month_Target_Date__c == opp.CloseDate.month()  && (target.BI_O4_Account_Leading_MNC_Unit__c == null) && (target.BI_O4_Invoicing_Unit__c == null)){
                                System.debug ('ooo Flag  GLOBAL2');
                                BI_O4_Oportunidad_Target__c oppTargetGlobalMensual= new BI_O4_Oportunidad_Target__c ();
                                oppTargetGlobalMensual.BI_O4_Oportunidad__c = opp.Id;
                                oppTargetGlobalMensual.BI_O4_Target__c = target.Id;
                                listToInsert.add(oppTargetGlobalMensual); 
                            }

                            if(target.BI_O4_Global__c == TRUE && (target.BI_O4_Month_Target_Date__c != null) && opp.StageName == 'F1 - Closed Won' && (opp.BI_Fecha_de_cierre_real__c.month() != null) && target.BI_O4_Month_Target_Date__c == opp.BI_Fecha_de_cierre_real__c.month()  && (target.BI_O4_Account_Leading_MNC_Unit__c == null) && (target.BI_O4_Invoicing_Unit__c == null)){
                                System.debug ('ooo Flag  GLOBAL2');
                                BI_O4_Oportunidad_Target__c oppTargetGlobalMensual= new BI_O4_Oportunidad_Target__c ();
                                oppTargetGlobalMensual.BI_O4_Oportunidad__c = opp.Id;
                                oppTargetGlobalMensual.BI_O4_Target__c = target.Id;
                                listToInsert.add(oppTargetGlobalMensual); 
                            }
                        }
    
                    }
                }
    
                if (!listToInsert.isempty()){
                    BI_MigrationHelper.skipAllTriggers();
                    System.debug ('ooo INSERT'); 
                    for (BI_O4_Oportunidad_Target__c ok: listToInsert){
                        System.debug('ooo target asignado = ' + ok.Id);
                    }
                    insert listToInsert;
                }
                
            }
            
        } catch (Exception exc) {
            BI_LogHelper.generate_BILog('BI_O4_OpportunityMethods.oppTargetAssign', 'BI_EN', Exc, 'Trigger');
        }  

    }


}