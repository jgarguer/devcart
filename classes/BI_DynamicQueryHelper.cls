public class BI_DynamicQueryHelper {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   
    
    History:
    
    <Date>            <Author>          <Description>
    02/02/2015        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   
    
    IN:            
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    02/02/2015        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static String dynamicQuery(String myObjectS){
		
        SObjectType objToken = Schema.getGlobalDescribe().get(myObjectS);
        DescribeSObjectResult objDef = objToken.getDescribe();  
        Map<String, SObjectField> fields = objDef.fields.getMap();
        Set<String> fieldSet = fields.keySet(); 
        list <DescribeFieldResult> selectedField=new list<DescribeFieldResult>();
        
        for (String S:fieldSet){           
            SObjectField fieldToken = fields.get(s);        
            selectedField.add(fieldToken.getDescribe());
        }
               
        string myFieldQS =('SELECT '+selectedField[0].getName());
        
        for(Integer i=1;i<selectedField.size();i++){                       
            if (selectedField[i].isAccessible())                   
                myFieldQS=myFieldQS+','+(selectedField[i].getName());
        }
    
        return myFieldQS; 
		
	}

}