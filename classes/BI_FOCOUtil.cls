/*-------------------------------------------------------------------
	Author:         Virgilio Utrera
	Company:        Salesforce.com
	Description:    Utilities class for FOCO data retrieval
	History
	<Date>          <Author>           <Change Description>
	15-Oct-2014     Virgilio Utrera    Initial Version
	20-Oct-2014		Virgilio Utrera    Added FOCO data retrieval methods
	22-Oct-2014		Virgilio Utrera    Modified getFOCOAmountsFromCurrentYear
	20-Feb-2015     Virgilio Utrera    Added deleteErrorLog method
	10-Mar-2015     Virgilio Utrera    Added getCurrentYearFOCOAmountsByProduct and getCurrentYearFOCOAmountsByPeriod methods
-------------------------------------------------------------------*/

public class BI_FOCOUtil {

	public BI_FOCOUtil() {}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Helper method for getting all subordinate role Ids
		IN:             Set<Id>: Id of the roles to find out about subordinates
		OUT:            Set<Id>: Set containing all subordinate role Ids
		History
		<Date>          <Author>           <Change Description>
		15-Oct-2014     Virgilio Utrera    Initial version
	-------------------------------------------------------------------*/

	static public Set<ID> getAllSubRoleIds(Set<ID> roleIds) {
		Set<ID> currentRoleIds = new Set<ID>();

		// Get all of the role Ids below the requested user roles on the hierarchy
		for(UserRole userRole : [ SELECT Id FROM UserRole WHERE ParentRoleId IN :roleIds AND ParentRoleID != null LIMIT 10000 ])
			currentRoleIds.add(userRole.Id);

		// Recursively fetch roles below the ones found 
		if(currentRoleIds.size() > 0)
			currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));

		return currentRoleIds;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Helper method for getting all subordinate user Ids
		IN:             Id: Id of the requested user to find out about subordinates
		OUT:            Set<Id>: Set containing all subordinate user Ids
		History
		<Date>          <Author>           <Change Description>
		15-Oct-2014     Virgilio Utrera    Initial version
		17-Oct-2014     Virgilio Utrera    Added error handling for users with unassigned roles
	-------------------------------------------------------------------*/

	static public Set<ID> getRoleSubordinateUsers(Id userId) {
		Id roleId;
		List<User> usersList = new List<User>();
		Map<Id,User> usersMap;
		Set<Id> allSubRoleIds;

		// Get requested user's role
		usersList = [ SELECT UserRoleId FROM User WHERE Id = :userId LIMIT 1 ];

		if(!usersList.isEmpty() && usersList[0].UserRoleId != null) {
			roleId = usersList[0].UserRoleId;

			// Get all of the role Ids below the requested user on the hierarchy
			allSubRoleIds = getAllSubRoleIds(new Set<ID>{roleId});

			// Get all of the Ids for the users in those roles
			usersMap = new Map<Id, User>([ SELECT Id, Name FROM User WHERE UserRoleId IN :allSubRoleIds LIMIT 10000 ]);
		}
		else {
			// No role is found
			usersMap = new Map<Id, User>();
		}

		return usersMap.keySet();
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Queries FOCO amounts for the current year grouped by product (Rama Local), for a specified
						user and his/her subordinates on the role hierarchy
		IN:				Id: user Id
						Set<Id>: Set containing all subordinate user Ids for the specified user Id
		OUT:			List<AggregateResult>: FOCO amounts
		History
		<Date>          <Author>           <Change Description>
		10-Mar-2015     Virgilio Utrera    Initial version
	-------------------------------------------------------------------*/

	static public List<AggregateResult> getCurrentYearFOCOAmountsByProduct(Id userId, Set<Id> subordinateIds) {
		List<AggregateResult> results = new List<AggregateResult>();

		// This method is intended to be used from a VisualForce page with the @ReadOnly annotation, therefore no LIMIT clause is needed in the query
		// BI_Monto__c is automatcally converted into the org's default currency
		results = 	[	
						SELECT BI_Rama__r.BI_Nombre__c, SUM(BI_Monto__c) BI_Monto__c
						FROM BI_Registro_Datos_FOCO__c
						WHERE BI_Fecha_Inicio_Periodo__c = THIS_YEAR
						AND (BI_Cliente__r.OwnerId IN :subordinateIds OR BI_Cliente__r.OwnerId = :userId)
						AND (BI_Oportunidad__c = NULL OR BI_Oportunidad__r.CloseDate >= TODAY)
						GROUP BY BI_Rama__r.BI_Nombre__c
						ORDER BY BI_Rama__r.BI_Nombre__c ASC
					];

		return results;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Queries FOCO amounts for the current year grouped by time period (month-year), for a specified
						user and his/her subordinates on the role hierarchy
		IN:				Id: user Id
						Set<Id>: Set containing all subordinate user Ids for the specified user Id
		OUT:			List<AggregateResult>: FOCO amounts
		History
		<Date>          <Author>           <Change Description>
		10-Mar-2015    	Virgilio Utrera    Initial version
	-------------------------------------------------------------------*/

	static public List<AggregateResult> getCurrentYearFOCOAmountsByPeriod(Id userId, Set<Id> subordinateIds) {
		List<AggregateResult> results = new List<AggregateResult>();

		// This method is intended to be used from a VisualForce page with the @ReadOnly annotation, therefore no LIMIT clause is needed in the query
		// BI_Monto__c is automatcally converted into the org's default currency
		results = 	[	
						SELECT BI_Fecha_Inicio_Periodo__c, SUM(BI_Monto__c) BI_Monto__c
						FROM BI_Registro_Datos_FOCO__c
						WHERE BI_Fecha_Inicio_Periodo__c = THIS_YEAR
						AND (BI_Cliente__r.OwnerId IN :subordinateIds OR BI_Cliente__r.OwnerId = :userId)
						AND (BI_Oportunidad__c = NULL OR BI_Oportunidad__r.CloseDate >= TODAY)
						GROUP BY BI_Fecha_Inicio_Periodo__c
						ORDER BY BI_Fecha_Inicio_Periodo__c ASC
					];

		return results;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Queries commercial goals for the current year and selected user and subordinates
		IN:             Id: Id of the requested user to find out about subordinates
						Set<Id>: Set containing all subordinate user Ids
		OUT:			List<BI_Objetivo_Comercial__c>: commercial goal amounts
		History
		<Date>          <Author>           <Change Description>
		20-Oct-2014     Virgilio Utrera    Initial version
		10-Mar-2015		Virgilio Utrera    Optimized queries to reduce heap size
	-------------------------------------------------------------------*/

	static public List<BI_Objetivo_Comercial__c> getProductGoalsFromCurrentYear(Id userId, Set<Id> subordinateIds) {
		List<BI_Objetivo_Comercial__c> results = new List<BI_Objetivo_Comercial__c>();

		// This method is intended to be used from a VisualForce page with the @ReadOnly annotation, therefore no LIMIT clause is needed in the queries
		if(subordinateIds != null) {
			for(BI_Objetivo_Comercial__c oc : 	[
													SELECT BI_Objetivo__c, BI_Rama__r.BI_Nombre__c, CurrencyIsoCode
													FROM BI_Objetivo_Comercial__c
													WHERE BI_Tipo__c = 'Producto'
													AND (OwnerId IN :subordinateIds OR OwnerId = :userId)
												]) {
				results.add(oc);
			}

		}
		else {
			for(BI_Objetivo_Comercial__c oc : 	[
													SELECT BI_Objetivo__c, BI_Rama__r.BI_Nombre__c, CurrencyIsoCode
													FROM BI_Objetivo_Comercial__c
													WHERE BI_Tipo__c = 'Producto' 
													AND OwnerId = :userId
												]) {
				results.add(oc);
			}
		}

		return results;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Queries commercial goals for the current year and selected user and subordinates
		IN:             Id: Id of the requested user to find out about subordinates
						Set<Id>: Set containing all subordinate user Ids
		OUT:			List<BI_Objetivo_Comercial__c>: commercial goal amounts
		History
		<Date>          <Author>           <Change Description>
		20-Oct-2014     Virgilio Utrera    Initial version
		10-Mar-2015		Virgilio Utrera    Optimized queries to reduce heap size
	-------------------------------------------------------------------*/

	// This method is intended to be used from a VisualForce page with the @ReadOnly annotation, therefore no LIMIT clause is needed in the queries
	static public List<BI_Objetivo_Comercial__c> getRevenueGoalsFromCurrentYear(Id userId, Set<Id> subordinateIds) {
		List<BI_Objetivo_Comercial__c> results = new List<BI_Objetivo_Comercial__c>();

		if(subordinateIds != null) {
			for(BI_Objetivo_Comercial__c oc : 	[
													SELECT BI_Objetivo__c, BI_Periodo__r.BI_Fecha_Inicio_Periodo__c, CurrencyIsoCode
													FROM BI_Objetivo_Comercial__c
													WHERE BI_Tipo__c = 'Cartera'
													AND BI_Periodo__r.BI_Fecha_Inicio_Periodo__c = THIS_YEAR 
													AND (OwnerId IN :subordinateIds OR OwnerId = :userId)
												]) {
				results.add(oc);
			}
		}
		else {
			for(BI_Objetivo_Comercial__c oc : 	[
													SELECT BI_Objetivo__c, BI_Periodo__r.BI_Fecha_Inicio_Periodo__c, CurrencyIsoCode
													FROM BI_Objetivo_Comercial__c
													WHERE BI_Tipo__c = 'Cartera'
													AND BI_Periodo__r.BI_Fecha_Inicio_Periodo__c = THIS_YEAR 
													AND OwnerId = :userId
												]) {
				results.add(oc);
			}
		}

		return results;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Gets currencies list loaded in the system
		IN:
		OUT:			List<CurrencyType>: currencies list
		History
		<Date>          <Author>           <Change Description>
		20-Oct-2014     Virgilio Utrera    Initial version
	-------------------------------------------------------------------*/

	static public List<CurrencyType> getCurrenciesList() {
		List<CurrencyType> currencyTypesList = new List<CurrencyType>();

		for(CurrencyType ct : [ SELECT IsoCode, ConversionRate, IsCorporate FROM CurrencyType LIMIT 200 ]) {
			currencyTypesList.add(ct);
		}

		return currencyTypesList;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Deletes a set of Registro Datos FOCO Error Log records identified under an error log Id
		IN:				String: error log Id to delete
		OUT:			Boolean: indicates whether the erro log has been scheduled for deletion
		History
		<Date>          <Author>           <Change Description>
		20-Feb-2015     Virgilio Utrera    Initial version
	-------------------------------------------------------------------*/

	static public Boolean deleteErrorLog(String focoErrorLogId) {
		BI_FOCO_Error_Logs_Pending_Deletion__c errorLogPendingDeletion;
		Boolean logAlreadyInDeletionProcess = false;
		Boolean scheduledForDeletion = false;
		List<BI_FOCO_Error_Logs_Pending_Deletion__c> errorLogsPendingDeletionList;
		List<BI_Registro_Datos_FOCO_Error_Log__c> errorLogsList = new List<BI_Registro_Datos_FOCO_Error_Log__c>();

		try {
			// Check is error log is already already deleted
			errorLogsList = [ SELECT Id FROM BI_Registro_Datos_FOCO_Error_Log__c WHERE BI_Identificador_Log__c = :focoErrorLogId LIMIT 1 ];

			if((BI_FOCO_Error_Logs_Pending_Deletion__c.getInstance(focoErrorLogId) != null) || (errorLogsList.size() == 0))
				logAlreadyInDeletionProcess = true;

			if(!logAlreadyInDeletionProcess) {
				// Mark data load Id as pending for deletion
				errorLogsPendingDeletionList = new List<BI_FOCO_Error_Logs_Pending_Deletion__c>();
				errorLogPendingDeletion = new BI_FOCO_Error_Logs_Pending_Deletion__c (Name = focoErrorLogId);
				errorLogsPendingDeletionList.add(errorLogPendingDeletion);

				insert errorLogsPendingDeletionList;

				// Delete error log
				BI_FOCOBatchDeleteErrorLog batchDeleteErrorLog = new BI_FOCOBatchDeleteErrorLog(focoErrorLogId);
				Id batchProcessId = Database.executeBatch(batchDeleteErrorLog, 1000);

				scheduledForDeletion = true;
			}
		}
		catch(Exception e) {
			throw new BI_FOCO_Exception('Error while batch deleting FOCO Error Log "' + focoErrorLogId + '"', e);
		}

		return scheduledForDeletion;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Gets a list of available error log Ids
		IN:
		OUT:			Map<String, BI_FOCO_Available_Error_Logs__c>: list of available error log Ids
		History
		<Date>          <Author>           <Change Description>
		20-Feb-2015     Virgilio Utrera    Initial version
		26-Feb-2015		Virgilio Utrera    Query custom setting to get Ids list
	-------------------------------------------------------------------*/

	static public Map<String, BI_FOCO_Available_Error_Logs__c> getErrorLogIds() {
		Map<String, BI_FOCO_Available_Error_Logs__c> errorLogIds = new Map<String, BI_FOCO_Available_Error_Logs__c>();

		errorLogIds = BI_FOCO_Available_Error_Logs__c.getAll();
		return errorLogIds;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Gets a list of error logs pending for deletion
		IN:
		OUT:			Map<String, BI_FOCO_Error_Logs_Pending_Deletion__c>: list of error log Ids pending for deletion
		History
		<Date>          <Author>           <Change Description>
		23-Feb-2015     Virgilio Utrera    Initial version
	-------------------------------------------------------------------*/

	static public Map<String, BI_FOCO_Error_Logs_Pending_Deletion__c> getErrorLogsPendingDeletion() {
		Map<String, BI_FOCO_Error_Logs_Pending_Deletion__c> errorLogsPendingDeletion = new Map<String, BI_FOCO_Error_Logs_Pending_Deletion__c>();

		errorLogsPendingDeletion = BI_FOCO_Error_Logs_Pending_Deletion__c.getAll();
		return errorLogsPendingDeletion;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Deletes an error log Id from list of error log Ids pending from deletion
		IN:				String: error log Id to be deleted
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		26-Feb-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static public void removeLogIdFromPendingForDeletionList(String errorLogId) {
		List<BI_FOCO_Error_Logs_Pending_Deletion__c> errorLogIdsToDeleteList = new List<BI_FOCO_Error_Logs_Pending_Deletion__c>();
		Map<String, BI_FOCO_Error_Logs_Pending_Deletion__c> errorLogIdsPendingDeletionMap = new Map<String, BI_FOCO_Error_Logs_Pending_Deletion__c>();

		// Get all error log Ids pending for deletion
		errorLogIdsPendingDeletionMap = BI_FOCO_Error_Logs_Pending_Deletion__c.getAll();

		// Check if the error log Id to be deleted exists
		if(errorLogIdsPendingDeletionMap.get(errorLogId) != null)
			errorLogIdsToDeleteList.add(errorLogIdsPendingDeletionMap.get(errorLogId));

		try {
			// Remove error log Id from pending-for-deletion list
			if(errorLogIdsToDeleteList.size() > 0)
				delete errorLogIdsToDeleteList;
		}
		catch(Exception e) {
			throw new BI_FOCO_Exception('Error while deleting FOCO Error Log Id "' + errorLogId + '" from the pending-for-deletion FOCO Error Log Ids list', e);
		}
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Deletes an error log Id from list of available error log Ids
		IN:				String: error log Id to be deleted
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		26-Feb-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static public void removeLogIdFromAvailableLogsList(String errorLogId) {
		List<BI_FOCO_Available_Error_Logs__c> errorLogIdsToDeleteList = new List<BI_FOCO_Available_Error_Logs__c>();
		Map<String, BI_FOCO_Available_Error_Logs__c> availableErrorLogIdsMap = new Map<String, BI_FOCO_Available_Error_Logs__c>();

		// Get all error log Ids available
		availableErrorLogIdsMap = BI_FOCO_Available_Error_Logs__c.getAll();

		// Check if the error log Id to be deleted exists
		if(availableErrorLogIdsMap.get(errorLogId) != null)
			errorLogIdsToDeleteList.add(availableErrorLogIdsMap.get(errorLogId));

		try {
			// Remove error log Id from pending-for-deletion list
			if(errorLogIdsToDeleteList.size() > 0)
				delete errorLogIdsToDeleteList;
		}
		catch(Exception e) {
			throw new BI_FOCO_Exception('Error while deleting FOCO Error Log Id "' + errorLogId + '" from the available FOCO Error Log Ids list', e);
		}
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Inserts error log Ids into list of available error log Ids
		IN:				List<BI_Registro_Datos_FOCO_Error_Log__c>: list of error log records
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		26-Feb-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static public void insertErrorLogIds(List<BI_Registro_Datos_FOCO_Error_Log__c> records) {
		List<BI_FOCO_Available_Error_Logs__c> errorLogIdsToInsert = new List<BI_FOCO_Available_Error_Logs__c>();
		Map<String, BI_FOCO_Available_Error_Logs__c> existingErrorLogIds = BI_FOCO_Available_Error_Logs__c.getAll();
		Set<String> newErrorLogIds = new Set<String>();

		// Add new log Ids from inserted records
		for(BI_Registro_Datos_FOCO_Error_Log__c errorLogRecord : records)
			newErrorLogIds.add(errorLogRecord.BI_Identificador_Log__c);

		// Add new log Ids to insert list if they do not already exist
		for(String newErrorLogId : newErrorLogIds) {
			if(!existingErrorLogIds.containsKey(newErrorLogId) && newErrorLogId != null)
				errorLogIdsToInsert.add(new BI_FOCO_Available_Error_Logs__c(Name = newErrorLogId));
		}

		// Insert new error log Ids
		if(!errorLogIdsToInsert.isEmpty()) {
			try {
				insert errorLogIdsToInsert;
			}
			catch(Exception e) {
				throw new BI_FOCO_Exception('Error while inserting available FOCO Error Log Ids', e);
			}
		}
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Returns the ISO code for a country
		IN:				String: country name
		OUT:            String: country ISO code
		History
		<Date>          <Author>           <Change Description>
		26-Feb-2015     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	static public String getCountryISOCode(String country) {
		String countryISOCode = '';

		if(country == 'Argentina')
			countryISOCode = 'ARG';

		if(country == 'Centroamérica' || country == 'Centroamerica')
			countryISOCode = 'CAM';

		if(country == 'Chile')
			countryISOCode = 'CHL';

		if(country == 'Colombia')
			countryISOCode = 'COL';

		if(country == 'Costa Rica')
			countryISOCode = 'CRI';

		if(country == 'Ecuador')
			countryISOCode = 'ECU';

		if(country == 'El Salvador')
			countryISOCode = 'SLV';

		if(country == 'Guatemala')
			countryISOCode = 'GTM';

		if(country == 'México' || country == 'Mexico')
			countryISOCode = 'MEX';

		if(country == 'Nicaragua')
			countryISOCode = 'NIC';

		if(country == 'Panamá' || country == 'Panama')
			countryISOCode = 'PAN';

		if(country == 'Perú' || country == 'Peru')
			countryISOCode = 'PER';

		if(country == 'Uruguay')
			countryISOCode = 'URY';

		if(country == 'Venezuela')
			countryISOCode = 'VEN';

		return countryISOCode;
	}
}