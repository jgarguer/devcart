/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for NEPrintOffer class
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    15/19/2017              Pedro Párraga            Initial Creation
    27/09/2017              Jaime Regidor            Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class Test_NEPrintOffer
{
    static testMethod void myUnitTest()
    {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        Account acc = new Account (Name = 'account1',NE__Type__c = 'Business',
            ShippingPostalCode ='1',
            ShippingCity ='1',
            ShippingCountry ='1',
            ShippingState ='1',
            ShippingStreet ='1',
            BillingPostalCode ='1',
            BillingCity ='1',
            BillingCountry ='1',
            BillingState ='1',
            BillingStreet='1',
            BI_Segment__c = 'test', //28/09/2017
            BI_Subsegment_Regional__c = 'test', //28/09/2017
            BI_Territory__c = 'test' //28/09/2017
        );
        
        insert acc;

        NEPrintOffer printDoc = new NEPrintOffer(); 
        
        NE__Order__c ord = new NE__Order__c(NE__AccountId__c = acc.Id,NE__BillAccId__c = acc.Id,NE__AssetEnterpriseId__c='testEnterprise',NE__ServAccId__c = acc.Id, NE__OrderStatus__c = 'Valid');
        insert ord;
        
        NE__Lov__c lovobj = new NE__Lov__c (Name = 'MailBodyes_AR',NE__Active__c=true,Text_Area_Value__c='text body',NE__Type__c = 'MAIL_BODY',NE__Value2__c=UserInfo.getLocale() 
        );
        insert lovobj;

        NE__Lov__c lovobj2 = new NE__Lov__c (Name = 'MailBodyes_AR',NE__Active__c=true,Text_Area_Value__c='text body',NE__Type__c = 'MAIL_SUBJECT',NE__Value2__c=UserInfo.getLocale() 
        );
        insert lovobj2;

        NE__Lov__c lovobj3 = new NE__Lov__c (Name = 'MailBodyes_AR',NE__Active__c=true,Text_Area_Value__c='text body',NE__Type__c = 'Origin',NE__Value2__c=UserInfo.getLocale() 
        );
        insert lovobj3;

        Contact cont = new Contact(LastName='TestContact_0', AccountId=acc.Id, email='Test@testest.test');
        insert cont;

        NE__Billing_Profile__c bp = new NE__Billing_Profile__c ();
        List<NE__OrderItem__c> lst_oi = new List<NE__OrderItem__c>();
        NE__Document_Component__c dc = new NE__Document_Component__c();
        //NE__OrderItem__c oi = new NE__OrderItem__c();
                
        
        Pagereference p = Page.NEPrintOffer;
        p.getParameters().put('OrderId',ord.Id);
        
        Test.setCurrentPageReference(p);
        NEPrintOffer printDoc2 = new NEPrintOffer();        
        
        list<String> stories    =   new list<String>();
        printDoc2.stories        =   stories;
        printDoc2.stories2       =   'test;test';
        
        String[] stori = printDoc2.getStories();
        String[] ls_stories = new String[]{'uno', 'dos', 'tres'};
        printDoc2.setStories(stori);
        printDoc2.stories = ls_stories;
        printDoc2.Page = 'page';
        printDoc2.quoteid = 'quoteid';
        printDoc2.AccountId = 'AccountId';
        printDoc2.acc = acc;
        printDoc2.billProf  = bp;
        printDoc2.ordItem  = lst_oi;
        printDoc2.DC = dc;
        printDoc2.DCitem = dc;
        printDoc2.DCattribute = dc; 
        printDoc2.content = 'content';
        printDoc2.AccountIdx = 'AccountIdx';
        printDoc2.selectclick();
        printDoc2.redirect();
        printDoc2.SaveIt();
        printDoc2.getStoryItems();


    }
}