@isTest
private class PCA_CaseCommentMethods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_CaseCommentMethods_TEST class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    05/09/2014              Antonio Moruno          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static{
        BI_TestUtils.throw_exception = false;
    } 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for PCA_CaseCommentMethods.sendEmail.
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    05/09/2014              Antonio Moruno          Initial Version
    10/10/2016              Antonio Pardo           Modified to cover the new method
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

    static testMethod void sendEmailTEST() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            List<Contact> conList = new List<Contact>();
            for(Contact item : con) {
                item.Email='a@b.c';
                conList.add(item);
            }
            update conList;
            system.debug('con: '+con);
           
            
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalPlusProfile());
            system.runAs(user1){
                System.debug('USUARIOOOO'+ user1);
                BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=user1.Id, BI_User__c=user1.Id,BI_Cliente__c=acc[0].Id,BI_Contacto__c=con[0].Id);
                insert ccp;
                Case Case1 = new Case(AccountId=acc[0].Id,contactId = user1.ContactId,BI_Country__c= Label.BI_Mexico,Reason='Reporte de avería',BI_Otro_Tipo__c = 'test',Status='Nuevo',Type='Reclamo',BI_Categoria_del_caso__c='Nivel 1',Priority='Media',Origin=Label.BI_OrigenCasoPortalCliente,Subject='Asunto prueba');
                insert Case1;
                                      
               //system.assertEquals(coment1.ParentId,Case1.Id);
            }
            Case Case1 = [Select id from Case];
            CaseComment Coment1 = new CaseComment(ParentId=Case1.Id,CommentBody = 'Nuevo Comentario',IsPublished=true); 
            insert Coment1;
        }
        
        /*
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        
         system.runAs(usr){
            List<Region__c> lst_pais = BI_DataLoad.loadPais(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List <Contact> lst_con = BI_DataLoad.loadContacts(1, acc);
            BI_Contact_Customer_Portal__c portalContact = new BI_Contact_Customer_Portal__c(BI_Activo__c = true, BI_Contacto__c = lst_con[0].Id,BI_Cliente__c = acc[0].Id,BI_Perfil__c='Apoderado',BI_User__c=usr.Id);
            insert portalContact; 
            Region__c mex = new Region__c(Name = Label.BI_Mexico); 
            
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            Case Case1 = new Case(Account=acc,BI_Country__c=mex.Id,Reason='Reporte de avería',Status='Nuevo',Type='Reclamo',BI_Categoria_del_caso__c='Nivel 1',Priority='Media',Origin='Portal cliente',Subject='Asunto prueba');
            insert Case1;
            CaseComment Coment1 = new CaseComment(ParentId=Case1.Id,CommentBody = 'Nuevo Comentario'); 
            system.debug('Comentarios** : ' +Coment1);
            insert Coment1;
            
         }*/
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Patricia Castillo 
    Company:       NEA
    Description:   Test method to manage the code coverage for PCA_CaseCommentMethods.validateCaseStatusBIEN.
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    18/10/2016              Patricia Castillo       Initial Version
    28/09/2017              Jaime Regidor           Add the mandatory fields on account creation: BI_Territory__c
    10/10/2017              Gawron, Julián          Adding BI_MigrationHelper.skipAllTriggers();  
------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

    static testMethod void validateCaseStatusBIEN_TEST() {
        BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        List<Profile> id_ProfPER = new List<Profile>([SELECT Id from Profile where name='BI_Standard_PER' limit 1]);
        User me = [SELECT Id from User where id=:UserInfo.getUserId() LIMIT 1];
        
        BI_MigrationHelper.skipAllTriggers();
        //Perú User
        User usPER = new User(
          Username = 'per321prueba@test.com',
          Email = 'per321prueba@test.com',
          Alias = 'per321',
          CommunityNickname = 'per321.prueba',
          IsActive = true,
          ProfileId = id_ProfPER[0].Id,
          BI_Permisos__c = 'Ejecutivo de Cliente',
          Pais__c = 'Peru',
          TimeZoneSidKey='America/Lima', 
          LocaleSidKey='es_ES', 
          EmailEncodingKey='ISO-8859-1', 
          LanguageLocaleKey='es', 
          LastName='prueba'
        );
        System.runAs(me){
          insert usPER;
        }
        
        Account acc = new Account(
            Name = 'Test1PER', 
            Industry = 'Comercio', 
            CurrencyIsoCode = 'PEN', 
            BI_Activo__c = 'Sí', 
            BI_Cabecera__c = 'Sí',  
            BI_Estado__c = true, 
            BI_No_Identificador_fiscal__c = '12345678', 
            BI_Validador_Fiscal__c = 'PER_DNI_12345678', 
            BI_Country__c = Label.BI_Peru, 
            BI_Sector__c = 'Comercio', 
            BI_Segment__c = 'Empresas', 
            BI_Subsector__c = 'Alimentos y Bebidas', 
            BI_Subsegment_Regional__c = 'Corporate', 
            BI_Territory__c = 'test', // 28/09/2017
            BI_Tipo_de_identificador_fiscal__c = 'DNI', 
            TGS_Es_MNC__c = false, 
            Sector__c = 'Private',
            OwnerId = usPER.Id
        );
        System.runAs(usPER) {
            insert acc;
        }
        BI_MigrationHelper.cleanSkippedTriggers();
        Test.startTest(); // 28/09/2017
        Contact con = new Contact(LastName = 'testPER',
        AccountId = acc.Id,
        Email = 'testper@gmail.com',
        HasOptedOutOfEmail = false,
        BI_Activo__c = true,
        BI_Country__c = acc.BI_Country__c,
        Phone = '914123456',
        BI_Cuenta_activa_en_portal__c=acc.Id);

        System.runAs(usPER) {
            insert con;
        }

        
        Case casePer = new Case(AccountId=acc.Id,OwnerId=usPER.id, ContactId = con.Id,BI_Country__c= Label.BI_Peru,Reason='Reporte de avería',BI_Otro_Tipo__c = 'test',Status=Label.BI_CaseStatus_New,Type='Reclamo',BI_Categoria_del_caso__c='Nivel 1',Priority='Media',Origin='Customer Communities',Subject='Asunto prueba' );
        System.runAs(usPER) {
            insert casePer;
        }
      
        casePer.STatus=Label.BI_CaseStatus_Closed;
        casePer.TGS_Resolution__c='Prueba Cerrado';
        casePer.TGS_Status_reason__c='Customer Satisfied';

        System.runAs(me) {
            update casePer;
        }

        CaseComment comment = new CaseComment(ParentId=casePer.Id,CommentBody = 'Nuevo Comentario Prueba Cerrado/Cancelado',IsPublished=false); 
        System.runAs(usPER) {
            try{
                insert comment;
                throw new BI_Exception('No se debe permitir esta operación');

            } catch(DmlException exc) {

            }
        }
        Test.stopTest();
        
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Fernández Díaz
    Company:       Accenture
    Description:   Test class to manage the coverage code for PCA_CaseCommentMethods.reopenPERCases method
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    21/04/2017              Alberto Fernández       Initial Version
    28/09/2017              Jaime Regidor           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c
    03/01/2018              Alfonso Alvarez         Cambio por migracion a licencias Customer Communities. Uso de etiqueta
    11/01/2018              Alberto Fernández       Removing some unnecesary inserts causing exception
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest
    static void reopenPERCases_TEST() {

        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);
        // 03-01-2018 AJAE
        Profile adminProf = [SELECT Id FROM Profile WHERE Name = :Label.BI_Customer_Portal_Profile ];
        // FIN 03-01-2018 AJAE

        BI_MigrationHelper.skipAllTriggers();

        Account portalAccount = new Account(name = 'portalAccount',
                                           BI_Segment__c = 'test', // 28/09/2017
                                           BI_Subsegment_Regional__c = 'test', // 28/09/2017
                                           BI_Territory__c = 'test' // 28/09/2017
                                           );//create a portal account first
        insert portalAccount;

        Contact portalContact = new contact(LastName = 'portalContact', AccountId = portalAccount.Id); //create a portal contact
        insert portalContact;

        User testUser = new User(email='genelia.dsouza@gmail.com',
                profileid = adminProf.Id, 
                UserName='genelia.dsouza@gmail.com', 
                Alias = 'GDS',
                TimeZoneSidKey='America/New_York',
                EmailEncodingKey='ISO-8859-1',
                LocaleSidKey='en_US', 
                LanguageLocaleKey='en_US',
                ContactId = portalContact.Id,
                PortalRole = 'Manager',
                FirstName = 'Genelia',
                BI_Permisos__c = Label.BI_PortalPlatino,
                LastName = 'Dsouza');

        insert testUser;

        BI_MigrationHelper.cleanSkippedTriggers();

        Test.startTest();

        Case testCase = new Case(Priority = 'Prioridad', AccountId = portalAccount.Id, Status = 'Pending');        

        System.runAs(testUser) {
            insert testCase;
            CaseComment testFi = new CaseComment();
            testFi.parentid = testCase.Id; 
            insert testFi;

            Case cas = [SELECT Id, Status, BI_PER_Comentario_nuevo__c FROM Case WHERE Id = :testCase.Id];

            System.assertEquals(cas.Status, 'In Progress'); 
            System.assertEquals(cas.BI_PER_Comentario_nuevo__c, true);
        }

        Test.stopTest();

        //Testing throw exception
        PCA_CaseCommentMethods.reopenPERCases(null);

    }

}