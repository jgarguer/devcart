@isTest
private class PCA_ReclamosPedidosCtrl_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_ReclamosPedidosCtrl class

    History:

    <Date>                  <Author>                <Change Description>
    30/09/2014              Micah Burgos 	        Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

   	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_ShowUserProfileCtrl class

    History:

    <Date>                  <Author>                <Change Description>
    30/09/2014              Micah Burgos 	        Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void PCA_ReclamosPedidosCtrl_TEST() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
    	//Simula Usuario del portal
       	User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());

	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);//new List<String>{Label.BI_Peru};//BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;

            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());
	    	system.runAs(user1){
	    		//END Simula Usuario del portal

	    		BI_TestUtils.throw_exception = false;

				//Carga de datos

	        NE__Order__c ne_order = new NE__Order__c(NE__Type__c = 'Test', NE__Description__c = 'Test');

			insert ne_order;


				BI_Actualizaciones_integracion__c act_integra = new BI_Actualizaciones_integracion__c(
					Name = 'Test',
					BI_Pais__c = lst_pais[0],
					BI_Entidad__c = Label.BI_OrdenesDeCompra,
					BI_UltimaModificacion__c = '25/07/2014'
				);

				insert act_integra;

				//END Carga de datos

				Test.startTest();

				//Simulación de página
	    		PageReference pageRef = new PageReference('PCA_ReclamosPedidos');
   		 		Test.setCurrentPage(pageRef);
   		 		ApexPages.currentPage().getParameters().put('type', 'o');
   		 		//END Simulación de página

	    		PCA_ReclamosPedidosCtrl tmpContr = new PCA_ReclamosPedidosCtrl();
				tmpContr.checkPermissions();
				tmpContr.searchText = 'Test';
				tmpContr.loadInfo();

				tmpContr.changeTab();

				tmpContr.defineRecords();
				tmpContr.defineViews();
				tmpContr.defineSearch();
				tmpContr.getItemPage();
				tmpContr.getHasPrevious();
				tmpContr.getHasNext();
				tmpContr.Previous();
				tmpContr.Next();
				tmpContr.valuesInPage();

				tmpContr.Order();

				tmpContr.searchRecords();
				List<Account> lstAcc=tmpContr.AccPais;
				String lastM=tmpContr.LastMod;
				String fhe=tmpContr.firstHeader;
				String f2=tmpContr.statusHeader;
				String f3=tmpContr.parentNumHeader;
				Map<String, String> fixedStatuses =tmpContr.fixedStatuses;
				PageReference checkPermissions=tmpContr.checkPermissions();

				//system.assertEquals(true, [select Id from BI_Log__c limit 1].isEmpty());
				system.debug(Logginglevel.ERROR, [select Id from BI_Log__c]);

				Test.stopTest();
	    	}
	    }
    }

    static testMethod void PCA_ReclamosPedidosCtrl_TEST2() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
    	//Simula Usuario del portal
       	User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());

       	usr.Pais__c = Label.BI_Peru;
       	update usr;

	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);//new List<String>{Label.BI_Peru};//BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;

            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());

            user1.Pais__c = Label.BI_Peru;
       		update user1;
	    	system.runAs(user1){
	    		//END Simula Usuario del portal

	    		BI_TestUtils.throw_exception = false;

				//Carga de datos

	        NE__Order__c ne_order = new NE__Order__c(NE__Type__c = 'Test', NE__Description__c = 'Test');

			insert ne_order;


				BI_Actualizaciones_integracion__c act_integra = new BI_Actualizaciones_integracion__c(
					Name = 'Test',
					BI_Pais__c = lst_pais[0],
					BI_Entidad__c = Label.BI_OrdenesDeCompra,
					BI_UltimaModificacion__c = '25/07/2014'
				);

				insert act_integra;

				//END Carga de datos

				Test.startTest();

				//Simulación de página
	    		PageReference pageRef = new PageReference('PCA_ReclamosPedidos');
   		 		Test.setCurrentPage(pageRef);
   		 		ApexPages.currentPage().getParameters().put('type', 'o');
   		 		//END Simulación de página

	    		PCA_ReclamosPedidosCtrl tmpContr = new PCA_ReclamosPedidosCtrl();
				tmpContr.checkPermissions();
				tmpContr.searchText = 'En curso';
				tmpContr.loadInfo();
				tmpContr.changeTab();
				tmpContr.searchText = 'In Progress';
				tmpContr.defineRecords();
				tmpContr.defineViews();
				tmpContr.defineSearch();
				tmpContr.getItemPage();
				tmpContr.getHasPrevious();
				tmpContr.getHasNext();
				tmpContr.Previous();
				tmpContr.Next();
				tmpContr.valuesInPage();
				tmpContr.valuesInPage();
				tmpContr.nameObject2 = 'Case';
				tmpContr.Order();
				tmpContr.pageDetail('Test Pais');
				tmpContr.searchRecords();

				//system.assertEquals(true, [select Id from BI_Log__c limit 1].isEmpty());
				system.debug(Logginglevel.ERROR, [select Id from BI_Log__c]);

				Test.stopTest();
	    	}
	    }
    }

    static testMethod void PCA_ReclamosPedidosCtrl_catch_TEST() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        	NETriggerHelper.setTriggerFired('TestException.generate_BILog');
			BI_TestUtils.throw_exception = true;

			Test.startTest();

			PCA_ReclamosPedidosCtrl tmpContr = new PCA_ReclamosPedidosCtrl();
				tmpContr.checkPermissions();
				tmpContr.loadInfo();
				tmpContr.changeTab();
				tmpContr.defineRecords();
				tmpContr.defineViews();
				tmpContr.defineSearch();
				tmpContr.getItemPage();
				tmpContr.getHasPrevious();
				tmpContr.getHasNext();
				tmpContr.Previous();
				tmpContr.Next();
				tmpContr.valuesInPage();
				tmpContr.Order();
				tmpContr.searchRecords();

			system.assertEquals(false, [select Id from BI_Log__c limit 1].isEmpty());

			Test.stopTest();
	}

	static testMethod void PCA_ReclamosPedidosCtrl_catch_TEST3() {

		Test.startTest();

		PCA_ReclamosPedidosCtrl tmpContr = new PCA_ReclamosPedidosCtrl();
		tmpContr.defineRecords();

		Test.stopTest();
	}

/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

    }
*/

}