public without sharing class BI_LineaDeRecambioMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Methods executed by the BI_Linea_de_Recambio__c trigger.
   
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Method for Insert BI_Linea_de_Venta__c
    
    IN:            ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon       Initial version
    11/10/2016        Antonio Pardo     NOMBRE PERSONA validation removed
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void insertLineaDeRecambioIMP(list<BI_Linea_de_Recambio__c> lNew){ 
        
        if (BI_GlobalVariables.stopTriggerIMPExcel == false){
        
            set<string> auxIdNameSol = new set<string>{};
            set<string> auxIdNameMod = new set<string>{};
            set<string> auxIdNameDesMod = new set<string>{};
            set<string> auxIdNameDesSIM = new set<string>{};
            set<string> auxIdSIMTEM = new set<string>{};

            Set <Id> set_sols = new Set <Id>();
            for (BI_Linea_de_Recambio__c aux: lNew){
                system.debug( 'aux.BI_Codigo_de_descuento_de_modelo__c: ' + aux.BI_Codigo_de_descuento_de_modelo__c );
                aux.BI_Cantidad_de_equipos__c = 1;
                
                if(aux.BI_Tipo_de_Contrato__c != null){
                    aux.BI_Tipo_de_Contrato__c = aux.BI_Tipo_de_Contrato__c.split(' - ')[0];
                }
                
                if(aux.BI_Modalidad_de_venta__c != null){
                    aux.BI_Modalidad_de_venta__c = aux.BI_Modalidad_de_venta__c.split(' - ')[0];
                }
                
                if(aux.BI_Catalogo_de_Servicio_1_TEM__c != null){
                    aux.BI_Catalogo_de_Servicio_1_TEM__c = aux.BI_Catalogo_de_Servicio_1_TEM__c.split(' - ')[0];
                }
                
                if(aux.BI_Catalogo_de_Servicio_2_TEM__c != null){
                    aux.BI_Catalogo_de_Servicio_2_TEM__c = aux.BI_Catalogo_de_Servicio_2_TEM__c.split(' - ')[0];
                }
                
                if(aux.BI_Catalogo_de_Servicio_3_TEM__c != null){
                    aux.BI_Catalogo_de_Servicio_3_TEM__c = aux.BI_Catalogo_de_Servicio_3_TEM__c.split(' - ')[0];
                }
                
                if(aux.BI_Catalogo_de_Servicio_4_TEM__c != null){
                    aux.BI_Catalogo_de_Servicio_4_TEM__c = aux.BI_Catalogo_de_Servicio_4_TEM__c.split(' - ')[0];
                }
                
                if(aux.BI_Catalogo_de_Servicio_5_TEM__c != null){
                    aux.BI_Catalogo_de_Servicio_5_TEM__c = aux.BI_Catalogo_de_Servicio_5_TEM__c.split(' - ')[0];
                }
                
                system.debug('auxx: '+aux);
                
                if ((aux.BI_Solicitud__c == null) && aux.BI_Solicitud_TEMP__c != '' && aux.BI_Solicitud_TEMP__c != null){
                    auxIdNameSol.add(aux.BI_Solicitud_TEMP__c);
                }
                if ((aux.BI_Modelo__c == null) && aux.BI_Modelo_TEM__c != '' && aux.BI_Modelo_TEM__c != null){
                    auxIdNameMod.add(aux.BI_Modelo_TEM__c);
                }
                if ((aux.BI_Codigo_de_descuento_de_modelo__c == null) && aux.BI_Codigo_de_Descuento_TEM__c != '' && aux.BI_Codigo_de_Descuento_TEM__c != null){
                    auxIdNameDesMod.add(aux.BI_Codigo_de_Descuento_TEM__c);
                } 
                if ((aux.BI_Codigo_de_descuento_de_SIM__c == null) && aux.BI_Descuento_SIM_TEM__c != '' && aux.BI_Descuento_SIM_TEM__c != null){
                    auxIdNameDesSIM.add(aux.BI_Descuento_SIM_TEM__c);
                }
                if (aux.BI_ID_de_simcard_TEM__c != '' && aux.BI_ID_de_simcard_TEM__c != null){
                    auxIdSIMTEM.add(aux.BI_ID_de_simcard_TEM__c);
                }
                if (aux.BI_Precio_final_de_modelo__c != null){aux.BI_Precio_base_de_modelo__c = aux.BI_Precio_final_de_modelo__c; aux.BI_Precio_modelo_definido_manualment__c = 'Sí';}  //Precio Base Modelo== Precio Final Modelo, y definido manualmente.
                if (aux.BI_Precio_final_de_SIM__c != null){aux.BI_Precio_base_de_SIM__c = aux.BI_Precio_final_de_SIM__c; aux.BI_Precio_SIM_definido_manualmente__c = 'Sí';}                 //Precio Base SIM == Precio Final SIM, y definido manualmente.                              
            }
            //Map <Id, BI_Solicitud_envio_productos_y_servicios__c> map_sols = new Map<Id, BI_Solicitud_envio_productos_y_servicios__c>([SELECT Id, BI_Tipo_solicitud__c FROM BI_Solicitud_envio_productos_y_servicios__c WHERE Id IN :set_sols]);
            //Buscar Id Solicitud para asociar las Lineas de Recambio
            if (auxIdNameSol.size()>0){ 
                list<BI_Solicitud_envio_productos_y_servicios__c> lauxSol = [SELECT Id,Name FROM BI_Solicitud_envio_productos_y_servicios__c  WHERE Id IN :auxIdNameSol OR Name IN :auxIdNameSol ORDER BY Id, Name];
                if (lauxSol.size()>0){
                    for (BI_Solicitud_envio_productos_y_servicios__c auxSol: lauxSol){
                        for (BI_Linea_de_Recambio__c aux: lNew){
                            if ((aux.BI_Solicitud_TEMP__c == String.valueOf(auxSol.Id)) || (aux.BI_Solicitud_TEMP__c == auxSol.Name)){
                                 aux.BI_Solicitud__c = auxSol.Id;
                            }
                        }
                    }
                }
            }
            //****************************************************
            //Buscar Id Modelo para asociar las L�neas de Recambio
            if (auxIdNameMod.size()>0){ 
                list<RecordType> lRT = new list<RecordType>([SELECT Id,DeveloperName FROM RecordType WHERE SobjectType = 'BI_Modelo__c' AND DeveloperName = 'BI_Modelo' ORDER BY Id]);
                if (lRT.size()>0){
                    list<BI_Modelo__c> lauxMod = [SELECT Id,Name,BI_Tipo_de_SIM__c FROM BI_Modelo__c  WHERE RecordTypeId=:lRT[0].Id AND ( Id IN :auxIdNameMod OR Name IN :auxIdNameMod ) ORDER BY Id, Name];
                    list<BI_Modelo__c> lauxSIM = [SELECT Id,Name,BI_Tipo_de_SIM__c,BI_Familia_Local__c FROM BI_Modelo__c  WHERE RecordTypeId=:lRT[0].Id AND ( Id IN :auxIdSIMTEM OR Name IN :auxIdSIMTEM ) ORDER BY Id, Name];
                    
                    if (lauxMod.size()>0){
                        for (BI_Modelo__c auxMod: lauxMod){
                            for (BI_Linea_de_Recambio__c aux: lNew){
                                set_sols.add(aux.BI_Solicitud__c);
                                if ((aux.BI_Modelo_TEM__c == String.valueOf(auxMod.Id)) || (aux.BI_Modelo_TEM__c == auxMod.Name)){
                                     aux.BI_Modelo__c = auxMod.Id;
                                }
                                if(aux.BI_Modelo__c != null){
                                    if(auxMod.BI_Tipo_de_SIM__c != null && aux.BI_Se_incluye_tarjeta_SIM__c == 'Sí' && aux.BI_ID_de_simcard_TEM__c == null)
                                        aux.BI_ID_de_simcard__c = auxMod.BI_Tipo_de_SIM__c;
                                }
                            }
                        }   
                        if (lauxSIM.size()>0){
                            for (BI_Modelo__c auxMod: lauxSIM){
                                for (BI_Linea_de_Recambio__c aux: lNew){                               
                                    if ((aux.BI_ID_de_simcard_TEM__c == String.valueOf(auxMod.Id)) || (aux.BI_ID_de_simcard_TEM__c == auxMod.Name)){                                     
                                         if(aux.BI_ID_de_simcard_TEM__c != null && aux.BI_ID_de_simcard__c == null){
                                            aux.BI_ID_de_simcard__c = auxMod.Id;
                                         }
                                    }
                                }
                            }    
                        }           
                    }
                }
            }
            //****************************************************          
            //Buscar Id Descuento Modelo para asociar las L�neas de Venta
            if (auxIdNameDesMod.size()>0){ 
                list<BI_Descuento_para_modelo__c> lauxDes = [SELECT Id,Name FROM BI_Descuento_para_modelo__c WHERE Id IN :auxIdNameDesMod OR Name IN :auxIdNameDesMod ORDER BY Id, Name];
                if (lauxDes.size()>0){
                    for (BI_Descuento_para_modelo__c auxDes: lauxDes){
                        for (BI_Linea_de_Recambio__c aux: lNew){
                            if ((aux.BI_Codigo_de_Descuento_TEM__c == String.valueOf(auxDes.Id)) || (aux.BI_Codigo_de_Descuento_TEM__c == auxDes.Name)){
                                 aux.BI_Codigo_de_descuento_de_modelo__c = auxDes.Id;
                            }
                        }
                    }               
                }
            }
            //****************************************************  
            //Buscar Id Descuento SIM para asociar las L�neas de Venta
            if (auxIdNameDesSIM.size()>0){ 
                list<BI_Descuento_para_modelo__c> lauxDes = [SELECT Id,Name FROM BI_Descuento_para_modelo__c WHERE Id IN :auxIdNameDesSIM OR Name IN :auxIdNameDesSIM ORDER BY Id, Name];
                if (lauxDes.size()>0){
                    for (BI_Descuento_para_modelo__c auxDes: lauxDes){
                        for (BI_Linea_de_Recambio__c aux: lNew){
                            if ((aux.BI_Descuento_SIM_TEM__c == String.valueOf(auxDes.Id)) || (aux.BI_Descuento_SIM_TEM__c == auxDes.Name)){
                                 aux.BI_Codigo_de_descuento_de_SIM__c = auxDes.Id;
                            }
                        }
                    }               
                }
            }
            //****************************************************  
            Map <Id, BI_Solicitud_envio_productos_y_servicios__c> map_sols = new Map<Id, BI_Solicitud_envio_productos_y_servicios__c>();  
             system.debug('#Debug set_sols '+ set_sols);
            if(!set_sols.isEmpty()) 
                map_sols = new Map<Id, BI_Solicitud_envio_productos_y_servicios__c>([SELECT Id, BI_Tipo_solicitud__c FROM BI_Solicitud_envio_productos_y_servicios__c WHERE Id IN :set_sols]);
             
            for (BI_Linea_de_Recambio__c aux: lNew){
                if(aux.BI_Modelo__c == null || aux.BI_Tipo_de_contrato__c == null || aux.BI_Modalidad_de_venta__c == null || 
                    aux.BI_Razon_para_recambio__c == null){
                        aux.addError('Por favor, rellene los campos obligatorios');
                }else{

                    if (!map_sols.isEmpty() && map_sols.get(aux.BI_Solicitud__c).BI_Tipo_solicitud__c == 'Split billing'){
                        //if(aux.BI_RUT_hijo__c == null || aux.BI_Nombre_persona__c == null){ APC - 11/10/2016 comentado por eHelp 2067623
                            aux.addError('Por favor, rellene los campos obligatorios');
                        //}
                    }else if(!map_sols.isEmpty() && map_sols.get(aux.BI_Solicitud__c).BI_Tipo_solicitud__c != 'Split billing'){
                        if(aux.BI_RUT_hijo__c != null || aux.BI_Codigo_cliente_hijo__c != null){ // || aux.BI_Nombre_persona__c != null comentado por eHelp 2067623
                            aux.addError('Campos erróneos para solicitud de tipo Normal');
                        }
                        if(aux.BI_Otro_plan__c == null  && aux.BI_Plan__c == null){
                            aux.addError('Por favor, rellene los campos obligatorios');
                        }
                    }
                }
                if((aux.BI_Precio_final_de_modelo__c != null && (aux.BI_Codigo_de_descuento_de_modelo__c != null ||
                    aux.BI_Bolsa_de_Dinero_Modelo_TEM_2__c != null)) ||
                    ((aux.BI_Bolsa_de_Dinero_Modelo_TEM_2__c != null || aux.BI_Codigo_de_descuento_de_SIM__c != null) && aux.BI_Precio_final_de_SIM__c != null) ){
                    aux.addError('No puede asignar bolsas ni descuentos si el precio es manual');
                }
            }
        }       
    } 

                 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Method for updating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void actualizarLineaDeServicio(list<BI_Linea_de_Recambio__c> news){
        
        Map <String, String> map_cat_act = new Map <String, String>();
        Map <String, String> map_cat_stat = new Map <String, String>();
        Set <Id> set_rec = new Set <Id>();
        for (BI_Linea_de_Recambio__c aux: news){ 
                    
            if(aux.BI_Catalogo_de_Servicio_1_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_1_TEM__c, aux.BI_Accion_1_TEM__c);   
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_1_TEM__c, aux.BI_Estado_1_TEM__c); 
                set_rec.add(aux.Id); 
            }       
            if(aux.BI_Catalogo_de_Servicio_2_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_2_TEM__c, aux.BI_Accion_2_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_2_TEM__c, aux.BI_Estado_2_TEM__c); 
                set_rec.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_3_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_3_TEM__c, aux.BI_Accion_3_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_3_TEM__c, aux.BI_Estado_3_TEM__c); 
                set_rec.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_4_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_4_TEM__c, aux.BI_Accion_4_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_4_TEM__c, aux.BI_Estado_4_TEM__c);
                set_rec.add(aux.Id);    
            }
            if(aux.BI_Catalogo_de_Servicio_5_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_5_TEM__c, aux.BI_Accion_5_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_5_TEM__c, aux.BI_Estado_5_TEM__c); 
                set_rec.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_6_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_6_TEM__c, aux.BI_Accion_6_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_6_TEM__c, aux.BI_Estado_6_TEM__c);
                set_rec.add(aux.Id);    
            }
            if(aux.BI_Catalogo_de_Servicio_7_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_7_TEM__c, aux.BI_Accion_7_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_7_TEM__c, aux.BI_Estado_7_TEM__c);
                set_rec.add(aux.Id);    
            }
            if(aux.BI_Catalogo_de_Servicio_8_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_8_TEM__c, aux.BI_Accion_8_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_8_TEM__c, aux.BI_Estado_8_TEM__c); 
                set_rec.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_9_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_9_TEM__c, aux.BI_Accion_9_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_9_TEM__c, aux.BI_Estado_9_TEM__c); 
                set_rec.add(aux.Id); 
            }
            if(aux.BI_Catalogo_de_Servicio_10_TEM__c != null){
                map_cat_act.put(aux.BI_Catalogo_de_Servicio_10_TEM__c, aux.BI_Accion_10_TEM__c);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_10_TEM__c, aux.BI_Estado_10_TEM__c);
                set_rec.add(aux.Id); 
            }   
        }

        if(!map_cat_act.keySet().isEmpty()){
            List <BI_Linea_de_Servicio__c> lst_serv =[SELECT Id, BI_Accion__c, BI_Estado_de_linea_de_servicio__c, Name, BI_Modelo__r.Name FROM BI_Linea_de_Servicio__c WHERE BI_Linea_de_recambio__c IN :set_rec];
            if(!lst_serv.isEmpty()){
                for(BI_Linea_de_Servicio__c serv:lst_serv){
                    serv.BI_Accion__c = map_cat_act.get(serv.BI_Modelo__r.Name);
                    serv.BI_Estado_de_linea_de_servicio__c = map_cat_stat.get(serv.BI_Modelo__r.Name);
                }
                update lst_serv;
            }
        }


        
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Method for updating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void actualizarLineaDeServicioStatus(List<BI_Linea_de_Recambio__c> news, List <BI_Linea_de_Recambio__c> olds){        
        
        Map <String, String> map_cat_stat = new Map <String, String>();
        Integer i = 0;
        Set <Id> set_rec = new Set <Id>();
        for (BI_Linea_de_Recambio__c aux: news){              
            if(aux.BI_Estado_1_TEM__c != olds[i].BI_Estado_1_TEM__c){
                set_rec.add(aux.Id);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_1_TEM__c, aux.BI_Estado_1_TEM__c); 
            }else if(aux.BI_Estado_2_TEM__c != olds[i].BI_Estado_2_TEM__c ){
                set_rec.add(aux.Id);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_2_TEM__c, aux.BI_Estado_2_TEM__c); 
            }else if(aux.BI_Estado_3_TEM__c != olds[i].BI_Estado_3_TEM__c ){
                set_rec.add(aux.Id);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_3_TEM__c, aux.BI_Estado_3_TEM__c);
            }else if(aux.BI_Estado_4_TEM__c != olds[i].BI_Estado_4_TEM__c ){
                set_rec.add(aux.Id);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_4_TEM__c, aux.BI_Estado_4_TEM__c); 
            }else if(aux.BI_Estado_5_TEM__c != olds[i].BI_Estado_5_TEM__c ){
                set_rec.add(aux.Id);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_5_TEM__c, aux.BI_Estado_5_TEM__c); 
            }else if(aux.BI_Estado_6_TEM__c != olds[i].BI_Estado_6_TEM__c ){
                set_rec.add(aux.Id);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_6_TEM__c, aux.BI_Estado_6_TEM__c); 
            }else if(aux.BI_Estado_7_TEM__c != olds[i].BI_Estado_7_TEM__c ){
                set_rec.add(aux.Id);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_7_TEM__c, aux.BI_Estado_7_TEM__c); 
            }else if(aux.BI_Estado_8_TEM__c != olds[i].BI_Estado_8_TEM__c ){
                set_rec.add(aux.Id);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_8_TEM__c, aux.BI_Estado_8_TEM__c); 
            }else if(aux.BI_Estado_9_TEM__c != olds[i].BI_Estado_9_TEM__c ){
                set_rec.add(aux.Id);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_9_TEM__c, aux.BI_Estado_9_TEM__c); 
            }else if(aux.BI_Estado_10_TEM__c != olds[i].BI_Estado_10_TEM__c ){
                set_rec.add(aux.Id);
                map_cat_stat.put(aux.BI_Catalogo_de_Servicio_10_TEM__c, aux.BI_Estado_10_TEM__c); 
            }
            i++;
        }  

        if(!map_cat_stat.keySet().isEmpty()){
            List <BI_Linea_de_Servicio__c> lst_serv =[SELECT Id, BI_Accion__c, BI_Estado_de_linea_de_servicio__c, Name, BI_Modelo__r.Name FROM BI_Linea_de_Servicio__c WHERE BI_Linea_de_recambio__c IN :set_rec];
            if(!lst_serv.isEmpty()){
                for(BI_Linea_de_Servicio__c serv:lst_serv){
                    serv.BI_Estado_de_linea_de_servicio__c = map_cat_stat.get(serv.BI_Modelo__r.Name);
                }
                update lst_serv;
            }
        }

        
    }
           
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Method for eliminating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void eliminarLineaDeServicio(list<BI_Linea_de_Recambio__c> lOld){
        
        list<Id> auxList = new list<Id>{};
        for (BI_Linea_de_Recambio__c aux: lOld){
             if (aux.BI_Estado_de_linea_de_recambio__c=='Pendiente')    auxList.add(aux.Id);
             else   aux.addError('No se puede Eliminar porque no se encuentra en estado Pendiente');
        }
        list<BI_Linea_de_Servicio__c> lLineaServicio=[SELECT Id FROM BI_Linea_de_Servicio__c WHERE BI_Linea_de_Recambio__c IN :auxList ORDER BY Id];
        
        if (lLineaServicio.size()>0){
            BI_LineaDeServicioHelper.eliminarLineaDeServicio(lLineaServicio);
        }
    }
    
    
    public static void validateBackOffice(List<BI_Linea_de_Recambio__c> news, List<BI_Linea_de_Recambio__c> olds){
        
        List<Id> lst_ven = new List<Id>();
        
        Integer i = 0;
        for(BI_Linea_de_Recambio__c lven:news){
            if(lven.BI_Estado_de_linea_de_recambio__c == 'Activado' && olds[i].BI_Estado_de_linea_de_recambio__c != 'Activado')
                lst_ven.add(lven.Id);
            i++;
        }
        
        if(!lst_ven.isEmpty())
            BI_Siscel_Helper.BackOfficeRecambio_Mass(UserInfo.getUserEmail(), lst_ven, 0, null);
        
    }
    
    public static void insertLineaDeRecambioValidate(Map<Id, BI_Linea_de_Recambio__c> news){
        
        if(BI_GlobalVariables.stopTriggerIMPExcel == false){
        
            List<Id> lst_ven = new List<Id>();
            lst_ven.addAll(news.keySet());
            
            //FUTURE
            BI_Siscel_Helper.LineaRecambio_Mass(UserInfo.getUserEmail(), lst_ven, 0, null, null);
        }
        
    }
    
    
    public static void updateSol(List<BI_Linea_de_Recambio__c> news, List<BI_Linea_de_Recambio__c> olds){
        
        Set<Id> set_sol = new Set<Id>();
        
        Integer i = 0;
        for(BI_Linea_de_Recambio__c rec:news){
            if(rec.BI_Estado_de_linea_de_recambio__c != olds[i].BI_Estado_de_linea_de_recambio__c || 
               rec.BI_Solicitud_descargada_BackOffice__c != olds[i].BI_Solicitud_descargada_BackOffice__c)
            {
                set_sol.add(rec.BI_Solicitud__c);
            }   
            i++;
        }
        
        if(!set_sol.isEmpty()){
            
            List<BI_Solicitud_envio_productos_y_servicios__c> lst_sol = new List<BI_Solicitud_envio_productos_y_servicios__c>();
            
            Set<String> set_status = new Set<String>{'Activado', 'Desactivado'};
            Set<String> set_status_back = new Set<String>{'Entregado', 'No entregado'};
            
            Map<Id, List<BI_Linea_de_Recambio__c>> map_rec = new Map<Id, List<BI_Linea_de_Recambio__c>>();
            
            for(BI_Linea_de_Recambio__c reca:[select Id, BI_Solicitud_descargada_BackOffice__c, BI_Estado_de_linea_de_recambio__c, BI_Solicitud__c 
                              from BI_Linea_de_Recambio__c
                              where BI_Solicitud__c IN :set_sol order by BI_Solicitud__c asc])
            {
              if(!map_rec.keySet().contains(reca.BI_Solicitud__c)){
                List<BI_Linea_de_Recambio__c> lst_rec = new List<BI_Linea_de_Recambio__c>();
                lst_rec.add(reca);
                map_rec.put(reca.BI_Solicitud__c, lst_rec);
              }else
                map_rec.get(reca.BI_Solicitud__c).add(reca);
            }
            
            for(BI_Solicitud_envio_productos_y_servicios__c sol:[select Id from BI_Solicitud_envio_productos_y_servicios__c where Id IN :set_sol])
            {
                
                if(map_rec.keySet().contains(sol.Id)){
                    
                    Boolean sol_desc = true;
                    Boolean sol_closed = true;
                    Boolean sol_incomplete = true;
                    
                    for(BI_Linea_de_Recambio__c rec:map_rec.get(sol.Id)){
                        if(!rec.BI_Solicitud_descargada_BackOffice__c || (rec.BI_Solicitud_descargada_BackOffice__c 
                            && !set_status_back.contains(rec.BI_Estado_de_linea_de_recambio__c)))
                        {
                            sol_desc = false;
                        }
                        if(!set_status.contains(rec.BI_Estado_de_linea_de_recambio__c))
                            sol_closed = false;
                        
                        if(rec.BI_Estado_de_linea_de_recambio__c == 'Enviando'){
                            sol_desc = true;
                            sol_closed = false;
                            sol_incomplete = false;
                        }
                        if(rec.BI_Estado_de_linea_de_recambio__c != 'No ejecutado' && rec.BI_Estado_de_linea_de_recambio__c != 'Enviando')
                            sol_incomplete = false;
                    }
                    
                    if(sol_incomplete){
                        sol.BI_Estado_de_recambios__c = 'Incompleto';
                        lst_sol.add(sol);
                    }else if(sol_closed){
                        sol.BI_Estado_de_recambios__c = 'Cerrado';
                        lst_sol.add(sol);
                    }else if(sol_desc){
                        sol.BI_Estado_de_recambios__c = 'Activación';
                        lst_sol.add(sol);
                    }
                    
                }
                
            }
            
            try{
                update lst_sol;
            }catch(Exception exc){
                BI_LogHelper.generate_BILog('BI_LineaDeRecambioMethods.updateSol', 'BI_EN', exc, 'Trigger');
            }
            
        }
        
    }
         /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Method for updating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void actualizarHoraSolicitud(list<BI_Linea_de_Recambio__c> news){
        
       
        for (BI_Linea_de_Recambio__c aux: news){ 
              aux.BI_Hora_de_solicitud__c = string.valueof(datetime.now().time());     
              
        }
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Aborda.es
    Description:   Method for updating associated discounts
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/05/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateDesc(list<BI_Linea_de_recambio__c> olds){
        
        Map<Id, Integer> map_desc = new Map<Id, Integer>();
        
        for(BI_Linea_de_recambio__c ven:olds){
            
            if(ven.BI_Codigo_de_descuento_de_modelo__c != null){
                
                if(map_desc.containsKey(ven.BI_Codigo_de_descuento_de_modelo__c))
                    map_desc.put(ven.BI_Codigo_de_descuento_de_modelo__c, map_desc.get(ven.BI_Codigo_de_descuento_de_modelo__c) + 1);
                else
                    map_desc.put(ven.BI_Codigo_de_descuento_de_modelo__c, 1);
                
            }
            
            if(ven.BI_Codigo_de_descuento_de_SIM__c != null){
                
                if(map_desc.containsKey(ven.BI_Codigo_de_descuento_de_SIM__c))
                    map_desc.put(ven.BI_Codigo_de_descuento_de_SIM__c, map_desc.get(ven.BI_Codigo_de_descuento_de_SIM__c) + 1);
                else
                    map_desc.put(ven.BI_Codigo_de_descuento_de_SIM__c, 1);
                
            }
            
        }
        
        if(!map_desc.isEmpty()){
            List<BI_Descuento_para_modelo__c> lst_desc = [select Id, BI_Cantidad_disponible__c from BI_Descuento_para_modelo__c where Id IN :map_desc.keySet()];
            for(BI_Descuento_para_modelo__c descMod:lst_desc){
                if(descMod.BI_Cantidad_disponible__c != null)
                    descMod.BI_Cantidad_disponible__c += map_desc.get(descMod.Id);
            }
            
            
            BI_GlobalVariables.stopTriggerDesc = true;
        
            update lst_desc;
        }
        
    }
}