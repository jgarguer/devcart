@isTest
private class TGS_ModTicketCHG_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena y Marta García
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_ModTicketCHG.createCHG
            
     <Date>                 <Author>                <Change Description>
    21/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void addCHGTest() {
      
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        userTest.TEMPEXT_ID__c = 'chgUser';
        insert userTest;
        
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        System.runAs(userTest){
       	Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
        Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
        Account acCC = [Select ParentId FROM Account WHERE Id=:acLe.ParentId];
        Account nameHo = [Select Name FROM Account WHERE Id=:acCC.ParentId];
            System.debug(nameHo.Name);
            System.debug(acBU.Name);
            
        Contact myContact = TGS_Dummy_Test_Data.dummyContactTGS('Test contact');
        insert myContact;
        
        Id change = [SELECT Id FROM RecordType WHERE Name = 'Change' LIMIT 1].Id;
        Test.startTest();   
         BI_Punto_de_instalacion__c s = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id); 
         System.debug(s.Name);
         BI_Punto_de_instalacion__c Site = [SELECT Name FROM BI_Punto_de_instalacion__c WHERE Name = 'Test site' ];
        System.debug(Site.Name);     
        Case caseTest = new Case(TGS_Impact__c = '1-Extensive/Widespread',
                                TGS_Urgency__c = '1-Critical',
                                Subject = 'Test ticket change',
                                Description = 'Description test ticket change',
                                RecordTypeId = change,
                                Status = 'Assigned',
                                Priority = 'To be Calculated',
                                AccountId = acBU.Id,
                                ContactId = myContact.Id
                                
                                );      
        Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
        insert caseTest;
        Test.stopTest(); 
        
        String numberC = [SELECT CaseNumber FROM Case WHERE Id=:caseTest.Id LIMIT 1].CaseNumber;
        
         
        try{
                Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
                TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                                           'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                                           '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'DUMMY_ROD_GROUP', 'Assigned', '', 'To be Calculated', '',
                                           '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','456');
    
       }catch(Exception e){}
              
       try{
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
            TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
        
            
            
        }catch(Exception e){}
        

      
         try{
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
           TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'test' , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
        
            
            
        }catch(Exception e){
            system.debug('Exception: '+e+' at line: '+e.getLineNumber());
        }   
        
        
        try{
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
            TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'Nivel 1 | CORE', 'In Progress', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
        
            
            
        }catch(Exception e){   
        
        }   
            
        try{
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
            TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_MODIFY_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'Nivel 1 | CORE', 'In Progress', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
        
            
            
        }catch(Exception e){   
        
        } 
            
            try{
                Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
                TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_MODIFY_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                                           'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                                           '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'DUMMY_ROD_GROUP', 'In Progress', '', 'To be Calculated', '',
                                           '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
                
                
                
            }catch(Exception e){} 
            try{
                Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
                TGS_ModTicketCHG.upsertCHG('15489654', '', 'ROD_SF_MODIFY_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                                           'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                                           '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'DUMMY_ROD_GROUP', 'In Progress', '', 'To be Calculated', '',
                                           '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
                
                
                
            }catch(Exception e){}     
           
            try{
                Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
                TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_MODIFY_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                                           'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                                           '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'DUMMY_ROD_GROUP', 'In Progress', '', 'To be Calculated', '',
                                           '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','456');
                
                
                
            }catch(Exception e){} 

            
      } 
    }
    
        static testMethod void addCHGTest2() {
      
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        userTest.TEMPEXT_ID__c = 'chgUser';
        insert userTest;
        Test.startTest();
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        System.runAs(userTest){
       	Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
        Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
        Account acCC = [Select ParentId FROM Account WHERE Id=:acLe.ParentId];
        Account nameHo = [Select Name FROM Account WHERE Id=:acCC.ParentId];
            System.debug(nameHo.Name);
            System.debug(acBU.Name);
            
        Contact myContact = TGS_Dummy_Test_Data.dummyContactTGS('Test contact');
        insert myContact;
        
        Id change = [SELECT Id FROM RecordType WHERE Name = 'Change' LIMIT 1].Id;
           
         BI_Punto_de_instalacion__c s = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id); 
         System.debug(s.Name);
         BI_Punto_de_instalacion__c Site = [SELECT Name FROM BI_Punto_de_instalacion__c WHERE Name = 'Test site' ];
        System.debug(Site.Name);     
        Case caseTest = new Case(TGS_Impact__c = '1-Extensive/Widespread',
                                TGS_Urgency__c = '1-Critical',
                                Subject = 'Test ticket change',
                                Description = 'Description test ticket change',
                                RecordTypeId = change,
                                Status = 'Assigned',
                                Priority = 'To be Calculated',
                                AccountId = acBU.Id,
                                ContactId = myContact.Id
                                
                                );      
        Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
        insert caseTest;
        Test.stopTest(); 
        
        String numberC = [SELECT CaseNumber FROM Case WHERE Id=:caseTest.Id LIMIT 1].CaseNumber;
        
         try{
            //Product_Categorization_Tier2 = null
           //Product_Categorization_Tier3  = null
           Test.setMock(HttpCalloutMock.class, new TGS_MockHttpResponseGenerator());
            TGS_ModTicketCHG.upsertCHG('15489654', numberC,  'ROD_SF_CREATE_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c, nameHo.Name, site.Name, acBU.Name , '', 'Connection Problems', 'Other', '', 'Enterprise Managed Mobility', '', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
        
            
            
        }catch(Exception e){}     
        
        try{
            //Product_Categorization_Tier3 = null
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
            TGS_ModTicketCHG.upsertCHG('15489654', numberC,  'ROD_SF_CREATE_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c, nameHo.Name, site.Name, acBU.Name , '', 'Connection Problems', 'Other', '', 'Enterprise Managed Mobility', 'Universal WIFI', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
        
            
            
        }catch(Exception e){}  
            
        try{
            //Direct_Contact_Department
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
            TGS_ModTicketCHG.upsertCHG('15489654', numberC,  'ROD_SF_CREATE_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c, nameHo.Name, site.Name, '' , '', 'Connection Problems', 'Other', '', 'Enterprise Managed Mobility', 'Universal WIFI', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
        
            
            
        }catch(Exception e){}  
     
       try{
            //Company 
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
            TGS_ModTicketCHG.upsertCHG('15489654', numberC,  'ROD_SF_CREATE_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c, '', site.Name, acBU.Name , '', 'Connection Problems', 'Other', '', 'Enterprise Managed Mobility', 'Universal WIFI', 'Universal WIFI', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
        
            
            
        }catch(Exception e){}  
          
        try{
          Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
          TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE'  , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  '', site.Name, '', '', '', '', '', '', '', '', acLe.Name, 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
         
        }catch(Exception e){}
        
        try{
          Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
          TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE'  , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  '', site.Name, '', '', '', '', '', '', '', '', acLe.Name, '', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
         
        }catch(Exception e){}
            
        try{
          Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
          TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE'  , '', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  '', site.Name, '', '', '', '', '', '', '', '', acLe.Name, 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
         
        }catch(Exception e){}    
        
        
      }    
        
    }
    
     static testMethod void addCHGTest3() {
      
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        userTest.TEMPEXT_ID__c = 'chgUser';
        insert userTest;
        
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        System.runAs(userTest){
       	Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
        Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
        Account acCC = [Select ParentId FROM Account WHERE Id=:acLe.ParentId];
        Account nameHo = [Select Name FROM Account WHERE Id=:acCC.ParentId];
            System.debug(nameHo.Name);
            System.debug(acBU.Name);
            
        Contact myContact = TGS_Dummy_Test_Data.dummyContactTGS('Test contact');
        insert myContact;
        
        Id change = [SELECT Id FROM RecordType WHERE Name = 'Change' LIMIT 1].Id;
        Test.startTest();   
         BI_Punto_de_instalacion__c s = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id); 
         System.debug(s.Name);
         BI_Punto_de_instalacion__c Site = [SELECT Name FROM BI_Punto_de_instalacion__c WHERE Name = 'Test site' ];
        System.debug(Site.Name);     
        Case caseTest = new Case(TGS_Impact__c = '1-Extensive/Widespread',
                                TGS_Urgency__c = '1-Critical',
                                Subject = 'Test ticket change',
                                Description = 'Description test ticket change',
                                RecordTypeId = change,
                                Status = 'Assigned',
                                Priority = 'To be Calculated',
                                AccountId = acBU.Id,
                                ContactId = myContact.Id
                                
                                );      
        Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
        insert caseTest;
        Test.stopTest(); 
        
        String numberC = [SELECT CaseNumber FROM Case WHERE Id=:caseTest.Id LIMIT 1].CaseNumber;
        
		try{
          Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
          TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE'  , '1-Extensive/Widespread', '',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  '', site.Name, '', '', '', '', '', '', '', '', acLe.Name, 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
         
        }catch(Exception e){}    
            
        try{
          Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
          TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE'  , '1-Extensive/Widespread', '2-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  '', site.Name, '', '', '', '', '', '', '', '', acLe.Name, 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
         
        }catch(Exception e){}  
            
         try{
          Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
          TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE'  , '2-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  '', site.Name, '', '', '', '', '', '', '', '', acLe.Name, 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
         
        }catch(Exception e){}     
            
        try{
          Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
          TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE'  , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', '', 'Web',
                            '', userTest.TEMPEXT_ID__c,  '', site.Name, '', '', '', '', '', '', '', '', acLe.Name, 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
         
        }catch(Exception e){}
     
      }    
        
    }
    
    static testMethod void addCHGTest4() {
      
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        userTest.TEMPEXT_ID__c = 'chgUser';
        insert userTest;
        
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        System.runAs(userTest){
       	Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
        Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
        Account acCC = [Select ParentId FROM Account WHERE Id=:acLe.ParentId];
        Account nameHo = [Select Name FROM Account WHERE Id=:acCC.ParentId];
            System.debug(nameHo.Name);
            System.debug(acBU.Name);
            
        Contact myContact = TGS_Dummy_Test_Data.dummyContactTGS('Test contact');
        insert myContact;
        
        Id change = [SELECT Id FROM RecordType WHERE Name = 'Change' LIMIT 1].Id;
        Test.startTest();   
         BI_Punto_de_instalacion__c s = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id); 
         System.debug(s.Name);
         BI_Punto_de_instalacion__c Site = [SELECT Name FROM BI_Punto_de_instalacion__c WHERE Name = 'Test site' ];
        System.debug(Site.Name);     
        Case caseTest = new Case(TGS_Impact__c = '1-Extensive/Widespread',
                                TGS_Urgency__c = '1-Critical',
                                Subject = 'Test ticket change',
                                Description = 'Description test ticket change',
                                RecordTypeId = change,
                                Status = 'Assigned',
                                Priority = 'To be Calculated',
                                AccountId = acBU.Id,
                                ContactId = myContact.Id
                                
                                );      
        Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
        insert caseTest;
        Test.stopTest(); 
        
        String numberC = [SELECT CaseNumber FROM Case WHERE Id=:caseTest.Id LIMIT 1].CaseNumber;
        
         
              
                  
        try{
            //reporSource=Email
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
            TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Email',
                            '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
        
            
            
        }catch(Exception e){}
            
       try{
            //reporSource=Phone
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
            TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Phone',
                            '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
        
            
            
        }catch(Exception e){}     
        
        try{
            //reporSource=Phone
            TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_CREATE_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Proactive',
                            '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
        
            
            
        }catch(Exception e){}         
       
      
            
            
       try{
            //Product_Categorization_Tier
            //Categorization_Tier
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
            TGS_ModTicketCHG.upsertCHG('15489654', numberC,  'ROD_SF_CREATE_CHANGE' , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test creation change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c, nameHo.Name, site.Name, acBU.Name , '', 'Connection Problems', 'Other', '', 'Enterprise Managed Mobility', 'Universal WIFI', 'Universal WIFI', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '',
                                    '2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','2017-07-27T17:48:00+02:00','');
        
            
            
        }catch(Exception e){}  
     
      } 
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena y Marta García
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_ModTicketCHG.updateCHG
            
     <Date>                 <Author>                <Change Description>
    21/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*static testMethod void updateCHGTest() {
        
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
        }
       
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        userTest.TEMPEXT_ID__c = 'chgUser';
        insert userTest;
        
        
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        System.runAs(userTest){
        
        Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
        Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
        Account acCC = [Select ParentId FROM Account WHERE Id=:acLe.ParentId];
        Account nameHo = [Select Name FROM Account WHERE Id=:acCC.ParentId];
        Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceSiteImpl());
        BI_Punto_de_instalacion__c site = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id);  
        Test.startTest();   
        	Contact myContact = TGS_Dummy_Test_Data.dummyContactTGS('Test contact');
        	insert myContact;
        	Id change = [SELECT Id FROM RecordType WHERE Name = 'Change' LIMIT 1].Id;
        	Case caseTest = new Case(TGS_Impact__c = '1-Extensive/Widespread',
                                TGS_Urgency__c = '1-Critical',
                                Subject = 'Test ticket change',
                                Description = 'Description test ticket change',
                                RecordTypeId = change,
                                Status = 'Assigned',
                                Priority = 'To be Calculated',
                                AccountId = acBU.Id,
                                ContactId = myContact.Id
                                );
        
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
        	insert caseTest;
        
		Test.stopTest();       
        

       
        String numberC = [SELECT CaseNumber FROM Case WHERE Id=:caseTest.Id LIMIT 1].CaseNumber;
        
       
        try{
            
          TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_MODIFY_CHANGE'  , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test modification change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', acBU.Name, 'Nivel 1 | CORE', 'Assigned', 'Status reason test', 'To be Calculated', '');
        
            
            
        }catch(Exception e){
           
        }   
               
        try{
            
          TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'test'  , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test modification change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '');
        
            
            
        }catch(Exception e){
         	system.debug('Exception: '+e+' at line: '+e.getLineNumber());   
        }   
		
            
         try{
            
          TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_MODIFY_CHANGE'  , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test modification change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'Nivel 1 | CORE', Constants.CASE_STATUS_RESOLVED, '', 'To be Calculated', '');
        
            
            
        }catch(Exception e){
            
        }   
         
        try{
            
          TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_MODIFY_CHANGE'  , '1-Extensive/Widespread', '1-Critical',
                            'Test ticket change', 'Test modification change ticket - remedy integration', 'Web',
                            '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', 'test');
        
            
            
        }catch(Exception e){}     
        
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////
/////   
            
      }
    }*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena y Marta García
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_ModTicketCHG.updateCHG
            
     <Date>                 <Author>                <Change Description>
    21/05/2015              Marta Laliena           Initial Version
    06/08/2016  Humberto Nunes        Se coloco el NETriggerHelper.setTriggerFired('BI_Case'); para que no se ejecutara ya que da too many querys... 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*static testMethod void updateCHGTest2() {
        
        NETriggerHelper.setTriggerFired('BI_Case');
        Test.startTest(); 
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
        }
       
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        userTest.TEMPEXT_ID__c = 'chgUser';
        insert userTest;
        
        
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        System.runAs(userTest){
        
        Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
        Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
        Account acCC = [Select ParentId FROM Account WHERE Id=:acLe.ParentId];
        Account nameHo = [Select Name FROM Account WHERE Id=:acCC.ParentId];
        Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceSiteImpl());
        BI_Punto_de_instalacion__c site = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id);   
        
          
        	Contact myContact = TGS_Dummy_Test_Data.dummyContactTGS('Test contact');
        	insert myContact;
        	Id change = [SELECT Id FROM RecordType WHERE Name = 'Change' LIMIT 1].Id;
        	Case caseTest = new Case(TGS_Impact__c = '1-Extensive/Widespread',
                                TGS_Urgency__c = '1-Critical',
                                Subject = 'Test ticket change',
                                Description = 'Description test ticket change',
                                RecordTypeId = change,
                                Status = 'Assigned',
                                Priority = 'To be Calculated',
                                AccountId = acBU.Id,
                                ContactId = myContact.Id
                                );
        
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
        	insert caseTest;

        
		Test.stopTest();       
        String numberC = [SELECT CaseNumber FROM Case WHERE Id=:caseTest.Id LIMIT 1].CaseNumber;
            
       
            caseTest.AccountId = acCC.ParentId;
            update caseTest;
            try{
                
              TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_MODIFY_CHANGE'  , '1-Extensive/Widespread', '1-Critical',
                                'Test ticket change', 'Test modification change ticket - remedy integration', 'Web',
                                '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '');
             
            }catch(Exception e){}
               
              
            caseTest.AccountId = acLe.Id;
            //update caseTest;
            try{
                
              TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_MODIFY_CHANGE'  , '1-Extensive/Widespread', '1-Critical',
                                'Test ticket change', 'Test modification change ticket - remedy integration', 'Web',
                                '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, '', '', '', '', '', '', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '');
             
            }catch(Exception e){}   
            
            
            NE__OrderItem__c orderItem = TGS_Dummy_Test_Data.dummyConfiguration();   
            
            NE__OrderItem__c orderItemName = [Select Id, Name FROM NE__OrderItem__c WHERE Id=:orderItem.Id];            
            
            try{
              //CI  
              TGS_ModTicketCHG.upsertCHG('15489654', numberC, 'ROD_SF_MODIFY_CHANGE'  , '1-Extensive/Widespread', '1-Critical',
                                'Test ticket change', 'Test modification change ticket - remedy integration', 'Web',
                                '', userTest.TEMPEXT_ID__c,  nameHo.Name, site.Name, acBU.Name, orderItemName.Name, '', '', '', '', '', '', '', 'Nivel 1 | CORE', 'Assigned', '', 'To be Calculated', '');
             
            }catch(Exception e){} 
       }
	}*/
}