global class BI_assignEntitlementsBatch implements Database.Batchable<sObject>, Schedulable {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Gawron, Julián
     Company:       Accenture
     Description:   Job to Update Entitlements on Cases 
     Test Class:    BI_assignEntitlementsBatch_TEST
    
     History:
     
     <Date>                     <Author>                    <Change Description>
    28/02/2017                  Gawron, Julián              Initial version
    29/03/2017                  Gawron, Julián              Adding Label BI_assignEntitlement_number
    11/04/2017                  Sevilla, Alvaro             Addition of filters to the query and subject's email
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    
    public String query;
    public Integer batch_size = 200;
    static List<String> lst_errors = new List<String>();

    global BI_assignEntitlementsBatch() {
        try{
            batch_size = integer.ValueOf(Label.BI_assignEntitlement_number); 
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_assignEntitlementsBatch()', 'BI_EN', Exc, 'Trigger');
        }


        query = 'SELECT Id, BI_Country__c, BI_Product_Service__c, BI_Line_of_Business__c, BI2_PER_Descripcion_de_producto__c, BI2_PER_Territorio_caso__c, '+
                'TGS_Disputed_number_of_lines__c, AccountId, Reason, BI_VEN_Tipificacion__c, BI2_per_subtipologia__c, BI2_per_tipologia__c ' +
                'from Case ' +
                'where EntitlementId = null and ' +
                '((Reason = \'Commercial Request\' and ' + 
                '(BI2_PER_Tipologia__c = \'Alta / Nuevo servicio\' or BI2_PER_Tipologia__c = \'Bajas de servicio\' or BI2_PER_Tipologia__c = \'Rutina con trabajo fisico\' or BI2_PER_Tipologia__c = \'Rutina sin trabajo fisico\' or BI2_PER_Tipologia__c = \'Cotizaciones\')) or '+ 
                '(Reason = \'Billing Complaint\' and (BI2_PER_Tipologia__c = \'Regulatorio/Monto menor a 0.5 UIT\' or BI2_PER_Tipologia__c = \'Regulatorio/Monto mayor a 0.5 UIT\' or BI2_PER_Tipologia__c = \'Apelación\' or BI2_PER_Tipologia__c = \'Queja\' or BI2_PER_Tipologia__c = \'Averias\' or BI2_PER_Tipologia__c = \'Provisión\' or BI2_PER_Tipologia__c = \'Solución anticipada (SAR)\' or BI2_PER_Tipologia__c = \'Proactivo/ Refacturaciones\' or BI2_PER_Tipologia__c = \'Proactivo/ Ajustes\')) or '+
                '(Reason = \'Administrative Request\' and ( BI2_PER_Tipologia__c = \'Facturación\'))) and '+
                'Reason != \'Commercial Query\' and RecordType.DeveloperName = \'BI2_caso_comercial\' and BI_Country__c = \'Peru\' and CreatedDate >= LAST_N_DAYS:2';

                System.debug('### query--> ' + query);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(SchedulableContext sc){
        BI_assignEntitlementsBatch obj = new BI_assignEntitlementsBatch();
        Database.executeBatch(obj,batch_size);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('### query--> ' + query);
        System.debug('### scope--> ' + scope);
        assignEntitlements(scope);

    }
    
    global void finish(Database.BatchableContext BC) {
        
            Messaging.SingleEmailMessage mailOwner = new Messaging.SingleEmailMessage();
                String body = '<h1>Listado de errores</h1>' + String.join(lst_errors,'<br>');
                mailOwner.setTargetObjectId(UserInfo.getUserId());
                mailOwner.setHtmlBody(body);
                mailOwner.setSubject('Batch de Asignacion de ANS a Casos');

                mailOwner.setSenderDisplayName('Batch de Asignacion de ANS a Casos');
                mailOwner.setSaveAsActivity(false);
            if(!Test.isRunningTest()){   
                Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mailOwner});
            }
    }
    


    public static void assignEntitlements(List<sObject> scope){

        //rellenamos un mapa con los campos que se utilizan para realizar las comparaciones en cada país
        System.debug('assignEntitlements scope--> ' + scope);
        List<sObject> lst_cases = scope;
        if (!lst_cases.isEmpty()) {

            List <BI_Mapeo_criterios_asignacion__c> lst_mca =  BI_Mapeo_criterios_asignacion__c.getAll().values();
            Map <String,String> map_mca = new Map <String,String>();

            for(BI_Mapeo_criterios_asignacion__c mca : lst_mca){

                map_mca.put(mca.BI_Country__c + '-' + mca.BI_Nombre_API_Criterios_de_asignacion__c, mca.BI_Nombre_API_Casos__c);
            }

            Set<String> setConcat = new Set<String>();
            //List<sObject> lstCase = new List<sObject>();
            Set<Id> set_cases = new Set<Id>();
           
            for (sObject currentCase : lst_cases) {
                
                //lstCase.add(currentCase);
                //Concatenamos el campo Country, Reason y la Tipología del país del caso
                if(map_mca.containsKey(String.valueOf(currentCase.get('BI_Country__c')) + '-BI2_PER_Tipologia__c')){
                    setConcat.add(String.valueOf(currentCase.get('BI_Country__c')) + '-' + String.valueOf(currentCase.get('Reason')) + '-' + String.valueOf(currentCase.get(map_mca.get(String.valueOf(currentCase.get('BI_Country__c')) + '-BI2_PER_Tipologia__c'))));
                }
                
            }

            System.debug('assignEntitlements-Referencia-->'+setConcat);

            
            Map<String, List<BI_Criterio_de_asignacion__c>> mapCriterios = new Map<String, List<BI_Criterio_de_asignacion__c>>();
            for (BI_Criterio_de_asignacion__c criterio : [select BI2_PER_Descripcion_de_producto__c, BI2_PER_Limite_inferior__c, BI2_PER_Limite_superior__c, 
                                                          BI2_PER_Linea_de_Negocio__c, BI2_PER_Motivo_del_caso__c, BI2_PER_Producto_Servicio__c, BI2_PER_SubTipologia__c, 
                                                          BI2_PER_Territorio__c, BI2_PER_Tipologia__c, BI_Country__c, BI_Line_of_Business__c,
                                                          BI_Proceso_de_asignacion__r.Name, BI_Referencia_del_criterio__c, BI_Segment__c, 
                                                          BI_Typology__c, Id, Name, BI2_PER_Motivo_Tipologia__c
                                                          FROM BI_Criterio_de_asignacion__c where BI2_PER_Motivo_Tipologia__c IN :setConcat]) 
            {

                System.debug('assignEntitlements-asignaciones-->'+criterio);
                
                if (mapCriterios.containsKey(criterio.BI2_PER_Motivo_Tipologia__c)) {
                    
                    mapCriterios.get(criterio.BI2_PER_Motivo_Tipologia__c).add(criterio);
                    
                } else {
                    
                    mapCriterios.put(criterio.BI2_PER_Motivo_Tipologia__c, new List<BI_Criterio_de_asignacion__c>{criterio});
                    
                }
                
                System.debug('assignEntitlements-mapCriterios-->'+criterio);
            }
            
            system.debug('mapCriterios: ' + mapCriterios);
            
            if (!mapCriterios.isEmpty()) {
                
                Map<sObject, String> mapCaseCriterio = new Map<sObject, String>();
                Set<String> setNameEntitlement = new Set<String>();
                Set<String> setIDCuentaCaso = new Set<String>();
                
                for (sObject currentCase : lst_cases) {

                    system.debug('CURRENTCASE: ' + currentCase);
                 
                    String tipologia = map_mca.get(String.valueOf(currentCase.get('BI_Country__c')) + '-BI2_PER_Tipologia__c');
                    String subtipologia = map_mca.get(String.valueOf(currentCase.get('BI_Country__c')) + '-BI2_PER_SubTipologia__c');
                    
        
                    
                    String concat = String.valueOf(currentCase.get('BI_Country__c')) + '-' + String.valueOf(currentCase.get('Reason')) + '-' + String.valueOf(currentCase.get(tipologia));
                    
                    system.debug('CONCAT: ' + concat);
                    
                    if (!mapCriterios.containsKey(concat)) {
                        
                        //Error: no encuentra
                        //currentCase.addError('Error: No encuentra SLA'); 
                        currentCase.put('EntitlementId', null); 
                        
                    } else {
                        
                        Boolean hasCriterio = false;
                        
                        for (BI_Criterio_de_asignacion__c criterio : mapCriterios.get(concat)) {
                            
                            system.debug('CRITERIO: ' + criterio);
                             
                            if (String.valueOf(currentCase.get(subtipologia)) == null || (String.valueOf(currentCase.get(subtipologia)) != null && String.valueOf(currentCase.get(subtipologia)) == criterio.BI2_PER_SubTipologia__c) || criterio.BI2_PER_SubTipologia__c == null) {
                                
                                if (String.valueOf(currentCase.get('BI_Product_Service__c')) == null || (String.valueOf(currentCase.get('BI_Product_Service__c')) != null && String.valueOf(currentCase.get('BI_Product_Service__c')) == criterio.BI2_PER_Producto_Servicio__c) || criterio.BI2_PER_Producto_Servicio__c == null) {
                                    
                                    if (String.valueOf(currentCase.get('BI_Line_of_Business__c')) == null || (String.valueOf(currentCase.get('BI_Line_of_Business__c')) != null && String.valueOf(currentCase.get('BI_Line_of_Business__c')) == criterio.BI2_PER_Linea_de_Negocio__c) || criterio.BI2_PER_Linea_de_Negocio__c == null) {
                                        
                                        if (String.valueOf(currentCase.get('BI2_PER_Descripcion_de_producto__c')) == null || (String.valueOf(currentCase.get('BI2_PER_Descripcion_de_producto__c')) != null && String.valueOf(currentCase.get('BI2_PER_Descripcion_de_producto__c')) == criterio.BI2_PER_Descripcion_de_producto__c) || criterio.BI2_PER_Descripcion_de_producto__c == null) {
                                            
                                            if (String.valueOf(currentCase.get('BI2_PER_Territorio_caso__c')) == null || (String.valueOf(currentCase.get('BI2_PER_Territorio_caso__c')) != null && String.valueOf(currentCase.get('BI2_PER_Territorio_caso__c')) == criterio.BI2_PER_Territorio__c) || criterio.BI2_PER_Territorio__c == null)
                                                
                                            {
                                                
                                                if (String.valueOf(currentCase.get('TGS_Disputed_number_of_lines__c')) == null ||
                                                    (criterio.BI2_PER_Limite_inferior__c == null && criterio.BI2_PER_Limite_superior__c == null)) 
                                                {
                                                    
                                                    if (hasCriterio) {
                                                        mapCaseCriterio.put(currentCase, null);
                                                        break;
                                                    } else {
                                                        mapCaseCriterio.put(currentcase, criterio.BI_Proceso_de_asignacion__r.Name);
                                                    }
                                                    
                                                } else {
                                                    
                                                    if (criterio.BI2_PER_Limite_inferior__c == null && criterio.BI2_PER_Limite_superior__c != null) {
                                                        
                                                        if (Integer.valueOf(currentCase.get('TGS_Disputed_number_of_lines__c')) <= criterio.BI2_PER_Limite_superior__c) {
                                                            
                                                            if (hasCriterio) {
                                                                mapCaseCriterio.put(currentCase, null);
                                                                break;
                                                            } else {
                                                                mapCaseCriterio.put(currentcase, criterio.BI_Proceso_de_asignacion__r.Name);
                                                            }       
                                                            
                                                        }
                                                        
                                                    } else if (criterio.BI2_PER_Limite_inferior__c != null && criterio.BI2_PER_Limite_superior__c == null) {
                                                        
                                                        if (Integer.valueOf(currentCase.get('TGS_Disputed_number_of_lines__c')) >= criterio.BI2_PER_Limite_inferior__c) {
                                                            
                                                            if (hasCriterio) {
                                                                mapCaseCriterio.put(currentCase, null);
                                                                break;
                                                            } else {
                                                                mapCaseCriterio.put(currentcase, criterio.BI_Proceso_de_asignacion__r.Name);
                                                            }
                                                            
                                                        }
                                                        
                                                    } else {
                                                        
                                                        if (Integer.valueOf(currentCase.get('TGS_Disputed_number_of_lines__c')) >= criterio.BI2_PER_Limite_inferior__c && Integer.valueOf(currentCase.get('TGS_Disputed_number_of_lines__c')) <= criterio.BI2_PER_Limite_superior__c) {
                                                            
                                                            if (hasCriterio) {
                                                                mapCaseCriterio.put(currentCase, null);
                                                                break;
                                                            } else {
                                                                mapCaseCriterio.put(currentcase, criterio.BI_Proceso_de_asignacion__r.Name);
                                                            }
                                                            
                                                        }                                                       
                                                    }
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                        system.debug('mapCaseCriterio: ' + mapCaseCriterio);
                        
                        if (mapCaseCriterio.containsKey(currentcase)) {
                            
                            if (mapCaseCriterio.get(currentcase) == null) {
                                
                                //Error: más de uno
                                currentCase.addError('Error: Se han encontrado varios SLA\'s');

                                
                            } else {
                                
                                //Proceso normal
                                setNameEntitlement.add(mapCaseCriterio.get(currentcase));
                                setIDCuentaCaso.add(String.valueOf(currentCase.get('AccountId')));
                            }
                            
                        } else {
                            
                            //Error: no encuentra
                            //currentCase.addError('Error: No encuentra SLA');
                            currentCase.put('EntitlementId', null);
                        } 
                    }  
                }
                
                if (!setNameEntitlement.isEmpty()) {
                    System.debug('--->setIDCuentaCaso-->'+setIDCuentaCaso);
                    List<Entitlement> lstEntitlement = [select Id, Name, AccountId from Entitlement where Name IN : mapCaseCriterio.values() and AccountId IN: setIDCuentaCaso and Status = 'Active'];
                        
                    system.debug('lstEntitlement: ' + lstEntitlement); 
                        
                    for (sObject currentCase : mapCaseCriterio.keySet()) {
                        
                        system.debug('Entra for');
                        
                        if (mapCaseCriterio.get(currentCase) != null) {
                            
                            system.debug('Entra if');
                            
                            //currentCase.EntitlementId = null;
                            Boolean nullValue = true;
                            for (Entitlement entitlement : lstEntitlement) {
                                
                                system.debug('Entra for entitlements');
                                
                                system.debug(mapCaseCriterio.get(currentCase));
                                system.debug(String.valueOf(currentCase.get('AccountId')));
                                system.debug(entitlement.AccountId);
                            
                                if (String.valueOf(currentCase.get('AccountId')) == entitlement.AccountId &&
                                    mapCaseCriterio.get(currentCase) == entitlement.Name) 
                                {
                                    
                                    system.debug('Entra entitlements '+entitlement.Id);
                                    currentCase.put('EntitlementId', entitlement.Id);
                                    nullValue = false;
                                    
                                }
                            
                            }
                            
                            if (nullValue == true) {
                                currentCase.put('EntitlementId', null);
                                System.debug('No se encontro ANS para la cuenta');
                            }
                            
                        }
                        
                    }   
                        
                } 
            }
            // 23/01/2017 se comento la validacion de Bypass para este casos
            Map<Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(Userinfo.getUserId(), true, true, false, false);
            try{


                Database.SaveResult[] lsr = database.update( lst_cases, false );
        
                 for( Database.SaveResult sr : lsr )
                   {
                       if(!sr.isSuccess() )
                       {
                           Database.Error err = sr.getErrors()[0];
                           string errorDetalles = '';

                           for( Database.Error errt:sr.getErrors() )
                               errorDetalles += errt.getMessage();

                           String textoMSG = 'Nro ID:' + sr.getId() + ' Detalles: ' + errorDetalles;    
                           lst_errors.add(textoMSG);
                       }           
                   }


                System.debug('lst_cases para actualizar:' + lst_cases);
            }catch(Exception e){
            System.debug('OCURRIO UNA EXCEPCION EN ETITLEMENT ASI: '+e);
            }

            finally{

                if(mapa != null){

                    BI_MigrationHelper.disableBypass(mapa);
                }
            }
            //fin cambio validacion bypasss
        }
    }

}