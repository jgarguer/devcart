@RestResource(urlMapping='/SiteManagement/v1/sites/*')
global class BI_AddressRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Class for Customer - Sites Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    29/05/2017   Ignacio G Schuhmacher  Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Creates a new Site in the server receiving the request.
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    29/05/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPut
	global static FS_TGS_RestWrapperFullStack.SiteRequestType updateSite() {
		FS_TGS_RestWrapperFullStack.SiteRequestType response;
		try{			
				RestRequest req = RestContext.request;
			
				Blob blob_json = req.requestBody; 
			
				String string_json = blob_json.toString(); 
				System.debug(string_json);
				FS_TGS_RestWrapperFullStack.SiteRequestType conDetType = (FS_TGS_RestWrapperFullStack.SiteRequestType) JSON.deserialize(string_json, FS_TGS_RestWrapperFullStack.SiteRequestType.class);

				// URI PARAMETERS
				if(RestContext.request.requestURI.split('/').size() >= 5){
					String externalAddress = RestContext.request.requestURI.split('/')[4];
					if(externalAddress != null || externalAddress != ''){
						System.debug('SiteId : ' + externalAddress);
						BI_Sede__c sfAddress = BI_RestHelper.getRelatedAddress(externalAddress);
						
						if(sfAddress != null){						

							sfAddress.Name = conDetType.address.addressName;
							sfAddress.BI_Direccion__c = conDetType.name;
							sfAddress.BI_Numero__c = conDetType.address.addressNumber.value;
							sfAddress.BI_Country__c = conDetType.address.country;
							sfAddress.BI_Provincia__c = conDetType.address.region;
							sfAddress.BI_Localidad__c = conDetType.address.locality;
							sfAddress.BI_Piso__c = conDetType.address.floor.value;
							sfAddress.BI_Apartamento__c = conDetType.address.apartment.value;
							sfAddress.BI_Codigo_postal__c = conDetType.address.postalCode;

							update sfAddress;				
							RestContext.response.statuscode = 200;
							RestContext.response.headers.put('Location','https://'+System.URL.getSalesforceBaseURL().getHost()+'/SiteManagement/v1/sites/' + externalAddress);
							if (String.IsNotBlank(RestContext.request.headers.get('UNICA-ServiceId'))) RestContext.response.headers.put('UNICA-ServiceId', RestContext.request.headers.get('UNICA-ServiceId'));
	           				if (String.IsNotBlank(RestContext.request.headers.get('UNICA-Application'))) RestContext.response.headers.put('UNICA-Application', RestContext.request.headers.get('UNICA-Application'));
							
						} else {
								RestContext.response.statuscode = 404;//BAD_REQUEST
								RestContext.response.headers.put('errorMessage', 'Resource Address : ' + externalAddress + ' does not exist');//SVC 1006
						}
					} else {
						//No viene id de site en la uri
						RestContext.response.statuscode = 404;//BAD_REQUEST
						RestContext.response.headers.put('errorMessage', 'Invalid Request URI');
					}
					
				} else {
					//URI MAL FORMADA, NO CONTIENE EL NUMERO DE ELEMENTOS QUE TIENEN QUE LLEGA
					RestContext.response.statuscode = 400;//BAD_REQUEST
					RestContext.response.headers.put('errorMessage', 'Invalid Request URI');
				}
			//}
			
		}catch(Exception exc){
			System.debug('Exception : '+ exc);
			RestContext.response.statuscode = 500;
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_RestV2UpdateSites.updateSite', 'BI_EN', Exc, 'Web Service');
			
		}
		return response;
		
	}
}