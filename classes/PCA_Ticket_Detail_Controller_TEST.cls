@isTest
private class PCA_Ticket_Detail_Controller_TEST {
	
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Marta Laliena
	Company:       Deloitte
	Description:   Test class to manage the coverage code for PCA_Ticket_Detail_Controller class 

	<Date> 					<Author> 				<Change Description>
	02/03/2015      		Marta Laliena    		Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Marta Laliena
	Company:       Deloitte
 	Description:   Test method to manage the code coverage for PCA_Ticket_Detail_Controller.init
		    
 	 <Date> 				<Author> 				<Change Description>
	02/03/2015      		Marta Laliena    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 
 	static testMethod void getloadInfo() {
        User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            
            Id incident = [SELECT Id FROM RecordType WHERE Name = 'Incident' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = incident, Subject = 'Test controller', TGS_Invoice_Number__c='2',TGS_Disputed_number_of_lines__c=3,TGS_Op_Commercial_Tier_1__c= 'Name',TGS_Product_Tier_1__c= 'Name');
       		insert caseTest;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            System.assertNotEquals(caseTest.Id, null);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            System.assertEquals(true, controller.visibility);
            System.assertNotEquals(null, controller.currentCase);
            System.assertEquals(caseTest.Id, controller.currentCase.Id);
            Test.stopTest();
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Marta Laliena
	Company:       Deloitte
 	Description:   Test method to manage the code coverage for PCA_Ticket_Detail_Controller.crearWorkinfo
		    
 	 <Date> 				<Author> 				<Change Description>
	02/03/2015      		Marta Laliena    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void crearWorkinfoTest() {
        User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
        	Id incident = [SELECT Id FROM RecordType WHERE Name = 'Incident' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = incident , Subject = 'Test controller');
       		insert caseTest;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            controller.newComment = 'Unit test WorkInfo description';
            controller.attachmentName1 = 'Unit Test Attachment';
            controller.attachmentDesc1 = 'Unit Test Description Attachment';
            controller.myAttachedBody = Blob.valueOf('Unit Test Attachment Body');
            controller.attachmentName2 = 'Unit Test Attachment';
            controller.attachmentDesc2 = 'Unit Test Description Attachment';
            controller.myAttachedBody2 = Blob.valueOf('Unit Test Attachment Body');
            controller.attachmentName3 = 'Unit Test Attachment';
            controller.attachmentDesc3 = 'Unit Test Description Attachment';
            controller.myAttachedBody3 = Blob.valueOf('Unit Test Attachment Body');
            controller.crearWorkinfo();
			List<TGS_Work_Info__c> wi = [SELECT Id, TGS_Description__c FROM TGS_Work_Info__c WHERE TGS_Case__c = :caseTest.Id];
            System.assertEquals(wi.size(),1);
            Boolean created = false;
            for(TGS_Work_Info__c work : wi){
                if(work.TGS_Description__c.equals('Unit test WorkInfo description')){
                    created = true;
                }
            }
            System.assert(created);
        	Test.stopTest();
        }
	}
 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Marta Laliena
	Company:       Deloitte
 	Description:   Test method to manage the code coverage for PCA_Ticket_Detail_Controller.uploadAttachment
 	
 	 <Date> 				<Author> 				<Change Description>
	02/03/2015      		Marta Laliena    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void uploadAttachmentTest() {
        User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id incident = [SELECT Id FROM RecordType WHERE Name = 'Incident' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = incident , Subject = 'Test controller');
       		insert caseTest;
        	Test.startTest();
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            controller.newAttachment.Name='Unit Test Attachment';
            controller.newAttachment.Body=Blob.valueOf('Unit Test Attachment Body');
            controller.uploadAttachment();
            System.assertEquals(1, controller.listNewCommentAttachments.size());
            System.assertEquals('Unit Test Attachment', controller.listNewCommentAttachments[0].Name);
        	Test.stopTest();
        }
	}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Marta Laliena
	Company:       Deloitte
 	Description:   Test method to manage the code coverage for PCA_Ticket_Detail_Controller.cancelar
 	
 	 <Date> 				<Author> 				<Change Description>
	02/03/2015      		Marta Laliena    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	/*static testMethod void cancelarTest() {
        User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id incident = [SELECT Id FROM RecordType WHERE Name = 'Incident' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = incident , Subject = 'Test controller', Status = 'Assigned');
       		insert caseTest;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            PageReference pg = controller.cancelar();
            System.assertEquals('/empresasplatino/PCA_Cases', pg.getUrl());
            // System.assertEquals('?caseId='+caseTest.Id, pg.getUrl());
        	Test.stopTest();
        }
	}*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Marta Laliena
	Company:       Deloitte
 	Description:   Test method to manage the code coverage for PCA_Ticket_Detail_Controller.acceptResolution
 	
 	 <Date> 				<Author> 				<Change Description>
	02/03/2015      		Marta Laliena    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void acceptResolutionTest() {
        User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            List<RecordType> listRecordTypes = [SELECT Id, Name FROM RecordType WHERE Name LIKE 'Incident'];
        	Case caseTest = new Case(RecordTypeId = listRecordTypes[0].Id, Subject = 'Test Subject', Description = 'Test description', Status='Assigned');
            insert caseTest;
            caseTest.Status = 'In Progress';
            caseTest.TGS_Status_reason__c = 'SD Validation';
            update caseTest;
            caseTest.Status = 'Resolved';
            update caseTest;
            Test.startTest();
            TGS_Proposed_Solutions__c sol1 = new TGS_Proposed_Solutions__c(TGS_Case__c = caseTest.Id, TGS_Description__c = 'Test Solution 1', TGS_Final_Solution__c = false);
            insert sol1;
            TGS_Proposed_Solutions__c sol2 = new TGS_Proposed_Solutions__c(TGS_Case__c = caseTest.Id, TGS_Description__c = 'Test Solution 2', TGS_Final_Solution__c = true);
            insert sol2;
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            System.assertEquals(2, controller.solutions.size());
            controller.selectedSolution = sol1.Id;
            controller.acceptResolution();
            Case caseTest2 = [SELECT Status FROM Case WHERE Id = :caseTest.Id LIMIT 1];
            System.assertEquals('Closed', caseTest2.Status);
            TGS_Proposed_Solutions__c finalSolutionTest = [SELECT TGS_Final_Solution__c FROM TGS_Proposed_Solutions__c WHERE Id = :sol1.Id LIMIT 1];
            System.assertEquals(true, finalSolutionTest.TGS_Final_Solution__c);
            Test.stopTest();
        }
        
	}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Marta Laliena
	Company:       Deloitte
 	Description:   Test method to manage the code coverage for PCA_Ticket_Detail_Controller.reopen

 	 <Date> 				<Author> 				<Change Description>
	02/03/2015      		Marta Laliena    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void reopenTest() {
		User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            List<RecordType> listRecordTypes = [SELECT Id, Name FROM RecordType WHERE Name LIKE 'Incident'];
        	Case caseTest = new Case(RecordTypeId = listRecordTypes[0].Id, Subject = 'Test Subject', Description = 'Test description', Status='Assigned');
            insert caseTest;
            caseTest.Status = 'In Progress';
            caseTest.TGS_Status_reason__c = 'SD Validation';
            update caseTest;
            caseTest.Status = 'Resolved';
            update caseTest;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            controller.reopen();
            Case caseTest2 = [SELECT Id, Status FROM Case WHERE Id = :caseTest.Id LIMIT 1];
            System.assertEquals(caseTest2.Status,'Assigned');
            Test.stopTest();
        }
	}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Marta Laliena
	Company:       Deloitte
 	Description:   Test method to manage the code coverage for PCA_Ticket_Detail_Controller.cancelTicket
		    
 	 <Date> 				<Author> 				<Change Description>
	02/03/2015      		Marta Laliena    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void cancelTicketTest() {
		User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id incident = [SELECT Id FROM RecordType WHERE Name = 'Incident' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = incident , Subject = 'Test controller', Status = 'Assigned');
       		insert caseTest;
            caseTest.Status = 'In progress';
            update caseTest;
            Test.startTest();
        	ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            controller.cancelTicket();
            Case caseTest2 = [SELECT Id, Status FROM Case WHERE Id = :caseTest.Id LIMIT 1];
            System.assertEquals(caseTest2.Status,'Cancelled');
        	Test.stopTest();
        }
	}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Marta Laliena
	Company:       Deloitte
 	Description:   Test method to manage the code coverage for PCA_Ticket_Detail_Controller.useAsTemplate
		    	
 	 <Date> 				<Author> 				<Change Description>
	02/03/2015      		Marta Laliena    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void useAsTemplateIncident() {
		User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id incident = [SELECT Id FROM RecordType WHERE Name = 'Incident' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = incident , Subject = 'Test controller', Status = 'Assigned');
       		insert caseTest;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            PageReference pg = controller.useAsTemplate();
            System.assertEquals('/empresasplatino/PCA_New_Incident?caseId='+caseTest.Id, pg.getUrl());
            //System.assertEquals('?caseId='+caseTest.Id, pg.getUrl());
        	Test.stopTest();
        }
	}
    static testMethod void useAsTemplateQuery() { 
        User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id query = [SELECT Id FROM RecordType WHERE Name = 'Query' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = query , Subject = 'Test controller', Status = 'Assigned');
       		insert caseTest;
            Test.startTest();
        	ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            PageReference pg = controller.useAsTemplate();
            //System.assertEquals('?caseId='+caseTest.Id, pg.getUrl());
            System.assertEquals('/empresasplatino/PCA_New_Query?caseId='+caseTest.Id, pg.getUrl());
        	Test.stopTest();
        }
	}
    static testMethod void useAsTemplateChange() {
        User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id change = [SELECT Id FROM RecordType WHERE Name = 'Change' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = change , Subject = 'Test controller', Status = 'Assigned');
       		insert caseTest;
            Test.startTest();
        	ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            PageReference pg = controller.useAsTemplate();
            System.assertEquals('/empresasplatino/PCA_New_Change?caseId='+caseTest.Id, pg.getUrl());
            //System.assertEquals('?caseId='+caseTest.Id, pg.getUrl());
        	Test.stopTest();
        }
	}
    static testMethod void useAsTemplateComplaint() {
        User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id complaint = [SELECT Id FROM RecordType WHERE Name = 'Complaint' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = complaint, Subject = 'Test controller', Status = 'Assigned');
       		insert caseTest;
            Test.startTest();
        	ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            PageReference pg = controller.useAsTemplate();
            System.assertEquals('/empresasplatino/PCA_New_Complaint?caseId='+caseTest.Id, pg.getUrl());
            //System.assertEquals('?caseId='+caseTest.Id, pg.getUrl());
        	Test.stopTest();
        }
	}
    static testMethod void useAsTemplateInquiry() {
        User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id inquiry = [SELECT Id FROM RecordType WHERE Name = 'Billing Inquiry' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = inquiry, Subject = 'Test controller', Status = 'Assigned');
       		insert caseTest;
            Test.startTest();
       		ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            PageReference pg = controller.useAsTemplate();
            System.assertEquals('/empresasplatino/PCA_New_Inquiry?caseId='+caseTest.Id, pg.getUrl());
            //System.assertEquals('?caseId='+caseTest.Id, pg.getUrl());
        	Test.stopTest();
        }
	}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Marta Laliena
	Company:       Deloitte
 	Description:   Test method to manage the code coverage for PCA_Ticket_Detail_Controller.WorkInfo class
		    	
 	 <Date> 				<Author> 				<Change Description>
	02/03/2015      		Marta Laliena    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void classWorkInfoTest1() {
        User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id incident = [SELECT Id FROM RecordType WHERE Name = 'Incident' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = incident , Subject = 'Test controller', Status = 'Assigned');
       		insert caseTest;
            TGS_Work_Info__c workInfoTest = new TGS_Work_Info__c(TGS_Case__c = caseTest.Id , TGS_Description__c = 'Test controller description', TGS_Public__c = true);
       		insert workInfoTest;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            System.assertEquals(1, controller.workinfos.size());
            System.assertEquals('Test controller description', controller.workinfos[0].work.TGS_Description__c);
            System.assertEquals(false, controller.workinfos[0].hasAttachments(controller.attachments));
            Test.stopTest();
        }
	}
    static testMethod void classWorkInfoTest2() {
        User userTest = TGS_Dummy_Test_data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id incident = [SELECT Id FROM RecordType WHERE Name = 'Incident' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = incident , Subject = 'Test controller', Status = 'Assigned');
       		insert caseTest;
            TGS_Work_Info__c workInfoTest = new TGS_Work_Info__c(TGS_Case__c = caseTest.Id , TGS_Description__c = 'Test controller description', TGS_Public__c = true);
       		insert workInfoTest;
            Attachment attachmentTest = new Attachment(ParentId = workInfoTest.Id, Name = 'Test Attachment', Body = Blob.valueOf('Test Attachment Body'), Description = 'Test description');
            insert attachmentTest;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_Ticket_Detail_Controller controller = new PCA_Ticket_Detail_Controller();
            System.assertEquals(1, controller.workinfos.size());
            System.assertEquals('Test controller description', controller.workinfos[0].work.TGS_Description__c);
            System.assertEquals(true, controller.workinfos[0].hasAttachments(controller.attachments));
            controller.workinfos[0].getCommentAttachments(controller.attachments);
            System.assertEquals('Test Attachment', controller.workinfos[0].listAttachment[0].Name);
            Test.stopTest();
        }
	}
}