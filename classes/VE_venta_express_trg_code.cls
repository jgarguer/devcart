public class VE_venta_express_trg_code {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Suarez
    Company:       Salesforce.com
    Description:   Aux class to do the Venta Express functionality in the Opty Trigger.
    
    History:
    
    <Date>            <Author>          <Description>
    02/02/2016        Javier Suarez      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    public static void moveOppty(List<Opportunity> opps){
        for (Opportunity op: opps){
            if (op.stagename=='F5 - Solution Definition' && op.ve_express__c==true && op.BI_Productos_numero__c != null && op.BI_Productos_numero__c > 0){
                op.StageName='F4 - Offer Development';
                op.BI_Fecha_de_entrega_de_la_oferta__c = datetime.now();            
            }
        }
    }

    public static void uncheckExpress(List <Opportunity> news, List <Opportunity> olds){
            if (news[0].stagename=='F4 - Offer Development' && olds[0].stagename=='F2 - Negotiation'){
                news[0].VE_Express__c = false;
            }
    }

    public static void veTakeOpportunity(List <Opportunity> news, List <Opportunity> olds){

        if (news[0].stagename=='F5 - Solution Definition' && olds[0].stagename=='F6 - Prospecting'){
            
	        List<id> ids = new List<id>();
            
			Map<id,id> mapAcctOwner = new Map<id,id>();
        
    	    ids.add(news[0].AccountId);
        
        	for (Account a: [select id,OwnerId from account where id in :ids]){
            		mapAcctOwner.put(a.Id, a.OwnerId);
        	}
 
            for (Opportunity opp: news){

//                system.debug('Estamos para ver lo del Señalizador');
            
//                system.debug('Parametros: '+ mapAcctOwner.get(o.accountid)+' '+opp.CreatedById+' '+opp.OwnerId);
            
            if((mapAcctOwner.get(opp.accountid)==opp.OwnerId) && (opp.OwnerId != opp.CreatedById)){

                boolean hasOtm = False;
            
//            	System.debug('******### hasOtm: '+hasOtm);
                List<OpportunityTeamMember> removeSen = new List<OpportunityTeamMember>();
//          	System.debug('******### otm.Id: '+otm.Id);
                for(OpportunityTeamMember otm : [Select id, UserId, TeamMemberRole From OpportunityTeamMember where OpportunityID = :opp.Id and TeamMemberRole = 'Sales Engineer' and UserId = :opp.CreatedById])
                {
                    if(otm.Id != null) removeSen.add(otm);
                }

//                system.debug('Remove Señalizador '+ removeSen);

                delete removeSen;        
            }		    	  		
                }

                }
}
}