/*------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jose María Martín Castaño
        Company:       Deloitte
        Description:   Función para la creación de Work Info en RoD.
        
        History:
        
        <Date>              <Author>                                        <Description>
        10/12/2015          Jose María Martín Castaño                       Initial version
        07/01/2016          Micah Burgos                                    Dynamic TimeOut
        24/05/2016          Julio Laplaza                                   Conversion to UNICA
------------------------------------------------------------------------------------------------------------------------------------------*/
public class BIIN_Crear_WorkInfo_WS extends BIIN_UNICA_Base
{
    public CrearWISalida crearWorkInfo(String authorizationToken, Case caso, List<Adjunto> ladjuntos, String casoNota, String viewAccess) 
    {
        System.debug(' BIIN_Crear_WorkInfo_WS.crearWorkInfo | ladjuntos: ' + ladjuntos);

        // Prepare URL
        String url = 'callout:BIIN_WS_UNICA/ticketing/v1/tickets/' + caso.BI_Id_del_caso_legado__c + '/notes';

        // Prepare Request
        BIIN_UNICA_Pojos.TicketNoteRequestType ticketNoteRequest = new BIIN_UNICA_Pojos.TicketNoteRequestType();
        ticketNoteRequest.text = casoNota;
        ticketNoteRequest.attachments = new List<BIIN_UNICA_Pojos.TicketAttachmentRequestType>();

        if (ladjuntos != null)
        {
            for (Adjunto adjunto : ladjuntos)
            {
                BIIN_UNICA_Pojos.TicketAttachmentRequestType attachment = new BIIN_UNICA_Pojos.TicketAttachmentRequestType();
                attachment.name = adjunto.name;
                attachment.attachment = new BIIN_UNICA_Pojos.AttachmentType();
                attachment.attachment.attachmentSummary = new BIIN_UNICA_Pojos.AttachmentSummaryType();
                attachment.attachment.attachmentSummary.contentId = adjunto.adjId;
                attachment.attachment.attachmentSummary.name = adjunto.name;
                attachment.attachment.content = EncodingUtil.base64Encode(adjunto.body);

                ticketNoteRequest.attachments.add(attachment);
            }
        }

        // Additional Data
        ticketNoteRequest.additionalData = new List<BIIN_UNICA_Pojos.KeyValueType>();

        system.debug(' BIIN_Crear_WorkInfo_WS.crearWorkInfo | caso.accountId: ' + caso.accountId);
        Account account = getAccount(caso.accountId);
        if (account != null)
        {
            ticketNoteRequest.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('companyId', account.BI_Validador_Fiscal__c));
        }
        ticketNoteRequest.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('viewAcces', viewAccess));
        ticketNoteRequest.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('workLogType', ''));
        ticketNoteRequest.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('workLogDate', string.valueof(System.currentTimeMillis()/1000)));

        // Make request
        HttpRequest req = getRequest(url, 'POST', ticketNoteRequest);
        BIIN_UNICA_Pojos.ResponseObject response = getResponse(req, null);

        // Return (adaptation to old model)
        CrearWISalida salida;
        if (response.isResponseOK())
        {
            salida = new CrearWISalida();
            salida.code = '000';
        }

        return salida;
        //BIIN_UNICA_Pojos.TicketStatusInfoType statusInfoType = (BIIN_UNICA_Pojos.TicketAttachmentInfoType) response.responseObject;
    }

    //############ OLD POJOs
    public class CrearWISalida
    {
        public String WorklogID;
        public String message;
        public String code;
    }

    public class Adjunto
    {
        public String name;
        public Blob body;
        public String adjId;

        public Adjunto(String name, Blob body)
        {
            this.name=name;
            this.body=body;
        }

        public Adjunto(String name, Blob body, String adjId)
        {
            this.name=name;
            this.body=body;
            this.adjId=adjId;
        }
    }
}