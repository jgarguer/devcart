global class BI_Currency_UF_JOB implements Schedulable {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Scheduled class that updates the UF currency
    
    History:
    
    <Date>            <Author>          <Description>
    20/01/2015        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/   
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that executes the job, and updates the UF currency
    
    IN:            Schedulable context (Parameters to schedule the job)
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    20/01/2015        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global void execute(SchedulableContext sch) {
        
        try{
            
            system.abortJob(sch.getTriggerId());
            
            executeFuture();
    		
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_Currency_UF_JOB.execute', 'BI_EN', Exc, 'Job');
        }
        
	}
	
	@future (callout=true)
	public static void executeFuture() {
        
        try{
        	
        	Map<String, String> map_conf = new Map<String, String>();
			for(BI_Configuracion__c conf:BI_Configuracion__c.getall().values())
				map_conf.put(conf.Name, conf.BI_Valor__c);
			
			system.debug(map_conf);
			
			HttpRequest req0 = new HttpRequest();
			req0.setEndpoint('https://login.salesforce.com/services/oauth2/token');
			req0.setMethod('POST');
			req0.setHeader('Content-Type','application/x-www-form-urlencoded');
			req0.setBody('grant_type=password&client_id='+map_conf.get('client_id_currency_uf')+'&client_secret='+map_conf.get('client_secret_currency_uf')+'&username='+map_conf.get('uname_currency_uf')+'&password='+map_conf.get('upass_currency_uf'));
			
			system.debug(req0.getBody());
			
			Http http0 = new Http();
			HttpResponse response0 = new HttpResponse();
			
			if(!Test.isRunningTest())
				response0 = http0.send(req0);
			else{ 
				response0.setHeader('Content-Type', 'application/json');
			    response0.setBody('{"access_token":"bar"}');
			    response0.setStatusCode(200);
			}
			
			system.debug('#### res: '+response0);
			system.debug('#### res body: '+response0.getBody());   
			
			Map<String,Object> mapBody = (Map<String,Object>)JSON.deserializeUntyped(response0.getBody());
			
			String sessionId = (String)mapBody.get('access_token');
			
			system.debug('SESSION ID: '+sessionId);
        	
            HttpRequest req = new HttpRequest();
            
            Map<String, String> map_currency_uf = new Map<String, String>();
            
            for(BI_Configuracion__c conf:BI_Configuracion__c.getAll().values())
            	map_currency_uf.put(conf.Name, conf.BI_Valor__c);
            
			req.setEndpoint(map_currency_uf.get('url_currency_uf').replace('@anyo@',String.valueOf(Date.today().year())));
			req.setMethod('GET');
			req.setTimeout(60000);
				
			Http http = new Http();
			HTTPResponse res = new HTTPResponse();
				
			if(!Test.isRunningTest())
				res = http.send(req);
			else{
				String resbody = '';
				for(Integer i=0;i<31;i++){
					resbody += '<th style=\'text-align:center;\'>'+i+'</th>';
					for(Integer j=0;j<12;j++){
						resbody += '<td style=\'text-align:right;\'>24.627,10</td>';
					}
				}

				res.setHeader('Content-Type', 'text/html; charset=utf-8');
		        res.setBody(resbody);

		        res.setStatusCode(200);
			}
			
			if(res.getStatusCode() == 200){
				
				String total = res.getBody();
				Date date_c = Date.today();
					
				String values_month = total.substring(total.indexOf(map_currency_uf.get('day_currency_uf').replace('@day@',String.valueOf(date_c.day()))));
				
				List<String> months = values_month.split(map_currency_uf.get('split_currency_uf'));
				
				String Ratio_s = months[date_c.Month()].substring(0, months[date_c.Month()].indexOf('</td>'));
				
				Ratio_s = Ratio_s.replace('.','');
				Ratio_s = Ratio_s.replace(',','.');
				
				Decimal Ratio = Decimal.valueOf(Ratio_s);
				
				system.debug('Ratio: '+Ratio);
				
				Map<String, DatedConversionRate> map_currency = new Map<String, DatedConversionRate>();
				for(DatedConversionRate ct:[SELECT Id, ConversionRate, IsoCode FROM DatedConversionRate WHERE IsoCode = 'CLF' OR IsoCode = 'CLP' order by Id asc])
					map_currency.put(ct.IsoCode, ct);
					
				DatedConversionRate ctype = map_currency.get('CLF');
				Decimal conversionRate = map_currency.get('CLP').ConversionRate / Ratio;
				
				system.debug('final value: '+conversionRate);
				
				Http http2 = new Http();
    			HttpRequest req2 = new HttpRequest();
    			req2.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v28.0/sobjects/DatedConversionRate/'+map_currency.get('CLF').Id+'?_HttpMethod=PATCH');
    			req2.setBody('{ "ConversionRate" : '+conversionRate+' }');
    			req2.setHeader('Authorization', 'OAuth ' + sessionId);
    			req2.setHeader('Content-Type', 'application/json');
    			req2.setMethod('POST');
    			
    			HttpResponse res2 = http2.send(req2);
    			
    			system.debug('#### res 2: '+res2);
				system.debug('#### res body 2: '+res2.getBody());
				
			}else{
				//WS ERROR
				system.debug('WS ERROR');
				BI_LogHelper.generateLog('BI_Currency_UF_JOB.executeFuture', 'BI_EN', 'Se ha producido un error en el proceso '+res.getBody(), 'Job');
			}
    		
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_Currency_UF_JOB.executeFuture', 'BI_EN', Exc, 'Job');
        }
        
	}

    
}