/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:          
* Description:         
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-07-24      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_cus_solFeasibilityTRS_ctr
*			 1.1	2015-08-19		Jeisson Rojas (JR)		Cambio realizado a la Query de Usuarios del campo Country por Pais__c
*                   20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest
private class BI_COL_cus_solFeasibilityTRS_tst 
{
	static {
      BI_TestUtils.throw_exception = false;
    }


//
	public static BI_COL_cus_solFeasibilityTRS_ctr objsolFeasibilityTRS;
	public static list <Profile> lstPerfil;
	public static list <UserRole> lstRoles;
	public static User objUsuario;
	public static Account objCuenta;
	public static list <Campaign> lstCampana; 
	//public static Campaign objCampana;
	public static Contract objContrato;
	public static Opportunity objOportunidad;
	public static List<RecordType>                      lstFUN;
	public static BI_COL_Anexos__c                      objAnexos;
	public static BI_COL_Descripcion_de_servicio__c     objDesSer;
	public static BI_COL_Modificacion_de_Servicio__c objMS;
	public static list<BI_COL_Modificacion_de_Servicio__c> lstMS;
	public static list<BI_COL_manage_cons__c>  lstManege;
	public static BI_COL_Viabilidad_Tecnica__c objViabilidad;
	public static List<BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS>     lstWrapper = new List<BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS>();
	public static List<BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS>     lstWrapper1 = new List<BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS>();
	public static List<BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS>     lstWrapper2 = new List<BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS>();
	public static list<BI_COL_manage_cons__c >  lstManage;
	
	public static void crearData ()
	{
		//BI_bypass__c objBibypass = new BI_bypass__c();
		//objBibypass.BI_migration__c=true;
		//insert objBibypass;
			
		lstPerfil             = new list <Profile>();
		lstPerfil               = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
		system.debug('datos Perfil '+lstPerfil);
		
		
		lstRoles                = new list <UserRole>();
		lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
		system.debug('datos Rol '+lstRoles);
		
		
		objCuenta = new Account();		
		objCuenta                                       = new Account();
		objCuenta.BI_Segment__c 						= 'Empresas';
        objCuenta.BI_Subsegment_Regional__c 			= 'Mayoristas';
        objCuenta.BI_Subsector__c 						= 'Banca';
        objCuenta.BI_Sector__c 							= 'Industria'; 
		objCuenta.Name                                  = 'prueba';
		objCuenta.BI_Country__c                         = 'Colombia';
		objCuenta.TGS_Region__c                         = 'América';
		objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta.CurrencyIsoCode                       = 'COP';
		 objCuenta.BI_Segment__c                         = 'test';
         objCuenta.BI_Subsegment_Regional__c             = 'test';
         objCuenta.BI_Territory__c                       = 'test';
		insert objCuenta;
		system.debug('datos Cuenta '+objCuenta);
		
		
		/*objCampana = new Campaign();
		
		//objCampana.BI_COL_Codigo_campana__c= 'Campaña 00001';
		objCampana.Name = 'Prueba';
		objCampana.Status = 'Planned';
		objCampana.BI_Country__c ='Colombia';
		objCampana.BI_Tipo_de_accion__c ='Fidelización';
		objCampana.IsActive =true;
		objCampana.StartDate =System.today().addDays(+1);
		objCampana.EndDate =System.today().addDays(+5);
		objCampana.BI_Canal_de_comunicacion__c ='Email';
		objCampana.CurrencyIsoCode ='COP';
		objCampana.BI_Cancela_tareas_cierre__c ='Sí';
		objCampana.BI_Cancela_oportunidad_cierre__c ='Sí';
		objCampana.BI_Crear_tareas_prospectos__c ='Si';
		objCampana.BI_Tipo_de_preoportunidad__c ='Preoportunidad Renegociación';
		objCampana.BI_Crear_oportunidad_contactos__c ='Si';
		
		insert objCuenta;
		system.debug('datos Cuenta '+objCuenta);*/
		
		lstCampana = new List<Campaign> (); 		
		lstCampana = [Select Id, Name from Campaign where id = '701w0000001NXmXAAW'];
		system.debug('datos Cuenta '+lstCampana);
		
		
		lstFUN = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
		system.debug('datos de FUN '+lstFUN);
		
		
		objAnexos               = new BI_COL_Anexos__c();
		objAnexos.Name          = 'FUN-0041414';
		objAnexos.RecordTypeId  = lstFUN[0].Id;
		insert objAnexos;
		System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);

		BI_COL_manage_cons__c objConPer = new BI_COL_manage_cons__c();

		objConPer.Name                  = 'Viabilidades';
		objConPer.BI_COL_Numero_Viabilidad__c = 46;
		objConPer.BI_COL_ConsProyecto__c     = 1;
		objConPer.BI_COL_ValorConsecutivo__c  =1;
		lstManege                      = new list<BI_COL_manage_cons__c >();		
		
		lstManege.add(objConPer);
		
		insert lstManege;
		
		System.debug('======= configuracion personalizada ======= '+lstManege);

		objOportunidad                                      = new Opportunity();
		objOportunidad.Name                                   = 'prueba opp';
		objOportunidad.AccountId                              = objCuenta.Id;
		objOportunidad.BI_Country__c                          = 'Colombia';
		objOportunidad.CloseDate                              = System.today().addDays(+5);
		objOportunidad.StageName                              = 'F5 - Solution Definition';
		objOportunidad.CurrencyIsoCode                        = 'COP';
		objOportunidad.Certa_SCP__contract_duration_months__c = 12;
		objOportunidad.BI_Plazo_estimado_de_provision_dias__c = 0 ;
		//objOportunidad.OwnerId                                = lstUsuarios[0].id;
		insert objOportunidad;
		system.debug('Datos Oportunidad'+objOportunidad);
		
		list<Opportunity> lstOportunidad = new list<Opportunity>();
		lstOportunidad = [select Id from Opportunity where Id =: objOportunidad.Id];
		system.debug('datos ListaOportunida '+lstOportunidad);
		
		
		objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
		objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
		objDesSer.CurrencyIsoCode               = 'COP';
		insert objDesSer;
		System.debug('\n\n\n Sosalida objDesSer '+ objDesSer);
		
		
		List<BI_COL_Descripcion_de_servicio__c> lstqry = [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
		System.debug('\n\n\n Sosalida lstqry '+ lstqry);
		
		
		objMS                         = new BI_COL_Modificacion_de_Servicio__c();
		objMS.BI_COL_FUN__c           = objAnexos.Id;
		objMS.BI_COL_Codigo_unico_servicio__c = objDesSer.Id;
		objMS.BI_COL_Clasificacion_Servicio__c  = 'ALTA';
		objMS.BI_COL_Oportunidad__c = objOportunidad.Id;
		objMS.BI_COL_Bloqueado__c = false;
		objMS.BI_COL_Estado__c = 'Pendiente';
		
		lstMS = new list<BI_COL_Modificacion_de_Servicio__c>();
		lstMS.add(objMs);
		insert lstMS;
		System.debug('\n\n\n datos MS '+ lstMS);
		
		
		objViabilidad = new BI_COL_Viabilidad_Tecnica__c();
		objViabilidad.BI_COL_Modificacion_de_Servicio__c = objMS.Id;
		objViabilidad.BI_COL_Estado__c = 'Solicitado';
		insert objViabilidad;
		
		list<BI_COL_Viabilidad_Tecnica__c> lstViabilidad = new list<BI_COL_Viabilidad_Tecnica__c>();
		lstViabilidad = [select Id, Name from BI_COL_Viabilidad_Tecnica__c where BI_COL_Modificacion_de_Servicio__c =: objMS.Id ];
		//Configuracion Personalizada
			BI_COL_manage_cons__c objConPermana                = new BI_COL_manage_cons__c();
			objConPermana.Name                                  = 'Viabilidades';
			objConPermana.BI_COL_Numero_Viabilidad__c           = 46;
			objConPermana.BI_COL_ConsProyecto__c                = 1;
			objConPermana.BI_COL_ValorConsecutivo__c            =1;
			lstManage         = new list<BI_COL_manage_cons__c >();        
			lstManege.add(objConPermana);
			insert lstManage;
			System.debug('======= configuracion personalizada ======= '+lstManage);
			BI_COL_Configuracion_campos_interfases__c objConfCamposInterfases = new BI_COL_Configuracion_campos_interfases__c();
			objConfCamposInterfases.BI_COL_Adicion__c=false;
			objConfCamposInterfases.BI_COL_Altas__c=false;
			objConfCamposInterfases.BI_COL_Bajas__c=false;
			objConfCamposInterfases.BI_COL_Calificador_Texto__c=null;
			objConfCamposInterfases.BI_COL_Cambio_Cuenta__c=false;
			objConfCamposInterfases.BI_COL_Cambio_Plan__c=false;
			objConfCamposInterfases.BI_COL_Cambio_servicio_alta__c=false;
			objConfCamposInterfases.BI_COL_Configuracion_Interfaz__c=null;
			objConfCamposInterfases.BI_COL_Convertir_mayuscula__c=false;
			objConfCamposInterfases.BI_COL_Decimales__c=null;
			objConfCamposInterfases.BI_COL_Descripcion__c='TRS-VIABILIDADES';
			objConfCamposInterfases.BI_COL_Direccion__c='Saliente';
			objConfCamposInterfases.BI_COL_Downgrade__c=false;
			objConfCamposInterfases.BI_COL_Formato_Campo__c=null;
			objConfCamposInterfases.BI_COL_Funcion_Especial__c=null;
			objConfCamposInterfases.BI_COL_Identificador__c=false;
			objConfCamposInterfases.BI_COL_ID_Colombia__c='a3Qg0000000ExpKEAS';
			objConfCamposInterfases.BI_COL_Inactivacion__c=false;
			objConfCamposInterfases.BI_COL_Ingreso__c=false;
			objConfCamposInterfases.BI_COL_Interfaz__c='TRS-VIABILIDADES';
			objConfCamposInterfases.BI_COL_Linea__c=null;
			objConfCamposInterfases.BI_COL_Longitud__c=null;
			objConfCamposInterfases.BI_COL_Nombre_campo__c=null;
			objConfCamposInterfases.BI_COL_Objeto__c='BI_COL_Modificacion_de_Servicio__c';
			objConfCamposInterfases.BI_COL_Observaciones__c=null;
			objConfCamposInterfases.BI_COL_Orden__c=null;
			objConfCamposInterfases.BI_COL_Permite_nulos__c=false;
			objConfCamposInterfases.BI_COL_Renegociacion__c=false;
			objConfCamposInterfases.BI_COL_Retiro__c=false;
			objConfCamposInterfases.BI_COL_Segmento__c=null;
			objConfCamposInterfases.BI_COL_Separador_Decimal__c='Õ';
			objConfCamposInterfases.BI_COL_Separador_Interno__c='.';
			objConfCamposInterfases.BI_COL_Separador__c='Õ';
			objConfCamposInterfases.BI_COL_Servicios_de_IngenierIa__c=false;
			objConfCamposInterfases.BI_COL_Susp_Temporal_Facturacion__c=false;
			objConfCamposInterfases.BI_COL_Tipo_campo__c=null;
			objConfCamposInterfases.BI_COL_Tipo_transaccion__c='VIABILIDADES';
			objConfCamposInterfases.BI_COL_Traslado__c=false;
			objConfCamposInterfases.BI_COL_Upgrade__c=false;
			objConfCamposInterfases.BI_COL_Valor_predeterminado__c=null;
			objConfCamposInterfases.Name ='Cabecera';
			insert objConfCamposInterfases;
			BI_COL_Configuracion_campos_interfases__c objConfCamposInterfases1 = new BI_COL_Configuracion_campos_interfases__c();
			objConfCamposInterfases1.BI_COL_Adicion__c=false;
			objConfCamposInterfases1.BI_COL_Altas__c=false;
			objConfCamposInterfases1.BI_COL_Bajas__c=false;
			objConfCamposInterfases1.BI_COL_Calificador_Texto__c=null;
			objConfCamposInterfases1.BI_COL_Cambio_Cuenta__c=false;
			objConfCamposInterfases1.BI_COL_Cambio_Plan__c=false;
			objConfCamposInterfases1.BI_COL_Cambio_servicio_alta__c=false;
			objConfCamposInterfases1.BI_COL_Configuracion_Interfaz__c=objConfCamposInterfases.id;
			objConfCamposInterfases1.BI_COL_Convertir_mayuscula__c=false;
			objConfCamposInterfases1.BI_COL_Decimales__c=null;
			objConfCamposInterfases1.BI_COL_Descripcion__c='VIABILIDADES';
			objConfCamposInterfases1.BI_COL_Direccion__c='Saliente';
			objConfCamposInterfases1.BI_COL_Downgrade__c=false;
			objConfCamposInterfases1.BI_COL_Formato_Campo__c=null;
			objConfCamposInterfases1.BI_COL_Funcion_Especial__c=null;
			objConfCamposInterfases1.BI_COL_Identificador__c=false;
			objConfCamposInterfases1.BI_COL_ID_Colombia__c='a3Qg0000000ExpKEAR';
			objConfCamposInterfases1.BI_COL_Inactivacion__c=false;
			objConfCamposInterfases1.BI_COL_Ingreso__c=false;
			objConfCamposInterfases1.BI_COL_Interfaz__c='TRS-VIABILIDADES';
			objConfCamposInterfases1.BI_COL_Linea__c=null;
			objConfCamposInterfases1.BI_COL_Longitud__c=null;
			objConfCamposInterfases1.BI_COL_Nombre_campo__c='BI_COL_Sucursal_Origen__r.BI_COL_Contacto_instalacion__r.Email';
			objConfCamposInterfases1.BI_COL_Objeto__c='BI_COL_Modificacion_de_Servicio__c';
			objConfCamposInterfases1.BI_COL_Observaciones__c=null;
			objConfCamposInterfases1.BI_COL_Orden__c=null;
			objConfCamposInterfases1.BI_COL_Permite_nulos__c=false;
			objConfCamposInterfases1.BI_COL_Renegociacion__c=false;
			objConfCamposInterfases1.BI_COL_Retiro__c=false;
			objConfCamposInterfases1.BI_COL_Segmento__c=null;
			objConfCamposInterfases1.BI_COL_Separador_Decimal__c='.';
			objConfCamposInterfases1.BI_COL_Separador_Interno__c=',';
			objConfCamposInterfases1.BI_COL_Separador__c='~';
			objConfCamposInterfases1.BI_COL_Servicios_de_IngenierIa__c=false;
			objConfCamposInterfases1.BI_COL_Susp_Temporal_Facturacion__c=false;
			objConfCamposInterfases1.BI_COL_Tipo_campo__c=null;
			objConfCamposInterfases1.BI_COL_Tipo_transaccion__c='VIABILIDADES';
			objConfCamposInterfases1.BI_COL_Traslado__c=false;
			objConfCamposInterfases1.BI_COL_Upgrade__c=false;
			objConfCamposInterfases1.BI_COL_Valor_predeterminado__c=null;
			objConfCamposInterfases1.Name ='Cabecera';
			insert objConfCamposInterfases1;
	}
	
	@isTest static void test_method_one()
	{
		objUsuario=BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario)
		{
		// Implement test code
		crearData();
		PageReference                 pageRef           = Page.BI_COL_cus_solFeasibilityTRS_pag;
		Test.setCurrentPage(pageRef);
		system.debug('objOportunidad '+objOportunidad.Id);
		
		Test.startTest();
		pageRef.getParameters().put('idOpt',objOportunidad.Id);
			
		objsolFeasibilityTRS = new BI_COL_cus_solFeasibilityTRS_ctr();
		
		BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS   wrapper     = new BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS(
			objMS.Id,
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste'
			);
			wrapper.check=true;
		lstWrapper.add(wrapper);
		objsolFeasibilityTRS.listMs = lstWrapper;

		objsolFeasibilityTRS.generalControl = true;
		
		objsolFeasibilityTRS.viabilidad();
		objsolFeasibilityTRS.cargarViabilidades();

		Test.stopTest();
		}
	}
	
	@isTest static void test_method_two()
	{
		// Implement test code
		objUsuario=BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario)
		{
		crearData(); 
		PageReference                 pageRef           = Page.BI_COL_cus_solFeasibilityTRS_pag;
		Test.setCurrentPage(pageRef);
		system.debug('objOportunidad '+objOportunidad.Id);
		
		Test.startTest();
		pageRef.getParameters().put('idOpt',objOportunidad.Id);
		
		objsolFeasibilityTRS = new BI_COL_cus_solFeasibilityTRS_ctr();
		objsolFeasibilityTRS.generalControl = false;
		objsolFeasibilityTRS.listNDs = lstMS;
		
		BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS   wrapper     = new BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS(
			objMS.Id,
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste'
			);
		lstWrapper.add(wrapper);
		system.debug('datos wrapper '+wrapper);
		objsolFeasibilityTRS.listMs = lstWrapper;
		objsolFeasibilityTRS.seleccionar();
		lstWrapper.add(wrapper);
		system.debug('datos wrapper2 '+wrapper); 
		objsolFeasibilityTRS.listMs = lstWrapper;
		
		objsolFeasibilityTRS.viabilidad();
		objsolFeasibilityTRS.cargarViabilidades();
		objsolFeasibilityTRS.desseleccionar();
		Test.stopTest();
	}
	}
	
	@isTest static void test_method_three()
	{
		// Implement test code
		objUsuario=BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario)
		{
		crearData(); 
		
		objMS.BI_COL_Estado__c = 'Sin Solicitud';
		update objMS;
		
		objViabilidad.BI_COL_Estado__c = 'Sin Solicitud';
		update objOportunidad;
		
		PageReference                 pageRef           = Page.BI_COL_cus_solFeasibilityTRS_pag;
		Test.setCurrentPage(pageRef);
		system.debug('objOportunidad '+objOportunidad.Id);
		
		Test.startTest();
		pageRef.getParameters().put('idOpt',objOportunidad.Id);
		
		objsolFeasibilityTRS = new BI_COL_cus_solFeasibilityTRS_ctr();
		objsolFeasibilityTRS.generalControl = false;
		objsolFeasibilityTRS.listNDs = lstMS;
		
		BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS   wrapper     = new BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS(
			objMS.Id,
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste',
			'Teste'
			);
		lstWrapper.add(wrapper);
		system.debug('datos wrapper '+wrapper);
		objsolFeasibilityTRS.listMs = lstWrapper;
		objsolFeasibilityTRS.seleccionar();
		lstWrapper.add(wrapper);
		system.debug('datos wrapper2 '+wrapper); 
		objsolFeasibilityTRS.listMs = lstWrapper;
		
		objsolFeasibilityTRS.viabilidad();
		objsolFeasibilityTRS.cargarViabilidades();
		objsolFeasibilityTRS.desseleccionar();
		Test.stopTest();
	}
	}

    @isTest static void test_method_four()
    {
        // Implement test code
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario)
		{
        crearData(); 
        
        PageReference                 pageRef           = Page.BI_COL_cus_solFeasibilityTRS_pag;
        Test.setCurrentPage(pageRef);
        system.debug('objOportunidad '+objOportunidad.Id);
        
        Test.startTest();
        pageRef.getParameters().put('idOpt',objOportunidad.Id);
        
        objsolFeasibilityTRS = new BI_COL_cus_solFeasibilityTRS_ctr();
        objsolFeasibilityTRS.generalControl = true;
        objsolFeasibilityTRS.listNDs = lstMS;
        BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS   wrapper     = new BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS(
        	objMS.Id,
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste'
            );

        for(Integer i = 0; i < 20 ; i++)
        {
        	wrapper  = new BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS(
        	objMS.Id,
            'Teste'+i+'',
            'Teste'+i+'',
            'Teste'+i+'',
            'Teste'+i+'',
            'Teste'+i+'',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste'
            );

            lstWrapper.add(wrapper);
        }
            
        
        system.debug('datos wrapper '+wrapper);
        
        objsolFeasibilityTRS.seleccionar();
        lstWrapper.add(wrapper);

        system.debug('datos wrapper2 '+wrapper); 
        objsolFeasibilityTRS.listMs = lstWrapper;
        
        objsolFeasibilityTRS.seleccionar();
        Test.stopTest();
    }
    }
    
    @isTest static void test_method_five()
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario)
		{
        // Implement test code
        crearData(); 
        PageReference                 pageRef           = Page.BI_COL_cus_solFeasibilityTRS_pag;
        Test.setCurrentPage(pageRef);
        system.debug('objOportunidad '+objOportunidad.Id);
        
        Test.startTest();
         BI_COL_cus_solFeasibilityTRS_ctr objController = new BI_COL_cus_solFeasibilityTRS_ctr();
          BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS   wrapper     = new BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS(
        	objMS.Id,
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste'
            );

        for(Integer i = 0; i < 20 ; i++)
        {
        	wrapper  = new BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS(
        	objMS.Id,
            'Teste'+i+'',
            'Teste'+i+'',
            'Teste'+i+'',
            'Teste'+i+'',
            'Teste'+i+'',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste',
            'Teste'
            );

            lstWrapper.add(wrapper);
            lstWrapper1.add(wrapper);
            lstWrapper2.add(wrapper);
        }
         objController.llenaLista(lstWrapper);
         
         List<List<BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS>> lstAllCalls1 = new List<List<BI_COL_cus_solFeasibilityTRS_ctr.WrapperMS>>();
         lstAllCalls1.add(lstWrapper);
         lstAllCalls1.add(lstWrapper1);
         lstAllCalls1.add(lstWrapper2);
         objController.lstAllCalls=lstAllCalls1;
         
         objController.siguiente();
         objController.anterior();
         objController.irOpt();
         objController.calculoListas(5);
         System.debug(objController.nRegistros);
         System.debug(objController.MostrarMensaje);
         System.debug(BI_COL_cus_solFeasibilityTRS_ctr.iCodigoErrorTest);	
         Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws() );
         List<String> listMSId = new List<String> ();
         listMSId.add(''+objMS.Id);
         List<String> listSolicitudViabilidadName = new List<String>();
         Map<String, String> mapInfoEnviarMS = new Map<String, String> ();
         Map<String, String> mapSolicitudNameValues = new Map<String, String>();
         //BI_COL_cus_solFeasibilityTRS_ctr.wsMethod(Double.valueOf(lstManage[0].BI_COL_Numero_Viabilidad__c), listMSId,listSolicitudViabilidadName,  mapInfoEnviarMS, mapSolicitudNameValues);
         
         objController.limpiar(listMSId);
         Test.stopTest();
     }
    }    
}