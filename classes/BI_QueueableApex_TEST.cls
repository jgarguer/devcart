@isTest
private class BI_QueueableApex_TEST {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_QueueableApex class
    History:
    
    <Date>              <Author>                <Description>
    17/11/2015          Fernando Arteaga        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void queueableTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Chile');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('CHI');
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        System.debug('lst_acc:' + lst_acc);
        
        BI_Log__c trans = new BI_Log__c(BI_Asunto__c = Label.BI_CSB_Transaccion,
                                        BI_CSB_Record_Id__c = lst_acc[0].Id,
                                        BI_Descripcion__c = 'Baja de cliente en CSB:' + lst_acc[0].Name + ' | Id ' + lst_acc[0].Id,
                                        BI_COL_Estado__c = Label.BI_COL_lblPendiente);
        insert trans;

        Test.startTest();
        
        BI_CSB_WS_Retry__c cs = new BI_CSB_WS_Retry__c();
        cs.BI_CSB_Reintentos__c = 5;
        cs.Name = 'TEST1';
        insert cs;

        // Test without custom setting records
		System.enqueueJob(new BI_QueueableApex(lst_acc[0], trans, 's'));
		
        Test.stopTest();
    }
    
    static testMethod void queueableTest2() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Chile');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('CHI');
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        System.debug('lst_acc:' + lst_acc);
        
        BI_Log__c trans = new BI_Log__c(BI_Asunto__c = Label.BI_CSB_Transaccion,
                                        BI_CSB_Record_Id__c = lst_acc[0].Id,
                                        BI_Descripcion__c = 'Baja de cliente en CSB:' + lst_acc[0].Name + ' | Id ' + lst_acc[0].Id,
                                        BI_COL_Estado__c = Label.BI_COL_lblPendiente);
        insert trans;

        Test.startTest();

        BI_CSB_WS_Retry__c cs = new BI_CSB_WS_Retry__c();
        cs.BI_CSB_Reintentos__c = 5;
        cs.Name = 'TEST1';
        insert cs;
        
        // Insert custom setting record with wrong information
        BI_CSB_Credenciales_Pais__c credencial = new BI_CSB_Credenciales_Pais__c(Name = lst_region[0], BI_CSB_Credencial__c = null);
        insert credencial;
        
        System.enqueueJob(new BI_QueueableApex(lst_acc[0], trans, 's'));

        Test.stopTest();
    
    }
    
    static testMethod void queueableTest3() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Chile');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('CHI');
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        System.debug('lst_acc:' + lst_acc);
        
        BI_Log__c trans = new BI_Log__c(BI_Asunto__c = Label.BI_CSB_Transaccion,
                                        BI_CSB_Record_Id__c = lst_acc[0].Id,
                                        BI_Descripcion__c = 'Baja de cliente en CSB:' + lst_acc[0].Name + ' | Id ' + lst_acc[0].Id,
                                        BI_COL_Estado__c = Label.BI_COL_lblPendiente);
        insert trans;

        Test.startTest();

        BI_CSB_WS_Retry__c cs = new BI_CSB_WS_Retry__c();
        cs.BI_CSB_Reintentos__c = 5;
        cs.Name = 'TEST1';
        insert cs;
        
        // Insert custom setting record with wrong information
        BI_CSB_Credenciales_Pais__c credencial = new BI_CSB_Credenciales_Pais__c(Name = lst_region[0], BI_CSB_Credencial__c = 'BI_CSB_API_UNICA_FAKE');
        insert credencial;
        
        System.enqueueJob(new BI_QueueableApex(lst_acc[0], trans, 's'));
        
        Test.stopTest();
    
    }
    
    static testMethod void queueableTest4() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Chile');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('CHI');
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        System.debug('lst_acc:' + lst_acc);
        
        BI_Log__c trans = new BI_Log__c(BI_Asunto__c = Label.BI_CSB_Transaccion,
                                        BI_CSB_Record_Id__c = lst_acc[0].Id,
                                        BI_Descripcion__c = 'Baja de cliente en CSB:' + lst_acc[0].Name + ' | Id ' + lst_acc[0].Id,
                                        BI_COL_Estado__c = Label.BI_COL_lblPendiente);
        insert trans;

        Test.startTest();

        BI_CSB_WS_Retry__c cs = new BI_CSB_WS_Retry__c();
        cs.BI_CSB_Reintentos__c = 5;
        cs.Name = 'TEST1';
        insert cs;
        
        // Insert custom setting record with wrong information
        BI_CSB_Credenciales_Pais__c credencial = new BI_CSB_Credenciales_Pais__c(Name = lst_region[0], BI_CSB_Credencial__c = 'BI_CSB_API_UNICA_FAKE');
        insert credencial;
        
        System.enqueueJob(new BI_QueueableApex(lst_acc[0], trans, 's'));
        
        BI_QueueableApex q = new BI_QueueableApex(lst_acc[0], trans, 'b');
        q.updateTransaction('error');
        
        try {
            System.enqueueJob(new BI_QueueableApex(null, trans, ''));
        }
        catch (Exception e)
        {
            System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
        }
        Test.stopTest();
    
    }
}