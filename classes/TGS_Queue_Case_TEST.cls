@isTest
public class TGS_Queue_Case_TEST {
    static testMethod void queueCase_test(){
        
       String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
        }
            
        User userTest = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyEndpointsTGS();
            Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
            NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId);
            insert testOrder;
            
            
            
            
            Group groups = new Group ( Name = 'Test', DeveloperName = 'Test', Type = 'Queue' );
            insert groups;
           	GroupMember groupMember = new GroupMember(UserOrGroupId = userTest.Id, GroupId=groups.id);
            insert groupMember;
           
            QueueSobject mappingObject = new QueueSobject(QueueId = groups.Id, SobjectType = 'Case');
			
            insert mappingObject;
            rtId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'Order_Management_Case');
        	Case testCase = new Case(RecordTypeId = rtId, Subject = 'Test Case', Status = 'Assigned', Order__c = testOrder.Id, TGS_Assignee__c = userTest.Id, OwnerId = groups.Id );
        	insert testCase;
            
            
            
        }
        
        
        
    }
}