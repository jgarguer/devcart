/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Jose Miguel Fierro
	Company:       New Energy Aborda
	Description:   Helper class that aids the creation and deletion of TGS_Error_Integrations__c logs

	History
	<Date>            <Author>                      <Description>
	17/10/2016        Jose Miguel Fierro            Initial Version
	04/10/2016		  Jose Miguel Fierro			Added methods to log errors in future, and properly serialize exceptions to JSON
	17/02/2017        Jose Miguel Fierro            Made interface more generic and added null checks to prevent inserting attachments with Body = 'null'
	03/03/2017        Jose Miguel Fierro            Added possibility to insert logs at a future time when called from a future method: functionality was
	                                                split into a future method, and a Queueable
	11/09/2017		  Guillermo Muñoz				Method generateFastLog added
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
global without sharing class TGS_ErrorIntegrations_Helper implements Schedulable,Database.Batchable<sObject> {

	///////////////////////// Deleting Logs /////////////////////////

	@testVisible String query;
	@testVisible Date FLAG_DATE;
	// Use this constructor for scheduling the deletion of logs
	global TGS_ErrorIntegrations_Helper() {
		FLAG_DATE = Date.today().addMonths(-3);
		if(Test.isRunningTest()) {
			FLAG_DATE = Date.today().addDays(1);
		}
		query = 'SELECT Id FROM TGS_Error_Integrations__c WHERE CreatedDate <: FLAG_DATE';
		System.debug('@@ Query: ' + query + ' (Date: ' + FLAG_DATE + ')');
	}

	// Schedule deletion of logs
	global void execute(SchedulableContext sc) {
		System.debug('Id de batch: ' + Database.executeBatch(this, 2000));
		//System.abortJob(sc.getTriggerId());
	}

	// Perform actual deletion of logs 
	global Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(query);
	}
	global void execute(Database.BatchableContext bc, List<sObject> scope) {
		List<TGS_Error_Integrations__c> lstLogs = (List<TGS_Error_Integrations__c>)scope;
		System.debug('@@ Deleting ' + lstLogs.size() + ' logs.');
		if(!lstLogs.isEmpty()) {
			delete lstLogs;
		}
	}
	global void finish(Database.BatchableContext bc) {
		System.debug(LoggingLevel.ERROR, 'Fin de procesamiento.');
	}


	///////////////////////// Inserting Logs /////////////////////////

	// Extend class to include fields that will be included in RequestInfo attachment
	global virtual class RequestInfo {
	}

	global class FakeExceptn {
		global String type;
		global String message;
		global String stack;
		global Integer line_number;
		global FakeExceptn cause;
	}


	//************* Creation of logs *************//
	// JMF 17/02/2017 - Changed reqInfo parameter's type from RequestInfo to Object, to allow for more flexibility
	global static void createExceptionLog(String nterface, String oper, Exception exc, Object reqInfo) {
		createExceptionLog(nterface, oper, exc, reqInfo, null);
	}
	global static void createExceptionLog(String nterface, String oper, Exception exc, Object reqInfo, String relatedRecord) {
		TGS_Error_Integrations__c error = new TGS_Error_Integrations__c();
		error.TGS_Interface__c = nterface;
		error.TGS_Operation__c = oper;
		error.TGS_ErrorType__c = exc.getTypeName();
		error.TGS_Error_Message__c = exc.getMessage();
		error.TGS_RequestInfo__c = exc.getStackTraceString();
		error.TGS_RecordId__c = relatedRecord;

		if(exc.getCause() != null) {
			error.TGS_RequestInfo__c += '\n\n\nCaused By: ' + exc.getCause().getTypeName() + ': ' + exc.getCause().getMessage();
			error.TGS_RequestInfo__c += '\n\nStack: ' + exc.getCause().getStackTraceString();
		}
		// JMF 17/02/2017 - If reqInfo is null, it was being serialized as >>'null'<<, changed to send null instead
		insertLog(JSON.serialize(error), reqInfo != null ? JSON.serialize(reqInfo) : null);
	}


	global static void createSuccessLog(String nterface, String oper, Object reqInfo) {
		createSuccessLog(nterface, oper, reqInfo, null);
	}
	// JMF 17/02/2017 - Changed reqInfo parameter's type from RequestInfo to Object, to allow for more flexibility
	global static void createSuccessLog(String nterface, String oper, Object reqInfo, String relatedRecord) {
		TGS_Error_Integrations__c error = new TGS_Error_Integrations__c();
		error.TGS_Interface__c = nterface;
		error.TGS_Operation__c = oper;
		error.TGS_ErrorType__c = 'OK';
		error.TGS_RecordId__c = relatedRecord;
		// JMF 17/02/2017 - If reqInfo is null, it was being serialized as >>'null'<<, changed to send null instead
		insertLog(JSON.serialize(error), reqInfo != null ? JSON.serialize(reqInfo) : null);
	}

	public static void insertLog(String jsonLog, String jsonRequestInfo) {

		System.debug(LoggingLevel.ERROR, '@@@ ERROR: ' + jsonLog);
		System.debug(LoggingLevel.ERROR, '@@@ REQUESTINFO: ' + jsonRequestInfo);
		TGS_Error_Integrations__c error = (TGS_Error_Integrations__c)JSON.deserialize(jsonLog, TGS_Error_Integrations__c.class);
		insert error;
		if(String.isNotBlank(jsonRequestInfo)) {
			Attachment attReqInfo = new Attachment(ParentId = error.Id);
			attReqInfo.Name = 'RequestInfo_' + Datetime.now().format('yyyyMMdd\'T\'hhmmss') + '_' + error.Id + '.json';
			attReqInfo.Body = Blob.valueOf(jsonRequestInfo);
			attReqInfo.ContentType = 'text/json';
			insert attReqInfo;
		}
	}


	//************* Asynchronous Creation of logs *************//
	/*---------------------------------------------------------------------------------------------------
	 Author:        Jose Miguel Fierro
	 Company:       New Energy Aborda
	 Description:   Allow creating logs in future

	 History
	 <Date>            <Author>                      <Description>
	 04/11/2016        Jose Miguel Fierro            Initial Version
	 03/03/2017        Jose Miguel Fierro            If this is being called from an already future method, call a queueuable to insert the log.
	---------------------------------------------------------------------------------------------------*/
	//// TODO: Make this work when already in a future, and handling a mixed_dml-able object
	global static void createExceptionLog_future(String nterface, String oper, String jsonExc, String jsonReqInfo) {
		createExceptionLog_future(nterface, oper, jsonExc, jsonReqInfo, null);
	}
	// JMF 03/03/2017 - Added related record: this helps the record be searchable
	global static void createExceptionLog_future(String nterface, String oper, String jsonExc, String jsonReqInfo, String relatedRecord) {
		TGS_Error_Integrations__c error = new TGS_Error_Integrations__c();
		error.TGS_Interface__c = nterface;
		error.TGS_Operation__c = oper;
		error.TGS_RecordId__c = relatedRecord;

		FakeExceptn exc = (FakeExceptn)JSON.deserialize(jsonExc, FakeExceptn.class);
		error.TGS_ErrorType__c = exc.type;
		error.TGS_Error_Message__c = exc.message;
		error.TGS_RequestInfo__c = exc.stack;

		if(exc.cause != null) {
			error.TGS_RequestInfo__c += '\n\n\nCaused By: ' + exc.cause.type + ': ' + exc.cause.message;
			error.TGS_RequestInfo__c += '\n\nStack: ' + exc.cause.stack;
		}
		if(System.isFuture()) { System.enqueueJob(new InsertErrorException_Queue(JSON.serialize(error), jsonReqInfo)); }
		if(!System.isFuture()) { createExceptionLog_future(JSON.serialize(error), jsonReqInfo); }
	}

	// JMF 03/03/2017 - The actual async method that inserts (in future) the log
	@future
	@testVisible
	private static void createExceptionLog_future(String jsonLog, String jsonRequestInfo) {
		insertLog(jsonLog, jsonRequestInfo);
	}

	// JMF 03/03/2017 - The actual async class that inserts (in queueuable) the log
	public class InsertErrorException_Queue implements Queueable {
		private String jsonLog, jsonRequestInfo;

		public InsertErrorException_Queue(String jsonLog, String jsonRequestInfo) {
			this.jsonLog = jsonLog;  this.jsonRequestInfo = jsonRequestInfo;
		}

		public void execute(QueueableContext qc) {
			insertLog(jsonLog, jsonRequestInfo);
		}
	}


	//************* Helper methods *************//


	private static void jsonWriteStringOrNull(JSONGenerator gen, String fieldName, String value) {
		if(value == null)  gen.writeNullField(fieldName);
		if(value != null)  gen.writeStringField(fieldName, value);
	}

	private static void jsonWriteNumberOrNull(JSONGenerator gen, String fieldName, Integer value) {
		if(value == null)  gen.writeNullField(fieldName);
		if(value != null)  gen.writeNumberField(fieldName, value);
	}

	/*---------------------------------------------------------------------------------------------------
	 Author:        Jose Miguel Fierro
	 Company:       New Energy Aborda
	 Description:   Custom serializer for exceptions

	 History
	 <Date>            <Author>                      <Description>
	 04/11/2016        Jose Miguel Fierro            Initial Version
	 03/03/2017        Jose Miguel Fierro            Corrected while loop when getting cause, avoiding cryptic 'UnknownException'
	---------------------------------------------------------------------------------------------------*/
	public static String exc2json(Exception exc) {
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		jsonWriteStringOrNull(gen, 'type', exc.getTypeName());
		jsonWriteStringOrNull(gen, 'message', exc.getMessage());
		jsonWriteNumberOrNull(gen, 'line_number', exc.getLineNumber());
		jsonWriteStringOrNull(gen, 'stack', exc.getStackTraceString());

		Exception cause = exc.getCause();
		while(cause != null) {
			gen.writeFieldName('cause');
			gen.writeStartObject();
			jsonWriteStringOrNull(gen, 'type', cause.getTypeName());
			jsonWriteStringOrNull(gen, 'message', cause.getMessage());
			jsonWriteNumberOrNull(gen, 'line_number', cause.getLineNumber());
			jsonWriteStringOrNull(gen, 'stack', cause.getStackTraceString());
			cause = cause.getCause();
		}
		return gen.getAsString();
	}

	public static void generateFastLog(String errorBody, String reqBody, String operation, String operationType, String recordId, String sInterface){

		TGS_Error_Integrations__c error = new TGS_Error_Integrations__c();
		error.TGS_Interface__c = sInterface;
		error.TGS_Operation__c = operation;
		error.TGS_ErrorType__c = operationType;
		error.TGS_RequestInfo__c = errorBody;
		error.TGS_RecordId__c = recordId;
		insert error;

		Attachment att = new Attachment();
		att.ParentId = error.Id;
		att.Name = sInterface + '_' + Datetime.now().format('yyyyMMdd\'T\'hhmmss') + '_' + error.Id + '.json';
		att.Body = Blob.valueOf(reqBody);
		att.ContentType = 'text/json';
		insert att;
	}

}