/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       23-Abr-2015     Javier Tibamoza              Creación de la Clase
    1.0       24-Ago-2017     Gawron Julián                Demanda 515 Se agrega condición para modificación a 'ALTA DEMO'
****************************************************************************************************/
global class BI_COL_DescriptionService_ws 
{
        webservice static String ActualizarInstalacionDS (
        String DS, String EstadoOT, 
        String FechaInsDesReal,  String FechaInstalacionComprometida,
        String Causal, String FechaInicioAplazamiento,
        String FechaFinAplazamiento, String IngenieroImplantador,
        string NumeroAbonado, String NumeroRouting,
        String parametrosTecnicos, String IngenieroInstalaciones,
        String IDPC, String MedioVendido,
        String medioDestino, String modServicio,                
        String FechaSuspencionTecnica, String FechaReconexionTecnica,       
        String fechaInicioRealDemoServicio,  String fechaFinDemoServicio,       
        String garantia, String gama, String serial, String marca,                      
        String modelo, String Imei, String numTelefonico )
    {
        String infoRecibida;
        Date fechaNueva = null;
        try
        {
            infoRecibida = 'DS-->: ' + DS + ', EstadoOT: ' + EstadoOT + ', FechaInsDesReal: ' + FechaInsDesReal + ', FechaInstalacionComprometida: ' + FechaInstalacionComprometida + ', FechaInicioAplazamiento: ' + FechaInicioAplazamiento + ', FechaFinAplazamiento: ' + FechaFinAplazamiento + ', IngenieroImplantador: ' + IngenieroImplantador+
            ' NumeroAbonado: '+NumeroAbonado+' NumeroRouting: '+NumeroRouting+' parametrosTecnicos: ' +parametrosTecnicos+' IngenieroInstalaciones'+IngenieroInstalaciones+' IDPC:'+IDPC +' MedioVendido='+MedioVendido+' modServicio: '+modServicio+'FechaSuspencionTecnica: '+FechaSuspencionTecnica+ 'FechaReconexionTecnica :'+FechaReconexionTecnica;
            System.debug('@-->>>infoRecibida'+infoRecibida);
            
            BI_COL_Modificacion_de_Servicio__c modficacionServ = consultaMS(DS,modServicio);                                            
            System.debug('@-->>>modficacionServ'+modficacionServ);
            BI_COL_Descripcion_de_servicio__c myDS = new BI_COL_Descripcion_de_servicio__c();
            myDS = DsInfo(DS);
            System.debug('@-->>>myDS'+myDS);
            
            if( FechaInsDesReal != null && FechaInsDesReal != ''&& 
                modficacionServ != null && modficacionServ.BI_COL_Fecha_comprometida_baja_tecnica__c == null && 
                modficacionServ.BI_COL_Clasificacion_Servicio__c == 'BAJA' )
            {
                String[] splitDate = FechaInsDesReal.split('-');
                fechaNueva = Date.newInstance( Integer.valueOf( splitDate[0] ), Integer.valueOf( splitDate[1] ), Integer.valueOf( splitDate[2] ) );
                modficacionServ.BI_COL_Fecha_comprometida_baja_tecnica__c = fechaNueva;
                myDS.BI_COL_Estado_de_servicio__c = 'Inactiva';
            }
            system.debug('@--->modficacionServ.BI_COL_Clasificacion_Servicio__c'+modficacionServ.BI_COL_Clasificacion_Servicio__c);
            system.debug('@--->FechaInsDesReal'+FechaInsDesReal);
            system.debug('@--->myDS'+myDS);
            if( modficacionServ.BI_COL_Clasificacion_Servicio__c == 'SUSPENSION TEMPORAL' && 
                FechaInsDesReal != null && FechaInsDesReal != '' && 
                modficacionServ != null && myDS != null )
            {
                modficacionServ.BI_COL_Estado__c ='Suspensión voluntaria';
                myDS.BI_COL_Estado_de_servicio__c ='Suspendida';
                modficacionServ.BI_COL_Estado_orden_trabajo__c = 'Activa';
            }
            
            if( modficacionServ.BI_COL_Clasificacion_Servicio__c == 'SUSPENSION TEMPORAL' && 
                FechaReconexionTecnica != null && FechaReconexionTecnica != '' && 
                modficacionServ != null && myDS != null )
            {
                modficacionServ.BI_COL_Estado__c = 'Inactiva';
                modficacionServ.BI_COL_Fecha_reconexionl_servicio__c = date.valueOf( FechaReconexionTecnica );
                myDS.BI_COL_Estado_de_servicio__c = 'Activa';
            }
            
            if( FechaInstalacionComprometida != null && FechaInstalacionComprometida != '' )
                modficacionServ.BI_COL_Fecha_Instalacion_Comprometida__c = date.valueOf( FechaInstalacionComprometida );
            
            if( Causal != null && Causal != '' )
                modficacionServ.BI_COL_Causal_aplazamiento__c = Causal;
            
            if( FechaInicioAplazamiento != null && FechaInicioAplazamiento != '')
                modficacionServ.BI_COL_Fecha_quiebre__c = date.valueOf( FechaInicioAplazamiento );
            
            if( FechaFinAplazamiento != null && FechaFinAplazamiento != '' )
                modficacionServ.BI_COL_Fecha_fin_suspension_temporal__c = date.valueOf( FechaFinAplazamiento );
            
            if( IngenieroImplantador != null && IngenieroImplantador != '' )
                modficacionServ.BI_COL_Ingeniero_implantador__c = IngenieroImplantador;

            if(parametrosTecnicos != null && parametrosTecnicos != '')
            {
                parametrosTecnicos = parametrosTecnicos.replaceAll('\\+','\n');
                modficacionServ.BI_COL_Parametros_tecnicos__c = parametrosTecnicos;
            }
            //JATC para eliminar
            //if(IngenieroInstalaciones!=null && IngenieroInstalaciones!='')
                //modficacionServ.Ingeniero_instalaciones__c=IngenieroInstalaciones;
            
            //modficacionServ.Serial__c = serial;
            //modficacionServ.Modelo_Pta_Origen__c = modelo;
            //modficacionServ.Marca__c = marca;
            //modficacionServ.Gama_Equipo__c = gama;    
            //modficacionServ.Garantia_Equipo__c = garantia;
            modficacionServ.BI_COL_IMEI__c = Imei;
            system.debug('\n@-->numTelefonico'+numTelefonico);
            if( numTelefonico != null && numTelefonico != '' )
                modficacionServ.BI_COL_Numero_telefonico__c = numTelefonico;
                
            if( gama != null && gama != '' )
            {
                String[] splitDate = FechaInsDesReal.split('-');
                fechaNueva = Date.newInstance( Integer.valueOf( splitDate[0] ), Integer.valueOf(splitDate[1]), Integer.valueOf( splitDate[2] ) );
            }
            
            if( Imei != null && Imei != '' )
                modficacionServ.BI_COL_Fecha_instalacion_servicio_RFS__c = system.now().Date();
            
            if( MedioVendido != null && MedioVendido != '' )
                modficacionServ.BI_COL_Medio_Vendido__c = MedioVendido;

            if( EstadoOT != null && EstadoOT != '')
            {
                if( EstadoOT.equals('Cancelada') )
                    EstadoOT = 'Cancelado';

                modficacionServ.BI_COL_Estado_orden_trabajo__c = EstadoOT;
                
                if( EstadoOT == 'EJECUTADA' && modficacionServ.BI_COL_Clasificacion_Servicio__c != 'SUSPENSION TEMPORAL' )
                {
                    List<BI_COL_Modificacion_de_Servicio__c> listModServ = [
                        Select  BI_COL_Estado__c 
                        from    BI_COL_Modificacion_de_Servicio__c m 
                        where   BI_COL_Estado__c = 'Activa' 
                        and     BI_COL_Codigo_unico_servicio__r.Name =: DS ];
                    
                    for( BI_COL_Modificacion_de_Servicio__c ms : listModServ )
                        ms.BI_COL_Estado__c = 'Inactiva';

                    update listModServ;
                    
                    if( modficacionServ.BI_COL_Estado_orden_trabajo__c == null || modficacionServ.BI_COL_Estado_orden_trabajo__c != 'Ejecutado')
                    {
                        modficacionServ.BI_COL_Estado_orden_trabajo__c = 'Ejecutado';
                        modficacionServ.BI_COL_Estado__c = 'Activa';
                        //cus_dsr_actulizacion_ctr cda = new cus_dsr_actulizacion_ctr();
                        //cda.actualizarDS( modficacionServ );
                    }
                }
            }
            
            if( modficacionServ.BI_COL_Clasificacion_Servicio__c == 'SERVICIO INGENIERIA' )
            {
                modficacionServ.BI_COL_Estado__c = 'Activa';
                modficacionServ.BI_COL_Estado_orden_trabajo__c = EstadoOT;      
            }           

            if( fechaInicioRealDemoServicio != null && fechaInicioRealDemoServicio != '' )
            {
                Date fechaA = Date.valueOf( fechaInicioRealDemoServicio );
                modficacionServ.BI_COL_Fecha_instalacion_servicio_RFS__c = fechaA;
             //// Demanda 515 comentando ALTADEMO
             //   if(modficacionServ.BI_COL_Clasificacion_Servicio__c != 'ALTA'){     //JEG Demanda 515 
             //      modficacionServ.BI_COL_Clasificacion_Servicio__c = 'ALTA DEMO';
             //   }
                modficacionServ.BI_COL_Estado__c = 'Activa';
                myDS.BI_COL_Estado_de_servicio__c = 'Activa';
            }
            /*JATC no se sabe el campo Demo_y_o__c 
            if( //modficacionServ.BI_COL_Producto__r.Demo_y_o__c != null && 
                //modficacionServ.BI_COL_Producto__r.Demo_y_o__c != '' && 
                fechaFinDemoServicio != null && 
                fechaFinDemoServicio != '' )
            {
                Date fechaB = Date.valueOf( fechaFinDemoServicio );
                modficacionServ.BI_COL_Fecha_fin_demo_SO_autoconsumo__c = fechaB;
                modficacionServ.BI_COL_Estado__c = 'Inactiva';
                myDS.BI_COL_Estado_orden_trabajo__c = 'Inactiva';
            }
            */
            List<BI_COL_Cod_Description_Reference__c> lstCodRefM2M = [
                select  Name, BI_COL_Description_Reference__c, BI_COL_Type__c 
                from    BI_COL_Cod_Description_Reference__c];
            
            set<String> setCodRefM2M = new set<String> ();
            map<String, BI_COL_Cod_Description_Reference__c> mapaCodigos = new map<String, BI_COL_Cod_Description_Reference__c>();
            
            for( BI_COL_Cod_Description_Reference__c cod : lstCodRefM2M )
            {
                setCodRefM2M.add( cod.name );
                mapaCodigos.put( cod.Name, cod );
            }
            
            if( setCodRefM2M.contains( modficacionServ.BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c ) )
            {
                modficacionServ.BI_COL_Fecha_instalacion_servicio_RFS__c = system.now().addMonths(-1).Date();
                modficacionServ.BI_COL_Fecha_fin__c = system.now().Date();
            }
            
            update modficacionServ;
            
            update myDS;
            
            BI_Log__c Log = new BI_Log__c();
            Log.Descripcion__c = 'SISGOT';
            Log.BI_COL_Estado__c = 'Exitoso'; 
            Log.BI_Descripcion__c = infoRecibida;
            Log.BI_COL_Informacion_recibida__c = armarRespuesta('True', 'N/A'); 
            //Log.codigoObjeto__c = DS;
            Log.BI_COL_Tipo_Transaccion__c = 'ActualizacionDS - SISGOT';
            Log.BI_COL_Modificacion_Servicio__c = modficacionServ.Id;
            
            insert log;
            //JATC no existe el nombre Venta Camaras
            BI_COL_Product_Codes_Telefonica__c codigo = BI_COL_Product_Codes_Telefonica__c.getInstance('Venta Cámaras');
            
            if( modficacionServ.BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c != null && 
                modficacionServ.BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c.equalsIgnoreCase( codigo.BI_COL_Product_code__c ) )
            {
                BI_COL_Remote_Monitoring_ctr.crearNovedad( modficacionServ.Id );
            }
            return armarRespuesta('True', 'N/A') ;
        }
        catch( QueryException SQL )
        {
            insert new BI_Log__c( BI_COL_Estado__c = 'Exitoso', BI_Descripcion__c = infoRecibida, 
                BI_COL_Informacion_recibida__c = armarRespuesta('False', SQL.getMessage()) , BI_COL_Tipo_Transaccion__c = 'ActualizacionDS - SISGOT' );
            return armarRespuesta( 'False', 'Error DS NO ENCONTRADA' ) ;
        }
        catch( Exception e )
        {
            insert new BI_Log__c( BI_COL_Estado__c = 'Exitoso', BI_Descripcion__c = infoRecibida, 
                BI_COL_Informacion_recibida__c = armarRespuesta('False', e.getMessage()) , BI_COL_Tipo_Transaccion__c = 'ActualizacionDS - SISGOT' );
            return armarRespuesta('False', e.getMessage()) ;
        } 
    }

    private static String armarRespuesta( String Resultado, String MsgError )
    {
        return '<ActualizarInstalacionDSResponse>' +
                '<Resultado>' + Resultado + '</Resultado>' +
                '<MsgError>' + MsgError + '</MsgError>' +
                '</ActualizarInstalacionDSResponse>';
    }
    /*
    webservice static String ActualizarIngenieroAseguramiento( String nombreIngAseguramiento, String identificador )
    {
        List<BI_COL_Modificacion_de_Servicio__c> listModServ = [
            Select  BI_COL_Ingeniero_implantador__c 
            from    BI_COL_Modificacion_de_Servicio__c m 
            where   BI_COL_Estado__c = 'Activa'
            and     BI_COL_Oportunidad__c.Account.BI_No_Identificador_fiscal__c =: identificador ];
        
        String mensaje = '';
        
        if( listModServ.size() > 0 )
        {
            for( BI_COL_Modificacion_de_Servicio__c ms : listModServ )
                ms.BI_COL_Ingeniero_implantador__c=nombreIngAseguramiento;
            try
            {
                update listModServ;
            }
            catch( Exception ex )
            {
                return armarRespuesta('False', ex.getMessage());
            }
            return armarRespuesta( 'True', 'Actualizacion de oportunidad Exitosa' );
        }
        else
            return armarRespuesta('False', 'No se encotro ninguna Modificacion de servicio con ese identificador');
    }
    */
   webservice static String ActualizarIngenieroImplantador( String nombreIngImplantador, String codProyecto )
    {
        List<BI_COL_Modificacion_de_Servicio__c> listModServ = [
            Select  BI_COL_Ingeniero_implantador__c 
            from    BI_COL_Modificacion_de_Servicio__c m 
            where   BI_COL_Estado__c = 'Activa'
            and     BI_COL_Oportunidad__r.BI_Numero_id_oportunidad__c =: codProyecto ];
        
        String mensaje='';
        
        if( listModServ.size() > 0 )
        {
            for( BI_COL_Modificacion_de_Servicio__c ms : listModServ )
                ms.BI_COL_Ingeniero_implantador__c = nombreIngImplantador;
            try
            {
                update listModServ;
            }
            catch(Exception ex)
            {
                return armarRespuesta('False', ex.getMessage());
            }
            return armarRespuesta('True', 'Actualizacion de oportunidad Exitosa');
        }
        else
            return armarRespuesta('False', 'No se encotro ninguna oportunidad con ese codigo de proyecto');
    }

    webservice static String actualizarCapexOppty( String capexTotal, String proyecto )
    {
        String msg = '';
        try
        {
            List<Opportunity> lstOppty = [
                select  Id, Name, BI_ARG_CAPEX_total__c
                from    Opportunity
                where   BI_Numero_id_oportunidad__c =: proyecto ];
                 System.debug('lstOppty ===> '+lstOppty);
            
            if( lstOppty != null && lstOppty.size() > 0 )
            {
                Opportunity oppty = lstOppty.get(0);
                Decimal vlrCAPEX_total = Decimal.valueOf( capexTotal );
                oppty.BI_ARG_CAPEX_total__c = vlrCAPEX_total;
                update oppty;
                return armarRespuesta( 'True', 'Actualizacion de oportunidad Exitosa' );
            }
            else
                return armarRespuesta('False', 'No se encotro ninguna oportunidad con ese codigo de proyecto');
        }
        catch(Exception e)
        {
            return armarRespuesta('False', e.getMessage());
        }
        return msg;
    }

    public static BI_COL_Modificacion_de_Servicio__c consultaMS(String DS,String MS)
    {
        BI_COL_Modificacion_de_Servicio__c modficacionServ = [
            Select  Id, BI_COL_Codigo_unico_servicio__c, BI_COL_Fecha_comprometida_baja_tecnica__c, 
                    BI_COL_Clasificacion_Servicio__c, BI_COL_Estado__c, BI_COL_Fecha_reconexionl_servicio__c,
                    BI_COL_Fecha_Instalacion_Comprometida__c, BI_COL_Causal_aplazamiento__c, BI_COL_Fecha_quiebre__c, 
                    BI_COL_Ingeniero_implantador__c, BI_COL_Parametros_tecnicos__c, BI_COL_IMEI__c, 
                    BI_COL_Numero_telefonico__c, BI_COL_Fecha_instalacion_servicio_RFS__c, 
                    BI_COL_Fecha_fin_demo_SO_autoconsumo__c, BI_COL_Fecha_fin__c
            from    BI_COL_Modificacion_de_Servicio__c m  
            where   Name =: MS 
            and     BI_COL_Codigo_unico_servicio__r.Name =: DS Limit 1];
        return modficacionServ;
    }

    public static BI_COL_Descripcion_de_servicio__c Dsinfo(String nombreds)
    {
        List<BI_COL_Descripcion_de_servicio__c> dsupdate = [
            Select  Name, Id 
            from    BI_COL_Descripcion_de_servicio__c 
            where   Name =: nombreds ];
        
        if( dsupdate.isEmpty() )
            return null;
        return dsupdate[0];
    }
    
    webservice static string respuestaWebServices( list<mensajeLog> mensajes )
    {
        string errorDetalles = '';
        
        try
        {
            map<string,mensajeLog> logData = new map<string,mensajeLog>();
            for( mensajeLog m : mensajes )
                logData.put(m.ms,m);

            if(!logData.isEmpty())
            {
                list<BI_COL_Modificacion_de_Servicio__c> Mslist = new list<BI_COL_Modificacion_de_Servicio__c>([
                    select  Id, Name, BI_COL_Codigo_unico_servicio__c,
                            BI_COL_Oportunidad__r.Owner.Email,
                            BI_COL_Oportunidad__r.BI_Numero_id_oportunidad__c,
                            BI_COL_Oportunidad__r.Account.Name,
                            BI_COL_FUN__r.Name,
                            LastModifiedBy.Email
                    from    BI_COL_Modificacion_de_Servicio__c 
                    where   Name IN: logData.keySet() ] );
                
                list<BI_Log__c> logList = new list<BI_Log__c>();
                Integer totalMS = 0;
                set<string> msFallida = new set<string>();  
                for( BI_COL_Modificacion_de_Servicio__c myMs : Mslist )
                {
                    mensajeLog idMensaje = logData.get( myMs.name );
                    string estado = ( idMensaje.cod == '0' ) ? 'EXITOSO' :'FALLIDO';
                    
                    if( estado == 'EXITOSO' )
                    {
                        myMs.BI_COL_Estado_orden_trabajo__c = 'En Desarrollo'; 
                        myMs.BI_COL_Fecha_liberacion_OT__c = System.now();
                        myMs.BI_COL_Numero_de_OT__c = idMensaje.ordenTrabajo;           
                    }
                    else
                    {
                        myMs.BI_COL_Estado_orden_trabajo__c = 'Fallido';            
                        myMs.BI_COL_Estado__c = 'Pendiente';
                        msFallida.add(myMs.id); 
                    }
                    
                    logList.add( new BI_Log__c( 
                        Descripcion__c = 'RESPUESTA TRS',
                        BI_COL_Modificacion_Servicio__c = myMs.Id,
                        BI_COL_Informacion_recibida__c = idMensaje.mensajeError,
                        BI_COL_Tipo_Transaccion__c = 'RESPUESTA TRS MASIVO',
                        BI_COL_Estado__c = estado ) );
                }
                
                Database.SaveResult[] lsr = database.update( Mslist, false);
                
                for( Database.SaveResult sr : lsr )
                { 
                    if(!sr.isSuccess())
                    {
                        Database.Error err = sr.getErrors()[0];
                        
                        for( Database.Error errt : sr.getErrors() )
                            errorDetalles += errt.getMessage();      
                    }
                }
                
                logList.add( new BI_Log__c( Descripcion__c = 'RESPUESTA TRS',
                    BI_COL_Informacion_recibida__c = 'No se pudo actualizar:\n' + errorDetalles,
                    BI_COL_Tipo_Transaccion__c = 'RESPUESTA TRS MASIVO',
                    BI_COL_Estado__c = 'FALLIDO' ) );
                
                insert logList;
                
                BI_COL_Contract_cls.msFallida( msFallida );
            }
        }
        catch(exception e)
        {
            insert new BI_Log__c( Descripcion__c ='RESPUESTA TRS E', BI_COL_Informacion_recibida__c = e.getMessage() );
        }
        return 'Ms Recibidas';
    }
    //webservice static String ActualizarInstalacionDS
    global class mensajeLog
    {
        WebService String ms{get;set;}
        WebService String cod{get;set;}
        WebService String mensaje{get;set;}
        WebService String mensajeError{get;set;}
        Webservice String ordenTrabajo{get;set;}
    }
    
}