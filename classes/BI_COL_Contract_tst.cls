/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:          
* Description:         
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-08-18      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_Contract_cls
*			 1.1	2015-08-19		Jeisson Rojas (JR)		Cambio realizado a la Query de Usuarios del campo Country por Pais__c
*			 1.2	2016-02-24		Javier Lopez (JavLo)	Cambios para evitar DML Exception
*			 1.3	2016-12-28		Julián Gawron			Cambios para facilitar trabajo async
*			 1.4    23-02-2017      Jaime Regidor           Adding Campaign Values, Adding Catalog Country
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
					19/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest
private class BI_COL_Contract_tst
{
	
	public static BI_COL_Contract_cls 							objBiContract;
	public static BI_COL_Modificacion_de_Servicio__c 			objMS;
	public static Account 										objCuenta;
	public static Account 										objCuenta2;
	public static Contact 										objContacto;
	public static Opportunity 									objOportunidad;
	public static BI_COL_Anexos__c 								objAnexos;
	public static BI_COL_Descripcion_de_servicio__c 			objDesSer;
	public static Contract 										objContrato;
	public static Contract 										objContrato2;
	public static Campaign 										objCampana;
	public static BI_Col_Ciudades__c 							objCiudad;
	public static BI_Sede__c 									objSede;
	public static User 											objUsuario;
	public static BI_Punto_de_instalacion__c 					objPuntosInsta;
	public static BI_Log__c 									objBiLog;
	public static RecordType									objRecTyp;

	public static list <Profile> 								lstPerfil;
	public static list <UserRole> 								lstRoles;
	public static list <Campaign> 								lstCampana;
	public static list<BI_COL_manage_cons__c> 					lstManege;
	public static List<RecordType> 								lstRecTyp;
	public static List<User> 									lstUsuarios;
	public static List<BI_COL_Modificacion_de_Servicio__c> 		lstMS;
	public static List<BI_COL_Descripcion_de_servicio__c> 		lstDS;


	@testSetup public static void crearData()
	{
		//perfiles
		lstPerfil               		= [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1 ];
		system.debug('datos Perfil '+lstPerfil);
				
		//usuarios
		//lstUsuarios = [Select Id FROM User where Pais__c = 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

        //Roles
			lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
			system.debug('datos Rol '+lstRoles);
			
        //ObjUsuario
        /*
        objUsuario = new User();
        objUsuario.Alias = 'standt';
        objUsuario.Email ='pruebas@test.com';
        objUsuario.EmailEncodingKey = '';
        objUsuario.LastName ='Testing';
        objUsuario.LanguageLocaleKey ='en_US';
        objUsuario.LocaleSidKey ='en_US'; 
        objUsuario.ProfileId = lstPerfil.get(0).Id;
        objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        objUsuario.UserName ='pruebas@test.com';
        objUsuario.EmailEncodingKey ='UTF-8';
        objUsuario.UserRoleId = lstRoles.get(0).Id;
        objUsuario.BI_Permisos__c ='Sucursales';
        objUsuario.Pais__c='Colombia';
        */
        objUsuario = BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);
        //JavLo Change for avoid System DML exception
        User me = [SELECT Id from User Where Id =:UserInfo.getUserId() LIMIT 1];
        User me1 =BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);
        me1.BI_Permisos__c ='Sucursales';
        me1.Pais__c='Colombia';
        me1.IsActive = true;
        System.runAs(me){
        	insert objUsuario;
        	insert me1;
        }
        
        
        lstUsuarios = new list<User>();
        lstUsuarios.add(objUsuario);

		System.runAs(lstUsuarios[0])
		{
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass;
			
			
			//Cuentas
			objCuenta 										= new Account();      
			objCuenta.Name                                  = 'prueba';
			objCuenta.BI_Country__c                         = 'Colombia';
			objCuenta.TGS_Region__c                         = 'América'; 
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Fraude__c                          = false;
		    objCuenta.BI_Segment__c                         = 'test';
            objCuenta.BI_Subsegment_Regional__c             = 'test';
            objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			system.debug('datos Cuenta '+objCuenta);
			
			objCuenta2                                       = new Account();
			objCuenta2.Name                                  = 'prueba';
			objCuenta2.BI_Country__c                         = 'Argentina';
			objCuenta2.TGS_Region__c                         = 'América';
			objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta2.CurrencyIsoCode                       = 'COP';
			objCuenta2.BI_Fraude__c                          = true;
            objCuenta.BI_Segment__c                         = 'test';
            objCuenta.BI_Subsegment_Regional__c             = 'test';
            objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta2;
			
			//Contactos
			objContacto                                     = new Contact();
			objContacto.LastName                        	= 'Test';
			objContacto.BI_Country__c             		    = 'Colombia';
			objContacto.CurrencyIsoCode           		    = 'COP'; 
			objContacto.AccountId                 		    = objCuenta.Id;
			objContacto.BI_Tipo_de_contacto__c				= 'Administrador Canal Online';
            //REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
            //REING_FIN
			Insert objContacto;
			
			//catalogo
			NE__Catalog__c objCatalogo 						= new NE__Catalog__c();
			objCatalogo.Name 								= 'prueba Catalogo';
			objCatalogo.BI_country__c                       = 'Colombia';
			insert objCatalogo; 
			
			//Campaña
			objCampana 										= new Campaign();
			objCampana.Name 								= 'Campaña Prueba';
			objCampana.BI_Catalogo__c 						= objCatalogo.Id;
			objCampana.BI_COL_Codigo_campana__c 			= 'prueba1';
			objCampana.BI_Country__c                        = 'Colombia';
            objCampana.BI_Opportunity_Type__c               = 'Fijo';
            objCampana.BI_SIMP_Opportunity_Type__c          = 'Alta';
			insert objCampana;
			lstCampana 										= new List<Campaign>();
			lstCampana 										= [select Id, Name from Campaign where Id =: objCampana.Id];
			system.debug('datos Cuenta '+lstCampana);
			
			//Tipo de registro
			lstRecTyp 										= [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
			system.debug('datos de FUN '+lstRecTyp);
			
			//Contrato
			objContrato2 									= new Contract ();
			objContrato2.AccountId 							= objCuenta.Id;
			objContrato2.StartDate 							=  System.today().addDays(+5);
			objContrato2.BI_COL_Formato_Tipo__c 				= 'Contrato Indefinido';
			objContrato2.ContractTerm 						= 12;
			objContrato2.BI_COL_Presupuesto_contrato__c 		= 1000000;
			objContrato2.StartDate							= System.today().addDays(+5);
			insert objContrato2;
			System.debug('datos Contratos ===>'+objContrato2);
			
			//FUN
			objAnexos               						= new BI_COL_Anexos__c();
			objAnexos.Name          						= 'FUN-0041414';
			objAnexos.RecordTypeId  						= lstRecTyp[0].Id;
			objAnexos.BI_COL_Contrato__c   					= objContrato2.Id;
			insert objAnexos;
			System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id+'\n\n --> objAnexos.BI_COL_Contrato__c '+objAnexos.BI_COL_Contrato__c);
			
			//Configuracion Personalizada
			BI_COL_manage_cons__c objConPer 				= new BI_COL_manage_cons__c();
			objConPer.Name                  				= 'Viabilidades';
			objConPer.BI_COL_Numero_Viabilidad__c 			= 46;
			objConPer.BI_COL_ConsProyecto__c     			= 1;
			objConPer.BI_COL_ValorConsecutivo__c  			=1;
			lstManege                      					= new list<BI_COL_manage_cons__c >();        
			lstManege.add(objConPer);
			insert lstManege;
			System.debug('======= configuracion personalizada ======= '+lstManege);
			
			//Oportunidad
			objOportunidad                                      	= new Opportunity();
			objOportunidad.Name                                   	= 'prueba opp';
			objOportunidad.AccountId                              	= objCuenta.Id;
			objOportunidad.BI_Country__c                          	= 'Colombia';
			objOportunidad.CloseDate                              	= System.today().addDays(+5);
			objOportunidad.StageName                              	= 'F5 - Solution Definition';
			objOportunidad.CurrencyIsoCode                        	= 'COP';
			objOportunidad.Certa_SCP__contract_duration_months__c 	= 12;
			objOportunidad.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
			objOportunidad.OwnerId                                	= lstUsuarios[0].id;
			insert objOportunidad;
			system.debug('Datos Oportunidad'+objOportunidad);
			list<Opportunity> lstOportunidad 	= new list<Opportunity>();
			lstOportunidad 						= [select Id from Opportunity where Id =: objOportunidad.Id];
			system.debug('datos ListaOportunida '+lstOportunidad);
			
			//Ciudad
			objCiudad = new BI_Col_Ciudades__c ();
			objCiudad.Name 						= 'Test City';
			objCiudad.BI_COL_Pais__c 			= 'Test Country';
			objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
			insert objCiudad;
			System.debug('Datos Ciudad ===> '+objSede);
			
			//Direccion
			objSede 								= new BI_Sede__c();
			objSede.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
			objSede.BI_Direccion__c 				= 'Test Street 123 Number 321';
			objSede.BI_Localidad__c 				= 'Test Local';
			objSede.BI_COL_Estado_callejero__c		= System.Label.BI_COL_LbValor3EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c 		= 'Libre';
			objSede.BI_Country__c 					= 'Colombia';
			objSede.Name 							= 'Test Street 123 Number 321, Test Local Colombia';
			objSede.BI_Codigo_postal__c 			= '12356';
			insert objSede;
			System.debug('Datos Sedes ===> '+objSede);
			
			//Sede
			objPuntosInsta 						= new BI_Punto_de_instalacion__c ();
			objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
			objPuntosInsta.BI_Sede__c          = objSede.Id;
			objPuntosInsta.BI_Contacto__c      = objContacto.id;
			objPuntosInsta.Name 				='No aparecen';
			insert objPuntosInsta;
			System.debug('Datos Sucursales ===> '+objPuntosInsta);
			
			//DS
			objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
			objDesSer.CurrencyIsoCode               			= 'COP';
			insert objDesSer;
			System.debug('Data DS ====> '+ objDesSer);
			System.debug('Data DS ====> '+ objDesSer.Name);
			lstDS 	= [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
			System.debug('Data lista DS ====> '+ lstDS);
			
			//MS
			objMS                         				= new BI_COL_Modificacion_de_Servicio__c();
			objMS.BI_COL_FUN__c           				= objAnexos.Id;
			objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
			objMS.BI_COL_Clasificacion_Servicio__c  	= 'ALTA';
			objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c 					= false;
			objMS.BI_COL_Estado__c 						= 'Activa';//label.BI_COL_lblActiva;
			objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
			objMs.BI_COL_Cargo_conexion__c				=3000;
			objMs.BI_COL_Cargo_fijo_mes__c 				=4000;
			objMs.BI_COL_Cobro_mensual_adicionales__c   =5000;
			objMs.BI_COL_Duracion_meses__c				=3;
			objMs.BI_COL_Cobro_unico_adicionales__c 	=200;
			objMs.BI_COL_TRM__c							=3000;
			insert objMS;
			system.debug('Data objMs ===>'+objMs);
			lstMS = [select Name, BI_COL_Codigo_unico_servicio__c,BI_COL_Producto__r.Name,BI_COL_Descripcion_Referencia__c,
							BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Sucursal_de_Facturacion__c, BI_COL_Codigo_unico_servicio__r.BI_COL_Sede_Destino__c, 
							BI_COL_Sucursal_Origen__c, BI_COL_Cuenta_facturar_davox__c,BI_COL_Monto_ejecutado__c, BI_COL_TRM__c,BI_COL_Cargo_conexion__c,BI_COL_Cargo_fijo_mes__c,BI_COL_Cobro_mensual_adicionales__c, BI_COL_Duracion_meses__c,BI_COL_Cobro_unico_adicionales__c from BI_COL_Modificacion_de_Servicio__c
					];
			System.debug('datos lista MS ===> '+lstMS);
			
			//objRecTyp = [ SELECT Id FROM RecordType Where SobjectType = 'Contract' Limit 1 ];	
			
		}	
	}
	static testMethod void test_method_1()
	{
		// TO DO: implement unit test
		
		//crearData();
		
		objCuenta = [Select Id from Account limit 1];
		List<User> lstUser = [ Select Id FROM User 
                              where BI_Permisos__c ='Sucursales' 
                              AND Pais__c='Colombia'
                              AND isActive = true
                              order by CreatedDate desc
                              limit 1];
		

		objBiContract = new BI_COL_Contract_cls();
		Set<Id> setStringId = new Set<Id>();
		List<Contract> lstContrato = new List<Contract>();
		Set<String> setMSId = new Set<String>();

		System.runAs(lstUser[0])
		{
			for ( Integer i = 0 ; i<5 ; i++)
			{
				
				objContrato 									= new Contract ();
				objContrato.AccountId 							= objCuenta.Id;
				objContrato.StartDate 							=  System.today().addDays(+5);
				objContrato.BI_COL_Formato_Tipo__c 				= 'Contrato Marco';
				objContrato.ContractTerm 						= 12;
				objContrato.BI_COL_Presupuesto_contrato__c 		= 1000000+i;
				objContrato.StartDate							= System.today().addDays(+5);
				//objContrato.BI_COL_Tipo_contrato__c				= objRecTyp.Id;
				objContrato.BI_COL_Presupuesto_inicial_contrato__c = 1000000-i;
				
				
				lstContrato.add(objContrato);
				System.debug('lista Contratos dato '+i+ '==============> \n'+lstContrato.get(i));
			}

			System.debug('lista Contratos ==============> \n'+lstContrato);

			Insert lstContrato;
			objContrato2 = [Select Id, AccountId, StartDate, BI_COL_Formato_Tipo__c, ContractTerm, 
			BI_COL_Presupuesto_contrato__c, BI_COL_Tipo_contrato__c, Status from Contract 
			where BI_COL_Formato_Tipo__c = 'Contrato Indefinido' limit 1];
			
			lstContrato.add( objContrato2 );
			
			/*objContrato = lstContrato.get(0);
			objContrato.Status = 'Firmado Activo';
			update objContrato;*/
			
			
			for ( Integer i = 0 ; i<6 ; i++)
			{
				setStringId.add(lstContrato.get(i).Id);
				System.debug('Set id Contratos dato '+i+ '==============> \n'+setStringId);
			}

			lstMS = [select Id from BI_COL_Modificacion_de_Servicio__c ];


			setMSId.add(lstMS.get(0).Id);
			
		}

		objBiContract.validaMSFacturacion(lstContrato,lstContrato);
		BI_COL_Contract_cls.invokeapexcallout(lstContrato);
		BI_COL_Contract_cls.asignaContrato(lstContrato);
		//objBiContract.impideModAnexos(lstContrato,lstContrato,setStringId);
		BI_COL_Contract_cls.motoEjecutado(setMSId);
		BI_COL_Contract_cls.msFallida(setMSId);
		
		//List<Profile> lstProfile = [ SELECT Id FROM profile WHERE Name != 'Administrador del sistema' ];
		//List<User> lstUser = [ Select Id FROM User where ProfileId =: lstProfile[0].Id And isActive = true Limit 1 ];
		
		/*System.runAs( lstUser[0] )
		{
			objBiContract.impideModAnexos(lstContrato,lstContrato,setStringId);
		}*/
		BI_COL_Contract_cls.msFallida( new set<string>() );
		BI_COL_Contract_cls.motoEjecutado( new set<string>() );

	}
	static testMethod void test_method_2()
	{
		// TO DO: implement unit test
		
		//crearData();
		objCuenta = [Select Id from Account limit 1];
		List<User> lstUser = [Select Id FROM User 
                              where BI_Permisos__c ='Sucursales' 
                              AND Pais__c='Colombia'
                              AND isActive = true
                              order by CreatedDate desc
                              limit 1];

		objBiContract = new BI_COL_Contract_cls();
		Set<Id> setStringId = new Set<Id>();
		List<Contract> lstContrato = new List<Contract>();
		Set<String> setMSId = new Set<String>();

		System.runAs(lstUser[0])
		{
			for ( Integer i = 0 ; i<5 ; i++)
			{
				
				objContrato 									= new Contract ();
				objContrato.AccountId 							= objCuenta.Id;
				objContrato.StartDate 							=  System.today().addDays(+5);
				objContrato.BI_COL_Formato_Tipo__c 				= 'Contrato indefinido';
				objContrato.ContractTerm 						= 12;
				objContrato.BI_COL_Presupuesto_contrato__c 		= 1000000+i;
				objContrato.StartDate							= System.today().addDays(+5);
				//objContrato.BI_COL_Tipo_contrato__c				= objRecTyp.Id;
				objContrato.BI_COL_Presupuesto_inicial_contrato__c = 1000000-i;
				
				lstContrato.add(objContrato);
				System.debug('lista Contratos dato '+i+ '==============> \n'+lstContrato.get(i));
			}

			System.debug('lista Contratos ==============> \n'+lstContrato);

			Insert lstContrato;
			objContrato2 = [Select Id, AccountId, StartDate, BI_COL_Formato_Tipo__c, ContractTerm, 
			BI_COL_Presupuesto_contrato__c, BI_COL_Tipo_contrato__c, Status from Contract 
			where BI_COL_Formato_Tipo__c = 'Contrato Indefinido' limit 1];

			lstContrato.add( objContrato2 );

			for ( Integer i = 0 ; i<6 ; i++)
			{
				
				setStringId.add(lstContrato.get(i).Id);
				System.debug('Set id Contratos dato '+i+ '==============> \n'+setStringId);
			}
			lstMS = [select Id from BI_COL_Modificacion_de_Servicio__c ];
			setMSId.add(lstMS.get(0).Id);
			
		}
		objBiContract.validaMSFacturacion(lstContrato,lstContrato);
		BI_COL_Contract_cls.invokeapexcallout(lstContrato);
		BI_COL_Contract_cls.asignaContrato(lstContrato);
		//objBiContract.impideModAnexos(lstContrato,lstContrato,setStringId);
		BI_COL_Contract_cls.motoEjecutado(setMSId);
		BI_COL_Contract_cls.msFallida(setMSId);
		
		//List<Profile> lstProfile = [ SELECT Id FROM profile WHERE Name != 'Administrador del sistema' ];
		//List<User> lstUser = [ Select Id FROM User where ProfileId =: lstProfile[0].Id And isActive = true Limit 1 ];
		
		/*System.runAs( lstUser[0] )
		{
			objBiContract.impideModAnexos(lstContrato,lstContrato,setStringId);
		}*/

	}
}