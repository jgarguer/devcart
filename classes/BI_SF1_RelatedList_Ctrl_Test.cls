@isTest
private class BI_SF1_RelatedList_Ctrl_Test {
	
	@isTest static void getRecords_Test() {
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Argentina'});
		String jsonRelatedList = '';
		String objName = 'Account';
		List<BI_SF1_Related_list_configuration__mdt> relatedListConfig = [SELECT Id, DeveloperName, MasterLabel, Language, NamespacePrefix, Label, QualifiedApiName, BI_SF1_Layout_assignments_SF1__c, BI_SF1_Nombre_lista_relacionada__c, BI_SF1_Icono__c, BI_SF1_Orden__c 
																		FROM BI_SF1_Related_list_configuration__mdt WHERE BI_SF1_Layout_assignments_SF1__r.BI_SF1_Objeto__c =:objName];
		jsonRelatedList = JSON.serialize(relatedListConfig);
		List<Map<String, object>> lst_return = BI_SF1_RelatedList_Ctrl.getRecords(lst_acc[0].Id, jsonRelatedList, objName);

		System.assert(lst_return!=null);
	}
	
}