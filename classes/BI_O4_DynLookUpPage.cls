global with sharing class BI_O4_DynLookUpPage {	

	public NE__Dynamic_Lookup__c dynLookup {get;set;}
	public NE__Lookup_Map__c lookupMap {get;set;}
	public String param {get;set;}											// Name of Dynamic Lookup to retrieve
	public String attributePfp {get;set;}									// Id of the productfamily property. Used to resolve the map
	public String isOffline {get;set;}										// Attribute of the product in the configurator
	public list<String> listColumns {get;set;}								// List of columns to display in the results table
	public list<String> listLabelColumns {get;set;}							// List with the labels of the columns to display in the results table
	public list<SObject> listResults {get;set;}								// List of the records returned by Dynamic Lookup
	public ApexPages.StandardSetController SSCResults {get;set;}			// StandardSetController with the list of records returned
	public ApexPages.StandardSetController SSCTotalResults {get;set;}		// StandardSetController with the total list of records returned
	public list<Condition> listOfConditions {get;set;}						// List of conditions used to filter the search
	public Integer selConditionRow {get;set;}								// Row number of the condition selected
	public String searchText {get;set;}										// Input text with the filter in the Search section
	public String columnsToSearchString {get;set;}							// List of columns to pass to the query
	public map<String,String> pageParameters {get;set;}						// Parameters passed from parent page
	public map<String,String> setAttributeFieldsMap {get;set;}				// Map key: Product Family Property ID, value: field name from Lookup Map Values
	public map<String,String> setAttributeValuesMap {get;set;}				// Map key: Product Family Property ID, value: value from Lookup Map Values
	public String selectedSobjectId {get;set;}								// ID of the record selected
	public String finalAttributeMap {get;set;}								// String with the mapping of setAttributeFieldsMap and setAttributeValuesMap
	public String productId {get;set;}										// used to find all the families related to a product
	
	
	public BI_O4_DynLookUpPage()
	{
		try
		{
			pageParameters 			= new map<String,String>();
			setAttributeFieldsMap 	= new map<String,String>();
			setAttributeValuesMap 	= new map<String,String>();
			pageParameters = ApexPages.currentPage().getParameters();
			system.debug('[BI_O4_DynLookUpPage] pageParameters: '+pageParameters);
			param 			=	pageParameters.remove('name');
			isOffline 		=	pageParameters.remove('offline');
			attributePfp	=	pageParameters.remove('attributePfp');
			productId		=	pageParameters.remove('productId');
			
			selectedSobjectId =	'';
			finalAttributeMap = '';			
			
			listOfConditions = new list<Condition>();
			searchText = '';
			listColumns = new list<String>();
			
			list<NE__Lookup_Map__c> listOfMaps;
			
			if(param != null)
			{
				dynLookup 		= [SELECT Id, NE__Columns__c, NE__Name__c, NE__Search_enabled__c, NE__Search_specification__c, NE__sObject__c 
							 		FROM NE__Dynamic_Lookup__c WHERE NE__Name__c =: param limit 1];
				system.debug(dynLookup);
				
				//GC 3.10 get all the families from the product instead of the one in context
				NE__ProductFamilyProperty__c productFamilyProperty	=	[SELECT NE__FamilyId__c FROM NE__ProductFamilyProperty__c WHERE id =: attributePfp];	
				list<NE__Family__c> familiesToFind					=	[SELECT id 
																	FROM NE__Family__c 
																	WHERE id IN
																		(SELECT NE__FamilyId__c FROM NE__ProductFamily__c WHERE NE__ProdId__c =: productId)];	
				
				
				listOfMaps		=	[SELECT NE__Dynamic_Lookup__c, NE__Family__c, NE__Name__c, Name, Id, 
										(SELECT NE__Value__c,NE__Lookup_Map__c,Name,NE__Type__c,NE__Product_Family_Property__c FROM NE__Lookup_Map_Values__r) 
									FROM NE__Lookup_Map__c
									WHERE NE__Family__c	IN: familiesToFind
										AND NE__Dynamic_Lookup__c =: dynLookup.id];
										
				system.debug('Found '+listOfMaps.size()+' maps');
				
				if(listOfMaps.size() > 0)					
				{					
					//lookupMap	=	listOfMaps.get(0);
					
					//GC 3.10 cycle all maps found
					for(NE__Lookup_Map__c lookupMap:listOfMaps)
					{
						for(NE__Lookup_Map_Value__c lookupMapValue : lookupMap.NE__Lookup_Map_Values__r)
						{
							if(lookupMapValue.NE__Type__c == 'Field')
								setAttributeFieldsMap.put(lookupMapValue.NE__Product_Family_Property__c, lookupMapValue.NE__Value__c);	
							else
								setAttributeValuesMap.put(lookupMapValue.NE__Product_Family_Property__c, lookupMapValue.NE__Value__c);			
						}
					}			
					
					system.debug('[BI_O4_DynLookUpPage] setAttributeFieldsMap after constructor: '+setAttributeFieldsMap);		
				
					listColumns 			= 	dynLookup.NE__Columns__c.split(', ');
					Schema.SObjectType obj 	= 	Schema.getGlobalDescribe().get(dynLookup.NE__sObject__c);
					system.debug('[BI_O4_DynLookUpPage] listColumns: '+listColumns);
					map<String, Schema.SObjectField> fieldMap = obj.getDescribe().fields.getMap();
					system.debug('[BI_O4_DynLookUpPage] fieldMap: '+fieldMap);
					listLabelColumns 		= new list<String>();
					for(String colName : listColumns)
					{
						system.debug('[BI_O4_DynLookUpPage] colName: '+colName);
						listLabelColumns.add(fieldMap.get(colName).getDescribe().getLabel());
					}
					
					// Substitute parameters in Search_specification__c with page parameter value
					if(pageParameters.size() > 0 && dynLookup.NE__Search_specification__c != null && dynLookup.NE__Search_specification__c != '')
					{
						system.debug(dynLookup.NE__Search_specification__c);
						dynLookup.NE__Search_specification__c = dynLookup.NE__Search_specification__c.replace('= {','={');
						dynLookup.NE__Search_specification__c = dynLookup.NE__Search_specification__c.replace('= \'{','=\'{');
						system.debug(dynLookup.NE__Search_specification__c);

						for(String keyParam : pageParameters.keySet())						
						{
							system.debug('keyParam ' + keyParam + ' value ' + pageParameters.get(keyParam)) ;
							String param = '{!' + keyParam + '}';
							if(pageParameters.get(keyParam)==null || pageParameters.get(keyParam)=='' || pageParameters.get(keyParam)=='null'){
								system.debug('Inserting like') ;
								dynLookup.NE__Search_specification__c = dynLookup.NE__Search_specification__c.replace('=\'' + param + '\'',' LIKE \'%%\' ');
								dynLookup.NE__Search_specification__c = dynLookup.NE__Search_specification__c.replace('=' + param ,'!=null');
							}else{
								dynLookup.NE__Search_specification__c = dynLookup.NE__Search_specification__c.replace(param, pageParameters.get(keyParam));
								system.debug('Inserting param') ;
							}
						}
					}
					
					// Create a string with all the fields to search in the query in performSearch method
					list<String> columnsToSearch	=	dynLookup.NE__Columns__c.split(', ');
					
					//GC 3.10 cycle all maps found
					for(NE__Lookup_Map__c lookupMap:listOfMaps)
					{					
						for(NE__Lookup_Map_Value__c lookupMapValue : lookupMap.NE__Lookup_Map_Values__r)
						{
							if(lookupMapValue.NE__Type__c == 'Field')
								columnsToSearch.add(lookupMapValue.NE__Value__c);					
						}
					}
					
					Set<String> setOfColumnsToSearch	=	new Set<String>();
					setOfColumnsToSearch.addAll(columnsToSearch);
					
					columnsToSearchString	=	'';
					for(String column : setOfColumnsToSearch)
						columnsToSearchString	+=	column + ',';
						
					columnsToSearchString	=	columnsToSearchString.removeEnd(',');
					
		    		listResults = performSearch('');
		    		SSCTotalResults = SSCResults;
		    		
		    		newConditionRow();
				}
				else
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No map defined'));
			}
			
		}
		catch(Exception e)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e+' at line: '+e.getLineNumber()));
			system.debug('[BI_O4_DynLookUpPage] Exception found: '+e+' at line: '+e.getLineNumber());
		}
	}
	
	
	/* Create listResults array */
	public list<SObject> performSearch(String filter)
	{
		try
		{
			String query = 'SELECT ' + columnsToSearchString + ' FROM ' + dynLookup.NE__sObject__c;
			if(dynLookup.NE__Search_specification__c != '' && dynLookup.NE__Search_specification__c != null)
				query += ' WHERE ' + '(' + dynLookup.NE__Search_specification__c + ')';
				
			if(filter != '')
			{
				if(dynLookup.NE__Search_specification__c != '' && dynLookup.NE__Search_specification__c != null)
					query += ' AND ';
				else
					query += ' WHERE ';
				
				query += '(' + filter + ')';
			}
			
			query += ' ORDER BY ' + listColumns[0] +' ASC NULLS LAST';
			query += ' LIMIT 9000';		// Fix 3.8.19
			
			system.debug('[performSearch] Query to do: '+query);
				
			SSCResults = new ApexPages.StandardSetController(Database.getQueryLocator(query));
            SSCResults.setPageSize(20);
    		SSCResults.first();
    		list<SObject> result = (list<SObject>) SSCResults.getRecords();
    		return result;
		}
		catch(Exception e)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e+' at line: '+e.getLineNumber()));
			system.debug('[performSearch] Exception found: '+e+' at line:'+e.getLineNumber());
			return null;
		}
	}
	
	
	/* Generate a string with lookup mapping to pass to cart configuration */
	public void generateAttributeMap()
	{
		try
		{
			String filterSelect					=	'id = \'' + selectedSobjectId + '\'';
			list<SObject> listOfSobjectToPick	=	performSearch(filterSelect);
			
			SObject sObjectToPick				=	listOfSobjectToPick.get(0);
			
			system.debug('[generateAttributeMap] setAttributeFieldsMap before generateAttributeMap: '+setAttributeFieldsMap);
			
			for(String fieldKey : setAttributeFieldsMap.keySet())
			{
				String fieldToGet	=	setAttributeFieldsMap.get(fieldKey);
				String fieldValue 	= 	String.valueOf(sObjectToPick.get(fieldToGet));
				setAttributeFieldsMap.put(fieldKey, fieldValue);
			}
			
			system.debug('[generateAttributeMap] setAttributeFieldsMap: '+setAttributeFieldsMap);
			system.debug('[generateAttributeMap] setAttributeValuesMap: '+setAttributeValuesMap);
			
			finalAttributeMap = '';
			for(String fieldKey : setAttributeFieldsMap.keySet())
			{
				if(setAttributeFieldsMap.get(fieldKey) == null)
					setAttributeFieldsMap.put(fieldKey, '');
				finalAttributeMap += fieldKey + '||' + setAttributeFieldsMap.get(fieldKey) + '|_|';
			}
			
			for(String valueKey : setAttributeValuesMap.keySet())
			{
				if(setAttributeValuesMap.get(valueKey) == null)
					setAttributeValuesMap.put(valueKey, '');
				finalAttributeMap += valueKey + '||' + setAttributeValuesMap.get(valueKey) + '|_|';
			}
			
			finalAttributeMap = finalAttributeMap.removeEnd('|_|');
			system.debug('[generateAttributeMap] finalAttributeMap: '+finalAttributeMap);
		}
		catch(Exception e)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e+' at line: '+e.getLineNumber()));
			system.debug('[generateAttributeMap] Exception found: '+e+' at line:'+e.getLineNumber());
		}
	}
	
	
	/* Run the filtered search */
    public void filterResults()
    {
    	if(SSCTotalResults.getResultSize() > 0)
    	{
    		if(listOfConditions.size() > 0)
    		{
    			String filter = translateFilterLogic();
    			if(filter != '')
	    			listResults = performSearch(filter);
	    		else
    			{
    				SSCResults = SSCTotalResults;
		    		SSCResults.first();
					listResults = (list<SObject>) SSCResults.getRecords();
    			}
    		}
    		else
    		{
    			// If listOfConditions is empty, returns the original array with all the results.
    			SSCResults = SSCTotalResults;
	    		SSCResults.first();
				listResults = (list<SObject>) SSCResults.getRecords();
    		}
    	}
    }
    
	
	/* Generate WHERE condition from searchText and listOfConditions */
	public String translateFilterLogic()
	{
		try
		{
			String total = '';
			map<String, Schema.SObjectField> fieldsMap = Schema.getGlobalDescribe().get(dynLookup.NE__sObject__c).getDescribe().fields.getMap();
			
			if(searchText != '' && searchText != null && !searchText.contains('null'))
			{
				//Avoid sql injection
				searchText	=	String.escapeSingleQuotes(searchText);
				
				list<String> searchElements = searchText.splitByCharacterType();	// Example: logic = '1AND(2 OR 3)', logicElements = {'1','AND','(','2',' ','OR',' ','3',')'}
				system.debug('[translateFilterLogic] logicElements: '+searchElements);
				
				for(String elem : searchElements)
				{
					// A number is equal to a condition
					if(elem.isNumeric())
					{
						if(Integer.valueOf(elem) <= listOfConditions.size() && Integer.valueOf(elem) > 0)
						{
							Condition cond = listOfConditions.get(Integer.valueOf(elem)-1);
							
							if(cond.field != null && cond.field != '')
							{
								if(cond.value == '')
									cond.value = 'null';
								
								Schema.SObjectField field = fieldsMap.get(cond.field);
		            			Schema.DisplayType fldType = field.getDescribe().getType();
		            			
								if(cond.value != null && (fldType == Schema.DisplayType.String || fldType == Schema.DisplayType.TextArea || fldType == Schema.DisplayType.ID || fldType == Schema.DisplayType.Picklist || fldType == Schema.DisplayType.Reference))
		            			{
		            				if(cond.operation != '=' || fldType == Schema.DisplayType.ID || fldType == Schema.DisplayType.Reference)
		            				{
			            				if(cond.value.startsWith('\'') && cond.value.endsWith('\''))
			            					total += cond.field + ' ' + cond.operation + ' ' + cond.value;
			            				else
			            					total += cond.field + ' ' + cond.operation + ' \'' + cond.value + '\'';
		            				}
		            				else
		            				{
		            					if(cond.value.startsWith('\'') && cond.value.endsWith('\''))
		            					{
		            						String val = cond.value;
		            						val = val.removeStart('\'');
		            						val = val.removeEnd('\'');
			            					total += cond.field + ' LIKE \'%' + val + '%\'';
		            					}
			            				else
			            					total += cond.field + ' LIKE \'%' + cond.value + '%\'';
		            				}
		            			}
		            			else
									total += cond.field + ' ' + cond.operation + ' ' + cond.value;
							}
						}
						else
						{
							ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Filter logic not valid'));
							return '';
						}
					}
					// Logical operators, parentheses, spaces, etc.
					else
					{
						total += elem;
					}
				}
			}
			else
			{
				for(Integer i=0; i<listOfConditions.size(); i++)
				{
					Condition cond = listOfConditions[i];
					
					if(cond.field != null && cond.field != '')
					{
						if(cond.value == '')
							cond.value = null;
						
						Schema.SObjectField field = fieldsMap.get(cond.field);
						Schema.DisplayType fldType = field.getDescribe().getType();
						
		            	if(cond.value != null && (fldType == Schema.DisplayType.String || fldType == Schema.DisplayType.TextArea || fldType == Schema.DisplayType.ID || fldType == Schema.DisplayType.Picklist || fldType == Schema.DisplayType.Reference))
		            	{
		            		if(cond.operation != '=' || fldType == Schema.DisplayType.ID || fldType == Schema.DisplayType.Reference)
            				{
	            				if(cond.value.startsWith('\'') && cond.value.endsWith('\''))
	            					total += cond.field + ' ' + cond.operation + ' ' + cond.value;
	            				else
	            					total += cond.field + ' ' + cond.operation + ' \'' + cond.value + '\'';
            				}
            				else
            				{
            					if(cond.value.startsWith('\'') && cond.value.endsWith('\''))
            					{
            						String val = cond.value;
            						val = val.removeStart('\'');
            						val = val.removeEnd('\'');
	            					total += cond.field + ' LIKE \'%' + val + '%\'';
            					}
	            				else
	            					total += cond.field + ' LIKE \'%' + cond.value + '%\'';
            				}
		            	}
		            	else
		            		total += cond.field + ' ' + cond.operation + ' ' + cond.value;
		            	
		            	if(i < listOfConditions.size()-1)
		            		total += ' AND ';
					}
				}
			}
			system.debug('[translateFilterLogic] Search: '+total);
			return total;
		}
		catch(Exception e)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e+' at line: '+e.getLineNumber()));
			system.debug('[translateFilterLogic] Exception found: '+e+' at line: '+e.getLineNumber());
			return '';
		}
	}
	
    
    /* Return a picklist with all the fields of the object selected */
	public list<SelectOption> getFields()
	{
		list<SelectOption> options = new list<SelectOption>();
		
		if(dynLookup.NE__sObject__c != '' && dynLookup.NE__sObject__c != null)
		{
			map<String, Schema.SObjectType> objectList = Schema.getGlobalDescribe();
			Schema.SObjectType obj = objectList.get(dynLookup.NE__sObject__c);
			map<String, Schema.SObjectField> fieldMap = obj.getDescribe().fields.getMap();
			
			if(dynLookup.NE__Search_enabled__c != '' && dynLookup.NE__Search_enabled__c != null)
			{
				// Return only fields specified in Search_enabled__c
				list<String> listAvailableFields = dynLookup.NE__Search_enabled__c.split(', ');
				for(String fieldName : listAvailableFields)
				{
					String fieldLabel = fieldMap.get(fieldName).getDescribe().getLabel();
					options.add(new SelectOption(fieldName, fieldLabel));
				}
			}
			/*
			else
			{
				// Return all the fields of the object
				for(String fieldName : fieldMap.keySet())
				{
					String fieldLabel = fieldMap.get(fieldName).getDescribe().getLabel();
					options.add(new SelectOption(fieldName, fieldLabel));
				}
			}*/
		}
		
		return sortOptionList(options);
	}
	
	
	/* This is a simple quicksort algorithm to sort a SelectOption list by label alphabetically */
	public static list<SelectOption> sortOptionList(list<SelectOption> ListToSort)
	{
		if(ListToSort == null || ListToSort.size() <= 1)
		{
			return ListToSort;					
		}
		
		list<SelectOption> Less = new list<SelectOption>();		
		list<SelectOption> Greater = new list<SelectOption>();	
			
		Integer pivot = 0;				
		// Save the pivot and remove it from the list		
		SelectOption pivotValue = ListToSort[pivot];		
		ListToSort.remove(pivot);				
		for(SelectOption x : ListToSort)
		{			
			if(x.getLabel().compareTo(pivotValue.getLabel()) <= 0)
				Less.add(x);
			else if(x.getLabel().compareTo(pivotValue.getLabel()) > 0)
				Greater.add(x);
		}		
		list<SelectOption> returnList = new list<SelectOption> ();		
		returnList.addAll(sortOptionList(Less));
		returnList.add(pivotValue);	
		returnList.addAll(sortOptionList(Greater));	
		
		return returnList; 
	}
	
	
	/* Add a new row to listOfConditions */
	public void newConditionRow()
	{
		Condition newCond = new Condition();
		listOfConditions.add(newCond);		
	}
	
	
	/* Delete selected row from listOfConditions */
	public void deleteConditionRow()
	{
		listOfConditions.remove(selConditionRow);
		// Call newConditionRow in order to make available at least a row
		if(listOfConditions.size() == 0)
			newConditionRow();
	}
	
	
	/* Returns true if there are pages next to the current one */
    public Boolean thereIsNextPage
    {
    	get {
	        if(SSCResults != null)             
				return SSCResults.getHasNext();
			else
				return false;
    	}
    	set;
    }
   
   
    /* Returns true if there are pages previous to the current one */
    public Boolean thereIsPreviousPage
    {
        get { 
          if(SSCResults != null)
              return SSCResults.getHasPrevious();  
            else
              return false;
        }  
        set;  
    }
    
    
	/* Update listResults with items of the next page */
	public void goToNextPage()
	{
		SSCResults.next();
    	listResults = (list<SObject>) SSCResults.getRecords();
	}
  	
  	
	/* Update listResults with items of the previous page */
	public void goToPreviousPage()
	{
 	    SSCResults.previous();
    	listResults = (list<SObject>) SSCResults.getRecords();
	}
	
	
	//---------------------------------------------------------------------------
	//----------------------------- WRAPPER CLASSES -----------------------------
	//---------------------------------------------------------------------------
	
	public class Condition
	{
		public String field {get;set;}
		public String operation {get;set;}
		public String value {get;set;}
		
		public Condition() {}
		
		public list<SelectOption> listOfOperations
		{
			get
			{
				list<SelectOption> listOfOp = new list<SelectOption>();
				
				listOfOp.add(new SelectOption('=', '='));
				listOfOp.add(new SelectOption('!=', '!='));
				listOfOp.add(new SelectOption('<', '<'));
				listOfOp.add(new SelectOption('<=', '<='));
				listOfOp.add(new SelectOption('>', '>'));
				listOfOp.add(new SelectOption('>=', '>='));
				
				return listOfOp;
			}
			set;
		}
	}
}