@RestResource(urlMapping='//restapi/*')
global with sharing class SER_CaseManagement_RESTAPI  {

    @HttpGet
    global static Case show() {

        Case obj;

        return obj;

    }

    @HttpPost
    global static Case create() {

        Case obj;

        return obj;

    }

    @HttpPut
    global static Case updateObject() {

        Case obj;

        return obj;

    }

    @HttpDelete
    global static void remove() {

    }

}