@isTest 
private class BI_LogHelper_TEST {
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   Author:        Ignacio Llorca
   Company:       Salesforce.com
   Description:   Test class to manage the coverage code for BI_LogHelper class 
   
   History: 
   
   <Date>                  <Author>                <Change Description>
   20/06/2014              Ignacio Llorca          Initial Version
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   Author:        Ignacio Llorca
   Company:       Salesforce.com
   Description:   Test method to manage the code coverage for generateLog
   
   History: 
   
   <Date>                   <Author>                <Change Description>
   20/06/2014               Ignacio Llorca          Initial Version        
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   static testMethod void generateLogTest() {
	
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		string subject 		= 'Test subject';
		string fBlock 		= 'Test fBlock';
		string description 	= 'Test description';
		string component 	= 'Test component';

		BI_LogHelper.generateLog(subject, fBlock, description, component);
		list<BI_Log__c> lst_log = [SELECT Id, BI_Asunto__c FROM BI_Log__c WHERE BI_Asunto__c = :subject];
		system.assert(!lst_log.isEmpty());
	   
	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos
	Company:       Salesforce.com
	Description:   Test method to manage the code coverage for generateLog_future

	History:     
	
	<Date>                  <Author>                <Change Description>
	28/11/2014              Micah Burgos             Initial Version
	08/09/2016				Guillermo Muñoz			Throw exception set to false to avoid errors.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void generateLog_future_Test(){
		Test.startTest();
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_TestUtils.throw_exception = false;

		BI_LogHelper.generateLog_future('Test Log', 'Test Log', 'Test Log', 'Test Log', Datetime.now());
		Test.stopTest();

		List<BI_Log__c> list_logs = [SELECT BI_Asunto__c,BI_Bloque_funcional__c,BI_Descripcion__c,BI_Tipo_de_componente__c FROM BI_Log__c];
		system.assert(!list_logs.isEmpty());

		for(BI_Log__c log :list_logs){
			system.assert( log.BI_Descripcion__c.contains('Future Log') );
		} 

	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos
	Company:       Salesforce.com
	Description:   Test method to manage the code coverage for generateLog_future
	
	History:
	
	<Date>            <Author>          <Description>
	05/05/2014        Micah Burgos       Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void generate_BILog_Test(){
		
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		String Exception_Test = 'Custom Exception for Test';

		Test.startTest();
		try{
			throw new BI_Exception(Exception_Test);
		}catch(Exception exc){
			BI_LogHelper.generate_BILog('Test Log', 'Test Log', exc, 'Test Log');
		}
		Test.stopTest();


		List<BI_Log__c> list_logs = [SELECT BI_Asunto__c,BI_Bloque_funcional__c,BI_Descripcion__c,BI_Tipo_de_componente__c FROM BI_Log__c];
		system.assert(!list_logs.isEmpty());

		for(BI_Log__c log :list_logs){
			system.assert( log.BI_Descripcion__c.contains(Exception_Test) );
		} 
	}	

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Pablo Bordas
	Company:       everis
	Description:   Test method to manage the code coverage for generate_BILog_DML
	
	History:
	
	<Date>            <Author>          <Description>
	07/03/2018        Pablo Bordas      Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void generate_BILog_GenericAccount(){
		
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
		String Exception_Test = 'Custom Exception for Test';
		String subjectStr = 'SubjectTest_generate_BILog_Generic';
        String fBlockStr = 'fBlockTest_generate_BILog_Generic';
        String componentStr = 'componentTest_generate_BILog_Generic';
        Account accountTest = new Account();
		Test.startTest();
		try{
			throw new BI_Exception(Exception_Test);
		}catch(Exception exc){
			BI_LogHelper.generate_BILog_Generic(subjectStr, fBlockStr, exc, componentStr, accountTest);
		}
		Test.stopTest();
        
        List<BI_Log__c> list_logs = [SELECT BI_Asunto__c,BI_Bloque_funcional__c,BI_Descripcion__c,BI_Tipo_de_componente__c FROM BI_Log__c];
		system.assert(!list_logs.isEmpty());

		for(BI_Log__c log :list_logs){
			system.assert( log.BI_Descripcion__c.contains(Exception_Test) );
		} 
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Pablo Bordas
	Company:       everis
	Description:   Test method to manage the code coverage for generate_BILog_DML
	
	History:
	
	<Date>            <Author>          <Description>
	07/03/2018        Pablo Bordas      Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void generate_BILog_GenericContact(){
		
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
		String Exception_Test = 'Custom Exception for Test';
		String subjectStr = 'SubjectTest_generate_BILog_Generic';
        String fBlockStr = 'fBlockTest_generate_BILog_Generic';
        String componentStr = 'componentTest_generate_BILog_Generic';
        Contact contactTest = new Contact();
		Test.startTest();
		try{
			throw new BI_Exception(Exception_Test);
		}catch(Exception exc){
			BI_LogHelper.generate_BILog_Generic(subjectStr, fBlockStr, exc, componentStr, contactTest);
		}
		Test.stopTest();
        
        List<BI_Log__c> list_logs = [SELECT BI_Asunto__c,BI_Bloque_funcional__c,BI_Descripcion__c,BI_Tipo_de_componente__c FROM BI_Log__c];
		system.assert(!list_logs.isEmpty());

		for(BI_Log__c log :list_logs){
			system.assert( log.BI_Descripcion__c.contains(Exception_Test) );
		} 
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Pablo Bordas
	Company:       everis
	Description:   Test method to manage the code coverage for generate_BILog_DML
	
	History:
	
	<Date>            <Author>          <Description>
	07/03/2018        Pablo Bordas      Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void generate_BILog_GenericContract(){
		
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
		String Exception_Test = 'Custom Exception for Test';
		String subjectStr = 'SubjectTest_generate_BILog_Generic';
        String fBlockStr = 'fBlockTest_generate_BILog_Generic';
        String componentStr = 'componentTest_generate_BILog_Generic';
        Contract contractTest = new Contract();
		Test.startTest();
		try{
			throw new BI_Exception(Exception_Test);
		}catch(Exception exc){
			BI_LogHelper.generate_BILog_Generic(subjectStr, fBlockStr, exc, componentStr, contractTest);
		}
		Test.stopTest();
        
        List<BI_Log__c> list_logs = [SELECT BI_Asunto__c,BI_Bloque_funcional__c,BI_Descripcion__c,BI_Tipo_de_componente__c FROM BI_Log__c];
		system.assert(!list_logs.isEmpty());

		for(BI_Log__c log :list_logs){
			system.assert( log.BI_Descripcion__c.contains(Exception_Test) );
		} 
	}    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Pablo Bordas
	Company:       everis
	Description:   Test method to manage the code coverage for generate_BILog_DML
	
	History:
	
	<Date>            <Author>          <Description>
	07/03/2018        Pablo Bordas      Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void generate_BILog_GenericContractException(){
		
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
		String Exception_Test = 'System.DmlException';
		String subjectStr = 'SubjectTest_generate_BILog_Generic';
        String fBlockStr = 'fBlockTest_generate_BILog_Generic';
        String componentStr = 'componentTest_generate_BILog_Generic';
        Account accountTest = new Account();
		Test.startTest();
		try{
            accountTest.Name='==========================================================================================================================================================================================================================================================================================';			
            insert accountTest;
            //throw new BI_Exception(Exception_Test);
		}catch(Exception exc){
			BI_LogHelper.generate_BILog_Generic(subjectStr, fBlockStr, exc, componentStr, accountTest);
		}
		Test.stopTest();
        
        List<BI_Log__c> list_logs = [SELECT BI_Asunto__c,BI_Bloque_funcional__c,BI_Descripcion__c,BI_Tipo_de_componente__c FROM BI_Log__c];
		system.assert(!list_logs.isEmpty());

	}    
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}