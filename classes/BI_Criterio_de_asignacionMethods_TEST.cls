@isTest
private class BI_Criterio_de_asignacionMethods_TEST {
	

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Guillermo Muñoz
	Company:       New Energy Aborda
	Description:   Test method that manage the code coverage from BI_Criterio_de_asignacionMethods.fillCriteriaReference

	History:
	 
	<Date>              <Author>                   <Change Description>
	20/02/2017          Guillermo Muñoz            Initial Version
	-------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void fillCriteriaReference_TEST() {
		
		BI_TestUtils.throw_exception=false;

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_Proceso_de_asignacion__c proceso = new BI_Proceso_de_asignacion__c(Name = 'procesotest');
        insert proceso;

        //////////CoE_eHelp_Type__c y CoE_eHelp_Request_type__c nulos/////////
        BI_Criterio_de_asignacion__c crit = new BI_Criterio_de_asignacion__c(        
        	BI_Country__c = 'eHelp',
        	CoE_eHelp_Owner__c = 'BI_Nivel_1;BI_Nivel_2',
        	BI_Proceso_de_asignacion__c = proceso.Id,
        	CoE_eHelp_Type__c = null,
        	CoE_eHelp_Priority__c = 'Low',
        	CoE_eHelp_Request_type__c = null
        );
		insert crit;

		////////CoE_eHelp_Type__c y CoE_eHelp_Request_type__c no nulos////////
		BI_Criterio_de_asignacion__c crit2 = new BI_Criterio_de_asignacion__c(        
        	BI_Country__c = 'eHelp',
        	CoE_eHelp_Owner__c = 'BI_Nivel_1;BI_Nivel_2',
        	BI_Proceso_de_asignacion__c = proceso.Id,
        	CoE_eHelp_Type__c = 'Problem;Question',
        	CoE_eHelp_Priority__c = 'Low',
        	CoE_eHelp_Request_type__c = 'Add and update public views'
        );
		insert crit2;

		/////////////////////////Más de 255 caracteres//////////////////////
		BI_Criterio_de_asignacion__c crit3 = new BI_Criterio_de_asignacion__c(        
        	BI_Country__c = 'eHelp',
        	CoE_eHelp_Owner__c = 'BI_Nivel_1;BI_Nivel_2',
        	BI_Proceso_de_asignacion__c = proceso.Id,
        	CoE_eHelp_Type__c = 'Problem;Question',
        	CoE_eHelp_Priority__c = 'Low',
        	CoE_eHelp_Request_type__c = 'Update custom buttons and local functionality links;Update the settings for local custom fields.;Update Case and Lead settings.;Update approval users;Update layout - local session;Add users to Chatter Groups;Add and update IP ranges;Add new queue for Cases;Add and update email templates with HTML letterheads;Add and update email templates;Add and update templates for offer documents;Add and update dashboards;Add and update reports;Add and update fiscal year settings;Add and update search settings;Add and update public views'
        );
		insert crit3;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Guillermo Muñoz
	Company:       New Energy Aborda
	Description:   Test method that manage the code coverage from all exceptions thrown in BI_Criterio_de_asignacionMethods.fillCriteriaReference

	History:
	 
	<Date>              <Author>                   <Change Description>
	20/02/2017          Guillermo Muñoz            Initial Version
	-------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void exceptions(){

		BI_TestUtils.throw_exception=true;

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_Proceso_de_asignacion__c proceso = new BI_Proceso_de_asignacion__c(Name = 'procesotest');
        insert proceso;

        BI_Criterio_de_asignacion__c crit = new BI_Criterio_de_asignacion__c(        
        	BI_Country__c = 'eHelp',
        	CoE_eHelp_Owner__c = 'BI_Nivel_1;BI_Nivel_2',
        	BI_Proceso_de_asignacion__c = proceso.Id,
        	CoE_eHelp_Type__c = null,
        	CoE_eHelp_Priority__c = 'Low',
        	CoE_eHelp_Request_type__c = null
        );
		insert crit;		
	}	
}