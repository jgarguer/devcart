public class BI_G4C_Test_Auxiliar {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Miguel Molina Cruz
Company:       everis
Description:   This class generates data for the Test Class BI_G4C_Visitas_Controller_TEST

History: 

<Date>                          <Author>                    	<Change Description>
20/06/2016                      Miguel Molina Cruz			    Initial version
20/09/2017                       Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c   
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static Campaign InsertCampaign(Id IdCat){
        
        
        Campaign ObjetoCampaign =new Campaign();
        ObjetoCampaign.Name='Campaña Prueba'; 
        ObjetoCampaign.BI_Catalogo__c = IdCat;
        ObjetoCampaign.BI_Country__c='Colombia';
        ObjetoCampaign.BI_Opportunity_Type__c ='Fijo' ;
        ObjetoCampaign.BI_SIMP_Opportunity_Type__c ='Alta';
      
        return ObjetoCampaign;
    }
    
    public static NE__Catalog__c InsertCatalogo(){
        
        NE__Catalog__c ObjetoCatalogo =new NE__Catalog__c();
        ObjetoCatalogo.Name='Catalogo Prueba';
        ObjetoCatalogo.BI_Country__c = 'Colombia';
        
        return ObjetoCatalogo;
    }
    
    public static event InsertEvent(Id AccId){
        
        Event ObjetoEvent =new Event();
        
        ObjetoEvent.Location='';
        ObjetoEvent.EndDateTime=Datetime.newInstance(2016, 6, 19);
        ObjetoEvent.Description='Esto es una prueba';
        ObjetoEvent.StartDateTime=Datetime.newInstance(2016, 6, 12);
        ObjetoEvent.Subject='Prueba';
        
        ObjetoEvent.WhatId = AccId;
        ObjetoEvent.BI_FVI_Estado__c='Iniciada';
        
        return ObjetoEvent;    	
    }
    
    public static Contact InsertContact(Id AccId){
        Contact ObjetoContact =new Contact();
        
        ObjetoContact.FirstName='Miguel';
        ObjetoContact.LastName='Molina';
        ObjetoContact.AccountId=AccId;
        
        return ObjetoContact;
    }
    
    public static Account InsertAccount(){
        Account ObjetoAccount =new Account();
        
        ObjetoAccount.Name='Radem';
        ObjetoAccount.BI_Segment__c                         = 'test';
        ObjetoAccount.BI_Subsegment_Regional__c             = 'test';
        ObjetoAccount.BI_Territory__c                       = 'test';
                return ObjetoAccount;
    }
    
    public static Opportunity InsertOpportunity(Id AccId){
        Opportunity ObjetoOpportunity =new Opportunity();
        ObjetoOpportunity.Name='opoPrueba';
        ObjetoOpportunity.AccountId=AccId;
        ObjetoOpportunity.CloseDate=Date.today();
        ObjetoOpportunity.StageName='Open';
        ObjetoOpportunity.BI_Ciclo_ventas__c ='Completo';
        ObjetoOpportunity.OwnerId=UserInfo.getUserId();
        ObjetoOpportunity.BI_Oportunidad_suspendida_por_riesgo__c = true;
        
        return ObjetoOpportunity;
    }
}