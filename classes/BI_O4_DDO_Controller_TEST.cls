/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Miguel Cabrera
 Company:       New Energy Aborda
 Description:   Test class for BI_O4_DDO_Controller class for "Asignar a oficinas" DDO Case button
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 28/08/2016          Miguel Cabrera             Initial Version
 20/09/2017          Antonio Mendivil           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
 25/09/2017          Jaime Regidor              Fields BI_Subsector__c and BI_Sector__c added
--------------------------------------------------------------------------------------------------------------------------------------------------------*/


@isTest
private class BI_O4_DDO_Controller_TEST {

  static{
    BI_TestUtils.throw_exception = false;
  }

    @isTest static void testMyController() {
        
    User me = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];

    System.runAs(me){
      CaseTeamRole ctr = new CaseTeamRole(Name = 'Propietario Caso Padre', AccessLevel = 'Edit');
      insert ctr;
    }
      
      BI_TestUtils.throw_exception = false;        
        RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' Limit 1];
      User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();

		List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'test',
                                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Segment__c = 'Empresas',
                                  BI_Subsector__c = 'test', //25/09/2017
                                  BI_Sector__c = 'test', //25/09/2017
                                  BI_Subsegment_local__c = 'Top 1',
                            	    RecordTypeId = rt1.Id,
                                  BI_Subsegment_Regional__c = 'test',
                                  BI_Territory__c = 'test'
                                  );    
        lst_acc1.add(acc1);
      System.runAs(usu){
        insert lst_acc1;
      }

		List<Account> lst_acc = new List<Account>();       
        Account acc = new Account(Name = 'test',
                                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Segment__c = 'Empresas',
                                  BI_Subsector__c = 'test', //25/09/2017
                                  BI_Sector__c = 'test', //25/09/2017
                                  BI_Subsegment_local__c = 'Top 1',
                                  ParentId = lst_acc1[0].Id,
                                  BI_Subsegment_Regional__c = 'test',
                                  BI_Territory__c = 'test'
                                  );    
        lst_acc.add(acc);
      System.runAs(usu){
        insert lst_acc;
      }

        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

        Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                                          AccountId = lst_acc[0].Id,
                                          BI_Ciclo_ventas__c = Label.BI_Completo, 
                                          RecordTypeId = rt.Id
                                          );    
        insert opp;

 		RecordType rt2 = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'BI_O4_Gen_rico_Preventa' limit 1];

        Case testCase = new Case();    
        
        testCase.Subject = 'Test Subject Opp';
        testCase.AccountId = opp.AccountId;
        testCase.RecordTypeId = rt2.Id;
        testCase.BI_Nombre_de_la_Oportunidad__c = opp.Id;
        testCase.BI_Country__c = opp.BI_Country__c;
        testCase.BI_Department__c = 'Ingeniería preventa';
        testCase.BI_Type__c = 'Solicitar Cotización';
        testCase.BI_O4_DDO__c = 'Venezuela';	
		testCase.BI_O4_N_Sites__c = 1;
		testCase.BI_O4_Tipo_oferta__c = 'Otro';
		testCase.Status = 'Pendiente Valoración';
		insert testCase;

		ApexPages.StandardController standardC = new ApexPages.StandardController(testCase); 
		BI_O4_DDO_Controller controller = new BI_O4_DDO_Controller(standardC);

     	controller.assignOffice();
      	controller.cancel();
     	System.assertNotEquals(null, testCase.Id);
    }
}