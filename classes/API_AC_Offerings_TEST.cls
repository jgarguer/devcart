@isTest
public class API_AC_Offerings_TEST {
    
    @testSetup
    static void createTestServiceCost() {
         // Create test record
        CWP_AC_ServicesCost__c serviceCostTest = new CWP_AC_ServicesCost__c(
            CWP_AC_Country__c='Spain',
            CWP_AC_City__c='Madrid',            
            CWP_AC_Contract_Term__c = 12,
            CWP_AC_ServiceName__c = 'INTERNET ACCESS',
            CWP_AC_Bandwidth__c = 100000,
            CWP_AC_Router__c = 'YES'
            );
        insert serviceCostTest;
        
    }
    
    @isTest 
    public static void testGetOfferings() {

        // createTestServiceCost();
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = System.Label.BI_URLAPIOfferings;
        request.httpMethod = 'GET';
        request.params.put('category.id', 'Spain');
        request.params.put('category.name', 'Madrid');
        request.params.put('name', '12');
        RestContext.request = request;
        RestContext.response = response;

        // Call the method to test
        API_AC_Offerings.doGet();
        System.assertEquals('[{"name":"INTERNET ACCESS","frameworkAgreement":"YES","description":"100000"}]', response.responseBody.toString());
    }
}