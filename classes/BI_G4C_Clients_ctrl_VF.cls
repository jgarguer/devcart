public class BI_G4C_Clients_ctrl_VF {
    
    public List<mapElement> clientesMapa {get;set;}
    public List<mapElement> clientesMapaDirec {get;set;}
    public mapElement selectedClient {get;set;}
    public String l {get;set;}
    public String acc {get;set;}
    public Integer zoom {get;set;}
    public String zoomStr {get;set;}
    public boolean d {get;set;}
    public String centerMap {get;set;}
    
    public BI_G4C_Clients_ctrl_VF() {
        zoom = 10;
        //Default center map
        selectedClient = new mapElement();
        selectedClient.ShippingStreet = 'Barcelona';
        selectedClient.ShippingCity = 'Barcelona';
        selectedClient.ShippingState = 'Barcelona';
        selectedClient.ShippingLatitude='-34.6155729';
        selectedClient.ShippingLongitude='-58.5033598';
        centerMap= '{latitude:-34.6155729,longitude:-58.5033598}';
      //  centerMap= '{latitude:-34.6155729,longitude:-58.5033598}';
        
    }
    
    public void recargaLista( ) {
        centerMap='';
         centerMap= '{latitude:-34.6155729,longitude:-58.5033598}';
        System.debug('[BI_G4C_Clients_ctrl_VF]-recargaLista'+l);
        if (l!=null && Integer.valueOf(zoomStr)>-1) {
            try{
                List<mapElement> lo = (List<mapElement>)JSON.deserialize(l, List<mapElement>.class);
                System.debug('[BI_G4C_Clients_ctrl_VF]-recargaLista'+lo);
                clientesMapa=new List<mapElement>();
                clientesMapaDirec=new List<mapElement>();
                for (mapElement li :lo){
                    li.tipo= li.BI_G4C_Estado_Coordenadas=='Pendiente Regla' || li.BI_G4C_Estado_Coordenadas=='Pendiente Google';
                    li.icono=getIcon(li.icono);
                    System.debug('[BI_G4C_Clients_ctrl_VF]-@@@@'+'Comprobamos si acc es null '+ acc);
                    if (String.isEmpty(acc) && String.isEmpty(centerMap) ) {
                        //Tenemos que decidir el centro del mapa 
                        if(li.tipo)
                            centerMap= li.ShippingStreet+','+li.ShippingCity+','+li.ShippingState;
                        else 
                            centerMap= '{latitude:'+li.ShippingLatitude+',longitude:'+li.ShippingLongitude+'}';
                        
                    }   
					if(li.tipo) 
                        clientesMapaDirec.add(li);
                    else 
                        clientesMapa.add(li);
                    
                    System.debug('[BI_G4C_Clients_ctrl_VF]-@@@@'+li.Name+li);
                }
            }catch(exception e){}  
        }else {
            clientesMapa=new List<mapElement>();
            
        }
        //System.debug('[BI_G4C_Clients_ctrl_VF]-@@@@'+acc);
        if (acc != null && !String.isEmpty(acc)) {
            List<mapElement>  mp = (List<mapElement>) JSON.deserialize(acc, List<mapElement>.class);
            selectedClient = mp.get(0);
            System.debug('[BI_G4C_Clients_ctrl_VF]-$$$$'+selectedClient);
             if(selectedClient.BI_G4C_Estado_Coordenadas=='Pendiente Regla' || selectedClient.BI_G4C_Estado_Coordenadas=='Pendiente Google')
                 centerMap= selectedClient.ShippingStreet+','+selectedClient.ShippingCity+','+selectedClient.ShippingState;
            else 
                centerMap= '{latitude:'+selectedClient.ShippingLatitude+',longitude:'+selectedClient.ShippingLongitude+'}';
        } 
        System.debug('[BI_G4C_Clients_ctrl_VF]-$$$$'+centerMap);
        if (zoomStr != null && Integer.valueOf(zoomStr)>-1 && !String.isEmpty(zoomStr)) {
            zoom = integer.valueof(zoomStr);
        }
    }
    public String getIcon(String estado){
        
        
        String intermedio='BI_G4C_MarkerOrange.png';
        String finOK='BI_G4C_MarkerGreen.png';
        String finKO='BI_G4C_MarkerRed.png';
        String sinAccion='BI_G4C_MarkerBlue.png';
        String sinEstado='BI_G4C_MarkerGrey.png';
        String color=sinEstado;
        
        Map<String, String> mapIconos = new Map<String, String>();
        mapIconos.put('F6 - Prospecting',sinAccion);
        mapIconos.put('F5 - Solution Definition',intermedio);
        mapIconos.put('F4 - Offer Development',intermedio);
        mapIconos.put('F3 - Offer Presented',intermedio);
        mapIconos.put('F2 - Negotiation',intermedio);
        mapIconos.put('F1 - Closed Won',finOK);
        mapIconos.put('F1 - Closed Lost',finKO);
        mapIconos.put('F1 - Cancelled | Suspended',finKO);
        mapIconos.put('Closed Won',finOK);
        mapIconos.put('Closed Lost',finKO);
        mapIconos.put('Cancelled | Suspended',finKO);
        mapIconos.put('Open',intermedio);
        
        if (mapIconos.containsKey(estado)){
            color=mapIconos.get(estado);
        }
        return color;
    }
    
    public class mapElement {
        public String Name{get;set;}
        public String ShippingStreet{get;set;}
        public String ShippingCity{get;set;}
        public String ShippingState{get;set;}
        public String ShippingPostalCode{get;set;}
        public String ShippingLatitude {get;set;}
        public String ShippingLongitude {get;set;}
        public String icono{get;set;}
        public String BI_G4C_Estado_Coordenadas {get;set;}
        public Boolean tipo {get;set;}
        
    }
}