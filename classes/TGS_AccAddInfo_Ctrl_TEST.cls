/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis
Company:       Everis
Description:   Test class to manage the coverage code for TGS_AccAddInfo_Ctrl class 

<Date>                  <Author>                <Change Description>
01/06/2017              Everis		            Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class TGS_AccAddInfo_Ctrl_TEST {

	@isTest
    public static void getAAI(){
        
        //Account cuenta = new Account(Name ='Test');
        set <string> setStrings = new set <string>{
            'Account', 
            'Case',
            'NE__Order__c',
            'NE__OrderItem__c'
        };
        map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        Account cuenta = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
     	insert cuenta;
        
        TGS_Account_Additional_Information__c accAInfo = new TGS_Account_Additional_Information__c();
        accAInfo.TGS_Account__c = cuenta.Id;
        insert accAInfo;
        
        PageReference pageRef = Page.TGS_Account_Additional_Info;
        Test.setCurrentPage(pageRef);
     	pageRef.getParameters().put('id', String.valueOf(cuenta.Id));
        pageRef.getParameters().put('inline', '1');
     	ApexPages.StandardController sc = new ApexPages.StandardController(cuenta);
        
        TGS_AccAddInfo_Ctrl aaiCtrl = new TGS_AccAddInfo_Ctrl(sc);
		aaiCtrl.accInfo = accAInfo; 
        accAInfo = aaiCtrl.getaccInfo();
        aaiCtrl.checkAI();
        pageRef = aaiCtrl.saveAddInfo();
        pageRef = aaiCtrl.deleteAddInfo();
        accAInfo = aaiCtrl.getaccInfo();
        
        PageReference pageRef2 = Page.TGS_Account_Additional_Info;
        Test.setCurrentPage(pageRef2);
     	pageRef2.getParameters().put('id', String.valueOf(accAInfo.Id));
        pageRef2.getParameters().put('inline', '1');
     	ApexPages.StandardController sc2 = new ApexPages.StandardController(accAInfo);
        TGS_AccAddInfo_Ctrl aaiCtrl2 = new TGS_AccAddInfo_Ctrl(sc2);
        pageRef2 = aaiCtrl2.saveAddInfo();
        pageRef2 = aaiCtrl2.deleteAddInfo();
    }
    
}