@isTest
private class BI_NEContractAccAssocMethods_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   Methods to test NEContract Trigger and BI_NEContractMethods
    History:
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@testSetup
	static void loadData()
	{
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();      
        List<String>lst_region = new List<String>();
        lst_region.add('Peru');
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('PER');  
        List <Account> lst_acc = BI_DataLoad.loadAccounts(3, lst_region);

        List <NE__Contract__c> lst_con = new List<NE__Contract__c>();
        List <NE__Contract_Header__c> lst_head = new List<NE__Contract_Header__c>();
        List <NE__Product__c> lst_prod = new List<NE__Product__c>();
        List <NE__Contract_Line_Item__c> lst_cli = new List<NE__Contract_Line_Item__c>();

        Integer i = 2;

        
        for(Integer j=0; j<i; j++){
            NE__Contract_Header__c header = new NE__Contract_Header__c(NE__Actual_Qty__c = 3,
                                                                       NE__Delta_Qty__c = 2,
                                                                       NE__Name__c ='Header'+String.valueOf(j));                                                             
            lst_head.add(header);           
            NE__Product__c prod = new NE__Product__c(Name = 'Prod');       
            lst_prod.add(prod);
            
        }
        
        insert lst_head;

        NE__Contract_Account_Association__c newCAA = new NE__Contract_Account_Association__c(
            NE__Contract_Header__c = lst_head[0].id,
            NE__Account__c = lst_acc[0].id
            
        );
        insert newCAA;

        insert lst_prod;
        
        for(Integer k=0; k<i; k++){
            NE__Contract__c contract = new NE__Contract__c(NE__Contract_Header__c = lst_head[k].Id,
                                                           NE__Status__c = 'Active');
            lst_con.add(contract);
        }
        insert lst_con;
        for(Integer l=0; l<i; l++){
            
            NE__Contract_Line_Item__c line = new NE__Contract_Line_Item__c(NE__Contract__c = lst_con[l].Id,
                                                                           NE__Commercial_Product__c = lst_prod[l].Id,
                                                                           NE__Allow_Qty_Override__c = false);
            
            lst_cli.add(line);
        }        
        insert lst_cli;

        Id profileId = [SELECT ID FROM PROFILE WHERE Name = 'BI_Standard_PER' limit 1][0].Id;
        BI_Dataload.MAX_LOOP = 4;
        List<User> lst_usr = BI_Dataload.loadUsers(4, profileId, 'Ejecutivo de Cliente');
        lst_usr[1].BI_Permisos__c = 'Super Usuario';
        lst_usr[2].BI_Permisos__c = 'Administrador de Contrato';

        System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
             update lst_usr;
        }
	}


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   
    IN:          
    OUT:               
    History:
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void verificarPermisos_adminContrac()
	{
        BI_TestUtils.throw_exception = false;
        List<Account> lst_acc = [SELECT ID FROM Account];
		User u = [SELECT ID FROM User WHERE BI_Permisos__c = 'Administrador de Contrato' AND isActive = true AND Profile.Name = 'BI_Standard_PER' limit 1];
        NE__Contract_Account_Association__c elContract = [SELECT Id, Search_Filter__c, NE__Contract_Header__c, NE__Account__c FROM NE__Contract_Account_Association__c limit 1];
        lst_acc[1].OwnerId = u.Id;
        update lst_acc[1];
        NE__Contract_Account_Association__c contract = new NE__Contract_Account_Association__c(NE__Contract_Header__c = elContract.NE__Contract_Header__c, NE__Account__c = lst_acc[1].Id);

		system.runAs(u){
            insert contract;
            contract.Search_Filter__c = 'all';
            update contract;
			try{
				delete contract;
				System.assert(false, 'El usuario Administrador de contrato no puede eliminar'); //El usuario Administrador de contrato no puede eliminar.
			}catch(Exception e){
				System.debug('verificarPermisos_adminContrac ' + e);
			}
		}
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   Comprobamos que el superUser pueda realizar todas las tareas requeridas.
    IN:          
    OUT:               
    History:
    
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void verificarPermisos_superUser()
	{
        BI_TestUtils.throw_exception = false;
		User u = [SELECT ID FROM User WHERE BI_Permisos__c = 'Super Usuario' AND isActive = true limit 1];
        List<Account> lst_acc = [SELECT ID FROM Account];
        lst_acc[1].OwnerId = u.Id;
        update lst_acc[1];
        NE__Contract_Account_Association__c elContract = [SELECT Id, Search_Filter__c, NE__Contract_Header__c, NE__Account__c FROM NE__Contract_Account_Association__c limit 1];
        NE__Contract_Account_Association__c contract = new NE__Contract_Account_Association__c(
            NE__Contract_Header__c = elContract.NE__Contract_Header__c, NE__Account__c = lst_acc[1].Id,
            Search_Filter__c = '');
		system.runAs(u){
            insert contract;
			contract.Search_Filter__c = 'all';
			update contract;
			delete contract;
		}
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   Probamos que no se pueda crear ni modificar ni borrar con un usuario
    IN:          
    OUT:               
    History:
    
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void verificarPermisos_all()
	{
        BI_TestUtils.throw_exception = false;
		User u = [SELECT ID FROM User WHERE BI_Permisos__c = 'Ejecutivo de Cliente' AND isActive = true AND Profile.Name = 'BI_Standard_PER' limit 1 ];
        List<Account> lst_acc = [SELECT ID FROM Account];
		NE__Contract_Account_Association__c elContract = [SELECT Id, Search_Filter__c, NE__Contract_Header__c, NE__Account__c FROM NE__Contract_Account_Association__c limit 1];

		system.runAs(u){
            NE__Contract_Account_Association__c contract = new NE__Contract_Account_Association__c(NE__Contract_Header__c = elContract.NE__Contract_Header__c, NE__Account__c = lst_acc[1].Id);
			try{
				 insert contract;
				 System.assert(false);//
			}catch(Exception e){
				System.debug('verificarPermisos_all ' + e);
			}
			elContract.Search_Filter__c = 'asdfasd';
			try{
				 update elContract;
				 System.assert(false);
			}catch(Exception e){
				System.debug('verificarPermisos_all ' + e);
			}
           
			try{
				delete elContract;
				System.assert(false);
			}catch(Exception e){
				System.debug('verificarPermisos_all ' + e);
			}
		}
	}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   
    IN:          
    OUT:               
    History:  
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    

    @isTest static void exceptions2(){
        BI_TestUtils.throw_exception = true;
        BI_NEContractHeaderMethods.verificarPermisos(null, null);
    }
}