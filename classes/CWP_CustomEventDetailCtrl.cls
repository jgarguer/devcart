/*-------------------------------------------------------------------------------------------------------------------------------------------------------
   
    Author:        Carlos Bastidas
    Company:       Everis.com
    Description:   Class that manages modal CWP_CustomEventDetailCtrl.
    
    History:
    
    <Date>            <Author>              <Description>
    17/03/2017        Carlos Bastidas       Initial version
    29/03/2017        Alberto Pina          GetLoadInfo Method. Date format.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


public  without sharing class CWP_CustomEventDetailCtrl {
    
    /*Method to return the initial info for modal CWP_CustomEventDetailCtrl*/
    @auraEnabled
    public static Event getLoadInfo (String eventId, Boolean pastDay,String day) {
        
        Event result = new Event();
        result = [Select StartDateTime, Subject, Location, EndDateTime, Description, Type, OwnerId, CreatedById, Id from Event where Id = :eventId];
        return result;
        //retorno.day = day;
        // INI - Everis - Evo lightning CWP - Calendar - 29/03/2017
        /*if(day.length()==10){
           retorno.fecha = day;
        }else{
           retorno.fecha = calculateDateEvent(day);     
        }*/
        // FIN - Everis - Evo lightning CWP - Calendar - 29/03/2017
        //retorno.eventId = eventId;
        
        /*if(eventId == null || String.isEmpty(eventId)) {
            
            retorno.initHour = '08:00';
            retorno.finishHour = '08:30';
            retorno.edit = true;
        } else {
            
            for(Event e: [Select StartDateTime,  EndDateTime From Event e where id= :eventId limit 1]){
                datetime startdate = e.StartDateTime;
                datetime finishdate = e.EndDateTime;
                retorno.initHour = formatHours(startdate.hour(), startdate.minute());
                retorno.finishHour = formatHours(finishdate.hour(), finishdate.minute());
            } 
            
            if(pastDay != null) {
                retorno.edit =   !pastDay;
            }
        }
        system.debug('RETORNO: '+retorno);
        return retorno;*/
    }
    
    /*Method to return the Event List*/
    @auraEnabled
    public static Event__c getEvent (String eventId) {
        Event__c ev = null;
        
        if(eventId == null || String.isEmpty(eventId)) {
            ev = new Event__c();
            
            ev.Type__c = Label.PCA_Meeting;
            ev.subject__c = Label.PCA_Meeting;
            
            Id accId = BI_AccountHelper.getCurrentAccountId();
            Account AccTeam = BI_AccountHelper.getCurrentAccount(accId);
            if (AccTeam != null){
                ev.OwnerId = AccTeam.OwnerId;
            }
            
        } else {
            for(Event e: [Select StartDateTime, Subject, Location, EndDateTime, Description, Type, OwnerId, CreatedById, Id, BI_ParentId__c From Event e where id= :eventId limit 1]){
                //ev = parseEventToEventc(e);
            } 
        }
        return ev;
    }
    
    /*Method to format hours*/
    public static String formatHours(Integer hour, Integer minutes) {
        String hourSt = '';
        if(hour < 10) {
            hourSt+='0';
        }
        hourSt+=hour+':';
        
        if(minutes < 10) {
            hourSt+='0';
        }
        hourSt+=minutes;
        
        return hourSt;
    }
    
     /*Method to return the User List*/
    @auraEnabled
    public static String[] getUsers(String eventId) {
        Set<Id> controlNoDupUsers = new Set<Id>();
        Id accId = BI_AccountHelper.getCurrentAccountId();
        List<SelectOption> selectedUser = new List<SelectOption>();
        List<User> selectedUserId = new List<User>();
        
        List<Id> guestIds = new List<Id>();
        if(!String.isEmpty(eventId)) {
            for(EventRelation item: [SELECT Id,RelationId FROM EventRelation where EventId =:eventId ]) {
                if(item.RelationId!=null){
                        guestIds.add(item.RelationId);
                }
            }
            
            for(Event item: [select OwnerId from Event where BI_ParentId__c=:eventId]) {
                guestIds.add(item.OwnerId);
            }
            
            for(Contact item:[select Id, Name from Contact where Id in:guestIds]) {
                selectedUser.add(new SelectOption(item.Id, item.Name));
            }
            
            for(User item: [select Id, Name from User where Id in :guestIds or ContactId in :guestIds or ProfileId in :guestIds]) {
                selectedUser.add(new SelectOption(item.Id, item.Name));
                selectedUserId.add(item);
            }
        }
        
        List<String> allUser = new List<String>();
        String useerID = UserInfo.getUserId();

        for( User usuarioPortal:[Select id, FullPhotoUrl, name,Usuario_SolarWinds__c, token_SolarWinds__c, CompanyName, Sector__c, ContactId, MobilePhone, Usuario_BO__c, pass_BO__c, Contact.Name , AccountId from User where id=:useerID limit 1]) {
            System.debug('usuarioPortal: '+usuarioPortal);
            for ( Hierarchy__c item: [Select User__c, NameUser__c From Hierarchy__c where Contact__c=:usuarioPortal.ContactId and User__c NOT IN :selectedUserId ORDER BY NameUser__c]) {
                if(!controlNoDupUsers.contains(item.User__c)){
                    allUser.add(item.User__c+','+item.NameUser__c);
                    controlNoDupUsers.add(item.User__c);
                }
                
                //allUser.add(new SelectOption(item.User__c, item.NameUser__c));
            }
            System.debug('allUser1: '+allUser);
            List<Id> userIdAccTeam = new List<Id>();
            for(AccountTeamMember accTeam: [SELECT UserId, User.Name, TeamMemberRole, Id, AccountAccessLevel FROM AccountTeamMember WHERE AccountId= :usuarioPortal.AccountId]) {
                
                userIdAccTeam.add(accTeam.UserId);
            }
            System.debug('userIdAccTeam: '+userIdAccTeam);
            for(User team:[SELECT Id, Name FROM User WHERE Id IN :userIdAccTeam and Id NOT IN :selectedUserId]) {
                if(!controlNoDupUsers.contains(team.Id)){
                    allUser.add(team.Id+','+team.Name);
                    controlNoDupUsers.add(team.Id);
                }
                //allUser.add(new SelectOption(team.Id, team.Name));
            }
            System.debug('allUser2: '+allUser);
        }
        
        if(accId != null) {
            for(Account acc:[SELECT Owner.Id,Owner.Name,Owner.Email,Owner.phone,Owner.MobilePhone,Owner.AboutMe,Owner.Profile.Name,Owner.BI_Permisos__c FROM Account where Id = :accId AND (Owner.Profile.Name != 'BI_Inteligencia Comercial' OR Owner.BI_Permisos__c !=: Label.BI_Inteligencia_Comercial)]) {
                if(!controlNoDupUsers.contains(acc.Owner.Id)){
                    allUser.add(acc.Owner.Id+','+acc.Owner.Name);
                    controlNoDupUsers.add(acc.Owner.Id);
                }
                //allUser.add(new SelectOption(acc.Owner.Id, acc.Owner.Name));
            }
            System.debug('allUser3: '+allUser);
        }
        List<Id> contactList = new List<Id>();
        for(BI_Contact_Customer_Portal__c item: [select Id, BI_User__c, BI_Contacto__c from BI_Contact_Customer_Portal__c where BI_User__c=:Userinfo.getUserId()]){
            contactList.add(item.BI_Contacto__c);
        }
        System.debug('contactList: '+contactList);
        for(Contact item:[select Id, Name from Contact where AccountId=:accId and BI_Activo__c=true and Id not in :contactList and Id NOT IN :guestIds]){
            if(!controlNoDupUsers.contains(item.Id)){
                allUser.add(item.Id+','+item.Name);
                controlNoDupUsers.add(item.Id);
            }
            //allUser.add(new SelectOption(item.Id, item.Name));
        }
        System.debug('allUser4: '+allUser);
        
        return allUser;
    }
    
    /*Method to return the Selected User List*/
    @auraEnabled
    public static String[] getSelectedUser(String eventId) {
        Id accId = BI_AccountHelper.getCurrentAccountId();
        List<User> guestUsers = new List<User>();
        List<String> selectedUser = new List<String>();
        
        List<Id> guestIds = new List<Id>();
        if(!String.isEmpty(eventId)) {
            for(EventRelation item: [SELECT Id,RelationId FROM EventRelation where EventId =:eventId]) {
                if(item.RelationId!=null){
                        guestIds.add(item.RelationId);
                }
            }
            
            for(Contact item:[select Id, Name from Contact where Id in:guestIds]) {
                selectedUser.add(item.Id+','+item.Name);
            }
            for(Event item: [select BI_ParentId__c,Id,OwnerId from Event where BI_ParentId__c=:eventId]) {
                guestIds.add(item.OwnerId);
            }
            
            guestUsers = [select Id, Name from User where Id in :guestIds];
            for(User item: guestUsers){
                selectedUser.add(item.Id+','+item.Name);
            }
        }
        return selectedUser;
    }
    
    
    /*Method to Map the Month*/
    public static String mapMonthNumber(String monthName) {
        Map<String, String> monthMap = new Map<String, String> {
            'JAN'=>'01','FEB'=>'02','MAR'=>'03','APR'=>'04',
                'MAY'=>'05','JUN'=>'06','JUL'=>'07','AUG'=>'08',
                'SEP'=>'09','OCT'=>'10','NOV'=>'11','DEC'=>'12'
                };                                               
                    return monthMap.get(monthName); 
    }
    
    /*Method to return the Hours List*/
    @auraEnabled
    public static String[] getHours() {
        
        List<String> options = new List<String>();
        for(Integer i=0; i<24; i++) {
            if(i<10) {
                options.add('0'+i+':00');
                options.add('0'+i+':30');
            } else {
                options.add(i+':00');
                options.add(i+':30');
            }
        }
        return options;
    }
  
    
    /*Method to Save Event*/
    @auraEnabled
    public static Boolean doSave (String eventS, String fecha,String selectedUser,String initHour,String finishHour,String webDate) {
        Event__c event = (Event__c) JSON.deserialize(eventS, Event__c.class);
        Id eventAux = event.Id;

        
        System.debug('fecha: '+fecha);
        System.debug('selectedUser: '+selectedUser);
        System.debug('initHour: '+initHour);
        System.debug('finishHour: '+finishHour);
        System.debug('webDate: '+webDate);
        System.debug('eventId: '+event.Id);
        boolean result = false;
        try {
            if(event.id != null && String.isNotEmpty(event.id)) {
                List<Event> eventToEdit = [Select StartDateTime, Subject, Location, EndDateTime, Description, Type, OwnerId, CreatedById, Id, BI_ParentId__c From Event e where id= :eventAux limit 1];                
       
                if(!eventToEdit.isEmpty()){
                    if(eventToEdit[0].BI_ParentId__c!=null) {
                        eventToEdit = [Select StartDateTime, Subject, Location, EndDateTime, Description, Type, OwnerId, CreatedById, Id, BI_ParentId__c From Event e where id= :eventToEdit[0].BI_ParentId__c limit 1];
                    }
                    event.Location__c = eventToEdit[0].Location;                                                     
                    event.OwnerId = eventToEdit[0].OwnerId;                             
                    event.StandardEventId__c = eventToEdit[0].Id;
     
                }
            } 
            
            string [] dates = fecha.split('-');
            string month = dates[1];
            month = String.valueOf(Integer.valueOf(month));
            string day = dates[2].substring(0,2);
            string year = dates[0];
            year = String.ValueOf(Integer.valueOf(year));
            
            String stringDate =  year + '-' + month + '-' + day + ' ' + initHour+':00';
            String stringDatefin = year + '-' + month + '-' + day + ' ' + finishHour+':00';
            
            event.EndDateTime__c = datetime.valueOf(stringDatefin);
            event.StartDateTime__c = datetime.valueOf(stringDate);
            
            event.GuestList__c = '';
            if(selectedUser != null && !String.isEmpty(selectedUser)) {
                System.debug('SELECTED: '+selectedUser);
                event.GuestList__c = selectedUser;
            } else {
                return false;
            }
            Id accId = BI_AccountHelper.getCurrentAccountId();
            for(Account acc:[select Id, OwnerId from Account where Id=:accId limit 1]){
                event.OwnerId = acc.OwnerId; 
            }
            event.Type__c = Label.PCA_Meeting;
            event.Id = null;
            system.debug('save: '+event);
            insert event;
            
            /*Create a Post*/
            system.debug('despues del insert');
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            ConnectApi.FeedElementCapabilitiesInput linkInput = new ConnectApi.FeedElementCapabilitiesInput();
            ConnectApi.LinkCapabilityInput urlInput = new ConnectApi.LinkCapabilityInput();
            System.debug('despues del insert 2');
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            
            mentionSegmentInput.id = Userinfo.getUserId();
            messageBodyInput.messageSegments.add(mentionSegmentInput);
            
            textSegmentInput.text = 'He solicitado una reunión';
            messageBodyInput.messageSegments.add(textSegmentInput);
            System.debug('despues del insert 3');
            /*ESTO ESTABA YA COMENTADO system.debug('headerdata : ' + ApexPages.currentPage().getHeaders().get('Host'));
            
            String headerdata= ApexPages.currentPage().getHeaders().get('Host');
            
            String urlvalue=Apexpages.currentPage().getUrl();
            system.debug('urlvalue : ' + urlvalue);
            String url='https://' + headerdata+ urlvalue;           
            system.debug('url link post : ' + url);*/
            
            String ntwrkId = Network.getNetworkId();
            if(ntwrkId != null){
                ConnectApi.Community comm = ConnectApi.Communities.getCommunity(ntwrkId);
                System.debug(comm.siteUrl);
                urlInput.url=comm.siteUrl + '/s/cwp-calendar';
            }
            
            System.debug('despues del insert 5');
            
            
            
            urlInput.urlName='Ver calendario';
            linkInput.link=urlInput;
            
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = Userinfo.getUserId();
            
            feedItemInput.capabilities=linkInput;
            if(ntwrkId != null){
            	ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(ntwrkId, feedItemInput);
                system.debug('feedElement ' + feedElement);     
            }
            
            //ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), Userinfo.getUserId(), ConnectApi.FeedElementType.FeedItem, 'He solicitado una reunión');
                  
            
            /*END Create a Post*/
            
            
            result = true;
        } catch (Exception e) {
            throw new AuraHandledException('Error attempting to save record');
        }
        return result;
    }
    /*Method to Delete Event*/
    @auraEnabled
    public static Boolean getDeleteEvent (String eventId) {
        Event ev = new Event();
        ev = [Select Id from Event where Id = :eventId limit 1];
        Database.delete(ev);
        
        return true;
    }
    
    @auraEnabled
    public static CustomEventDetailWrapper getEmpetyInfo(String day){
        CustomEventDetailWrapper result= new CustomEventDetailWrapper();
         result.initHour = '08:00';
         result.finishHour = '08:30';
         result.edit = false;
         result.fecha=day;
        return result;
    }
    
    /*Class Custom Event Detail Wrapper*/
    public class CustomEventDetailWrapper {
        @AuraEnabled
        public String initHour {get; set;}
        @AuraEnabled
        public String finishHour {get; set;}
        @AuraEnabled
        public Boolean edit {get; set;}
        @AuraEnabled
        public String fecha {get; set;}
        @AuraEnabled
        public  String day {get; set;}
        @AuraEnabled
        public  String eventId {get; set;}
    }
    
    
}