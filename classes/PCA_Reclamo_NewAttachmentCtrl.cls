public class PCA_Reclamo_NewAttachmentCtrl extends PCA_HomeController{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Ana Escrich
Company:       Salesforce.com
Description:   Class to attach files to cases

History:
   
<Date>            <Author>          	<Description>
10/07/2014        Ana Escrich      	 	Initial version
12/07/2016        José Luis Gonzalez    Fix UNICA modifications
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Id caseId 				{get;set;}
    
    public string caseName			{get;set;}
    
    public boolean haveError		{get;set;}
    
    public boolean noError			{get;set;}
    
    public Attachment myDoc	        {get;set;}
    
    //contAdjuntos y limiteAdjuntos metidas por Alex
    public integer contAdjuntos 	{get;set;}
    
    public boolean limiteAdjuntos 	{get;set;}
    
    public List<String> listaAdjuntos_Name {get;set;}

    //private List<Attachment> listaAdjuntos;
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Moruno
Company:       Salesforce.com
Description:   Constructor

History:

<Date>            <Author>          	<Description>
25/06/2014        Antonio Moruno       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
    public PCA_Reclamo_NewAttachmentCtrl(){}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Moruno
Company:       Salesforce.com
Description:   check permissions of current user

History:

<Date>            <Author>          	<Description>
28/07/2014        Antonio Moruno       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_Reclamo_NewAttachmentCtrl.checkPermissions', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Moruno
Company:       Salesforce.com
Description:   Load info of Reclamo Attachments

History:

<Date>            <Author>          	<Description>
28/07/2014        Antonio Moruno       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfo (){
        //try{
        haveError=false;
        noError=false;
        //_ui/core/userprofile/UserProfilePage?u={!usuarioId}&tab=sfdc.ProfilePlatformFeed
        if((Apexpages.currentPage().getParameters().get('Id')!=null)&&(Apexpages.currentPage().getParameters().get('Name')!=null)){
            caseName = String.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('Name'));
            caseId = String.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('Id'));            
        }else if(Apexpages.currentPage().getParameters().get('prev')!=null){
            List<Case> CasePortal = [Select Id, caseNumber, Origin FROM Case Where Origin='Customer Communities' AND CreatedById =: UserInfo.getUserId() Order By CreatedDate DESC LIMIT 1];
            if(!CasePortal.isEmpty()){  
                caseName = CasePortal[0].caseNumber;
                caseId = CasePortal[0].Id;
            } 
            system.debug('caseId' +caseId);
        }
        contAdjuntos = 0;        //metido por Alex
        limiteAdjuntos = false;  //metido por Alex
        //listaAdjuntos = new List<Attachment>();
        listaAdjuntos_Name = new List<String>();
        myDoc = new Attachment();
        caso = new Case(); 
        caso =  [SELECT RecordTypeId FROM case WHERE Id =: caseId ];
        RecordName = [SELECT Name FROM RecordType WHERE Id =: caso.RecordTypeId ].Name;
        //}catch (exception Exc){
        //   BI_LogHelper.generate_BILog('PCA_Reclamo_NewAttachmentCtrl.loadInfo', 'Portal Platino', Exc, 'Class');
        //}
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Moruno
Company:       Salesforce.com
Description:   Attach files in Reclamos

History:

<Date>            <Author>          	<Description>
25/06/2014        Antonio Moruno       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
    public void attachFile(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            system.debug('DENTRODEIF---FUNCION---->' + contAdjuntos  +' - ' + caseId );
            system.debug('DENTRODEIF---myDoc---->' + myDoc );
            //List<Case> cases = [select Id from Case where Id = :caseI Order by CreatedDate];
            if(myDoc.Name!=null && caseId!=null){
                if(Test.isRunningTest()){
            		contAdjuntos = integer.valueOf('1');   
                } 
                 system.debug('DENTRODEIF---0---->' + contAdjuntos +  caseId + myDoc.ParentId);
                if(contAdjuntos < 3){ //metido por Alex (sólo borrar el if en sí, no lo de dentro)
                    system.debug('DENTRODEIF ---->' + contAdjuntos +  caseId + myDoc.ParentId);
                    myDoc.ParentId = caseId;
                    system.debug('****myDoc: '+myDoc);
                    insert myDoc;
                    system.debug('****myDoc-2: '+myDoc);
                    //system.debug('****listaAdjuntos-2: '+listaAdjuntos);
                    system.debug('****listaAdjuntos_Name-2: '+listaAdjuntos_Name);
                    //listaAdjuntos.add(myDoc);
                    listaAdjuntos_Name.add(myDoc.name);
                    contAdjuntos = contAdjuntos + 1; //metido por Alex
                    haveError=false;
                    noError=true;
                    myDoc = new Attachment();
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'my error msg');
                    ApexPages.addMessage(myMsg);
                } //llave de cierre del if metido por Alex
                else limiteAdjuntos = true; //metido por alex
            }
            else{
                haveError=true;
                noError=false;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'El archivo elegido no es válido');
            }
            
        }catch (exception Exc){
            haveError = true;
            noError=false;
            BI_LogHelper.generate_BILog('PCA_Reclamo_NewAttachmentCtrl.attachFile', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
                Author:        
                Company:       Deloitte
                Description:   Web services solicitud incidencia
                
                History:
                
                <Date>            <Author>          	<Description>

--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
    public string RecordName {get;set;}
    public Case caso {get;set;}
    
    public PageReference onclickCerrar(){
        Case casoAdj = new Case();
        system.debug('onClickCerrar: '+RecordName);
        if(Test.isRunningTest()){
            		RecordName = 'Solicitud Incidencia Técnica';   
                } 
        if(RecordName == 'Solicitud Incidencia Técnica'){
            //system.debug('Lista de adjuntos--->' + listaAdjuntos );
            BIIN_Obtener_Token_BAO ot=new BIIN_Obtener_Token_BAO();
            BIIN_Creacion_Ticket_WS ct=new BIIN_Creacion_Ticket_WS();
            String authorizationToken = ot.obtencionToken();
            Integer nIntento=0;
            Boolean exito=false;
            try{
            	system.debug('Lanzamos la query');
                casoAdj = [SELECT  Id,  
                           AccountId, 
                           ContactId, 
                           TGS_Impact__c, 
                           TGS_Urgency__c, 
                           BI_Line_of_Business__c, 
                           BI_Product_Service__c, 
                           BIIN_Site__c,
                           BIIN_Id_Producto__c,
                           BI_Codigo_del_producto__c,
                           Description,
                           BI_COL_Codigo_CUN__c,
                           Subject,
                           BI2_PER_Descripcion_de_producto__c,
                           BIIN_Categorization_Tier_1__c,
                           BIIN_Categorization_Tier_2__c,
                           BIIN_Categorization_Tier_3__c FROM Case WHERE Id =: caseId];
                
                do{
            		system.debug('nIntento: '+nIntento+' Llamamos al WS de crearTicket con caso: '+casoAdj);
                    //MODIFICAR!_!_!_!_!_!!_!_!_!_!_
                    List<BIIN_Creacion_Ticket_WS.Adjunto> ladjuntos = new List<BIIN_Creacion_Ticket_WS.Adjunto>();
                    BIIN_Creacion_Ticket_WS.CamposOutsiders co= new BIIN_Creacion_Ticket_WS.CamposOutsiders();
                    exito=ct.crearTicket(authorizationToken, casoAdj, ladjuntos, co);
                    nIntento++;
                }while((nIntento<3)&&(exito==false));
                if(!exito){
                        system.debug('ERROR: Lanzamos la task');
                    //Lanzamos la tarea porque no tenemos respuesta del BAO
                } 
                //return new PageReference('javascript:parent.jQuery.fancybox.close(), window.parent.location.reload()');
            }catch(QueryException e){
                system.debug('Error en la query'+e);
            }
        }
        return new PageReference('javascript:parent.jQuery.fancybox.close(), window.parent.location.reload()');
    }
}