/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Borja María
Company:        Everis España
Description:    Test para la Clase BI_GD_CalloutManager

History:

<Date>                      <Author>                        <Change Description>
10/08/2017                 Borja María         		    	Versión Inicial.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
//(SeeAllData=true)

public class BI_GD_sincGestorDocumentalLightningTest {
    @isTest static void BI_GD_sincGestorDocumentalControllerTest1() {
        
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario =  BI_GD_sincGestorDocumentalLightningTest.crearRole('rol 0');
            insert rolUsuario;
            
            unUsuario = BI_GD_sincGestorDocumentalLightningTest.crearUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }        
        Contract contrato = new Contract(BI_FVI_Autoriza__c = true,
                  BI_FVI_Cobertura__c = false,
                  BI_FVI_Autoriza_representante__c = false,
                  BI_FVI_desconoce_CPF__c = false,
                  BI_FVI_Control_Parental__c = false,
                  BI_FVI_IdFileTxn__c = null,
                  BI_COL_Formato_Tipo__c = 'Tipo Cliente',
                  BI_COL_Tipo_contrato__c = 'Condiciones Uniformes',
                  BI_COL_Presupuesto_contrato__c  =12000,
                  BI_FVI_No_de_linea__c = '10',
                  BI_Indefinido__c = 'No',
                  StartDate = system.Today());
        
       Account account = new Account(Name = 'nameAccount',
                                      BI_Country__c = 'ARG',
                                      BI_Tipo_de_identificador_fiscal__c = 'Cliente exterior',BI_Segment__c = 'Empresas',
                                      BI_Subsegment_Regional__c = 'Corporate',
                                      BI_Territory__c = 'CABA',
                                      BI_Sector__c = 'Comercio',
                                      Sector__c = 'Private',
                                      BI_Subsector__c= 'Banca');
 
        insert(account);
        
        
        contrato.AccountId= account.Id;
        
        
        DateTime date1= System.now();
        Date date3= Date.today();
        Opportunity oppTest= new Opportunity (BI_Fecha_de_entrega_de_la_oferta__c = date1, 
                                              BI_Duracion_del_contrato_Meses__c= 10,
                                              OwnerId = userInfo.getUserId(),
                                              Name ='Opp1', 
                                              StageName='F2 - Contract Negotiation' , 
                                              BI_No_Identificador_fiscal__c='123', 
                                              AccountId=contrato.AccountId,
                                              BI_Opportunity_Type__c = 'Móvil',
                                              BI_Plazo_estimado_de_provision_dias__c = 22, 
                                              BI_Licitacion__c = 'No', 
                                              BI_Probabilidad_de_exito__c = '95',
               								  CurrencyIsoCode = 'MXN',
                                              BI_Requiere_contrato__c = true,
                                              BI_ARG_Tipo_de_Contratacion__c = 'Concurso Privado',
                                              BI_ARG_Nro_de_Licitacion__c = 'No',
                                              BI_Fecha_de_vigencia__c =  date3,
                                              Amount = 16,
                                              BI_ARG_Fecha_Sol_Elaboracion_Rta_Pliego__c = Date.today(),
                                              CloseDate = Date.today(),
                                              BI_ARG_Fecha_presentacion_de_sobres__c = Date.today(),
                                              BI_Country__c ='Argentina'); 

        
        insert oppTest;

        contrato.BI_Oportunidad__c = oppTest.Id;
        contrato.BI_ARG_Id_Legado__c = '';
        contrato.BI_ARG_Empresa_Licitacion__c = 'TASA';
        contrato.BI_ARG_Fecha_de_Adjudicacion__c = Date.today();
        contrato.BI_ARG_Empresa_Local__c = 'TASA';
        contrato.CustomerSignedDate = Date.today();
        contrato.BI_Divisas_del_contrato__c = 'ARS - Peso Argentino';
        contrato.Name = 'contrato';
        contrato.ContractTerm = 12;
        insert(contrato);

        test.startTest();
        System.RunAs(unUsuario){
      	BI_GD_sincGestorDocumentalLightning.sinc(contrato.id);
        test.stopTest();
         }
    }
    
    
       private static User crearUser (string name, id roleId, id profileId){
        
            User u =  new User( 
            Alias = name,
            Email= name +'@roletest1.com',
            UserRoleId = roleId, 
            LocalesIdKey='en_US', 
            EmailEncodingKey='UTF-8',
            Lastname='Testing', 
            LanguageLocaleKey='en_US', 
            ProfileId = profileId, 
            TimezonesIdKey='America/Los_Angeles',
            FirstName = name,
            UserName= name +'@roletest1.com', 
            BI_Permisos__c = 'TGS', 
            isActive = true,
            BI_CodigoUsuario__c= 'str'
        );
        
        return u;
    }
    
    private static UserRole crearRole(String name){
                UserRole r = new UserRole(Name = name);
        return r;
    }
}