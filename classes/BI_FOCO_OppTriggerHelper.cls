/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Bejoy Babu
    Company:       Salesforce.com
    Description:   Takes care of the update of FOCO table based on Opportunities

    History:
    
    <Date>            <Author>              <Description>
    09/26/2014        Bejoy Babu           Initial version
    10/20/2014		  Bejoy Babu 		   Added Custom Settings to toggle trigger ON/OFF
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public without sharing class BI_FOCO_OppTriggerHelper {

    	public static Boolean isInsRunning = false;
    	public static Boolean isUpdRunning = false;
    	public static Boolean isDelRunning = false;

    	static Id recTypeId;
    	static Map<String, List<Date>> dateMap;
    	static Map<String, BI_FOCO_Pais__c> mcs;
    	static Set<String> set_Paises;

	//Get all the static values
	static{
		//Get the valid record Type Id
		RecordType[] recTypeIds = [SELECT Id from RecordType where name = :System.Label.BI_Opp_RecType_Agrupacion];
		if(recTypeIds.size()>0){
			recTypeId = recTypeIds[0].Id;
		}

		//Get Date information for all countries
		dateMap = getAllCountriesDates();
		
		//Get all countries' information from custom settings
		mcs =  BI_FOCO_Pais__c.getAll();
		set_Paises = new Set<String>{'Peru'};
	}
	

	public BI_FOCO_OppTriggerHelper() {

	}


	public static String[] getIdArray(List<Opportunity> ol){
		List<String> idArray = new List<String>();
		if (ol!=null){
			for(Opportunity o: ol){
				if(set_Paises.contains(o.BI_Country__c))
					idArray.add(o.Id);
			}
		}
		return idArray;
	}

	private static Map<String, List<Date>> getAllCountriesDates(){

		dateMap = new Map<String, List<Date>>();

		map<string, BI_FOCO_Dates_by_Country__c> countryMap = BI_FOCO_Dates_by_Country__c.getAll();

		for (string s : countryMap.keySet()) {
			Date nd = countryMap.get(s).Min_Date__c;
			Date xd = countryMap.get(s).Max_Date__c;
			if(xd != null){
				xd = xd.addMonths(1).addDays(-1);
			}

			String country = s;
			if(xd != null && nd != null){
				dateMap.put(country, new List<Date>{nd, xd});
			}
		}

		/*
		AggregateResult[] ars = [SELECT  max(BI_Fecha_Inicio_Periodo__c) maxDate, min(BI_Fecha_Inicio_Periodo__c) minDate, BI_Cliente__r.BI_Country__c country from BI_Registro_Datos_FOCO__c group by BI_Cliente__r.BI_Country__c];

		for(AggregateResult ar: ars){
			Date nd = (Date)ar.get('minDate');
			Date xd = (Date)ar.get('maxDate');
			if(xd != null){
				xd = xd.addMonths(1).addDays(-1);
			}

			String country = String.valueOf( ar.get('country'));
			if(xd != null && nd != null){
				dateMap.put(country, new List<Date>{nd, xd});
			}
		}
		*/

		return dateMap;
	}


	@future
	public static void afterIns(String[] ids){

		
		//Get the On-Off Switch Status
		Map<String, BI_FOCO_Trigger__c> triggerMap;
		try{
			triggerMap = BI_FOCO_Trigger__c.getAll();
			if(triggerMap.get('Opportunity Insert').On_Off__c != true ){
				System.debug('Trigger Toggle Switch Value set to Off, quitting update Foco Process');
			}
			else if(BI_FOCO_OppTriggerHelper.isInsRunning){
				System.debug('Method already running, quitting to void data duplication');
			}
			else{
				BI_FOCO_OppTriggerHelper.isInsRunning = true;
				if(recTypeId ==null || dateMap == null || mcs == null){
					throw new BI_FOCO_Exception ( 'BI_FOCO_OppTriggerHelper.Unable to initialize static values: \'Agrupación\' recordtype | Date range for focos | Custom Setting for foco: BI_FOCO_Pais__c');
				}
				else{

					List<Opportunity> oppsList = [SELECT Id, AccountId, Account.Id, Account.BI_Country__c, CloseDate, Stagename, BI_Probabilidad_de_Exito_Numero__c, RecordTypeId, BI_Recurrente_bruto_mensual__c, BI_Recurrente_bruto_mensual_anterior__c, BI_Comienzo_estimado_de_facturacion__c, BI_Duracion_del_contrato_Meses__c, CurrencyIsoCode, Generate_Acuerdo_Marco__c from Opportunity where Id in:ids];
					

					List<Opportunity> oListUps = new List<Opportunity> ();
					for (Opportunity so : oppsList) {

						List<Date> dates = dateMap.get(so.Account.BI_Country__c);
						if(dates != null ){
							Date startDate = dates[0];
							Date endDate = dates[1];
							System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper.afterIns(): Criteria values:-------StartDate: '+startDate+' EndDate: '+endDate+'Probability: '+(Decimal)mcs.get(String.valueOf( so.Account.BI_Country__c)).Opportunity_Probability__c);
							System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper.afterIns(): Checking Opp record to update foco '+so);
							//if the new opp record satisfied the entry criteria, add to upsert list
							if((startDate < so.CloseDate && so.CloseDate < endDate) &&  
								((System.Label.BI_F5DefSolucion.equals(so.Stagename)) || (System.Label.BI_DesarrolloOferta.equals(so.Stagename)) || (System.Label.BI_F3OfertaPresentada.equals(so.Stagename)) || (System.Label.BI_F2Negociacion.equals(so.Stagename))) &&
								(recTypeId != null && !recTypeId.equals(so.RecordTypeId)) &&
								(so.BI_Probabilidad_de_Exito_Numero__c >= (Decimal)mcs.get(String.valueOf( so.Account.BI_Country__c)).Opportunity_Probability__c) &&
								(so.Generate_Acuerdo_Marco__c == false)
								){
								System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper.afterIns():Adding Opp record to update foco '+so);
								oListUps.add(so);
							} 
						}
						else
						{
							System.debug('BI_FOCO_OppTriggerHelper.afterIns():No existing records in FOCO table for '+so.Account.BI_Country__c +'. Hence quitting the Opportunity update process.');
						} 
					}

					BI_FOCO_OpptyUpdate foco = new BI_FOCO_OpptyUpdate();

					if(oListUps.size() > 0){

						System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper.afterIns(): A total of '+oListUps.size()+' Opp records to update foco for.');
						foco.updateFOCOs(oListUps);
					}
				}
				BI_FOCO_OppTriggerHelper.isInsRunning = false;
			}
		}
		catch(BI_FOCO_Exception be){
			notifyByEmail( be.getMessage(), be.getStackTraceString(), be.getLineNumber(), be.getTypeName());
		}
		catch(Exception e){
			notifyByEmail( e.getMessage(), e.getStackTraceString(), e.getLineNumber(), e.getTypeName());

		}
	}

	@future
public static void afterUpd(String[] ids){
	//Get the On-Off Switch Status
	Map<String, BI_FOCO_Trigger__c> triggerMap;
	try{
		triggerMap = BI_FOCO_Trigger__c.getAll();
		
		if(BI_FOCO_OppTriggerHelper.isUpdRunning){
			System.debug('Method already running, quitting to void data duplication');
		}
		else if(triggerMap.get('Opportunity Update').On_Off__c != true ){
			System.debug('Trigger Toggle Switch Value set to Off, quitting update Foco Process');
		}
		else{
			BI_FOCO_OppTriggerHelper.isUpdRunning = true;
			if(recTypeId ==null || dateMap == null || mcs == null){
				throw new BI_FOCO_Exception( 'Unable to initialize static values: \'Agrupación\' recordtype | Date range for focos | Custom Setting for foco: BI_FOCO_Pais__c');
			}
			else{

	 //PLL_PickList_Refactorizacion_Necesaria
				List<Opportunity> oppsList = [SELECT Id, AccountId, Account.Id, Account.BI_Country__c, CloseDate, Stagename, BI_Probabilidad_de_Exito_Numero__c, RecordTypeId, BI_Recurrente_bruto_mensual__c, BI_Recurrente_bruto_mensual_anterior__c, BI_Comienzo_estimado_de_facturacion__c, BI_Duracion_del_contrato_Meses__c, CurrencyIsoCode, Generate_Acuerdo_Marco__c from Opportunity where Id in:ids];
				

				List<Opportunity> oListUps = new List<Opportunity> ();
				List<Opportunity> oListDel = new List<Opportunity> ();
				for (Opportunity so : oppsList) {
					List<Date> dates = dateMap.get(so.Account.BI_Country__c);
					if(dates != null ){
						Date startDate = dates[0];
						Date endDate = dates[1];
						System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper.afterUpd(): Criteria values:-------StartDate: '+startDate+' EndDate: '+endDate+'Probability: '+(Decimal)mcs.get(String.valueOf( so.Account.BI_Country__c)).Opportunity_Probability__c);
						System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper.afterUpd(): Checking Opp record to update foco '+so);
						//if the new opp rec satisfies the entry criteria, add to the upsert List
						//no matter what the old record looked like
						if((startDate < so.CloseDate && so.CloseDate < endDate) &&  
							((System.Label.BI_F5DefSolucion.equals(so.Stagename)) || (System.Label.BI_DesarrolloOferta.equals(so.Stagename)) || (System.Label.BI_F3OfertaPresentada.equals(so.Stagename)) || (System.Label.BI_F2Negociacion.equals(so.Stagename))) &&
							(recTypeId != null && !recTypeId.equals(so.RecordTypeId)) &&
							(so.BI_Probabilidad_de_Exito_Numero__c >= (Decimal)mcs.get(String.valueOf( so.Account.BI_Country__c)).Opportunity_Probability__c) &&
							(so.Generate_Acuerdo_Marco__c == false)
							){

							System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper.afterUpd(): Adding Opp record to update foco '+so);
							oListUps.add(so);
						}
						else{
							oListDel.add(so);
						}
					}
					else
					{
						System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper.afterUpd():No FOCO records to insert for '+so.Account.BI_Country__c);
					} 
				
				}
					
				BI_FOCO_OpptyUpdate foco = new BI_FOCO_OpptyUpdate();

				if(oListUps.size() > 0){
					System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper.afterUpd():A total of '+oListUps.size()+' Opp records to update foco for.');
					foco.updateFOCOs(oListUps);
				}
				if(oListDel.size() > 0){
					String[] idsDel = new String[oListDel.size()];
					Integer i = 0;
					for(Opportunity o: oListDel){
						idsDel[i++] = o.id;
					}
					System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper.afterUpd():A total of '+idsDel.size()+' Opp records to delete foco for.');
					foco.deleteFOCOs(ids);
				}
			}
			BI_FOCO_OppTriggerHelper.isUpdRunning = false;
		}
	}
	catch(BI_FOCO_Exception be){

		notifyByEmail( be.getMessage(), be.getStackTraceString(), be.getLineNumber(), be.getTypeName());
	}
	catch(Exception e){
		notifyByEmail( e.getMessage(), e.getStackTraceString(), e.getLineNumber(), e.getTypeName());
	}
}


@future
public static void afterDel(String[] ids){
		//Get the On-Off Switch Status
		Map<String, BI_FOCO_Trigger__c> triggerMap;
		try{
			triggerMap = BI_FOCO_Trigger__c.getAll();
			if(triggerMap.get('Opportunity Delete').On_Off__c != true ){
				System.debug('Trigger Toggle Switch Value set to Off, quitting update Foco Process');
			}
			else if(BI_FOCO_OppTriggerHelper.isDelRunning){
				System.debug('Method already running, quitting to void data duplication');
			}
			else{
				isDelRunning = true;
				BI_FOCO_OpptyUpdate foco = new BI_FOCO_OpptyUpdate();
				if(ids!=null && ids.size()>0){
					System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper: .afterDel(): A total of '+ids.size()+' Opp records to delete from foco.');
					foco.deleteFOCOs(ids);
				}
				BI_FOCO_OppTriggerHelper.isDelRunning = false;
			}
		}
		catch(BI_FOCO_Exception be){

			notifyByEmail( be.getMessage(), be.getStackTraceString(), be.getLineNumber(), be.getTypeName());
		}
		catch(Exception e){
			notifyByEmail( e.getMessage(), e.getStackTraceString(), e.getLineNumber(), e.getTypeName());
		}		
	}

	public static void notifyByEmail(String eMsg, String eStrc, Integer eLn, String eTy) {
		System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper: Notification process: START');
		String orgId = UserInfo.getOrganizationId();
		String orgName = UserInfo.getOrganizationName();
		String user = UserInfo.getUserName();
		String message = eMsg;
		String stacktrace = eStrc;
		String exType = '' + eTy;
		String line = '' + eLn;
		String theTime = '' + System.now();

		String subject = 'FOCO update based on Opportunity Trigger failed';
		String body = String.format('Time: {0}\nMessage: {1}\nStacktrace: {2}\nLine: {3}', new List<String>{ theTime, message, stacktrace, line });
		System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper: Notification process:Message Body:'+body);
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		BI_FOCO_Pais__c cs = mcs.get('General');

		if(cs!= null){
			String toList = cs.Notify_Email_List__c;
			String[] toAddresses = toList.split(',', -1) ;
			mail.setToAddresses(toAddresses);
			mail.setSubject(subject);
			mail.setUseSignature(false);
			mail.setPlainTextBody(body);

			//Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); //Check límits and target to userId 
			BI_SendMails_Helper.sendMails(new Messaging.SingleEmailMessage[] { mail }, false, 'FOCO');
		}
		System.debug('---------------------------------------------------BI_FOCO_OppTriggerHelper: Notification process: END');
	}
}