/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Jesús Párraga Bórnez
    Company:       New Energy Aborda
    Description:   Methods executed by BI_O4 Opportunity Triggers
    Test Class:    BI_O4_OpportunityTeamMemberMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    21/09/2016              Pedro Párraga            Initial version
    22/11/2016				Alvaro García			 Add methods exceptions
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class BI_O4_OpportunityTeamMemberMethods_TEST {
	
	static{
        BI_TestUtils.throw_exception = false;
    }
    
	@isTest static void updateQuotesWithOpptyTeamMembersDelete() {

		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		List<OpportunityTeamMember> oldOTM = new  List<OpportunityTeamMember>();


	    Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                      					  CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                      					  BI_Ciclo_ventas__c = Label.BI_Completo  
                      					  );

	    insert opp;

	    OpportunityTeamMember oppTeam = new OpportunityTeamMember(UserId = UserInfo.getUserId(), OpportunityId = opp.Id, TeamMemberRole = 'Regional Presales');
	    oldOTM.add(oppTeam);

	    Quote quote = new Quote(Name = 'Quote_01', Status = 'Working on proposal', OpportunityId = opp.Id);
	    insert quote;

	    Test.startTest();
	    insert oldOTM;
	    delete oldOTM;
	    //BI_O4_OpportunityTeamMemberMethods.updateQuotesWithOpptyTeamMembersDelete(oldOTM);
	    Test.stopTest();
	}
	
	@isTest static void updateQuotesWithOpptyTeamMembers() {

		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		List<OpportunityTeamMember> newOTM = new  List<OpportunityTeamMember>();
		Map<Id, OpportunityTeamMember> oldOTM = null;

	    Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                      					  CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                      					  BI_Ciclo_ventas__c = Label.BI_Completo  
                      					  );		

	    insert opp;

	    Quote quote = new Quote(Name = 'Quote_01', Status = 'Working on proposal', OpportunityId = opp.Id);
	    insert quote;

	    opp.SyncedQuoteId = quote.Id;
	    update opp;

	    OpportunityTeamMember oppTeam = new OpportunityTeamMember(UserId = UserInfo.getUserId(), OpportunityId = opp.Id, TeamMemberRole = 'GSE Principal');
	    
	    newOTM.add(oppTeam);
	    

	    Test.startTest();
	    insert newOTM;
	    update newOTM;
	    //BI_O4_OpportunityTeamMemberMethods.updateQuotesWithOpptyTeamMembers(newOTM ,oldOTM);

	    Test.stopTest();

	}

	@isTest static void updateQuotesWithOpptyTeamMembersDos() {

		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		List<OpportunityTeamMember> newOTM = new  List<OpportunityTeamMember>();
		Map<Id, OpportunityTeamMember> oldOTM = null;

	    Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                      					  CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                      					  BI_Ciclo_ventas__c = Label.BI_Completo  
                      					  );		

	    insert opp;



        OpportunityTeamMember oppTeam2 = new OpportunityTeamMember(UserId = UserInfo.getUserId(), OpportunityId = opp.Id, TeamMemberRole = 'Regional Presales');
		newOTM.add(oppTeam2);

	    Test.startTest();
	    insert newOTM;
	    //BI_O4_OpportunityTeamMemberMethods.updateQuotesWithOpptyTeamMembers(newOTM ,oldOTM);

	    Test.stopTest();

	}
	
		@isTest static void exceptions() {

		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
		List<OpportunityTeamMember> newOTM = new  List<OpportunityTeamMember>();
		Map<Id, OpportunityTeamMember> oldOTM = null;

	    Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                      					  CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                      					  BI_Ciclo_ventas__c = Label.BI_Completo  
                      					  );		

	    insert opp;

        OpportunityTeamMember oppTeam2 = new OpportunityTeamMember(UserId = UserInfo.getUserId(), OpportunityId = opp.Id, TeamMemberRole = 'Regional Presales');
		newOTM.add(oppTeam2);

	    Test.startTest();
	    BI_TestUtils.throw_exception = true;
	    insert newOTM;
	    update newOTM;
	    delete newOTM;
	    //BI_O4_OpportunityTeamMemberMethods.updateQuotesWithOpptyTeamMembers(newOTM ,oldOTM);

	    Test.stopTest();

	}
}