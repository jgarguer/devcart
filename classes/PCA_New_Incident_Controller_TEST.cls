@isTest
private class PCA_New_Incident_Controller_TEST {

    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test class to manage the coverage code for PCA_New_Incident_Controller class 

    <Date>                  <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Incident_Controller.init
            
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getloadInfoCaseId() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            Id incident = [SELECT Id FROM RecordType WHERE Name = 'Incident' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = incident , Subject = 'Test Subject');
            insert caseTest;
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
            System.assertEquals('Test Subject', controller.mcase.Subject);
            
            Test.stopTest();
        }
        
    }
    static testMethod void getloadInfosId() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.dummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            NE__Catalog_Item__c[] catalogItems = TGS_Portal_Utils.getCatalogItemsBySId(testProduct.Id);
            if (catalogItems.size()>0){
                System.debug('Hay catalogItems');
                NE__Catalog_Item__c catalogItem = catalogItems[0];
                Test.startTest();
                ApexPages.currentPage().getParameters().put('sId', testProduct.Id);
                PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
                System.assertEquals(catalogItem.NE__ProductId__r.Name, controller.mCase.TGS_Product_Tier_3__c);
                Test.stopTest();
            }
        }
    }
    static testMethod void getloadInfosuId() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.dummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
            System.assertEquals(testOrderItem.Id, controller.mCase.TGS_Customer_Services__c);
            Test.stopTest();
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Incident_Controller.crearCase
    
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void crearCaseTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.dummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
            controller.mCase.Subject = 'New case';
            controller.endSite = 'endSite';
            controller.level1 = 'level 1';
            controller.level2 = 'level 2';
            controller.level3 = 'level 3';
            controller.level4 = 'level 4';
            controller.level5 = 'level 5';
            PageReference pg = controller.crearCase();
            List<Case> caseList = [SELECT Id, CaseNumber FROM Case WHERE Subject = 'New case'];
            if(caseList.size()>0){
                System.assertEquals(1, caseList.size());
            }
            else{
                System.assertEquals('/empresasplatino/PCA_Cases?error=true', pg.getUrl());
            }
            controller.loadListYesNo();
            controller.loadListCPE();
            Test.stopTest();
        }
    }
    static testMethod void crearCaseTest2() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.dummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
            controller.mCase.Subject = 'New case';
            PageReference pg = controller.crearCase();
            List<Case> caseList = [SELECT Id, CaseNumber FROM Case WHERE Subject = 'New case'];
            if(caseList.size()>0){
                System.assertEquals(1, caseList.size());
            }
            else{
                System.assertEquals('/empresasplatino/PCA_Cases?error=true', pg.getUrl());
            }
            Test.stopTest();
        }
    }
    
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Incident_Controller.cancelar
    
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void cancelarTest() {
      
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest(); //JEG 01/02/2016
            Id incident = [SELECT Id FROM RecordType WHERE Name = 'Incident' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = incident , Subject = 'Test controller', Status = 'Assigned');
            insert caseTest;
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
            PageReference pg = controller.cancelar();
            System.assertEquals('/empresasplatino/PCA_Cases', pg.getUrl());
            Test.stopTest(); //JEG 01/02/2016
        }
    }
   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Incident_Controller.initializeCaseFields
    
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*static testMethod void initializeCaseFieldsTest() {
        //User userTest = [SELECT Id, Name FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Community') AND isActive = true LIMIT 1];
        User userTest = [SELECT Id, Name FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS Customer Community Plus') AND isActive = true LIMIT 1];
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            Test.startTest();
            NE__Catalog_Item__c[] catalogItems = TGS_Portal_Utils.getCatalogItemsBySId(testProduct.Id);
            if (catalogItems.size()>0){
                System.debug('Hay catalogItems');
                NE__Catalog_Item__c catalogItem = catalogItems[0];
                ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
                PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
                controller.initializeCaseFields();
                System.assertEquals(testProduct.Name, controller.mCase.TGS_Product_Tier_3__c);
            }
            Test.stopTest();
        }
    }*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Incident_Controller.initializeProductTiers
    
     <Date>                 <Author>                <Inquiry Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void initializeProductTiersTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.dummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            NE__Catalog_Item__c[] catalogItems = TGS_Portal_Utils.getCatalogItemsBySId(testProduct.Id);
            Test.startTest();
            if (catalogItems.size()>0){
                NE__Catalog_Item__c catalogItem = catalogItems[0];
                ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
                PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
                controller.mCase.TGS_Customer_Services__c = testOrderItem.Id;
                controller.initializeProductTiers();
                System.assertEquals(testProduct.Name, controller.mCase.TGS_Product_Tier_3__c);
            }
            Test.stopTest();
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Incident_Controller.initializeProductTiers(suId)
    
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void initializeProductTierssuIdTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.dummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            NE__Catalog_Item__c[] catalogItems = TGS_Portal_Utils.getCatalogItemsBySId(testProduct.Id);
            Test.startTest();
            if (catalogItems.size()>0){
                NE__Catalog_Item__c catalogItem = catalogItems[0];
                PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
                controller.initializeProductTiers(testOrderItem.Id);
                System.assertEquals(testProduct.Name, controller.mCase.TGS_Product_Tier_3__c);
            }
            Test.stopTest();
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Incident_Controller.loadLevel1/loadLevel2/loadLevel3/loadLevel4/loadLevel5/loadSites
    
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void loadLocationTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
            List<Account> levelList = TGS_Portal_Utils.getLevel1(userTest.Id,0);
            System.assertEquals(levelList.size()+1, controller.listLevel1.size());
            if(controller.level1 == '' || controller.level1 == null){ controller.level1 = levelList[0].Id;}
            controller.loadLevel2();
            levelList = TGS_Portal_Utils.getLevel2(controller.level1,1);
            System.assertEquals(levelList.size()+1, controller.listLevel2.size());
            if(controller.level2 == '' || controller.level2 == null){ controller.level2 = levelList[0].Id;}
            controller.loadLevel3();
            levelList = TGS_Portal_Utils.getLevel3(controller.level2,1);  
            System.assertEquals(levelList.size()+1, controller.listLevel3.size());
            if(controller.level3 == '' || controller.level3 == null){ controller.level3 = levelList[0].Id;}
            controller.loadLevel4();
            levelList = TGS_Portal_Utils.getLevel4(controller.level3,1);  
            System.assertEquals(levelList.size()+1, controller.listLevel4.size());
            if(controller.level4 == '' || controller.level4 == null){ controller.level4 = levelList[0].Id;}
            controller.loadLevel5();
            levelList = TGS_Portal_Utils.getLevel5(controller.level4,1); 
            System.assertEquals(levelList.size()+1, controller.listLevel5.size());
            controller.loadSites();
            List<BI_Punto_de_instalacion__c> sites = TGS_Portal_Utils.getSites(controller.level4);
            System.assertEquals(sites.size()+1, controller.siteList.size());
            Test.stopTest();
        }
    }
    
 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Incident_Controller.loadLookupServices
            
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void loadLookupServicesTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
            controller.loadLookupServices();
            List<Account> accounts = TGS_Portal_Utils.getAccountsBelowHierarchy(userTest.Id);        
            List<NE__OrderItem__c> services = TGS_Portal_Utils.getlookupServiceUnits(accounts);
            System.assertEquals(services.size(), controller.lookupServices.size());
            Test.stopTest();
        }
    }
  
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Incident_Controller.crearWorkinfo
            
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void crearWorkinfoTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id incident = [SELECT Id FROM RecordType WHERE Name = 'Incident' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = incident , Subject = 'Test controller');
            insert caseTest;
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
            controller.attachmentName1 = 'Unit Test Attachment';
            controller.attachmentDesc1 = 'Unit Test Description Attachment';
            controller.myAttachedBody = Blob.valueOf('Unit Test Attachment Body');
            controller.attachmentName2 = 'Unit Test Attachment';
            controller.attachmentDesc2 = 'Unit Test Description Attachment';
            controller.myAttachedBody2 = Blob.valueOf('Unit Test Attachment Body');
            controller.attachmentName3 = 'Unit Test Attachment';
            controller.attachmentDesc3 = 'Unit Test Description Attachment';
            controller.myAttachedBody3 = Blob.valueOf('Unit Test Attachment Body');
            controller.crearWorkinfo(caseTest.Id);
            List<TGS_Work_Info__c> wi = [SELECT Id, TGS_Description__c FROM TGS_Work_Info__c WHERE TGS_Case__c = :caseTest.Id];
            Test.startTest();
            System.assertEquals(1, wi.size());
            Test.stopTest();
        }
    }
   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Incident_Controller.uploadAttachment
    
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void uploadAttachmentTest() {

        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest(); //JEG 01/02/2016
            Id incident = [SELECT Id FROM RecordType WHERE Name = 'Incident' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = incident , Subject = 'Test controller');
            insert caseTest;
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
            controller.newAttachment.Name = 'Unit Test Attachment';
            controller.myAttachedBody = Blob.valueOf('Unit Test Attachment Body');
            controller.uploadAttachment();
            System.assertEquals(1, controller.listCaseAttachments.size());
            System.assertEquals('Unit Test Attachment', controller.listCaseAttachments[0].Name);
            Test.stopTest(); //JEG 01/02/2016
        }

    }
}