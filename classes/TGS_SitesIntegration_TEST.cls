@isTest
public class TGS_SitesIntegration_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Cirac
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_SitesIntegration.createSites and TGS_SitesIntegration.updateSites
            
     <Date>                 <Author>                <Change Description>
     28/05/2015             Ana Cirac               Initial Version
     03/04/2016             Jose Miguel Fierro      Fixed  non-unique TEMPEXT_ID__c
     20-Nov-2017            Álvaro López            Dynamic integration for Ferrovial sites
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createUpdateSites() {
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
        }      
        
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        //userTest.TEMPEXT_ID__c = 'acirac';
        userTest.TEMPEXT_ID__c = 'TEMPEXTID-' + String.valueOf((Integer)(Math.random()*1000)); // JMF 03/04/2016
        insert userTest;
        System.runAs(userTest){
            Test.startTest();
            Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
            Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
            TGS_Ferrovial_Sites_Integration__c fsi = new TGS_Ferrovial_Sites_Integration__c();
            fsi.Name = 'Custom setting test';
            fsi.TGS_AccountID_18__c = 'Id Test';
            fsi.TGS_Account_Name__c = 'Test';
            insert fsi;
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceSiteImpl());
            BI_Punto_de_instalacion__c site = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id);
            
            BI_Punto_de_instalacion__c s = [SELECT iD from BI_Punto_de_instalacion__c Where id = :site.Id];
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceSiteImpl());
            s.TEMPEXT_ID__c = 'test';
            update s;           
            Test.stopTest();
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Cirac
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_SitesIntegration.createSites and TGS_SitesIntegration.updateSites
            
     <Date>                 <Author>                <Change Description>
     28/05/2015             Ana Cirac               Initial Version
     03/04/2016             Jose Miguel Fierro      Fixed  non-unique TEMPEXT_ID__c
     20-Nov-2017            Álvaro López            Dynamic integration for Ferrovial sites
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void updateSites() {
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
        }      
        
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        //userTest.TEMPEXT_ID__c = 'acirac';
        userTest.TEMPEXT_ID__c = 'TEMPEXTID-' + String.valueOf((Integer)(Math.random()*1000)); // JMF 03/04/2016
        insert userTest;
        System.runAs(userTest){
           
            Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
            Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
            TGS_Ferrovial_Sites_Integration__c fsi = new TGS_Ferrovial_Sites_Integration__c();
            fsi.Name = 'Custom setting test';
            fsi.TGS_AccountID_18__c = 'Id Test';
            fsi.TGS_Account_Name__c = 'Test';
            insert fsi;
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceSiteImpl());
            BI_Punto_de_instalacion__c site = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id);
           
            BI_Punto_de_instalacion__c s = [SELECT iD from BI_Punto_de_instalacion__c Where id = :site.Id];
            s.TEMPEXT_ID__c = '';
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceSiteImpl());    
            Test.startTest();
            update s;
            Test.stopTest();
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Cirac
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_SitesIntegration.deleteSites
            
     <Date>                 <Author>                <Change Description>
     28/05/2015             Ana Cirac               Initial Version
     03/04/2016             Jose Miguel Fierro      Fixed  non-unique TEMPEXT_ID__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void deleteSites() {
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
        }      
        
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        //userTest.TEMPEXT_ID__c = 'acirac';
        userTest.TEMPEXT_ID__c = 'TEMPEXTID-' + String.valueOf((Integer)(Math.random()*1000)); // JMF 03/04/2016
        insert userTest;
        System.runAs(userTest){
            Test.startTest();
            Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
            Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceSiteImpl());
            BI_Punto_de_instalacion__c site = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id);
            
            BI_Punto_de_instalacion__c s = [SELECT iD from BI_Punto_de_instalacion__c Where id = :site.Id];
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceSiteImpl());
            delete s;
            Test.stopTest();
        }
    }  
    
    static testMethod void saveErrors() {
        
        TGS_SitesIntegration.saveTGSerror('', 'Operation', 'message');
    }

}