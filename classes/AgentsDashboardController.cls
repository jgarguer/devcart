public with sharing class AgentsDashboardController {
    
    public List<TableRecord> getAgents() {
        //List<Group> queuesList = [SELECT Name FROM Group WHERE Type = 'Queue' AND Id IN (SELECT GroupId FROM GroupMember WHERE UserOrGroupId = :UserInfo.getUserId())];
        List<TableRecord> resultList = new List<TableRecord>();
        
        
        List<Group> queuesList = getQueues();
        
        if (queuesList.size() > 0) {
            List<User> usersList = [SELECT Name, (SELECT RecordType.Name FROM Cases__r) FROM User WHERE Id IN (SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :queuesList[0].Id)];
  
            for (User u : usersList) {
                  TableRecord tr = new TableRecord();
                  tr.name = u.name;
                  tr.totalTickets = u.cases__r.size();
                  tr.totalBillingInquiries = 0;
                  tr.totalComplaints = 0;
                  tr.totalOrders = 0;
                  for (Case c : u.cases__r) {
                      if (c.recordType.name == 'Billing Inquiry') {
                          tr.totalBillingInquiries++;
                      } else if (c.recordType.name == 'Complaint') {
                          tr.totalComplaints++;
                      } else if (c.recordType.name == 'Order Management Case') {
                          tr.totalOrders++;
                      }
                  }
                  resultList.add(tr);
            }
        }
        
        return resultList;
    }
    
    public List<Group> getQueues() {
        //TO BE CHANGED
        List<Group> queuesList = [SELECT Name FROM Group WHERE Type = 'Queue' AND Name LIKE 'SMC L1 Ordering'];
        return queuesList;
    }
    
    class TableRecord {
        public String name {get; set;}
        public Integer totalTickets {get; set;}
        public Integer totalBillingInquiries {get; set;}
        public Integer totalComplaints {get; set;}
        public Integer totalOrders {get; set;}
    }
}