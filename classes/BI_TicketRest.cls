@RestResource(urlMapping='/ticketing/v1/tickets/*')
global class BI_TicketRest {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Ticket Rest WebServices.
    
    History:
    
    <Date>            <Author>                                                  <Description>
    20/08/2014        Pablo Oliva                                               Initial version
    28/04/2016        José Luis González                                        Adapt resource to UNICA interface
    04/07/2016        José Luis González                                        Set Case Owner before insert
    27/07/2016        Julio Laplaza                                             Remove case owner when updating (commenting)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains detailed information stored in the server for a specific ticket.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.TicketInfoType structure
                   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    20/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @HttpGet
    global static BI_RestWrapper.TicketInfoType getTicket() { 
        
        BI_RestWrapper.TicketInfoType res;
        
        try{
            
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
                
            if(RestContext.request.headers.get('countryISO') == null){
                    
                RestContext.response.statuscode = 400;//BAD_REQUEST
                RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
            
            }else{
                
                //ONE TICKET
                res = BI_RestHelper.getOneTicket(RestContext.request.requestURI.split('/')[4], RestContext.request.headers.get('countryISO'), 
                                                 RestContext.request.headers.get('systemName'));
                
                RestContext.response.statuscode = (res == null)?404:200;//404 NOT_FOUND, 200 OK
                
            }
            
        }catch(Exception exc){
            
            RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
            RestContext.response.headers.put('errorMessage',exc.getMessage());
            BI_LogHelper.generate_BILog('BI_TicketRest.getOneTicket', 'BI_EN', exc, 'Web Service');
            
        }
        
        return res;
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Modifies an existing ticket in the server receiving the request.
    
    IN:            RestContext.request & BIIN_UNICA_Pojos.TicketRequestType ticketRequest
    OUT:           RestContext.response & BIIN_UNICA_Pojos.TicketDetailType
    
    History:
    
    <Date>            <Author>                  <Description>
    20/08/2014        Pablo Oliva               Initial version.
    28/04/2016        José Luis González        Adapt resource to UNICA interface
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @HttpPut
  global static void doPut(BIIN_UNICA_Pojos.TicketRequestType ticketRequest)
  {
        system.debug(' BI_TicketMultipleRest.doPut | ticketRequest entrada: ' + JSON.serialize(ticketRequest));
        
        RestContext.response.addHeader('Content-Type', 'application/json');

        try 
        {
            BIIN_UNICA_RestHelper.checkTicketRequestMandatoryFields(ticketRequest);

            Map<String, String> trAdditionalData = BIIN_UNICA_Utils.additionalDataToMap(ticketRequest.additionalData);

            Case ticket = BIIN_UNICA_TicketManager.getTicketByCaseNumber(trAdditionalData.get('Case Number'));
            if(ticket == null)
            {
                throw new BIIN_UNICA_BadRequestException('Ticket not found: Case Number: ' + trAdditionalData.get('Case Number') + ', CorrelatorId: ' + ticketRequest.correlatorId);
            }

            if(ticket.BI_Id_del_caso_Legado__c != null && ticket.BI_Id_del_caso_Legado__c != '' && ticket.BI_Id_del_caso_Legado__c != ticketRequest.correlatorId)
            {
                throw new BIIN_UNICA_BadRequestException('Ticket not found: Case Number: ' + trAdditionalData.get('Case Number') + ', CorrelatorId: ' + ticketRequest.correlatorId);
            }
            
            ticket = BIIN_UNICA_RestHelper.generateTicket(ticket, ticketRequest);
            
            List<Case> clist = new List<Case>();
            clist.add(ticket);

            //BI_CaseMethods.setCaseOwner(clist);
            //BI2_CaseMethods.updateOwner(clist, null);
                    
            update ticket;

            RestContext.response.statuscode = 200; // OK
                    
            if(RestContext.request.headers.get('UNICA-ServiceId') != null) 
            {
                RestContext.response.addHeader('UNICA-ServiceId', RestContext.request.headers.get('UNICA-ServiceId'));
            }

            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(BIIN_UNICA_RestHelper.generateTicketDetail(ticket)).replaceAll('NEW_STATUS', 'NEW'));
        }
        catch(BIIN_UNICA_BadRequestException ex)
        {
            RestContext.response.statuscode = 400; // BAD REQUEST
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new BIIN_UNICA_Pojos.ExceptionType('SVC1000', ex.getMessage())));
        }
        catch(Exception ex)
        {
            RestContext.response.statuscode = 500; // INTERNAL SERVER ERROR
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new BIIN_UNICA_Pojos.ExceptionType('SVC0001', 'Generic Client Error: ' + ex.getMessage())));
        }
  }
}