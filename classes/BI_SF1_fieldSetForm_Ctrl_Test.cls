@isTest
private class BI_SF1_fieldSetForm_Ctrl_Test {
	
	@isTest static void getMapFields_Test() {
		String fieldSet = 'BI_SF1_Conjunto_de_campos_SF1';
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Argentina'});
		Contact cnt = new Contact(LastName='Prueba',
								AccountId=lst_acc[0].Id,
								Email='prueba@prueba.prueba',
								CurrencyIsoCode='EUR');
		insert cnt;
		Id recordId = cnt.Id;
		SObject record = cnt;
		List<Map<String, String>> lst_return = BI_SF1_fieldSetForm_Ctrl.getMapFields(fieldSet, recordId, record);
		System.assert(lst_return!=null);
	}
	
	@isTest static void getRecordtypeId_test() {
		String objName = 'Contact';
		Map<String, String> map_devName_Id = BI_SF1_fieldSetForm_Ctrl.getRecordtypeId(objName);
		System.assert(!map_devName_Id.isEmpty());
	}
	
}