@isTest public with sharing class BI2_ClosePastCases_Job_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Test class for BI2_ClosePastCases_Job

	 History:

	 <Date>				<Author>				<Description>
	 10/09/2015			Jose Miguel Fierro		Initial version
     10/02/2017         Pedro Párraga           Error Resolved
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    @isTest static void test_execute() {
        BI_TestUtils.throw_exception = false;
        Profile standardProfile = [select id from Profile where Name='BI_Standard_PER'];
        User asesor = new User(
            Username = 'abortest@email.com',
            LastName = 'TestUser',
            CommunityNickname = 'nick' + String.valueOf(Math.random()),
            Alias = 'testuser', //max length 8
            Email = 'abortest@email.com',
            ProfileId = standardProfile.Id,
            TimeZoneSidKey = 'America/New_York',
            EmailEncodingKey='UTF-8',
            BI_Permisos__c = 'Asesor',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            Pais__c = Label.BI_Peru);
        System.runAs(new User(Id=UserInfo.getUserId())) {
            insert asesor;
        }
        
        System.debug(LoggingLevel.ERROR, '{PER: Permisos: ' + asesor.BI_Permisos__c);
        
        System.assertEquals(0, [Select Id FROM BI_Log__c WHERE CreatedDate = TODAY].size());
        
        //System.runAs(asesor) {
            RecordType rtCasoPadre = [SELECT Id FROM RecordType WHERE DeveloperName='BI2_Caso_Padre'];

            Account acc = new Account(Name='TestAccount', BI2_asesor__c=asesor.Id, OwnerId=asesor.Id,BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
            insert acc;

            Case c = new Case(Status='Assigned', RecordTypeId=rtCasoPadre.Id, Subject='TestSubject', AccountId = acc.Id);
            insert c;
        
            System.debug(LoggingLevel.ERROR, '{PER: RT: ' + c.RecordTypeId);

            c.Status = 'In Progress';
            update c;

            c.Status = 'Resolved';
            update c;

            c.BI_Fecha_ltimo_cambio_de_estado__c = Datetime.now().addDays(-(Integer.valueOf(Label.BI2_Antiguedad_Caso_Cierre_Automatico))).date();
            update c;
        //}

        //System.debug('CurrentUser (in test):' + UserInfo.getUserId());
        //System.debug('CurrentUser (in query):' + [SELECT Id FROM USer WHERE Name = 'Administrador B2B'].Id);
        
        // El Job se ejecutará con nuestro usuario, apesar del runas. Nos tenemos que dar permiso de asesor.
        User me = [SELECT Id, BI_Permisos__c FROM User WHERE Id = :UserInfo.getUserId()];
        me.BI_Permisos__c = 'Asesor';
        update me;
        
        delete [select id from BI_Log__c];
        
        Test.startTest();
        //System.runAs(asesor) {
            //System.debug('CurrentUser (in runas):' + UserInfo.getUserId());
            System.schedule('Test->BI2_ClosePastCases_Job', '0 0 0 15 3 ? 2022', new BI2_ClosePastCases_Job());
        //}
        Test.stopTest();
        
        List<BI_Log__c> lst_logs = [SELECT Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_NumeroLinea__c, BI_Descripcion__c, BI_StackTrace__c, BI_Tipo_Error__c, BI_Tipo_de_componente__c FROM BI_Log__c];
        system.assertEquals(0, lst_logs.size());
        System.debug(lst_logs);

        c = [SELECT Id, Status FROM Case WHERE Id = :c.Id];
        
        system.assertEquals('Closed', c.Status);
    }

    @isTest static void test_execute_fail_openChild() {
        BI_TestUtils.throw_exception = false;
        
        // El Job se ejecutará con nuestro usuario, apesar del runas. Nos tenemos que dar permiso de asesor.
        User me = [SELECT Id, BI_Permisos__c FROM User WHERE Id = :UserInfo.getUserId()];
        me.BI_Permisos__c = 'Asesor';
        update me;

        System.debug(LoggingLevel.ERROR, '{PER: Permisos: ' + me.BI_Permisos__c);
        
        System.assertEquals(0, [Select Id FROM BI_Log__c WHERE CreatedDate = TODAY].size());
        
        List<RecordType> lst_rtypes = [SELECT Id FROM RecordType WHERE DeveloperName IN ('BI2_Caso_Padre')]; // Caso comercial puede ser tanto padre como hijo

        
        List<Account> acc = BI_DataLoad.loadAccounts(1, new List<String>{Label.BI_Peru});
        acc[0].BI2_asesor__c = me.Id;
        update acc;

        List<Case> lst_cas = BI_DataLoad.loadCases(acc, null, lst_rtypes);//new Case(Status='Assigned', RecordTypeId=rtCasoPadre.Id, Subject='TestSubject', AccountId = acc[0].Id);
        System.debug('Errorrorororro : ' + lst_cas);
        System.debug(LoggingLevel.ERROR, '{PER: RT: ' + lst_cas[0].RecordTypeId);

        lst_cas[0].Status = 'In Progress';
        update lst_cas;

        lst_cas[0].Status = 'Resolved';
        update lst_cas;

        lst_cas[0].BI_Fecha_ltimo_cambio_de_estado__c = Datetime.now().addDays(-(Integer.valueOf(Label.BI2_Antiguedad_Caso_Cierre_Automatico))).date();
        update lst_cas;     
        
        Test.startTest();
            System.schedule('Test->BI2_ClosePastCases_Job', '0 0 0 15 3 ? 2022', new BI2_ClosePastCases_Job());
        Test.stopTest();
        
        List<BI_Log__c> lst_logs = [SELECT Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_NumeroLinea__c, BI_Descripcion__c, BI_StackTrace__c, BI_Tipo_Error__c, BI_Tipo_de_componente__c FROM BI_Log__c];
        /*System.assertEquals(0, lst_logs.size());*/
        System.debug(lst_logs);

        lst_cas = [SELECT Id, Status, ParentId FROM Case WHERE ID IN (:lst_cas.get(0).Id, :lst_cas.get(1).Id)];
        
        System.assertNotEquals('Resolved', lst_cas[0].Status);
    }

    @isTest static void test_execute_fail_unfulfilledSLA() {
        BI_TestUtils.throw_exception = false;
        
        // El Job se ejecutará con nuestro usuario, apesar del runas. Nos tenemos que dar permiso de asesor.
        User me = [SELECT Id, BI_Permisos__c FROM User WHERE Id = :UserInfo.getUserId()];
        me.BI_Permisos__c = 'Asesor';
        update me;

        System.debug(LoggingLevel.ERROR, '{PER: Permisos: ' + me.BI_Permisos__c);
        
        System.assertEquals(0, [Select Id FROM BI_Log__c WHERE CreatedDate = TODAY].size());
        
        List<RecordType> lst_rtypes = [SELECT Id FROM RecordType WHERE DeveloperName IN ('BI2_Caso_Padre')]; // Caso comercial puede ser tanto padre como hijo

        
        List<Account> acc = BI_DataLoad.loadAccounts(1, new List<String>{Label.BI_Peru});
        acc[0].BI2_asesor__c = me.Id;
        update acc;

        List<Case> lst_cas = BI_DataLoad.loadCases(acc, null, lst_rtypes);//new Case(Status='Assigned', RecordTypeId=rtCasoPadre.Id, Subject='TestSubject', AccountId = acc[0].Id);
        System.debug(LoggingLevel.ERROR, '{PER: RT: ' + lst_cas[0].RecordTypeId);

        lst_cas[0].Status = 'In Progress';
        update lst_cas;

        lst_cas[0].Status = 'Resolved';
        update lst_cas;

        lst_cas[0].BI_Fecha_ltimo_cambio_de_estado__c = Datetime.now().addDays(-(Integer.valueOf(Label.BI2_Antiguedad_Caso_Cierre_Automatico))).date();
        lst_cas[0].TGS_SLA_Light__c = 'Red';    // TODO: Better SLA Test than setting this flag?
        update lst_cas;
        
        
        Test.startTest();
            System.schedule('Test->BI2_ClosePastCases_Job', '0 0 0 15 3 ? 2022', new BI2_ClosePastCases_Job());
        Test.stopTest();
        
        List<BI_Log__c> lst_logs = [SELECT Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_NumeroLinea__c, BI_Descripcion__c, BI_StackTrace__c, BI_Tipo_Error__c, BI_Tipo_de_componente__c FROM BI_Log__c];
        /*System.assertEquals(0, lst_logs.size());*/
        System.debug(lst_logs);

        lst_cas = [SELECT Id, Status, ParentId FROM Case WHERE ID IN (:lst_cas.get(0).Id, :lst_cas.get(1).Id)];
        
        System.assertNotEquals('Closed', lst_cas[0].Status);
    }

    @isTest static void test_cleanupUpdateErrors() {
        BI_TestUtils.throw_exception = false;
        List<Case> lst = new List<Case>();
        lst.add(new Case());

        insert lst;

        lst.add(new Case());
        BI2_ClosePastCases_Job.cleanupUpdateErrors(Database.insert(lst, false)); // insert case that was already inserted
        
        system.assertEquals(false, [select Id from BI_Log__c limit 1].isEmpty());
    }
    
    @isTest static void test_exceptions() {
        BI_TestUtils.throw_exception = true;
        
        new BI2_ClosePastCases_Job().execute(null);
        BI2_ClosePastCases_Job.cleanupUpdateErrors(null);
        
        system.assertEquals(false, [select Id from BI_Log__c limit 1].isEmpty());
    }
}