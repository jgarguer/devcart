public with sharing class PCA_CatalogoController extends PCA_HomeController{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Class to invoke catalog 
    
    History:
    
    <Date>            <Author>              <Description>
    26/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String url {get;set;}
    
    public List<Market_Place_PP__c> marketPlaceFamilies     {get;set;}
    
    public List<BI_Catalogo_PP__c> products                 {get;set;}
    
    public PCA_CatalogoController(){}
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest()){
            throw new BI_Exception('test');
            }
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_CatalogoController.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info of catalogo
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfo (){
        
        try{
            if(BI_TestUtils.isRunningTest()){
            throw new BI_Exception('test');
            }
            Id accId = BI_AccountHelper.getCurrentAccountId();
            system.debug('acccId: ' +accId);
            if(getAccountCountry(accId)!=null){
                marketPlaceFamilies = [SELECT BI_Descripcion__c,BI_Link_icono__c, BI_Link_imagen_slider__c, BI_Link_imagen__c,BI_Country__c,BI_Posicion__c,BI_Subtitulo__c,BI_Texto__c,BI_Titulo__c,Id,Name FROM Market_Place_PP__c where BI_Country__c = :getAccountCountry(accId) order By BI_Posicion__c limit 4];
                List<Id> familyIds = new List<Id>();
                for(Market_Place_PP__c item : marketPlaceFamilies){
                    familyIds.add(item.Id);
                }
                products =[SELECT BI_Familia_del_producto__c, Id, Name, BI_Description__c FROM BI_Catalogo_PP__c where BI_Familia_del_producto__c in :familyIds];
            }
            
            String currencyCode = getCurrencyISOCode(accId);
            url = 'apex/NE__NewConfiguration?accId='+accId+'&servAccId='+accId+'&billAccId='+accId+'&createOpty=true&cType=New&cSType=Rápido&confCurrency='+currencyCode;
            system.debug('url: '+url);
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_CatalogoController.PCA_CatalogoController', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   This method retrieves the ISOCode of the current account
    
    History:
    
    <Date>            <Author>              <Description>
    22/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public string getCurrencyISOCode(Id accId){
        try{
            if(BI_TestUtils.isRunningTest()){
            throw new BI_Exception('test');
            }
            if(accId!=null){
                List<Account> accounts = [select Id, CurrencyIsoCode from Account where Id = :accId];
                if(!accounts.isEmpty()){
                    return accounts[0].CurrencyIsoCode;
                }
                }
            return null;
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_CatalogoController.getCurrencyISOCode', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   This method retrieves the Country of the current account
    
    History:
    
    <Date>            <Author>              <Description>
    22/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public string getAccountCountry(Id accId){
        try{
            if(BI_TestUtils.isRunningTest()){
            throw new BI_Exception('test');
            }
            if(accId!=null){
                List<Account> accounts = [select Id, CurrencyIsoCode, BI_Country__c from Account where Id = :accId];
                system.debug('accountsCatalogo: '+accounts);
                if(!accounts.isEmpty()){
                    return accounts[0].BI_Country__c;
                }
                }
            return null;
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_CatalogoController.getCurrencyISOCode', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }

}