@RestResource(urlMapping='/accountresources/v1/contacts')
global class BI_ContactMultipleRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Contact Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    06/08/2014        Pablo Oliva       Initial version
    10/11/2015	      Fernando Arteaga	BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains information stored in the server for contacts.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.ContactsListType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    06/08/2014        Pablo Oliva       Initial version.
    01/12/2015	      Fernando Arteaga	BI_EN - CSB Integration
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	//global static BI_RestWrapper.ContactsListType getMultipleContacts() {
	global static BI_RestWrapper.ContactsInfoListType getMultipleContacts() {
		
		//BI_RestWrapper.ContactsListType res;
		BI_RestWrapper.ContactsInfoListType res;
		
		try{
			
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
		
			if(RestContext.request.params.get('accountId') != null || RestContext.request.params.get('customerId') != null || 
			   RestContext.request.params.get('status') != null)
			{
				
				//MULTIPLE CONTACTS
				String accId;
					
				Boolean continue_process = true;
						
				if(RestContext.request.params.get('accountId') != null){
					
					accId = RestContext.request.params.get('accountId');
					
					if(RestContext.request.headers.get('countryISO') == null)
						continue_process = false;
					
				}else if(RestContext.request.params.get('customerId') != null){
					
					accId = RestContext.request.params.get('customerId');
					
					if(RestContext.request.headers.get('countryISO') == null)
						continue_process = false;
					
				}
				
				if(continue_process){
					
					res = BI_RestHelper.getMultipleContacts(accId, RestContext.request.params.get('status'),
															RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
					
					RestContext.response.statuscode = (res == null)?404:200;//404 NOT_FOUND; 200 OK
						
				}else{
					
					RestContext.response.statuscode = 400;//BAD_REQUEST
					RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
					
				}

			}else{
				
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingRequiredParameters);
				
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_ContactMultipleRest.getMultipleContacts', 'BI_EN', Exc, 'Web Service');
			
		}
		
		return res;
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Creates a new contact in the server receiving the request.
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    06/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPost
	global static void createContact() {
		
		try{
			
			if(RestContext.request.headers.get('countryISO') == null){
								
					RestContext.response.statuscode = 400;//BAD_REQUEST
					RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);			
								
			}else{
		
				RestRequest req = RestContext.request;
			
				Blob blob_json = req.requestBody; 
			
				String string_json = blob_json.toString(); 
				
				BI_RestWrapper.ContactDetailsType conDetType = (BI_RestWrapper.ContactDetailsType) JSON.deserialize(string_json, BI_RestWrapper.ContactDetailsType.class);
			
				String contactExtId;
			
				if(conDetType.additionalData != null){
					
					for(BI_RestWrapper.KeyvalueType keyValue:conDetType.additionalData){
						
						if(keyValue.Key == 'contactId')
							contactExtId = keyValue.value;
						
					}
					
					if(contactExtId == null){
						
						RestContext.response.statuscode = 400;//BAD_REQUEST
						RestContext.response.headers.put('errorMessage', Label.BI_MissingRequiredFields+' contactId');
						
					}else{
						
						Contact con = new Contact();
				
						con = BI_RestHelper.generateContact(con, conDetType, RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'), contactExtId);
						
						if(con == null){
							
							RestContext.response.statuscode = 404;//NOT_FOUND
							RestContext.response.headers.put('errorMessage', Label.BI_AccountOrCustomerNotFound);
							
						}else{
							
							insert con;
						
							RestContext.response.statuscode = 201;
							RestContext.response.headers.put('Location','https://'+System.URL.getSalesforceBaseURL().getHost()+'/services/apexrest/accountresources/v1/contacts/'+con.BI_Id_del_contacto__c);
							
						}
						
					}
					
				}else{
					
					RestContext.response.statuscode = 400;//BAD_REQUEST
					RestContext.response.headers.put('errorMessage', Label.BI_MissingRequiredFields+' contactId');
					
				}
				
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_ContactMultipleRest.createContact', 'BI_EN', Exc, 'Web Service');
			
		}
		
	}

}