/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:					Eduardo Ventura
Company:				Everis España
Description:			Sincronización de contratos y licitaciones para gestor documental.

History:

<Date>								<Author>								<Change Description>
15/03/2017							Eduardo Ventura							Versión inicial.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class BI_GD_CalloutManager {
    
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:					Eduardo Ventura
Company:				Everis España
Description:			Orquestación de sincronización de contratos y licitaciones para gestor documental.

History:

<Date>								<Author>								<Change Description>
15/03/2017							Eduardo Ventura							Versión inicial.
17/10/2017							Paloma Lastra							Ever01 - Se cambia la fecha de ultima sincronización a Datetime
19/10/2017							Daniel Leal								Ever02 - Captura de error
19/10/2017							Paloma Lastra 							Ever03 - Retirada restricción en actualización de licitación cuando ya existe un  contrato
13/11/2017							Alejandro García Olmedo 				Ever05 - Captura de la excepción provocada por la inexistencia de una oportunidad asociada a un contrato.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    webservice static String sincronizar(String contractId){
        /* Variable Configuration */
        String returnValue = '';
        
        /* Rest Configuration */
        HttpRequest httpRequest;
        HttpResponse httpResponse;
        String endpoint = '';
        
        /* Retrieve sObject Data*/
        Contract contract = BI_GD_Helper.getContractFromId(contractId);
        
       
        
        
            Account account = BI_GD_Helper.getAccountFromId(contract.AccountId);
        try{
            Opportunity opportunity = BI_GD_Helper.getOpportunityFromId(contract.BI_Oportunidad__c);
        //ever02-INI
        	try{
        //ever02-FIN
            /* Licitation */
            List<String> emptyFieldsLicitation = BI_GD_conversorContract.emptyFieldsLicitation(contract);
            
            boolean esLicitacion=false; 
            
            if(opportunity.BI_Licitacion__c =='Si')
                esLicitacion=!esLicitacion;
            if(esLicitacion){
                
                //Ever03 - INI 
                //if( String.isEmpty(contract.BI_ARG_Id_Legado__c) && BI_GD_Helper.isValidStageForLicitation(opportunity) && emptyFieldsLicitation.isEmpty()){
                //if(BI_GD_Helper.isValidStageForLicitation(opportunity) && emptyFieldsLicitation.isEmpty() && contract.BI_GD_Estado_sincronizacion_licitacion__c!='Sincronizado'){
                //Ever03 - FIN   
                //EVER04 - INI 
                if(BI_GD_Helper.isValidStageForLicitation(opportunity) && emptyFieldsLicitation.isEmpty() && contract.BI_GD_Estado_sincronizacion_licitacion__c!='Sincronizado'&& !BI_GD_Helper.isMulticurrencyContract(contract)){    
                    //EVER04 - FIN
                    /* Apex Instance Generation */
                    returnValue = '• Información de licitación\n\n';
                    BI_RestWrapper.ContractRequestType contractRequestType_Licitation = BI_GD_conversorContract.createLicitation(contract, account, opportunity);
                    
                    if(contract.BI_GD_ID_Gestor_Documental_licitacion__c != null && contract.BI_GD_ID_Gestor_Documental_licitacion__c != '') 
                        httpResponse = BI_GD_createContract.createContract(contractRequestType_Licitation, 'PUT');
                    else 
                        httpResponse = BI_GD_createContract.createContract(contractRequestType_Licitation, 'POST');
                    
                    /* Response Evaluation */
                    if (httpResponse.getStatusCode() != 201 &&  httpResponse.getStatusCode() != 200){
                        /* Sincronización Erronea */
                        contract.BI_GD_Estado_sincronizacion_licitacion__c = 'Error';
                        returnValue += 'Error en la Sincronización. Código de ejecución: ' + httpResponse.getStatusCode() + '. ' + HttpResponse.getBody();
                    } else {
                        /* Sincronización correcta */
                        if(httpresponse.getHeader('Location') != null) contract.BI_GD_ID_Gestor_Documental_licitacion__c = httpresponse.getHeader('Location');//.substringAfterLast('/contracts/');
                        contract.BI_GD_Estado_sincronizacion_licitacion__c = 'Sincronizado';
                        
                        // Ever01 - INI
                        //contract.BI_GD_Ultima_sincronizacion_licitacion__c = Date.today();
                        contract.BI_GD_Ultima_sincronizacion_licitacion__c = Datetime.now();
                        // Ever01 - FIN
                        
                        returnValue += 'Sincronización realizada correctamente';
                    }
                }
                
                //Ever03 - INI
                //else if(!String.isEmpty(contract.BI_ARG_Id_Legado__c))
                //  returnValue += 'Existe un contrato sincronizado.';
                else if(contract.BI_GD_Estado_sincronizacion_licitacion__c=='Sincronizado')
                    returnValue += '• Información de licitación\n\nLa licitación ya se encuentra sincronizada';
                //Ever03 - FIN
                
                else if(!BI_GD_Helper.isValidStageForLicitation(opportunity)) 
                    returnValue += '• Información de licitación\n\n'+'La oportunidad no se encuentra en la etapa adecuada para la sincronización.';
                //EVER04 - INI
                else if(BI_GD_Helper.isMulticurrencyContract(contract)) 
                    returnValue += 'No es posible sincronizar una licitación con más de una divisa.';
                //EVER04 - FIN
                
                else if(!emptyFieldsLicitation.isEmpty() && emptyFieldsLicitation.size() < 10){
                    returnValue += '• Información de licitación\n\nSe ha de informar los siguientes campos para sincronizar la licitación: \n';
                    for(String item : emptyFieldsLicitation) 
                        returnValue += item;
                }
                
                /* Control */
                returnValue += '\n\n';
            }
            
            // Everi03 - INI  
            //if(!esLicitacion || (esLicitacion && contract.BI_GD_ID_Gestor_Documental_licitacion__c != null && contract.BI_GD_ID_Gestor_Documental_licitacion__c != '')) {
            //if((!esLicitacion && contract.BI_GD_Estado_sincronizacion_contrato__c !='Sincronizado') || (esLicitacion && contract.BI_GD_ID_Gestor_Documental_licitacion__c != null && contract.BI_GD_ID_Gestor_Documental_licitacion__c != '' && contract.BI_GD_Estado_sincronizacion_contrato__c !='Sincronizado') {
            // Everi03 - FIN
            //EVER04 - INI
            if((!esLicitacion && contract.BI_GD_Estado_sincronizacion_contrato__c !='Sincronizado' && !BI_GD_Helper.isMulticurrencyContract(contract)) || (esLicitacion && contract.BI_GD_ID_Gestor_Documental_licitacion__c != null && contract.BI_GD_ID_Gestor_Documental_licitacion__c != '' && contract.BI_GD_Estado_sincronizacion_contrato__c !='Sincronizado' && !BI_GD_Helper.isMulticurrencyContract(contract))) {
                //EVER04 - FIN  
                /* Contract */
                
                List<String> emptyFieldsContract = BI_GD_conversorContract.emptyFieldsContract(contract);
                returnValue += '• Información de contrato\n\n';
                System.debug ('@@Etapavalida' +BI_GD_Helper.isValidStageForContract(opportunity)+ ' @@Camposobligatoriosnoinformados:' + emptyFieldsContract );
                if(BI_GD_Helper.isValidStageForContract(opportunity) && emptyFieldsContract.isEmpty()){
                    
                    /* Apex Instance Generation */
                    BI_RestWrapper.ContractRequestType contractRequestType_Contract = BI_GD_conversorContract.createContract(contract, account, opportunity);
                    
                    if(contract.BI_ARG_Id_Legado__c != null && contract.BI_ARG_Id_Legado__c != '') {
                        system.debug('CALLING with PUT');
                        httpResponse = BI_GD_createContract.createContract(contractRequestType_Contract, 'PUT');
                    }
                    else {
                        system.debug('CALLING with POST');
                        httpResponse = BI_GD_createContract.createContract(contractRequestType_Contract, 'POST');
                    }
                    /* Response Evaluation */
                    if (httpResponse.getStatusCode() != 201 &&  httpResponse.getStatusCode() != 200){
                        /* Sincronización Erronea */
                        
                        
                        contract.BI_GD_Estado_sincronizacion_contrato__c = 'Error';
                        returnValue += 'Error en la Sincronización. Código de ejecución: ' + httpResponse.getStatusCode() + '. ' + HttpResponse.getBody();
                        //ever03-Ini
                        try{
                            throw new CalloutException (httpResponse.getStatusCode() + '. ' + HttpResponse.getBody());
                            
                        }catch(CalloutException e){
                            
                            BI_FVI_Contrato_Zytrust_CALL.generate_BILog_ContracFVI('BI_GD_CalloutManager.sincronizar', 'BI_EN', e, 'Web Service',contract.Id);
                            
                            
                            //ever03-Fin
                        }    
                        
                    }else{
                        /* Sincronización Correcta */
                        if(httpresponse.getHeader('Location') != null) contract.BI_ARG_Id_Legado__c = httpresponse.getHeader('Location');//.substringAfterLast('/contracts/');
                        contract.BI_GD_Estado_sincronizacion_contrato__c = 'Sincronizado';
                        
                        // Ever01 - INI
                        //contract.BI_GD_Ultima_sincronizacion_contrato__c = Date.today();
                        contract.BI_GD_Ultima_sincronizacion_contrato__c = Datetime.now();
                        // Ever01 - FIN
                        
                        returnValue += 'Sincronización realizada correctamente';
                    }
                }
                else if(!BI_GD_Helper.isValidStageForContract(opportunity)) 
                    returnValue += 'La oportunidad no se encuentra en la etapa adecuada para la sincronización.';
                
                else if(!emptyFieldsContract.isEmpty() && emptyFieldsContract.size() < 7){
                    returnValue += '• Información de contrato\n\nSe ha de informar los siguientes campos para sincronizar la contrato:\n';
                    for(String item : emptyFieldsContract) 
                        returnValue += item;
                }
            }
            
            
            
            //EVER03 - INI
            else if(contract.BI_GD_Estado_sincronizacion_contrato__c =='Sincronizado'){
                returnValue += '• Información de contrato\n\nEl contrato ya se encuentra sincronizado';
            }
            
            //EVER03 - FIN
            
            //EVER04 - INI
            else if(BI_GD_Helper.isMulticurrencyContract(contract)) 
                returnValue += 'No es posible sincronizar un contrato con más de una divisa.';
            //EVER04 - FIN
            
            System.debug('\n returnValue \n:'+returnValue+'\n\n');
            /* Update Sync Data & return errors */
            update contract;
            
            //ever02-Ini
        }catch(Exception except){
            
            
            //BI_LogHelper.generate_BILog('BI_GD_CalloutManager.sincronizar', 'BI_EN', except, 'Web Service');
            BI_FVI_Contrato_Zytrust_CALL.generate_BILog_ContracFVI('BI_GD_CalloutManager.sincronizar', 'BI_EN', except, 'Web Service',contract.Id);
            
            returnValue=except.getTypeName()+' - '+except.getMessage();
            //ever02-Fin
        } 
      //ever05-Ini
        }catch(Exception except){
           
            if(except.getTypeName()=='System.ListException'){
            
                // se genera el log
                 BI_FVI_Contrato_Zytrust_CALL.generate_BILog_ContracFVI('BI_GD_CalloutManager.sincronizar', 'BI_EN', except, 'Web Service',contract.Id);
                // se muestra el mensaje que se mostrará por pantalla
                 returnValue='No es posible sincronizar contratos sin una oportunidad asociada';
            }else{
                // se genera el log
              	BI_FVI_Contrato_Zytrust_CALL.generate_BILog_ContracFVI('BI_GD_CalloutManager.sincronizar', 'BI_EN', except, 'Web Service',contract.Id);
            	 // se muestra el mensaje que se mostrará por pantalla
            	returnValue=except.getTypeName()+' - '+except.getMessage();        
            }  
           
        } 
      //ever05-Fin  
        return returnValue;
    }
}