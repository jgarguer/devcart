@isTest 
private class PCA_TemplateController_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_TemplateController class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    30/09/2014              Micah Burgos            Initial Version
    25/08/2016              Jose Miguel Fierro      Added data to test buildFooter()
    13/10/2016				Sara Nuñez				Added data to test Google Analytics Id
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void PCA_TemplateController_TEST() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        System.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());

            // START JMF 25/08/2016
            if(accList != null && !accList.isEmpty()) {
                Id redesSociales = TGS_RecordTypes_Util.getRecordTypeId(BI_Multimarca_PP__c.sObjectType, 'BI_Redes_sociales');
                Id pieDePagina = TGS_RecordTypes_Util.getRecordTypeId(BI_Multimarca_PP__c.sObjectType, 'BI_Pie_de_pagina');

                List<BI_Multimarca_PP__c> lstMultiMarca = new List<BI_Multimarca_PP__c>();
                lstMultiMarca.add(new BI_Multimarca_PP__c(
                        BI_Country__c = accList[0].BI_Country__c,
                        BI_Segment__c = accList[0].BI_Segment__c,
                        RecordTypeId = redesSociales,
                        BI_Activo__c = true));
                
                lstMultiMarca[0].BI_URL__c = 'example.com';
                lstMultiMarca[0].BI_Facebook__c = 'www.facebook.com';
                lstMultiMarca[0].BI_Google__c = 'www.google.es';
                lstMultiMarca[0].BI_Instagram__c = 'www.instagram.com';
                lstMultiMarca[0].BI_Tuenti__c = 'tuenti.es';
                lstMultiMarca[0].BI_Twitter__c = 'twitter.com';
                lstMultiMarca[0].BI_Yammer__c = 'yammer.com';

                lstMultiMarca.add(new BI_Multimarca_PP__c(
                        BI_Country__c = accList[0].BI_Country__c,
                        BI_Segment__c = accList[0].BI_Segment__c,
                        BI_Url__c='example.com',
                        BI_Activo__c = true));

                insert lstMultiMarca;
            }
            // END JMF 25/08/2016
            
            //Start SN 13/10/2016 
            BI_PP_GoogleAnalytics__c portalPlatinoGAId = new BI_PP_GoogleAnalytics__c(Name = 'Id', BI_Google_Analytics_Id__c = 'UA-000001-1');
            insert portalPlatinoGAId;
            //End SN 13/10/2016

            system.runAs(user1){
                Test.startTest();
                BI_TestUtils.throw_exception = false;
                
                PageReference pageRef = new PageReference('PCA_Catalogo?param1=paramValue');
                Test.setCurrentPage(pageRef);
             
                PCA_TemplateController tmpContr = new PCA_TemplateController();
                User userVar = tmpContr.usuarioPortal;
                system.assert(userVar != null);

                Test.stopTest();

                // START JMF 25/08/2016
                System.assertNotEquals(null, tmpContr.footerLinks); // BI_Pie_de_pagina
                System.assertNotEquals(0, tmpContr.footerLinks.size());
                System.assertNotEquals(null, tmpContr.SocialNetwork); // BI_Redes_sociales
                System.assertNotEquals(0, tmpContr.SocialNetwork.size());
                // END JMF 25/08/2016

                //Start SN 13/10/2016 
                String ppGAId = tmpContr.googleAnalyticsId;
                System.assert(ppGAId == portalPlatinoGAId.BI_Google_Analytics_Id__c);
                //End SN 13/10/2016
            
                List<SelectOption> lst_acc = tmpContr.getAccs();
                system.assert(lst_acc != null);
                
                PageReference pageRef_ret = tmpContr.enviarALoginComm();
                system.assert(pageRef_ret != null);
                
                tmpContr.changeAcc();
                String retStr = tmpContr.getCurrentPage();
                tmpContr.changeToES();
                tmpContr.changeToDE();
                tmpContr.changeToFR();
                tmpContr.changeToEN();
                tmpContr.changeToPT();
                String tp=tmpContr.ticketsPage;
                //List<PCA_TemplateController.Categoria> lc=tmpContr.listaCategorias;
                list<BI_Contact_Customer_Portal__c> lstcontacto=tmpContr.CCPActual;
                String tsc=tmpContr.templateStyleCommon;
                String rsc=tmpContr.resourceStyleCommon;
                String acname=tmpContr.AccName;
                try{
                    //List<PCA_TemplateController.Categoria> lstCat=tmpContr.listaCategorias;
                    List<String> lstCat=tmpContr.listaCategorias;  
                }catch(Exception e){} 
                
                String sp =tmpContr.servicesPage;
                boolean estgs=tmpContr.esTGS;
                String ppurl=tmpContr.profilePhotoURL;
                String idioma=tmpContr.idioma;  
                
                PCA_TemplateController.Categoria testCat=new PCA_TemplateController.Categoria('probando', 'probando');
                String pruebCat= testCat.tId+testCat.tName;   
                
                try{
                    List<String> lstGrupos=tmpContr.listaGrupos;  
                    System.assertEquals(Label.TGS_CWP_GROUPS, lstGrupos[0]);
                }catch(Exception e){} 
            }
        }
    }

    static testMethod void PCA_TemplateController_catch_TEST() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        BI_TestUtils.throw_exception = true;
        
        
         PageReference pageRef = new PageReference('PCA_Login');
         Test.setCurrentPage(pageRef);
     
        PCA_TemplateController tmpContr = new PCA_TemplateController();
        
            User userVar = tmpContr.usuarioPortal;
            tmpContr.getAccs();
            tmpContr.enviarALoginComm();
            tmpContr.changeAcc();
            tmpContr.getCurrentPage();
    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}