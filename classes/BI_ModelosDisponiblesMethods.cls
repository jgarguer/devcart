public with sharing class BI_ModelosDisponiblesMethods
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Methods executed by BI_Modelos_disponibles Triggers 
    Test Class:    BI_ModelosDisponiblesMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    27/03/2015              Fernando Arteaga        Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Validates a 'Bolsa de Dinero' has only one 'Modelos_disponibles__c' record associated to the same model (BI_Modelo__c field)
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Fernando Arteaga        Initial Version         
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void checkBolsaDineroHasOnlyOneRecordPerModel(List<BI_Modelos_disponibles__c> news, List<BI_Modelos_disponibles__c> olds)
	{
		try
		{
			Set<Id> setIdBolsaDinero = new Set<Id>();
			Set<Id> setIdModelo = new Set<Id>();
			Map<String, Integer> mapExistingRecords = new Map<String, Integer>();
			
			for (BI_Modelos_disponibles__c md: news)
			{
				setIdBolsaDinero.add(md.BI_Bolsa_de_dinero__c);
				setIdModelo.add(md.BI_Modelo__c);
			}
				
			for (AggregateResult aggResult : [SELECT BI_Bolsa_de_dinero__c, BI_Modelo__c, COUNT(Id)
								   			  FROM BI_Modelos_disponibles__c
								   			  WHERE BI_Bolsa_de_dinero__c IN :setIdBolsaDinero
								   			  AND BI_Modelo__c IN :setIdModelo
								   			  GROUP BY BI_Bolsa_de_dinero__c, BI_Modelo__c])
			{
				String key = String.valueOf(aggResult.get('BI_Bolsa_de_dinero__c')) + String.valueOf(aggResult.get('BI_Modelo__c'));
				mapExistingRecords.put(key, (Integer) aggResult.get('expr0'));
			}
			System.debug('### mapExistingRecords: ' + mapExistingRecords);
			
			for (BI_Modelos_disponibles__c md: news)
			{
				if (mapExistingRecords.containsKey(String.valueOf(md.BI_Bolsa_de_dinero__c) + String.valueOf(md.BI_Modelo__c)))
					md.addError('La bolsa de dinero ya tiene un registro de Modelos Disponibles para ese modelo');
			}
		}
		catch (Exception exc)
		{
			BI_LogHelper.generate_BILog('BI_ModelosDisponiblesMethods.checkBolsaDineroHasOnlyOneRecordPerModel', 'BI_EN', exc, 'Trigger');
		}
	}
}