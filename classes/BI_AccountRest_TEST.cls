@isTest
private class BI_AccountRest_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_AccountRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    08/09/2014              Pablo Oliva             Initial Version
    17/11/2015        		Fernando Arteaga  		BI_EN - CSB Integration
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_AccountRest.getAccount()
    History:
    
    <Date>              <Author>                <Description>
    08/09/2014          Pablo Oliva             Initial version
    25/03/2015			Juan Santisi			Refactored Region__c 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getAccountTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	/*  
    	Commented out.  Left as reference....  
        List <Region__c> lst_region = BI_DataLoad.loadRegions().values();  
        */
       List<Profile> prof = [select Id from Profile where Name = 'BI_Administrator' limit 1];

        User usertest = new User(alias = 'cctest',
                          email = 'cctest@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof[0].Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'cctest@testorg.com');
        
        insert usertest;
        
        System.runAs(usertest)
        {
        	Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
	        List<String>lst_region = new List<String>();
	       
	        lst_region.add('Argentina');
	        
	        //retrieve the country
	        BI_Code_ISO__c region = biCodeIso.get('ARG');
	       
	        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
	        
	        List <Contact> lst_con = BI_DataLoadRest.loadContacts(1, lst_acc);
	        
	        RestRequest req = new RestRequest(); 
	        RestResponse res = new RestResponse();
	             
	        req.requestURI = '/customeraccounts/v1/accounts/'+lst_acc[0].BI_Id_del_cliente__c;  
	        req.httpMethod = 'GET';
	
	        RestContext.request = req;
	        RestContext.response = res;
	
	        Test.startTest();
	        
	        //INTERNAL_SERVER_ERROR
	        BI_TestUtils.throw_exception = true;
	        BI_AccountRest.getAccount();
	        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
	       
	        system.assertEquals(500,RestContext.response.statuscode);
	        /*
	        Antonio Masferrer Garc?a
	        Start 
	        */
	
	        if(lst_log.isEmpty()){
	            throw new BI_Exception('Se esperaba la excepcion y no se ha generado');
	        }
	
	        /*        
	        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
	        Fallando: Expected: test, Actual: Test
	        
	        system.assertEquals(1, lst_log.size());
	        Fallando: Expected: 1, Actual: 5
	        
	        End
	        */
	        
	        //BAD REQUEST
	        BI_TestUtils.throw_exception = false;
	        BI_AccountRest.getAccount();
	        system.assertEquals(400,RestContext.response.statuscode);
	        
	        //NOT FOUND
	        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
	        req.requestURI = '/customeraccounts/v1/accounts/idacc';  
	        req.httpMethod = 'GET';
	
	        RestContext.request = req;
	        RestContext.response = res;
	        
	        BI_AccountRest.getAccount();
	        system.assertEquals(404,RestContext.response.statuscode);
	        
	        //OK
	        req.requestURI = '/customeraccounts/v1/accounts/'+lst_acc[0].BI_Id_del_cliente__c;
	        RestContext.request = req;
	        BI_AccountRest.getAccount();
	        system.assertEquals(200,RestContext.response.statuscode);
	        
	        lst_acc[0].BI_Activo__c = Label.BI_No;
	        update lst_acc[0];
	        BI_AccountRest.getAccount();
	        system.assertEquals(200,RestContext.response.statuscode);
	        
	        lst_acc[0].BI_Activo__c = 'Pendiente de aprobaci?n';
	        update lst_acc[0];
	        BI_AccountRest.getAccount();
	        system.assertEquals(200,RestContext.response.statuscode);
        
        	Test.stopTest();
        }
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_AccountRest.createAccount()
    History:
    
    <Date>              <Author>                <Description>
    08/09/2014          Pablo Oliva             Initial version
    25/03/2015			Juan Santisi			Refactored Region__c
    17/11/2015        	Fernando Arteaga  		BI_EN - CSB Integration
    09/05/2016          Guillermo Muñoz         BI_TestUtils.throw_exception = false added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createAccountTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_TestUtils.throw_exception = false;

        List<Profile> prof = [select Id from Profile where Name = 'BI_Administrator' limit 1];

        User usertest = new User(alias = 'cctest',
                          email = 'cctest@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof[0].Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'cctest@testorg.com');
        
        insert usertest;
        
        System.runAs(usertest)
        {
	    	Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
	        
	        List<String>lst_region = new List<String>();
	       
	        lst_region.add('Chile');
	        
	        //retrieve the country
	        BI_Code_ISO__c region = biCodeIso.get('CHI');
	    	
	    	RestRequest req = new RestRequest(); 
	        RestResponse res = new RestResponse();
	             
	        req.requestURI = '/customeraccounts/v1/accounts/';  
	        req.httpMethod = 'POST';
	        
	        RestContext.request = req;
	        RestContext.response = res;
	
	        Test.startTest();
	        
	        //BAD REQUEST
	        BI_AccountRest.createAccount();
	        system.assertEquals(400,RestContext.response.statuscode);
	        
	        //INTERNAL_SERVER_ERROR
	        Blob body = Blob.valueof('"types": "Administrativo", "status": "inactive", "name": "Test value", "legalId": { "docType": "Otros", "docNumber": "1234567", "country": "aa" }, "customers": ["bb"], "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "address": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicaci?n, 33", "additionalData": null }], "additionalData": null, "accounts": null }');
	        req.requestBody = body;
	        RestContext.request = req;
	        RestContext.request.addHeader('countryISO', 'CHI');
	        
	        //BI_TestUtils.throw_exception = true;
	        BI_AccountRest.createAccount();
	        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
	       
	        system.assertEquals(500,RestContext.response.statuscode);
	        system.assertEquals(1, lst_log.size());
	        
	        //BAD_REQUEST missing fields
	        body = Blob.valueof('{"accountDetails": { "subsegment": null, "statusReason": null, "segment": null, "satisfaction": null, "riskRank": null, "paymentMethods": null, "name": "Prueba Integra 3", "legalId": { "docType": "Test", "docNumber": "20000000007", "country": "0"}, "fraud": false, "description": null, "customerId": null, "contacts": null, "contactingModes": [], "address": [], "additionalData": null, "accountStatus": null, "accountInfoType": "Potencial" }}');
			BI_TestUtils.throw_exception = false;
	        req.requestBody = body;
	        RestContext.request = req;
	        RestContext.request.addHeader('countryISO', 'CHI');
	        
	        BI_AccountRest.createAccount();
	        system.assertEquals(400,RestContext.response.statuscode);
	        
	        //OK
	        //body = Blob.valueof('{"accountDetails": { "subsegment": null, "statusReason": null, "segment": null, "satisfaction": null, "riskRank": null, "paymentMethods": null, "name": "Prueba Integra 3", "legalId": { "docType": "RUT", "docNumber": "19", "country": "CHI"}, "fraud": false, "description": null, "customerId": null, "contacts": null, "contactingModes": [], "address": [], "additionalData": [ { "Key": "accountId", "value": "543210" } ], "accountStatus": null, "accountInfoType": "Potencial" }}');
	        body = Blob.valueof('{"account": {"accountId": "543210", "accountName":"Prueba Integra 3", "accountStatus": "active", "subsegment": null, "statusReason": null, "segment": "corporate", "satisfaction": null, "riskRank": null, "paymentMethods": [], "fraud": false, "description": null}, "contacts": null, "contactingModes": [], "address": [], "identification": [{ "nationalIDType": "RUT", "nationalID": "19", "country": "CHI"}], "additionalData": []}');
	        req.requestBody = body;
	        RestContext.request = req;
	        RestContext.request.addHeader('countryISO', 'CHI');
	        
	        BI_AccountRest.createAccount();
	        system.assertEquals(201,RestContext.response.statuscode);
	        
	        Test.stopTest();
        }
 	}
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_AccountRest.updateAccount()
    History:
    
    <Date>              <Author>                <Description>
    08/09/2014          Pablo Oliva             Initial version
    25/03/2015			Juan Santisi			Refactored Region__c
    17/11/2015        	Fernando Arteaga  		BI_EN - CSB Integration
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void updateAccountTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    	Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Chile');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('CHI');
       	
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
    
    	RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/customeraccounts/v1/accounts/'+lst_acc[0].BI_Id_del_cliente__c;  
        req.httpMethod = 'PUT';
        
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        //BAD REQUEST
        BI_AccountRest.updateAccount();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //INTERNAL_SERVER_ERROR
        Blob body = Blob.valueof('types": "Administrativo", "status": "inactive", "name": "Test value", "legalId": { "docType": "Otros", "docNumber": "1234567", "country": "aa" }, "customers": ["bb"], "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "address": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicaci?n, 33", "additionalData": null }], "additionalData": null, "accounts": null }');
        req.requestBody = body;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', 'CHI');
        
        BI_TestUtils.throw_exception = true;
        BI_AccountRest.updateAccount();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
       
        system.assertEquals(500,RestContext.response.statuscode);
        /*
        Antonio Masferrer Garc?a
        Start 
        */

        if(lst_log.isEmpty()){
            throw new BI_Exception('Se esperaba la excepcion y no se ha generado');
        }

        /*        
       
        system.assertEquals(1, lst_log.size());
        Fallando: Expected: 1, Actual: 5
        
        End
        */
        
        //BAD_REQUEST NOT_FOUND
        body = Blob.valueof('{"accountDetails": { "subsegment": null, "statusReason": null, "segment": null, "satisfaction": null, "riskRank": null, "paymentMethods": null, "name": "Prueba Integra 3", "legalId": { "docType": "Test", "docNumber": "20000000007", "country": "0"}, "fraud": false, "description": null, "customerId": null, "contacts": null, "contactingModes": [], "address": [], "additionalData": null, "accountStatus": null, "accountInfoType": "Potencial" }}');
		BI_TestUtils.throw_exception = false;
        req.requestBody = body;
        req.requestURI = '/customerinfo/v1/customers/test';
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', 'CHI');
        
        BI_AccountRest.updateAccount();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        body = Blob.valueof('{"accountDetails": { "subsegment": null, "statusReason": null, "segment": null, "satisfaction": null, "riskRank": null, "paymentMethods": null, "name": "Prueba Integra 3", "legalId": { "docType": "RUT", "docNumber": "19", "country": "CHI"}, "fraud": false, "description": null, "customerId": null, "contacts": null, "contactingModes": [], "address": [], "additionalData": [ { "Key": "accountId", "value": "543210" } ], "accountStatus": null, "accountInfoType": "Potencial" }}');
        req.requestBody = body;
        req.requestURI = '/customerinfo/v1/customers/'+lst_acc[0].BI_Id_del_cliente__c;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', 'CHI');
        
        BI_AccountRest.updateAccount();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
    
    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}