@isTest
private class BI_TaskMethods_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_TaskMethods class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/05/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/



      /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_TaskMethods.preventDeleteTask
   
    History:
    
    <Date>                  <Author>                <Change Description>
    19/05/2014              Ignacio Llorca             Initial Version       
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void updateMTaskTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
       List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
       List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);      
       system.assert(!lst_acc.isEmpty(), 'lst_acc failed to load');
       List<Opportunity> lst_opp = BI_Dataload.loadOpportunities(lst_acc[0].Id);
       system.assert(!lst_opp.isEmpty(), 'lst_opp failed to load');
        
       Milestone1_Project__c proj = new Milestone1_Project__c();       
       proj.Name='Project 1';
       proj.BI_Oportunidad_asociada__c = lst_opp[0].Id; 
	   
	   insert proj;
	   
       Milestone1_Milestone__c rec = new Milestone1_Milestone__c();
        rec.Name = proj.Name + '' + Datetime.now().getTime();
        rec.Project__c = proj.id;
        rec.Complete__c = false; 
        rec.Kickoff__c = date.today();
        rec.Deadline__c = date.today()+1;
        rec.Description__c = 'Description for ' + rec.Name;
        rec.Expense_Budget__c = 20;
        rec.Hours_Budget__c = 50;
        rec.BI_Country__c=lst_pais[0];
        
       insert rec;
       List <Milestone1_Task__c> tsk_lst = new List <Milestone1_Task__c>();
       Set <Id> set_mtask = new set <Id>();
       for(Integer i=0;i<200;i++){	       
	       Milestone1_Task__c newTask = new Milestone1_Task__c();
	       newTask.Assigned_To__c = UserInfo.getUserId();
	       newTask.Project_Milestone__c = rec.Id;
	       newTask.Description__c = 'Test Description';
	       newTask.Name = 'test name' + string.valueof(i);
	       newTask.Task_Stage__c = 'In Progress';
	       tsk_lst.add(newTask);
       }
       insert tsk_lst;

       Task [] sttsk_lst = [SELECT Id FROM Task WHERE WhatId IN :tsk_lst];

       for (Task tsk:sttsk_lst){
       	tsk.Status = Label.BI_TaskStatus_Completed;
       }

       update sttsk_lst;
       
       for (Task tsk:sttsk_lst){
       	tsk.BI_Relacionado_con_etapa__c = 'etapa';
       }
       
       try
       {
			BI_TaskMethods.preventDeleteTask(sttsk_lst);
       }
       catch(Exception e)
       {
       	System.assert(e.getMessage().contains(Label.BI_ControlBorradoTareaAutomatica));
       }
       
       try
       {
       		BI_TaskMethods.preventTaskForActionPlan(null);
       }
       catch(Exception e)
       {
       		System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
       }
       
       try
       {
       		BI_TaskMethods.updateActionPlan(null);
       }
       catch(Exception e)
       {
       		System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
       }

       try
       {
       		BI_TaskMethods.check_HasClosedPlanDeAccion(null);
       }
       catch(Exception e)
       {
       		System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
       }


    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Test method for BI_TaskMethods.check_HasClosedPlanDeAccion() 
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    03/02/2015                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void check_HasClosedPlanDeAccion_Test() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		BI_Dataload.set_ByPass(false,false);
		/*
		Region__c chile = new Region__c(Name = Label.BI_Chile,
		              BI_Codigo_ISO__c = 'CHL');
		insert chile;
	*/
		String chile = Label.BI_Chile;
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{chile});
		system.assert(!lst_acc.isEmpty());

		List<BI_Plan_de_accion__c> lst_coord = BI_Dataload.loadCustomPlanDeAccion(2, lst_acc[0]);
		system.assert(!lst_coord.isEmpty());

		List<Id> whatId = new List<Id>();
		for(BI_Plan_de_accion__c coo :lst_coord){
			whatId.add(coo.Id);
			coo.BI_Casilla_desarrollo__c = true;
			coo.BI_Estado__c = Label.BI_Finalizado;
		}
		List<Recordtype> lst_rt_task = [SELECT ID, sObjectType  FROM RecordType WHERE Developername  = 'BI_Acuerdo_Plan_de_accion'];

		List<Task> lst_Task = BI_Dataload.loadTaskWithRT(1,lst_rt_task, whatId, Label.BI_TaskStatus_Completed );
		system.assert(!lst_Task.isEmpty());

		update lst_coord;

		Test.StartTest();

		for(Task tsk :lst_Task){
			tsk.Subject = 'update Subject '+ String.valueOf(Math.random() *100000);  
		}

		try{
			update lst_Task;
			system.assert(false);
		}catch(Exception exc){
			system.assert(exc.getMessage().contains(Label.BI_Msg_NoModificarSiActionPlanCerrado));      
		}

		Test.StopTest();

  }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Test method for BI_TaskMethods.updateActionPlan() 
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    04/02/2015                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void updateActionPlan_Test() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		BI_Dataload.set_ByPass(false,false);
		/*
		Region__c chile = new Region__c(Name = Label.BI_Chile,
		              BI_Codigo_ISO__c = 'CHL');
		insert chile;
	*/
		String chile = Label.BI_Chile;
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{chile});
		system.assert(!lst_acc.isEmpty());

		List<BI_Plan_de_accion__c> lst_coord = BI_Dataload.loadCustomPlanDeAccion(2, lst_acc[0]);
		system.assert(!lst_coord.isEmpty());

		List<Id> whatId = new List<Id>();
		for(BI_Plan_de_accion__c coo :lst_coord){
			whatId.add(coo.Id);
		}


		for(BI_Plan_de_accion__c coo :[SELECT BI_Estado__c FROM BI_Plan_de_accion__c WHERE ID IN :whatId]){
			system.assertEquals(Label.BI_PlanAccion_Estado_En_creacion, coo.BI_Estado__c);
		}
		List<Recordtype> lst_rt_task = [SELECT ID, sObjectType  FROM RecordType WHERE Developername  = 'BI_Acuerdo_Plan_de_accion'];

		List<Task> lst_Task = BI_Dataload.loadTaskWithRT(1,lst_rt_task, whatId, Label.BI_TaskStatus_Waiting );
		system.assert(!lst_Task.isEmpty());

		for(Task tsk :lst_Task){
			tsk.Status = Label.BI_TaskStatus_Completed;
		}

		Test.StartTest();
		update lst_Task;

		List<BI_Plan_de_accion__c> lst_assert = [select Id,BI_Estado__c from BI_Plan_de_accion__c where Id IN :whatId];
		system.assert(!lst_assert.isEmpty());

		for(BI_Plan_de_accion__c ap:lst_assert){
			system.assertEquals( Label.BI_PlanAccion_Estado_Pendiente_finalizacion, ap.BI_Estado__c);
		}

		Test.StopTest();
  }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Test method for BI_TaskMethods.preventTaskForActionPlan() 
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    05/02/2015                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void preventTaskForActionPlan_TEST() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		BI_Dataload.set_ByPass(false,false);
		/*
		Region__c chile = new Region__c(Name = Label.BI_Chile,
									  BI_Codigo_ISO__c = 'CHL');
		insert chile;
	*/
		String chile = Label.BI_Chile;
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{chile});
		system.assert(!lst_acc.isEmpty());

		List<BI_Plan_de_accion__c> lst_coord = BI_Dataload.loadCustomPlanDeAccion(2, lst_acc[0]);
		system.assert(!lst_coord.isEmpty());

		List<Id> whatId = new List<Id>();
		for(BI_Plan_de_accion__c coo :lst_coord){
		    whatId.add(coo.Id);
		    coo.BI_Estado__c = Label.BI_PlanAccion_Estado_En_curso;
		}
		update lst_coord;

		//List<Recordtype> lst_rt_task = [select Id from RecordType where SobjectType = 'Task' and DeveloperName = 'BI_Tarea' limit 1];
		List<Recordtype> lst_rt_task = [select Id from RecordType where SobjectType = 'Task' and DeveloperName = 'BI_Acuerdo_Plan_de_accion' limit 1];

		try{
			List<Task> lst_task2 = BI_Dataload.loadTaskWithRT(2,lst_rt_task, whatId, Label.BI_TaskStatus_Waiting);
			system.assert(false);
		}catch(Exception exc){
			system.assert(exc.getMessage().contains(Label.BI_Tareas_Plan_Accion));
		}
		
    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Iñaki Frial
    Company:       Accenture
    Description:   Test method for BI_TaskMethods.checkBlockingTask() 
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    14/03/2018                      Iñaki Frial               Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
  static testMethod void checkBlockingTask(){
    HO_e_SOF__c eSof = new HO_e_SOF__c(HO_Status__c='Initialization',
                          HO_Es_OB_Comercializadora__c='NO'

      );
    insert eSof;
     Id devRecordTypeId = [Select id,SobjectType from RecordType where DeveloperName ='HO_Tarea_Bloqueante' and SobjectType ='Task'][0].Id;
     Task tarea = new Task(OwnerId = UserInfo.getUserId(), 
                           RecordTypeId= devRecordTypeId,
                           Status = 'In Creation',
                           Priority='Normal',
                           WhatId= eSof.Id,
                           Subject='Call');
     insert tarea;
     HO_Service_Unit__c serUnit= new HO_Service_Unit__c(HO_Status__c='Pending Economic Approval');
     insert serUnit;
     Task tarea2 = new Task(OwnerId = UserInfo.getUserId(), 
                           RecordTypeId= devRecordTypeId,
                           Status = 'In Creation',
                           Priority='Normal',
                           WhatId= serUnit.Id,
                           Subject='Call');
     insert tarea2;
     
  }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}