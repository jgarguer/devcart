/*------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julio Laplaza
    Company:       HPE
    Description:   método de TEST para obtener el token de autenticación de RoD.
        
    History:
        
    <Date>              <Author>                            <Description>
    14/06/2016          Julio Laplaza                       Implementation
    22/06/2016          José Luis González Beltrán          Remove BIIN_UNICA_Base tests
------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest(seeAllData = false)
public class BIIN_Login_TEST 
{
    @isTest static void test_login() 
    {
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('Login'));
        
        Test.startTest(); 

        BIIN_Login_WS loginController = new BIIN_Login_WS();
        String token = loginController.login('Salesforce', 'Salesforce');
        System.assert(String.isNotEmpty(token));

        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicketException500'));
        token = loginController.login('Salesforce', 'Salesforce');

        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('AuthException401'));
        token = loginController.login('Salesforce', 'Salesforce');

        Test.stopTest();
    }
}