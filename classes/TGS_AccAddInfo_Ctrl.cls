/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis
Company:       Everis
Description:   Controller for TGS_Account_Additional_Info.vfp

<Date>                  <Author>                <Change Description>
01/06/2017              Everis		            Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class TGS_AccAddInfo_Ctrl {
    
    public TGS_Account_Additional_Information__c accInfo;
    public List<TGS_Account_Additional_Information__c> accInfo2;
    public Boolean registro {get; set;}
    
    public TGS_AccAddInfo_Ctrl(ApexPages.StandardController controller){
        registro = false;
    }
    
    public TGS_Account_Additional_Information__c getaccInfo() {    
        
        String idAcc = ApexPages.currentPage().getUrl().split('id=')[1].split('&')[0];
        List<TGS_Account_Additional_Information__c> accInfo2 = [SELECT TGS_Account__c, TGS_Customer_profile__c, TGS_Full_Contract_Value__c, TGS_Initial_Full_Contract_Value_Date__c, TGS_Finish_Full_Contract_Value_Date__c, TGS_Service__c, TGS_Traffic_profile__c 
                   FROM TGS_Account_Additional_Information__c WHERE TGS_Account__c = :idAcc LIMIT 1];     
        if(accInfo2.size() > 0){
            accInfo = accInfo2[0];
        }else{
            accInfo = new TGS_Account_Additional_Information__c(TGS_Account__c=idAcc);
        }
        return accInfo;
    }
    
    public void checkAI(){
        String idAcc = ApexPages.currentPage().getUrl().split('id=')[1].split('&')[0];
        List<TGS_Account_Additional_Information__c> lista_add_info = [SELECT TGS_Account__c, TGS_Customer_profile__c, TGS_Full_Contract_Value__c, TGS_Initial_Full_Contract_Value_Date__c, TGS_Finish_Full_Contract_Value_Date__c, TGS_Service__c, TGS_Traffic_profile__c 
                   FROM TGS_Account_Additional_Information__c WHERE TGS_Account__c = :idAcc ];
        if(lista_add_info.size() > 0){
            registro = true;
        }else{
            registro = false;
        }
    } 
    
    public pageReference saveAddInfo() {
       try {
           upsert accInfo;
       }
       catch(Exception e){
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage()));
           registro = true;
       }
       return null;
    }
    
    public pageReference deleteAddInfo() {
       try {
           accInfo = getaccInfo();
           delete accInfo;
           registro = false; 
       }
       catch(Exception e){
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage()));
       }
       return null;
    }
}