public class BI_O4_Quote_Methods {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Salesforce.com
    Description:   Methods executed by Quote Triggers 
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    03/01/2017              Alvaro García          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Salesforce.com
    Description:   Method that check if the quote is active and then active the proposal item
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    03/01/2017              Alvaro García          	Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void activeProposalItem(List <Quote> news, List <Quote> olds){
        try{
            if(BI_TestUtils.isRunningTest()) {
                throw new BI_Exception('test');
            }

            List <Id> toCheckId = new List <Id>();

            for (Integer i = 0; i < news.size(); i++) {

            	if (olds == null || olds[i].IsSyncing != news[i].IsSyncing) {

            		toCheckId.add(news[i].Id);
            	}
            }

            if (!toCheckId.isEmpty()) {

            	List <BI_O4_Proposal_Item__c> lst_ProposalItem = [SELECT Id, BI_O4_Quote__r.IsSyncing, BI_O4_Proposal_Activa__c FROM BI_O4_Proposal_Item__c WHERE BI_O4_Quote__c IN : toCheckId];
	            
	            if (!lst_ProposalItem.isEmpty()) {
	            	
	            	for (BI_O4_Proposal_Item__c item : lst_ProposalItem) {

		            	item.BI_O4_Proposal_Activa__c = item.BI_O4_Quote__r.IsSyncing;
		            }

		            update lst_ProposalItem;
	            }

            }
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_O4_Quote_Methods.activeProposalItem', 'BI_EN', exc, 'Trigger');
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Almirón
    Company:       Salesforce.com
    Description:   Method that fill the analagous fields from Approvals to Opportunity when a proposal is synced and the Approval has passed all the Gates.
    
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    10/07/2017              Javier Almirón          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void migAppOppQuote(List <Quote> news, Map <Id, Quote> oldMap ){


        Set <Id> propSet = new Set <Id>();
        List <Quote> propList = new List <Quote>();
        List <Quote> propToUpdate = new List <Quote>();
        List <BI_O4_Approvals__c> approvList = new List <BI_O4_Approvals__c>();
        List <Opportunity> oppToUpdate = new List <Opportunity>();

        if (news!= null){

            for(Quote quot: news){

                if (quot.IsSyncing == true && oldMap.get(quot.ID).IsSyncing == false){
                    propSet.add(quot.ID);
                    propList.add(quot);

                }

            }
             

            if (!propSet.isEmpty()){
                approvList = [SELECT ID, BI_O4_Gate_status__c, BI_O4_Opportunity__c, BI_O4_FCV__c, BI_O4_FCV_Off_Net__c, 
                                                BI_O4_NRR__c, BI_O4_NRR_TGS__c,
                                                BI_O4_MRR_TEF__c, BI_O4_MRR_TGS__c,
                                                BI_O4_NRC_TEF__c, BI_O4_NRC_TGS__c,
                                                BI_O4_MRC_TEF__c, BI_O4_MRC_TGS__c,
                                                BI_O4_iCOST_TGS__c, BI_O4_Incremental_Cost__c,
                                                BI_O4_Incremental_OIBDA__c, BI_O4_oibda_tgs__c,
                                                BI_O4_Incremental_OIBDA_Percent__c, BI_O4_Incremental_oibda_tgs__c,
                                                BI_O4_CAPEX__c, BI_O4_CAPEX_TGS__c,
                                                BI_O4_Incremental_OI__c, BI_O4_iOI_TGS__c,
                                                BI_O4_Incremental_OI_Percent__c, BI_O4_Incremental_OI_tgs__c
                                                FROM BI_O4_Approvals__c 
                                                WHERE BI_O4_Proposal__c IN: propSet order by CreatedDate desc Limit 1];
            }


            if (!approvList.isEmpty()){

                for (BI_O4_Approvals__c app : approvList){

                    if (app.BI_O4_Gate_status__c == 'GO' || app.BI_O4_Gate_status__c == 'Conditional Go'){

                        Opportunity opp = new Opportunity (Id=app.BI_O4_Opportunity__c, 
                                                           BI_O4_FCV_Business_Case__c = app.BI_O4_FCV__c,
                                                           BI_O4_BC_FCV_TGS__c = app.BI_O4_FCV_Off_Net__c,
                                                           BI_O4_Opportunity_Won_BC_NRR_TEF__c = app.BI_O4_NRR__c,
                                                           BI_O4_BC_NRR_TGS__c = app.BI_O4_NRR_TGS__c,
                                                           BI_O4_Opportunity_Won_BC_MRR_TEF__c = app.BI_O4_MRR_TEF__c,
                                                           BI_O4_BC_MRR_TGS__c = app.BI_O4_MRR_TGS__c,
                                                           BI_O4_Opportunity_Won_BC_NRC_TEF__c = app.BI_O4_NRC_TEF__c,
                                                           BI_O4_BC_NRC_TGS__c = app.BI_O4_NRC_TGS__c,
                                                           BI_O4_Opportunity_Won_BC_MRC_TEF__c = app.BI_O4_MRC_TEF__c,
                                                           BI_O4_BC_MRC_TGS__c = app.BI_O4_MRC_TGS__c,
                                                           BI_O4_Opportunity_Won_BC_iCOST_TEF__c = app.BI_O4_Incremental_Cost__c,
                                                           BI_O4_BC_iCOST_TGS__c = app.BI_O4_iCOST_TGS__c,  
                                                           BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c = app.BI_O4_CAPEX__c,
                                                           BI_O4_BC_iCAPEX_TGS__c = app.BI_O4_CAPEX_TGS__c);

                        oppToUpdate.add(opp);

                    } else { 
                        Opportunity opp = new Opportunity (Id=app.BI_O4_Opportunity__c, 
                                                           BI_O4_FCV_Business_Case__c = 0,
                                                           BI_O4_BC_FCV_TGS__c = 0,
                                                           BI_O4_Opportunity_Won_BC_NRR_TEF__c = 0,
                                                           BI_O4_BC_NRR_TGS__c = 0,
                                                           BI_O4_Opportunity_Won_BC_MRR_TEF__c = 0,
                                                           BI_O4_BC_MRR_TGS__c = 0,
                                                           BI_O4_Opportunity_Won_BC_NRC_TEF__c = 0,
                                                           BI_O4_BC_NRC_TGS__c = 0,
                                                           BI_O4_Opportunity_Won_BC_MRC_TEF__c = 0,
                                                           BI_O4_BC_MRC_TGS__c = 0,
                                                           BI_O4_Opportunity_Won_BC_iCOST_TEF__c = 0,
                                                           BI_O4_BC_iCOST_TGS__c = 0,
                                                           BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c = 0,
                                                           BI_O4_BC_iCAPEX_TGS__c = 0);

                        oppToUpdate.add(opp);
                    }  
     
                }

            } else {

                    for (Quote q: propList){ 
        
                     Opportunity opp = new Opportunity (Id=q.BI_O4_Opportunity__c, 
                                                        BI_O4_FCV_Business_Case__c = 0,
                                                        BI_O4_BC_FCV_TGS__c = 0,
                                                        BI_O4_Opportunity_Won_BC_NRR_TEF__c = 0,
                                                        BI_O4_BC_NRR_TGS__c = 0,
                                                        BI_O4_Opportunity_Won_BC_MRR_TEF__c = 0,
                                                        BI_O4_BC_MRR_TGS__c = 0,
                                                        BI_O4_Opportunity_Won_BC_NRC_TEF__c = 0,
                                                        BI_O4_BC_NRC_TGS__c = 0,
                                                        BI_O4_Opportunity_Won_BC_MRC_TEF__c = 0,
                                                        BI_O4_BC_MRC_TGS__c = 0,
                                                        BI_O4_Opportunity_Won_BC_iCOST_TEF__c = 0,
                                                        BI_O4_BC_iCOST_TGS__c = 0,
                                                        BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c = 0,
                                                        BI_O4_BC_iCAPEX_TGS__c = 0);

                     oppToUpdate.add(opp);
                   }

            }
        }

        if (!oppToUpdate.isEmpty()){
    
                update oppToUpdate;
    
        }

    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Fernández Díaz
    Company:       Accenture
    Description:   Updates 'Skip Gates' field on Opportunity, when Skip Gates field is checked
    
    History: 
    
    <Date>                  <Author>                    <Change Description>
    06/09/2017              Alberto Fernández Díaz      Initial Version      
    11/09/2017              Alberto Fernández Díaz      Permissions validation fix
    25/09/2017              Gawron, Julián              Changing query on SyncedQuoteId limit 200.000 to many rows
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void selectQuoteSkipGates(List <Quote> news, Map <Id, Quote> oldMap){

        System.debug('ENTRO AQUI EN METODO');

        // List<Id> lst_idQuotes = new List<Id>();

        Map<Id, Quote> map_OppId_Quote = new Map<Id, Quote>();

        try {

            if(BI_TestUtils.isRunningTest()) { //JEG 
                throw new BI_Exception('test');
            }

            List<PermissionSetAssignment>   psa =  [SELECT Id
                                            FROM PermissionSetAssignment
                                            WHERE AssigneeId = :UserInfo.getUserId()
                                            AND PermissionSet.name = 'BI_O4_Skip_Gates_and_Approval'];

            //Comprobamos que el campo BI_O4_Skip_Gates__c este siendo modificado, de false a true
            for(Quote quote : news) {
                if(quote.BI_O4_Skip_Gates__c != oldMap.get(quote.Id).BI_O4_Skip_Gates__c) {
                    //Comprobamos si el usuario tiene privilegios para realizar la accion
                    if(psa.isEmpty()) {
                        quote.addError('No tiene permisos necesarios para modificar el campo Skip Gates');
                    }
                    else if(quote.BI_O4_Skip_Gates__c == false) {
                        quote.addError('El campo Skip Gates no puede ser modificado una vez activado');
                    }
                    //Guardamos, en el campo Skip Gates Comment el nombre de usuario, fecha y hora de la modificación del campo
                    else {
                        quote.BI_O4_SkipGates_Comment__c = 'Ult. modificación: ' + UserInfo.getUserName() + ' (' + UserInfo.getUserId() + '). Fecha: ' + String.valueOfGmt(Datetime.now());
                         //lst_idQuotes.add(quote.Id);
                         map_OppId_Quote.put(quote.OpportunityId, quote);
                    }
                }
            }
            System.debug('selectQuoteSkipGates');
            if(!map_OppId_Quote.isEmpty()){
                 Map<Id, Opportunity> map_idOpp = new Map<Id, Opportunity>(
                 [SELECT Id, BI_O4_Skip_Gates__c FROM Opportunity WHERE Id IN :map_OppId_Quote.keySet()]); 
                List<Opportunity> lst_oppsToUpdate = new List<Opportunity>();
                System.debug('selectQuoteSkipGates' + map_idOpp );
                for(Opportunity opp : map_idOpp.values()){ //Recorremos las oportunidades
                    //Comparamos el valor actual en la opp con el nuevo de la quote relacionada
                    if(opp.BI_O4_Skip_Gates__c != map_OppId_Quote.get(opp.Id).BI_O4_Skip_Gates__c ){
                        opp.BI_O4_Skip_Gates__c = map_OppId_Quote.get(opp.Id).BI_O4_Skip_Gates__c;
                        lst_oppsToUpdate.add(opp);
                    } 
                }

                if(!lst_oppsToUpdate.isEmpty()) {
                    //Actualizamos las oportunidades
                    update lst_oppsToUpdate;
                }
                System.debug('selectQuoteSkipGates lst_oppsToUpdate' + lst_oppsToUpdate );
            }
        }
        catch(Exception ex) {
            BI_LogHelper.generate_BILog('BI_O4_Quote_Methods.selectQuoteSkipGates', 'BI_EN', ex, 'Trigger');
        }
    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Bartolo
    Company:       Aborda
    Description:   Fill proposal fields with the data of the field BI_O4_Business_Case_Output__c
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    17/10/2017                      Oscar Bartolo               Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void fillBusinessFields(List<Quote> news) {
        if(BI_TestUtils.isRunningTest()){
            throw new BI_Exception('Test');
        }
        
        for(Quote newQuote : news) {
            if (newQuote.BI_O4_Business_Case_Output__c != null) {
                List<String> lst_value = newQuote.BI_O4_Business_Case_Output__c.split(';');
                Schema.FieldSet fsBusiness = Schema.SObjectType.Quote.fieldSets.BI_O4_Business_Case_Output;
                List<Schema.FieldSetMember> lst_fields = fsBusiness.getFields();

                /*Business Case Output = Proposal NRR_TEF; Proposal MRR_TEF; Proposal FCV_TEF; Proposal NRC_TEF; Proposal MRC_TEF; Proposal iCost_TEF; 
                    Proposal iCapex_TEF; Proposal NRR_TGS; Proposal MRR_TGS; Proposal FCV_TGS; Proposal NRC_TGS; Proposal MRC_TGS; Proposal iCOST_TGS; 
                    Proposal iCAPEX_TGS; Date Proposal Ready;Quotation System ID*/

               /* Business Case Output = 23.25; 59.39; 98.21; 1,200.68; 99; 1.125; 9,863.59; 93.36; 63.96; 369.3; 11; 983.69; 5,840.956955; 853.5; 11/28/2016; Datos de test*/


                if (lst_fields.size() == lst_value.size()) {
                    for(Integer i = 0; i < lst_value.size(); i++) {
                        String fieldName = lst_fields[i].getFieldPath();
                        String fieldType = String.ValueOf(lst_fields[i].getType());

                        if (fieldType.equalsIgnoreCase('DATE')) {
                            List<String> lst_Date = lst_value[i].split('/');
                            
                            if (lst_value[i] == '' || lst_value[i] == ' ') {
                                newQuote.put(fieldName, null);
                            } else {
                                String regex = '(?:0?[1-9]|1?[0-2])([/])(3?[01]|[1-2][0-9]|0?[1-9])\\1\\d{4}$';
                                String strDate = lst_value[i].trim();
                                Date quoteDate;

                                Pattern p = Pattern.compile(regex);
                                Matcher pm = p.matcher(strDate);
                                if(pm.matches()) {
                                    quoteDate = Date.newInstance(Integer.ValueOF(lst_Date[2].trim()), Integer.ValueOF(lst_Date[0].trim()), Integer.ValueOF(lst_Date[1].trim()));
                                    newQuote.put(fieldName, quoteDate);
                                } else {
                                    newQuote.addError('The field ' + lst_fields[i].getLabel() + ' is not a date.');
                                }
                            }
                        } else if (fieldType.equalsIgnoreCase('CURRENCY')) {
                            String strDec = lst_value[i].trim();
                            strDec = strDec.replace(',', '');
                           
                            if (strDec == '') {
                                newQuote.put(fieldName, null);
                            } else {
                                Pattern p = Pattern.compile('[0-9]+([.][0-9]+)?');
                                Matcher pm = p.matcher(String.ValueOf(strDec));
                                if(pm.matches()) {
                                    newQuote.put(fieldName, Decimal.ValueOf(strDec));
                                } else {
                                    newQuote.addError('The field ' + lst_fields[i].getLabel() + ' is not a decimal.');
                                }
                            }
                        } else {
                            newQuote.put(fieldName, lst_value[i]);
                        }
                    }
                    
                    newQuote.BI_O4_Business_Case_Output__c = '';
                } else {
                    newQuote.addError('There is not the same number of fields and values in the field Business Case Output');
                }
            }
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Almirón García
    Company:       Accenture
    Description:   If the user who activates the trigger is inside the Custom Setting BI_O4_Skip_Gates_Users, the field BI_O4_Skip_gates of the proposals must be true.
    
    History: 
    
    <Date>                  <Author>                    <Change Description>
    30/10/2017              Javier Almirón García       Initial Version      
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void userSkipGates (List <Quote> news){
        String idUser = Id.valueOf(UserInfo.getUserId());
        List <BI_O4_Skip_Gates_Users__c> idSkipGates = BI_O4_Skip_Gates_Users__c.getall().values();
        if (news!= null){
            for (Quote proposal: news){
                for( Integer i = 0; i < idSkipGates.size(); i++ ){
                    if (idUser == idSkipGates[i].BI_O4_Id_Skip_Gates__c){
                        proposal.BI_O4_Skip_Gates__c = true;
                    }
                }
            }
        }
    }
}