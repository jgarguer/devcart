//Generated by wsdl2apex

public class BI_ListaCodigoClienteWSDL {
    public class ListaCodigoClienteWS {
        public String endpoint_x = 'http://salesforce-crm.movistar.cl/ServiciosCRMCIR/services/ListaCodigoClienteWS';//'http://tempuri.org/ServiciosCRMCIR/services/ListaCodigoClienteWS';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://movistar.cl/CIR/vo/', 'BI_ListaCodigoClienteWrapper', 'http://movistar.cl/CIR/ServiceListaCodigoCliente', 'BI_ListaCodigoClienteWSDL'};
        public BI_ListaCodigoClienteWrapper.ListaCodigoClienteOutVO obtenerListaCodCliente(BI_ListaCodigoClienteWrapper.ListaCodigoClienteInVO listaCodigoClienteInVO) {
            BI_ListaCodigoClienteWrapper.ListaCodigoClienteInMsg request_x = new BI_ListaCodigoClienteWrapper.ListaCodigoClienteInMsg();
            request_x.listaCodigoClienteInVO = listaCodigoClienteInVO;
            BI_ListaCodigoClienteWrapper.ListaCodigoClienteOutMsg response_x;
            Map<String, BI_ListaCodigoClienteWrapper.ListaCodigoClienteOutMsg> response_map_x = new Map<String, BI_ListaCodigoClienteWrapper.ListaCodigoClienteOutMsg>();
            response_map_x.put('response_x', response_x);
            if(!Test.isRunningTest()){
	            WebServiceCallout.invoke(
	              this,
	              request_x,
	              response_map_x,
	              new String[]{endpoint_x,
	              'http://movistar.cl/CIR/ServiceListaCodigoCliente/ListaCodCliente',
	              'http://movistar.cl/CIR/vo/',
	              'obtenerListaCodCliente',
	              'http://movistar.cl/CIR/vo/',
	              'obtenerListaCodClienteResponse',
	              'BI_ListaCodigoClienteWrapper.ListaCodigoClienteOutMsg'}
	            );
	            response_x = response_map_x.get('response_x');
	            return response_x.obtenerListaCodClienteReturn;
	    	}else{
              return null; 
            }
        }
    }
}