public  class ADQ_NumberToText {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julio Ortega
    Company:       Adquira México
    Description:   Permite  el monto en textos
    
    History:
    
    <Date>            <Author>              <Description>
    09/26/2014        Julio Ortega         Initial version
    20/07/2015		  Julio Ortega 		   Moneda en letra
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
		
	private Integer flag;
	public Double numero;
	public String importe_parcial;
	public String num;
	public String num_letra;
	public String num_letras;
	public String num_letram;
	public String num_letradm;
	public String num_letracm;
	public String num_letramm;
	public String num_letradmm;
	
	public ADQ_NumberToText(){
		numero = 0;
		flag=0;
	}
	public ADQ_NumberToText(Double n){
		numero = n;
		flag=0;
	}

	
	private String unidad(Integer numero){
		

		if (numero == 9) {
    		num = 'NUEVE';
		} 
		else if (numero == 8) {
    		num = 'OCHO';
		} 
		else if (numero == 7) {
    		num = 'SIETE';
		} 
		else if (numero == 6) {
    		num = 'SEIS';
		} 
		else if (numero == 5) {
    		num = 'CINCO';
		} 
		else if (numero == 4) {
    		num = 'CUATRO';
		} 
		else if (numero == 3) {
    		num = 'TRES';
		} 
		else if (numero == 2) {
    		num = 'DOS';
		} 
		else if (numero == 1) {
    		if (flag == 0)
				num = 'UNO';
			else 
				num = 'UN';
		}
		else if (numero == 0) {
    		num = '';
		} 
		
		return num;
	}
	
	private String decena(Integer numero){
	
		if (numero >= 90 && numero <= 99)
		{
			num_letra = 'NOVENTA ';
			if (numero > 90)
				num_letra = num_letra + ('y ') + (unidad(numero - 90));
		}
		else if (numero >= 80 && numero <= 89)
		{
			num_letra = 'OCHENTA ';
			if (numero > 80)
				num_letra = num_letra + ('y ') + (unidad(numero - 80));
		}
		else if (numero >= 70 && numero <= 79)
		{
			num_letra = 'SETENTA ';
			if (numero > 70)
				num_letra = num_letra + ('y ') + (unidad(numero - 70));
		}
		else if (numero >= 60 && numero <= 69)
		{
			num_letra = 'SESENTA ';
			if (numero > 60)
				num_letra = num_letra + ('y ') + (unidad(numero - 60));
		}
		else if (numero >= 50 && numero <= 59)
		{
			num_letra = 'CINCUENTA ';
			if (numero > 50)
				num_letra = num_letra + ('y ') + (unidad(numero - 50));
		}
		else if (numero >= 40 && numero <= 49)
		{
			num_letra = 'CUARENTA ';
			if (numero > 40)
				num_letra = num_letra + ('y ') + (unidad(numero - 40));
		}
		else if (numero >= 30 && numero <= 39)
		{
			num_letra = 'TREINTA ';
			if (numero > 30)
				num_letra = num_letra + ('y ') + (unidad(numero - 30));
		}
		else if (numero >= 20 && numero <= 29)
		{
			if (numero == 20)
				num_letra = 'VEINTE ';
			else
				num_letra = 'VEINTI' + (unidad(numero - 20));
		}
		else if (numero >= 10 && numero <= 19)
		{
			
			if (numero == 10) {
	    		num_letra = 'DIEZ ';
			} 
			else if (numero == 11) {
	    		num_letra = 'ONCE ';
			} 
			else if (numero == 12) {
	    		num_letra = 'DOCE ';
			} 
			else if (numero == 13) {
	    		num_letra = 'TRECE ';
			} 
			else if (numero == 14) {
	    		num_letra = 'CATORCE ';
			} 
			else if (numero == 15) {
	    		num_letra = 'QUINCE ';
			} 
			else if (numero == 16) {
	    		num_letra = 'DIECISEIS ';
			} 
			else if (numero == 17) {
	    		num_letra = 'DIECISIETE ';
			} 
			else if (numero == 18) {
	    		num = 'DIECIOCHO ';
			} 
			else if (numero == 19) {
	    		num = 'DIECINUEVE ';
			} 

		}
		else
			num_letra = unidad(numero);

	return num_letra;
	}	

	private String centena(Integer numero){
		if (numero >= 100)
		{
			if (numero >= 900 && numero <= 999)
			{
				num_letra = 'NOVECIENTOS ';
				if (numero > 900)
					num_letra = num_letra + (decena(numero - 900));
			}
			else if (numero >= 800 && numero <= 899)
			{
				num_letra = 'OCHOCIENTOS ';
				if (numero > 800)
					num_letra = num_letra + (decena(numero - 800));
			}
			else if (numero >= 700 && numero <= 799)
			{
				num_letra = 'SETECIENTOS ';
				if (numero > 700)
					num_letra = num_letra + (decena(numero - 700));
			}
			else if (numero >= 600 && numero <= 699)
			{
				num_letra = 'SEISCIENTOS ';
				if (numero > 600)
					num_letra = num_letra + (decena(numero - 600));
			}
			else if (numero >= 500 && numero <= 599)
			{
				num_letra = 'QUINIENTOS ';
				if (numero > 500)
					num_letra = num_letra + (decena(numero - 500));
			}
			else if (numero >= 400 && numero <= 499)
			{
				num_letra = 'CUATROCIENTOS ';
				if (numero > 400)
					num_letra = num_letra + (decena(numero - 400));
			}
			else if (numero >= 300 && numero <= 399)
			{
				num_letra = 'TRESCIENTOS ';
				if (numero > 300)
					num_letra = num_letra + (decena(numero - 300));
			}
			else if (numero >= 200 && numero <= 299)
			{
				num_letra = 'DOSCIENTOS ';
				if (numero > 200)
					num_letra = num_letra + (decena(numero - 200));
			}
			else if (numero >= 100 && numero <= 199)
			{
				if (numero == 100)
					num_letra = 'CIEN ';
				else
					num_letra = 'CIENTO ' + (decena(numero - 100));
			}
		}
		else
			num_letra = decena(numero);
		
		return num_letra;	
	}	

	private String miles(Integer numero){
		if (numero >= 1000 && numero <2000){
			num_letram = ('MIL ') + (centena(math.mod(numero, 1000)));
		}
		if (numero >= 2000 && numero <10000){
			flag=1;
			num_letram = unidad(numero/1000) + (' MIL ') + (centena(math.mod(numero, 1000)));
		}
		if (numero < 1000)
			num_letram = centena(numero);
		
		return num_letram;
	}		

	private String decmiles(Integer numero){
		if (numero == 10000)
			num_letradm = 'DIEZ MIL';
		if (numero > 10000 && numero <20000){
			flag=1;
			num_letradm = decena(numero/1000) + ('MIL ') + (centena(math.mod(numero, 1000)));		
		}
		if (numero >= 20000 && numero <100000){
			flag=1;
			num_letradm = decena(numero/1000) + (' MIL ') + (miles(math.mod(numero, 1000)));		
		}
		
		
		if (numero < 10000)
			num_letradm = miles(numero);
		
		return num_letradm;
	}		

	private String cienmiles(Integer numero){
		if (numero == 100000)
			num_letracm = 'CIEN MIL';
		if (numero >= 100000 && numero <1000000){
			flag=1;
			num_letracm = centena(numero/1000) + (' MIL ') + (centena(math.mod(numero, 1000)));		
		}
		if (numero < 100000)
			num_letracm = decmiles(numero);
		return num_letracm;
	}		

	private String millon(Integer numero){
		if (numero >= 1000000 && numero <2000000){
			flag=1;
			num_letramm = ('UN MILLON ') + (cienmiles(math.mod(numero, 1000000)));
		}
		if (numero >= 2000000 && numero <10000000){
			flag=1;
			num_letramm = unidad(numero/1000000) + (' MILLONES ') + (cienmiles(math.mod(numero, 1000000)));
		}
		if (numero < 1000000)
			num_letramm = cienmiles(numero);
		
		return num_letramm;
	}		
	
	private String decmillon(Integer numero){
		if (numero == 10000000)
			num_letradmm = 'DIEZ MILLONES';
		if (numero > 10000000 && numero <20000000){
			flag=1;
			num_letradmm = decena(numero/1000000) + ('MILLONES ') + (cienmiles(math.mod(numero, 1000000)));		
		}
		if (numero >= 20000000 && numero <100000000){
			flag=1;
			num_letradmm = decena(numero/1000000) + (' MILLONES ') + (millon(math.mod(numero, 1000000)));	
		}
		
		
		if (numero < 10000000)
			num_letradmm = millon(numero);
		
		return num_letradmm;
	}		

	
	public String convertirLetras(Double numeroDoble){
		
		Integer numero = numeroDoble.intValue();
		num_letras = decmillon(numero);
		
		Decimal decimales = numeroDoble - numero;
		Double decimalDoble = decimales*100;
		Integer decimalEntero = Math.round(decimalDoble);
		String cadenaDecimales;
		
		if(decimalEntero > 0 && decimalEntero < 10){
			cadenaDecimales = '0' + decimalEntero + '/100 ';
		}
		else if(decimalEntero >= 10){
			cadenaDecimales = decimalEntero + '/100 ';
		}
		else cadenaDecimales = '00/100';

		
		return num_letras + ' MONEDA& ' + cadenaDecimales;
	}
	
	
}