/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:			Alejandro Labrin
Company:		Everis Centers Temuco
Description:	RFG-NOR-DIR-004 / RFG-NOR-DIR-005 / RFG-NOR-DIR-006: 
Página Lightning Experience para normalización de direcciones.

History:

<Date>                      <Author>                        <Change Description>
25/11/2016                  Alejandro Labrin				Initial Version
24/10/2017					Daniel Leal						Ever01: Inclusión de Logs
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

global with sharing class FS_CHI_Address_Management {
    /* SALESFORCE VARIABLE DECLARATION */
    /* Empty */
    
    global static BI_RestWrapper.ValidateAddressResponseType getValidAddresses(String addressName, String addressNumber, String floor, String apartment, String area, String locality, String municipality, String region, String country, String postalCode, String bloque, String tipoComplejo, String nombreComplejo, String numCasilla){
        System.debug('Nombre de clase: FS_CHI_Address_Management   Nombre de método: getValidAddresses   Comienzo del método');
        BI_RestWrapper.ValidateAddressResponseType result;        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        BI_RestWrapper.ValidateAddressResponseWrapper validateAddressResponseAux;        
        
        /* WEBSERVICE CONFIGURATION */
        String endPointBase = '/addressvalidation/V1/validaddresses';
        
        //Parametros obligatorios
        String endPointMandatoryParameters = '?addressName=' + EncodingUtil.urlEncode(addressName,'UTF-8');
        endPointMandatoryParameters = endPointMandatoryParameters + '&addressNumber=' + EncodingUtil.urlEncode(addressNumber,'UTF-8');
        endPointMandatoryParameters = endPointMandatoryParameters + '&country=' + EncodingUtil.urlEncode(country,'UTF-8');
        
        //Parametros opcionales
        String endPointOptionalParameters='';       
        if (floor != '' && floor != null) 
            endPointOptionalParameters = '&floor=' + EncodingUtil.urlEncode(floor,'UTF-8');
        if (apartment != '' && apartment != null) 
            endPointOptionalParameters = endPointOptionalParameters + '&apartment=' + EncodingUtil.urlEncode(apartment,'UTF-8');
        if (area != '' && area != null) 
            endPointOptionalParameters = endPointOptionalParameters + '&area=' + EncodingUtil.urlEncode(area,'UTF-8');
        if (locality != '' && locality != null) 
            endPointOptionalParameters = endPointOptionalParameters + '&locality=' + EncodingUtil.urlEncode(locality,'UTF-8');
        if (municipality != '' && municipality != null) 
            endPointOptionalParameters = endPointOptionalParameters + '&municipality=' + EncodingUtil.urlEncode(municipality,'UTF-8');
        if (region != '' && region != null) 
            endPointOptionalParameters = endPointOptionalParameters + '&region=' + EncodingUtil.urlEncode(region,'UTF-8');
        if (postalCode != '' && postalCode != null) 
            endPointOptionalParameters = endPointOptionalParameters + '&postalCode=' + EncodingUtil.urlEncode(postalCode,'UTF-8');
        endPointOptionalParameters = endPointOptionalParameters + '&ad_isForShipment=0';
        endPointOptionalParameters = endPointOptionalParameters + '&addressType=Standard';
        
        //Nuevos Campos
        if (bloque != '' && bloque != null) 
            endPointOptionalParameters = endPointOptionalParameters + '&ad_block=' + EncodingUtil.urlEncode(bloque,'UTF-8');
        if (tipoComplejo != '' && tipoComplejo != null)
            endPointOptionalParameters = endPointOptionalParameters + '&ad_housingComplexType=' + EncodingUtil.urlEncode(tipoComplejo,'UTF-8');
        if (nombreComplejo != '' && nombreComplejo != null) 
            endPointOptionalParameters = endPointOptionalParameters + '&ad_housingComplexName=' + EncodingUtil.urlEncode(nombreComplejo,'UTF-8');
        if (numCasilla != '' && numCasilla != null) 
            endPointOptionalParameters = endPointOptionalParameters + '&ad_boxNr=' + EncodingUtil.urlEncode(numCasilla,'UTF-8');
        
        FS_CHI_Integraciones__c config_integracion = FS_CHI_Integraciones__c.getInstance();
        System.debug('config_integracion.FS_CHI_AuthorizationID__c: ' + config_integracion.FS_CHI_AuthorizationID__c + ' config_integracion.FS_CHI_ApiKey__c ' + config_integracion.FS_CHI_ApiKey__c);
        endPointBase = endPointBase + endPointMandatoryParameters + endPointOptionalParameters + '&apikey='+config_integracion.FS_CHI_ApiKey__c;
        
        //Ever01-INI
        
        request.setEndpoint('callout:FS_CHI_DataPower'+endPointBase);
        //Ever01-FIN
        request.setMethod('GET');
        request.setHeader('Authorization', config_integracion.FS_CHI_AuthorizationID__c);
        request.setTimeout(20000);         
        try{
            if(Test.isRunningTest()) response = FS_CHI_Address_Management_Mock.respond(request);
            else response = http.send(request);
            
            if(response.getStatusCode() == 200){
                /* Successfully Created */
                validateAddressResponseAux = (BI_RestWrapper.ValidateAddressResponseWrapper) JSON.deserialize(response.getBody(), BI_RestWrapper.ValidateAddressResponseWrapper.class);
                result = validateAddressResponseAux.validateAddressResponse;
            }           
        }catch(Exception exc){
            //Ever01-ini
            BI_LogHelper.generate_BILog('FS_CHI_Address_Management.getValidAddresses', 'BI_EN', exc, 'Web Service');              
            //Ever01-fin
        }
        System.debug('Nombre de clase: FS_CHI_Address_Management   Nombre de método: getValidAddresses   Finalización del método    Resultado: ' + result + 'JSON: ' + response.getBody());
        return result;
    }
}