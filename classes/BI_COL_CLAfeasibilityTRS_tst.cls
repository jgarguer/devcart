/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for class BI_COL_CLAfeasibilityTRS.
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-08-21      Raul Mora (RM)				    Create class.
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
*					20/09/2017      Angel F. Santaliestra			Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private class BI_COL_CLAfeasibilityTRS_tst 
{

	Public static User 											objUsuario = new User();
	public static List<BI_COL_Modificacion_de_Servicio__c> lstModServCreate;
	public static List<BI_COL_Viabilidad_Tecnica__c> lstViaTec;
	public static List<String> lstIdModServ;
    public static Map<String, List<BI_COL_Viabilidad_Tecnica__c>> mpLstViaTec;
	Public static list <UserRole> 									lstRoles;
	public static Contact                               objContacto;
	public static BI_Col_Ciudades__c                    objCiudad;
    public static BI_Sede__c                            objSede;
    public static BI_Punto_de_instalacion__c            objPuntosInsta;
	
	public static void createData()
	{
		//List<Profile> lstProfile = [ SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1 ];
		////lstRol
  //      lstRoles = new list <UserRole>();
  //      lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];
		//List<User> lstUser = [ Select Id FROM User where country != 'Colombia' AND ProfileId =: lstProfile[0].Id And isActive = true Limit 1 ];
		
		////ObjUsuario
  //      objUsuario = new User();
  //      objUsuario.Alias = 'standt';
  //      objUsuario.Email ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey = '';
  //      objUsuario.LastName ='Testing';
  //      objUsuario.LanguageLocaleKey ='en_US';
  //      objUsuario.LocaleSidKey ='en_US'; 
  //      objUsuario.ProfileId = lstProfile.get(0).Id;
  //      objUsuario.TimeZoneSidKey ='America/Los_Angeles';
  //      objUsuario.UserName ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey ='UTF-8';
  //      objUsuario.UserRoleId = lstRoles.get(0).Id;
  //      objUsuario.BI_Permisos__c ='Sucursales';
  //      objUsuario.Pais__c='Colombia';
  //      insert objUsuario;
		
		//System.runAs( lstUser[0] )
		//{
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass; 

			Account objAccount = new Account();      
			objAccount.Name = 'prueba';
			objAccount.BI_Country__c = 'Colombia';
			objAccount.TGS_Region__c = 'América';
			objAccount.BI_Tipo_de_identificador_fiscal__c = '';
			objAccount.CurrencyIsoCode = 'COP';
			objAccount.BI_Fraude__c = false;
			objAccount.BI_Segment__c                         = 'test';
			objAccount.BI_Subsegment_Regional__c             = 'test';
			objAccount.BI_Territory__c                       = 'test';
			insert objAccount;

			//Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
            objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objAccount.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
        	//REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
        	objContacto.FS_CORE_Referencias_Funcionales__c 			= 'Administrativo';
        	objContacto.BI_Representante_legal__c			= true;
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
            //REING_FIN
            Insert objContacto; 
			
			//Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);

            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);

            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objAccount.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name      = 'QA Erroro';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);

			Opportunity objOpp = new Opportunity();
			objOpp.Name = 'prueba opp';
			objOpp.AccountId = objAccount.Id;
			objOpp.BI_Country__c = 'Colombia';
			objOpp.CloseDate = System.today().addDays(+5);
			objOpp.StageName = 'F5 - Solution Definition';
			objOpp.CurrencyIsoCode = 'COP';
			objOpp.Certa_SCP__contract_duration_months__c = 12;
			objOpp.BI_Plazo_estimado_de_provision_dias__c = 0 ;
			//objOpp.OwnerId = lstUser[0].id;
			insert objOpp;		

			RecordType objRecTyp = [ Select Id From RecordType Where sObjectType = 'NE__Product__c' Limit 1 ];

			NE__Product__c objProd = new NE__Product__c();
			objProd.Name = Constants.PRODUCT_SMDM_INVENTORY;
			objProd.BI_COL_TipoRegistro__c = objRecTyp.Id;
	        insert objProd;

			NE__Order__c objOrder =  new NE__Order__c();
			objOrder.NE__OptyId__c = objOpp.Id; 
			objOrder.NE__OrderStatus__c='Active';
			insert objOrder;

			NE__OrderItem__c objOrdItm  = new NE__OrderItem__c();
			objOrdItm.NE__OrderId__c = objOrder.Id; 
			objOrdItm.CurrencyIsoCode = 'USD';
			objOrdItm.NE__OneTimeFeeOv__c = 100;
			objOrdItm.NE__RecurringChargeOv__c = 100;
			objOrdItm.Fecha_de_reasignaci_n_a_factura__c = date.today();
			objOrdItm.NE__Qty__c = 1;
			objOrdItm.NE__ProdId__c = objProd.Id;
			insert objOrdItm;		

			BI_COL_Descripcion_de_servicio__c objDescServ = new BI_COL_Descripcion_de_servicio__c();
			objDescServ.BI_COL_Codigo_paquete__c = '123456789';
			objDescServ.BI_COL_Producto_Telefonica__c = objOrdItm.Id;
			objDescServ.BI_COL_Estado_de_servicio__c = 'Inactiva';
			objDescServ.BI_COL_Oportunidad__c = objOpp.Id;
			insert objDescServ;	

			lstModServCreate = new List<BI_COL_Modificacion_de_Servicio__c>();

			BI_COL_Modificacion_de_Servicio__c objModServ = new BI_COL_Modificacion_de_Servicio__c();
			objModServ.BI_COL_Codigo_unico_servicio__c       = objDescServ.Id;
            objModServ.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
            objModServ.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModServ.BI_COL_Bloqueado__c                   = false;
            objModServ.BI_COL_Estado__c                      = 'Enviado';//label.BI_COL_lblActiva;
			objModServ.BI_COL_Delta_modificacion_upgrade__c = 50;
			objModServ.BI_COL_Cargo_fijo_mes__c = 100;
            objModServ.BI_COL_Producto__c = objOrdItm.Id;
            objModServ.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModServ.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
			lstModServCreate.add( objModServ );

			BI_COL_Modificacion_de_Servicio__c objModServ2 = new BI_COL_Modificacion_de_Servicio__c();
			objModServ2.BI_COL_Codigo_unico_servicio__c       = objDescServ.Id;
			objModServ2.BI_COL_Estado__c = 'Enviado';
			objModServ2.BI_COL_Clasificacion_Servicio__c = 'SERVICIO INGENIERIA';
			objModServ2.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModServ2.BI_COL_Bloqueado__c                   = false;
            objModServ2.BI_COL_Estado__c                      = 'Enviado';//label.BI_COL_lblActiva;
			objModServ2.BI_COL_Delta_modificacion_upgrade__c = 50;
			objModServ2.BI_COL_Cargo_fijo_mes__c = 100;
            objModServ2.BI_COL_Producto__c = objOrdItm.Id;
            objModServ2.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModServ2.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
			lstModServCreate.add( objModServ2 );		

			BI_COL_Modificacion_de_Servicio__c objModServ3 = new BI_COL_Modificacion_de_Servicio__c();
			objModServ3.BI_COL_Codigo_unico_servicio__c       = objDescServ.Id;
			objModServ3.BI_COL_Estado__c = 'Enviado';
			objModServ3.BI_COL_Clasificacion_Servicio__c = 'ALTA POR NORMALIZACION';
			objModServ3.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModServ3.BI_COL_Bloqueado__c                   = false;
            objModServ3.BI_COL_Estado__c                      = 'Enviado';//label.BI_COL_lblActiva;
			objModServ3.BI_COL_Delta_modificacion_upgrade__c = 50;
			objModServ3.BI_COL_Cargo_fijo_mes__c = 100;
            objModServ3.BI_COL_Producto__c = objOrdItm.Id;
            objModServ3.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModServ3.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
			lstModServCreate.add( objModServ3 );			

			BI_COL_Modificacion_de_Servicio__c objModServ4 = new BI_COL_Modificacion_de_Servicio__c();
			objModServ4.BI_COL_Codigo_unico_servicio__c       = objDescServ.Id;
			objModServ4.BI_COL_Estado__c = 'Enviado';
			objModServ4.BI_COL_Clasificacion_Servicio__c = 'SUSPENSION TEMPORAL';
			objModServ4.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModServ4.BI_COL_Bloqueado__c                   = false;
            objModServ4.BI_COL_Estado__c                      = 'Enviado';//label.BI_COL_lblActiva;
			objModServ4.BI_COL_Delta_modificacion_upgrade__c = 50;
			objModServ4.BI_COL_Cargo_fijo_mes__c = 100;
            objModServ4.BI_COL_Producto__c = objOrdItm.Id;
            objModServ4.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModServ4.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
			lstModServCreate.add( objModServ4 );	

			insert lstModServCreate;

			lstViaTec = new List<BI_COL_Viabilidad_Tecnica__c>();

			BI_COL_Viabilidad_Tecnica__c objViabilidad = new BI_COL_Viabilidad_Tecnica__c();
			objViabilidad.BI_COL_Modificacion_de_Servicio__c = objModServ.Id;
			objViabilidad.BI_COL_Estado__c = 'Solicitado';
			lstViaTec.add( objViabilidad );

			BI_COL_Viabilidad_Tecnica__c objViabilidad2 = new BI_COL_Viabilidad_Tecnica__c();
			objViabilidad2.BI_COL_Modificacion_de_Servicio__c = objModServ.Id;
			objViabilidad2.BI_COL_Estado__c = 'Solicitado';
			lstViaTec.add( objViabilidad2 );

			BI_COL_Viabilidad_Tecnica__c objViabilidad3 = new BI_COL_Viabilidad_Tecnica__c();
			objViabilidad3.BI_COL_Modificacion_de_Servicio__c = objModServ2.Id;
			objViabilidad3.BI_COL_Estado__c = 'Solicitado';
			lstViaTec.add( objViabilidad3 );

			BI_COL_Viabilidad_Tecnica__c objViabilidad4 = new BI_COL_Viabilidad_Tecnica__c();
			objViabilidad4.BI_COL_Modificacion_de_Servicio__c = objModServ3.Id;
			objViabilidad4.BI_COL_Estado__c = 'Solicitado';
			lstViaTec.add( objViabilidad4 );

			BI_COL_Viabilidad_Tecnica__c objViabilidad5 = new BI_COL_Viabilidad_Tecnica__c();
			objViabilidad5.BI_COL_Modificacion_de_Servicio__c = objModServ4.Id;
			objViabilidad5.BI_COL_Estado__c = 'Solicitado';
			lstViaTec.add( objViabilidad5 );

			insert lstViaTec;

			lstIdModServ = new List<String>();
	        mpLstViaTec = new Map<String, List<BI_COL_Viabilidad_Tecnica__c>>();

	        List<BI_COL_Viabilidad_Tecnica__c> lstFirstVia = new List<BI_COL_Viabilidad_Tecnica__c>();
	        lstFirstVia.add( objViabilidad );
	        lstIdModServ.add( objModServ.Id );
	        lstFirstVia.add( objViabilidad2 );
			mpLstViaTec.put( objModServ.Id, lstFirstVia );
			List<BI_COL_Viabilidad_Tecnica__c> lstFirstVia2 = new List<BI_COL_Viabilidad_Tecnica__c>();
			lstFirstVia2.add( objViabilidad3 );
			lstIdModServ.add( objModServ2.Id );
			mpLstViaTec.put( objModServ2.Id, lstFirstVia2 );
			List<BI_COL_Viabilidad_Tecnica__c> lstFirstVia3 = new List<BI_COL_Viabilidad_Tecnica__c>();
			lstFirstVia3.add( objViabilidad4 );
			lstIdModServ.add( objModServ3.Id );
			mpLstViaTec.put( objModServ3.Id, lstFirstVia3 );
			List<BI_COL_Viabilidad_Tecnica__c> lstFirstVia4 = new List<BI_COL_Viabilidad_Tecnica__c>();
			lstFirstVia4.add( objViabilidad5 );
			lstIdModServ.add( objModServ4.Id );
			mpLstViaTec.put( objModServ4.Id, lstFirstVia4 );	        
		//}
	}

    static testMethod void myUnitTest() 
    {
    	objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
        	createData();

        	System.debug('\n\n .-.-.-. lstIdModServ.get(0) '+lstIdModServ.get(0)+'\n\n -.-.-. mpLstViaTec.get() '+mpLstViaTec.get( lstIdModServ.get(0) )); 

        	Test.startTest();
        		BI_COL_CLAfeasibilityTRS.iniciarBitacoraViabilidad( lstIdModServ, mpLstViaTec );
        	Test.stopTest();
        }
    }
}