public with sharing class BI_DocumentacionProyectos_CTRL {

    public class ConfigurationWrapper{
        public boolean mandatory {get; set;}
        public string nameConfDocument {get; set;}
        public boolean existDocument  {get; set;}
        public string documentId  {get; set;}
    }

    public Milestone1_Project__c project;
    public list<BI_Configuracion_de_documentacion__c> listConfDocument;
    public list<ConfigurationWrapper> listConfigurationWrapper {get; set;}
    public list<ConfigurationWrapper> listConfigurationWrapper2 {get; set;}
    public map<string, string> mapDescriptionId;
    
    public string projectId {get; set;}
    public string urlAtt {get; set;}
    public string ajaxParameter {get; set;}
    
    public void generateURL (){
        urlAtt = '{!URLFOR($Action.Attachment.Download,' + ajaxParameter + ')}';
    }

    public BI_DocumentacionProyectos_CTRL (ApexPages.StandardController stdController){
        project = (Milestone1_Project__c)stdController.getRecord();
        listConfigurationWrapper = new list<ConfigurationWrapper>();
        listConfigurationWrapper2 = new list<ConfigurationWrapper>();
        mapDescriptionId = new map<string, string>();
        if(project != null){
            project = [Select Id, BI_Country__c, BI_Complejidad_del_proyecto__c, BI_Etapa_del_proyecto__c From Milestone1_Project__c where id = : project.Id];
            projectId = project.Id;

            String queryStr = 'Select Id, BI_Nombre_del_documento__c, BI_Objeto__c, BI_Obligatoriedad__c, BI_Country__c, BI_Etapa_del_proyecto__c, BI_Complejidad_del_proyecto__c';
            queryStr = queryStr + ' From BI_Configuracion_de_documentacion__c'; 
            queryStr = queryStr + ' where BI_Objeto__c = \'Proyecto\' and BI_Country__c = \'' +  project.BI_Country__c + '\'';
            queryStr = queryStr + ' and BI_Complejidad_del_proyecto__c = \'' +  project.BI_Complejidad_del_proyecto__c + '\'';
            queryStr = queryStr + ' and BI_Etapa_del_proyecto__c INCLUDES (\'' + project.BI_Etapa_del_proyecto__c +  '\')';
system.debug(queryStr);
            listConfDocument = Database.query(queryStr);

            /*listConfDocument = [Select Id, BI_Nombre_del_documento__c, BI_Objeto__c, BI_Obligatoriedad__c, BI_Country__c, BI_Etapa_del_proyecto__c, BI_Complejidad_del_proyecto__c
                            From BI_Configuracion_de_documentacion__c 
                            where   BI_Country__c = : project.BI_Country__c and 
                                    BI_Complejidad_del_proyecto__c = :project.BI_Complejidad_del_proyecto__c and
                                    BI_Etapa_del_proyecto__c INCLUDES (project.BI_Etapa_del_proyecto__c)]; 
                                    //BI_Etapa_del_proyecto__c= : project.BI_Etapa_del_proyecto__c];*/
            if(!listConfDocument.isEmpty()){
                set<string> setDescriptionAttachment = new set<string>();
                for(BI_Configuracion_de_documentacion__c item: listConfDocument){
                    setDescriptionAttachment.add(item.BI_Nombre_del_documento__c);
                }
                list<Attachment> listAttachmentAlreayInsert = [Select Id, Description, CreatedDate From Attachment 
                                                                where parentId = : project.Id and Description IN : setDescriptionAttachment order by CreatedDate desc];
                if(!listAttachmentAlreayInsert.isEmpty()){
                    //Relleno el mapa con la descripción y el Id de cada attachment insertando unicamente el más actualizado por Descripción por eso se ordena la query por la fecha de creación
                    for(Attachment item : listAttachmentAlreayInsert){
                        if(!mapDescriptionId.containsKey(item.Description)){
                            mapDescriptionId.put(item.Description, item.Id);
                        }
                    }
                }
                integer position = 0;
                for(BI_Configuracion_de_documentacion__c item: listConfDocument){
                    ConfigurationWrapper configurationWrapperAux = new ConfigurationWrapper();
                    configurationWrapperAux.mandatory = item.BI_Obligatoriedad__c == Label.BI_No ? false : true; //boolean.valueOf(item.BI_Obligatoriedad__c);
                    configurationWrapperAux.nameConfDocument = item.BI_Nombre_del_documento__c;
                    configurationWrapperAux.existDocument = mapDescriptionId.containsKey(item.BI_Nombre_del_documento__c);
                    configurationWrapperAux.documentId = configurationWrapperAux.existDocument == true ? mapDescriptionId.get(item.BI_Nombre_del_documento__c) : null;
                    //listConfigurationWrapper.add(configurationWrapperAux);
                    if(position == 0){
                        listConfigurationWrapper.add(configurationWrapperAux);
                        position = 1;
                    }else if(position == 1){
                        listConfigurationWrapper2.add(configurationWrapperAux);
                        position = 0;
                    }
                }
            }
        }
    }
    
    
    public pageReference openPage(){
        system.debug(ajaxParameter);
        Pagereference pageNew = new Pagereference('/apex/BI_ProjectAttachment2?id='+projectId+'&description='+ajaxParameter);
        return pageNew;
    }
    
}