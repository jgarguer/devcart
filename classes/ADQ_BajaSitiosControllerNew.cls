public  class ADQ_BajaSitiosControllerNew {
	/**********************Datos encabezado origen***************************************/
	//NE__OrderItem__c
	public String idEncabezado; 
	public List<NE__OrderItem__c> listaProductosOrigen = new List<NE__OrderItem__c>();
	public List<WrapperProductos> listaWrapperProductosOrigen = new List<WrapperProductos>();
	public List<Programacion__c> listaProgramacionesOrigen = new List<Programacion__c>();
	public Factura__c encabezadoOrigen = new Factura__c();

	/**********************Datos encabezado destino***************************************/ 
	public List<NE__OrderItem__c> listaProductosDestino = new List<NE__OrderItem__c>();
	public List<WrapperProductos> listaWrapperProductosDestino = new List<WrapperProductos>();
	public Factura__c encabezadoDestino = new Factura__c();
 
	/**********************Listas de los productos seleccionados para reasiganr***************************************/
	public List<NE__OrderItem__c> listaProductosReasignar = new List<NE__OrderItem__c>();
	public List<NE__OrderItem__c> listaProductosUnicosReasignar = new List<NE__OrderItem__c>();
	
	/**********************Variables de selecci?n y listas de combos***************************************/
	
	public List<Factura__c> listaEncabezadosCuenta = new List<Factura__c>();
	public List<Programacion__c> listaProgramaciones = new List<Programacion__c>();
	public String encabezadoSeleccionado {get; set;}
	public String sitioSeleccionado {get; set;}
	public String programacionSeleccionada {get; set;}
	
	/**********************Variables para el control de la p?gina***************************************/
	public Boolean showComboProgramaciones; 
	public Boolean disabledBotonReasignar {get; set;}
	public Boolean blockDestinoActivado{get;set;}
	public Boolean tablaDestinoActivado{get;set;}
	
	public Boolean showComboPeriodos{get; set;}
	public NE__OrderItem__c productoSeleccionado;
	public String periodoSeleccionado {get;set;}
	ADQ_DivisaUtility cambiaDivisa = new ADQ_DivisaUtility();
	
	//Modificado para IOD
	public static String FACTURA_CARGO_UNICO = '';
	public static String FACTURA_RECURRENTE = '';
	
	
	//>>>>private List<NE__OrderItem__c> listaServiciosOnDemand;
	
	public ADQ_BajaSitiosControllerNew(){ 

		for (RecordType tiporegistro : [SELECT BusinessProcessId,Id,Name,NamespacePrefix,SobjectType 
											FROM RecordType Where SobjectType  = 'Factura__c' 
											 ]){
				if (tiporegistro.Name == 'Recurrente'){
					FACTURA_RECURRENTE = tiporegistro.Id ;	
				}
				if (tiporegistro.Name == 'Cargo único'){
					FACTURA_CARGO_UNICO = tiporegistro.Id ;	
				}

		}

		for(Factura__c fac001 : [ SELECT Id,Cliente__r.Name FROM Factura__c where Cliente__r.NAme = 'CUENTAS ELIMINADAS' limit 1]){
			encabezadoSeleccionado = fac001.Id;
		}
		//encabezadoSeleccionado = 'a2h11000000EBiMAAW';	
		periodoSeleccionado = '2010-06-01 00:00:00';	


		showComboProgramaciones = false;
		tablaDestinoActivado = false;
		idEncabezado = ApexPages.currentPage().getParameters().get('fid');
		encabezadoDestino = null;
		cargaEncabezados();
	}
	
	public void cargaEncabezados(){
		listaEncabezadosCuenta = new List<Factura__c>();
		if(idEncabezado != null && idEncabezado != ''){
			generaEncabezadoOrigen();
			/*
			listaEncabezadosCuenta = [SELECT Id, Name, Cliente__r.Name, Activo__c, IVA__c, CurrencyIsoCode, Tipo_de_Factura__c, Fecha_Inicio__c, Fecha_Fin__c 
			FROM Factura__c 
			WHERE Cliente__c=:encabezadoOrigen.Cliente__c 
				AND Id!=:encabezadoOrigen.Id 
				AND IVA__c =:encabezadoOrigen.IVA__c
				AND Activo__c = true 
				AND RecordTypeId !=:FACTURA_CARGO_UNICO
				AND RecordTypeId =:encabezadoOrigen.RecordTypeId
				AND CreatedDate >= : Datetime.newInstance(2009,10,7,0,0,0)];
				*/

			listaEncabezadosCuenta = [SELECT Id, Name, Cliente__r.Name, Activo__c, IVA__c, CurrencyIsoCode, Tipo_de_Factura__c, Fecha_Inicio__c, Fecha_Fin__c 
			FROM Factura__c 
			WHERE Id=: encabezadoSeleccionado ];

		}else ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,  'Seleccione un encabezado de origen'));
		if(listaEncabezadosCuenta.size() > 0){
			generaEncabezadoDestino();
		}
		else ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,  'No existen más encabezados de facturación compatibles con el actual.'));
	}	
	

	public void generaEncabezadoOrigen(){
		listaProductosOrigen.clear();
		listaWrapperProductosOrigen.clear();
		encabezadoOrigen = [SELECT f.Id, f.Name, f.Cliente__r.Name, f.Activo__c, f.IVA__c, f.RecordTypeId From Factura__c f where f.Id=:idEncabezado];
		if(encabezadoOrigen.Activo__c !=false){
			
			//JG cambiado referencias en la query a BI_Tipo_de_oferta_Per__r y BI_Pais__c
			//GMN 13/06/2017 - Deprecated references to Raz_n_social__c (Optimización)
			listaProductosOrigen = [SELECT Id ,NE__Qty__c, NE__OneTimeFeeOv__c, NE__Account__r.Id, NE__ProdId__c,NE__ProdId__r.Id, NE__ProdId__r.Name, NE__OrderId__r.Id, NE__OrderId__r.NE__OptyId__c,
											NE__OrderId__r.NE__OptyId__r.BI_Full_contract_value_neto_FCV__c, NE__OrderId__r.NE__OptyId__r.BI_Duracion_del_contrato_Meses__c,
											NE__RecurringChargeOv__c,Name/*,NE__Account__r.Raz_n_social__c*/, NE__Account__r.BI_No_Identificador_fiscal__c,Installation_point__c,
											Installation_point__r.id, Factura__c,
											NE__OrderId__r.NE__OptyId__r.BI_Opportunity_Type__c,NE__OrderId__r.NE__OptyId__r.BI_Country__c,
											NE__OrderId__r.NE__OptyId__r.Probability , NE__OrderId__r.Name, NE__OrderId__r.NE__OptyId__r.Name , Oportunidad_Name__c  , 
											Installation_point__r.Name,NE__OrderId__r.NE__OptyId__r.CurrencyIsoCode 
										FROM NE__OrderItem__c
										WHERE Factura__c =:idEncabezado 
											AND Id NOT IN 
												(SELECT Producto_en_Sitio__c 
												FROM Pedido_PE__c 
												WHERE Programacion__r.Factura__c=:idEncabezado 
												AND (Programacion__r.Exportado__c = true
														OR Programacion__r.Facturado__c = true) 
												AND Producto_en_Sitio__r.NE__OrderId__r.NE__OptyId__r.BI_Duracion_del_contrato_Meses__c = 1)];

			System.debug('>>> listaProductosOrigen: ' + listaProductosOrigen);
			if(listaProductosOrigen.size()>0){
				for(NE__OrderItem__c s : listaProductosOrigen){
						listaWrapperProductosOrigen.add(new WrapperProductos(s, false));
				}
			}
			system.debug('>>>> listaWrapperProductosOrigen :' + listaWrapperProductosOrigen );
			if(listaWrapperProductosOrigen.size()>0){
				disabledBotonReasignar = false;
				blockDestinoActivado = true;
			}
			else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,  'No existen productos para reasignar'));
				disabledBotonReasignar = true;
				blockDestinoActivado = false;
			}
			listaProgramacionesOrigen = [Select Id, Name, Inicio_del_per_odo__c, Fin_del_per_odo__c, CurrencyIsoCode from Programacion__c where Numero_de_factura_SD__c = '' and Factura__c =:encabezadoOrigen.Id and Miscelanea__c = false order by Inicio_del_per_odo__c];
			
		}
		else{ 
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Este encabezado de factura no esta activo, no es posible reasignar productos.'));
			disabledBotonReasignar = true;
			blockDestinoActivado = false;
		}
		filtroSitios ();
	}

	public void generaEncabezadoDestino(){
		listaWrapperProductosDestino.clear();
		listaProgramaciones.clear();

		if(encabezadoSeleccionado != null && encabezadoSeleccionado != ''){
			for(Factura__c e : listaEncabezadosCuenta){
				if(encabezadoSeleccionado == e.Id){
					encabezadoDestino = e; 
				}
			}
			/*
			listaProgramaciones = [Select Id, Name, Inicio_del_per_odo__c, Fin_del_per_odo__c, CurrencyIsoCode 
										from Programacion__c 
										where Numero_de_factura_SD__c = '' and Factura__c =:encabezadoDestino.Id 
										and Miscelanea__c = false order by Inicio_del_per_odo__c];

			listaProductosDestino = [Select Id, Name, NE__OrderId__r.Opty_Name__c , NE__ProdId__r.Name, Oportunidad_Name__c
										From NE__OrderItem__c where Factura__c=:encabezadoDestino.Id];
			for(NE__OrderItem__c s : listaProductosDestino){
				listaWrapperProductosDestino.add(new WrapperProductos(s, false));
			}
			*/
			tablaDestinoActivado = true;
		}else{
			encabezadoDestino = null;
			listaProductosDestino.clear();
			listaWrapperProductosDestino.clear();
			tablaDestinoActivado = false;

		}
	}
	
	
	public void filtroSitios(){
		//listaWrapperProductosOrigen.clear();
		
			for(NE__OrderItem__c s : listaProductosOrigen){

				if(sitioSeleccionado == s.Installation_point__c){
					//listaWrapperProductosOrigen.add(new WrapperProductos(s, false));
				}
			}
		
	}
	
	public void refresh(){
		cargaEncabezados();
		filtroSitios();
	}
	
	public void reasignaEquipo(){
		if(validaEncabezadoDestino() && guardaSeleccion()){
			system.debug('>>>> listaProductosReasignar: '+ listaProductosReasignar);
			if(listaProductosReasignar.size()>0){
				reasignaProductosRecurrentes();
			}
			system.debug('>>>> listaProductosUnicosReasignar: '+ listaProductosUnicosReasignar);
			if(listaProductosUnicosReasignar.size()>0){
				guardaCargosUnicos();
			}

			refresh();
			system.debug('>>>> listaProductosOrigen: '+ listaProductosOrigen);
			if(listaProductosOrigen.size()==0){
				encabezadoOrigen.Activo__c = false;
				update encabezadoOrigen;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,  'El encabezado de facturación se ha desactivado, ya que se ha quedado sin productos.'));
			}else validaEncabezadoRecurrente();
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,  'Los productos se asignaron correctamente'));
		}
	}
	
	public Boolean guardaSeleccion(){
		//Para la reasignaci?n de servicios On demand
	//>>>>	listaServiciosOnDemand = new List<NE__OrderItem__c>();
		
		Boolean valido = false;
		listaProductosReasignar.clear();
		listaProductosUnicosReasignar.clear();
		for(WrapperProductos wp : listaWrapperProductosOrigen){
			if(Test.isRunningTest()){
				valido = true;
				listaProductosUnicosReasignar.add(wp.equipoSitio);
				listaProductosReasignar.add(wp.equipoSitio);
			}
			if(wp.selected == true){
				if(wp.equipoSitio.NE__OrderId__r.NE__OptyId__r.BI_Duracion_del_contrato_Meses__c == 1){
					listaProductosUnicosReasignar.add(wp.equipoSitio);
				}
			
					listaProductosReasignar.add(wp.equipoSitio);
				
				//Para los servicios On Demand
				/*>>>>
				else if(wp.equipoSitio.Equipo__r.EsOnDemand__c){
					listaServiciosOnDemand.add(wp.equipoSitio);
				}
				*/
				valido = true;
			}
		} 
		if(!valido) ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,  'Debe seleccionar al menos un producto.'));
		else if(listaProductosUnicosReasignar.size()>0 && valido && (programacionSeleccionada == 'Ninguno' || programacionSeleccionada == null)){
			valido = false;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,  'Debe seleccionar manualmente una programación para la reasignación de productos únicos.'));
		}
		return valido;
	}
	
	
	
	public void reasignaProductosRecurrentes(){
		List<Facturas_SenS__c> listaFacturasSensInsert = new List<Facturas_SenS__c>();
		List<Facturas_SenS__c> listaFacturasSensUpdate;
		List<Pedido_PE__c> listaPedidosPrimeraFac = new List<Pedido_PE__c>();
		Programacion__c progCargaPrimeraFac;
		Set<String> idsProductos = new Set<String>();
		Date periodoInicio = Date.valueOf(periodoSeleccionado);
		
		
		for(NE__OrderItem__c lpr: listaProductosReasignar){
			lpr.Factura__c = encabezadoDestino.Id; 								//Se actualiza la referencia a la factura que pertenece ahora el producto
			lpr.Fecha_de_reasignaci_n_a_factura__c = periodoInicio; 			//Se actualiza la fecha de reasignación sobre el producto
			idsProductos.add(lpr.Id);											//Se guardan los ids de los productos a reasignar
			listaFacturasSensInsert.add(new Facturas_SenS__c(Equipos_en_Sitio__c = lpr.Id, Factura__c = encabezadoDestino.Id, Fecha_de_inicio__c = periodoInicio)); //Se crea la lista de los registros Factura_Sens a insertar, cada cambio de producto implica guardar un registro de la fecha de este cambio
		}
		//Se consulta los registros de FActura_Sens para actualizar el campo cuando el producto dejo de estar en esa factura
		listaFacturasSensUpdate = [SELECT Equipos_en_Sitio__c, Factura__c, Fecha_de_inicio__c, Fecha_de_fin__c 
									FROM Facturas_SenS__c 
									WHERE Equipos_en_Sitio__c IN :idsProductos 
									AND Factura__c =: encabezadoOrigen.Id 
									AND Fecha_de_fin__c = null];
		
		//Se cunsulta si de los productos a reasignar alguno o algunos cuentan con un pedido de 'Primera Factura' sin facturar
		listaPedidosPrimeraFac = [SELECT Id, Programacion__c, Precio_original__c, Precio_convertido__c, CurrencyIsoCode
									FROM Pedido_PE__c
									WHERE Producto_en_Sitio__c IN :idsProductos
									AND Concepto__c = 'Primera factura'
									AND Programacion__r.Numero_de_factura_SD__c = NULL];
		
		//Si existen pedidos de primera factura sin facturar
		if(listaPedidosPrimeraFac.size() > 0){
			//Se consuta si existe una programacion en el encabezado para el periodo seleccionado
			for(Programacion__c prog : listaProgramaciones){
				if(prog.Inicio_del_per_odo__c <= periodoInicio && periodoInicio <= prog.Fin_del_per_odo__c){
					progCargaPrimeraFac = prog;
				}
			}
			//Si ya existe la programacion se carga a esta los pedidos de Primera factura 
			// A estos pedidos se les actualiza a la moneda a la de la programación a la que pertenecen ahora
			if(progCargaPrimeraFac != null){
				for(Pedido_PE__c pe:listaPedidosPrimeraFac){
					pe.Programacion__c = progCargaPrimeraFac.Id;
					pe.Precio_convertido__c = cambiaDivisa.round(cambiaDivisa.transformCurrency(pe.CurrencyIsoCode, progCargaPrimeraFac.CurrencyIsoCode, Double.valueOf(''+pe.Precio_original__c)),2);
					pe.Moneda_de_conversi_n__c = progCargaPrimeraFac.CurrencyIsoCode;
					pe.Lleva_IEPS__c = false;
					pe.IEPS__c = (pe.Lleva_IEPS__c)? pe.Precio_convertido__c*0.03:0;
				}
				update listaPedidosPrimeraFac;
			}
			//Si no existe una programacion en el encabezado destino se crea una nueva
			else{
				Programacion__c nuevaProgramacion = new Programacion__c();
				Double importeProductos = 0; 
				Double ieps = 0;
				nuevaProgramacion.Inicio_del_per_odo__c = periodoInicio;
				nuevaProgramacion.Fin_del_per_odo__c = periodoInicio.addMonths(1).addDays(-1);
				nuevaProgramacion.Fecha_de_inicio_de_cobro__c = (encabezadoDestino.Tipo_de_Factura__c == 'Anticipado')? periodoInicio:periodoInicio.addMonths(1);
				nuevaProgramacion.Factura__c = encabezadoDestino.Id;
				nuevaProgramacion.CurrencyIsoCode = encabezadoDestino.CurrencyIsoCode;
				insert nuevaProgramacion;
				
				//Se cargan los pedidos de Primera Factura a la nueva programación
				for(Pedido_PE__c pe:listaPedidosPrimeraFac){
					pe.Programacion__c = nuevaProgramacion.Id;
					pe.Precio_convertido__c = cambiaDivisa.round(cambiaDivisa.transformCurrency(pe.CurrencyIsoCode, nuevaProgramacion.CurrencyIsoCode, Double.valueOf(''+pe.Precio_original__c)),2);
					pe.Moneda_de_conversi_n__c = nuevaProgramacion.CurrencyIsoCode;
					pe.Lleva_IEPS__c = false;
					pe.IEPS__c = (pe.Lleva_IEPS__c)? pe.Precio_convertido__c*0.03:0;
					ieps += pe.IEPS__c;
					importeProductos += pe.Precio_convertido__c;
				}
				//Se actualiza el importe de la nueva programación
				nuevaProgramacion.Importe__c = importeProductos;

				system.debug('>>>> listaPedidosPrimeraFac: ' + listaPedidosPrimeraFac);
				system.debug('>>>> nuevaProgramacion: ' + nuevaProgramacion);
				update listaPedidosPrimeraFac;
				update nuevaProgramacion;
			}
		}
		
		//Se actualizan los registros de Fecha fin de la lista de Facturas_Sens a actualizar
		if(listaFacturasSensUpdate.size()>0){
			for(Facturas_SenS__c fs : listaFacturasSensUpdate){
				fs.Fecha_de_fin__c = periodoInicio.addDays(-1);
			}
			update listaFacturasSensUpdate;
		}
		// se reasigna el producto y se crean los nuevos registros de factura_sens
		update listaProductosReasignar;
		insert listaFacturasSensInsert;
	}
	
		
		
	public void validaEncabezadoRecurrente(){
		Boolean isRecurrente = false;
		for(NE__OrderItem__c s : listaProductosOrigen){
			if(s.NE__OrderId__r.NE__OptyId__r.BI_Duracion_del_contrato_Meses__c != 1){
				isRecurrente = true;
			}
		}
		if(!isRecurrente && FACTURA_CARGO_UNICO != ''){
			encabezadoOrigen.RecordTypeId = FACTURA_CARGO_UNICO;
			update encabezadoOrigen;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,  'El tipo de registro del encabezado se ha cambiado a cargo único, ya que el encabezado cuenta solo con productos de este tipo.'));
		} 
	}
	
	public void guardaCargosUnicos(){
		List<Pedido_PE__c> listaPedidos = new List<Pedido_PE__c>();
		List<Pedido_PE__c> listaPedidosBorrar = new List<Pedido_PE__c>();
		Set<String> setIdsSens = new Set<String>();
		Programacion__c programacion = new Programacion__c ();
		for (Programacion__c progra01 : [Select Id, Name, Inicio_del_per_odo__c, Fin_del_per_odo__c, CurrencyIsoCode From Programacion__c where Id =:programacionSeleccionada limit 1]){
			programacion = progra01;
		}
		for(NE__OrderItem__c pu: listaProductosUnicosReasignar){
			try {
				pu.Factura__c = encabezadoDestino.Id;
				pu.Fecha_de_reasignaci_n_a_factura__c = date.today();
				ADQ_DivisaUtility cambiaDivisa = new ADQ_DivisaUtility();
				Double precioConvertido = cambiaDivisa.round(cambiaDivisa.transformCurrency(pu.CurrencyIsoCode, programacion.CurrencyIsoCode, Double.valueOf(pu.NE__OneTimeFeeOv__c ) + Double.valueOf(pu.NE__RecurringChargeOv__c )),2);
				Pedido_PE__c pe = new Pedido_PE__c();
				pe.Concepto__c = 'Cargo único';
				pe.Id_sitio_original__c = pu.Installation_point__c;
				pe.Producto_en_Sitio__c = pu.Id;
				pe.Programacion__c = programacion.Id;
				pe.CurrencyIsoCode = pu.CurrencyIsoCode;
				pe.Moneda_de_conversi_n__c = programacion.CurrencyIsoCode;
				pe.Precio_original__c = pu.NE__OneTimeFeeOv__c + pu.NE__RecurringChargeOv__c;
				pe.Precio_convertido__c = precioConvertido;
				pe.Inicio_del_per_odo__c = programacion.Inicio_del_per_odo__c;
				pe.Fin_del_per_odo__c = (programacion.Inicio_del_per_odo__c).addMonths(1).toStartOfMonth().addDays(-1);
				pe.Lleva_IEPS__c = false;
				pe.IEPS__c = (pe.Lleva_IEPS__c)? pe.Precio_convertido__c*0.03:0;
				
				listaPedidos.add(pe);
				setIdsSens.add(pu.Id);
			}catch (Exception err){
				
			}
			
		}
		listaPedidosBorrar = [Select Id From Pedido_PE__c where Producto_en_Sitio__c IN: setIdsSens AND Programacion__r.Factura__c =:encabezadoOrigen.Id];
		
		update listaProductosUnicosReasignar;
		delete listaPedidosBorrar;
		insert listaPedidos;
	}

	public Boolean validaEncabezadoDestino(){
		Boolean valido = false;
		if(encabezadoSeleccionado != null && encabezadoDestino!= null && encabezadoDestino.Activo__c == true){
			valido=true;
		}else ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Debe seleccionar un encabezado de destino válido.'));
		return valido;	
	}
	
	public void existenCargosUnicos(){
		showComboProgramaciones = false;
		showComboPeriodos = false;
		//Se agrega validación del encabezado On Demand
		if(encabezadoOrigen.RecordTypeId == FACTURA_RECURRENTE || encabezadoOrigen.RecordTypeId == FACTURA_CARGO_UNICO){
			for(WrapperProductos wp : listaWrapperProductosOrigen){
				if(wp.selected == true && wp.equipoSitio.NE__OrderId__r.NE__OptyId__r.BI_Duracion_del_contrato_Meses__c == 1){
					showComboProgramaciones = true;
				}
				if(wp.selected == true && (wp.equipoSitio.NE__OrderId__r.NE__OptyId__r.BI_Duracion_del_contrato_Meses__c != 1 || wp.equipoSitio.NE__OrderId__r.NE__OptyId__r.BI_Duracion_del_contrato_Meses__c == null)){
					showComboPeriodos = true; 
					productoSeleccionado = wp.equipoSitio;
				}
			}
		}
	} 

	public PageReference cancelar(){
    	return new Apexpages.Pagereference('/' + idEncabezado);
    }
	
	public Factura__c getEncabezadoOrigen(){
		return encabezadoOrigen;
	}
	
	public Factura__c getEncabezadoDestino(){
		return encabezadoDestino;
	}
	
	public List<SelectOption> getListaEncabezadosCuentas(){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','-Ninguno-'));
		for(Factura__c fac : listaEncabezadosCuenta){
			options.add(new SelectOption(fac.Id,fac.Name));
		}
      	return options;
	}
	
	public List<SelectOption> getListaSitios(){
		List<SelectOption> options = new List<SelectOption>();
		Map<String, String> mapSitios = new Map<String, String>();
		
		options.add(new SelectOption('-Todos-','-Todos-'));
		
		for(NE__OrderItem__c sens : listaProductosOrigen){
			if (sens.Installation_point__c != null){
				mapSitios.put(sens.Installation_point__c, sens.Installation_point__r.Name);
			}
		}
		
		for(String SitioId : mapSitios.keySet()){
			options.add(new SelectOption(SitioId,mapSitios.get(SitioId)));
		}
		
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,  ''+  mapSitios.keySet() + mapSitios.values()));
      	return options;
	}
	
	public List<SelectOption> getListaProgramaciones(){
		List<SelectOption> programaciones = new List<SelectOption>();
		
		programaciones.add(new SelectOption('Ninguno', 'Ninguno'));
		for(Programacion__c pro : listaProgramaciones){
			String etiqueta = pro.Name + ' (' + (String.valueOf(pro.Inicio_del_per_odo__c)).split(' ')[0] + ' - ' + (String.valueOf(pro.Fin_del_per_odo__c)).split(' ')[0] + ')';
			programaciones.add(new SelectOption(pro.Id, etiqueta));
		}
 	 	return programaciones;
	}
	
	public List<SelectOption> getListaPeriodos(){
		List<SelectOption> listaMeses = new List<SelectOption>();
		List<String> meses = new String[13]; 
		meses[0] = '';
		meses[1] = 'Enero';
		meses[2] = 'Febrero';
		meses[3] = 'Marzo';
		meses[4] = 'Abril';
		meses[5] = 'Mayo';
		meses[6] = 'Junio';
		meses[7] = 'Julio';
		meses[8] = 'Agosto';
		meses[9] = 'Septiembre';
		meses[10] = 'Octubre';
		meses[11] = 'Noviembre';
		meses[12] = 'Diciembre';
		
		String month;
		String year;
		
		Date fechaInicioPeriodos;
		Date fechaUltimaProgramacionExpOrigen;
		Date fechaUltimaProgramacionExpDestino;
		if(encabezadoDestino != null){
			
				for (Programacion__c porgra01 : [SELECT Fin_del_per_odo__c FROM Programacion__c
											WHERE Numero_de_factura_SD__c != '' 
											AND Numero_de_factura_SD__c != NULL 
											AND Factura__c = :encabezadoDestino.id 
											AND Miscelanea__c = false 
											ORDER BY Fin_del_per_odo__c desc limit 1]){
					fechaUltimaProgramacionExpDestino = porgra01.Fin_del_per_odo__c;
				}						
				 
				for (Programacion__c porgra02 : [SELECT Fin_del_per_odo__c FROM Programacion__c
											WHERE Numero_de_factura_SD__c != '' 
											AND Numero_de_factura_SD__c != NULL 
											AND Factura__c = :encabezadoOrigen.id 
											AND Miscelanea__c = false 
											//AND Fin_del_per_odo__c >:Date.today().toStartOfMonth()
											ORDER BY Fin_del_per_odo__c desc limit 1]){
					fechaUltimaProgramacionExpOrigen = porgra02.Fin_del_per_odo__c;
				}			 
				

			/*En el caso de que ambos encabezados tengan una programación facturada, 
			se podrá agregar el producto apartir de la siguiente programación no facturada del encabezado destino, 
			sólo si esta no es anterior al período de la última programación facturada del encabezado origen.*/
			if(fechaUltimaProgramacionExpDestino != null && fechaUltimaProgramacionExpOrigen != null){
				fechaInicioPeriodos = (fechaUltimaProgramacionExpDestino >= fechaUltimaProgramacionExpOrigen)? fechaUltimaProgramacionExpDestino.addDays(1):fechaUltimaProgramacionExpOrigen.addDays(1);
			}
			
			/*En el caso de que sólo el encabezado de destino no tenga alguna programación facturada, 
			se podrá agregar el producto apartir del período siguiente a la programación facturada del encabezado origen, 
			sólo si este no es anterior al período de la primera programación del encabezado destino.*/
			
			else if(fechaUltimaProgramacionExpDestino == null && fechaUltimaProgramacionExpOrigen != null){
				fechaInicioPeriodos = (fechaUltimaProgramacionExpOrigen.addDays(1)>=listaProgramaciones[0].Inicio_del_per_odo__c)?fechaUltimaProgramacionExpOrigen.addDays(1):listaProgramaciones[0].Inicio_del_per_odo__c;
			}
			/*En el caso de que sólo el encabezado de origen no tenga alguna programación facturada, 
			se podrá agregar el producto apartir del período siguiente a la programación facturada del encabezado destino, 
			sólo si este no es anterior al período de la primera programación del encabezado origen.*/
			
			else if(fechaUltimaProgramacionExpDestino != null && fechaUltimaProgramacionExpOrigen == null){
				fechaInicioPeriodos = (fechaUltimaProgramacionExpDestino.addDays(1)>=listaProgramacionesOrigen[0].Inicio_del_per_odo__c)?fechaUltimaProgramacionExpDestino.addDays(1):listaProgramacionesOrigen[0].Inicio_del_per_odo__c;				
			}
			
			/*En el caso de que ambos encabezados no tengan programaciones facturadas, 
			se podra agregar el producto apartir de la fecha de inicio de la primera programación del encabezado destino.*/
			else{
				fechaInicioPeriodos = listaProgramaciones[0].Inicio_del_per_odo__c;
			}
			
			Date fechaInicio = fechaInicioPeriodos;
			Integer num = 0; 
			try{
				Date fechaFin= listaProgramaciones[listaProgramaciones.size()-1].Fin_del_per_odo__c;
				num =  fechaInicio.monthsBetween(fechaFin.addDays(1));
			}catch(Exception e){}
			Integer mes = fechaInicioPeriodos.month();
			Integer anio = fechaInicioPeriodos.year();
			for(Integer i = fechaInicioPeriodos.month(); i <= num + fechaInicioPeriodos.month(); i++){
				if(mes ==13){
					mes=1;
					anio++;
				}
				year = String.valueOf(anio);
				month = String.valueOf(mes);
				String stringDate = year + '-' + month + '-1 00:00:00';
				listaMeses.add(new SelectOption(stringDate, meses[mes]+ ' '+ anio));
				mes++; 
			}
		}else listaMeses.add(new SelectOption('Ninguno', 'Ninguno'));
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,  '' + listaProgramacionesOrigen));
		//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,  '' + encabezadoDestino + fechaUltimaProgramacionExpOrigen + fechaUltimaProgramacionExpDestino));
		return listaMeses;
	}
	
	public List<WrapperProductos> getListaWrapperProductosOrigen(){
		
		return listaWrapperProductosOrigen;
	} 
	
	
	public List<WrapperProductos> getListaWrapperProductosDestino(){
		return listaWrapperProductosDestino;
	}
	
	public Boolean getshowComboProgramaciones(){
    	return showComboProgramaciones;
    }
	
	public class WrapperProductos{
		public NE__OrderItem__c equipoSitio{get;set;}
		public Boolean selected{get;set;}
		
		public WrapperProductos(NE__OrderItem__c equipoSitio, Boolean selected){
			this.equipoSitio = equipoSitio;
			this.selected = selected;
		}
	}
	
}