public class BI_O4_BulkProgressController {	 

	public String bulkImportRequestId {get;set;}
	public String idOptyParent {get;set;}
	public list<String> listBulkImportRequestId {get;set;}
	public Integer reloadTimes {get;set;}
	public List<Bit2WinHUB__Bulk_Import_Request__c> listBIR {get;set;}
	public Map<String,Bit2WinHUB__Bulk_Import_Request__c> mapBIRs = new Map<String,Bit2WinHUB__Bulk_Import_Request__c>();
	public List <Bit2WinHUB__Bulk_Configuration_Request__c> processedBCRList {get;set;}
	public boolean boolAllProcessed {get;set;}


	public  BI_O4_BulkProgressController (){
		bulkImportRequestId	=	Apexpages.currentPage().getParameters().get('BIRId');
		idOptyParent=	Apexpages.currentPage().getParameters().get('optyId');	
		reloadTimes = 0;		
		init();
	}


	public void init(){

		listBIR = New List<Bit2WinHUB__Bulk_Import_Request__c>([SELECT id,Name,Bit2WinHUB__Request_Id__c,Bit2WinHUB__Processed_Blocks__c,Bit2WinHUB__Total_Blocks__c,Bit2WinHUB__Step__c,Bit2WinHUB__Status__c  FROM Bit2WinHUB__Bulk_Import_Request__c WHERE Bit2WinHUB__Description__c =:bulkImportRequestId]);
		listBulkImportRequestId = new list<String>();

		if (!listBIR.isEmpty() ){
			for(Bit2WinHUB__Bulk_Import_Request__c BIR : listBIR){
				listBulkImportRequestId.add(BIR.Bit2WinHUB__Request_Id__c);
				mapBIRs.put(BIR.Bit2WinHUB__Request_Id__c,BIR);

			}		
		}
	}


	public void loadBulkImportObject(){
		
		listBIR = [SELECT id,Name,Bit2WinHUB__Request_Id__c,Bit2WinHUB__Processed_Blocks__c,Bit2WinHUB__Total_Blocks__c,Bit2WinHUB__Step__c,Bit2WinHUB__Status__c  FROM Bit2WinHUB__Bulk_Import_Request__c WHERE Bit2WinHUB__Description__c =:bulkImportRequestId];
		processedBCRList =  [SELECT Id,
									Name,
									Bit2WinHUB__Configuration__c,
									Bit2WinHUB__Configuration__r.name,
									Bit2WinHUB__Configuration__r.NE__OptyId__c,
									Bit2WinHUB__Configuration__r.NE__OptyId__r.name,
									Bit2WinHUB__Description__c, 
									Bit2WinHUB__Status__c,
									Bit2WinHUB__Request_Id__c
									FROM Bit2WinHUB__Bulk_Configuration_Request__c 
									WHERE (Bit2WinHUB__Status__c = 'Completed' OR Bit2WinHUB__Status__c = 'Failed') 
									AND Bit2WinHUB__Request_Id__c IN :listBulkImportRequestId];
        System.debug('listBIR.size(): '+listBIR.size());
		boolAllProcessed=false;

		if (!listBIR.isEmpty() && !processedBCRList.isEmpty()){
	  		System.debug('listBIR: '+listBIR);
            System.debug('processedBCRList: '+processedBCRList);
            
            System.debug('processedBCRList.size(): '+processedBCRList.size());
			if(listBIR.size()==processedBCRList.size()){
				boolAllProcessed=true;
			}else{
				for(Bit2WinHUB__Bulk_Configuration_Request__c BCR : processedBCRList){
					Bit2WinHUB__Bulk_Import_Request__c BIR = mapBIRs.get(BCR.Bit2WinHUB__Request_Id__c);
					BIR.Bit2WinHUB__Processed_Blocks__c = 1;
				}
			}		
			update mapBIRs.values();			
		}	
	}


	public void loadStatus(){
		reloadTimes++;
		System.debug('loadStatus method: '+reloadTimes);
		loadBulkImportObject(); 
	}
}