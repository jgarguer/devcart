/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:          
* Description:         
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-08-03      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_PackDSbusiness_ctr
*            1.1    2015-08-19      Jeisson Rojas (JR)      Cambio realizado a la Query de Usuarios del campo Country por Pais__c
*            1.2    2015-08-25      Daniel Lopez   (DL)     Modificacion en la estructura para subir la cobertura.
*            1.3    2017-01-02      Gawron, Julián E.       Add testSetup
                    20/09/2017      Angel F. Santaliestra   Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest
private class BI_COL_PackDSbusiness_tst
{
    //Objetos
    public static BI_COL_PackDSbusiness_ctr objPackSubsines;
    public static Account   objCuenta;
    public static Opportunity objOportunidad;
    public static BI_COL_Descripcion_de_servicio__c objDesSer;
    public static BI_COL_manage_cons__c objManage;
    public static BI_COL_PackDSbusiness_ctr.Wrapempaquetar objWrap;
    public static User objUsuario; 
    
    //Listas
    public static List<BI_COL_Descripcion_de_servicio__c> lstDesSer;
    public static list <Profile> lstPerfil;
    public static list <UserRole> lstRoles;
    public static List<User> lstUsuarios;
    public static List<BI_COL_PackDSbusiness_ctr.Wrapempaquetar> lstWraper;
    public static List<List<BI_COL_PackDSbusiness_ctr.Wrapempaquetar>> lstWrapList;
    public static list<BI_COL_manage_cons__c> lstManage;
    public static list<Opportunity> lstOpp;

    //Mapas
    public static Map<String, BI_COL_Empaquetamiento__c> mapEmpaquetamiento;
    
    
    @testSetup public static void crearData ()
    {
        /*lstPerfil             = new list <Profile>();
        lstPerfil               = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1 ];
        system.debug('datos Perfil '+lstPerfil);
        
        
        lstRoles                = new list <UserRole>();
        lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
        system.debug('datos Rol '+lstRoles);
        
        
        /*lstUsuarios = new List<User>();
        lstUsuarios                       = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];
        system.debug('datos Usuario '+lstUsuarios);*/

        /*//ObjUsuario
        objUsuario = new User();
        objUsuario.Alias = 'standt';
        objUsuario.Email ='pruebas@test.com';
        objUsuario.EmailEncodingKey = '';
        objUsuario.LastName ='Testing';
        objUsuario.LanguageLocaleKey ='en_US';
        objUsuario.LocaleSidKey ='en_US'; 
        objUsuario.ProfileId = lstPerfil.get(0).Id;
        objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        objUsuario.UserName ='pruebas@test.com';
        objUsuario.EmailEncodingKey ='UTF-8';
        objUsuario.UserRoleId = lstRoles.get(0).Id;
        objUsuario.BI_Permisos__c ='Sucursales';
        objUsuario.Pais__c='Colombia';
        insert objUsuario;
        
        lstUsuarios = new list<User>();
        lstUsuarios.add(objUsuario);*/
        
        
        objCuenta = new Account();
        objCuenta                                         = new Account();
        objCuenta.Name                                  = 'prueba';
        objCuenta.BI_Country__c                         = 'Colombia';
        objCuenta.TGS_Region__c                         = 'América';
        objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
        objCuenta.CurrencyIsoCode                       = 'COP';
        objCuenta.BI_COL_Segmento_Telefonica__c         = 'Negocios';
        objCuenta.BI_Segment__c                         = 'test';
        objCuenta.BI_Subsegment_Regional__c             = 'test';
        objCuenta.BI_Territory__c                       = 'test';
        insert objCuenta;
        system.debug('datos Cuenta '+objCuenta);
        
        
        objOportunidad                                      = new Opportunity();
        objOportunidad.Name                                   = 'prueba opp';
        objOportunidad.AccountId                              = objCuenta.Id;
        objOportunidad.BI_Country__c                          = 'Colombia';
        objOportunidad.CloseDate                              = System.today().addDays(+5);
        objOportunidad.StageName                              = 'F5 - Solution Definition';
        objOportunidad.CurrencyIsoCode                        = 'COP';
        objOportunidad.Certa_SCP__contract_duration_months__c = 12;
        objOportunidad.BI_Plazo_estimado_de_provision_dias__c = 0 ;
        //objOportunidad.OwnerId                                = lstUsuarios[0].id;
        insert objOportunidad;
        system.debug('Datos Oportunidad  '+objOportunidad);
        lstOpp = [Select Name, AccountId, BI_Country__c,CloseDate, StageName,CurrencyIsoCode,
                        Certa_SCP__contract_duration_months__c, BI_Plazo_estimado_de_provision_dias__c, OwnerId
                From Opportunity limit 100];
        
        
        BI_COL_Descripcion_de_servicio__c objDesSer2         = new BI_COL_Descripcion_de_servicio__c();
        objDesSer2.BI_COL_Oportunidad__c                     = objOportunidad.Id;
        objDesSer2.CurrencyIsoCode               = 'COP';
        insert objDesSer2;
        
        BI_COL_Descripcion_de_servicio__c objDesSer3         = new BI_COL_Descripcion_de_servicio__c();
        objDesSer3.BI_COL_Oportunidad__c                     = objOportunidad.Id;
        objDesSer3.CurrencyIsoCode               = 'COP';
        insert objDesSer3;
        
        objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
        objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
        objDesSer.CurrencyIsoCode               = 'COP';
        insert objDesSer;
        
        lstDesSer = new List<BI_COL_Descripcion_de_servicio__c>();
        lstDesSer.add(objDesSer);
        lstDesSer.add(objDesSer2);
        lstDesSer.add(objDesSer3);
        System.debug('\n\n\n Sosalida objDesSer '+ objDesSer);

        objManage = new BI_COL_manage_cons__c();
        objManage.BI_COL_ConsProyecto__c = 12;
        objManage.BI_COL_Numero_Viabilidad__c = 15;
        objManage.BI_COL_ValorConsecutivo__c = 18;
        objManage.BI_COL_Numero_Viabilidad__c = 9;
        objManage.Name = 'ConsPaquetes';
        insert objManage;
        //lstManage =[Select Name, BI_COL_Numero_Viabilidad__c from BI_COL_manage_cons__c where Name='ConsPaquetes']; 
    }


    static testMethod void General() 
    {
        System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
            objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        }
       objDesSer = [Select Id, BI_COL_Oportunidad__c,CurrencyIsoCode 
         from BI_COL_Descripcion_de_servicio__c limit 1];

       objOportunidad  = [Select Id, Name, AccountId, BI_Country__c, CloseDate, StageName,
         CurrencyIsoCode, Certa_SCP__contract_duration_months__c, BI_Plazo_estimado_de_provision_dias__c
         from Opportunity limit 1];

       objCuenta = [Select Id, Name, BI_Country__c, TGS_Region__c, 
       BI_Tipo_de_identificador_fiscal__c, CurrencyIsoCode 
         from Account limit 1];
                  

        System.runAs(objUsuario) 
        {
            BI_COL_PackDSbusiness_ctr objController2 = new BI_COL_PackDSbusiness_ctr();
            objController2.Buscar();
            objController2.constructor( 'Test Segm', new List<BI_COL_Empaquetamiento__c>(), new List<BI_COL_Descripcion_de_servicio__c>() );
            //crearData();
            PageReference                 pageRef           = Page.BI_COL_PackDSbusiness_pag;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id',objCuenta.Id);
            List<BI_COL_Descripcion_de_servicio__c> lstDS =new List<BI_COL_Descripcion_de_servicio__c>();
            List<Account> lstCuentas = new List<Account>();
            BI_COL_Empaquetamiento__c objEmpaquetamiento = new BI_COL_Empaquetamiento__c();
            List<BI_COL_Empaquetamiento__c> lstObjEmpaq = new List<BI_COL_Empaquetamiento__c>();
            lstWraper = new List<BI_COL_PackDSbusiness_ctr.Wrapempaquetar>();
            lstWrapList = new List<List<BI_COL_PackDSbusiness_ctr.Wrapempaquetar>>();
            for(Integer i = 0; i < 5 ; i++)
            {
                objWrap = new BI_COL_PackDSbusiness_ctr.Wrapempaquetar ();
    
                objDesSer.BI_COL_Oportunidad__c = objOportunidad.Id;
                objDesSer.CurrencyIsoCode       = 'COP';
                
                lstDS.add(objDesSer);
    
                objCuenta.Name                                  = 'prueba';
                objCuenta.BI_Country__c                         = 'Argentina';
                objCuenta.TGS_Region__c                         = 'América';
                objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
                objCuenta.CurrencyIsoCode                       = 'COP';
    
                lstCuentas.add(objCuenta);
                objEmpaquetamiento.BI_COL_Segmento__c = 'prueba'+i;
                
                lstObjEmpaq.add(objEmpaquetamiento);
                objWrap.seleccion = false;
                objWrap.paquete='Sin Empaquetar';
                objWrap.NombrePaquete='Sin Empaquetar ';
                objWrap.idDs = lstDS.get(i).Id;
                    
                lstWraper.add(objWrap);
                lstWrapList.add(lstWraper);
            }
    
            User objUser=[Select CompanyName,Profile.Name from User where id=: userinfo.getuserid()];
            mapEmpaquetamiento = new Map<String, BI_COL_Empaquetamiento__c>();
            objPackSubsines = new BI_COL_PackDSbusiness_ctr();
            BI_COL_PackDSbusiness_ctr objController = new BI_COL_PackDSbusiness_ctr();
            
            objController.Buscar();
            system.debug('\n@-->lstWrapList'+lstWrapList);
            system.debug('\n@-->Tamano lstWrapList\n'+lstWrapList.size());
            objPackSubsines.lstPrincipal = lstWrapList;
            objController.llenarPaquetes();
            objController.calculoListas(12);
            objController.siguiente();
            objController.anterior();
            objController.empaquetar();
            objController.opcEmpaquetar = 'Test Empaquetar';
            objController.empaquetar();
            system.debug( ' \n\n lst principal from test class '+objController.lstPrincipal );
            BI_COL_PackDSbusiness_ctr.Wrapempaquetar objWrap2 = new BI_COL_PackDSbusiness_ctr.Wrapempaquetar();
            List<BI_COL_PackDSbusiness_ctr.Wrapempaquetar> lstWr = objController.lstPrincipal.get(0);
            objWrap2 = lstWr.get(0);
            objWrap2.seleccion = true;
            objWrap2.disable = false;
            List<BI_COL_PackDSbusiness_ctr.Wrapempaquetar> lstToAdd = new List<BI_COL_PackDSbusiness_ctr.Wrapempaquetar>();
            lstToAdd.add( objWrap2 );
            objController.lstPrincipal.add( lstToAdd );
            objController.empaquetar();
            String strNameDesSer = [ Select Id, Name From BI_COL_Descripcion_de_servicio__c Where Id =: objDesSer.Id ].Name;
            system.debug('\n\n .-.-. strNameDesSer '+strNameDesSer);
            objController.Busqueda = strNameDesSer;
            objController.Buscar();
            objController.Busqueda = strNameDesSer+','+strNameDesSer;
            objController.nextBtnClick();
            objController.previousBtnClick();
            objController.getPageNumber();
            objController.getPageSize();
            objController.getTotalPageNumber();
            objController.getPreviousButtonEnabled();
            objController.getNextButtonDisabled();
            objController.getLstMostrar();
        }
    }

    /*static testMethod void siguiente()
    {
        crearData();
        PageReference                 pageRef           = Page.BI_COL_PackDSbusiness_pag;
        Test.setCurrentPage(pageRef);
        objPackSubsines = new BI_COL_PackDSbusiness_ctr();
        BI_COL_PackDSbusiness_ctr objController = new BI_COL_PackDSbusiness_ctr();
        objController.Buscar();
        objController.iterador = 1;
        objPackSubsines.lstPrincipal = lstWrapList;
        objController.llenarPaquetes();
        objController.calculoListas(12);
        objController.siguiente();
    }

    static testMethod void anterior()
    {
        crearData();
        PageReference                 pageRef           = Page.BI_COL_PackDSbusiness_pag;
        Test.setCurrentPage(pageRef);
        BI_COL_PackDSbusiness_ctr objController = new BI_COL_PackDSbusiness_ctr();
        objController.iterador = 2;
        objController.anterior();
    }

    static testMethod void empaquetar()
    {
        crearData();
        PageReference                 pageRef           = Page.BI_COL_PackDSbusiness_pag;
        Test.setCurrentPage(pageRef);
        BI_COL_PackDSbusiness_ctr objController = new BI_COL_PackDSbusiness_ctr();
        objController.empaquetar();
        objController.opcEmpaquetar = 'Test Empaquetar';
        objController.empaquetar();
    }*/
    static testMethod void getDS()
    {
        System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
            objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        }
        System.runAs(objUsuario) 
        {
            //crearData();
            PageReference                 pageRef           = Page.BI_COL_PackDSbusiness_pag;
            Test.setCurrentPage(pageRef);
            BI_COL_PackDSbusiness_ctr objController = new BI_COL_PackDSbusiness_ctr();
            objController.getDS(lstOpp);
        }
    }
}