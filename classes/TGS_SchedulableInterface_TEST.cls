/*------------------------------------------------------------
    Author:         Everis
    Company:        Everis
    Description:    Clase Test de schedulableInterface.
    History
    <Date>          <Author>                <Change Description>
    14/02/2017      Everis          		Initial Version.  
    ------------------------------------------------------------*/
@istest class TGS_SchedulableInterface_TEST {

   	static testmethod void generateBillingMobileLine_TEST() {
          /* Enable Bypass */
        BI_bypass__c bypass = new BI_bypass__c();
        bypass.BI_migration__c = true;
        bypass.BI_skip_trigger__c = true;
        bypass.BI_stop_job__c = true;
        bypass.BI_skip_assert__c = true;
        bypass.BI_Skip_case_assignations__c = true;
        
        insert bypass;
        upsert new TGS_User_Org__c(TGS_Is_TGS__c = true);
        User admin = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        admin.BI_Permisos__c = 'TGS';
        insert admin;
        System.runAs(admin) {
            Map<String, Account> accountMap = TGS_Dummy_Test_Data.getAccountHierarchy();
            List<NE__OrderItem__c> ois = new List<NE__OrderItem__c>();
            //NE__OrderItem__c oi = TGS_Dummy_Test_Data.getOrderItemConfigured(accountMap, Constants.MOBILE_LINE, Constants.CASE_STATUS_ASSIGNED, '', Constants.TYPE_NEW, Constants.MOBILE_LINE, Constants.RECORD_TYPE_HOLDING_ACCOUNT_LABEL);
            //NE__OrderItem__c oi2 = TGS_Dummy_Test_Data.getOrderItemConfigured(accountMap, Constants.MOBILE_LINE, Constants.CASE_STATUS_ASSIGNED, '', Constants.TYPE_NEW, Constants.MOBILE_LINE, Constants.RECORD_TYPE_BUSINESS_UNIT_ACCOUNT_LABEL);
            //NE__OrderItem__c oi3 = TGS_Dummy_Test_Data.getOrderItemConfigured(accountMap, Constants.MOBILE_LINE, Constants.CASE_STATUS_ASSIGNED, '', Constants.TYPE_NEW, Constants.MOBILE_LINE, Constants.RECORD_TYPE_LEGAL_ENTITY_ACCOUNT_LABEL);
            //ois.add(oi); ois.add(oi2); ois.add(oi3);
            NE__Product__c product = new NE__Product__c(name = Constants.BILLABLE_MOBILE_LINE, TGS_CWP_Tier_2__c=Constants.MSIP_TRUNKING_SITE);
            NE__DynamicPropertyDefinition__c dpd = new NE__DynamicPropertyDefinition__c(name = Constants.BILLING_MOBILE_LINE_OIA);
            NE__Family__c family = new NE__Family__c(name = Constants.BILLING_MOBILE_LINE_FAMILY);
            insert dpd;
            insert product; 
            insert family;
            NE__Catalog__c catalog = new NE__Catalog__c(Name = Constants.TGSOL_CATALOG);
            insert catalog;
            NE__Catalog_Item__c OI_catalogItem = new NE__Catalog_Item__c(NE__ProductId__c = product.id, NE__Type__c = Constants.PRODUCT, NE__Catalog_Id__c = catalog.ID);
            insert OI_catalogItem;
            NE__ProductFamilyProperty__c familyProp = new NE__ProductFamilyProperty__c(NE__PropId__c = dpd.id, NE__FamilyId__c = family.id);  
            insert familyProp;
            List<ID> lista = new List<ID>();
            
            Test.startTest();
            
            // Schedule the test job
            String CRON_LAST_DAY_MONTH = '0 0 0 L * ?';
            String jobId = System.schedule('Billable Mobile Line Job',
            CRON_LAST_DAY_MONTH, new TGS_SchedulableInterface());
            
            // Get the information from the CronTrigger API object
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
    
            System.assertEquals(CRON_LAST_DAY_MONTH, ct.CronExpression);
        
            // Verify the job has not run
            System.assertEquals(0, ct.TimesTriggered);
            
            Test.stopTest();
        }
    }  
    
    static testmethod void generateBillingMobileLine_TEST2() {
     
        upsert new TGS_User_Org__c(TGS_Is_TGS__c = true);
        User admin = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        admin.BI_Permisos__c = 'TGS';
        insert admin;
             /* Enable Bypass */
        BI_bypass__c bypass = new BI_bypass__c();
        bypass.BI_migration__c = true;
        bypass.BI_skip_trigger__c = true;
        bypass.BI_stop_job__c = true;
        bypass.BI_skip_assert__c = true;
        bypass.BI_Skip_case_assignations__c = true;
        
        insert bypass;
       
            Map<String, Account> accountMap = TGS_Dummy_Test_Data.getAccountHierarchy();
            List<NE__OrderItem__c> ois = new List<NE__OrderItem__c>();
                NE__OrderItem__c testOrderItem = dummyOrderItemAux();
        	system.debug ('Line84:' +testOrderItem);
             insert testOrderItem;
            //NE__OrderItem__c oi = TGS_Dummy_Test_Data.getOrderItemConfigured(accountMap, Constants.MOBILE_LINE, Constants.CASE_STATUS_ASSIGNED, '', Constants.TYPE_NEW, Constants.MOBILE_LINE, Constants.RECORD_TYPE_HOLDING_ACCOUNT_LABEL);
            //NE__OrderItem__c oi2 = TGS_Dummy_Test_Data.getOrderItemConfigured(accountMap, Constants.MOBILE_LINE, Constants.CASE_STATUS_ASSIGNED, '', Constants.TYPE_NEW, Constants.MOBILE_LINE, Constants.RECORD_TYPE_BUSINESS_UNIT_ACCOUNT_LABEL);
            //NE__OrderItem__c oi3 = TGS_Dummy_Test_Data.getOrderItemConfigured(accountMap, Constants.MOBILE_LINE, Constants.CASE_STATUS_ASSIGNED, '', Constants.TYPE_NEW, Constants.MOBILE_LINE, Constants.RECORD_TYPE_LEGAL_ENTITY_ACCOUNT_LABEL);
            //ois.add(oi); ois.add(oi2); ois.add(oi3);
            NE__Product__c product = new NE__Product__c(name = Constants.BILLABLE_MOBILE_LINE, TGS_CWP_Tier_2__c=Constants.MSIP_TRUNKING_SITE);
            NE__DynamicPropertyDefinition__c dpd = new NE__DynamicPropertyDefinition__c(name = Constants.BILLING_MOBILE_LINE_OIA);
            NE__Family__c family = new NE__Family__c(name = Constants.BILLING_MOBILE_LINE_FAMILY);
            insert dpd;
            insert product; 
            insert family;
            NE__Catalog__c catalog = new NE__Catalog__c(Name = Constants.TGSOL_CATALOG);
            insert catalog;
            NE__Catalog_Item__c OI_catalogItem = new NE__Catalog_Item__c(NE__ProductId__c = product.id, NE__Type__c = Constants.PRODUCT, NE__Catalog_Id__c = catalog.ID);
            insert OI_catalogItem;
            NE__ProductFamilyProperty__c familyProp = new NE__ProductFamilyProperty__c(NE__PropId__c = dpd.id, NE__FamilyId__c = family.id);  
            insert familyProp;
        
            List<ID> lista = new List<ID>();
        
        	Account prueba = new Account (Name = 'TestPruebaHolding', BI_Ambito__c = 'Público', BI_Country__c = 'Spain', TGS_Region__c = '2|Europe', TGS_Account_Leading_MNC_Unit__c = 'GERMANY', BI_O4_New_Hold_Account_Leading_MNC_Unit__c = 'FRANCE',
                                           TGS_Account_Category__c = 'GBDA', CurrencyIsoCode = 'EUR', BI_Activo__c = 'Sí', BI_O4_Holding_Gestion_TGS__c = true,
                                           BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
        	
        	insert prueba;
        	
        	lista.add(prueba.id);
              
            // Schedule the test job
            TGS_SchedulableInterface inter = new TGS_SchedulableInterface();
        	Test.startTest();
        	Database.BatchableContext bc;
       		inter.start(bc);
        	inter.execute(bc,lista);
        	inter.finish(bc);
            Test.stopTest();
    }  
    static testmethod void generateBillingMobileLine_TEST3() {
     
        upsert new TGS_User_Org__c(TGS_Is_TGS__c = true);
        User admin = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        admin.BI_Permisos__c = 'TGS';
        insert admin;
             /* Enable Bypass */
        BI_bypass__c bypass = new BI_bypass__c();
        bypass.BI_migration__c = true;
        bypass.BI_skip_trigger__c = true;
        bypass.BI_stop_job__c = true;
        bypass.BI_skip_assert__c = true;
        bypass.BI_Skip_case_assignations__c = true;
        
        insert bypass;
       
            Map<String, Account> accountMap = TGS_Dummy_Test_Data.getAccountHierarchy();
            List<NE__OrderItem__c> ois = new List<NE__OrderItem__c>();
                     //NE__OrderItem__c oi = TGS_Dummy_Test_Data.getOrderItemConfigured(accountMap, Constants.MOBILE_LINE, Constants.CASE_STATUS_ASSIGNED, '', Constants.TYPE_NEW, Constants.MOBILE_LINE, Constants.RECORD_TYPE_HOLDING_ACCOUNT_LABEL);
            //NE__OrderItem__c oi2 = TGS_Dummy_Test_Data.getOrderItemConfigured(accountMap, Constants.MOBILE_LINE, Constants.CASE_STATUS_ASSIGNED, '', Constants.TYPE_NEW, Constants.MOBILE_LINE, Constants.RECORD_TYPE_BUSINESS_UNIT_ACCOUNT_LABEL);
            //NE__OrderItem__c oi3 = TGS_Dummy_Test_Data.getOrderItemConfigured(accountMap, Constants.MOBILE_LINE, Constants.CASE_STATUS_ASSIGNED, '', Constants.TYPE_NEW, Constants.MOBILE_LINE, Constants.RECORD_TYPE_LEGAL_ENTITY_ACCOUNT_LABEL);
            //ois.add(oi); ois.add(oi2); ois.add(oi3);
            NE__Product__c product = new NE__Product__c(name = Constants.BILLABLE_MOBILE_LINE, TGS_CWP_Tier_2__c=Constants.MSIP_TRUNKING_SITE);
            NE__DynamicPropertyDefinition__c dpd = new NE__DynamicPropertyDefinition__c(name = Constants.BILLING_MOBILE_LINE_OIA);
            NE__Family__c family = new NE__Family__c(name = Constants.BILLING_MOBILE_LINE_FAMILY);
            insert dpd;
            insert product; 
            insert family;
            NE__Catalog__c catalog = new NE__Catalog__c(Name = Constants.TGSOL_CATALOG);
            insert catalog;
            NE__Catalog_Item__c OI_catalogItem = new NE__Catalog_Item__c(NE__ProductId__c = product.id, NE__Type__c = Constants.PRODUCT, NE__Catalog_Id__c = catalog.ID);
            insert OI_catalogItem;
            NE__ProductFamilyProperty__c familyProp = new NE__ProductFamilyProperty__c(NE__PropId__c = dpd.id, NE__FamilyId__c = family.id);  
            insert familyProp;
            List<ID> lista = new List<ID>();
              
            // Schedule the test job;
           TGS_SchedulableInterface inter = new TGS_SchedulableInterface();
           Test.startTest();
        	Database.BatchableContext bc;
       		inter.start(bc);
        	inter.execute(bc,lista);
        	inter.finish(bc);
            Test.stopTest();
    }  
    //-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.

  static testmethod void generateBillingMobileLine_TEST4() {
     
        upsert new TGS_User_Org__c(TGS_Is_TGS__c = true);
        User admin = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        admin.BI_Permisos__c = 'TGS';
        insert admin;
             /* Enable Bypass */
        BI_bypass__c bypass = new BI_bypass__c();
        bypass.BI_migration__c = true;
        bypass.BI_skip_trigger__c = true;
        bypass.BI_stop_job__c = true;
        bypass.BI_skip_assert__c = true;
        bypass.BI_Skip_case_assignations__c = true;
        
        insert bypass;
       
            Map<String, Account> accountMap = TGS_Dummy_Test_Data.getAccountHierarchy();
            List<NE__OrderItem__c> ois = new List<NE__OrderItem__c>();
                     //NE__OrderItem__c oi = TGS_Dummy_Test_Data.getOrderItemConfigured(accountMap, Constants.MOBILE_LINE, Constants.CASE_STATUS_ASSIGNED, '', Constants.TYPE_NEW, Constants.MOBILE_LINE, Constants.RECORD_TYPE_HOLDING_ACCOUNT_LABEL);
            //NE__OrderItem__c oi2 = TGS_Dummy_Test_Data.getOrderItemConfigured(accountMap, Constants.MOBILE_LINE, Constants.CASE_STATUS_ASSIGNED, '', Constants.TYPE_NEW, Constants.MOBILE_LINE, Constants.RECORD_TYPE_BUSINESS_UNIT_ACCOUNT_LABEL);
            //NE__OrderItem__c oi3 = TGS_Dummy_Test_Data.getOrderItemConfigured(accountMap, Constants.MOBILE_LINE, Constants.CASE_STATUS_ASSIGNED, '', Constants.TYPE_NEW, Constants.MOBILE_LINE, Constants.RECORD_TYPE_LEGAL_ENTITY_ACCOUNT_LABEL);
            //ois.add(oi); ois.add(oi2); ois.add(oi3);
            NE__Product__c product = new NE__Product__c(name = Constants.BILLABLE_MOBILE_LINE, TGS_CWP_Tier_2__c=Constants.MSIP_TRUNKING_SITE);
            NE__DynamicPropertyDefinition__c dpd = new NE__DynamicPropertyDefinition__c(name = Constants.BILLING_MOBILE_LINE_OIA);
            NE__Family__c family = new NE__Family__c(name = Constants.BILLING_MOBILE_LINE_FAMILY);
            insert dpd;
            insert product; 
            insert family;
            NE__Catalog__c catalog = new NE__Catalog__c(Name = Constants.TGSOL_CATALOG);
            insert catalog;
            NE__Catalog_Item__c OI_catalogItem = new NE__Catalog_Item__c(NE__ProductId__c = product.id, NE__Type__c = Constants.PRODUCT, NE__Catalog_Id__c = catalog.ID);
            insert OI_catalogItem;
            NE__ProductFamilyProperty__c familyProp = new NE__ProductFamilyProperty__c(NE__PropId__c = dpd.id, NE__FamilyId__c = family.id);  
            insert familyProp;
    		List<ID> lista = new List<ID>();
            List<ID> lista1 = new List<ID>();
      		
      
      /*
      		//Account accnt = new Account(Name='Prueba');	
      		//insert accnt;
      		 Account accnt = TGS_Dummy_Test_Data.dummyHierarchy();
      		lista.add(accnt.Id);
      		 Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
        	NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId);
        	insert testOrder;
        	NE__Catalog__c testCatalog = new NE__Catalog__c(Name='Test Catalog');
        	insert testCatalog;
        	NE__Catalog_Category__c testCatalogCategory = new NE__Catalog_Category__c(Name='Test Category', NE__CatalogId__c = testCatalog.Id);
        	insert testCatalogCategory;
        	NE__Catalog_Category__c testCatalogSubCategory = new NE__Catalog_Category__c(Name='Test SubCategory', NE__CatalogId__c = testCatalog.Id, NE__Parent_Category_Name__c = testCatalogCategory.Id);
        	insert testCatalogSubCategory;
        	NE__Product__c testProduct = new NE__Product__c(Name='Test Product');
        	insert testProduct;
        	NE__Catalog_Item__c testCatalogItem = new NE__Catalog_Item__c(NE__Catalog_Category_Name__c = testCatalogSubCategory.Id, NE__Catalog_Id__c = testCatalog.Id, NE__ProductId__c = testProduct.Id );
        	insert testCatalogItem;
        
       		 NE__OrderItem__c testOrderItem = new NE__OrderItem__c(NE__OrderId__c = testOrder.Id, NE__ProdId__c = testProduct.Id, NE__CatalogItem__c = testCatalogItem.Id, NE__Qty__c=1);
       		testOrderItem.NE__Billing_Account__c = accnt.Id;
      		insert testOrderItem;
      		*/
      
        RecordType RecId = [SELECT Id FROM RecordType WHERE DeveloperName <> 'TGS_Legal_Entity' and DeveloperName <> 'TGS_Legal_Entity_Inactive' and SobjectType = 'account' LIMIT 1]; 
        
        //Account acc = new Account(Name = 'Testacc.Idount', RecordTypeId = RecId.Id);
        //insert acc;
       // Account acc = accountMap.get(Constants.RECORD_TYPE_HOLDING_ACCOUNT_LABEL);
        //insert acc;
      	//Account acc1 = new Account(Name = 'Test', RecordTypeId = RecId.Id);
        //insert acc1;
        Account acc = TGS_Dummy_Test_Data.dummyHierarchy();
      /*
      Id ccId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_CUSTOMER_COUNTRY_ACCOUNT);
        Account accCC = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Holding + String.valueOf(Math.random()),
            RecordTypeId = ccId,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            BI_No_Identificador_fiscal__c = 'a123'+ String.valueOf(Math.random()), //JEG
            BI_Tipo_de_identificador_fiscal__c = 'DNI', //JEG
            ParentId=acc.Id
            );
        insert accCC;
      
      
       Id leId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_LEGAL_ENTITY_ACCOUNT);
        Account accLE = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Holding + String.valueOf(Math.random()),
            RecordTypeId = leId,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            BI_No_Identificador_fiscal__c = 'a123'+ String.valueOf(Math.random()), //JEG
            BI_Tipo_de_identificador_fiscal__c = 'DNI', //JEG
            ParentId=accCC.Id
            );
        insert accLE;
      */
       Id buId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_BUSINESS_UNIT_ACCOUNT);
      // Account accBusinessUnit = new Account(Name = 'BUTest', RecordTypeId = buId, ParentId=acc.Id);
     	// insert accBusinessUnit;

       Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_COST_CENTER);
       //Account accCostCentre = new Account(Name = 'CCTest', RecordTypeId = rtId, ParentId=accBusinessUnit.Id);
     	 //insert accCostCentre;
      
     List <Account> listaId = [SELECT Id FROM Account WHERE TGS_Aux_Legal_Entity__c = :acc.Id AND RecordTypeId =: rtId];
      List <Account> listaIdBU = [SELECT Id FROM Account WHERE ParentId = :acc.Id AND RecordTypeId =: buId];
      
     // List <Account> listaIdBU = [SELECT Id FROM Account WHERE TGS_Aux_Legal_Entity__c = :acc.Id AND RecordTypeId =: buId];

   
      	 Opportunity opp = new Opportunity(Name = 'Test',
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                                          AccountId = acc.Id,
                                          BI_Ciclo_ventas__c = Label.BI_Completo,
                                          BI_Duracion_del_contrato_Meses__c = 36 );
        insert opp;

        NE__Catalog_Header__c catalogheaderobj = new NE__Catalog_Header__c(
            CurrencyIsoCode = 'ARS',
            Name = 'Catalog Test PayBack',
            NE__Active__c = true,
            NE__Enable_Global_Cart__c = false,
            NE__Name__c = 'Catalog Test PayBack',
            NE__Source_Catalog_Header_Id__c = 'a1R11000000H0tc',
            NE__Start_Date__c = system.today()-1
        );
        
        insert catalogheaderobj;
        
        NE__Catalog__c catalogobj = new NE__Catalog__c(
            CurrencyIsoCode = 'ARS',
            Name = 'Catalog Test PayBack x',
            NE__Active__c = true,
            NE__Catalog_Header__c = catalogheaderobj.Id,
            NE__Enable_for_oppty__c = false,
            NE__Engine_Code__c = 'a1U110000002812',
            NE__Source_Catalog_Id__c = 'a1U110000002812',
            NE__Version__c = 1.0,
            NE__Visible_web__c = false
        );
        
        insert catalogobj;
        
        NE__Commercial_Model__c commercialmodelobj = new NE__Commercial_Model__c(
            CurrencyIsoCode = 'ARS',
            Name = 'Catalog Test PayBack x',
            NE__Catalog_Header__c = catalogheaderobj.Id,
            NE__Engine_Code__c = 'a1Y11000000Z2k5',
            NE__Offline__c = false,
            NE__ProgramName__c = 'Catalog_Test_PayBack_x',
            NE__Source_Commercial_Model_Id__c = 'a1Y11000000Z2k5',
            NE__Status__c = 'Pending'
        );
        
        insert commercialmodelobj;
        
        NE__Order__c orderobj = new NE__Order__c(
            CurrencyIsoCode = 'ARS',
            NE__accountId__c = acc.Id,
            NE__BillaccId__c = listaId.get(0).Id,
            NE__CatalogId__c = catalogobj.Id,
            NE__CommercialModelId__c = commercialmodelobj.Id,
            NE__ConfigurationStatus__c = 'Valid',
            NE__Configuration_SubType__c = 'Rápido',
            NE__Configuration_Type__c = 'New',
            NE__One_Time_Fee_Total__c = 32929.86,
            NE__OpportunityId__c = opp.Id,
            NE__OptyId__c = opp.Id,
            NE__OrderStatus__c = 'Active - Rápido',
            NE__Recurring_Charge_Total__c = 4308.7,
            NE__SerialNum__c = 'OR-0000001546',
            NE__ServaccId__c = acc.Id,
            NE__Sync_oppty__c  = false,
            NE__TotalRecurringFrequency__c = 'Monthly',
            NE__Type__c = 'InOrder',
            NE__Version__c = 8.0
        );
        
        insert orderobj;
               
        NE__Product__c cont = new NE__Product__c();
        List<NE__Product__c> productobj = new List<NE__Product__c>{};

        cont.Country__c = 'ARG';
        cont.CurrencyIsoCode = 'ARS';
        cont.Name = 'SEGURIDAD GESTIONADA x';
        cont.NE__Block_Change__c = false;
        cont.NE__Check_Stock__c = false;
        cont.NE__Engine_Code__c = 'a21110000001pku';
        cont.NE__IsPromo__c = false;
        cont.NE__Source_Product_Id__c ='a21110000001pku';
        cont.NE__Starting_on__c = system.today()-1;
        cont.Payback_Family__c = 'Servicios';
        productobj.add(cont);
        
        cont = new NE__Product__c();
        cont.Country__c = 'ARG';
        cont.CurrencyIsoCode = 'ARS';
        cont.Name = 'ROUTER CISCO 1905 PROVINCIA x';
        cont.NE__Block_Change__c = false;
        cont.NE__Check_Stock__c = false;
        cont.NE__Engine_Code__c = 'a21110000001pku';
        cont.NE__IsPromo__c = false;
        cont.NE__Source_Product_Id__c ='a21110000001pku';
        cont.NE__Starting_on__c = system.today()-1;
        cont.Payback_Family__c = 'Equipos';
        //productobj.add(cont);      
        
        cont = new NE__Product__c();
        cont.Country__c = 'ARG';
        cont.CurrencyIsoCode = 'ARS';
        cont.Name = 'EESS infraestructura x';
        cont.NE__Block_Change__c = false;
        cont.NE__Check_Stock__c = false;
        cont.NE__Engine_Code__c = 'a21110000001pku';
        cont.NE__IsPromo__c = false;
        cont.NE__Source_Product_Id__c ='a21110000001pku';
        cont.NE__Starting_on__c = system.today()-1;
        cont.Payback_Family__c = 'Equipos';
        //productobj.add(cont);      
        
        cont = new NE__Product__c();
        cont.Country__c = 'ARG';
        cont.CurrencyIsoCode = 'ARS';
        cont.Name = 'EESS Equipos x';
        cont.NE__Block_Change__c = false;
        cont.NE__Check_Stock__c = false;
        cont.NE__Engine_Code__c = 'a21110000001pku';
        cont.NE__IsPromo__c = false;
        cont.NE__Source_Product_Id__c ='a21110000001pku';
        cont.NE__Starting_on__c = system.today()-1;
        cont.Payback_Family__c = 'Equipos';
        //productobj.add(cont);  
        
        cont = new NE__Product__c();
        cont.Country__c = 'ARG';
        cont.CurrencyIsoCode = 'ARS';
        cont.Name = 'RADIO PROVINCIA x';
        cont.NE__Block_Change__c = false;
        cont.NE__Check_Stock__c = false;
        cont.NE__Engine_Code__c = 'a21110000001pku';
        cont.NE__IsPromo__c = false;
        cont.NE__Source_Product_Id__c ='a21110000001pku';
        cont.NE__Starting_on__c = system.today()-1;
        cont.Payback_Family__c = 'Instalación';
        //productobj.add(cont);  
        
        cont = new NE__Product__c();
        cont.Country__c = 'ARG';
        cont.CurrencyIsoCode = 'ARS';
        cont.Name = 'SEGURIDAD GESTIONADA PROVINCIA x';
        cont.NE__Block_Change__c = false;
        cont.NE__Check_Stock__c = false;
        cont.NE__Engine_Code__c = 'a21110000001pku';
        cont.NE__IsPromo__c = false;
        cont.NE__Source_Product_Id__c ='a21110000001pku';
        cont.NE__Starting_on__c = system.today()-1;
        cont.Payback_Family__c = 'Instalación';
        //productobj.add(cont);    
        
        cont = new NE__Product__c();
        cont.Country__c = 'ARG';
        cont.CurrencyIsoCode = 'ARS';
        cont.Name = 'INFOINTERNET 2M PROVINCIA x';
        cont.NE__Block_Change__c = false;
        cont.NE__Check_Stock__c = false;
        cont.NE__Engine_Code__c = 'a21110000001pku';
        cont.NE__IsPromo__c = false;
        cont.NE__Source_Product_Id__c ='a21110000001pku';
        cont.NE__Starting_on__c = system.today()-1;
        cont.Payback_Family__c = 'Servicios';
        //productobj.add(cont);      
        
        cont = new NE__Product__c();
        cont.Country__c = 'ARG';
        cont.CurrencyIsoCode = 'ARS';
        cont.Name = 'CONVERSOR G703 / V35 PROVINCIA x';
        cont.NE__Block_Change__c = false;
        cont.NE__Check_Stock__c = false;
        cont.NE__Engine_Code__c = 'a21110000001pku';
        cont.NE__IsPromo__c = false;
        cont.NE__Source_Product_Id__c ='a21110000001pku';
        cont.NE__Starting_on__c = system.today()-1;
        cont.Payback_Family__c = 'Equipos';
        //productobj.add(cont);  
        
        cont = new NE__Product__c();
        cont.Country__c = 'ARG';
        cont.CurrencyIsoCode = 'ARS';
        cont.Name = 'RADIO MINILINK x';
        cont.NE__Block_Change__c = false;
        cont.NE__Check_Stock__c = false;
        cont.NE__Engine_Code__c = 'a21110000001pku';
        cont.NE__IsPromo__c = false;
        cont.NE__Source_Product_Id__c ='a21110000001pku';
        cont.NE__Starting_on__c = system.today()-1;
        cont.Payback_Family__c = 'Satelital';
        //productobj.add(cont);
        
        cont = new NE__Product__c();
        cont.Country__c = 'ARG';
        cont.CurrencyIsoCode = 'ARS';
        cont.Name = 'FG 60C PROVINCIA x';
        cont.NE__Block_Change__c = false;
        cont.NE__Check_Stock__c = false;
        cont.NE__Engine_Code__c = 'a21110000001pku';
        cont.NE__IsPromo__c = false;
        cont.NE__Source_Product_Id__c ='a21110000001pku';
        cont.NE__Starting_on__c = system.today()-1;
        cont.Payback_Family__c = 'Seguridad Gestionada';
        //productobj.add(cont);  

        insert productobj;
        
        NE__Catalog_Category__c catalogcategoryobj = new NE__Catalog_Category__c(
            CurrencyIsoCode = 'ARS',
            Name = 'Catalog Test PayBack x',
            NE__CatalogId__c = catalogobj.Id,
            NE__Engine_Code__c = 'a1Q110000006O7Z',
            NE__Hidden__c = false,
            NE__Multiplier_Category__c = false,
            NE__Sequence__c = 0.0,
            NE__Source_Category_Id__c = 'a1Q110000006O7Z',
            NE__Visible_web__c = false
        );
        
        insert catalogcategoryobj;
        
        List<NE__Catalog_Item__c> catalogitemobj = new List<NE__Catalog_Item__c>{};
        
        for(NE__Product__c productobjtemporary:productobj)
        {
            NE__Catalog_Item__c temporary = new NE__Catalog_Item__c(
                Country__c = 'ARG',
                CurrencyIsoCode = 'ARS',
                Is_Dummy__c = false,
                NE__Active__c = true,
                NE__BaseRecurringCharge__c = 0.0,
                NE__Base_OneTime_Fee__c = 0.0,
                NE__Catalog_Category_Name__c = catalogcategoryobj.Id,
                NE__Catalog_Id__c = catalogobj.Id,
                NE__Category_Max_Qty__c = 999.0,
                NE__Category_Min_Qty__c = 0.0,
                NE__Commitment__c = false,
                NE__ConfigurationRequired__c = false,
                NE__Currency__c = 'ARS',
                NE__Delay_Time__c = 0.0,
                NE__Enable_for_oppty__c = false,
                NE__Enable_One_Time_Fee__c = false,
                NE__Enable_Recurring_Charge__c = false,
                NE__Engine_Code__c = 'a1T11000000NHG6',
                NE__Generate_Asset_Item__c = true,
                NE__Id__c = 'a1T11000000NHG6',
                NE__Max_Qty__c = 999.0,
                NE__Min_Qty__c = 0.0,
                NE__Offline_Cart__c = false,
                NE__Offline__c = true,
                NE__Penalty__c = false,
                NE__ProductId__c = productobjtemporary.Id,
                NE__Recurring_Charge_Frequency__c = 'Monthly',
                NE__Sequence__c = 0.0,
                NE__Start_Date__c = system.today()-1,
                NE__Status__c = 'Pending',
                NE__Type__c = 'Product',
                NE__Visible__c = true,
                Not_available_for_campaigns__c = false,
                NE__Technical_Behaviour__c = 'Attachment Required'
            );
            catalogitemobj.add(temporary);
        }
        
        insert catalogitemobj;
        
        
        List<NE__OrderItem__c> orderItemobj = new List<NE__OrderItem__c>{};
        
        for(NE__Catalog_Item__c catalogitemtempobj:catalogitemobj)
        {
            NE__OrderItem__c orderItemtempobj = new NE__OrderItem__c(
                NE__OrderId__c = orderobj.Id,
                CurrencyIsoCode = 'ARS',
                NE__account__c = acc.Id,
                NE__Action__c = 'Add',
                NE__BaseOneTimeFee__c = 0.0,
                NE__BaseRecurringCharge__c = 0.0,
                NE__Billing_account__c = listaId.get(0).Id,
                NE__CatalogItem__c = catalogitemtempobj.Id,
                NE__Commitment__c = false,
                NE__Generate_Asset_Item__c = true,
                NE__IsPromo__c = false,
                NE__OneTimeFeeOv__c = 0.0,
                NE__Optional__c = false,
                NE__Penalty_Activated__c = false,
                NE__Penalty__c = false,
                NE__Picked_By__c = 'Human',
                NE__ProdId__c = catalogitemtempobj.NE__ProductId__c,
                NE__Qty__c = 1.0,
                NE__RecurringChargeFrequency__c = 'Monthly',
                NE__RecurringChargeOv__c = 0.0,
                NE__Service_Account__c = listaIdBU.get(0).Id,
                NE__Status__c = 'Pending'
            );
            orderItemobj.add(orderItemtempobj);
            insert orderItemtempobj;
        }
        
        //insert orderItemobj;
      
      	
      	lista1.add(listaId.get(0).Id);
        //lista1.add(acc1.Id);

      
      
              
            // Schedule the test job;
           TGS_SchedulableInterface inter = new TGS_SchedulableInterface();
           Test.startTest();
        	Database.BatchableContext bc;
       		inter.start(bc);
        	inter.execute(bc,lista1);
        	inter.finish(bc);
      		//TGS_SchedulableInterface_Helper.generateBillingMobileLine(lista1);
            Test.stopTest();
    }  

    
    //-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.

    
    
    
       public static NE__OrderItem__c dummyOrderItemAux() {
        //Id rtId = Schema.SObjectType.NE__Order__c.getRecordTypeInfosByName().get('Order').getRecordTypeId();
        Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
        NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId);
        insert testOrder;
        NE__Catalog__c testCatalog = new NE__Catalog__c(Name='Test Catalog');
        insert testCatalog;
        NE__Catalog_Category__c testCatalogCategory = new NE__Catalog_Category__c(Name='Test Category', NE__CatalogId__c = testCatalog.Id);
        insert testCatalogCategory;
        NE__Catalog_Category__c testCatalogSubCategory = new NE__Catalog_Category__c(Name='Test SubCategory', NE__CatalogId__c = testCatalog.Id, NE__Parent_Category_Name__c = testCatalogCategory.Id);
        insert testCatalogSubCategory;
        NE__Product__c testProduct = new NE__Product__c(Name='Test Product');
        insert testProduct;
        NE__Catalog_Item__c testCatalogItem = new NE__Catalog_Item__c(NE__Catalog_Category_Name__c = testCatalogSubCategory.Id, NE__Catalog_Id__c = testCatalog.Id, NE__ProductId__c = testProduct.Id );
        insert testCatalogItem;
        
        NE__OrderItem__c testOrderItem = new NE__OrderItem__c(NE__OrderId__c = testOrder.Id,TGS_CSUID1__c = '1234',  NE__ProdId__c = testProduct.Id, NE__CatalogItem__c = testCatalogItem.Id, NE__Qty__c=1);
   
        return testOrderItem;
    }
}