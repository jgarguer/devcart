@isTest
private class PCA_CustomCalendarMonth_TEST {

    static testMethod void loadInfoTest() {
        User users = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        
        
         system.runAs(users){ 
            
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> accounts2 = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: accounts2){
                    item.OwnerId = users.Id;
                    accList.add(item);
            }
            update accList;
            List<Contact> con = BI_DataLoad.loadPortalContacts(2, accounts2);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());
            System.runAs(user1){
               BI_TestUtils.throw_exception = false;
                PCA_CustomCalendarMonth constructor = new PCA_CustomCalendarMonth(Date.TODAY());
                DateTime day = Datetime.NOW();
                DateTime day10 = day.AddMinutes(10);
                eventWrapper event1 = new eventWrapper('test','WrapperTest',day,day10,true);
                List<eventWrapper> eventList = new List<eventWrapper>();
                eventList.add(event1);
                constructor.getMonthName();
                constructor.getYearName();
                constructor.getWeekdayNames();
                constructor.getfirstDate();
                constructor.getValidDateRange();
                constructor.setEventsStd(eventList);
                constructor.getWeeks();
                PCA_CustomCalendarMonth.Week constructor2 = new PCA_CustomCalendarMonth.Week();
                constructor2.getDays();
                constructor2.getWeekNumber();
                PCA_CustomCalendarMonth.Day constructor3 = new PCA_CustomCalendarMonth.Day(Date.TODAY(),9);
                constructor2.getStartingDate();
                constructor3.getDate();
                constructor3.getDayOfMonth();
                constructor3.getDayOfMonth2();
                constructor3.getDayOfYear();
                constructor3.getFormatedDate();
                constructor3.getDayNumber();
                constructor3.getpastDay();
                constructor3.getEventsToday();
                constructor3.getCSSName();
                constructor3.getIsEditable();
                system.assertEquals(constructor3.getdate(), Date.today());
            }
            System.runAs(user1){
               BI_TestUtils.throw_exception = true;
                PCA_CustomCalendarMonth constructor = new PCA_CustomCalendarMonth(Date.TODAY());
                constructor.getMonthName();
                constructor.getYearName();
                constructor.getWeekdayNames();
                PCA_CustomCalendarMonth.Week constructor2 = new PCA_CustomCalendarMonth.Week();
                constructor2.getWeekNumber();
                PCA_CustomCalendarMonth.Day constructor3 = new PCA_CustomCalendarMonth.Day(Date.TODAY(),9);
                constructor2.getStartingDate();
                constructor3.getDate();
                constructor3.getDayOfMonth();
                constructor3.getDayOfMonth2();
                constructor3.getDayOfYear();
                constructor3.getFormatedDate();
                constructor3.getDayNumber();
                constructor3.getpastDay();
                constructor3.getEventsToday();
                constructor3.getCSSName();
                constructor3.getIsEditable();
            }
         }

         System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
              update new User(Id = users.Id, LanguageLocaleKey = 'zh_TW');
         }
         system.runAs(users) {
            PCA_CustomCalendarMonth constructor = new PCA_CustomCalendarMonth(Date.TODAY());
            constructor.getMonthName();
         }
    }
    static testMethod void loadInfoTest2() {
        User users = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
            update new User(Id = users.Id, LanguageLocaleKey = 'zh_TW');
        }
        system.runAs(users) {
            BI_TestUtils.throw_exception = false;
            PCA_CustomCalendarMonth constructor = new PCA_CustomCalendarMonth(Date.TODAY());
            constructor.getMonthName();
        }
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}