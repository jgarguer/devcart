global without sharing class BI_Visualiza_Clientes_ctrl {
      @AuraEnabled public static SObject[] getAccountList(String laQuery) {
          List<User> u_me = [SELECT Id, Pais__c FROM User WHERE Id = :UserInfo.getUserId()];
          String addQueryCountry = 'where BI_country__c = \''+ u_me[0].Pais__c + '\'';
        
          System.debug(laQuery);
          if(laQuery == null){
              laQuery = ' limit 2';
          }
          else{
              laQuery += ' limit 101';
          }
          
          addQueryCountry += laQuery;     
          //Pais del usuario
         String dynQuery = 'select id, toLabel(type), owner.name, name, BI_Tipo_de_identificador_fiscal__c, BI_No_Identificador_fiscal__c, BI_Segment__c, BI_Subsegment_Local__c, ' +
                           ' BI_Riesgo__c, BI_Fraude__c, BI_Victima_de_fraude__c, BI_Modelo_de_Atencion_Transversal__c, BI_Asignacion__c, ' +
                           ' (Select id, User.Name, toLabel(TeamMemberRole) from AccountTeamMembers) from account ';
          dynQuery += addQueryCountry;
          
          System.debug('Segundo intento' + addQueryCountry);  
         return Database.query(dynQuery); 
    }
    /*
    @AuraEnabled public static Map<String, SObject> getAccountList() {
        Map<String, SObject> map_obj = new Map<String, SObject>();
        map_obj.put('c.Id', new List<String>{'op'});
        respuesta.put('ListaClientes', laLista);       
        return map_obj;
    }
	*/
}