public Class UtilSync {
    
    public static Boolean isMyTest = false;

    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    //final static List<User> LST_USER_INFO;

    static{
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'Account' ]){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }

        //LST_USER_INFO = [SELECT Id, Pais__c, BI_Permisos__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
    }
    
    public static void AccountSync(List<Account> accounts) {
        //TODO: Refactor with Custom Setting Logic.
       // List<Region__c> regions = [select id from region__c where name = 'Global' limit 1];
        /*Map<String, BI_Code_ISO__c> map_region = BI_DataLoad.loadRegions();
        BI_Code_ISO__c region;
        List<String> lst_region = new List<String>();

        for(BI_Code_ISO__c biCode: map_region.values()){
            if(biCode.BI_Country_ISO_Code__c == 'GLB'){
                    region = biCode;
                    lst_region.add(biCode.Name);
            }
        }*/
        String regions = 'Global';
                
        For (Account a :accounts) {
            if (a.ConnectionReceivedId != null || (Test.isRunningTest() && UtilSync.isMyTest)) {
//                if (lst_region.size()>0) {
//                    a.BI_Country__c = lst_region[0];
//                }
                a.BI_Country__c = regions;
                //a.BI_Holding__c = 'Sí'; this filed now is a Workflow Update based on RecordType.
                a.RecordTypeId = MAP_NAME_RT.get('TGS_Customer_Country');

            }
        }    
    
    }
    
    public static void ContactSync(List<Contact> contacts) {

        Map<Id,List<Contact>> contactIdsMap = new Map<Id,List<Contact>>();
        String regions = 'Global';

        for (Contact c :contacts) {
            if (c.accountSync__c != null && c.accountSync__c != '') {
                if (contactIdsMap.get(c.accountSync__c) == null) {
                    contactIdsMap.put(c.accountSync__c, new List<Contact>());
                }
                contactIdsMap.get(c.accountSync__c).add(c);
                c.BI_Country__c = regions;
                c.BI_Activo__c = true;
            }
        }
        
        for (PartnerNetworkRecordConnection pnrc :[select id, PartnerRecordId, LocalRecordId from PartnerNetworkRecordConnection where PartnerRecordId in :contactIdsMap.KeySet()]) {
            for (Contact c :contactIdsMap.get(pnrc.PartnerRecordId)) {
                c.accountid = pnrc.LocalRecordId;
            }
        }

    }

}