global class BI_FOCOSBatchDelete implements Database.Batchable<sObject> {
	
	global final String country;
	global final String entity;
	global Integer failed;
	global String errMsg;
	
	global BI_FOCOSBatchDelete(String c, String e) {
		failed = 0;
		this.country = c;
		this.entity = e;
		System.debug('-------------------------------------------BI_FOCOSBatchDelete.constructor(): Country: '+ c +' and entity: '+ entity);
	}
	
	/*
	global Iterable<sObject> start(Database.BatchableContext BC) {
		System.debug('-------------------------------------------BI_FOCOSBatchDelete.start()');
		
		List<sObject> fList;
		if('BI_Registro_Datos_FOCO__c'.equals(entity)){
			fList = new List<BI_Registro_Datos_FOCO__c> ();
			try{
				fList = [SELECT Id from BI_Registro_Datos_FOCO__c where BI_Country__c = :country];
				} catch(QueryException qe)
				{
					throw new BI_FOCO_Exception('Could not retrieve records from BI_Registro_Datos_FOCO__c table to delete for '+ country, qe);
				}
				} else if('BI_Registros_Datos_FOCO_Stage__c'.equals(entity)){
					fList = new List<BI_Registros_Datos_FOCO_Stage__c> ();
					try{
						fList = [SELECT Id from BI_Registros_Datos_FOCO_Stage__c where BI_Country__c = :country];
						}catch(QueryException qe)
						{
							throw new BI_FOCO_Exception('Could not retrieve records from BI_Registros_Datos_FOCO_Stage__c table to delete for '+ country, qe);
						}
					}

					return fList;
				}
				*/


	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('-------------------------------------------BI_FOCOSBatchDelete.start()');
		
		return Database.getQueryLocator('SELECT Id from ' + entity + ' where BI_Country__c = :country');
	}



				global void execute(Database.BatchableContext BC, List<sObject> scope) {
					System.debug('-------------------------------------------BI_FOCOSBatchDelete.execute(): Start batch');

					List<sObject> fList;

					if('BI_Registro_Datos_FOCO__c'.equals(entity)) 
					fList = (List<BI_Registro_Datos_FOCO__c>)scope;

					else if('BI_Registros_Datos_FOCO_Stage__c'.equals(entity)) 
					fList = (List<BI_Registros_Datos_FOCO_Stage__c>)scope;

					List<Database.DeleteResult> dsrs;
					try{
						dsrs = Database.delete(fList, false);
					}
					catch(DmlException dmle){
						throw new BI_FOCO_Exception('Error while batch deleting FOCO records', dmle);

					}
					catch(Exception e){
						throw new BI_FOCO_Exception('Error while batch deleting FOCO records', e);
					}

					for(Database.DeleteResult dsr : dsrs){
						if(!dsr.isSuccess()){
							failed++;
							for(Database.Error err : dsr.getErrors()) {
								errMsg = errMsg + err.getStatusCode() + ': ' + err.getMessage() + '\n';
							}
						}
					}

					if(failed > 0){
						throw new BI_FOCO_Exception('Batch delete failed for '+failed +' records for '+entity+'. Details below - \n '+errMsg);
					}

					Database.emptyRecycleBin(scope);
					System.debug('-------------------------------------------BI_FOCOSBatchDelete.execute(): End batch');

				}

				global void finish(Database.BatchableContext BC) {
					if('BI_Registro_Datos_FOCO__c'.equals(entity)) {
						Map<String, BI_FOCO_Pais__c> mcs =  BI_FOCO_Pais__c.getAll();

						BI_FOCOBatchInsert batchIns = new BI_FOCOBatchInsert(country);
						Id insBatchId = Database.executeBatch(batchIns, (Integer)mcs.get(country).Batch_Size__c);
					}
				}

			}