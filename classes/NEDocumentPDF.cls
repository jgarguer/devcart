public with sharing class NEDocumentPDF {
    
    public NE.DocumentBuilder docBuilder {get;set;}
    public transient NE__Document_Component__c docComponent {get;set;}
    public String content {get;set;}
    public String contentHeader {get;set;} //3.3 RDF
    public String contentFooter {get;set;} //3.3 RDF
    public String toPDF {get;set;}
    public List<NE__Document_Component__c> docCompon;
    public List <NE__Document_Component_Header__c> listOfDCH;
    public NE__Document_Component__c DC         {get;set;}
    public NE__Document_Component__c DCitem       {get;set;} 
    public NE__Document_Component__c DCitem2       {get;set;} 
    public NE__Document_Component__c DCitem3       {get;set;} 
    public String content1                 {get;set;}
    public String idopty                {get;set;}
    public String nameDC {get;set;}
    public String formato {get;set;}
    public String DOC  {get;set;}
    
    
    public NEDocumentPDF()
    {
        Map<String,Object> placeholders = null;
        String textprew='';
        contentHeader ='';
        contentFooter = '';
        String idc= Apexpages.Currentpage().getParameters().get('Id');
        String mapName= Apexpages.Currentpage().getParameters().get('mapName');
        String parentId= Apexpages.Currentpage().getParameters().get('parentId');
        DOC =Apexpages.Currentpage().getParameters().get('DOC');
        if(mapName!=null && mapName!='')
        {
            String dateFormat = '';
            NE.XmlMap.XmlMapResponse respnew;
            Map<String,Object> placeholderstable = null; //3.2 RDF Add for new version of Bit2Publish
            docBuilder = new NE.DocumentBuilder();
            toPDF = 'PDF';
    
            docComponent = [SELECT Id,NE__Document_Component_Header__r.NE__Name__c,NE__Published__c,NE__Header_Document__c,NE__Footer_Document__c from NE__Document_Component__c where id=:idc];
    
            content = docComponent.NE__Published__c;
            nameDC= docComponent.NE__Document_Component_Header__r.NE__Name__c+'.doc';
            formato = 'msword';
            if ( mapName != null && parentId != null) 
            {
               NE.XmlMap.XmlMapRequest req = new NE.XmlMap.XmlMapRequest();
               req.mapName = mapName;
               req.ParentId =parentId;
               NE.XmlMap xm = new NE.XmlMap();
               NE.XmlMap.XmlMapResponse resp = xm.generateXml(req);
               respnew =xm.generateXml(req);
               String xml = resp.xml.toXmlString();
               system.debug(xml);
               placeholders = docBuilder.getExternalMapVariable(xml);          
                        
             }
    
            //3.3 RDF Add new logic for header and footer
             List<NE__Document_Component__c> mainComponentsheader =    [SELECT Id, Name, NE__Name__c, NE__Description__c, NE__published__c, NE__Start_Date__c, NE__End_Date__c,NE__Type__c 
                                                    FROM NE__Document_Component__c 
                                                    WHERE NE__Document_Component_Header__c =: docComponent.NE__Header_Document__c
                                                    AND NE__End_Date__c =:null
                                                    LIMIT 1];
             if( mainComponentsheader.size()>0){ 
                 string conth= mainComponentsheader[0].NE__Published__c;                                  
                contentHeader = mainComponentsheader[0].NE__Published__c;
               // contentHeader = previewnew(contentHeader ,placeholders,mainComponentsheader[0].Id,respnew,mapName,parentId);
                if( contentHeader ==''){
                  contentHeader =conth;
                }
                contentHeader =  contentHeader .unescapeHtml4();
             }
          
             List<NE__Document_Component__c> mainComponentsfooter =    [SELECT Id, Name, NE__Name__c, NE__Description__c, NE__published__c, NE__Start_Date__c, NE__End_Date__c,NE__Type__c 
                                                    FROM NE__Document_Component__c 
                                                    WHERE NE__Document_Component_Header__c =: docComponent.NE__Footer_Document__c
                                                    AND NE__End_Date__c =:null
                                                    LIMIT 1];
             if( mainComponentsfooter.size()>0)
             {
                String foth=   mainComponentsfooter[0].NE__Published__c;                                  
                contentFooter = mainComponentsfooter[0].NE__Published__c; 
               // contentFooter =  previewnew(contentFooter , placeholders,docComponent.Id,respnew,mapName,parentId);
                if(contentFooter =='')
                {
                    contentFooter =foth;
                }
                contentFooter =  contentFooter.unescapeHtml4();
                
             }
             
             if(content != null )
             {
                  content  =  content.unescapeHtml4();
                  content = previewnew(content, placeholders,docComponent.Id,respnew,mapName,parentId); //3.2 RDF Add for new version of Bit2Publish

                  content  =  content.unescapeHtml4();
                  if(DOC=='true')
                  {
                      String brek='(<div).*(page-break-after).*(:).*(always;).*(><span).*(style=\'display:none\').*(</div>)';
                      String brek1='<br style=\'page-break-after: always;\'/>';
                      system.debug('CONTENUTOold: ' + content);
                      content = content.replaceAll(brek,brek1);
                      system.debug('CONTENUTO: ' + content);
                  }
                  
                
                  
             }else
                    content = '';
         }
         else
         {
            DC = [select NE__Name__c,NE__Document_Component_Header__r.NE__Name__c, NE__Published__c, NE__Workspace__c FROM NE__Document_Component__c WHERE          NE__Document_Component_Header__r.NE__Name__c='PRP-Cotizacion' LIMIT 1];
            DCitem = [select NE__Name__c,NE__Document_Component_Header__r.NE__Name__c, NE__Published__c, NE__Workspace__c FROM NE__Document_Component__c WHERE NE__Document_Component_Header__r.NE__Name__c='PRP-ITEM' LIMIT 1];
            DCitem2 = [select NE__Name__c,NE__Document_Component_Header__r.NE__Name__c, NE__Published__c, NE__Workspace__c FROM NE__Document_Component__c WHERE NE__Document_Component_Header__r.NE__Name__c='PRP-ARGENTINA' LIMIT 1];
            DCitem3 = [select NE__Name__c,NE__Document_Component_Header__r.NE__Name__c, NE__Published__c, NE__Workspace__c FROM NE__Document_Component__c WHERE NE__Document_Component_Header__r.NE__Name__c='PRP-URUGUAY' LIMIT 1];
            idopty=Apexpages.Currentpage().getParameters().get('Id');
    
            List <NE__Order__c> ord = [Select NE__AccountId__r.BI_Country__c,  NE__Recurring_Charge_Total__c,NE__One_Time_Fee_Total__c,NE__OptyId__r.BI_Duracion_del_contrato_Meses__c,Owner.Name, NE__AccountId__r.Name,NE__OrderStatus__c, NE__OptyId__c From NE__Order__c Where Id =: idopty  limit 1];
    
            Decimal total1=0;
            Decimal total2=0;
            
            List <NE__OrderItem__c> ordItem = [SELECT NE__ProdId__r.name,
                           NE__RecurringChargeCode__c,
                NE__RecurringChargeOv__c, 
                NE__OneTimeFeeOv__c,
                NE__Qty__c,          
                Id,           
                (SELECT NE__AttrEnterpriseId__c,
                NE__AttrEnterpriseIdCalc__c,
                CreatedById,
                CreatedDate,
                IsDeleted,
                NE__FamPropId__c,
                LastModifiedById,
                LastModifiedDate,
                NE__Order_Item__c,
                Name,
                Id,
                SystemModstamp,
                NE__Value__c FROM NE__Order_Item_Attributes__r)FROM NE__OrderItem__c WHERE NE__OrderId__c =: ord[0].Id ];
                content1='';
    
                List<String> idorderlist = new List<String>();
                for(NE__OrderItem__c oik : ordItem)
                {
                    idorderlist.add(oik.Id);
                }
    
            if(ordItem.size()>0)
            {
              List<NE__Order_Item_Attribute__c> orderItMap= [SELECT Name,NE__Order_Item__c, NE__Value__c FROM NE__Order_Item_Attribute__c WHERE NE__Order_Item__c IN:idorderlist AND NE__Value__c!=null ORDER BY Name];
      
              for(Integer i=0; i<ordItem.size();i++)
              {
                String attr = '';
               
                total1 = total1 + ordItem[i].NE__OneTimeFeeOv__c;
                total2 = total2 + ordItem[i].NE__RecurringChargeOv__c;
                content1 = content1 + DCitem.NE__Published__c;
                content1 = content1.replaceAll('\\{!nameprod\\}', isNull(ordItem[i].NE__ProdId__r.name));
                content1 = content1.replaceAll('\\{!imp1\\}', isNull(''+Double.valueOf(ordItem[i].NE__RecurringChargeOv__c)));
                content1 = content1.replaceAll('\\{!imp2\\}', isNull(''+Double.valueOf(ordItem[i].NE__OneTimeFeeOv__c)));
                content1 = content1.replaceAll('\\{!imp3\\}', '0.0');
                content1 = content1.replaceAll('\\{!imp4\\}', '0.0');
                content1 = content1.replaceAll('\\{!dom\\}', '');
                content1 = content1.replaceAll('\\{!plazo\\}', '');
                content1 = content1.replaceAll('\\{!cantidad\\}', isNull(''+ordItem[i].NE__Qty__c));
                
                try{
                    if(orderItMap.size()>0)  
                    {
                      for(Integer k=0; i<orderItMap.size();k++)
                      {
                        attr=attr+orderItMap[k].Name+' = ' + orderItMap[k].NE__Value__c +'<br></br>' ;
                      }
                      content1 = content1.replaceAll('\\{!charact\\}', isNull(''+attr));
                    }
                }
                catch(Exception e){}
                if(attr=='')
                  content1 = content1.replaceAll('\\{!charact\\}', '');
                content1 = content1.unescapeHtml4();
              }
            }
    
    
            content = DC.NE__Published__c;
                content = content.unescapeHtml4();
            
            Integer rand = Math.round(Math.random()*100);
            Integer rand2 = Math.round(Math.random()*100000);
            content = content.replaceAll('\\{!Cliente\\}', isNull(ord[0].NE__AccountId__r.Name));
            content = content.replaceAll('\\{!NumberNeg\\}', 'CTZ-'+rand);
            content = content.replaceAll('\\{!Proprietario\\}', isNull(ord[0].Owner.Name));
            content = content.replaceAll('\\{!DurationContract\\}', isNull(''+ord[0].NE__OptyId__r.BI_Duracion_del_contrato_Meses__c)+' meses.');
            content = content.replaceAll('\\{!ValidOferta\\}', '');
            content = content.replaceAll('\\{!CodConfig\\}', ''+rand2);
            content = content.replaceAll('\\{!tot1\\}', isNull(''+ord[0].NE__Recurring_Charge_Total__c));
            content = content.replaceAll('\\{!tot2\\}', isNull(''+ord[0].NE__One_Time_Fee_Total__c));
            content = content.replaceAll('\\{!tot3\\}', '0.0');
            content = content.replaceAll('\\{!tot4\\}', '0.0');
            content = content.replaceAll('\\{!#PRP-ITEM#\\}', content1);
            
            if(ord[0].NE__AccountId__r.BI_Country__c=='Argentina'){
              content = content.replaceAll('\\{!paese\\}', (DCitem2.NE__Published__c).unescapeHtml4());
            }  
            else if(ord[0].NE__AccountId__r.BI_Country__c=='Uruguay'){
              content = content.replaceAll('\\{!paese\\}', (DCitem3.NE__Published__c).unescapeHtml4());  
            }
            else{
              content = content.replaceAll('\\{!paese\\}', '');
            }
         }           
    }
    
    
    //Replace placeholders DC with DC content
    public String substituteComponent(String contentOfMainComponent){
        try{
            String result = contentOfMainComponent;
            system.debug(result);
            if (contentOfMainComponent== null || contentOfMainComponent== ''){
                return result;
            }
            
            String startPlaceholder = '\\{!#';
            String endPlaceholder = '#\\}';
            
            List<String> splitOne = result.split(endPlaceholder);
            Set <String> setOfName = new Set <String>();
            
            system.debug(splitOne);
            
            for (String firstSplitted : splitOne){
                List<String> splitTwo = firstSplitted.split(startPlaceholder);
                system.debug(splitTwo);
                if (splitTwo.size()==2){
                    setOfName.add(splitTwo[1]);
                    system.debug(splitTwo[1]);
                }
            }
            system.debug(setOfName);
            
            if (setOfName.size()>0){

                List <NE__Document_Component__c> listOfComponent = new List <NE__Document_Component__c>();
                
                for (NE__Document_Component_Header__c dch:listOfDCH){
                    if (dch.NE__Document_Components__r.size()>0){
                        for(String namdch : setOfName)
                        {
                            if(namdch==dch.NE__Name__c){
                                listOfComponent.add(dch.NE__Document_Components__r);
                            }   
                        }
                    }
                }
                
                for (NE__Document_Component__c aComponent : listOfComponent ){
                    String placeholder = '\\{!#'+aComponent.NE__Document_Component_Header__r.NE__Name__c+'#\\}';
                    if ( aComponent.NE__published__c != null ){
                        result = result.replaceAll(placeholder, aComponent.NE__published__c );   
                    }
                    else
                        result = result.replaceAll(placeholder, '' );
                }
            }
            result  =  result.unescapeHtml4();
            return result;
        }catch(Exception e){
            System.debug('Error: '+e.getMessage());
            return null;
        }
    }
    
    //Replace placeholders variable with variable content
    /*private*/public String substitutePlaceholder(String contentOfMainComponent, Map<String,Object> placeholderToSubstitute){
        try{
           
            String result = contentOfMainComponent;
            if ( contentOfMainComponent == null ){
                result='';
            }
            List <String> listPlaceholder = new List<String> (placeholderToSubstitute.keySet());
            

            
            
            for (String placeholderKey : listPlaceholder ){
                String placeholder = '\\{!'+placeholderKey+'\\}';

                Object value = placeholderToSubstitute.get(placeholderKey);
                if (value != null){
                    if ( value instanceof String || value instanceof Id ){
                        system.debug('before substitution '+result);
                        result = result.replaceAll(placeholder,(String)value);
                        system.debug('after substitution '+result);
                    }/*else if ( value instanceof Decimal || value instanceof Double || value instanceof Integer || value instanceof Long){
                        result = result.replaceAll(placeholder,String.valueOf(((Decimal)value).format()));
                    }else if ( value instanceof Date){
                        result = result.replaceAll(placeholder,String.valueOf(((Date)value).format()));
                    }else if ( value instanceof DateTime ){
                        result = result.replaceAll(placeholder,String.valueOf(((DateTime)value).format()));
                    }else if ( value instanceof Time ){
                        result = result.replaceAll(placeholder,String.valueOf(((Time)value)));
                    }else if ( value instanceof Boolean ){
                        result = result.replaceAll(placeholder,String.valueOf((Boolean)value));
                    }else if ( value instanceof Blob ){
                        result = result.replaceAll(placeholder,String.valueOf(((Blob)value).toString()));
                    }else{
                        result = result.replaceAll(placeholder,'Not Found');
                    }*/
                }
            }
            return result;
        }catch(Exception e){
            System.debug('Error: '+e.getMessage());
            return null;
        }
    }  
    
        //generate new preview
    public String previewnew(String contentOfMainComponent, Map<String,Object> placeholders, String idQuery2,NE.XmlMap.XmlMapResponse respnew,String mapname,String parentid){
        try{
            String result = '';
            String contentOfMainComp = contentOfMainComponent;
            Date executionDate = Date.today();
            
            docCompon =[SELECT NE__Name__c,Id,NE__Description__c,NE__Header_Name__c, NE__State__c, NE__Published__c,NE__Start_Date__c, NE__End_Date__c FROM NE__Document_Component__c WHERE ((NE__State__c='Published' or NE__State__c='Authoring') and (NE__Start_Date__c<=:Date.today()) and (NE__End_Date__c>=:Date.today() or NE__End_Date__c=:null)) ORDER BY NE__Header_Name__c ];
        
            listOfDCH =     [SELECT CreatedById, CreatedDate, LastModifiedDate, IsDeleted, Id, 
                                                                    LastModifiedById, SystemModstamp,NE__Name__c,Name,NE__Type__c,
                                                                    (SELECT NE__Start_Date__c,NE__Type__c,
                                                                    NE__State__c,
                                                                    NE__Published__c,
                                                                    Name,NE__Name__c,
                                                                    Id,NE__End_Date__c,
                                                                    NE__Document_Component_Header__c,NE__Description__c,
                                                                    NE__Document_Component_Header__r.NE__Name__c
                                                                    FROM NE__Document_Components__r
                                                                    WHERE   NE__Start_Date__c <=: executionDate
                                                                    AND (NE__End_Date__c >=: executionDate OR NE__End_Date__c =:null)
                                                                    AND (NE__State__c='Published' or NE__State__c='Authoring')
                                                                    ORDER BY NE__Start_Date__c DESC NULLS LAST
                                                                    LIMIT 1) 
                                                                    FROM NE__Document_Component_Header__c
                                                                    ];
                                                                    
            system.debug(contentOfMainComp);
            
            NE.XmlMap.XmlMapRequest req = new NE.XmlMap.XmlMapRequest();
            req.mapName = Mapname;
            req.ParentId =parentId;
            NE.XmlMap xm = new NE.XmlMap();
            NE.XmlMap.XmlMapResponse resp = xm.generateXml(req);
            String xml = resp.xml.toXmlString(); 
            transient Map<String,Object> placeholdersnew = getExternalMapVariablenew(xml);
            
            for(Integer i=0; i<4; i++ )
            {
                if(contentOfMainComp.split('\\{!#RIC_').size()>0)
                {
                    contentOfMainComp = substituteComponentric(contentOfMainComp);
                    if(mapname!=null && mapname!='')
                    {
                        try{
                            contentOfMainComp =generateXMLNEW(contentOfMainComp, mapname,parentid,placeholders,respnew);
                                
                        }
                        catch(Exception e)
                        {
                             System.debug('Error: '+e.getMessage());
                    
                        }
                    }   
                }           
            
                contentOfMainComp  =  contentOfMainComp.unescapeHtml4();
            
                if(contentOfMainComp.contains('{!#')){
                    contentOfMainComp = substituteComponent(contentOfMainComp);
                   // contentOfMainComp = substituteComponentWithCondition(contentOfMainComp,placeholders);
                }
                if(contentOfMainComp.contains('{!')){
                    result = substitutePlaceholder(contentOfMainComp, placeholdersnew);
                }
            }
            result  =  result.unescapeHtml4();
            result  =  result.replaceAll('"', '\'');

            return result;  
       }catch(Exception e){
            System.debug('Error: '+e.getMessage());
            return null;
        }   
    }
    
    //Replace placeholders DC with DC content
    public String substituteComponentric(String contentOfMainComponent)
    {
        try{
                String result = contentOfMainComponent;
                
                String startPlaceholder = '\\{!#RIC_';
                String endPlaceholder = '_RIC#\\}';
                
                List<String> splitOne = result.split(endPlaceholder);
                Set <String> setOfName = new Set <String>();
                
                system.debug(splitOne);
                
                for (String firstSplitted : splitOne)
                {
                    List<String> splitTwo = firstSplitted.split(startPlaceholder);
                    system.debug(splitTwo);
                    
                    if (splitTwo.size()==2)
                    {
                        setOfName.add('RIC_'+splitTwo[1]+'_RIC');
                        system.debug(splitTwo[1]);
                    }
                }
                system.debug(setOfName);
                
                if (setOfName.size()>0)
                {
                    docCompon =[SELECT NE__Document_Component_Header__r.NE__Name__c,
                                       NE__Name__c,
                                       Id,
                                       NE__Description__c,
                                       NE__Header_Name__c,
                                       NE__State__c,
                                       NE__Published__c,
                                       NE__Start_Date__c,
                                       NE__End_Date__c 
                                FROM NE__Document_Component__c 
                                WHERE 
                                ((NE__State__c='Published' or NE__State__c='Authoring') 
                                and (NE__Start_Date__c<=:Date.today()) and (NE__End_Date__c>=:Date.today() or NE__End_Date__c=:null)) 
                                and NE__Document_Component_Header__r.NE__Name__c IN: setOfName
                                ORDER BY NE__Header_Name__c ];
                }
                return result;
            }
            catch(Exception e)
            {
                System.debug('Error: '+e.getMessage());
                return null;
            }
    }
    
    public  Map<String,Object> addplaceholder(Map<String,Object> result,DOM.XMLNode child, Integer i){
        
        for (DOM.XMLNode child0 : child.getChildren())
        {
             if ( child0.getNodeType() == DOM.XMLNodeType.ELEMENT ) 
             {
                    if( child0.getName() == 'Variable'+i)
                    {
                        if ( child0.getAttributeValue( 'Name' , null ) != null && child0.getAttributeValue( 'Example' , null ) !=null)
                        {
                            result.put(child0.getAttributeValue( 'Name' , null ),child0.getAttributeValue( 'Example' , null ));
                        }
                        for(DOM.XMLNode var0:child0.getChildElements())
                        {               
                            result.put(var0.getName(),var0.getText());

                        }
                        if(i<3)
                        {
                            addplaceholder(result,child0,i+1);
                        }
                    }    
             }
        }
            
        return result;
     }
    
    public Map<String,Object> getExternalMapVariablenew(String XmlVariables)
    {
         try{
                Map<String,Object> result = new Map<String,Object>();
                DOM.Document doc = new DOM.Document();
                doc.load(XmlVariables); 
                DOM.XMLNode root = doc.getRootElement();
                for (DOM.XMLNode child : root.getChildren())
                {
                    if ( child.getNodeType() == DOM.XMLNodeType.ELEMENT )
                    {
                        if( child.getName() == 'Variable')
                        {
                            System.debug(child);
                            if ( child.getAttributeValue( 'Name' , null ) != null && child.getAttributeValue( 'Example' , null ) !=null)
                            {
                                result.put(child.getAttributeValue( 'Name' , null ),child.getAttributeValue( 'Example' , null ));
                            }
    
                            for(DOM.XMLNode var:child.getChildElements())
                            {
                                
                                result.put(var.getName(),var.getText());
                            }
                            addplaceholder(result,child,0);
                        }    
                    }
                }
                system.debug('exiting getExternalMap');
                system.debug(result);
                return result;
            }
            catch(Exception e)
            {
                System.debug('getExternalMap Error: '+e.getMessage());
                return null;
            }
    }
    
    public Map<String,Object> getExternalMapVariablenew1(String XmlVariables)
    {
         try{
                Map<String,Object> result = new Map<String,Object>();
                DOM.Document doc = new DOM.Document();
                doc.load(XmlVariables); 
                DOM.XMLNode root = doc.getRootElement();
                for (DOM.XMLNode child : root.getChildren())
                {
                    if ( child.getNodeType() == DOM.XMLNodeType.ELEMENT )
                    {
                        if( child.getName() == 'Variable')
                        {
                            System.debug(child);
                            if ( child.getAttributeValue( 'Name' , null ) != null && child.getAttributeValue( 'Example' , null ) !=null)
                            {
                                result.put(child.getAttributeValue( 'Name' , null ),child.getAttributeValue( 'Example' , null ));
                            }
    
                            for(DOM.XMLNode var:child.getChildElements())
                            {
                                
                                result.put(var.getName(),var.getText());
                            }
                            addplaceholder(result,child,1);
                        }    
                    }
                }
                system.debug('exiting getExternalMap');
                system.debug(result);
                return result;
            }
            catch(Exception e)
            {
                System.debug('getExternalMap Error: '+e.getMessage());
                return null;
            }
    }
    
    
    public String generateXMLNEW (String content, String Mapname, String Idparent,Map<String,Object> placeholdersx, NE.XmlMap.XmlMapResponse respnew){
        

        NE.XmlMap.XmlMapRequest req = new NE.XmlMap.XmlMapRequest();
        req.mapName = Mapname;
        req.ParentId =Idparent;
        NE.XmlMap xm = new NE.XmlMap();
        NE.XmlMap.XmlMapResponse resp = xm.generateXml(req);
        String xml = resp.xml.toXmlString(); 
        system.debug('xml:::'+xml);
        transient Map<String,Object> placeholders = getExternalMapVariablenew(xml);
        transient List <String> listPlaceholder = new List<String> (placeholders.keySet());
        transient list<NE.XMLDOM.Element> elemList0 = resp.xml.GetElementsByTagName('Variable');
        transient list<NE.XMLDOM.Element> elemList = resp.xml.GetElementsByTagName('Variable0');
        transient list<NE.XMLDOM.Element> elemList1 = resp.xml.GetElementsByTagName('Variable1');
        transient list<NE.XMLDOM.Element> elemList2 = resp.xml.GetElementsByTagName('Variable2');

        
        
        for(Integer k=0; k<docCompon.size();k++)
        {
            
            String placeholderDC = '\\{!#'+docCompon[k].NE__Document_Component_Header__r.NE__Name__c+'#\\}';
            system.debug('content::::::'+content);
            if(content.contains(docCompon[k].NE__Document_Component_Header__r.NE__Name__c))
            {
                String content2='';
                String contentxx0=docCompon[k].NE__Published__c;
                
                for (Integer x=0; x < elemList1.size(); x++){   
                    for (String placeholderKeyc : listPlaceholder ){ 

                            
                        if(contentxx0.contains(placeholderKeyc)&&(elemList1[x].getValue(placeholderKeyc)!=null)&&(x>0)){
                            content2=content2+docCompon[k].NE__Published__c;
                            String placeholder = '\\{!'+placeholderKeyc+'\\}';

                            content2= content2.replaceAll(placeholder,elemList1[x].getValue(placeholderKeyc));

                        }        
                        
                    }       
                }       
                
                for (Integer z=0; z < elemList.size(); z++)
                {   
                    String contentxx=docCompon[k].NE__Published__c;
                    Boolean check= false;

                    for (String placeholderKeyc : listPlaceholder ){
                        if(contentxx.contains(placeholderKeyc)&&(elemList[z].getValue(placeholderKeyc)!=null)){
                            check=true;
                        }    
                            
                    }
                    
                    if(check==true)
                    {
                        content2=content2+docCompon[k].NE__Published__c;
                        system.debug('content:::eee:::'+content);
                    
                        for (String placeholderKey : listPlaceholder ){
                            if(content2.contains(placeholderKey))
                            {
                            
                                String placeholder = '\\{!'+placeholderKey+'\\}';
                                    
                                try{
                                    content2= content2.replaceAll(placeholder,elemList[z].getValue(placeholderKey));
                            
                                    if(content2.split('\\{!#RIC_').size()>0)
                                    {
                            
                                        for(Integer j=0; j<docCompon.size();j++)
                                        {
                                            String content3='';
                                            String placeholderDC2 = '\\{!#'+docCompon[j].NE__Document_Component_Header__r.NE__Name__c+'#\\}';
                                            if(content2.contains(docCompon[j].NE__Document_Component_Header__r.NE__Name__c))
                                            {
                                        
                                                for (Integer x=0; x< elemList1.size(); x++)
                                                {   
                                        
                                                    String contentxx2=docCompon[j].NE__Published__c;
                                                    Boolean check2= false;
                                                    for (String placeholderKey2 : listPlaceholder ){
                                                        if(contentxx2.contains(placeholderKey2)&&(elemList1[z].getValue(placeholderKey2)!=null))
                                                            check2=true;
                                                    }
                                            
                                                    if(check2==true)
                                                    {
                                                        if(elemList1[x].parentNode==elemList[z])
                                                        {
                                                            content3=content3+docCompon[j].NE__Published__c;
                                                            for (String placeholderKey2 : listPlaceholder )
                                                            {
                                                                if(content3.contains(placeholderKey2))
                                                                {
                                                                    String placeholder2 = '\\{!'+placeholderKey2+'\\}';
                                                
                                                                    content3= content3.replaceAll(placeholder2,elemList1[x].getValue(placeholderKey2));
                                                                    if(content2.split('\\{!#RIC_').size()>0)
                                                                    {
                                                                        for(Integer h=0; h<docCompon.size();h++)
                                                                        {
                                                                            String content4='';
                                                                            String placeholderDC3 = '\\{!#'+docCompon[h].NE__Header_Name__c+'#\\}';
                                                                            if(content3.contains(docCompon[h].NE__Header_Name__c)){
                                                                            for (Integer f=0; f< elemList2.size(); f++)
                                                                            {   
                                                                            
                                                                                String contentxx3=docCompon[h].NE__Published__c;
                                                                                Boolean check3= false;
                                                                                for (String placeholderKey3 : listPlaceholder ){
                                                                                    if(contentxx3.contains(placeholderKey)&&(elemList2[z].getValue(placeholderKey)!=null))
                                                                                        check3=true;
                                                                                }
                                                                                
                                                                                if(check3==true){
                                                                                    if(elemList2[f].parentNode==elemList1[x])
                                                                                    {
                                                                                        content4=content4+docCompon[h].NE__Published__c;
                                                                                        for (String placeholderKey3 : listPlaceholder )
                                                                                        {
                                                                                            if(content4.contains(placeholderKey3))
                                                                                            {
                                                                                                String placeholder3 = '\\{!'+placeholderKey3+'\\}';

                                                                                                content4= content4.replaceAll(placeholder3,elemList2[f].getValue(placeholderKey3));

                                                                                            }   
                                                                                        }   
                                                                                    }
                                                                                }   
                                                                            }
                                                                            content3= content3.replaceAll(placeholderDC3,content4); 
                                                                        }   
                                                                    }
                                                                    
                                                                }
                                                        }  
                                                    }   
                                                }       
                                            }   
                                        }       
                                    }   
                                    content2= content2.replaceAll(placeholderDC2,content3);
                                }
                            }
                            
                        }
                        catch(Exception e){}
                    }
                }   
            }
                    
        }
            content= content.replaceAll(placeholderDC,content2);   
        }   
    }
        
    return content;
 }
 
    public String isNull( String s){
      if((s==null)||(s==''+null))
            s='';
        return s;
    }
    
    public string bom{
        get {

                return EncodingUtil.base64decode('77u/').tostring();
        }
    }

}