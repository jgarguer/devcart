@isTest
private class BI_O4_CaseSplitButtonController_TEST {

    static{
        BI_TestUtils.throw_exception = false;
    }
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Ricardo Pereira
	Company:       New Energy Aborda
	Description:   Test class for BI_O4_CaseSplitButtonController
	
	History:
	
	<Date>					<Author>				<Change Description>
	20/09/2016				Ricardo Pereira			Initial version
	20/09/2017                       Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
	25/09/2017		  		Jaime Regidor		   	Fields BI_Subsector__c and BI_Sector__c added 
	------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void test_splitCase_Both() {

	List<Profile> profile = [Select Id from Profile where Name= 'TGS System Administrator' limit 1];
		        
	User user = new User(
            		Username = 'abortest@email.com',LastName = 'TestUser',CommunityNickname = 'nick' + String.valueOf(Math.random()),
            		Alias = 'testuser', Email = 'abortest@email.com', Profileid = profile[0].id, TimeZoneSidKey = 'America/New_York',
            		EmailEncodingKey='UTF-8', BI_Permisos__c = 'Atención al Cliente', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');


	insert user;		


System.runAs(user){

	    BI_TestUtils.throw_exception = false;
	    TGS_User_Org__c uO = new TGS_User_Org__c();
	    uO.TGS_Is_BI_EN__c = True;
	    uO.SetupOwnerId = UserInfo.getUserId();
	    insert uO;
	    
		RecordType rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno'];
		RecordType rtAcc = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];

		List<Account> lst_acc1 = new List<Account>();       
		Account acc1 = new Account(Name = 'test',
						BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
						BI_Activo__c = Label.BI_Si,
						BI_Segment__c = 'Empresas',
						BI_Subsector__c = 'test', //25/09/2017
						BI_Sector__c = 'test', //25/09/2017
						BI_Subsegment_local__c = 'Top 1',
						RecordTypeId = rtAcc.Id,
						BI_Subsegment_Regional__c = 'test',
       					BI_Territory__c = 'test');

		lst_acc1.add(acc1);
		insert lst_acc1;

		List<Case> lstCasos = new List<Case>();
		Case cas = new Case();
		cas.Subject = 'Test Subject Opp';
		cas.AccountId = acc1.Id;
		cas.RecordTypeId = rt.Id;
		//cas.BI_Nombre_de_la_Oportunidad__c = opp.Id;
		cas.BI_O4_Identificativo_Origen__c = 'XXX';
		cas.BI_O4_Type_of_Support_Request__c = 'Both';
		//cas.Status = 'Pendiente Cotización';
		cas.BI_Department__c = 'Ingeniería preventa';
		cas.BI_Type__c = 'Proyectos Especiales';
		cas.Status = 'assigned';
		cas.BI_O4_Presales_Satisfaction_Index__c = 3;
		cas.ParentId = null;
		cas.Description = 'desc1';
		cas.BI_O4_Sales_Channel__c = 'MNC';
		cas.BI_O4_Leading_Channel__c = 'Spain';
		cas.BI_O4_Type_of_Offer__c  = 'PoC';
		cas.BI_O4_Service__c = 'D-Cloud';
        	cas.BI_O4_Main_Topics__c = 'PS - Other';
	        cas.BI_PER_Fecha_de_compromiso_de_atencion__c = datetime.now();
		insert cas;

		Test.setCurrentPage(Page.BI_O4_CaseSplitButton);
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(cas);

        Test.startTest();

        BI_O4_CaseSplitButtonController controller = new BI_O4_CaseSplitButtonController(stdController);
        controller.splitCase();

        Test.stopTest();

     }
	}
	
	@isTest static void test_splitCase_Technical() {
		
	    BI_TestUtils.throw_exception = false;
	    TGS_User_Org__c uO = new TGS_User_Org__c();
	    uO.TGS_Is_BI_EN__c = True;
	    uO.SetupOwnerId = UserInfo.getUserId();
	    insert uO;

	List<Profile> profile = [Select Id from Profile where Name= 'TGS System Administrator' limit 1];
		        
	User user = new User(
            		Username = 'abortest@email.com',LastName = 'TestUser',CommunityNickname = 'nick' + String.valueOf(Math.random()),
            		Alias = 'testuser', Email = 'abortest@email.com', Profileid = profile[0].id, TimeZoneSidKey = 'America/New_York',
            		EmailEncodingKey='UTF-8', BI_Permisos__c = 'Atención al Cliente', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');


	insert user;		


System.runAs(user){

		RecordType rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno'];
		RecordType rtAcc = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];

		List<Account> lst_acc1 = new List<Account>();       
		Account acc1 = new Account(Name = 'test',
						BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
						BI_Activo__c = Label.BI_Si,
						BI_Segment__c = 'Empresas',
						BI_Subsector__c = 'test', //25/09/2017
						BI_Sector__c = 'test', //25/09/2017
						BI_Subsegment_local__c = 'Top 1',
						RecordTypeId = rtAcc.Id,
				        BI_Subsegment_Regional__c = 'test',
				        BI_Territory__c = 'test');

		lst_acc1.add(acc1);
		insert lst_acc1;

		List<Case> lstCasos = new List<Case>();
		Case cas = new Case();
		cas.Subject = 'Test Subject Opp';
		cas.AccountId = acc1.Id;
		cas.RecordTypeId = rt.Id;
		//cas.BI_Nombre_de_la_Oportunidad__c = opp.Id;
		cas.BI_O4_Identificativo_Origen__c = 'XXX';
		cas.BI_O4_Type_of_Support_Request__c = 'Technical';
		//cas.Status = 'Pendiente Cotización';
		cas.BI_Department__c = 'Ingeniería preventa';
		cas.BI_Type__c = 'Proyectos Especiales';
		cas.Status = 'assigned';
		cas.BI_O4_Presales_Satisfaction_Index__c = 3;
		cas.ParentId = null;
		cas.Description = 'desc1';
		cas.BI_O4_Sales_Channel__c = 'MNC';
		cas.BI_O4_Leading_Channel__c = 'Spain';
		cas.BI_O4_Type_of_Offer__c  = 'PoC';
		cas.BI_O4_Service__c = 'D-Cloud';
        	cas.BI_O4_Main_Topics__c = 'PS - Other';
	        cas.BI_PER_Fecha_de_compromiso_de_atencion__c = datetime.now();
		insert cas;

		Case cas1 = new Case();
		cas1.Subject = 'Test Subject Opp';
		cas1.AccountId = acc1.Id;
		cas1.RecordTypeId = rt.Id;
		cas1.ParentId = cas.Id;
		//cas1.BI_Nombre_de_la_Oportunidad__c = opp.Id;
		cas.BI_O4_Type_of_Support_Request__c = 'Technical';
		cas1.BI_O4_Identificativo_Origen__c = 'XXX';
		//cas1.Status = 'Pendiente Cotización';
		cas.Status = 'assigned';
		cas1.BI_Department__c = 'Ingeniería preventa';
		cas1.BI_O4_DDO__c = 'Local Office Estonia';
		cas1.Description = 'desc1';
		lstCasos.add(cas1);

		Test.setCurrentPage(Page.BI_O4_CaseSplitButton);
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(cas);

        Test.startTest();

        BI_O4_CaseSplitButtonController controller = new BI_O4_CaseSplitButtonController(stdController);
        controller.splitCase();

        Test.stopTest();
      }
	}

	@isTest static void test_splitCase_Operational() {
		
	    BI_TestUtils.throw_exception = false;
	    TGS_User_Org__c uO = new TGS_User_Org__c();
	    uO.TGS_Is_BI_EN__c = True;
	    uO.SetupOwnerId = UserInfo.getUserId();
	    insert uO;

	List<Profile> profile = [Select Id from Profile where Name= 'TGS System Administrator' limit 1];
		        
	User user = new User(
            		Username = 'abortest@email.com',LastName = 'TestUser',CommunityNickname = 'nick' + String.valueOf(Math.random()),
            		Alias = 'testuser', Email = 'abortest@email.com', Profileid = profile[0].id, TimeZoneSidKey = 'America/New_York',
            		EmailEncodingKey='UTF-8', BI_Permisos__c = 'Atención al Cliente', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');


	insert user;		


System.runAs(user){

		RecordType rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno'];
		RecordType rtAcc = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];

		List<Account> lst_acc1 = new List<Account>();       
		Account acc1 = new Account(Name = 'test',
						BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
						BI_Activo__c = Label.BI_Si,
						BI_Segment__c = 'Empresas',
						BI_Subsector__c = 'test', //25/09/2017
						BI_Sector__c = 'test', //25/09/2017
						BI_Subsegment_local__c = 'Top 1',
						RecordTypeId = rtAcc.Id,
						BI_Subsegment_Regional__c = 'test',
      					BI_Territory__c = 'test');

		lst_acc1.add(acc1);
		insert lst_acc1;

		List<Case> lstCasos = new List<Case>();
		Case cas = new Case();
		cas.Subject = 'Test Subject Opp';
		cas.AccountId = acc1.Id;
		cas.RecordTypeId = rt.Id;
		//cas.BI_Nombre_de_la_Oportunidad__c = opp.Id;
		cas.BI_O4_Identificativo_Origen__c = 'XXX';
		cas.BI_O4_Type_of_Support_Request__c = 'Operational';
		//cas.Status = 'Pendiente Cotización';
		cas.BI_Department__c = 'Ingeniería preventa';
		cas.BI_Type__c = 'Proyectos Especiales';
		cas.Status = 'assigned';
		cas.BI_O4_Presales_Satisfaction_Index__c = 3;
		cas.ParentId = null;
		cas.Description = 'desc1';
		cas.BI_O4_Sales_Channel__c = 'MNC';
		cas.BI_O4_Leading_Channel__c = 'Spain';
		cas.BI_O4_Type_of_Offer__c  = 'PoC';
		cas.BI_O4_Service__c = 'D-Cloud';
        	cas.BI_O4_Main_Topics__c = 'PS - Other';
	        cas.BI_PER_Fecha_de_compromiso_de_atencion__c = datetime.now();
		insert cas;

		Case cas1 = new Case();
		cas1.Subject = 'Test Subject Opp';
		cas1.AccountId = acc1.Id;
		cas1.RecordTypeId = rt.Id;
		cas1.ParentId = cas.Id;
		//cas1.BI_Nombre_de_la_Oportunidad__c = opp.Id;
		cas.BI_O4_Type_of_Support_Request__c = 'Operational';
		cas1.BI_O4_Identificativo_Origen__c = 'XXX';
		cas.Status = 'assigned';
		cas1.BI_Department__c = 'Ingeniería preventa';
		cas1.BI_O4_DDO__c = 'Local Office Estonia';
		cas1.Description = 'desc1';
		lstCasos.add(cas1);

		Test.setCurrentPage(Page.BI_O4_CaseSplitButton);
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(cas);

        Test.startTest();

        BI_O4_CaseSplitButtonController controller = new BI_O4_CaseSplitButtonController(stdController);
        controller.splitCase();

        Test.stopTest();
      }
	}

	@isTest static void test_cancel() {
	    BI_TestUtils.throw_exception = false;
	    TGS_User_Org__c uO = new TGS_User_Org__c();
	    uO.TGS_Is_BI_EN__c = True;
	    uO.SetupOwnerId = UserInfo.getUserId();
	    insert uO;

		RecordType rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno'];
		RecordType rtAcc = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];

		List<Account> lst_acc1 = new List<Account>();       
		Account acc1 = new Account(Name = 'test',
						BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
						BI_Activo__c = Label.BI_Si,
						BI_Segment__c = 'Empresas',
						BI_Subsector__c = 'test', //25/09/2017
						BI_Sector__c = 'test', //25/09/2017
						BI_Subsegment_local__c = 'Top 1',
						RecordTypeId = rtAcc.Id,
						BI_Subsegment_Regional__c = 'test',
        				BI_Territory__c = 'test'	);

		lst_acc1.add(acc1);
		insert lst_acc1;

		List<Case> lstCasos = new List<Case>();
		Case cas = new Case();
		cas.Subject = 'Test Subject Opp';
		cas.AccountId = acc1.Id;
		cas.RecordTypeId = rt.Id;
		//cas.BI_Nombre_de_la_Oportunidad__c = opp.Id;
		cas.BI_O4_Identificativo_Origen__c = 'XXX';
		cas.BI_O4_Type_of_Support_Request__c = 'Both';
		cas.Status = 'assigned';
		cas.BI_Department__c = 'Ingeniería preventa';
		cas.BI_Type__c = 'Proyectos Especiales';
		//cas.Status = 'Closed';
		cas.BI_O4_Presales_Satisfaction_Index__c = 3;
		cas.ParentId = null;
		cas.Description = 'desc1';
		cas.BI_O4_Sales_Channel__c = 'MNC';
		cas.BI_O4_Leading_Channel__c = 'Spain';
		cas.BI_O4_Type_of_Offer__c  = 'PoC';
		cas.BI_O4_Service__c = 'D-Cloud';
        	cas.BI_O4_Main_Topics__c = 'PS - Other';
	        cas.BI_PER_Fecha_de_compromiso_de_atencion__c = datetime.now();
		insert cas;

		Test.setCurrentPage(Page.BI_O4_CaseSplitButton);
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(cas);

        Test.startTest();

        BI_O4_CaseSplitButtonController controller = new BI_O4_CaseSplitButtonController(stdController);
        controller.cancel();

        Test.stopTest();
	}
	
}