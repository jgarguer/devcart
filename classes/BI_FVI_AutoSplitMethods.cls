/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jorge Galindo
        Company:       Aborda
        Description:   Methods to auto split an OI of type 'Oferta Móvil' (Catalog FVI and BI_FVI_Lineas_de_Portabilidad__c != null) from an Opty
                        when Opty change its stage (StageName) from F2 - Negotiation to F1 Closed Won. Also split the numbers in 
                        BI_FVI_Lineas_de_Portabilidad__c of 'Oferta Móvil' to inform every OI CHild of type 'PLANES'
                        (NE__ProdId__r.Payback_Family__c = 'Planes']) with one of the splitted values.
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        27/06/2016                      Jorge Galindo             Initial version
        21/07/2016                      Jorge Galindo             Added functionality for not split more than a limit declared
        01/08/2016                      Jorge Galindo             Attributes can be null, then , we have to control empty list
        03/08/2016                      Jorge Galindo             Change to avoid problems in Test classes without Catalog or Catalog created after Opportunity
        04/08/2016                      Jorge Galindo             Reduce the DML usage
        05/10/2016                      Jorge Galindo             Instead of only line numbers separated by , now we receive CompanyName1:LineNumber1,CompanyName2:LineNumber2, etc and we have to inform a new field BI_FVI_Oferente_Competencia__c
        13/10/2016                      Jorge Galindo             It's not CompanyName1:LineNumber1, actually is LineNumer1:CompanyName1 , change the split of the two fields. For OI root, if no pending splits, BI_FVI_Lineas_de_Portabilidad__c will be empty 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class BI_FVI_AutoSplitMethods {
    
      public static final integer maxSplit = 49; // maximum detected minus one
      public static integer totalSplitted = 0;
      public static Map<id,NE__OrderItem__c> mapNewOrderItems;
      public static List<NE__OrderItem__c> listOfOitoUpdateQty;
      public static List<NE__OrderItem__c> listClonedRootOrderItems;
      public static List<NE__OrderItem__c> listCloneChilds;
      public static List<NE__Order_Item_Attribute__c> listOIAttributes;
      public static Map<id,List<NE__OrderItem__c>> mapOriginalRootClonedRoot;
      public static Map<id,id> idsOrderItemsToUpdateMap;
      public static List<id> originalIdsOfRootOI;
      public static Map<id,double> remainQtyMap; 
      //04/08/2016 JGL                      
      public static Boolean split;  
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Galindo 
    Company:       New Energy Aborda
    Description:   Method that obtain the OI to split and calls the methods of "Cloning" 
    
    IN:            Void
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    22/07/2016              Jorge Galindo            Initial Version
    03/08/2016              Jorge Galindo           Change to avoid problems in Test classes without Catalog or Catalog created after Opportunity
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
     public static void rootListOrderItemsGenerator(){
            
            remainQtyMap = new Map<id,double>();
            originalIdsOfRootOI = new List<id>();
            
            System.debug('BI_FVI_AutoSplitMethods autoSplit: Map: ' + JSON.serialize(mapNewOrderItems));
            
         
             // JGL 03/08/2016 change to avoid problems in classes without Catalog or Catalog created after Opportunity
            String catalogNameTest = 'FVI';
         
            List<NE__Catalog__c> catalog = [SELECT id,name FROM NE__Catalog__c LIMIT 1];
         
            if(BI_TestUtils.isRunningTest() && !catalog.isEmpty()) catalogNameTest = catalog[0].name;
            
             for(NE__OrderItem__c ordIt:mapNewOrderItems.values()){
                if(ordIt.NE__Catalog__c != null) {
                     // OI has to be from FVI Catalog to be autosplitted and has to be complex product, the root of that complex product,
                     String catalogName = BI_TestUtils.isRunningTest() ? catalogNameTest : ordIt.NE__Catalog__r.Name;
          
                     if(catalogName.contains('FVI')  && ordIt.BI_FVI_Lineas_de_Portabilidad__c != null
                        && ordIt.NE__Root_Order_Item__c == null){
                            
                         // at least one Item has to be "splitted" 
                         split = true;   
                            
                         // Not to launch in every new OI (except in test)
                         if(!Bi_TestUtils.isRunningTest()) NETriggerHelper.setTriggerFired('BI_FVI_AutoSplit');   
                            
                         System.debug('BI_FVI_AutoSplitMethods autoSplit: OI To split: ' + ordIt.id);   
                      
                         //JGL 20/07/2016   
                         remainQtyMap.put(ordIt.id,ordIt.NE__Qty__c);
                            
                         id itemToSplit = ordIt.id;
                         Decimal qtyToSplit = ordIt.NE__Qty__c - 1;
                         ordIt.NE__Qty__c = 1; // if not set to 1, datamap clone the original number
                            
                         System.debug('BI_FVI_AutoSplitMethods: OrderItem to Split ' + ordIt.Name + ' ; To split in ' + qtyToSplit + ' more Order Items');

                            originalIdsOfRootOI.add(ordIt.id);
                         
                            if ((qtyToSplit >= 0) && (itemToSplit != null)){
                                for(Integer i=0; i< qtyToSplit;i++){
                                        
                                    rootOrderItemGeneratorWOInsert(ordIt);    
                                
                                    decimal aux =  remainQtyMap.get(ordIt.id) - 1;
                                    remainQtyMap.remove(ordIt.id);
                                    remainQtyMap.put(ordIt.id,aux);
                                    
                                    totalSplitted += 1;
                                    
                                   System.debug('BI_FVI_AutoSplitMethods totalSplitted: ' + totalSplitted);
                                    
                                    if(totalSplitted > maxSplit){
                                        break;
                                    }
                                    
                                    
                                }
                            }    
                     }

                }
            }
      
     }    
    
    
         /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Galindo 
    Company:       New Energy Aborda
    Description:   Method that clone the Original Root OI and adds to public map
    
    IN:            Void
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    22/07/2016              Jorge Galindo            Initial Version    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
     public static void rootOrderItemGeneratorWOInsert(NE__OrderItem__c orderItemToSplit){
        
        // first we are going to duplicate the root and the OI attribute
        NE__OrderItem__c clonedRoot = new NE__OrderItem__c(CurrencyIsoCode = orderItemToSplit.CurrencyIsoCode,
                                                           NE__Account__c = orderItemToSplit.NE__Account__c,
                                                           NE__Action__c = orderItemToSplit.NE__Action__c,
                                                           NE__Catalog__c = orderItemToSplit.NE__Catalog__c,
                                                           NE__CatalogItem__c =  orderItemToSplit.NE__CatalogItem__c,
                                                           NE__Root_Order_Item__c = null,
                                                           NE__Qty__c = 1,
                                                           BI_FVI_Lineas_de_Portabilidad__c = '',
                                                           NE__ProdId__c = orderItemToSplit.NE__ProdId__c,
                                                           NE__OrderId__c = orderItemToSplit.NE__OrderId__c,
                                                           NE__Billing_Account__c = orderItemToSplit.NE__Billing_Account__c,
                                                           NE__One_Time_Cost__c = orderItemToSplit.NE__One_Time_Cost__c,
                                                           NE__OneTimeFeeOv__c = orderItemToSplit.NE__OneTimeFeeOv__c,
                                                           NE__RecurringChargeOv__c = orderItemToSplit.NE__RecurringChargeOv__c,
                                                           NE__RecurringChargeFrequency__c = orderItemToSplit.NE__RecurringChargeFrequency__c,
                                                           NE__Service_Account__c = orderItemToSplit.NE__Service_Account__c,
                                                           NE__Status__c = orderItemToSplit.NE__Status__c,
                                                           Configuration_SubType__c = orderItemToSplit.Configuration_SubType__c
                                                           );             
         
         if(mapOriginalRootClonedRoot.containsKey(orderItemToSplit.id)){
             mapOriginalRootClonedRoot.get(orderItemToSplit.id).add(clonedRoot);
         }else{
             mapOriginalRootClonedRoot.put(orderItemToSplit.id,new List<Ne__OrderItem__c>{clonedRoot});
         }
        
       
    
    }

    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Galindo 
    Company:       New Energy Aborda
    Description:   Method that creates the Order Item Atribute for Root Cloned Items 
    
    IN:            Void
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    22/07/2016              Jorge Galindo            Initial Version
    01/08/2016              Jorge Galindo            Attributes can be null, then , we have to control empty list
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void rootOrderItemAttributesGenerator(){
        
         // we need unique ids of parent Root OI
         
         Map<id,id> auxMapForCleaning = new Map<id,id>();
         
         for(id parentId:idsOrderItemsToUpdateMap.values()){
             if(!auxMapForCleaning.containsKey(parentId)){
                 auxMapForCleaning.put(parentId,parentId);
             } 
         }
        
        
         List<NE__Order_Item_Attribute__c> totalOIAttributesForRoots = [SELECT id,Name,
                                                            CurrencyIsoCode,
                                                            NE__Action__c,
                                                            NE__FamPropExtId__c,
                                                            NE__FamPropId__c,
                                                            NE__Old_Value__c,
                                                            NE__Order_Item__c,
                                                            NE__Previous_Attribute_Value__c,
                                                            NE__Value__c             
                                                            FROM NE__Order_Item_Attribute__c where NE__Order_Item__c = :auxMapForCleaning.values()];

        
        // Originally, at least one attribute has to be created, but some cases is not, then, we have to control the possiblity of null
        
        if(!totalOIAttributesForRoots.isEmpty()){
             
            //Now, we create a map with Parent Id from every child as key
             Map<id,List<NE__Order_Item_Attribute__c>> mapParentIdChildsOIA = new  Map<id,List<NE__Order_Item_Attribute__c>>();
             
             for(NE__Order_Item_Attribute__c oia:totalOIAttributesForRoots){
                 if(mapParentIdChildsOIA.containsKey(oia.NE__Order_Item__c)){
                     mapParentIdChildsOIA.get(oia.NE__Order_Item__c).add(oia);
                 }else{
                     mapParentIdChildsOIA.put(oia.NE__Order_Item__c,new List<NE__Order_Item_Attribute__c>{oia});
                 }
             }
            
            
            // now we are going to follow the map clonedRootOI - OriginalRootOI, then for every new rootOI we are going to retrieve
             // form the  mapParentIdChildsOI the list of the Child Order Items, then clone them with the cloned root id as parent
             
             
             for(id clonedRootId : idsOrderItemsToUpdateMap.keySet()){
                 for(NE__Order_Item_Attribute__c oiaToClone: mapParentIdChildsOIA.get(idsOrderItemsToUpdateMap.get(clonedRootId))){
                    
                     NE__Order_Item_Attribute__c clonedAttribute = new NE__Order_Item_Attribute__c(
                                                                Name = oiaToClone.Name,
                                                                CurrencyIsoCode = oiaToClone.CurrencyIsoCode,
                                                                NE__Action__c = oiaToClone.NE__Action__c,
                                                                NE__FamPropExtId__c = oiaToClone.NE__FamPropExtId__c,
                                                                NE__FamPropId__c = oiaToClone.NE__FamPropId__c,
                                                                NE__Old_Value__c = oiaToClone.NE__Old_Value__c,
                                                                NE__Order_Item__c = clonedRootId,
                                                                NE__Previous_Attribute_Value__c = oiaToClone.NE__Previous_Attribute_Value__c,
                                                                NE__Value__c = oiaToClone.NE__Value__c);
                     
                     listOIAttributes.add(clonedAttribute);
                    
                 }
             }
            
        }
 
        
    }
    
    
    
         /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Galindo 
    Company:       New Energy Aborda
    Description:   Method that creates the childs of every cloned Root Item and adds to a public list 
    
    IN:            Void
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    22/07/2016              Jorge Galindo           Initial Version   
    19/06/2017              Humberto Nunes          Se Agrego el campo BI_FVI_Country__c y se igualo al de la Cuenta asociada ya que se usa para poder 
                                                    rellenar mas adelante la empresa cedente (BI_FVI_Oferente_Competencia__c) que depende del Pais. 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
     public static void childOrderItemGenerator(){
         
         // we need unique ids of parent Root OI
         
         Map<id,id> auxMapForCleaning = new Map<id,id>();
         
         for(id parentId:idsOrderItemsToUpdateMap.values()){
             if(!auxMapForCleaning.containsKey(parentId)){
                 auxMapForCleaning.put(parentId,parentId);
             } 
         }
         
         List<NE__OrderItem__c> totalChildsOI = [SELECT id,Name,NE__Catalog__r.Name,
                                                            NE__OrderId__r.NE__OptyId__c, 
                                                            CurrencyIsoCode,
                                                            NE__Account__c,
                                                            NE__Action__c,
                                                            NE__Catalog__c,
                                                            NE__CatalogItem__c,
                                                            NE__Root_Order_Item__c,
                                                            NE__Qty__c,
                                                            BI_FVI_Lineas_de_Portabilidad__c,
                                                            NE__ProdId__c,
                                                            NE__OrderId__c,
                                                            NE__Billing_Account__c,
                                                            NE__One_Time_Cost__c,
                                                            NE__OneTimeFeeOv__c,
                                                            NE__RecurringChargeOv__c,
                                                            NE__RecurringChargeFrequency__c,
                                                            NE__Service_Account__c,
                                                            NE__Status__c,
                                                            Configuration_SubType__c 
                                                            FROM NE__OrderItem__c where NE__Parent_Order_Item__c = :auxMapForCleaning.values()];
         
         //Now, we create a map with Parent Id from every child as key
         Map<id,List<NE__OrderItem__c>> mapParentIdChildsOI = new  Map<id,List<NE__OrderItem__c>>();
         
         for(NE__OrderItem__c oi:totalChildsOI){
             if(mapParentIdChildsOI.containsKey(oi.NE__Root_Order_Item__c)){
                 mapParentIdChildsOI.get(oi.NE__Root_Order_Item__c).add(oi);
             }else{
                 mapParentIdChildsOI.put(oi.NE__Root_Order_Item__c,new List<NE__OrderItem__c>{oi});
             }
         }
         
         
         System.debug('BI_FVI_AutoSplit METHODS MAP auxReverseMapForChilds: ' + JSON.serialize(idsOrderItemsToUpdateMap));
         
         // now we are going to follow the map clonedRootOI - OriginalRootOI, then for every new rootOI we are going to retrieve
         // form the  mapParentIdChildsOI the list of the Child Order Items, then clone them with the cloned root id as parent
         
         
         for(id clonedRootId : idsOrderItemsToUpdateMap.keySet()){
             if( mapParentIdChildsOI.containsKey(idsOrderItemsToUpdateMap.get(clonedRootId))){
             for(NE__OrderItem__c oiChild: mapParentIdChildsOI.get(idsOrderItemsToUpdateMap.get(clonedRootId))){
                 
                 NE__OrderItem__c clonedChild = new NE__OrderItem__c(CurrencyIsoCode = oiChild.CurrencyIsoCode,
                                                           NE__Account__c = oiChild.NE__Account__c,
                                                           NE__Action__c = oiChild.NE__Action__c,
                                                           NE__Catalog__c = oiChild.NE__Catalog__c,
                                                           NE__CatalogItem__c =  oiChild.NE__CatalogItem__c,
                                                           NE__Root_Order_Item__c = clonedRootId,
                                                           NE__Parent_Order_Item__c = clonedRootId,         
                                                           NE__Qty__c = 1,
                                                           BI_FVI_Lineas_de_Portabilidad__c = '',
                                                           NE__ProdId__c = oiChild.NE__ProdId__c,
                                                           NE__OrderId__c = oiChild.NE__OrderId__c,
                                                           NE__Billing_Account__c = oiChild.NE__Billing_Account__c,
                                                           NE__One_Time_Cost__c = oiChild.NE__One_Time_Cost__c,
                                                           NE__OneTimeFeeOv__c = oiChild.NE__OneTimeFeeOv__c,
                                                           NE__RecurringChargeOv__c = oiChild.NE__RecurringChargeOv__c,
                                                           NE__RecurringChargeFrequency__c = oiChild.NE__RecurringChargeFrequency__c,
                                                           NE__Service_Account__c = oiChild.NE__Service_Account__c,
                                                           NE__Status__c = oiChild.NE__Status__c,
                                                           Configuration_SubType__c = oiChild.Configuration_SubType__c
                                                           );
            
                listCloneChilds.add(clonedChild);
             }
             }    
         }
            
     }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Galindo 
    Company:       New Energy Aborda
    Description:   Method that launch Autosplit if Opty is changing from F2 to F1 Closed Won, and it's an Oferta móvil
    
    IN:            ApexTrigger.New, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/07/2016              Jorge Galindo            Initial Version    
    28/07/2016              Guillermo Muñoz         Fix bugs on triggers desactivations
    06/08/2016              Humberto Nunes          Se Omitio el if(Test.isRunningTest()) ya que para las pruebas siempre estaba entrando y debe cumplir las condiciones para ejecutarse, solo para opp de tipo BI_FVI_Ciclo_completo.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void autoSplit(Map<Id, Opportunity> optyNews, Map<Id, Opportunity> optyOlds){  

       boolean wasFired =   NETriggerHelper.getTriggerFired('BI_FVI_AutoSplit');
    
       system.debug('BI_FVI_AutoSplit METHODS ENTRY ');
       system.debug('*wasFired ' + wasFired + ' !wasFired ' + !wasFired);
        
        if(!wasFired){ 
            // if there's nothing to split, nothing to be done
            split = false;
            
                // we are going only to update the OI of those Opportunities which have change the status from one to Oferta Sincronizada
                
                List<id> idsOptysToProcess = new List<id>();
                           
                for(Opportunity optyNew: OptyNews.values()){
                    // In test other triggers are failing in changing state
                    //if(Test.isRunningTest()){
                    //    idsOptysToProcess.add(optyNew.id);
                    //    break;
                    //}
                    
                    if((optyNew.StageName == Label.BI_F1Ganada) && optyOlds.get(optyNew.Id).StageName.contains('F2') && optyNew.RecordTypeId == TGS_RecordTypes_Util.getRecordTypeId(Opportunity.sObjectType, 'BI_FVI_Ciclo_completo')){
                        idsOptysToProcess.add(optyNew.id);
                        //GMN 28/07/2016 - Moved to fix bugs on triggers desactivations
                        // we are going to avoid the OI triggers because when using the data map all are launched, obtaining when the 
                        // quantity of OI to split is high a Too Many Queries exception and itself to avoid loops
                        NETriggerHelper.setTriggerFired('BI_FVIBorrar');
                        NETriggerHelper.setTriggerFired('NECheckDeltaQuantity');
                        NETriggerHelper.setTriggerFired('NECheckOrderItems');
                        NETriggerHelper.setTriggerFired('NESetDefaultOIFields');
                        NETriggerHelper.setTriggerFired('TGS_Order_Trigger');
                        NETriggerHelper.setTriggerFired('TGS_Order_Handler.NeCheckOrder');
                        NETriggerHelper.setTriggerFired('BI_OpportunityMethods.createTask');
                        NETriggerHelper.setTriggerFired('NECheckOptyFields');
                        NETriggerHelper.setgenerateCase(false); 
                    }
                }
            
            // 04/08/2016 only try to do the split if there's at least one Opty accomplishing requirements
            if(!idsOptysToProcess.isEmpty()){  
                
                // we create a map to register the original OrderItem and its splitted orders to inform in Splitted - Original format
                idsOrderItemsToUpdateMap = new Map<id,id>();
            
                mapNewOrderItems = new Map<id,NE__OrderItem__c>([SELECT id,Name,NE__Catalog__r.Name,
                                                                NE__OrderId__r.NE__OptyId__c, 
                                                                CurrencyIsoCode,
                                                                NE__Account__c,
                                                                NE__Action__c,
                                                                NE__Catalog__c,
                                                                NE__CatalogItem__c,
                                                                NE__Root_Order_Item__c,
                                                                NE__Qty__c,
                                                                BI_FVI_Lineas_de_Portabilidad__c,
                                                                NE__ProdId__c,
                                                                NE__OrderId__c,
                                                                NE__Billing_Account__c,
                                                                NE__One_Time_Cost__c,
                                                                NE__OneTimeFeeOv__c,
                                                                NE__RecurringChargeOv__c,
                                                                NE__RecurringChargeFrequency__c,
                                                                NE__Service_Account__c,
                                                                NE__Status__c,
                                                                Configuration_SubType__c
                                                                FROM NE__OrderItem__c where NE__OrderId__r.NE__OptyId__c IN :idsOptysToProcess]);
                
                listOfOitoUpdateQty = new List<NE__OrderItem__c>();
                listCloneChilds = new List<NE__OrderItem__c>();
                listOIAttributes = new List<NE__Order_Item_Attribute__c>();
                mapOriginalRootClonedRoot = new Map<id,List<NE__OrderItem__c>>(); 
            
                
                //clone the rootItems
                rootListOrderItemsGenerator();  
            
                // if at least one item has to be splitted
                if(split){
                             
                        listClonedRootOrderItems = new List<Ne__OrderItem__c>();
                        
                        // once created all the roots we have to insert
                        for(id originalRoot:mapOriginalRootClonedRoot.keySet()){
                            listClonedRootOrderItems.addAll(mapOriginalRootClonedRoot.get(originalRoot));
                        }
                        
                        if(!listClonedRootOrderItems.isEmpty()){
                            insert  listClonedRootOrderItems;
                        }
                        
                        
                        // in the map we have the ids of the new cloned roots
                        for(id originalRoot:mapOriginalRootClonedRoot.keySet()){
                            for(NE__OrderItem__c clonedRootItem: mapOriginalRootClonedRoot.get(originalRoot)){
                                 idsOrderItemsToUpdateMap.put(clonedRootItem.id,originalRoot);
                            }
                        }
                        
                        
                        System.debug('BI_FVI_AutoSplit METHODS MAP idsOrderItemsToUpdateMap: ' + JSON.serialize(idsOrderItemsToUpdateMap));
                        
                        // creating the childs
                        childOrderItemGenerator();
                        
                        if(!listCloneChilds.isEmpty()){
                           insert listCloneChilds;
                        }
                        
                        // creating the root Order Item Attributes
                        rootOrderItemAttributesGenerator();
                        
                        if(!listOIAttributes.isEmpty()){
                            insert listOIAttributes;
                        }
                    
                        
                        // now we add the original OI to list for updating IMPORTANT
                        for(id origId:originalIdsOfRootOI){
                            idsOrderItemsToUpdateMap.put(origId,origId);
                        }
                        
                        // NOW STARTS OLD CODE ********************************************************
                        // For each key in the map of OrderItem to update, we have to retrieve the field  BI_FVI_Lineas_de_Portabilidad__c from the original
                        // split it (it's in XXXX;XXXX;XXXX format) an leave the first value in the original OI, and the next values in every 
                        // field BI_FVI_Service_Number_Id__c in the childs OI which payback_family = “Planes”. 
                        
                        System.debug('BI_FVI_AutoSplitMethods ids para buscar hijos: ' + JSON.serialize(idsOrderItemsToUpdateMap.values()));
            
                        
                        List<NE__OrderItem__c> childOIList = [SELECT id,Name,NE__Catalog__c,NE__CatalogItem__c,NE__Root_Order_Item__c,NE__Qty__c,BI_FVI_Lineas_de_Portabilidad__c,NE__ProdId__c,BI_FVI_Service_Number_Id__c, BI_FVI_Country__c, NE__Account__r.BI_Country__c  FROM NE__OrderItem__c where NE__Root_Order_Item__c IN :idsOrderItemsToUpdateMap.keySet() AND NE__ProdId__r.Payback_Family__c = 'Planes'];
                        
                        // We are going to iterate  through this child list to update, for avoid nested fors we are going to create a String.
                        // Inside qe have to put the id of the original record that was splitted, and the number of times it appears is the position
                        // of the splitted string we have to inform
                        String splittedTimes = '';
                    
                        for(NE__OrderItem__c oIChild : childOIList){
                            id originalId = idsOrderItemsToUpdateMap.get(oiChild.NE__Root_Order_Item__c);
                    
                            String toSplit = mapNewOrderItems.get(originalId).BI_FVI_Lineas_de_Portabilidad__c;
                            
                            List<String> splittedValues = toSplit.split(',');
                           
                            if(oIChild.BI_FVI_Service_Number_Id__c== null || oIChild.BI_FVI_Service_Number_Id__c== ''){
                                // JGL 05/10/2016 new structure, portability lines comes as Company1:line1,Company2:line2, etc
                                // Company has to fill BI_FVI_Oferente_Competencia__c and lines as usual
                                //oIChild.BI_FVI_Service_Number_Id__c = splittedValues.get(splittedTimes.countMatches(originalId));
                                
                                List<String> splittedCompanyAndLine = splittedValues.get(splittedTimes.countMatches(originalId)).split(':');
                                
                                 // once splitted , if size is greater than 1, we have the new structure, if not, as usual
                                oIChild.BI_FVI_Country__c = oIChild.NE__Account__r.BI_Country__c;
                                
                                if(splittedCompanyAndLine.size() > 1){
                                    // JGL 13/10/2016 order modified
                                    //String company = splittedCompanyAndLine.get(0);
                                    //String line = splittedCompanyAndLine.get(1);	
                                    String company = splittedCompanyAndLine.get(1);
                                    String line = splittedCompanyAndLine.get(0);
                                    // END JGL 13/10/2016 order modified
                                    
                                    oIChild.BI_FVI_Service_Number_Id__c = line;

                                    // it comes as 'undefined' from app if is not a portability
                                    if(company != 'undefined'){
                                       oIChild.BI_FVI_Oferente_Competencia__c = company;
                                    }
                                    
                                }else{
                                    oIChild.BI_FVI_Service_Number_Id__c = splittedValues.get(splittedTimes.countMatches(originalId));
                                } 	
                                // END JGL
                                
                            }               
                   
                            splittedTimes += originalId + ';';
                            
                            System.debug('BI_FVI_AutoSplitMethods: OI: ' + oIChild.id + '; Splitted: ' + oIChild.BI_FVI_Service_Number_Id__c);
                        }
                    
                        // we have to update also the original OI and copies because we have change the quantity to 1
                        // Also, we have to delete in the BI_FVI_Lineas_de_Portabilidad__c the ones that have been informed in the new childs
                    
                        for(NE__OrderItem__c OIforQty:[SELECT id,NE__Qty__c,BI_FVI_Lineas_de_Portabilidad__c from NE__OrderItem__c where id IN :idsOrderItemsToUpdateMap.keySet()]){
                            if(remainQtyMap.containsKey(OIforQty.id)){
                                OIforQty.NE__Qty__c = remainQtyMap.get(OIforQty.id);
                                
                                // here, where we have only the original roots , we have to retrieve the BI_FVI_Lineas_de_Portabilidad__c and let
                                // only the lines that have not been splitted
                                String originalLineas = OIforQty.BI_FVI_Lineas_de_Portabilidad__c;
                                List<String> splittedLineasValues = originalLineas.split(',');
                                
                                // first number remains in the list
                                String newLineas = splittedLineasValues.get(0) + ',';
                                
                                for(Integer x=0; x < remainQtyMap.get(OIforQty.id) - 1; x++){
                                    newLineas += splittedLineasValues.get(splittedLineasValues.size() - x - 1) + ',';
                                }
                                
                                // remove the last comma
                                newLineas = newLineas.substring(0, newLineas.length()-1);
                                
                                // JGL 13/10/2016
                                // for root OI, if it has pending splites, they remain , if not, the field is set empty
                                // OIforQty.BI_FVI_Lineas_de_Portabilidad__c = newLineas;
                                
                                if(remainQtyMap.get(OIforQty.id) == 1){
                                    OIforQty.BI_FVI_Lineas_de_Portabilidad__c = '';
                                }else{
                                    OIforQty.BI_FVI_Lineas_de_Portabilidad__c = newLineas;
                                }
                          		// END JGL 13/10/2016      
                                
                                
                                
                            }else{
                                OIforQty.NE__Qty__c = 1;    
                            }
                            
                            childOIList.add(OIforQty);    
                        }
                    
                        System.debug('BI_FVI_AutoSplitMethods: Final: ' + JSON.serialize(childOIList));
                        update childOIList;
                        
                        // if totalsplitted is greater than the limit means that autosplit is not complete and optys have to return to F2 Stage
                        if(totalSplitted > maxSplit){
                             
                            List<Opportunity> optyToChangeState = new  List<Opportunity>();
                            
                             for(Opportunity optyNew: [SELECT id,StageName,BI_Fecha_de_cierre_real__c from Opportunity where id IN :OptyNews.keySet()] ){
                                optyNew.StageName = 'F2 - Negotiation'; 
                                optyNew.BI_Fecha_de_cierre_real__c = null;
                                optyToChangeState.add(optyNew); 
                             }
                        
                             update optyToChangeState;
                        }
                        // end original code
                    
                    
                }
               
            
            }//end there are otpys to process
        
       }//end was fired 
        
    }
    
    
    
    
}