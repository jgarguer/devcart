/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for BI_COL_CreateUserOnTRS_cls
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-30      Raul Mora (RM)                  Create Class  
*    
*                    20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*    
*    
*************************************************************************************/
@isTest
private class BI_COL_CreateUserOnTRS_tst
{
    Public static list<Profile>                                             lstPerfil;
    Public static list <UserRole>                                           lstRoles;
    Public static User                                                  objUsuario;

    public List<Account> createData()
    {
        BI_bypass__c objBibypass = new BI_bypass__c();
        objBibypass.BI_migration__c=true;
        insert objBibypass; 

        ////lstPerfil
  //      lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];

  //      //lstRol
  //      lstRoles = new list <UserRole>();
  //      lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

  //      //ObjUsuario
  //      objUsuario = new User();
  //      objUsuario.Alias = 'standt';
  //      objUsuario.Email ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey = '';
  //      objUsuario.LastName ='Testing';
  //      objUsuario.LanguageLocaleKey ='en_US';
  //      objUsuario.LocaleSidKey ='en_US'; 
  //      objUsuario.ProfileId = lstPerfil.get(0).Id;
  //      objUsuario.TimeZoneSidKey ='America/Los_Angeles';
  //      objUsuario.UserName ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey ='UTF-8';
  //      objUsuario.UserRoleId = lstRoles.get(0).Id;
  //      objUsuario.BI_Permisos__c ='Sucursales';
  //      objUsuario.Pais__c='Colombia';
  //      insert objUsuario;

  //      list<User> lstUsuarios = new list<User>();
  //      lstUsuarios.add(objUsuario);

        List<Account> lstAccReturn = new List<Account>();
        Account objAcc = new Account();
        objAcc.Name = 'Test Account';
        objAcc.BI_Country__c = 'Colombia';
        objAcc.TGS_Region__c = 'América';
        objAcc.BI_Tipo_de_identificador_fiscal__c = 'CC';
        objAcc.BI_No_Identificador_fiscal__c = '1234567787';
        objAcc.BI_Activo__c = 'Si';
        objAcc.BI_Segment__c                         = 'test';
         objAcc.BI_Subsegment_Regional__c             = 'test';
          objAcc.BI_Territory__c                       = 'test';
        lstAccReturn.add( objAcc );
        
        Account objAcc2 = new Account();
        objAcc2.Name = 'Tes2t Account2';
        objAcc2.BI_Country__c = 'Colombia';
        objAcc2.TGS_Region__c = 'América';
        objAcc2.BI_Tipo_de_identificador_fiscal__c = 'CC';
        objAcc2.BI_No_Identificador_fiscal__c = '1234567788';
        objAcc2.BI_Activo__c = 'Si';
        objAcc2.BI_Segment__c                         = 'test';
        objAcc2.BI_Subsegment_Regional__c             = 'test';
        objAcc2.BI_Territory__c                       = 'test';
        lstAccReturn.add( objAcc2 );
        
        insert lstAccReturn;
        
        AccountTeamMember objAccTeamMemb = new AccountTeamMember();
        objAccTeamMemb.AccountId = objAcc.Id;
        objAccTeamMemb.TeamMemberRole ='Service Manager';
        objAccTeamMemb.UserId = UserInfo.getUserId();
        insert objAccTeamMemb;
        
        AccountTeamMember objAccTeamMemb2 = new AccountTeamMember();
        objAccTeamMemb2.AccountId = objAcc2.Id;
        objAccTeamMemb2.TeamMemberRole ='Service Manager';
        objAccTeamMemb2.UserId = UserInfo.getUserId();
        insert objAccTeamMemb2;
        
        return lstAccReturn;
    }
    
    static testMethod void myUnitTest() 
    {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
            BI_COL_CreateUserOnTRS_tst testClass = new BI_COL_CreateUserOnTRS_tst();
            List<Account> lstAcc = testClass.createData();
            
            List<String> lstIdAcc = new List<String>();
            
            for( Account objAc : lstAcc )
            {
                lstIdAcc.add( objAc.Id );
            }
            Test.startTest();
                BI_COL_CreateUserOnTRS_cls.invokeapexcallout( lstAcc );
            Test.stopTest();
        }       
    }
}