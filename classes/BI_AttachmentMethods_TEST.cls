@isTest
private class BI_AttachmentMethods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_AttachmentMethods class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    11/09/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static{
        
        BI_TestUtils.throw_exception = false;
    }

 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @testSetup
    static void loadData()
    {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_TestUtils.throw_exception = false;
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();      
        List<String>lst_region = new List<String>();
        lst_region.add('Perú');
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('PER');  
        List <Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_region);

        List <NE__Contract__c> lst_con = new List<NE__Contract__c>();
        List <NE__Contract_Header__c> lst_head = new List<NE__Contract_Header__c>();
        List <NE__Product__c> lst_prod = new List<NE__Product__c>();
        List <NE__Contract_Line_Item__c> lst_cli = new List<NE__Contract_Line_Item__c>();

        Integer i = 2;

        for(Integer j=0; j<i; j++){
            NE__Contract_Header__c header = new NE__Contract_Header__c(NE__Actual_Qty__c = 3,
                                                                       NE__Delta_Qty__c = 2,
                                                                       NE__Name__c ='Header'+String.valueOf(j));
            lst_head.add(header);           
            NE__Product__c prod = new NE__Product__c(Name = 'Prod');       
            lst_prod.add(prod);
        }
        insert lst_head;

        NE__Contract_Account_Association__c newCAA = new NE__Contract_Account_Association__c(
            NE__Contract_Header__c = lst_head[0].id,
            NE__Account__c = lst_acc[0].id
        );
        insert newCAA;

        insert lst_prod;
        
        for(Integer k=0; k<i; k++){
            NE__Contract__c contract = new NE__Contract__c(NE__Contract_Header__c = lst_head[k].Id,
                                                           NE__Status__c = 'Active');
            lst_con.add(contract);
        }
        insert lst_con;
        for(Integer l=0; l<i; l++){
            NE__Contract_Line_Item__c line = new NE__Contract_Line_Item__c(NE__Contract__c = lst_con[l].Id,
                                                                           NE__Commercial_Product__c = lst_prod[l].Id,
                                                                           NE__Allow_Qty_Override__c = false);
            lst_cli.add(line);
        }        
        insert lst_cli;

        Attachment newAttc = new Attachment();
            newAttc.parentId = lst_con[0].Id;
            newAttc.Name = 'Titulo';
            newAttc.body = Blob.valueOf('Texto');
        insert newAttc;

        Id profileId = [SELECT ID FROM PROFILE WHERE Name = 'BI_Standard_PER' limit 1][0].Id;
        BI_Dataload.MAX_LOOP = 3;
        List<User> lst_user = BI_Dataload.loadUsers(3, profileId, 'Administrador de Contrato');
        lst_user[1].BI_Permisos__c = 'Super Usuario';
        lst_user[2].BI_Permisos__c = 'Ejecutivo de Cliente';
        update lst_user;
        /*
        BI_Dataload.loadUsers(1, profileId, 'Super Usuario');
        BI_Dataload.loadUsers(1, profileId, 'Ejecutivo de Cliente');
        */
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_AttachmentMethods.validateImageAttachment
    History:
    
    <Date>              <Author>                <Description>
    11/09/2014          Ignacio Llorca          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void validateImageAttachmentTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1);
              
        List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);
        
        BI_Videoteca__c vid = new BI_Videoteca__c(Name='Video Test',BI_URL__c='www.google.es');
        
        insert vid;
        
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        
        Blob bodyBlob2=Blob.valueOf('UnitTestAttachmentBody 2');
        
        Attachment attach=new Attachment(Name='Unit Test Attachment',
                                        body=bodyBlob,
                                        parentId=vid.Id, 
                                        ContentType='image/png');  
        
        insert attach;
        
        
         Attachment attach2=new Attachment(Name='Unit Test Attachment 2',
                                        body=bodyBlob2,
                                        parentId=vid.Id);  
        
       try{       
        insert attach2;
        system.assert(false);        
       }catch(exception e){
        system.assertequals(true, e.getMessage().contains(Label.BI_AttachmentExist));
       }
        
        

    }

    /*
        COMENTADO POR COE, ERROR EN PRODUCCION:
        System.DmlException: Insert failed. First exception on row 0; first error: CANNOT_EXECUTE_FLOW_TRIGGER, The record couldn’t be saved because it failed to trigger a flow. <br>A flow trigger failed to execute the flow with version ID 301w00000000Jhe. <br/> Contact your administrator for help.: [] 
        Stack Trace: Class.BI_AttachmentMethods_TEST.sendEmailTest: line 116, column 1
    */
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_AttachmentMethods.sendEmail
    History:
    
    <Date>              <Author>                <Description>
    11/09/2014          Ignacio Llorca          Initial version
    02/03/2015          Micah Burgos            Remake
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    /*static testMethod void sendEmailTest() {

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        User usr;
        
        User u = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(u)
        {
            usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(), BI_DataLoad.searchAdminProfile());
        }
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(2);
            List<Account> lst_acc = BI_DataLoad.loadAccounts(2, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: lst_acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalPlusProfile());    
            system.runAs(user1){

                List<Case> lst_cases = new list<Case>();
            
                for(Integer i=0; i<2; i++){
                    Case cas = new Case();
                    cas.Type = Label.BI_CaseType_Question;
                    cas.Subject = 'Caso test';
                    cas.BI_Otro_Tipo__c = 'test';
                    //cas.ContactId = lst_con[0].Id;
                    cas.AccountId = lst_acc[0].Id;
                    cas.Status = 'Assigned';
                    lst_cases.add(cas);
                }

                insert lst_cases;

                List<Case> lst_cases2 = [SELECT Id FROM Case WHERE Id IN: lst_cases];
                system.assert(!lst_cases2.isEmpty());
                
                Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
                
                Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body 2');
                
                Test.startTest();
                
                Attachment attach=new Attachment(Name='Unit Test Attachment',
                                                body=bodyBlob,
                                                parentId=lst_cases2[0].Id, 
                                                ContentType='image/png');  
                
                insert attach;
                
                
                Attachment attach2=new Attachment(Name='Unit Test Attachment 2',
                                                body=bodyBlob2,
                                                parentId=lst_cases2[1].Id);  
                
                insert attach2;
                
                BI_Log__c [] lst_log = [SELECT Id,  BI_Asunto__c,BI_Bloque_funcional__c,BI_Descripcion__c,BI_Tipo_de_componente__c  FROM BI_Log__c];
                if(!lst_log.isEmpty()){
                    system.debug('Test Exception: ' + lst_log);
                }
                //system.assert(lst_log.isEmpty());
                
                Test.stopTest();
            } 
        }      
    }*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_AttachmentMethods.countAttachments
    History:
    
    <Date>              <Author>                <Description>
    21/10/2014          Antonio Moruno          Initial version
    07/04/2015          Juan Santisi            Refactored BI_Pais__c for BI_Country__c and removed lookup dependency.
    ------------------------------------------------------------------------------------- -------------------------------------------------------------------*/
    static testMethod void countAttachTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);

        BI_MigrationHelper.skipAllTriggers();//JEG
        List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
        
        Opportunity opp = new Opportunity(Name = 'Test2',
                                                  CloseDate = Date.today(),
                                                  StageName = Label.BI_F6Preoportunidad,
                                                  AccountId = lst_acc[0].Id,
                                                  BI_Ciclo_ventas__c = Label.BI_Completo,
                                                  BI_Per_Cantidad_de_archivos__c=2,
                                                  BI_Country__c =  lst_pais[0]);
                                                  
        Opportunity oppX = new Opportunity(Name = 'Test2',
                                                  CloseDate = Date.today(),
                                                  StageName = Label.BI_F6Preoportunidad,
                                                  AccountId = lst_acc[0].Id,
                                                  BI_Ciclo_ventas__c = Label.BI_Completo,
                                                  BI_Per_Cantidad_de_archivos__c=null,
                                                  BI_Country__c =  lst_pais[0]);
                                                  
        insert new List<Opportunity>{opp, oppX};
        BI_MigrationHelper.cleanSkippedTriggers();//JEG
        Test.startTest();    
            
        List <Attachment> lst_att = new List <Attachment>();

        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        for(Integer i=0;i<100;i++){
            Attachment attach=new Attachment(Name='Unit Test Attachment',
                                        body=bodyBlob,
                                        parentId=opp.Id, 
                                        ContentType='image/png'); 

                                        
            lst_att.add(attach);
        }
        insert lst_att;
        
        for(Attachment att:lst_att){
            att.Name='New attach';
        }        
        update lst_att;
        Opportunity opp1 = [SELECT Id, BI_Per_Cantidad_de_archivos__c FROM Opportunity WHERE Id =: opp.Id];

        system.assertequals(opp1.BI_Per_Cantidad_de_archivos__c, 102);

        delete lst_att;
        Opportunity opp2 = [SELECT Id, BI_Per_Cantidad_de_archivos__c FROM Opportunity WHERE Id =: opp.Id];

        system.assertequals(opp2.BI_Per_Cantidad_de_archivos__c, 2);
        
        
        Attachment attach2=new Attachment(Name='Unit Test Attachment',
                                    body=bodyBlob,
                                    parentId=oppX.Id, 
                                    ContentType='image/png');
                                    
        insert attach2;
        Opportunity opp3 = [SELECT Id, BI_Per_Cantidad_de_archivos__c FROM Opportunity WHERE Id =: oppX.Id];
        system.assertequals(opp3.BI_Per_Cantidad_de_archivos__c, 1);
        Test.stopTest();
    }
  
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_AttachmentMethods.preventDelete
    History:
    
    <Date>              <Author>                <Description>
    17/11/2014          Pablo Oliva             Initial version
    ------------------------------------------------------------------------------------- -------------------------------------------------------------------*/
    static testMethod void preventDeleteTest() {
        
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        User user = [select Id from User where Id = :UserInfo.getUserId() limit 1];
        
        user.Pais__c = Label.BI_Ecuador;
        user.BI_Permisos__c = Label.BI_Ejecutivo_de_Cliente;
        
        update user;
        BI_MigrationHelper.skipAllTriggers();
        system.runAs(user){
            
            List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
               
            List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
        
            Opportunity opp = new Opportunity(Name = 'Test2',
                                              CloseDate = Date.today(),
                                              StageName = Label.BI_F6Preoportunidad,
                                              AccountId = lst_acc[0].Id,
                                              BI_Ciclo_ventas__c = Label.BI_Completo,
                                              BI_Per_Cantidad_de_archivos__c=2,
                                              BI_Country__c =  lst_pais[0]);
        
            insert opp;

            List <Attachment> lst_att = new List <Attachment>();

            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            for(Integer i=0;i<200;i++){
                Attachment attach=new Attachment(Name='Unit Test Attachment',
                                                 body=bodyBlob,
                                                 parentId=opp.Id, 
                                                 ContentType='image/png'); 

                lst_att.add(attach); 
            }
            
            insert lst_att;
            BI_MigrationHelper.cleanSkippedTriggers();
            Test.startTest();
            
            try{
                delete lst_att;
                system.assert(false);
            }catch(DMLException exc){
                system.assert (exc.getMessage().contains(Label.BI_PreventDeleteAttachment));
            }
            
            Test.stopTest();
            
        }
        
    }

/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_AttachmentMethods.sendEmail
    History:
    
    <Date>              <Author>                <Description>
    19/10/2015          Guillermo Muñoz         Initial version
    13/01/2017          Pedro Párraga           Add User whith Profile 'BI_Customer_Community_Plus'
    25/09/2017          Jaime Regidor           Fields BI_Subsector__c and BI_Sector__c added

    ------------------------------------------------------------------------------------- -------------------------------------------------------------------*/
    static testMethod void sendEmailTest(){

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_MigrationHelper.skipAllTriggers();
        //User usu = [SELECT Id FROM User WHERE Profile.Name = 'BI_Customer_Community_Plus' AND IsActive=true LIMIT 1];
        List<Profile> lst_profile = [select Id from Profile where Name = 'BI_Customer_Community_Plus' limit 1];

             Account acc = new Account(Name = 'test',
                                        BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                        BI_Activo__c = Label.BI_Si,
                                        BI_Country__c = 'Peru',
                                        BI_Segment__c = 'Empresas',
                                        BI_Subsector__c = 'test', //25/09/2017
                                        BI_Sector__c = 'test', //25/09/2017
                                        BI_Subsegment_local__c = 'Top 1',
                                        BI_Subsegment_Regional__c = 'test',
                                        BI_Territory__c = 'test'
                                        );
            insert acc;

           Contact con = new Contact(LastName = 'test',
                                    AccountId = acc.Id,
                                    Email = 'test@gmail.com',
                                    HasOptedOutOfEmail = false,
                                    BI_Activo__c = true,
                                     BI_Country__c = acc.BI_Country__c,
                                    Phone = '914123456',
                                    BI_Cuenta_activa_en_portal__c = acc.Id);
           insert con;

            User usu = new User (Alias = '1t',
                                Email= 'u1@testorg.com',
                                Emailencodingkey='UTF-8',
                                Lastname= 'Testing1',
                                Languagelocalekey='en_US',
                                Localesidkey='en_US',
                                IsActive = true,
                                ProfileId = lst_profile[0].Id,
                                BI_Permisos__c = 'Empresas Platino',
                                Timezonesidkey= 'America/Lima',
                                Username= 'ur231@testorg.com',
                                ContactId = con.Id);

        //System.runAs(new User(Id=UserInfo.getUserId())){
            insert usu;
        //}


        RecordType [] lst_rt = [Select Id from RecordType where DeveloperName = 'BI_Caso_Comercial_Abierto'];
        List<String> paises = new List<String>();

        List<Account> accs = BI_DataLoad.loadAccounts(1,new List<String>{'Chile'});
        
        Case cas = new Case(
                                            Subject = 'Test Subject Acc',
                                            AccountId = accs[0].Id,
                                            RecordTypeId = lst_rt[0].Id,
                                            Type = Label.BI_CaseType_Request,
                                            BI_Country__c = accs[0].BI_Country__c);

        insert cas; 
        CaseShare cs = new CaseShare(
            CaseId = cas.Id,
            UserOrGroupId = usu.Id,
            CaseAccessLevel = 'Edit'
            );
        insert cs;
        BI_MigrationHelper.cleanSkippedTriggers();
        System.runAs(new User(Id=UserInfo.getUserId())){
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        Attachment att = new Attachment(
            Name='Unit Test Attachment',
            body=bodyBlob,
            parentId=cas.Id, 
            ContentType='image/png'
            );      
        System.runAs(usu){
            insert att;
        }


        }  
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that manage the code coverage from BI_AttachmentMethods
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        04/02/2016                      Guillermo Muñoz             Initial version
        17/02/2016                      Guillermo Muñoz             Fix bugs in Test
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void preventInsertCHITest(){

        User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        List <Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1,new List<String>{label.BI_Chile});

        BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(
            BI_Estado__c = 'Abierto',
            BI_Cliente__c = lst_acc[0].id,
            BI_Lineas_de_venta__c = true
        );

        System.runAs(usu){
            insert sol;
        }

        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        Attachment att = new Attachment(
            Name='Unit Test Attachment',
            body=bodyBlob,
            parentId=sol.Id, 
            ContentType='image/png'
        );      

        insert att;

        sol.BI_Estado_de_ventas__c = 'Aprobado';
        update sol;

        
        Attachment att2 = new Attachment(
            Name='Unit Test Attachment2',
            body=bodyBlob,
            parentId=sol.Id, 
            ContentType='image/png'
        );
        try{  
            insert att2;
            throw new BI_Exception('La validación no se ha realizado correctamente');
        }catch(DmlException e){}    
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that manage the code coverage for all exceptions launched in BI_AttachmentMethods
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        04/02/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void exceptions(){
        BI_TestUtils.throw_exception = true;

        List <Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1,new List<String>{label.BI_Chile});

        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        Attachment att = new Attachment(
            Name='Unit Test Attachment',
            body=bodyBlob,
            parentId=lst_acc[0].Id, 
            ContentType='image/png'
        );      

        insert att;
        update att;
        delete att;

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Pedro Párraga
        Company:       Aborda
        Description:   Method that manage the code coverage from BI_AttachmentMethods
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        13/01/2017                      Pedro Párraga               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    

    @isTest static void exceptions2(){
        BI_AttachmentMethods.validateImageAttachment(null);
        BI_AttachmentMethods.sendEmail(null);
        BI_AttachmentMethods.countAttachments(null, null);
        BI_AttachmentMethods.createHistorialOppCase(null, null, null);
        BI_AttachmentMethods.preventInsertCHI(null);
    }


 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   
    IN:          
    OUT:               
    History:
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian   Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest
    static void verificarPermisosContrac()
    {
        BI_TestUtils.throw_exception = false;
        User uContract = [SELECT ID FROM User WHERE BI_Permisos__c = 'Administrador de Contrato' and isActive = true and Profile.Name = 'BI_Standard_PER' limit 1];
        User uSuper = [SELECT ID FROM User WHERE BI_Permisos__c = :Label.BI_SuperUsuario AND isActive = true limit 1];
        User uOtros = [SELECT ID FROM User WHERE BI_Permisos__c = 'Ejecutivo de Cliente' and Profile.Name = 'BI_Standard_PER' AND isActive = true limit 1];
        NE__Contract__c elContract = [Select id from NE__Contract__c limit 1];
        Attachment attch = [SELECT ID, Name FROM Attachment limit 1];
        
        attch.OwnerId = uContract.Id;
        update attch;
        system.runAs(uContract){
            try{
                delete attch;
                System.assert(false); //El usuario Administrador de contrato no puede eliminar.
            }catch(Exception e){
                System.debug('verificarPermisos_adminContrac ' + e);
            }
        }

        attch.OwnerId = uOtros.Id;
        update attch; 
        system.runAs(uOtros){
            
            Attachment newNote1 = new Attachment(
            Name='Unit Test Attachment',
            body= Blob.valueOf('Unit Test Attachment'),
            parentId=elContract.Id, 
            ContentType='image/png');

            try{
                 insert newNote1;
                 System.assert(false);//
            }catch(Exception e){
                System.debug('verificarPermisos_all ' + e);
            }

            attch.name = 'asdfasd';
            try{
                 update attch;
                 System.assert(false);
            }catch(Exception e){
                System.debug('verificarPermisos_all ' + e);
            }
           
            try{
                delete attch;
                System.assert(false);
            }catch(Exception e){
                System.debug('verificarPermisos_all ' + e);
            }
        }

        attch.OwnerId = uSuper.Id;
        update attch;        
        system.runAs(uSuper){
            attch.Name = 'all';
            update attch;
            delete attch;
        }


    }

    @isTest
    static void validateExtension_TEST(){

        SEG_Allowed_Extensions__c seg = new SEG_Allowed_Extensions__c();
        seg.Name = 'Ext001';
        seg.SEG_Extension__c = '.zip';
        insert seg;
        List <Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1,new List<String>{label.BI_Chile});
        String test = 'test body';
        Blob body = Blob.valueof(test); 
        
        
        Attachment att = new Attachment(Name = 'test.zip', Body = body, ParentId = lst_acc[0].Id);
        insert att;
    }





}