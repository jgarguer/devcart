@isTest
private class PCA_Portales_TEST {

    static{
        BI_TestUtils.throw_exception = false;
    }
	
	static testMethod void test_method_one() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/

        /*JLA_Refactorizacion_Necesaria*/
    	 User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
         Network netw = [Select Id from Network where Name= 'Empresas Platino'];
         Profile prof1 = [Select Id from Profile where Name='BI_Customer Communities'];
	    User user1;

         system.runAs(usr){
            Set<String> countryCantBe = new Set<String>();
             countryCantBe.add('Argentina');
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickListNotBeing(1, countryCantBe);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais); 
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
                item.BI_Segment__c = null;
            	accList.add(item);
            }
            update accList;
            
             System.debug('+*+*accList+*+*: ' + accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            
            user1 = BI_DataLoad.loadPortalUser(con[0].Id, prof1.Id);	    	
	    	system.runAs(user1){
	    		
	    		//BI_Portal_local__c Portal1 = new BI_Portal_local__c(Name='Portal test',BI_Descripcion_del_portal__c ='Test Desc',BI_Imagen_del_portal__c='<img alt="a" src="http://www.google.com/images/srpr/logo11w.png"></img>',	BI_Link_del_portal__c='www.google.es',BI_Country__c=null,BI_Segment__c='Empresas',BI_Posicion__c='1', BI_Activo__c=true);
	    		BI_Portal_local__c Portal1 = new BI_Portal_local__c(Name='Portal test',BI_Descripcion_del_portal__c ='Test Desc',BI_Imagen_del_portal__c='<img alt="a" src="https://crqdev-telefonicab2b.cs80.force.com/empresasplatino/PCA_Portales"></img>',	BI_Link_del_portal__c='www.google.es',BI_Country__c=null,BI_Segment__c='Empresas',BI_Posicion__c='1', BI_Activo__c=true);

                insert Portal1;
	    		BI_Portal_local__c Portal2 = new BI_Portal_local__c(Name='Portal test 2',BI_Descripcion_del_portal__c ='Test Desc2',BI_Imagen_del_portal__c='<img alt="a" src="http://www.google.com/images/srpr/logo11w.png"></img>',	BI_Link_del_portal__c='www.google.es',BI_Country__c=null,BI_Segment__c='Empresas',BI_Posicion__c='2', BI_Activo__c=true);
	    		insert Portal2;
                
                System.debug('+*+*Portal1*+*+: ' + Portal1);
                System.debug('+*+*Portal2*+*+: ' + Portal2);
                
	    		PageReference pageRef = new PageReference('PCA_Portales');
	       		Test.setCurrentPage(pageRef);
                ApexPages.currentPage().getParameters().put('myParam',Portal1.Id);
        		PCA_PortalesCtrl constructor = new PCA_PortalesCtrl();
        		BI_TestUtils.throw_exception = false;
                if(constructor.ImageToshow != null) {}
        		constructor.checkPermissions();
        		constructor.loadInfo();
                constructor.getObj();
        		BI_TestUtils.throw_exception = true;
        		constructor.checkPermissions();
        		constructor.loadInfo();
                constructor.getObj();
        		//system.assertEquals(FAQ1.Id,[SELECT Id From Solution Where SolutionName = 'Solution test'].Id);
	    	}
    	}
    	
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}