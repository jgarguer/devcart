@isTest
private class BI_TicketMultipleRest_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_TicketMultipleRest class 
    
    History: 
    
    <Date>              <Author>                <Change Description>
    19/09/2014          Pablo Oliva             Initial Version
    17/08/2015          Francisco Ayllon        Solved problems with class BI_TestUtils (whole class mod)
    19/05/2016          José Luis González      Adapt test to UNICA interface class modifications
	06/07/2016          José Luis González      Fix test asserts
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static
    {
        BI_TestUtils.throw_exception = false;
    }

    private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
    private final static String LE_NAME = 'TestAccount_TicketMultipleRest_TEST';

    @testSetup static void dataSetup() 
    {
        BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
        Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert account;

        Account[] acc = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketTest = BIIN_UNICA_TestDataGenerator.createTicket(acc[0]);
        insert ticketTest;
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_TicketMultipleRest.getMultipleTickets()
    History:
    
    <Date>              <Author>                <Description>
    19/09/2014          Pablo Oliva             Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getMultipleTicketsTest() {
        BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;        
        
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
       
        system.debug(lst_log);
        
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');

        BI_DataLoadRest.SKIP_PARAMETER = false; // JEG 20/12/2016
        //BI_DataLoadRest.MAX_LOOP = 5; // JEG 20/12/2016 
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');                                                
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        List <Case> lst_tickets = BI_DataLoadRest.loadTickets(10, lst_acc[0].Id, lst_region[0]);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/ticketing/v1/tickets';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        //INTERNAL_SERVER_ERROR
        BI_TestUtils.throw_exception = true;
        BI_TicketMultipleRest.getMultipleTickets();
        lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
       
        system.debug(lst_log);
       
        system.assertEquals(500,RestContext.response.statuscode);
        // system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        // system.assertEquals(1, lst_log.size());
        
        //BAD REQUEST missing required parameters
        BI_TestUtils.throw_exception = false;
        BI_TicketMultipleRest.getMultipleTickets();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //BAD REQUEST invalid parameters
        req.addParameter('limit', 'test');
        RestContext.request = req;
        BI_TicketMultipleRest.getMultipleTickets();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //BAD REQUEST invalid date
        req.addParameter('limit', '2');
        req.addParameter('startCreationDate', 'test');
        RestContext.request = req;
        BI_TicketMultipleRest.getMultipleTickets();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //BAD REQUEST countryISO
        req.addParameter('limit', '2');
        req.addParameter('offset', '2');
        
        req.addParameter('startCreationDate', '2000-05-30T09:30:10Z');
        req.addParameter('endCreationDate', '2100-05-30T09:30:10Z');
        
        req.addParameter('startLastModificationDate', '2000-05-30T09:30:10Z');
        req.addParameter('endLastModificationDate', '2100-05-30T09:30:10Z');
        
        req.addParameter('accountId', lst_acc[0].BI_Id_del_cliente__c);
        //req.addParameter('status', 'Nuevo');
        req.addParameter('country', region.BI_Country_ISO_Code__c);
        
        RestContext.request = req;
        BI_TicketMultipleRest.getMultipleTickets();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //NOT FOUND
        req.addParameter('accountId', 'test');
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_TicketMultipleRest.getMultipleTickets();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.addParameter('accountId', lst_acc[0].BI_Id_del_cliente__c);
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_TicketMultipleRest.getMultipleTickets();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();

    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_TicketMultipleRest.doPost()
    History:
    
    <Date>              <Author>                <Description>
    19/09/2014          Pablo Oliva             Initial version
    19/05/2016          José Luis González      Adapt test to UNICA interface class modifications
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void doPostTest()
    {
        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        String correlatorId = String.valueOf(Math.round(Math.random() * 100000000));
        Case ticketIncidenciaTecnica = [SELECT Id, BI_Id_del_caso_legado__c, CaseNumber FROM Case WHERE AccountId =: account.Id];
        
        String ticketRequestMissingFieldsStr = '{"correlatorId":"' + correlatorId  + '","subject":"","description":"Description Test","country":"Peru","customerId":"' + account.BI_Validador_Fiscal__c + '","reportedDate":"1462871973","perceivedSeverity":"1000","perceivedPriority":1,"ticketType":"Incident","source":"Phone","parentTicket":"","additionalData":[{"key":"First Name","value":"Edgard Alberto"},{"key":"Last Name","value":"Anaya Gonzalez"},{"key":"Internet Email","value":"edgardalberto.anayagonzalez@acme.com"},{"key":"Direct Contact First Name","value":"Edgard Alberto"},{"key":"Direct Contact Last Name","value":"Anaya Gonzalez"},{"key":"Direct Contact Internet Email","value":"edgardalberto.anayagonzalez@acme.com"},{"key":"Urgency","value":"2000"},{"key":"Site","value":"Lima ACME"},{"key":"Categorization Tier 1","value":"IMPLANTACION  DEMANDA"},{"key":"Categorization Tier 2","value":"GOLDEN JUMPER"},{"key":"Categorization Tier 3","value":"CAMBIO"},{"key":"Product Categorization Tier 1","value":"DOCUMENTACION"},{"key":"Product Categorization Tier 2","value":"DOCUMENTO"},{"key":"Product Categorization Tier 3","value":"DOCUMENTO"},{"key":"BAO_ExternalID","value":""},{"key":"Form Name","value":"OBHAA5V0HJXX9ANTZUW9FMNL0P03GM"},{"key":"Name","value":"AST_OB_BS_IN_CI_Name"},{"key":"Case Number","value":"00299913"},{"key":"Created Date","value":"1462871973"},{"key":"Status","value":"1"},{"key":"Status Reason","value":null},{"key":"Assigned Group","value":""},{"key":"BAO_TiempoNetoApertura","value":""},{"key":"Last Modified Date","value":"1462871973"},{"key":"Close Date","value":""},{"key":"SLM Status","value":"1"},{"key":"Resolution","value":""},{"key":"Last Resolved Date","value":"1462871973"},{"key":"Resolution Category Tier 1","value":""},{"key":"Resolution Category Tier 2","value":""},{"key":"Resolution Category Tier 3","value":""}],"callbackUrl":""}';
        BIIN_UNICA_Pojos.TicketRequestType ticketRequestMissingFields = (BIIN_UNICA_Pojos.TicketRequestType) JSON.deserialize(ticketRequestMissingFieldsStr, BIIN_UNICA_Pojos.TicketRequestType.class);

        String ticketRequestBadRequestStr = '{"correlatorId":null,"subject":"Subject Test","description":"Description Test","country":"Peru","customerId":"00000000","reportedDate":"1462871973","perceivedSeverity":"minor","perceivedPriority":0,"ticketType":"Incident","source":"Phone","parentTicket":"","additionalData":[{"key":"First Name","value":"Edgard Alberto"},{"key":"Last Name","value":"Anaya Gonzalez"},{"key":"Internet Email","value":"edgardalberto.anayagonzalez@acme.com"},{"key":"Direct Contact First Name","value":"Edgard Alberto"},{"key":"Direct Contact Last Name","value":"Anaya Gonzalez"},{"key":"Direct Contact Internet Email","value":"edgardalberto.anayagonzalez@acme.com"},{"key":"Urgency","value":"2000"},{"key":"Site","value":"Lima ACME"},{"key":"Categorization Tier 1","value":"IMPLANTACION  DEMANDA"},{"key":"Categorization Tier 2","value":"GOLDEN JUMPER"},{"key":"Categorization Tier 3","value":"CAMBIO"},{"key":"Product Categorization Tier 1","value":"DOCUMENTACION"},{"key":"Product Categorization Tier 2","value":"DOCUMENTO"},{"key":"Product Categorization Tier 3","value":"DOCUMENTO"},{"key":"BAO_ExternalID","value":""},{"key":"Form Name","value":"OBHAA5V0HJXX9ANTZUW9FMNL0P03GM"},{"key":"Name","value":"AST_OB_BS_IN_CI_Name"},{"key":"Case Number","value":null},{"key":"Created Date","value":"1462871973"},{"key":"Status","value":"1"},{"key":"Status Reason","value":null},{"key":"Assigned Group","value":""},{"key":"BAO_TiempoNetoApertura","value":""},{"key":"Last Modified Date","value":"1462871973"},{"key":"Close Date","value":""},{"key":"SLM Status","value":"1"},{"key":"Resolution","value":""},{"key":"Last Resolved Date","value":"1462871973"},{"key":"Resolution Category Tier 1","value":""},{"key":"Resolution Category Tier 2","value":""},{"key":"Resolution Category Tier 3","value":""}],"callbackUrl":""}';
        BIIN_UNICA_Pojos.TicketRequestType ticketRequestBadRequest = (BIIN_UNICA_Pojos.TicketRequestType) JSON.deserialize(ticketRequestBadRequestStr, BIIN_UNICA_Pojos.TicketRequestType.class);

        String ticketRequestDuplicateStr = '{"correlatorId":"' + ticketIncidenciaTecnica.BI_Id_del_caso_legado__c + '","subject":"Subject Test","description":"Description Test","country":"Peru","customerId":"' + account.BI_Validador_Fiscal__c + '","reportedDate":"1462871973","perceivedSeverity":"1000","perceivedPriority":1,"ticketType":"Incident","source":"Phone","parentTicket":"","additionalData":[{"key":"First Name","value":"Edgard Alberto"},{"key":"Last Name","value":"Anaya Gonzalez"},{"key":"Internet Email","value":"edgardalberto.anayagonzalez@acme.com"},{"key":"Direct Contact First Name","value":"Edgard Alberto"},{"key":"Direct Contact Last Name","value":"Anaya Gonzalez"},{"key":"Direct Contact Internet Email","value":"edgardalberto.anayagonzalez@acme.com"},{"key":"Urgency","value":"2000"},{"key":"Site","value":"Lima ACME"},{"key":"Categorization Tier 1","value":"IMPLANTACION  DEMANDA"},{"key":"Categorization Tier 2","value":"GOLDEN JUMPER"},{"key":"Categorization Tier 3","value":"CAMBIO"},{"key":"Product Categorization Tier 1","value":"DOCUMENTACION"},{"key":"Product Categorization Tier 2","value":"DOCUMENTO"},{"key":"Product Categorization Tier 3","value":"DOCUMENTO"},{"key":"BAO_ExternalID","value":""},{"key":"Form Name","value":"OBHAA5V0HJXX9ANTZUW9FMNL0P03GM"},{"key":"Name","value":"AST_OB_BS_IN_CI_Name"},{"key":"Case Number","value":"00299913"},{"key":"Created Date","value":"1462871973"},{"key":"Status","value":"1"},{"key":"Status Reason","value":null},{"key":"Assigned Group","value":""},{"key":"BAO_TiempoNetoApertura","value":""},{"key":"Last Modified Date","value":"1462871973"},{"key":"Close Date","value":""},{"key":"SLM Status","value":"1"},{"key":"Resolution","value":""},{"key":"Last Resolved Date","value":"1462871973"},{"key":"Resolution Category Tier 1","value":""},{"key":"Resolution Category Tier 2","value":""},{"key":"Resolution Category Tier 3","value":""}],"callbackUrl":""}';
        BIIN_UNICA_Pojos.TicketRequestType ticketRequestDuplicate = (BIIN_UNICA_Pojos.TicketRequestType) JSON.deserialize(ticketRequestDuplicateStr, BIIN_UNICA_Pojos.TicketRequestType.class);

        String ticketRequestNoCaseNumberStr = '{"correlatorId":"' + correlatorId  + '","subject":"Subject Test","description":"Description Test","country":"Peru","customerId":"' + account.BI_Validador_Fiscal__c + '","reportedDate":"1462871973","perceivedSeverity":"1000","perceivedPriority":1,"ticketType":"Incident","source":"Phone","parentTicket":"","additionalData":[{"key":"First Name","value":"Edgard Alberto"},{"key":"Last Name","value":"Anaya Gonzalez"},{"key":"Internet Email","value":"edgardalberto.anayagonzalez@acme.com"},{"key":"Direct Contact First Name","value":"Edgard Alberto"},{"key":"Direct Contact Last Name","value":"Anaya Gonzalez"},{"key":"Direct Contact Internet Email","value":"edgardalberto.anayagonzalez@acme.com"},{"key":"Urgency","value":"2000"},{"key":"Site","value":"Lima ACME"},{"key":"Categorization Tier 1","value":"IMPLANTACION  DEMANDA"},{"key":"Categorization Tier 2","value":"GOLDEN JUMPER"},{"key":"Categorization Tier 3","value":"CAMBIO"},{"key":"Product Categorization Tier 1","value":"DOCUMENTACION"},{"key":"Product Categorization Tier 2","value":"DOCUMENTO"},{"key":"Product Categorization Tier 3","value":"DOCUMENTO"},{"key":"BAO_ExternalID","value":""},{"key":"Form Name","value":"OBHAA5V0HJXX9ANTZUW9FMNL0P03GM"},{"key":"Name","value":"AST_OB_BS_IN_CI_Name"},{"key":"Case Number","value":"00299913"},{"key":"Created Date","value":"1462871973"},{"key":"Status","value":"1"},{"key":"Status Reason","value":null},{"key":"Assigned Group","value":""},{"key":"BAO_TiempoNetoApertura","value":""},{"key":"Last Modified Date","value":"1462871973"},{"key":"Close Date","value":""},{"key":"SLM Status","value":"1"},{"key":"Resolution","value":""},{"key":"Last Resolved Date","value":"1462871973"},{"key":"Resolution Category Tier 1","value":""},{"key":"Resolution Category Tier 2","value":""},{"key":"Resolution Category Tier 3","value":""}],"callbackUrl":""}';
        BIIN_UNICA_Pojos.TicketRequestType ticketRequestNoCaseNumber = (BIIN_UNICA_Pojos.TicketRequestType) JSON.deserialize(ticketRequestNoCaseNumberStr, BIIN_UNICA_Pojos.TicketRequestType.class);

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.addHeader('UNICA-ServiceId', 'Peru');
        req.requestURI = '/ticketing/v1/tickets';
        req.httpMethod = 'POST';

        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        
        // BAD REQUEST missing mandatory fields
        req.requestBody = Blob.valueof(JSON.serialize(ticketRequestMissingFields));
        RestContext.request = req;
        BI_TicketMultipleRest.doPost(ticketRequestMissingFields);
        System.AssertEquals(400, RestContext.response.statuscode);

        // BAD REQUEST duplicate ticket
        req.requestBody = Blob.valueof(JSON.serialize(ticketRequestDuplicate));
        RestContext.request = req;
        BI_TicketMultipleRest.doPost(ticketRequestDuplicate);
        System.AssertEquals(400, RestContext.response.statuscode);

        // CREATED (no Case Number)
        req.requestBody = Blob.valueof(JSON.serialize(ticketRequestNoCaseNumber));
        RestContext.request = req;
        BI_TicketMultipleRest.doPost(ticketRequestNoCaseNumber);
        System.AssertEquals(201, RestContext.response.statuscode);

        // INTERNAL_SERVER_ERROR
        /*
        req.requestBody = Blob.valueof(JSON.serialize(ticketRequestBadRequest));
        RestContext.request = req;
        BI_TicketMultipleRest.doPost(ticketRequestBadRequest);
        System.AssertEquals(500, RestContext.response.statuscode);
        */

        Test.stopTest();
    }
}