public class BI_ContactMethods {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Methods executed by Contact Triggers 
    
    History:
     
    <Date>            <Author>         <Change Description>
    06/11/2014        Pablo Oliva      Initial version
	05/05/2017		  Eduardo Ventura  Integración de datos inicial en Fullstack.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    final static Map<String,Id> MAP_NAME_PAIS = new Map<String,Id>();
    
    static{

        /*for(Region__c region :[SELECT Id, Name FROM Region__c]){
            MAP_NAME_PAIS.put(region.Name, region.Id);
        }*/
    }     	

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that assign the Account country
    
    IN:            ApexTrigger.New
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    06/11/2014        		Pablo Oliva      		Initial version
    19/03/2015				Juan Santisi			Refactored BI_Paises_ref__c && BI_Pais_ref__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void assignCountry(List<Contact> news){
		
		Set<Id> set_account = new Set<Id>();
		List<Contact> lst_contact = new List<Contact>();
		
		for(Contact con:news){
			if(con.BI_Country__c == null){
				set_account.add(con.AccountId);
				lst_contact.add(con);
			}
		}
		
		if(!set_account.isEmpty()){
			Map <Id, Account> map_acc = new Map <Id, Account>([SELECT Id, BI_Country__c FROM Account WHERE Id in :set_account]);
			
			for(Contact contact:lst_contact){
				if(map_acc.get(contact.AccountId) != null)
					contact.BI_Country__c = map_acc.get(contact.AccountId).BI_Country__c;
			}
		}
		
	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos
	Company:       Aborda
	Description:   
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	05/02/2015                      Micah Burgos                Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
		public static void validateId(List <Contact> olds, List <Contact> news){
		try{

			Set <Id> set_options = new Set <Id>();

			Integer i = 0;
			for (Contact item:news){
				system.debug('Contact.ValidateID: item.BI_Country__c: ' + item.BI_Country__c);
				system.debug('Contact.ValidateID: MAP_NAME_PAIS.get(Label.BI_Chile): ' + Label.BI_Chile);
				system.debug('Contact.ValidateID: item.BI_Tipo_de_documento__c: ' + item.BI_Tipo_de_documento__c);
				if(item.BI_Country__c == Label.BI_Chile && item.BI_Tipo_de_documento__c == 'RUT'){
				system.debug('Contact.ValidateID: 1');
					if(item.BI_Tipo_de_documento__c != null || item.BI_Numero_de_documento__c != null){
						system.debug('Contact.ValidateID: BI_Tipo_de_documento__c ' + item.BI_Tipo_de_documento__c);       
						system.debug('Contact.ValidateID: BI_Numero_de_documento__c ' + item.BI_Numero_de_documento__c);                
                            if(item.BI_Tipo_de_documento__c == null || item.BI_Numero_de_documento__c == null){
                                item.addError(Label.BI_ValidacionIdFiscal);
                            }else if (olds == null  || ((item.BI_Numero_de_documento__c != olds[i].BI_Numero_de_documento__c) || (item.BI_Tipo_de_documento__c != olds[i].BI_Tipo_de_documento__c))){
								system.debug('Contact.ValidateID: 2'  );
                                if(item.BI_Tipo_de_documento__c != null && item.BI_Numero_de_documento__c == null){
                                    item.addError(Label.BI_ValidacionIdFiscal);
                                }else if (!BI_ValidateRutHelper.check_RUT_chile(item.BI_Numero_de_documento__c)){ //00000000-X  //X= Alphanumeric 0= numeric
                                    item.BI_Numero_de_documento__c.addError(Label.BI_ValidacionRutChile);//RMJ cambio de Label D-540
                                }
                            }                           
                    }
                
                }
                i++;
           	}
		}catch (exception Exc){
			BI_LogHelper.generate_BILog('BI_ContactMethods.validateId', 'BI_EN', Exc, 'Trigger');
		}
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Method that assign the Account country
    
    IN:            ApexTrigger.New
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    10/12/2015        		Fernando Arteaga      	Initial version
	02/01/2017              Marta Gonzalez			REING-01Adaptacion a la reingenieria de contactos
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void checkOnlyOneCSBAdmin(List<Contact> newContacts)
	{
		try
		{
			Set<Id> setAccountIds = new Set<Id>();
			Map<Id, Integer> mapAccNumAdmins = new Map<Id, Integer>();
			
			for (Contact cont: newContacts)
			{
				//REING-01.Para detectar si es adminitrador CSB, implica que tiene el check FS_CORE_Acceso_a_Portal_CSB__c=true 
                //(tipo de registro TGS active contact) o BI_Tipo_de_contacto__c == 'Administrador CSB' (resto de tipos de registro)
                //if (cont.BI_Tipo_de_contacto__c == 'Administrador CSB')
         		 if (cont.BI_Tipo_de_contacto__c == 'Administrador CSB' || cont.FS_CORE_Acceso_a_Portal_CSB__c)
					setAccountIds.add(cont.AccountId);
			}
			
			for (AggregateResult ar: [SELECT AccountId, COUNT(Name)
						  FROM Contact WHERE AccountId IN :setAccountIds
						  //REING-01-INI
						  //AND BI_Tipo_de_contacto__c = 'Administrador CSB'
						  AND (BI_Tipo_de_contacto__c = 'Administrador CSB' OR FS_CORE_Acceso_a_Portal_CSB__c=true)
						  //REING-01-FIN
						  GROUP BY AccountId])
			{
				mapAccNumAdmins.put((Id) ar.get('AccountId'), (Integer) ar.get('expr0'));
			}

			for (Contact c: newContacts)
			{
				if (mapAccNumAdmins.containsKey(c.AccountId) && mapAccNumAdmins.get(c.AccountId) > 0)
					c.addError(Label.BI_CSB_Un_Administrador);
			}
		}
		catch (Exception exc)
		{
			BI_LogHelper.generate_BILog('BI_ContactMethods.checkOnlyOneCSBAdmin', 'BI_EN', exc, 'Trigger');
		}
	}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Eduardo ventura
	Company:       Everis España	
	Description:   Lanzador de integraciones con Fullstack.
	
	History:

	<Date>						<Author>						<Change Description>
	17/04/2017					Eduardo Ventura					Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void insertToFullstack(List<Contact> contactsOld, List<Contact> contactsNew){
        /* Variable Configuration */
        /* Empty */
        
        
        List<Id> IdsCuentasAsocContacts = new List<Id>(); 
        for (Contact cAsociado:contactsNew){
            IdsCuentasAsocContacts.add(cAsociado.AccountId);                        
        }
        //List<Account> listAccount1 = [SELECT Id, BI_Segment__c FROM Account WHERE BI_Segment__c != 'Negocios' AND Id IN:IdsCuentasAsocContacts];
        List<Account> listAccount1 = [SELECT Id, BI_Segment__c FROM Account WHERE Id IN:IdsCuentasAsocContacts];
        /*Pasar Lista  a Set */
        Set<Id> idsAccountSet = new Set<Id>();
        for(Account accountSet:listAccount1){
            idsAccountSet.add(accountSet.Id);
        }
        for(Contact item : contactsNew){
            if( idsAccountSet.contains(item.AccountId))  {
                if(item.BI_Country__c == 'Peru' &&  FS_CORE_Fullstack_Structures.isValidContactRecord(item, true)){
                    FS_CORE_Fullstack_Manager.callout(item.accountId, 3, true, item.Id);
                }
            }
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Eduardo ventura
	Company:       Everis España	
	Description:   Lanzador de integraciones con Fullstack.
	
	History:

	<Date>						<Author>						<Change Description>
	17/04/2017					Eduardo Ventura					Initial Version
	02/08/2017					Julio Asenjo					Cambio para tratar actualización de ROL de contacto en AMDOCS //CHANGE-001
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    public static void updateToFullstack(List<Contact> contactsOld, List<Contact> contactsNew){
        /* Variable Configuration */
        Map<Id, Contact> contactsOldMap = new Map<Id, Contact>(contactsOld);
        List<Id> IdsCuentasAsocContacts = new List<Id>(); 
        for (Contact cAsociado:contactsNew){
            IdsCuentasAsocContacts.add(cAsociado.AccountId);                        
        }
        //List<Account> listAccount1 = [SELECT Id, BI_Segment__c FROM Account WHERE BI_Segment__c != 'Negocios' AND Id IN:IdsCuentasAsocContacts];
        List<Account> listAccount1 = [SELECT Id, BI_Segment__c FROM Account WHERE Id IN:IdsCuentasAsocContacts];
        /*Pasar Lista  a Set */
        Set<Id> idsAccountSet = new Set<Id>();
        for(Account accountSet:listAccount1){
            idsAccountSet.add(accountSet.Id);
        }
        /* Process Data */
        for(Contact item : contactsNew){
            //INIT CHANGE-001
            if( idsAccountSet.contains(item.AccountId))  {
                
                //comprobamos si ha cambaido el tipo de contacto para realizar la actualizazión del rol el AMDOCS
                System.debug('\n\n\n contactsOldMap.get(item.Id).BI_Tipo_de_contacto__c  \n '+contactsOldMap.get(item.Id).BI_Tipo_de_contacto__c);
                System.debug('\n\n\n item.BI_Tipo_de_contacto__c  \n '+item.BI_Tipo_de_contacto__c);
                if(contactsOldMap.get(item.Id).BI_Tipo_de_contacto__c != item.BI_Tipo_de_contacto__c ||contactsOldMap.get(item.Id).BI_Representante_legal__c != item.BI_Representante_legal__c){
                    System.debug('\n\n\n Deslink CONTACT  \n step=6');
                    if(item.BI_Country__c == 'Peru' &&  FS_CORE_Fullstack_Structures.isValidContact(contactsOldMap.get(item.Id))){
                        System.debug('\n\n\n 1-CALLING WS contactMethods \n step=6');
                        FS_CORE_Fullstack_Manager.callout(item.accountId, 6, false, item.Id);
                    }else {
                        System.debug('\n\n\n 1-CALLING WS contactMethods \n step=3');
                        FS_CORE_Fullstack_Manager.callout(item.accountId, 3, false, item.Id);
                    }  
                }
                else {// Si no se ha cambiado el rol -->actualización de campos
                    boolean oldSincronizado = ([SELECT Count() FROM Account WHERE Id =: contactsOldMap.get(item.Id).AccountId AND FS_CORE_Estado_Sincronizacion__c = 'Sincronizado'] != 0);
                    boolean newSincronizado = ([SELECT Count() FROM Account WHERE Id =: item.AccountId AND FS_CORE_Estado_Sincronizacion__c = 'Sincronizado'] != 0);
                    //FIN CHANGE-001
                    if(item.BI_Country__c == 'Peru' &&  FS_CORE_Fullstack_Structures.isValidContactRecord(item, false)){
                        if(FS_CORE_Fullstack_Structures.isContactChanged(contactsOldMap.get(item.Id), item)){
                            if(contactsOldMap.get(item.Id).accountId == item.AccountId) {
                                System.debug('\n\n\n 1-CALLING WS contactMethods \n step=3');
                                FS_CORE_Fullstack_Manager.callout(item.accountId, 3, false, item.Id);
                            } else {
                                if(!FS_CORE_Fullstack_Manager.cambioDeCuenta){
                                    FS_CORE_Fullstack_Manager.cambioDeCuenta=true;
                                    //se añade la siguiente condición para que realize la operación de delete si la cuenta anterior esta sincronizada
                                    if(oldSincronizado){
                                       	System.debug('\n\n\n 2-CALLING WS contactMethods \n step=5');
                                    	FS_CORE_Fullstack_Manager.callout(item.accountId, 5, false, item.Id,contactsOldMap.get(item.Id).accountId); 
                                    }
                                    System.debug('\n\n\n 2-CALLING WS contactMethods \n step=3');
                                    FS_CORE_Fullstack_Manager.callout(item.accountId, 3, true, item.Id);
                                }
                               
                               //System.debug('\n\n\n 2-CALLING WS contactMethods \n step=4');
                              //  FS_CORE_Fullstack_Manager.callout(item.accountId, 4, true, item.Id);
                            }
                        }
                    }
                    /*Everis 01 - Pablo Bordas Modificación para controlar el siguiente caso: Contacto cambia de cliente Sincronizado a un cliente No sincronizado
                     * Sin esta modificación ocurre lo siguiente:
                     * NO OK. El contacto pasa a No sincronizado, pero en trazas no se está enviando nada a FS, con lo que para FS el contacto sigue vinculado al primer cliente.
                     * 
                     * Con este cambio se pretende desvincular tambien el Contacto con el anterior cliente en FS
					*/
                    
                    System.debug('\n Pre Everis 01 BI_ContactMethods \n oldSincronizado - contactsOldMap.get(item.Id).AccountId: '+ oldSincronizado + '\n newSincronizado y item.AccountId: ' + newSincronizado);
                    

                    if(oldSincronizado && !newSincronizado){
                        //se realiza una llamada al callout para eliminar utilizar el unlinkContactToAccount
                        System.debug('\n oldSincronizado==true && newSincronizado==false \n item.accountId: ' + item.accountId + '\n item.Id: ' + item.Id + '\n contactsOldMap.get(item.Id).AccountId: ' + contactsOldMap.get(item.Id).AccountId +'\n');
                        FS_CORE_Fullstack_Manager.callout(item.accountId, 6, false, item.Id, contactsOldMap.get(item.Id).AccountId);
                    }
                }     
            }
        }
    }   
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Eduardo ventura
	Company:       Everis España	
	Description:   Lanzador de integraciones con Fullstack.
	
	History:

	<Date>						<Author>						<Change Description>
	17/04/2017					Eduardo Ventura					Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void deleteFromFullstack(List<Contact> contactsOld, List<Contact> contactsNew){
        /* Variable Configuration */
        /* Empty */
        
        for(Contact item : contactsOld){
            if(item.BI_Country__c == 'Peru' && !String.isEmpty(item.BI_Id_del_contacto__c) && item.FS_CORE_Estado_de_Sincronizacion__c == 'Sincronizado') FS_CORE_Fullstack_Manager.callout(item.accountId, 5, false, item.Id);
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture - New Energy Aborda    
    Description:   Method that sen to full stack a Contact when is converted
    
    History:
    
    <Date>                      <Author>                        <Change Description>
    28/09/2017                  Guillermo Muñoz                 Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void sendConvertedContactToFullStack (List <Contact> news){
        
        try{

            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }

            List <Contact> toProcess = new List <Contact>();
            
            for(Contact con : news){
                
                if(con.FS_TGS_Id_del_prospecto__c != null){
                    
                    toProcess.add(con);
                }
            }
            
            if(!toProcess.isEmpty()){
                
                System.debug('!!@@ toProcess --> ' + toProcess.size());
                
                List <Contact> lst_contacts = [SELECT Birthdate, TGS_Language__c, AccountId, Account.TGS_Es_MNC__c, Account.BI_Identificador_Externo__c, Email, Phone, Fax, Department, 
                                               Name, FirstName, LastName, Salutation, BI_Genero__c, BI_Activo__c, FS_TGS_Id_Contacto_FullStack__c
                                               FROM Contact WHERE Id IN: toProcess];
                
                Map <Id, List <Contact>> map_contacts = new Map <Id, List <Contact>>();
                
                for(Contact con : lst_contacts){
                    
                    if(con.Account.BI_Identificador_Externo__c != null && con.Account.TGS_Es_MNC__c){
                        
                        if(map_contacts.containsKey(con.AccountId)){
                            
                            map_contacts.get(con.AccountId).add(con);
                        }
                        else{
                            
                            map_contacts.put(con.AccountId, new List <Contact>{con});
                        }
                    }
                }
                
                System.debug('!!@@ contacts --> ' + map_contacts.size());
                if(!map_contacts.isEmpty()){
                    System.enqueueJob(new FS_TGS_SendToFullStack_Job(map_contacts, null));
                }
            }
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_ContactMethods.sendConvertedContactToFullStack', 'BI_EN', exc, 'Trigger');
        }
    }

 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jaime Regidor
    Company:       Accenture - New Energy Aborda    
    Description:   Metodo que evita que haya correos duplicados en una misma cuenta.
    
    History:
    
    <Date>                      <Author>                        <Change Description>
    07/03/2018                  Jaime Regidor                     Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public static void validateDuplicateEmail (List<Contact> contactsOld, List<Contact> contactsNew){

         try{
            NETriggerHelper.setTriggerFired('validateDuplicateEmail');
            List<Contact> toProcess = new List<Contact>();
            List<Id> lst_IdAcc = new List<Id>();
            Map<Id, String> map_IdEmial = new Map<Id, String>();

            if(contactsOld == null){
                toProcess = contactsNew;
            }else{
                for(Integer i=0; i< contactsNew.size();i++){
                    if(contactsNew[i].Email != contactsOld[i].Email){
                        toProcess.add(contactsNew[i]);
                    }
                }
            }

            if(!toProcess.isEmpty()){
                for(Contact c : toProcess){
                    lst_IdAcc.add(c.AccountId);
                }
            }

            if(!lst_IdAcc.isEmpty()){
                Map <Id, Account> map_AccContact = new Map<Id, Account> ([SELECT Id ,(SELECT Id, Email FROM Contacts WHERE BI_Activo__c = true) FROM Account WHERE Id IN:lst_IdAcc]);
                for(Contact c :toProcess){
                    for(Contact cAcc : map_AccContact.get(c.AccountId).Contacts){
                        if(c.Email == cAcc.Email){
                            c.addError(Label.BI_Duplicado_de_Email);
                        }
                    }
                }
            }

     }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_ContactMethods.duplicateEmail', 'BI_EN', exc, 'Trigger');
        }
    }
}