@isTest
private class BI_SolicitudEnvioMethods_TEST {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando González
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_SolicitudEnvioMethods class
    
    History: 
    <Date> 					<Author> 				<Change Description>
    27/03/2015      		Fernando González    	Initial Version	
    03/02/2016               Guillermo Muñoz         Fix bugs caused by news validations rules	
    20/09/2017        Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    static{
        
        BI_TestUtils.throw_exception = false;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Fernando González
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_SolicitudEnvioMethods
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	27/03/2015      		Fernando González    	Initial Version	
    03/02/2016              Guillermo Muñoz         Fix bugs caused by news validations rules
 	---------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	
    static testMethod void solicitudEnvioTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        list <Recordtype> lst_rt = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Libre' AND sObjectType = 'BI_Bolsa_de_Dinero__c']; 
        list<BI_Solicitud_envio_productos_y_servicios__c> lstNewSolicitudEnvio = new list<BI_Solicitud_envio_productos_y_servicios__c>();
        list<BI_Solicitud_envio_productos_y_servicios__c> lstSelectSolicitudEnvio = new list<BI_Solicitud_envio_productos_y_servicios__c>();
        list<BI_Solicitud_envio_productos_y_servicios__c> lsSolicitudEnvioUpd = new list<BI_Solicitud_envio_productos_y_servicios__c>();
        list<BI_Linea_de_Recambio__c> lstLineaRecambio = new list<BI_Linea_de_Recambio__c>();
        list<BI_Linea_de_Venta__c> lstLineaVenta = new list<BI_Linea_de_Venta__c>();       
        User usu = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema AND Id !=: UserInfo.getUserId() LIMIT 1];

		Account acc = new Account(Name = 'test Account 2',
							BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
							BI_Activo__c = Label.BI_Si,
							BI_Holding__c = 'No',
                            BI_Tipo_de_identificador_fiscal__c = 'RFC',
							BI_No_Identificador_fiscal__c = 'VECB380354XXX',
                            BI_Segment__c = 'test',
        BI_Subsegment_Regional__c = 'test',
        BI_Territory__c = 'test');
		        
		insert acc;
		
        System.runAs(usu){
            lstNewSolicitudEnvio = BI_DataLoadAgendamientos.loadSolicitudEnvio(200, null, true);
        }
        
        if ( !lstNewSolicitudEnvio.isEmpty() ){
        	
        	lstLineaRecambio = BI_DataLoadAgendamientos.loadLineaRecambio(1, lstNewSolicitudEnvio[0].Id, acc);
        	lstLineaVenta = BI_DataLoadAgendamientos.loadLineaVentaWithSolicitud(1, lstNewSolicitudEnvio[0].Id);
        	
        	
	        BI_Bolsa_de_Dinero__c bolsa = new BI_Bolsa_de_Dinero__c();
	        bolsa.BI_Cliente__c = acc.Id;
	        bolsa.BI_Monto_Inicial__c = 1000000;
	        bolsa.CurrencyIsoCode = 'EUR';
	        bolsa.RecordtypeId = lst_rt[0].Id;
	        insert bolsa;
	        
        	BI_Transaccion_de_bolsa_de_dinero__c trans = new BI_Transaccion_de_bolsa_de_dinero__c();
        	trans.BI_Movimiento__c = 100;
    		trans.BI_Codigo_de_bolsa_de_dinero__c = bolsa.Id;
    		trans.BI_Linea_de_recambio__c = lstLineaRecambio[0].Id;
    		
    		insert trans;
    		
        	for ( BI_Solicitud_envio_productos_y_servicios__c sol : lstNewSolicitudEnvio){
        	
	        	sol.BI_Estado_de_solicitud__c = 'Cancelado';
	        	lsSolicitudEnvioUpd.Add(sol);
	        }
	        
	        update lsSolicitudEnvioUpd;
        }
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Test method that manage the code coverage of BI_SolicitudEnvioMethods.preventModification
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        17/02/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void preventModification_TEST(){

        GroupMember [] lst_gm = [SELECT UserOrGroupId, GroupId FROM GroupMember WHERE Group.Developername = 'BI_CHI_Ejecutivos_de_Adm_Ventas'];
        User usu = new User();
        if(!lst_gm.isEmpty()){
            Set <Id> set_usu = new Set <Id> ();
            for(GroupMember gm : lst_gm){
                set_usu.add(gm.UserOrGroupId);
            }

            if(set_usu.contains(UserInfo.getUserId())){
                usu = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema AND Id !=: set_usu LIMIT 1];
            }
            else{
                usu = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
            }
        }
        else{
            usu = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        }

        List <Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1,new List<String>{label.BI_Chile});

        BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(
            BI_Cliente__c = lst_acc[0].Id,
            BI_Estado_de_solicitud__c = 'Abierto',
            BI_Lineas_de_venta__c = true
        );

        System.runAs(Usu){ 

            insert sol;
    
            sol.BI_Tipo_solicitud__c = 'test';
            update sol;
    
            sol.BI_Estado_de_ventas__c = 'Aprobado';
    
            try{
                update sol;
                throw new BI_Exception('No se realizo la validacion correctamente');
            }catch(DMLException e){}
        }

        Id idGroup = [SELECT Id FROM Group WHERE DeveloperName = 'BI_CHI_Ejecutivos_de_Adm_Ventas' LIMIT 1].Id;
        
        GroupMember gm = new GroupMember(
            UserOrGroupId = UserInfo.getUserId(),
            GroupId = idGroup
        );
        System.runAs(usu){
            insert gm;
        }
        System.runAs(usu){
            update sol;
        }     
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Test method that manage the code coverage of all exceptions thrown in BI_SolicitudEnvioMethods
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        17/02/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void exceptions(){

        BI_TestUtils.throw_exception = true;

        List <Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1,new List<String>{label.BI_Chile});

        BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(
            BI_Cliente__c = lst_acc[0].Id,
            BI_Estado_de_solicitud__c = 'Abierto',
            BI_Lineas_de_venta__c = true
        );
        insert sol;
        update sol;
        delete sol;

    }

}