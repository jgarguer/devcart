@isTest
public class PCA_New_Complaint_Controller_TEST {

    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test class to manage the coverage code for PCA_New_Complaint_Controller class 

    <Date>                  <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
 /*   private static void createDummyCase(Id confId) {
        List<RecordType> listRecordTypes = [SELECT Id, Name FROM RecordType WHERE Name LIKE 'Order Management Case'];
        if(listRecordTypes.size()>0){            
            testCase = new Case(RecordTypeId = listRecordTypes[0].Id, Subject = 'Test Subject', Status='Assigned', Order__c = confId,
                                TGS_Customer_Services__c = testOrderItem.Id, TGS_Invoice_Date__c = date.today(),
                               TGS_Product_Tier_1__c = 'Tier 1', TGS_Product_Tier_2__c = 'Tier 2', TGS_Product_Tier_3__c = 'Tier 3');
            insert testCase;
        }
    }
    
    private static void createDummyConfiguration() {
        
        List<RecordType> listRecordTypes = [SELECT Id, Name FROM RecordType WHERE Name LIKE 'Order'];
        if(listRecordTypes.size()>0){
            testOrder = new NE__Order__c(RecordTypeId = listRecordTypes[0].Id);
            insert testOrder;
            NE__Catalog__c testCatalog = new NE__Catalog__c(Name='Test Catalog');
            insert testCatalog;
            NE__Catalog_Category__c testCatalogCategory = new NE__Catalog_Category__c(Name='Test Category', NE__CatalogId__c = testCatalog.Id);
            insert testCatalogCategory;
            testProduct = new NE__Product__c(Name='Test Product');
            insert testProduct;
            NE__Catalog_Item__c testCatalogItem = new NE__Catalog_Item__c(NE__Catalog_Category_Name__c = testCatalogCategory.Id, NE__Catalog_Id__c = testCatalog.Id, NE__ProductId__c = testProduct.Id );
            insert testCatalogItem;
            testOrderItem = new NE__OrderItem__c(NE__OrderId__c = testOrder.Id, NE__ProdId__c = testProduct.Id, NE__CatalogItem__c = testCatalogItem.Id, NE__Qty__c=1);
            insert testOrderItem;
            
        }
       
    }
    
*/    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Complaint_Controller.init
            
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getloadInfoCaseId() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id Complaint = [SELECT Id FROM RecordType WHERE Name = 'Complaint' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = Complaint , Subject = 'Test Subject');
            insert caseTest;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_New_Complaint_Controller controller = new PCA_New_Complaint_Controller();
            Test.stopTest();
            System.assertEquals('Test Subject', controller.mcase.Subject);
        }
        
    }
    static testMethod void getloadInfosId() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem =TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            NE__Catalog_Item__c[] catalogItems = TGS_Portal_Utils.getCatalogItemsBySId(testProduct.Id);
            if (catalogItems.size()>0){
                NE__Catalog_Item__c catalogItem = catalogItems[0];
            	Test.startTest();
                ApexPages.currentPage().getParameters().put('sId', testProduct.Id);
                PCA_New_Complaint_Controller controller = new PCA_New_Complaint_Controller();
            	Test.stopTest();
                System.assertEquals(catalogItem.NE__ProductId__r.Name, controller.mCase.TGS_Product_Tier_3__c);
            }
        }
    }
    static testMethod void getloadInfosuId() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_New_Complaint_Controller controller = new PCA_New_Complaint_Controller();
            Test.stopTest();
            System.assertEquals(testOrderItem.Id, controller.mCase.TGS_Customer_Services__c);
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Complaint_Controller.crearCase
    
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void crearCaseTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem =TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
           
            //Case myCase=createDummyCase(testOrder.Id);
            
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_New_Complaint_Controller controller = new PCA_New_Complaint_Controller();
            controller.mCase.Subject = 'New case';
            controller.endSite = 'endSite';
            controller.level1 = 'level 1';
            controller.level2 = 'level 2';
            controller.level3 = 'level 3';
            controller.level4 = 'level 4';
            controller.level5 = 'level 5';
            PageReference pg = controller.crearCase();
            Test.stopTest();
            List<Case> caseList = [SELECT Id, CaseNumber FROM Case WHERE Subject = 'New case'];
            if(caseList.size()>0){
                System.assertEquals(1, caseList.size());
            }
            else{
                System.assertEquals('/empresasplatino/PCA_Cases?error=true', pg.getUrl());
            }
        }
    }
    static testMethod void crearCaseTest2() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_New_Complaint_Controller controller = new PCA_New_Complaint_Controller();
            controller.mCase.Subject = 'New case';
            PageReference pg = controller.crearCase();
            Test.stopTest();
            List<Case> caseList = [SELECT Id, CaseNumber FROM Case WHERE Subject = 'New case'];
            if(caseList.size()>0){
                System.assertEquals(1, caseList.size());
            }
            else{
                System.assertEquals('/empresasplatino/PCA_Cases?error=true', pg.getUrl());
            }
        }
    }
    
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Complaint_Controller.cancelar
    
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void cancelarTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            Id Complaint = [SELECT Id FROM RecordType WHERE Name = 'Complaint' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = Complaint , Subject = 'Test controller', Status = 'Assigned');
            insert caseTest;
            Test.stopTest();
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_New_Complaint_Controller controller = new PCA_New_Complaint_Controller();
            PageReference pg = controller.cancelar();
            System.assertEquals('/empresasplatino/PCA_Cases', pg.getUrl());
        }
    }
   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Complaint_Controller.initializeCaseFields
    
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*static testMethod void initializeCaseFieldsTest() {
        //User userTest = [SELECT Id, Name FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS_Partner Community') AND isActive = true LIMIT 1];
        User userTest = [SELECT Id, Name FROM User WHERE profileId IN (SELECT Id FROM Profile WHERE Name = 'TGS Customer Community Plus') AND isActive = true LIMIT 1];
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            NE__Catalog_Item__c[] catalogItems = TGS_Portal_Utils.getCatalogItemsBySId(testProduct.Id);
            if (catalogItems.size()>0){
                NE__Catalog_Item__c catalogItem = catalogItems[0];
				Test.startTest();
                ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
                PCA_New_Incident_Controller controller = new PCA_New_Incident_Controller();
                controller.initializeCaseFields();
            	Test.stopTest();
                System.assertEquals(testProduct.Name, controller.mCase.TGS_Product_Tier_3__c);
            }
        }
    }*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Complaint_Controller.initializeProductTiers
    
     <Date>                 <Author>                <Inquiry Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void initializeProductTiersTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            NE__Catalog_Item__c[] catalogItems = TGS_Portal_Utils.getCatalogItemsBySId(testProduct.Id);
            if (catalogItems.size()>0){
                NE__Catalog_Item__c catalogItem = catalogItems[0];
            	Test.startTest();
                ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
                PCA_New_Complaint_Controller controller = new PCA_New_Complaint_Controller();
                controller.mCase.TGS_Customer_Services__c = testOrderItem.Id;
                controller.initializeProductTiers();
            	Test.stopTest();
                System.assertEquals(testProduct.Name, controller.mCase.TGS_Product_Tier_3__c);
            }
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Complaint_Controller.initializeProductTiers(suId)
    
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void initializeProductTierssuIdTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.DummyConfiguration();
            NE__Order__c testOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id = :testOrderItem.NE__OrderId__c LIMIT 1];
            NE__Product__c testProduct = [SELECT Id, Name FROM NE__Product__c WHERE Id = :testOrderItem.NE__ProdId__c LIMIT 1];
            Case testCase = [SELECT Id FROM Case WHERE Id = :testOrder.Case__c LIMIT 1];
            testCase.TGS_Product_Tier_1__c = 'Tier 1';
            testCase.TGS_Product_Tier_2__c = 'Tier 2';
            testCase.TGS_Product_Tier_3__c = 'Tier 3';
            update testCase;
            NE__Catalog_Item__c[] catalogItems = TGS_Portal_Utils.getCatalogItemsBySId(testProduct.Id);
            if (catalogItems.size()>0){
                NE__Catalog_Item__c catalogItem = catalogItems[0];
            	Test.startTest();
                PCA_New_Complaint_Controller controller = new PCA_New_Complaint_Controller();
                controller.initializeProductTiers(testOrderItem.Id);
            	Test.stopTest();
                System.assertEquals(testProduct.Name, controller.mCase.TGS_Product_Tier_3__c);
            }
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Complaint_Controller.loadLevel1/loadLevel2/loadLevel3/loadLevel4/loadLevel5/loadSites
    
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void loadLocationTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            PCA_New_Complaint_Controller controller = new PCA_New_Complaint_Controller();
            List<Account> levelList = TGS_Portal_Utils.getLevel1(userTest.Id,0);
            System.assertEquals(levelList.size()+1, controller.listLevel1.size());
            if(controller.level1 == '' || controller.level1 == null){ controller.level1 = levelList[0].Id;}
            controller.loadLevel2();
            levelList = TGS_Portal_Utils.getLevel2(controller.level1,1);
            System.assertEquals(levelList.size()+1, controller.listLevel2.size());
            if(controller.level2 == '' || controller.level2 == null){ controller.level2 = levelList[0].Id;}
            controller.loadLevel3();
            levelList = TGS_Portal_Utils.getLevel3(controller.level2,1);  
            System.assertEquals(levelList.size()+1, controller.listLevel3.size());
            if(controller.level3 == '' || controller.level3 == null){ controller.level3 = levelList[0].Id;}
            controller.loadLevel4();
            levelList = TGS_Portal_Utils.getLevel4(controller.level3,1);  
            System.assertEquals(levelList.size()+1, controller.listLevel4.size());
            if(controller.level4 == '' || controller.level4 == null){ controller.level4 = levelList[0].Id;}
            controller.loadLevel5();
            levelList = TGS_Portal_Utils.getLevel5(controller.level4,1); 
            System.assertEquals(levelList.size()+1, controller.listLevel5.size());
            controller.loadSites();
            Test.stopTest();
            List<BI_Punto_de_instalacion__c> sites = TGS_Portal_Utils.getSites(controller.level4);
            System.assertEquals(sites.size()+1, controller.siteList.size());
        }
    }
    
 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Complaint_Controller.loadLookupServices
            
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void loadLookupServicesTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Test.startTest();
            PCA_New_Complaint_Controller controller = new PCA_New_Complaint_Controller();
            controller.loadLookupServices();
            Test.stopTest();
            List<Account> accounts = TGS_Portal_Utils.getAccountsBelowHierarchy(userTest.Id);        
            List<NE__OrderItem__c> services = TGS_Portal_Utils.getlookupServiceUnits(accounts);
            System.assertEquals(services.size(), controller.lookupServices.size());
        }
    }
  
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Complaint_Controller.crearWorkinfo
            
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void crearWorkinfoTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id complaint = [SELECT Id FROM RecordType WHERE Name = 'Complaint' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = complaint , Subject = 'Test controller');
            insert caseTest;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_New_Complaint_Controller controller = new PCA_New_Complaint_Controller();
            controller.attachmentName1 = 'Unit Test Attachment';
            controller.attachmentDesc1 = 'Unit Test Description Attachment';
            controller.myAttachedBody = Blob.valueOf('Unit Test Attachment Body');
            controller.attachmentName2 = 'Unit Test Attachment';
            controller.attachmentDesc2 = 'Unit Test Description Attachment';
            controller.myAttachedBody2 = Blob.valueOf('Unit Test Attachment Body');
            controller.attachmentName3 = 'Unit Test Attachment';
            controller.attachmentDesc3 = 'Unit Test Description Attachment';
            controller.myAttachedBody3 = Blob.valueOf('Unit Test Attachment Body');
            controller.crearWorkinfo(caseTest.Id);
            Test.stopTest();
            List<TGS_Work_Info__c> wi = [SELECT Id, TGS_Description__c FROM TGS_Work_Info__c WHERE TGS_Case__c = :caseTest.Id];
            System.assertEquals(1, wi.size());
        }
    }
   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_New_Complaint_Controller.uploadAttachment
    
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void uploadAttachmentTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id Complaint  = [SELECT Id FROM RecordType WHERE Name = 'Complaint' LIMIT 1].Id;
            Case caseTest = new Case(RecordTypeId = Complaint , Subject = 'Test controller');
            insert caseTest;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('caseId', caseTest.Id);
            PCA_New_Complaint_Controller controller = new PCA_New_Complaint_Controller();
            controller.newAttachment.Name = 'Unit Test Attachment';
            controller.myAttachedBody = Blob.valueOf('Unit Test Attachment Body');
            controller.uploadAttachment();
            Test.stopTest();
            System.assertEquals(1, controller.listCaseAttachments.size());
            System.assertEquals('Unit Test Attachment', controller.listCaseAttachments[0].Name);
        }
    }
    
    
    
    
}