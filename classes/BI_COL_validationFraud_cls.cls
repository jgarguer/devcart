/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:		Carlos Carvajal
	Company:	  Salesforce.com
	Description:  Class validation acount
	
	History:
	
	<Date>			<Author>		  <Description>
	01/04/2015	 Carlos Carvajal	Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	

public class BI_COL_validationFraud_cls 
{
	public static Map<String,List<String>> Consultar_Fraude (List<ID> IDClientes)
	{
	//llamando las clases creadas con el wsdl
		planCanalonlineWsFx3TelefonicaComC.PlanCanalOnlineSOAP fr =new planCanalonlineWsFx3TelefonicaComC.PlanCanalOnlineSOAP();
		dtoWsFx3TelefonicaComCo.PlanCanalOnlineMassiveRequest canalOnlineMassiveRequest=new dtoWsFx3TelefonicaComCo.PlanCanalOnlineMassiveRequest();
		List<Account> lstAccount=[SELECT ID,BI_No_Identificador_fiscal__c,BI_Fraude__c FROM Account WHERE ID IN:IDClientes];

		List<String> lstDocumen=new List<String>(); 
		for(Account a:lstAccount)
		{
			lstDocumen.add(a.BI_No_Identificador_fiscal__c);
		}
		canalOnlineMassiveRequest.documento=lstDocumen;
		
		//estructura para pasar la Autenticacion
		fr.Authentication=new utilFx3TelefonicaComCo.Authentication();
		fr.Authentication.host='10.200.10.101';
		fr.Authentication.password='1234';
		fr.Authentication.username='FASS';

		Map<String,List<String>> mpRespuesta=new  Map<String,List<String>>();
		Map<String,String> mpResp=new Map<String,String>();
		dtoWsFx3TelefonicaComCo.PlanCanalOnlineMassiveResponse resp=new dtoWsFx3TelefonicaComCo.PlanCanalOnlineMassiveResponse();
		if(!Test.isRunningTest())
		{
			try
			{
				resp=fr.queryForeignPlanCallsMassive(canalOnlineMassiveRequest); 
				System.debug('\n\n Linea 33 resp.result= '+resp.result+'\n\n');
				if(resp.result==null)
				{
					//EmailSender_cls.SendEmail_ws('jhans.sanabria@telefonica.com', 'cfcarvajal@avanxo.com;andres.reyes@telefonica.com', 'cfcarvajal@avanxo.com', 'pruaba.svc@salesforce.com', 'Prueba SVC ERROR', '\n\n\n Error SVC \n\n' + resp +'\n\n\n', false);
					resp.result=objetoError(lstAccount,'Respuesta Null');
				}
			}
			catch(Exception ex)
			{
				resp.result=objetoError(lstAccount,ex.getMessage());
				System.debug('\n\n '+ex.getMessage()+' \n\n');
			}  
		}
		else
		{
			resp.result=objetoError(lstAccount,'Prueba WS');
		}
		
		System.debug('\n\n resp.result= '+resp.result+' \n\n');
		for (dtoWsFx3TelefonicaComCo.ClientStateDTO r:resp.result)
		{
			List<String> lstParametros=new List<String>(); 
			System.debug('\n\n CodEstado='+r.CodEstado+' r.DescCausal=' +r.DescCausal+ ' r.Suscriptor='+r.Suscriptor+'\n\n');
			lstParametros.add(r.CodEstado);
			lstParametros.add(r.DescCausal);
			mpRespuesta.put(r.Suscriptor,lstParametros);
			mpResp.put(r.Suscriptor,r.CodEstado);
		}
		//Actualiza la cuenta si esta en fraude
		Boolean actualizaCliente=false;
		/*for(Account a:lstAccount)
		{
			if(a.Estado_Fraude__c==true && mpResp.get(a.BI_No_Identificador_fiscal__c)=='2')
			{
				a.Estado_Fraude__c=false;
				actualizaCliente=true;
			}
			else if(a.Estado_Fraude__c==false && mpResp.get(a.BI_No_Identificador_fiscal__c)=='1')
			{
				a.Estado_Fraude__c=true;
				actualizaCliente=true;
			}
		}*/
		
		/*if(actualizaCliente)
		{
			try {update lstAccount;}catch(Exception ex){}
		}*/
		System.debug('\n\n Despues  de actualizar Cliente \n\n');
		return mpRespuesta;
	}

	public static List<dtoWsFx3TelefonicaComCo.ClientStateDTO> objetoError(List<Account> lstAccount,String Mensaje)
	{
		
		List<dtoWsFx3TelefonicaComCo.ClientStateDTO> lstrespObje=new List<dtoWsFx3TelefonicaComCo.ClientStateDTO>();
		for(Account a:lstAccount)
		{
			dtoWsFx3TelefonicaComCo.ClientStateDTO respobj=new dtoWsFx3TelefonicaComCo.ClientStateDTO();
			respobj.Suscriptor=a.BI_No_Identificador_fiscal__c;
			respobj.CodEstado='2';
			respobj.DescEstado=Mensaje;
			respobj.CodCausal='2';
			respobj.DescCausal=Mensaje;
			lstrespObje.add(respobj);
		}

		return lstrespObje;
	}
}