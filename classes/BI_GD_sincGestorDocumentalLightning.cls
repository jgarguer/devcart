/*--------------------------------------------------------------------------------------------------------------------------
	Author:					Eduardo Ventura
	Company:				Everis España
	Description:			Adaptacion del boton Sincronizar getsor documental (argentina)en lighting

	History:

	<Date>								<Author>								<Change Description>
	15/03/2017							Eduardo Ventura							Versión inicial.
	------------------------------------------------------------------------------------------------------------------------*/



public class BI_GD_sincGestorDocumentalLightning {

    public static String mensaje{get;set;}
    
    @AuraEnabled
    public static String sinc(String contId){
        mensaje = BI_GD_CalloutManager.sincronizar(contId);
        return mensaje;   
    }
}