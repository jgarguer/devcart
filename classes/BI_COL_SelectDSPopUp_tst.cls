/****************************************************************************************************
	Información general
	-------------------
	author: Javier Tibamoza Cubillos
	company: Avanxo Colombia
	Project: Implementación Salesforce
	Customer: TELEFONICA
	Description: 
	
	Information about changes (versions)
	-------------------------------------
	Number    Dates           Author                       Description
	------    --------        --------------------------   -----------
	1.0       09-Abr-2015     Javier Tibamoza              Creación de la Clase Prueba BI_COL_SelectDSPopUp_ctr
	1.1		  2015-08-19	  Jeisson Rojas (JR)		   Cambio realizado a la Query de Usuarios del campo Country por Pais__c
	1.2		  2016-12-29	  Gawron, Julián E.			   Add @testSetup
	1.3       2017-02-18	  Guillermo Muñoz			   Changed lstUsuarios variable from List <User> to User
	1.4		  23-02-2017      Jaime Regidor                Adding Campaign Values, Adding Catalog Country, Adding Contact Values
			 13/03/2017		  Marta Gonzalez(Everis)	   REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
             20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
****************************************************************************************************/
@isTest
private class BI_COL_SelectDSPopUp_tst 
{
	public static Account objCuenta;
	public static Account objCuenta2;
	public static Contact objContacto;
	public static Opportunity objOportunidad;
	public static BI_COL_Anexos__c objAnexos;
	public static BI_COL_Descripcion_de_servicio__c objDesSer;
	public static Contract objContrato;
	public static Campaign objCampana;
	public static BI_Col_Ciudades__c objCiudad;
	public static BI_Sede__c objSede;
	public static User objUsuario;
	public static BI_Punto_de_instalacion__c objPuntosInsta;
	public static BI_Log__c objBiLog;
	public static BI_COL_Modificacion_de_Servicio__c 		objMS;
	
	public static list <Profile> lstPerfil;
	public static list <UserRole> lstRoles;
	public static list <Campaign> lstCampana;
	public static list<BI_COL_manage_cons__c> lstManege;
	public static List<RecordType> lstRecTyp;
	//public static List<User> lstUsuarios;
	public static User usu;
	public static List<BI_COL_Descripcion_de_servicio__c> lstDS;
	public static list<BI_Log__c> lstLog = new list <BI_Log__c>(); 
	public static List<BI_COL_Modificacion_de_Servicio__c>	lstMS;


	@testSetup public static void CrearData()
	{
		//perfiles
		//lstPerfil               		= [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1 ];
		
		//JEG		
		//Usuario		
		List<User> lst_usu = [SELECT Id, Email FROM User WHERE IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema and BI_Permisos__c = 'Administrador del sistema' LIMIT 1];
		//usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();
		usu = lst_usu[0];

		System.runAs(usu)
		{		
			//Roles
			//lstRoles                = new list <UserRole>();
			//lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
			//system.debug('datos Rol '+lstRoles);
			//lstRol

			//lstRol
        	lstRoles = new list <UserRole>();
        	lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        	/*
        	//ObjUsuario
        	objUsuario = new User();
        	objUsuario.Alias = 'standt';
        	objUsuario.Email ='pruebas@test.com';
        	objUsuario.EmailEncodingKey = '';
        	objUsuario.LastName ='Testing';
        	objUsuario.LanguageLocaleKey ='en_US';
        	objUsuario.LocaleSidKey ='en_US'; 
        	objUsuario.ProfileId = lstPerfil.get(0).Id;
        	objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        	objUsuario.UserName ='pruebas@test.com';
        	objUsuario.EmailEncodingKey ='UTF-8';
        	objUsuario.UserRoleId = lstRoles.get(0).Id;
        	objUsuario.BI_Permisos__c ='Sucursales';
        	objUsuario.Pais__c='Colombia';
        	insert objUsuario;
        
        	usu = new list<User>();
        	usu.add(objUsuario);
        	*/

			//Cuentas
			objCuenta 										= new Account();      
			objCuenta.Name                                  = 'prueba';
			objCuenta.BI_Country__c                         = 'Colombia';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Fraude__c                          = false;
			objCuenta.BI_Segment__c                         = 'test';
	        objCuenta.BI_Subsegment_Regional__c             = 'test';
	        objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			system.debug('datos Cuenta '+objCuenta);

			/*
			objCuenta2                                       = new Account();
			objCuenta2.Name                                  = 'prueba';
			objCuenta2.BI_Country__c                         = 'Colombia';
			objCuenta2.TGS_Region__c                         = 'América';
			objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta2.CurrencyIsoCode                       = 'COP';
			objCuenta2.BI_Fraude__c                          = true;
			insert objCuenta2;
			*/
			//Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
			 
            //Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
            objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objCuenta.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
            objContacto.Phone                               = '12345678'; //JEG 2016/12
            objContacto.MobilePhone                         = '3104785925';
            objContacto.Email                               = 'pruebas@pruebas1.com';
            objContacto.BI_COL_Direccion_oficina__c         = 'Dirprueba 34550';
            objContacto.BI_COL_Ciudad_Depto_contacto__c     = objCiudad.Id;
            objContacto.BI_Tipo_de_documento__c				= 'RUT';	
			objContacto.BI_Numero_de_documento__c			= '123456789';
            //REING-INI
            //objContacto.BI_Tipo_de_contacto__c          = 'Autorizado';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
            objContacto.BI_Tipo_de_contacto__c          = 'General';	
       		//REING_FIN
            Insert objContacto;
			
			//catalogo
			NE__Catalog__c objCatalogo 						= new NE__Catalog__c();
			objCatalogo.Name 								= 'prueba Catalogo';
			objCatalogo.BI_Country__c                       =  'Colombia';
			insert objCatalogo; 
			
			//Campaña
			objCampana 										= new Campaign();
			objCampana.Name 								= 'Campaña Prueba';
			objCampana.BI_Catalogo__c 						= objCatalogo.Id;
			objCampana.BI_COL_Codigo_campana__c 			= 'prueba1';
			objCampana.BI_Country__c 						= 'Colombia';
            objCampana.BI_Opportunity_Type__c 				= 'Fijo';
            objCampana.BI_SIMP_Opportunity_Type__c			= 'Alta'; 
			insert objCampana;
			lstCampana 										= new List<Campaign>();
			lstCampana 										= [select Id, Name from Campaign where Id =: objCampana.Id];
			system.debug('datos Cuenta '+lstCampana);
			
			//Tipo de registro
			lstRecTyp 										= [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
			system.debug('datos de FUN '+lstRecTyp);
			
			//Contrato
			objContrato 									= new Contract ();
			objContrato.AccountId 							= objCuenta.Id;
			objContrato.StartDate 							=  System.today().addDays(+5);
			objContrato.BI_COL_Formato_Tipo__c 				= 'Contrato Marco';
			objContrato.BI_COL_Tipo_contrato__c             = 'Contrato Telefónica';
			objContrato.BI_Indefinido__c 					= 'Si';
			objContrato.BI_COL_Cuantia_Indeterminada__c 	= True;
			objContrato.BI_COL_Duracion_Indefinida__c 		= True;
			objContrato.ContractTerm 						= 12;
			objContrato.BI_COL_Presupuesto_contrato__c 		= 1000000;
			objContrato.StartDate							= System.today().addDays(+5);
			insert objContrato;
			System.debug('datos Contratos ===>'+objContrato);
			
			//FUN
			objAnexos               						= new BI_COL_Anexos__c();
			objAnexos.Name          						= 'FUN-0041414';
			objAnexos.RecordTypeId  						= lstRecTyp[0].Id;
			objAnexos.BI_COL_Contrato__c   					= objContrato.Id;
			insert objAnexos;
			System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
			
			//Configuracion Personalizada
			BI_COL_manage_cons__c objConPer 				= new BI_COL_manage_cons__c();
			objConPer.Name                  				= 'Viabilidades';
			objConPer.BI_COL_Numero_Viabilidad__c 			= 46;
			objConPer.BI_COL_ConsProyecto__c     			= 1;
			objConPer.BI_COL_ValorConsecutivo__c  			=1;
			lstManege                      					= new list<BI_COL_manage_cons__c >();        
			lstManege.add(objConPer);
			insert lstManege;
			System.debug('======= configuracion personalizada ======= '+lstManege);
			
			//Oportunidad
			objOportunidad                                      	= new Opportunity();
			objOportunidad.Name                                   	= 'prueba opp';
			objOportunidad.AccountId                              	= objCuenta.Id;
			objOportunidad.BI_Country__c                          	= 'Colombia';
			objOportunidad.CloseDate                              	= System.today().addDays(+5);
			objOportunidad.StageName                              	= 'F6 - Prospecting';
			objOportunidad.CurrencyIsoCode                        	= 'COP';
			objOportunidad.Certa_SCP__contract_duration_months__c 	= 12;
			objOportunidad.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
			objOportunidad.OwnerId                                	= usu.id;
			insert objOportunidad;
			system.debug('Datos Oportunidad'+objOportunidad);
			list<Opportunity> lstOportunidad 	= new list<Opportunity>();
			lstOportunidad 						= [select Id from Opportunity where Id =: objOportunidad.Id];
			system.debug('datos ListaOportunida '+lstOportunidad);
			
			//Ciudad
			objCiudad = new BI_Col_Ciudades__c ();
			objCiudad.Name 						= 'Test City';
			objCiudad.BI_COL_Pais__c 			= 'Test Country';
			objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
			insert objCiudad;
			System.debug('Datos Ciudad ===> '+objSede);
			
			//Direccion
			objSede 								= new BI_Sede__c();
			objSede.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
			objSede.BI_Direccion__c 				= 'Test Street 123 Number 321';
			objSede.BI_Localidad__c 				= 'Test Local';
			objSede.BI_COL_Estado_callejero__c 		= System.Label.BI_COL_LbValor3EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c 		= 'Libre';
			objSede.BI_Country__c 					= 'Colombia';
			objSede.Name 							= 'Test Street 123 Number 321, Test Local Colombia';
			objSede.BI_Codigo_postal__c 			= '12356';
			insert objSede;
			System.debug('Datos Sedes ===> '+objSede);
			
			//Sede
			objPuntosInsta 						= new BI_Punto_de_instalacion__c ();
			objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
			objPuntosInsta.BI_Sede__c          = objSede.Id;
			objPuntosInsta.BI_Contacto__c      = objContacto.id;
			objPuntosInsta.Name 				='Prueba Sucursales';
			insert objPuntosInsta;
			System.debug('Datos Sucursales ===> '+objPuntosInsta);
			
			//DS
			objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
			objDesSer.CurrencyIsoCode               			= 'COP';
			insert objDesSer;
			System.debug('Data DS ====> '+ objDesSer);
			System.debug('Data DS ====> '+ objDesSer.Name);
			lstDS 	= [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
			System.debug('Data lista DS ====> '+ lstDS);

			//MS
			objMS                         				= new BI_COL_Modificacion_de_Servicio__c();
			objMS.BI_COL_FUN__c           				= objAnexos.Id;
			objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
			objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c 					= false;
			objMS.BI_COL_Estado__c 						= 'Activa';//label.BI_COL_lblActiva;
			objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
			objMs.BI_COL_Medio_Preferido__c 			= 'Cobre';
			objMS.BI_COL_Clasificacion_Servicio__c		= 'Alta demo';
			objMS.BI_COL_TRM__c 						= 5;
			insert objMS;
			system.debug('Data objMs ===>'+objMs);
			BI_COL_Modificacion_de_Servicio__c objr = [select BI_COL_Monto_ejecutado__c from BI_COL_Modificacion_de_Servicio__c where id =: objMS.id];
			system.debug('\n@-->Data consulta ===>'+objr);
		}

	}
	
	@isTest static void test_method_1() 
	{
		//// Implement test code
		//crearData();
		//objDSPopUp = new BI_COL_SelectDSPopUp_ctr();

		//objDSPopUp.lstDSQuery = lstDS;

		//objDSPopUp.fnFilterDS();
		////objDSPopUp.fnFindDS();

		//CrearData();
		objCuenta = [Select Id from Account limit 1];
		PageReference pageRef           = Page.BI_COL_SelectDSPopUp_pag;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		pageRef.getParameters().put('sIdwrp','prueba');
		pageRef.getParameters().put('IdProd', 'a0ig0000002RtHy');           
		pageRef.getParameters().put('NomProd','prueba');
		pageRef.getParameters().put('Filtro',objCuenta.id);
		BI_COL_SelectDSPopUp_ctr objDSPopUp = new BI_COL_SelectDSPopUp_ctr();
		objDSPopUp.lstDSQuery = lstDS;		
		objDSPopUp.fnFilterDS();
		Test.stopTest(); 
	}
	
}