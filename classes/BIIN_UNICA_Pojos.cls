/*---------------------------------------------------------------------------------------
  Author:        José Luis González
  Company:       HPE
  Description:   Pojos for UNICA interface

  <Date>                 <Author>                <Change Description>
  26/05/2016             José Luis González      Initial Version
  28/12/2016             Adrián Caro Moure       ExceptionType2 added
---------------------------------------------------------------------------------------*/
global class BIIN_UNICA_Pojos
{
  public class CustomersLixt
  {
    public CustomersListType customersListType;
  }
  
  public class CustomersListType
  {
    public List<CustomerType> customerList;
    public Integer totalResults;
  }

  public class CustomerType
  {
    public String customerId;
    public CustomerInfoType customerDetails;
    public List<IdentificationType> legalId;
    public List<AddressInfoType> customerAddresses;
    public List<ContactingModeType> contactingModes;
  }


  public class CustomerRequestType
  {
    public CustomerInfoType customerDetails;
    public List<IdentificationType> legalId;
    public List<AddressInfoType> customerAddresses;
    public List<ContactingModeType> contactingModes;
    public String callbackUrl;
  }

  public class CustomerInfoType
  {
    public String correlatorId;
    public String customerName;
    public String description;
    public String parentCustomer;
    public Integer customerRank;
    public String customerStatus;
    public SegmentType segment;
    public String subsegment;
    public String cstType;
    public Integer satisfaction;
    public List<CreditProfileType> creditProfiles;
    public List<KeyValueType> additionalData;
  }

  public class KeyValueType
  {
    public KeyValueType(String key, String value) {
      this.key = key;
      this.value= value;
    }
    public String key;
    public String value;
  }

  public class IdentificationType
  {
    public String country;
    public String nationalIDType;
    public String nationalID;
    public boolean isPrimary;

  }

  public class AddressInfoType
  {
    public String addressType;
    public String normalized;
    public String addressName;
    public AddrValueType addressNumber;
    public AddrValueType floor;
    public AddrValueType apartment;
    public String area;
    public String locality;
    public String municipality;
    public String region;
    public String country;
    public String postalCode;
    public String addressExtension;
    public CoordinatesType coordinates;
    public boolean isDangerous;
    public List<KeyValueType> additionalData;
  }

  public class AddrValueType
  {
    public String value;
    public AddrRangeType range;
  }

  public class AddrRangeType
  {
    public String lowerValue;
    public String upperValue;
  }

  public class CoordinatesType
  {
    public String longitude;
    public String latitude;
  }

  public class ContactingModeType
  {
    public boolean isPreferred;
    public String contactMode;
    public String details;
  }

  public enum CustomerStatusType
  {
    active, inactive, prospective
  }

  public enum SegmentType
  {
    consumer, smb, corporate, retailer, wholesale
  }

  public enum CreditProfileType
  {
    creditProfileDate, creditRiskRating, creditScore
  }
  
  // INI POJOS BIIN_Listado_Ticket_WS_UNICA
  public class TicketsListType 
  {
    public List<TicketDetailType> ticketList;
    public Integer totalResults;
  }

  global class TicketDetailType 
  {
    public String ticketId;
    public String correlatorId;
    public String subject;
    public String description;
    public String country;
    public String customerId;
    public String accountId;
    public String reportedDate;
    public String creationDate;
    public String severity;
    public Integer priority;
    public String requestedSeverity;
    public Integer requestedPriority;
    public String ticketType;
    public String source;
    public String parentTicket;
    public TicketStatusType ticketStatus;
    public String ticketSubstatus;
    public String statusChangeDate;
    public String statusChangeReason;
    public String targetResolutionDate;
    public String resolutionDate;
    public String resolution;
    public String responsibleParty;
    public List<RelatedPartyType> relatedParties;
    public List<RelatedObjectType> relatedObjects;
    public List<TicketNoteInfoType> notes;
    public List<TicketAttachmentInfoType> attachments;
    public List<KeyValueType> additionalData;
  }

  global class TicketStatusChangeType
  {
    public TicketStatusType ticketStatus;
    public String  ticketSubstatus;
    public String statusChangeReason;

      // No cumple UNICA pero lo han anyadido en BAO
      public List<KeyValueType> additionalData;
    }

    global class TicketStatusInfoType
    {
      public String ticketId;
      public String ticketStatus;//TicketStatusType
      public DateTime statusChangeDate;
      public String statusChangeReason;
    }


    public enum TicketStatusType
    { 
      NEW_STATUS, SUBMITTED, ACKNOWLEDGED, IN_PROGRESS, RESOLVED, CLOSED, CANCELLED, REJECTED, PENDING, ASSIGNED
    }
    
    public class RelatedPartyType 
    {
      public String role;
      public String reference;
      public String resourceUri;
    }
    
    public class RelatedObjectType 
    {
      public String relationship;
      public String reference;
      public String resourceUri;
      public List<KeyValueType> additionalData;
    }
    
    public class TicketNoteInfoType 
    {
      public String noteId;
      public String creationDate;
      public String author;
      public String text;
      public List<TicketAttachmentInfoType> attachments;
      public List<KeyValueType> additionalData;
    }
    
    public class TicketAttachmentInfoType 
    {
      public String attachmentId;
      public String creationDate;
      public String author;
      public String name;
      public String documentLink;
      public List<KeyValueType> additionalData;
    }
    // FIN POJOS BIIN_Listado_Ticket_WS_UNICA
    
    // INI POJOS BIIN_Actualizacion_Caso_UNICA
    global class TicketRequestType
    {
      public String correlatorId;
      public String subject;
      public String description;
      public String country;
      public String customerId;
      public String accountId;
      public String reportedDate;
      public String perceivedSeverity;
      public integer perceivedPriority;
      public String ticketType;
      public String source;
      public String parentTicket;
      public List<RelatedPartyType> relatedParties;
      public List<RelatedObjectType> relatedObjects;
      public List<TicketNoteRequestType> notes;
      public List<TicketAttachmentRequestType> attachments;
      public List<KeyValueType> additionalData;
      public String callbackUrl;
    }

    public class TicketNoteRequestType
    {
      public String author;
      public String text;
      public List<TicketAttachmentRequestType> attachments;
      public List<KeyValueType> additionalData;
    }

    public class TicketAttachmentRequestType
    {
      public String author;
      public String name;
      public String documentLink;
      public AttachmentType attachment;
      public List<KeyValueType> additionalData;
    }

    public class AttachmentType
    {
      public AttachmentSummaryType attachmentSummary;
      /** Encoded in Base64 */
      public String content;
    }

    public class AttachmentSummaryType
    {
      public String name;
      public String contentType;
      public String contentId;
      public Integer contentLength;
    }

    // FIN POJOS BIIN_Actualizacion_Caso_UNICA
    
    // INI POJOS BIIN_SiteSearch_Creacion_Ctrl_UNICA
    public class SitesListType 
    {
      public List<SiteType> sites;
      public Integer totalResults;
    }
    
    public class SiteType 
    {
      public String siteId;
      public SiteDetailsType siteDetails;
    }
    
    public class SiteDetailsType 
    {
      public String siteName;
      public SiteStatusType siteStatus;
      public AddressInfoType siteAddress;
      public List<KeyValueType> additionalData;
    }
    
    public enum SiteStatusType 
    {
      active, inactive
    }
    // FIN POJOS BIIN_SiteSearch_Creacion_Ctrl_UNICA
    
    // INI POJOS BIIN_LegalEntity_WS_UNICA
    public class ContactDetailsType
    {
      public String correlatorId;
      public NameDetailsType name;
      public List<String> customers;
      public List<String> accounts;
      public List<String> roles;
      public ContactStatusType contactStatus;
      public List<ContactingModeType> contactingModes;
      public List<AddressInfoType> addresses;
      public List<IdentificationType> legalId;
      public PersonalInfoType personal;
      public List<KeyValueType> additionalData;
    }
    
    public class NameDetailsType
    {
      public String fullName;
      public String title;
      public String givenName;
      public String familyName;
      public String middleName;
      public String suffix;
      public String displayName;
    }
    
    public enum ContactStatusType
    {
      active, inactive
    }
    
    public class PersonalInfoType
    {
      public String gender;
      public String birthdate;
      public Integer age;
      public String maritalStatus;
      public List<KeyValueType> additionalData;
    }
    // FIN POJOS BIIN_LegalEntity_WS_UNICA
    
    // INI POJOS BIIN_Base_UNICA
    public class ResponseObject
    {
      public Integer responseStatusCode;
      public String responseStatus;
      public Object responseObject;
      public ExceptionType exceptionObject;
      public ExceptionType2 exceptionObject2;

      public ResponseObject()
      {
        
      }
      public ResponseObject(HttpResponse res)
      {
        responseStatus = res.getStatus();
        responseStatusCode = res.getStatusCode();
      }

      public Boolean isResponseOK()
      {
        return responseStatusCode == 200 || responseStatusCode == 201  || responseStatusCode == 202;
      }
    }

    // FIN POJOS BIIN_Base_UNICA
    public class CisListType 
    {
      public List<CisType> assets;
      public Integer totalResults;
    }

    public class CisType
    {
      public String category;
      public String classId;
      public String company;
      public String country;
      public String item;
      public String name;
      public String reconId;
      public String type;
    }

    // INI POJOS BIIN_Obtener_Contacto_WS
    public class ContactsListType
    {
      public List<ContactType> contacts;
      public Integer totalResults;
    }

    public class ContactType
    {
      public String contactId;
      public ContactDetailsType contactDetails;
    }
    // FIN POJOS BIIN_Obtener_Contacto_WS

    public class ApiTransactionType
    {
      public String transactionId;
      public ApiTransactionStatusType transactionStatus;
      public String resourceUri;
    }

    public class ApiTransactionStatusType
    {
      public String transactionStatus;
      public ExceptionType error;
      public ExceptionType2 error2;
    }

    public class ExceptionType
    {
      public String id;
      public String text;

      public ExceptionType(String id, String text)
      {
        this.id   = id;
        this.text = text;
      }
    }

    public class ExceptionType2
    {
      public String id;
      public String text1;
      public String text2;

      public ExceptionType2(String id, String text1, String text2)
      {
        this.id   = id;
        this.text1 = text1;
        this.text2 = text2;
      }
    }
    
    public class LoginRequestType
    {
      public Credentials credentials;
    }

    public class Credentials
    {
      public String username;
      public String password;
    }
  }