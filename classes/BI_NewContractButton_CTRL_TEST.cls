@isTest
private class BI_NewContractButton_CTRL_TEST {
	
	@testSetup static void testSetup(){
		 List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
 		 List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
		 List <Opportunity> lst_opp = BI_DataLoad.loadOpportunities(lst_acc[0].Id);
	}

	@isTest static void redirectBIEN_test() {
		    Opportunity miOpty = [Select id from Opportunity limit 1];
			Test.startTest();
			PageReference page = new PageReference('/id=' + miOpty.Id );
			Test.setCurrentPage(page);
			BI_NewContractButton_CTRL obj = new BI_NewContractButton_CTRL(new ApexPages.StandardController(miOpty));			
        	obj.redirect();
			Test.stopTest();

	}

	@isTest static void redirectOLA4_test() {
            Account miAcc = [Select id from Account limit 1];
            miAcc.TGS_Es_MNC__c = true;
            update miAcc;

            Opportunity miOpty = [Select id from Opportunity limit 1];
			Test.startTest();
			PageReference page = new PageReference('/id=' + miOpty.Id );
			Test.setCurrentPage(page);
			BI_NewContractButton_CTRL obj = new BI_NewContractButton_CTRL(new ApexPages.StandardController(miOpty));			
        	obj.redirect();
			Test.stopTest();

	}
	

}