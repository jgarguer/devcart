/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro Sevilla
    Company:       NE Aborda
    Description:   Methods exceuted by Contract Triggers
    
    History:
    
    <Date>            <Author>          <Description>
    02/06/2016       Alvaro Sevilla     Initial version
    03/06/2016       Alvaro Sevilla     version 2.0 <Se adicionó la creacion de registros datos desempeño>
    14/07/2016       Alvaro Sevilla     Se modifico condicion de creacion de caso y valores para alguno campos del mismo
    19/07/2016       Alvaro Sevilla     Se modifico para que llenara el campo descripcion con la URL durante la creacion del caso no durante la actualizacion
    22/07/2016       Humberto Nunes     Se Cambio el contacto de la Oportunidad por el del Contrato.
    22/07/2016       Humberto Nunes     Se Quito el llenado del URL de la Encuesta de aca y se coloco en el update que reasigna el caso BI_CaseMethods.createAssignmentRuleRows. 
    09/11/2016       Humberto Nunes     Se Cambio 'AC' POR 'Actos Comerciales'.
    22/11/2016       Humberto Nunes     Se Omitio la generacion del BI_Registro_Datos_Desempeno__c ya que se crea en BI_ContractMethods.CreateRegistroDesemp.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_FVI_CasosContratosFVI {

    public static void BI_FVI_CrearCasosInternos(list<Contract> lstContratosNew,list<Contract> lstContratosOld){

        try{

            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');                
            
            list<Case> lstCasosInternosFVI = new list<Case>();
            list<BI_Registro_Datos_Desempeno__c> lstRegDatosDesemp = new list<BI_Registro_Datos_Desempeno__c>();
            //map<String,id> mpTiposRegistros = new map<String,id>();
            RecordType TRContrato = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_FVI_Contrato'];


            //for(RecordType TR :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'Contract']){
            //        mpTiposRegistros.put(TR.DeveloperName, TR.Id);
            // }

            RecordType TRCasoInterno = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno'];

            map<Id,Contract> mpContrato = new map<Id,Contract>([SELECT Account.BI_Country__c,BI_Oportunidad__r.BI_FVI_Contacto__c, CustomerSignedId  FROM Contract WHERE Id IN :lstContratosNew]);

            for(Integer i = 0; i < lstContratosNew.size(); i++){

                if(lstContratosOld[i].Status != lstContratosNew[i].Status){


                    if(lstContratosNew[i].RecordTypeId == TRContrato.Id && (lstContratosNew[i].Status == 'Presentado Firmado' || lstContratosNew[i].Status == 'Presented Signed')){

                        Case CasoInternoFVI = new Case();
                        CasoInternoFVI.RecordTypeId = TRCasoInterno.Id;
                        CasoInternoFVI.AccountId = lstContratosNew[i].AccountId;
                        CasoInternoFVI.BI_FVI_Contrato__c = lstContratosNew[i].Id;
                        CasoInternoFVI.BI_Country__c = mpContrato.get(lstContratosNew[i].Id).Account.BI_Country__c;
                        CasoInternoFVI.ContactId = mpContrato.get(lstContratosNew[i].Id).CustomerSignedId; // mpContrato.get(lstContratosNew[i].Id).BI_Oportunidad__r.BI_FVI_Contacto__c;
                        CasoInternoFVI.BI_Nombre_de_la_Oportunidad__c = lstContratosNew[i].BI_Oportunidad__c;
                        CasoInternoFVI.BI_Department__c = 'FVI';
                        CasoInternoFVI.BI_Type__c = 'Solicitudes';
                        CasoInternoFVI.BI_Subtype__c = 'Validación Telefónica y Encuesta de Satisfacción';
                        CasoInternoFVI.Status = 'New';
                        CasoInternoFVI.Priority = 'Media';
                        CasoInternoFVI.Origin = 'Tablet';
                        CasoInternoFVI.Subject = 'Rellenar encuesta de satisfaccion al cliente';
                        CasoInternoFVI.Description = 'URL Encuesta: ';
                        
                        lstCasosInternosFVI.add(CasoInternoFVI);
/*
// 22/11/2016 OMITIDO POR HN YA SE ESTA HACIENDO EN BI_ContractMethods.CreateRegistroDesemp
                        if(lstContratosNew[i].BI_FVI_Actos_Comerciales__c != null && lstContratosNew[i].BI_FVI_Actos_Comerciales__c > 0){

                            BI_Registro_Datos_Desempeno__c objRegDatosDesemp = new BI_Registro_Datos_Desempeno__c();
                            Datetime dtConvertDT = System.now();    
                            objRegDatosDesemp.BI_FVI_Fecha__c = date.newinstance(dtConvertDT.year(), dtConvertDT.month(), dtConvertDT.day());
                            objRegDatosDesemp.BI_FVI_Id_Cliente__c = lstContratosNew[i].AccountId;
                            objRegDatosDesemp.BI_Tipo__c = 'Actos Comerciales'; // ANTES 'AC';
                            objRegDatosDesemp.BI_FVI_Valor__c = lstContratosNew[i].BI_FVI_Actos_Comerciales__c;
                            objRegDatosDesemp.BI_FVI_IdObjeto__c = lstContratosNew[i].Id;

                            lstRegDatosDesemp.add(objRegDatosDesemp);

                        }else{

                            System.debug('---># Actos Comerciales esta vacio o es cero');
                        }
*/

                    }else{

                        System.debug('--->El tipo de registro no es Contrato FVI');
                    }
                    
                }
            }

            if(lstCasosInternosFVI != null && lstCasosInternosFVI.size() > 0){

                System.debug('--->Se lleno la lista');
                insert lstCasosInternosFVI;

            } 
/*
// 22/11/2016 OMITIDO POR HN YA SE ESTA HACIENDO EN BI_ContractMethods.CreateRegistroDesemp

            if(lstRegDatosDesemp != null && lstRegDatosDesemp.size() > 0){

                insert lstRegDatosDesemp;
                System.debug('--->llego con algo');
            }      
*/
        }catch(exception e){
            System.debug('----->EXEPTION: '+e);
             BI_LogHelper.generate_BILog('BI_FVI_CasosContratosFVI.BI_FVI_CrearCasosInternos', 'BI_EN', e, 'Trigger');
        }
    }
}