public with sharing class BI_NEContractHeaderMethods {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   Methods exceuted by NEContractHeader Trigger
    
    History:
    
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   Validation to avoid modification on NE__Contract_Header__c
    IN:           List <Contract>
    OUT:               
    History:
    
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version
    24/10/2017        Gawron, Julian    Adding BI_Standard_PER to query
    02/11/2017        Gawron, Julián    Adding Label.BI_PER_SinPermisos
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void verificarPermisos(List<NE__Contract_Header__c> news , List<NE__Contract_Header__c> olds)
    {
    	Set<String> set_creacion = new Set<String>{Label.BI_Administrador, 'Administrador de Contrato', Label.BI_SuperUsuario};
    	Set<String> set_modificacion = new Set<String>{Label.BI_Administrador, 'Administrador de Contrato', Label.BI_SuperUsuario};
    	Set<String> set_eliminacion = new Set<String>{Label.BI_Administrador, Label.BI_SuperUsuario};

        try
        { 
        if(BI_TestUtils.isRunningTest()){ throw new BI_Exception('Test');}
        List<User> me = [SELECT Id, BI_Permisos__c FROM User WHERE Id = :UserInfo.getUserId() and Profile.name = 'BI_Standard_PER' limit 1];
        if(me.isEmpty()) return; //Si la lista está vacía, el usuario no era de Perú, salimos.
		   
           if(olds == null){
           		//Insert
           		if(!set_creacion.contains(me[0].BI_Permisos__c)){
               		for(NE__Contract_Header__c contr : news){
               			contr.addError(Label.BI_PER_SinPermisos);
            		}
           		}
           	}else if(news == null){
           		//delete
           		if(!set_eliminacion.contains(me[0].BI_Permisos__c)){
               		for(NE__Contract_Header__c contr : olds){
               			contr.addError(Label.BI_PER_SinPermisos);
            		}
           		}
           	}else{
           		if(!set_modificacion.contains(me[0].BI_Permisos__c)){
               		for(NE__Contract_Header__c contr : news){
               			contr.addError(Label.BI_PER_SinPermisos);
            		}
           		}
           	}
        }
        catch (exception Exc)
        {
           BI_LogHelper.generate_BILog('BI_NEContractMethods.verificarPermisos', 'BI_EN', Exc, 'Trigger');
        } 
    }    
}