public with sharing class BI_OrderMethods {
	
	

    public static Map<Id, Opportunity> getMapOpp(List <NE__Order__c> news){
    	Map <Id,Opportunity> map_opp;
        Set <Id> set_idOpps = new Set <Id>();
        for(NE__Order__c ord : news){
            if(ord.NE__OptyId__c != null){

                set_idOpps.add(ord.NE__OptyId__c);
            }
        }
        if(!set_idOpps.isEmpty()){

            //map_opp = new Map <Id,Opportunity> ([SELECT Id,BI_Descuento_bloqueado__c, (SELECT Id, NE__Version__c, NE__OrderStatus__c FROM NE__Orders__r) FROM Opportunity WHERE Id in:set_idOpps AND BI_CoE_Duplicando__c = false]);
            // FAR 23/09/2016: Don't get Quote orders
            map_opp = new Map <Id,Opportunity> ([SELECT Id,BI_Descuento_bloqueado__c, (SELECT Id, NE__Version__c, NE__OrderStatus__c FROM NE__Orders__r WHERE RecordType.DeveloperName != 'Quote') FROM Opportunity WHERE Id in:set_idOpps AND BI_CoE_Duplicando__c = false]);
        }
        return map_opp;
    }
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that set the version of the new order
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    08/04/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void setOrderVersion(List <NE__Order__c> news){

		List <NE__Order__c> toProcess = new List <NE__Order__c>();
		Map <Id, Set <Integer>> map_oppVersions = new Map <Id, Set <Integer>>();
		Map <Id, Opportunity> map_opp = new Map <Id, Opportunity>();
		System.debug('########news----->' + news);
		try{
			for(NE__Order__c ord : news){

				if(ord.NE__OptyId__c != null && ord.NE__Version__c != null){
					toProcess.add(ord);
					System.debug('########orden---->' + ord);
				}
			}
	        
	        if(!toProcess.isEmpty()){
	        	map_opp = getMapOpp(toProcess);
	    	}
	        

			if(map_opp != null){

				for(NE__Order__c ord : toProcess){

					if(map_opp.containsKey(ord.NE__OptyId__c)){
						if(!map_opp.get(ord.NE__OptyId__c).NE__Orders__r.isEmpty()){

							Set <Integer> aux = new Set <Integer>();
							for(NE__Order__c ordOfOpp : map_opp.get(ord.NE__OptyId__c).NE__Orders__r){

								aux.add(Integer.valueOf(ordOfOpp.NE__Version__c));
							}
							map_oppVersions.put(ord.NE__OptyId__c, aux);
							System.debug('...............aux---> ' + aux);
						}
					}
				}

				for(NE__Order__c ord : toProcess){
					if(map_oppVersions.containsKey(ord.NE__OptyId__c)){
						Integer max = 0;
						for(Integer i : map_oppVersions.get(ord.NE__OptyId__c)){
							if(i > max){
								max = i;
							}
						}
						ord.NE__Version__c = Integer.valueOf(max + 1);
						System.debug(LoggingLevel.ERROR, '**********Version -->' + Integer.valueOf(ord.NE__Version__c));
						map_oppVersions.get(ord.NE__OptyId__c).add(Integer.valueOf(ord.NE__Version__c));
						/*if(map_oppVersions.get(ord.NE__OptyId__c).contains(Integer.valueOf(ord.NE__Version__c))){
							ord.addError('No se pueden asociar dos ofertas con la misma versión a la oportunidad');
						}*/
					}
				}
			}
		}catch(Exception exc){
			BI_LogHelper.generate_BILog('BI_OrderMethods.setOrderVersion', 'BI_EN', exc, 'Trigger');
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that set the status of all the ordes that are related to an opportunity
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        12/04/2016                      Guillermo Muñoz             Initial version
        26/09/2016                      Fernando Arteaga            Don't set status for Quotes (status is Draft when created, and Active on user demand)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void setOrderStatus(List <NE__Order__c> news){

		List <NE__Order__c> toProcess = new List <NE__Order__c>();
		Map <Id, List <NE__Order__c>> map_oppVersions = new Map <Id, List <NE__Order__c>>();
		List <NE__Order__c> lst_toUpdate = new List <NE__Order__c>();
		Map <Id, Opportunity> map_opp = new Map <Id, Opportunity>();
		System.debug('********news----->' + news);
		try{
			for(NE__Order__c ord : news){

				if(ord.NE__OptyId__c != null && ord.NE__Version__c != null){
					toProcess.add(ord);
					System.debug('********orden---->' + ord);
				}
			}

			if(!toProcess.isEmpty()){
	        	map_opp = getMapOpp(toProcess);
	    	}

			if(map_opp != null){

				for(NE__Order__c ord : toProcess){

					if(map_opp.containsKey(ord.NE__OptyId__c)){
						if(!map_opp.get(ord.NE__OptyId__c).NE__Orders__r.isEmpty()){

							List <NE__Order__c> aux = new List <NE__Order__c>();
							for(NE__Order__c ordOfOpp : map_opp.get(ord.NE__OptyId__c).NE__Orders__r){

								aux.add(ordOfOpp);
							}
							map_oppVersions.put(ord.NE__OptyId__c, aux);
						}
					}
				}

				for(NE__Order__c ord : toProcess){
					Integer maxVersion = 0;
					if(map_oppVersions.containsKey(ord.NE__OptyId__c)){

						if(!map_oppVersions.get(ord.NE__OptyId__c).isEmpty()){
							for(NE__Order__c ordOfOpp : map_oppVersions.get(ord.NE__OptyId__c)){
								if(Integer.valueOf(ordOfOpp.NE__Version__c) > maxVersion){
									maxVersion = Integer.valueOf(ordOfOpp.NE__Version__c);
								}
							}

							for(NE__Order__c ordOfOpp : map_oppVersions.get(ord.NE__OptyId__c)){
								// FAR 26/09/2016 : Don't set status for Quotes (status is Revised when created, and Active on user demand)
								//if (ordOfOpp.RecordType.DeveloperName != 'Quote') {
									if(Integer.valueOf(ordOfOpp.NE__Version__c) < maxVersion){
										if(ordOfOpp.NE__OrderStatus__c != 'Revised'){
											ordOfOpp.NE__OrderStatus__c = 'Revised';
											lst_toUpdate.add(ordOfOpp);
										}
									}
									else if(Integer.valueOf(ordOfOpp.NE__Version__c) == maxVersion){
										if(ordOfOpp.NE__OrderStatus__c != Label.BI_Active){
											ordOfOpp.NE__OrderStatus__c = Label.BI_Active;
											lst_toUpdate.add(ordOfOpp);
										}
									}
								//}
							}

						}

					}

				}
				if(!lst_toUpdate.isEmpty()){
					update lst_toUpdate;
				}
			}
		}catch(Exception exc){
			BI_LogHelper.generate_BILog('BI_OrderMethods.setOrderStatus', 'BI_EN', exc, 'Trigger');
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that set the order status when an Order was created or modified
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/04/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void setEditableDiscount(List <NE__Order__c> news, List <NE__Order__c> olds){

		List <NE__Order__c> toProcess = new List <NE__Order__c>();
		Map <Id, Opportunity> map_opp;
		Set <Id> set_idOpp = new Set <Id>();
		System.debug('entro setEditableDiscount');
		try{

			for(NE__Order__c ord : news){

				if(ord.NE__OptyId__c != null && ord.NE__OrderStatus__c == Label.BI_Active){
					toProcess.add(ord);
					set_idOpp.add(ord.NE__OptyId__c);
				}
			}

			if(!toProcess.isEmpty()){

				map_opp = new Map <Id, Opportunity>([SELECT Id, BI_Descuento_bloqueado__c, StageName FROM Opportunity WHERE Id IN: set_idOpp]);
				System.debug('---------->' + map_opp);

				for(NE__Order__c ord : toProcess){

					if(map_opp.containsKey(ord.NE__OptyId__c)){

						if(map_opp.get(ord.NE__OptyId__c).BI_Descuento_bloqueado__c && map_opp.get(ord.NE__OptyId__c).StageName != 	Label.BI_F1Ganada){

							ord.NE__OrderStatus__c = Label.BI_Order_producto_bloqueado;
							System.debug('----->Modificado');
						}
					}
				}
			}
		}catch(Exception exc){
			BI_LogHelper.generate_BILog('BI_OrderMethods.setEditableDiscount', 'BI_EN', exc, 'Trigger');
		}
	}

		/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Julián Gawron
	    Company:       Accenture
	    Description:   Method that call the BI_AccountHelper.updateOfertaDigital if is necessary
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    17/02/2017                      Gawron, Julian              Initial version D398
	    22/02/2017                      Gawron, Julián              Adding Future Methods
	    18/05/2017		  				Ángela Latorre	   			Add wasFired to make sure that the future method is only called one time
		06-03-2018		  				Manuel Ochoa          		check if isBatch() for bulk orders
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void actualizarAccountCoberturaDigital(List <NE__Order__c> news, List <NE__Order__c> olds){

		Set <Id> toProcess = new Set <Id>();
		System.debug('entro actualizarAccountCoberturaDigital el news tiene:' + news );
		try{
			for(Integer i=0; i < news.size(); i++){
				//si la order ahora está activa
				//Insert que está activa
				if(news[i].NE__OrderStatus__c == Label.BI_Active 
					|| news[i].NE__OrderStatus__c == Label.BI_Activo_pendiente_aprobacion 
					|| news[i].NE__OrderStatus__c == Label.BI_ActiveRapido ){
				     toProcess.add(news[i].NE__AccountId__c);
				     		System.debug('entro actualizarAccountCoberturaDigital agregamos uno activo');
				}
				if((olds != null) && olds[i].NE__OrderStatus__c != news[i].NE__OrderStatus__c ){
				     toProcess.add(olds[i].NE__AccountId__c);
				     		System.debug('entro actualizarAccountCoberturaDigital agregamos uno que deja de estar activo');
				}
				
			}
			// ALM
	                boolean wasFired  =  NETriggerHelper.getTriggerFired('updateOfertaDigital_fut');
			if(!wasFired){
	                	if(!toProcess.isEmpty()){
		                    // 06-03-2018 - Manuel Ochoa -  check if isBatch() for bulk orders
                            if(!System.isFuture() && !System.isBatch()){
                            // 06-03-2018 - Manuel Ochoa
			                    NETriggerHelper.setTriggerFired('updateOfertaDigital_fut');
			                    BI_AccountHelper.updateOfertaDigital_fut(toProcess);
		                    }else{
			                    BI_AccountHelper.updateOfertaDigital(toProcess);
		                    }
		                }
	                }
		}catch(Exception exc){
			BI_LogHelper.generate_BILog('BI_OrderMethods.actualizarAccountCoberturaDigital', 'BI_EN', exc, 'Trigger');
		}
	}

}