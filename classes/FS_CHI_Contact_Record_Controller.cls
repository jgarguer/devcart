/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:			Eduardo Ventura
Company:		Everis España
Description:	RFG-INT-CHI-01: Validación del identificador fiscal.

History:

<Date>                      <Author>                        <Change Description>
07/10/2016                  Eduardo Ventura					Initial Version
14/10/2016                  Eduardo Ventura					Status Update
24/10/2017					Daniel Leal		   				Ever01:Inclusion de logs
20/11/2017					Julio Asenjo					Ever02:Cambio a LightningApp
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
global with sharing class FS_CHI_Contact_Record_Controller {
    /* SALESFORCE VARIABLE CONFIGURATION */
    /* Empty */
    
    @AuraEnabled
    public static String createContact(String authorizations, String references, boolean representative, String documentType, String documentNumber, String serialNumber, String firstName, String lastName, String email, String streetName, String streetNumber, String city, String state, String accountId, String currencyCode, boolean manual, String phone, String mobile) {
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: createContact   Comienzo del método');
        /* Variable Configuration */
        LightningResponse response = new LightningResponse();
        String status;
        /* Retrieve Account */
        list<Account> accounts = [SELECT Id, BI_Country__c FROM Account WHERE Id = :accountId LIMIT 1];
        Account account;
        if (!accounts.isEmpty())  account = accounts[0];
        else account = null;
        if(manual != null && !manual) status = 'Validado';
        else status = 'No validado';
        Contact contact = new Contact();
        try{                       
            if(authorizations != 'Ninguno') contact.BI_Tipo_de_contacto__c = authorizations;
            if(references != 'Ninguno') contact.FS_CORE_Referencias_Funcionales__c = references;
            if(representative != null) contact.BI_Representante_legal__c = representative;             
            if(documentNumber != null && documentType != 'undefined'){
                contact.BI_Tipo_de_documento__c = documentType;
            }
            if(documentNumber != null) contact.BI_Numero_de_documento__c = documentNumber;
            if(serialNumber != 'undefined') contact.FS_CHI_Numero_Serie__c = serialNumber;
            if(firstName != 'undefined') contact.FirstName = firstName;
            if(lastName != 'undefined') contact.LastName = lastName;
            if(email != 'undefined') contact.Email = email;
            String numeroCalleAux = '';
            if(streetNumber != 'undefined' || streetNumber != null) {
                contact.FS_CORE_MailingNumber__c = streetNumber;
                numeroCalleAux=','+streetNumber;
            }            
            if(city != 'undefined' ) contact.MailingCity = city;
            if(state != 'undefined') contact.MailingState = state;                
            if(streetName != 'undefined' && streetName != null) contact.MailingStreet = streetName+numeroCalleAux;              
            if(currencyCode != 'undefined') contact.CurrencyIsoCode = currencyCode;
            if(phone != 'undefined') contact.Phone = phone;
            if(mobile != 'undefined') contact.MobilePhone = mobile;
            /* Extra */
            contact.AccountId = account.Id;
            contact.BI_Country__c = account.BI_Country__c;
            contact.FS_CHI_Estado_Validacion__c = status;            
            insert contact;
            response.id = contact.Id; 
        }catch(Exception exc){
            //Ever01-INI
            BI_LogHelper.generate_BILog_Generic('FS_CHI_Contact_Record_Controller.createContract', 'BI_EN', exc, 'Web Service',contact);
            response.error = exc.getMessage();
            //Ever01-FIN
        }
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: createContact   Finalización del método    Resultado: ' + JSON.serialize(response));
        return JSON.serialize(response);
    }
    
    webservice static void reintentar(String contactId) {
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: reintentar   Comienzo del método');
        //Ever01-INI
        Contact contact;
        try{
            //Ever01-FIN
            /* Variable Configuration */
            contact = [SELECT Id, FS_CHI_Estado_Validacion__c, BI_Tipo_de_documento__c, BI_Numero_de_documento__c, FS_CHI_Numero_Serie__c, FirstName, LastName FROM Contact WHERE Id = :contactId LIMIT 1][0];           
            if(contact.FS_CHI_Estado_Validacion__c != 'Validado'){
                BI_RestWrapper.ContactsListType returnValue = FS_CHI_Contact_Management.Sync(contact.BI_Tipo_de_documento__c, contact.BI_Numero_de_documento__c, contact.FS_CHI_Numero_Serie__c);
                if(returnValue != null && returnValue.totalResults == 1){
                    /* JSON */
                    if(returnValue.contacts[0].contactDetails != null){
                        if(returnValue.contacts[0].contactDetails.name != null){
                            if(returnValue.contacts[0].contactDetails.name.givenName != null) contact.FirstName = returnValue.contacts[0].contactDetails.name.givenName;
                            if(returnValue.contacts[0].contactDetails.name.familyName != null) contact.LastName = returnValue.contacts[0].contactDetails.name.familyName;
                        }                     
                        if(returnValue.contacts[0].contactDetails.addresses != null){
                            if(returnValue.contacts[0].contactDetails.addresses[0].addressName != null) contact.MailingStreet = returnValue.contacts[0].contactDetails.addresses[0].addressName;
                            if(returnValue.contacts[0].contactDetails.addresses[0].addressNumber != null) contact.FS_CORE_MailingNumber__c = returnValue.contacts[0].contactDetails.addresses[0].addressNumber.value;
                            if(returnValue.contacts[0].contactDetails.addresses[0].locality != null) contact.MailingCity = returnValue.contacts[0].contactDetails.addresses[0].locality;
                            if(returnValue.contacts[0].contactDetails.addresses[0].region != null) contact.MailingStreet = returnValue.contacts[0].contactDetails.addresses[0].region;
                        }                        
                        contact.FS_CHI_Estado_Validacion__c = 'Validado';
                    }
                } else contact.FS_CHI_Estado_Validacion__c = 'No validado';                
                update contact;
            }
            //Ever01-INI
        }catch(Exception exc){
            BI_LogHelper.generate_BILog_Generic('FS_CHI_Contact_Record_Controller.reintentar', 'BI_EN', exc, 'Web Service',contact);
        } 
        //Ever01-FIN
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: reintentar   Finalización del método');
    }
    
    public static LightningResponse reintentarL(String contactId) {
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: reintentarL   Comienzo del método');
        //Ever01-INI
        LightningResponse l= new LightningResponse();
        Contact contact;
        boolean process= true;
        try{
            //Ever01-FIN
            /* Variable Configuration */
            contact= [SELECT Id, BI_Country__c, FS_CHI_Estado_Validacion__c, BI_Tipo_de_documento__c, BI_Numero_de_documento__c, FS_CHI_Numero_Serie__c, FirstName, LastName, BI_Tipo_de_contacto__c, BI_Representante_legal__c FROM Contact WHERE Id = :contactId LIMIT 1][0];            
            if(String.isEmpty(contact.BI_Tipo_de_contacto__c) && !contact.BI_Representante_legal__c){ 
                l.id= '300';
                l.error= 'Para poder validar, el contacto debe estar autorizado.';
                process=false;
            }else if(String.isEmpty(contact.FS_CHI_Numero_Serie__c)){
                l.id= '300';
                l.error= 'Para poder validar el contacto debe informar el número de serie.';
                process=false;
            }else if(contact.BI_Country__c!='Chile'){
                l.id= '300';
                l.error= 'No es posible recuperar la información del registro para el país seleccionado.';
                process=false;
            }
            if(contact.FS_CHI_Estado_Validacion__c != 'Validado' && process){
                BI_RestWrapper.ContactsListType returnValue = FS_CHI_Contact_Management.Sync(contact.BI_Tipo_de_documento__c, contact.BI_Numero_de_documento__c, contact.FS_CHI_Numero_Serie__c);
                if(returnValue != null && returnValue.totalResults == 1){
                    /* JSON */
                    if(returnValue.contacts[0].contactDetails != null){
                        if(returnValue.contacts[0].contactDetails.name != null){
                            if(returnValue.contacts[0].contactDetails.name.givenName != null) 
                                contact.FirstName = returnValue.contacts[0].contactDetails.name.givenName;
                            if(returnValue.contacts[0].contactDetails.name.familyName != null) 
                                contact.LastName = returnValue.contacts[0].contactDetails.name.familyName;
                        }
                        if(returnValue.contacts[0].contactDetails.addresses != null){
                            if(returnValue.contacts[0].contactDetails.addresses[0].addressName != null) contact.MailingStreet = returnValue.contacts[0].contactDetails.addresses[0].addressName;
                            if(returnValue.contacts[0].contactDetails.addresses[0].addressNumber != null) contact.FS_CORE_MailingNumber__c = returnValue.contacts[0].contactDetails.addresses[0].addressNumber.value;
                            if(returnValue.contacts[0].contactDetails.addresses[0].locality != null) contact.MailingCity = returnValue.contacts[0].contactDetails.addresses[0].locality;
                            if(returnValue.contacts[0].contactDetails.addresses[0].region != null) contact.MailingStreet = returnValue.contacts[0].contactDetails.addresses[0].region;
                        }
                        l.id= '200';
                        l.error= 'Contacto validado correctamente';
                        contact.FS_CHI_Estado_Validacion__c = 'Validado';
                    }
                } 
                else if (returnValue.totalResults == 9) {
                    l.id= '500';
                    l.error= 'Error al validar el contacto';
                    contact.FS_CHI_Estado_Validacion__c = 'No validado';} 
                else{                          
                    l.id= '300';
                    l.error= 'No es posible validar el contacto';
                    contact.FS_CHI_Estado_Validacion__c = 'No validado'; } 
                update contact;
            }else{
                if (contact.FS_CHI_Estado_Validacion__c == 'Validado'){
                    l.id= '200';
                    l.error= 'El contacto ya está validado.';  
                }
            }
            //Ever01-INI
        }catch(Exception exc){
            BI_LogHelper.generate_BILog_Generic('FS_CHI_Contact_Record_Controller.reintentar', 'BI_EN', exc, 'Web Service',contact); 
            l.id= '500';
            String aux = exc.getMessage().split('description":"')[1];
            l.error= 'Error al validar el contacto. '+aux.substring(0, aux.indexOf('"'));
            contact.FS_CHI_Estado_Validacion__c = 'No validado';
            update contact;         
        }   
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: reintentarL   Finalización del método    Resultado: ' + l);
        return l;
        //Ever01-FIN
    }
    
    @AuraEnabled
    public static String validateContact(String documentType, String documentNumber, String SerialNumber) {
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: validateContact   Comienzo del método');
        /* Variable Configuration */
        /* Empty */
        String response='';
        BI_RestWrapper.ContactsListType contactList= new BI_RestWrapper.ContactsListType();
        try{
            contactList =  FS_CHI_Contact_Management.Sync(documentType, documentNumber, SerialNumber);
            if (contactList != null) response = JSON.serialize(contactList);  
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('FS_CHI_Contact_Management.validateContact', 'BI_EN', exc, 'Web Service'); 
            contactList.totalResults = 666;
            String aux = exc.getMessage().split('description":"')[1];
            contactList.errorDescription= 'Error al validar el contacto. '+aux.substring(0, aux.indexOf('"'));
            return JSON.serialize(contactList);
            
        }
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: validateContact   Finalización del método    Resultado: ' + response);
        return response;
    }
    
    public static boolean validateId(String Idparam) {
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: validateId   Comienzo del método');
        String id = String.escapeSingleQuotes(Idparam);
        if((id.length() == 15 || id.length() == 18) && Pattern.matches('^[a-zA-Z0-9]*$', id)) {
            System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: validateId   Finalización del método    Resultado: true');
            return true;
        }
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: validateId   Finalización del método    Resultado: false');
        return false;
    }
    
    @AuraEnabled
    public static Account[] retrieveInfo(String searchKey) {
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: retrieveInfo   Comienzo del método');
        /* Declaración de variables */
        /* Vacío */
        /* Consulta */
        String query = 'SELECT Id, Name, BI_Country__c, BI_No_Identificador_fiscal__c FROM Account WHERE';
        if(String.isEmpty(searchKey))
            query += ' OwnerId = \'' + UserInfo.getUserId() +'\' LIMIT 10';
        else if(validateId(searchKey))
        {
            query += ' id=\''+searchKey+'\'';
        }else{
            query += ' Name LIKE \'%'+searchKey+'%\' ';
        }
        /* Ejecución */
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: retrieveInfo   Finalización del método    Resultado: ' + Database.query(query));
        return Database.query(query);
    }
    
    //Cambiado a publico
    public class LightningResponse {
        @AuraEnabled
        public string id{get;set;}
        @AuraEnabled
        public string error{get;set;}
        @AuraEnabled
        public string title{get;set;}
    }
    public class FS_CHI_IntegrationException extends Exception {}
    
    /*--Ever02:Cambio a LightningApp */
    @AuraEnabled
    public static LightningResponse doInitController(String contactId){
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: doInitController   Comienzo del método');
        LightningResponse l=FS_CHI_Contact_Record_Controller.reintentarL(contactId);
        System.debug('Nombre de clase: FS_CHI_Contact_Record_Controller   Nombre de método: doInitController   Finalización del método    Resultado: ' + l);
        return l;
    }
    
    @AuraEnabled
    public static List <String> getSelectOptions(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
 	}
    
}