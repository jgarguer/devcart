@isTest
private class TGS_ModTicketINC_TEST {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Marta Laliena y Marta García
Company:       Deloitte
Description:   Test method to manage the code coverage for TGS_ModTicketINC.createINC

<Date>                 <Author>                <Change Description>
21/05/2015              Marta Laliena           Initial Version
06/08/2016  Humberto Nunes        Se coloco el NETriggerHelper.setTriggerFired('BI_Case'); para que no se ejecutara ya que da too many querys... 
07/08/2016	Jorge Galindo		  Added not to execute BI_FVIBorrar,BI_FVI_Autosplit and BI_OpportunityMethods.createTask, chnage position of start and stop test
--------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    
    static testMethod void addINCTestCreate() {

        NETriggerHelper.setTriggerFired('BI_Case');
        //07/08/2016	Jorge Galindo
        NETriggerHelper.setTriggerFired('BI_FVIBorrar');
        NETriggerHelper.setTriggerFired('BI_FVI_Autosplit');
        NETriggerHelper.setTriggerFired('BI_OpportunityMethods.createTask');
        // end 07/08/2016	Jorge Galindo
        
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        userTest.TEMPEXT_ID__c = 'chgUser';
        insert userTest;
        
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        System.runAs(userTest){
            Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
            Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
            Account acCC = [Select ParentId FROM Account WHERE Id=:acLe.ParentId];
            Account nameHo = [Select Name FROM Account WHERE Id=:acCC.ParentId];
            
            Contact myContact = TGS_Dummy_Test_Data.dummyContactTGS('Test contact');
            insert myContact;
            
            Id change = [SELECT Id FROM RecordType WHERE Name = 'Change' LIMIT 1].Id;
            BI_Punto_de_instalacion__c s = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id); 
            BI_Punto_de_instalacion__c Site = [SELECT Name FROM BI_Punto_de_instalacion__c WHERE Name = 'Test site' ];
            Case caseTest = new Case(TGS_Impact__c = '1-Extensive/Widespread',
                                     TGS_Urgency__c = '1-Critical',
                                     Subject = 'Test ticket change',
                                     Description = 'Description test ticket change',
                                     RecordTypeId = change,
                                     Status = 'Assigned',
                                     Priority = 'To be Calculated',
                                     AccountId = acBU.Id,
                                     ContactId = myContact.Id
                                     
                                    );      
            //Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
            insert caseTest;
            //07/08/2016	Jorge Galindo Test.stopTest();
            TGS_ModTicketINC.mWanInfo mWanInfo = new TGS_ModTicketINC.mWanInfo(); 
            String numberC = [SELECT CaseNumber FROM Case WHERE Id=:caseTest.Id LIMIT 1].CaseNumber;
            
            Test.startTest();    
            TGS_ModTicketINC.incident incidencia = new TGS_ModTicketINC.incident();
			incidencia = createIncident(incidencia);
            incidencia.CaseNumber = numberC;
            incidencia.LogInContact = userTest.TEMPEXT_ID__c;
            incidencia.Company = nameHo.Name;
            incidencia.Direct_Contact_Site = site.Name;
            incidencia.Direct_Contact_Department = acBU.Name;
            incidencia.Operation = TGS_ModTicket.ROD_SF_CREATE_INCIDENT;
            
            try{
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Operation = 'test';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.CaseNumber = '';
                incidencia.Operation = 'ROD_SF_CREATE_INCIDENT';
                incidencia.Service_Type = TGS_ModTicket.QUERY_TICKET;
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Status = 'In Progress';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Service_Type = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Service_Type = 'fail';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Service_Type = 'User Service Restoration';
                incidencia.Owner_Group = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Owner_Group = 'Nivel 1 | CORE';
                incidencia.Direct_Contact_Site = 'fail aaaa';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Direct_Contact_Site = site.Name;
                incidencia.Impact = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Impact = 'fail';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Impact = '1-Extensive/Widespread';
                incidencia.Urgency = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Urgency = 'fail';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Urgency = '1-Critical';
                incidencia.Subject = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Subject = 'Test ticket incident';
                incidencia.Description = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Description = 'Test creation incident ticket - remedy integration';
                incidencia.Reported_Source = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Reported_Source = 'fail';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Reported_Source = 'Web';
                incidencia.Status = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }  

            
            Id recordTypeProblem = [SELECT Id FROM RecordType WHERE Name='Incident'].Id;
            caseTest.RecordTypeId = recordTypeProblem;
            //TGS_ModTicket.validateProductCategorizations(caseTest,'PruebaCategorizacion1','PruebaCategorizacion2','PruebaCategorizacion3');
            
            recordTypeProblem = [SELECT Id FROM RecordType WHERE Name='Complaint'].Id;
            caseTest.RecordTypeId = recordTypeProblem;
            TGS_ModTicket.validateProductCategorizations(caseTest,'Enterprise Managed Mobility','Universal WIFI','Universal WIFI');
            TGS_ModTicket.validateProductCategorizations(caseTest,'Enterprise Managed Mobility','Universal WIFI','');
            TGS_ModTicket.validateProductCategorizations(caseTest,'Enterprise Managed Mobility','','');
            TGS_ModTicket.validateProductCategorizations(caseTest,'','','');
            try{
                TGS_ModTicket.validateProductCategorizations(caseTest,'Enterprise Managed Mobility','Universal WIFI','Fail3');
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            try{
                TGS_ModTicket.validateProductCategorizations(caseTest,'Enterprise Managed Mobility','Fail2','');
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            try{
                TGS_ModTicket.validateProductCategorizations(caseTest,'Fail1','','');
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            try{
                TGS_ModTicket.validateContact(caseTest,'test');
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            try{
                TGS_ModTicket.validateCI(caseTest,'CI');
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            
            TGS_ModTicket.setResolution('description test',caseTest.Id);
            
            TGS_ModTicketINC.mWanInfo newMWan = new TGS_ModTicketINC.mWanInfo();
            
            newMWan.Customer_Contacted = 'test';
            try{
                TGS_ModTicket.updateInfoMwan(caseTest, newMWan);
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            newMWan.Customer_Contacted = '';
            newMWan.Electrical_Power = 'test';
            try{
                TGS_ModTicket.updateInfoMwan(caseTest, newMWan);
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            newMWan.Electrical_Power = '';
            newMWan.CPE_Status = 'test';
            try{
                TGS_ModTicket.updateInfoMwan(caseTest, newMWan);
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            newMWan.CPE_Status = '';
            newMWan.CPE_Reset = 'test';
            try{
                TGS_ModTicket.updateInfoMwan(caseTest, newMWan);
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            newMWan.CPE_Reset = '';
            newMWan.Cabling_Checked = 'test';
            try{
                TGS_ModTicket.updateInfoMwan(caseTest, newMWan);
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            newMWan.Cabling_Checked = '';
            newMWan.Working_Before = 'test';
            try{
                TGS_ModTicket.updateInfoMwan(caseTest, newMWan);
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            newMWan.Working_Before = '';
            newMWan.Change_Pending = 'test';
            try{
                TGS_ModTicket.updateInfoMwan(caseTest, newMWan);
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            newMWan.Change_Pending = '';
            newMWan.Allowed_Intrusive_Test = 'test';
            try{
                TGS_ModTicket.updateInfoMwan(caseTest, newMWan);
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            newMWan.Allowed_Intrusive_Test = '';
            newMWan.Repetitive_Issue = 'test';
            try{
                TGS_ModTicket.updateInfoMwan(caseTest, newMWan);
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }
            
            Test.stopTest();     
        }
        
    } 
    
    static testMethod void addINCTestModify() {
        
        NETriggerHelper.setTriggerFired('BI_Case');
        //07/08/2016	Jorge Galindo
        NETriggerHelper.setTriggerFired('BI_FVIBorrar');
        NETriggerHelper.setTriggerFired('BI_FVI_Autosplit');
        NETriggerHelper.setTriggerFired('BI_OpportunityMethods.createTask');
        // end 07/08/2016	Jorge Galindo
        
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        userTest.TEMPEXT_ID__c = 'chgUser';
        insert userTest;
        
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        System.runAs(userTest){
            Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
            Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
            Account acCC = [Select ParentId FROM Account WHERE Id=:acLe.ParentId];
            Account nameHo = [Select Name FROM Account WHERE Id=:acCC.ParentId];

            Contact myContact = TGS_Dummy_Test_Data.dummyContactTGS('Test contact');
            insert myContact;
            
            Id change = [SELECT Id FROM RecordType WHERE Name = 'Change' LIMIT 1].Id;
            BI_Punto_de_instalacion__c s = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id); 
            BI_Punto_de_instalacion__c Site = [SELECT Name FROM BI_Punto_de_instalacion__c WHERE Name = 'Test site' ];
     
            Case caseTest = new Case(TGS_Impact__c = '1-Extensive/Widespread',
                                     TGS_Urgency__c = '1-Critical',
                                     Subject = 'Test ticket change',
                                     Description = 'Description test ticket change',
                                     RecordTypeId = change,
                                     Status = 'Assigned',
                                     Priority = 'To be Calculated',
                                     AccountId = acBU.Id,
                                     ContactId = myContact.Id
                                     
                                    );      
            insert caseTest;
            TGS_ModTicketINC.mWanInfo mWanInfo = new TGS_ModTicketINC.mWanInfo(); 
            String numberC = [SELECT CaseNumber FROM Case WHERE Id=:caseTest.Id LIMIT 1].CaseNumber;
            
            Test.startTest();    
            TGS_ModTicketINC.incident incidencia = new TGS_ModTicketINC.incident();
			incidencia = createIncident(incidencia);
            incidencia.CaseNumber = numberC;
            incidencia.LogInContact = userTest.TEMPEXT_ID__c;
            incidencia.Company = nameHo.Name;
            incidencia.Direct_Contact_Site = site.Name;
            incidencia.Direct_Contact_Department = acBU.Name;
            incidencia.Operation = TGS_ModTicket.ROD_SF_MODIFY_INCIDENT;
            
            try{            
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Operation = 'test';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.CaseNumber = '';
                incidencia.Operation = 'ROD_SF_CREATE_INCIDENT';
                incidencia.Service_Type = TGS_ModTicket.QUERY_TICKET;
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Status = 'In Progress';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Service_Type = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Service_Type = 'fail';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Service_Type = 'User Service Restoration';
                incidencia.Owner_Group = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Owner_Group = 'Nivel 1 | CORE';
                incidencia.Direct_Contact_Site = 'fail aaaa';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Direct_Contact_Site = site.Name;
                incidencia.Impact = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Impact = 'fail';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Impact = '1-Extensive/Widespread';
                incidencia.Urgency = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Urgency = 'fail';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Urgency = '1-Critical';
                incidencia.Subject = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Subject = 'Test ticket incident';
                incidencia.Description = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Description = 'Test creation incident ticket - remedy integration';
                incidencia.Reported_Source = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Reported_Source = 'fail';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.Reported_Source = 'Web';
                incidencia.Status = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);                
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            }  
            
            Test.stopTest();     
        }
        
    }
    
    static testMethod void addINCTestCreate2() {
        
        NETriggerHelper.setTriggerFired('BI_Case');
        //07/08/2016	Jorge Galindo
        NETriggerHelper.setTriggerFired('BI_FVIBorrar');
        NETriggerHelper.setTriggerFired('BI_FVI_Autosplit');
        NETriggerHelper.setTriggerFired('BI_OpportunityMethods.createTask');
        // end 07/08/2016	Jorge Galindo
        
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        userTest.TEMPEXT_ID__c = 'chgUser';
        insert userTest;
        
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        System.runAs(userTest){
            Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
            Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
            Account acCC = [Select ParentId FROM Account WHERE Id=:acLe.ParentId];
            Account nameHo = [Select Name FROM Account WHERE Id=:acCC.ParentId];
            
            Contact myContact = TGS_Dummy_Test_Data.dummyContactTGS('Test contact');
            insert myContact;
            
            Id change = [SELECT Id FROM RecordType WHERE Name = 'Change' LIMIT 1].Id;
            BI_Punto_de_instalacion__c s = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id); 
            BI_Punto_de_instalacion__c Site = [SELECT Name FROM BI_Punto_de_instalacion__c WHERE Name = 'Test site' ];

            Case caseTest = new Case(TGS_Impact__c = '1-Extensive/Widespread',
                                     TGS_Urgency__c = '1-Critical',
                                     Subject = 'Test ticket change',
                                     Description = 'Description test ticket change',
                                     RecordTypeId = change,
                                     Status = 'Assigned',
                                     Priority = 'To be Calculated',
                                     AccountId = acBU.Id,
                                     ContactId = myContact.Id
                                     
                                    );      
            //Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
            insert caseTest;
            //07/08/2016	Jorge Galindo Test.stopTest();
            TGS_ModTicketINC.mWanInfo mWanInfo = new TGS_ModTicketINC.mWanInfo(); 
            String numberC = [SELECT CaseNumber FROM Case WHERE Id=:caseTest.Id LIMIT 1].CaseNumber;
            
            Test.startTest();    
            TGS_ModTicketINC.incident incidencia = new TGS_ModTicketINC.incident();
			incidencia = createIncident(incidencia);
            incidencia.CaseNumber = numberC;
            incidencia.LogInContact = userTest.TEMPEXT_ID__c;
            incidencia.Company = nameHo.Name;
            incidencia.Direct_Contact_Site = site.Name;
            incidencia.Direct_Contact_Department = acBU.Name;
            incidencia.Operation = TGS_ModTicket.ROD_SF_CREATE_INCIDENT;
            
            try{
                incidencia.TGS_Fecha_Aparicion = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Fecha_Aparicion = '2017-09-28 11:51:';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Fecha_Aparicion = '2017-09-28 11:51:22';
                incidencia.TGS_Fecha_Arranque_Reloj = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Fecha_Arranque_Reloj = '2017-09-28 11:51:';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Fecha_Arranque_Reloj = '2017-09-28 11:51:55';
                incidencia.TGS_Fecha_Parada_Reloj = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Fecha_Parada_Reloj = '2017-09-28 11:51:';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Fecha_Parada_Reloj = '2017-09-28 11:51:27';
                incidencia.TGS_Resolution_Category_Tier_1 = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Resolution_Category_Tier_1 = 'Resolution 1';
                incidencia.TGS_Resolution_Category_Tier_2 = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Resolution_Category_Tier_2 = 'Resolution 2';
                incidencia.TGS_Resolution_Category_Tier_3 = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Resolution_Category_Tier_3 = 'Resolution 3';
                incidencia.TGS_Tiempo_Afectacion = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Tiempo_Afectacion = 'aaa';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            } 
            
            Test.stopTest();     
            
        }   
        
    }
    
    static testMethod void addINCTestModify2() {
        
        NETriggerHelper.setTriggerFired('BI_Case');
        //07/08/2016	Jorge Galindo
        NETriggerHelper.setTriggerFired('BI_FVIBorrar');
        NETriggerHelper.setTriggerFired('BI_FVI_Autosplit');
        NETriggerHelper.setTriggerFired('BI_OpportunityMethods.createTask');
        // end 07/08/2016	Jorge Galindo
        
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        userTest.TEMPEXT_ID__c = 'chgUser';
        insert userTest;
        
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        System.runAs(userTest){
            Account acLe = TGS_Dummy_Test_Data.dummyHierarchy();    
            Account acBU = [Select Id, Name FROM Account WHERE ParentId=:acLe.Id];
            Account acCC = [Select ParentId FROM Account WHERE Id=:acLe.ParentId];
            Account nameHo = [Select Name FROM Account WHERE Id=:acCC.ParentId];

            Contact myContact = TGS_Dummy_Test_Data.dummyContactTGS('Test contact');
            insert myContact;
            
            Id change = [SELECT Id FROM RecordType WHERE Name = 'Change' LIMIT 1].Id;
            BI_Punto_de_instalacion__c s = TGS_Dummy_Test_Data.dummyPuntoInstalacion(acBU.Id); 
            BI_Punto_de_instalacion__c Site = [SELECT Name FROM BI_Punto_de_instalacion__c WHERE Name = 'Test site' ];

            Case caseTest = new Case(TGS_Impact__c = '1-Extensive/Widespread',
                                     TGS_Urgency__c = '1-Critical',
                                     Subject = 'Test ticket change',
                                     Description = 'Description test ticket change',
                                     RecordTypeId = change,
                                     Status = 'Assigned',
                                     Priority = 'To be Calculated',
                                     AccountId = acBU.Id,
                                     ContactId = myContact.Id
                                     
                                    );      
            //Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
            insert caseTest;
            //07/08/2016	Jorge Galindo Test.stopTest();
            TGS_ModTicketINC.mWanInfo mWanInfo = new TGS_ModTicketINC.mWanInfo(); 
            String numberC = [SELECT CaseNumber FROM Case WHERE Id=:caseTest.Id LIMIT 1].CaseNumber;
            
            Test.startTest();    
            TGS_ModTicketINC.incident incidencia = new TGS_ModTicketINC.incident();
			incidencia = createIncident(incidencia);
            incidencia.CaseNumber = numberC;
            incidencia.LogInContact = userTest.TEMPEXT_ID__c;
            incidencia.Company = nameHo.Name;
            incidencia.Direct_Contact_Site = site.Name;
            incidencia.Direct_Contact_Department = acBU.Name;
            incidencia.Operation = TGS_ModTicket.ROD_SF_MODIFY_INCIDENT;
            try{
                incidencia.TGS_Fecha_Aparicion = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Fecha_Aparicion = '2017-09-28 11:51:';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Fecha_Aparicion = '2017-09-28 11:51:22';
                incidencia.TGS_Fecha_Arranque_Reloj = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Fecha_Arranque_Reloj = '2017-09-28 11:51:';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Fecha_Arranque_Reloj = '2017-09-28 11:51:55';
                incidencia.TGS_Fecha_Parada_Reloj = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Fecha_Parada_Reloj = '2017-09-28 11:51:';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Fecha_Parada_Reloj = '2017-09-28 11:51:27';
                incidencia.TGS_Resolution_Category_Tier_1 = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Resolution_Category_Tier_1 = 'Resolution 1';
                incidencia.TGS_Resolution_Category_Tier_2 = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Resolution_Category_Tier_2 = 'Resolution 2';
                incidencia.TGS_Resolution_Category_Tier_3 = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Resolution_Category_Tier_3 = 'Resolution 3';
                incidencia.TGS_Tiempo_Afectacion = '';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
                incidencia.TGS_Tiempo_Afectacion = 'aaa';
                TGS_ModTicketINC.upsertINC(incidencia, mWanInfo);
            }catch(Exception e){
                system.debug('Exception: '+e+' at line: '+e.getLineNumber());
            } 
            Test.stopTest();     
        }   
    }
    
    static TGS_ModTicketINC.incident createIncident(TGS_ModTicketINC.incident inc){
            inc.Ticket_ID = '15489654';
            inc.Service_Type = 'User Service Restoration';
            inc.Impact = '1-Extensive/Widespread';
            inc.Urgency = '1-Critical';
            inc.Subject = 'Test ticket incident';
            inc.Description = 'Test creation incident ticket - remedy integration';
            inc.Reported_Source = 'Web';
            inc.AssigneeName = '';
            inc.CI_ID = '';
            inc.Categorization_Tier_1 = '';
            inc.Categorization_Tier_2 = '';
            inc.Categorization_Tier_3 = '';
            inc.Product_Categorization_Tier_1 = '';
            inc.Product_Categorization_Tier_2 = '';
            inc.Product_Categorization_Tier_3 = '';
            inc.Direct_Contact_Organization = '';
            inc.Owner_Group = 'SMC EMEA L2 Technical';
            inc.Status = 'Assigned';
            inc.Status_Reason = '';
            inc.Priority = 'To be Calculated';
            inc.Resolution = '';
            inc.TGS_Fecha_Aparicion = '2017-09-28 11:51:22';
            inc.TGS_Fecha_Arranque_Reloj = '2017-09-28 11:51:55';
            inc.TGS_Fecha_Parada_Reloj = '2017-09-28 11:51:27';
            inc.TGS_Resolution_Category_Tier_1 = 'Resolution 1';
            inc.TGS_Resolution_Category_Tier_2 = 'Resolution 2';
            inc.TGS_Resolution_Category_Tier_3 = 'Resolution 3';
            inc.TGS_Tiempo_Afectacion = '7';
        return inc;
    }
}