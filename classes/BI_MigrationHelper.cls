public with sharing class BI_MigrationHelper {
		
	static Map<Integer,BI_bypass__c> instances;
	static final String INSERT_DML = 'INSERT';
	static final String UPDATE_DML = 'UPDATE';

	//Variable that disable the triggers of the objets that was inserted into the set
	public static Set <String> set_skippedTriggers = new Set <String>();


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that set the BI_bypass custom setting with the specified values for the specified user and return the last instance for the user of the BI_bypass
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    10/12/2015                      Guillermo Muñoz             Initial version
	    02/03/2016						Guillermo Muñoz				Changed to avoid soql querys
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static Map<Integer,BI_bypass__c> enableBypass(Id userId,boolean migration, boolean skipTrigger, boolean stopJob, boolean skipAssert){

		try{
			if(BI_TestUtils.isRunningTest()){
				Throw new BI_Exception('Test');
			}
			BI_bypass__c bypass = BI_bypass__c.getInstance(userId);
			
			if(bypass.Id != null){
				instances = new Map<Integer,BI_bypass__c>{0 => bypass};
				BI_bypass__c cs = new BI_bypass__c(
					Id = bypass.Id,
					BI_migration__c = migration,
					BI_skip_assert__c = skipAssert,
					BI_stop_job__c = stopJob,
					BI_skip_trigger__c = skipTrigger
				);				
				update cs;

			}
			else{
				BI_bypass__c cs = new BI_bypass__c(
					SetupOwnerId = userId,
					BI_migration__c = migration,
					BI_skip_assert__c = skipAssert,
					BI_stop_job__c = stopJob,
					BI_skip_trigger__c = skipTrigger
				);
				insert cs;
				instances = new Map<Integer,BI_bypass__c>{1 => cs};
			}
 
		}catch(Exception exc){
			BI_LogHelper.generate_BILog('BI_MigrationHelper.disableValidations', 'BI_EN', exc, 'Apex Class');
		}

		return instances;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that receives the last instance of the BI_bypass custom setting returned by enableBypass method and set the BI_bypass with the last
	     			   instance of the bypass when the required operation finish.
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    10/12/2015                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void disableBypass(Map <Integer, BI_bypass__c> bypass){
		
		try{
			if(BI_TestUtils.isRunningTest()){
				Throw new BI_Exception('Test');
			}

			if(bypass.containsKey(1)){
				delete bypass.get(1);
			}
			else{
				update bypass.get(0);
			}

		}catch(Exception exc){

			BI_LogHelper.generate_BILog('BI_MigrationHelper.enableValidations', 'BI_EN', exc, 'Apex Class');
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that realize an insert or update operation with the bypass custom setting enabled and return de result of the operation.
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    01/03/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static List<Database.SaveResult> bypassInsertUpdateDML(Id userId,boolean migration, boolean skipTrigger, boolean stopJob, boolean skipAssert, List <sObject> records, String dml, Boolean allOrNone){

		List <Database.SaveResult> lst_toReturn;
		BI_bypass__c bypass = BI_bypass__c.getInstance(userId);
		System.debug('*********' + bypass);
		BI_bypass__c cs;

		try{

			if(BI_TestUtils.isRunningTest()){
				Throw new BI_Exception('Test');
			}

			if(bypass.Id != null){
					
				cs = new BI_bypass__c(
					Id = bypass.Id,
					BI_migration__c = migration,
					BI_skip_assert__c = skipAssert,
					BI_stop_job__c = stopJob,
					BI_skip_trigger__c = skipTrigger
				);				
				update cs;
			}
			else{
				cs = new BI_bypass__c(
					SetupOwnerId = userId,
					BI_migration__c = migration,
					BI_skip_assert__c = skipAssert,
					BI_stop_job__c = stopJob,
					BI_skip_trigger__c = skipTrigger
				);
				insert cs;
			}			

			if(dml.equalsIgnoreCase(INSERT_DML)){
				lst_toReturn = database.insert(records, allOrNone);
			}
			else if(dml.equalsIgnoreCase(UPDATE_DML)){
				lst_toReturn = database.update(records, allOrNone);
			}

		}catch(Exception exc){
			BI_LogHelper.generate_BILog('BI_MigrationHelper.bypassInsertUpdateDML', 'BI_EN', exc, 'Apex Class');
		}
		finally{

			if(bypass.Id != null){
				update bypass;
			}
			else if(cs != null){
				delete cs;
			}
		}

		return lst_toReturn;
	}
	///////**********************************************************************************************************************************************///////
	//////////////////////////////////Descomentar si se necesita esta funcionalidad en el futuro y realizar un método en la clase BI_MigrationHelper_TEST///////
	///////**********************************************************************************************************************************************///////
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that realize an delete or update operation with the bypass custom setting enabled and return de result of the operation.
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    01/03/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	/*public static List<Database.DeleteResult> bypassDeleteDML(Id userId,boolean migration, boolean skipTrigger, boolean stopJob, boolean skipAssert, List <sObject> records, Boolean allOrNone){

		List <Database.DeleteResult> lst_toReturn;
		BI_bypass__c bypass = BI_bypass__c.getInstance(userId);
		BI_bypass__c cs;

		try{

			if(BI_TestUtils.isRunningTest()){
				Throw new BI_Exception('Test');
			}

			if(bypass.Id != null){
				cs = new BI_bypass__c(
					SetupOwnerId = userId,
					BI_migration__c = migration,
					BI_skip_assert__c = skipAssert,
					BI_stop_job__c = stopJob,
					BI_skip_trigger__c = skipTrigger
				);
				insert cs;
			}
			else{
				cs = new BI_bypass__c(
					Id = bypass.Id,
					BI_migration__c = migration,
					BI_skip_assert__c = skipAssert,
					BI_stop_job__c = stopJob,
					BI_skip_trigger__c = skipTrigger
				);				
				update cs;
			}

			lst_toReturn = database.delete(records, allOrNone);			

		}catch(Exception exc){
			BI_LogHelper.generate_BILog('BI_MigrationHelper.bypassDeleteDML', 'BI_EN', exc, 'Apex Class');
		}
		finally{

			if(bypass.Id != null){
				update bypass;
			}
			else if(cs != null){
				delete cs;
			}
		}

		return lst_toReturn;
	}*/

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       NEAborda
	    Description:   Method that realize a Case update operation with the bypass custom setting enabled.
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    15/12/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void updateCaseWithoutAssignationsAndTrigger(List <Case> casesToUpdate){

		BI_bypass__c bypass = BI_bypass__c.getInstance(UserInfo.getUserId());
		System.debug('*********' + bypass);
		BI_bypass__c cs;

		try{

			if(BI_TestUtils.isRunningTest()){
				Throw new BI_Exception('Test');
			}

			if(casesToUpdate != null && !casesToUpdate.isEmpty()){
				
				if(bypass.Id != null){
						
					cs = new BI_bypass__c(
						Id = bypass.Id,
						BI_migration__c = true,
						BI_skip_trigger__c = true,
						BI_Skip_case_assignations__c = true
					);				
					update cs;
				}
				else{
					cs = new BI_bypass__c(
						SetupOwnerId = UserInfo.getUserId(),
						BI_migration__c = true,
						BI_skip_trigger__c = true,
						BI_Skip_case_assignations__c = true
					);
					insert cs;
				}
				update casesToUpdate;
			}
		}catch(Exception exc){
				BI_LogHelper.generate_BILog('BI_MigrationHelper.updateCaseWithoutAssignationsAndTrigger', 'BI_EN', exc, 'Apex Class');
		}
		finally{

			if(bypass.Id != null){
				update bypass;
			}
			else if(cs != null){
				delete cs;
			}
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Accenture
	    Description:   Method that validate if the triggers of an SObject are disabled.
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    19/07/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static Boolean isTriggerDisabled(String objectName){

		Boolean isValidObject = Schema.getGlobalDescribe().get(objectName) != null;

		if(isValidObject){
			return set_skippedTriggers.contains(objectName) || (set_skippedTriggers.size() == 1 && set_skippedTriggers.contains('all'));
		}

		return false;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Accenture
	    Description:   Method that disable the triggers of an SObject.
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    19/07/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static Boolean setSkippedTrigger(String objectName){
		
		Boolean isValidObject = Schema.getGlobalDescribe().get(objectName) != null;

		if(isValidObject){

			if(set_skippedTriggers.contains('all')){

				set_skippedTriggers.remove('all');
			}

			set_skippedTriggers.add(objectName);
		}

		return isValidObject;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Accenture
	    Description:   Method that disable the triggers of all SObjects.
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    19/07/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void skipAllTriggers(){

		 if(!set_skippedTriggers.isEmpty()){
		 	set_skippedTriggers = new Set <String>();
		 }

		 set_skippedTriggers.add('all');
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Accenture
	    Description:   Method that enable the triggers of all SObjects.
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    19/07/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void cleanSkippedTriggers(){

		set_skippedTriggers = new Set <String>();
	}

    /*------------------------------------
    Author:        Julián Gawron
    Company:       Accenture - New Energy Aborda
    Description:   Class to avoid with sharing class definition in DML operations
    
    History:
    
    <Date>            <Author>              <Description>
    19/01/2018        Julián Gawron         Initial version
    ----------------------------------------------------------*/
    public without sharing class DMLHelper{

        public DMLHelper (SObject obj, String operation){

            if(operation == 'INSERT'){
                insert obj;
            }
            if(operation == 'UPDATE'){
                update obj;
            }
            if(operation == 'DELETE'){
                delete obj;
            }
            if(operation == 'UNDELETE'){
                undelete obj;
            }
        }

        public DMLHelper (List<SObject> obj, String operation){

            if(operation == 'INSERT'){
                insert obj;
            }
            if(operation == 'UPDATE'){
                update obj;
            }
            if(operation == 'DELETE'){
                delete obj;
            }
            if(operation == 'UNDELETE'){
                undelete obj;
            }
        }


    }


}