@isTest
private class BI_Visualiza_Clientes_ctrl_TEST
{


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   
    IN:          
    OUT:               
    History:
    <Date>            <Author>          <Description>
    26/09/2017      Gawron, Julian  Initial version.
    26/10/2017        Gawron, Julian    Adding runAs
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

  @isTest static void getAccountList_TEST (){
        List<String> lst_pais = new List<String>();
    lst_pais.add('Argentina');
    String nombre = BI_Dataload.generateRandomString(4);
        Id idProfile =BI_DataLoad.searchAdminProfile();
        User usr = new User (alias = nombre +'s1',
                                    Email = 's1u@testorg.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName= nombre + 'Testing1',
                                    ProfileId = idProfile,
                                    LanguageLocaleKey='es',
                                    Localesidkey='es_ES',
                                    Pais__c = 'Argentina',
                                    BI_Permisos__c = Label.BI_Administrador,
                                    Timezonesidkey=Label.BI_TimeZoneLA,
                                    UserName= nombre + '1' + idProfile + String.valueOf(Math.random()) +'ur23@testorg.com',
                                    IsActive = true);

        System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
            insert usr;
        }
 
    List<SObject> lst_null;
    System.runAs(usr){ //prevent mixed opp JEG
         List<Account> accountobj = BI_DataLoad.loadAccounts(2, lst_pais);
     lst_null = BI_Visualiza_Clientes_ctrl.getAccountList(null);   
        }
    System.assert(lst_null.size() > 1);

     }
}