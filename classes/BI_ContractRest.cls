@RestResource(urlMapping='/accountresources/v1/contracts/*')
global class BI_ContractRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Contract Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    22/07/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains basic information stored in the server for a specific contract.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.ContractInfoType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    22/07/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.ContractInfoType getContract() {
		
		BI_RestWrapper.ContractInfoType res;
		
		try{
			
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
				
			//ONE CONTRACT
			res = BI_RestHelper.getOneContract(String.valueOf(RestContext.request.requestURI).split('/')[4]);
			
			RestContext.response.statuscode = (res == null)?404:200;//404 NOT_FOUND, 200 OK
			
		}catch(Exception Exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',Exc.getMessage());
			BI_LogHelper.generate_BILog('BI_ContractRest.getContract', 'BI_EN', Exc, 'Web Service');
			
		}
		
		return res;
		
	}

}