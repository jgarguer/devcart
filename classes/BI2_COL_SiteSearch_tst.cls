@isTest(seeAllData = false)
public class BI2_COL_SiteSearch_tst {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José María Martín
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BI2_COL_SiteSearch_ctr

    <Date>               <Author>                    <Change Description>
    11/2015              José María Martín           Initial Version
    05/07/2016           José Luis González          Adapt test to UNICA modifications
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private final static String VALIDADOR_FISCAL = 'COL_NIT_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(9);
    private final static String LE_NAME = 'TestAccount_ContactoFinalSearch_TEST';

    @testSetup static void dataSetup() 
    {
        Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert account;

        Account[] acc = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketTest = BIIN_UNICA_TestDataGenerator.createTicket(acc[0]);
        insert ticketTest;
    }

    static testMethod void test_BI2_COL_SiteSearch_tst() 
    {
        BI_TestUtils.throw_exception = false;

        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case caso = [SELECT Id, BI_Id_del_caso_legado__c FROM Case WHERE AccountId =: account.Id];

        Test.startTest();  
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerContacto'));
        PageReference p = Page.BIIN_Detalle_Incidencia;
        p.getParameters().put('namefield', 'LongitudName');
        p.getParameters().put('surnamefield', 'LongitudSurname');
        p.getParameters().put('emailfield', 'LongitudEmail');
        p.getParameters().put('accountfield', account.Id);
        p.getParameters().put('siteName', 'Sede');
        Test.setCurrentPageReference(p);
        
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
        BI2_COL_SiteSearch_ctr ceController = new BI2_COL_SiteSearch_ctr();
        ceController.getContact();
        ceController.HabilitarIr();
        ceController.getHttpRequest();
        ceController.getHttpResponse();
        Test.stopTest();
    }
    
    static testMethod void test_BI2_COL_SiteSearch_tst_ELSE() 
    {
        BI_TestUtils.throw_exception = false;
        
        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case caso = [SELECT Id, BI_Id_del_caso_legado__c FROM Case WHERE AccountId =: account.Id];

        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerContacto'));
        PageReference p = Page.BIIN_Detalle_Incidencia;
        p.getParameters().put('namefield', '');
        p.getParameters().put('surnamefield', '');
        p.getParameters().put('emailfield', '');
        p.getParameters().put('accountfield', null);
        p.getParameters().put('siteName', 'Sede');
        Test.setCurrentPageReference(p);
        
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
        BI2_COL_SiteSearch_ctr ceController = new BI2_COL_SiteSearch_ctr();
        ceController.getContact();
        ceController.HabilitarIr();
        ceController.getHttpRequest();
        ceController.getHttpResponse();

        //BIIN_Obtener_Contacto_WS.ContactSalida salida = new BIIN_Obtener_Contacto_WS.ContactSalida();
        //salida = cfs.getContact(authorizationToken, query1, '', query2, query3);
        p.getParameters().put('siteName', '');
        ApexPages.Standardcontroller controller2 = new ApexPages.Standardcontroller(caso);
        BI2_COL_SiteSearch_ctr ceController2 = new BI2_COL_SiteSearch_ctr();
        //salida.outputs.output[0].code='2003';
        //ceController2.getContact();
        ceController2.query1='12';
        ceController2.query2='12';
        ceController2.HabilitarIr();
        Test.stopTest();
    }
}