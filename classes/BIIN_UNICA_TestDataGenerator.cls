public with sharing class BIIN_UNICA_TestDataGenerator 
{
/*----------------------------------------------------------------------------------------------------------------------
    Author:        José Luis González
    Company:       HPE
    Description:   Data generator class for UNICA test classess

    <Date>                 <Author>                         <Change Description>
    13/05/2016             José Luis González               Initial Version
    21/06/2016             José Luis González               Add methods generateContact() and generateSede()
	29/06/2016             José Luis González               Fix method generateSede()
	03/01/2017		  	   Marta Glez						REING-01-Adaptacion reingenieria de contactos
    20/09/2017             Angel F. Santaliestra            Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
    27/09/2017             Jaime Regidor                    Add the mandatory fields on account creation: BI_Subsector__c and BI_Sector__c

----------------------------------------------------------------------------------------------------------------------*/
  public static List<BIIN_UNICA_Pojos.KeyValueType> getAdditionalData(String caseNumber, String status, String slmStatus)
  {
    List<BIIN_UNICA_Pojos.KeyValueType> additionalData = new List<BIIN_UNICA_Pojos.KeyValueType>();
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('First Name', 'TestFirst Name'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Last Name', 'TestLast Name'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Case Number', caseNumber));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Created Date', '1462269521'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Last Modified Date', '1462269521'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Internet Email', 'test@test.com'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Urgency', '3000'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Site', 'TestSite'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Product Categorization Tier 1', 'DOCUMENTACION'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Product Categorization Tier 2', 'DOCUMENTO'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Product Categorization Tier 3', 'DOCUMENTO'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Resolution Category Tier 1', ''));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Resolution Category Tier 2', ''));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Resolution Category Tier 3', ''));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Categorization Tier 1', 'IMPLANTACION  DEMANDA'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Categorization Tier 2', 'GOLDEN JUMPER'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Categorization Tier 3', 'CAMBIO'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('BAO_ExternalID', ''));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Name', 'AST_OB_BS_IN_CI_Name'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Status', status));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Assigned Group', ''));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Resolution', ''));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('SLM Status', slmStatus));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Last Name', 'TestLast Name'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Last Name', 'TestLast Name'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Last Resolved Date', '1462269521'));
    additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('Status Reason', 'Infrastructure Change Created'));

    return additionalData;
  }

  public static BIIN_UNICA_Pojos.TicketRequestType getTicketRequest(String validadorFiscal, String correlatorId, Integer priority, String caseNumber, String status, String slmStatus)
  {
    BIIN_UNICA_Pojos.TicketRequestType ticketRequest = new BIIN_UNICA_Pojos.TicketRequestType();
    ticketRequest.subject           = 'TestSubject';
    ticketRequest.description       = 'TestDescription';
    ticketRequest.country           = 'Peru';
    ticketRequest.ticketType        = 'Incident';
    ticketRequest.customerId        = validadorFiscal;
    ticketRequest.reportedDate      = '1462269521';
    ticketRequest.correlatorId      = correlatorId;
    ticketRequest.perceivedSeverity = '2';
    ticketRequest.perceivedPriority = priority;
    ticketRequest.additionalData    = getAdditionalData(caseNumber, status, slmStatus);

    return ticketRequest;
  }

  public static Account createLegalEntity(String name, String validadorFiscal)
  {
    RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' LIMIT 1];
    Account account = new Account(
                        Name                               = name,
                        BI_Activo__c                       = Label.BI_Si,
                        BI_Country__c                      = 'Peru',
                        BI_Segment__c                      = 'Empresas',
                        BI_Subsector__c                    = 'test', //27/09/2017
                        BI_Sector__c                       = 'test', //27/09/2017
                        BI_Subsegment_Regional__c          = 'test',
                        BI_Territory__c                    = 'test',
                        BI_Tipo_de_identificador_fiscal__c = validadorFiscal.substring(validadorFiscal.indexOf('_') + 1, validadorFiscal.lastIndexOf('_')),
                        BI_No_Identificador_fiscal__c      = validadorFiscal.substring(validadorFiscal.lastIndexOf('_') + 1),
                        BI_Validador_Fiscal__c             = validadorFiscal,
                        RecordTypeId                       = rt.id
                      );

    return account;
  }

  public static Case createTicket(Account acc)
  {
    Id recordTypeIncTec = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Incidencia Tecnica').getRecordTypeId();
    
    Case ticketTest = new Case(
                        BI_Id_del_caso_legado__c = '00000000',
                        Subject                  = 'Subject test',
                        Description              = 'Description test',
                        BI_Country__c            = 'Peru',
                        AccountId                = acc.Id,
                        Priority                 = 'High',
                        Status                   = 'Assigned',
                        RecordTypeId             = recordTypeIncTec,
                        TGS_Urgency__c           = '2-High',
                        Origin                   = 'Correo electrónico',
                        TGS_SLA_Light__c         = 'Green'
                      );

    return ticketTest;
  }

  public static void loadTablaCorrespondencia()
  {
    BIIN_Tabla_Correspondencia__c tc = new BIIN_Tabla_Correspondencia__c();
    tc.BIIN_Tipo_Tabla__c = 'Prioridad';
    tc.BIIN_Valor_Remedy__c = 'Critical';
    tc.BIIN_Valor_Salesforce__c = 'Crítica';
    tc.Name = 'Prioridad-Critical';
    tc.BIIN_RoD_ID__c = '0';
    insert tc;

    tc = new BIIN_Tabla_Correspondencia__c();
    tc.BIIN_Tipo_Tabla__c = 'Prioridad';
    tc.BIIN_Valor_Remedy__c = 'High';
    tc.BIIN_Valor_Salesforce__c = 'Alta';
    tc.Name = 'Prioridad-High';
    tc.BIIN_RoD_ID__c = '1';
    insert tc;

    tc = new BIIN_Tabla_Correspondencia__c();
    tc.BIIN_Tipo_Tabla__c = 'Prioridad';
    tc.BIIN_Valor_Remedy__c = 'Medium';
    tc.BIIN_Valor_Salesforce__c = 'Medio';
    tc.Name = 'Prioridad-Medium';
    tc.BIIN_RoD_ID__c = '2';
    insert tc;

    tc = new BIIN_Tabla_Correspondencia__c();
    tc.BIIN_Tipo_Tabla__c = 'Prioridad';
    tc.BIIN_Valor_Remedy__c = 'Low';
    tc.BIIN_Valor_Salesforce__c = 'Baja';
    tc.Name = 'Prioridad-Low';
    tc.BIIN_RoD_ID__c = '3';
    insert tc;

    tc = new BIIN_Tabla_Correspondencia__c();
    tc.BIIN_Tipo_Tabla__c = 'Tipo de Incidencia';
    tc.BIIN_Valor_Remedy__c = 'User Service Restoration';
    tc.BIIN_Valor_Salesforce__c = 'Restauración de servicio a usuario';
    tc.Name = 'Tipo de Incidencia-Restoration';
    tc.BIIN_RoD_ID__c = '0';
    insert tc;

    tc = new BIIN_Tabla_Correspondencia__c();
    tc.BIIN_Tipo_Tabla__c = 'Origen';
    tc.BIIN_Valor_Remedy__c = 'Email';
    tc.BIIN_Valor_Salesforce__c = 'Correo electrónico';
    tc.Name = 'Origen-Email';
    tc.BIIN_RoD_ID__c = '2000';
    insert tc;

    tc = new BIIN_Tabla_Correspondencia__c();
    tc.BIIN_Tipo_Tabla__c = 'Urgencia';
    tc.BIIN_Valor_Remedy__c = 'High';
    tc.BIIN_Valor_Salesforce__c = 'Alta';
    tc.Name = 'Urgencia-High';
    tc.BIIN_RoD_ID__c = '2000';
    insert tc;

    tc = new BIIN_Tabla_Correspondencia__c();
    tc.BIIN_Tipo_Tabla__c = 'Subestado';
    tc.BIIN_Valor_Remedy__c = 'ROD27000';
    tc.BIIN_Valor_Salesforce__c = 'SFDC27000';
    tc.Name = 'TestTabla';
    tc.BIIN_RoD_ID__c = '27000';
    insert tc;

    tc = new BIIN_Tabla_Correspondencia__c();
    tc.BIIN_Tipo_Tabla__c = 'Estado';
    tc.BIIN_Valor_Remedy__c = 'Assigned';
    tc.BIIN_Valor_Salesforce__c = 'Asignado';
    tc.Name = 'Estado-Cancelled';
    tc.BIIN_RoD_ID__c = '6';
    insert tc;

    tc = new BIIN_Tabla_Correspondencia__c();
    tc.BIIN_Tipo_Tabla__c = 'Estado';
    tc.BIIN_Valor_Remedy__c = 'Cancelled';
    tc.BIIN_Valor_Salesforce__c = 'Cancelado';
    tc.Name = 'Estado-Assigned';
    tc.BIIN_RoD_ID__c = '1';
    insert tc;

    tc = new BIIN_Tabla_Correspondencia__c();
    tc.BIIN_Tipo_Tabla__c = 'SLM-Status';
    tc.BIIN_Valor_Remedy__c = 'Within the Service Target';
    tc.BIIN_Valor_Salesforce__c = 'Green';
    tc.Name = 'SLM-Status-WTST';
    tc.BIIN_RoD_ID__c = '1';
    insert tc;

  }

  public static String getRandomNumber(Integer digits)
  {
    String random = String.valueOf(DateTime.now().getTime());
    if(random.length() > digits)
    {
        random = random.substring(random.length() - digits, random.length());
    }
    else 
    {
        while (random.length() < digits)
        {
            random += '0';
        }
    }
    return random;
  }

  public static Contact generateContact(String email, Id accId)
  {
    Contact con = new Contact(
                            FirstName              = 'TestFirstName',
                            LastName               = 'TestLasttName',
                            Email                  = email,
                            BI_Activo__c           = true,
        					//REING-01-INI
        					//BI_Tipo_de_contacto__c = 'Técnico;Autorizado',
                            BI_Tipo_de_contacto__c = 'Técnico;General;Autorizado',
        					BI_Tipo_de_documento__c= 'DNI',
							BI_Numero_de_documento__c='12345678' ,  
        					FS_CORE_Referencias_Funcionales__c = 'Técnico',
        					  MailingState='Santiago',
                              FS_CORE_Codigo_Ciudad__c='123456',
                              MailingCity= 'Santiago',
                              MailingStreet= 'Urbinas',
                              FS_CORE_MailingNumber__c = '2',
                              FS_CORE_Barrio__c= 'BarrioFake',
                              //REING-01-FIN
                            AccountId              = accId
                            );
    return con;
  }

  public static BI_Punto_de_instalacion__c generateSede(String name, Id contactId, Id accountId)
  {
    BI_Sede__c site = new BI_Sede__c(BI_Direccion__c = 'Test Dir', BI_Codigo_postal__c = '500017');
    insert site;

    BI_Punto_de_instalacion__c sede = new BI_Punto_de_instalacion__c(
                                                                    Name           = name, 
                                                                    BI_Contacto__c = contactId,
                                                                    BI_Cliente__c  = accountId,
                                                                    BI_Sede__c     = site.Id,
                                                                    BI_Tipo_de_sede__c = 'Punto Instalación'
                                                                    );

    return sede;
  }

}