/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:          
* Description:         
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-07-24      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_Filter_cls
*            1.1	2016-12-28		Gawron, Julián E.		Add @testSetup 
*                   20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*
*************************************************************************************************************/
@isTest
private class BI_COL_Filter_tst
{
	public static BI_COL_Filter_cls								objFilter;
	public static Account                                   	objCuenta;
	Public static User 											objUsuario = new User();

	public static List <Profile>                            	lstPerfil;
	public static List <User>                               	lstUsuarios;
	public static List <UserRole>                           	lstRoles;
	
	@testSetup public static void crearData()
	{

		//perfiles
		lstPerfil                       = new list <Profile>();
		lstPerfil                       = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
		system.debug('datos Perfil '+lstPerfil);
		
		//lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);

		System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
			insert objUsuario;
		}

		System.runAs(objUsuario)
		{
			
			//Cuentas
			objCuenta                                       = new Account();      
			objCuenta.Name                                  = 'prueba';
			objCuenta.BI_Country__c                         = 'Colombia';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Fraude__c                          = false;
		    objCuenta.BI_Segment__c                         = 'test';
            objCuenta.BI_Subsegment_Regional__c             = 'test';
            objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			system.debug('datos Cuenta '+objCuenta);

		}
		
	}

	static testMethod void test_method_1()
	{
		// TO DO: implement unit test

		//crearData();
		objCuenta = [Select Id, Name, BI_Country__c, TGS_Region__c, 
					BI_Tipo_de_identificador_fiscal__c, CurrencyIsoCode, 
					BI_Fraude__c from Account limit 1];	

		ApexPages.StandardController  controler         = new ApexPages.StandardController(objCuenta);
		PageReference                 pageRef           = Page.BI_COL_SendSessionDavox_pag;
		Test.setCurrentPage(pageRef);


		Test.startTest();

			ApexPages.currentPage().getParameters().put('id',objCuenta.id);
			ApexPages.currentPage().getParameters().put('c','d2hlcmUgTmFtZT0nbnVsbCc=');
			ApexPages.currentPage().getParameters().put('f','LE5hbWU=');
			objFilter = new BI_COL_Filter_cls ();
			BI_COL_Filter_cls.GetKeyPrefix(objCuenta.Id);
			BI_COL_Filter_cls.codificar('Name');
			objfilter.abuscar=objCuenta.Id;
			objFilter.getDynamicTable();
			objFilter.buscar();

		Test.stopTest();
	}
}