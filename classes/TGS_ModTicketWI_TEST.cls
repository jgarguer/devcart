@isTest()
public class TGS_ModTicketWI_TEST {

    static Case ticket;
    
    static void configuration(){
        Test.startTest();
        
        // Level 1: Holding
        Id recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
        Contact CaseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
        insert CaseContact;
        Account holding = new Account(                     
            Name = 'Dummy holding',
            RecordTypeId = recordtypeId,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True);
        insert holding;
        
        // Level 2: Customer Country
        recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY);
        Account customerCountry = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Customer_Country + String.valueOf(Math.random()),
            RecordTypeId = recordtypeId,
            BI_Country__c = 'Spain', // JMF 19/10/2016 - Add Country
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holding.Id,
            ParentId = holding.Id);
        insert customerCountry;
        
        // Level 3: Legal Entity
        recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_LEGAL_ENTITY);
        Account legalEntity = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Legal_Entity + String.valueOf(Math.random()),
            RecordTypeId = recordtypeId,
            ParentId = customerCountry.Id,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holding.Id);
        insert legalEntity;
        
        // Level 4: Business Unit
        recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_BUSINESS_UNIT);
        Account businessUnit = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Business_Unit + String.valueOf(Math.random()),
            RecordTypeId = recordtypeId,
            ParentId = legalEntity.Id,
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holding.Id);
        insert businessUnit;

        // Level 5: Cost Center
        recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_COST_CENTER);
        Account costCenter = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Cost_Center + String.valueOf(Math.random()),
            RecordTypeId = recordtypeId,
            ParentId = businessUnit.Id,
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holding.Id,
            TGS_Aux_Business_Unit__c = businessUnit.Id,
            TGS_Aux_Customer_Country__c = customerCountry.Id,
            TGS_Aux_Legal_Entity__c = legalEntity.Id
            );
        insert costCenter;

        TGS_Tier_Dependence__c tr_CustomSetting = new TGS_Tier_Dependence__c();
        tr_CustomSetting.Name = 'Test CS Record';
        tr_CustomSetting.TGS_Holding_Name__c = 'Dummy holding';
        tr_CustomSetting.TGS_RTDevName__c = 'TGS_Incident';
        tr_CustomSetting.TGS_Product_Tier_1__c = 'Dummy tier 1';
        tr_CustomSetting.TGS_Product_Tier_2__c = 'Dummy tier 2';
        tr_CustomSetting.TGS_Product_Tier_3__c = 'Dummy tier 3';
        insert tr_CustomSetting;

        Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'TGS_Incident');
        Case caseTest = new Case(RecordTypeId = rtId , Subject = 'Test ticket case', Status = 'Assigned', ContactId=CaseContact.Id, TGS_Service__c = '1234567');
        caseTest.AccountId = legalEntity.Id;
        caseTest.TGS_Product_Tier_1__c = 'Dummy tier 1';
        caseTest.TGS_Product_Tier_2__c = 'Dummy tier 2';
        caseTest.TGS_Product_Tier_3__c = 'Dummy tier 3';
     
        insert caseTest;
        ticket = caseTest;
        ticket.TGS_Ticket_Id__c = '123456TIC';
        update ticket;
        Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the class TGS_ModTicketWI
            
     <Date>                 <Author>                <Change Description>
    27/05/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createTicketWI() {
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
        } 
        
        
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c = 'TGS';
        insert u;
        System.runAs(u){
            if(ticket==null){configuration();}
            
            /* PARAMETERS
                String Ticket_ID, String WI_ID, String Operation, String Contact_Email, String Work_Info_Notes, String Work_Info_View_Access,
                String Attachment_Name1, String Attachment_Name2, String Attachment_Name3, Blob Attachment_Body1, Blob Attachment_Body2, Blob Attachment_Body3
            */
            
            /* No operation */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID1', '', 'test@test.es', 'Description modified', 'Public',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* No ticket id */
            TGS_ModTicketWI.upsertWI( '', '123456EXTID2', 'ROD_SF_ADD_WORKINFO', 'test@test.es', 'Description', 'Public',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));        
            
            /* Invalid ticket id */
            TGS_ModTicketWI.upsertWI( '12345INV', '123456EXTID3', 'ROD_SF_ADD_WORKINFO', 'test@test.es', 'Description', 'Public',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* No workinfo id */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '', 'ROD_SF_ADD_WORKINFO', 'test@test.es', 'Description', 'Public',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* No access */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID4', 'ROD_SF_ADD_WORKINFO', 'test@test.es', 'Description', '',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* Invalid access */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID5', 'ROD_SF_ADD_WORKINFO', 'test@test.es', 'Description', 'Invalid',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* No attachment 1 name and internal view access */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID6', 'ROD_SF_ADD_WORKINFO', 'test@test.es', 'Description', 'Internal',
                                      '', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* No attachment 2 name */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID7', 'ROD_SF_ADD_WORKINFO', 'mlaliena@deloitte.es', 'Description', 'Public',
                                      'Name Attachment 1', '','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* No attachment 3 name */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID8', 'ROD_SF_ADD_WORKINFO', 'test@test.es', 'Description', 'Public',
                                      'Name Attachment 1', 'Name Attachment 2','',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* Create workinfo with attachments */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID9', 'ROD_SF_ADD_WORKINFO', 'test@test.es', 'Description', 'Public',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* Create workinfo without attachments */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID10', 'ROD_SF_ADD_WORKINFO', 'test@test.es', 'Description', 'Public',
                                      '', '','', null, null, null);
            
            
            /* Modify workinfo with attachments */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID9', 'ROD_SF_MODIFY_WORKINFO', 'test@test.es', 'Description modified', 'Public',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
           
            /* Modify workinfo without attachments */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID10', 'ROD_SF_MODIFY_WORKINFO', 'test@test.es', 'Description modified', 'Public',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* Invalid ticket id */
            TGS_ModTicketWI.upsertWI( '12345INV', '123456EXTID11', 'ROD_SF_MODIFY_WORKINFO', 'test@test.es', 'Description', 'Public',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* No ticket id */
            TGS_ModTicketWI.upsertWI( '', '123456EXTID11', 'ROD_SF_MODIFY_WORKINFO', 'test@test.es', 'Description', 'Public',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* No workinfo id */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '', 'ROD_SF_MODIFY_WORKINFO', 'test@test.es', 'Description', 'Public',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* Invalid access */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID9', 'ROD_SF_MODIFY_WORKINFO', 'test@test.es', 'Description', 'Invalid',
                                      'Name Attachment 1', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* No attachment 1 name */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID10', 'ROD_SF_MODIFY_WORKINFO', 'test@test.es', 'Description', 'Internal',
                                      '', 'Name Attachment 2','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* No attachment 2 name */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID10', 'ROD_SF_MODIFY_WORKINFO', 'test@test.es', 'Description', 'Public',
                                      'Name Attachment 1', '','Name Attachment 3',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* No attachment 3 name */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID10', 'ROD_SF_MODIFY_WORKINFO', 'test@test.es', 'Description', 'Public',
                                      'Name Attachment 1', 'Name Attachment 2','',
                                      Blob.valueOf('Body Attachment 1'), Blob.valueOf('Body Attachment 2'), Blob.valueOf('Body Attachment 3'));
            
            /* Delete attachments */
            TGS_ModTicketWI.upsertWI( ticket.TGS_Ticket_Id__c, '123456EXTID9', 'ROD_SF_MODIFY_WORKINFO', 'test@test.es', 'Description', 'Public',
                                      '', '','', null, null, null);
            
        }
    }
    static testMethod void createWI() {
         
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
        } 
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
         User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
         userTest.BI_Permisos__c = 'TGS';
         insert userTest;
         System.runAs(userTest){
        
            Contact CaseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
            insert CaseContact; 

            // Level 1: Holding
            Id recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
            Account holding = new Account(                     
                Name = 'Dummy holding',
                RecordTypeId = recordtypeId,
                BI_Country__c = 'Spain',
                BI_Segment__c = 'Empresas',
                BI_Subsegment_Regional__c='Corporate',
                TGS_Es_MNC__c = True);
            insert holding;
            
            // Level 2: Customer Country
            recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY);
            Account customerCountry = new Account(                     
                Name = 'Account Test Name - ' + Label.TGS_Customer_Country + String.valueOf(Math.random()),
                RecordTypeId = recordtypeId,
                BI_Country__c = 'Spain', // JMF 19/10/2016 - Add Country
                BI_Segment__c = 'Empresas',
                BI_Subsegment_Regional__c='Corporate',
                TGS_Es_MNC__c = True,
                TGS_Aux_Holding__c = holding.Id,
                ParentId = holding.Id);
            insert customerCountry;
            
            // Level 3: Legal Entity
            recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_LEGAL_ENTITY);
            Account legalEntity = new Account(                     
                Name = 'Account Test Name - ' + Label.TGS_Legal_Entity + String.valueOf(Math.random()),
                RecordTypeId = recordtypeId,
                ParentId = customerCountry.Id,
                BI_Country__c = 'Spain',
                BI_Segment__c = 'Empresas',
                BI_Subsegment_Regional__c='Corporate',
                TGS_Es_MNC__c = True,
                TGS_Aux_Holding__c = holding.Id);
            insert legalEntity;
            
            // Level 4: Business Unit
            recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_BUSINESS_UNIT);
            Account businessUnit = new Account(                     
                Name = 'Account Test Name - ' + Label.TGS_Business_Unit + String.valueOf(Math.random()),
                RecordTypeId = recordtypeId,
                ParentId = legalEntity.Id,
                BI_Segment__c = 'Empresas',
                BI_Subsegment_Regional__c='Corporate',
                TGS_Es_MNC__c = True,
                TGS_Aux_Holding__c = holding.Id);
            insert businessUnit;

            // Level 5: Cost Center
            recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_COST_CENTER);
            Account costCenter = new Account(                     
                Name = 'Account Test Name - ' + Label.TGS_Cost_Center + String.valueOf(Math.random()),
                RecordTypeId = recordtypeId,
                ParentId = businessUnit.Id,
                BI_Segment__c = 'Empresas',
                BI_Subsegment_Regional__c='Corporate',
                TGS_Es_MNC__c = True,
                TGS_Aux_Holding__c = holding.Id,
                TGS_Aux_Business_Unit__c = businessUnit.Id,
                TGS_Aux_Customer_Country__c = customerCountry.Id,
                TGS_Aux_Legal_Entity__c = legalEntity.Id
                );
            insert costCenter;

            TGS_Tier_Dependence__c tr_CustomSetting = new TGS_Tier_Dependence__c();
            tr_CustomSetting.Name = 'Test CS Record';
            tr_CustomSetting.TGS_Holding_Name__c = 'Dummy holding';
            tr_CustomSetting.TGS_RTDevName__c = 'TGS_Change';
            tr_CustomSetting.TGS_Product_Tier_1__c = 'Dummy tier 1';
            tr_CustomSetting.TGS_Product_Tier_2__c = 'Dummy tier 2';
            tr_CustomSetting.TGS_Product_Tier_3__c = 'Dummy tier 3';
            insert tr_CustomSetting;

            Case TestCase = TGS_Dummy_Test_Data.dummyCaseTGS(Constants.RECORD_TYPE_TGS_CHANGE, 'On Behalf');
            TestCase.ContactId = CaseContact.id;
            TestCase.AccountId = legalEntity.Id;
            TestCase.TGS_Product_Tier_1__c = 'Dummy tier 1';
            TestCase.TGS_Product_Tier_2__c = 'Dummy tier 2';
            TestCase.TGS_Product_Tier_3__c = 'Dummy tier 3';
            Test.startTest();
                insert TestCase;
            Test.stopTest();
            
            TGS_Work_Info__c workInfoTest = new TGS_Work_Info__c(TGS_Case__c = TestCase.Id , TGS_Description__c = 'Test controller description', TGS_Public__c = false);
            insert workInfoTest;
         }   
     }       
}