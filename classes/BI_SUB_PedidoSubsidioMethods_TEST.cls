/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Geraldine Sofía Pérez Montes
    Company:       Accenture LTDA
    Description:   Test class BI_SUB_PedidoSubsidioMethods

    History: 
    
    <Date>                     <Author>                <Change Description>
    07/03/2018                   GSPM                   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_SUB_PedidoSubsidioMethods_TEST {
	public static User objUsuario;
	public static User objUsuario1;
	public static BI_SUB_Pedido_Subsidio__c objPed1;
	public static BI_SUB_Pedido_Subsidio__c objPed2;
	public static BI_SUB_Pedido_Subsidio__c objPed3;
	public static BI_SUB_Pedido_Subsidio__c objPed4;
	public static Account acc;
	public static Opportunity opp;
	public static List<BI_SUB_Pedido_Subsidio__c> lstPed = new List<BI_SUB_Pedido_Subsidio__c>();
	
	
	Public static void CreatedData() {

		///lstPerfil
       List<Profile> lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
       //lstRol
       List<UserRole> lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

		objUsuario = BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);
        System.debug('**Name: +'+objUsuario.Pais__c);
		System.runAs(new User(Id=UserInfo.getUserId())){ 
        insert objUsuario;
    	}

		acc = new Account(
          Name = 'Test1ARG', 
          Industry = 'Comercio', 
          CurrencyIsoCode = 'ARS', 
          BI_Activo__c = 'Sí', 
          BI_Cabecera__c = 'Sí',  
          BI_Estado__c = true, 
          BI_Country__c = Label.BI_Argentina, 
          
          BI_Segment__c = 'Empresas', 
       
          BI_Subsegment_Regional__c = 'Corporate', 
          TGS_Es_MNC__c = false, 
          BI_No_Identificador_fiscal__c = '30111111118',
          Sector__c = 'Private',
           BI_Subsector__c = 'Banca',
            BI_Sector__c = 'Industria',
            BI_Territory__c = 'test'
        );
        insert acc;

         opp = new Opportunity(
            Name = 'OppTest1',
            CloseDate = Date.today(),
            StageName = 'F5 - Solution Definition',
            BI_No_requiere_comite_arg__c = true,
            AccountId = acc.Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            CurrencyIsoCode = 'ARS',
            BI_Licitacion__c = 'No',
            BI_SUB_Subsidio__c = 'Cambio'
        );
        insert opp;

        
        objPed1 = new BI_SUB_Pedido_Subsidio__c(
        	BI_SUB_Cliente__c = acc.Id,
        	BI_SUB_Oportunidad__c = opp.Id,
        	BI_SUB_Tipo_pedido__c = 'Cambio Con Subsidio',
        	BI_SUB_Estado__c = 'Completo',
        	CurrencyIsoCode = 'ARS'
        	);
        	lstPed.add(objPed1);
        objPed2 = new BI_SUB_Pedido_Subsidio__c(
        	BI_SUB_Cliente__c = acc.Id,
        	BI_SUB_Oportunidad__c = opp.Id,
        	BI_SUB_Tipo_pedido__c = 'Cambio Con Subsidio',
        	BI_SUB_Estado__c = 'Abierto',
        	CurrencyIsoCode = 'ARS'
        	);
        	lstPed.add(objPed2);
        	Insert lstPed;

        	
	    RecordType rt = [SELECT Id,Name,SobjectType FROM RecordType where SobjectType = 'NE__Order__c' and Name = 'Opty' limit 1];

	    NE__Order__c objOrder = new NE__Order__c();                                             
	    objOrder.NE__OptyId__c = opp.Id;
	    objOrder.NE__OrderStatus__c = 'Active';
	    objOrder.NE__AccountId__c = acc.Id;
	    objOrder.RecordTypeId = rt.Id;
	    insert objOrder;

	     NE__Product__c prod = new NE__Product__c();
         prod.Offer_SubFamily__c = 'a-test;';
         prod.Offer_Family__c = 'b-test';

        
        insert prod; 

        NE__Product__c prod2 = new NE__Product__c();
         prod2.Offer_SubFamily__c = 'a-testUSD;';
         prod2.Offer_Family__c = 'b-testUSD';
        
        insert prod2;

        NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;
        NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id, 
            NE__ProductId__c = prod.Id, 
            NE__Change_Subtype__c = 'test', 
            NE__Currency__c = 'ARS',
            NE__Disconnect_Subtype__c = 'test');
        insert catIt;

        NE__Catalog_Item__c catIt2 = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id, 
            NE__ProductId__c = prod.Id, 
            NE__Change_Subtype__c = 'test', 
            NE__Currency__c = 'USD',
            NE__Disconnect_Subtype__c = 'test');
        insert catIt2;
        System.debug('agregaTasa mapaCatalogItems catIt' + catIt);
      

        //Insertamos el order item asociado a la order
        NE__OrderItem__c oi = new NE__OrderItem__c(
            NE__OrderId__c = objOrder.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = prod.Id,
            NE__CatalogItem__c = catIt.Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 1000,
            BI_Ingreso_Recurrente_Anterior_Producto__c = 2000,
            NE__RecurringChargeOv__c = 3000
        );
        

        NE__OrderItem__c oi2 = new NE__OrderItem__c(
            NE__OrderId__c = objOrder.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = prod2.Id,
            NE__CatalogItem__c = catIt2.Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 2000,
            BI_Ingreso_Recurrente_Anterior_Producto__c = 4000,
            NE__RecurringChargeOv__c = 6000,
            BI_O4_TdCP__c = 11.0
        );
      
        List<NE__OrderItem__c> lst_itemToInsert = new List<NE__OrderItem__c>();
        lst_itemToInsert.add(oi);
        lst_itemToInsert.add(oi2);
    
        insert lst_itemToInsert;
	}
	

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Geraldine Sofía Pérez Montes
    Company:       Accenture LTDA
    Description:   metodo de prueba para metodo UpdateCaseSubsidio

    History: 
    
    <Date>                     <Author>                <Change Description>
    07/03/2018                   GSPM                   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void UpdateCaseSubsidio_TEST() {
		
		CreatedData();
		System.runAs(objUsuario)
        {
        	Test.StartTest(); 
        	BI_SUB_PedidoSubsidioMethods.UpdateCaseSubsidio(lstPed, lstPed);
        	//lstPed[0].Id = lstPed[0].Id;
        	lstPed[0].BI_SUB_Estado__c = 'Completo';        	

            Database.SaveResult[] srList = Database.update(lstPed, false);
                for(Database.SaveResult srOI: srList)
                {
                    System.assertEquals(true, srOI.isSuccess());
                }

        	Test.StopTest();

        }
	}

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Geraldine Sofía Pérez Montes
    Company:       Accenture LTDA
    Description:   metodo de prueba para metodo UpdateCaseSubsidio

    History: 
    
    <Date>                     <Author>                <Change Description>
    07/03/2018                   GSPM                   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void UpdateCaseSubsidio_TEST1() {
		
		CreatedData();
		System.runAs(objUsuario)
        {
        	Test.StartTest(); 
        	//lstPed[0].Id = lstPed[0].Id;
        	lstPed[0].BI_SUB_Estado__c = 'Cancelado';

             Database.SaveResult[] srList = Database.update(lstPed, false);
                for(Database.SaveResult srOI: srList)
                {
                    System.assertEquals(true, srOI.isSuccess());
                }

        	BI_SUB_PedidoSubsidioMethods.UpdateCaseSubsidio(lstPed, lstPed);

        	Test.StopTest();

        }
	}

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Geraldine Sofía Pérez Montes
    Company:       Accenture LTDA
    Description:   metodo de prueba para metodo NoCreatePedido

    History: 
    
    <Date>                     <Author>                <Change Description>
    07/03/2018                   GSPM                   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void NoCreatePedido_TEST() {
		
		CreatedData();
		System.runAs(objUsuario)
        {
        	Test.StartTest(); 
        	
        	BI_SUB_PedidoSubsidioMethods.NoCreatePedido(lstPed, lstPed);

        	Test.StopTest();

        }
	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Geraldine Sofía Pérez Montes
    Company:       Accenture LTDA
    Description:   metodo de prueba para metodo NoCreatePedido

    History: 
    
    <Date>                     <Author>                <Change Description>
    07/03/2018                   GSPM                   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void NoCreatePedido_TEST1() {
		
		List<Profile> lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
       //lstRol
       List<UserRole> lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];
		objUsuario1 = BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);
        System.debug('**Name: +'+objUsuario1.Pais__c);
		System.runAs(new User(Id=UserInfo.getUserId())){ 
        insert objUsuario1;
    	}
		Account acc1 = new Account(
          Name = 'Test1ARG', 
          Industry = 'Comercio', 
          CurrencyIsoCode = 'ARS', 
          BI_Activo__c = 'Sí', 
          BI_Cabecera__c = 'Sí',  
          BI_Estado__c = true, 
          BI_Country__c = Label.BI_Argentina, 
          
          BI_Segment__c = 'Empresas', 
       
          BI_Subsegment_Regional__c = 'Corporate', 
          TGS_Es_MNC__c = false, 
          BI_No_Identificador_fiscal__c = '30111111118',
          Sector__c = 'Private',
           BI_Subsector__c = 'Banca',
            BI_Sector__c = 'Industria',
            BI_Territory__c = 'test'
        );
        insert acc1;

        Opportunity opp1 = new Opportunity(
            Name = 'OppTest1',
            CloseDate = Date.today(),
            StageName = 'F5 - Solution Definition',
            BI_No_requiere_comite_arg__c = true,
            AccountId = acc1.Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            CurrencyIsoCode = 'ARS',
            BI_Licitacion__c = 'No',
            BI_SUB_Subsidio__c = 'Cambio'
        );
        insert opp1;
        List<BI_SUB_Pedido_Subsidio__c> lstPed1 = new List<BI_SUB_Pedido_Subsidio__c>();
		objPed3 = new BI_SUB_Pedido_Subsidio__c(
        	BI_SUB_Cliente__c = acc1.Id,
        	BI_SUB_Oportunidad__c = opp1.Id,
        	BI_SUB_Tipo_pedido__c = 'Cambio Con Subsidio',
        	BI_SUB_Estado__c = 'Completo',
        	CurrencyIsoCode = 'ARS'
        	);
        	lstPed1.add(objPed3);
        objPed4 = new BI_SUB_Pedido_Subsidio__c(
        	BI_SUB_Cliente__c = acc1.Id,
        	BI_SUB_Oportunidad__c = opp1.Id,
        	BI_SUB_Tipo_pedido__c = 'Cambio Con Subsidio',
        	BI_SUB_Estado__c = 'Abierto',
        	CurrencyIsoCode = 'ARS'
        	);
        	lstPed1.add(objPed4);
        	Insert lstPed1;

        List<BI_SUB_Pedido_Subsidio__c> lstPed2 = new List<BI_SUB_Pedido_Subsidio__c>();
        	BI_SUB_Pedido_Subsidio__c objPed5 = new BI_SUB_Pedido_Subsidio__c(
        	BI_SUB_Cliente__c = acc1.Id,
        	BI_SUB_Oportunidad__c = opp1.Id,
        	BI_SUB_Tipo_pedido__c = 'Cambio Con Subsidio',
        	BI_SUB_Estado__c = 'Completo',
        	CurrencyIsoCode = 'ARS'
        	);
        	lstPed2.add(objPed5);
        BI_SUB_Pedido_Subsidio__c objPed6 = new BI_SUB_Pedido_Subsidio__c(
        	BI_SUB_Cliente__c = acc1.Id,
        	BI_SUB_Oportunidad__c = opp1.Id,
        	BI_SUB_Tipo_pedido__c = 'Cambio Con Subsidio',
        	BI_SUB_Estado__c = 'Abierto',
        	CurrencyIsoCode = 'ARS'
        	);
        	lstPed2.add(objPed6);
        	Insert lstPed2;
		//CreatedData();
		System.runAs(objUsuario1)
        {

        	Test.StartTest(); 
        	BI_SUB_PedidoSubsidioMethods.NoCreatePedido(lstPed2,lstPed1);
        	List<GroupMember> lstGM = new List<GroupMember>();
			Group idgrupo = [SELECT Id FROM Group WHERE Developername = 'BI_SUB_TBD'];			
			GroupMember gm = new GroupMember(
				GroupId = idgrupo.Id,
				UserOrGroupId = objUsuario1.Id
			);
			lstGM.add(gm);
			GroupMember gm1 = new GroupMember(
				GroupId = idgrupo.Id,
				UserOrGroupId = objUsuario1.Id
			);
			lstGM.add(gm1);
			insert lstGM;
            Database.SaveResult[] srList = Database.insert(lstGM, false);
            for(Database.SaveResult srOI: srList)
            {
                System.assertEquals(true, srOI.isSuccess());
            }


        	Test.StopTest();

        }
	}
	
}