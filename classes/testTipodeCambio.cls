@isTest
private class testTipodeCambio {
	static testmethod void classtestTipodeCambio (){
		CurrencyTypeFacturacion__c CurrencyTypeFacturacion = new CurrencyTypeFacturacion__c(
		IsoCode__c = 'MXN1'
		
		);
		
		TipoCambio__c tipodecambio = new TipoCambio__c (
	    	CodigoDivisa__c = 'MXM',
	    	Factor__c = Double.valueOf('0.07768015971040836'),
	    	ConversionRate__c =  Double.valueOf('12.2188')
  		 );
    	insert tipodecambio;
    	
    	// Verify that the initial state is as expected.

	    tipodecambio = [SELECT Factor__c, CodigoDivisa__c
        FROM TipoCambio__c
        WHERE Id = :tipodecambio.Id];
    	
    	    	
    	tipodecambio.Factor__c =  Double.valueOf('0.08768015971040836');
    	update  tipodecambio;
    	
    	List<CurrencyType> listaCT = new  List<CurrencyType>([Select IsoCode, IsCorporate, Id, DecimalPlaces, ConversionRate,IsActive from CurrencyType]);
	}

}