@isTest
public class API_AC_Categories_TEST {
  

  
    @testSetup
    static void createTestServiceCost() {
         // Create test record
        CWP_AC_ServicesCost__c serviceCostTest = new CWP_AC_ServicesCost__c(
            CWP_AC_City__c='Madrid',
            CWP_AC_Country__c='Spain',
            CWP_AC_Contract_Term__c = 12);
        insert serviceCostTest;
        
    }
    
    @isTest 
    public static void testGetCountryCityData() {
        
        createTestServiceCost();
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = System.Label.BI_URLAPICategories;
        request.httpMethod = 'GET';
        request.params.put('name', 'country-city');
        RestContext.request = request;
        RestContext.response = response;

        // Call the method to test
        API_AC_Categories.doGet();
        System.assertEquals('[{"name":"Spain-Madrid","id":"country-city"}]', response.responseBody.toString());
    }

    @isTest 
    public static void testGetContractTermData() {
        
        createTestServiceCost();
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = System.Label.BI_URLAPICategories;
        request.httpMethod = 'GET';
        request.params.put('name', 'contract-term');
        request.params.put('fields', 'Spain');
        request.params.put('limit', 'Madrid');
        RestContext.request = request;
        RestContext.response = response;
        
        // Call the method to test
        API_AC_Categories.doGet();
        System.debug('response.responseBody.toString() -> ' + response.responseBody.toString());
        System.assertEquals('[{"name":"12","id":"contract-term"}]', response.responseBody.toString());
       

    }
    
    @isTest 
    public static void testGenericError() {
        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = System.Label.BI_URLAPICategories;
        request.httpMethod = 'GET';
        request.params.put('false', 'testfalse');
         RestContext.request = request;
        RestContext.response = response;
        // Call the method to test
        API_AC_Categories.doGet();
        System.assertEquals('Type requested invalid.', response.responseBody.toString());
    }
}