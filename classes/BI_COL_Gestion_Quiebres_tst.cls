/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    22015-06-02      Daniel ALexander Lopez (DL)     Cloned test Class      
*************************************************************************************/
@isTest
public with sharing class BI_COL_Gestion_Quiebres_tst {
	
	//static testMethod void myUnitTest2() {
      
 //     Account cliente = new Account(Name ='NEXTANT SUCURSAL COLOMBIA',
 //                 Tipo_Id__c= 'NIT',
 //                 Identificaci_n__c = '000000001-1',
 //                 Tipo__c = 'Activo',
 //                 Aplica_LD__c = true,
 //                 Cartera__c = 'OK',
 //                 Naturaleza__c = 'JURIDICA',
 //                 Marcaci_n_del_cliente__c = '3',
 //                 Segmento_Telef_nica__c = 'EMPRESAS',
 //                 Sector_Telef_nica__c = 'CORPORATIVO',
 //                 Subsegmento_Telef_nica__c = 'FINANCIERO',
 //                 Subsector_Telef_nica__c = 'BANCA',
 //                 Actividad_Telef_nica__c = 'BANCA COMERCIAL PRIVADA',
 //                 Description = 'Nextant',
 //                 Gerencia_Comercial__c = 'JOSE LUIS RODRIGUEZ GARCIA',
 //                 Mercado_Objetivo__c = true,
 //                 Jefatura_Comercial__c = 'FEDERICO PELAEZ',
 //                 Pais__c = 'Colombia',
 //                 Phone = '13450280');
                  
 //   insert cliente;
     
     
 //    Opportunity oppty = new Opportunity(
 //                     Name = 'Oportunidad prueba ',
 //                     AccountId = cliente.id,
 //                     Prioridad__c = '3',
 //                     CloseDate = System.today().addDays(7),
 //                     StageName = 'F5 - En comentarios o En Estructuración');
 //   insert oppty;
    
 //   Anchos_de_Banda__c anchoBanda = new Anchos_de_Banda__c(Name = 'DS1',
 //                           Ancho_de_Banda__c = 'DS1');
 //   insert anchoBanda;
                          
 //   Producto_Telefonica__c producto = new Producto_Telefonica__c(
 //                       Tipo_registro_DS__c = 'DS-Conectividad',
 //                       Segmento__c = 'EMPRESAS',
 //                       Producto__c = 'MPLS',
 //                       Linea__c = 'BANDA ANCHA',
 //                       Familia__c = 'Datos',
 //                       Descripcion_Referencia__c = 'VPN IP MPLS DATOS',
 //                       Cod_Desc_Referencia__c = 'EQCENO79',
 //                       Anchos_de_banda__c = anchoBanda.Id,
 //                       Activo__c = true);
                        
 //   producto.Acceso_internet__c='No';     
 //     producto.Activo__c=true;     
 //     producto.Administracion__c='No';    
 //     producto.Agrupacion__c='Agrupacion__c';    
 //     producto.Aplicacion_homologada_sobre_Red_celular__c='Si';     
 //     producto.AutoAtendant__c=true;  
 //     producto.Backup__c='Si';    
 //     producto.Banda_espectral__c='C';     
 //     producto.Cantidad_servicios__c=0;    
 //     producto.Producto__c = 'INTERNET'; 
 //     producto.Descripcion_Referencia__c = 'INTERNET SEGURO';
 //     producto.Acceso_internet__c = 'Si';
 //     producto.Segmento__c = 'EMPRESAS';
 //     producto.Tipo_registro_DS__c = 'DS-Internet Dedicado';
 //     producto.Cod_Desc_Referencia__c = 'INT12345';
 //     producto.Linea__c = 'DATOS E INTERNET';
 //     producto.SubFamilia__c = 'ACCESO A INTERNET';
 //     producto.Familia__c = 'Internet';
 //     producto.Tipo_anexo__c = 'Anexo de TV';  
                        
 //   insert producto;
    
 //   Ciudad__c ciudad = new Ciudad__c(Name= 'BOGOTA DC / CUNDINAMARCA',
 //               Codigo_DANE__c = '11001000',
 //               Regional__c = 'Centro Sur');
                
 //   insert ciudad;  
    
    
 //   Sucursal__c sucursal = new Sucursal__c(Cliente__r = cliente,
 //                   Cliente__c = cliente.Id,
 //                   Name = 'Sucursal Prueba',
 //                   Tipo_Sucursal__c = 'PRINCIPAL',
 //                   Estado_callejero__c='Validado por callejero',
 //                   Direcci_n_Sucursal__c='calle 142 # 22-09',
 //                   Ciudad__c = ciudad.Id);
 //    insert sucursal;
     
 //    Nueva_DS__c ds = new Nueva_DS__c(  Oportunidad__c = oppty.id,
 //                     Producto_Telefonica__c = producto.id,
 //                     Sucursal_Origen__c = sucursal.id);
 //   insert ds;
                      
 //   BI_COL_Modificacion_de_Servicio__c ms = [select Id,Observaciones__c,Name,Estado_orden_trabajo__c,Estado__c  from BI_COL_Modificacion_de_Servicio__c where   Descripcion_Servicio_R__c=:ds.Id limit 1];
 //     ms.Estado__c='Enviado';
 //     ms.Estado_orden_trabajo__c='En Desarrollo';
 //     update ms;
      
 //     Gestion_Quiebres_sw.GestionQuiebrestRequest req=new Gestion_Quiebres_sw.GestionQuiebrestRequest();
 //     Gestion_Quiebres_sw.GestionQuiebrestResponse resp=new Gestion_Quiebres_sw.GestionQuiebrestResponse();
 //     Gestion_Quiebres_sw.UsuariosResponse usResp=new Gestion_Quiebres_sw.UsuariosResponse();
 //     Gestion_Quiebres_sw.ErrorLog el=new Gestion_Quiebres_sw.ErrorLog();
      
 //     req.Causal='Comercial - Permisos';
 //     req.CodTrs='1234';
 //     req.Ms=ms.Name;
 //     req.Estado='Aplazada';
 //     req.FechaAplazamiento=system.now().date();
 //     req.Observaciones='ninguna';
 //     req.IdUsuario=UserInfo.getUserId();
 //     Gestion_Quiebres_sw.insertarQuiebre(req);
 //     Gestion_Quiebres_sw.actualizarQuiebre(req);      
 //     Gestion_Quiebres_sw.ConsultarUsarios();
 //     Gestion_Quiebres_sw.adicionaError(resp, 1, 'Error');
      
 //   }
    
}