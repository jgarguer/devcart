/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Oscar Ena
Company:       Accenture - New Energy Aborda
Description:   Helper with all Ebounding Methods

History:

<Date>            <Author>          <Description>
30/08/2017        Oscar Ena        Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class TGS_Ebounding_Helper {

	private static final String RTYPE_ID_ORDER_MANAGEMENT_CASE = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, Constants.CASE_RTYPE_DEVNAME_ORDER_MNGMNT);

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that creates a Case for a given Account 

    IN:            Id sAccountId 
    			   TGS_Ebounding_WS.TicketRequestType wOrder
    OUT:           Case
    
    History:

    <Date>            <Author>          <Description>
    30/08/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static Case createCase (Id sAccountId, TGS_Ebounding_WS.TicketRequestType wOrder){

		Case oCase = new Case();

		oCase.RecordTypeId = RTYPE_ID_ORDER_MANAGEMENT_CASE; 
		oCase.Subject = Constants.CASE_RTYPE_ORDER_MNGMNT;
		oCase.Origin = 'Case';
		oCase.AccountId = sAccountId;
		oCase.Status = Constants.CASE_STATUS_ASSIGNED;
		
		oCase.Type = wOrder.type; 
		oCase.Description = wOrder.description; 
		oCase.TGS_Agrupador__c = wOrder.parentTicket; 
		oCase.BI_Id_SistemaLegado__c = wOrder.correlationId;

		System.debug('TGS_Ebounding_Helper.createCase :: oCase '+oCase);
		insert oCase;

        //Query CreatedDate and CaseNumber after insert to return all required fields in interface
        List<Case> listCase = [SELECT CreatedDate, CaseNumber, BI_Id_SistemaLegado__c, TGS_Agrupador__c, Description, Type, Status, AccountId, Origin, Subject, RecordTypeId 
                                FROM Case WHERE Id =: oCase.id];
		return listCase.get(0);
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that creates a Modification/Termination Order

    IN:            String sAssetName
    			   String sConfigurationType
    			   
    OUT:           NE__ORder__c 
    
    History:

    <Date>            <Author>          <Description>
    30/08/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static NE__Order__c createModificationOrTerminationOrder(NE__Asset__c oComercialAsset, String sConfigurationType){

		NE__Order__c oOrder;
		Case oCase = new Case();

	    if (sConfigurationType.equals('Change')){
        
        	NE.JS_RemoteMethods.changeAsset(oComercialAsset.id, oComercialAsset.Name);
    	
    	} else if (sConfigurationType.equals('Disconnect')){

    		NE.JS_RemoteMethods.disconnectAsset(oComercialAsset.id, oComercialAsset.Name);

    	}
        
        //Get modification/termination order which has been created
        List<NE__Order__c> listOrder = [SELECT id, Name, NE__Asset__r.Name, CreatedDate, Case__r.Id, Case__r.CaseNumber, Case__r.CreatedDate, Case__r.Type, Case__r.Status
                                        FROM NE__Order__c 
                                        WHERE NE__Asset__r.Id =: oComercialAsset.id
                                            AND NE__OrderStatus__c = 'Pending' //Revisar este criterio con Guille
                                            AND NE__Configuration_Type__c =: sConfigurationType
                                        ORDER BY CreatedDate Desc];
        system.debug('TGS_Ebounding_Helper.createModificationOrTerminationOrder ## listOrder: ' + listOrder);

        if (!listOrder.isEmpty()){
        	oOrder = listOrder.get(0);                                                                     
        }
        
        return oOrder;    
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that returns a Case list related to its order

    IN:            Map<Id, NE__Order__c> mapIdCaseOrder
    			       			   
    OUT:           List<Case> 
    
    History:

    <Date>            <Author>          <Description>
    30/08/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static List<Case> getCasesRelatedToOrders(Map<Id, NE__Order__c> mapIdCaseOrder){

		List<Case> listCases = new List<Case>();

		for (Case inCase : [SELECT Id, Order__c FROM Case WHERE Id IN : mapIdCaseOrder.keyset() AND BI_Id_SistemaLegado__c != null]){
			
			if (!mapIdCaseOrder.isEmpty() && mapIdCaseOrder.get(inCase.Id) != null && mapIdCaseOrder.get(inCase.Id).Id != null) {
				inCase.Order__c = mapIdCaseOrder.get(inCase.Id).Id;
				listCases.add(inCase);	
			} 
		}
		System.debug('TGS_Ebounding_Helper.getCasesRelatedToOrders :: listCases ' +listCases);
        return listCases;
    }

	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that returns if exist a given BI_Id_SistemaLegado__c in another case

    IN:            String sEboundingId
    			       			   
    OUT:           Boolean 
    
    History:

    <Date>            <Author>          <Description>
    26/09/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static Boolean existEboundingId(String sEboundingId){

		Boolean existEboundingId = false;

		List<Case> listCase = [SELECT id FROM Case WHERE BI_Id_SistemaLegado__c =: sEboundingId];

		existEboundingId = !listCase.isEmpty() ? true : false;

		return existEboundingId;
	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that returns if exist a given TGS_Agrupador__c in another case

    IN:            String tgsAgrupador
    			       			   
    OUT:           Boolean 
    
    History:

    <Date>            <Author>          <Description>
    26/09/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Boolean existTGS_Agrupador(String tgsAgrupador){
    	Boolean existTGS_Agrupador = false;

    	List<Case> listCase = [SELECT id FROM Case WHERE TGS_Agrupador__c =: tgsAgrupador];
    	System.debug('TGS_Ebounding_Helper.existTGS_Agrupador :: listCase ' +listCase);

    	existTGS_Agrupador = !listCase.isEmpty() ? true : false;

    	return existTGS_Agrupador;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that returns if all cases of a given TGS_Agrupador__c are Closed or Cancelled

    IN:            String tgsAgrupador
                                   
    OUT:           Boolean 
    
    History:

    <Date>            <Author>          <Description>
    26/09/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Boolean areGroupCasesClosedOrCancelled(String tgsAgrupador){
        
        Boolean areGroupCasesClosedOrCancelled = false;

        for (Case inCase : [SELECT id, Status FROM Case WHERE TGS_Agrupador__c =: tgsAgrupador]){

            if (inCase.Status.equals(Constants.CASE_STATUS_CANCELLED) || inCase.Status.equals(Constants.CASE_STATUS_CLOSED) ){
                areGroupCasesClosedOrCancelled = true;
            } else {
                areGroupCasesClosedOrCancelled = false;
                break;
            }
        }

        return areGroupCasesClosedOrCancelled;
    }
    
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that returns a Comercial Asset from a given TGS_Agrupador__c

    IN:            String tgsAgrupador
    			       			   
    OUT:           NE__Asset__c 
    
    History:

    <Date>            <Author>          <Description>
    26/09/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static NE__Asset__c getComercialAssetFromCase(String tgsAgrupador){
    	
    	NE__Asset__c oComercialAsset = new NE__Asset__c();

    	List<Case> listCase = [SELECT id, Order__c, Asset__r.Name, Asset__r.id, Asset__r.NE__Asset__r.Id, Asset__r.NE__Asset__r.Name  
    							FROM Case 
    							WHERE TGS_Agrupador__c =: tgsAgrupador
    							AND RecordtypeId =: TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, Constants.CASE_RTYPE_DEVNAME_ORDER_MNGMNT) 
    							AND Asset__c != null
    							ORDER BY createdDate Desc];

		System.debug('TGS_Ebounding_Helper.getComercialAssetFromCase :: listCase ' +listCase);    							

    	if (!listCase.isEmpty() && (listCase.get(0).Asset__r != null) ){
    		
    		oComercialAsset = listCase.get(0).Asset__r.NE__Asset__r;
    	}
    	System.debug('TGS_Ebounding_Helper.getComercialAssetFromCase :: oComercialAsset ' +oComercialAsset); 

    	return oComercialAsset; 

    }

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that generates a request

    
    IN:            String sURL
    			   String sMethod
    			   Object sBody
    			       			   
    OUT:           HttpRequest 
    
    History:

    <Date>            <Author>          <Description>
    26/09/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static HttpRequest getRequest(String sURL, String sMethod, Object sBody){
            
        final Integer TIMEOUT = 120000;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(sURL);
        req.setMethod(sMethod);

        if (sBody != null){
        	String sCleanBody = cleanRequest(JSON.serialize(sBody,true));
        	req.setBody(sCleanBody);
        }
        req.setTimeout(TIMEOUT);

        System.debug('TGS_Ebounding_Helper.getRequest :: req.toString ' + req.toString());
        System.debug('TGS_Ebounding_Helper.getRequest :: sURL' + sURL);
        System.debug('TGS_Ebounding_Helper.getRequest :: req.getBody' + req.getBody());

        return req;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that fix the request body replacing reserved words if is necessary

    
    IN:            String reqBody
    			       			   
    OUT:           HttpRequest 
    
    History:

    <Date>            <Author>          <Description>
    26/09/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static String cleanRequest(String reqBody){

        if (String.isNotEmpty(reqBody)){
            // Avoid reserved words before parsing/deserializing like: 'new', 'Exception'
            //reqBody = reqBody.replaceAll('"NEW_STATUS"', '"NEW"');
        }

        return reqBody;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that process only one Ebounding callout in a future context

    
    IN:            String sEboundingCaseId
                   String sEboundingCaseStatus
                                   
    OUT:           Void
    
    History:

    <Date>            <Author>          <Description>
    06/10/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @future (callout=true)
    public static void sendEboundingNotification(Set <Id> set_idCases){

        List <Case> lst_cases = [SELECT Id, Status, Bi_Id_SistemaLegado__c FROM Case WHERE Id IN: set_idCases];

        if(lst_cases.size() == 1){
            BI_RestRequestHelper.sendEboundingNotification(lst_cases[0].Bi_Id_SistemaLegado__c, lst_cases[0].Status);
        }
        else{
            System.enqueueJob(new TGS_Ebounding_Notifications_Job(lst_cases));
        }

        

    }


}