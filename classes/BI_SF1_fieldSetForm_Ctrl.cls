/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Pardo Corral
Company:       Accenture
Description:   Controller for the component BI_SF1_fieldSetForm, retrieves the values needed to print view form and new form

History: 
<Date>                  <Author>                <Change Description>
30/11/2017              Antonio Pardo corral    Initial Version 
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_SF1_fieldSetForm_Ctrl {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Pardo Corral
Company:       Accenture
Description:   returns the information of the fields given by the fieldsSet, given the fieldSet name and the record information

IN:             String fieldSet - name of the fieldSet
                String recordId - Id of the record
                Sobject record - sObject
                
OUT:            List<Map<String, String>> - emulates an JSON containing the field information needed, every position of the list
                contains apiName, label, type (the type given by the schema) and value to make javascript coding easier.
                Example {"apiName" : "BI_Country__c", "label" : "Counrty", "type" : "TEXT" ,"value" : "Argentina"}[0]

History: 
<Date>                  <Author>                <Change Description>
30/11/2017              Antonio Pardo corral    Initial Version 
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static List<Map<String, String>>getMapFields(String fieldSet, Id recordId, Sobject record){
        
        //I'm using the shcema to retrieve the values of a given field api name of an unknown sObject
        List<Map<String, String>> lst_return = new List<Map<String, String>>();
    
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(recordId.getSObjectType().getDescribe().getName());
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSet);
        system.debug('DescribeSObjectResultObj: '+ DescribeSObjectResultObj);
        system.debug('fieldSetObj: '+fieldSetObj);
        System.debug(record);
        Map<String, String> map_field;

         for (Schema.FieldSetMember fld : fieldSetObj.getFields())
            {
                map_field= new Map<String, String>();
                map_field.put('apiName',  fld.getFieldPath());
                map_field.put('label', fld.getLabel());
                map_field.put('type', String.valueOf(fld.getType()));
                
                map_field.put('value', String.valueOf(record.get(fld.getFieldPath())));

                lst_return.add(map_field);
            
            }
        return lst_return;
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Pardo Corral
    Company:       Accenture
    Description:   returns recordTypes of a given sObject

    IN:             String objNamet - Object API name
                    
    OUT:            Map<String, String> - a map containing the id of the recordType and the developer name

    History: 
    <Date>                  <Author>                <Change Description>
    30/11/2017              Antonio Pardo corral    Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static Map<String, String> getRecordtypeId(String objName){
        Map<String, String> map_devName_Id = new Map<String, String>();
        for(RecordType rt : [SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = :objName]){
            map_devName_Id.put(rt.DeveloperName, rt.Id);
        }
        return map_devName_Id;

    }
}