public class BIIN_UNICA_SiteMethods 
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        José Luis González Beltrán
  Company:       HPE
  Description:   Test method to manage the code coverage for BIIN_UNICA_AccountHandler
  
  <Date>             <Author>                        		    <Change Description>
  22/06/2016         José Luis González Beltrán           	Initial Version
  01/07/2016         José Luis González Beltrán             Retrieve AccountId
  06/07/2016         José Luis González Beltrán             Check contacts list size
  25/10/2016         Antonio Pardo                          Encapsulate for Peru accounts
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
  public static void updateRoDContact(List<BI_Punto_de_instalacion__c> lst_sites_new, List<BI_Punto_de_instalacion__c> lst_sites_old)
  { 
    Set<Id> set_toProcess = new Set<Id>();

    for(BI_Punto_de_instalacion__c site : lst_sites_new)
    {
      if(site.BI_Tipo_de_sede__c == 'Punto Instalación' && site.BI_Contacto__c != null){
        set_toProcess.add(site.Id);
      }
    }
    
    if(!set_toProcess.isEmpty()){
        
      List<Id> contactIds = new List<Id>();
        
      for(BI_Punto_de_instalacion__c site : [SELECT Id, BI_Cliente__r.BI_Country__c,BI_Contacto__r.Id  FROM BI_Punto_de_instalacion__c WHERE Id IN : set_toProcess AND BI_Cliente__r.BI_Country__c = :Label.BI_Peru]){
        contactIds.add(site.BI_Contacto__r.Id);
      }
     
      if(!contactIds.isEmpty()){

        List<Contact> contacts = [SELECT Id, BI_Tipo_de_contacto__c, FirstName, LastName, Estado__c, Email, AccountId 
                                  FROM Contact WHERE Id IN :contactIds];
        for(Contact con : contacts){
          if(!Test.isRunningTest()){
            BIIN_UNICA_ContactHandler.afterUpdate(JSON.serialize(con), JSON.serialize(new Contact()));
          }
        }
      }
    }
  }
}