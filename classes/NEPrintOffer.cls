public class NEPrintOffer {
    
    public String Page {get; set;}
    public String OpenPageURL {get; set;}
    public String quoteid {get; set;}
    public String  orderId{get;set;}//id dell'order
    public String  AccountId{get;set;}//id dell'account
    public NE__Order__c orderPdf    {get;set;}
    public Account acc          {get;set;}
    public NE__Billing_Profile__c billProf {get;set;}
    public List  <NE__Order_Item_Attribute__c> orderItMap;
    public List <NE__OrderItem__c> ordItem {get;set;}
    public NE__Document_Component__c DC {get;set;}
    public NE__Document_Component__c DCitem {get;set;}
    public NE__Document_Component__c DCattribute {get;set;}
    public String content {get;set;} 
    public List<Contact > contactList {get; set;}  
    public String AccountIdx {get;set;} 
    
    Set<string> rightvalues = new Set<string>();
    public String[] stories {get;set;}
    public String stories2 {get;set;}
    public String bodyMail {get;set;}
    public String objectMail {get;set;}
    public NE__Lov__c lovBody{get;set;}
    
    
    public NEPrintOffer()
    {
        try
        {
            System.debug('test ');   
            orderId            =       ApexPages.currentPage().getParameters().get('OrderId');
            
            //Find the body/subject from the lov
            system.debug('UserInfo.getLocale(): '+UserInfo.getLocale());
            try
            {
                NE__Lov__c lovSubj  =   [Select SystemModstamp, OwnerId, Name, NE__Value3__c, NE__Value2__c, NE__Value1__c, NE__Type__c, NE__Lov__c, NE__Active__c, LastModifiedDate, LastModifiedById, IsDeleted, Id, CurrencyIsoCode, CreatedDate, CreatedById 
                                        From NE__Lov__c
                                        WHERE NE__Type__c = 'MAIL_SUBJECT' and NE__Value2__c =: UserInfo.getLocale() LIMIT 1];
                                        
                objectMail          =   lovSubj.NE__Value1__c;
            }
            catch(Exception e)
            {
                objectMail  =   '';
            }

            try
            {           
                lovBody             =   [Select Text_Area_Value__c,SystemModstamp, OwnerId, Name, NE__Value3__c, NE__Value2__c, NE__Value1__c, NE__Type__c, NE__Lov__c, NE__Active__c, LastModifiedDate, LastModifiedById, IsDeleted, Id, CurrencyIsoCode, CreatedDate, CreatedById 
                                        From NE__Lov__c
                                        WHERE NE__Type__c = 'MAIL_BODY' and NE__Value2__c =: UserInfo.getLocale() LIMIT 1];
                
             
                bodyMail            =   lovBody.Text_Area_Value__c;
            }
            catch(Exception e)
            {
                bodyMail    =   '';
            }           
        }
        catch (System.CalloutException e)
        {
            System.debug('ERROR:' + e);
        }
        
    }
    
   public List<SelectOption> getStoryItems()
    {
        orderId            =       ApexPages.currentPage().getParameters().get('OrderId');
        List<SelectOption> options = new List<SelectOption>();
        try{
        orderPdf = [select id,Name, NE__AccountId__c,NE__BillingProfId__r.Name, NE__BillingProfId__r.NE__Card_Type__c, NE__BillingProfId__r.NE__Card_Number__c  from NE__Order__c where id=:orderId];
        contactList = [Select  email From Contact where AccountId =: orderPdf.NE__AccountId__c and email !=:null];
        for(Contact a: contactList)
        {
                options.add(new SelectOption(a.email,a.email));
        }
        }
        catch(Exception e) {}
        return options;
    }
    
    public String[] getStories() 
    {
        return stories;
    }
    
    public void setStories(String[] stories) 
    {
        this.stories= stories;
    } 
    
    //07/12/2015              Guillermo Muñoz         Prevent the email send in Test class    
    public PageReference selectclick()
    {
        try
        {
            for(String s : stories)
            {
                rightvalues.add(s);
                System.debug('Address '+s);   
            }
    
            //orderId =  Page;
            orderId            =       ApexPages.currentPage().getParameters().get('OrderId');
            
            Messaging.EmailFileAttachment pdfAttc = new Messaging.EmailFileAttachment();
            
            //TODO, waiting for a print template
            PageReference MailPDF = new PageReference('/apex/NEDocumentPDF?id='+orderId);
            if(!Test.isRunningTest())
                pdfAttc.Body    =   MailPDF.getContentAsPDF();
            else
                pdfAttc.Body    =   Blob.valueof('test');
            
            Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
            String[] toAddresses;
            
            //Setting subject
            NE__Order__c noffer = [Select Name,OwnerId,NE__OptyId__c, NE__AccountId__c, NE__Version__c FROM NE__Order__c WHERE Id=:orderId limit 1];
            String nameatt  =   noffer.Name+'_version_'+noffer.NE__Version__c+'.pdf';
            String subject  =   nameatt;
            string csvname  =   nameatt;
            pdfAttc.setFileName(csvname);           
            
            email.setSubject(objectMail);
            
            String[] toAddresses2=stories2.split(';');
            if(toAddresses2.size()>0)
            {
                for(Integer i=0;i<toAddresses2.size();i++)
                {
                    if(toAddresses2[i] != null && toAddresses2[i] != '')
                        stories.add(toAddresses2[i]);
                }
            }
            
            system.debug('stories: '+stories);
            system.debug('stories size: '+stories.size());
            if(stories != null && stories.size() > 0)
            {
                //Setting addresses
                email.setToAddresses( stories);
                
                //Setting body
               
                email.setHtmlBody(lovBody.Text_Area_Value__c);
                //email.setPlainTextBody(lovBody.Text_Area_Value__c);
                email.setFileAttachments(new Messaging.EmailFileAttachment[]{pdfAttc});

                try
                {
                    if(!Test.isRunningTest()){
                        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
                    }
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, Label.SendOK));
                }
                catch(Exception e)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendKO));
                }
                
                //Create task
                String contactid    = null;
                if(noffer.NE__OptyId__c != null)
                {
                    Opportunity optyPrint   =   [SELECT AccountId, (SELECT ContactId,Id,CreatedById,Role,isPrimary FROM OpportunityContactRoles where isPrimary = true) FROM Opportunity WHERE id =: noffer.NE__OptyId__c];
                    if(optyPrint.OpportunityContactRoles.size() > 0)
                        contactid   =   optyPrint.OpportunityContactRoles.get(0).ContactId;
                }   
                
                Task mailTask       =   new Task();
                mailTask.Priority   =   'Normal';
                mailTask.description=   'Envío PDF de la oferta desde Catálogo Comercial';
                mailTask.status     =   'Completada';   
                mailTask.Subject    =   'Correo electrónico: Envío PDF de la oferta desde Catálogo Comercial';      
                mailTask.WhatId     =   noffer.NE__OptyId__c;
                mailTask.WhoId      =   contactid;
                mailTask.OwnerId    =   noffer.OwnerId;
                mailTask.ActivityDate=  Date.today();
                insert mailTask;
                
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.NoAddrKO));
            }
            
            return null;
        }
        catch (System.CalloutException e)
        {
            System.debug('ERROR:' + e);
            return null;
        }
        
    }

    public PageReference redirect()
    {          
        //TODO, waiting for a print template
        OpenPageURL = '/apex/NEDocumentPDF?id='+orderId;
        
        return null;
    }
    
    public PageReference SaveIt()
    {  
        String message      =   '';
        
        try
        {
            NE__Order__c noffer =   [Select Name,OwnerId,NE__OptyId__c, NE__AccountId__c, NE__Version__c FROM NE__Order__c WHERE Id=:orderId limit 1];
            String nameatt      =   noffer.Name+'_version_'+noffer.NE__Version__c+'.pdf';

            Attachment mailAtt  =   new Attachment(name =nameatt);
           
            //TODO, waiting for a print template
            PageReference attachmentPDF     = new PageReference('/apex/NEDocumentPDF?id='+orderId);
    
            mailAtt.Body        = attachmentPDF.getContent();
            
            if(noffer.NE__OptyId__c == null)
                mailAtt.parentid    = orderId;
            else
                mailAtt.parentid    = noffer.NE__OptyId__c;
            
            insert mailAtt;
            message =   Label.OKSave;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, message));
        }
        catch(exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.ErrorSave));
        }
        return null;    
    }   
}