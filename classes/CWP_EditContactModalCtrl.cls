/*-------------------------------------------------------------------------------------------------------------------------------------------------------
   
    Author:        Carlos Bastidas
    Company:       Everis.com
    Description:   Class that managed of modal CWP_CustomEventDetailModal.
    
    History:
    
    <Date>            <Author>              <Description>
    17/03/2017        Carlos Bastidas       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

public without sharing class CWP_EditContactModalCtrl {
    /*Method to return the selected contact information.*/
    @AuraEnabled
    public static Contact getloadInfo (String idContact) {
        Contact cont;
        if(!String.isEmpty(idContact)) { 
           cont = [Select Name, Email, Phone, MobilePhone, BI_Activo__c, Title FROM Contact Where Id =:idContact limit 1];
        }
        return cont;
    }
    
}