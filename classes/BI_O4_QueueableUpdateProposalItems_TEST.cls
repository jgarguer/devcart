/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Test class for BI_O4_QueueableUpdateProposalItems. Updates proposal items related to local opportunities.
    Test Class:   
    
    History:
     
    <Date>            <Author>                  <Change Description>
    06/10/2016        Miguel Cabrera        	Initial version
    20/09/2017        Angel F. Santaliestra	Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_QueueableUpdateProposalItems_TEST {

    static{
        BI_TestUtils.throw_exception = false;
    }
    
    @isTest static void test_method_one() {
        
        Map<Id, Opportunity> mapNewOpps = new Map<Id, Opportunity>();
        Map<Id, Opportunity> mapOldOpps = new Map<Id, Opportunity>();

        BI_TestUtils.throw_exception=false;

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_DataLoad.set_ByPass(true, false);

        Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
        
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType IN ('Opportunity', 'Account')]){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }

        Account account = new Account(Name = 'Cliente Puerto Rico',
            RecordTypeId = MAP_NAME_RT.get(Constants.RECORD_TYPE_TGS_HOLDING),
            type = 'Prospect',
            BI_Country__c = 'Puerto Rico',
            BI_No_Identificador_fiscal__c = '2222222',
            BI_Segment__c = 'test',
            BI_Subsegment_Regional__c = 'test',
            BI_Territory__c = 'test',
            CurrencyIsoCode = 'USD');
        insert account;




        Opportunity opptyAgrup = new Opportunity(Name = 'Oportunidad Test Agrup',
            RecordTypeId = MAP_NAME_RT.get('BI_Oportunidad_Regional'),
            AccountId = account.Id,
            BI_Country__c = 'Puerto Rico',
            BI_Opportunity_Type__c = 'Digital',
            CloseDate = date.today().addDays(5),
            StageName = Label.BI_F5DefSolucion,
            BI_Probabilidad_de_exito__c = '25',
            CurrencyIsoCode = 'USD',
            BI_Duracion_del_contrato_Meses__c = 6,
            BI_Plazo_estimado_de_provision_dias__c = 5,
            BI_Recurrente_bruto_mensual_anterior__c = 12.4,
            BI_Fecha_de_vigencia__c = date.today().addDays(8),
            Amount = 5.8,
            SyncedQuoteId = null); 


        insert opptyAgrup;


        Quote quote = new Quote();  
        quote.Name = BI_O4_QuoteHelper.getNextQuoteName(opptyAgrup.Name);
        quote.OpportunityId = opptyAgrup.Id;
        quote.BI_O4_Presales_workload__c = null;
        quote.BI_O4_Proposal_scenarios__c = BI_O4_QuoteHelper.getNextQuoteScenario(opptyAgrup.Id);
        quote.Status = BI_O4_QuoteHelper.QUOTE_STATUS_GSE;
        quote.BI_O4_Countries__c = opptyAgrup.BI_O4_Service_Lines__c;
        quote.BI_O4_Service_Lines__c = opptyAgrup.BI_O4_Service_Lines__c;
        quote.BI_O4_Regional_PresalesTEXT__c = 'Regional Presales';
        quote.BI_O4_GSE__c = 'GSE Principal';
        quote.BI_O4_USER_GSE__c = UserInfo.getUserId();
        insert quote;

        Quote quote2 = new Quote();  
        quote2.Name = BI_O4_QuoteHelper.getNextQuoteName(opptyAgrup.Name);
        quote2.OpportunityId = opptyAgrup.Id;
        quote2.BI_O4_Presales_workload__c = null;
        quote2.BI_O4_Proposal_scenarios__c = BI_O4_QuoteHelper.getNextQuoteScenario(opptyAgrup.Id);
        quote2.Status = BI_O4_QuoteHelper.QUOTE_STATUS_GSE;
        quote2.BI_O4_Countries__c = opptyAgrup.BI_O4_Service_Lines__c;
        quote2.BI_O4_Service_Lines__c = opptyAgrup.BI_O4_Service_Lines__c;
        quote2.BI_O4_Regional_PresalesTEXT__c = 'Regional Presales';
        quote2.BI_O4_GSE__c = 'GSE Principal';
        quote2.BI_O4_USER_GSE__c = UserInfo.getUserId();
        insert quote2;


        BI_O4_Proposal_Item__c[] lstPi = new List<BI_O4_Proposal_Item__c>();
        BI_O4_Proposal_Item__c pi = new BI_O4_Proposal_Item__c(BI_O4_Quote__c = quote.Id, BI_O4_SubOpportunity__c = opptyAgrup.Id);
        lstPi.add(pi);

        BI_O4_Proposal_Item__c pi2 = new BI_O4_Proposal_Item__c(BI_O4_Quote__c = quote2.Id, BI_O4_SubOpportunity__c = opptyAgrup.Id);
        lstPi.add(pi2);
        Test.startTest();
        insert lstPi;


        Opportunity opptyLocal = new Opportunity(Name = 'Oportunidad Test Local',
            RecordTypeId = MAP_NAME_RT.get('BI_Oportunidad_Regional'),
            AccountId = account.Id,
            BI_Country__c = 'Puerto Rico',
            BI_Opportunity_Type__c = 'Digital',
            CloseDate = date.today().addDays(5),
            StageName = Label.BI_F5DefSolucion,
            BI_Probabilidad_de_exito__c = '25',
            CurrencyIsoCode = 'USD',
            BI_Ingreso_por_unica_vez__c = 50,
            BI_Recurrente_bruto_mensual__c = 10,
            BI_Duracion_del_contrato_Meses__c = 6,
            BI_Plazo_estimado_de_provision_dias__c = 5,
            BI_Recurrente_bruto_mensual_anterior__c = 12.4,
            BI_Fecha_de_vigencia__c = date.today().addDays(8),
            BI_Oportunidad_padre__c = opptyAgrup.Id,
            BI_O4_Bussiness_Line__c = 'Móvil',
            Amount = 5.8,
            SyncedQuoteId = quote.Id);

        insert opptyLocal;

        OpportunityTeamMember otm = new OpportunityTeamMember(OpportunityId = opptyLocal.Id, UserId = UserInfo.getUserId(), TeamMemberRole = 'LSE');
        insert otm;
        
        //Test.startTest();
        BI_O4_QueueableUpdateProposalItems upi = new BI_O4_QueueableUpdateProposalItems(lstPi);
        System.enqueueJob(upi);
        BI_O4_QueueableUpdateProposalItems upi1 = new BI_O4_QueueableUpdateProposalItems(null);
        System.enqueueJob(upi1);
        Test.stopTest();
    }   
}