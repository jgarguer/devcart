@isTest(seeAllData = false)
public class BIIN_Creacion_Ticket_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José María Martín
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_Creacion_Ticket_WS
    
    <Date>             <Author>                        <Change Description>
    11/2015            José María Martín               Initial Version
    20/05/2016         José Luis González Beltrán      Adapt TEST to UNICA interface modifications in tested class
    25/02/2017         José Ruben Suarez - Avanxo      Agregado método para probar la creación de casos de la fase 2 de BI_EN Colombia
    20/09/2017         Angel F. Santaliestra           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
    27/09/2017         Jaime Regidor                   Add the mandatory fields on account creation: BI_Subsector__c and BI_Sector__c
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
    private final static String LE_NAME = 'TestAccount_Ticket_TEST';

    @testSetup static void dataSetup()
    {
        BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
        Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert account;

        Account[] acc = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketTest = BIIN_UNICA_TestDataGenerator.createTicket(acc[0]);
        insert ticketTest;
    }

    @isTest static void test_BIIN_Creacion_Ticket_TEST() 
    {
        String authorizationToken = 'CREACION_TICKET';
        
        Blob cuerpo = Blob.valueof('Attachment test body');

        List<BIIN_Creacion_Ticket_WS.Adjunto> ladjuntos = new List<BIIN_Creacion_Ticket_WS.Adjunto>();
        BIIN_Creacion_Ticket_WS.Adjunto att1 = new BIIN_Creacion_Ticket_WS.Adjunto('Attachment1', cuerpo);
        BIIN_Creacion_Ticket_WS.Adjunto att2 = new BIIN_Creacion_Ticket_WS.Adjunto('Attachment2', cuerpo, '000002345968712');
        ladjuntos.add(att1);
        ladjuntos.add(att2);

        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketIncidenciaTecnica = [SELECT Id, BI_Id_del_caso_legado__c, AccountId, BI_Line_of_Business__c, BI_Product_Service__c, BI2_PER_Descripcion_de_producto__c, BIIN_Categorization_Tier_1__c, BIIN_Categorization_Tier_2__c, BIIN_Categorization_Tier_3__c FROM Case WHERE AccountId =: account.Id];

        Test.startTest();
            
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(ticketIncidenciaTecnica);
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicket'));
        BIIN_Creacion_Ticket_WS ceController = new BIIN_Creacion_Ticket_WS();
        ceController.crearTicket(authorizationToken, ticketIncidenciaTecnica, ladjuntos, null);

        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicketException500'));
        ceController.crearTicket(authorizationToken, ticketIncidenciaTecnica, ladjuntos, null);
            
        Test.stopTest();
    }
    //Prueba cuando ticket y cuenta son de país Colombia
    @isTest static void test_BIIN_Creacion_Ticket_Colombia_TEST() 
    {
        //Creación cuenta país Colombia
        //RECORDTYPE id = 012w0000000iT34AAE => TGS_Legal_Entity
        Account accountCOL = new Account(
            Name                               = 'cuentaCOL',
            BI_Activo__c                       = Label.BI_Si,
            BI_Country__c                      = 'Colombia',
            BI_Segment__c                      = 'Empresas',
            BI_Subsector__c                    = 'test', //27/09/2017
            BI_Sector__c                       = 'test', //27/09/2017
            BI_Subsegment_Regional__c          = 'test',
            BI_Territory__c                    = 'test',
            BI_Tipo_de_identificador_fiscal__c = 'NIT',
            BI_No_Identificador_fiscal__c      = '800000000',
            BI_Validador_Fiscal__c             = 'COL_NIT_800000000',
            RecordTypeId                       = '012w0000000iT34AAE'
        );
        insert accountCOL;
        //Creción ticket país Colombia
        //RECORDTYPE  id = 012w000000071OcAAI => BIIN_Solicitud_Incidencia_Tecnica
        Case ticketCOL = new Case(
            BI_Id_del_caso_legado__c = '00000001',
            Subject                  = 'Subject test',
            Description              = 'Description test',
            BI_Country__c            = 'Colombia',
            AccountId                =  accountCOL.Id,
            Priority                 = 'High',
            Status                   = 'Assigned',
            RecordTypeId             = '012w000000071OcAAI',
            TGS_Urgency__c           = '2-High',
            Origin                   = 'Correo electrónico',
            TGS_SLA_Light__c         = 'Green'
        );
        insert ticketCOL;
        String authorizationToken = 'CREACION_TICKET';     
        Blob cuerpo = Blob.valueof('Attachment test body');
        List<BIIN_Creacion_Ticket_WS.Adjunto> ladjuntos = new List<BIIN_Creacion_Ticket_WS.Adjunto>();
        BIIN_Creacion_Ticket_WS.Adjunto att1 = new BIIN_Creacion_Ticket_WS.Adjunto('Attachment1', cuerpo);
        BIIN_Creacion_Ticket_WS.Adjunto att2 = new BIIN_Creacion_Ticket_WS.Adjunto('Attachment2', cuerpo, '000002345968712');
        ladjuntos.add(att1);
        ladjuntos.add(att2);
        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name = 'cuentaCOL' LIMIT 1];
        Case ticketSolIncidenciaTecnica = [SELECT Id, BI_Id_del_caso_legado__c, AccountId, BI_Line_of_Business__c, BI_Product_Service__c, BI2_PER_Descripcion_de_producto__c, BIIN_Categorization_Tier_1__c, BIIN_Categorization_Tier_2__c, BIIN_Categorization_Tier_3__c FROM Case WHERE AccountId =: account.Id];

        Test.startTest();
            
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(ticketSolIncidenciaTecnica);
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicket'));
        BIIN_Creacion_Ticket_WS ceController = new BIIN_Creacion_Ticket_WS();
        ceController.crearTicket(authorizationToken, ticketSolIncidenciaTecnica, ladjuntos, null);
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicketException500'));
        ceController.crearTicket(authorizationToken, ticketSolIncidenciaTecnica, ladjuntos, null);
            
        Test.stopTest();
    }
}