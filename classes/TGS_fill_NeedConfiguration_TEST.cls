@isTest
public class TGS_fill_NeedConfiguration_TEST {
    static testMethod void fillNeedConfiguration_test(){
        
       String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
        }
            
        User userTest = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        System.runAs(userTest){
            
            Test.startTest();
            TGS_Dummy_Test_Data.dummyEndpointsTGS();
            NE__Product__c testProduct = new NE__Product__c(Name='Service');
            insert testProduct;
           
            Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
            NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId);
            insert testOrder;
            rtId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, 'Order_Management_Case');
        	TGS_Status_machine__c StatusMachine = new TGS_Status_machine__c(TGS_Needs_Configuration__c = true, TGS_Service__c = testProduct.id, TGS_Status__c = 'Assigned', TGS_Status_reason__c = 'test');
            insert StatusMachine;
            Case testCase = new Case(RecordTypeId = rtId, Subject = 'Test Case', Status = 'Assigned', TGS_Status_reason__c = 'test', Order__c = testOrder.Id, TGS_Service__c = testProduct.Name, TGS_Needs_Configuration__c = true);
        	insert testCase;
        	testOrder.Case__c = testCase.Id;
       		update testOrder;
            TGS_Status_machine__c StatusMachine2 = new TGS_Status_machine__c(TGS_Needs_Configuration__c = true, TGS_Service__c = testProduct.id, TGS_Status__c = 'Assigned', TGS_Status_reason__c = '');
            insert StatusMachine2;
                        
            testCase.Status = StatusMachine2.TGS_Status__c;
            testCase.TGS_Status_reason__c = StatusMachine2.TGS_Status_reason__c;
            testCase.TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
            update testCase;   
                
            Test.stopTest();
           
               
            
            
        }
        
        
        
    }
}