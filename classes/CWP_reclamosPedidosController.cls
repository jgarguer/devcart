/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for CWP_reclamosPedidos component
    Test Class:    CWP_reclamosPedidosController_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/05/2017              Everis                    Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

public class  CWP_reclamosPedidosController {
@auraEnabled
	public static map<string,string> getRecords(String tableControllerS){
		List<ClaimsRequestRegs> records = new List<ClaimsRequestRegs>();
		List<ClaimsRequestHeaders> recordsdHeaders = new List<ClaimsRequestHeaders>();
		String accountId= BI_AccountHelper.getCurrentAccountId();
		String nameFS = 'PCA_MainTablePedidos';
		String nameObject = 'NE__Order__c';
		String accountFieldName = 'NE__AccountId__c';
		String accountValue = accountFieldName + ';' + AccountId;
        System.debug('Account Value: ' + accountValue + ' AccountId: ' + accountId);
		FieldSetHelper.FieldSetContainer fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(nameFS, nameObject, '', accountValue, null);
        System.debug('FieldSetContainer: ' + fieldSetRecords);
        Map <String,String> ret= new Map <String,String>();
        TableController tableControl;
        String query='';
        List<Case> lCases;
        // Comprobamos si es la llamada inicial o es un cambio de parámetros 
        if (tableControllerS!='init'){
            tableControl = (CWP_reclamosPedidosController.TableController)JSON.deserialize(tableControllerS, CWP_reclamosPedidosController.TableController.class);
            tableControl.finIndex=tableControl.iniIndex+tableControl.viewSize-1;
        }else {
            tableControl= new TableController('tickets',  1,  10,  0,  10 );
        }
        for(FieldSetHelper.FieldSetRecord fieldSet : fieldSetRecords.regs){   
        	system.debug('CAMPOS: ' + fieldSet); 
		    
        	ClaimsRequestRegs record = new ClaimsRequestRegs();
        	record.id = fieldSet.id; 
        	for(FieldSetHelper.FieldSetResult result : fieldSet.values){    
        		system.debug('RESULT: ' + result);  		
        		if(result.field.equals('NE__Type__c')){
        			record.caseType = result.value;
        		}
        		if(result.field.equals('NE__Order_date__c')){
		        	record.orderDate = result.value;
        		}
        		if(result.field.equals('NE__OrderStatus__c')){
		        	record.orderStatus = result.value;
        		}
        		if(result.field.equals('Order_Code_Pedido__c')){
		        	record.orderCodePedido = result.value;
        		}      		
        	}	
        	records.add(record);
        }

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        ClaimsRequestHeaders recordHeader = new ClaimsRequestHeaders();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get('NE__Order__c');
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('PCA_MainTablePedidos');
        for (Schema.FieldSetMember fld2 : fieldSetObj.getFields()){
        	
        	if(fld2.getFieldPath().equals('NE__Type__c')){
            	recordHeader.caseType = fld2.getLabel();
        	}
        	if(fld2.getFieldPath().equals('NE__Order_date__c')){
	    		recordHeader.orderDate = fld2.getLabel();
        	}
        	if(fld2.getFieldPath().equals('NE__OrderStatus__c')){
	    		recordHeader.orderStatus = fld2.getLabel();
        	}
        	if(fld2.getFieldPath().equals('Order_Code_Pedido__c')){
	    		recordHeader.orderCodePedido = fld2.getLabel();
        	}
        }
    	recordsdHeaders.add(recordHeader);
        tableControl.numRecords=records.size();

        //Controlamos si tenemos menos registros de los que se pueden mostrar. 
        if (tableControl.numRecords < tableControl.viewSize){
            tableControl.finIndex= tableControl.numRecords;
        }
        if (tableControl.finIndex > tableControl.numRecords){
            tableControl.finIndex= tableControl.numRecords;
        }
        system.debug(records);
        
        List<ClaimsRequestRegs> recordsFilter = new List<ClaimsRequestRegs>();
        Integer inicio = tableControl.iniIndex -1;
        Integer fin = tableControl.finIndex;
        
        if (fin > tableControl.numRecords) {
        	fin = tableControl.numRecords;
        }
        
        for(Integer i=inicio;i < fin;i++){
        	recordsFilter.add(records[i]);
        }
        
        ret.put('lisTickets', JSON.serialize(recordsFilter));
        
        ret.put('offsetController', JSON.serialize(tableControl));
        
        ret.put('recordsdHeaders', JSON.serialize(recordsdHeaders));
        
        return ret; 

	}
	
	@auraEnabled
    public static  map<String,String> getFieldsValues(String tableControllerS){
        Map <String,String> ret= new Map <String,String>();
        String CASERT = Label.CWP_ticketsProfiles;
        String whereQuery;
        whereQuery=' WHERE RecordType.DeveloperName IN'+ CASERT;
        //SELECT Id, Subject, caseNumber, TGS_Product_Tier_2__c, TGS_Product_Tier_3__c, toLabel(type), status,TGS_Customer_Services__r.NE__Country__c, TGS_Customer_Services__r.NE__City__c, CWP_textDate__c, CWP_KeyValue__c FROM Case'
        ret.put('type',JSON.serialize(getOptions('type', whereQuery)));
        ret.put('TGS_Product_Tier_1__c',JSON.serialize(getOptions('TGS_Product_Tier_1__c', whereQuery)));
        List<CWP_SelectOption> options = new List<CWP_SelectOption>();
        options.add(new CWP_SelectOption(Label.CWP_FistSelect+ ' '+Label.TGS_CWP_T1, Label.CWP_FistSelect+ ' '+Label.TGS_CWP_T1));
        ret.put('TGS_Product_Tier_2__c',JSON.serialize(options));
        ret.put('TGS_Product_Tier_3__c',JSON.serialize(options));
        ret.put('status',JSON.serialize(getOptions('status', whereQuery)));
        ret.put('Country',JSON.serialize(getOptions('TGS_Customer_Services__r.NE__Country__c', whereQuery)));
        ret.put('City',JSON.serialize(getOptions('TGS_Customer_Services__r.NE__City__c', whereQuery)));
        
        
        options = new List<CWP_SelectOption>();
        options.add(new CWP_SelectOption(Label.TGS_CWP_SELOPT,Label.TGS_CWP_SELOPT));
        options.add(new CWP_SelectOption('My Requests', String.valueOf(userInfo.getUserId())));
        options.add(new CWP_SelectOption('Requires your attention','ATTREQ'));
        options.add(new CWP_SelectOption('All Requests', 'NONESELECTED'));
        
        
        ret.put('toShow',JSON.serialize(options));
        
        
        
        return ret;
        
    }
        public static  List<CWP_SelectOption> getOptions(String field, String whereQuery){
        List<CWP_SelectOption> options = new List<CWP_SelectOption>();
        String query;
        query='SELECT '+field+ ' FROM Case '+ whereQuery + 'AND '+field+' !=null GROUP BY '+field+' ORDER BY '+field ;
        System.debug('la query es -->' +query); 
        options.add(new CWP_SelectOption(Label.TGS_CWP_SELOPT,Label.TGS_CWP_SELOPT));
        
        field=(field.substringAfterLast('.')=='')?field:field.substringAfterLast('.');
        for (AggregateResult a: Database.query(query)){
            
            System.debug ('@@ AggregateResult '+a);
            
            options.add(new CWP_SelectOption(String.valueOf(a.get(field)),String.valueOf(a.get(field))));
            
        }
        if (options.size()<2){
            options.clear();
            options.add(new CWP_SelectOption(Label.CWP_NoOptions,Label.CWP_NoOptions));            
        }
        return options;
    }
    
     @auraEnabled
	public static map<string,string> getFilteredRecords(String field, String value){
		List<ClaimsRequestRegs> records = new List<ClaimsRequestRegs>();
		List<ClaimsRequestHeaders> recordsdHeaders = new List<ClaimsRequestHeaders>();
		String accountId= BI_AccountHelper.getCurrentAccountId();
		String nameFS = 'PCA_MainTablePedidos';
		String nameObject = 'NE__Order__c';
		String accountFieldName = 'NE__AccountId__c';
		String accountValue = accountFieldName + ';' + AccountId;
        String searchFinal = field + ';' + value;
		FieldSetHelper.FieldSetContainer fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(nameFS, nameObject, searchFinal , accountValue, null);
        Map <String,String> ret= new Map <String,String>();
        TableController tableControl;
        String query='';
        List<Case> lCases;
        tableControl= new TableController('tickets',  1,  10,  0,  10 );
        for(FieldSetHelper.FieldSetRecord fieldSet : fieldSetRecords.regs){
        	 
        	ClaimsRequestRegs record = new ClaimsRequestRegs();
        	record.id = fieldSet.id;
        	for(FieldSetHelper.FieldSetResult result : fieldSet.values){
        		if(result.field.equals('NE__Type__c')){
        			record.caseType = result.value;
        		}
        		if(result.field.equals('NE__Order_date__c')){
		        	record.orderDate = result.value;
        		}
        		if(result.field.equals('NE__OrderStatus__c')){
		        	record.orderStatus = result.value;
        		}
        		if(result.field.equals('Order_Code_Pedido__c')){
		        	record.orderCodePedido = result.value;
        		}
        	}	
        	records.add(record);
        }

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        ClaimsRequestHeaders recordHeader = new ClaimsRequestHeaders();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get('NE__Order__c');
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('PCA_MainTablePedidos');
        for (Schema.FieldSetMember fld2 : fieldSetObj.getFields()){
        	if(fld2.getFieldPath().equals('NE__Type__c')){
            	recordHeader.caseType = fld2.getLabel();
        	}
        	if(fld2.getFieldPath().equals('NE__Order_date__c')){
	    		recordHeader.orderDate = fld2.getLabel();
        	}
        	if(fld2.getFieldPath().equals('NE__OrderStatus__c')){
	    		recordHeader.orderStatus = fld2.getLabel();
        	}
        	if(fld2.getFieldPath().equals('Order_Code_Pedido__c')){
	    		recordHeader.orderCodePedido = fld2.getLabel();
        	}
        }
    	recordsdHeaders.add(recordHeader);
        tableControl.numRecords=records.size();

        //Controlamos si tenemos menos registros de los que se pueden mostrar. 
        if (tableControl.numRecords < tableControl.viewSize){
            tableControl.finIndex= tableControl.numRecords;
        }
        if (tableControl.finIndex > tableControl.numRecords){
            tableControl.finIndex= tableControl.numRecords;
        }
        system.debug(records);
        
        List<ClaimsRequestRegs> recordsFilter = new List<ClaimsRequestRegs>();
        Integer inicio = tableControl.iniIndex -1;
        Integer fin = tableControl.finIndex;
        
        if (fin > tableControl.numRecords) {
        	fin = tableControl.numRecords;
        }
        
        for(Integer i=inicio;i < fin;i++){
        	recordsFilter.add(records[i]);
        }
        
        ret.put('lisTickets', JSON.serialize(recordsFilter));
        
        ret.put('offsetController', JSON.serialize(tableControl));
        
        ret.put('recordsdHeaders', JSON.serialize(recordsdHeaders));
        
        return ret; 

	}
	
	  @auraEnabled
	public static map<string,string> getSortRecords(String field, String direction){
		List<ClaimsRequestRegs> records = new List<ClaimsRequestRegs>();
		List<ClaimsRequestHeaders> recordsdHeaders = new List<ClaimsRequestHeaders>();
		String accountId= BI_AccountHelper.getCurrentAccountId();
		String nameFS = 'PCA_MainTablePedidos';
		String nameObject = 'NE__Order__c';
		String accountFieldName = 'NE__AccountId__c';
		String accountValue = accountFieldName + ';' + AccountId;
        String searchFinal = field;
		FieldSetHelper.FieldSetContainer fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(nameFS, nameObject, searchFinal , accountValue, direction);
        Map <String,String> ret= new Map <String,String>();
        TableController tableControl;
        String query='';
        List<Case> lCases;
        tableControl= new TableController('tickets',  1,  10,  0,  10 );
        for(FieldSetHelper.FieldSetRecord fieldSet : fieldSetRecords.regs){
        	ClaimsRequestRegs record = new ClaimsRequestRegs();
        	record.id = fieldSet.id; 
        	for(FieldSetHelper.FieldSetResult result : fieldSet.values){
        		if(result.field.equals('NE__Type__c')){
        			record.caseType = result.value;
        		}
        		if(result.field.equals('NE__Order_date__c')){
		        	record.orderDate = result.value;
        		}
        		if(result.field.equals('NE__OrderStatus__c')){
		        	record.orderStatus = result.value;
        		}
        		if(result.field.equals('Order_Code_Pedido__c')){
		        	record.orderCodePedido = result.value;
        		}
        	}	
        	records.add(record);
        }

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        ClaimsRequestHeaders recordHeader = new ClaimsRequestHeaders();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get('NE__Order__c');
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('PCA_MainTablePedidos');
        for (Schema.FieldSetMember fld2 : fieldSetObj.getFields()){
        	if(fld2.getFieldPath().equals('NE__Type__c')){
            	recordHeader.caseType = fld2.getLabel();
        	}
        	if(fld2.getFieldPath().equals('NE__Order_date__c')){
	    		recordHeader.orderDate = fld2.getLabel();
        	}
        	if(fld2.getFieldPath().equals('NE__OrderStatus__c')){
	    		recordHeader.orderStatus = fld2.getLabel();
        	}
        	if(fld2.getFieldPath().equals('Order_Code_Pedido__c')){
	    		recordHeader.orderCodePedido = fld2.getLabel();
        	}
        }
    	recordsdHeaders.add(recordHeader);
        tableControl.numRecords=records.size();

        //Controlamos si tenemos menos registros de los que se pueden mostrar. 
        if (tableControl.numRecords < tableControl.viewSize){
            tableControl.finIndex= tableControl.numRecords;
        }
        if (tableControl.finIndex > tableControl.numRecords){
            tableControl.finIndex= tableControl.numRecords;
        }
        system.debug(records);
        
        List<ClaimsRequestRegs> recordsFilter = new List<ClaimsRequestRegs>();
        Integer inicio = tableControl.iniIndex -1;
        Integer fin = tableControl.finIndex;
        
        if (fin > tableControl.numRecords) {
        	fin = tableControl.numRecords;
        }
        
        for(Integer i=inicio;i < fin;i++){
        	recordsFilter.add(records[i]);
        }
        
        ret.put('lisTickets', JSON.serialize(recordsFilter));
        
        ret.put('offsetController', JSON.serialize(tableControl));
        
        ret.put('recordsdHeaders', JSON.serialize(recordsdHeaders));
        
        return ret; 

	}
	
	  @auraEnabled
	public static map<string,string> getRecorDetail(String recordId){
		List<ClaimsRequestDetails> details = new List<ClaimsRequestDetails>();
		Map <String,String> ret= new Map <String,String>();
		String objectName = 'NE__OrderItem__c';
		String fieldSetName = 'PCA_RelationTableLineasProductos';
		String OrderId = recordId;
		String pedidoValue = 'NE__OrderId__c;' + OrderId;
		system.debug('pedidoValue: ' +pedidoValue);
		Set<String> textValues = FieldSetHelper.textAreaFields(fieldSetName, objectName);
		FieldSetHelper.FieldSetContainer fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(fieldSetName, objectName, '', pedidoValue,null);
		
		List<FieldSetHelper.FieldSetRecord> viewRecords = new List<FieldSetHelper.FieldSetRecord>();
		for (FieldSetHelper.FieldSetRecord item : fieldSetRecords.regs){
			FieldSetHelper.FieldSetRecord iRecord = item; 
			viewRecords.add(iRecord);
		}  
		
		for(FieldSetHelper.FieldSetRecord fieldSet : viewRecords){
        	ClaimsRequestDetails detail = new ClaimsRequestDetails();
        	for(FieldSetHelper.FieldSetResult result : fieldSet.values){
        		if(result.field.equals('NE__ProdId__r.Name')){
        			detail.name = result.value;
        		}
        		if(result.field.equals('NE__Qty__c')){
		        	detail.cantidad = result.value;
        		}
        		if(result.field.equals('NE__OneTimeFeeOv__c')){
		        	detail.ingreso = result.value;
        		}
        		if(result.field.equals('NE__BaseRecurringCharge__c')){
		        	detail.recurrente = result.value;
        		}
        	}	
        	details.add(detail);
        }
		
		ret.put('detailList', JSON.serialize(details));
        return ret; 

	}
	
	
    public class TableController{
        public string table;
        public Integer iniIndex;
        public Integer finIndex;
        public Integer numRecords;
        public Integer viewSize;
        
        public  TableController(String tab, Integer iniIndex, Integer finIndex, Integer numRecords, Integer viewSize ){
            this.table=tab;
            this.iniIndex=iniIndex;
            this.finIndex=finIndex;
            this.numRecords=numRecords;
            this.viewSize=viewSize;
        }       
    }
    
    public class CWP_SelectOption {
        public string label;
        public string value;
        
        
        public  CWP_SelectOption(string label, string value){
            this.label=label;
            this.value=value;
            
        }       
    }

    public class ClaimsRequestRegs{
    	@auraEnabled
        public String caseType {get;set;}
        @auraEnabled
        public String orderDate {get;set;}
        @auraEnabled
        public String orderStatus {get;set;}
        @auraEnabled
        public String orderCodePedido {get;set;}
        @auraEnabled
        public String id {get;set;}
        
    }
    
     public class ClaimsRequestDetails{
    	@auraEnabled
        public String name {get;set;}
        @auraEnabled
        public String cantidad {get;set;}
        @auraEnabled
        public String ingreso {get;set;}
        @auraEnabled
        public String recurrente {get;set;}
    }
    
    public class ClaimsRequestHeaders{
    	@auraEnabled
        public String caseType {get;set;}
        @auraEnabled
        public String orderDate {get;set;}
        @auraEnabled
        public String orderStatus {get;set;}
        @auraEnabled
        public String orderCodePedido {get;set;}  
    }
}