public with sharing class CWP_FacturacionLightningController {
    //public static User userName {get;Set;}
        
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    
    Description:   Class to migrate "Facturación & Cobranza" to Lightning functionallity
    
    History:
    
    <Date>            <Author>              <Description>
    07/03/2017        Everis       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static  String lastModification {get;set;}
    @AuraEnabled
    public static  String lastModificationFact {get;set;}
    
    public static  String nameFS;
    public static  String nameObject                    {get; set;}
    public static  String accountFieldName;
    
    public static  String nameFS1;
    public static  String nameObject1                   {get; set;}
    public static  String accountFieldName1;
    
    public static  String nameFS2;
    public static  String nameObject2                   {get; set;}
    public static  String accountFieldName2;
    
    public static  String searchFinal;
    public static  map<string,string>  mapVisibility {get;set;}
    public static  Id AccountId;
    
    public static  String searchText                    {get; set;}
    public static  String searchField                   {get; set;}    
    
    @AuraEnabled
    public static  Integer totalRegs                    {get; set;}    
    
    public static  List<SelectOption> searchFields      {get; set;}
    
    public static  List<FieldSetHelper.FieldSetRecord> viewRecords  {get; set;}
    public static  FieldSetHelper.FieldSetContainer fieldSetRecords {get; set;}
    
    
    //public static  String Field;    
    /*
    public static  String PageValue {get;set;}
    */    
    @AuraEnabled
    public static  String tabActive                    {get; set;}
        
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Constructor
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public CWP_FacturacionLightningController(){}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        
    Company:       
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>              <Description>    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    */
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info of facturas and cobranzas
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    29/08/2016        Jose Miguel Fierro   Separar la comprobación del Estado de Deudas de la comprobación del estado de Facturación
    07/03/2017        Everis               Método loadInfo() replicado de la clase PCA_FacturacionCobranza.Ctrl para realizar la carga de datos iniciales
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled    
    public static void loadInfo (string auxTabActive){      
    
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }            
            
            tabActive = auxTabActive;
                       
            searchFinal = null;        
            AccountId = BI_AccountHelper.getCurrentAccountId();
            if(AccountId == null){
                return;
            }
            string countryAccount = BI_AccountHelper.getCountryAccount(AccountId);            
            
            system.debug('loadInfo - tabActive:' + tabActive);
            
            if (tabActive == 'tab1'){
                
                List<BI_Facturas__c> FactDate = [Select Id, BI_Fecha_de_carga__c FROM BI_Facturas__c where BI_Fecha_de_carga__c != null AND BI_Nombre_del_cliente__c = :BI_AccountHelper.getCurrentAccountId() ORDER BY BI_Fecha_de_carga__c DESC LIMIT 1];
                system.debug('loadInfo - FactDate: ' + FactDate);
                if(!FactDate.isEmpty()){
                    lastModificationFact = FactDate[0].BI_Fecha_de_carga__c.format(); //Last modified Facturas
                    system.debug('loadInfo - lastModificationFact: ' + lastModificationFact);
                }
                nameObject = 'BI_Facturas__c';
                nameFS = 'PCA_MainTableFacturas';
                accountFieldName = 'BI_Nombre_del_cliente__c';
                    
            }else if (tabActive == 'tab2'){
                
                List<BI_Facturacion__c> FactDate2 = [Select Id, BI_Fecha_de_carga__c FROM BI_Facturacion__c where BI_Fecha_de_carga__c != null AND BI_Cliente__c = :BI_AccountHelper.getCurrentAccountId() ORDER BY BI_Fecha_de_carga__c DESC LIMIT 1];
                system.debug('loadInfo - FactDate2: ' + FactDate2);
                if(!FactDate2.isEmpty()) {
                    lastModification = FactDate2[0].BI_Fecha_de_carga__c.format(); //Last modified Estado de deuda
                    system.debug('loadInfo - lastModification: ' + lastModification);
                }
                nameObject = 'BI_Facturacion__c';
                nameFS = 'PCA_MainTableFacturacion';
                accountFieldName = 'BI_Cliente__c';
                    
            }

            system.debug('loadInfo - nameObject: ' + nameObject );
            system.debug('loadInfo - nameObject: ' + nameFS );
            system.debug('loadInfo - accountFieldName: ' + accountFieldName );                        
                        
            mapVisibility = PCA_ProfileHelper.getPermissionSet();            
            
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.loadInfo', 'Portal Platino', Exc, 'Class');
        }
    }    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines search records
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    07/03/2017        Everis               Método defineRecords() replicado de la clase PCA_FacturacionCobranza.Ctrl
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static list<String> defineRecords (String auxSearchResult, String auxSearchText){       
                
        try{                    
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }            
            
            String accountValue = accountFieldName + ';' + AccountId;            
            
            if ((auxSearchResult == null) || (auxSearchText == null)){
                searchFinal = null;
            }else{
                searchFinal = auxSearchResult + ';' + auxSearchText;
            }
                                    
            system.debug('defineRecords - searchFinal: ' + searchFinal);
            fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(nameFS, nameObject, searchFinal, accountValue,null);
            system.debug('defineRecords - fieldSetRecords: ' + fieldSetRecords);
            
            totalRegs = fieldSetRecords.regs.size();
            system.debug('defineRecords - totalRegs: ' + totalRegs);            
            
            List<String> auxiliarHeader = new List<String>();
            
            if (fieldSetRecords.regs.size() > 0)
            {               
                Map<String, Schema.SObjectType> GlobalMap = Schema.getGlobalDescribe();             
                Schema.DescribeSObjectResult obj = GlobalMap.get(nameObject).getDescribe();                         
                
                for (FieldSetHelper.FieldSetResult i: fieldSetRecords.regs[0].values){
                    Schema.DescribeFieldResult TestField = obj.Fields.getMap().get(i.field).getDescribe();                  
                    auxiliarHeader.add(TestField.getLabel());   
                }
            }     
                
            return auxiliarHeader;
            
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.defineRecords', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
   
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines search views
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    07/03/2017        Everis               Método defineViews() replicado de la clase PCA_FacturacionCobranza.Ctrl
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    
    @auraEnabled
    public static String defineViews(){        
       
                                             
            viewRecords = new List<FieldSetHelper.FieldSetRecord>();            
            
            for (Integer i = 0; i < totalRegs; i++) {
                FieldSetHelper.FieldSetRecord iRecord = fieldSetRecords.regs[i]; 
                viewRecords.add(iRecord);
            }
            system.debug('defineViews - JSON.serialize(viewRecords): ' + JSON.serialize(viewRecords));
            return JSON.serialize(viewRecords);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines search criteria
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    07/03/2017        Everis               Método defineViews() replicado de la clase PCA_FacturacionCobranza.Ctrl
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
        
    @auraEnabled
    public static List<customOption> defineSearch(){        
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
        searchFields = FieldSetHelper.defineSearchFields(nameFS, nameObject);                
        
        List<customOption> customOptions = new List<customOption>();        
        
        for(SelectOption f : searchFields){
            system.debug('label: ' + f.getLabel());
            system.debug('value: ' + f.getValue());
            customOptions.add(new customOption(String.valueOf(f.getLabel()), String.valueOf(f.getValue())));
        }   
        system.debug('customOptions: ' + customOptions);
        return customOptions;
        
        
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.defineSearch', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }    
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Get the last modification date in Debt Tab
    
    History:
    
    <Date>            <Author>              <Description>    
    07/03/2017        Everis               
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static String getLastModification(){
                 
            return lastModification;
                  
    }
    
        
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Get the last modification date in Account Status Tab 
    
    History:
    
    <Date>            <Author>              <Description>    
    07/03/2017        Everis               
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static String getLastModificationFact(){
                  
            return lastModificationFact;
              
    }    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Set the active Tab selected 
    
    History:
    
    <Date>            <Author>              <Description>    
    07/03/2017        Everis               
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static string setTabActive(string auxTab){
       
        tabActive = auxTab;
        system.debug('setTabActive - tabActive: '+ tabActive);  
        return tabActive;      
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Create Task for User
    
    History:
    
    <Date>            <Author>              <Description>
    18/06/2015        Antonio Moruno       Initial version
    07/12/2015        Guillermo Muñoz      Prevent the email send in Test class
    07/03/2017        Everis               Método createDebtTask() replicado de la clase PCA_Cobranza_PopUpDetail.Ctrl     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static boolean createDebtTask (Id FacturaId){
        try{
            if(BI_TestUtils.isRunningTest()){
              throw new BI_Exception('test');
            }
            system.debug('createDebtTask - FacturaId: ' + FacturaId);
            //Id FacturaId = (apexpages.currentpage().getparameters().get('id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('id')):null;
            List<BI_Facturas__c> factsToTask = [Select Id, Name FROM BI_Facturas__c where id = :FacturaId];
            Task newTask = new Task();
            if(!factsToTask.IsEmpty()){
                newTask.Subject = 'Factura '+factsToTask[0].Name;
            }else{
                newTask.Subject = 'Factura';
            }
            Id accId = BI_AccountHelper.getCurrentAccountId();
            newTask.OwnerId = BI_AccountHelper.getCurrentAccount(accId).Owner.Id;
            List<BI_Contact_Customer_Portal__c> CCP = [SELECT BI_Contacto__c FROM BI_Contact_Customer_Portal__c where BI_User__c =: Userinfo.getUserId() AND BI_Cliente__c=: BI_AccountHelper.getCurrentAccountId() LIMIT 1];
            
            if(!CCP.IsEmpty()){                
                newTask.WhoId = CCP[0].BI_Contacto__c;
                system.debug('WhoId:' + newTask.WhoId);
            }
            system.debug('CCP** :' +CCP);
            if(FacturaId!=null){
                newTask.WhatId = FacturaId;
            } 
            newTask.Status = Label.BI_NoIniciada;
            newTask.Priority = 'Normal';
            
            system.debug('newTask****: ' + newTask);
            Database.Saveresult res = Database.insert(newTask,false);
            system.debug('result****: ' + res);
            List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject, DeveloperName, Body from EmailTemplate where DeveloperName='PCA_comunicacion_nueva_tarea'];            
            
            if(!templates.isEmpty()){
                List<Messaging.SingleEmailMessage> emailSend = new List<Messaging.SingleEmailMessage>();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                mail.setTargetObjectId(newTask.OwnerId);
                string body = templates[0].Body;
                system.debug('body: '+body);
                body = body.replace('{!Task.Subject}', newTask.Subject);
                body = body.replace('{!Task.Status}', newTask.Status);
                body = body.replace('{!Task.Priority}', newTask.Priority);            
                mail.setSubject(templates[0].Subject);
                mail.setPlainTextBody(body);
                mail.setSaveAsActivity(false);
                emailSend.add(mail);
                if(!Test.isRunningTest()){
                    Messaging.sendEmail(emailSend);
                }
            }
            ConnectApi.ChatterMessage Msg = ConnectAPI.ChatterMessages.sendMessage('El usuario '+UserInfo.getName() +' ha pedido información acerca de la factura '+factsToTask[0].Name+'. ', newTask.OwnerId);
            system.debug('createDebtTask - END');
            return true;
        }
        catch (exception Exc){
            system.debug('error');
            BI_LogHelper.generate_BILog('PCA_CatalogoProdController.createTask', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }    
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Create Task for User
    
    History:
    
    <Date>            <Author>              <Description>
    18/06/2015        Antonio Moruno       Initial version
    07/12/2015        Guillermo Muñoz      Prevent the email send in Test class
    07/03/2017        Everis               Método createAccountStatusTask() replicado de la clase PCA_Facturacion_PopUpDetail.Ctrl 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static boolean createAccountStatusTask (Id FacturacionId){    
        try{
            if(BI_TestUtils.isRunningTest()){
              throw new BI_Exception('test');
            }
            system.debug('createAccountStatusTask - FacturacionId: ' + FacturacionId);            
            //Id FacturacionId = (apexpages.currentpage().getparameters().get('id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('id')):null;
            List<BI_Facturacion__c> factusToTask = [Select Id, Name FROM BI_Facturacion__c where id = :FacturacionId];
            
            Task newTask = new Task();
            
            if(!factusToTask.IsEmpty()){
                newTask.Subject = 'Estado de deuda '+factusToTask[0].Name;
            }else{
                newTask.Subject = 'Estado de deuda';
            }
            
            Id accId = BI_AccountHelper.getCurrentAccountId();
            newTask.OwnerId = BI_AccountHelper.getCurrentAccount(accId).Owner.Id;
            
            List<BI_Contact_Customer_Portal__c> CCP = [SELECT BI_Contacto__c FROM BI_Contact_Customer_Portal__c where BI_User__c =: Userinfo.getUserId() AND BI_Cliente__c=: BI_AccountHelper.getCurrentAccountId() LIMIT 1];
            
            if(!CCP.IsEmpty()){                
                newTask.WhoId = CCP[0].BI_Contacto__c;
                system.debug('WhoId:' + newTask.WhoId);
            }
            
            system.debug('CCP** :' +CCP);
            if(FacturacionId!=null){
                newTask.WhatId = FacturacionId;
            }
             
            newTask.Status = Label.BI_NoIniciada;
            newTask.Priority = 'Normal';
            //newTask.IsReminderSet = true;
            system.debug('newTask****: ' + newTask);
            Database.Saveresult res = Database.insert(newTask,false);
            system.debug('result****: ' + res);
            List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject, DeveloperName, Body from EmailTemplate where DeveloperName='PCA_comunicacion_nueva_tarea'];
                        
            if(!templates.isEmpty()){
                List<Messaging.SingleEmailMessage> emailSend = new List<Messaging.SingleEmailMessage>();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();                
                mail.setTargetObjectId(newTask.OwnerId);
                string body = templates[0].Body;
                system.debug('body: '+body);
                body = body.replace('{!Task.Subject}', newTask.Subject);
                body = body.replace('{!Task.Status}', newTask.Status);
                body = body.replace('{!Task.Priority}', newTask.Priority);                
                mail.setSubject(templates[0].Subject);
                mail.setPlainTextBody(body);
                mail.setSaveAsActivity(false);
                emailSend.add(mail);                
                
                if(!Test.isRunningTest()){
                    Messaging.sendEmail(emailSend);
                }
                system.debug('createAccountStatusTask - END');
            }
            ConnectApi.ChatterMessage Msg = ConnectAPI.ChatterMessages.sendMessage('El usuario '+UserInfo.getName() +' ha pedido información acerca del estado de deuda '+factusToTask[0].Name+'. ', newTask.OwnerId);
            return true;
        }
        catch (exception Exc){
            system.debug('error');            
            BI_LogHelper.generate_BILog('PCA_CatalogoProdController.createTask', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }   
    
        
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Method for manage the dropdown list 
    
    History:
    
    <Date>            <Author>              <Description>    
    07/03/2017        Everis                
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public class customOption{
        @auraEnabled
        String label {get;set;}
        @auraEnabled
        String value {get;set;}
        
        public customOption(String l, String v){
            label = l;
            value = v;
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Order table with field
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    09/03/2017        Everis               Método Order() replicado de la clase PCA_FacturacionCobranzaCtrl.Ctrl
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    @auraEnabled
    public static void Order(string auxFieldOrder, string auxMode){        
        try{          
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }                        
            
            System.debug('Method Order - auxMode: ' + auxMode);
            //FieldOrder = auxFieldOrder;
            System.debug('Method Order - FieldOrder: ' + auxFieldOrder);
            String accountValue = accountFieldName + ';' + AccountId;
            System.debug('Method Order - accountValue: ' + accountValue);
                        
            
            fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(nameFS, nameObject, auxFieldOrder, accountValue, auxMode);                       
            System.debug('Method Order - accountValue: ' + accountValue);
            system.debug('defineViews - JSON.serialize(fieldSetRecords): ' + JSON.serialize(fieldSetRecords)); 
             
            totalRegs = fieldSetRecords.regs.size();
            System.debug('Method Order - totalRegs: ' + totalRegs);                       
            
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.Order','Portal Platino', Exc, 'Class');            
        }
        
    }
    
}