/*-------------------------------------------------------------------
	Author:         Virgilio Utrera
	Company:        Salesforce.com
	Description:    Custom controller for the FOCO Revenue Goal Chart
	Test Class:     BI_FOCOAmountVsGoalChart_TEST
	History
	<Date>          <Author>           <Change Description>
	26-Sep-2014     Virgilio Utrera    Initial Version
	29-Sep-2014     Virgilio Utrera    Modified getChartData
	01-Oct-2014     Virgilio Utrera    Modified constructor and getChartData
	14-Oct-2014     Virgilio Utrera    Added amount conversion to user's default currency
	15-Oct-2014     Virgilio Utrera    Modified methods to take into account visibility rules
	20-Oct-2014     Virgilio Utrera    Moved queries to BI_FOCOUtil class and added currency conversion
	11-Mar-2015		Virgilio Utrera    Refactored getChartData to handle large data volumes
-------------------------------------------------------------------*/

public class BI_FOCOAmountVsGoalChart_CTRL {

	public transient Boolean renderErrorMessage { get; set; }
	public transient String chartData { get; set; }
	public transient String errorMessage { get; set; }
	public transient String userDefaultCurrency { get; set; }

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Class constructor
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		26-Sep-2014     Virgilio Utrera    Initial Version
		01-Oct-2014     Virgilio Utrera    Added error message display
	-------------------------------------------------------------------*/

	public BI_FOCOAmountVsGoalChart_CTRL() {
		errorMessage = '';
		chartData = '';
		getChartData();

		if(errorMessage.length() > 0)
			renderErrorMessage = true;
		else
			renderErrorMessage = false;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Gets the chart data for the running user
		IN:
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		25-Sep-2014     Virgilio Utrera    Initial Version
		29-Sep-2014     Virgilio Utrera    Added chart totals calculation
		01-Oct-2014     Virgilio Utrera    Refactored method and added exception handling
		14-Oct-2014     Virgilio Utrera    Added amount conversion to user's default currency
		15-Oct-2014     Virgilio Utrera    Modified queries to take into account visibility rules
		20-Oct-2014     Virgilio Utrera    Moved queries to BI_FOCOUtil class and added currency conversion
		11-Mar-2015		Virgilio Utrera    Refactored method to handle large data volumes
	-------------------------------------------------------------------*/

	private void getChartData() {
		Decimal amount;
		Decimal gap;
		Decimal goal;
		Decimal totalAmount;
		Decimal totalGoal;
		Id userId;
		List<AggregateResult> focoResults = new List<AggregateResult>();
		List<BI_Objetivo_Comercial__c> goalResults = new List<BI_Objetivo_Comercial__c>();
		List<CurrencyType> currenciesList = new List<CurrencyType>();
		Map<Integer, Decimal> amountsMap = new Map<Integer, Decimal>();
		Map<Integer, Decimal> gapsMap = new Map<Integer, Decimal>();
		Map<Integer, Decimal> goalsMap = new Map<Integer, Decimal>();
		Map<Integer, String> monthsMap = new Map<Integer, String>();
		Map<String, Double> conversionRatesMap = new Map<String, Double>();
		Set<Id> subordinateIds;
		String corporateCurrency;

		// Build months map. This map is used to output month names in the VF Page
		monthsMap.put(1, 'Enero');
		monthsMap.put(2, 'Febrero');
		monthsMap.put(3, 'Marzo');
		monthsMap.put(4, 'Abril');
		monthsMap.put(5, 'Mayo');
		monthsMap.put(6, 'Junio');
		monthsMap.put(7, 'Julio');
		monthsMap.put(8, 'Agosto');
		monthsMap.put(9, 'Septiembre');
		monthsMap.put(10, 'Octubre');
		monthsMap.put(11, 'Noviembre');
		monthsMap.put(12, 'Diciembre');

		amount = 0;
		goal = 0;
		totalAmount = 0;
		totalGoal = 0;
		userId = UserInfo.getUserId();
		userDefaultCurrency = UserInfo.getDefaultCurrency();
		subordinateIds = BI_FOCOUtil.getRoleSubordinateUsers(userId);
		currenciesList = BI_FOCOUtil.getCurrenciesList();

		// Fills up map with conversion rates, sets the currency corporate code
		for(CurrencyType currencyType : currenciesList) {
			conversionRatesMap.put(currencyType.IsoCode, currencyType.ConversionRate);

			if(currencyType.IsCorporate)
				corporateCurrency = currencyType.IsoCode;
		}

		// Get Ids from all subordinate users
		subordinateIds = BI_FOCOUtil.getRoleSubordinateUsers(userId);

		if(!subordinateIds.isEmpty())
			// There are subordinates, query FOCO records from user and subordinates for current year
			focoResults = BI_FOCOUtil.getCurrentYearFOCOAmountsByPeriod(userId, subordinateIds);
		else
			// There are no subordinates, query FOCO records from user for current year
			focoResults = BI_FOCOUtil.getCurrentYearFOCOAmountsByPeriod(userId, null);

		// Stop the execution if there are no FOCO records to show
		if(focoResults.isEmpty()) {
			errorMessage = 'No se han encontrado Registros de Datos FOCO para el año en curso';
			System.debug(errorMessage);
			return;
		}

		if(!subordinateIds.isEmpty())
			// There are subordinates, query commercial goals from user and subordinates for current year
			goalResults =  BI_FOCOUtil.getRevenueGoalsFromCurrentYear(userId, subordinateIds);
		else
			// There are no subordinates, query commercial goals from user for current year
			goalResults =  BI_FOCOUtil.getRevenueGoalsFromCurrentYear(userId, null);

		// Stop the execution if there are no commercial goal records to show
		if(goalResults.isEmpty() || goalResults[0].BI_Objetivo__c == null) {    
			errorMessage = 'No se han encontrado Objetivos Comerciales para el año en curso';
			System.debug(errorMessage);
			return;
		}

		// Populate amounts map
		for(AggregateResult ar : focoResults) {
			// FOCO results' BI_Monto__c is automatically returned in the org's currency because the query contains a SUM() aggregate function with a GROUP BY clause
			if(userDefaultCurrency == corporateCurrency)
				// No currency conversion needed
				amount = ((Decimal)ar.get('BI_Monto__c')).setScale(2, System.RoundingMode.CEILING);
			else
				// Convert amount into user's currency
				amount = (((Decimal)ar.get('BI_Monto__c')) * conversionRatesMap.get(userDefaultCurrency)).setScale(2, System.RoundingMode.CEILING);

			if((Date)ar.get('BI_Fecha_Inicio_Periodo__c') != null) {
				if(amountsMap.get(((Date)ar.get('BI_Fecha_Inicio_Periodo__c')).month()) != null)
					amountsMap.put(((Date)ar.get('BI_Fecha_Inicio_Periodo__c')).month(), amountsMap.get(((Date)ar.get('BI_Fecha_Inicio_Periodo__c')).month()) + amount);
				else
					amountsMap.put(((Date)ar.get('BI_Fecha_Inicio_Periodo__c')).month(), amount);
			}

			totalAmount = totalAmount + amount;
		}

		// Populate goals map
		for(BI_Objetivo_Comercial__c oc : goalResults) {
			if(oc.CurrencyIsoCode == userDefaultCurrency)
				// Currency is the same as user's currency
				goal = oc.BI_Objetivo__c.setScale(2, System.RoundingMode.CEILING);
			else {
				if(oc.CurrencyIsoCode == corporateCurrency)
					// Currency is corporate currency, amount is converted into user's default currency
					goal = (oc.BI_Objetivo__c * conversionRatesMap.get(userDefaultCurrency)).setScale(2, System.RoundingMode.CEILING);
				else
					// Currency is other than user's or corporate
					goal = ((oc.BI_Objetivo__c / conversionRatesMap.get(oc.CurrencyIsoCode)) * conversionRatesMap.get(userDefaultCurrency)).setScale(2, System.RoundingMode.CEILING);
			}

			if(goalsMap.get(oc.BI_Periodo__r.BI_Fecha_Inicio_Periodo__c.month()) != null)
				goalsMap.put(oc.BI_Periodo__r.BI_Fecha_Inicio_Periodo__c.month(), goalsMap.get(oc.BI_Periodo__r.BI_Fecha_Inicio_Periodo__c.month()) + goal);
			else
				goalsMap.put(oc.BI_Periodo__r.BI_Fecha_Inicio_Periodo__c.month(), goal);

			totalGoal = totalGoal + goal;
		}

		// Populate gaps map
		for(Integer month : amountsMap.keySet()) {
			if(goalsMap.get(month) != null)
				gapsMap.put(month, goalsMap.get(month) - amountsMap.get(month));
			else
				gapsMap.put(month, (0 - amountsMap.get(month)));
		}

		// Build the chart data for each month
		chartData = '[';

		for(Integer i = 1; i <= 12; i++) {
			amount = 0;
			goal = 0;
			gap = 0;

			if(amountsMap.get(i) != null) {
				amount = amountsMap.get(i);

				if(goalsMap.get(i) != null) {
					goal = goalsMap.get(i);
					gap = goal - amount;
				}
				else
					gap = 0 - amount;
			}
			else {
				if(goalsMap.get(i) != null) {
					goal = goalsMap.get(i);
					gap = goalsMap.get(i);
				}
			}

			chartData = chartData + '[\'' + monthsMap.get(i) + '\', ' + amount + ', ' + goal + ', ' + gap + '], ';
		}

		chartData = chartData + '[\'Total\', ' + totalAmount + ', ' + totalGoal + ', ' + String.valueOf(totalGoal - totalAmount) + ']]';
	}
}