@isTest
private class BI_User_JOB_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_User_JOB class 
    
    History: 
    
    <Date> 					<Author> 				<Change Description>
    28-04-14    			Pablo Oliva				Initial Version
    08/06/2015				Fernando Arteaga		Added method BI_User_JOB_Exceute_Exception_TEST
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Micah Burgos
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_User_JOB
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	23-07-2014      		Micah Burgos			Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void BI_User_JOB_Exceute_TEST() {
        ////////////////////////////////////
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
                    Integer K = 2;
        ///////////////////////////////////
    	List<User> lst_usr = BI_DataLoad.loadUsers(K, BI_DataLoad.searchAdminProfile(), Label.BI_Administrador_del_sistema);
    	system.assertEquals(K , lst_usr.size());
        
        Test.startTest();
    	Set<Id> set_usersId = new Set<Id>();

    	for(User usr :lst_usr){
    		System.resetPassword(usr.Id, false);
            set_usersId.add(usr.Id);

    	}

    	
		BI_Jobs.updateUsers(null, false);
		

		lst_usr = [SELECT Id, Name, LastPasswordChangeDate, BI_ContadorReset__c, BI_LastPasswordChange__c FROM User WHERE Id IN :set_usersId];


		
        Test.stopTest();

        //I Can´t assert because Test.stopTest() don´t wait rerun of BI_Jobs.
        /*for(User usr :lst_usr){
            system.assertEquals(usr.LastPasswordChangeDate, usr.BI_LastPasswordChange__c);
            system.assertEquals(1, usr.BI_ContadorReset__c);
        }*/
    }

    static testMethod void BI_User_JOB_Exceute_Exception_TEST()
    {
    	try
    	{
    		BI_Jobs.updateUsers(null, null);
    	}
    	catch (Exception e)
    	{
    		System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
    	}
    	
		try
		{
			List<User> users = [SELECT Id, BI_LastPasswordChange__c, BI_ContadorReset__c, LastPasswordChangeDate FROM User WHERE IsActive = true and Pais__c <> null and BI_Permisos__c <> null ORDER BY Id ASC limit 3];
    		if (users.size() >= 2)
    		{
    			users[1].BI_ContadorReset__c = null;
    			update users[1];
    			BI_Jobs.updateUsers(users[0].Id, false);
    		}
		}
    	catch (Exception e)
    	{
    		
    	}
    }
    

	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}