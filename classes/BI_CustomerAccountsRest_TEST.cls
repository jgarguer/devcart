@isTest
private class BI_CustomerAccountsRest_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_CustomerAccountsRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    16/09/2014              Pablo Oliva             Initial Version
    27/05/2016              Guillermo Muñoz         BI_TestUtils.throw_exception set to false to avoid test errors
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static{
        BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CustomerAccountsRest.getAccounts()
    History:
    
    <Date>              <Author>                <Description>
    16/09/2014          Pablo Oliva             Initial version
    27/05/2016          Guillermo Muñoz         Changed BI_TestUtils.throw_exception values caused by changes in the global value
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getAccountsTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');               							   			 
        
        List <Account> lst_cus = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsForCustomer(lst_cus[0], lst_region, 100);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/customerinfo/v1/customers/'+lst_cus[0].BI_Id_del_cliente__c+'/accounts';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

        BI_TestUtils.throw_exception = true;
        
        //INTERNAL_SERVER_ERROR
        BI_CustomerAccountsRest.getAccounts();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        //BAD REQUEST
        BI_TestUtils.throw_exception = false;
        BI_CustomerAccountsRest.getAccounts();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //NOT FOUND
        req.requestURI = '/customerinfo/v1/customers/test/accounts';

        RestContext.request = req;
        RestContext.response = res;
        
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_CustomerAccountsRest.getAccounts();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/customerinfo/v1/customers/'+lst_cus[0].BI_Id_del_cliente__c+'/accounts'; 
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_CustomerAccountsRest.getAccounts();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CustomerAccountsRest.updateAccounts()
    History:
    
    <Date>              <Author>                <Description>
    16/09/2014          Pablo Oliva             Initial version
    27/05/2016          Guillermo Muñoz         Changed BI_TestUtils.throw_exception values caused by changes in the global value
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void updateAccountsTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');                 							   			 
        
        List <Account> lst_cus = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsForCustomer(lst_cus[0], lst_region, 1);
        
        lst_acc[0].ParentId = null;
        update lst_acc[0];
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/customerinfo/v1/customers/'+lst_cus[0].BI_Id_del_cliente__c+'/accounts/aaa';  
        req.httpMethod = 'PUT';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        BI_TestUtils.throw_exception = true;

        //BAD REQUEST
        BI_CustomerAccountsRest.updateAccounts();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //INTERNAL_SERVER_ERROR
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_CustomerAccountsRest.updateAccounts();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        //NOT_FOUND
        BI_TestUtils.throw_exception = false;
        BI_CustomerAccountsRest.updateAccounts();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/customerinfo/v1/customers/'+lst_cus[0].BI_Id_del_cliente__c+'/accounts/'+lst_acc[0].BI_Id_del_cliente__c; 
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_CustomerAccountsRest.updateAccounts();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CustomerAccountsRest.deleteAccounts()
    History:
    
    <Date>              <Author>                <Description>
    16/09/2014          Pablo Oliva             Initial version
    27/05/2016          Guillermo Muñoz         Changed BI_TestUtils.throw_exception values caused by changes in the global value
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void deleteAccountsTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');        							   			 
        
        List <Account> lst_cus = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsForCustomer(lst_cus[0], lst_region, 1);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/customerinfo/v1/customers/'+lst_cus[0].BI_Id_del_cliente__c+'/accounts/aaa';  
        req.httpMethod = 'DELETE';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

        BI_TestUtils.throw_exception = true;
        
        //BAD REQUEST
        BI_CustomerAccountsRest.deleteAccounts();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //INTERNAL_SERVER_ERROR
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_CustomerAccountsRest.deleteAccounts();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        //NOT_FOUND
        BI_TestUtils.throw_exception = false;
        BI_CustomerAccountsRest.deleteAccounts();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/customerinfo/v1/customers/'+lst_cus[0].BI_Id_del_cliente__c+'/accounts/'+lst_acc[0].BI_Id_del_cliente__c; 
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_CustomerAccountsRest.deleteAccounts();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}