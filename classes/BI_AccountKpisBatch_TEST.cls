@isTest
public with sharing class BI_AccountKpisBatch_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Alberto Fernández
Company:       Accenture
Description:   Test class to manage the code coverage for BI_AccountKpisBatch class

History: 

<Date>                  <Author>                <Change Description>
28/02/2017              Alberto Fernández        Initial version
23/03/2017				Alberto Fernández		 Trigger Bypass activated
18/08/2017				Guillermo Muñoz			 Changed to use new BI_MigrationHelper functionality
--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	
	@isTest
	public static void accountKpisBatch_TEST (){
		
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
		userTGS.TGS_Is_BI_EN__c = true;
		insert userTGS;

		// 23/01/2017 se comento la validacion de Bypass para este casos
        /*Map<Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(Userinfo.getUserId(), true, true, true, true);
	    BI_bypass__c bypass = new BI_bypass__c(BI_skip_trigger__c = true);
	    insert bypass;*/
	    BI_MigrationHelper.skipAllTriggers();

        //try{
			Account testAccount = new Account(Name = 'testAcc', BI_Country__c = 'Argentina',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
      		insert testAccount;    

      		DateTime dtNow = DateTime.now();
		    Test.startTest();
		    Opportunity testOpp = new Opportunity(BI_No_Identificador_fiscal__c = '00000000C',
		                                            Name = 'testOpp', 
		                                            BI_Country__c = 'Argentina', 
		                                            BI_SIMP_Opportunity_Type__c = 'Alta', 
		                                            CloseDate = Date.today(), 
		                                            BI_Probabilidad_de_exito__c = '100', 
		                                            CurrencyIsoCode = 'EUR', 
		                                            BI_Duracion_del_contrato_Meses__c = 12, 
		                                            BI_Plazo_estimado_de_provision_dias__c = 90, 
		                                            StageName = Label.BI_DefinicionSolucion,
		                                            AccountId = testAccount.Id);
		   	insert testOpp;  

		   	Opportunity testOpp3 = new Opportunity(BI_No_Identificador_fiscal__c = '00000000L',
		                                            Name = 'testOpp3', 
		                                            BI_Country__c = 'Argentina', 
		                                            BI_SIMP_Opportunity_Type__c = 'Alta', 
		                                            CloseDate = Date.today(),
		                                            BI_Fecha_de_cierre_real__c = Date.today(), //JEG
		                                            BI_Probabilidad_de_exito__c = '100', 
		                                            CurrencyIsoCode = 'EUR', 
		                                            BI_Duracion_del_contrato_Meses__c = 12, 
		                                            BI_Plazo_estimado_de_provision_dias__c = 90, 
		                                            StageName = Label.BI_F1Ganada,
		                                            AccountId = testAccount.Id);
		   	insert testOpp3; 

		   	Event testEvent = new Event(Subject = 'Visita a Cliente', 
		                                  Description = 'test description', 
		                                  BI_Temas_tratados__c = 'test',
		                                  WhatId = testAccount.Id,
		                                  BI_FVI_Estado__c = 'Cualificada',
		                                  StartDateTime = dtNow, 
		                                  EndDateTime = dtNow, 
		                                  BI_FVI_FechaHoraFinVisita__c = dtNow.date());

		    insert testEvent;  

		    NE__Order__c ord = new NE__Order__c(NE__AccountId__c = testAccount.Id,
                                                NE__BillAccId__c = testAccount.Id,
                                                NE__OptyId__c = testOpp.Id,
                                                NE__AssetEnterpriseId__c='testEnterprise',
                                                NE__ServAccId__c = testAccount.Id,
                                                NE__OrderStatus__c = 'Active',
                                                CurrencyIsoCode = 'ARS');
	        insert ord;
	        System.debug('updateValPresupuestoOrder test ord'+ ord.Id);

	        NE__Product__c pord = new NE__Product__c();
	         pord.Offer_SubFamily__c = 'a-test;';
	         pord.Offer_Family__c = 'b-test';
	         pord.BI_Rama_Digital__c = 'asdfasdf';
	        
	        insert pord; 

	        NE__Catalog__c cat = new NE__Catalog__c();
	        insert cat;
	        NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(
	            NE__Catalog_Id__c = cat.Id, 
	            NE__ProductId__c = pord.Id, 
	            NE__Change_Subtype__c = 'test', 
	            NE__Disconnect_Subtype__c = 'test');
	        insert catIt;
	               
	        //Insertamos el order item asociado a la order
	        NE__OrderItem__c oi = new NE__OrderItem__c(
	            NE__OrderId__c = ord.Id,
	            NE__Account__c = testAccount.Id,
	            NE__ProdId__c = pord.Id,
	            NE__CatalogItem__c = catIt.Id,
	            NE__qty__c = 1,
	            NE__OneTimeFeeOv__c = 1000,
	            BI_Ingreso_Recurrente_Anterior_Producto__c = 2000,
	            NE__RecurringChargeOv__c = 3000,
	            CurrencyISOCode = 'ARS'        
        	);
      
	        List<NE__OrderItem__c> lst_itemToInsert = new List<NE__OrderItem__c>();
	        lst_itemToInsert.add(oi);

	        insert lst_itemToInsert;

	        Account testAccount2 = new Account(Name = 'testAcc2', BI_Country__c = 'Argentina',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
      		insert testAccount2;   

      		Opportunity testOpp2 = new Opportunity(BI_No_Identificador_fiscal__c = '00000000C',
		                                            Name = 'testOpp2', 
		                                            BI_Country__c = 'Argentina', 
		                                            BI_SIMP_Opportunity_Type__c = 'Alta', 
		                                            CloseDate = Date.today(), 
		                                            BI_Probabilidad_de_exito__c = '100', 
		                                            CurrencyIsoCode = 'EUR', 
		                                            BI_Duracion_del_contrato_Meses__c = 12, 
		                                            BI_Plazo_estimado_de_provision_dias__c = 90, 
		                                            StageName = 'F6 - Other',
		                                            AccountId = testAccount2.Id);
		   	insert testOpp2;

		   	BI_MigrationHelper.cleanSkippedTriggers();

	        BI_AccountKpisBatch batch = new BI_AccountKpisBatch();
	        batch.todos = true; //JEG
			Database.executeBatch(batch);

  		/*}

        catch(Exception e){
        	System.debug('BI_AccountKpisBatch EXCEPTION: '+ e);
        }

        finally{
            if(mapa != null){
                BI_MigrationHelper.disableBypass(mapa);
            }
            
        }*/

	}

}