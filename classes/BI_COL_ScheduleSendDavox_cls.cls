global class BI_COL_ScheduleSendDavox_cls implements Schedulable
{
	public Integer opc{get;set;}
	global BI_COL_ScheduleSendDavox_cls()
	{
		opc=1;
	}
	global BI_COL_ScheduleSendDavox_cls(Integer op)
	{
		opc=op;
	}
	
	global BI_COL_ScheduleSendDavox_cls(Integer op,String pck)
	{
		opc=op;
		//packagecode=pck;
	}

	global void execute(SchedulableContext sc) 
	{
		System.debug('Entra a execute justo antes de llamar a ElimarSch');
		System.debug('La variable opc = ' + opc);
		this.EliminarSch();
		System.debug('Supera el llamado de ElimarSch');
		/*if(opc==1)
		{
			System.debug('\n\n Entra al caso donde opc = 1 \n\n');
			database.executebatch(new DAVOXPLUS_GeneraNovedadesBatch(false),100);
		}*/
		if(opc==1)
		{
			System.debug('\n\n Entra al caso donde opc = 1 \n\n');
			database.executebatch(new BI_COL_GenerateNewBilling_cls(false),100);
		}
		else if(opc==2)
		{
			System.debug('\n\n Entra al caso donde opc = 2 \n\n');
			BI_COL_SendDavox_cls procBatch = new BI_COL_SendDavox_cls(false,opc);
			string strConsulta = 'SELECT ID, BI_COL_Informacion_Enviada__c,BI_COL_Informacion_recibida__c,BI_COL_Cobro_Parcial__c,BI_COL_Modificacion_Servicio__c,BI_COL_Estado__c'+
								' FROM BI_Log__c ' +
								' where isdeleted = false and BI_COL_Tipo_Transaccion__c IN (\'SERV. CORPORATIVO - COBROS PARCIALES\')'+
								' and BI_COL_Estado__c in (\'Pendiente\', \'Devuelto\', \'Error Conexion\', \'Procesando \') order by createddate'; 
			procBatch.TipoNovedad = 'SERV. CORPORATIVO - NOVEDADES';
			procBatch.Query = strConsulta;
			System.debug('\n\n'+strConsulta+'\n\n');
			System.Debug('Inicia ejecución del batch');
			ID batchprocessid = Database.executeBatch(procBatch,500);
			System.Debug('Finaliza ejecución del batch batchprocessid: '+batchprocessid);
		}
		else if(opc==3)
		{
			System.debug('\n\n Entra al caso donde opc = 3 \n\n');
			System.debug('Termino');
			this.EliminarSch();
		}
	}

	public void EliminarSch()
	{
		System.debug('Entra a ElimarSch');
		
		List<Crontrigger> a=[Select id,state from CronTrigger where state='DELETED'];
		System.debug('List<Crontrigger> = ' + a);
		for(CronTrigger ct : a)
		{
			System.abortjob(ct.Id);
		}
	}

	public String getPackageCode()
	{
		//PackageCode__c codPaq=[Select Name from PackageCode__c where NombreAplicacion__c='DAVOX' limit 1];
		//codPaq.Name='ASD654AS'+System.now();
		//update codPaq; 
		return 'ASD654AS'+System.now();
	}
}