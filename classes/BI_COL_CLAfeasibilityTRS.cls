public with sharing class BI_COL_CLAfeasibilityTRS {
	
	public static List<BI_Log__c> iniciarBitacoraViabilidad(List<String> descripcionesSeleccionadas,
    Map<String, List<BI_COL_Viabilidad_Tecnica__c>> mapMsListSolicitudViabilidad){
    List<BI_Log__c> bitacorasDescripciones = new List<BI_Log__c>();

    try{
      List<BI_COL_Modificacion_de_Servicio__c> consultaProductoDescripciones = ([SELECT 
                                                                     Id, Name 
                                                                     FROM
                                                                     BI_COL_Modificacion_de_Servicio__c 
                                                                     WHERE 
                                                                     Id IN :descripcionesSeleccionadas
                                                                     AND IsDeleted = false 
                                                                     ORDER BY 
                                                                     Name ASC 
                                                                     LIMIT 1000]);
      
      for (BI_COL_Modificacion_de_Servicio__c registroDescripcion : consultaProductoDescripciones){        
        System.debug('\n\n ******** Se recorre la lista de MS para crear los logs de transacciones');
        
        //Obtenemos la lista de Solicitudes de Viabilidad asociadas a la MS
        List<BI_COL_Viabilidad_Tecnica__c> listSolicitudViabilidad =
          mapMsListSolicitudViabilidad.get(String.valueOf(registroDescripcion.Id));
        
        for(BI_COL_Viabilidad_Tecnica__c objSolicitudViabilidad : listSolicitudViabilidad)
        {
          BI_Log__c registroBitacora = new BI_Log__c();
    
          //registroBitacora.Name = 'VIATEC TRS';
          registroBitacora.BI_COL_Modificacion_Servicio__c = registroDescripcion.Id;
          registroBitacora.BI_COL_Estado__c = 'Pendiente';
          registroBitacora.BI_COL_Identificador__c = registroDescripcion.Name;
          registroBitacora.BI_COL_Tipo_Transaccion__c = 'SOLICITUD VIABILIDAD TECNICA';
          registroBitacora.BI_Descripcion__c = '';
          registroBitacora.BI_COL_Informacion_recibida__c = '';
          registroBitacora.BI_COL_Solicitud_viabilidad__c = objSolicitudViabilidad.Id;
          
          System.debug('\n\n ******** Se agrega un log a la lista de logs. El log es: ' + registroBitacora);
          
          bitacorasDescripciones.add(registroBitacora);
        }
      }
      
      System.debug('\n\n ******** La lista de logs a insertar es: ' + bitacorasDescripciones);
        
      insert bitacorasDescripciones;
      
      return bitacorasDescripciones;
    }catch(Exception errorSistema){
      System.debug('(Crear bítacora descripciones) Se ha presentado el siguiente error: ' + errorSistema.getMessage());
      
      return bitacorasDescripciones;
    }    
  }
}