@isTest(seeAllData=true)
private class BI_ContactRest_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_ContactRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    15/09/2014              Pablo Oliva             Initial Version
    10/11/2015              Fernando Arteaga        BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_ContactRest.getContact()
    History:
    
    <Date>              <Author>                <Description>
    15/09/2014          Pablo Oliva             Initial version
    10/11/2015          Fernando Arteaga        BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   static testMethod void getContactTest() {

        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }
        
        //Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        //BI_Code_ISO__c region = biCodeIso.get('ARG');                                               
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        List <Contact> lst_con = BI_DataLoadRest.loadContacts(1, lst_acc);
        System.debug('lst_con:'+ lst_con);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/accountresources/v1/contacts/'+lst_con[0].BI_Id_del_contacto__c;  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        BI_TestUtils.throw_exception = true;
        
        //INTERNAL_SERVER_ERROR
        BI_ContactRest.getContact();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c where BI_Asunto__c = 'BI_ContactRest.getContact' order by CreatedDate DESC limit 1];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        BI_TestUtils.throw_exception = false;
        
        //BAD REQUEST 
        BI_ContactRest.getContact();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        RestContext.request.addHeader('countryISO', 'ARG');
        
        //NOT FOUND
        req.requestURI = '/accountresources/v1/contacts/idContact';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;
        
        BI_ContactRest.getContact();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/accountresources/v1/contacts/'+lst_con[0].BI_Id_del_contacto__c;
        RestContext.request = req;
        BI_ContactRest.getContact();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_ContactRest.updateContact()
    History:
    
    <Date>              <Author>                <Description>
    15/09/2014          Pablo Oliva             Initial version
    10/11/2015          Fernando Arteaga        BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
	04/05/2017			Marta Glez				REING- Reingenieria de contactos--> nationalIDType y nationalID se deben informar
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  static testMethod void updateContactTest() {
        
        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }
        
        //Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        //BI_Code_ISO__c region = biCodeIso.get('ARG');                                               
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        List <Contact> lst_con = BI_DataLoadRest.loadContacts(1, lst_acc);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/accountresources/v1/contacts/'+lst_con[0].BI_Id_del_contacto__c;  
        req.httpMethod = 'PUT';
        
        Blob body = Blob.valueof('{"accountDetails": { "subsegment": null, "statusReason": null, "segment": null, "satisfaction": null, "riskRank": null, "paymentMethods": null, "name": "Prueba Integra 3", "legalId": { "docType": "Test", "docNumber": "20000000007", "country": "0"}, "fraud": false, "description": null, "customerId": null, "contacts": null, "contactingModes": [], "address": [], "additionalData": [ { "Key": "accountId", "value": "543210" } ], "accountStatus": null, "accountInfoType": "Potencial" }}');
        req.requestBody = body;

        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        
        //BAD REQUEST countryISO
        BI_ContactRest.updateContact();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //INTERNAL_SERVER_ERROR
        //RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        RestContext.request.addHeader('countryISO', 'ARG');
        BI_ContactRest.updateContact();
        
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c where BI_Asunto__c = 'BI_ContactRest.updateContact' order by CreatedDate DESC limit 1];
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals(1, lst_log.size());
        
        //body = Blob.valueof('{ "roles": [ "Administrativo" ], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+region.BI_Country_ISO_Code__c+'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": null, "accounts": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ] }');
        //REING-INI body = Blob.valueof('{ "roles": [ "Administrativo" ], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+ 'ARG' + '" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": null, "accounts": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ] }');
        body = Blob.valueof('{ "roles": [ "Administrativo" ], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "nationalIDType": "Otros", "nationalID": "1234567", "country": "'+ 'ARG' + '" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": null, "accounts": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ] }');
        
        req.requestBody = body;
        RestContext.request = req;
        
        //OK
        BI_ContactRest.updateContact();
        system.assertEquals(200,RestContext.response.statuscode);
        
        //NOT FOUND
        //body = Blob.valueof('{ "roles": [ "Administrativo" ], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+region.BI_Country_ISO_Code__c+'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": null, "accounts": [ "bbb" ] }');
        //REING_INI body = Blob.valueof('{ "roles": [ "Administrativo" ], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+ 'ARG' +'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": null, "accounts": [ "bbb" ] }');
        body = Blob.valueof('{ "roles": [ "Administrativo" ], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "nationalIDType": "Otros", "nationalID": "1234567", "country": "'+ 'ARG' +'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": null, "accounts": [ "bbb" ] }');
          
      req.requestBody = body;
        RestContext.request = req;
        
        BI_ContactRest.updateContact();
        system.assertEquals(404,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_ContactRest.updateContact()
    History:
    
    <Date>              <Author>                <Description>
    15/09/2014          Pablo Oliva             Initial version
    10/11/2015          Fernando Arteaga        BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
	04/05/2017			Marta Glez				REING- Reingenieria de contactos--> nationalIDType y nationalID se deben informar
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void updateContactCustomerTest() {
        
        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }
        
        //Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        //BI_Code_ISO__c region = biCodeIso.get('ARG');                                                
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        
        List <Contact> lst_con = BI_DataLoadRest.loadContacts(1, lst_acc);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/accountresources/v1/contacts/'+lst_con[0].BI_Id_del_contacto__c;  
        req.httpMethod = 'PUT';
        
        //Blob body = Blob.valueof('{ "roles": [ "Administrativo" ], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+region.BI_Country_ISO_Code__c+'" }], "customers": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ], "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": null, "accounts": null }');
        //REING-INI Blob body = Blob.valueof('{ "roles": [ "Administrativo" ], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+ 'ARG' +'" }], "customers": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ], "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": null, "accounts": null }');
        Blob body = Blob.valueof('{ "roles": [ "Administrativo" ], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "nationalIDType": "Otros", "nationalID": "1234567", "country": "'+ 'ARG' +'" }], "customers": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ], "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": null, "accounts": null }');
        req.requestBody = body;

        RestContext.request = req;
        RestContext.response = res;
        
        //RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        RestContext.request.addHeader('countryISO', 'ARG');
        
        Test.startTest();
        
        //OK
        BI_ContactRest.updateContact();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}