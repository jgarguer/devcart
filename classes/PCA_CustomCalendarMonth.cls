public class PCA_CustomCalendarMonth {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Class to manage custom events 
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private List<Week> weeks; 
    public Date firstDate; // always the first of the month
    private Date upperLeft; 
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   retrieves days of the month 
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public List<Date> getValidDateRange() { 
        try{
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
            // return one date from the upper left, and one from the lower right
            List<Date> ret = new List<Date>();
            ret.add(upperLeft);
            ret.add(upperLeft.addDays(5*7) );
            return ret;
        }catch (exception Exc){
                BI_LogHelper.generate_BILog('PCA_CustomCalendarMonth.getValidDateRange', 'Portal Platino', Exc, 'Class');
                return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Ana Escrich
     Company:       Salesforce.com
     Description:   retrieves name of the month 
     
     History:
     
     <Date>            <Author>              <Description>
     30/06/2014        Ana Escrich           Initial version
     20/04/2017        Jose Miguel Fierro    Month names are now always in English
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String getMonthName() { 
        try{
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
            map<string,string> mapMonthENESP = new map<string,string>();
            if(UserInfo.getLanguage().startsWith('en_')) {
                mapMonthENESP.put('January','January');
                mapMonthENESP.put('February','February');
                mapMonthENESP.put('March','March');
                mapMonthENESP.put('April','April');
                mapMonthENESP.put('May','May');
                mapMonthENESP.put('June','June');
                mapMonthENESP.put('July','July');
                mapMonthENESP.put('August','August');
                mapMonthENESP.put('September','September');
                mapMonthENESP.put('October','October');
                mapMonthENESP.put('November','November');
                mapMonthENESP.put('December','December');
            } else {
                mapMonthENESP.put('January','Enero');
                mapMonthENESP.put('February','Febrero');
                mapMonthENESP.put('March','Marzo');
                mapMonthENESP.put('April','Abril');
                mapMonthENESP.put('May','Mayo');
                mapMonthENESP.put('June','Junio');
                mapMonthENESP.put('July','Julio');
                mapMonthENESP.put('August','Agosto');
                mapMonthENESP.put('September','Septiembre');
                mapMonthENESP.put('October','Octubre');
                mapMonthENESP.put('November','Noviembre');
                mapMonthENESP.put('December','Diciembre');
            }
            
            return mapMonthENESP.get(DateTime.newInstance(firstDate.year(),firstdate.month(),firstdate.day()).format('MMMM'));
            //return DateTime.newInstance(firstDate.year(),firstdate.month(),firstdate.day()).format('MMMM');
        }catch (exception Exc){
                BI_LogHelper.generate_BILog('PCA_CustomCalendarMonth.getMonthName', 'Portal Platino', Exc, 'Class');
                return null;
        }
    } 
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Ana Escrich
     Company:       Salesforce.com
     Description:   retrieves formatted date 
     
     History:
     
     <Date>            <Author>              <Description>
     30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String getYearName() { 
        try{
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
            return DateTime.newInstance(
            firstDate.year(),firstdate.month(),firstdate.day()).format('yyyy');
        }catch (exception Exc){
                BI_LogHelper.generate_BILog('PCA_CustomCalendarMonth.getYearName', 'Portal Platino', Exc, 'Class');
                return null;
        }
    } 
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Ana Escrich
     Company:       Salesforce.com
     Description:   retrieves weeks of the month 
     
     History:
     
     <Date>            <Author>              <Description>
     30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String[] getWeekdayNames() { 
        try{
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
              
            Date today = system.today().toStartOfWeek();
            DateTime dtime = DateTime.newInstanceGmt(today.year(),today.month(),today.day());       
            list<String> ret = new list<String>();
            for(Integer i = 0; i < 7;i++) { 
                ret.add( dtime.formatgmt('EEEE') );
                dtime= dtime.addDays(1);
            } 
            return ret;
        }catch (exception Exc){
                BI_LogHelper.generate_BILog('PCA_CustomCalendarMonth.getWeekdayNames', 'Portal Platino', Exc, 'Class');
                return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Ana Escrich
     Company:       Salesforce.com
     Description:   retrieves first day of the month 
     
     History:
     
     <Date>            <Author>              <Description>
     30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Date getfirstDate() {
        try{ 
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
            return firstDate; 
        }
        catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_CustomCalendarMonth.getfirstDate', 'Portal Platino', Exc, 'Class');
            return null;
        }
    } 
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Ana Escrich
     Company:       Salesforce.com
     Description:   calculate weeks and first date of the month
     
     History:
     
     <Date>            <Author>              <Description>
     30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_CustomCalendarMonth( Date value ) {  // 2013-11-26 00:00:00  
        
            weeks = new List<Week>();
            firstDate = value.toStartOfMonth();
            upperLeft = firstDate.toStartOfWeek();
            Date tmp = upperLeft;
    
            /** TO DO: change static value of 5 for login weeks for a month*/
            for (Integer i = 0; i < 5; i++) {
                Week item = new Week(i+1,tmp,value.month());    
                system.assert(item!=null); 
                this.weeks.add( item );
                tmp = tmp.addDays(7);
                
                if(i>3 && firstDate.monthsBetween(tmp)< 1){// Case December 2013, no enougth weeks for all month days
                    // Decrease control variable i for repeat one more time de loop and create a new week line
                    i--;
                }
            }
            system.debug('weeks2: '+weeks);
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Set standard events to calendar
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void setEventsStd(List<eventWrapper> events) { 
        try{
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
            // merge these events into the proper day 
            system.debug('1');
            for(eventWrapper event:events) { 
                for(Week week:weeks) { 
                    for(Day item: week.getDays() ) {                
                        DateTime dtAux = DateTime.newInstance(item.theDate, Time.newInstance(00, 00, 00, 00));
                        if ( event.startTime.isSameDay(dtAux))  { 
                            System.debug('event.StartDateTime__c: ' + event.startTime+ '. c.theDate: ' + item.theDate);
                            // add this event to this calendar date
                            item.eventsToday.add(new PCA_CustomCalendarEventItem(event)); 
                            // add only three events, then a More... label if there are more
                        } 
                    } 
                } 
            }
        }catch (exception Exc){
                BI_LogHelper.generate_BILog('PCA_CustomCalendarMonth.setEventsStd', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Retrieves weeks of the month
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public List<Week> getWeeks() { 
        try{
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
            system.assert(weeks!=null,'could not create weeks list');
            return this.weeks; 
        }catch (exception Exc){
                BI_LogHelper.generate_BILog('PCA_CustomCalendarMonth.getWeeks', 'Portal Platino', Exc, 'Class');
                return null;
        }
    }
        
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Helper class to define a month in terms of week
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public class Week {
        public List<Day> days;
        public Integer weekNumber; 
        public Date startingDate; // the date that the first of this week is on
        // so sunday of this week
        
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Constructor: Initializes days of the week
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public Week () { 
            try{
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                days = new List<Day>();     
            }catch (exception Exc){
                    BI_LogHelper.generate_BILog('Week.Week', 'Portal Platino', Exc, 'Class');
            }
         }
        
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Retrieves days of the week
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public List<Day> getDays() {
            try{ 
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                return this.days;
            }catch (exception Exc){
                BI_LogHelper.generate_BILog('Week.getDays', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
        
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   add days to the week
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public Week(Integer value,Date monday,Integer month) { 
        try{
                this();
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                System.debug('%%%%%%%% value: ' + value + '. monday: ' + monday + '. month: ' + month);
                //  value: 1. monday: 2013-10-28 00:00:00. month: 11
                weekNumber = value;
                startingDate = monday;
                Date tmp = startingDate;
                for (Integer i = 0; i < 7; i++) {
                    Day day = new Day( tmp,month ); 
                    tmp = tmp.addDays(1);
                    day.dayOfWeek = i+1;        
                    system.debug('### d: ' + day);
                    days.add(day);
                } 
            }catch (exception Exc){
                    BI_LogHelper.generate_BILog('Week.Week', 'Portal Platino', Exc, 'Class');
            }
        }
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Retrieves number of the week
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public Integer getWeekNumber() 
        {
            try{ 
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                return this.weekNumber;
            }
            catch (exception Exc){
                BI_LogHelper.generate_BILog('Week.getWeekNumber', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
        
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Retrieves first day of the week
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public Date getStartingDate() 
        {
            try{ 
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                return this.startingDate;
            }catch (exception Exc){
                BI_LogHelper.generate_BILog('Week.getStartingDate', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Helper class to define a week in terms of day
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public class Day {
         
        public Date         theDate;
        public List<PCA_CustomCalendarEventItem>    eventsToday; // list of events for this date
        public Integer      month, dayOfWeek;
        public String       formatedDate; // for the formated time  
        private String      cssclass = 'calActive';     
        
        public boolean pastDay;
        
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Constructor
        
        History:
        
        <Date>            <Author>              <Description>
        02/07/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public Day(Date value,Integer vmonth) {
            try{
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                theDate=value; month=vmonth;        
                formatedDate = '12 21 08';// time range..
                //9:00 AM - 1:00 PM
                eventsToday = new List<PCA_CustomCalendarEventItem>();  
                // three possible Inactive,Today,Active  
                if ( theDate.daysBetween(System.today()) == 0 ) cssclass ='calToday';
                // define inactive, is the date in the month?
                if ( theDate.month() != month) cssclass = 'calInactive';
            }catch (exception Exc){
                BI_LogHelper.generate_BILog('Day.Day', 'Portal Platino', Exc, 'Class');
            }
        }
        
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Retrieves current date
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public Date getDate() 
        {
            try{ 
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                return theDate; 
            }catch (exception Exc){
                BI_LogHelper.generate_BILog('Day.getDate', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
        
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Retrieves current day
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public Integer getDayOfMonth() 
        {
            try{ 
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                return theDate.day(); 
            }
            catch (exception Exc){
                BI_LogHelper.generate_BILog('Day.getDayOfMonth', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Format the date if number of day is less than 10 
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public String getDayOfMonth2() { 
            try{
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                if ( theDate.day() <= 9 ){ 
                    return '0'+theDate.day();
                } 
                return String.valueof( theDate.day()); 
            }catch (exception Exc){
                    BI_LogHelper.generate_BILog('Day.getDayOfMonth2', 'Portal Platino', Exc, 'Class');
                    return null;
            }
        }
        
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Retrieves day of the year
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public Integer getDayOfYear() {
            try{ 
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                return theDate.dayOfYear();
            }
            catch (exception Exc){
                BI_LogHelper.generate_BILog('Day.getDayOfYear', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
        
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Retrieves day well formatted
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public String getFormatedDate() {
            try{ 
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                return formatedDate; 
            }catch (exception Exc){
                BI_LogHelper.generate_BILog('Day.getFormatedDate', 'Portal Platino', Exc, 'Class');
                return null;
            } 
        }
        
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Retrieves day of week (number)
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public Integer getDayNumber() {
            try{ 
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                return dayOfWeek; 
            }
            catch (exception Exc){
                BI_LogHelper.generate_BILog('Day.getDayNumber', 'Portal Platino', Exc, 'Class');
                return null; 
            }
        }
        
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Retrieves true if the day is previous of today
        
        History:
        
        <Date>            <Author>              <Description>
        02/07/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public boolean getpastDay(){
            try{
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                if(theDate<Date.today()){
                    return true;
                }
                return false;
            }catch (exception Exc){
                BI_LogHelper.generate_BILog('Day.getpastDay', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
        
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Retrieves true if the day is previous of today
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public List<PCA_CustomCalendarEventItem>    getEventsToday() { 
            try{
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                if(!eventsToday.IsEmpty())
                    system.debug('eventsToday: '+eventsToday);
                return eventsToday; 
            }catch (exception Exc){
                BI_LogHelper.generate_BILog('Day.getEventsToday', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Retrieves cssclass parameter
        
        History:
        
        <Date>            <Author>              <Description>
        30/06/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public String getCSSName() {
            try{    
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                return cssclass; 
            }
            catch (exception Exc){
                BI_LogHelper.generate_BILog('Day.getCSSName', 'Portal Platino', Exc, 'Class');
                return null;
            } 
        }
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Retrieves true if the event is editable by the user
        
        History:
        
        <Date>            <Author>              <Description>
        02/07/2014        Ana Escrich           Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public Boolean getIsEditable(){
            try{
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
                return theDate>=Date.Today();
            }catch (exception Exc){
                BI_LogHelper.generate_BILog('Day.getIsEditable', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
    }

}