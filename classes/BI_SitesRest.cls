@RestResource(urlMapping='/SiteManagement/v1/customers/*')
global class BI_SitesRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Class for Site Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    29/05/2017   Ignacio G Schuhmacher  Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Creates a new Site in the server receiving the request.
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>		          <Description>
    29/05/2017 		  Ignacio G Schuhmacher   Initial version
    29/11/2017        Gawron, Julián		  Change Response Location to SF Id
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPost
	global static FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType createSite() {
		FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType response;
		try{			
			
			RestRequest req = RestContext.request;
		
			Blob blob_json = req.requestBody; 
		
			String string_json = blob_json.toString(); 
			System.debug(string_json);
			FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType conDetType = (FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType) JSON.deserialize(string_json, FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType.class);

			// URI PARAMETERS
			List<String> uri = RestContext.request.requestURI.split('/');
			
			if(uri.size() == 7){
				String externalSite = RestContext.request.requestURI.split('/')[6];
				String externalCustomer = RestContext.request.requestURI.split('/')[4];
				System.debug('SiteId : ' + externalSite);
				System.debug('externalCustomer : ' + externalCustomer);
				System.debug('uri size : ' + RestContext.request.requestURI.split('/').size());						

				BI_Sede__c sfAddress = BI_RestHelper.getRelatedAddress(externalSite);

				Account sfCustomer = BI_RestHelper.getRelatedAccount(externalCustomer);                               
				System.debug('sfCustomer : ' + sfCustomer);
				BI_Punto_de_instalacion__c newSite = new BI_Punto_de_instalacion__c();
				if(sfCustomer != null) {
					if(sfAddress == null){
						System.debug('Entra a crear el address');						
						BI_Sede__c newAddress = new BI_Sede__c();													
						newAddress.Name = conDetType.address.addressName;
						newAddress.BI_Direccion__c = conDetType.name;
						newAddress.BI_Numero__c = conDetType.address.addressNumber.value;
						newAddress.BI_Country__c = conDetType.address.country;
						newAddress.BI_Provincia__c = conDetType.address.region;
						newAddress.BI_Localidad__c = conDetType.address.locality;
						newAddress.BI_Piso__c = conDetType.address.floor.value;
						newAddress.BI_Apartamento__c = conDetType.address.apartment.value;
						newAddress.BI_Codigo_postal__c = conDetType.address.postalCode;
						newAddress.BI_ID_de_la_sede__c  = externalSite;

						
	 					Schema.DescribeFieldResult fieldResult = BI_Sede__c.Estado__c.getDescribe();
						List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
						    
						for( Schema.PicklistEntry f : ple){					  	
						  	if(conDetType.status == FS_TGS_RestWrapperFullStack.SiteStatusType.active && f.getValue() == 'Validado'){
						  		newAddress.Estado__c = f.getValue();		
							} else if(conDetType.status == FS_TGS_RestWrapperFullStack.SiteStatusType.inactive && f.getValue() == 'Rechazado'){
								newAddress.Estado__c = f.getValue();		
							}					
						}       
						insert newAddress;

						if(newAddress.id != null){
							newSite.BI_Cliente__c = sfCustomer.Id;
							newSite.BI_Sede__c = newAddress.id;
							//newSite.BI_Tipo_de_sede__c = conDetType.address.addressType;
							newSite.name = conDetType.address.addressName;

							insert newSite;

							if(newSite.id != null){
								RestContext.response.statuscode = 201;
								//RestContext.response.headers.put('Location','https://'+System.URL.getSalesforceBaseURL().getHost()+'/SiteManagement/v1/customers/'+externalCustomer + 'sites/' + externalSite);
								RestContext.response.headers.put('Location',newSite.id); //JEG
								if (String.IsNotBlank(RestContext.request.headers.get('UNICA-ServiceId'))) RestContext.response.headers.put('UNICA-ServiceId', RestContext.request.headers.get('UNICA-ServiceId'));
	            				if (String.IsNotBlank(RestContext.request.headers.get('UNICA-Application'))) RestContext.response.headers.put('UNICA-Application', RestContext.request.headers.get('UNICA-Application'));								
								
							}							
						}
						
					} else {
						System.debug('El address existe, vamos a comprobarlo');
						if(sfAddress.Name == conDetType.address.addressName){
							System.debug('El address existe y es igual al que le mandamos, creamos directamente el site');							
							newSite.BI_Cliente__c = sfCustomer.Id;
							newSite.BI_Sede__c = sfAddress.id;
							newSite.name = conDetType.address.addressName;
							//newSite.BI_Tipo_de_sede__c = conDetType.address.addressType;

							//insertamos el Site porque ya tenemos toda la info necesaria
							insert newSite;

							if(newSite.id != null){

								RestContext.response.statuscode = 201;
								RestContext.response.headers.put('Location','https://'+System.URL.getSalesforceBaseURL().getHost()+'/SiteManagement/v1/customers/'+externalCustomer + 'sites/' + externalSite);
								if (String.IsNotBlank(RestContext.request.headers.get('UNICA-ServiceId'))) RestContext.response.headers.put('UNICA-ServiceId', RestContext.request.headers.get('UNICA-ServiceId'));
	            				if (String.IsNotBlank(RestContext.request.headers.get('UNICA-Application'))) RestContext.response.headers.put('UNICA-Application', RestContext.request.headers.get('UNICA-Application'));
								
							}						
						}else {
							System.debug('El address existe y es distinto al que mandan, damos un error');
							//Devolvemos un error porque el address existe y tiene otra configuración
							RestContext.response.statuscode = 400;//BAD_REQUEST
							RestContext.response.headers.put('errorMessage', 'Site : ' + externalSite + ' already exist with another configuration ');
						}
					}
				} else {
					System.debug('El account no existe, damos un error');
					RestContext.response.statuscode = 404;//BAD_REQUEST
					RestContext.response.headers.put('errorMessage', 'Resource : ' + externalCustomer + ' does not exist');//SVC 1006
				}
			
			} else {
				//NO VIENE EL SITE EN LA URI
				System.debug('El uri no está bien formado, damos un error');
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', 'Invalid Request URI :' + uri);//SVC0001
			}			
			
		}catch(Exception exc){			
			RestContext.response.statuscode = 500;
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_RestV2Sites.createSite', 'BI_EN', Exc, 'Web Service');
			
		}
		return response;
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Creates a new relationship between customer and address
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    31/05/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPut
	global static FS_TGS_RestWrapperFullStack.AssociationType createRelationshipSite() {
		FS_TGS_RestWrapperFullStack.AssociationType response;
		try{
			System.debug('Entramos en create Relationship');
						
			RestRequest req = RestContext.request;
			Blob blob_json = req.requestBody;					
			String string_json = blob_json.toString();
			if(string_json != ''){
				FS_TGS_RestWrapperFullStack.AssociationType conDetType = (FS_TGS_RestWrapperFullStack.AssociationType) JSON.deserialize(string_json, FS_TGS_RestWrapperFullStack.AssociationType.class);
			}
			// URI PARAMETERS
			List<String> uri = RestContext.request.requestURI.split('/');
			
			if(uri.size() == 7 && (uri[4] != '' && uri[4] != null) && (uri[6] != '' && uri[6] != null) ){
				String externalSite = RestContext.request.requestURI.split('/')[6];
				String externalCustomer = RestContext.request.requestURI.split('/')[4];
				System.debug('SiteId : ' + externalSite);
				System.debug('externalCustomer : ' + externalCustomer);
				System.debug('uri size : ' + RestContext.request.requestURI.split('/').size());						

				Account sfCustomer = BI_RestHelper.getRelatedAccount(externalCustomer);                               
				BI_Sede__c sfAddress = BI_RestHelper.getRelatedAddress(externalSite);                               				

				BI_Punto_de_instalacion__c newSite = new BI_Punto_de_instalacion__c();
				if(sfCustomer != null){
					if(sfAddress != null){
												
						newSite.BI_Cliente__c = sfCustomer.Id;
						newSite.BI_Sede__c = sfAddress.id;
						newSite.name = sfAddress.Name;
						//newSite.BI_Tipo_de_sede__c = conDetType.Name; //ESTE CAMPO ACTUALMENTE NO SE MANDA, SE GUARDA COMENTADO PARA UN FUTURO

						insert newSite;

						if(newSite.id != null){
							RestContext.response.statuscode = 201;
							RestContext.response.headers.put('Location','https://'+System.URL.getSalesforceBaseURL().getHost()+'/SiteManagement/v1/customers/'+externalCustomer + 'sites/' + externalSite);
							if (String.IsNotBlank(RestContext.request.headers.get('UNICA-ServiceId'))) RestContext.response.headers.put('UNICA-ServiceId', RestContext.request.headers.get('UNICA-ServiceId'));
	           				if (String.IsNotBlank(RestContext.request.headers.get('UNICA-Application'))) RestContext.response.headers.put('UNICA-Application', RestContext.request.headers.get('UNICA-Application'));
								
						}
					} else{
						RestContext.response.statuscode = 404;//BAD_REQUEST
						RestContext.response.headers.put('errorMessage', 'Resource Address : ' + externalSite + ' does not exist');//SVC 1006
					}							
				} else {
					RestContext.response.statuscode = 404;//BAD_REQUEST
					RestContext.response.headers.put('errorMessage', 'Resource Customer : ' + externalCustomer + ' does not exist');//SVC 1006
				}
			} else {
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', 'Invalid Request URI : ' + uri);
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_SitesRest.deleteSites', 'BI_EN', Exc, 'Web Service');
			
		}
		return response;
		
	}

	/*
	DE MOMENTO SE SUBE COMENTADO ESTE SERVICIO PARA MÁS ADELANTE
	-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Deletes a relationship between customer and address
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    31/05/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	/*@HttpDelete
	global static void deleteRelationshipSite() {
		try{
				
			String customerId = String.valueOf(RestContext.request.requestURI).split('/')[4];
			String addressId = String.valueOf(RestContext.request.requestURI).split('/')[6];
			
			if((customerId==null || customerId == '') || (addressId == null || addressId== '')){		
				
				RestContext.response.statuscode = 400;			
					
			} else {
				Account acc = [select Id, BI_Identificador_Externo__c from Account where BI_Identificador_Externo__c = :customerId Limit 1];

				BI_Sede__c address = [select Id, BI_ID_de_la_sede__c from BI_Sede__c where BI_ID_de_la_sede__c = :addressId limit 1];

				List<BI_Punto_de_instalacion__c> listaSede = [Select Id, Name, BI_Tipo_de_sede__c, BI_Cliente__c, BI_Sede__c from BI_Punto_de_instalacion__c where 
									BI_Cliente__c = :acc.Id AND BI_Sede__c =:address.Id Limit 1];

				if(listaSede.isEmpty()){
					RestContext.response.statuscode = 404;
				} else {
					delete listaSede[0];
					RestContext.response.statuscode = 204;
				}
			}
					
		}catch(Exception Exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',Exc.getMessage());
			BI_LogHelper.generate_BILog('BI_SitesRest.deleteSites', 'BI_EN', Exc, 'Web Service');
			
		}	

	}	*/ 
}