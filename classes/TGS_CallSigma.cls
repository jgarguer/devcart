/*------------------------------------------------------------ 
Author:         Marta Laliena
Company:        Deloitte
Description:    //TODO
                
History
<Date>          <Author>        <Change Description>
23-Jul-2015     Marta Laliena    Initial Version
02-Nov-2015		Marta Laliena	 setAttachments always return an object, whether is empty or with attathments
09-Nov-2015		Marta Laliena	 setAssetAccountsFromOrderInClosed
21-Nov-2015		Marta Laliena	 setAssetAccountsFromOrderInClosed, installationPoint is set for the order's and asset's CIs
27-Mar-2017     Álvaro López     Added aceptacionRFS sent from In progress to Resolved
04-Apr-2017     Álvaro López     Added @TestVisible to setAttachments and setAssetAccountsFromOrderInClosed methods
05-Jun-2017     Álvaro López     Deleted aceptacionRFS sent from In progress to Resolved
19-Oct-2017     Mariano García   Fix accounts assigment
------------------------------------------------------------*/
global with sharing class TGS_CallSigma {

    //Set your username and password here
    private static final String username = 'consulta';
    private static final String password = 'consulta';
    
    public static void sentToSigmaCase(Id caseId, String status, String statusReason, Map<Id,String> oldStatus, Map<Id,String> oldStatusReason, Date RFSDate, String TGS_SIGMA_ID, String Sigma_WH_Site_ID){
        System.debug('sentToSigmaCase start');
        TGS_SigmaOrdering.SrvSigmaOrderingHttpPort sigmaOrdering = new TGS_SigmaOrdering.SrvSigmaOrderingHttpPort();
        Blob headerValue = Blob.valueOf(username+':'+password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        sigmaOrdering.inputHttpHeaders_x = new Map<String,String>();
        sigmaOrdering.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        sigmaOrdering.inputHttpHeaders_x.put('Accept', 'application/json');
        sigmaOrdering.inputHttpHeaders_x.put('Content-Type','application/json; charset=ISO-8859-1');
        TGS_BmcSigmaTiwsCom.ResultOrderingDTO result;
        System.debug('sentToSigma end headers');
        Datetime dt = null;
        if(RFSDate != null){
            DateTime dtime = datetime.newInstance(RFSDate.year(), RFSDate.month(), RFSDate.day());
            String stringDTime = dtime.format('yyyy-MM-dd\'T\'HH:mm:ss');
            //stringDTime = stringDTime + '+02:00';
			//dt = datetime.parse(stringDTime);
            dt = dtime;
            System.debug('datatime '+dt);
        }
        TGS_CommonSigmaTiwsCom.ArrayOfAttachmentFullDTO arrayAtt = setAttachments(null);
        Integer numOfTries = 0;
        Boolean exceptionResult = true;
        while(exceptionResult && numOfTries<3){
            exceptionResult = false;
            try{
                if(oldStatus.get(caseId).equals(Constants.CASE_STATUS_RESOLVED) && status.equals(Constants.CASE_STATUS_CLOSED)){
                    System.debug('sentToSigma Resolved -> Closed');
                    result = sigmaOrdering.aceptacionRFS(TGS_SIGMA_ID, Sigma_WH_Site_ID, dt, arrayAtt);
                }

                /* START Álvaro López 27/03/2017 - Added aceptacionRFS sent from In progress to Closed */
                else if(oldStatus.get(caseId).equals(Constants.CASE_STATUS_IN_PROGRESS) && oldStatusReason.get(caseId).equals(Constants.CASE_STATUS_REASON_IN_PROVISION)
                        && status.equals(Constants.CASE_STATUS_CLOSED)){
                    result = sigmaOrdering.aceptacionRFS(TGS_SIGMA_ID, Sigma_WH_Site_ID, dt, arrayAtt);
                }
                /* END Álvaro López*/

                else if(oldStatus.get(caseId).equals(Constants.CASE_STATUS_RESOLVED) && status.equals(Constants.CASE_STATUS_IN_PROGRESS) && statusReason.equals(Constants.CASE_STATUS_REASON_IN_PROVISION)){
                    System.debug('sentToSigma Resolved -> In Progress - In Provision');
                    /*
                     * Argumento n2 y n3: Rejection reason y Associated observations
                     */
                    result = sigmaOrdering.rechazoRFS(TGS_SIGMA_ID, Sigma_WH_Site_ID, 'otrosMotivosPI - Others', '', arrayAtt);
                }
                else if(oldStatus.get(caseId).equals(Constants.CASE_STATUS_PENDING) && oldStatusReason.get(caseId).equals(Constants.CASE_STATUS_REASON_IN_PROVISION)
                        && status.equals(Constants.CASE_STATUS_IN_PROGRESS) && statusReason.equals(Constants.CASE_STATUS_REASON_IN_PROVISION)){
                    System.debug('sentToSigma Pending - In Provision -> In Progress - In Provision');
                    result = sigmaOrdering.respuestaAclaracion(TGS_SIGMA_ID, Sigma_WH_Site_ID, 'Observaciones', arrayAtt);
                }
                System.debug('sentToSigma ' + result);
                /* START Álvaro López 27/03/2017 - Check if result is null */
                if(result != null){ 
                    if(!(result.resultado.equals('0') || result.resultado.equals('1') || result.resultado.equals('2') || result.resultado.equals('3')
                        || result.resultado.equals('4') || result.resultado.equals('5'))){
                        exceptionResult = true;
                        numOfTries = numOfTries + 1;
                    }
                }
                else{
                    exceptionResult = true;
                    numOfTries = numOfTries + 1;
                }
                /* END Álvaro López*/
            }catch(Exception e){
                System.debug('sentToSigma exception: '+e.getMessage()+' '+e.getLineNumber());
            	exceptionResult = true;
                numOfTries = numOfTries + 1;
            }
        }
        if(oldStatus.get(caseId).equals(Constants.CASE_STATUS_RESOLVED) && status.equals(Constants.CASE_STATUS_CLOSED)){
        	setAssetAccountsFromOrderInClosed(caseId);
        }
        System.debug('sentToSigma end');
    }
    
    
    public static void sentToSigmaWI(String TGS_SIGMA_ID, String Sigma_WH_Site_ID, String note){
        System.debug('sentToSigmaWI start');
        TGS_SigmaOrdering.SrvSigmaOrderingHttpPort sigmaOrdering = new TGS_SigmaOrdering.SrvSigmaOrderingHttpPort();
        Blob headerValue = Blob.valueOf(username+':'+password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        sigmaOrdering.inputHttpHeaders_x = new Map<String,String>();
        sigmaOrdering.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        sigmaOrdering.inputHttpHeaders_x.put('Accept', 'application/json');
        sigmaOrdering.inputHttpHeaders_x.put('Content-Type','application/json; charset=ISO-8859-1');
        TGS_BmcSigmaTiwsCom.ResultOrderingDTO result;
        System.debug('sentToSigma end headers   case id: '+TGS_SIGMA_ID+'   Administrative num: '+Sigma_WH_Site_ID+'  Description: '+note);
        TGS_CommonSigmaTiwsCom.ArrayOfAttachmentFullDTO arrayAtt = setAttachments(null);
        Integer numOfTries = 0;
        Boolean exceptionResult = true;
        while(exceptionResult && numOfTries<3){
            exceptionResult = false;
            try{
                result = sigmaOrdering.creacionNotaEvolucion(TGS_SIGMA_ID, Sigma_WH_Site_ID, note, arrayAtt);
                System.debug('sentToSigma ' + result);
                /* Álvaro López 30/03/2017 - Added result codes */
                if(result != null){
                    if(!(result.resultado.equals('0') || result.resultado.equals('1') || result.resultado.equals('2') || result.resultado.equals('3')
                            || result.resultado.equals('4') || result.resultado.equals('5'))){
                        exceptionResult = true;
                    	numOfTries = numOfTries + 1;
                    }
                    else{System.debug('sentToSigmaWI OK - result: ' + result.resultado);}
                }
                else{
                    exceptionResult = true;
                    numOfTries = numOfTries + 1;
                }
            }catch(Exception e){
                System.debug('sentToSigma exception: '+e.getMessage());
            	exceptionResult = true;
                numOfTries = numOfTries + 1;
            }
        }
        System.debug('sentToSigma end');
    }
    
    
    @future(callout=true)
    public static void actualizacionIdOrdering(String External_ID, String Sigma_WH_Site_ID, String SRM_Request_ID, String caseNumber){
        System.debug('actualizacionIdOrdering start');
        TGS_SigmaOrdering.SrvSigmaOrderingHttpPort sigmaOrdering = new TGS_SigmaOrdering.SrvSigmaOrderingHttpPort();
        Blob headerValue = Blob.valueOf(username+':'+password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        sigmaOrdering.inputHttpHeaders_x = new Map<String,String>();
        sigmaOrdering.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        TGS_BmcSigmaTiwsCom.ResultOrderingDTO result;
        Integer numOfTries = 0;
        Boolean exceptionResult = true;
        while(exceptionResult && numOfTries<3){
            exceptionResult = false;
            try{
                result = sigmaOrdering.actualizacionOrdering(External_ID, Sigma_WH_Site_ID, SRM_Request_ID, caseNumber);
                System.debug('sentToSigma ' + result);
                /* Álvaro López 30/03/2017 - Added result codes */
                if(result != null){
                    if(!(result.resultado.equals('0') || result.resultado.equals('1') || result.resultado.equals('2') || result.resultado.equals('3')
                            || result.resultado.equals('4') || result.resultado.equals('5'))){
                        exceptionResult = true;
                        numOfTries = numOfTries + 1;
                    }
                    else{System.debug('actualizacionIdOrdering OK - result: ' + result.resultado);}
                }
                else{
                    exceptionResult = true;
                    numOfTries = numOfTries + 1;
                }
            }catch(Exception e){
                System.debug('actualizacionIdOrdering exception: '+e.getMessage());
                exceptionResult = true;
                numOfTries = numOfTries + 1;
            }
        }
        System.debug('actualizacionIdOrdering end');
    }
    
    /**
      * Creates the TGS_CommonSigmaTiwsCom.ArrayOfAttachmentFullDTO with the list of attachment passed.
      * If the list of attachments is null, the returned object only has an null arrayf;
      **/
    @TestVisible
    private static TGS_CommonSigmaTiwsCom.ArrayOfAttachmentFullDTO setAttachments(List<Attachment> listAtt){
        TGS_CommonSigmaTiwsCom.ArrayOfAttachmentFullDTO arrayFullAtt = new TGS_CommonSigmaTiwsCom.ArrayOfAttachmentFullDTO();
        TGS_CommonSigmaTiwsCom.AttachmentFullDTO[] arrayAtt;
        if(listAtt != null && listAtt.size()>0){
            arrayAtt = new TGS_CommonSigmaTiwsCom.AttachmentFullDTO[listAtt.size()];
            Integer index = 0;
            for(Attachment att : listAtt){
                TGS_CommonSigmaTiwsCom.AttachmentFullDTO attDTO = new TGS_CommonSigmaTiwsCom.AttachmentFullDTO();
                attDTO.campoBlob = (att.Body).toString();
                attDTO.fechaInsercion = att.CreatedDate;
                attDTO.idAttachment = att.Id;
                //attDTO.idAttachmentInventario = ;
                //attDTO.idRecurso = ;
                attDTO.mimeType = att.ContentType;
                attDTO.nombreFichero = att.Name;
                attDTO.tamano = att.BodyLength;
                //attDTO.temp = ;
                attDTO.usuario = att.OwnerId;
                arrayAtt[index] = attDTO;
                index = index + 1;
            }
        }
        else{
            arrayAtt = new TGS_CommonSigmaTiwsCom.AttachmentFullDTO[]{};
        }
        arrayFullAtt.AttachmentFullDTO = arrayAtt;
        return arrayFullAtt;
    }
    

	@TestVisible
    private static void setAssetAccountsFromOrderInClosed(Id caseId){
        TGS_CallRodWs.inFutureContextCI = true;
        List<Case> listCases = [SELECT Id, Asset__c, Asset__r.Id, Order__r.Id, Order__r.NE__BillAccId__c, Order__r.NE__ServAccId__c, Order__r.Site__c
                                FROM Case WHERE Id = :caseId];
        if(listCases.size()>0 && listCases[0].Asset__c != null){
            Case caseN = listCases[0];
            NE__Order__c asset = new NE__Order__c(Id = caseN.Asset__r.Id);
            asset.NE__BillAccId__c = caseN.Order__r.NE__BillAccId__c; //BU
            asset.NE__ServAccId__c = caseN.Order__r.NE__ServAccId__c; //CC
            asset.Site__c = caseN.Order__r.Site__c; // Site
            update asset;
            
            // START MGC - 19/10/2017
            List<NE__Order__c> listOrd = [SELECT Id, 
                                                (SELECT Id, Installation_point__c, NE__Billing_Account__c, NE__Billing_Account_Asset_Item__c, 
                                                        NE__Service_Account__c, NE__Service_Account_Asset_Item__c, TGS_Type_Order__c 
                                                FROM NE__Order_Items__r) 
                                        FROM NE__Order__c WHERE Id = :caseN.Asset__r.Id OR Id = :caseN.Order__r.Id];

            List<NE__OrderItem__c> listCIs = new List<NE__OrderItem__c>();
            for(NE__Order__c ord : listOrd){
                for(NE__OrderItem__c CI : ord.NE__Order_Items__r){
                    CI.Installation_point__c = caseN.Order__r.Site__c;
                    CI.NE__Billing_Account__c = caseN.Order__r.NE__BillAccId__c;
                    CI.NE__Service_Account__c = caseN.Order__r.NE__ServAccId__c;

                    if(CI.TGS_Type_Order__c == Constants.RECORD_TYPE_ASSET){
                        CI.NE__Billing_Account_Asset_Item__c = caseN.Order__r.NE__BillAccId__c;
                        CI.NE__Service_Account_Asset_Item__c = caseN.Order__r.NE__ServAccId__c;
                    }
                    listCIs.add(CI);
                }
            }
            update listCIs;
        }
        // END MGC - 19/10/2017

        TGS_CallRodWs.inFutureContextCI = false;
        
    }
    
}