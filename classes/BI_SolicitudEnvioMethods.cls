public without sharing class BI_SolicitudEnvioMethods {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Method for eliminating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void manageTransactions(List<BI_Solicitud_envio_productos_y_servicios__c> news, List<BI_Solicitud_envio_productos_y_servicios__c> olds){
		
		Integer i = 0;
		for(BI_Solicitud_envio_productos_y_servicios__c sol:news){
			if(sol.BI_Estado__c == 'Visando' || olds[i].BI_Estado__c == 'Pendiente'){
				
			}
			
			i++;
		}
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Method to create negative transactions when Request changes status to rejected.
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:		
    
    <Date>            <Author>          <Description>
    25/02/2014        Ignacio Llorca    Initial version
    17/02/2016		  Guillermo Muñoz   BI_TestUtils.isRunningTest added for upgrade the code coverage
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void rejectedRequestTransactions(List<BI_Solicitud_envio_productos_y_servicios__c> news, List<BI_Solicitud_envio_productos_y_servicios__c> olds){
		try{

			if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
    		}

			Set <Id> set_solicitudes = new Set <Id>();
			Set <Id> set_recambios = new Set <Id>();
			Set <Id> set_ventas = new Set <Id>();
			Set <Id> set_acc = new Set <Id>();
			Integer i = 0;
			for(BI_Solicitud_envio_productos_y_servicios__c sol:news){
				if(sol.BI_Estado_de_solicitud__c == 'Cancelado' && olds[i].BI_Estado_de_solicitud__c != 'Cancelado'){
					set_acc.add(sol.BI_Cliente__c);
					set_solicitudes.add(sol.Id);
				}			
				i++;
				
			}
			system.debug('#Debug set_acc'+ set_acc);
			system.debug('#Debug set_solicitudes'+ set_solicitudes);
			if(!set_acc.IsEmpty() && !set_solicitudes.IsEmpty()){
				List <BI_Linea_de_Recambio__c> lst_rec = [SELECT Id FROM BI_Linea_de_Recambio__c WHERE BI_Solicitud__c IN :set_solicitudes];
				for(BI_Linea_de_Recambio__c rec:lst_rec){
					set_recambios.add(rec.Id);
				}
				 system.debug('#Debug set_recambios'+ set_recambios);

				List <BI_Linea_de_Venta__c> lst_ven = [SELECT Id FROM BI_Linea_de_Venta__c WHERE BI_Solicitud__c IN :set_solicitudes];
				for(BI_Linea_de_Venta__c ven:lst_ven){
					set_ventas.add(ven.Id);
				}
				 system.debug('#Debug set_ventas'+ set_ventas);

				if(!set_ventas.IsEmpty() || !set_recambios.IsEmpty()){
					List <BI_Transaccion_de_bolsa_de_dinero__c> lst_bolsa = [SELECT Id, BI_Codigo_de_bolsa_de_dinero__c, BI_Linea_de_recambio__c, BI_Linea_de_venta__c, BI_Movimiento__c 
																			FROM BI_Transaccion_de_bolsa_de_dinero__c WHERE BI_Linea_de_recambio__c IN :set_recambios OR BI_Linea_de_venta__c IN :set_ventas];
				 	system.debug('#Debug lst_bolsa'+ lst_bolsa);					
					List <BI_Transaccion_de_bolsa_de_dinero__c> lst_trans = new List <BI_Transaccion_de_bolsa_de_dinero__c>();
					for(BI_Transaccion_de_bolsa_de_dinero__c bolsa:lst_bolsa){			
						BI_Transaccion_de_bolsa_de_dinero__c transNew = new BI_Transaccion_de_bolsa_de_dinero__c (BI_Codigo_de_bolsa_de_dinero__c = bolsa.BI_Codigo_de_bolsa_de_dinero__c,
																												BI_Fecha_de_creacion__c = date.today(),
																												BI_Linea_de_recambio__c = bolsa.BI_Linea_de_recambio__c,
																												BI_Linea_de_venta__c = bolsa.BI_Linea_de_venta__c,
																												BI_Movimiento__c = bolsa.BI_Movimiento__c * (-1));
						lst_trans.add(transNew);

					}
					if(!lst_trans.IsEmpty())
						insert lst_trans;
				}
			}
		}catch (exception exc){
			BI_LogHelper.generate_BILog('BI_SolicitudEnvioMethods.rejectedRequestTransactions', 'BI_EN', exc, 'Trigger');
		}	
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Aborda.es
    Description:   Method to return "descuentos para modelo" from "Líneas de Venta" and "Líneas de Recambio"
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:		
    
    <Date>            <Author>          <Description>
    20/11/2015        Guillermo Muñoz   Initial version
    17/02/2016		  Guillermo Muñoz   BI_TestUtils.isRunningTest added for upgrade the code coverage
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void returnModelDiscounts(List <BI_Solicitud_envio_productos_y_servicios__c> news, List <BI_Solicitud_envio_productos_y_servicios__c> olds){
		try{

			if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
    		}

			List <Id> toProcess = new List <Id>();

			for(Integer i = 0;i<news.size();i++){

				if(news[i].BI_Estado_de_solicitud__c != olds[i].BI_Estado_de_solicitud__c && news[i].BI_Estado_de_solicitud__c == 'Cancelado'){
					toProcess.add(news[i].Id);
				}
			}

			if(!toProcess.isEmpty()){
				BI_DevolverDescuentoModelo_JOB.executeJobDevolverDescuentos(toProcess);
			}

		}
		catch (exception exc){
			BI_LogHelper.generate_BILog('BI_SolicitudEnvioMethods.returnModelDiscounts', 'BI_EN', exc, 'Trigger');
		}	

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Method that prevent the modification of the solicitud by the AM if the solicitud have been sent for approval
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    17/02/2016                      Guillermo Muñoz             Initial version
	    22/02/2016                      Guillermo Muñoz             Changes in the validation criteria
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void preventModification(List <BI_Solicitud_envio_productos_y_servicios__c> news, List <BI_Solicitud_envio_productos_y_servicios__c> olds){

		try{

			if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
    		}

			BI_bypass__c bypass = BI_bypass__c.getInstance();
	    	if (!bypass.BI_migration__c){
				List <GroupMember> lst_gm = [SELECT UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'BI_CHI_Ejecutivos_de_Adm_Ventas'];
		        Set <Id> set_usuId= new Set <Id>();
		        for(GroupMember gm : lst_gm){
		            set_usuId.add(gm.UserOrGroupId);
		        }

		        for(BI_Solicitud_envio_productos_y_servicios__c sol : news){

		        	if(sol.OwnerId == UserInfo.getUserId() && !set_usuId.contains(UserInfo.getUserId())){
		        		if((sol.BI_Estado_de_ventas__c != 'Vacío' && sol.BI_Estado_de_ventas__c != 'Visando') || (sol.BI_Estado_de_servicios__c != 'Vacío' && sol.BI_Estado_de_servicios__c != 'Visando')){
		        			sol.addError('No tiene permisos para modificar la solicitud');
		        		}
		        	}
		        }
	    	}
    	}catch(Exception exc){
    		BI_LogHelper.generate_BILog('BI_SolicitudEnvioMethods.preventModification', 'BI_EN', exc, 'Trigger');
    	}
	}
}