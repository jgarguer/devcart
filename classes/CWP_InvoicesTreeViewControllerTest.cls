@isTest
public with sharing class CWP_InvoicesTreeViewControllerTest {
    
     @testSetup 
    private static void dataModelSetup() {          
       
        //USER
        UserRole rolUsuario;
        User unUsuario;
        User otroUsuario;
        Profile profileTGS;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            profileTGS = CWP_TestDataFactory.getProfile('TGS System Administrator');
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, profileTGS.Id);
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
            //ACCOUNT
            map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(new User(id=userInfo.getUserId())){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
        
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        Contact contactTest1 = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest1');
        
        contactTest.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest;
        contactTest1.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest1;
        
        User usuario;
        User usuarioNoTGS;
        
         //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 't1';
        producto.TGS_CWP_Tier_2__c = 't2';
        insert producto;
        list<NE__Catalog_Item__c> ciList = new list<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
        ciList.add(catalogoItem);
        
        NE__Catalog_Item__c catalogoItem1 = CWP_TestDataFactory.createCatalogoItem('ProductoTB', catalogCategoryChildren.id, catalogo.id, producto.id);
        catalogoItem1.NE__Technical_Behaviour__c = 'Service with pre-approved changes';
        ciList.add(catalogoItem1);
        insert ciList;
        
        NE__Contract_Header__c newCH = new NE__Contract_Header__c(
            NE__name__c = 'theContract'
        );
        insert newCH;
        
        NE__Contract_Account_Association__c newCAA = new NE__Contract_Account_Association__c(
            NE__Contract_Header__c = newCH.id,
            NE__Account__c = accLegalEntity.id
        );
        insert newCAA;
        
        NE__Contract__c newC =new  NE__Contract__c(
            NE__Contract_Header__c = newCH.id
        );
        insert newC;
        
        NE__Contract_Line_Item__c newCII = new NE__Contract_Line_Item__c(
            NE__Commercial_Product__c = producto.id,
            NE__Contract__c = newC.id
        );
        insert newCII;
        
        
        
        
          //ORDER
        
        NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        insert testOrder;
        //createOrderItem(id accountId, id orderId, id productId, id catalogItem, integer quantity){
        NE__OrderItem__c newOI;
        list<NE__OrderItem__c> oiList = new list<NE__OrderItem__c>();
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
        oiList.add(newOI);
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[1].id, 1);
        newOI.NE__City__c = 'Gondor';
        newOI.NE__city__c = 'Middle-earth';
        oiList.add(newOI);
        insert oiList;
        
        
        //CASE
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        insert newCase;       
        
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');       
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        /*System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil2 = CWP_TestDataFactory.getProfile('BI_Customer Communities');       
            usuarioNoTGS = CWP_TestDataFactory.createUser('nombre3', null, perfil2.id);
            usuarioNoTGS.contactId = contactTest1.id;
            insert usuarioNoTGS;
        }*/
         
        CaseComment Comment = new CaseComment(ParentId = newCase.Id, CommentBody = 'new comment');
        insert Comment;
        
         Market_Place_PP__c newMarketplace = new Market_Place_PP__c();
        insert newMarketplace;
     
     }
     
    
    @isTest public static void getDetailsTest() {
        User usuario = [SELECT id,AccountId FROM User WHERE FirstName =: 'nombre1'];  
		BI_TestUtils.throw_exception=false;       
        System.runAs(usuario){                          
            Test.startTest();             
        
            generateData(usuario.AccountId);
            
            CWP_InvoicesTreeViewController.getDetails(usuario.AccountId, '201305', 'Rama2', 'Fam2', 'Fam22', 'FCIC');   
            Test.stopTest();        
        }

    }
    
    @isTest public static void testInit() {
        Account acc = [SELECT Id FROM Account WHERE Name = 'holding1' LIMIT 1];
        BI_TestUtils.throw_exception=false;
        generateData(acc.Id);
        //generateData('0012500000dVH59');
        CWP_InvoicesTreeViewController.init();
        
        
    }
    
    @isTest public static void testFetchInvoices() {
        //generateData('0012500000dVH59');
        Account acc = [SELECT Id FROM Account WHERE Name = 'holding1' LIMIT 1];
        BI_TestUtils.throw_exception=false;
        generateData(acc.Id);
        List<BI_Cobranza__c> invoiceList = [Select  Id, Aggregation_Level__c, BI_YearMonth__c, 
                                                    BI_ID_Fiscal__c, BI_Rama__c, BI_Familia_1__c, 
                                                    BI_Familia_2__c, BI_Tipo_Facturacion__c, BI_FCIC_Amount__c, BI_FCON_Amount__c,
                                                    BI_Codigo_concepto__c, BI_Descripcion_concepto__c,
                                                    Aggregation_Type__c
                                             from   BI_Cobranza__c 
                                            where   BI_Clientes__c=: acc.Id and Active__c=true and 
                                                    Aggregation_Type__c IN ('1-3M', '4-13M') and
                                                    BI_Tipo_Facturacion__c = 'FCON'
                                         order by   BI_YearMonth__c desc, BI_Rama__c Asc, BI_Familia_1__c Asc, BI_Familia_2__c Asc, BI_Tipo_Facturacion__c Asc LIMIT 1000];
        CWP_InvoicesTreeViewController.fetchInvoices(acc.Id,'FCON');
    }

    @isTest public static void testGetPreviousMonthKey() {
        //generateData('0012500000dVH59');
        BI_TestUtils.throw_exception=false;
        Account acc = [SELECT Id FROM Account WHERE Name = 'holding1' LIMIT 1];
        generateData(acc.Id);
        CWP_InvoicesTreeViewController.getPreviousMonthKey('201608',-1);
    }
    
    private static void generateData(Id accountId) {
		BI_TestUtils.throw_exception=false;
        List<BI_Cobranza__c> items = new List<BI_Cobranza__c>();

        String year = '2013';

        for (Integer i = 0; i < 12; i++) {
            String month = ''+i;

            if (i < 9) {
                month = '0'+i;
            }

            BI_Cobranza__c bi = new BI_Cobranza__c();
            bi.BI_YearMonth__c = year+month;
            bi.BI_Clientes__c = accountId;
            bi.Active__c = true;
            bi.Aggregation_Level__c = 'Level0';
            bi.BI_Tipo_Facturacion__c = 'FCON';


            String aggType;
            if (i < 9) {
                aggType = '4-13M';
                bi.BI_Rama__c = 'Rama2';
                bi.BI_Familia_1__c = 'Fam2';
                bi.BI_Familia_2__c = 'Fam22';
            } else {
                aggType = '1-3M';
                bi.BI_Rama__c = 'Rama1';
                bi.BI_Familia_1__c = 'Fam1';
                bi.BI_Familia_2__c = 'Fam11';
            }
            bi.Aggregation_Type__c = aggType;
            items.add(bi);
        }

        for (Integer i = 0; i < 12; i++) {
            String month = ''+i;

            if (i < 9) {
                month = '0'+i;
            }

            BI_Cobranza__c bi = new BI_Cobranza__c();
            bi.BI_YearMonth__c = year+month;
            bi.BI_Clientes__c = accountId;
            bi.Active__c = true;
            bi.Aggregation_Level__c = 'Level1';
            bi.BI_Tipo_Facturacion__c = 'FCON';

            String aggType;
            if (i < 9) {
                aggType = '4-13M';
                bi.BI_Rama__c = 'Rama2';
                bi.BI_Familia_1__c = 'Fam2';
                bi.BI_Familia_2__c = 'Fam22';
            } else {
                aggType = '1-3M';
                bi.BI_Rama__c = 'Rama1';
                bi.BI_Familia_1__c = 'Fam1';
                bi.BI_Familia_2__c = 'Fam11';
            }
            bi.Aggregation_Type__c = aggType;
            items.add(bi);
        }

        for (Integer i = 0; i < 12; i++) {
            String month = ''+i;

            if (i < 9) {
                month = '0'+i;
            }

            BI_Cobranza__c bi = new BI_Cobranza__c();
            bi.BI_YearMonth__c = year+month;
            bi.BI_Clientes__c = accountId;
            bi.Active__c = true;
            bi.Aggregation_Level__c = 'Level2';
            bi.BI_Tipo_Facturacion__c = 'FCON';

            String aggType;
            if (i < 9) {
                aggType = '4-13M';
                bi.BI_Rama__c = 'Rama2';
                bi.BI_Familia_1__c = 'Fam2';
                bi.BI_Familia_2__c = 'Fam22';
            } else {
                aggType = '1-3M';
                bi.BI_Rama__c = 'Rama1';
                bi.BI_Familia_1__c = 'Fam1';
                bi.BI_Familia_2__c = 'Fam11';
            }
            bi.Aggregation_Type__c = aggType;
            items.add(bi);
        }

        for (Integer i = 0; i < 12; i++) {
            String month = ''+i;

            if (i < 9) {
                month = '0'+i;
            }

            BI_Cobranza__c bi = new BI_Cobranza__c();
            bi.BI_YearMonth__c = year+month;
            bi.BI_Clientes__c = accountId;
            bi.Active__c = true;
            bi.Aggregation_Level__c = 'Level3';
            bi.BI_Tipo_Facturacion__c = 'FCON';

            String aggType;
            if (i < 9) {
                aggType = '4-13M';
                bi.BI_Rama__c = 'Rama2';
                bi.BI_Familia_1__c = 'Fam2';
                bi.BI_Familia_2__c = 'Fam22';
            } else {
                aggType = '1-3M';
                bi.BI_Rama__c = 'Rama1';
                bi.BI_Familia_1__c = 'Fam1';
                bi.BI_Familia_2__c = 'Fam11';
            }
            bi.Aggregation_Type__c = aggType;
            items.add(bi);
        }

        for (Integer i = 0; i < 12; i++) {
            String month = ''+i;

            if (i < 9) {
                month = '0'+i;
            }

            BI_Cobranza__c bi = new BI_Cobranza__c();
            bi.BI_YearMonth__c = year+month;
            bi.BI_Clientes__c = accountId;
            bi.Active__c = true;
            bi.Aggregation_Level__c = 'Level4';
            bi.BI_Tipo_Facturacion__c = 'FCON';
            bi.BI_Codigo_concepto__c = 'Conc111';
            bi.BI_Descripcion_concepto__c = 'Desc111';

            String aggType;
            if (i < 9) {
                aggType = '4-13M';
                bi.BI_Rama__c = 'Rama2';
                bi.BI_Familia_1__c = 'Fam2';
                bi.BI_Familia_2__c = 'Fam22';
            } else {
                aggType = '1-3M';
                bi.BI_Rama__c = 'Rama1';
                bi.BI_Familia_1__c = 'Fam1';
                bi.BI_Familia_2__c = 'Fam11';
            }
            bi.Aggregation_Type__c = aggType;
            items.add(bi);
        }
        system.debug('items ' +items);
        insert items;
    }
}