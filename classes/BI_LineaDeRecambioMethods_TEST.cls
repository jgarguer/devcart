@isTest
private class BI_LineaDeRecambioMethods_TEST {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando González
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_LineaDeRecambioMethods class
    
    History: 
    <Date> 					<Author> 				<Change Description>
    27/03/2015      		Fernando González    	Initial Version		
    18/12/2015              Guillermo Muñoz         Fix bugs caused by news validations rules
    20/09/2017              Angel F. Santaliestra   Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    static{
        
        BI_TestUtils.throw_exception = false;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Fernando González
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_LineaDeRecambioMethods
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	27/03/2015      		Fernando González    	Initial Version	
    18/12/2015              Guillermo Muñoz         Fix bugs caused by news validations rules 
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
 	
    static testMethod void lineaDeRecambioTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
		list<BI_Linea_de_Recambio__c> lstRecambio = new list<BI_Linea_de_Recambio__c>();
		list<BI_Linea_de_Recambio__c> lstRecambioSolicitud = new list<BI_Linea_de_Recambio__c>();
		list<BI_Linea_de_Servicio__c> lstLineaServicio = new list<BI_Linea_de_Servicio__c>();

   		
		List <Recordtype> lst_rtServicio = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Servicio' AND sObjectType = 'BI_Modelo__c'];
		
		List <Recordtype> lst_rt = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Modelo' AND sObjectType = 'BI_Modelo__c'];

		Account acc = new Account(Name = 'test Account',
							BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
							BI_Activo__c = Label.BI_Si,
							BI_Holding__c = 'No',
							BI_Segment__c = 'Empresas',
                            BI_Subsegment_Regional__c = 'Mayoristas',
                            BI_Subsector__c = 'Banca',
                            BI_Sector__c = 'Industria',
							BI_Tipo_de_identificador_fiscal__c = 'RFC',
                            BI_Territory__c = 'test',
							BI_No_Identificador_fiscal__c = 'VECB380326XXX');
		        
		insert acc;

		
		BI_Modelo__c modelo2 = new BI_Modelo__c(Name = 'test modelo2' ,RecordTypeId = lst_rtServicio[0].Id);
		insert modelo2;

		BI_Solicitud_envio_productos_y_servicios__c solicitud = new BI_Solicitud_envio_productos_y_servicios__c(BI_Tipo_solicitud__c = 'test solicitud',
		    															BI_Cliente__c = acc.Id,
                                                                        BI_Lineas_de_recambio__c = true,
                                                                        BI_Estado_de_recambios__c = 'Aprobado');
		insert solicitud;
		
		lstRecambio = BI_DataLoadAgendamientos.loadLineaRecambio(1 ,solicitud.Id ,acc);
		
		test.startTest();
		
		for ( BI_Linea_de_Recambio__c rec : lstRecambio ){
			
			BI_Linea_de_Servicio__c lineaServicio = new BI_Linea_de_Servicio__c();
			lineaServicio.BI_Accion__c = 'test Action';
			lineaServicio.BI_Estado_de_linea_de_servicio__c = 'test Servicio';
			lineaServicio.BI_Modelo__c = modelo2.Id;
			lineaServicio.BI_Linea_de_recambio__c = rec.Id;
			
			lstLineaServicio.Add(lineaServicio);
		}
		
		insert lstLineaServicio;
		
		BI_DataLoadAgendamientos.UpdateLineaRecambio(lstRecambio);
		
		test.stopTest();
		
		
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando González
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_LineaDeRecambioMethods
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    27/03/2015              Fernando González       Initial Version 
    18/12/2015              Guillermo Muñoz         Fix bugs caused by news validations rules 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void lineaDeRecambioTest2()
    {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
		list<BI_Linea_de_Recambio__c> lstRecambio = new list<BI_Linea_de_Recambio__c>();
		list<BI_Linea_de_Recambio__c> lstRecambioSolicitud = new list<BI_Linea_de_Recambio__c>();
		list<BI_Linea_de_Servicio__c> lstLineaServicio = new list<BI_Linea_de_Servicio__c>();
   		
		List <Recordtype> lst_rtServicio = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Servicio' AND sObjectType = 'BI_Modelo__c'];
		
		List <Recordtype> lst_rt = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Modelo' AND sObjectType = 'BI_Modelo__c'];

		Account acc = new Account(Name = 'test Account',
							BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
							BI_Activo__c = Label.BI_Si,
							BI_Holding__c = 'No',
							BI_Segment__c = 'Empresas',
                      	  	BI_Subsegment_Regional__c = 'Mayoristas',
                          	BI_Subsector__c = 'Banca',
                          	BI_Sector__c = 'Industria',
							BI_Tipo_de_identificador_fiscal__c = 'RFC',
                            BI_Territory__c = 'test',
							BI_No_Identificador_fiscal__c = 'VECB380326XXX');
		        
		insert acc;

		
		BI_Modelo__c modelo2 = new BI_Modelo__c(Name = 'test modelo2', RecordTypeId = lst_rtServicio[0].Id);
        BI_Modelo__c modeloSIM = new BI_Modelo__c(Name = 'test modelo sim', RecordTypeId = lst_rt[0].Id,  BI_Familia_Local__c = 'SIMCARD');
		insert new List<BI_Modelo__c>{modelo2, modeloSIM};

		BI_Solicitud_envio_productos_y_servicios__c solicitud = new BI_Solicitud_envio_productos_y_servicios__c(BI_Tipo_solicitud__c = 'test solicitud',
		    															BI_Cliente__c = acc.Id,
                                                                        BI_Lineas_de_recambio__c = true,
                                                                        BI_Estado_de_recambios__c = 'Aprobado');
		insert solicitud;
		
		//lstRecambio = BI_DataLoadAgendamientos.loadLineaRecambio(1 ,solicitud.Id ,acc);
        BI_Linea_de_Recambio__c recambio = new BI_Linea_de_Recambio__c();
        recambio.BI_Solicitud__c = solicitud.Id;
        //recambio.BI_Modelo__c = modeloSIM.Id;
        recambio.BI_Modelo_TEM__c = String.valueOf(modeloSIM.Id);
        recambio.BI_Precio_final_de_modelo__c = 210;
        recambio.BI_Precio_final_de_SIM__c = 100 ;
        recambio.BI_Estado__c = 'test estado';
        recambio.BI_Razon_para_recambio__c = 'test razon';
        recambio.BI_Telefono_de_contacto__c = '666563221';
        recambio.BI_Modalidad_de_venta__c = 'test modalidad';
        recambio.BI_Tipo_de_contrato__c = 'test tipo contrato';
        recambio.BI_Plan__c = 'test plan';
        recambio.BI_Se_incluye_tarjeta_SIM__c = 'No';
        recambio.BI_ID_de_simcard_TEM__c = String.valueOf(modeloSIM.Id);

        BI_Linea_de_Recambio__c recambio2 = new BI_Linea_de_Recambio__c();
        recambio2.BI_Solicitud__c = solicitud.Id;
        //recambio.BI_Modelo__c = modeloSIM.Id;
        recambio2.BI_Modelo_TEM__c = String.valueOf(modeloSIM.Id);
        recambio2.BI_Precio_final_de_modelo__c = 210;
        recambio2.BI_Precio_final_de_SIM__c = 100 ;
        recambio2.BI_Estado__c = 'test estado';
        recambio2.BI_Razon_para_recambio__c = 'razón';
        recambio2.BI_Estado_de_linea_de_recambio__c = 'test estado linea';
        recambio2.BI_Telefono_de_contacto__c = '666563221';
        recambio2.BI_Modalidad_de_venta__c = 'test modalidad';
        recambio2.BI_Tipo_de_contrato__c = 'test tipo contrato';
        recambio2.BI_Plan__c = 'test plan';
        recambio2.BI_Se_incluye_tarjeta_SIM__c = 'No';
        recambio2.BI_ID_de_simcard_TEM__c = String.valueOf(modeloSIM.Id);
        
        Test.startTest();
        
        insert new List<BI_Linea_de_Recambio__c>{recambio, recambio2};

		BI_Linea_de_Servicio__c lineaServicio = new BI_Linea_de_Servicio__c();
		lineaServicio.BI_Accion__c = 'test Action';
		lineaServicio.BI_Estado_de_linea_de_servicio__c = 'test Servicio';
		lineaServicio.BI_Modelo__c = modelo2.Id;
		lineaServicio.BI_Linea_de_recambio__c = recambio2.Id;
		
		insert lineaServicio;
		
		delete recambio;
		
		try
		{
			delete recambio2;
		}catch(Exception e)
		{
			System.assert(e.getMessage().contains('No se puede Eliminar porque no se encuentra en estado Pendiente'));
		}
			

		
		//BI_DataLoadAgendamientos.UpdateLineaRecambio(lstRecambio);
		
		Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando González
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_LineaDeRecambioMethods
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    27/03/2015              Fernando González       Initial Version 
    18/12/2015              Guillermo Muñoz         Fix bugs caused by news validations rules 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void lineaDeRecambioTest3()
    {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
		list<BI_Linea_de_Recambio__c> lstRecambio = new list<BI_Linea_de_Recambio__c>();
		list<BI_Linea_de_Recambio__c> lstRecambioSolicitud = new list<BI_Linea_de_Recambio__c>();
		list<BI_Linea_de_Servicio__c> lstLineaServicio = new list<BI_Linea_de_Servicio__c>();
   		
		List <Recordtype> lst_rtServicio = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Servicio' AND sObjectType = 'BI_Modelo__c'];
		
		List <Recordtype> lst_rt = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Modelo' AND sObjectType = 'BI_Modelo__c'];

		Account acc = new Account(Name = 'test Account',
							BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
							BI_Activo__c = Label.BI_Si,
							BI_Holding__c = 'No',
							BI_Segment__c = 'Empresas',
                      	  	BI_Subsegment_Regional__c = 'Mayoristas',
                          	BI_Subsector__c = 'Banca',
                          	BI_Sector__c = 'Industria',
							BI_Tipo_de_identificador_fiscal__c = 'RFC',
                            BI_Territory__c = 'test',
							BI_No_Identificador_fiscal__c = 'VECB380326XXX');
		        
		insert acc;

		
		BI_Modelo__c modelo2 = new BI_Modelo__c(Name = 'test modelo2', RecordTypeId = lst_rtServicio[0].Id);
        BI_Modelo__c modeloSIM = new BI_Modelo__c(Name = 'test modelo sim', RecordTypeId = lst_rt[0].Id,  BI_Familia_Local__c = 'SIMCARD');
		insert new List<BI_Modelo__c>{modelo2, modeloSIM};

		BI_Solicitud_envio_productos_y_servicios__c solicitud = new BI_Solicitud_envio_productos_y_servicios__c(BI_Tipo_solicitud__c = 'test solicitud',
		    															BI_Cliente__c = acc.Id,
                                                                        BI_Lineas_de_recambio__c = true,
                                                                        BI_Estado_de_recambios__c = 'Aprobado');
		insert solicitud;
		
		//lstRecambio = BI_DataLoadAgendamientos.loadLineaRecambio(1 ,solicitud.Id ,acc);
        BI_Linea_de_Recambio__c recambio = new BI_Linea_de_Recambio__c();
        recambio.BI_Solicitud__c = solicitud.Id;
        //recambio.BI_Modelo__c = modeloSIM.Id;
        recambio.BI_Modelo_TEM__c = String.valueOf(modeloSIM.Id);
        recambio.BI_Precio_final_de_modelo__c = 210;
        recambio.BI_Precio_final_de_SIM__c = 100 ;
        recambio.BI_Estado__c = 'test estado';
        recambio.BI_Razon_para_recambio__c = 'test razon';
        recambio.BI_Telefono_de_contacto__c = '666563221';
        recambio.BI_Modalidad_de_venta__c = 'test modalidad';
        recambio.BI_Tipo_de_contrato__c = 'test tipo contrato';
        recambio.BI_Plan__c = 'test plan';
        recambio.BI_Se_incluye_tarjeta_SIM__c = 'No';
        recambio.BI_ID_de_simcard_TEM__c = String.valueOf(modeloSIM.Id);
        recambio.BI_Estado_de_linea_de_recambio__c = 'Entregado';

        BI_Linea_de_Recambio__c recambio2 = new BI_Linea_de_Recambio__c();
        recambio2.BI_Solicitud__c = solicitud.Id;
        //recambio.BI_Modelo__c = modeloSIM.Id;
        recambio2.BI_Modelo_TEM__c = String.valueOf(modeloSIM.Id);
        recambio2.BI_Precio_final_de_modelo__c = 210;
        recambio2.BI_Precio_final_de_SIM__c = 100 ;
        recambio2.BI_Estado__c = 'test estado';
        recambio2.BI_Razon_para_recambio__c = 'razón';
        recambio2.BI_Estado_de_linea_de_recambio__c = 'test estado linea';
        recambio2.BI_Telefono_de_contacto__c = '666563221';
        recambio2.BI_Modalidad_de_venta__c = 'test modalidad';
        recambio2.BI_Tipo_de_contrato__c = 'test tipo contrato';
        recambio2.BI_Plan__c = 'test plan';
        recambio2.BI_Se_incluye_tarjeta_SIM__c = 'No';
        recambio2.BI_ID_de_simcard_TEM__c = String.valueOf(modeloSIM.Id);
        recambio2.BI_Estado_de_linea_de_recambio__c = 'Entregado';
        
        List<BI_Linea_de_Recambio__c> lst_rec = new List<BI_Linea_de_Recambio__c>();
        
        lst_rec.add(recambio);
        lst_rec.add(recambio2);
        
        insert lst_rec;
        
        Test.startTest();
        
        recambio.BI_Estado_de_linea_de_recambio__c = 'Activado';
        recambio2.BI_Estado_de_linea_de_recambio__c = 'Desactivado';
        
        update lst_rec;
        
        recambio.BI_Estado_de_linea_de_recambio__c = 'No ejecutado';
        recambio2.BI_Estado_de_linea_de_recambio__c = 'No ejecutado';
        
        update lst_rec;
        
        recambio.BI_Estado_de_linea_de_recambio__c = 'Activado';
        //recambio.BI_Solicitud_descargada_BackOffice__c = true;
        recambio2.BI_Estado_de_linea_de_recambio__c = 'Activado';
        //recambio2.BI_Solicitud_descargada_BackOffice__c = true;
        
        update lst_rec;
        
		Test.stopTest();
    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}