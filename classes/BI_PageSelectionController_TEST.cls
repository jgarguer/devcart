@isTest //(seeAllData=true)
private class BI_PageSelectionController_TEST
{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_PageSelectionController class
    
    History: 
    <Date> 					<Author> 				<Change Description>
    27/03/2015      		Fernando Arteaga    	Initial Version		
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    static{
        
        BI_TestUtils.throw_exception = false;
    }
	
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Fernando Arteaga
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_PageSelectionController
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	27/03/2015      		Fernando Arteaga    	Initial Version	
 	03/02/2016              Guillermo Muñoz         Fix bugs caused by news validations rules
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void TestPageSelectionLineaVenta()
    {
    	User ActualUser = [ SELECT Id FROM User WHERE Id = :UserInfo.getUserId() ];
    	User usu = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema AND Id !=: UserInfo.getUserId() LIMIT 1];
    	
    		
    		
	    List<BI_Linea_de_Venta__c> lstNewLineaVenta = new List<BI_Linea_de_Venta__c>();

		System.runAs(usu){	
			lstNewLineaVenta = BI_DataLoadAgendamientos.loadLineaVentaWithSolicitud(1);
		}

		System.runAs( ActualUser ){
			Test.startTest();
			
			PageReference p = Page.BI_PageSelectionLineaVenta;
			p.getParameters().put('id', lstNewLineaVenta[0].Id);
			Test.setCurrentPageReference(p);
			
			ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(lstNewLineaVenta[0]);
			BI_PageSelectionController psController = new BI_PageSelectionController(controller);
			psController.doRedirect();
			
			List<PermissionSet> psList = [SELECT Id
										  FROM PermissionSet
										  WHERE Name = 'BI_CHI_Admin_Agendamientos'];
			
			if (!psList.isEmpty())
			{
				try{
					PermissionSetAssignment assignment = new PermissionSetAssignment(AssigneeId = UserInfo.getUserId(), PermissionSetId = psList[0].Id);
					insert assignment;
				}catch(Exception exc){
					system.assert(exc.getMessage().contains('DUPLICATE_VALUE')); //If permission set has been asigned, don´t throw the exception.
				}
				
				psController.doRedirect();
			}
								
			Test.stopTest();
    	}
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        -------
	    Company:       -------
	    Description:   -------
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    03/02/2016                      Guillermo Muñoz             Fix bugs caused by news validations rules
	        20/09/2017        Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void TestPageSelectionLineaRecambio()
	{
		User ActualUser = [ SELECT Id FROM User WHERE Id = :UserInfo.getUserId() ];
		
		System.runAs( ActualUser ){			
		
			List<BI_Linea_de_Recambio__c> lstRecambio = new List<BI_Linea_de_Recambio__c>();
	   		
			List <Recordtype> lst_rtServicio = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Servicio' AND sObjectType = 'BI_Modelo__c'];
			
			String op1 = 'RFC';
		   		
			List <Recordtype> lst_rt = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Modelo' AND sObjectType = 'BI_Modelo__c'];
	
			Account acc = new Account(Name = 'test Account',
								BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
								BI_Activo__c = Label.BI_Si,
								BI_Holding__c = 'No',
								BI_Segment__c = 'Empresas',
                          	  	BI_Subsegment_Regional__c = 'Mayoristas',
                              	BI_Subsector__c = 'Banca',
                              	BI_Sector__c = 'Industria',
								BI_Tipo_de_identificador_fiscal__c = op1,
								BI_No_Identificador_fiscal__c = 'VECB380326XXX', BI_Territory__c = 'test');
			        
			insert acc;
			
			BI_Modelo__c modelo2 = new BI_Modelo__c(Name = 'test modelo2' ,RecordTypeId = lst_rtServicio[0].Id);
			insert modelo2;
	
			BI_Solicitud_envio_productos_y_servicios__c solicitud = new BI_Solicitud_envio_productos_y_servicios__c(BI_Tipo_solicitud__c = 'test solicitud',
			    																									BI_Cliente__c = acc.Id,
			    																									BI_Lineas_de_recambio__c = true);
			insert solicitud;
			
			lstRecambio = BI_DataLoadAgendamientos.loadLineaRecambio(1, solicitud.Id, acc);
		
			Test.startTest();

			PageReference p = Page.BI_PageSelectionLineaRecambio;
			p.getParameters().put('id', lstRecambio[0].Id);
			Test.setCurrentPageReference(p);
			
			ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(lstRecambio[0]);
			BI_PageSelectionController psController = new BI_PageSelectionController(controller);
			psController.doRedirect();
			
			List<PermissionSet> psList = [SELECT Id
										  FROM PermissionSet
										  WHERE Name = 'BI_CHI_Admin_Agendamientos'];
			
			if (!psList.isEmpty())
			{
				try{
					PermissionSetAssignment assignment = new PermissionSetAssignment(AssigneeId = UserInfo.getUserId(), PermissionSetId = psList[0].Id);
					insert assignment;
				}catch(Exception exc){
					system.assert(exc.getMessage().contains('DUPLICATE_VALUE')); //If permission set has been asigned, don´t throw the exception.
				}
				psController.doRedirect();
			}
			
			Test.stopTest();
		}
	}

	static testMethod void TestPageSelectionLineaServicio()
	{
		User ActualUser = [ SELECT Id FROM User WHERE Id = :UserInfo.getUserId() ];
		
		System.runAs( ActualUser ){
		
			List<BI_Linea_de_Servicio__c> lstNewLineaServicio = new list<BI_Linea_de_Servicio__c>();
			List<BI_Linea_de_Venta__c> lstNewLineaVenta = new List<BI_Linea_de_Venta__c>();
			
			lstNewLineaServicio = BI_DataLoadAgendamientos.loadLineaServicio(1);
				
			PageReference p = Page.BI_PageSelectionLineaServicio;
			p.getParameters().put('id', lstNewLineaServicio[0].Id);
			Test.setCurrentPageReference(p);

			Test.startTest();
			
			ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(lstNewLineaServicio[0]);
			BI_PageSelectionController psController = new BI_PageSelectionController(controller);
			psController.doRedirect();
			
			List<PermissionSet> psList = [SELECT Id
										  FROM PermissionSet
										  WHERE Name = 'BI_CHI_Admin_Agendamientos'];
			
			if (!psList.isEmpty())
			{
				try{
					PermissionSetAssignment assignment = new PermissionSetAssignment(AssigneeId = UserInfo.getUserId(), PermissionSetId = psList[0].Id);
					insert assignment;
				}catch(Exception exc){
					system.assert(exc.getMessage().contains('DUPLICATE_VALUE')); //If permission set has been asigned, don´t throw the exception.
				}
				psController.doRedirect();
			}
			
			Test.stopTest();			
		}			
		
	}
	
	static testMethod void TestPageSelectionSolicitudEnvio()
	{
        User ActualUser = [ SELECT Id FROM User WHERE Id = :UserInfo.getUserId() ];
		
		System.runAs( ActualUser ){			
		
        	List<BI_Solicitud_envio_productos_y_servicios__c> lstNewSolicitudEnvio = new List<BI_Solicitud_envio_productos_y_servicios__c>();
		
			Test.startTest();
			
			lstNewSolicitudEnvio = BI_DataLoadAgendamientos.loadSolicitudEnvio(1, null, true);
			
			PageReference p = Page.BI_PageSelectionSolicitudEnvio;
			p.getParameters().put('id', lstNewSolicitudEnvio[0].Id);
			Test.setCurrentPageReference(p);
			
			ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(lstNewSolicitudEnvio[0]);
			BI_PageSelectionController psController = new BI_PageSelectionController(controller);
			psController.doRedirect();
			
			List<PermissionSet> psList = [SELECT Id
										  FROM PermissionSet
										  WHERE Name = 'BI_CHI_Admin_Agendamientos'];
			
			if (!psList.isEmpty())
			{
				try{
					PermissionSetAssignment assignment = new PermissionSetAssignment(AssigneeId = UserInfo.getUserId(), PermissionSetId = psList[0].Id);
					insert assignment;
				}catch(Exception exc){
					system.assert(exc.getMessage().contains('DUPLICATE_VALUE')); //If permission set has been asigned, don´t throw the exception.
				}
				psController.doRedirect();
			}
			
			Test.stopTest();
		
		}
	}
}