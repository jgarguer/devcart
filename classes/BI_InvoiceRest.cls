@RestResource(urlMapping='/accountresources/v1/invoices/*')
global class BI_InvoiceRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Invoice Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    21/08/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains basic information stored in the server for a specific invoice.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.InvoiceInfoType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    21/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.InvoiceInfoType getInvoice() {
		
		BI_RestWrapper.InvoiceInfoType response;
		
		try{
			
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
				
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
				
				//ONE INVOICE
				response = BI_RestHelper.getOneInvoice(RestContext.request.requestURI.split('/')[4], RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
				
				RestContext.response.statuscode = (response == null)?404:200;//404 NOT_FOUND, 200 OK
				
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_InvoiceRest.getInvoice', 'BI_EN', exc, 'Web Service');
			
		}
		
		return response;
		
	}
	
}