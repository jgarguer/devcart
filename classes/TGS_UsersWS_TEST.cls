@isTest()
public class TGS_UsersWS_TEST {

    static Account holding;
    static Account customerCountry;
    static Account businessUnit;
    static Account legalEntity;
    static Profile p;
    static Group madridQueue;
    static Group miamiQueue;
    static Group emeaQueue;
    
    private static void setConfiguration() {
       //Test.startTest();
        legalEntity = TGS_Dummy_Test_Data.dummyHierarchy();
        legalEntity.TGS_IntegrationID__c = '123456LE';
        update legalEntity;
        
        List<Account> listAcc = TGS_Portal_Utils.getLevel1(legalEntity.Id, 1);
        if(listAcc.size()>0){
            holding = listAcc[0];
            holding.TGS_IntegrationID__c = '123456HO';
            update holding;
        }
        listAcc = TGS_Portal_Utils.getLevel2(legalEntity.Id, 1);
        if(listAcc.size()>0){
            customerCountry = listAcc[0];
            customerCountry.TGS_IntegrationID__c = '123456CC';
            update customerCountry;
        }
        listAcc = TGS_Portal_Utils.getLevel4(legalEntity.Id, 1);
        if(listAcc.size()>0){
            businessUnit = listAcc[0];
            businessUnit.TGS_IntegrationID__c = '123456BU';
            update businessUnit;
            TGS_Dummy_Test_Data.dummyPuntoInstalacion(businessUnit.Id);
        }
        
        p = [SELECT Id, Name FROM Profile WHERE Name = 'TGS Customer Community Plus' LIMIT 1];
        
        madridQueue = [SELECT Id, Name FROM Group WHERE Name LIKE '%Madrid%' AND Type = 'Queue' LIMIT 1];
        miamiQueue = [SELECT Id, Name FROM Group WHERE Name LIKE '%Miami%' AND Type = 'Queue' LIMIT 1];
        emeaQueue = [SELECT Id, Name FROM Group WHERE Name LIKE '%Emea%' AND Type = 'Queue' LIMIT 1];
        
        //Test.stopTest();
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the class TGS_UsersWS
            
     <Date>                 <Author>                <Change Description>
    26/05/2015              Marta Laliena           Initial Version
    03/10/2017              Adrián Caro             updated test coverage
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createUserWS() {
		BI_TestUtils.throw_exception = false;

        Account acc = TGS_Dummy_Test_Data.dummyHierarchy();
        
        User u2 = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u2.FederationIdentifier = '1111111111111';
        u2.TEMPEXT_ID__c        = '2222222';
        u2.Pais__c              = 'Spain';
        u2.BI_Permisos__c       = 'TGS';
        u2.EmailEncodingKey     = 'UTF-8';
        u2.BI_Clasificacion_de_Usuario__c ='TBS';
        u2.CommunityNickname ='aaaUsersWs';
        insert u2;
        Contact contact = new Contact(LastName = 'contactTest', AccountId = acc.Id, Email = u2.Email);
        insert contact; 

        User u = TGS_Dummy_Test_Data.getPortalUserFromAccount(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess,null,acc,true);

        //numero de licencias
        BI_Licencias_Pais__c lic = new BI_Licencias_Pais__c(Name = u2.BI_Clasificacion_de_Usuario__c, BI_Numero_Licencias_Usadas__c = 0, BI_Numero_Total_Licencias__c = 1);
        insert lic;

        if (p==null){setConfiguration();}

        TGS_UsersWS.create(u.FirstName, u.LastName, '', u.Username,'AU', u.Email, 'EN', true, '99999999', 'prueba', 
							'99999', '', '', '','', u.ProfileId, 'aaaaa', 'asdas','aaaa', 'dafdasfsd');   
        TGS_UsersWS.create(u.FirstName, u.LastName, '', u.Username,'SD', u.Email, 'EN', true, '99999999', 'prueba', 
							'99999', '', '', '','', u.ProfileId, 'aaaaa', 'asdas','aaaa', 'dafdasfsd');
            
        System.runAs(u2){
            if (p==null){setConfiguration();}
	        Test.startTest();
            TGS_UsersWS.create(u.FirstName, u.LastName, '', u.Username,'AU', u.Email, 'EN', true, '99999999', 'prueba',
								'99999', '', customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
								businessUnit.TGS_IntegrationID__c, p.Id, 'aaaaa', 'asdas','aaaa', 'dafdasfsd');
            TGS_UsersWS.create(u.FirstName, u.LastName, '', u.Username,'AU', u.Email, 'EN', true, '99999999', 'prueba', 
								'99999', holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
								businessUnit.TGS_IntegrationID__c, p.Id, 'aaaaa', 'asdas','aaaa', 'dafdasfsd');
            TGS_UsersWS.create(u.FirstName, u.LastName, '', u.Username,'AU', u.Email, 'EN', true, '99999999', 'prueba', 
								'99999', holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
								'', p.Id, 'aaaaa', 'asdas','aaaa', 'dafdasfsd');
            TGS_UsersWS.create(u.FirstName, u.LastName, '', u.Username,'AU', u.Email, 'EN', true, '99999999', 'prueba', 
								'99999', holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, '',
								'', p.Id, 'aaaaa', 'asdas','aaaa', 'dafdasfsd');

            TGS_UsersWS.read(u.Username);
            TGS_UsersWS.deactive(u.Username);
             
            TGS_UsersWS.create(u2.FirstName, u2.LastName, u2.Title, u2.Username, 'AU', u2.Email, 'EN', true, '99999999', 'prueba', 
								u2.Fax, holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c, 
								businessUnit.TGS_IntegrationID__c, p.Id, 'aaaaa', 'sgadygsay','aaaa', 'dafdasfsd');

            TGS_UsersWS.read(u2.Username);
            TGS_UsersWS.deactive(u2.Username);

            /*  PARAMETROS
                String FirstName, String LastName, String Title, String Username, String UserType, String Email, String Language, Boolean Vip,
                String Mobile, String LandLine, String Fax, String Company, String Country, String LegalEntity, String BusinessUnit,
                String ProfileId, String FederationId, String loginId, String Notification, String Queue
            */
            /* No username */
            TGS_UsersWS.create('a', 'a', 'a', '', 'AU', 'pruebaAna118@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, p.Id, '', 'pruebaAna117', 'All Notifications', '');
    
            /* No user type */
            TGS_UsersWS.create('a', 'a', 'a', u2.Username, '', 'pruebaAna119@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, p.Id, '', 'pruebaAna117', 'All Notifications', '');
      
            /* No first name */
            TGS_UsersWS.create('', 'a', 'a', u2.Username, 'AU', 'pruebaAna119@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, p.Id, '', 'pruebaAna117', 'All Notifications', '');
       
            /* No last name */
            TGS_UsersWS.create('a', '', 'a', u2.Username, 'AU', 'pruebaAna119@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, p.Id, '', 'pruebaAna117', 'All Notifications', '');
       
            /* No email */
            TGS_UsersWS.create('a', 'a', 'a', 'pruebaAna119@telef.es', 'AU', '', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, p.Id, '', 'pruebaAna117', 'All Notifications', '');
        
            /* Invalid username */
            TGS_UsersWS.create('a', 'a', 'a', 'pruebaAna119', 'AU', 'pruebaAna119@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, '',
                               businessUnit.TGS_IntegrationID__c, p.Id, '', 'pruebaAna117', 'All Notifications', '');
    
            /* Notifications */
            TGS_UsersWS.create('a', 'a', 'a', 'pruebaAna118@telef.es', 'AU', 'pruebaAna118@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, p.Id, '', 'pruebaAna117',
                               'Billing Inquiry;Change;Complaint;Incident;Order Management Case;Query', '');

             /* Notifications */
            TGS_UsersWS.create('a', 'a', 'a', 'pruebaAna118@telef.es', 'AU', 'pruebaAna118@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, p.Id, '', 'pruebaAna117',
                               'Billing Inquiry;Change;Complaint;Incident;Order Management Case;Query', '');
    
            /* Nickname length and languaje en */
            TGS_UsersWS.create('a', 'a', 'a', 'prueba@telef.es', 'AU', 'prueba@telef.es', 'en', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, p.Id, '', 'pruebaAna117', 'All Notifications', '');
       
            /* No profile */
            TGS_UsersWS.create('a', 'a', 'a', 'pruebaAna118@telef.es', 'AU', 'pruebaAna118@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, '', '', 'pruebaAna117', 'All Notifications', '');
      
            /* No loginId */
            TGS_UsersWS.create('a', 'a', 'a', 'pruebaAna118@telef.es', 'AU', 'pruebaAna118@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, p.Id, '', '', 'All Notifications', '');
            
            /* No business unit */
            TGS_UsersWS.create('a', 'a', 'a', 'pruebaAna118@telef.es', 'AU', 'pruebaAna118@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               '', p.Id, '', 'pruebaAna117', 'All Notifications', '');
            
            /* No business unit or legal entity */
            TGS_UsersWS.create('a', 'a', 'a', 'pruebaAna118@telef.es', 'AU', 'pruebaAna118@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, '',
                               '', p.Id, '', 'pruebaAna117', 'All Notifications', '');
    
            /* User type SD and madrid queue */
            TGS_UsersWS.create('a', 'a', 'a', 'pruebaAna118@telef.es', 'SD', 'pruebaAna118@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, '1', '', 'pruebaAna117', 'All Notifications', madridQueue.Id);
    
            /* User type SD and miami queue */
            TGS_UsersWS.create('a', 'a', 'a', 'pruebaAna118@telef.es', 'SD', 'pruebaAna118@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, '1', '', 'pruebaAna117', 'All Notifications', miamiQueue.Id);
        
            /* User type SD and prague queue */
            TGS_UsersWS.create('a', 'a', 'a', 'pruebaAna118@telef.es', 'SD', 'pruebaAna118@telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, '1', '', 'pruebaAna117', 'All Notifications', emeaQueue.Id);
            
            /* Email sin @ */
            TGS_UsersWS.create('a', 'a', 'a', 'pruebaAna118@telef.es', 'AU', 'pruebaAna118telef.es', 'es', true, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, p.Id, '', 'pruebaAna117', 'All Notifications', '');
            
            /* userName hasta @ <= 8 */
            TGS_UsersWS.create('a', 'a', 'a', 'prueba@telef.es', 'AU', 'pruebaAna118@telef.es', 'es', false, '1', '1', '1',
                               holding.TGS_IntegrationID__c, customerCountry.TGS_IntegrationID__c, legalEntity.TGS_IntegrationID__c,
                               businessUnit.TGS_IntegrationID__c, p.Id, '', 'pruebaAna117', 'All Notifications', '');
            
            TGS_UsersWS.read(u2.Username);
            TGS_UsersWS.deactive(u2.Username);
            TGS_UsersWS.read('');
            TGS_UsersWS.deactive('');
            Test.stopTest();
        }
    }
}