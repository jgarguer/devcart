public class PCA_NewCaseCtrl extends PCA_HomeController{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ana Escrich
	    Company:       Salesforce.com
	    Description:   Controller of new case page 
	    
	    History:
	    
	    <Date>            <Author>          	<Description>
	    25/06/2014        Ana Escrich      	 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public PCA_NewCaseCtrl(){
		
	}
	public PageReference checkPermissions (){
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			
			PageReference page = enviarALoginComm();
			if(page == null){
			}
			return page;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_NewCaseCtrl.checkPermissions', 'Portal Platino', Exc, 'Class');
		   return null;
		}
	}

	

}