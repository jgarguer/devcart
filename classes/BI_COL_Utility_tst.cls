/**
* Avanxo Colombia
* @author           Dolly Fierro href=<dfierro@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           		Descripción
*           -----   ----------      --------------------            		---------------
* @version   1.0    2015-08-11      Dolly Fierro (DF)					    New Test Class
*			 1.1    2017-01-27		Pedro Párraga							call method quitarCaractees()
*************************************************************************************************************/
@isTest
private class BI_COL_Utility_tst {

    static testMethod void myUnitTest() {
    	test.startTest();
    		BI_COL_Utility_cls clsBiColUtility = new BI_COL_Utility_cls();
    		clsBiColUtility.encodingMD5('cadena1');
    		clsBiColUtility.getConfInfz('Interfaz', 'tipoTransacción', 'Dirección');
    		clsBiColUtility.configCampos('Identificador');
    		BI_COL_Utility_cls.quitarCaractees('Test');
    	test.stopTest();
        
    }
}