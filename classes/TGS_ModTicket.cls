/*------------------------------------------------------------ 
Author:         Ana Cirac 
Company:        Deloitte
Description:    Class with common functions that are uses in TGS_ModTicketINC,
                TGS_ModTicketCHG and TGS_ModTicketWI
                
History
<Date>          <Author>        <Change Description>
30-Jan-2015     Ana Cirac        Initial Version
24-Jan-2015     Ana Cirac        Version 1.1
22-Sep-2015     Ana Cirac        Add updateInfoMwan
30-Aug-2016     Juan Carlos Terrón     Added  ownergroup exists checking.   Lines 166-181
06-Oct-2016     Juan Carlos Terrón     Added null temptext checking to avoid incomplete data errors.Lines 147-152.
05-Jun-2017     Álvaro López           Added info in validateProductCategorizations exceptions
09-May-2018     Iñaki Frial            Added condition in validateCI
------------------------------------------------------------*/
global with sharing class TGS_ModTicket {
        /*Operations*/
        public static final String ROD_SF_CREATE_INCIDENT ='ROD_SF_CREATE_INCIDENT';
        public static final String ROD_SF_MODIFY_INCIDENT = 'ROD_SF_MODIFY_INCIDENT';
        public static final String ROD_SF_CREATE_CHANGE = 'ROD_SF_CREATE_CHANGE';
        public static final String ROD_SF_MODIFY_CHANGE = 'ROD_SF_MODIFY_CHANGE';
        public static final String ROD_SF_ADD_WORKINFO = 'ROD_SF_ADD_WORKINFO';
        /*Access*/
        public static final String PUBLIC_ACCESS ='Public';
        public static final String INTERNAL_ACCESS = 'Internal';
        /* Types of Case */
        public static final String USER_SERVICE_RESTORATION = 'User Service Restoration';
        public static final String QUERY_TICKET = 'Query Ticket';
        /* valid Reported Sources */
        public static final Set<String> validReportedSources = new Set<String> {'Web', 'Email', 'Phone', 'Proactive', 'Other'};
        /* Origins */
        public static final String WEB ='Customer Web Portal';
        public static final String EMAIL_ORIGIN ='Email';
        public static final String PHONE ='Phone Call';
        public static final StrinG PROACTIVE = 'Proactive Remedy';
        public static final String OTHER = 'Other'; /*ALA 10/02/2017 - Added the 'Other' value*/
        /* Valid Status for a case*/
        public static final Set<String> validStatus = new Set<String> {'New','Assigned', 'In Progress', 'Pending', 'Resolved', 'Closed', 'Cancelled', 'Rejected'};
         /* Valid Impact for a case*/
        public static final Set<String> validImpact = new Set<String> {'Extensive/Widespread', 'Significant/Large', 'Moderate/Limited', 'Minor/Localized'};
        /* Services Type to Incidents */
        public static final Set<String> userServerRestoration = new Set<String> {'User Service Restoration','User Service Request','Infrastructure Event','Infrastructure Restoration'};
        public static final String AU = 'Authorized User';
        public static final String SA = 'Support Agent';
        public static final String OB = 'On Behalf';
        /* Global classes*/
        global with sharing class Result{ 
        
            webservice string Operation_Status;
            webservice string Error_Msj;
            webservice string CaseNumber;
            
        }
        
        /* Return a set with the pickvalues of the Field_name, which belogs to ObjectApi_name */
        public static Set<String> getPickListValues(String ObjectApi_name, String Field_name){
            
            Set<String> lstPickvals=new Set<String>();
            List<Schema.PicklistEntry> pick_list_values = getListValues(ObjectApi_name,Field_name);
            for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
              lstPickvals.add(a.getValue());//add the value  to our final list
            }
            return lstPickvals;
            
        }
        static List<Schema.PicklistEntry> getListValues(String ObjectApi_name, String Field_name){
            
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
            Sobject Object_name = targetType.newSObject();
            Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
            Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
            Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
            return field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
            
        }

        /* Return if the String id is a valid Salesforce ID */
        /*public static Boolean isValidId(String id){
            
            return Pattern.matches('[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}', id);
        }*/
        /* Assignes the origin of the case if reporSource is valid */
        public static String validateReportedSource(String reporSource){
            String origin = null;
            if(reporSource.equals('Web')){
                     /* Origin = Web */
                    origin = WEB;
            }else if(reporSource.equals('Email')){
                /* Origin = Email */
                origin = EMAIL_ORIGIN;
            }else if(reporSource.equals('Phone')){
                /* Origin = Phone */
                origin = PHONE;
            }else if(reporSource.equals('Proactive')){
                /* Origin = Proactive */
                origin = PROACTIVE;
            }
            /*ALA 10/02/2017 - Added the 'Other' value as origin*/
            else if(reporSource.equals('Other')){
                /* Origin = Other */
                origin = OTHER;
            }
            return origin;
        }

        /* Assignes a valid account Account to the case and returns if exists a valid account with this data */
        public static boolean validateAccount(Case caseToModify, String contacOrg, String businnesUnit, String compan){
            
             boolean validAccount = false;
             //24/01/2017 ALA - Added recordtype Holding to the company query.
             if (String.isNotBlank(compan)){
                     List<Account> accounts =[SELECT Id FROM Account WHERE Name=:compan AND recordType.DeveloperName = :Constants.RECORD_TYPE_TGS_HOLDING AND TGS_Es_MNC__c = true];
                     if (accounts.size()==0){
                        
                        throw new IntegrationException('Invalid Company');
                    }
                    caseToModify.AccountId = accounts.get(0).Id;
                    validAccount = true;
             }else if (String.isNotBlank(businnesUnit)){
             System.debug('Business Unit: '+businnesUnit);
                    List<Account> accounts = [SELECT Id FROM Account WHERE Name=:businnesUnit AND recordType.DeveloperName = :Constants.RECORD_TYPE_TGS_BUSINESS_UNIT AND TGS_Es_MNC__c = true];
                    if (accounts.size()==0){
                        
                        throw new IntegrationException('Invalid Direct_Contact_Department');
                    }
                    caseToModify.AccountId = accounts.get(0).Id;
                    validAccount =true;
             }else if (String.isNotBlank(contacOrg)){
                    List<Account> accounts = [SELECT Id FROM Account WHERE Name=:contacOrg AND recordType.DeveloperName = :Constants.RECORD_TYPE_TGS_LEGAL_ENTITY AND TGS_Es_MNC__c = true];
                    if (accounts.size()==0){
                        
                        throw new IntegrationException('Invalid Direct_Contact_Organization');
                    }
                    caseToModify.AccountId = accounts.get(0).Id;
                    validAccount =true;
             }  
             return validAccount;
        }
        /* Assignes the field Assignee to the case 
        public static void validateAssignee(Case caseToModify, String UserName){
            
            List<User> users = [Select ID FROM User WHERE UserName=:UserName];
            if ( users.size()==0){
                
                throw new IntegrationException('Invalid Assignee'); 
            }else{
                
                caseToModify.TGS_Assignee__c = users.get(0).Id;
            }
            
        }*/
        /* Assignes the field Assignee to the case */
        public static void validateContact(Case caseToModify, String temptext){
            //06/10/2016 JCT Added null temptext checking to avoid incomplete data errors.
            List<User> users = new List<User>();
            if(temptext!=null && temptext!=''&& String.isNotBlank(temptext)){
               users = [Select ID, ContactId FROM User WHERE TEMPEXT_ID__c=:temptext];
            }
            //ENd JCT 06/10/2016
            if ( users.size()==0 || users.isEmpty()){
                try{//JCT 18/07/2016 Added try-catch block surrounding dummy assignment.
                    //JCT 18/07/2016 If there's not anyone available a dummy contact is assigned to the case
                    List <Contact> contacts = [Select Id FROM Contact WHERE TEMPEXT_ID__c=:'DUMMY_ROD_CONTACT' limit 1];
                    caseToModify.ContactId = contacts[0].Id;
                    }catch(Exception ex){
                        throw new IntegrationException('No contact available'); 
                    }
            }else{
                
                caseToModify.ContactId = users.get(0).ContactId;
            }
            
        }
        /* The Owner is assigned */
        public static void validateOwner(Case caseToModify, String OwnerGroup){
            /* Searches the queue whose name is OWNER_GROUP/COORDINATOR_GROUP */ 
        //MGC 14-03-2016
            try{//JCT 18/07/2016 Added try-catch block surrounding the method
                //JCT 30/08/2016 Added  ownergroup exists checking.
                Id ownerG;
                List<Group> list_Groups = [SELECT Id FROM Group WHERE Group.Name =:OwnerGroup  AND Group.Type = 'Queue'];
                if(!list_Groups.isEmpty()){
                    ownerG = list_Groups[0].Id;
                    caseToModify.OwnerId = ownerG;
                }
                else{//JCT 18/07/2016 Added Filter to check if there's any owners available.
                try{//JCT 18/07/2016 Added try-catch block surrounding the dummy assignment, if there's no owners available a dummy owner is assigned to the case.
                        List <Group> list_group_dummyGroup = [Select Id,Name FROM Group WHERE Name='DUMMY_ROD_GROUP' limit 1];                     
                        caseToModify.OwnerId = list_group_dummyGroup[0].Id;
                        if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');
                    }catch(Exception ex){
                        throw new IntegrationException('There\'s no group available');             
                    }
                }
            }catch(Exception e){
                
                throw new IntegrationException('There is not a queue with name ' +OwnerGroup);  
            } 
        //END MGC 14-03-2016
        }
        /* Validates and Asigns TGS_Product_Tier_i */
        public static void validateProductCategorizations(Case caseToModify, String PC_Tier_1, String PC_Tier_2, String PC_Tier_3){
                /* If Product_Categorization_Tier_1!=null and Product_Categorization_Tier_1!=''*/
                if (String.isNotBlank(PC_Tier_1)){
                    Set<String> validProdTier1 = getPickListValues('Case','TGS_Product_Tier_1__c');
                    /* validates the value of TGS_Product_Tier_1__c */
                    if (validProdTier1.contains(PC_Tier_1)){
                         caseToModify.TGS_Product_Tier_1__c = PC_Tier_1;
                         /* Checks Product_Tier_2*/
                         if (String.isNotBlank(PC_Tier_2)){
                            Set<String> validProdTier2 = getPickListValues('Case','TGS_Product_Tier_2__c');
                            if (validProdTier2.contains(PC_Tier_2)){
                                caseToModify.TGS_Product_Tier_2__c = PC_Tier_2;
                                /* Checks Product_Tier_3*/
                                 if (String.isNotBlank(PC_Tier_3)){
                                    Set<String> validProdTier3 = getPickListValues('Case','TGS_Product_Tier_3__c');
                                    if (validProdTier3.contains(PC_Tier_3)){
                                        caseToModify.TGS_Product_Tier_3__c = PC_Tier_3;
                                        
                                    }else{
                                    throw new IntegrationException('The value of Product_Categorization_Tier_3 is invalid: SF value = ' + caseToModify.TGS_Product_Tier_3__c + ' - RoD value: ' + PC_Tier_3);    
                                    }
                                 }else{
                                    
                                    caseToModify.TGS_Product_Tier_3__c = null;
                                 }  
                            }else{
                            throw new IntegrationException('The value of Product_Categorization_Tier_2 is invalid: SF value = ' + caseToModify.TGS_Product_Tier_2__c + ' - RoD value: ' + PC_Tier_2);    
                            }
                         }else{
                            
                            caseToModify.TGS_Product_Tier_2__c = null;
                            caseToModify.TGS_Product_Tier_3__c = null;
                         }  
                    }
                    else{
                    throw new IntegrationException('The value of Product_Categorization_Tier_1 is invalid: SF value = ' + caseToModify.TGS_Product_Tier_1__c + ' - RoD value: ' + PC_Tier_1);    
                    }
                }else{
                    caseToModify.TGS_Product_Tier_1__c = null;
                    caseToModify.TGS_Product_Tier_2__c = null;
                    caseToModify.TGS_Product_Tier_3__c = null;
                }
        }
        /* Validates and Associates the Configuration object with the case */
        public static void validateCI(Case caseToModify, String CI_id){
            System.debug('CI_ID: '+CI_id);
            if(String.isNotBlank(CI_id)){
                /* Associates the case with a Configuration object */
                /*
                List<NE__OrderItem__c> configurationIds = [SELECT Id, NE__OrderId__c FROM NE__OrderItem__c WHERE Id = :CI_id];
                if(configurationIds.size()==0){
                    
                    throw new IntegrationException('Invalid CI ID');    
                }
                Id configurationId = configurationIds.get(0).NE__OrderId__c;
                
                List<NE__Order__c> configurations =[SELECT Id FROM NE__Order__c WHERE Id = :configurationId];
                if(configurations.size()==0){
                    
                    throw new IntegrationException('Invalid Configuration');    
                }
                NE__Order__c configuration = configurations.get(0);
                configuration.Case__c = caseToModify.Id; 
                update configuration;
                */
                //IFS   09/05/2018
               if(!CI_id.startsWithIgnoreCase('OI')){
                    caseToModify.TGS_Customer_Services__c = null;
                    
                }
                else{
                    List<NE__OrderItem__c> configurationItems = [SELECT Id, Name FROM NE__OrderItem__c WHERE Name = :CI_id];
                     if(configurationItems.size()==0){
                        
                        throw new IntegrationException('Invalid CI Name');    
                     }
                     System.debug('configurationItems: '+configurationItems);
                     NE__OrderItem__c ci = configurationItems.get(0);
                     system.debug('CI: '+ ci);
                     caseToModify.TGS_Customer_Services__c = ci.Id;
                }
            }
            
        }
        
        public static String ValidateSite(String site){
        /*String sit = '';
         //TGS_Ticket_Site__c 
        if (String.isBlank(site)) {
            //throw new IntegrationException('Site is mandatory');
            //MGC 01-03-2016
            List<BI_Punto_de_instalacion__c> sites = [Select Id FROM BI_Punto_de_instalacion__c WHERE TEMPEXT_ID__c='DUMMY_ROD_SITE'];
            if (sites.size()==0) {
                sit='-1';
            } else {
                sit = sites.get(0).Id;
            }
            //END MGC 01-03-2016
        }else {
            if (site.equals(Constants.SITE_DEFAULT)) {
                    site = '';
                }else{
                    List<BI_Punto_de_instalacion__c> sites = [Select Id FROM BI_Punto_de_instalacion__c WHERE Name=:site];
                if (sites.size()==0) {
                        
                        sit='-1';
                } else {
                    sit = sites.get(0).Id;
                }
            } 
        }*/
        //JCT 18/07/2016 changed the method commented above for the one shown here.
        try{
            //JCT 18/07/2016 Surrounded with a try-catch block the method.
            List<BI_Punto_de_instalacion__c> list_sites = [Select Id FROM BI_Punto_de_instalacion__c WHERE Name=:site];
            if(list_sites.size()==0){
                //JCT 18/07/2016 Added Filter to check if there's any sites available.
                try{
                    //JCT 18/07/2016 If there's no sites available a dummy site is assigned to the case.Surrounded by a try-catch block.
                    List<BI_Punto_de_instalacion__c> list_dummySite = [Select Id FROM BI_Punto_de_instalacion__c WHERE TEMPEXT_ID__c='DUMMY_ROD_SITE' limit 1];
                    return String.valueOf(list_dummySite[0].Id);
                }
                catch(Exception ex){
                    throw new IntegrationException('No site available'); 
                }
                    }else{
                return String.valueOf(list_sites[0].Id);
                    }
                } 
        catch(Exception ex){
            throw new IntegrationException('No site available'); 
            }
        //End JCT 18/07/2016
        }
        
        public static void validateNotMandatoryAtt(Case caseToModify, String Status_Reason, String Priority, String Categorization_Tier_1, String Categorization_Tier_2,
                                                String Categorization_Tier_3, String Product_Categorization_Tier_1, String Product_Categorization_Tier_2, String Product_Categorization_Tier_3 ){
            
             /* Status Reason*/
            if (String.isNotBlank(Status_Reason)){      
               
                 caseToModify.TGS_Status_reason__c = Status_Reason;
            }
             /* Priority */
            if (String.isNotBlank(Priority)){       
                
                 Set<String> validPriority = getPickListValues('Case','Priority');
                 if (validPriority.contains(Priority)){
                    
                    caseToModify.Priority = Priority;
                 }else{
                    
                    throw new IntegrationException('The priority ' +Priority+ ' is invalid');   
                 }
            }   
            //RPM 04/05/2017 modificacion para permitir que los casos que llegan de remedy puedan rellenar los TGS_Operational_Tier con cualquier valor                                        
            
            String recordTypeIdRoD = caseToModify.RecordTypeId;
            List<RecordType> recordTypeRoD = [SELECT Id,Name,DeveloperName FROM RecordType WHERE Id =:recordTypeIdRoD];
            if(recordTypeRoD.size() > 0 && (recordTypeRoD[0].DeveloperName.equals(Constants.RECORD_TYPE_TGS_INCIDENT) || recordTypeRoD[0].DeveloperName.equals(Constants.RECORD_TYPE_TGS_CHANGE))){
                caseToModify.TGS_Operational_Tier_1_text__c = Categorization_Tier_1;
            	caseToModify.TGS_Operational_Tier_2_text__c = Categorization_Tier_2;
            	caseToModify.TGS_Operational_Tier_3_text__c = Categorization_Tier_3;        
            }else{
                caseToModify.TGS_Op_Technical_Tier_1__c = Categorization_Tier_1;
            	caseToModify.TGS_Op_Technical_Tier_2__c = Categorization_Tier_2;
            	caseToModify.TGS_Op_Technical_Tier_3__c = Categorization_Tier_3;
            }
            //FIN RPM
            System.debug(Product_Categorization_Tier_3);
            /* validate and assigns Product_Categorizacion_Tier_i*/
            validateProductCategorizations(caseToModify, Product_Categorization_Tier_1, Product_Categorization_Tier_2, Product_Categorization_Tier_3);                    
        }
        
         public static void setResolution(String description, Id caseId){
             boolean duplicate = false;
             List<Case> cases = [Select (Select  CommentBody From CaseComments) From Case WHere id = :caseId ];
             Case c = cases.get(0);
             if (String.isNotBlank(description)){
                   for(CaseComment comm : c.CaseComments){
                       if (comm.CommentBody.equals(description)){
                           duplicate = true;
                           break;
                       }
                    } 
                    if(!duplicate){
                        if(description.length() > 4000){
                            description = description.abbreviate(4000);
                        }
                        CaseComment resolution = new CaseComment(ParentId=caseId, CommentBody=description,IsPublished=true);
                        insert resolution;
                    }
             }
                
          }
         
         
         public static void updateAccounts(Case caseUpdate, String Direct_Contact_Department, String Direct_Contact_Organization, String Company){
            
            //Id businessUnitId = null; 
            String caseAccount = '';
            
            if (caseUpdate.Account.RecordType.DeveloperName.equals(Constants.RECORD_TYPE_TGS_BUSINESS_UNIT)){
                    
                caseAccount = Direct_Contact_Department;
            
            }else if(caseUpdate.Account.RecordType.DeveloperName.equals(Constants.RECORD_TYPE_TGS_HOLDING )){
            
                caseAccount = Company;
            
            }else if(caseUpdate.Account.RecordType.DeveloperName.equals(Constants.RECORD_TYPE_TGS_LEGAL_ENTITY )){
            
                caseAccount = Direct_Contact_Organization;
            
            }

            if(String.isNotBlank(caseAccount)){
                /* ALA 03/02/2017 - Added TGS_Es_MNC__c filter to the query*/
                List<Account> accounts = [SELECT Id FROM Account WHERE Name=:caseAccount 
                                                                        AND RecordType.DeveloperName=:caseUpdate.Account.RecordType.DeveloperName 
                                                                        AND TGS_Es_MNC__c = true];
                if (accounts.size()==0){
                
                    throw new IntegrationException('Invalid Company');
                }
                caseUpdate.AccountId = accounts.get(0).Id;
           }     
             
       }
       /*------------------------------------------------------------ 
       Author:         Ana Cirac 
       Company:        Deloitte
       Description:    Updates case fields about info mWan
       History
       <Date>          <Author>        <Change Description>
       22-Sept-2015     Ana Cirac        Initial Version
       ------------------------------------------------------------*/
       public static void updateInfoMwan(Case caseUpdated,TGS_ModTicketINC.mWanInfo infoMwan){
           if (infoMwan!=null){
               /* Validates and sets TGS_Customer_Contacted__c */
               Set<String> validCustomContrac = getPickListValues('Case','TGS_Customer_Contacted__c');
               if (validCustomContrac.contains(infoMwan.Customer_Contacted) || String.isBlank(infoMwan.Customer_Contacted)){
                    caseUpdated.TGS_Customer_Contacted__c = infoMwan.Customer_Contacted; 
               }else{
                   throw new IntegrationException('Invalid Customer_Contacted');
               }
               /* Validates and sets TGS_Electrical_power_issue_at_customer_s__c */
               Set<String> validElectricalPower = getPickListValues('Case','TGS_Electrical_power_issue_at_customer_s__c');
               if (validElectricalPower.contains(infoMwan.Electrical_Power) || String.isBlank(infoMwan.Electrical_Power)){
                    caseUpdated.TGS_Electrical_power_issue_at_customer_s__c = infoMwan.Electrical_Power; 
               }else{
                   throw new IntegrationException('Invalid Electrical_Power'); 
               }
               /* Validates and sets TGS_CPE_status__c */
               Set<String> validCPE_Status = getPickListValues('Case','TGS_CPE_status__c');
               if (validCPE_Status.contains(infoMwan.CPE_Status) || String.isBlank(infoMwan.CPE_Status)){
                    caseUpdated.TGS_CPE_status__c = infoMwan.CPE_Status; 
               }else{
                   throw new IntegrationException('Invalid CPE_Status'); 
               }
               /* Validates and sets TGS_Has_the_CPE_been_reset__c */
               Set<String> validCPE_Reset = getPickListValues('Case','TGS_Has_the_CPE_been_reset__c');
               if (validCPE_Reset.contains(infoMwan.CPE_Reset) || String.isBlank(infoMwan.CPE_Reset)){
                    caseUpdated.TGS_Has_the_CPE_been_reset__c = infoMwan.CPE_Reset; 
               }else{
                   throw new IntegrationException('Invalid CPE_Reset'); 
               }
               /* Validates and sets TGS_Cabling_Checked__c */
               Set<String> validCabling_Checked = getPickListValues('Case','TGS_Cabling_Checked__c');
               if (validCabling_Checked.contains(infoMwan.Cabling_Checked) || String.isBlank(infoMwan.Cabling_Checked)){
                    caseUpdated.TGS_Cabling_Checked__c = infoMwan.Cabling_Checked; 
               }else{
                   throw new IntegrationException('Invalid Cabling_Checked'); 
               }
               /* Validates and sets TGS_Has_it_been_working_before__c */
               Set<String> validWorking_Before = getPickListValues('Case','TGS_Has_it_been_working_before__c');
               if (validWorking_Before.contains(infoMwan.Working_Before) || String.isBlank(infoMwan.Working_Before)){
                    caseUpdated.TGS_Has_it_been_working_before__c = infoMwan.Working_Before; 
               }else{
                   throw new IntegrationException('Invalid Working_Before'); 
               }
               /* Validates and sets TGS_Any_Change_pending__c */
               Set<String> validChange_Pending = getPickListValues('Case','TGS_Any_Change_pending__c');
               if (validChange_Pending.contains(infoMwan.Change_Pending) || String.isBlank(infoMwan.Change_Pending)){
                    caseUpdated.TGS_Any_Change_pending__c = infoMwan.Change_Pending; 
               }else{
                   throw new IntegrationException('Invalid Change_Pending'); 
               }
               /* Validates and sets TGS_Is_it_allowed_to_do_intrusive_tests__c */
               Set<String> validAllowed_Intrusive_Test = getPickListValues('Case','TGS_Is_it_allowed_to_do_intrusive_tests__c');
               if (validAllowed_Intrusive_Test.contains(infoMwan.Allowed_Intrusive_Test) || String.isBlank(infoMwan.Allowed_Intrusive_Test)){
                    caseUpdated.TGS_Is_it_allowed_to_do_intrusive_tests__c = infoMwan.Allowed_Intrusive_Test; 
               }else{
                   throw new IntegrationException('Invalid Allowed_Intrusive_Test'); 
               }
               /* Validates and sets TGS_Repetitive_issue__c */
               Set<String> validRepetitive_Issue = getPickListValues('Case','TGS_Repetitive_issue__c');
               if (validRepetitive_Issue.contains(infoMwan.Repetitive_Issue) || String.isBlank(infoMwan.Repetitive_Issue)){
                    caseUpdated.TGS_Repetitive_issue__c = infoMwan.Repetitive_Issue; 
               }else{
                   throw new IntegrationException('Invalid Repetitive_Issue'); 
               }
               /* Sets TGS_Date_time_that_the_issue_came_up__c */
               caseUpdated.TGS_Date_time_that_the_issue_came_up__c = infoMwan.Date_CameUp; 
               /* Sets TGS_Site_details_contacts__c */
               caseUpdated.TGS_Site_details_contacts__c = infoMwan.Site_Details; 
               /* Sets TGS_Incident_Summary__c */
               caseUpdated.TGS_Incident_Summary__c = infoMwan.Incident_Summary; 
           }    
       }
}