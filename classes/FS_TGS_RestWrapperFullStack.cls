/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Oscar Ena
Company:       Accenture - New Energy Aborda
Description:   UNICA Structures for Rest WebServices.
                EACH CLASS ORDERED IN ALPHABETICAL ORDER

History:

<Date>            <Author>          <Description>
25/05/2017        Oscar Ena        Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class FS_TGS_RestWrapperFullStack{

   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - NEA
    Description:   Infinity - FullStack Integration (adapt to UNICA TMF629 R14.5.1)
    
    History:
    
    <Date>            <Author>                              <Description>
    24/05/2017        Victoria Gutierrez                    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class AddressInfoType{
        
                public String addressType;
                public String normalized;
                public String addressName;
                public AddrValueType addressNumber;
                public AddrValueType floor;
                public AddrValueType apartment;
                public String area;
                public String locality;
                public String municipality;
                public String region;
                public String country;
                public String postalCode;
                public String addressExtension;
                public CoordinatesType coordinates;
                public Boolean isDangerous;
                public List<KeyvalueType> additionalData;
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA AddressRelInfoType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class AddressRelInfoType{
        public Boolean preferred;
        public TimePeriodType validFor;
        public List<KeyValueType> relInfo; //[1..unbounded]
        public String addressType;
        public String normalized;
        public String addressName;
        public AddrValueType addressNumber;
        public AddrValueType floor;
        public AddrValueType apartment;
        public String area;
        public String locality;
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA AddrRangeType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class AddrRangeType{
        public String lowerValue;
        public String upperValue;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA AddrValueType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class AddrValueType {
        public String value;
        public AddrRangeType range;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       Accenture - New Energy Aborda
    Description:   UNICA AddrValueType structure
    
    History:
    
    <Date>               <Author>                <Description>
    26/05/2017       Ignacio G Schuhmacher      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class AssociatedSiteRequestType {
        public Boolean preferred;
        public List<String> roles;
        public TimePeriodType validFor;
        public List<KeyvalueType> relInfo;
        public String name;
        public SiteStatusType status;
        public AddressInfoType address;

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       Accenture - New Energy Aborda
    Description:   UNICA AddrValueType structure
    
    History:
    
    <Date>            <Author>              <Description>
    12/06/2017    Ignacio G Schuhmacher     Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class AssociatedSiteType {
        public String id;
        public String href;
        public Boolean preferred;
        public List<String> roles;
        public TimePeriodType validFor;
        public List<KeyvalueType> relInfo;
        public String name;
        public SiteStatusType status;
        public AddressInfoType address;        

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       Accenture - New Energy Aborda
    Description:   UNICA AddrValueType structure
    
    History:
    
    <Date>            <Author>              <Description>
    12/06/2017    Ignacio G Schuhmacher     Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class AssociationType {
        public Boolean preferred;
        public TimePeriodType validFor;
        public List<String> roles;
        public List<KeyvalueType> relInfo;
    }


   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA ContactDetailsType structure
    
    History:
    
    <Date>            <Author>              <Description>
    30/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class ContactDetailsType {
        public String id;
        public String anyURI;
        public String correlatorId;
        public string type;
        public ContactStatusType status;
        public String contactName;
        public NameDetailsType name;
        public List<AddressInfoType> addresses; //[1..unbounded]       
        public List<IdentificationType> legalId; //[0..unbounded]
        public List<ContactingModeType> contactMedium;
        public PersonalInfoType personal;
        public List<KeyValueType> additionalData;        
    }



    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA ContactingModeType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class ContactingModeType {
        public String type;
        public Boolean preferred;
        public TimePeriodType validFor;
        public List<KeyValueType> medium; //[1..unbounded]
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA ContactRequestType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class ContactRequestType{
        public String correlatorId;
        public String type;
        public ContactStatusType status;
        public String contactName;
        public NameDetailsType name;
        public List<AddressRelInfoType> adresses; //[1..unbounded]
        public List<IdentificationType> legalId; //[0..unbounded]
        public List<ContactingModeType> contactMedium; //[0..unbounded]
        public PersonalInfoType personal;
        public List<KeyValueType> additionalData; //[1…unbounded]   
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA ContactRelationType structure
    
    History:
    
    <Date>            <Author>              <Description>
    30/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class ContactRelationType{
        public Boolean preferred;
        public List<String> contactTypes; //[0..unbounded]
        public String partyRoleType;
        public TimePeriodType validFor;
        public List<KeyValueType> relInfo; //[1…unbounded]   
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA ContactStatusType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global enum ContactStatusType {
        
        active, inactive, prospective
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - NEA
    Description:   Infinity - FullStack Integration (adapt to UNICA TMF629 R14.5.1)
    
    History:
    
    <Date>            <Author>                             <Description>
    24/05/2017        Victoria Gutierrez                   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class ContactUseType{ 
        public boolean preferred;
        public String contactType;
        public String[] contactExtendedTypes;
        public String role;
        public list<KeyValueType> relInfo;


    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - NEA
    Description:   Infinity - FullStack Integration (adapt to UNICA TMF629 R14.5.1)
    
    History:
    
    <Date>            <Author>                            <Description>
    24/05/2017        Victoria Gutierrez                  Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class CoordinatesType
    {
        public String longitude;
        public String latitude;
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - NEA
    Description:   Infinity - FullStack Integration (adapt to UNICA TMF629 R14.5.1)
    
    History:
    
    <Date>            <Author>                              <Description>
    24/05/2017        Victoria Gutierrez                   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
     global class CreditProfileType {
        public Datetime creditProfileDate;
        public TimePeriodType validFor; 
        public Integer creditRiskRating;
        public Integer creditScore;
        public String financialInstitutionName;
        public String financialInstitutionType;

    }
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - NEA
    Description:   Infinity - FullStack Integration (adapt to UNICA TMF629 R14.5.1)
    
    History:
    
    <Date>            <Author>                                <Description>
    24/05/2017        Victoria Gutierrez                     Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    global class CustomerAccountRefType{

        public String id;
        public String href;
        public String name;
        public String description;
        public String status;
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - NEA
    Description:   Infinity - FullStack Integration (adapt to UNICA TMF629 R14.5.1)
    
    History:
    
    <Date>            <Author>              <Description>
    24/05/2017        Victoria Gutierrez                    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    global class CustomerAccountsListType{

        public list<CustomerAccountRefType> customerAccounts;
        public Integer totalResults;

    } 

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - NEA
    Description:   Infinity - FullStack Integration (adapt to UNICA TMF629 R14.5.1)
    
    History:
    
    <Date>            <Author>                             <Description>
    24/05/2017        Victoria Gutierrez                   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class CustomerRequestType{

        public String type;
        public String callbackUrl;
        public String correlationId;
        public String name;
        public CustomerStatusType status;
        public String description;
        public Integer customerRank;
        public String parentCustomer;
        public SegmentType segment;
        public String subsegment;
        public Decimal satisfaction;
        public list<KeyValueType> additionalData;
        public list<ContactingModeType> contactMedium;
        public list<AddressInfoType>  customerAddress;
        public list<CreditProfileType> customerCreditProfile;
        public list<NameValueType>  characteristic;
        public list<IdentificationType> legalId;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - NEA
    Description:   Infinity - FullStack Integration (adapt to UNICA TMF629 R14.5.1)
    
    History:
    
    <Date>            <Author>                             <Description>
    24/05/2017        Victoria Gutierrez                   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global enum CustomerStatusType{
        active,
        inactive,
        prospective
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - NEA
    Description:   Infinity - FullStack Integration (adapt to UNICA TMF629 R14.5.1)
    
    History:
    
    <Date>            <Author>                             <Description>
    24/05/2017        Victoria Gutierrez                   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class CustomerType{
        public String id;
        public String href;
        public String cstType;
        public KeyValueType additionalData;
        public list<CustomerAccountRefType> customerAccount;
        public String correlationId;
        public String name;
        public CustomerStatusType status;
        public String description;
        public Integer customerRank;
        public String parentCustomer;
        public String subsegment;
        public Decimal satisfaction;
        public list<ContactingModeType> contactMedium;
        public list<AddressInfoType>  customerAddress;
        public list<CreditProfileType> customerCreditProfile;
        public list<NameValueType>  characteristic;
        public list<IdentificationType> legalId;
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Generic response class for UNICA Webservices
    
    History:
    
    <Date>            <Author>              <Description>
    30/05/2017        Oscar Ena             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class GenericResponse{
        
        public String error;
        public String message;

        public ContactDetailsType contactDetailsType;

       
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA IdentificationType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class IdentificationType {
        public String country;
        public String nationalIDType;
        public String nationalID;
        public Boolean isPrimary;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA KeyValueType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class KeyValueType{
        public String key;
        public String value;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA NameDetailsType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class NameDetailsType {
        public String fullName;
        public String title;
        public String givenName;
        public String familyName;
        public String middleName;
        public String suffix; 
        public String displayName;      
    }

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - New Energy Aborda
    Description:   UNICA NameDetailsType structure
    
    History:
    
    <Date>            <Author>                            <Description>
    25/05/2017        Victoria Gutierrez                  Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
  global class NameValueType{
      
            public String name;
            public String value;

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA PersonalInfoType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class PersonalInfoType {
        public String gender;
        public String birthdate;
        public Integer age;
        public String maritalStatus;
        public List<KeyValueType> additionalData; //[1..unbounded]
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria GHutierrez
    Company:       Accenture - New Energy Aborda
    Description:   UNICA SegmentType structure
    
    History:
    
    <Date>            <Author>                  <Description>
    02/06/2016        Victoria Gutierrez         Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    global enum SegmentType{
        consumer,smb,corporate,retailer, wholesale
    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Galindo
    Company:       Accenture - New Energy Aborda
    Description:   UNICA SiteRequestType structure
    
    History:
    
    <Date>          <Author>              <Description>
    26/05/2017      Ignacio Galindo         Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    global class SiteDetailsType{
        public String id;
        public String anyUri;
        public String name;
        public SiteStatusType status;
        public AddressInfoType address;
        public List<KeyValueType> additionalData; //[1..unbounded]

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Galindo
    Company:       Accenture - New Energy Aborda
    Description:   UNICA SiteRequestType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Ignacio Galindo         Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    global class SiteRequestType{
        public String name;
        public SiteStatusType status;
        public AddressInfoType address;
        public List<KeyValueType> additionalData; //[1..unbounded]

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Galindo
    Company:       Accenture - New Energy Aborda
    Description:   UNICA SiteStatusType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Ignacio Galindo         Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    global enum SiteStatusType{
        active, inactive
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   UNICA TimePeriodType structure
    
    History:
    
    <Date>            <Author>              <Description>
    26/05/2017        Oscar Ena           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global class TimePeriodType{
        public Datetime startDateTime;
        public Datetime endDateTime;
    } 


}