@isTest
private class PCA_LoginController_TEST {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos
	Company:       Salesforce.com
	Description:   Test Class that manage coverage of PCA_NewCaseCtrl Class
				
	History: 

	<Date>                     <Author>                <Change Description>
	06/10/2014                  Micah Burgos             Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest static void PCA_LoginController_try_TEST() {
		User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
	    	system.runAs(user1){
	    		BI_TestUtils.throw_exception = false;
	    		
	    		PageReference pageRef = new PageReference('PCA_Reclamo_PopUpDetail');
   		 		Test.setCurrentPage(pageRef);

   		 		ApexPages.currentPage().getParameters().put('startURL', 'Test');
   		 		ApexPages.currentPage().getParameters().put('retURL', 'Test');

				PCA_LoginController controller = new PCA_LoginController();

				controller.username = 'test';
				controller.password = 'test1234';

				system.assertEquals(controller.checkUser(),true);
				system.assertEquals(controller.checkPass(),true);
				controller.login();
				controller.forgotPassword();
	
	    	}
	    }
	}
	@isTest static void PCA_LoginController_invalidPassAndUser_TEST() {
		BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		PCA_LoginController controller = new PCA_LoginController();
		system.assertEquals(controller.checkUser(),false);
		system.assertEquals(controller.checkPass(),false);
	}
	
	@isTest static void PCA_LoginController_catch_TEST() {
		BI_TestUtils.throw_exception = true;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        PCA_LoginController controller = new PCA_LoginController();

		controller.checkUser();
		controller.checkPass();
		controller.login();
		controller.forgotPassword();
	}
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}