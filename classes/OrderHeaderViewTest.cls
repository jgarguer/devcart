/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (seeAllData = true)
private class OrderHeaderViewTest {

    static testMethod void myUnitTest() 
    {
        Account acc= new Account(Name = 'test',NE__Type__c = 'Business', ShippingCity = 'Roma',ShippingPostalCode = '00100',  ShippingCountry = 'Lazio',  ShippingState = 'Italia',  ShippingStreet = 'Garibaldi', BillingPostalCode = '00100', BillingCity = 'Roma', BillingCountry ='Lazio', BillingState = 'Italia', BillingStreet = 'aurelia', BI_Segment__c = 'test', BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test'); //28/09/2017
        insert acc;
        
        Date myTt = date.today();
        Opportunity opp=new Opportunity(Name='opp',AccountId=acc.id,StageName='Prospecting',CloseDate=myTt);
        insert opp;
        
        RecordType rec2     =   [select Name,Id from RecordType WHERE Name = 'Opty' AND (SObjectType = 'Order__c' OR SObjectType = 'NE__Order__c') LIMIT 1];
        
        NE__Order__c ord=new NE__Order__c(NE__AccountId__c=acc.id,NE__BillAccId__c=acc.id,NE__ServAccId__c=acc.id,RecordTypeId=rec2.id,NE__OptyId__c=opp.id,NE__Shipping_City__c='city',NE__Shipping_Country__c='country',NE__Shipping_State_Province__c='SSP',NE__Shipping_Street__c='Street',NE__Shipping_Zip_Postal_Code__c='ZPC',NE__OrderStatus__c='Active');
        insert ord;
        
        NE__Order_Header__c ordHead= new NE__Order_Header__c(NE__AccountId__c = acc.Id, NE__OrderId__c = ord.Id);
        insert ordHead;
        
        ApexPages.StandardController cont   =   new ApexPages.StandardController(ordHead);
        OrderHeaderView ole                 =   new OrderHeaderView(cont);
        
        PageReference redirectPage = ole.redirectToOrder();
    }
}