/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis 
Company:       Everis
Description:   Test Methods executed to check CWP_SelfManagementOfferCtrl 
History:

<Date>                  <Author>                <Change Description>
30/03/2017              Everis                  Initial Version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
public class CWP_SelfManagementOfferCtrlTest {
    
    @testSetup 
    private static void dataModelSetup() {  
        
         //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
        
            set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
           //ACCOUNT
        map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(new User(id=userInfo.getUserId())){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
            
          
         //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        insert contactTest;
        
        User usuario;
              
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            DateTime date1= DateTime.newInstance(2017, 11, 18, 3, 3, 3);
            Date date2= Date.newInstance(2018, 09, 18);
            insert usuario;
            User usuario1 = [SELECT id,AccountId FROM User WHERE  FirstName =: 'nombre1']; 
            system.debug('TEST: ' + usuario1.AccountId);
            Opportunity oppTest= new Opportunity (BI_Fecha_de_entrega_de_la_oferta__c = date1,
                                          BI_Duracion_del_contrato_Meses__c= 10,
                                          CloseDate = date2,
                                          OwnerId = usuario1.id,Name ='Opp1', StageName='aaa' , BI_No_Identificador_fiscal__c='123', AccountId=usuario1.AccountId); 
                                          
           insert oppTest; 
            
           
            
           
        }       
       
      
    }
    
    /*
    static testMethod void test1(){
        BI_TestUtils.throw_exception=false; 

        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
            BI_TestUtils.throw_exception = false;
            //List<Region__c> lst_pais = BI_DataLoad.loadPais(1);
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
            
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadPortalContacts(1, accList);
            BI_DataLoad.loadOpportunities(accList[0].Id);
            system.debug('con: '+con);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            
            system.runAs(user1){
                Test.startTest();
                CWP_SelfManagementOfferCtrl.getRecords('init');
                CWP_SelfManagementOfferCtrl.getFieldsValues('init');
                String jsonTest = '{        "table": "table",       "iniIndex": 1,      "finIndex": 1,      "numRecords": 1,        "viewSize": 1   }   ';
                CWP_SelfManagementOfferCtrl.getRecords(jsonTest);
                CWP_SelfManagementOfferCtrl.ClaimsRequestRegs requestRegs = new CWP_SelfManagementOfferCtrl.ClaimsRequestRegs();
                requestRegs.bI_Duracion_del_contrato_Meses='';
                requestRegs.bI_Fecha_de_entrega_de_la_oferta='';
                requestRegs.bI_Opportunity_Type='';
                requestRegs.bI_Recu_bruto_mensual='';
                requestRegs.closeDate='';
                requestRegs.idOpportunity=''; 
                Test.stopTest(); 
            } 
        } 
    } 
    */
    @isTest
    private static void getRecordsTest() {     
        system.debug('test 1 ');
        User usuario = [SELECT id FROM User WHERE  FirstName =: 'nombre1'];  
        BI_TestUtils.throw_exception=false; 

           CWP_SelfManagementOfferCtrl.ClaimsRequestRegs requestRegs = new CWP_SelfManagementOfferCtrl.ClaimsRequestRegs();
                requestRegs.bI_Duracion_del_contrato_Meses='';
                requestRegs.bI_Fecha_de_entrega_de_la_oferta='';
                requestRegs.bI_Opportunity_Type='';
                requestRegs.bI_Recu_bruto_mensual='';
                requestRegs.closeDate='';
                requestRegs.idOpportunity='';          
        
        System.runAs(usuario){                          
            Test.startTest();                     
            CWP_SelfManagementOfferCtrl.getRecords('init');   
            String jsonTest = '{        "table": "table",       "iniIndex": 1,      "finIndex": 1,      "numRecords": 1,        "viewSize": 1   }   ';
            CWP_SelfManagementOfferCtrl.getRecords(jsonTest);                           
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void getFieldsValuesTest() {     
        system.debug('test 2 ');
        BI_TestUtils.throw_exception=false; 

        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        
        System.runAs(usuario){                          
            Test.startTest();                     
            CWP_SelfManagementOfferCtrl.getFieldsValues('init');                  
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void getFilteredRecordsTest() {     
        system.debug('test 3 ');
        BI_TestUtils.throw_exception=false; 

        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        
        System.runAs(usuario){                          
            Test.startTest();                     
            CWP_SelfManagementOfferCtrl.getFilteredRecords('BI_Id_Interno_de_la_Oportunidad__c','001075797');                  
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void getSortRecordsTest() {     
        system.debug('test 4');
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        BI_TestUtils.throw_exception=false; 

        System.runAs(usuario){                          
            Test.startTest();                     
            CWP_SelfManagementOfferCtrl.getSortRecords('BI_Id_Interno_de_la_Oportunidad__c','ASC');                  
            Test.stopTest();
        }       
        
    } 
}