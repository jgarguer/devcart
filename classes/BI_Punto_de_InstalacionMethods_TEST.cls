/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro Sevilla Durango
    Company:       NEAborda
    Description:   Test Class for to cover and to test the class named BI_Punto_de_InstalacionMethods
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    23/05/2016          Alvaro Sevilla Durango      Initial Version
    05/09/2016			Antonio Pardo 				New method prevenirBorradoSede_test to cover BI_Punto_de_instalacionMethods.prevenirBorradoSede  
    01/02/2017			MArta Gonzalez				REING_Reingenieria de contactos, se insertan campos obligatorios de contactos y la nueva logica para el contacto de canal online
	20/09/2017        Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                                                                                                      BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
                                                                                                                                                                                                                 
@isTest
private class BI_Punto_de_InstalacionMethods_TEST {
	
	@isTest static void test_method_one() {
                                                                                                                                                 
		list<BI_Punto_de_instalacion__c> lstNews = new list<BI_Punto_de_instalacion__c>();
		list<BI_Punto_de_instalacion__c> lstOlds = new list<BI_Punto_de_instalacion__c>();

        String en_US = 'en_US';
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        String region = Label.BI_Argentina;
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name = 'Administrador del sistema' limit 1];
        User user = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = en_US,
                          localesidkey = en_US,
                          ProfileId = prof.Id,
                          BI_Permisos__c = 'Inteligencia Comercial',
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com',
                          Pais__c = Label.BI_Argentina);
        
        insert user;    
           
        System.runAs(user){

			Test.startTest();
			
    		System.debug('-USER--->'+userTGS.TGS_Is_BI_EN__c+'--->'+user);
		//Cuentas
			Account objCuenta 								= new Account();
			objCuenta.Name                                  = 'prueba2';
			objCuenta.BI_Country__c                         = 'Colombia';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Fraude__c                          = false;

			objCuenta.BI_Segment__c                         = 'test';
			objCuenta.BI_Subsegment_Regional__c             = 'test';
			objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			system.debug('datos Cuenta ' + objCuenta);

			//Ciudad
			BI_Col_Ciudades__c objCiudad = new BI_Col_Ciudades__c ();
			objCiudad.Name 						= 'Test City';
			objCiudad.BI_COL_Pais__c 			= 'Test Country';
			objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
			insert objCiudad;

			//Contactos
			Contact objContacto                             = new Contact();
			objContacto.LastName                         	= 'Test2';
			objContacto.BI_Country__c                   	= 'Colombia';
			objContacto.CurrencyIsoCode                 	= 'COP';
			objContacto.AccountId                       	= objCuenta.Id;
			objContacto.BI_Tipo_de_contacto__c   			= 'Administrador Canal Online';
			objContacto.Phone         						= '1234567';
			objContacto.MobilePhone       					= '3104785925';
			objContacto.Email        						= 'pruebas@pruebas12.com';
			objContacto.BI_COL_Direccion_oficina__c   		= 'Dirprueba';
			objContacto.BI_COL_Ciudad_Depto_contacto__c  	= objCiudad.Id;
			objContacto.BI_Tipo_de_contacto__c    			= 'Autorizado';
            //REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
            //REING_FIN
			Insert objContacto;

			//Direccion COL
			BI_Sede__c objSede 								= new BI_Sede__c();
			objSede.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
			objSede.BI_Direccion__c 				= 'Test Street 1223 Number 3221';
			objSede.BI_Localidad__c 				= 'Test Local';
			objSede.BI_COL_Estado_callejero__c 		= System.Label.BI_COL_LbValor3EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c 		= 'Libre';
			objSede.BI_Country__c 					= 'Colombia';
			objSede.Name 							= 'Test Street 1223 Number 3221, Test Local Colombia';
			objSede.BI_Codigo_postal__c 			= '123562';
			insert objSede;
			
			BI_Sede__c objSede2 								= new BI_Sede__c();
			objSede2.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
			objSede2.BI_Direccion__c 				= 'Test Street 1222';
			objSede2.BI_Localidad__c 				= 'Test Local';
			objSede2.BI_COL_Estado_callejero__c 	= System.Label.BI_COL_LbValor3EstadoCallejero;
			objSede2.BI_COL_Sucursal_en_uso__c 		= 'Libre';
			objSede2.BI_Country__c 					= 'Colombia';
			objSede2.Name 							= 'Test Street Local Colombia';
			objSede2.BI_Codigo_postal__c 			= '12300';
			insert objSede2;

			//Sede COL
			BI_Punto_de_instalacion__c objPuntosInstaOld 	= new BI_Punto_de_instalacion__c ();
			objPuntosInstaOld.BI_Cliente__c       = objCuenta.Id;
			objPuntosInstaOld.BI_Sede__c          = objSede.Id;
			objPuntosInstaOld.BI_Contacto__c      = objContacto.id;
			objPuntosInstaOld.Name 				= 'Nombre sede prueba2';
			objPuntosInstaOld.BI_Tipo_de_sede__c = 'Fiscal';

			insert objPuntosInstaOld;

			BI_Punto_de_instalacion__c objPuntosInsta 	= new BI_Punto_de_instalacion__c ();
			objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
			objPuntosInsta.BI_Sede__c          = objSede2.Id;
			objPuntosInsta.BI_Contacto__c      = objContacto.id;
			objPuntosInsta.Name 				= 'Nombre sede prueba';
			objPuntosInsta.BI_Tipo_de_sede__c = 'Comercial Principal';
			insert objPuntosInsta;			

			Test.stopTest();

			lstNews.add(objPuntosInsta);
			lstOlds.add(objPuntosInstaOld);

			}


		BI_Punto_de_InstalacionMethods.updateUsershippingAddressFromSede(lstNews, lstOlds);

	}
	
	@isTest
	public static void prevenirBorradoSede_test(){
		
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
			
		Test.startTest();
		
		//Ciudad
		BI_Col_Ciudades__c objCiudad = new BI_Col_Ciudades__c (
		Name = 'Test City',
		BI_COL_Pais__c = 'Test Country',
		BI_COL_Codigo_DANE__c = 'TestCDa');
		insert objCiudad;
		
		//Cuentas
		Account objCuenta = new Account(
		Name = 'prueba2',
		BI_Country__c = 'Colombia',
		TGS_Region__c = 'América',
		BI_Tipo_de_identificador_fiscal__c = '',
		CurrencyIsoCode = 'COP',
		BI_Fraude__c = false,
		BI_Segment__c = 'test',
        BI_Subsegment_Regional__c = 'test',
        BI_Territory__c = 'test');
		insert objCuenta;
		
		//Contactos
		Contact objContacto = new Contact(
		LastName = 'Test2',
		BI_Country__c = 'Colombia',
		CurrencyIsoCode = 'COP',
		AccountId  = objCuenta.Id,
		BI_Tipo_de_contacto__c = 'Administrador Canal Online',
        //REING-INI
      	BI_Tipo_de_documento__c 			= 'Otros',
        BI_Numero_de_documento__c 			= '00000000X',
        FS_CORE_Acceso_a_Portal_Platino__c  = true,
        //REING_FIN
		Phone = '1234567',
		MobilePhone = '3104785925',
		Email = 'pruebas@pruebas12.com',
		BI_COL_Direccion_oficina__c = 'Dirprueba',
		BI_COL_Ciudad_Depto_contacto__c = objCiudad.Id
		);
		Insert objContacto;
		
		//Direccion COL
		BI_Sede__c objSede = new BI_Sede__c(
		BI_COL_Ciudad_Departamento__c = objCiudad.Id,
		BI_Direccion__c = 'Test Street 1223 Number 3221',
		BI_Localidad__c = 'Test Local',
		BI_COL_Estado_callejero__c 	= System.Label.BI_COL_LbValor3EstadoCallejero,
		BI_COL_Sucursal_en_uso__c = 'Libre',
		BI_Country__c = 'Colombia',
		Name = 'Test Street 1223 Number 3221, Test Local Colombia',
		BI_Codigo_postal__c = '123562');
		insert objSede;
		
		//Sede COL
		BI_Punto_de_instalacion__c objPuntosInstaOld = new BI_Punto_de_instalacion__c (
		BI_Cliente__c = objCuenta.Id,
		BI_Sede__c = objSede.Id,
		BI_Contacto__c = objContacto.id,
		Name = 'Nombre sede prueba2',
		BI_Tipo_de_sede__c = 'Fiscal');
		insert objPuntosInstaOld;
		
		Test.stopTest();
		
		List<BI_Punto_de_instalacion__c> lstOlds = new List<BI_Punto_de_instalacion__c>();
		
		lstOlds.add(objPuntosInstaOld);

		//Al tratarse de un addError capturo la excepción en el TryCatch y compruebo que el mensaje es el correcto
		try{
			delete lstOlds;
		}catch(Exception e) {
			System.Assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
		}
		
	}
	
}