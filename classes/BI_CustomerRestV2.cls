@RestResource(urlMapping='/customerinfo/v2/customers/*')
global class BI_CustomerRestV2 {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - New Energy Aborda
    Description:   Class for Customer Rest WebServices v.2.0
    
    History:
    
    <Date>            <Author>				   <Description>
    24/05/2017         Victoria Gutierrez      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - New Energy Aborda
    Description:   Creates a new customer in Salesforce receiving the request.
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          	   <Description>
    31/05/2017        Victoria Gutierrez       Initial version.
	29/11/2017		  Gawron, Julián		   Change Response Location
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPost
	global static void createCustomer() {
		
		try{
			
		
				RestRequest req = RestContext.request;

			
				Blob blob_json = req.requestBody;
			
				String string_json = blob_json.toString(); 
				System.debug('!!@@ request body--->' + string_json);
				
				FS_TGS_RestWrapperFullStack.CustomerRequestType custReqType = (FS_TGS_RestWrapperFullStack.CustomerRequestType) JSON.deserialize(string_json, FS_TGS_RestWrapperFullStack.CustomerRequestType.class);
				
				String customerExtId  = RestContext.request.requestURI.split('/')[4];
				
				
				Account cust = new Account();
				
			    cust.BI_Identificador_Externo__c = customerExtId;
				if(RestContext.request.headers.get('systemName') != null){
					cust.BI_Sistema_legado__c = RestContext.request.headers.get('systemName');		
				}
						
							
				cust = BI_RestHelper.postCustomer(cust, custReqType);
					
				
				
				insert cust;
				
				RestContext.response.statuscode = 201;
				//RestContext.response.headers.put('Location','https://'+System.URL.getSalesforceBaseURL().getHost()+'/services/apexrest/customerinfo/v2/customers/'+cust.BI_Identificador_Externo__c);
				RestContext.response.headers.put('Location', cust.Id); //JEG
				if (String.IsNotBlank(RestContext.request.headers.get('UNICA-ServiceId'))) RestContext.response.headers.put('UNICA-ServiceId', RestContext.request.headers.get('UNICA-ServiceId'));
        		if (String.IsNotBlank(RestContext.request.headers.get('UNICA-Application'))) RestContext.response.headers.put('UNICA-Application', RestContext.request.headers.get('UNICA-Application'));
			
								
		}catch(Exception Exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR 
			RestContext.response.headers.put('errorMessage',Exc.getMessage());
			BI_LogHelper.generate_BILog('BI_CustomerRestV2.createCustomer', 'BI_EN', Exc, 'Web Service');
			
		}
		
	}
	

	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - New Energy Aborda
    Description:   Modifies an existing customer in the server receiving the request.
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          	   <Description>
    25/05/2017        Victoria Gutierrez       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPut
	global static void updateCustomer() {
		
		try{

				System.debug('BI_CustomerRestV2.updateCustomer ## Inicio updateCustomer' );

				RestRequest req = RestContext.request;
				Blob blob_json = req.requestBody; 
				String string_json = blob_json.toString(); 

				System.debug('BI_CustomerRestV2.updateCustomer ## deserialize: ' + string_json);

				FS_TGS_RestWrapperFullStack.CustomerRequestType custReqType = (FS_TGS_RestWrapperFullStack.CustomerRequestType) JSON.deserialize(string_json, FS_TGS_RestWrapperFullStack.CustomerRequestType.class);

			    String externalId = RestContext.request.requestURI.split('/')[4];	


			    System.debug('BI_CustomerRestV2.updateCustomer ## externalId :' + externalId);	
				

				list<Account> listAcc = [SELECT Id, Name, RecordTypeId, Fax, Phone, NumberOfEmployees, BI_No_Identificador_fiscal__c, AnnualRevenue,
					   					 BI_Tipo_de_identificador_fiscal__c,BI_Country__c,Website, BI_Inicio_actividad_del_cliente__c, Rating, BI_Riesgo__c,
					   					 BI_Activo__c,BI_Denominacion_comercial__c, TGS_Account_Mnemonic__c, TGS_Account_Category__c, TGS_Es_MNC__c, 
					   					 TGS_Account_Leading_MNC_Unit__c, SAP_ID__c, TGS_Invoice_Language__c, Parent.BI_Identificador_Externo__c
					  				     FROM Account WHERE BI_Identificador_Externo__c =:externalId];

				if (!listAcc.isEmpty()){
						
					Account acc	= listAcc.get(0);

					acc = BI_RestHelper.postCustomer(acc, custReqType);

					update acc;

					if (String.IsNotBlank(RestContext.request.headers.get('UNICA-ServiceId'))) RestContext.response.headers.put('UNICA-ServiceId', RestContext.request.headers.get('UNICA-ServiceId'));
           			if (String.IsNotBlank(RestContext.request.headers.get('UNICA-Application'))) RestContext.response.headers.put('UNICA-Application', RestContext.request.headers.get('UNICA-Application'));
            		RestContext.response.statuscode = 200;	


				}else{

					System.debug('BI_CustomerRestV2.updateCustomer ## CUSTOMER NOT FOUND: Resource '+externalId+' does not exist');
        			RestContext.response.statuscode = 404;
        			RestContext.response.headers.put('errorMessage',externalId+' does not exist');

				}

			
				
		}catch(Exception Exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR 
			RestContext.response.headers.put('errorMessage',Exc.getMessage());
			BI_LogHelper.generate_BILog('BI_CustomerRest.updateCustomer', 'BI_EN', Exc, 'Web Service');
			
		}

		System.debug('BI_CustomerRestV2.updateCustomer ## Fin updateCustomer' );
		
	}
}