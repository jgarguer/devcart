@isTest
private class BI_FOCO_CountryDateHelperBatchTest {
	
	@isTest static void batchTest() {
		BI_FOCO_Pais__c cs = new BI_FOCO_Pais__c(Name='Argentina',Batch_Size__c=10,BI_BatchDelete_Size__c=10, Currency_ISO_Code__c='ARS', Opportunity_Probability__c=50, Notify_Email_List__c='bejoy.babu@salesforce.com'); 
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		insert cs;

		//Region__c pais = new Region__c(CurrencyIsoCode = 'ARS', Name='Testx001');
		//Load Test Data
		Map<String, BI_Code_ISO__c> map_region = BI_DataLoad.loadRegions();
    	BI_Code_ISO__c region;

       	for(BI_Code_ISO__c biCode: map_region.values()){
       		if(biCode.BI_Country_Currency_ISO_Code__c == 'ARS'){
       				region = biCode;
       		}
       	}
		if(region == NULL)
			System.assert(false, 'Region is empty: Check the BI_DataLoad.loadRegions Method');
		BI_Periodo__c per1 = new BI_Periodo__c(BI_Mes_Ano__c='01-2015', Name='Enero 2015');
		BI_Periodo__c per2 = new BI_Periodo__c(BI_Mes_Ano__c='04-2015', Name='Enero 2015');
		BI_Periodo__c per3 = new BI_Periodo__c(BI_Mes_Ano__c='02-2016', Name='Enero 2016');
		BI_Periodo__c per4 = new BI_Periodo__c(BI_Mes_Ano__c='01-2014', Name='Enero 2014');
		insert new list<BI_Periodo__c>{per1,per2,per3};

		BI_Rama__c rama = new BI_Rama__c(BI_Nombre__c='Testx001');
		insert rama;

		Account a1 = new Account(Name='Testx001', 
											  BI_Country__c  = region.Name, 
											  BI_Validador_Fiscal__c='Testx001',
											  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
	                                          BI_Activo__c = Label.BI_Si,
	                                          BI_Segment__c= 'Empresas',
	                                          BI_Subsegment_Local__c = 'Multinacionales');
		insert a1;

		List<BI_Registro_Datos_FOCO__c> toInsert = new List<BI_Registro_Datos_FOCO__c>();

		for (Integer i = 0; i < 50; i++) {
			Id toUse;
 
			if (i < 5) {
				toUse = per1.id;
			} else if (i < 10) {
				toUse = per4.id;
			} else if (i < 30) {
				toUse = per2.id;
			} else {
				toUse = per3.id;
			}

			BI_Registro_Datos_FOCO__c f = new BI_Registro_Datos_FOCO__c(BI_Cliente__c=a1.Id, 
																	BI_Monto__c = 100.40,
																	BI_Periodo__c=toUse,
																	BI_Rama__c=rama.Id,
																	BI_Tipo__c='Backlog (Recurrente)',
																	CurrencyIsoCode='ARS');

			toInsert.add(f);

		} 

		insert toInsert;

		Test.startTest();

		Database.executeBatch(new BI_FOCO_CountryDateHelperBatch(region.Name));

		Test.stopTest();

		List<BI_FOCO_Dates_by_Country__c> l = [SELECT Name, Min_Date__c, Max_Date__c FROM BI_FOCO_Dates_by_Country__c];

		system.assertEquals(1, l.size());

		system.assertEquals(Date.newInstance(2015,1,1), l[0].Min_Date__c);
		system.assertEquals(Date.newInstance(2016,2,1), l[0].Max_Date__c);
	}
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}