public with sharing class BI_Opportunity_HiddenSubmitButton_CTRL {

    private Opportunity opp;

    public boolean isAdmin {get; set;}
    public boolean edit {get;set;}
    public boolean casillaDesarrollo {get; set;}
    public boolean bypass {get;set;}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Constructor that validate if the user have the 'Administrador del sistema' profile
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        10/12/2015                      Guillermo Muñoz             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI_Opportunity_HiddenSubmitButton_CTRL(ApexPages.StandardController std) {
        
        opp = (Opportunity)std.getRecord();
        isAdmin = false;
        edit = false;
        casillaDesarrollo = false;
        bypass = false;
        if(UserInfo.getProfileId() == [SELECT Id FROM Profile WHERE Name =: Label.BI_Administrador_Sistema].Id){
            isAdmin = true;
        }

    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that enable the edit layout of the AdminSection
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        10/12/2015                      Guillermo Muñoz             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void editable(){
        edit = true;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that disable the edit layout of the AdminSection
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        10/12/2015                      Guillermo Muñoz             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void cancel(){
        edit = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that save the modifications of the AdminSection
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        10/12/2015                      Guillermo Muñoz             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference save(){

        Map<Integer,BI_bypass__c> bypassInstance;

        if(casillaDesarrollo && bypass){
            opp.addError('Solo puedes seleccionar una de las opciones de la sección "Validaciones"');
        }
        else{

            if(casillaDesarrollo){
                opp.BI_Casilla_desarrollo__c = true;
            }
            else if(bypass){
                bypassInstance = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, false, false, false);
            }
            try{
                update opp;
                edit = false;
                casillaDesarrollo = false;
                bypass = false;
            }
            catch(Exception e){
                 opp.addError(e.getMessage());
            }
            finally{
                BI_MigrationHelper.disableBypass(bypassInstance);
            }
        }
        return new PageReference('/apex/BI_Opportunity_HiddenSubmitButton?id=' + opp.Id);

    }
}