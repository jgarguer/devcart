global class BI_FOCO_BatchUpdFOCOFromOppty implements Database.Batchable<sObject> {
	
	global final String country;
	global Integer failed;
	global String errMsg;


	global BI_FOCO_BatchUpdFOCOFromOppty(String c) {
		failed=0;
		country = c;
		
	}
	
	//global Iterable<sObject> start(Database.BatchableContext BC) {
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('-------------------------------------------BI_FOCO_BatchUpdFOCOFromOppty.start()');

		//Max and min date of Oppty to be considered would the first and last day of the 
			//first and last period in the FOCO table respectively
		//AggregateResult[] ar = [SELECT max(BI_Fecha_Inicio_Periodo__c) maxDate, min(BI_Fecha_Inicio_Periodo__c) minDate from BI_Registro_Datos_FOCO__c WHERE BI_Country__c = :country];

		//Date xd = (Date)ar[0].get('maxDate');
		//Date nd = (Date)ar[0].get('minDate');


		map<string, BI_FOCO_Dates_by_Country__c> countryMap = BI_FOCO_Dates_by_Country__c.getAll();
		BI_FOCO_Dates_by_Country__c countryDate = countryMap.get(country);

		Date xd;
		Date nd;

		if (countryDate != null) {
			xd = countryDate.Max_Date__c;
			nd = countryDate.Min_Date__c;
		}
	
		//Get first day of the month
		Date focoStartDate = nd;

		//Get last day of the month
		Date focoEndDate;
		if(xd !=null){
			focoEndDate = xd.addMonths(1).addDays(-1);
		}

		if(focoStartDate == null || focoEndDate ==null ){
			throw new BI_FOCO_Exception('Error while calculating Date range from BI_Registro_Datos_FOCO__c');
			
		}

		Map<String, BI_FOCO_Pais__c> mcs =  BI_FOCO_Pais__c.getAll();

		
		//Query all opportunities that falls within the criteria and the date range
		List<Opportunity> oppList = new List<Opportunity>();
		System.debug('-------------------------------------------BI_FOCO_BatchUpdFOCOFromOppty.start(). Query-------------------------: SELECT Id, AccountId, Account.Id, Account.BI_Country__c, CloseDate, BI_Comienzo_estimado_de_facturacion__c, BI_Duracion_del_contrato_Meses__c, RecordTypeId,  BI_Recurrente_bruto_mensual__c, BI_Recurrente_bruto_mensual_anterior__c  FROM Opportunity WHERE CloseDate > '+focoStartDate+' AND CloseDate < '+focoEndDate+' and Account.BI_Country__c = \''+country+'\' AND (Stagename=\'F5 - Solution Definition\' OR  Stagename=  \'F4 - Offer Development\' OR Stagename= \'F3 - Offer Presented\' OR Stagename= \'F2 - Negotiation\') and RecordTypeId NOT IN(SELECT Id from RecordType where name = \'Agrupación\') AND BI_Probabilidad_de_Exito_Numero__c >= '+(Decimal)mcs.get(country).Opportunity_Probability__c + ' AND Generate_Acuerdo_Marco__c = FALSE');

/*
		oppList = [	SELECT Id, AccountId, Account.Id, Account.BI_Country__c, CloseDate, BI_Comienzo_estimado_de_facturacion__c, BI_Duracion_del_contrato_Meses__c, RecordTypeId,  BI_Recurrente_bruto_mensual__c, BI_Recurrente_bruto_mensual_anterior__c, CurrencyIsoCode
					FROM Opportunity WHERE CloseDate > :focoStartDate
					AND CloseDate < :focoEndDate and Account.BI_Country__c = :country
					AND (Stagename ='F5 - Solution Definition' OR Stagename = 'F4 - Offer Development' OR Stagename = 'F3 - Offer Presented' OR Stagename ='F2 - Negotiation')
					AND RecordTypeId NOT IN (SELECT Id FROM RecordType WHERE name = 'Agrupación')
					AND BI_Probabilidad_de_Exito_Numero__c >= :(Decimal)mcs.get(country).Opportunity_Probability__c
					AND Generate_Acuerdo_Marco__c = FALSE
				];
		*/

		//System.debug('-------------------------------------------BI_FOCO_BatchUpdFOCOFromOppty.End(). Processing '+oppList.size()+'records');
		//return oppList;
		return Database.getQueryLocator([	SELECT Id, AccountId, Account.Id, Account.BI_Country__c, BI_Country__c, CloseDate, BI_Comienzo_estimado_de_facturacion__c, BI_Duracion_del_contrato_Meses__c, RecordTypeId,  BI_Recurrente_bruto_mensual__c, BI_Recurrente_bruto_mensual_anterior__c, CurrencyIsoCode
					FROM Opportunity WHERE CloseDate > :focoStartDate
					AND CloseDate < :focoEndDate and Account.BI_Country__c = :country
					AND (Stagename ='F5 - Solution Definition' OR Stagename = 'F4 - Offer Development' OR Stagename = 'F3 - Offer Presented' OR Stagename ='F2 - Negotiation')
					AND RecordTypeId NOT IN (SELECT Id FROM RecordType WHERE name = 'Agrupación')
					AND BI_Probabilidad_de_Exito_Numero__c >= :(Decimal)mcs.get(country).Opportunity_Probability__c
					AND Generate_Acuerdo_Marco__c = FALSE
				]); 
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		List<Opportunity> oList = (List<Opportunity>)scope;

   		List<BI_Registro_Datos_FOCO__c> fList = BI_FOCO_OpptyUpdate.generateFOCOsForOppties(oList);
   		
   		List<Database.SaveResult> dsrs;
   		try{
   			dsrs = Database.insert(fList, false);
   		}
   		catch(DmlException dmle){
			throw new BI_FOCO_Exception('Error while batch inserting FOCO records', dmle);

		}
		catch(Exception e){
			throw new BI_FOCO_Exception('Error while batch inserting FOCO records', e);
		}

		for(Database.SaveResult dsr : dsrs){
			if(!dsr.isSuccess()){
				failed++;
				for(Database.Error err : dsr.getErrors()) {
					errMsg = errMsg + err.getStatusCode() + ': ' + err.getMessage() + '\n';
				}
			}
		}

		if(failed > 0){
			throw new BI_FOCO_Exception('Batch insert failed for '+failed +' records for '+country+'. Details below - \n '+errMsg);
		}

	
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}