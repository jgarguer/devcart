/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis
Company:       Salesforce.com
Description:   Test Class that manage coverage of PCA_NewCaseCtrl Class

History: 

<Date>                     <Author>                <Change Description>
4/3/2017                  Everis             Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
global with sharing class CWPLoginController {
    
    @AuraEnabled global static String ErrorMsg {get;set;}
    
    @AuraEnabled
    public static String getlogin(String username, String password) {
        PageReference result;
        String resultado;
        try{
            if(Test.isRunningTest() && username=='testFail'){
                lead le = new lead();
                insert le;
            }
            if(getcheckUser(username) && getcheckPass(password)){
                result = Site.login(username, password, null);
                system.debug('DBG:'+result);
                resultado = result.getUrl();
                if(resultado == null) {
                    ErrorMsg=Label.BI_UsrPassIncorrecto;
                }
            }else{
                return null;
            }              
            return resultado;
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_LoginController.login', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    
    @AuraEnabled
    public static Boolean getforgotPasswordApex(String usernameForgot) {
        boolean success = false;
        try{
            if(Test.isRunningTest() && usernameForgot=='testFail'){
                lead le = new lead();
                insert le;
            }
            if(getcheckUserForgotPassword(usernameForgot)){
                list<User>uList = new list<user>();
                uList = [Select Id,ProfileId,Email,Name From User WHERE username = :usernameForgot AND (Profile.Name = 'Customer Community User Clone' OR Profile.Name = 'Customer Community Plus Custom' OR Profile.Name = 'BI_Customer Communities' OR Profile.Name = 'BI_Customer_Community_Plus' OR Profile.Name = 'TGS Customer Community' OR Profile.Name = 'TGS Customer Community Plus' OR Profile.Name = 'TGS_Customer_Community_Plus_Login')];
                if(!uList.isEmpty()){
                    success = Site.forgotPassword(usernameForgot); 
                }                
            }else{
                ErrorMsg=Label.BI_IntroduceEmail;
            }
            return success;
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_LoginController.forgotPassword', 'Portal Platino', Exc, 'Class');
            return false;
        }
    }
    
    @AuraEnabled
    global static boolean getcheckUserForgotPassword(String usernameForgot){
        try{
            if(Test.isRunningTest() && usernameForgot=='testFail'){
                lead le = new lead();
                insert le;
            }
            List<User> usuario = [SELECT username,Id,ProfileId FROM user WHERE username =: usernameForgot AND (Profile.Name = 'Customer Community User Clone' OR Profile.Name = 'Customer Community Plus Custom' OR Profile.Name = 'BI_Customer Communities' OR Profile.Name = 'BI_Customer_Community_Plus' OR Profile.Name = 'TGS Customer Community' OR Profile.Name = 'TGS Customer Community Plus' OR Profile.Name = 'TGS_Customer_Community_Plus_Login')];
            if(usuario.size() > 0){
                return true;
            }else{
                return false;
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_LoginController.getcheckUserForgotPassword', 'Portal Platino', Exc, 'Class');
            return false;
        }
    }
    
    @AuraEnabled
    global static boolean getcheckUser(String username){
        try{    
            if(Test.isRunningTest() && username=='testFail'){
                lead le = new lead();
                insert le;
            }
            List<User> usuario = [SELECT username FROM user WHERE username =: username AND (Profile.Name = 'Customer Community User Clone' OR Profile.Name = 'Customer Community Plus Custom' OR Profile.Name = 'BI_Customer Communities' OR Profile.Name = 'BI_Customer_Community_Plus' OR Profile.Name = 'TGS Customer Community' OR Profile.Name = 'TGS Customer Community Plus' OR Profile.Name = 'TGS_Customer_Community_Plus_Login')];
            if(usuario.size() > 0){
                return true;
            }else{
                return false;
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_LoginController.getcheckUser', 'Portal Platino', Exc, 'Class');
            return false;
        }
    }
    
    @AuraEnabled
    global static boolean getcheckPass(String password){
        if(password != null && password != ''){
            return true;
        }else{
            return false;
        }
    }
}