public class BIIN_CISearch_Creacion_Ctrl extends BIIN_UNICA_Base
{

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Morales Rodríguez, Jose María Martín Castaño
    Company:       Deloitte
    Description:   Clase asociada al Pop Up de búsqueda del CI de BIIN_CreacionIncidencia
        
    History:
        
    <Date>             <Author>                                             <Description>
    10/12/2015         Ignacio Morales Rodríguez, Jose María Martín         Initial version
    11/05/2016         José Luis González Beltrán                           Adapt to UNICA interface
    07/06/2016         Julio Laplaza                                        Error handling. URL modification
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public String CIName {get;set;}
    public String CIId {get;set;}
    public String query {get;set;}
    public String accountName {get;set;}
    public String siteName {get;set;}
    public List<Case> lcases {get;set;}
    public Case caso {get;set;}
    public boolean BoolDeshabilitar {get;set;} 
    public integer queryLength {get;set;}

    public boolean MostrarError {get;set;}
    public String errorMessage {get;set;}
    
    private transient HttpRequest req;
    public HttpRequest getHttpRequest () {
        return req;
    }
    
    private transient HttpResponse res;
    public HttpResponse getHttpResponse () {
        return res;
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Jose María Martín Castaño
	    Company:       Deloitte
	    Description:   Constructor, donde se definen las variables globales y se toma el valor heredado del CI de la página padre de creación. 
	    History:
	    
	    <Date>            <Author>          			<Description>
	    12/12/2015      Jose María Martín Castaño    	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
    public BIIN_CISearch_Creacion_Ctrl ()
    {
        MostrarError = false;
        if(Test.isRunningTest()) {
            CIname = '';
            CIId   = '';
        } 
        query       = ApexPages.currentPage().getParameters().get('inputfield');
        accountName = ApexPages.currentPage().getParameters().get('accountfield');
        siteName    = ApexPages.currentPage().getParameters().get('sitefield');
        queryLength = query.length();
        if(queryLength < 3) {       
            BoolDeshabilitar = true;
        } else {
            BoolDeshabilitar = false;  
        }
        System.debug(' BIIN_CISearch_Creacion_Ctrl | Busqueda: ' + query);
        lcases  = new List<Case>();
        caso    = new Case();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Jose María Martín Castaño
	    Company:       Deloitte
	    Description:   Función que obtiene la lista de CI.
	    
	    History:
	    
	    <Date>           <Author>          				    <Description>
	    12/12/2015       Jose María Martín Castaño      	Initial version
        11/05/2016       José Luis González Beltrán         Adapt to UNICA interface
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void getCI()
    {
        try {
            // Get account
            Account account = getAccount(accountName);

            // Callout URL
            String url = 'callout:BIIN_WS_UNICA/asset/v1/assets';
            url = BIIN_UNICA_Utils.addParameterAdditional(url, 'CompanyID', account.BI_Validador_Fiscal__c);
            if(!String.isEmpty(siteName))
            {
                url = BIIN_UNICA_Utils.addParameterAdditional(url, 'Site', siteName);
            }
            url = BIIN_UNICA_Utils.addParameter(url, 'assetName', query);

           	// Prepare Request
            HttpRequest req = getRequest(url, 'GET');

            BIIN_UNICA_Pojos.ResponseObject response = getResponse(req, BIIN_UNICA_Pojos.CisListType.class);
            System.debug(' BIIN_SiteSearch_Creacion_Ctrl.getSite | La salida del servicio es (response): ' + response);

            BIIN_UNICA_Pojos.CisListType cisList = (BIIN_UNICA_Pojos.CisListType) response.responseObject;
            System.debug(' BIIN_SiteSearch_Creacion_Ctrl.getSite | Lista de CIs disponibles (cisList): ' + cisList);
            
            Case caso = new Case();
            if (cisList.totalResults > 0) 
            {
                // clean output list
                lcases = new List<Case>();
                for(BIIN_UNICA_Pojos.CisType ci : cisList.assets) 
                {
                    caso.BIIN_Id_Producto__c = ci.name;
                    caso.BIIN_Form_Name__c = ci.classId;
                    caso.BI_Categorization_Tier_1__c = ci.category;
                    caso.BI_Categorization_Tier_2__c = ci.type;
                    caso.BI_Categorization_Tier_3__c = ci.company;
                    lcases.add(caso);
                    caso = new Case();
                }
            }
            else
            {
                MostrarError = true;
                errorMessage = Label.BIIN_NO_RESULTS_MODIFY_CRITERIA;
            }

            System.debug ('SITE-SUCCESS: Salida: ' + lcases);
        }
        catch(Exception e) 
        {
            System.debug ('SITE-ERROR' + e);
            
            MostrarError = true;
            errorMessage = e.getMessage();
        }
	}   
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez	  
	    Company:       Deloitte
	    Description:   Función que cuenta el número de caracteres del campo de búsqueda para habilitar el botón Ir.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez	     	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public void HabilitarIr()
    {
        queryLength = query.length();
        if(queryLength < 3) {       
            BoolDeshabilitar = true;
        } else {
            BoolDeshabilitar = false;  
        } 
        system.debug('HA ENTRADO EN HabilitarIr---->' + BoolDeshabilitar + queryLength );
    }

    public void closePopUp()
    {
        MostrarError = false;
    }

}