@isTest
public class CWP_FieldSetHelperLightning_TEST {
  @testSetup 
    private static void dataModelSetup() {               
        
        //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{
            'Account', 
                'Case',
                'NE__Order__c',
                'NE__OrderItem__c'
                };
                    
                    //ACCOUNT
                    map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            //rtMapBydevName.put(i.DeveloperName, i.id);
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accLegalEntity;     // Level 3       
        System.runAs(unUsuario){        
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            insert accLegalEntity;            
        }          
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        contactTest.BI_Cuenta_activa_en_portal__c = accLegalEntity.id;
        insert contactTest;
        
        User usuario;
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            usuario.Pais__c='Peru';
            insert usuario;
        }
        
        //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 'Catalogo padre';
        producto.TGS_CWP_Tier_2__c = 'Catalogo hijo';
        insert producto;
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Product', catalogCategoryChildren.id, catalogo.id, producto.id);
        insert catalogoItem;
        
        //ORDER
        
        //NE__Asset__c testAsset = CWP_TestDataFactory.createAsset();
        //insert testAsset;
        //Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
        //NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId, NE__Type__c = 'Asset');        
        //NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        //insert testOrder;
        
        
        
        //private final set<string> REQUESTRT = new set<string>{'Order_Management_Case'};
        //private final set<string> CASERT = new set<string>{'TGS_Billing_Inquiry', 'TGS_Change', 'TGS_Complaint', 'TGS_Incident', 'TGS_Problem', 'TGS_Query'};
        
        //CASO     
        list <Case> listaDeCasos = new list <Case>();     
        //BI2_caso_comercial
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('BI2_caso_comercial'), 'sub1', 'Assigned', '');
        newCase.AccountId = accLegalEntity.id;
        newCase.contactId = contactTest.id;     
        newCase.BI_Confidencial__c = false;
        newCase.BI_Line_of_Business__c  = 'test';
        newCase.BI_Product_Service__c   = 'test';
        newCase.TGS_Disputed_number_of_lines__c = 0;
        newCase.BI2_PER_Territorio_caso__c = 'test';
        newCase.BI2_per_tipologia__c = 'test';
        newCase.BI2_per_subtipologia__c  = 'test';
    newCase.BI2_fecha_hora_de_resolucion__c=datetime.now();
        listaDeCasos.add(newCase);
        insert listaDeCasos;        
        
        
        
        NE__Order__c testOrder2 = CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', accLegalEntity.id);
        testOrder2.Case__c = listaDeCasos[0].id;
        insert testOrder2;        
        
        // ORDER ITEM
        NE__OrderItem__c newOI;        
       // insert newOI;       
        
        newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder2.id, producto.id, catalogoItem.id,  2);
        newOI.NE__Status__c='Activo';
        insert newOI;      
        
        NE__Family__c family = CWP_TestDataFactory.createFamily('corleone');
        insert family;
        
        NE__DynamicPropertyDefinition__c dynProp = CWP_TestDataFactory.createDynamiyProperty('dynamic');
        insert dynProp;
        
        NE__ProductFamilyProperty__c famProp = CWP_TestDataFactory.createFamilyProperty(family.id, dynProp.id); 
        famProp.TGS_Is_key_attribute__c = true;
        famProp.CWP_KeyValue__c = true;
        insert famProp;
        
        NE__Order_Item_Attribute__c oia1 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        oia1.Name = 'picked';
        oia1.NE__Value__c = 'filter';
        insert oia1;
        NE__Order_Item_Attribute__c oia3 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        oia3.Name = 'picked2';
        oia3.NE__Value__c = 'filter';
        insert oia3;
        
        NE__Order_Item_Attribute__c oia2 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        insert oia2;      
   
    }
    
    
    
    
    @isTest
     private static void Test1() {     
        BI_TestUtils.throw_exception=false;
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();      
            system.debug('#####    usuario: '+usuario.id);
            Case c= [SELECT id,CaseNumber,TGS_Ticket_Site__c,AccountId,RecordTypeId,BI_Confidencial__c FROM Case Limit 1];
            String tControlS='init';
            system.debug('#####    AccountId: '+c.AccountId + ' RecordTypeId: '+c.RecordTypeId + ' BI_Confidencial__c: '+c.BI_Confidencial__c);
            CWP_ClaimsRequestCtrl.getRecords(tControlS);
            CWP_ClaimsRequestCtrl.TableController tControl=new CWP_ClaimsRequestCtrl.TableController('tickets',1,5,5,5,'','Name::ASC');
            tControlS=JSON.serialize(tControl);
            CWP_ClaimsRequestCtrl.getRecords(tControlS);
            
            
            CWP_FieldSetHelperLightning.FieldSetResult fs = new CWP_FieldSetHelperLightning.FieldSetResult('a','b');
            List<CWP_FieldSetHelperLightning.FieldSetResult> lfs = new List<CWP_FieldSetHelperLightning.FieldSetResult>();
            lfs.add(fs);
            CWP_FieldSetHelperLightning.FieldSetRecord fsr = new CWP_FieldSetHelperLightning.FieldSetRecord(null,lfs);
            
            List<String> heads = new List<String>();
            heads.add('aux');
            List<CWP_FieldSetHelperLightning.FieldSetRecord> regs = new List<CWP_FieldSetHelperLightning.FieldSetRecord>();
            regs.add(fsr);
           // CWP_FieldSetHelperLightning.FieldSetContainer(heads, regs);
            CWP_FieldSetHelperLightning.FieldSetContainer fieldSetRecords = CWP_FieldSetHelperLightning.FieldSetHelperLightningSearch('BI2_DetailCaseComercialResolved', 'Case', '', '', '');          
            CWP_FieldSetHelperLightning.FieldSetContainer fieldSetRecords7 = CWP_FieldSetHelperLightning.FieldSetHelperLightningSearch('BI2_DetailCaseComercialResolved', 'Case', 'Order;1::ASC', '', ''); 
            CWP_FieldSetHelperLightning.FieldSetContainer fieldSetRecords2 = CWP_FieldSetHelperLightning.FieldSetHelperLightningSearch('BI_CoE_Informacion_Administrador', 'Opportunity', '', '', '');
            CWP_FieldSetHelperLightning.FieldSetContainer fieldSetRecords3 = CWP_FieldSetHelperLightning.FieldSetHelperLightningSearch('PCA_DetailContact', 'Contact', '', '', '');
            CWP_FieldSetHelperLightning.FieldSetContainer fieldSetRecords4 = CWP_FieldSetHelperLightning.FieldSetHelperLightningSearch('BI2_DetailCaseComercialResolved', '', '', '', '');
            CWP_FieldSetHelperLightning.FieldSetContainer fieldSetRecords5 = CWP_FieldSetHelperLightning.FieldSetHelperLightningSearch('PCA_MainTableFacturacion', 'BI_Facturacion__c', '', '', '');
            CWP_FieldSetHelperLightning.FieldSetContainer fieldSetRecords6 = CWP_FieldSetHelperLightning.FieldSetHelperLightningSearch('PCA_DetailContact', 'Contact', null, '', '');
            
            BI_TestUtils.throw_exception=true;
            CWP_FieldSetHelperLightning.FieldSetResult fs2 = new CWP_FieldSetHelperLightning.FieldSetResult('a','b');
            CWP_FieldSetHelperLightning.FieldSetRecord fsr2 = new CWP_FieldSetHelperLightning.FieldSetRecord(null,lfs);
            CWP_FieldSetHelperLightning.FieldSetContainer fsc = new CWP_FieldSetHelperLightning.FieldSetContainer(null,null,null,null);
            CWP_FieldSetHelperLightning.FieldSetHelperLightningSearch ('fieldSetName', 'ObjectName', 'searchMethod', 'accountField', 'orderType');    
            Test.stopTest();
        }       
         
     }
}