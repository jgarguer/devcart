@isTest
private class BI_FacturacionMethods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_FacturacionMethods class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    23/07/2014              Micah Burgos            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_FacturacionMethods.createTask
   
    History:
    
    <Date>                  <Author>                <Change Description>
    23/07/2014              Micah Burgos            Initial Version    
    20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c    
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void createTask_Test() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        Id idProfile = BI_DataLoad.searchAdminProfile();
        
        List<User> lst_user = BI_DataLoad.loadUsers(1, idProfile, Label.BI_Administrador);
        system.debug('BI_FacturacionMethods_TEST-> lst_user: ' + lst_user);
        system.runAs(lst_user[0]){
            
            
           List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(3);
            
           system.debug('BI_FacturacionMethods_TEST->lst_pais ' +lst_pais);      
           List<Account> lst_acc = new List<Account>();
                for(Integer j = 0; j < 3; j++){
                    Account acc = new Account(Name = 'test'+ String.valueOf(j),
                                              BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                              BI_Activo__c = Label.BI_Si,
                                              BI_Country__c = lst_pais[j],
                                              BI_Segment__c = 'test',
                                              BI_Subsegment_Regional__c = 'test',
                                              BI_Territory__c = 'test');
                                              //Region__c = lst_pais[j].id);
                    
                    lst_acc.add(acc);
                }
            insert lst_acc;
            
            
            system.debug('BI_FacturacionMethods_TEST->lst_acc ' +lst_acc);
            List<String> lst_criterio = new List<String>();
                lst_criterio.add('Con deuda <30 días');
                lst_criterio.add('Con deuda 30-60 días');
                lst_criterio.add('Con deuda 60-90 días');
                lst_criterio.add('Con deuda 90-120 días');
                lst_criterio.add('Con deuda >120 días');

            
            List<BI_Tarea_Obligatoria__c> lst_tob = new List<BI_Tarea_Obligatoria__c>();
                for(Integer j = 0; j < 3; j++){
                    for(Integer i = 0; i < 5; i++){
                        system.debug('BI_FacturacionMethods_TEST->lst_pais['+j+'] ' +lst_pais[j]); 
                        
                        BI_Tarea_Obligatoria__c tob = new BI_Tarea_Obligatoria__c(BI_Asunto__c = 'test'+string.valueof(i) + string.valueof(j),
                                                                                  BI_Country__c = lst_pais[j],
                                                                                  BI_Asignar_a__c = Label.BI_GestorPropietario,
                                                                                  BI_Rol_propietario__c = Label.BI_RolEjecutivoClientes,
                                                                                  BI_Tipo__c = Label.BI_TareaObligatoria,
                                                                                  BI_Criterio_de_creacion__c = lst_criterio[i],
                                                                                  BI_Typology__c = Label.BI_TipologiaTareaDeudas);
                        
                        system.debug('BI_FacturacionMethods_TEST->tob lst_pais[j] : ' +lst_pais[j]);
                        lst_tob.add(tob);
                    }
                }
                insert lst_tob;
                
                List<AccountTeamMember> lst_otm = new List<AccountTeamMember>();
                for(Integer j = 0; j < 3; j++){
                    AccountTeamMember otm = new AccountTeamMember(AccountId = lst_acc[0].id,
                                                                  UserId = lst_user[0].Id ,
                                                                  TeamMemberRole = Label.BI_RolEjecutivoCobros);
                    lst_otm.add(otm);
                }
                
                system.debug('BI_FacturacionMethods_TEST->lst_otm' +lst_otm);
                insert lst_otm ;
                 List <BI_Facturacion__c> lst_fact = new List <BI_Facturacion__c>();
                
                for(Integer j = 0; j < 100; j++){//300 Registros de prueba
                    for(Integer i =0 ; i<3; i++){
                        
                    BI_Facturacion__c fact = new BI_Facturacion__c();
                        fact.BI_Cliente__c = lst_acc[i].Id;
// Field is not writeable: fact.BI_Country__c = lst_pais[i];
                        fact.BI_Deuda_0_30_dias__c = 12.90;
                        fact.BI_Deuda_30_60_dias__c = 10;                       
                        fact.BI_Deuda_60_90_dias__c = 2.000011;
                        fact.BI_Deuda_90_120_dias__c = 1992346; 
                        fact.BI_Deuda_mayor_120_dias__c = 23;
                        
                        system.debug('BI_FacturacionMethods_TEST->fact PAIS ' +lst_pais[i]);
                        lst_fact.add(fact);
                    }
    
                   
                }
                
                Test.startTest();
                                    
                insert lst_fact;
                
                list<Id> lst_factIds = new list<Id>();
                for(BI_Facturacion__c fact :lst_fact){
                    lst_factIds.add(fact.Id);
                }
                system.assert(!lst_fact.isEmpty());

                list<Task> lst_tasks = [SELECT Id FROM Task WHERE WhatId IN :lst_factIds ];
                system.assert(!lst_tasks.isEmpty());
                //system.assertEquals('SIZE TASK: ',String.valueOf([SELECT Id FROM Task WHERE WhatId IN :lst_factIds ].size()));//2500
                

        }
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_FacturacionMethods.checkMaxDate()
   
    History:
    
    <Date>                  <Author>                <Change Description>
    23/07/2014              Micah Burgos            Initial Version       
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void checkMaxDate_Test() {     
        
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List <String> lst_region = BI_DataLoad.loadPaisFromPickList(1);
        
        //BI_Pickup_Option__c pickupOption = BI_DataLoadRest.loadPickUpOptions(lst_region);                                                    
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(2, lst_region, false);
        
        List<BI_Facturacion__c> lst_old = new List<BI_Facturacion__c>();
        List<BI_Facturacion__c> lst_new = new List<BI_Facturacion__c>();
        
        BI_Facturacion__c fact;
        
        //OLD FACTS
        for(Integer i = 0; i<100; i++){//100
            fact = new BI_Facturacion__c();
            
            if(i/2 == 0 ){
                fact.BI_Cliente__c  = lst_acc[1].Id;
            }else{
                fact.BI_Cliente__c  = lst_acc[0].Id;
            }
            
          //  fact.BI_Country__c = lst_region[0];
            fact.BI_Fecha_de_carga__c = Date.today();
            
            lst_old.add(fact);
        }
        
        insert lst_old;
        
        //NEW FACTS
        for(Integer i = 0; i<200; i++){//200
            fact = new BI_Facturacion__c();
            
            if(i/2 == 0 ){
                fact.BI_Cliente__c  = lst_acc[1].Id;
            }else{
                fact.BI_Cliente__c  = lst_acc[0].Id;
            }
            
    //        fact.BI_Country__c = lst_region[0];
            fact.BI_Fecha_de_carga__c = Date.today().addDays(1);
            
            lst_new.add(fact);
        }
        
        Test.startTest();
        
        list<Id> lst_oldID = new list<Id>();

        for(BI_Facturacion__c oldFact :lst_old){
            lst_oldID.add(oldFact.Id);
        }
        
        for(BI_Facturacion__c oldFact :[SELECT Id, BI_Contabiliza_registro__c FROM BI_Facturacion__c WHERE Id IN :lst_oldID]){
            system.debug('***oldFact: '+ oldFact);
            system.assert(oldFact.BI_Contabiliza_registro__c);
            
        }
        
        insert lst_new;
        
        for(BI_Facturacion__c oldFact :[SELECT Id, BI_Contabiliza_registro__c FROM BI_Facturacion__c WHERE Id IN :lst_oldID]){
            system.debug('***oldFact AFTER_UPDATE: '+ oldFact);
            system.assertEquals(oldFact.BI_Contabiliza_registro__c, false);
        }
        
        list<Id> lst_newID = new list<Id>();
        for(BI_Facturacion__c newFact :lst_new){
            lst_newID.add(newFact.Id);
        }      
        
        for(BI_Facturacion__c newFact :[SELECT Id, BI_Contabiliza_registro__c FROM BI_Facturacion__c WHERE Id IN :lst_newID]){
            system.debug('***newFact: '+ newFact);
            system.assert(newFact.BI_Contabiliza_registro__c);
        }
        Test.stopTest();
        
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}