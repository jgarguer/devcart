@isTest
public class TGS_CaseAutoclose_TEST {

    @isTest
    public static void test_autoclose() {
        insert new TGS_User_Org__c(TGS_Is_TGS__c = true);
        BI_TestUtils.throw_exception = false;
        
        User portUsr = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(portUsr) {
            NE__OrderItem__c oi = TGS_Dummy_Test_Data.dummyConfiguration(UserInfo.getUserId(), 'New'); 
            Case cas = [SELECT Id FROM Case WHERE Order__c = :oi.NE__OrderId__c];
            Test.startTest();
            cas.TGS_Casilla_Desarrollo__c = true;
            cas.Status = 'In Progress';
            cas.TGS_Status_Reason__c = 'In Provision';
            update cas;
            
            cas.TGS_Casilla_Desarrollo__c = true;
            cas.Status = 'Resolved';
            cas.TGS_Status_Reason__c = null;
            update cas;
            
            cas.TGS_Resolve_Date__c = Date.today().addDays(- Integer.valueOf(Label.TGS_Autoclose_date) * 5);
            update cas;
            Test.stopTest();
        }
        BI_TestUtils.throw_exception = false;
        System.schedule('AAATESTAAA TGS_CaseAutoclose', '20 30 8 10 2 ?', new TGS_CaseAutoclose());
    }
    
    @isTest
    public static void test_exceptions() {
        BI_TestUtils.throw_exception = true;
        new TGS_CaseAutoclose.ProcessAutoClose('').execute(null);
    }

}