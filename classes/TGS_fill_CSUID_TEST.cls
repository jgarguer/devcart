@isTest
public class TGS_fill_CSUID_TEST { //Clase de test generada para la clase TGS_fill_CSUID
    
    static testMethod void csuid_test(){
        
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        //Se escoge el perfil dentro de Salesforce
        System.runAs(new User(Id = Userinfo.getUserId())) {     
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            //insert uO;
        }
        // Se genera una user organization TGS para que el trigger salte.
           
        User userTest = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        userTest.BI_Permisos__c = 'TGS';
        //insert userTest;
        
       //Se genera un usuario con permisos TGS y profile TGS System Administrator
        
        //System.runAs(userTest){
            //Se genera un Order que tenga un Case de tipo "Order Management"
            Test.StartTest();
            Id rtId_Asset = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Asset');
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.dummyConfigurationOrder();     //Con este comando se genera un order item con una order y case relacionados
            //NE__Order__c order_RTAsset = [SELECT Id, RecordTypeId FROM NE__Order__c WHERE Id = :testOrderItem_Asset.NE__OrderId__c];
            NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId_Asset, NE__Type__c = 'Asset');
            insert testOrder;
            NE__OrderItem__c testOrderItem_Asset = new NE__OrderItem__c(NE__OrderId__c = testOrder.Id, NE__ProdId__c = testOrderItem.NE__ProdId__c, NE__CatalogItem__c = testOrderItem.NE__CatalogItem__c, NE__Qty__c=1);
            insert testOrderItem_Asset;
            NE__ProductFamilyProperty__c familyProp = [SELECT Id FROM NE__ProductFamilyProperty__c];
            List<NE__Order_Item_Attribute__c> ListAttributes =  new List<NE__Order_Item_Attribute__c>();
        
            NE__Order_Item_Attribute__c attribute = new NE__Order_Item_Attribute__c(Name = 'Attribute 1', NE__Value__c = 'test', NE__Order_Item__c = testOrderItem_Asset.Id, NE__FamPropId__c=familyProp.Id );
            ListAttributes.add(attribute);
            NE__Order_Item_Attribute__c attributeID = new NE__Order_Item_Attribute__c(Name = 'Attribute 1_id', NE__Value__c = 'test', NE__Order_Item__c = testOrderItem_Asset.Id, NE__FamPropId__c=familyProp.Id );
            ListAttributes.add(attributeID);
            NE__Order_Item_Attribute__c attributeClass = new NE__Order_Item_Attribute__c(Name = 'Attribute 1_class', NE__Value__c = '', NE__Order_Item__c = testOrderItem_Asset.Id, NE__FamPropId__c=familyProp.Id );
            ListAttributes.add(attributeClass);
            insert ListAttributes;

            Case testCase = [Select Id,asset__c,status FROM Case WHERE Order__c=:testOrderItem.NE__OrderId__c];
            testCase.Asset__c = testOrderItem_Asset.NE__OrderId__c;                                                   //Se añade el asset al case.
            testCase.Order__c = testOrderItem.NE__OrderId__c;
            update testCase;
            /*List<NE__Order_Item_Attribute__c> lst_toUpdate = new List<NE__Order_Item_Attribute__c>();                                                        //Se cierra el case
            for(NE__Order_Item_Attribute__c ATTR : [SELECT Id, NE__AttrEnterpriseId__c from NE__Order_Item_Attribute__c where NE__Order_Item__c = :testOrderItem.Id]){
                ATTR.NE__AttrEnterpriseId__c = ATTR.Id;
                lst_toUpdate.add(ATTR);
            }
            update lst_toUpdate;
            System.debug('PRUEBA PRUEBA PRUEBA '+lst_toUpdate);*/
            testOrderItem.NE__AssetItemEnterpriseId__c = testOrderItem_Asset.Id;
            testOrderItem_Asset.NE__AssetItemEnterpriseId__c = testOrderItem.Id;
            testOrderItem.NE__Action__c='Add';
            update testOrderItem;
            update testOrderItem_Asset;
            testCase.status = 'Closed'; 
            update testCase;
            Map<Id,Case> map_fillParameters = new Map<Id,Case>();
            map_fillParameters.put(testCase.Id, testCase);
            Map<String, NE__Order__c> map_fillParameters2 = new Map<String,NE__Order__c>();
            TGS_fill_CSUID.csuid(testCase,TGS_fill_CSUID.fill_EnterpriseID(map_fillParameters));
            //TGS_fill_CSUID.csuid(testCase,map_fillParameters2);
            Test.StopTest();
        //}
    }
}