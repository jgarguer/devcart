/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for CWP_IncMod visualforce page. This page is needed
                   to insert cases for my Service since my page cannot do it itself due 
                   to it has been declared as readOnly
    Test Class:    CWP_IncModCreationController
    
    History:
     
    <Date>                  <Author>                <Change Description>
    14/03/2017              Everis                    Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public without sharing class CWP_IncModCreationController{
    public string resultId{get; set;}
    public integer result{get; set;}
    public string tipoCaso {get;set;}
    public List<Attachment> attachments {get;set;}
    public customAtt currentAtt {get;set;}
    public map<string,id> wiMap{get;set;} 
    
    public boolean allToRoD{get;set;}
    public Set<Id> casesIdSet{get;set;}
    public Set<Id> workInfosIdSet{get;set;}
    
    /*constructor*/
    public CWP_IncModCreationController(){
        result = 0;
        currentAtt = new customAtt();
        attachments = new List<Attachment>();
        casesIdSet = new Set<Id>();
        workInfosIdSet = new Set<Id>();
        allToRoD = false;
    }
    
    /*
    Developer Everis
    Function: retrieves the information of the case which is being created and sends it to the appropriate method
    */
    public void crearIncMod(){
        system.debug('creacion de incidencia/modificacion');
        allToRoD = false;
        CWP_Helper.caseWrapToInsert retrievedCaseInfo = new CWP_Helper.caseWrapToInsert();
        retrievedCaseInfo = (CWP_Helper.caseWrapToInsert)Cache.Session.get('local.CWPexcelExport.newIncModUserId'+userInfo.getUserId());
        system.debug('informacion de caso recuperada    '+ retrievedCaseInfo);
        tipoCaso = retrievedCaseInfo.tipo;
        if(retrievedCaseInfo.tipo=='incidencia'){
            createIncident(retrievedCaseInfo);
        }else if(retrievedCaseInfo.tipo=='modificacion'){
            createModification(retrievedCaseInfo);
        }
        
        
        system.debug('resultado numerico    '+ result);
        System.debug('[crearCasModificacion] END  '+ resultId);
    }
    
    /*process the case when it is of type incient*/
    private void createIncident(CWP_Helper.caseWrapToInsert retrievedCaseInfo){
        system.debug('se va a insertar el caso tipo incidencia    ' + retrievedCaseInfo);
        CWP_New_Incident_Controller cInc=new CWP_New_Incident_Controller(retrievedCaseInfo.oiId);
        cInc.listCaseAttachments=retrievedCaseInfo.attList;
        if(cInc.listCaseAttachments != null && !cInc.listCaseAttachments.isEmpty()){
            allToRoD = true;
        }
        cInc.incAsuntoParam=retrievedCaseInfo.subject;
        cInc.incDescriptionParam=retrievedCaseInfo.description;
        cInc.incImpactParam=retrievedCaseInfo.impact;
        cInc.incUrgenciaParam=retrievedCaseInfo.urgency;
        try{
            resultId=cInc.crearCase();
            boolean newIncError= cInc.newIncError;
            result = 1;
            wiMap = new Map<string,id>();
            wiMap = (map<string,id>)Cache.Session.get('local.CWPexcelExport.wiMap'+userInfo.getUserId());
            system.debug('CWP_IncModCreationController - wiMap' + wiMap);
            Map<Id, Case> cases = new Map<Id, Case>([SELECT Id FROM Case WHERE caseNumber =: resultId LIMIT 1]);
            casesIdSet.addAll(cases.keySet());
            if(newIncError){
                result = -1;
            }

        }catch(exception e){
            result = -1;
        }
    }
    
    /*process the case when it is of type modification*/
    private void createModification(CWP_Helper.caseWrapToInsert retrievedCaseInfo){
        system.debug('se va a insertar el caso tipo modificacion' + retrievedCaseInfo);
        CWP_New_Modification_Controller cMod = new CWP_New_Modification_Controller();
        cMod.attachmentModificationList=retrievedCaseInfo.attList;
        cMod.detailIdNewModification=retrievedCaseInfo.oiId;
        cMod.caseDescriptionParam=retrievedCaseInfo.description;
        try{
            resultId=cMod.crearMod();
            result = 1;
            wiMap = new Map<string,id>();
            wiMap = (map<string,id>)Cache.Session.get('local.CWPexcelExport.wiMap'+userInfo.getUserId());
            system.debug('CWP_IncModCreationController - wiMap' + wiMap);
        }catch(exception e){
            result = -1;
        }
    }
    /*AddAttachments to list (Direct DML due to ViewState*/
    public void addAttachment(){
      system.debug('parentId'+wiMap);
      system.debug('name'+currentAtt.name);
        List<String> attNameWOPoints;
        if(currentAtt.name.split('\\.').size()>0)
            attNameWOPoints = currentAtt.name.split('\\.');
        String regExp = '[^a-zA-Z0-9]';
        String attNameFormated;
            if(attNameWOPoints.size()>0)
             attNameFormated = attNameWOPoints.get(0).replaceAll(regExp, '') + '.' + attNameWOPoints.get(1);
     try{
        if(allToRoD){
            TGS_CallRoDWs.inFutureContextAttachment = true;
        }
        insert (
            new Attachment (
            Name=currentAtt.name,
            ParentId=wiMap.get(attNameFormated),
            body = EncodingUtil.Base64Decode(currentAtt.body),
            ContentType = currentAtt.contentType
            )
        );
        if(allToRoD){
            TGS_CallRoDWs.inFutureContextAttachment = false;
        }
        currentAtt = new customAtt();
       }catch(exception e){
            result = -1;
       }
    }
    
    public void integraRoD(){
        if(allToRoD){
            CWP_CallRoDWSFromCWP.invokeWebServiceRODFromCWP(casesIdSet, null, null, false);
            Map<Id,TGS_Work_Info__c> wIs = new Map<Id, TGS_Work_Info__c>([SELECT Id FROM TGS_Work_Info__c WHERE TGS_Case__c IN: casesIdSet]);
            workInfosIdSet.addAll(wIs.keySet());
            CWP_CallRoDWSFromCWP.invokeRoDWSFromCWP(workInfosIdSet);
        }
    }
    
    public class customAtt {
       public string name {get;set;}
       public string body {get;set;}
       public string description {get;set;}
       public string contentType {get;set;}
    }
}