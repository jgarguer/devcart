/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Eduardo Ventura
Company:        Everis España
Description:    Generic JSON & XML parser.

History:
<Date>                      <Author>                        <Change Description>
13/02/2017                  Eduardo Ventura                 Versión Inicial.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

public class BI_Serializer {
    
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Eduardo Ventura
    Company:        Everis España
    Description:    Generic JSON to XML parser.
    
    History:
    <Date>                      <Author>                        <Change Description>
    13/02/2017                  Eduardo Ventura                 Versión Inicial.
    ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static String parseXML(String source){
        /* Variable Configuration */
        String xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        
        /* JSON To XML */
        xml += jsonNodeExplorer(source);
        
        return xml;
    }
    
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Eduardo Ventura
    Company:        Everis España
    Description:    Generic XML to JSON parser.
    
    History:
    <Date>                      <Author>                        <Change Description>
    13/02/2017                  Eduardo Ventura                 Versión Inicial.
    ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static String parseJSON(String source){
        /* Variable Configuration */
        String json = '';
        
        /* XML to JSON */
        DOM.Document document = new DOM.Document();
        document.load(source.remove('<?xml version="1.0" encoding="UTF-8" ?>'));
        
        List<DOM.XMLNode> rootList = new List<DOM.XMLNode>();
        DOM.XmlNode root = document.getRootElement();
        rootList.add(root);
        
        json = '{' + XMLArrayAux(rootList) + '}';
        
        return json;
    }
    
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Eduardo Ventura
    Company:        Everis España
    Description:    Generic JSON to XML parser aux method.
    
    History:
    <Date>                      <Author>                        <Change Description>
    13/02/2017                  Eduardo Ventura                 Versión Inicial.
    ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static String jsonNodeExplorer (String node){
        /* Variable Configuration */
        String xml = '';
        
        Map<String, Object> nodeStructure = (Map<String, Object>) JSON.deserializeUntyped(node);
        
        
        /* Recursive process */
        String content;
        for(String key : nodeStructure.keySet()){
            content = '' + nodeStructure.get(key);
            
            if (!content.contains('{') && !content.contains('(')) xml += '<'+ key +'>' + content + '</'+ key +'>';
            else if (content.substring(0,1).equals('(')){
                if (content.contains('{')) for(Object item : (Object[]) nodeStructure.get(key)) xml += '<'+ key +'>' + jsonNodeExplorer(JSON.serialize(item)) + '</'+ key +'>';
                else for(Object item : (Object[]) nodeStructure.get(key)) xml += '<'+ key +'>' + item + '</'+ key +'>';
            }else{
                if (content.contains('{')) xml += '<'+ key +'>' + jsonNodeExplorer(JSON.serialize(nodeStructure.get(key))) + '</'+ key +'>';
                else for(Object item : (Object[]) nodeStructure.get(key)) xml += '<'+ key +'>' + item + '</'+ key +'>'; 
            }
        }
        
        return xml;
    }
    
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Eduardo Ventura
    Company:        Everis España
    Description:    Generic XML to JSON parser aux method.
    
    History:
    <Date>                      <Author>                        <Change Description>
    13/02/2017                  Eduardo Ventura                 Versión Inicial.
    ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    private static String xmlNodeExplorer(List<DOM.XMLNode> node, boolean isArray){
        /* Variable Declaration */
        String json = '';
        
        /* Recursive process */
        for(Integer i = 0; i < node.size(); i++){
            if(node[i].getChildElements().isEmpty()){
                if(!isArray) json += '"' + node[i].getName() + '":"' + node[i].getText() + '"';
                else json += '"' + node[i].getText() + '"';
            }
            else {
                if(!isArray) json += '"' + node[i].getName() + '":{' + XMLArrayAux(node[i].getChildElements()) + '}';
                else json += '{' + XMLArrayAux(node[i].getChildElements()) + '}';
            }
                
                
            
            /* Adjustment */
            if(i + 1 != node.size()) json += ',';
        }
        
        return json;
    }
    
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Eduardo Ventura
    Company:        Everis España
    Description:    Generic XML to JSON parser aux method.
    
    History:
    <Date>                      <Author>                        <Change Description>
    13/02/2017                  Eduardo Ventura                 Versión Inicial.
    ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    private static String XMLArrayAux(List<DOM.XMLNode> childNodes){
        /* Variable Declaration */
        String json = '';
        Map <String, List<DOM.XMLNode>> duplicateNodes = new Map <String, List<DOM.XMLNode>>();
        Map <String, DOM.XMLNode> uniqueNodes = new Map <String, DOM.XMLNode>();
        
        /* Duplicate and Unique Map Process*/
        set<String> usedKeys = new set<String>();
        List<DOM.XMLNode> transportNode = new List<DOM.XMLNode>();
        for(DOM.XMLNode node : childNodes){
            if(!uniqueNodes.containsKey(node.getName()) && !usedKeys.contains(node.getName())) uniqueNodes.put(node.getName(), node);
            else {
                if(!usedKeys.contains(node.getName())){
                    transportNode.add(uniqueNodes.get(node.getName()));
                    transportNode.add(node);
                    duplicateNodes.put(node.getName(), transportNode);
                    
                    /* Adjustment */
                    uniqueNodes.remove(node.getName());
                    usedKeys.add(node.getName());
                }
                else{
                    transportNode = new List<DOM.XMLNode>(duplicateNodes.get(node.getName())); 
                    transportNode.add(node);
                    duplicateNodes.put(node.getName(), transportNode);
                }
            }
        }
        
        /* Array Process */
        for(String key : duplicateNodes.keySet()){
            json += '"' + key + '":[' + xmlNodeExplorer(duplicateNodes.get(key), true) + ']';
            
            /* Adjustment */
            if(!uniqueNodes.isEmpty()) json += ',';
        }
        
        /* Standard Process */
        List<DOM.XmlNode> nodeList = new List<DOM.XmlNode>(uniqueNodes.values());
        json += xmlNodeExplorer(nodeList, false);
        
        return json;
    }
}