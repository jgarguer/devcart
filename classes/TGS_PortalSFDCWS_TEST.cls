@isTest()
public class TGS_PortalSFDCWS_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage of the class TGS_PortalSFDCWS
            
     <Date>                 <Author>                <Change Description>
    26/05/2015              Marta Laliena           Initial Version
	02/03/2018				Antonio Pardo			Fixed
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 @testSetup
    static void testsetup(){
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c = 'TGS';
        insert u;
        System.runAs(u){
            Id accountId = TGS_Dummy_Test_Data.dummyHierarchy().Id;
            List<Account> listAcc = TGS_Portal_Utils.getLevel4(accountId, 1);
            if(listAcc.size()>0){
                TGS_Dummy_Test_Data.dummyPuntoInstalacion(listAcc[0].Id);
            }
        }
    }
    static testMethod void getInfo() {
        
        List<TGS_PortalSFDCWS.Company> listCompany = TGS_PortalSFDCWS.getCompanies();
        List<TGS_PortalSFDCWS.Country> listCountry = TGS_PortalSFDCWS.getCountries();
        List<TGS_PortalSFDCWS.LegalEntity> listLegalEntity = TGS_PortalSFDCWS.getLegalEntities();
        List<TGS_PortalSFDCWS.BusinessUnit> listBusinessUnit = TGS_PortalSFDCWS.getBusinessUnits();
        List<TGS_PortalSFDCWS.CostCenter> listCostCenter = TGS_PortalSFDCWS.getCostCenters();
        List<TGS_PortalSFDCWS.Sites> listSites = TGS_PortalSFDCWS.getSites();
        List<TGS_PortalSFDCWS.Profiles> listProfiles = TGS_PortalSFDCWS.getProfiles();
        List<TGS_PortalSFDCWS.Queue> listQueue = TGS_PortalSFDCWS.getQueues();
        List<TGS_PortalSFDCWS.BSMProfile> listBSMProfile = TGS_PortalSFDCWS.getBSMProfiles();
        List<TGS_PortalSFDCWS.MailNotification> listMailNotification = TGS_PortalSFDCWS.getMailNotifications();
    }
        
}