@isTest
public class TGS_CallUDO_TEST {

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta García
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_CallUDO
            
     <Date>                 <Author>                <Change Description>
    29/05/2015              Ana Cirac               Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    
    
    static testMethod void testGetRequestMethod(){
    
        TGS_CallUdo.getRequest('POST','','');
    
    }
    static testMethod void testInvokeUDOMethod(){
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new TGS_MockHttpResponseGenerator());       
            TGS_CallUdo.invokeUDO('POST','','','');
         Test.stopTest();
    
    
    }
    
    static testMethod void testInvokeUDOAttachment(){
    
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new TGS_MockHttpResponseGenerator());       
            TGS_CallUdo.invokeUDOAttachment(Blob.valueOf('Body Attachment 1'),'Attachment1','','');
        Test.stopTest();    
    }
    
}