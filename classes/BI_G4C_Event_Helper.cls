public class BI_G4C_Event_Helper {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Miguel Molina Cruz
	    Company:       everis
	    Description:   Helper Class for the class BI_G4C_Visitas_Nueva
	    
	    History: 
	    
	    <Date>                          <Author>                    	<Change Description>
	    20/06/2016                      Miguel Molina Cruz			    Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
          
    public Event e {get; set;}
    public list<Contact> c {get; set;}
    public opportunity o {get; set;}
    public BI_G4C_Event_Helper(){}
    
    
}