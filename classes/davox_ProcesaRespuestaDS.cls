global class davox_ProcesaRespuestaDS {
	private static String strSeparador;
    
    private static List<Respuesta> coleccionRespuestas;
    
    private static Map<String, String> CatalogoCodigos;
    
    private static XmlStreamWriter xmlWriter;

    public davox_ProcesaRespuestaDS(){}
    
    global class Respuesta
    {
        WebService String strCodigoDS;
        WebService String strRespuestaDavox;
        WebService String tipoNovedad;
    }

    public davox_ProcesaRespuestaDS( string Xml )
    {
        ProcesarRespuesta( Xml );
    }

    webService static String ProcesarRespuesta( String XmlRespuesta )
    {
        String rtaWS = '100';
        System.debug('\n\n'+XmlRespuesta+'\n\n');
        if( XmlRespuesta.trim().length() != 0 )
        {
            try
            {
                XmlStreamReader xmlReader = new XmlStreamReader( XmlRespuesta );
                xmlWriter = new XmlStreamWriter();
                xmlWriter.writeStartDocument('utf-8','1.0');

                while( xmlReader.hasNext() )
                {
                    if( xmlReader.getEventType() == XmlTag.START_ELEMENT )
                    {
                        if( xmlReader.getLocalName() == 'SEPARATOR' )
                            strSeparador = Parse( xmlReader );
                        else if (xmlReader.getLocalName() == 'VALUE')
                            ConvertirRespuesta( Parse( xmlReader ) ); 
                    }
                    xmlReader.next();
                }
                ActualizarNovedad();
                xmlWriter.writeEndElement();
            }
            catch( Exception ex )
            {
                rtaWS = ex.getMessage();
            }
        }
        else
            rtaWS = 'ERROR: NO SE HAN ESPECIFICADO TRANSACCIONES EN EL PARÁMETRO ENVIADO';
        return rtaWS;
    }

    private static String Parse( XmlStreamReader reader )
    {
        String strTextoDevuelve = '';
        
        while( reader.hasNext() )
        {
            if( reader.getEventType() == XmlTag.END_ELEMENT )
                break;
            else if (reader.getEventType() == XmlTag.CHARACTERS)
            {
                if( reader.getText() != null )
                    strTextoDevuelve = reader.getText();
            }
            reader.next();
        }
        return strTextoDevuelve;
    }

    private static void ConvertirRespuesta( String strRespuestaXML )
    {
        Respuesta r = new Respuesta();
        String[] respuesta = strRespuestaXML.split( strSeparador );
        r.strCodigoDS = respuesta[1];
        r.strRespuestaDavox = respuesta[2];
        if( respuesta.size() > 3 )
            r.tipoNovedad=respuesta[3];
        if( coleccionRespuestas == null )
            coleccionRespuestas = new List<Respuesta>();
        coleccionRespuestas.add(r);
    }
  
    private static void ActualizarNovedad()
    {
        LeeCatalogoCodigos();
        if( coleccionRespuestas.Size() > 0 )
        {
            xmlWriter.writeStartElement(null, 'DESCRIPCIONES_SERVICIO', null);
            List<String> strCodigosDs = new List<String>(); 
            List<String> rtaDavox = new List<String>(); 
            List<String> strCodigoDSerror = new List<String>();
            List<String> stridCliente = new List<String>();
            List<String> rtaDavoxCliente = new List<String>(); 
            List<String> stridCobroParcial = new List<String>();
            List<String> rtaCobroParcial = new List<String>();
            List<String> listActualizacionContrato = new List<String>();
            List<String> lstCesionContrato = new List<String>();    

            Map<String,String> mapMSErr = new Map<String,String>(); 
            Map<String,String> mapDevuelto = new Map<String,String>();
            
            for( Respuesta r : coleccionRespuestas )
            {
                Integer codig=0;
                String codigRta = r.strRespuestaDavox;
                codigRta = codigRta.replaceAll(',','');
                
                try{ codig=Integer.valueOf( r.strRespuestaDavox );}catch( Exception ex ){ codig=0; }
                
                if( r.strRespuestaDavox!='100' && r.strRespuestaDavox!='101' )
                {
                    strCodigoDSerror.add(r.strCodigoDS);
                    mapMSErr.put(r.strCodigoDS,r.strRespuestaDavox);
                }
                else if( codig >= 9000 )
                    mapDevuelto.put(r.strCodigoDS,r.strRespuestaDavox);

                if( r.tipoNovedad == 'DATOS CUENTA' )
                {
                    stridCliente.add( r.strCodigoDS );
                    mapMSErr.put( r.strCodigoDS, r.strRespuestaDavox );
                    rtaDavoxCliente.add( r.strRespuestaDavox );
                }
                else if( r.tipoNovedad == 'COBROS PARCIALES' )
                {
                    stridCobroParcial.add( r.strCodigoDS );
                    rtaCobroParcial.add( r.strRespuestaDavox );
                    mapMSErr.put( r.strCodigoDS,r.strRespuestaDavox );
                }
                else if( r.tipoNovedad == 'MODIFICACION CONTRATO' )
                {
                    listActualizacionContrato.add( r.strCodigoDS );
                    mapMSErr.put( r.strCodigoDS,r.strRespuestaDavox );
                }
                else if( r.tipoNovedad == 'CESION CONTRATO' )
                {
                    lstCesionContrato.add( r.strCodigoDS );
                    mapMSErr.put( r.strCodigoDS, r.strRespuestaDavox );
                }
                else
                {
                    strCodigosDs.add( r.strCodigoDS );
                    mapMSErr.put( r.strCodigoDS,r.strRespuestaDavox );
                    rtaDavox.add( r.strRespuestaDavox );
                }
            }
            
            if( !strCodigosDs.isEmpty() )
                actualizarSincronizadoNovedad( strCodigosDs, mapMSErr, mapDevuelto );
            
            if( !stridCliente.isEmpty() )
                actualizarDatosCuenta( stridCliente, mapMSErr );
            
            if( !stridCobroParcial.isEmpty() )
                actualizarCobroParcial( stridCobroParcial, mapMSErr );
            
            if( !strCodigoDSerror.isEmpty() )
                actualizarMSError( strCodigoDSerror );
            
            if(!listActualizacionContrato.isEmpty())
                crearLogContrato( listActualizacionContrato, mapMSErr, 'DAVOX ACTUALIZAR CONTRATO' );
            
            if(!lstCesionContrato.isEmpty())
                crearLogContrato( lstCesionContrato, mapMSErr,' DAVOX NOVEDAD DE CESION DE CONTRATO' );
        }
    }

    private static Map<String,String> LeeCatalogoCodigos()
    {
        CatalogoCodigos = new Map<String, String>();

        for( BI_COL_RespuestasDavox__c c : [ Select Name, BI_COL_Descripcion_Codigo__c from BI_COL_RespuestasDavox__c ] )
            CatalogoCodigos.put( c.Name, c.BI_COL_Descripcion_Codigo__c );
        return CatalogoCodigos;
    }
    
    public static String ElaboraDescripcion( String strCadenaRespuesta )
    {
        List<String> codigos = strCadenaRespuesta.split(',');
        String strRespuesta = '';

        for( Integer i = 0; i <= codigos.Size()-1; i++ )
            strRespuesta += CatalogoCodigos.get( codigos[i] ) + ' -- ';
        return strRespuesta;
    }
    
    public static void actualizarMSError( List<String> strCodigoDSerror )
    {
        List<BI_COL_Modificacion_de_Servicio__c> lstMS=[  
            Select  BI_COL_Autorizacion_facturacion__c, BI_COL_Fecha_de_facturacion__c 
            from    BI_COL_Modificacion_de_Servicio__c 
            where   Id IN: strCodigoDSerror];
        for( BI_COL_Modificacion_de_Servicio__c ms : lstMS )
        {
            ms.BI_COL_Autorizacion_facturacion__c = false;
            ms.BI_COL_Fecha_de_facturacion__c = null;
        }
        update lstMS;
    }
    
    public static void actualizarCobroParcial(List<String> stridCobroParcial,Map<String,String> mapMSErr)
    {
        List<BI_Log__c> lnovFactCobroParcial = [
            select  Name, BI_COL_Cuenta__c, BI_COL_Cobro_Parcial__c, BI_COL_Modificacion_Servicio__c,
                    BI_COL_Estado__c, BI_COL_Informacion_recibida__c
            from    BI_Log__c 
            where   BI_COL_Cobro_Parcial__c IN: stridCobroParcial 
            and     BI_COL_Estado__c <> 'Cerrado'];

        for( Integer i=0; i<lnovFactCobroParcial.size(); i++ )
        {
            string strInformacionRecibida = ElaboraDescripcion( mapMSErr.get( lnovFactCobroParcial[i].BI_COL_Cobro_Parcial__c ) );
            lnovFactCobroParcial[i].BI_COL_Informacion_recibida__c = strInformacionRecibida;
            lnovFactCobroParcial[i].BI_COL_Estado__c = 'Sincronizado';
        }
        update lnovFactCobroParcial;
    }
    
    public static void actualizarDatosCuenta(List<String> stridCliente,Map<String,String> mapMSErr)
    {
        List<BI_Log__c> lnovFactCliente = [
            select  Name, BI_COL_Cuenta__c, BI_COL_Cobro_Parcial__c, BI_COL_Modificacion_Servicio__c,
                    BI_COL_Informacion_recibida__c, BI_COL_Estado__c
            from    BI_Log__c 
            where   BI_COL_Modificacion_Servicio__c IN: stridCliente 
            and     BI_COL_Estado__c <> 'Cerrado'];

        for( Integer i=0; i<lnovFactCliente.size(); i++ )
        {
            string strInformacionRecibida = ElaboraDescripcion( mapMSErr.get( lnovFactCliente[i].BI_COL_Cuenta__c ) );
            lnovFactCliente[i].BI_COL_Informacion_recibida__c = strInformacionRecibida;
            lnovFactCliente[i].BI_COL_Estado__c = 'Sincronizado';
        }
        update lnovFactCliente;
    }
    
    public static void actualizarSincronizadoNovedad( List<String> strCodigosDs, Map<String,String> mapMSErr, Map<String,String> mapDevuelto  )
    {
        List<BI_Log__c> lnovFact = [
            select  Name, BI_COL_Modificacion_Servicio__c, BI_COL_Informacion_recibida__c, BI_COL_Estado__c
            from    BI_Log__c 
            where   BI_COL_Modificacion_Servicio__c In: strCodigosDs 
            and     BI_COL_Estado__c <> 'Cerrado'];
        for( Integer i = 0; i < lnovFact.size(); i++ )
        {
            string strInformacionRecibida = ElaboraDescripcion( mapMSErr.get( lnovFact[i].BI_COL_Modificacion_Servicio__c ) );
            lnovFact[i].BI_COL_Informacion_recibida__c = strInformacionRecibida;
            if( mapDevuelto.containsKey( lnovFact[i].BI_COL_Modificacion_Servicio__c))
                lnovFact[i].BI_COL_Estado__c = 'Sincronizado - Devuelto';
            else
                lnovFact[i].BI_COL_Estado__c = 'Sincronizado';
        }
        
        update lnovFact;
    }
    
    public static void crearLogContrato( List<String> listActualizacionContrato, map<String,String> mapMSErr, String strTipoTransaccion )
    {
        List<BI_Log__c> lstLogTran = new List<BI_Log__c>(); 
        
        for( String idContrato : listActualizacionContrato )
        {
            BI_Log__c Log = new BI_Log__c();
            Log.BI_Descripcion__c = 'RESPUESTA DAVOX - CONTRATO';
            Log.BI_COL_Estado__c = 'Exitoso';
            Log.Descripcion__c = ElaboraDescripcion(mapMSErr.get(idContrato));
            Log.BI_COL_Informacion_recibida__c = ElaboraDescripcion(mapMSErr.get(idContrato));
            Log.BI_COL_Tipo_Transaccion__c = 'DAVOX ACTUALIZAR CONTRATO';
            Log.BI_COL_Contrato__c = idContrato;
            lstLogTran.add( Log );
        }
        insert lstLogTran;
    }
}