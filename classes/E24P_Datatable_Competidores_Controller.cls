public class E24P_Datatable_Competidores_Controller {
    @AuraEnabled
    public static List < Object > doFetchDFE() {
        
        List < Object > returnList = new List < Object > ();
        
       returnList = [Select Id, OwnerId, IsDeleted, Name, CurrencyIsoCode, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastViewedDate, LastReferencedDate, ConnectionReceivedId, ConnectionSentId, E24P_Account_Plan__c, E24P_Fortaleza__c, E24P_Debilidad__c, E24P_Nuestra_Estrategia__c, E24P_Competidor__r.Name, E24P_Plan_de_accion__r.Name 
       			FROM E24P_DFE__c WHERE E24P_Account_Plan__c = 'a8b0E000000CcLAQA0'];
        
        return returnList;
    }
    
        @AuraEnabled
    public static List < Object > doFetchLDA() {
        
        List < Object > returnList = new List < Object > ();
        
       returnList = [Select Id, Name, BI_Estado__c, E24P_Prioridad__c, E24P_Tipo_de_actividad__c, E24P_Responsable__c
       			FROM BI_Plan_de_accion__c WHERE E24P_Account_Plan__c = 'a8b0E000000CcLAQA0' ];
        
        return returnList;
    }
    
   		@AuraEnabled
    public static Boolean doUpdateDFE(List<E24P_DFE__c> editedDFEList){
        try{
            update editedDFEList;
            return true;
        } catch(Exception e){
            return false;


        }
    }
   	 @AuraEnabled
        public static Boolean doUpdateLDALookup(E24P_DFE__c Lookup){
        system.debug('Entro en Apex');
        try{
            update Lookup;
            return true;
        } catch(Exception e){
            return false;
        }
    }
}