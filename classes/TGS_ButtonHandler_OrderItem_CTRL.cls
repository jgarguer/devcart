public with sharing class TGS_ButtonHandler_OrderItem_CTRL {
	public Boolean render_GlobalRender {get;set;}
	public Boolean render_BillingDatesAssigner {get;set;}
	public NE__OrderItem__c orderitem {get;set;}
	public String url {get;set;}

	public TGS_ButtonHandler_OrderItem_CTRL() {}
	public TGS_ButtonHandler_OrderItem_CTRL(ApexPages.StandardController controller)
	{
		this.render_GlobalRender = false;
		this.render_BillingDatesAssigner = false;
		Set<String> set_GMServices = new Set<String>{''};
		 // 10/04/2018 - Iñaki Frial - Change filter for all services.
		for(NE__Catalog_Item__c citem :
            [
                SELECT  Id,NE__Root_Catalog_Item__r.NE__Catalog_Category_Name__r.Name,NE__ProductId__r.Name
                FROM    NE__Catalog_Item__c WHERE NE__Catalog_Id__r.Name ='TGSOL Catalog'
                //WHERE   NE__Root_Catalog_Item__r.NE__Catalog_Category_Name__r.Name = :Constants.GLOBAL_MOBILITY_FOR_MNCS OR NE__ProductId__r.Name IN :Constants.FERROVIAL_SERVICES
            ]
            )
        {
            set_GMServices.add(citem.NE__ProductId__r.Name);
        }
        set_GMServices.add('Bundles With Pooling');
		this.orderitem = (NE__OrderItem__c) controller.getRecord();
		this.orderitem = 	[
								SELECT 	Id,TGS_Product_Name__c, NE__OrderId__r.RecordType.DeveloperName
								FROM 	NE__OrderItem__c
								WHERE 	Id = :this.orderitem.Id
							];
		System.debug('ORDERITEM: '+this.orderitem);
		this.url = '/apex/TGS_Update_Terminate_Product?ciId='+this.orderitem.Id;
		if(set_GMServices.contains(this.orderitem.TGS_Product_Name__c) && orderitem.NE__OrderId__r.RecordType.DeveloperName == 'Asset')
		{
			this.render_BillingDatesAssigner = true;
			this.render_GlobalRender = true;
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Juan Carlos Terrón
	    Company:       Accenture
	    Description:   Redirects to TGS_Update_Terminate_Product page.
	    
	    IN:            None
	    OUT:           PageReference
	    
	    History:   
	    <Date>                  <Author>                <Change Description>
	    03/01/2017			Juan Carlos Terrón		Initial Version.        
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public void redirect(){
		this.url = '/apex/TGS_Update_Terminate_Product?ciId='+this.orderitem.Id;
	}
}