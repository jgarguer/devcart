public class BI_PromocionesMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by BI_PromocionesMethods Triggers 
    Test Class:    BI_PromocionesMethods_TEST
    History:
     
    <Date>            <Author>                <Change Description>
    24/04/2014        Ignacio Llorca          Initial Version
    03/06/2014        Pablo Oliva             Field changed (BI_Campana__c ---> BI_Campanas__c)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
            
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method to prevent the update or delete of promotions with the field BI_Editable__c = false
    
    IN:            ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    24/04/2014              Ignacio Llorca          Initial Version      
    12/09/2014				Ignacio Llorca			Try/catch commented
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void preventUpdatePromotion(List<BI_Promociones__c>olds, List<BI_Promociones__c>news) {
    	//try{ Try/catch commented. The method is created to force an error
	        integer i=0;
	        for (BI_Promociones__c promo:olds){ 
	            if (promo.BI_Editable__c == false){
	                news[i].addError(Label.BI_Trigger_Promociones_preventUpdateDelete);    
	            }
	            i++;
	        }
    	//}catch (exception e){
		//   BI_LogHelper.generateLog('BI_PromocionesMethods.preventUpdatePromotion', 'BI_EN', e.getMessage(), 'Trigger');
		//}
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method to prevent the update or delete of promotions with the field BI_Editable__c = false
    
    IN:            ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    24/04/2014              Ignacio Llorca          Initial Version   
    12/09/2014				Ignacio Llorca			Try/catch commented     
    20/07/2015              Francisco Ayllon        Added clauses to the condition due to Demand number 88 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void preventDeletePromotion(List<BI_Promociones__c>olds) {
    	//try{ Try/catch commented. The method is created to force an error
            Set<Id> promoIds = new Set<Id>();
            for(BI_Promociones__c currentPromo: olds){
                promoIds.add(currentPromo.BI_Campanas__c);
            }

            Map<Id,Campaign> campaignsMap = new Map<Id,Campaign>([SELECT Id, Status FROM Campaign WHERE Id IN: promoIds]);
            User userProfile = [SELECT Id, BI_Permisos__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
            BI_bypass__c bypass = BI_bypass__c.getInstance();

            if(!bypass.BI_migration__c){
                for (BI_Promociones__c promo:olds){ 
                    if (!promo.BI_Editable__c||(userProfile.BI_Permisos__c!='Administrador del sistema'
                                              &&userProfile.BI_Permisos__c!='Super Usuario'
                                              &&promo.CreatedById!=UserInfo.getUserId())){
                        promo.addError(Label.BI_Trigger_Promociones_preventUpdateDelete);
                    }
                }
            }
    	//}catch (exception e){
		  // BI_LogHelper.generateLog('BI_PromocionesMethods.preventDeletePromotion', 'BI_EN', e.getMessage(), 'Trigger');
		//}
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method to prevent the undelete of promotions with Status different to 'En planificación'
    
    IN:            ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>            <Author>                <Change Description>
    24/04/2014        Ignacio Llorca          Initial Version   
    03/06/2014        Pablo Oliva             Field changed (BI_Campana__c ---> BI_Campanas__c)      
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void preventUndelete(List<BI_Promociones__c>news){
	        
        try{
	        Set <Id> set_c = new Set <Id>();
	        Set <Id> set_camp = new Set <Id>();
	        
	        for (BI_Promociones__c nuevo:news){
	            set_c.add(nuevo.BI_Campanas__c);
	        }
	        
	        
	        system.debug(set_c + 'DEBUG SET C'); 
	        for (Campaign camp:[SELECT Id, Status FROM Campaign WHERE Id IN :set_c AND Status = :Label.BI_EnPlanificacion]){
	            set_camp.add(camp.Id);
	        }
	        
	        for (BI_Promociones__c prom:news){
	            
	            if (!set_camp.contains(prom.BI_Campanas__c)){
	                prom.addError(Label.BI_Trigger_Promociones_preventUndelete);
	            }
	        
	        }
        }catch (exception Exc){
		   BI_LogHelper.generate_BILog('BI_PromocionesMethods.preventUndelete', 'BI_EN', Exc, 'Trigger');
		}
        
    }

    
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Pablo Oliva
	Company:       Salesforce.com
	Description:   Method that sync promotions currency equals to NE__Catalog_Item__c

	History: 

	<Date>                     <Author>                <Change Description>
	09/10/2014                  Pablo Oliva             Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void changeCurrencyAndValidateEconomic(List<BI_Promociones__c> news, List<BI_Promociones__c> olds) {
        
        Set<Id> set_catalog = new Set<Id>();
        List<BI_Promociones__c> lst_promo = new List<BI_Promociones__c>();
        for(BI_Promociones__c promo:news){ 
            if(promo.BI_Producto__c != null){
                lst_promo.add(promo);
                set_catalog.add(promo.BI_Producto__c);
            }    
        }
        
        if(!set_catalog.isEmpty()){
            Map<Id, NE__Catalog_Item__c> map_catalog = new Map<Id, NE__Catalog_Item__c>([select Id, CurrencyIsoCode, NE__BaseRecurringCharge__c,
            																			 NE__Enable_Recurring_Charge__c, NE__Enable_One_Time_Fee__c,
            																			 NE__Base_OneTime_Fee__c from NE__Catalog_Item__c where Id IN :set_catalog]);
            																			 
            for(BI_Promociones__c prom:lst_promo){
                if(map_catalog.get(prom.BI_Producto__c) != null){
                	
                	if(olds == null)
                    	prom.CurrencyIsoCode = map_catalog.get(prom.BI_Producto__c).CurrencyIsoCode;
                    
                    if((prom.BI_Ingreso_por_unica_vez__c != null && !map_catalog.get(prom.BI_Producto__c).NE__Enable_One_Time_Fee__c && prom.BI_Ingreso_por_unica_vez__c != map_catalog.get(prom.BI_Producto__c).NE__Base_OneTime_Fee__c) ||
                       (prom.Recurrente_bruto_mensual__c != null && !map_catalog.get(prom.BI_Producto__c).NE__Enable_Recurring_Charge__c && prom.Recurrente_bruto_mensual__c != map_catalog.get(prom.BI_Producto__c).NE__BaseRecurringCharge__c))
                    {
                    	prom.addError(Label.BI_Precio_producto_Campana);
                    }
                    
                    
                }
            }
            
        }
        
    }

}