/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for IntegrationException class
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    14/02/2017              Pedro Párraga            Initial Creation
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class IntegrationException_TEST {
	
	@isTest static void test_method_one() {
		try{
			throw new IntegrationException('', '');
		}catch(Exception e){

		}finally{
			try{
				throw new IntegrationException('', '', '');
			}catch(Exception e){

			}finally{
			 	try{
					throw new IntegrationException('', '', '', '');
				}catch(Exception e){

					}finally{
						try{
							throw new IntegrationException('', '', '', '');
						}catch(Exception e){

						}finally{
							try{
								throw new IntegrationException('', '', '', '', '', '');
							}catch(Exception e){
								IntegrationException ie = new IntegrationException();
								TGS_Error_Integrations__c tei = ie.getErrorIntegration();
								ie.setErrorIntegration(tei);
							}finally{

							}
						}
					}
				}
			}			
		}	
	}