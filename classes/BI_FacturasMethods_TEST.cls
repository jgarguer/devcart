@isTest
private class BI_FacturasMethods_TEST 
{
    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    static
    {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'BI_FVI_Nodos__c' OR sObjectType = 'BI_Objetivo_Comercial__c' OR sObjectType = 'BI_FVI_Nodos_Objetivos__c'])  
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage cover of BI_FacturasMethods class

    History: 
    
    <Date>                      <Author>                 <Change Description>
    30/10/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void createRegion_Facturas_TEST(){
 		//////////////////////////////////////////////////////
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
			Integer Q = 3; 	 //Number of Account and diff Region__c
			Integer K = 100; //BI_Facturas__c for each Acc
			//Total number of records: Q * K
		//////////////////////////////////////////////////////
		List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(Q);
        
        List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(Q,lst_pais);
        lst_acc[0].BI_Country__c = 'Peru';
        lst_acc[0].BI_Subsegment_Local__c = 'FVI';
        update lst_acc;

		List<BI_Facturas__c> lst_inv = new List<BI_Facturas__c>();

		for(Account acc :lst_acc){
        	for(Integer j = 0; j < K; j++){
            	BI_Facturas__c inv = new BI_Facturas__c(Name = 'test'+ String.valueOf(j),
            										BI_Cliente_facturacion__c = 'test',
            										BI_Cobrada__c = Label.BI_No,
            										BI_Nombre_del_cliente__c = acc.Id,            									
            										BI_Tipo_de_facturacion__c = 'Test',
            										BI_Fecha_de_factura__c = Datetime.now(),
            										BI_Importe_s_tax__c = 1000,
            										BI_Tax__c = 0,
                                                    BI_FVI_FechaCobro__c = System.today(),
                                                    BI_FVI_Cargo_Fijo_Fija_Avanzada__c  = 3);
            lst_inv.add(inv);
     	   }
    	}
        
        Test.startTest();
        insert lst_inv;
        Test.stopTest();

        lst_acc = [SELECT Id,BI_Country__c,(SELECT BI_Country__c FROM Facturas__r) FROM Account];
        system.assert(!lst_acc.isEmpty());
		
		for(Account acc :lst_acc){
			for(BI_Facturas__c fact :acc.Facturas__r){
				system.assert(fact.BI_Country__c != null);
				system.assertEquals(fact.BI_Country__c,acc.BI_Country__c);
			}
        }
   	
	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method to manage cover of BI_FacturasMethods class, method createRegion.
    Test Class:    snippet

    History: 
    
     <Date>                     <Author>                <Change Description>
    30/10/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static testMethod void createRegion_Facturacion_TEST() {
	 	//////////////////////////////////////////////////////
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
			Integer Q = 3; 	 //Number of Account and diff Region__c
			Integer K = 100; //BI_Facturacion__c for each Acc
			//Total number of records: Q * K
		//////////////////////////////////////////////////////
		List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(Q);
        
        List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(Q,lst_pais);

		List<BI_Facturacion__c> lst_fact = new List<BI_Facturacion__c>();

		for(Account acc :lst_acc){
        	for(Integer j = 0; j < K; j++){
             BI_Facturacion__c fact = new BI_Facturacion__c(BI_Cliente__c = acc.Id);
            lst_fact.add(fact);
     	   }
    	}
        
        Test.startTest();
        insert lst_fact;
        Test.stopTest();

        lst_acc = [SELECT Id,BI_Country__c,(SELECT BI_Country__c FROM Facturaci_n__r) FROM Account];
        system.assert(!lst_acc.isEmpty());
		
		for(Account acc :lst_acc){
			for(BI_Facturacion__c fact :acc.Facturaci_n__r){
				system.assert(fact.BI_Country__c != null);
				system.assertEquals(fact.BI_Country__c,acc.BI_Country__c);
			}
        }
	}


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method to manage cover of BI_FacturasMethods class, method createRegion.
    Test Class:    snippet

    History: 
    
     <Date>                     <Author>                <Change Description>
    30/10/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void createRegion_Cobranza_TEST() {
	 	//////////////////////////////////////////////////////
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
			Integer Q = 3; 	 //Number of Account and diff Region__c
			Integer K = 100; //BI_Cobranza__c for each Acc
			//Total number of records: Q * K
		//////////////////////////////////////////////////////
		List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(Q);
        
        List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(Q,lst_pais);

		List<BI_Cobranza__c> lst_cobr = new List<BI_Cobranza__c>();

		for(Account acc :lst_acc){
        	for(Integer j = 0; j < K; j++){
            	BI_Cobranza__c cobr = new BI_Cobranza__c(
					BI_Clientes__c = acc.Id);

            lst_cobr.add(cobr);
     	   }
    	}
        
        Test.startTest();
        insert lst_cobr;
        Test.stopTest();

        lst_acc = [SELECT Id,BI_Country__c,(SELECT BI_Country__c FROM Cobranzas1__r) FROM Account];
        system.assert(!lst_acc.isEmpty());
		
		for(Account acc :lst_acc){
			for(BI_Cobranza__c fact :acc.Cobranzas1__r){
				system.assert(fact.BI_Country__c != null);
				system.assertEquals(fact.BI_Country__c,acc.BI_Country__c);
			}
        }

	}

    /*----------------------------------------------------------------------------------------------------
    Author:        Geraldine Pérez Montes
    Company:       Salesforce.com
    Description:   Method to manage cover of BI_FacturasMethods class, method CreateRegistroDesemp.
    Test Class:    snippet

    History: 
    
     <Date>                     <Author>                        <Change Description>
    08/06/2016                  Geraldine Pérez Montes          Initial Version
    20/02/2018                  Humberto Nunes                  Se Agrego un Nodo Objetivo Jerarquico      
    --------------------------------------------------------------------------------------------------------*/
	static testMethod void CreateRegistroDesemp_TEST() {

        
        List<Account> lstAcc = new List<Account>();
        Account objAcc = new Account();
        objAcc.Name = 'CLIENTE_TEST';
        objAcc.BI_Country__c = Label.BI_Peru;
        objAcc.BI_No_Identificador_fiscal__c = '57019465G';
        objAcc.CurrencyIsoCode = 'MXN';
        objAcc.BI_FVI_Tipo_Planta__c = 'No Cliente';
        objAcc.BI_Segment__c = 'test';
        objAcc.BI_Subsegment_Regional__c = 'test';
        objAcc.BI_Territory__c = 'test';
            
        lstAcc.add(objAcc);
        insert lstAcc;

        // NODO JERARQUICO 
        BI_FVI_Nodos__c NJ = new BI_FVI_Nodos__c(
        Name = 'NCJ_Prueba', 
        RecordTypeId = MAP_NAME_RT.get('BI_OBJ_NCJ'),
        BI_FVI_Pais__c = Label.BI_Argentina);
        insert NJ;

        // OBJETIVO COMERCIAL JERARQUICO
        BI_Objetivo_Comercial__c OC = new BI_Objetivo_Comercial__c(
        RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Objetivos_Jerarquicos'),
        BI_FVI_Activo__c = true,
        BI_Tipo__c = 'Ingresos Comerciales',
        BI_OBJ_Campo__c = '',
        BI_FVI_Descripcion__c = 'Facturas (Jerarquicos)',
        BI_FVI_Unidad_Medida__c = 'Moneda');
        insert OC;

        // PERIDO 
        BI_Periodo__c PER = new BI_Periodo__c(
            BI_FVI_Activo__c = true,
            BI_FVI_Fecha_Inicio__c = Date.today() - 60, 
            BI_FVI_Fecha_Fin__c = Date.today() + 10
        );
        insert PER;

        system.debug(OC);

        // NODO OBJETIVO JERARQUICO
        BI_FVI_Nodos_Objetivos__c NOJ = new BI_FVI_Nodos_Objetivos__c(
            RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Jerarquicos'),
            BI_FVI_Id_Nodo__c = NJ.Id,
            BI_FVI_IdPeriodo__c = PER.Id,
            BI_FVI_Objetivo_Comercial__c = OC.Id,
            BI_FVI_Objetivo__c = 15000
        );  
        insert NOJ;

        List<BI_Facturas__c> lstFac = new List<BI_Facturas__c>();
        BI_Facturas__c objFact = new BI_Facturas__c();
        objFact.name = 'FAC001';
        objFact.BI_Nombre_del_cliente__c = lstAcc[0].Id;
        objFact.CurrencyIsoCode = 'PEN';
        objFact.BI_FVI_FechaCobro__c = System.today();
        objFact.BI_Importe_Factura__c = 950;
        objFact.BI_FVI_Cargo_Fijo_Fija_Avanzada__c  = 3;
        lstFac.add(objFact);
       

        Test.startTest();
            insert lstFac;
            BI_FacturasMethods.CreateRegistroDesemp(lstFac);
        Test.stopTest();
    }

}