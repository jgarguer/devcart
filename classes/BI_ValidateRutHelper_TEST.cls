@isTest
private class BI_ValidateRutHelper_TEST {
	
	static testMethod void BI_ValidateRutHelper_TEST_1() {

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		List<String> lst_test_rut = new List<String>();
			lst_test_rut.add('092704505');
			lst_test_rut.add('167629326');
			lst_test_rut.add('059219707');
			lst_test_rut.add('148169578');
			lst_test_rut.add('093709330');
			lst_test_rut.add('140416681');
			lst_test_rut.add('205042458');
			lst_test_rut.add('148610983');
			lst_test_rut.add('164495531');
			lst_test_rut.add('204390363');
			lst_test_rut.add('221662482');
			lst_test_rut.add('061388273');
			lst_test_rut.add('156224987');
			lst_test_rut.add('05754127k');
			lst_test_rut.add('074365590');
			lst_test_rut.add('078032472');
			lst_test_rut.add('056210393');
			lst_test_rut.add('139123921');
			lst_test_rut.add('206090162');
			lst_test_rut.add('075566557');

		Boolean aux;
		for(String rut :lst_test_rut){
			aux = BI_ValidateRutHelper.check_rut_chile(rut);
			system.debug('RUT: ' + rut +' | return: ' + aux);
			system.assert(aux);
		}

		
	}
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}