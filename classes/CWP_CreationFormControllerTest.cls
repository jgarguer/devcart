@isTest
private class CWP_CreationFormControllerTest {
	
	     @testSetup 
    private static void dataModelSetup() {          
       
        //USER
        UserRole rolUsuario;
        User unUsuario;
        User otroUsuario;
        Profile profileTGS;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            profileTGS = CWP_TestDataFactory.getProfile('TGS System Administrator');
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, profileTGS.Id);
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
            //ACCOUNT
            map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
        
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        Contact contactTest1 = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest1');
        
        contactTest.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest;
        contactTest1.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest1;
        
        User usuario;
        User usuarioNoTGS;
        
         //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 't1';
        producto.TGS_CWP_Tier_2__c = 't2';
        insert producto;
        list<NE__Catalog_Item__c> ciList = new list<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
        //catalogoItem.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name = 't1';
        ciList.add(catalogoItem);
        
        NE__Catalog_Item__c catalogoItem1 = CWP_TestDataFactory.createCatalogoItem('ProductoTB', catalogCategoryChildren.id, catalogo.id, producto.id);
        catalogoItem1.NE__Technical_Behaviour__c = 'Service with pre-approved changes';
        ciList.add(catalogoItem1);
        insert ciList;
        
        NE__Contract_Header__c newCH = new NE__Contract_Header__c(
            NE__name__c = 'theContract'
        );
        insert newCH;
        
        NE__Contract_Account_Association__c newCAA = new NE__Contract_Account_Association__c(
            NE__Contract_Header__c = newCH.id,
            NE__Account__c = accLegalEntity.id
        );
        insert newCAA;
        
        NE__Contract__c newC =new  NE__Contract__c(
            NE__Contract_Header__c = newCH.id
        );
        insert newC;
        
        NE__Contract_Line_Item__c newCII = new NE__Contract_Line_Item__c(
            NE__Commercial_Product__c = producto.id,
            NE__Contract__c = newC.id
        );
        insert newCII;
        
        
        
        
          //ORDER
        
        NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        insert testOrder;
        //createOrderItem(id accountId, id orderId, id productId, id catalogItem, integer quantity){
        NE__OrderItem__c newOI;
        list<NE__OrderItem__c> oiList = new list<NE__OrderItem__c>();
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
        oiList.add(newOI);
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[1].id, 1);
        newOI.NE__City__c = 'Gondor';
        newOI.NE__city__c = 'Middle-earth';
        oiList.add(newOI);
        insert oiList;
        
        
        //CASE
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        insert newCase;       
        
        //TGS_GME_GM_Product_Categorization__c
        TGS_GME_GM_Product_Categorization__c categRegularizada = new TGS_GME_GM_Product_Categorization__c(
        Name='Test',
        TGS_Categorization_tier_1__c = 'UNIFIED COMMUNICATIONS AND COLLABORATION',
        TGS_Categorization_tier_2__c = 'Managed Unified Communications',
            TGS_Service__c='Producto Comercial',
        TGS_Categorization_tier_3__c = 'HCS License');
        insert categRegularizada;              
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');       
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil2 = CWP_TestDataFactory.getProfile('BI_Customer Communities');       
            usuarioNoTGS = CWP_TestDataFactory.createUser('nombre3', null, perfil2.id);
            usuarioNoTGS.contactId = contactTest1.id;
            //insert usuarioNoTGS;
        }
         
        CaseComment Comment = new CaseComment(ParentId = newCase.Id, CommentBody = 'new comment');
        insert Comment;
        
         Market_Place_PP__c newMarketplace = new Market_Place_PP__c();
        insert newMarketplace;
     
     }
	
	@isTest static void getUrgencyPickList() {
		List<SelectItem> selectOptionList = CWP_CreationFormController.getUrgencyPickList();
		System.assert(selectOptionList != null);
		System.assert(selectOptionList.size() > 0);
	}
	
	@isTest static void getImpactPickList() {
		List<SelectItem> selectOptionList = CWP_CreationFormController.getImpactPickList();
		System.assert(selectOptionList != null);
		System.assert(selectOptionList.size() > 0);
	}
	
	@isTest static void getFamilyPickList() {
		User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       	System.runAs(usuario){
			 CWP_CreationFormController.getFamilyPickList('TGS_Incident');
             CWP_CreationFormController.getFamilyPickList('TGS_Change');
            CWP_CreationFormController.getFamilyPickList('loren');
            
		}
	}
	
	@isTest static void accountList() {
		User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       	System.runAs(usuario){		
       		List<Id> selectOptionList = CWP_CreationFormController.accountList();
			System.assert(selectOptionList != null);
			System.assert(selectOptionList.size() > 0);
       	}
	}
	
	@isTest static void getTier1PickList() {
		List<SelectItem> selectOptionList = CWP_CreationFormController.getTier1PickList();
		System.assert(selectOptionList != null);
		System.assert(selectOptionList.size() > 0);
	}
	
	@isTest static void getTier2PickList() {
		String str = 'String';
		List<SelectItem> selectOptionList = CWP_CreationFormController.getTier2PickList(str);
		
	}
	
	@isTest static void getTier3PickList() {
		List<SelectItem> selectOptionList = CWP_CreationFormController.getTier3PickList('String');
		
	}

	@isTest static void doSave() {

        
        
		User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
		Case caso = [SELECT id, TGS_Product_Tier_1__c, TGS_Product_Tier_2__c, TGS_Product_Tier_3__c, AccountId, CWP_Legal_Entity__c, CWP_Business_Unit__c, CWP_Cost_Center__c, origin, RecordTypeId, TGS_Urgency__c, Type, TGS_Service__c, TGS_Impact__c, Priority FROM Case WHERE TGS_Product_Tier_1__c =: 't1' limit 1];
		TGS_GME_GM_Product_Categorization__c categRegularizada = [SELECT Id, TGS_Categorization_tier_1__c, TGS_Categorization_tier_2__c, TGS_Categorization_tier_3__c FROM TGS_GME_GM_Product_Categorization__c limit 1];
		categRegularizada.TGS_Service__c = caso.TGS_Product_Tier_3__c;

		update caso;
		update categRegularizada;
       	System.runAs(usuario){
     
			CWP_CreationFormController.doSave('order','termination','prueba','prueba', caso, true);
			CWP_CreationFormController.doSave('ticket','','prueba','prueba', caso, false);
		
       	}
	}
    
    @isTest static void Test1(){
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        System.runAs(usuario){
            
            Case c = [Select Id, CaseNumber,recordTypeId From Case Limit 1];            
            CWP_CreationFormController.getCaseFromNumber(c.CaseNumber);
            
            CWP_CreationFormController.doInit('ticket','termination', c);
            
            CWP_CreationFormController.getUrgencyPickList();
            CWP_CreationFormController.getImpactPickList();
            CWP_CreationFormController.getPriorityPickList();
            CWP_CreationFormController.getServicePickList('');
            CWP_CreationFormController.getLegalEntityPickList();
            CWP_CreationFormController.getBusinessUnitPicklist('test');
            CWP_CreationFormController.getCostCenterPicklist('');
            CWP_CreationFormController.getServiceUnitPickList('familyName', 'serviceName');
             NE__OrderItem__c oi=[SELECT id, Name FROM NE__OrderItem__c Limit 1];
            
            System.debug('@@@@@@@@@@\n\n\n\n'+[SELECT Id,TGS_Service__c, TGS_Categorization_tier_1__c, TGS_Categorization_tier_2__c, TGS_Categorization_tier_3__c FROM TGS_GME_GM_Product_Categorization__c]+'\n\n\n\n');
            CWP_CreationFormController.getCaseFromOrder(String.valueOf(oi.Id));
            
             CWP_CreationFormController.saveTheFile(oi.Id, 'filename', 'base64Data', 'tipo de contenido','');
            CWP_CreationFormController.saveTheChunk('filename', 'fileDescription', 'base64Data', 'contentType', CWP_MyService_services_detail_controller.saveTheFile(oi.Id, 'filename', 'base64Data', 'tipo de contenido',''), 'caso');
            //CWP_CreationFormController.getCostCenterPicklist('');
            //CWP_CreationFormController.getCostCenterPicklist('');
        }
        
    }
}