@isTest
public class TGS_ProductOrdering_UNICA_TEST {
    
    public static Map<String, String> map_json;
    
    static{
        map_json  = new Map<String, String>{
            'JSONnew' => '{"correlationId": "0999","priority" : 1,"productOrderType" : "new","relatedParty": [  {"id": "string","name": "string","entityType": "Contact","description": "String"  }],"orderItem": [{"action": "add","product": {   "id": "a0ig0000002ObtnAAC",  "name": "SIM Card Suscription",  "characteristic": [{"name": "a0fg0000002WOSrAAO","value": "12995"}  ]},"place": [  {"id": "S0001","name": "Site","entityType": "Site"  }],"quantity": "1","orderItemPrice": [  {"priceType": "MRC",  "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}  },  { "priceType": "NRC",  "recurringChargePeriod": "","price": {  "amount": 10,  "units": "EUR"}  }],  "billingAccount": [{"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"}  ],  "status" : "pending",  "validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"}  },  {"action": "add","product": {   "id": "a0ig0000002ObuqAAC",  "name": "Suscription v2",  "characteristic": [{"name": "a0fg0000002WOWoAAO","value": "Suscription v2"}  ]},"place": [  {"id": "S0001","name": "Site","entityType": "Site"  }],"quantity": "1","orderItemPrice": [  {"priceType": "MRC", "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}  },  {  "priceType": "NRC",  "recurringChargePeriod": "","price": {  "amount": 10,  "units": "EUR"}  }],  "billingAccount": [{"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"}  ],  "status" : "pending",  "validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"}  },  {"action": "add","product": {   "id": "a0ig0000002ObulAAX",  "name": "Suscription v3",  "characteristic": [{"name": "a0fg0000002WOWoAAO","value": "Suscription v3"}  ]},"place": [  {"id": "S0001","name": "Site","entityType": "Site"  }],"quantity": "1","orderItemPrice": [  {"priceType": "MRC", "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}  },  {  "priceType": "NRC",  "recurringChargePeriod": "","price": {  "amount": 10,  "units": "EUR"} }],  "billingAccount": [{"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"}  ],  "status" : "pending",  "validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"}  }]}',
                'JSONnew2' => '{"correlationId":"0999","priority":1,"productOrderType":"new2","relatedParty":[{"id":"string","name":"string","entityType":"Contact","description":"String"}],"orderItem":[{"action":"add","product":{"id":"a0ig0000002ObtnAAC","name":"SIMCardSuscription","characteristic":[{"name":"a0fg0000002WOSrAAO","value":"12995"}]},"place":[{"id":"S0001","name":"Site","entityType":"Site"}],"quantity":"1","orderItemPrice":[{"priceType":"MRC","recurringChargePeriod":"monthly","price":{"amount":10,"units":"EUR"}},{"priceType":"NRC","recurringChargePeriod":"","price":{"amount":10,"units":"EUR"}}],"billingAccount":[{"id":"ACFRC0005","name":"TestCostCenterAcc","role":"costcenter","entityType":"Account"}],"status":"pending","validFor":{"startDateTime":"2018-06-06T10:18:37.597Z","endDateTime":"2018-06-06T10:18:37.597Z"}},{"action":"add","product":{"id":"a0ig0000002ObuqAAC","name":"Suscriptionv2","characteristic":[{"name":"a0fg0000002WOWoAAO","value":"Suscriptionv2"}]},"place":[{"id":"S0001","name":"Site","entityType":"Site"}],"quantity":"1","orderItemPrice":[{"priceType":"MRC","recurringChargePeriod":"monthly","price":{"amount":10,"units":"EUR"}},{"priceType":"NRC","recurringChargePeriod":"","price":{"amount":10,"units":"EUR"}}],"billingAccount":[{"id":"ACFRC0005","name":"TestCostCenterAcc","role":"costcenter","entityType":"Account"}],"status":"pending","validFor":{"startDateTime":"2018-06-06T10:18:37.597Z","endDateTime":"2018-06-06T10:18:37.597Z"}},{"action":"add","product":{"id":"a0ig0000002ObulAAX","name":"Suscriptionv3","characteristic":[{"name":"a0fg0000002WOWoAAO","value":"Suscriptionv3"}]},"place":[{"id":"S0001","name":"Site","entityType":"Site"}],"quantity":"1","orderItemPrice":[{"priceType":"MRC","recurringChargePeriod":"monthly","price":{"amount":10,"units":"EUR"}},{"priceType":"NRC","recurringChargePeriod":"","price":{"amount":10,"units":"EUR"}}],"billingAccount":[{"id":"ACFRC0005","name":"TestCostCenterAcc","role":"costcenter","entityType":"Account"}],"status":"pending","validFor":{"startDateTime":"2018-06-06T10:18:37.597Z","endDateTime":"2018-06-06T10:18:37.597Z"}}]}',
                'JSONchange' => '{ "correlationId": "0999",  "priority" : 1,  "productOrderType" : "change",  "relatedParty": [{  "id": "string",  "name": "string",  "entityType": "Contact",  "description": "String"}  ],  "orderItem": [{  "action": "add",  "product": { "id": "a0ig0000002ObtnAAC","name": "SIM Card Suscription","characteristic": [  {"name": "a0fg0000002WOSrAAO","value": "12995"  }]  },  "place": [{  "id": "S0001",  "name": "Site",  "entityType": "Site"}  ],  "quantity": "2",  "orderItemPrice": [{  "priceType": "MRC",   "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}},{   "priceType": "NRC","recurringChargePeriod": "",  "price": {  "amount": 10,  "units": "EUR"  }}],"billingAccount": [  {"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"  }],"status" : "pending","validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"  }},{  "action": "add",  "product": { "id": "a0ig0000002ObuqAAC","name": "Suscription v2","characteristic": [  {"name": "a0fg0000002WOWoAAO","value": "Suscription v2"  }]  },  "place": [{  "id": "S0001",  "name": "Site",  "entityType": "Site"}  ],  "quantity": "2",  "orderItemPrice": [{  "priceType": "MRC",   "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}},{"priceType": "NRC","recurringChargePeriod": "",  "price": {  "amount": 10,  "units": "EUR"  }}],"billingAccount": [  {"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"  }],"status" : "pending","validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"  }},{  "action": "add",  "product": { "id": "a0ig0000002ObuqAAC","name": "Suscription v22","characteristic": [  {"name": "a0fg0000002WOWoAAO","value": "Suscription v22"  }]  },  "place": [{  "id": "S0001",  "name": "Site",  "entityType": "Site"}  ],  "quantity": "2",  "orderItemPrice": [{  "priceType": "MRC",   "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}},{"priceType": "NRC","recurringChargePeriod": "",  "price": {  "amount": 10,  "units": "EUR"  }}],"billingAccount": [  {"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"  }],"status" : "pending","validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"  }},{  "action": "add",  "product": { "id": "a0ig0000002ObulAAX","name": "Suscription v3","characteristic": [  {"name": "a0fg0000002WOWoAAO","value": "Suscription v3"  }]  },  "place": [{  "id": "S0001",  "name": "Site",  "entityType": "Site"}  ],  "quantity": "1",  "orderItemPrice": [{  "priceType": "MRC",   "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}},{"priceType": "NRC","recurringChargePeriod": "",  "price": {  "amount": 10,  "units": "EUR"  }   }],"billingAccount": [  {"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"  }],"status" : "pending","validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"  }},{  "action": "add",  "product": { "id": "a0ig0000002ObulAAX","name": "Suscription v3","characteristic": [  {"name": "a0fg0000002WOWoAAO","value": "Suscription v3"  }]  },  "place": [{  "id": "S0001",  "name": "Site",  "entityType": "Site"}  ],  "quantity": "1",  "orderItemPrice": [{  "priceType": "MRC",   "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}},{"priceType": "NRC","recurringChargePeriod": "",  "price": {  "amount": 10,  "units": "EUR"  }   }],"billingAccount": [  {"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"  }],"status" : "pending","validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"  }}]}',
                'JSONchange1' => '{"correlationId": "0999","priority" : 1,"productOrderType" : "change","relatedParty": [{"id": "string","name": "string","entityType": "Contact","description": "String"}],"orderItem": [{"action": "add","product": { "id": "a0ig0000002ObtnAAC","name": "SIM Card Suscription","characteristic": [{"name": "a0fg0000002WOSrAAO","value": "12995"}]},"place": [{"id": "S0001","name": "Site","entityType": "Site"}],"quantity": "2","orderItemPrice": [{"priceType": "MRC", "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}},{ "priceType": "NRC","recurringChargePeriod": "","price": {"amount": 10,"units": "EUR"}}],"billingAccount": [{"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"}],"status" : "pending","validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"}},{"action": "add","product": { "id": "a0ig0000002ObulAAX","name": "Suscription v3","characteristic": [{"name": "a0fg0000002WOWoAAO","value": "Suscription v3"}]},"place": [{"id": "S0001","name": "Site","entityType": "Site"}],"quantity": "1","orderItemPrice": [{"priceType": "MRC", "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}},{"priceType": "NRC","recurringChargePeriod": "","price": {"amount": 10,"units": "EUR"} }],"billingAccount": [{"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"}],"status" : "pending","validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"}}]}',
                'JSONchange2' => '{"correlationId":"0000","priority":1,"productOrderType":"change","relatedParty":[{"id":"string","name":"string","entityType":"Contact","description":"String"}],"orderItem":[{"action":"add","product":{"id":"a0ig0000002ObtnAAC","name":"SIMCardSuscription","characteristic":[{"name":"a0fg0000002WOSrAAO","value":"12995"}]},"place":[{"id":"S0001","name":"Site","entityType":"Site"}],"quantity":"2","orderItemPrice":[{"priceType":"MRC","recurringChargePeriod":"monthly","price":{"amount":10,"units":"EUR"}},{"priceType":"NRC","recurringChargePeriod":"","price":{"amount":10,"units":"EUR"}}],"billingAccount":[{"id":"ACFRC0005","name":"TestCostCenterAcc","role":"costcenter","entityType":"Account"}],"status":"pending","validFor":{"startDateTime":"2018-06-06T10:18:37.597Z","endDateTime":"2018-06-06T10:18:37.597Z"}},{"action":"add","product":{"id":"a0ig0000002ObulAAX","name":"Suscriptionv3","characteristic":[{"name":"a0fg0000002WOWoAAO","value":"Suscriptionv3"}]},"place":[{"id":"S0001","name":"Site","entityType":"Site"}],"quantity":"1","orderItemPrice":[{"priceType":"MRC","recurringChargePeriod":"monthly","price":{"amount":10,"units":"EUR"}},{"priceType":"NRC","recurringChargePeriod":"","price":{"amount":10,"units":"EUR"}}],"billingAccount":[{"id":"ACFRC0005","name":"TestCostCenterAcc","role":"costcenter","entityType":"Account"}],"status":"pending","validFor":{"startDateTime":"2018-06-06T10:18:37.597Z","endDateTime":"2018-06-06T10:18:37.597Z"}}]}',
                'JSONchange3' => '{"correlationId":"0999","priority":1,"productOrderType":"change","relatedParty":[{"id":"string","name":"string","entityType":"Contact","description":"String"}],"orderItem":[{"action":"add","product":{"id":"a0ig0000002ObtnAAT","name":"SIMCardSuscription","characteristic":[{"name":"a0fg0000002WOSrAAO","value":"12995"}]},"place":[{"id":"S0001","name":"Site","entityType":"Site"}],"quantity":"2","orderItemPrice":[{"priceType":"MRC","recurringChargePeriod":"monthly","price":{"amount":10,"units":"EUR"}},{"priceType":"NRC","recurringChargePeriod":"","price":{"amount":10,"units":"EUR"}}],"billingAccount":[{"id":"ACFRC0005","name":"TestCostCenterAcc","role":"costcenter","entityType":"Account"}],"status":"pending","validFor":{"startDateTime":"2018-06-06T10:18:37.597Z","endDateTime":"2018-06-06T10:18:37.597Z"}},{"action":"add","product":{"id":"a0ig0000002ObulAAX","name":"Suscriptionv3","characteristic":[{"name":"a0fg0000002WOWoAAO","value":"Suscriptionv3"}]},"place":[{"id":"S0001","name":"Site","entityType":"Site"}],"quantity":"1","orderItemPrice":[{"priceType":"MRC","recurringChargePeriod":"monthly","price":{"amount":10,"units":"EUR"}},{"priceType":"NRC","recurringChargePeriod":"","price":{"amount":10,"units":"EUR"}}],"billingAccount":[{"id":"ACFRC0005","name":"TestCostCenterAcc","role":"costcenter","entityType":"Account"}],"status":"pending","validFor":{"startDateTime":"2018-06-06T10:18:37.597Z","endDateTime":"2018-06-06T10:18:37.597Z"}}]}',
                'JSONdc' => '{"correlationId": "0999","priority" : 1,"productOrderType" : "disconnect","relatedParty": [  {"id": "string","name": "string","entityType": "Contact","description": "String"  }],"orderItem": [{"action": "add","product": {   "id": "a0ig0000002ObtnAAC",  "name": "SIM Card Suscription",  "characteristic": [{"name": "a0fg0000002WOSrAAO","value": "12995"}  ]},"place": [  {"id": "S0001","name": "Site","entityType": "Site"  }],"quantity": "1","orderItemPrice": [  {"priceType": "MRC",  "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}  },  { "priceType": "NRC",  "recurringChargePeriod": "","price": {  "amount": 10,  "units": "EUR"}  }],  "billingAccount": [{"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"}  ],  "status" : "pending",  "validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"}  },  {"action": "add","product": {   "id": "a0ig0000002ObuqAAC",  "name": "Suscription v2",  "characteristic": [{"name": "a0fg0000002WOWoAAO","value": "Suscription v2"}  ]},"place": [  {"id": "S0001","name": "Site","entityType": "Site"  }],"quantity": "1","orderItemPrice": [  {"priceType": "MRC", "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}  },  {  "priceType": "NRC",  "recurringChargePeriod": "","price": {  "amount": 10,  "units": "EUR"}  }],  "billingAccount": [{"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"}  ],  "status" : "pending",  "validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"}  },  {"action": "add","product": {   "id": "a0ig0000002ObulAAX",  "name": "Suscription v3",  "characteristic": [{"name": "a0fg0000002WOWoAAO","value": "Suscription v3"}  ]},"place": [  {"id": "S0001","name": "Site","entityType": "Site"  }],"quantity": "1","orderItemPrice": [  {"priceType": "MRC", "recurringChargePeriod": "monthly","price": {"amount": 10,"units": "EUR"}  },  {  "priceType": "NRC",  "recurringChargePeriod": "","price": {  "amount": 10,  "units": "EUR"} }],  "billingAccount": [{"id": "ACFRC0005","name": "TestCostCenterAcc","role": "cost center","entityType": "Account"}  ],  "status" : "pending",  "validFor": {"startDateTime": "2018-06-06T10:18:37.597Z","endDateTime": "2018-06-06T10:18:37.597Z"}  }]}'
                };
                    
                    } 
    
    @testSetup static void setup() {
        
        // Create data for the test class
        Map<String,String> mapRTsTEST=new Map<String,String>();
        for(RecordType rt:[SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('TGS_Legal_Entity','TGS_Business_Unit','TGS_Cost_Center', 'TGS_Holding', 'TGS_Customer_Country')]){
            mapRTsTEST.put(rt.DeveloperName,rt.id);
        } 
        //RecordType-&-Account 
        Account holding=new Account();
        holding.Name='TestHoldingAcc';
        holding.TGS_IntegrationID__c='ACFRC0001';
        holding.RecordTypeId= mapRTsTEST.get('TGS_Holding');
        holding.TGS_Fecha_Activacion__c =Date.valueOf('2018-01-01');
        insert holding;
        
        Account CustomerCountry=new Account();
        CustomerCountry.Name='TestCustomerCountryAcc';
        CustomerCountry.Parent=holding;
        CustomerCountry.ParentId=holding.id;
        CustomerCountry.TGS_IntegrationID__c='ACFRC0002';
        CustomerCountry.RecordTypeId=mapRTsTEST.get('TGS_Customer_Country');
        CustomerCountry.TGS_Fecha_Activacion__c =Date.valueOf('2018-05-01');
        insert CustomerCountry;
        
        Account LegalEntity=new Account();
        LegalEntity.Name='TestLegalEntityAcc';
        LegalEntity.Parent=CustomerCountry;
        LegalEntity.ParentId=CustomerCountry.id;
        LegalEntity.TGS_IntegrationID__c='ACFRC0003';
        LegalEntity.RecordTypeId=mapRTsTEST.get('TGS_Legal_Entity');
        LegalEntity.BillingCountry='Spain';
        LegalEntity.BillingCity='Barcelona';
        LegalEntity.TGS_Fecha_Activacion__c =Date.valueOf('2018-06-02');
        insert LegalEntity; 
        
        Account BusinessUnit=new Account();
        BusinessUnit.Name='TestBussniesUnitAcc';
        BusinessUnit.Parent=LegalEntity;
        BusinessUnit.ParentId=LegalEntity.id;
        BusinessUnit.TGS_IntegrationID__c='ACFRC0004';
        BusinessUnit.RecordTypeId=mapRTsTEST.get('TGS_Business_Unit');
        BusinessUnit.TGS_Es_MNC__c=true;
        BusinessUnit.TGS_Fecha_Activacion__c =Date.valueOf('2018-07-03');
        insert BusinessUnit;
        
        
        Account CostCenter=new Account();
        CostCenter.Name='TestCostCenterAcc';
        CostCenter.Parent=BusinessUnit;
        CostCenter.ParentId=BusinessUnit.id;
        CostCenter.TGS_IntegrationID__c='ACFRC0005';
        CostCenter.RecordTypeId=mapRTsTEST.get('TGS_Cost_Center');
        CostCenter.TGS_Aux_Legal_Entity__c=LegalEntity.id;
        CostCenter.TGS_Fecha_Activacion__c =Date.valueOf('2018-08-04');
        insert CostCenter;
        //contract
        NE__Contract_Header__c ch=new NE__Contract_Header__c();
        ch.NE__Name__c='TestContractHeader';
        insert ch;
        NE__Contract_Account_Association__c caa=new NE__Contract_Account_Association__c();
        
        caa.NE__Contract_Header__c=ch.id;
        caa.NE__Account__c=LegalEntity.id; 
        insert caa;
        
        //sede
        BI_Sede__c sede=new BI_Sede__c();
        sede.Name='TestSede';
        sede.BI_Direccion__c='TestDi';
        insert sede;
        //site
        BI_Punto_de_Instalacion__c pdi=new BI_Punto_de_Instalacion__c();
        pdi.TEMPEXT_ID__c='S0001';
        pdi.Name='Site';
        pdi.BI_Cliente__c=BusinessUnit.id;
        pdi.BI_Sede__c=sede.id;
        insert pdi;
        System.debug('+++ Holding name: '+pdi.TGS_Holding_Name__c);
        System.debug('+++ Cliente name: '+pdi.BI_Cliente__r+' Cliente id: '+pdi.BI_Cliente__c);
        
        
        // *********************************
        // Catalog test data - START
        // *********************************
        
        RecordType RTProd = [SELECT Id FROM RecordType WHERE (SobjectType = 'NE__Product__c' OR SobjectType = 'Product__c') AND Name = 'Standard'];
        List<NE__Product__c> lst_prod = new List<NE__Product__c>();
        NE__Product__c prod1 = new NE__Product__c(Name='SIM Card Suscription', RecordTypeId=RTProd.Id, NE__Source_Product_Id__c = 'a0ig0000002ObtnAAC');
        lst_prod.add(prod1);
        
        NE__Product__c prod2 = new NE__Product__c(Name='Suscription', RecordTypeId=RTProd.Id, NE__Source_Product_Id__c = 'a0ig0000002ObulAAC');
        lst_prod.add(prod2);
        
        NE__Product__c prod3 = new NE__Product__c(Name='Suscription v2', RecordTypeId=RTProd.Id, NE__Source_Product_Id__c = 'a0ig0000002ObuqAAC');
        lst_prod.add(prod3);
        
        NE__Product__c prod4 = new NE__Product__c(Name='Suscription v3', RecordTypeId=RTProd.Id, NE__Source_Product_Id__c = 'a0ig0000002ObulAAX');
        lst_prod.add(prod4);
        Insert lst_prod;
        
        NE__Family__c familyProd1    =   new NE__Family__c(name = 'Test Family Prod1');
        insert familyProd1;
        
        NE__Family__c familyProd2    =   new NE__Family__c(name = 'Test Family Prod2');
        insert familyProd2;
        
        NE__Family__c familyProd3    =   new NE__Family__c(name = 'Test Family Prod3');
        insert familyProd3;
        
        NE__Family__c familyProd4 = new NE__Family__c(name='Test Family Prod4');
        insert familyProd4;
        
        List<NE__DynamicPropertyDefinition__c> lstDynProperty = new List<NE__DynamicPropertyDefinition__c>();
        NE__DynamicPropertyDefinition__c prop1   =   new NE__DynamicPropertyDefinition__c(name = 'String1',NE__Type__c ='String');
        NE__DynamicPropertyDefinition__c prop2   =   new NE__DynamicPropertyDefinition__c(name = 'String2',NE__Type__c ='String');
        lstDynProperty.add(prop1);
        lstDynProperty.add(prop2);
        insert lstDynProperty;
        
        List<NE__ProductFamily__c> lstProdFam = new List<NE__ProductFamily__c>();
        NE__ProductFamily__c prodFamilyProd1 =   new NE__ProductFamily__c(NE__ProdId__c = prod1.id, NE__FamilyId__c = familyProd1.id);
        lstProdFam.add(prodFamilyProd1);
        
        NE__ProductFamily__c prodFamilyProd2 =   new NE__ProductFamily__c(NE__ProdId__c = prod2.id, NE__FamilyId__c = familyProd2.id);
        lstProdFam.add(prodFamilyProd2);
        
        NE__ProductFamily__c prodFamilyProd3 =   new NE__ProductFamily__c(NE__ProdId__c = prod3.id, NE__FamilyId__c = familyProd3.id);
        lstProdFam.add(prodFamilyProd3);
        
        NE__ProductFamily__c prodFamilyProd4=new NE__ProductFamily__c(NE__ProdId__c=prod4.id,NE__FamilyId__c=familyProd4.id);
        lstProdFam.add(prodFamilyProd4);
        
        Insert lstProdFam;
        
        
        List<NE__ProductFamilyProperty__c> lstPfp = new List<NE__ProductFamilyProperty__c>();
        NE__ProductFamilyProperty__c pfp1 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd1.Id, NE__PropId__c=prop1.Id, NE__Source_Product_Family_Property_Id__c = 'a0fg0000002WOSrAAO');
        NE__ProductFamilyProperty__c pfp2 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd2.Id, NE__PropId__c=prop2.Id, NE__Source_Product_Family_Property_Id__c = 'a0fg0000002WOWoAAO');
        NE__ProductFamilyProperty__c pfp3 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd3.Id, NE__PropId__c=prop2.Id, NE__Source_Product_Family_Property_Id__c = 'a0fg0000002WOWoAAO');
        NE__ProductFamilyProperty__c pfp4 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd4.Id, NE__PropId__c=prop2.Id, NE__Source_Product_Family_Property_Id__c = 'a0fg0000002WOWoAAO');
        
        lstPfp.add(pfp1);
        lstPfp.add(pfp2);
        lstPfp.add(pfp3);
        lstPfp.add(pfp4);
        Insert lstPfp;
        
        
        NE__Catalog_Header__c catHead = new NE__Catalog_Header__c(Name='Catalog Header',
                                                                  NE__Name__C='Catalog Header');
        insert catHead;
        
        NE__Catalog__c  cat = new NE__Catalog__c(Name='TGSOL Catalog',
                                                 NE__Catalog_Header__c=catHead.Id,
                                                 NE__Source_Catalog_Id__c='	a0B5E000002wsw7UAA', 
                                                 NE__StartDate__c=Datetime.now()+1, 
                                                 NE__Active__c = true);
        insert cat;
        
        NE__Catalog_Category__c catCat = new NE__Catalog_Category__c(Name='Global Mobility for MNCs', 
                                                                     NE__CatalogId__c=cat.Id);
        insert catCat;
        
        /*NE__Catalog_Item__c catItem = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Enumerated',
NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
NE__Start_Date__c=Datetime.now()+1, NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
NE__Recurring_Charge_Frequency__c='Monthly');
insert catItem;*/
        
        
        
        NE__Catalog_Item__c catItemC1 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Product',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                NE__Start_Date__c=Datetime.now()+1, NE__Base_OneTime_Fee__c=0, NE__BaseRecurringCharge__c=0, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly');
        insert catItemC1;
        
        
        
        NE__Item_Header__c itemHead = new  NE__Item_Header__c(Name='Item Header',
                                                              NE__Catalog__c=cat.id,
                                                              NE__Catalog_Item__c=catItemC1.id,
                                                              NE__Source_Item_Header_Id__c ='a0Tg0000002gUpJEAU'
                                                             );
        insert itemHead;       
        
        catItemC1.NE__Item_Header__c=itemHead.id;
        update catItemC1;
        
        System.debug('catitemc1id: '+catItemC1.id);
        
        NE__Catalog_Item__c catItemC2 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Root',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                NE__Start_Date__c=Datetime.now()+1, NE__Base_OneTime_Fee__c=0, NE__BaseRecurringCharge__c=0, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                                NE__Parent_Catalog_Item__c=catItemC1.id);
        insert catItemC2;
        
        System.debug('catitemc2id: '+catItemC2.id);
        
        NE__Catalog_Item__c catItemC3 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Category',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod2.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                NE__Start_Date__c=Datetime.now()+1, NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                                NE__Parent_Catalog_Item__c=catItemC2.id,
                                                                NE__Root_Catalog_Item__c=catItemC2.id);
        insert catItemC3;
        
        NE__Catalog_Item__c catItemC4 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Child-Product',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod3.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                NE__Start_Date__c=Datetime.now()+1, NE__Base_OneTime_Fee__c=0, NE__BaseRecurringCharge__c=0, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                                NE__Parent_Catalog_Item__c=catItemC3.id,
                                                                NE__Root_Catalog_Item__c=catItemC2.id);
        insert catItemC4;
        
        NE__Catalog_Item__c catItemC5 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Category',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod2.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                NE__Start_Date__c=Datetime.now()+1, NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                                NE__Parent_Catalog_Item__c=catItemC4.id,
                                                                NE__Root_Catalog_Item__c=catItemC4.id);
        insert catItemC5;
        
        NE__Catalog_Item__c catItemC6 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Child-Product',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod4.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                NE__Start_Date__c=Datetime.now()+1, NE__Base_OneTime_Fee__c=0, NE__BaseRecurringCharge__c=0, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                                NE__Parent_Catalog_Item__c=catItemC5.id,
                                                                NE__Root_Catalog_Item__c=catItemC4.id);
        insert catItemC6;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/v1/ordering/accounts/ACFRC0003/productOrders'; 
        req.httpMethod = 'POST';
        //NEW
        req.requestBody = Blob.valueof(map_json.get('JSONnew'));
        RestContext.request = req;
        RestContext.response = res;
        TGS_ProductOrdering_UNICA.createOrder();
        
        req.requestURI = '/v1/ordering/accounts/ACFRC0003/productOrders'; 
        req.httpMethod = 'POST';
        //NEW2
        req.requestBody = Blob.valueof(map_json.get('JSONnew2'));
        RestContext.request = req;
        RestContext.response = res;
        TGS_ProductOrdering_UNICA.createOrder();
    }
    
    @isTest
    public static void testNew(){
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/v1/ordering/accounts/ACFRC0003/productOrders'; 
        req.httpMethod = 'POST';
        
        //NEW
        Test.startTest();
        req.requestBody = Blob.valueof(map_json.get('JSONnew'));
        RestContext.request = req;
        RestContext.response = res;
        TGS_ProductOrdering_UNICA.createOrder();
        Test.stopTest();
    }
    
    @isTest
    public static void testChange(){
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/v1/ordering/accounts/ACFRC0003/productOrders'; 
        req.httpMethod = 'POST';
        
        //CHANGE
        Test.startTest();
        req.requestBody = Blob.valueOf(map_json.get('JSONchange'));
        RestContext.request = req;
        RestContext.response = res;
        TGS_ProductOrdering_UNICA.createOrder();
        Test.stopTest();
    }
    
    @isTest
    public static void testChange1(){
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/v1/ordering/accounts/ACFRC0003/productOrders'; 
        req.httpMethod = 'POST';
        
        //CHANGE
        Test.startTest();
        req.requestBody = Blob.valueOf(map_json.get('JSONchange1'));
        RestContext.request = req;
        RestContext.response = res;
        TGS_ProductOrdering_UNICA.createOrder();
        Test.stopTest();
    }
    
    @isTest
    public static void testChange2(){
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/v1/ordering/accounts/ACFRC0003/productOrders'; 
        req.httpMethod = 'POST';
        
        //CHANGE
        Test.startTest();
        req.requestBody = Blob.valueOf(map_json.get('JSONchange2'));
        RestContext.request = req;
        RestContext.response = res;
        TGS_ProductOrdering_UNICA.createOrder();
        Test.stopTest();
    }
    
    @isTest
    public static void testChange3(){
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/v1/ordering/accounts/ACFRC0003/productOrders'; 
        req.httpMethod = 'POST';
        
        //CHANGE
        Test.startTest();
        req.requestBody = Blob.valueOf(map_json.get('JSONchange3'));
        RestContext.request = req;
        RestContext.response = res;
        TGS_ProductOrdering_UNICA.createOrder();
        Test.stopTest();
    }
    
    @isTest
    public static void testDisconnect(){
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/v1/ordering/accounts/ACFRC0003/productOrders'; 
        req.httpMethod = 'POST';
        
        //DISCONNECT
        Test.startTest();
        req.requestBody = Blob.valueof(map_json.get('JSONdc'));
        RestContext.request = req;
        RestContext.response = res;
        TGS_ProductOrdering_UNICA.createOrder();
        
        req.requestBody = Blob.valueof(map_json.get('JSONdc'));
        RestContext.request = req;
        RestContext.response = res;
        TGS_ProductOrdering_UNICA.createOrder();
        Test.stopTest();	
    }
}