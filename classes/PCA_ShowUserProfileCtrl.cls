public class PCA_ShowUserProfileCtrl  extends PCA_HomeController{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longalera
    Company:       Salesforce.com
    Description:   Controller of Show user profile page
    
    History:
    
    <Date>            <Author>          	<Description> 
    27/06/2014        Jorge Longalera       Initial version
    25/07/2014		  Antonio Moruno 		v2
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    	
	public string URL {get;set;}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>          	<Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public PageReference checkPermissions (){
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			
			PageReference page = enviarALoginComm();
			if(page == null){
				loadInfo();
			}
			return page;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_ShowUserProfileCtrl.checkPermissions', 'Portal Platino', Exc, 'Class');
		   return null;
		}
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Contructor
    
    History:
    
    <Date>            <Author>          	<Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public PCA_ShowUserProfileCtrl(){}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   load user chatter profile
    
    History:
    
    <Date>            <Author>          	<Description>
    25/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public void loadInfo(){
		try{
			
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			
			if(ApexPages.currentPage().getParameters().get('Id')!=null){
				Id userId = String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('Id'));
				system.debug('userId: '+userId);
				
				URL = Site.getBaseUrl()+'/_ui/core/userprofile/UserProfilePage?u='+userId+'&tab=sfdc.ProfilePlatformFeed';
				system.debug('URL: ' +URL);
			}	
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_ShowUserProfileCtrl.loadInfo', 'Portal Platino', Exc, 'Class');
		}
	}
	
}