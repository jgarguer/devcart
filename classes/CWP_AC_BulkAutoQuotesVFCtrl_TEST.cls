@isTest
public class CWP_AC_BulkAutoQuotesVFCtrl_TEST {
	

	@testSetup 
    static void startUp(){
        System.runAs(new User(Id = UserInfo.getUserId())) {
            String strRecordType = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Quote');

            Id recordtypeCustCtry = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY); 
            Account accCountry =   new Account(name = 'TestAccountCountry' , RecordTypeId=recordtypeCustCtry,BI_Segment__c='Negocios',BI_Subsegment_Regional__c='Negocios');
            insert accCountry;
             
            Id recordtypeAccId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_LEGAL_ENTITY);
            Account acc = new Account(Name='Account', ParentId = accCountry.Id,RecordTypeId=recordtypeAccId,BI_Segment__c='Negocios',BI_Subsegment_Regional__c='Negocios');
             insert acc;
             
            //Create contact and accounts 
             Contact contact1 = new Contact(
                 FirstName = 'Test',
                 Lastname = 'Test',
                 AccountId = acc.Id,
                 Email = System.now().millisecond() + 'test@test.com'
             );
             insert contact1;
             // 21-02-2018 - Manuel Ochoa -  Disabled Line -  commented only for information purposes, delete in UAT
             //List<Account> lstAccount = BI_O4_DisconnectionMethods_TEST.loadAccountsWithOtherRT(1, new String[]{'Spain'}); 

            // Create catalog and services data

            RecordType RTProd = [SELECT Id FROM RecordType WHERE (SobjectType = 'NE__Product__c' OR SobjectType = 'Product__c') AND Name = 'Standard'];
            NE__Product__c prod1 = new NE__Product__c(Name='Test Product 1', RecordTypeId=RTProd.Id);
            insert prod1;
            
            NE__Product__c prod2 = new NE__Product__c(Name='Test Child Product 2', RecordTypeId=RTProd.Id);
            insert prod2;
                
            NE__Family__c familyProd1    =   new NE__Family__c(name = 'Test Family Prod1');
            insert familyProd1;

            NE__Family__c familyProd2    =   new NE__Family__c(name = 'Test Family Prod2');
            insert familyProd2;
                
            NE__DynamicPropertyDefinition__c prop1   =   new NE__DynamicPropertyDefinition__c(name = 'TestPropertyEnum',NE__Type__c ='Enumerated');
            insert prop1;
            NE__PropertyDomain__c enumProp1 = new NE__PropertyDomain__c(Name='1',NE__PropId__c=prop1.Id);
            NE__PropertyDomain__c enumProp2 = new NE__PropertyDomain__c(Name='2',NE__PropId__c=prop1.Id);
            insert enumProp1;
            insert enumProp2;
            NE__DynamicPropertyDefinition__c prop2   =   new NE__DynamicPropertyDefinition__c(name = 'TestPropertyString',NE__Type__c ='String');
            NE__DynamicPropertyDefinition__c prop3   =   new NE__DynamicPropertyDefinition__c(name = 'TestPropertyNumber',NE__Type__c ='Number');
            NE__DynamicPropertyDefinition__c prop4   =   new NE__DynamicPropertyDefinition__c(name = 'TestPropertyLookup',NE__Type__c ='Dynamic Lookup');        
            insert prop2;
            insert prop3;
            insert prop4;
                
            NE__ProductFamily__c prodFamilyProd1 =   new NE__ProductFamily__c(NE__ProdId__c = prod1.id, NE__FamilyId__c = familyProd1.id);
            insert prodFamilyProd1;

            NE__ProductFamily__c prodFamilyProd2 =   new NE__ProductFamily__c(NE__ProdId__c = prod2.id, NE__FamilyId__c = familyProd2.id);
            insert prodFamilyProd2;
                
            NE__ProductFamilyProperty__c pfp1 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd1.Id, NE__PropId__c=prop1.Id);
            NE__ProductFamilyProperty__c pfp2 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd1.Id, NE__PropId__c=prop2.Id);
            NE__ProductFamilyProperty__c pfp3 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd1.Id, NE__PropId__c=prop3.Id);
            NE__ProductFamilyProperty__c pfp4 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd1.Id, NE__PropId__c=prop4.Id,NE__Dynamic_Lookup__c='Lookup Test');
            insert pfp1;
            insert pfp2;
            insert pfp3;
            insert pfp4;
            NE__ProductFamilyProperty__c pfp5 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd2.Id, NE__PropId__c=prop1.Id);
            NE__ProductFamilyProperty__c pfp6 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd2.Id, NE__PropId__c=prop2.Id);
            NE__ProductFamilyProperty__c pfp7 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=familyProd2.Id, NE__PropId__c=prop3.Id);
            insert pfp5;
            insert pfp6;
            insert pfp7;

            NE__Dynamic_Lookup__c dynLookUp = new NE__Dynamic_Lookup__c(NE__Name__c='Lookup Test',
                                                                        NE__Columns__c='name',
                                                                        NE__sObject__c='Account');
            insert dynLookUp;

            NE__Lookup_Map__c dynLookUpMap = new NE__Lookup_Map__c( NE__Name__c='Lookup Map Test',
                                                                    NE__Dynamic_Lookup__c=dynLookUp.id,
                                                                    NE__Family__c=familyProd1.id);
            insert dynLookUpMap;

            NE__Lookup_Map_Value__c dynLookUpMapValue = new NE__Lookup_Map_Value__c(NE__Lookup_Map__c=dynLookUpMap.id,
                                                                                    NE__Product_Family_Property__c=pfp4.id,
                                                                                    NE__Type__c='Field',
                                                                                    NE__Value__c='Name');
            insert dynLookUpMapValue;

            NE__Catalog_Header__c catHead = new NE__Catalog_Header__c(Name='Catalog Header',
                                                                     NE__Name__C='Catalog Header');
            insert catHead;
            
            NE__Catalog__c  cat = new NE__Catalog__c(Name='Wholesale Catalog',
                                                    NE__Catalog_Header__c=catHead.Id, 
                                                    NE__StartDate__c=Datetime.now(), 
                                                    NE__Active__c = true);
            insert cat;
            
             NE__Catalog_Category__c catCat = new NE__Catalog_Category__c(Name='Service Lines', 
                                                               NE__CatalogId__c=cat.Id);
            insert catCat;
           
            NE__Catalog_Item__c catItem = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Enumerated',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly');
            insert catItem;
            
            
            
            NE__Catalog_Item__c catItemC1 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Product',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly');
            insert catItemC1;
            
            NE__Item_Header__c itemHead = new  NE__Item_Header__c(Name='PROJECT',
                                                                  NE__Catalog__c=cat.id,
                                                                  NE__Catalog_Item__c=catItemC1.id
                                                                );
            insert itemHead;       
            
            catItemC1.NE__Item_Header__c=itemHead.id;
            update catItemC1;
            
            NE__Catalog_Item__c catItemC2 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Root',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                                NE__Parent_Catalog_Item__c=catItemC1.id);
            insert catItemC2;
            
            NE__Catalog_Item__c catItemC3 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Category',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                                NE__Parent_Catalog_Item__c=catItemC2.id,
                                                                NE__Root_Catalog_Item__c=catItemC2.id);
            insert catItemC3;
            
            NE__Catalog_Item__c catItemC4 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Child-Product',
                                                                NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod2.Id, NE__Catalog_Category_Name__c=catCat.Id,
                                                                 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
                                                                NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                                NE__Parent_Catalog_Item__c=catItemC3.id,
                                                                NE__Root_Catalog_Item__c=catItemC2.id);
            insert catItemC4;

            // add CWP_AC_ServicesCost__c rows
            // SELECT Country__c, City__c FROM CWP_AC_ServicesCost__c GROUP BY Country__c,City__c ORDER BY Country__c,City__c

            CWP_AC_ServicesCost__c scItem = new CWP_AC_ServicesCost__c(CWP_AC_Country__c='Country', CWP_AC_City__c='City');
            insert scItem;
        }       
    }

    @isTest static void test_getProductTree(){

      // launch test

        //getTestData();        
        //PageReference pageRef = new PageReference('/apex/CWP_AC_BulkAutoQuotesVF');
        //Test.setCurrentPage(pageRef);
    	Test.startTest();
    	CWP_AC_BulkAutoQuotesVFCtrl ctrl=new CWP_AC_BulkAutoQuotesVFCtrl();
    	ctrl.getProductTree();
    	Test.stopTest();
    }

    @isTest static void test_getDynamicLookUpTable(){

      // launch test

        //getTestData();        
        //PageReference pageRef = new PageReference('/apex/CWP_AC_BulkAutoQuotesVF');
        //Test.setCurrentPage(pageRef);
    	Test.startTest();
    	CWP_AC_BulkAutoQuotesVFCtrl ctrl=new CWP_AC_BulkAutoQuotesVFCtrl();
    	ctrl.getDynamicLookUpTable();
    	Test.stopTest();
    }

    @isTest static void test_importFile(){       
      // launch test
        String csvFile = 'head_ConfigurationID;head_Account;head_Catalog;head_Category;head_ConfItemRoot;head_Category;head_ConfItemChild1;head_Category;head_ConfItemChild2;head_Qty;head_Family;head_DynPropDef;head_Value;head_ID;Configuration HoldingId__c;Configuration NE__ServAccId__c;Configuration Cost_Center__c;Configuration Configuration Site__c;Configuration Authorized_User__c;Configuration NE__AccountId__c;Configuration NE__Contract_Account__c';
            
        csvFile += 'ORD-001;accNme;catHeadName;;;;;;;;;;;;;\n';
        csvFile += ';;;catCatName;prod1.Name;;;;;3;familyName;propName;This is a value;;accountId;account2Id;account3Id;;uId;account.Id;contractHeadId\n';
        csvFile += 'ORD-002;accNme;catHeadName;catCatName;catItem1NE__ProductId__rName;catCatName;catItem1NE__ProductId__rName;catCatName;catItem1NE__ProductId__rName;1;;;;;;;;;;;;\n';
        
        csvFile += 'ORD-003;accNme;catHeadName;;;;;;;;;;;;;\n';
        csvFile += ';;;catCatName;prod1.Name;;;;;3;familyName;propName;This is a value;;accountId;account2Id;account3Id;;uId;account.Id;contractHeadId\n';
        csvFile += 'ORD-004;accNme;catHeadName;catCatName;catItem1NE__ProductId__rName;catCatName;catItem1NE__ProductId__rName;catCatName;catItem1NE__ProductId__rName;1;;;;;;;;;;;;\n';
        
        csvFile += 'ORD-005;accNme;catHeadName;;;;;;;;;;;;;\n';
        csvFile += ';;;catCatName;prod1.Name;;;;;3;familyName;propName;This is a value;;accountId;account2Id;account3Id;;uId;account.Id;contractHeadId\n';
        csvFile += 'ORD-006;accNme;catHeadName;catCatName;catItem1NE__ProductId__rName;catCatName;catItem1NE__ProductId__rName;catCatName;catItem1NE__ProductId__rName;1;;;;;;;;;;;;\n';
        
               
        Test.startTest();
        CWP_AC_BulkAutoQuotesVFCtrl ctrl=new CWP_AC_BulkAutoQuotesVFCtrl();
        ctrl.strCsvData=csvFile;

        //other methods with less importance
        /*ctrl.echoVal();
        BI_O4_BulkFromOptyController.generateRandomNumber();
        BI_O4_BulkFromOptyController.generateIdentifier('TEST');
        BI_O4_BulkFromOptyController.generate();*/
        ctrl.echoVal();
        ctrl.importFile(csvFile);
        CWP_AC_BulkAutoQuotesVFCtrl.safeSplit(csvFile, '\n', 9);
        
        Test.stopTest();
    }


    @isTest static void test_getServiceCosts(){
        Test.startTest();
    	CWP_AC_BulkAutoQuotesVFCtrl ctrl = new CWP_AC_BulkAutoQuotesVFCtrl();
        List<List<String>> list1 = new List<List<String>>();
        List<String> list2 = new List<String>();
        list2.add('Test');
        list2.add('Spain');
        list2.add('Madrid');
        list1.add(list2);
        list2 = new List<String>();
        list2.add('Test');
        list2.add('Argentina');
        list2.add('Buenos Aires');
        list1.add(list2);
        
        Id recordtypeCustCtry = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING); 
        Account accCountry = [	SELECT Id,name,RecordTypeId,BI_Segment__c,BI_Subsegment_Regional__c
								FROM Account
								LIMIT 1];
        ctrl.strAccountId = accCountry.Id;
        ctrl.tableChangesJson = JSON.serialize(list1);
    	ctrl.getServiceCosts();
    	Test.stopTest();
    }
}