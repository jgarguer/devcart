public with sharing class BI_NEPageRedirect {
    
    public String ordId;
    public PageReference redirect()    {
        ordId                   =   ApexPages.currentPage().getParameters().get('orderId');
        String createOpty       =   ApexPages.currentPage().getParameters().get('createOpty');
        String siteId           =   ApexPages.currentPage().getParameters().get('site');
        String authorizedUserId =   ApexPages.currentPage().getParameters().get('authUser');
        String holdingId        =   ApexPages.currentPage().getParameters().get('holdingId');
        String oppId            =   ApexPages.currentPage().getParameters().get('oppId');
        Pagereference red;
        
        try   {
            NE__Order__c ord;
            if(ordId != null){
                ord    =   [SELECT id, NE__OrderStatus__c, NE__OpportunityId__c, NE__AccountId__c, NE__BillAccId__c, NE__ServAccId__c, NE__OptyId__c, RecordType.name,RecordType.DeveloperName, RecordTypeId, NE__Version__c, Site__c, Authorized_User__c, Business_Unit__c, Cost_Center__c FROM NE__Order__c WHERE id =: ordId];
            }else{
                ord    =   [SELECT id, NE__OrderStatus__c, NE__OpportunityId__c, NE__AccountId__c, NE__BillAccId__c, NE__ServAccId__c, NE__OptyId__c, RecordType.name,RecordType.DeveloperName, RecordTypeId, NE__Version__c, Site__c, Authorized_User__c, Business_Unit__c, Cost_Center__c FROM NE__Order__c WHERE NE__OptyId__c =: oppId AND NE__OrderStatus__c Like 'Activ%' ORDER BY CreatedDate DESC LIMIT 1];
                ordId = ord.id;
            }
            String optyId       =   ord.NE__OptyId__c;
            System.debug('try optyid: '+ord.NE__OptyId__c);
            System.debug('ord.RecordType.name'+ ord.RecordType.name);
            if(ord.RecordType.DeveloperName == 'Quote'){
                return new PageReference('/' + optyId);
            }
            if(createOpty != null)  {
                RecordType recType  =   [SELECT Id FROM RecordType WHERE (SobjectType = 'Order__c' OR SobjectType = 'NE__Order__c') AND Name = 'Opty' limit 1]; 
                optyId              =   BI_OpportunityMethods.createQuickOpp(ord.NE__AccountId__c, ord.id);
                system.debug('optyId new test:'+optyId );
                ord.RecordTypeId    =   recType.id;
                ord.NE__Version__c  =   1;
                ord.NE__OrderStatus__c      =   'F1 - Closed Won';
                ord.NE__OptyId__c   =   optyId;
                ord.NE__OpportunityId__c    =   optyId;                              
                update ord;             
            }           
            if(ord.RecordType.name == 'Opty' || createOpty != null)  {
                List<NE__OrderItem__c> ordit = [SELECT NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name, NE__OneTimeFeeOv__c, NE__RecurringChargeOv__c,NE__CatalogItem__r.NE__ProductId__r.BI_COT_MEX_Analisis_Economico__c, NE__BaseOneTimeFee__c, NE__BaseRecurringCharge__c FROM NE__OrderItem__c WHERE NE__OrderId__c =: ordId];
                Boolean isParque = false;
                for(NE__OrderItem__c oi : ordit)   {
                    if(oi.NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name == 'Parque Modification' || oi.NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name == 'Parque Baja') {
                        isParque = true;
                        break;
                    }
                }
                System.debug('isParque: '+isParque);
                if(isParque)
                {   
                    System.debug('orderoptyid: '+ ord.NE__OptyId__c);
                    
                    Opportunity opty = [SELECT Id, BI_Ciclo_ventas__c FROM Opportunity WHERE Id =: ord.NE__OptyId__c];
                    System.debug('opty id: '+ opty.Id);
                    opty.BI_Ciclo_ventas__c = 'Completo';
                    update opty;
                    
                    for(NE__OrderItem__c oi : ordit)  {
                        oi.Configuration_Type__c = 'New';
                    }
                    update ordit;
                    red =   new PageReference('/apex/BI_LightOptySummary?id='+ord.id);
                    if(UserInfo.getUiThemeDisplayed()!='Theme4t'){
                      red =   new PageReference('/'+optyId);
                    }
                }
                else
                    red =   new PageReference('/apex/BI_LightOptySummary?id='+optyId);
                    if(UserInfo.getUiThemeDisplayed()!='Theme4t'){
                      red =   new PageReference('/'+optyId);
                    }
            }
            else  {
                ord.HoldingId__c = holdingId;
                System.debug('holdingId: '+holdingId);
                if(siteId != null) {
                    list<NE__OrderItem__c> ordItToUpd = new list<NE__OrderItem__c>();
                    for(NE__OrderItem__c ordit : [SELECT Installation_point__c FROM NE__OrderItem__c WHERE NE__OrderId__c =: ord.Id])  {
                        ordit.Installation_point__c = siteId;                        
                        ordItToUpd.add(ordit);
                    }                
                    if(ordItToUpd.size() > 0) {update ordItToUpd;}
                    ord.Site__c = siteId;  
                }                   
                
                if(authorizedUserId != null){ ord.Authorized_User__c = authorizedUserId;}
                
                update ord;
                
                red =   new PageReference('/'+ord.Id);
            }               
            //GC TEST to run opty
            system.debug('TEST RUN');
            List<NE__OrderItem__c> ordit = [SELECT Id FROM NE__OrderItem__c WHERE NE__OrderId__c =: ordId];
            update ordit;
        }
        catch(Exception e)  {
            red =   new PageReference('/'+ordId);
            System.debug('mensaje: '+e.getMessage());
        }       
        return red;
    }

    /*----------------------------------------------------------------------------------------------------------------
    Author:         Alejandro Pantoja   
    Company:        Accenture NEA
    Description:    Method to check products for Mexico and FE. 

    History
    <Date>          <Author>            <Change Description>
    25/04/2017       Alejandro Pantoja           Initial Version
    08/05/2017       Angela Latorre              Add a custom field to store the prices NRC and RC and comparate 
                                                 them to know if you have to pass or not for economic feasibility
    24/05/2017       Guillermo Muñoz             Changed to enable bulk comparation
    ------------------------------------------------------------------------------------------------------------------*/

   public void checkEconomicFactiMex(List<NE__OrderItem__c> lstOrdIt, Opportunity opty)
    {

        List<Opportunity> lstOptyUpdate = new list<Opportunity>();
        List<NE__OrderItem__c> ordItToUpdate = new List<NE__OrderItem__c>();

        if(lstOrdIt.size()>0 && lstOrdIt != null){

            Boolean auxPricing = true;
            Integer auxCont = 0;

            for(NE__OrderItem__c aux : lstOrdIt){
                if(aux.NE__CatalogItem__r.NE__ProductId__r.BI_COT_MEX_Analisis_Economico__c){   

                    auxCont++;

                    if(aux.NE__OneTimeFeeOv__c != aux.NE__BaseOneTimeFee__c || aux.NE__RecurringChargeOv__c != aux.NE__BaseRecurringCharge__c){

                        auxPricing = false;
                    }
                }
            }
            //GMN 24/05/2017 - modified to enable bulk comparation
            if(auxPricing && auxCont > 0){

                opty.BI_No_requiere_Pricing_arg__c = true;
                lstOptyUpdate.add(opty);
            }
        }

        //ALM 08/05/2017
        if(!ordItToUpdate.isEmpty()){
            update ordItToUpdate;
        }

        // update opty with a BI_LoadAllKindQueable
        System.enqueueJob(new BI_LoadAllKindQueable(lstOptyUpdate, 1, 'UPDATE', 'Oportunity Update ' + Datetime.now()));
        //END ALM 08/05/2017
    }
}