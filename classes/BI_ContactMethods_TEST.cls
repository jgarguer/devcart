@isTest
private class BI_ContactMethods_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_ContactMethods class 

    History: 
    
    <Date>                     <Author>                <Change Description>
    04/12/2014                 Micah Burgos            Initial Version
    10/12/2015                 Fernando Arteaga        BI_EN - CSB Integration
    20/09/2017                 Angel F. Santaliestra   Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test to manage the coverage code for assignCountry class 

    History: 

     <Date>                     <Author>                <Change Description>
    04/12/2014                  Micah Burgos            Initial Version
    30/01/2017                  Marta Gonzalez         REING-INI-001 - REING-FIN-001: Updating mandatory contact data.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void assignCountry_Test() {
        /////////////////////////////////
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
            Integer K = 2;  // Number of Contacts
        /////////////////////////////////
        List<String> lst_pais = BI_Dataload.loadPaisFromPickList(1);
        List<Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1, lst_pais);

        //BI_Dataload.loadContacts(Integer i, LIST lst_acc);
        List<Contact> lst_contact = new List<Contact>();
        for(Integer j = 0; j < K; j++){
            Contact con = new Contact(LastName = 'test'+ String.valueOf(j),
                                      AccountId = lst_acc[0].Id,
                                      Email = 'test'+ String.valueOf(j)+'@gmail.com',
                                      HasOptedOutOfEmail = false,
                                      BI_Country__c = lst_pais[0],
                                      BI_Activo__c = true,
                                      /*REING-INI-001*/
                                      BI_Tipo_de_documento__c = 'Otros',
                                      BI_Numero_de_documento__c = '00000000X'
                                      /*REING-FIN-001*/);
            lst_contact.add(con);
        }


        Test.startTest();
        insert lst_contact;
        Test.stopTest();

        lst_contact = [SELECT ID, BI_Country__c FROM Contact];

        for(Contact con :lst_contact){
            system.assertEquals(lst_pais[0] , con.BI_Country__c);
        }
    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        
    Company:       Salesforce.com
    Description:   

    History: 

     <Date>                     <Author>                <Change Description>
    XX/XX/XXXX                         
    02/01/2017                  Marta Gonzalez          REING-01.Adaptacion para la reingenieria de contactos
    28/09/2017                  Julián Gawron           Phone and MobilePhone fields added with correct format
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void validateId_test() {
        /////////////////////////////////
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
            Integer K = 2;  // Number of Contacts. Only two records because the field "BI_Numero_de_documento__c" needs a real RUT
        /////////////////////////////////
        List<String> lst_valid_ruts = new List<String>{'153479070','23506345k'};
        map<String,String> map_paises = BI_DataLoad.loadPaises();
        List<Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1, new List<String>{Label.BI_Chile});

        List<Contact> lst_contact = new List<Contact>();
        for(Integer j = 0; j < K; j++){
            Contact con = new Contact(LastName = 'test'+ String.valueOf(j),
                                      AccountId = lst_acc[0].Id,
                                      Email = 'test'+ String.valueOf(j)+'@gmail.com',
                                      Phone = '556748397', // 28/09/2017
                                      MobilePhone = '789544221', // 28/09/2017
                                      HasOptedOutOfEmail = false,
                                      BI_Activo__c = true,
                                      BI_Tipo_de_documento__c = 'RUT',
                                      BI_Country__c = map_paises.get(Label.BI_Chile),
                                      BI_Numero_de_documento__c = lst_valid_ruts[j] , //Valid RUT
                                      //REING-01-INI
                                      MailingState='Santiago',
                                      FS_CORE_Codigo_Ciudad__c='123456',
                                      MailingCity= 'Santiago',
                                      MailingStreet= 'Urbinas',
                                      FS_CORE_MailingNumber__c = '2');
                                      //REING-01-INI
            lst_contact.add(con);
            
        }
        Test.startTest();
        //Try test
        try{
            insert lst_contact;
        }catch(Exception exc){
            system.assertEquals('Error','Exception: ' + exc.getMessage());
        }

        Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Test to manage the coverage code for checkOnlyOneCSBAdmin method 

    History: 

     <Date>                     <Author>                <Change Description>
    10/12/2015                 Fernando Arteaga         BI_EN - CSB Integration
    02/01/2017                  Marta Gonzalez          REING-01.Adaptacion para la reingenieria de contactos
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void testOnlyOneCSBAdmin()
    {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        Contact admin = new Contact(AccountId = lst_acc[0].Id,
                                    FirstName = 'Test',
                                    LastName = 'AdminCSB',
                                    BI_Tipo_de_contacto__c = 'Administrador CSB',
                                     //REING-01-INI
                                    BI_Country__c='Argentina',
                                    FS_CORE_Acceso_a_Portal_CSB__c = true,
                                    BI_Tipo_de_documento__c = 'Otros',
                                    BI_Numero_de_documento__c = '00000000X',
                                    //REING-01-FIN
                                    Email = 'testmethod@neccloudhub.com');

        Contact admin2 = new Contact(AccountId = lst_acc[0].Id,
                                    FirstName = 'Test2',
                                    LastName = 'AdminCSB',
                                    BI_Tipo_de_contacto__c = 'Administrador CSB',
                                    //REING-01-INI 
                                    BI_Country__c='Argentina',                                   
                                    FS_CORE_Acceso_a_Portal_CSB__c = true,
                                    BI_Tipo_de_documento__c = 'Otros',
                                    BI_Numero_de_documento__c = '00000000X',
                                    //REING-01-FIN
                                    Email = 'testmethod2@neccloudhub.com');
        
        insert admin;
        
        Test.startTest();
        
        try {       
            insert admin2;
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains(Label.BI_CSB_Un_Administrador));
        }
        
        try {       
            BI_ContactMethods.checkOnlyOneCSBAdmin(null);
        }
        catch (Exception e) {
            System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
        }
        
        Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Eduardo Ventura
    Company:       Everis España
    Description:   Clase de prueba: BI_ContactMethods (insertToFullstack)
    
    History:
    
    <Date>					<Author>					<Description>
    24/04/2017				Eduardo Ventura				Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void insertToFullstack() {
        /* Enable Bypass */
        BI_bypass__c bypass = new BI_bypass__c();
        bypass.BI_migration__c = true;
        bypass.BI_skip_trigger__c = true;
        bypass.BI_stop_job__c = true;
        bypass.BI_skip_assert__c = true;
        bypass.BI_Skip_case_assignations__c = true;
        
        insert bypass;
        
        /* Integrations */
        FS_CORE_Integracion__c ORGConfiguration = new FS_CORE_Integracion__c();
        ORGConfiguration.FS_CORE_Segmentos_Cliente__c = 'Top 1 Top 2 Top 3 Top 4 Platino';
        ORGConfiguration.FS_CORE_AccountTeamMember__c = 'Ejecutivo de Cobros Gestor de Reclamos';
        ORGConfiguration.FS_CORE_Autorizaciones_Contacto__c = 'Administrativo Cobranza Facturación General PostVenta Técnico';
        ORGConfiguration.FS_CORE_Peru__c = true;
        ORGConfiguration.FS_CORE_Chile__c = false;
        ORGConfiguration.FS_CORE_SrvName_AddContactToOrganization__c = 'Salesforce.AddContactToOrganization';
        ORGConfiguration.FS_CORE_SrvName_AddUserToOrganization__c = 'Salesforce.AddUserToOrganization';
        ORGConfiguration.FS_CORE_SrvName_CreateContact__c ='Salesforce.CreateContact';
        ORGConfiguration.FS_CORE_SrvName_CreateCustomer__c ='Salesforce.CreateCustomer';
        ORGConfiguration.FS_CORE_SrvName_GetCustomerList__c ='Salesforce.GetCustomerList';
        ORGConfiguration.FS_CORE_SrvName_RemoveContactFromOrg__c ='Salesforce.RemoveContactFromOrg';
        ORGConfiguration.FS_CORE_SrvName_RemoveUserOrganization__c = 'Salesforce.RemoveUserOrganization';
        ORGConfiguration.FS_CORE_SrvName_UpdateCustomer__c = 'Salesforce.UpdateCustomer';
        ORGConfiguration.FS_CORE_VersionCabecera__c = '1.0';        
        insert ORGConfiguration;
        
        /* Account */
        Account account = new Account(Name = 'Test [Integración] - Perú', Description = 'null', BI_Activo__c = 'Sí',
                                      BI_Segment__c = 'Empresas', BI_Subsegment_Local__c = 'Top 1', BI_Riesgo__c = null,
                                      BI_Denominacion_comercial__c = 'Test [Integración] - Perú [Denominación comercial]', BI_Sector__c = 'Comercio', TGS_Es_MNC__c = false,
                                      BI_Ambito__c = 'Público', BI_Subsector__c = 'Droguerías', Website = 'www.cliente.pe',
                                      TGS_Fecha_Activacion__c = Date.today(), TGS_Deactivation_Date__c = Date.today(), FS_CHI_Tipo_Credito__c = 'Otras',
                                      FS_CHI_CREDIT_CLASS__c = 'PLUS', FS_CHI_Credit_Score__c = 0, FS_CHI_Limite_de_consumo__c = 0,
                                      FS_CHI_Limite_de_credito__c = 0, FS_CHI_Limite_credito_portabilidad__c = 0, FS_CHI_Fecha_de_ultima_ev__c = Date.today(),
                                      BI_Country__c = 'Peru', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20552003609',
                                      Phone = '6895623147', Fax = '123456789', FS_CORE_Account_Massive__c = false,BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test',
                                      FS_CORE_ATM_Massive__c = false, RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' LIMIT 1][0].Id, FS_CORE_ID_Cliente_legado__c = '000000000X',
                                      FS_CORE_Estado_Sincronizacion__c = 'Sincronizado');
        
        insert account;
        
        /* Opportunity */
        Opportunity opportunity = new Opportunity(Name = 'Test [Integración] - Perú', AccountId = account.Id, BI_Country__c = 'Peru',
                                                  BI_SIMP_Opportunity_Type__c = 'Alta', BI_Licitacion__c = 'No', BI_Opportunity_Type__c = 'Fijo',
                                                  CloseDate = Date.today(), BI_Fecha_de_cierre_real__c = Date.today(), StageName = 'F1 - Closed Won',
                                                  BI_Probabilidad_de_exito__c = '100', BI_Duracion_del_contrato_Meses__c = 10, BI_Plazo_estimado_de_provision_dias__c = 10,
                                                  BI_Fecha_de_entrega_de_la_oferta__c = Date.today(), BI_Fecha_de_vigencia__c = Date.today(), BI_Fecha_vigencia_factibilidad_tecnica__c = Date.today(),
                                                  RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Ciclo_completo' LIMIT 1][0].Id);
        
        insert opportunity;
        
        /* Contact */
        Contact contact = new Contact(FirstName = 'Integración', LastName = 'Perú', BI_Activo__c = true,
                                      BI_Tipo_de_contacto__c = 'Administrativo', BI_Representante_legal__c = true, Phone = '000000000',
                                      AssistantPhone = '000000000', HomePhone = '000000000', MobilePhone = '000000000',
                                      OtherPhone = '000000000', Fax = '000000000', email = 'integraciones@integraciones.test',
                                      BI_Country__c = 'Peru', BI_Tipo_de_documento__c = 'Otros', BI_Numero_de_documento__c = '0000000X',
                                      Birthdate = Date.today(), BI_Genero__c = 'Masculino', accountId = account.Id,
                                      RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Active_Contact' LIMIT 1][0].Id);
        
        insert contact;
        
        /* Test */
        System.Test.startTest();
        
        List<Contact> contacts = new list<Contact>();
        contacts.add([SELECT Id, FirstName, LastName, BI_Activo__c, BI_Tipo_de_contacto__c, BI_Representante_legal__c, Phone,
                      AssistantPhone, HomePhone, MobilePhone, OtherPhone, Fax, email, BI_Country__c,
                      BI_Tipo_de_documento__c, BI_Numero_de_documento__c, Birthdate, BI_Genero__c, accountId, FS_CORE_ISO_2_Country__c, RecordTypeId,
                      BI_Id_del_contacto__c, FS_CORE_Estado_de_Sincronizacion__c FROM Contact LIMIT 1][0]);
        
        BI_ContactMethods.insertToFullstack(null, contacts);
        
        System.Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Eduardo Ventura
    Company:       Everis España
    Description:   Clase de prueba: BI_ContactMethods (updateToFullstack)
    
    History:
    
    <Date>					<Author>					<Description>
    24/04/2017				Eduardo Ventura				Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void updateToFullstack() {
        /* Enable Bypass */
        BI_bypass__c bypass = new BI_bypass__c();
        bypass.BI_migration__c = true;
        bypass.BI_skip_trigger__c = true;
        bypass.BI_stop_job__c = true;
        bypass.BI_skip_assert__c = true;
        bypass.BI_Skip_case_assignations__c = true;
        
        insert bypass;
        
        /* Integrations */
        FS_CORE_Integracion__c ORGConfiguration = new FS_CORE_Integracion__c();
        ORGConfiguration.FS_CORE_Segmentos_Cliente__c = 'Top 1 Top 2 Top 3 Top 4 Platino';
        ORGConfiguration.FS_CORE_AccountTeamMember__c = 'Ejecutivo de Cobros Gestor de Reclamos';
        ORGConfiguration.FS_CORE_Autorizaciones_Contacto__c = 'Administrativo Cobranza Facturación General PostVenta Técnico';
        ORGConfiguration.FS_CORE_Peru__c = true;
        ORGConfiguration.FS_CORE_Chile__c = false;
        ORGConfiguration.FS_CORE_SrvName_AddContactToOrganization__c = 'Salesforce.AddContactToOrganization';
        ORGConfiguration.FS_CORE_SrvName_AddUserToOrganization__c = 'Salesforce.AddUserToOrganization';
        ORGConfiguration.FS_CORE_SrvName_CreateContact__c ='Salesforce.CreateContact';
        ORGConfiguration.FS_CORE_SrvName_CreateCustomer__c ='Salesforce.CreateCustomer';
        ORGConfiguration.FS_CORE_SrvName_GetCustomerList__c ='Salesforce.GetCustomerList';
        ORGConfiguration.FS_CORE_SrvName_RemoveContactFromOrg__c ='Salesforce.RemoveContactFromOrg';
        ORGConfiguration.FS_CORE_SrvName_RemoveUserOrganization__c = 'Salesforce.RemoveUserOrganization';
        ORGConfiguration.FS_CORE_SrvName_UpdateCustomer__c = 'Salesforce.UpdateCustomer';
        ORGConfiguration.FS_CORE_VersionCabecera__c = '1.0';          
        insert ORGConfiguration;
        
        /* Account */
        Account account = new Account(Name = 'Test [Integración] - Perú', Description = 'null', BI_Activo__c = 'Sí',
                                      BI_Segment__c = 'Empresas', BI_Subsegment_Local__c = 'Top 1', BI_Riesgo__c = null,
                                      BI_Denominacion_comercial__c = 'Test [Integración] - Perú [Denominación comercial]', BI_Sector__c = 'Comercio', TGS_Es_MNC__c = false,
                                      BI_Ambito__c = 'Público', BI_Subsector__c = 'Droguerías', Website = 'www.cliente.pe',
                                      TGS_Fecha_Activacion__c = Date.today(), TGS_Deactivation_Date__c = Date.today(), FS_CHI_Tipo_Credito__c = 'Otras',
                                      FS_CHI_CREDIT_CLASS__c = 'PLUS', FS_CHI_Credit_Score__c = 0, FS_CHI_Limite_de_consumo__c = 0,
                                      FS_CHI_Limite_de_credito__c = 0, FS_CHI_Limite_credito_portabilidad__c = 0, FS_CHI_Fecha_de_ultima_ev__c = Date.today(),
                                      BI_Country__c = 'Peru', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20552003609',
                                      Phone = '6895623147', Fax = '123456789', FS_CORE_Account_Massive__c = false,BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test',
                                      FS_CORE_ATM_Massive__c = false, RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' LIMIT 1][0].Id, FS_CORE_ID_Cliente_legado__c = '000000000X',
                                      FS_CORE_Estado_Sincronizacion__c = 'Sincronizado');
        
        Account account_extra = new Account(Name = 'Test [Integración] - Perú - Extra', Description = 'null', BI_Activo__c = 'Sí',
                                      BI_Segment__c = 'Empresas', BI_Subsegment_Local__c = 'Top 1', BI_Riesgo__c = null,
                                      BI_Denominacion_comercial__c = 'Test [Integración] - Perú [Denominación comercial]', BI_Sector__c = 'Comercio', TGS_Es_MNC__c = false,
                                      BI_Ambito__c = 'Público', BI_Subsector__c = 'Droguerías', Website = 'www.cliente.pe',
                                      TGS_Fecha_Activacion__c = Date.today(), TGS_Deactivation_Date__c = Date.today(), FS_CHI_Tipo_Credito__c = 'Otras',
                                      FS_CHI_CREDIT_CLASS__c = 'PLUS', FS_CHI_Credit_Score__c = 0, FS_CHI_Limite_de_consumo__c = 0,
                                      FS_CHI_Limite_de_credito__c = 0, FS_CHI_Limite_credito_portabilidad__c = 0, FS_CHI_Fecha_de_ultima_ev__c = Date.today(),
                                      BI_Country__c = 'Peru', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20552003600',
                                      Phone = '6895623147', Fax = '123456789', FS_CORE_Account_Massive__c = false,BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test',
                                      FS_CORE_ATM_Massive__c = false, RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' LIMIT 1][0].Id, FS_CORE_ID_Cliente_legado__c = '000000000Y',
                                      FS_CORE_Estado_Sincronizacion__c = 'Sincronizado');
        
        insert account;
        insert account_extra;
        
        /* Opportunity */
        Opportunity opportunity = new Opportunity(Name = 'Test [Integración] - Perú', AccountId = account.Id, BI_Country__c = 'Peru',
                                                  BI_SIMP_Opportunity_Type__c = 'Alta', BI_Licitacion__c = 'No', BI_Opportunity_Type__c = 'Fijo',
                                                  CloseDate = Date.today(), BI_Fecha_de_cierre_real__c = Date.today(), StageName = 'F1 - Closed Won',
                                                  BI_Probabilidad_de_exito__c = '100', BI_Duracion_del_contrato_Meses__c = 10, BI_Plazo_estimado_de_provision_dias__c = 10,
                                                  BI_Fecha_de_entrega_de_la_oferta__c = Date.today(), BI_Fecha_de_vigencia__c = Date.today(), BI_Fecha_vigencia_factibilidad_tecnica__c = Date.today(),
                                                  RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Ciclo_completo' LIMIT 1][0].Id);
        
        Opportunity opportunity_extra = new Opportunity(Name = 'Test [Integración] - Perú', AccountId = account.Id, BI_Country__c = 'Peru',
                                                  BI_SIMP_Opportunity_Type__c = 'Alta', BI_Licitacion__c = 'No', BI_Opportunity_Type__c = 'Fijo',
                                                  CloseDate = Date.today(), BI_Fecha_de_cierre_real__c = Date.today(), StageName = 'F1 - Closed Won',
                                                  BI_Probabilidad_de_exito__c = '100', BI_Duracion_del_contrato_Meses__c = 10, BI_Plazo_estimado_de_provision_dias__c = 10,
                                                  BI_Fecha_de_entrega_de_la_oferta__c = Date.today(), BI_Fecha_de_vigencia__c = Date.today(), BI_Fecha_vigencia_factibilidad_tecnica__c = Date.today(),
                                                  RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Ciclo_completo' LIMIT 1][0].Id);
        
        insert opportunity;
        insert opportunity_extra;
        
        /* Contact */
        Contact contact = new Contact(FirstName = 'Integración', LastName = 'Perú', BI_Activo__c = true,
                                      BI_Tipo_de_contacto__c = 'Administrativo', BI_Representante_legal__c = true, Phone = '000000000',
                                      AssistantPhone = '000000000', HomePhone = '000000000', MobilePhone = '000000000',
                                      OtherPhone = '000000000', Fax = '000000000', email = 'integraciones@integraciones.test',
                                      BI_Country__c = 'Peru', BI_Tipo_de_documento__c = 'Otros', BI_Numero_de_documento__c = '0000000X',
                                      Birthdate = Date.today(), BI_Genero__c = 'Masculino', accountId = account.Id,
                                      RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Active_Contact' LIMIT 1][0].Id);
        
        insert contact;
        
        /* Test */
        System.Test.startTest();
        
        List<Contact> oldContacts = new list<Contact>();
        oldContacts.add([SELECT Id, FirstName, LastName, BI_Activo__c, BI_Tipo_de_contacto__c, BI_Representante_legal__c, Phone,
                         AssistantPhone, HomePhone, MobilePhone, OtherPhone, Fax, email, BI_Country__c,
                         BI_Tipo_de_documento__c, BI_Numero_de_documento__c, Birthdate, BI_Genero__c, accountId, FS_CORE_ISO_2_Country__c, RecordTypeId,
                         BI_Id_del_contacto__c, FS_CORE_Estado_de_Sincronizacion__c FROM Contact LIMIT 1][0]);
        
        List<Contact> newContacts = new list<Contact>();
        
        /* Field #1 */
        contact.BI_Numero_de_documento__c = '0000000X';
        update contact;
        
        newContacts.add(contact);
        
        BI_ContactMethods.updateToFullstack(oldContacts, newContacts);
        
        /* Field #2 */
        newContacts.clear();
        contact.AccountId = account_extra.Id;
        update contact;
        
        newContacts.add(contact);
        
        BI_ContactMethods.updateToFullstack(oldContacts, newContacts);
        
        System.Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Eduardo Ventura
    Company:       Everis España
    Description:   Clase de prueba: BI_ContactMethods (deleteFromFullstack)
    
    History:
    
    <Date>					<Author>					<Description>
    24/04/2017				Eduardo Ventura				Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void deleteFromFullstack() {
        /* Enable Bypass */
        BI_bypass__c bypass = new BI_bypass__c();
        bypass.BI_migration__c = true;
        bypass.BI_skip_trigger__c = true;
        bypass.BI_stop_job__c = true;
        bypass.BI_skip_assert__c = true;
        bypass.BI_Skip_case_assignations__c = true;
        
        insert bypass;
        
        /* Integrations */
        FS_CORE_Integracion__c ORGConfiguration = new FS_CORE_Integracion__c();
        ORGConfiguration.FS_CORE_Segmentos_Cliente__c = 'Top 1 Top 2 Top 3 Top 4 Platino';
        ORGConfiguration.FS_CORE_AccountTeamMember__c = 'Ejecutivo de Cobros Gestor de Reclamos';
        ORGConfiguration.FS_CORE_Autorizaciones_Contacto__c = 'Administrativo Cobranza Facturación General PostVenta Técnico';
        ORGConfiguration.FS_CORE_Peru__c = true;
        ORGConfiguration.FS_CORE_Chile__c = false;
        ORGConfiguration.FS_CORE_SrvName_AddContactToOrganization__c = 'Salesforce.AddContactToOrganization';
        ORGConfiguration.FS_CORE_SrvName_AddUserToOrganization__c = 'Salesforce.AddUserToOrganization';
        ORGConfiguration.FS_CORE_SrvName_CreateContact__c ='Salesforce.CreateContact';
        ORGConfiguration.FS_CORE_SrvName_CreateCustomer__c ='Salesforce.CreateCustomer';
        ORGConfiguration.FS_CORE_SrvName_GetCustomerList__c ='Salesforce.GetCustomerList';
        ORGConfiguration.FS_CORE_SrvName_RemoveContactFromOrg__c ='Salesforce.RemoveContactFromOrg';
        ORGConfiguration.FS_CORE_SrvName_RemoveUserOrganization__c = 'Salesforce.RemoveUserOrganization';
        ORGConfiguration.FS_CORE_SrvName_UpdateCustomer__c = 'Salesforce.UpdateCustomer';
        ORGConfiguration.FS_CORE_VersionCabecera__c = '1.0';  
        insert ORGConfiguration;
        
        /* Account */
        Account account = new Account(Name = 'Test [Integración] - Perú', Description = 'null', BI_Activo__c = 'Sí',
                                      BI_Segment__c = 'Empresas', BI_Subsegment_Local__c = 'Top 1', BI_Riesgo__c = null,
                                      BI_Denominacion_comercial__c = 'Test [Integración] - Perú [Denominación comercial]', BI_Sector__c = 'Comercio', TGS_Es_MNC__c = false,
                                      BI_Ambito__c = 'Público', BI_Subsector__c = 'Droguerías', Website = 'www.cliente.pe',
                                      TGS_Fecha_Activacion__c = Date.today(), TGS_Deactivation_Date__c = Date.today(), FS_CHI_Tipo_Credito__c = 'Otras',
                                      FS_CHI_CREDIT_CLASS__c = 'PLUS', FS_CHI_Credit_Score__c = 0, FS_CHI_Limite_de_consumo__c = 0,
                                      FS_CHI_Limite_de_credito__c = 0, FS_CHI_Limite_credito_portabilidad__c = 0, FS_CHI_Fecha_de_ultima_ev__c = Date.today(),
                                      BI_Country__c = 'Peru', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20552003609',
                                      Phone = '6895623147', Fax = '123456789', FS_CORE_Account_Massive__c = false,BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test',
                                      FS_CORE_ATM_Massive__c = false, RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' LIMIT 1][0].Id, FS_CORE_ID_Cliente_legado__c = '000000000X',
                                      FS_CORE_Estado_Sincronizacion__c = 'Sincronizado');
        
        insert account;
        
        /* Opportunity */
        Opportunity opportunity = new Opportunity(Name = 'Test [Integración] - Perú', AccountId = account.Id, BI_Country__c = 'Peru',
                                                  BI_SIMP_Opportunity_Type__c = 'Alta', BI_Licitacion__c = 'No', BI_Opportunity_Type__c = 'Fijo',
                                                  CloseDate = Date.today(), BI_Fecha_de_cierre_real__c = Date.today(), StageName = 'F1 - Closed Won',
                                                  BI_Probabilidad_de_exito__c = '100', BI_Duracion_del_contrato_Meses__c = 10, BI_Plazo_estimado_de_provision_dias__c = 10,
                                                  BI_Fecha_de_entrega_de_la_oferta__c = Date.today(), BI_Fecha_de_vigencia__c = Date.today(), BI_Fecha_vigencia_factibilidad_tecnica__c = Date.today(),
                                                  RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Ciclo_completo' LIMIT 1][0].Id);
        
        insert opportunity;
        
        /* Contact */
        Contact contact = new Contact(FirstName = 'Integración', LastName = 'Perú', BI_Activo__c = true,
                                      BI_Tipo_de_contacto__c = 'Administrativo', BI_Representante_legal__c = true, Phone = '000000000',
                                      AssistantPhone = '000000000', HomePhone = '000000000', MobilePhone = '000000000',
                                      OtherPhone = '000000000', Fax = '000000000', email = 'integraciones@integraciones.test',
                                      BI_Country__c = 'Peru', BI_Tipo_de_documento__c = 'Otros', BI_Numero_de_documento__c = '0000000X',
                                      Birthdate = Date.today(), BI_Genero__c = 'Masculino', accountId = account.Id,
                                      RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Active_Contact' LIMIT 1][0].Id, BI_Id_del_contacto__c = '00000000X', FS_CORE_Estado_de_Sincronizacion__c = 'Sincronizado');
        
        insert contact;
        
        /* Test */
        System.Test.startTest();
        
        List<Contact> contacts = new list<Contact>();
        contacts.add([SELECT Id, FirstName, LastName, BI_Activo__c, BI_Tipo_de_contacto__c, BI_Representante_legal__c, Phone,
                      AssistantPhone, HomePhone, MobilePhone, OtherPhone, Fax, email, BI_Country__c,
                      BI_Tipo_de_documento__c, BI_Numero_de_documento__c, Birthdate, BI_Genero__c, accountId, FS_CORE_ISO_2_Country__c, RecordTypeId,
                      BI_Id_del_contacto__c, FS_CORE_Estado_de_Sincronizacion__c FROM Contact LIMIT 1][0]);
        
        BI_ContactMethods.deleteFromFullstack(contacts, null);
        
        System.Test.stopTest();
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture - New Energy Aborda
    Description:   Test method to manage the coverage code for BI_ContactMethods.sendConvertedContactToFullStack()
  
    History:
  
    <Date>            <Author>              <Description>
    28/09/2017        Guillermo Muñoz       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    @isTest static void sendConvertedContactToFullStack_TEST(){

        BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
    
        Account oAccount = new Account();
    
        oAccount.Name = 'TEST 2';
        oAccount.BI_Identificador_Externo__c = 'ACC_123456789';
        oAccount.RecordtypeId =TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
        oAccount.TGS_Es_MNC__c = true;
    
        insert oAccount;
    
        Contact oContact = new Contact();
    
        oContact.AccountId = oAccount.id;
        oContact.BirthDate = Date.valueof('2016-02-08');
        oContact.TGS_Language__c = 'ES';
        oContact.Email = 'test@test.com';
        oContact.Phone = '976123456';
        oContact.Fax = '976654321';
        oContact.Department = 'TEST DEPARMENT';
        oContact.FirstName = 'FIRST_NAME';
        oContact.LastName = 'LAST_NAME';
        oContact.Salutation = 'Mr';
        oContact.BI_Genero__c = 'male';
        oContact.Bi_Activo__c = true;
        oContact.FS_TGS_Id_del_prospecto__c = 'TestId';
        

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Location', 'TestResponse');
    
        Contact con;
        HttpResponse res;
        //BI_MigrationHelper.cleanSkippedTriggers();

        Test.startTest();
    
        Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));
        insert oContact;

        //Desactivación del desarrollo
        BI_MigrationHelper.cleanSkippedTriggers();
        BI_ContactMethods.sendConvertedContactToFullStack(new List <Contact>{oContact});
        //Fin desactivacion del desarrollo

        Test.stopTest();

        con = [SELECT Id, FS_TGS_Id_Contacto_FullStack__c FROM Contact WHERE Id =: oContact.Id LIMIT 1];
        System.assertEquals(con.FS_TGS_Id_Contacto_FullStack__c, 'TestResponse');
    }
}