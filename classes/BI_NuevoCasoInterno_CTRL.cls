public with sharing class BI_NuevoCasoInterno_CTRL {

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Guillermo Muñoz
 Company:       Accenture
 Description:   Redirect to new Case page
 Test Class:    BI_NuevoCasoInterno_CTRL_TEST
 
 History:
  
 <Date>              <Author>                   <Change Description>
 05/10/2017          Guillermo Muñoz            Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@testVisible private final Id optyId;

    public BI_NuevoCasoInterno_CTRL(ApexPages.StandardSetController controller) {
        optyId = System.currentPagereference().getParameters().get('Id');
    }

    public PageReference redirect(){

        Opportunity opp = [SELECT Id, AccountId, Name, BI_Country__c FROM Opportunity WHERE ID =: optyId];

        String url = '/' + Schema.sObjectType.Case.getKeyPrefix() + '/e';
        PageReference retPage = new PageReference(url);

        if(opp != null){
            Id rtId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' and DeveloperName = 'BI_Caso_Interno' LIMIT 1].Id;

            Map<String, String> mapFieldId = BI_DynamicFieldId.getDynamicField('Case', new Set <String>{'BI_Country__c', 'BI_Nombre_de_la_Oportunidad__c'}, rtId);
            
            retPage.getParameters().put('RecordType', rtId);
            retPage.getParameters().put('def_account_id', opp.AccountId);

            for (String fieldName : mapFieldId.keySet())
            {
                if (fieldName.equals('BI_Nombre_de_la_Oportunidad__c'))
                {
                    retPage.getParameters().put( mapFieldId.get(fieldName), opp.Name);
                    retPage.getParameters().put( mapFieldId.get(fieldName) + '_lkid', opp.Id);
                }
                if (fieldName.equals('BI_Country__c'))
                {
                    retPage.getParameters().put(mapFieldId.get(fieldName), opp.BI_Country__c);
                }
            }
            retPage.getParameters().put('nooverride','1');
            retPage.getParameters().put('retURL', opp.Id);
            retPage.setRedirect(true);
        }

        return retPage;
    }
}