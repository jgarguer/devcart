public with sharing class eventWrapper {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ana Escrich
	    Company:       Salesforce.com
	    Description:   Wrapper with event data to show in Portal Platino
	    
	    History:
	    
	    <Date>            <Author>          	<Description>
	    22/07/2014        Ana Escrich      	 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public String subject		{get;set;}
	public String description	{get;set;}
	public DateTime startTime	{get;set;}
	public DateTime finishTime	{get;set;}
	public Boolean isOwner		{get;set;}
	public Boolean isVisible	{get;set;}
	public Boolean isEditable	{get;set;}
	public Id eventId			{get;set;}
	public eventWrapper(string subj, string descr, DateTime start, DateTime finisht, boolean isOwner){
		this.subject = subj;
		this.description = descr==null?'':descr;
		this.startTime = start;
		this.finishTime = finisht;
		this.isEditable = false;
		this.isVisible = false;
	}
	public void setEventId(Id eventId){
		this.eventId = eventId;
	}
	
	public void setVisible(boolean value){
		this.isVisible = value;
	}
	
	public void setOwner(boolean value){
		this.isOwner = value;
	}
	public void setEditable(boolean value){
		this.isEditable = value;
	}
}