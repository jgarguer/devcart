/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Jesús Párraga Bórnez
    Company:       New Energy Aborda
    Description:   Methods executed by BI_O4 Opportunity Triggers
    Test Class:    BI_O4_QuoteHelper_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    21/09/2016              Pedro Párraga            Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


@isTest
private class BI_O4_QuoteHelper_TEST {

	static{
        BI_TestUtils.throw_exception = false;
    }
	
	@isTest static void getNextQuoteName() {

		String name = null;

		Test.startTest();
		BI_O4_QuoteHelper.getNextQuoteName(name);
		Test.stopTest();
		
	}

	@isTest static void getNextQuoteNameDos() {

		String name = 'quote';

	    Opportunity opp = new Opportunity(Name = name + system.now(),
                      					  CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                      					  BI_Ciclo_ventas__c = Label.BI_Completo  
                      					  );

	    insert opp;

 	    Quote quote = new Quote(Name = 'quote-001', Status = 'Working on proposal', OpportunityId = opp.Id);
	   	insert quote; 
	     		

		Test.startTest();
		BI_O4_QuoteHelper.getNextQuoteName(name);
		Test.stopTest();
		
	}	
	
	@isTest static void getNextQuoteScenario() {

	    Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                      					  CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                      					  BI_Ciclo_ventas__c = Label.BI_Completo  
                      					  );

	    insert opp;

 	    Quote quote = new Quote(Name = 'Quote_01', Status = 'Working on proposal', OpportunityId = opp.Id);


	   
	    insert quote;   


	     BI_O4_Solution__c sol = new BI_O4_Solution__c(BI_O4_Quote__c = quote.Id);
	     BI_O4_Proposal_Item__c pI = new BI_O4_Proposal_Item__c(BI_O4_Quote__c = quote.Id, CurrencyIsoCode = 'ARS', BI_O4_SubOpportunity__c = opp.Id);
	     insert sol;
	     insert pI;

	    Decimal numberD = 12.5;

		Test.startTest();
		BI_O4_QuoteHelper.getNextQuoteScenario(quote.OpportunityId);
		BI_O4_QuoteHelper.cloneQuote(quote.Id);
		BI_O4_QuoteHelper.ChangeForceGates(quote.Id);
		BI_O4_QuoteHelper.getNextQuoteScenarioCloned(quote.OpportunityId, numberD);
		Test.stopTest();
			                      					 		
	}

	
}