@isTest
private class PCA_HelpGroupController_TEST {

    static testMethod void getloadInfo() {
      
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
      User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());      
      system.runAs(usr){
       //List<Region__c> lst_pais = BI_DataLoad.loadPais(2);
          
          List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(2);
          
          List<Account> accounts = BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais);
           List<Account> accList = new List<Account>();
          for(Account item: accounts){
                item.OwnerId = usr.Id;
                accList.add(item);
          }
          update accList;
          List<Contact> con = BI_DataLoad.loadPortalContacts(1, accounts);
      User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());
      
      PCA_HelpGroupController constructor = new PCA_HelpGroupController();
          system.runAs(user1){ 
            BI_TestUtils.throw_exception = false;
          constructor.checkPermissions();  
        } 
      PCA_HelpGroupController constructor2 = new PCA_HelpGroupController();
        system.runAs(user1){ 
            BI_TestUtils.throw_exception = true;
          constructor2.checkPermissions();    
        } 
      }
  }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}