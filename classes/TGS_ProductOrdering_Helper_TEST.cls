@isTest
public class TGS_ProductOrdering_Helper_TEST {
	@isTest
	public static void testMethods(){
		/*Test.startTest();
		String json=getJSON();

		 Map<String,String> mapRTsTEST=new Map<String,String>();
        for(RecordType rt:[SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName IN ('Asset', 'Order', 'Order_Management_Case','TGS_Legal_Entity','TGS_Business_Unit','TGS_Cost_Center') LIMIT 5]){
            mapRTsTEST.put(rt.DeveloperName,rt.id);
    	}  
		//RecordType-&-Account 
		Account d=new Account();
		d.Name='TestAccParentLegalEntity';
		d.RecordTypeId=mapRTsTEST.get('TGS_Legal_Entity');
		insert d;

		Account c=new Account();
		c.Name='TestAccParentLegalEntity';
		c.ParentId=d.id;
		c.RecordTypeId=mapRTsTEST.get('TGS_Business_Unit');
		insert c;

		Account a=new Account();
		a.Name='TestAcc';
		a.ParentId=c.id;
		a.TGS_IntegrationID__c='ACCFRB0001';
		a.RecordTypeId=mapRTsTEST.get('TGS_Cost_Center');
		a.TGS_Aux_Legal_Entity__c=d.id;
		insert a;
		//contract
		NE__Contract_Header__c ch=new NE__Contract_Header__c();
		ch.NE__Name__c='Test ContractHeader';
		insert ch;
		NE__Contract_Account_Association__c caa=new NE__Contract_Account_Association__c();
		caa.NE__Contract_Header__c=ch.id;
		caa.NE__Account__c=d.id;
		insert caa;
		//acc for the site
		Account b=new Account();
		b.Name='TestAcc';
		b.ParentId=d.id;
		b.RecordTypeId=mapRTsTEST.get('TGS_Business_Unit');
		insert b;
		//sede
		BI_Sede__c sede=new BI_Sede__c();
		sede.Name='TestSede';
		sede.BI_Direccion__c='TestDi';
		insert sede;
		//site
		BI_Punto_de_Instalacion__c pdi=new BI_Punto_de_Instalacion__c();
		pdi.TEMPEXT_ID__c='S0001';
		pdi.Name='Site';
		pdi.BI_Cliente__c=b.id;
		pdi.BI_Sede__c=sede.id;
		insert pdi;
		//catalog
		NE__Catalog__c catalog=new NE__Catalog__c ();
		catalog.NE__Source_Catalog_Id__c='a0Bg00000043bE8EAI';
		catalog.Name='TGSOL Catalog';
		insert catalog;


		// *********************************
        // Catalog test data - START
        // *********************************
        NE__Catalog_Header__c catHead = new NE__Catalog_Header__c(Name='Test Catalog', 
                                                                    NE__Name__c='Test Catalog');
        insert catHead;
       
        NE__Catalog__c  cat = new NE__Catalog__c(Name='Test Cat',
                                                NE__Catalog_Header__c=catHead.Id, 
                                                NE__StartDate__c=Datetime.now(), 
                                                NE__Active__c = true);
        insert cat;
        
        NE__Catalog_Category__c catCat = new NE__Catalog_Category__c(Name='Test Category', 
                                                                    NE__CatalogId__c=cat.Id);
        insert catCat;

        NE__Commercial_Model__c comModel = new NE__Commercial_Model__c(Name='Test Commercial Model',
                                                                        NE__Catalog_Header__c=catHead.Id);
        insert comModel;

        NE__Family__c family    =   new NE__Family__c(name = 'Test Family');
        insert family;
        
        RecordType RTProd = [SELECT Id FROM RecordType WHERE (SobjectType = 'NE__Product__c' OR SobjectType = 'Product__c') LIMIT 1];
        NE__Product__c prod1 = new NE__Product__c(Name='SIM Card Suscription', RecordTypeId=RTProd.Id,NE__Source_Product_Id__c='a0ig0000002ObtnAAC');
        insert prod1;
        NE__Product__c prod2 = new NE__Product__c(Name='Suscription', RecordTypeId=RTProd.Id,NE__Source_Product_Id__c='a0ig0000002ObulAAC');
        insert prod2;
        NE__Product__c prod3 = new NE__Product__c(Name='Test Product 3', RecordTypeId=RTProd.Id,NE__Source_Product_Id__c='a0ig0000002ObtnAAC');
        insert prod3;       
        
        
        List<NE__ProductFamily__c> listProdFamily = New List<NE__ProductFamily__c>();
        NE__ProductFamily__c prodFamily =   new NE__ProductFamily__c(NE__ProdId__c = prod1.id, NE__FamilyId__c = family.id);
        listProdFamily.add(prodFamily);
        prodFamily  =   new NE__ProductFamily__c(NE__ProdId__c = prod2.id, NE__FamilyId__c = family.id);
        listProdFamily.add(prodFamily);
        prodFamily  =   new NE__ProductFamily__c(NE__ProdId__c = prod3.id, NE__FamilyId__c = family.id);
        listProdFamily.add(prodFamily);
        insert listProdFamily;
        
        NE__DynamicPropertyDefinition__c prop1  =   new NE__DynamicPropertyDefinition__c(name = 'TestProperty1',NE__Type__c ='Product');
        insert prop1;
         NE__DynamicPropertyDefinition__c prop2 =   new NE__DynamicPropertyDefinition__c(name = 'TestProperty1',NE__Type__c ='Product');
        insert prop2;               

        NE__ProductFamilyProperty__c pfp1 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=family.Id, NE__PropId__c=prop1.Id,NE__Required__c='false');
        insert pfp1;
        NE__ProductFamilyProperty__c pfp2 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=family.Id, NE__PropId__c=prop2.Id,NE__Required__c='false');
        insert pfp2;


        NE__Catalog_Item__c ci=new NE__Catalog_Item__c();
        ci.NE__Catalog_Id__c=cat.id;
        insert ci;

        List<NE__Catalog_Item__c> listCatItem = New List<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catItem = new NE__Catalog_Item__c(NE__Item_Header__c  ='a0T5E000000WT8o',NE__Active__c=true,NE__Type__c = 'Product',CurrencyIsoCode='EUR', NE__ProductId__c=prod2.Id,NE__Catalog_Id__c=cat.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, NE__Recurring_Charge_Frequency__c='Monthly');
        listCatItem.add(catItem);
        catItem = new NE__Catalog_Item__c(NE__Item_Header__c  ='a0T5E000000WT8o',NE__Active__c=true,NE__Type__c = 'Product',NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod2.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, NE__Recurring_Charge_Frequency__c='Monthly',CurrencyIsoCode='EUR');
        listCatItem.add(catItem);
        catItem = new NE__Catalog_Item__c(NE__Item_Header__c  ='a0T5E000000WT8o',NE__Active__c=true,NE__Type__c = 'Product',NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod2.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, NE__Recurring_Charge_Frequency__c='Monthly',CurrencyIsoCode='EUR');
        listCatItem.add(catItem);
        catItem = new NE__Catalog_Item__c(NE__Item_Header__c  ='a0T5E000000WT8o',NE__Active__c=true,NE__Type__c = 'Category',NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod2.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, NE__Recurring_Charge_Frequency__c='Monthly',CurrencyIsoCode='EUR');
        listCatItem.add(catItem);
         catItem = new NE__Catalog_Item__c(NE__Item_Header__c  ='a0T5E000000WT8o',NE__Active__c=true,NE__Type__c = 'Category',NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod3.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, NE__Recurring_Charge_Frequency__c='Monthly',CurrencyIsoCode='EUR');
        listCatItem.add(catItem);
         catItem = new NE__Catalog_Item__c(NE__Item_Header__c  ='a0T5E000000WT8o',NE__Active__c=true,NE__Type__c = 'Category',NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod3.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, NE__Recurring_Charge_Frequency__c='Monthly',CurrencyIsoCode='EUR');
        listCatItem.add(catItem);
        insert listCatItem;

        // *********************************
        // Catalog test data - END
        // *********************************





		
		TGS_ProductOrdering_Helper.createDummyContact(String.valueOf(a.Id));
		TGS_ProductOrdering_Helper.OrderJSON order=TGS_ProductOrdering_Helper.parse(json);
		TGS_ProductOrdering_Helper.getProductIds(order.orderItem);
		Map<String, String> map_status = TGS_ProductOrdering_Helper.getStatus(order.productOrderType);
		Map<String,String> mapRTs=TGS_ProductOrdering_Helper.getRTs();
		
		BI_Punto_de_Instalacion__c site=TGS_ProductOrdering_Helper.getSite(order.OrderItem[0]);
		Map<String, Account> map_account = TGS_ProductOrdering_Helper.getAccounts('ACCFRB0001', order.orderItem[0].billingAccount[0].id);
		System.debug('map_acc'+map_account);
		Map<String,Id> mapContractInfo=TGS_ProductOrdering_Helper.getContactInfo(map_account.get('TGS_Cost_Center').TGS_Aux_Legal_Entity__c);
		System.debug('mapContractInfo'+mapContractInfo);

        NE__Order__c orderToInsert=TGS_ProductOrdering_Helper.createOrder(map_status.get('OrderStatus'), order,site,mapRTs.get('Order'),map_account.get('TGS_Cost_Center'),mapContractInfo);
        NE__Order__c orderAssetToInsert=TGS_ProductOrdering_Helper.createAsset(map_status.get('AssetStatus'), orderToInsert,mapRTs.get('Asset'));
        
        Map<String,NE__Order__c>mapOrders=new Map<String,NE__Order__c>(); 
        mapOrders.put('Order',orderToInsert);
        mapOrders.put('Asset',orderAssetToInsert);
		TGS_ProductOrdering_Helper.updateAssetEnterpriseId(mapOrders);
		


		System.debug('order.OrderItem[0]).get(IdItemHeader)'+order.OrderItem[0].Product.id);
        Map<String, Object> map_productTree = TGS_CatalogHelper.getProductTree(new Set<Id>{(Id)TGS_ProductOrdering_Helper.getItemHeaderFromProduct(order.OrderItem[0]).get('IdItemHeader')}, 'TGSOL Catalog');
        System.debug('mapproducttree-->'+map_productTree);
        TGS_ProductOrdering_Helper.getItemHeaderFromProduct(order.orderItem[0]);
		TGS_ProductOrdering_Helper.createCase(TGS_ProductOrdering_Helper.createDummyContact(a.id),mapOrders,(String)TGS_ProductOrdering_Helper.getItemHeaderFromProduct(order.OrderItem[0]).get('Name'),order.priority,mapRTs.get('Order_Management_Case'));
		
		System.debug('before createOI:::map_status-'+map_status+' mapOrders-'+mapOrders+' order.OrderItem-'+order.OrderItem+' map_productTree-'+map_productTree);
        TGS_ProductOrdering_Helper.createOrderItems(map_status,mapOrders,order.OrderItem,map_productTree,(String)TGS_ProductOrdering_Helper.getItemHeaderFromProduct(order.OrderItem[0]).get('IdItemHeader'));
		
		Test.stopTest();*/

	}
	 public static String getJSON() {
	
	String json=		'{'+
		'  "correlationId": "001",'+
		'  "priority" : 0,'+
		'  "productOrderType" : "New",'+
		'  "relatedParty": ['+
		'    {'+
		'      "id": "string",'+
		'      "name": "string",'+
		'      "entityType": "Contact",'+
		'      "description": "String"'+
		'    }'+
		'  ],'+
		'  "orderItem": [{'+
		'      "action": "add",'+
		'      "product": { '+
		'        "id": "a0ig0000002ObtnAAC",'+
		'        "name": "SIM Card Suscription",'+
		'        "characteristic": ['+
		'          {'+
		'            "name": "a0fg0000002WOSrAAO",'+
		'            "value": "1234567890"'+
		'          }'+
		'        ]'+
		'      },'+
		'      "place": ['+
		'        {'+
		'          "id": "S0001",'+
		'          "name": "Site",'+
		'          "entityType": "Site"'+
		'        }'+
		'      ],'+
		'      "quantity": "1",'+
		'      "orderItemPrice": ['+
		'        {'+
		'          "priceType": "MRC", '+
		'          "recurringChargePeriod": "monthly",'+
		'            "price": {'+
		'                "amount": 10,'+
		'                "units": "EUR"'+
		'            }'+
		'        },'+
		'        {'+
		'        "priceType": "NRC",'+
		'        "recurringChargePeriod": "",'+
		'          "price": {'+
		'              "amount": 10,'+
		'              "units": "EUR"'+
		'          }'+
		'        }],'+
		'        "billingAccount": ['+
		'          {'+
		'            "id": "ACFRC0001",'+
		'            "name": "Acc Cost Center FR",'+
		'            "role": "cost center",'+
		'            "entityType": "Account"'+
		'          }'+
		'        ],'+
		'        "status" : "pending",'+
		'        "validFor": {'+
		'            "startDateTime": "2018-01-03T10:18:37.597Z",'+
		'            "endDateTime": "2018-01-03T10:18:37.597Z"'+
		'          }'+
		'    },'+
		'    {'+
		'      "action": "add",'+
		'      "product": { '+
		'        "id": "a0ig0000002ObulAAC",'+
		'        "name": "SIM Card Suscription",'+
		'        "characteristic": ['+
		'          {'+
		'            "name": "a0fg0000002WOWoAAO",'+
		'            "value": "12345"'+
		'          }'+
		'        ]'+
		'      },'+
		'      "place": ['+
		'        {'+
		'          "id": "Stest00001",'+
		'          "name": "Site",'+
		'          "entityType": "Site"'+
		'        }'+
		'      ],'+
		'      "quantity": "1",'+
		'      "orderItemPrice": ['+
		'        {'+
		'          "priceType": "MRC", '+
		'          "recurringChargePeriod": "monthly",'+
		'            "price": {'+
		'                "amount": 10,'+
		'                "units": "EUR"'+
		'            }'+
		'        },'+
		'        {'+
		'        "priceType": "NRC",'+
		'        "recurringChargePeriod": "",'+
		'          "price": {'+
		'              "amount": 10,'+
		'              "units": "EUR"'+
		'          }'+
		'        }],'+
		'        "billingAccount": ['+
		'          {'+
		'            "id": "ACFRCtest0001",'+
		'            "name": "Acc Cost Center FR",'+
		'            "role": "cost center",'+
		'            "entityType": "Account"'+
		'          }'+
		'        ],'+
		'        "status" : "pending",'+
		'        "validFor": {'+
		'            "startDateTime": "2018-01-03T10:18:37.597Z",'+
		'            "endDateTime": "2018-01-03T10:18:37.597Z"'+
		'          }'+
		'    }]'+
		''+
		'}'+
		'';
		return json;
		
	}
}