/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-04-20      Manuel Esthiben Mendez Devia (MEMD)     Cloned Controller
*************************************************************************************************************/
global class BI_COL_InterfacesSisgot_ctr 
{
 
      WebService static String EnviarAplazamiento(String NomDS,String fechaInicioAplazamiento,String fechaFinAplazamiento,String causal){
           String Resultado='';
           String RespuestaWS = '';
           String infoAEnviar='';

           BI_COL_Modificacion_de_Servicio__c Mod_Servicio;
           Mod_Servicio = [ Select m.BI_COL_Causal_aplazamiento__c, m.BI_COL_Codigo_unico_servicio__c, 
           				m.BI_COL_Codigo_unico_servicio__r.Name, m.BI_COL_Fecha_quiebre__c,m.BI_COL_Producto__c, (select id, BI_COL_Fecha_fin_aplazamiento__c from BI_COL_Modificacion_de_Servicio__r) 
           				from BI_COL_Modificacion_de_Servicio__c m where Name =: NomDS];
           
           System.debug('Procesando aplazamiento DS: ' + NomDS);                               
           //Consulta informacion de todos los campos solicitados para enviar   
           
            
            infoAEnviar  =  Mod_Servicio.BI_COL_Codigo_unico_servicio__r.Name + ', ' +
                            EsNull(fechaInicioAplazamiento) + ', ' +
                            EsNull(fechaFinAplazamiento) + ', ' +
                            causal+','+EsNull(NomDS);
           

           //Invoca WS de SISGOT
           ws_Sisgot_Aplazamientos.serviceAplazamientosPort stub =	new ws_Sisgot_Aplazamientos.serviceAplazamientosPort();
           System.debug('\n\n Informacion Enviada: '+infoAEnviar+'\n\n');
			if(!Test.isRunningTest())
			{
				RespuestaWS = stub.processAplazamiento(infoAEnviar);                                 
				System.debug('\n\n Informacion Recibida: '+RespuestaWS+'\n\n');
			}
			else
			{
				RespuestaWS=label.BI_COL_LbTrueData;
			}    
           //Manejar respuesta de SISGOT: estado, num ticket,  descripcion                         
			List<String> listRespuesta=RespuestaWS.split(',');
			Boolean RespuestaExitosa =Boolean.valueOf(listRespuesta[0]); 
            //interpretarRespuesta(EncodingUtil.urlDecode(String.valueof(RespuestaWS), 'UTF-8'));
			
           //si el envio fue exitoso, actualiza estado de la DS ---------------------------------------------->>>>>>>>>>>>> ACA VOY
           if (RespuestaExitosa)
           {
             //  System.debug('\n\n Mod_Servicio.(select id, BI_COL_Fecha_fin_aplazamiento__c from BI_COL_Modificacion_de_Servicio__r) : '+Mod_Servicio.(select id, BI_COL_Fecha_fin_aplazamiento__c from BI_COL_Modificacion_de_Servicio__r) +'\n\n');
                //analiza si es aplazamiento o desaplazamiento
               if (fechaFinAplazamiento == null)
               { //es aplazamiento
                   Mod_Servicio.BI_COL_Estado_orden_trabajo__c= label.BI_COL_LbAplazada;
               }  
               else //es desaplazamiento, vuelve a estado inicial
               {
                   Mod_Servicio.BI_COL_Estado_orden_trabajo__c ='';
               }  
               
               Resultado = NomDS + label.BI_COL_LbEenviadaSISGOT; 
            
               //Escribir resultado en el log      
                          
               GuardarEnLog('SISGOT','Exitoso', String.valueof(infoAEnviar) , String.valueof(RespuestaWS), NomDS , 'Aplazamientos', Mod_Servicio.id);
    
           }else{
               Resultado = NomDS + label.BI_COL_LbNoEnviadaSISGOTAplazamiento + RespuestaWS; 
               GuardarEnLog('SISGOT','Fallido',String.valueof(infoAEnviar), String.valueof(RespuestaWS), NomDS , 'Aplazamientos', Mod_Servicio.id);             
           }

           update Mod_Servicio;
                                 
           return Resultado;
   }
   
   
// Consulta el segmento del usuario autenticado
	WebService static String ConsultarSegmentoUsuario(String Login)
	{

		System.debug('Consultando segmento de usuario: ' + Login);
		User Usuario = [Select u.CompanyName from User u  where u.Username=: Login];
		System.debug('Consultando segmento de usuario: ' + Login);            
		return Usuario.CompanyName;
	}
	
	public static Object EsNull(Object cadena)
	{
		if (cadena == null)
		{
			return '';
		}
		else
		{
			return EncodingUtil.urlEncode(String.valueof(cadena), 'UTF-8');
		}
	}
		
		

		
	
	
	
	public static list<String> processSplit(string spl){
			
			list<String> lstPar;		
			//SPLIT
			if(spl==null){
				lstPar=new String[]{'-','0','-','-','-','-','0','-','-'};

			}else{
				lstPar=spl.split('\\x7c');
			}
			system.debug('lstPar01'+lstPar);

			System.debug('\n\n inicia split-->'+lstPar+' --'+lstPar.size()+'\n'+spl+'\n\n');
			if(lstPar.size()<9)
			{
				lstPar=new String[]{'-','0','-','-','-','-','0','-','-'};
				system.debug('lstPar02'+lstPar);
				
				return lstPar;
			}else{
				//orden = pathType,pathNumber,firstPathCharacters,secondPathCharacters,pathZone,crossPathType,crossPathNumber,firstCrossPathCharacters,secondCrossPathCharacters,crossPathZone
				for(Integer i=0;i<lstPar.size();i++){
					if(lstPar.get(i).remove(' ')=='' && i!=1 && i!=6){
						lstPar[i]='-';
					}
					
					if(lstPar.get(i).remove(' ')=='' && (i==1 || i==6)){
						lstPar[i]='0';					
					}				
				}
				
				 
				
				if(lstPar.size()!=10)
				{
					lstPar.add('-');	
				}
				return lstPar;
					
			}			
			//END SPLIT
		
	
	}
	
	
	public static Boolean interpretarRespuesta(String respuesta)
	{
		Boolean RespuestaValida = false;
		
		String[] arrRespuesta = respuesta.split(',');
		System.debug('\n\n-->arrRespuesta[0]'+arrRespuesta[0]+'\n\n #######' +respuesta+'\n\n');
		if (arrRespuesta[0] == 'true')
		{
			RespuestaValida = true;
		}

		return RespuestaValida;
	} 
	
	public static void GuardarEnLog(String SistemaExterno,String Estado, String MensajeEnviado, String MensajeRecibido, String CodObjeto, String TipoTransaccion, String DSId)	
    {
    	if(DSId != null && DSId != '')
    	{
    	    BI_Log__c Log = new
			BI_Log__c(BI_COL_Estado__c = Estado, BI_Descripcion__c=MensajeEnviado, BI_COL_Informacion_recibida__c=MensajeRecibido, BI_COL_Identificador__c=CodObjeto, BI_COL_Tipo_Transaccion__c=TipoTransaccion, BI_COL_Modificacion_Servicio__c = DSId);

			insert Log;
    	}  	
    	else 
    	{
    	    BI_Log__c Log = new
			BI_Log__c(BI_COL_Estado__c = Estado, BI_Descripcion__c=MensajeEnviado, BI_COL_Informacion_recibida__c=MensajeRecibido, BI_COL_Identificador__c=CodObjeto, BI_COL_Tipo_Transaccion__c=TipoTransaccion);
			insert Log;

    	}  	

    	
    }
	

}