@isTest
private class TestNEImportDataFromCSVController {

    static testMethod void myUnitTest() {
        try{
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        	
        	RecordType RecId = [SELECT Id FROM RecordType WHERE DeveloperName <> 'TGS_Legal_Entity' and DeveloperName <> 'TGS_Legal_Entity_Inactive' and SobjectType = 'Account' LIMIT 1]; 
            
	        Account acc = new Account(Name = 'TestAccount', RecordTypeId = RecId.Id);
	        insert acc;

            
            
            NE__Product__c prod=new NE__Product__c(Name='NE__prova');
            insert prod;
            NE__Family__c fam1=new NE__Family__c(Name='fam1');
            insert fam1;
            NE__DynamicPropertyDefinition__c dpd1=new NE__DynamicPropertyDefinition__c(Name='prova1');
            insert dpd1;
            
            NE__PropertyDomain__c dom = new NE__PropertyDomain__c(NE__PropId__c = dpd1.id, name = 'test');
            insert dom;
            
            NE__ProductFamilyProperty__c pdp=new NE__ProductFamilyProperty__c(NE__PropId__c=dpd1.Id,NE__FamilyId__c=fam1.Id);
            insert pdp;
            NE__ProductFamily__c profam = new NE__ProductFamily__c(NE__FamilyId__c = fam1.id,NE__ProdId__c = prod.id);
            insert profam;
            NE__Order__c ord = new NE__Order__c();
            ord.NE__AccountId__c = acc.Id;
            insert ord;
            
            NE__OrderItem__c item = new NE__OrderItem__c();
            item.NE__Qty__c = 5;
            item.NE__Account__c = acc.Id;
            item.NE__OrderId__c = ord.Id;
            item.NE__ProdId__c  =   prod.id;
            
            insert item;
            
            NE__OrderItem__c item2 = new NE__OrderItem__c();
            item2.NE__Qty__c = 2;
            item2.NE__Account__c = acc.Id;
            item2.NE__OrderId__c = ord.Id;
            item2.NE__ProdId__c  =   prod.id;
            
            insert item2;
            

            
            Pagereference p = Page.NEImportDataFromCSV;
            
            p.getParameters().put('orderItemId', item.Id);
            p.getParameters().put('orderId', ord.Id);
            
            Test.setCurrentPageReference(p);            
                   
            NE__OrderItem__c childitem = new NE__OrderItem__c();
            childitem.NE__Qty__c = 2;
            childitem.NE__Account__c = acc.Id;
            childitem.NE__OrderId__c = ord.Id;
            childitem.NE__Parent_Order_Item__c = item.Id;
            childitem.NE__Multiply_Configuration_Item__c = item.Id;
            childitem.NE__ProdId__c =   prod.id;
            
            insert childitem;
            
            
            NE__Order_Item_Attribute__c childitemattr = new NE__Order_Item_Attribute__c(name='prova1', NE__Value__c = 'test', NE__Order_Item__c = childitem.id, NE__FamPropId__c= pdp.id);
            insert childitemattr;
            
                        
            BI_Sede__c sed = new BI_Sede__c();
            sed.BI_Direccion__c = 'a';
            sed.BI_Provincia__c = 'Milan';
            sed.BI_Localidad__c = 'localidad';
            sed.BI_Codigo_postal__c = '21125';
            sed.BI_ID_de_la_sede__c = '145236563456542';
            
            insert sed;
            
            BI_Punto_de_instalacion__c punto = new BI_Punto_de_instalacion__c();
            punto.BI_Cliente__c = childItem.NE__Account__c;//acc.Id;
            punto.BI_Sede__c = sed.Id;
            
            insert punto;
            
            Apexpages.Currentpage().getParameters().put('orderItemId',item2.Id);
            Apexpages.Currentpage().getParameters().put('orderId',ord.Id);
            NEImportDataFromCSVController contr = new NEImportDataFromCSVController();
            
            Apexpages.Currentpage().getParameters().put('orderItemId',item2.Id);
            Apexpages.Currentpage().getParameters().put('isback','true');
            try{
                NEImportDataFromCSVController contr2 = new NEImportDataFromCSVController();
            }
            catch(Exception e){}
            contr.orderItemid = item.id;    
            contr.orderid = ord.id;   
            Apexpages.Currentpage().getParameters().put('isback','false');
            String bef = 'ID SEDE;prova1;Contacto;Contacto tecnico;\n145236563456542;test;test2;test3;';
            Blob beforeblob = Blob.valueOf(bef);
            String paramvalue = EncodingUtil.base64Encode(beforeblob);
            Blob afterblob = EncodingUtil.base64Decode(paramvalue);
                        
            contr.csvFileBody = afterblob;
            
            contr.csvAsString = contr.csvFileBody.toString();
            contr.csvFileLines = contr.csvAsString.split('\n'); 
            
            
            Test.startTest();
            try{    
                   
                contr.importCSVFile();
                contr.returnOrder();
            
            }catch(Exception e){
        
            }
            try{    
                contr.csvFileBody = null;
            
                contr.csvAsString = null;
                contr.csvFileLines = null;   
                contr.importCSVFile();
                contr.returnOrder();
            
            }catch(Exception e){
        
            }
            
            try{    
                String bef2 = 'ID SEDE;prova3;Contacto;Contacto tecnico;\n145236563456542;test;test2;test3;';
                Blob beforeblob2 = Blob.valueOf(bef2);
                String paramvalue2 = EncodingUtil.base64Encode(beforeblob2);
                Blob afterblob2 = EncodingUtil.base64Decode(paramvalue2);
                            
                contr.csvFileBody = afterblob2;
                
                contr.csvAsString = contr.csvFileBody.toString();
                contr.csvFileLines = contr.csvAsString.split('\n');   
                contr.importCSVFile();
            
            }catch(Exception e){
        
            }
            try{
                 contr.ItemWrapperList=null;
                 contr.returnOrder();
            }
            catch(Exception e){}
            
           
           contr.ind='0'; 
           contr.changeadd();
           List<NE__OrderItem__c> oilist = new List<NE__OrderItem__c>();
           oilist.add(item);
           oilist.add(item2);
           contr.ItemList2=oilist;
           contr.alreadysplitted=false;
           try{
               contr.parentOrderItem=item;
               contr.parentOrderItem.NE__Qty__c=5;
               contr.ItemListSplitted.add(item);
               NEImportDataFromCSVController.MultiplierItemWrapper owm = new NEImportDataFromCSVController.MultiplierItemWrapper();
               owm.orderitem=item;
               list<NEImportDataFromCSVController.MultiplierItemWrapper> ow = new list<NEImportDataFromCSVController.MultiplierItemWrapper>();
               ow.add(owm);
               contr.splitLines(ow);
           }
           catch(Exception e){} 
           
           try{
               contr.generateSplittedMultiplierAttributes(null,null);
           }
           catch(Exception er){}
           
           try{
               contr.generateOrderItemAttributes(item,null,null,null);
           }
           catch(Exception ee){}
           
           try{
                
               contr.generateSplittedMultiplier(item);
           }
           catch(Exception ero){}
           
           contr.goBack();
           
           contr.setblankField1();
           
           contr.setblankField2();
          
            
        }
        catch(Exception e1){}
       
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}