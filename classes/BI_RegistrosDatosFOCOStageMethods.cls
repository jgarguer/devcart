public without sharing class BI_RegistrosDatosFOCOStageMethods {

	public static void getInsertedCountries(List<BI_Registros_Datos_FOCO_Stage__c> records) {
		 //PLL_PickList_Refactorizacion_Necesaria
		List<BI_FOCO_Stage_Countries__c> countriesToInsert;
		Map<String, BI_FOCO_Stage_Countries__c> existingCountries;
		Set<String> newCountries;

		countriesToInsert = new List<BI_FOCO_Stage_Countries__c>();
		existingCountries = BI_FOCO_Stage_Countries__c.getAll();
		newCountries = new Set<String>();

		for(BI_Registros_Datos_FOCO_Stage__c stageRecord : records) {
			newCountries.add(stageRecord.BI_Country__c);
		}

		for(String newCountry : newCountries) {
			if(!existingCountries.containsKey(newCountry) && newCountry != null) {
				countriesToInsert.add(new BI_FOCO_Stage_Countries__c(Name = newCountry));
			}
		}

		if(!countriesToInsert.isEmpty()) {
			insert countriesToInsert;
		}
		
	}
}