public with sharing class CWP_Redirect {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        
    Company:       Everis
    Description:   Class for redirect between CWP y Platino  
    
    History:
    
    <Date>            <Author>              <Description>
   14/12/2016        Julio Asenjo         Initial version

    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    /*
    Developer Everis
    Function: method used to check if the profile name of the logged users starts with TGS
    */
    public static boolean esTGS {get{
        Id pId = UserInfo.getProfileId();
        system.debug('llamada a tgs');
        Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
        if (p.size()>0){
            String pName = p[0].Name;
            return pName.startsWith('TGS');
        }
        return false;
    }
    set;}
    
    public String link{get;set;}
    
    /*
    Developer Everis
    Function: method used to redirect to one page or another depending on the prefix of the profile name of the logged user
    */
    @RemoteAction
    public static PageReference dotestpage(String origin){
        
        //origin = ApexPages.currentPage().getParameters().get('from');
        system.debug('origin:' + origin);
        PageReference pageRef;
        if(esTGS){
            if(origin != null && origin.equalsIgnoreCase('doc')){
                //pageRef = new PageReference('https://' + ApexPages.currentPage().getParameters().get('url'));
                pageRef = new PageReference('/empresasplatino/PCA_MyServices');
                pageRef.setRedirect(true);
            }else{
                system.debug('llamada a CWP installed services');
                pageRef = new PageReference('/empresasplatino/CWP_Installed_Services');
                pageRef.setRedirect(true);
            }
        }else{
            if(origin != null && origin.equalsIgnoreCase('doc')){
                pageRef = new PageReference('/empresasplatino/PCA_DocumentManagement');
                pageRef.setRedirect(true);
            }else{
                pageRef = new PageReference('/empresasplatino/PCA_Inventory');
                pageRef.setRedirect(true);
            }
        }
        system.debug('retPage:' + pageRef);
        return pageRef;
    }
    
    public void getLink(){
        link = ApexPages.currentPage().getParameters().get('url');
        system.debug('link' + link);
    }

}