@isTest
private class CWP_New_Incident_Controller_TEST {
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis 
Company:       Everis
Description:   Test Methods executed to comprobe failure coverage 
Test Class:    CWP_Installed_Services_Controller.cls
CWP_New_Incident_Controller

History:

<Date>                  <Author>                <Change Description>
28/02/2016               Everis                   Initial Version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    @testSetup 
    private static void dataModelSetup() {               
                    
        //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
       
        set <string> setStrings = new set <string>{
            'Account', 
            'Case',
            'NE__Order__c',
            'NE__OrderItem__c'
        };
                
        //ACCOUNT
        map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            //rtMapBydevName.put(i.DeveloperName, i.id);
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }          
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        insert contactTest;
        
        /* SEDE & PUNTO DE INSTALACIÓN */
        BI_Sede__c sede = new BI_Sede__c (BI_Codigo_postal__c = '11111', BI_Direccion__c = 'test', BI_ID_de_la_sede__c = '1', BI_Localidad__c = 'test', BI_Provincia__c = 'test');
        insert sede;
        
        BI_Punto_de_instalacion__c punto = new BI_Punto_de_instalacion__c(BI_Cliente__c = accLegalEntity.id, BI_Sede__c = sede.Id);
        insert punto;
        
        User usuario;
              
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        insert producto;
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
        insert catalogoItem;
      
        //CASO     
        list <Case> listaDeCasos = new list <Case>();                
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.contactId = contactTest.id;        
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Billing_Inquiry'), 'sub2', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Change'), 'sub3', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Complaint'), 'sub4', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Problem'), 'sub5', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Query'), 'sub6', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub7', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;                
        listaDeCasos.add(newCase);
        insert listaDeCasos;        
        
        
        // ORDER   
        NE__Order__c testOrder1= CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', accLegalEntity.id);
        testOrder1.Case__c = listaDeCasos[1].id;
        insert testOrder1;
        
        NE__Order__c testOrder2 = CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', accLegalEntity.id);
        testOrder2.Case__c = listaDeCasos[0].id;
        insert testOrder2;        
        
        // ORDER ITEM
        NE__OrderItem__c newOI;        
        newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder1.id, producto.id, catalogoItem.id,  1);
        insert newOI;       
        
        newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder2.id, producto.id, catalogoItem.id,  2);
        insert newOI;      
        
        NE__Family__c family = CWP_TestDataFactory.createFamily('corleone');
        insert family;
        
        NE__DynamicPropertyDefinition__c dynProp = CWP_TestDataFactory.createDynamiyProperty('dynamic');
        insert dynProp;
        
        NE__ProductFamilyProperty__c famProp = CWP_TestDataFactory.createFamilyProperty(family.id, dynProp.id); 
        famProp.TGS_Is_key_attribute__c = true;
        famProp.CWP_KeyValue__c = true;
        insert famProp;
        
        NE__Order_Item_Attribute__c oia1 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        insert oia1;
        
        NE__Order_Item_Attribute__c oia2 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        insert oia2;      
        
        Attachment attachment = new Attachment();
        attachment.Body = Blob.valueOf('body test');
        attachment.Name = String.valueOf('test.txt');
        attachment.ParentId = accLegalEntity.Id; 
        insert attachment;
       
        TGS_GME_GM_Product_Categorization__c aTGS_GME= new  TGS_GME_GM_Product_Categorization__c();
  		aTGS_GME.name='001';  
    	aTGS_GME.TGS_Service__c='Producto Comercial';
        insert aTGS_GME;
        //crearWorkinfo
        System.runAs(usuario){                          
            Test.startTest();            
            CWP_New_Incident_Controller controller  = new CWP_New_Incident_Controller(); 
            //controller.initializeProductTiersBySID(catalogoItem.Id);
            List<Attachment> attachmentList = new List<Attachment>();
            attachmentList.add(attachment);
            Map<String,List<String>> valorParaMapa = new Map<String,List<String>>();
            List<String> listaAttach = new List<String>();
            //listaAttach.add(attachment.Name);
            listaAttach.add('test');
            string name=attachment.Name.left(attachment.Name.indexOf('.'));
            system.debug('name='+name);
            valorParaMapa.put(name, listaAttach);
            Cache.Session.put('local.CWPexcelExport.adjuntosMap', valorParaMapa);
            Cache.Session.put('local.CWPexcelExport.myAttachmentList' + userInfo.getUserId(), attachmentList);
            String aux = 'local.CWPexcelExport.'+name;
            Cache.Session.put(aux, 'test valor');
            controller.listCaseAttachments.add(attachment);
            controller.suid = 'testSuid';
            controller.sid = 'testSid';
            controller.crearWorkinfo(newCase.Id);
            controller.level4 = accHolding.Id;
            controller.templateLevel = 6;
            controller.templateCase = newCase;
            controller.loadLevel5();
            controller.templateLevel = 3;
            controller.level = 6;
            controller.loadLevel5();
            String suidParam = null;
            CWP_New_Incident_Controller controller1  = new CWP_New_Incident_Controller(suidParam);
            Test.stopTest();
         }

    }
    
    @isTest
    private static void createIncidentTest(){
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        
        System.runAs(usuario){                          
            Test.startTest();            
            CWP_New_Incident_Controller controller  = new CWP_New_Incident_Controller();            
            //controller.getSelectedIdNewIncident();
            controller.initializeProductTiers(); 
            //controller.initializeLocations();     
            controller.loadLevel1();      
            controller.loadLevel2();
            controller.loadLevel3();
            controller.loadLevel4();
            controller.loadLevel5();
            
            controller.loadLookupServices();
            controller.loadListYesNo();
            controller.loadListCPE();
            controller.uploadAttachment();
            //controller.crearWorkinfo(id);
            controller.crearCase();   
            controller.cancelar();                                              
            Test.stopTest();
        }   
        
    }
    
    @isTest
    private static void newIncidentControllerTest() {      

        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        
        System.runAs(usuario){                          
            Test.startTest();                        
            CWP_Installed_Services_Controller controllerIS  = new CWP_Installed_Services_Controller();
            
            NE__OrderItem__c orderItem = [SELECT id FROM NE__OrderItem__c WHERE NE__Qty__c =: 2];
            
            controllerIS.idOrderItem = orderItem.id;
            
            controllerIS.attachmentName = 'name_testAttachment';
            controllerIS.attachmentDescription = 'description_testAttachment';
            controllerIS.attachmentContent = 'Y29udGVudF90ZXN0QXR0YWNobWVudA=='; //debe ir codificado en BASE64
            
            ApexPages.currentPage().getParameters().put('sId', orderItem.id);
            ApexPages.currentPage().getParameters().put('suId', orderItem.id);
            ApexPages.currentPage().getParameters().put('selectedServiceUnit', orderItem.id);   
            
            CWP_New_Incident_Controller controllerIncident0 = new CWP_New_Incident_Controller();
            
            controllerIncident0.errorMessage = 'error';
            controllerIncident0.templateLevel = 1;
            
            controllerIncident0.detailIdNewIncident = orderItem.id;
            
            controllerIncident0.initializeProductTiers(); 
            //controller.initializeLocations();     
            controllerIncident0.loadLevel1();      
            controllerIncident0.loadLevel2();
            controllerIncident0.loadLevel3();
            controllerIncident0.loadLevel4();
            controllerIncident0.loadLevel5();
            
            controllerIncident0.loadLookupServices();
            
            controllerIncident0.listHours = new List<SelectOption>();
            controllerIncident0.listMinutes = new List<SelectOption>();
            controllerIncident0.loadListYesNo();
            controllerIncident0.loadListCPE();          
            
            controllerIncident0.crearCase();  
            
            controllerIncident0.attachmentName1 = 'name1';
            controllerIncident0.attachmentName2 = 'name2';
            controllerIncident0.attachmentName3 = 'name3';          
            controllerIncident0.attachmentDesc1 = 'descripcion1';
            controllerIncident0.attachmentDesc2 = 'descripcion2';
            controllerIncident0.attachmentDesc3 = 'descripcion3';           
            
            Blob k = EncodingUtil.base64Decode('Y29udGVudF90ZXN0QXR0YWNobWVudA==');  
            controllerIncident0.myAttachedBody = k;
            controllerIncident0.newAttachment.Name = 'Name';
            
            controllerIncident0.uploadAttachment();
            controllerIncident0.cancelar();
            
            controllerIncident0.detailIdNewIncident = orderItem.id;
            controllerIncident0.getSelectedIdNewIncident();
            
            CWP_New_Incident_Controller controllerIncident1 = new CWP_New_Incident_Controller(controllerIS);
            List<SelectOption> listSelects = new List<SelectOption>();
            listSelects = controllerIncident1.getBackupLines();
                        
            Test.stopTest();
            
        }       
        
                
        
        
    }        
    
}