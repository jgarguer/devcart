/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           		Descripción
*           -----   ----------      --------------------            		---------------
* @version   1.0    2015-04-15      Manuel Esthiben Mendez Devia (MEMD)     Cloned Controller
*************************************************************************************************************/
public with sharing class BI_COL_ContractTial_ctr 
{

	public string token =''; 
	public string nContrato {get;set;}
	public string idc {get;set;}
	public List<SelectOption> listidc {get;set;}
	public ws_wwwXappsTialComCoWebservicesSolicitud.wPort servTial =new ws_wwwXappsTialComCoWebservicesSolicitud.wPort();
	public list<reg> listreg=new list<reg>();
	public List<string> urls {get;set;}

	private final Contract acct;

	 public BI_COL_ContractTial_ctr() {
        
        

    }
    
    /*
    private integer removerLetras(string limpiar){

		limpiar = limpiar.replaceAll('[^\\d.]', '');
		System.debug('limpiar '+limpiar);
		return Integer.valueOf(limpiar);
    }
    */
    
    public void Inicio(){
    	
    	saludo();
    	string nc=ApexPages.currentPage().getParameters().get('nContrato');
    	
    	list<Contract> ctrid=new list<Contract>([select id, BI_COL_Numero_de_documento__c,CreatedDate,ContractNumber from Contract where id=:nc]); 
		
		//Antes del paso a produccion se debe tomar ContractNumber despues del PAP BI_COL_Numero_de_documento__c para el enviar a tial
		datetime fechaPAP = datetime.newInstance(2014, 8, 03);		
   		
   		if(fechaPAP<ctrid[0].CreatedDate){
   			nContrato=ctrid[0].ContractNumber;
   		}else{
   			nContrato=ctrid[0].ContractNumber;   		
   		}
   		

    	
    	
        if(token==''){return;}
        else if(nc=='' || nc==null){
        	BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.ERROR, 'No contiene Consecutivo Contrato SIAV');
        	return;
        	}
        
		listidc=new List<SelectOption>();

    
    
    }
    
    
    
    public PageReference ArchivosDisp(){
    	saludo();
    	idc=ApexPages.currentPage().getParameters().get('id');
    	urls=tialArchivo();
    	
    	if(urls.size()==1){
    		PageReference redirect = new PageReference(urls[0]);
    		redirect.setRedirect(true);
    		return redirect;    	
    	}
    	//------------------>> clonar BI_COL_MessageUtil_cls
    	System.debug(urls.isEmpty());
    	if(urls.isEmpty()){
    		BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.WARNING , 'No se han encontrado archivos');    	
    	}
    	
    	
    	return null;
    }
	
	
	public boolean tialSolicitud(){
		
	
		ws_wwwXappsTialComCoWebservicesSolicitud.respSOL_element respuestaSolicitud=new ws_wwwXappsTialComCoWebservicesSolicitud.respSOL_element();
		
		try{
			respuestaSolicitud=servTial.solicitud(token, nContrato);
			if(respuestaSolicitud.error==1){
				BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.ERROR, 'No se ha podido conectar por favor intente mas tarde');
				token='';
				return false;			
			}
			System.debug(respuestaSolicitud.data+'\n\n'+respuestaSolicitud);
			
			if(respuestaSolicitud.data!=null && respuestaSolicitud.data.reg!=null){
	            for(Integer i=0; respuestaSolicitud.data.reg.size()>i;i++){
	                listreg.add(new reg(respuestaSolicitud.data.reg[i].id,respuestaSolicitud.data.reg[i].nrodoc,respuestaSolicitud.data.reg[i].nuip,respuestaSolicitud.data.reg[i].nombre));
	                listidc.add(new SelectOption(respuestaSolicitud.data.reg[i].id,respuestaSolicitud.data.reg[i].id));
	            }
			}
		}catch(Exception e){
			
			BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.ERROR, 'La consulta ha fallado: '+e.getMessage() + e.getStackTraceString());
			return null;
		}
        
        
        return false;		
	}
	
	public list<reg> getInfo(){
		

		if(token!=''){
		tialSolicitud();
		}
		
		if(listreg.isEmpty()){
			BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.INFO, 'No se obtuvieron registros del contrato: '+nContrato);		
		}
		
		
		return listreg;
	}
	
	public list<string> tialArchivo(){
		
		List<String> listUrl=new List<String>();
		ws_wwwXappsTialComCoWebservicesSolicitud.respARCH_element respuestaArchivo=new ws_wwwXappsTialComCoWebservicesSolicitud.respARCH_element();
		
		try{
			respuestaArchivo=servTial.archivo(token, idc);
			
			if(respuestaArchivo.data!=null && respuestaArchivo.data.url!=null){
	            for(Integer i=0; respuestaArchivo.data.url.size()>i;i++){
	            	System.debug('\n\n'+respuestaArchivo.data.url[i]);
	                listUrl.add(respuestaArchivo.data.url[i]);
	            }
			}
		}catch(Exception e){
			
			BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.ERROR, 'La consulta ha fallado: '+e.getMessage());
		}
		
		return listUrl;
	
	}
	
	public void saludo(){
		
		token='MovistarSalesforce';return;//MovistarSalesforce
		
		/*
		ws_wwwXappsTialComCoWebservicesSolicitud.respTG_element saludo=new ws_wwwXappsTialComCoWebservicesSolicitud.respTG_element();
		
		try{
			saludo=servTial.saludo('Telefonica');
			
			if(saludo.error==1){
				BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.ERROR, 'No se ha podido conectar por favor intente mas tarde');
				token='';
				return;			
			}
			
			token=saludo.id;
			
			
		}catch(Exception e){
			
			BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.ERROR, 'No se ha podido conectar por favor intente mas tarde: '+e.getMessage());
		}
		*/

	}
	
    public class reg {
    	public String id{get;set;}
        public String nrodoc{get;set;}
        public String nuip{get;set;}
        public String nombre{get;set;}
        public Boolean opt {get;set;}
        
        public reg(String id,String nrodoc,String nuip,String nombre){
        	this.id=id;
        	this.nrodoc=nrodoc;
        	this.nuip=nuip;
        	this.nombre=nombre;
        	this.opt=false;

        }
        /*
        public void getUrl(){
        	Contratos_Tial c=new Contratos_Tial();
        }*/
    }
    


}