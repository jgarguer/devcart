public with sharing class PCA_IdeasCornerDetailController extends PCA_HomeController{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Class for show ideas corner details
    
    History:
    
    <Date>            <Author>              <Description>
    11/06/2014        Jorge Longarela       Initial version
    30/06/2014        Antonio Moruno        v1
    05/07/2014        Micah Burgos          v2
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public String searchText {get;set;}
    public boolean searchResult {get;set;}
    public Id idOtherFaq {get;set;}
    public List<Idea> IdeasPlatinoTotal {get;set;}
    public String TitleSearch {get;set;}
    public String DetailSearch {get;set;}
  
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   This method retrieves the controller data
    
    History:
    
    <Date>            <Author>              <Description>
    11/06/2014        Jorge Longarela       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_IdeasCornerDetailController getRecord(){
        try{            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            return this;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_IdeasCornerDetailController.getRecord', 'Portal Platino', Exc, 'Class');
           return null;
         }
    }

    public string ideaId {get; set;}
    public string typeI {get;set;}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Class for show idea details
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public class ideasWrapper{
        public string Title {get; set;}
        public string VoteTotal {get; set;}
        public string createdDate {get; set;}
        public string parsedDate {get; set;}
        public string id {get; set;}
        public string Body {get; set;}
        public string Category {get; set;}
        public string Community {get; set;}
        public string createdBy {get; set;}
    }
    public List<Idea> listIdea  {get;set;}
    public IdeaComment newComment {get; set;}
    public ideasWrapper selectedIdeaDetail {get;set;}
    public list<ideasWrapper> commentWrapperList {get; set;}
    public string ajaxParamValueVote {get; set;}
    public string allComments {get; set;}
    public boolean hasVote {get; set;}
    //public List<Idea> listIdea {get; set;}
    
    public PCA_IdeasCornerDetailController() {}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions2 (){
        try{
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfoDetail();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_IdeasCornerDetailController.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   search some ideas of ideas page 
    
    History:
    
    <Date>            <Author>            <Description>
    15/06/2015        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
  /*public void searchIdeas(){
    try{
      if(BI_TestUtils.isRunningTest()){
        throw new BI_Exception('test');
      }
      //this.numberAllFaqsShow = 20;
    
      listIdea = Database.query('Select Title, Body, VoteScore, VoteTotal, Id From Idea WHERE Title like \'%' + searchText + '%\'');
                          //[Select SolutionNote, SolutionName, Id From Solution WHERE SolutionName like '%' :searchText '%' limit 100];
      if(listIdea.isEmpty()){
        TitleSearch = null;
        DetailSearch = null;
        searchResult = false;
      }else{
        TitleSearch = listIdea[0].Title;
        DetailSearch = listIdea[0].Body;
        searchResult = true;      
        loadInfoDetail();  
      }
    }catch (exception Exc){
       BI_LogHelper.generate_BILog('PCA_IdeasFAQCtrl.searchFAQS', 'Portal Platino',Exc, 'Class');
    }
  }*/
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Method to go back in ideasFAQ 
    
    History:
    
    <Date>            <Author>            <Description>
    08/10/2014        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
 /* public void BackIdeas(){    
      searchText='';
      searchResult = false;
      searchIdeas();

  }*/

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Loads ideas details
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfoDetail (){
        try{
            //if(true /*super.enviarALoginComm()==null || Test.isRunningTest()*/){
                
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
                ideaId = Apexpages.currentPage().getParameters().get('Id')!=null?String.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('Id')):null;
                
                if(ideaId != null){
                    ideaId = String.escapeSingleQuotes(ideaId);
                }
                System.debug('PCA_IdeasCornerDetailController:ideaId= '+ ideaId);
                selectedIdeaDetail = new ideasWrapper();
                hasVote = false;
                commentWrapperList = new list<ideasWrapper>();
                allComments = '0';
                if(ideaId != null && ideaId != ''){
                    if((ideaId.length() == 15 || ideaId.length() == 18) && Pattern.matches('^[a-zA-Z0-9]*$', ideaId)) {
                    list<Vote> listVote = [SELECT CreatedById, ParentId, Id, Type 
                                            FROM Vote where ParentId = : ideaId and CreatedById =: Userinfo.getUserId()];
                    
                    if(!listVote.isEmpty()){
                        hasVote = true;
                    }
                    
                    
                    listIdea = [select Categories, CommunityId, Community.Name, CreatedById, CreatedBy.Name, CreatedDate, IsDeleted,  Body, LastCommentId, Id, ParentIdeaId, 
                                        IsHtml, LastCommentDate, LastModifiedById, LastModifiedDate, NumComments, 
                                        RecordTypeId, Status, SystemModstamp, Title, VoteScore, VoteTotal 
                                        from Idea where id = : ideaId ];
                    System.debug('PCA_IdeasCornerDetailController:listIdea= '+ listIdea);
                                        
                    if(!listIdea.isEmpty()){
                        selectedIdeaDetail.Title = listIdea[0].title;
                        System.debug('selectedIdeaDetail.Title= '+ selectedIdeaDetail.Title);
                        selectedIdeaDetail.Id = listIdea[0].id;
                        selectedIdeaDetail.VoteTotal = string.valueOf(listIdea[0].VoteTotal);
                        selectedIdeaDetail.Body = listIdea[0].Body;
                        selectedIdeaDetail.Category = listIdea[0].Categories;
                        selectedIdeaDetail.Community = listIdea[0].Community.Name;
                        selectedIdeaDetail.createdDate = string.valueOf(listIdea[0].CreatedDate);
                        selectedIdeaDetail.createdBy = listIdea[0].CreatedBy.Name;
                    }
                    
                    list<IdeaComment> commentList = [SELECT CommentBody, CreatedById, CreatedBy.Name, CreatedDate, IsDeleted, Id, IdeaId 
                                                            FROM IdeaComment where ideaID = : ideaId order by CreatedDate];
                    if(!commentList.isEmpty()){
                        allComments = string.valueOf(commentList.size());
                        for(IdeaComment item: commentList){
                            ideasWrapper ideasWrapperAux = new ideasWrapper();
                            ideasWrapperAux.Body = item.CommentBody;
                            ideasWrapperAux.createdBy = item.CreatedBy.Name;
                            ideasWrapperAux.createdDate = string.valueOf(item.CreatedDate);
                            ideasWrapperAux.Id = item.id; 
                            commentWrapperList.add(ideasWrapperAux);
                        }
                    }   
                    inicializeNewComment();                                             
                }
                }
            //}
            typeI  = apexpages.currentpage().getparameters().get('typeI')!=null?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('typeI')):null;   
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_IdeasCornerDetailController.loadInfo', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Initializes new comment
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void inicializeNewComment(){
        try{
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            error = false;
            newComment = new IdeaComment();
            newComment.IdeaId = ideaId;
            newComment.CommentBody = '';
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_IdeasCornerDetailController.inicializeNewComment', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Vote a selected Idea
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void voteIdea(){
        try{
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            error = false;
            Vote vote = new Vote();
            vote.ParentId = ideaId;
            System.debug('ajaxParamValueVote: ' + ajaxParamValueVote);
            if(ajaxParamValueVote == 'OK'){
                vote.Type = 'Up';
            }else{
                vote.Type = 'Down';
            }
            insert vote;

        }catch (exception Exc){
           error = true;
           BI_LogHelper.generate_BILog('PCA_IdeasCornerDetailController.voteIdea', 'Portal Platino', Exc, 'Class');
        }
    }
    
    public boolean error {get; set;}
    public string message {get; set;}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Create a new comment
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void createComment(){
        try{
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            error = false;
            system.debug(newComment.CommentBody);
            //newComment.CommentBody = 'Ok. Thanks!!!!!!';
            if(newComment.CommentBody == '' || newComment.CommentBody == null){
                error = true;
                message = Label.BI_ErrorRellenaCampos;
                return;
            }
            insert newComment;
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_IdeasCornerDetailController.createComment', 'Portal Platino', Exc, 'Class');
            error = true;
            message = 'Excepción: ' + Exc.getMessage();
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Reload the page
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference recargar (){
        try{
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            PageReference pageRef = Page.PCA_IdeasCornerDetails;
            pageRef.getParameters().put('id', ideaId);
            return pageRef;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_IdeasCornerDetailController.recargar', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Redirect to IdeasCorner page
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Micah Burgos          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference goToIdeas (){
        try{
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            PageReference pageRef = Page.PCA_IdeasCorner;
            return pageRef;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_IdeasCornerDetailController.goToIdeas', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }

}