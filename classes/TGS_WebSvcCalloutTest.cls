@isTest
                                               
private class TGS_WebSvcCalloutTest {
    @isTest static void testCreateIncident() { 
        TGS_Dummy_Test_Data.dummyEndpointsTGS(); 
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
            u.BI_Permisos__c='TGS';
            insert u;
            System.runAs(u) 
            {                
            // This causes a fake response to be generated
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateIncidentImpl());
            //TGS_Dummy_Test_Data.dummyEndpointsTGS(); 
            // Call the method that invokes a callout
            Test.startTest();
             TGS_RodTicketingWs.OutputMapping9 output = TGS_WebSvcCallout.createIncident();
             
            // Verify that a fake result is returned
            System.assertEquals('OK', output.Operation_Status);     
            Test.stopTest();
           } 
    }   
    @isTest static void testModifyIncident() {  
        TGS_Dummy_Test_Data.dummyEndpointsTGS();  
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c='TGS';
        insert u;
        System.runAs(u) 
        {              
            // This causes a fake response to be generated
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceModifyIncidentImpl());
            TGS_Dummy_Test_Data.dummyEndpointsTGS(); 
           Test.startTest();
            // Call the method that invokes a callout
             TGS_RodTicketingWs.OutputMapping10 output1 = TGS_WebSvcCallout.modifyIncident();  
             Test.stopTest();  
         }
    }  
    @isTest static void testCreateChange() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateChangeImpl());
        TGS_Dummy_Test_Data.dummyEndpointsTGS(); 
        // Call the method that invokes a callout
        Test.startTest();
         TGS_RodTicketingWs.OutputMapping7 output1 = TGS_WebSvcCallout.createChange();
         Test.stopTest();
         
    }    
    @isTest static void testModifyChange() {    
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c='TGS';
        insert u;
        System.runAs(u) 
        {             
            // This causes a fake response to be generated
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceModifyChangeImpl());
            TGS_Dummy_Test_Data.dummyEndpointsTGS(); 
            // Call the method that invokes a callout
            Test.startTest();
             TGS_RodTicketingWs.OutputMapping8 output1 = TGS_WebSvcCallout.modifyChange();        
             Test.stopTest();
        }     
             
    }    
    
    @isTest static void testWorkInfos() {  
    User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c='TGS';
        insert u;
        System.runAs(u) 
        {   
            // This causes a fake response to be generated
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateWorkinfoImpl());
            TGS_Dummy_Test_Data.dummyEndpointsTGS(); 
            // Call the method that invokes a callout
            Test.startTest();
             TGS_RodTicketingWs.OutputMapping5 output1 = TGS_WebSvcCallout.workinfos();
             Test.stopTest();
        }     
             
    }   
    @isTest static void testCreateAsset() {          
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c='TGS';
        insert u;
        System.runAs(u) 
        {       
            // This causes a fake response to be generated
            Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceCreateAssetImpl());
            TGS_Dummy_Test_Data.dummyEndpointsTGS(); 
            // Call the method that invokes a callout
            Test.startTest();
             TGS_RoDAttASTWs.OutputMapping1 output1 = TGS_WebSvcCallout.Create_Modify_Asset(); 
             Test.stopTest();
       }         
         
    }     
    @isTest static void testCreateSite() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new TGS_WebServiceImp.TGS_WebServiceSiteImpl());
        
        // Call the method that invokes a callout
         TGS_Site_Sync.OutputMapping1 output1 = TGS_WebSvcCallout.Create_Update_Site();
         
    }       
       
       
    
}