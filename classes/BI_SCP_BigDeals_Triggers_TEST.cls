@isTest
public class BI_SCP_BigDeals_Triggers_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Gustavo Kupcevich
    Company:        Certa
    Description:    
    
    History

    <Date>              <Author>            <Description>
    29/10/2015          Gustavo Kupcevich   Initial version
    20/09/2017        Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static testMethod void whenAnOpportunityIsInsertedItShouldCheckIfItShouldBeABigDealAndMarkItAccordingly() {
        string aCountryName = 'Robonia';
        Account anAccount = new AccountBuilder().withBICountry(aCountryName).build();
        insert anAccount;
        
        integer million = 1000000;
        double fcvLimit = 6, capexLimit = 5, navLimit = 4;
        Certa_SCP__Limite_de_Big_deal_por_pais__c aCountryLimit = createBigDealsCountryLimit(aCountryName, 'RBN', fcvLimit, capexLimit, navLimit);
        insert aCountryLimit;
        CurrencyType aCurrencyType = [SELECT ConversionRate, IsoCode FROM CurrencyType WHERE isActive = true AND isCorporate = false LIMIT 1][0];
        CurrencyType corporateCurrencyType = [SELECT ConversionRate FROM CurrencyType WHERE IsoCode = 'USD' LIMIT 1][0];
        
        List<Opportunity> Opportunities = new List<Opportunity> {            
            //CAPEX
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).withBICapex((capexLimit+1)*million).build(),
            //One time NAV
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).withBIoneTimePayment((navLimit+1)*million).build(),
            //Monthly NAV
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).withBIcontractDuration(13).withBImonthlyPayment(navLimit*million/12).withBIpreviousMonthlyPayment(0).build(),
            //Manual override
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).withCEbigDeals(true).build(),            
            //Not big deal
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).withCurrencyIsoCode(aCurrencyType.IsoCode)
                        .withAmount((fcvLimit-1)*million*(double)(aCurrencyType.ConversionRate/corporateCurrencyType.ConversionRate)).build(),
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).withAmount((fcvLimit-1)*million).withBICapex((capexLimit-1)*million)
                                .withBIoneTimePayment((navLimit-1)*million).withBImonthlyPayment(0).withBIpreviousMonthlyPayment(0).build()
        };

        List<boolean> expectedResults = new List<boolean> {true, true, true, true, false, false};
        Test.startTest();
        insert Opportunities;
        OpportunityCompetitor competitor = new OpportunityCompetitor(CompetitorName = 'Test', OpportunityId = Opportunities[0].id);
        insert competitor;
        update Opportunities;
        Test.stopTest();
        
        //refresh the value modified in the trigger
        for(Opportunity op : [SELECT Id, Certa_SCP__BigDeals__c FROM Opportunity]) {
            for(integer i = 0; i < Opportunities.size(); i++) {
                if(Opportunities[i].Id == op.Id) {
                    Opportunities[i].Certa_SCP__BigDeals__c = op.Certa_SCP__BigDeals__c;
                }
            }
        }
        
        for(integer i = 0; i < Opportunities.size(); i++) {
            System.assertEquals(expectedResults[i], Opportunities[i].Certa_SCP__BigDeals__c, 'Failed for oportunity #' + (i + 1));
        }
        
        delete Opportunities;
    }
    
    public static testMethod void whenAnOpportunityIsInsertedAndItsABigDeal_ItShouldCreateTheBigDealSurvey() {
        string aCountryName = 'Robonia';
        Account anAccount = new AccountBuilder().withBICountry(aCountryName).build();
        insert anAccount;

        List<Opportunity> Opportunities = new List<Opportunity> {
            createAnOpportunity().withCEbigDeals(true).withBICountry(aCountryName).withAccountId(anAccount.Id).build()
        };
        
        insert Opportunities;
        
        System.assertEquals(1, [SELECT Id FROM Certa_SCP__Big_deals_Survey__c].size());
    }
    
    public static testMethod void whenAnOpportunityIsUpdatedItShouldCheckIfItIsShouldBeABigDealAndMarkItAccordingly() {
        string aCountryName = 'Robonia';
        Account anAccount = new AccountBuilder().withBICountry(aCountryName).build();
        insert anAccount;
        
        integer million = 1000000, aYear = 12;
        double fcvLimit = 6, capexLimit = 5, navLimit = 4;
        Certa_SCP__Limite_de_Big_deal_por_pais__c aCountryLimit = createBigDealsCountryLimit(aCountryName, 'RBN', fcvLimit, capexLimit, navLimit);
        insert aCountryLimit;
        
        List<Opportunity> Opportunities = new List<Opportunity> {
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).build(),
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).build(),
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).build(),
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).build(),
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).withCEbigDeals(true).build(),
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).build()
        };
        
        insert Opportunities;
                
        Opportunities[1].BI_ARG_CAPEX_total__c = (capexLimit+1)*million;
        Opportunities[2].BI_Ingreso_por_unica_vez__c = (navLimit+1)*million;
        Opportunities[3].BI_Recurrente_bruto_mensual__c = (navLimit+1)*million/aYear;
        Opportunities[3].BI_Duracion_del_contrato_Meses__c = aYear;
        
        update Opportunities;
        List<boolean> expectedResults = new List<boolean> {false, true, true, true, true, false};
        //refresh the value modified in the trigger
        for(Opportunity op : [SELECT Id, Certa_SCP__BigDeals__c FROM Opportunity]) {
            for(integer i = 0; i < Opportunities.size(); i++) {
                if(Opportunities[i].Id == op.Id) {
                    Opportunities[i].Certa_SCP__BigDeals__c = op.Certa_SCP__BigDeals__c;
                }
            }
        }
        
        for(integer i = 0; i < Opportunities.size(); i++) {
            System.assertEquals(expectedResults[i], Opportunities[i].Certa_SCP__BigDeals__c, 'Failed for oportunity #' + (i + 1));
        }

    }
    
    public static testMethod void whenAnOpportunityIsUpdatedAndItsABigDeal_ItShouldCreateTheBigDealSurvey() {
        string aCountryName = 'Robonia';
        Account anAccount = new AccountBuilder().withBICountry(aCountryName).build();
        insert anAccount;
        
        List<Opportunity> Opportunities = new List<Opportunity> {
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).withCEbigDeals(false).build()
        };
        
        insert Opportunities;
        
        Opportunities[0].Certa_SCP__Manual_Big_Deal__c = true;
        update Opportunities;
        
        System.assertEquals(1, [SELECT Id FROM Certa_SCP__Big_deals_Survey__c].size());
    }

    public static testMethod void whenAnOpportunityIsUpdatedAndItsAlreadyABigDeal_ItShouldCreateTheBigDealSurveyOnlyOnce() {
        string aCountryName = 'Robonia';
        Account anAccount = new AccountBuilder().withBICountry(aCountryName).build();
        insert anAccount;
        
        List<Opportunity> Opportunities = new List<Opportunity> {
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).withCEbigDeals(true).build()
        };
        
        insert Opportunities;
        
        Opportunities[0].BI_Recurrente_bruto_mensual__c = 2;//change anything
        update Opportunities;
        
        System.assertEquals(1, [SELECT Id FROM Certa_SCP__Big_deals_Survey__c].size());
    }
    
    public static testMethod void whenTheSurveyCompetencialIsAllreadySetTheOpportunityDoesntReflectTheChange(){
        string aCountryName = 'Robonia';
        Account anAccount = new AccountBuilder().withBICountry(aCountryName).build();
        insert anAccount;
        Opportunity anOpportunity = new OpportunityBuilder().withBICountry(aCountryName).withAccountId(anAccount.Id).withCEbigDeals(true).build();
        insert anOpportunity;
       
        Certa_SCP__Big_deals_Survey__c aSurvey = [SELECT Certa_SCP__Es_moroso_con_el_Grupo__c FROM Certa_SCP__Big_deals_Survey__c WHERE Certa_SCP__Oportunidad__c = :anOpportunity.Id][0];
        
        aSurvey.Certa_SCP__Competencia__c = 'no';
        update aSurvey;
        
        
        OpportunityCompetitor oppCompetitor = new OpportunityCompetitor(CompetitorName = 'competidor de prueba', OpportunityId = anOpportunity.id );
        insert oppCompetitor;
        
        anOpportunity.name = 'nuevo nombre';
        update anOpportunity;

        aSurvey = [SELECT Certa_SCP__Competencia__c FROM Certa_SCP__Big_deals_Survey__c WHERE Certa_SCP__Oportunidad__c = :anOpportunity.Id][0];
        
        
        system.assertEquals('no', aSurvey.Certa_SCP__Competencia__c);
 		
        //if Certa_SCP__Competencia__c is null or empty, i set the field whit the opp competitors.
        aSurvey.Certa_SCP__Competencia__c = '';
        update aSurvey;
        
        update anOpportunity;
        
        aSurvey = [SELECT Certa_SCP__Competencia__c FROM Certa_SCP__Big_deals_Survey__c WHERE Certa_SCP__Oportunidad__c = :anOpportunity.Id][0];
        
        system.assertEquals('competidor de prueba', aSurvey.Certa_SCP__Competencia__c);
        
    }
    
    public static testMethod void whenAnAccountWithABigDealOpportunityIsUpdated_TheBigDealsSurveyShouldReflectTheChanges () {
        string aCountryName = 'Robonia';
        Account anAccount = new AccountBuilder().withBICountry(aCountryName).build();
        insert anAccount;
        Opportunity anOpportunity = new OpportunityBuilder().withBICountry(aCountryName).withAccountId(anAccount.Id).withCEbigDeals(true).build();
        insert anOpportunity;
        
        anAccount.BI_Riesgo__c = '1';
        update anAccount;
        
        Certa_SCP__Big_deals_Survey__c aSurvey = [SELECT Certa_SCP__Es_moroso_con_el_Grupo__c FROM Certa_SCP__Big_deals_Survey__c WHERE Certa_SCP__Oportunidad__c = :anOpportunity.Id][0];
        
        system.assertEquals('No', aSurvey.Certa_SCP__Es_moroso_con_el_Grupo__c);
        
        anAccount.BI_Riesgo__c = '2';
        update anAccount;
        
        aSurvey = [SELECT Certa_SCP__Es_moroso_con_el_Grupo__c FROM Certa_SCP__Big_deals_Survey__c WHERE Certa_SCP__Oportunidad__c = :anOpportunity.Id][0];
        
        system.assertEquals('Si', aSurvey.Certa_SCP__Es_moroso_con_el_Grupo__c);
    }

    public static testMethod void whenAnOpportunityIsUpdatedAndNoLongerBigDeal_TheOpportunityShouldBeUnmarked () {
        system.debug('whenAnOpportunityIsUpdatedAndNoLongerBigDeal_TheOpportunityShouldBeUnmarked');
        string aCountryName = 'Robonia';
        Account anAccount = new AccountBuilder().withBICountry(aCountryName).build();
        insert anAccount;
        
        integer million = 1000000;
        double fcvLimit = 6, capexLimit = 5, navLimit = 4;
        Certa_SCP__Limite_de_Big_deal_por_pais__c aCountryLimit = createBigDealsCountryLimit(aCountryName, 'RBN', fcvLimit, capexLimit, navLimit);
        insert aCountryLimit;
        //CurrencyType aCurrencyType = [SELECT ConversionRate, IsoCode FROM CurrencyType WHERE isActive = true AND isCorporate = false LIMIT 1][0];
        //CurrencyType corporateCurrencyType = [SELECT ConversionRate FROM CurrencyType WHERE IsoCode = 'USD' LIMIT 1][0];
        
        List<Opportunity> Opportunities = new List<Opportunity> {            
            //CAPEX
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).withBICapex((capexLimit+1)*million).build(),            
            //One time NAV
            createAnOpportunity().withBICountry(aCountryName).withAccountId(anAccount.Id).withBIoneTimePayment((navLimit+1)*million).build()
        };
        List<boolean> expectedResults = new List<boolean> {true,true};
        Test.startTest();
        insert Opportunities;
                
        //refresh the value modified in the trigger
        for(Opportunity op : [SELECT Id, Certa_SCP__BigDeals__c FROM Opportunity]) {
            for(integer i = 0; i < Opportunities.size(); i++) {
                if(Opportunities[i].Id == op.Id) {
                    Opportunities[i].Certa_SCP__BigDeals__c = op.Certa_SCP__BigDeals__c;
                }
            }
        }
        
        for(integer i = 0; i < Opportunities.size(); i++) {
            System.assertEquals(expectedResults[i], Opportunities[i].Certa_SCP__BigDeals__c, 'Failed for oportunity #' + (i + 1));
        }    

        system.debug('whenAnOpportunityIsUpdatedAndNoLongerBigDeal_TheOpportunityShouldBeUnmarked - Update');
        //Update Opportunities 
        for (Opportunity op :Opportunities) {
            op.Amount = 0;
            op.BI_ARG_CAPEX_total__c = 0;
            op.BI_Ingreso_por_unica_vez__c = 0;
        }
        
        update Opportunities;
        Test.stopTest();

        //refresh the value modified in the trigger
        for(Opportunity op : [SELECT Id, Certa_SCP__BigDeals__c FROM Opportunity]) {
            for(integer i = 0; i < Opportunities.size(); i++) {
                if(Opportunities[i].Id == op.Id) {
                    Opportunities[i].Certa_SCP__BigDeals__c = op.Certa_SCP__BigDeals__c;
                }
            }
        }

        for(integer i = 0; i < Opportunities.size(); i++) {
            System.assertEquals(!expectedResults[i], Opportunities[i].Certa_SCP__BigDeals__c, 'Failed for oportunity #' + (i + 1));
        }

    }    
    //------------------------------------------------
    //Helper methods
    
    static Certa_SCP__Limite_de_Big_deal_por_pais__c createBigDealsCountryLimit (string name, string countryCode, double fcvLimit, double capexLimit, double navLimit) {
        return new Certa_SCP__Limite_de_Big_deal_por_pais__c(Name = name, 
                                                            Certa_SCP__Pais_corto__c = countryCode,
                                                            Certa_SCP__FCV__c = fcvLimit, Certa_SCP__CAPEX__c = capexLimit, 
                                                            Certa_SCP__NAV__c = navLimit);
    }
    
    static OpportunityBuilder createAnOpportunity() {
        return new OpportunityBuilder();
    }
    
    //------------------------------------------------
    //Helper classes
    
    class OpportunityBuilder {
        Opportunity op;
        
        public OpportunityBuilder() {
            op = new Opportunity(Name = 'someName', BI_Country__c = 'ARG',
                            Amount = 0, BI_Ingreso_por_unica_vez__c = 0,
                            BI_ARG_CAPEX_total__c = 0, BI_Duracion_del_contrato_Meses__c = 1,
                            BI_Recurrente_bruto_mensual__c = 0, BI_Recurrente_bruto_mensual_anterior__c = 0,
//                            StageName = 'F6 - Prospecting', CloseDate = System.today(), Certa_SCP__BigDeals__c = false,
                            StageName = 'F5 - Solution Definition', CloseDate = System.today(), Certa_SCP__BigDeals__c = false,		//Manuel Medina
                            CurrencyIsoCode = 'USD');
        }
        
        public OpportunityBuilder withAccountId(string value) {
            op.AccountId = value;
            return this;
        }
       
        public OpportunityBuilder withCEbigDeals(boolean value) {
            //op.Certa_SCP__BigDeals__c = value;
            op.Certa_SCP__Manual_Big_Deal__c = value;
            return this;
        }
        
        public OpportunityBuilder withAmount(double value) {
            op.Amount = value;
            return this;
        }
        
        public OpportunityBuilder withCloseDate(Date value) {
            op.CloseDate = value;
            return this;
        }
        
        public OpportunityBuilder withStageName(string value) {
            op.StageName = value;
            return this;
        }
        
        public OpportunityBuilder withBICapex(double value) {
            op.BI_ARG_CAPEX_total__c = value;
            return this;
        }
        
        public OpportunityBuilder withBIoneTimePayment(double value) {
            op.BI_Ingreso_por_unica_vez__c = value;
            return this;
        }
        
        public OpportunityBuilder withBIcontractDuration(integer value) {
            op.BI_Duracion_del_contrato_Meses__c = value;
            return this;
        }
        
        public OpportunityBuilder withBImonthlyPayment(double value) {
            op.BI_Recurrente_bruto_mensual__c = value;
            return this;
        }
        
        public OpportunityBuilder withBIpreviousMonthlyPayment(double value) {
            op.BI_Recurrente_bruto_mensual_anterior__c = value;
            return this;
        }
        
        //BInav = 
        //  BI_Ingreso_por_unica_vez__c + 
        //  (IF( BI_Duracion_del_contrato_Meses__c > 12, 12, BI_Duracion_del_contrato_Meses__c ) * 
        //      ( BI_Recurrente_bruto_mensual__c - BI_Recurrente_bruto_mensual_anterior__c ))
        
        public OpportunityBuilder withName(string value) {
            op.Name = value;
            return this;
        }
        
        public OpportunityBuilder withBICountry(string value) {
            op.BI_Country__c = value;
            return this;
        }
        
        public OpportunityBuilder withCurrencyIsoCode(string value) {
            op.CurrencyIsoCode = value;
            return this;
        }
        
        public Opportunity build() {
            return op;
        }
    }
    
    class AccountBuilder {
        Account ac;
        
        public AccountBuilder() {
            ac = new Account(Name='Pepe',BI_Segment__c = 'test',
        BI_Subsegment_Regional__c = 'test',
        BI_Territory__c = 'test');

        }
        
        public AccountBuilder withRisk(string value) {
            ac.BI_Riesgo__c = value;
            return this;
        }
        
        public AccountBuilder withBICountry(string value) {
            ac.BI_Country__c = value;
            return this;
        }
        
        public Account build() {
            return ac;
        }
    }
}