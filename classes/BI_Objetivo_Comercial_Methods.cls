public with sharing class BI_Objetivo_Comercial_Methods	 
{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes
    Company:       Accenture
    Description:   Methods executed by BI_Objetivo_Comercial Triggers 
    Test Class:    BI_Objetivo_Comercial_MethodsTest
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/01/2018              Humeberto NUnes         Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	public static void validateFieldType(List<BI_Objetivo_Comercial__c> news) 
	{
		Map<String, Schema.SObjectField> M;
		M = Schema.SObjectType.Opportunity.fields.getMap();

		Schema.DisplayType FldType;

		for(BI_Objetivo_Comercial__c oc: news)
		{
			if (oc.BI_Tipo__c == 'Oportunidad')
			{			
				if (oc.BI_OBJ_Campo__c != '' && oc.BI_OBJ_Campo__c != null)
				{
					Schema.SObjectField field = M.get(oc.BI_OBJ_Campo__c);
					if (field == null)
						oc.addError('No Existe el "Api del Campo" inidcado.');
					else
						FldType = field.getDescribe().getType();
				}
				else  //FldType == Schema.DisplayType
				{
					oc.addError('Debe rellenar el campo "Api del Campo".');
					return;
				}

				if (FldType!=null)
				{
					if (oc.BI_Tipo__c == 'Oportunidad' && oc.BI_FVI_Unidad_Medida__c == 'Cantidad' && (FldType != Schema.DisplayType.Double || FldType == Schema.DisplayType.Integer))
							oc.addError('Solo son admitidos campos NUMERICOS (DOBLE O ENTERO) en el "Api del Campo" cuando "Unidad Medida" = Cantidad.');

					if (oc.BI_Tipo__c == 'Oportunidad' && oc.BI_FVI_Unidad_Medida__c == 'Moneda' && FldType != Schema.DisplayType.Currency)
							oc.addError('Solo son admitidos campos DIVISA en el "Api del Campo" cuando "Unidad Medida" = Moneda.');
				}
				else
				{
					oc.addError('El campo "Api del Campo" indicado NO EXISTE en el objeto "Opportunity".');
				}
			}
		}	
	}
}