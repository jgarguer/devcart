public with sharing class BI_O4_BIDTeamMemberMethods {
	
	 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Methods executed by BI_O4_BIDTeamMember Triggers
    Test Class:    BI_O4_BIDTeamMemberMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Method that adds the BIDTeamMember user to Opportunity Team
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera          Initial version
    30/08/2016              Fernando Arteaga        Update OpportunityShare access level to Edit
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void addBIDTeamMemberUserToOpp(List<BI_O4_BID_team_member__c>news){

		try
		{
			Set<Id> usuariosOPP = new Set<Id>();
			
			List<Id> listOppId = new List<Id>();
			for(BI_O4_BID_team_member__c item : news){
				listOppId.add(item.BI_O4_Opportunity__c);
			}
			
			Map<String, Id> mapRecordTypes = new Map<String, Id>();
	  		for(RecordType rt : [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'BI_O4_BID_Team']) {
	   			mapRecordTypes.put(rt.DeveloperName, rt.Id);
	   		}
	
			List<OpportunityTeamMember> lstOppTeamMember = [SELECT Id, OpportunityId, UserId, TeamMemberRole FROM OpportunityTeamMember WHERE OpportunityId IN : listOppId];		
			if(!lstOppTeamMember.isEmpty()){
				for(OpportunityTeamMember otm : lstOppTeamMember){
					usuariosOPP.add(otm.UserId);
				}
			}
			
			List<OpportunityTeamMember> lstOppTeamMemberAdd = new List<OpportunityTeamMember>();
			List<OpportunityTeamMember> lstOppTeamMemberUpd = new List<OpportunityTeamMember>();
			List<OpportunityShare> listOppShareToUpdate = new List<OpportunityShare>();
			
			Set<Id> setOppId = new Set<Id>();
			Set<Id> setUserId = new Set<Id>();
			
			for(BI_O4_BID_team_member__c item : news){
				if(mapRecordTypes.get('BI_O4_BID_Team') == item.RecordTypeId){
					if(!usuariosOPP.contains(item.BI_O4_Name2__c)){	
						OpportunityTeamMember otmUser = new OpportunityTeamMember();
						otmUser.UserId = item.BI_O4_Name2__c;
						otmUser.OpportunityId = item.BI_O4_Opportunity__c;
						otmUser.TeamMemberRole = item.BI_O4_Bid_team_role2__c;
						lstOppTeamMemberAdd.add(otmUser);
						setOppId.add(otmUser.OpportunityId);
						setUserId.add(otmUser.UserId);
					}
					else{
						for(OpportunityTeamMember opp : lstOppTeamMember){
							if(opp.UserId == item.BI_O4_Name2__c){
								opp.TeamMemberRole = item.BI_O4_Bid_team_role2__c;
								lstOppTeamMemberUpd.add(opp);
							}
						}
					}
				}
			}
			if(!lstOppTeamMemberAdd.isEmpty()){
				insert lstOppTeamMemberAdd;
			}
			if(!lstOppTeamMember.isEmpty()){
				update lstOppTeamMember;
			}
			
	        for (OpportunityShare oppShare : [SELECT OpportunityAccessLevel 
			                                  FROM OpportunityShare
			                                  WHERE OpportunityId IN :setOppId
			                                  AND UserOrGroupId IN :setUserId
			                                  AND RowCause = 'Team']) 
	        {
	            oppShare.OpportunityAccessLevel = 'Edit';
	            listOppShareToUpdate.add(oppShare);
	        }
	        
	        update listOppShareToUpdate;
		}
		catch(Exception exc){
			System.debug(exc);
			BI_LogHelper.generate_BILog('BI_O4_BIDTeamMemberMethods.addBIDTeamMemberUserToOpp', 'BI_EN', exc, 'Trigger');
		}
	}
}