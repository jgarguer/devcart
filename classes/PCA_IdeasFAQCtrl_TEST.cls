@isTest
private class PCA_IdeasFAQCtrl_TEST {

    static testMethod void getloadinfo() {
    	
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
    	 User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
         Community comm = [Select Id from Community where Name= :LABEL.PCA_Nombre_comunidad_ideas];
         Network netw = [Select Id from Network where Name= 'Empresas Platino'];
         Profile prof1 = [Select Id from Profile where Name='BI_Customer Communities'];
	    //insert usr;
	    system.debug('usr: '+usr);
	    User user1;
	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais); 
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            
            user1 = BI_DataLoad.loadPortalUser(con[0].Id, prof1.Id);	    	
	    	system.runAs(user1){
	    		
	    		PageReference pageRef = new PageReference('PCA_IdeasFAQ');
	       		Test.setCurrentPage(pageRef);
	       		
	    		Solution FAQ1 = new Solution(SolutionName='Solution test',IsPublished =true,SolutionNote='FAQ Test');
	    		insert FAQ1;
	    		Solution FAQ2 = new Solution(SolutionName='Solution test 2',IsPublished =true,SolutionNote='FAQ Test 2');
	    		insert FAQ2;
	    		ApexPages.currentPage().getParameters().put('Id', FAQ1.Id);
        		PCA_IdeasFAQCtrl constructor = new PCA_IdeasFAQCtrl();
        		BI_TestUtils.throw_exception = false;
        		constructor.searchText='test';
        		constructor.idOtherFaq=FAQ2.Id;
        		constructor.numberAllFaqsShow=1;
        		constructor.checkPermissions();
        		constructor.loadInfo();
        		constructor.recoverySolutions();
        		constructor.showOtherFaq();
        		constructor.searchFAQS();
        		constructor.incrementNumberAllFaqsShow();
        		constructor.getshowMoreNumberAllFaqs();
        		constructor.selectLanguage();
        		system.assertEquals(FAQ1.Id,[SELECT Id From Solution Where SolutionName = 'Solution test'].Id);
	    	}
	    	system.runAs(user1){
	    		
	    		PageReference pageRef = new PageReference('PCA_IdeasFAQ');
	       		Test.setCurrentPage(pageRef);
	       		
	    		Solution FAQ1 = new Solution(SolutionName='Solution test',IsPublished =true,SolutionNote='FAQ Test');
	    		insert FAQ1;
	    		ApexPages.currentPage().getParameters().put('Id', FAQ1.Id);
        		PCA_IdeasFAQCtrl constructor = new PCA_IdeasFAQCtrl();
        		BI_TestUtils.throw_exception = false;
        		constructor.numberAllFaqsShow=1;
        		constructor.checkPermissions();
        		constructor.loadInfo();
        		constructor.recoverySolutions();
        		constructor.showOtherFaq();
        		constructor.searchFAQS();
        		constructor.incrementNumberAllFaqsShow();
        		constructor.getshowMoreNumberAllFaqs();
        		constructor.selectLanguage();
        		
	    	}
	    	system.runAs(user1){
	    		
	    		PageReference pageRef = new PageReference('PCA_IdeasFAQ');
	       		Test.setCurrentPage(pageRef);
	       		
	    		Solution FAQ1 = new Solution(SolutionName='Solution test',IsPublished =true,SolutionNote='FAQ Test');
	    		insert FAQ1;
	    		ApexPages.currentPage().getParameters().put('Id', FAQ1.Id);
        		PCA_IdeasFAQCtrl constructor = new PCA_IdeasFAQCtrl();
        		BI_TestUtils.throw_exception = true;
        		constructor.searchText='test';
        		constructor.Dato=acc[0].Id;
        		constructor.checkPermissions();
        		constructor.loadInfo();
        		constructor.recoverySolutions();
        		constructor.showOtherFaq();
        		constructor.searchFAQS();
        		constructor.incrementNumberAllFaqsShow();
        		constructor.getshowMoreNumberAllFaqs();
        		constructor.selectLanguage();
        		
	    	}
    	}
    	
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}