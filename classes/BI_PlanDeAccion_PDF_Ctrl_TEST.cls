@isTest
private class BI_PlanDeAccion_PDF_Ctrl_TEST {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Test method for BI_PlanDeAccion_PDF_Ctrl contrller   
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    04/02/2015                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void BI_PlanDeAccion_PDF_Ctrl_Test() {
        BI_Dataload.set_ByPass(false,false);
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'CHL'});
        system.assert(!lst_acc.isEmpty());

        List<BI_Plan_de_accion__c> lst_coord = BI_Dataload.loadCustomPlanDeAccion(1, lst_acc[0]);
        system.assert(!lst_coord.isEmpty());

        Test.StartTest();

        try{
            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(lst_coord[0]);
            BI_PlanDeAccion_PDF_Ctrl constructor = new BI_PlanDeAccion_PDF_Ctrl(stdCtrl);
        }catch(Exception exc){
            system.assert(false);
        }

        Test.StopTest();
    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}