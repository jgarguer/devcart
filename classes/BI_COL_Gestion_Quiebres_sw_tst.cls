/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for BI_COL_Gestion_Quiebres_sw
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-30      Raul Mora (RM)				    Create Class
* @version   2.0    2015-08-28      Raul Mora (RM)				    Ajuste crear data desde usuario diferente de pais Colombia
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
                    20/09/2017      Angel F. Santaliestra           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private class BI_COL_Gestion_Quiebres_sw_tst 
{
	Public static list <UserRole> 							lstRoles;
	Public static User 										objUsuario;
	public static BI_COL_Modificacion_de_Servicio__c 		objModServ;
	public static List <Profile> 							lstPerfil;
	public static List <User>	 							lstUsuarios;
	public static Contact                               	objContacto;
	public static BI_Col_Ciudades__c                    	objCiudad;
    public static BI_Sede__c                            	objSede;
    public static BI_Punto_de_instalacion__c            	objPuntosInsta;
	
	public static void createData()
	{
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass;
				
			BI_COL_PerfilesUsuario__c objPerfUser = new BI_COL_PerfilesUsuario__c();
			objPerfUser.Name = 'Ingeniero';
			objPerfUser.BI_COL_idPerfil__c = UserInfo.getProfileId();
			insert objPerfUser;
			
			Account objAcc = new Account();
			objAcc.Name = 'Test Account';
			objAcc.BI_Country__c = 'Colombia';
			objAcc.TGS_Region__c = 'América';
			objAcc.BI_Tipo_de_identificador_fiscal__c = 'CC';
			objAcc.BI_No_Identificador_fiscal__c = '1234567787';
			objAcc.BI_Activo__c = 'Si';
            objAcc.BI_Segment__c                         = 'test';
            objAcc.BI_Subsegment_Regional__c             = 'test';
            objAcc.BI_Territory__c                       = 'test';
			insert objAcc;

			//Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
            objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objAcc.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
        	//REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
            //REING_FIN
            Insert objContacto; 
			
			//Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);

            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);

            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objAcc.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name      = 'QA Erroro';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);
			
			Opportunity objOpp = new Opportunity();
			objOpp.Name = 'Opp Test';
			objOpp.AccountId = objAcc.Id;
			objOpp.StageName = 'F5 - Solution Definition';
			objOpp.CloseDate = System.today().addDays(2);
			objOpp.BI_Country__c = 'Colombia';
			insert objOpp;
			
			BI_COL_Descripcion_de_servicio__c objDesSer = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c = objOpp.Id;
			insert objDesSer;

			objModServ                                       = new BI_COL_Modificacion_de_Servicio__c();
            objModServ.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objModServ.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
            objModServ.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModServ.BI_COL_Bloqueado__c                   = false;
            objModServ.BI_COL_Estado__c                      = 'Activa';//label.BI_COL_lblActiva;
            objModServ.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModServ.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            insert objModServ;
			
			BI_COL_Seguimiento_Quiebre__c objSegQui = new BI_COL_Seguimiento_Quiebre__c();
			objSegQui.BI_COL_Codigo_Quiebre_Sisgot__c = 'Test123';
			objSegQui.BI_COL_Modificacion_de_Servicio__c = objModServ.Id;
			objSegQui.BI_COL_Estado_de_Gestion__c = 'Abierto';
			objSegQui.BI_COL_Observaciones__c = 'Test';
			insert objSegQui;
		
	}

    static testMethod void myUnitTest() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
            Test.startTest();
            BI_COL_Gestion_Quiebres_sw_tst clsTest = new BI_COL_Gestion_Quiebres_sw_tst();
            //BI_COL_Modificacion_de_Servicio__c objMS = clsTest.createData();
            createData();
            BI_COL_Modificacion_de_Servicio__c objMSQ = [ Select Id, Name From BI_COL_Modificacion_de_Servicio__c Where Id =: objModServ.Id ];
            
            List<BI_COL_Gestion_Quiebres_sw.UsuariosResponse> lstUserResp = new List<BI_COL_Gestion_Quiebres_sw.UsuariosResponse>();
            lstUserResp = BI_COL_Gestion_Quiebres_sw.ConsultarUsarios();
            
            BI_COL_Gestion_Quiebres_sw.GestionQuiebrestRequest objGQReq = new BI_COL_Gestion_Quiebres_sw.GestionQuiebrestRequest();
            objGQReq.Causal = 'Test';
            objGQReq.CodTrs = 'Test123';
            objGQReq.Ds = 'Test';
            objGQReq.Estado = 'Aplazada';
            objGQReq.FechaAplazamiento = System.today();
            objGQReq.IdUsuario = UserInfo.getUserId();
            objGQReq.Ms = objMSQ.Name;
            objGQReq.Observaciones = 'Test';
            
            BI_COL_Gestion_Quiebres_sw.GestionQuiebrestResponse objGQRes = BI_COL_Gestion_Quiebres_sw.insertarQuiebre( objGQReq );
            BI_COL_Gestion_Quiebres_sw.GestionQuiebrestResponse objGQRes2 = BI_COL_Gestion_Quiebres_sw.actualizarQuiebre( objGQReq );
            
            objGQReq.Ms = 'Test';
            BI_COL_Gestion_Quiebres_sw.GestionQuiebrestResponse objGQRes3 = BI_COL_Gestion_Quiebres_sw.insertarQuiebre( objGQReq );
            Test.stopTest();
        }
        
    }
}