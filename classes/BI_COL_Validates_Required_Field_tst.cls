/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for BI_COL_Validates_Required_Field_cls
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-27      Raul Mora (RM)                  Create Class      
*            1.1    2017-02-01      Pedro Párraga                   Increase in coverage
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
		            20/09/2017      Angel F. Santaliestra           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private class BI_COL_Validates_Required_Field_tst 
{

    public static List<BI_COL_Modificacion_de_Servicio__c> lstModServCreate;
    public static list <UserRole> lstRoles;
    public static User objUsuario = new User();
    public static Contact                               objContacto;
    public static BI_Col_Ciudades__c                    objCiudad;
    public static BI_Sede__c                            objSede;
    public static BI_Punto_de_instalacion__c            objPuntosInsta;
    public static BI_COL_Anexos__c                                  objAnexos;
    
    public static void createData()
    {
            BI_bypass__c objBibypass = new BI_bypass__c();
            objBibypass.BI_migration__c = true;
            insert objBibypass;
            
            Account objAccount = new Account();      
            objAccount.Name = 'prueba';
            objAccount.BI_Country__c = 'Colombia';
            objAccount.TGS_Region__c = 'América';
            objAccount.BI_Tipo_de_identificador_fiscal__c = '';
            objAccount.CurrencyIsoCode = 'COP'; 
            objAccount.BI_Fraude__c = false;
            objAccount.BI_Segment__c                         = 'test';
            objAccount.BI_Subsegment_Regional__c             = 'test';
            objAccount.BI_Territory__c                       = 'test';
            insert objAccount;

            Opportunity objOpp = new Opportunity();
            objOpp.Name = 'prueba opp';
            objOpp.AccountId = objAccount.Id;
            objOpp.BI_Country__c = 'Colombia';
            objOpp.CloseDate = System.today().addDays(+5);
            objOpp.StageName = 'F5 - Solution Definition';
            objOpp.CurrencyIsoCode = 'COP';
            objOpp.Certa_SCP__contract_duration_months__c = 12;
            objOpp.BI_Plazo_estimado_de_provision_dias__c = 0 ;
            //objOpp.OwnerId = lstUser[0].id;
            insert objOpp;      

            NE__Order__c objOrder =  new NE__Order__c();
            objOrder.NE__OptyId__c = objOpp.Id; 
            objOrder.NE__OrderStatus__c='Active';
            insert objOrder;    

            RecordType objRecTyp = [ Select Id From RecordType Where sObjectType = 'NE__Product__c' Limit 1 ];

            NE__Product__c objProd = new NE__Product__c();
            objProd.Name = Constants.PRODUCT_SMDM_INVENTORY;
            objProd.BI_COL_TipoRegistro__c = objRecTyp.Id;
            objProd.BI_COL_LegadoID__c='Legado';
            insert objProd;

            NE__OrderItem__c objOrdItm  = new NE__OrderItem__c();
            objOrdItm.NE__OrderId__c = objOrder.Id; 
            objOrdItm.CurrencyIsoCode = 'USD';
            objOrdItm.NE__OneTimeFeeOv__c = 100;
            objOrdItm.NE__RecurringChargeOv__c = 100;
            objOrdItm.Fecha_de_reasignaci_n_a_factura__c = date.today();
            objOrdItm.NE__Qty__c = 1;
            objOrdItm.NE__ProdId__c = objProd.Id;
            insert objOrdItm;       

            BI_COL_Descripcion_de_servicio__c objDescServ = new BI_COL_Descripcion_de_servicio__c();
            objDescServ.BI_COL_Codigo_paquete__c = '123456789';
            objDescServ.BI_COL_Producto_Telefonica__c = objOrdItm.Id;
            objDescServ.BI_COL_Estado_de_servicio__c = 'Inactiva';
            objDescServ.BI_COL_Oportunidad__c = objOpp.Id;
            insert objDescServ; 
            
                //Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
            objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objAccount.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
        	//REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.MobilePhone          				= '1236547890';
            objContacto.Phone						= '11111111';
            objContacto.Email						= 'test@test.com';
       		objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
          		
       		//REING_FIN
        	
            Insert objContacto; 
            
            //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);

            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);

            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objAccount.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name      = 'QA Erroro';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);

            List<RecordType> rtBI_FUN = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];

            objAnexos               = new BI_COL_Anexos__c();
            objAnexos.Name          = 'FUN-0041414';
            objAnexos.RecordTypeId  = rtBI_FUN[0].Id;
            insert objAnexos;

            BI_COL_Descripcion_de_servicio__c objDesSer         = new BI_COL_Descripcion_de_servicio__c();
            objDesSer.BI_COL_Oportunidad__c                     = objOpp.Id;
            objDesSer.CurrencyIsoCode                           = 'COP';
            insert objDesSer;
            
            lstModServCreate = new List<BI_COL_Modificacion_de_Servicio__c>(); 

            BI_COL_Modificacion_de_Servicio__c objModServ1                                       = new BI_COL_Modificacion_de_Servicio__c();
            objModServ1.BI_COL_FUN__c                         = objAnexos.Id;
            objModServ1.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objModServ1.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
            objModServ1.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModServ1.BI_COL_Bloqueado__c                   = false;
            objModServ1.BI_COL_Estado__c                      = 'Enviado';//label.BI_COL_lblActiva;
            objModServ1.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModServ1.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            //insert objModServ1;
            lstModServCreate.add( objModServ1 );

            //BI_COL_Modificacion_de_Servicio__c objModServ = new BI_COL_Modificacion_de_Servicio__c();
            //objModServ.BI_COL_Estado__c = 'Enviado';
            //objModServ.BI_COL_Clasificacion_Servicio__c = 'ALTA';
            //objModServ.BI_COL_Cargo_fijo_mes__c = 100;
            //objModServ.BI_COL_Delta_modificacion_upgrade__c = 50;
            //objModServ.BI_COL_Producto__c = objOrdItm.Id;
            //objModServ.BI_COL_Codigo_unico_servicio__c = objDescServ.Id;
            //lstModServCreate.add( objModServ );  

            BI_COL_Modificacion_de_Servicio__c objModServ2     = new BI_COL_Modificacion_de_Servicio__c();
            objModServ2.BI_COL_FUN__c                         = objAnexos.Id;
            objModServ2.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objModServ2.BI_COL_Clasificacion_Servicio__c      = 'SERVICIO INGENIERIA';
            objModServ2.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModServ2.BI_COL_Bloqueado__c                   = false;
            objModServ2.BI_COL_Estado__c                      = 'Enviado';//label.BI_COL_lblActiva;
            objModServ2.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModServ2.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            objModServ2.BI_COL_Producto__c                    = objOrdItm.Id;
            //insert objModServ2;
            lstModServCreate.add( objModServ2 );

            //BI_COL_Modificacion_de_Servicio__c objModServ2 = new BI_COL_Modificacion_de_Servicio__c();
            //objModServ2.BI_COL_Estado__c = 'Enviado';
            //objModServ2.BI_COL_Clasificacion_Servicio__c = 'SERVICIO INGENIERIA';
            //objModServ2.BI_COL_Cargo_fijo_mes__c = 100;
            //objModServ2.BI_COL_Delta_modificacion_upgrade__c = 50;
            //objModServ2.BI_COL_Producto__c = objOrdItm.Id;
            //objModServ2.BI_COL_Codigo_unico_servicio__c = objDescServ.Id;
            //lstModServCreate.add( objModServ2 );    
            
            insert lstModServCreate;
            
            
            lstModServCreate=[select BI_COL_FUN__c, BI_COL_Producto_Anterior__r.BI_COL_Viaja_TRS__c, BI_COL_Medio_Vendido__c, BI_COL_Codigo_unico_servicio__c,BI_COL_Monto_ejecutado__c,
                                                            BI_COL_Oportunidad__c,BI_COL_Bloqueado__c,BI_COL_Estado__c,BI_COL_Sucursal_de_Facturacion__c,
                                                            BI_COL_Sucursal_Origen__c,BI_COL_Medio_Preferido__c,BI_COL_Clasificacion_Servicio__c,
                                                            BI_COL_TRM__c, BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c from BI_COL_Modificacion_de_Servicio__c where id =: objModServ2.id];
            BI_COL_CamposObligatorios__c objCamObl = new BI_COL_CamposObligatorios__c();
            objCamObl.BI_COL_Objeto__c = 'BI_COL_Modificacion_de_Servicio__c';
            objCamObl.BI_COL_Campo_Api__c = 'BI_COL_Estado__c';
            objCamObl.BI_COL_Nombre_Etiqueta__c  = 'Estado';
            objCamObl.BI_COL_Codigo_Referencia__c  = 'Legado';
            objCamObl.Name  = 'Test campo obli';
            insert objCamObl;
    }

    static testMethod void myUnitTest() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            createData();
            Test.startTest();
            System.debug('####StartTEST');
                BI_COL_Validates_Required_Field_cls.fnValidates_Required_Field_MS( lstModServCreate );
            Test.stopTest();
        }
    }
    
}