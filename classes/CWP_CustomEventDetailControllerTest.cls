/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis 
    Company:       Everis
    Description:   Test Methods executed to test CWP_CustomEventDetailCtrl.cls 
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    15/03/2016              Everis                   Initial Version
    
--------------------------------------------------------------------------------------------------------------------------------------------------------*/


@isTest
public without sharing class CWP_CustomEventDetailControllerTest {
	 @testSetup 
    private static void dataModelSetup() {          
       
        //USER
        UserRole rolUsuario;
        User unUsuario;
        User otroUsuario;
        Profile profileTGS;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            profileTGS = CWP_TestDataFactory.getProfile('TGS System Administrator');
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, profileTGS.Id);
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
            //ACCOUNT
            map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
        
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        Contact contactTest1 = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest1');
        
        contactTest.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest;
        contactTest1.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest1;
        
        User usuario;
        /*
        User usuarioNoTGS;
        
         //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 't1';
        producto.TGS_CWP_Tier_2__c = 't2';
        insert producto;
        list<NE__Catalog_Item__c> ciList = new list<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
        ciList.add(catalogoItem);
        
        NE__Catalog_Item__c catalogoItem1 = CWP_TestDataFactory.createCatalogoItem('ProductoTB', catalogCategoryChildren.id, catalogo.id, producto.id);
        catalogoItem1.NE__Technical_Behaviour__c = 'Service with pre-approved changes';
        ciList.add(catalogoItem1);
        insert ciList;
        
        NE__Contract_Header__c newCH = new NE__Contract_Header__c(
            NE__name__c = 'theContract'
        );
        insert newCH;
        
        NE__Contract_Account_Association__c newCAA = new NE__Contract_Account_Association__c(
            NE__Contract_Header__c = newCH.id,
            NE__Account__c = accLegalEntity.id
        );
        insert newCAA;
        
        NE__Contract__c newC =new  NE__Contract__c(
            NE__Contract_Header__c = newCH.id
        );
        insert newC;
        
        NE__Contract_Line_Item__c newCII = new NE__Contract_Line_Item__c(
            NE__Commercial_Product__c = producto.id,
            NE__Contract__c = newC.id
        );
        insert newCII;
        
        
        
        
          //ORDER
        
        NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        insert testOrder;
        //createOrderItem(id accountId, id orderId, id productId, id catalogItem, integer quantity){
        NE__OrderItem__c newOI;
        list<NE__OrderItem__c> oiList = new list<NE__OrderItem__c>();
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
        oiList.add(newOI);
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[1].id, 1);
        newOI.NE__City__c = 'Gondor';
        newOI.NE__city__c = 'Middle-earth';
        oiList.add(newOI);
        insert oiList;
        
        
        //CASE
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
		insert newCase;       
        
        */
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');       
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }/*
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil2 = CWP_TestDataFactory.getProfile('BI_Customer Communities');       
            usuarioNoTGS = CWP_TestDataFactory.createUser('nombre3', null, perfil2.id);
            usuarioNoTGS.contactId = contactTest1.id;
            insert usuarioNoTGS;
        }
         
        CaseComment Comment = new CaseComment(ParentId = newCase.Id, CommentBody = 'new comment');
        insert Comment;
        
         Market_Place_PP__c newMarketplace = new Market_Place_PP__c();
        insert newMarketplace;
        */
        System.runAs(unUsuario){ 
        	
	        Event__c eventObject1=new Event__c();
			eventObject1.Subject__c='S1';
			eventObject1.Description__c='D1';
			String fecha = '2017-05-10';
			String initHour = '08:00';
			String finishHour = '08:30';
			String webDate = null;      
	        CWP_TestDataFactory.createEvent(unUsuario.Id, eventObject1,fecha, initHour, finishHour, webdate);
	            
	        Event__c eventObject2=new Event__c();
			eventObject2.Subject__c='S2';
			eventObject2.Description__c='D2';
			String fecha2 = '2017-05-10';
			String initHour2 = '09:00';
			String finishHour2 = '09:30';
			String webDate2 = null;      
	        CWP_TestDataFactory.createEvent(unUsuario.Id, eventObject2,fecha2, initHour2, finishHour2, webdate2);
	                  
	        Event__c eventObject3=new Event__c();
			eventObject3.Subject__c='S3';
			eventObject3.Description__c='D3';
			String fecha3 = '2017-05-11';
			String initHour3 = '08:00';
			String finishHour3 = '08:30';
			String webDate3 = null;      
	        CWP_TestDataFactory.createEvent(unUsuario.Id, eventObject3,fecha3, initHour3, finishHour3, webdate3);
	        
        }
        
     
     }

	@isTest static void doSaveTest() {			
		system.debug('test 1');
        Event__c eventObject=new Event__c();
		EventObject.Subject__c='testCalendarS';
		EventObject.Description__c='testCalendarD';
		String fecha = '2017-06-06';
		String initHour = '08:00';
		String finishHour = '08:30';
		String webDate = null;
		
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];
      
        System.runAs(usuario){                          
            Test.startTest();     
            CWP_CustomEventDetailCtrl.CustomEventDetailWrapper aux = new CWP_CustomEventDetailCtrl.CustomEventDetailWrapper();
            aux.eventId='';
            aux.day='';
            CWP_CustomEventDetailCtrl.doSave(JSON.serialize(eventObject), fecha,String.valueOf(usuario.Id),initHour,finishHour,webDate);   
            Test.stopTest();
        }
	}
	
    @isTest static void doSaveTestUpdate() {			
		system.debug('test 1');
        User unusuario = [SELECT id FROM User WHERE FirstName =: 'nombre0']; 
        system.debug('USUARIO ID: ' + unusuario.id);
        list<Event> evtList = [Select Id From Event where createdById = :unusuario.Id];
        Event event=evtList.get(0);    
        String fecha = '2017-06-06';
		String initHour = '08:00';
		String finishHour = '08:30';
		String webDate = null;
        
        System.runAs(unusuario){                          
            Test.startTest();     
            CWP_CustomEventDetailCtrl.doSave(JSON.serialize(event), fecha,String.valueOf(unusuario.Id),initHour,finishHour,webDate);   
            Test.stopTest();
        }
	}
    
     @isTest static void doSaveTestUpdate2() {			
		system.debug('test 1');
        User unusuario = [SELECT id FROM User WHERE FirstName =: 'nombre0']; 
        system.debug('USUARIO ID: ' + unusuario.id);
        list<Event> evtList = [Select Id From Event where createdById = :unusuario.Id];
        Event event=evtList.get(0);    
        String fecha = '2017-06-06';
		String initHour = '08:00';
		String finishHour = '08:30';
		String webDate = null;
        
        System.runAs(unusuario){                          
            Test.startTest();     
            CWP_CustomEventDetailCtrl.doSave(JSON.serialize(event), fecha,'',initHour,finishHour,webDate);   
            Test.stopTest();
        }
	}
    
	@isTest static void getLoadInfoTest() {		
		
		system.debug('test 2');
		Test.startTest();
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre0']; 
        system.debug('USUARIO ID: ' + usuario.id);
        list<Event> evtList = [Select Id From Event where createdById = :usuario.Id];
      	
      	
      	Event event=evtList.get(0);
      	
        System.runAs(usuario){                       
          	CWP_CustomEventDetailCtrl.getLoadInfo(event.Id, false,'2017-06-06');  
            Test.stopTest();
        }   
	}
	
	  @isTest
    private static void getEventTest() {     
        system.debug('test 3');
        
          Test.startTest(); 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre0']; 
        list<Event> evtList = [Select StartDateTime, IsVisibleInSelfService, ActivityDate, subject, description, id, EndDateTime,CreatedById,OwnerId, BI_ParentId__c From Event where createdById = :usuario.Id];
      	Event event=evtList.get(0);
      
        System.runAs(usuario){                          
                                 
            CWP_CustomEventDetailCtrl.getEvent(event.Id);   
            Test.stopTest();
        }       
        
    } 
    
     @isTest
    private static void getEventTest2() {     
        
          Test.startTest(); 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre0']; 

      
        System.runAs(usuario){                          
                                 
            CWP_CustomEventDetailCtrl.getEvent('');   
            Test.stopTest();
        }       
        
    } 
   
     @isTest
    private static void formatHoursTest() {     
        system.debug('test 4');
         Test.startTest(); 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
      
        System.runAs(usuario){                       
            CWP_CustomEventDetailCtrl.formatHours(5,23);   
            Test.stopTest();
        }       
        
    } 
    
    
        @isTest
    private static void getUsersTest() {     
        system.debug('test 6');
         Test.startTest(); 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre0']; 
        list<Event> evtList = [Select StartDateTime, IsVisibleInSelfService, ActivityDate, subject, description, id, EndDateTime,CreatedById,OwnerId, BI_ParentId__c From Event where createdById = :usuario.Id];
      	Event event=evtList.get(0); 
      
        System.runAs(usuario){                      
            CWP_CustomEventDetailCtrl.getUsers(event.Id);   
            Test.stopTest();
        }       
        
    } 
    
        @isTest
    private static void getSelectedUserTest() {     
        system.debug('test 7');
        Test.startTest(); 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre0']; 
        list<Event> evtList = [Select StartDateTime, IsVisibleInSelfService, ActivityDate, subject, description, id, EndDateTime,CreatedById,OwnerId, BI_ParentId__c From Event where createdById = :usuario.Id];
      	Event event=evtList.get(0);
        /*EventRelation er = new EventRelation();
        er.EventId = event.Id;
        er.RelationId = '';
        insert er;*/
        System.runAs(usuario){                      
            CWP_CustomEventDetailCtrl.getSelectedUser(event.Id);   
            Test.stopTest();
        }       
        
    } 
    
          @isTest
    private static void mapMonthNumberTest() {     
        system.debug('test 9');
		Test.startTest(); 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
      
        System.runAs(usuario){                      
            CWP_CustomEventDetailCtrl.mapMonthNumber('JAN');   
            Test.stopTest();
        }       
        
    } 
    
          @isTest
    private static void getHoursTest() {     
        system.debug('test 10');
		Test.startTest(); 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
      
        System.runAs(usuario){                     
            CWP_CustomEventDetailCtrl.getHours();   
            Test.stopTest();
        }       
        
    } 
    
           @isTest
    private static void getDeleteEventTest() {     
        system.debug('test 11');
		  Test.startTest(); 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre0']; 
        list<Event> evtList = [Select StartDateTime, IsVisibleInSelfService, ActivityDate, subject, description, id, EndDateTime,CreatedById,OwnerId, BI_ParentId__c From Event where createdById = :usuario.Id];
      	Event event=evtList.get(0);
      	
        System.runAs(usuario){                     
            CWP_CustomEventDetailCtrl.getDeleteEvent(event.Id);   
            Test.stopTest();
        }       
        
    } 
    
           @isTest
    private static void getEmpetyInfo() {     
        system.debug('test 12');
		Test.startTest();  
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
      
        System.runAs(usuario){                          
                                
            CWP_CustomEventDetailCtrl.getEmpetyInfo('2017-06-06');   
            Test.stopTest();
        }       
        
    } 
    

}