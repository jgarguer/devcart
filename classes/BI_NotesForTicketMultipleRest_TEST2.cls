@isTest (seeAllData = true)
private class BI_NotesForTicketMultipleRest_TEST2 {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_NotesForTicketMultipleRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    22/09/2014              Pablo Oliva             Initial Version
    17/08/2015              Francisco Ayllon        Solved problems with class BI_TestUtils (whole class mod)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_NotesForTicketMultipleRest.createNote()
    History:
    
    <Date>              <Author>                <Description>
    22/09/2014          Pablo Oliva             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/   
    
    static testMethod void createNoteTest() {
    
        BI_TestUtils.throw_exception = false;
        
        Id adminId = BI_DataLoad.searchAdminProfile();
        
        BI_MigrationHelper.skipAllTriggers(); //JEG
        List <User> lst_user = BI_dataload.loadUsers(1, adminId, Label.BI_Administrador);
        
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');                                                
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        BI_MigrationHelper.cleanSkippedTriggers(); //JEG
        
        List <Case> lst_tickets = BI_DataLoadRest.loadTickets(1, lst_acc[0].Id, lst_region[0]);

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/ticketing/v1/tickets/'+lst_tickets[0].BI_Id_del_caso_Legado__c+'/notes';  
        req.httpMethod = 'POST';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        //BAD REQUEST countryIso 
        BI_NotesForTicketMultipleRest.createNote();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //INTERNAL_SERVER_ERROR
        Blob body = Blob.valueof('types": "Administrativo", "status": "active", "name": "Test value", "legalId": { "docType": "Otros", "docNumber": "1234567", "country": "" }, "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "address": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null }], "additionalData": null, "accounts": [""] }');
        req.requestBody = body;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_NotesForTicketMultipleRest.createNote();
        system.assertEquals(500,RestContext.response.statuscode);
        
        //MISSING REQUIRED FIELDS
        body = Blob.valueof('{ "author": "1000", "text": "prueba", "additionalData": null }');
        req.requestBody = body;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_NotesForTicketMultipleRest.createNote();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //MISSING REQUIRED FIELDS 2
        body = Blob.valueof('{ "author": "1000", "text": "prueba", "additionalData": [ { "Key": "test", "value": "test2" } ] }');
        req.requestBody = body;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_NotesForTicketMultipleRest.createNote();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //AUTHOR NOT FOUND
        body = Blob.valueof('{ "author": "test", "text": "prueba", "additionalData": [ { "Key": "noteId", "value": "test2" } ] }');
        req.requestBody = body;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_NotesForTicketMultipleRest.createNote();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //TICKET NOT FOUND
        body = Blob.valueof('{ "author": "'+lst_user[0].BI_CodigoUsuario__c+'", "text": "prueba", "additionalData": [ { "Key": "noteId", "value": "test2" } ] }');
        req.requestURI = '/ticketing/v1/tickets/test/notes'; 
        req.requestBody = body;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_NotesForTicketMultipleRest.createNote();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/ticketing/v1/tickets/'+lst_tickets[0].BI_Id_del_caso_Legado__c+'/notes'; 
        req.requestBody = body;
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        
        BI_NotesForTicketMultipleRest.createNote();
        system.assertEquals(201,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
}