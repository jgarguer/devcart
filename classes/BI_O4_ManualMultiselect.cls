/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Jose Miguel Fierro
 Company:       New Energy Aborda
 Description:   Component that emulates a standard multipicklist
 Keywords:      Multipicklist, multiselect
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 21/07/2016          Jose Miguel Fierro         Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_ManualMultiselect {

	public Boolean Rendered {get; set;}

	/** List that represents the selected options */
	public List<String> backingList; // getters and setters underneath

	/** All the values the picklist can have */
	public List<SelectOption> AllValues {get; set {
		this.AllValues = value;
		setOptions();
	}}

	/** The values that are currently selected */
	public List<SelectOption> SelectedValues {get; set {
		this.SelectedValues = value;
		setOptions();
	}}

	public List<SelOpt> LeftOptions  {get; set;}
	public List<SelOpt> RightOptions {get; set;}

	public void setOptions() {
		if(SelectedValues == null || AllValues == null) {
			return; // Do not create the left and right lists until both have a value
		}
		if(backingList == null) backingList = new List<String>();

		if(backingList.size() == 0) {
			for(SelectOption opt : SelectedValues) {
				backingList.add(opt.getValue());
			}
			setBackingList(backingList);
		}
	}

	// Called by VF
	public List<String> getBackingList() {
		return backingList;
	}
	public void setBackingList(List<String> value){
		this.backingList = value != null ? value : new List<String>();

		SelectedValues.clear();
		LeftOptions  = new List<SelOpt>();
		RightOptions = new List<SelOpt>();
		Set<String> setSelVals = new Set<String>(backingList);

		for(Integer i = 0; i < AllValues.size(); i++) {
			SelectOption so = AllValues[i];
			if(setSelVals.contains(so.getValue())) {
				RightOptions.add(new SelOpt(i, true, so.getDisabled(), so.getLabel(), so.getValue()));
				SelectedValues.add(so);
			} else {
				LeftOptions.add(new SelOpt(i, false, so.getDisabled(), so.getLabel(), so.getValue()));
			}
		}
	}

	public class SelOpt {
		public Integer Index	{get; set;}
		public Boolean Selected	{get; set;}
		public Boolean Disabled	{get; set;}
		public String  Label	{get; set;}
		public String  Value	{get; set;}

		public SelOpt(Integer i, Boolean s, Boolean d, String l, String v) {
			Index = i;
			Selected = s;
			Disabled = d;
			Label = l;
			Value = v;
		}
	}
}