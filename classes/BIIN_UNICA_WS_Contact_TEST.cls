@isTest(seeAllData = false)
public class BIIN_UNICA_WS_Contact_TEST 
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        José Luis González Beltrán
  Company:       HPE
  Description:   Test method to manage the code coverage for BIIN_UNICA_WS_Contact
  
  <Date>             <Author>                        			<Change Description>
  20/06/2016         José Luis González Beltrán           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
  private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
  private final static String LE_NAME = 'TestAccount_BIIN_UNICA_WS_Contact_TEST';
  private static final String CONTACT_EMAIL = 'Test_BIIN_UNICA_WS_Contact_TEST@email.com';
  private static final String CONTACT_EMAIL_1 = 'Test_BIIN_UNICA_WS_Contact_TEST_1@email.com';

  @testSetup static void dataSetup() 
  {
  	Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
    insert account;

    Contact con = BIIN_UNICA_TestDataGenerator.generateContact(CONTACT_EMAIL, account.Id);
    insert con;

    Contact conNoSede = BIIN_UNICA_TestDataGenerator.generateContact(CONTACT_EMAIL_1, account.Id);
    insert conNoSede;

    BI_Punto_de_instalacion__c sede = BIIN_UNICA_TestDataGenerator.generateSede('Test_BIIN_UNICA_WS_Contact_TEST', con.Id, account.Id);
    insert sede;
  }

  @isTest static void test_BIIN_UNICA_WS_Contact_TEST()
  {
  	Contact con = [SELECT Id, FirstName, LastName, Name, Estado__c, Email 
  									FROM Contact WHERE Email =: CONTACT_EMAIL LIMIT 1];
  	Contact conNoSede = [SELECT Id, FirstName, LastName, Name, Estado__c, Email
  												FROM Contact WHERE Email =: CONTACT_EMAIL_1 LIMIT 1];

    Test.startTest();

    BIIN_UNICA_WS_Contact wsContact = new BIIN_UNICA_WS_Contact();

    Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicket'));
    wsContact.updateAuthorizedUser(conNoSede, con);
    Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA(''));
    wsContact.createAuthorizedUser(con);
        
    Test.stopTest();
  }
}