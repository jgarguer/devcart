/**
**************************************************************************************************************
* @company          Avanxo
* @author           Antonio Torres href=<atorres@avanxo.com>
* @project          Telefonica BI_EN Fase 2 Colombia
* @name             BI2_COL_DeleteRecords_bch
* @description      Batchable class for massive record deletion.
* @dependencies     None
* @changes (Version)
* --------   ---   ----------   ---------------------------   ------------------------------------------------
*            No.   Date         Author                        Description
* --------   ---   ----------   ---------------------------   ------------------------------------------------
* @version   1.0   2017-02-28   Antonio Torres (AT)           Initial version.
**************************************************************************************************************
**/

global class BI2_COL_DeleteRecords_bch implements Database.Batchable<sObject>, Database.Stateful {
    global final String strQuery            {get; set;}
    global Integer intSuccessRecordCount    {get; set;}
    global Integer intFailedRecordCount     {get; set;}
    global Boolean blnSendEmailNotification {get; set;}

    global BI2_COL_DeleteRecords_bch(String strQuery) {
        this(strQuery, false);
    }

    global BI2_COL_DeleteRecords_bch(String strQuery, Boolean blnSendEmailNotification) {
        System.debug('\n\n-=#=-\n' + 'BI2_COL_DeleteRecords_bch - Constructor' + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'strQuery' + ': ' + strQuery + '\n-=#=-\n');

        this.strQuery                   = strQuery;
        this.blnSendEmailNotification   = blnSendEmailNotification;
        intSuccessRecordCount           = 0;
        intFailedRecordCount            = 0;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('\n\n-=#=-\n' + 'start' + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'strQuery' + ': ' + strQuery + '\n-=#=-\n');

        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        System.debug('\n\n-=#=-\n' + 'execute' + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'scope.size()' + ': ' + scope.size() + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'scope' + ': ' + scope + '\n-=#=-\n');
        
        List<Database.DeleteResult> lstDeleteResults = Database.delete(scope, false);
        for(Integer i = 0; i < lstDeleteResults.size(); i++) {
            if(lstDeleteResults[i].isSuccess()) {
                intSuccessRecordCount++;
            } else {
                intFailedRecordCount++;
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('\n\n-=#=-\n' + 'finish' + '\n-=#=-\n');
        
        if(blnSendEmailNotification) {
            Messaging.SingleEmailMessage objSingleEmailMessage = new Messaging.SingleEmailMessage();
            objSingleEmailMessage.setTargetObjectId(UserInfo.getUserId());
            objSingleEmailMessage.setSubject(Label.BI2_COL_DeleteRecordsEmailSubject);
            objSingleEmailMessage.saveAsActivity = false;
            objSingleEmailMessage.setPlainTextBody(Label.BI2_COL_DeleteRecordsEmailBody.replace('%Processed%', String.valueOf(intSuccessRecordCount + intFailedRecordCount))
                                                                                       .replace('%Deleted%', String.valueOf(intSuccessRecordCount))
                                                                                       .replace('%NotDeleted%', String.valueOf(intFailedRecordCount)));
            
            System.debug('\n\n-=#=-\n' + 'objSingleEmailMessage' + ': ' + objSingleEmailMessage + '\n-=#=-\n');

            Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ objSingleEmailMessage });
            if(results[0].success) {
                System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'The email was sent successfully.' + '   <<<<<<<<<<\n-=#=-\n');
            } else {
                System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'The email failed to send: ' + results[0].errors[0].message + '   <<<<<<<<<<\n-=#=-\n');
            }
        }
    }
}