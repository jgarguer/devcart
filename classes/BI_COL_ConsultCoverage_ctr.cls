public class BI_COL_ConsultCoverage_ctr {
	
	 //public BI_COL_ConsultaCobertura_ctr() {   }
    
    public String idDireccion { get; set; }
    public String cobertura { get; set; }
    
    public List<SelectOption> getItemsCobertura() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Cobertura LB y BA','Cobertura LB y BA')); 
        options.add(new SelectOption('Cobertura Conectividad Básica','Cobertura Conectividad Básica')); 
        
        return options;
    }
    
    
    public PageReference ejecutar()
    {
        if(cobertura==null){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Por favor seleccione un tipo de Cobertura'));       
        return null;
        }
        
        string tipoCobertura='';
        if(cobertura=='Cobertura Conectividad Básica'){
        tipoCobertura='MPLS';
        }
    
        Boolean ir=true;
        idDireccion=ApexPages.currentPage().getParameters().get('idSucursal');
        
        if(idDireccion != null && idDireccion != '')
        {
        	System.debug('---->idDireccion '+idDireccion);
        	
	        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Procesando'+idDireccion));
	        BI_Sede__c sucConsCobert=[SELECT BI_COL_BW_Maximo__c,
	                                         BI_COL_BW_maximo2__c,
	                                         BI_COL_Direccion_Split__c,BI_COL_Estado_cobertura__c,
	                                         BI_COL_Estado_cobertura2__c,
	                                         BI_COL_Estado_callejero__c,
	                                         BI_COL_Fecha_consulta__c,
	                                         BI_COL_Fecha_consulta2__c,
	                                         BI_COL_Gestion_callejero__c,
	                                         BI_COL_Latitud__c,
	                                         BI_COL_Linea_basica__c,
	                                         BI_COL_Longitud__c,
	                                         SystemModstamp,
	                                         Name,
	                                         Id,
	                                         BI_COL_Ciudad_Departamento__r.BI_COL_Regional__c,
	                                         BI_COL_Usuario_Consulta__c,
	                                         BI_COL_Usuario_consulta2__c,
	                                         BI_COL_Conectividad_basica__c,
	                                         BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c,
	                                         BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_ciudad__c,
	                                         BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_Depto__c,
	                                         BI_COL_Placa__c,
	                                         BI_COL_Complemento_1__c,
	                                         BI_COL_Complemento_2__c,
	                                         BI_COL_Numero_de_puertos__c,
	                                         BI_COL_Numero_de_puertos2__c
											 FROM BI_Sede__c
											 where id=:idDireccion];
	            
	        User usu=[Select Name,CompanyName,Profile.Name from User where id=: userinfo.getuserid()];
	        ws_wwwTelefonicaComTelefonicaservices.TelefonicaServicesSOAP tsoap=new ws_wwwTelefonicaComTelefonicaservices.TelefonicaServicesSOAP(); 
	        tsoap.timeout_x=120000;
	        
	        ws_wwwTelefonicaComTelefonicaservices.TechnicalViabilityResponse_element respuesta=null;
	        
	        if(tipoCobertura==''){
	            sucConsCobert.BI_COL_Usuario_Consulta__c=usu.Name;
	            sucConsCobert.BI_COL_Fecha_consulta__c=system.today();
	        }else{
	            sucConsCobert.BI_COL_Usuario_consulta2__c=usu.Name;
	            sucConsCobert.BI_COL_Fecha_consulta2__c=system.today();
	        }
	                    
	            String direccionSplit='';
	            //Corta la diereccion
	            if(sucConsCobert.BI_COL_Direccion_Split__c!=null){
	            
	                direccionSplit=sucConsCobert.BI_COL_Direccion_Split__c;
	                System.debug('------>direccionSplit'+direccionSplit);
	            
	            }else{
	                direccionSplit='|||||||||';
	            }
	            //direccionSplit=direccionSplit.replace('|', '-');
	            String[] lstPar=direccionSplit.split('\\x7c');
	    
	            system.debug('----Direccion_Split__c----->'+sucConsCobert.BI_COL_Direccion_Split__c);
	            system.debug(lstPar.size()+'<<<<----split----->'+lstPar);
	            
	            if(lstPar.size()<9)
	            {
	                if(tipoCobertura==''){
	                    sucConsCobert.BI_COL_Estado_cobertura__c='Sin georeferenciar - La dirección no está georeferenciada, no es posible obtener información de cobertura';
	                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: La dirección no tiene la estructura correcta'));
	                }else{
	                    sucConsCobert.BI_COL_Estado_cobertura2__c ='Sin georeferenciar - La dirección no está georeferenciada, no es posible obtener información de cobertura';
	                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: La dirección no tiene la estructura correcta'));
	                }
	                update sucConsCobert;
	            }
	            else
	            {
	                String pathType=lstPar[0]; Integer pathNumber=Integer.valueOf(lstPar[1].trim()); String firstPathCharacters=lstPar[2];String secondPathCharacters=lstPar[3];String pathZone=lstPar[4];String crossPathType=lstPar[5];Integer crossPathNumber=Integer.valueOf(lstPar[6].trim());String firstCrossPathCharacters=lstPar[7]; String secondCrossPathCharacters=lstPar[8];String crossPathZone='';
	                if(lstPar.size()==10)
	                {
	                    crossPathZone=lstPar[9];
	                } 
	                String addressNumber=String.valueOf(sucConsCobert.BI_COL_Placa__c);String addressCharacters='KCL';Integer department=0;Integer city=0;Integer zone=0;Integer sector=0;Integer block=0;String blockSide='0';Integer spaceNumber=0;Integer serviceNumber=0;Integer psCode=0;Integer requestType=3;String complement1=sucConsCobert.BI_COL_Complemento_1__c;String complement2=sucConsCobert.BI_COL_Complemento_2__c;Integer subCityNumber=0;String subCity='';String Cod_DANE=sucConsCobert.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c;
	                if(Cod_DANE!=null && Cod_DANE.length()>=2)
	                {
	                    department=Integer.valueOf(Cod_DANE.substring(0, 2));
	                }
	                if(Cod_DANE!=null)
	                {
	                    city=Integer.valueOf(Cod_DANE);
	                }
	                if(Cod_DANE!=null && Cod_DANE.length()>=9)
	                {
	                    subCity=Cod_DANE.substring(6, 9);
	                }
	                System.debug('\n\n pathType= '+pathType+ ' pathNumber= '+pathNumber+ ' firstPathCharacters= '+ firstPathCharacters+' secondPathCharacters= '+ secondPathCharacters+
	                    ' pathZone= '+ pathZone +' crossPathType= '+ crossPathType+' crossPathNumber= '+ crossPathNumber+' firstCrossPathCharacters= '+ firstCrossPathCharacters+' secondCrossPathCharacters= '+  secondCrossPathCharacters +
	                    ' crossPathZone= '+ crossPathZone+' addressNumber= '+   addressNumber+' addressCharacters= '+ addressCharacters+' department= '+ department+' city   = '+  city +
	                    ' zone   = '+  zone +' sector = '+  sector +' block  = '+  block+' blockSide= '+    blockSide+' spaceNumber= '+ spaceNumber+' serviceNumber= '+ serviceNumber+
	                    ' psCode = '+   psCode+ ' requestType= '+ requestType+ ' complement1= '+  complement1+ ' complement2= '+  complement2+' subCityNumber= '+ subCityNumber+
	                    ' subCity='+subCity+' tipoCobertura='+tipoCobertura+'\n\n');
	                
	                try {
	                    respuesta=tsoap.TechnicalViability(
	                                         pathType,                    //String
	                                         pathNumber,                    //Integer
	                                         firstPathCharacters,           //String
	                                         secondPathCharacters,          //String
	                                         pathZone,                      //String
	                                         crossPathType,                 //String
	                                         crossPathNumber,               //Integer
	                                         firstCrossPathCharacters,      //String
	                                         secondCrossPathCharacters,     //String
	                                         crossPathZone,                 //String
	                                         addressNumber,                 //String
	                                         addressCharacters,             //String 
	                                         department,                    //Integer
	                                         city   ,                       //Integer
	                                         zone   ,                       //Integer
	                                         sector ,                       //Integer
	                                         block  ,                       //Integer
	                                         blockSide,                     //String
	                                         spaceNumber,                   //Integer
	                                         serviceNumber,                 //Integer
	                                         psCode ,                       //Integer
	                                         requestType,                   //Integer
	                                         complement1,                   //String
	                                         complement2,                   //String
	                                         subCityNumber,                 //Integer
	                                         subCity,                       //String
	                                         tipoCobertura                  //string
	                                         );
						System.debug('-------response try-------->'+respuesta);//debug para verificar estado de la respuesta
	                                         
	                }
	                catch(Exception ex)
	                {
	                    system.debug('WEBB'+ex);
	                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: No se pudo establecer comunicación con el servicio web '+ex.getMessage()));
		                    
	                    if(tipoCobertura==''){
	                        sucConsCobert.BI_COL_Estado_cobertura__c='Consulta no Exitosa - La consulta no obtuvo respuesta, favor intente más tarde';
	                    }else{
	                        sucConsCobert.BI_COL_Estado_cobertura2__c='Consulta no Exitosa - La consulta no obtuvo respuesta, favor intente más tarde';                    
	                    }
	
	                }
	                System.debug('-------response-------->'+respuesta);
	                if(respuesta!=null)
	                {
	                    System.debug('\n\n respuesta.response= '+respuesta.response+' respuesta.disponibilty='+respuesta.disponibilty+' \n\n');
	                    
	                    if(tipoCobertura==''){
	                    if(respuesta.response!=null && respuesta.response.equals('R'))
	                    {
	                        sucConsCobert.BI_COL_Linea_basica__c='SI';
	                    }
	                    else
	                    {
	                        sucConsCobert.BI_COL_Linea_basica__c='NO';
	                    }
	                    
	                   /* if(respuesta.rangeZoneId!=null && !respuesta.rangeZoneId.trim().equals('') && !respuesta.disponibilty.equals('0'))
	                    {
	                        sucConsCobert.BI_COL_Banda_Ancha__c='SI';
	                    }
	                    else
	                    {
	                        sucConsCobert.BI_COL_Banda_Ancha__c='NO';
	                    }*/
	                    sucConsCobert.BI_COL_BW_maximo__c=''+respuesta.speedDown;
	                    sucConsCobert.BI_COL_Estado_cobertura__c='Exitosa';
	                    sucConsCobert.BI_COL_Numero_de_puertos__c=''+respuesta.disponibilty;
	                    }else{
	                    
	                    if(respuesta.response!=null && respuesta.response.equals('R')){
	                            sucConsCobert.BI_COL_Conectividad_basica__c='SI';
	                    }
	                    else
	                    {
	                        sucConsCobert.BI_COL_Conectividad_basica__c='NO';
	                    }
	                    
	                    sucConsCobert.BI_COL_BW_maximo2__c=''+respuesta.speedDown;
	                    sucConsCobert.BI_COL_Estado_cobertura2__c='Exitosa';
	                    sucConsCobert.BI_COL_Numero_de_puertos2__c=''+respuesta.disponibilty;
	                    
	                    
	                    }
	                    
	                    
	                }
	                else
	                {
	                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: No se pudo establecer comunicación con el servicio web'));
	                    if(tipoCobertura==''){
	                        sucConsCobert.BI_COL_Estado_cobertura__c='Pendiente-La consulta no obtuvo respuesta, favor intente más tarde';
	                    }else{
	                        sucConsCobert.BI_COL_Estado_cobertura2__c='Pendiente-La consulta no obtuvo respuesta, favor intente más tarde';                    
	                    }
	                    
	                    system.debug('fallo');
	                }
	                //update sucConsCobert;
	            }
	
	        system.debug('actualiza'+sucConsCobert.BI_COL_Estado_cobertura__c);
	        update sucConsCobert;
	        system.debug('actualizado'+sucConsCobert.BI_COL_Estado_cobertura2__c);
	        
	        //tecSoap.ActivateOperation(entityCode, colUserId, colUserName, colUserLastname, colUserEmail, colAgentId, colScreenTargetId, ttClientId, ttClientName, ttClientLastname, ttClientSegment, ttClientSubsegment, ttAccountId, ttAccountCycle, ttPSStructure)
	        PageReference acctPage;
	        acctPage = new PageReference('/'+idDireccion);
	        acctPage.setRedirect(true);
	        return acctPage;
        
    }else{
    	
    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Necesita especificar una direccion como parametro'));
    	return null;
    }
    
   }

}