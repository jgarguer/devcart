/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Juan Carlos Terron
 Company:       New Energy Aborda
 Description:   trigger helper of opportunity to insert
 
 History:
  
 <Date>              <Author>                   <Change Description>
 20/01/2017          Juan Carlos Terron         Initial Version
 03/02/2017          Alvaro Garcia      	    Added opportunity query
 09/02/2017          Alvaro Garcia      	    Added fileds to opportunity query
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_Opportunity_TriggerHelper {

	public static Set<Id> set_User_IDS {get;set;}
	public static Set<Id> set_Account_IDS {get;set;}
	public static Set<Id> set_Opportunity_IDS {get;set;}
	public static Set<Id> set_OpportunityNEW_IDS {get;set;}
	public static Set<Id> set_OpportunityOLD_IDS {get;set;}

	public static Map<Id,User> map_User {get;set;}
	public static Map<Id,Opportunity> map_childId_OppParent {get;set;}
	public static Map<Id,Opportunity> map_OppNew {get;set;}

	public static void loader_TriggerData(List<Opportunity> newList){
		//JCT 01/02/2017 Added null pointer validation.
		if(newList!=null)
		{
			NETriggerHelper.setTriggerFired('BI_Opportunity_TriggerHelper BEFORE');

			BI_Opportunity_TriggerHelper.set_User_IDS 			= new Set<Id>();
			BI_Opportunity_TriggerHelper.set_Account_IDS 		= new Set<Id>();
			BI_Opportunity_TriggerHelper.set_Opportunity_IDS 	= new Set<Id>();
			BI_Opportunity_TriggerHelper.set_OpportunityNEW_IDS = new Set<Id>();
			BI_Opportunity_TriggerHelper.set_OpportunityOLD_IDS = new Set<Id>();

			BI_Opportunity_TriggerHelper.map_User = new Map<Id,User>();
			BI_Opportunity_TriggerHelper.map_childId_OppParent = new Map<Id,Opportunity>();
			BI_Opportunity_TriggerHelper.map_OppNew = new Map<Id,Opportunity>();

			for(Opportunity OPP : newList)
			{
				if(	OPP.OwnerId != null
					&& !BI_Opportunity_TriggerHelper.set_User_IDS.contains(OPP.OwnerId))
				{
					BI_Opportunity_TriggerHelper.set_User_IDS.add(OPP.OwnerId);
				}

				if( OPP.BI_Oportunidad_Padre__c != null && !BI_Opportunity_TriggerHelper.set_Opportunity_IDS.contains(OPP.BI_Oportunidad_Padre__c)) {
					
					BI_Opportunity_TriggerHelper.set_Opportunity_IDS.add(OPP.BI_Oportunidad_Padre__c);
				}

				if( !BI_Opportunity_TriggerHelper.set_OpportunityNEW_IDS.contains(OPP.Id)) {
					
					BI_Opportunity_TriggerHelper.set_OpportunityNEW_IDS.add(OPP.Id);
				}

			}

			BI_Opportunity_TriggerHelper.map_User = new Map<Id,User>(
				[
					SELECT 	Id, Name, Profile.Name, Pais__c, BI_Permisos__c,  Contact.AccountId, DefaultCurrencyIsoCode
					FROM 	User
					WHERE 	Id = :UserInfo.getUserId()
							Or Id IN :BI_Opportunity_TriggerHelper.set_User_IDS
				]
				);

			BI_Opportunity_TriggerHelper.map_childId_OppParent = new Map<Id,Opportunity>( [SELECT Id, BI_O4_Opportunity_Type__c, SyncedQuoteId, CurrencyIsoCode, BI_O4_Paises__c, BI_O4_Geographical_Scope__c, BI_O4_Accountability__c, 
                                                                                            (SELECT TeamMemberRole, UserId, User.Name, User.BI_O4_MNC_Unit__c FROM OpportunityTeamMembers)		
																						        FROM   Opportunity
																						        WHERE  Id IN :BI_Opportunity_TriggerHelper.set_Opportunity_IDS]);

			BI_Opportunity_TriggerHelper.map_OppNew = new Map<Id,Opportunity>( [SELECT Id, BI_Country__c, OwnerId, AccountId, BI_Oportunidad_Padre__r.AccountId, (SELECT TeamMemberRole FROM OpportunityTeamMembers), 
																					(SELECT Id, NE__Version__c, NE__OrderStatus__c, NE__AccountId__c FROM NE__Orders__r ORDER BY NE__Version__c DESC), (SELECT AccountId FROM Casos__r)
																						FROM   Opportunity
																						WHERE  Id IN :BI_Opportunity_TriggerHelper.set_OpportunityNEW_IDS]);
		}
	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Juan Carlos Terron
 Company:       New Energy Aborda
 Description:   trigger helper of opportunity to update
 
 History:
  
 <Date>              <Author>                   <Change Description>
 20/01/2017          Juan Carlos Terron         Initial Version
 03/02/2017          Alvaro Garcia      	    Added opportunity query
 09/02/2017          Alvaro Garcia      	    Added fileds to opportunity query
--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public static void loader_TriggerData(Map<Id,Opportunity> newMap,Map<Id,Opportunity> oldMap){
		//JCT 01/02/2017 Added null pointer validation.
		if(newMap!=null)
		{
			NETriggerHelper.setTriggerFired('BI_Opportunity_TriggerHelper AFTER');

			BI_Opportunity_TriggerHelper.set_User_IDS 			= new Set<Id>();
			BI_Opportunity_TriggerHelper.set_Account_IDS 		= new Set<Id>();
			BI_Opportunity_TriggerHelper.set_Opportunity_IDS 	= new Set<Id>();
			BI_Opportunity_TriggerHelper.set_OpportunityNEW_IDS = new Set<Id>();
			BI_Opportunity_TriggerHelper.set_OpportunityOLD_IDS = new Set<Id>();

			BI_Opportunity_TriggerHelper.map_User = new Map<Id,User>();
			BI_Opportunity_TriggerHelper.map_childId_OppParent = new Map<Id,Opportunity>();
			BI_Opportunity_TriggerHelper.map_OppNew = new Map<Id,Opportunity>();

			for(Opportunity OPP : newMap.values())
			{
				if(	OPP.OwnerId != null
					&& !BI_Opportunity_TriggerHelper.set_User_IDS.contains(OPP.OwnerId))
				{
					BI_Opportunity_TriggerHelper.set_User_IDS.add(OPP.OwnerId);
				}

				if(OPP.BI_Oportunidad_Padre__c != null && !BI_Opportunity_TriggerHelper.set_Opportunity_IDS.contains(OPP.BI_Oportunidad_Padre__c)) {
					
					BI_Opportunity_TriggerHelper.set_Opportunity_IDS.add(OPP.BI_Oportunidad_Padre__c);
				}

				if( !BI_Opportunity_TriggerHelper.set_OpportunityNEW_IDS.contains(OPP.Id)) {
					
					BI_Opportunity_TriggerHelper.set_OpportunityNEW_IDS.add(OPP.Id);
				}

			}

			BI_Opportunity_TriggerHelper.map_User = new Map<Id,User>(
				[
					SELECT 	Id, Name, Profile.Name, Pais__c, BI_Permisos__c,  Contact.AccountId, DefaultCurrencyIsoCode
					FROM 	User
					WHERE 	Id = :UserInfo.getUserId()
							Or Id IN :BI_Opportunity_TriggerHelper.set_User_IDS
				]
				);

			BI_Opportunity_TriggerHelper.map_childId_OppParent = new Map<Id,Opportunity>( [SELECT Id, BI_O4_Opportunity_Type__c, SyncedQuoteId, CurrencyIsoCode, BI_O4_Paises__c, BI_O4_Geographical_Scope__c, BI_O4_Accountability__c, 
                                                                                            (SELECT TeamMemberRole, UserId, User.Name, User.BI_O4_MNC_Unit__c FROM OpportunityTeamMembers)		
																						        FROM   Opportunity
																						        WHERE  Id IN :BI_Opportunity_TriggerHelper.set_Opportunity_IDS]);

			BI_Opportunity_TriggerHelper.map_OppNew = new Map<Id,Opportunity>( [SELECT Id, BI_Country__c, OwnerId, AccountId, BI_Oportunidad_Padre__r.AccountId, (SELECT TeamMemberRole FROM OpportunityTeamMembers), 
																					(SELECT Id, NE__Version__c, NE__OrderStatus__c, NE__AccountId__c FROM NE__Orders__r ORDER BY NE__Version__c DESC), (SELECT AccountId FROM Casos__r)			
																						FROM   Opportunity
																						WHERE  Id IN :BI_Opportunity_TriggerHelper.set_OpportunityNEW_IDS]);

		}
	}
}