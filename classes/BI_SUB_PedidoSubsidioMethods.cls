/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Geraldine Sofía Perez Montes (GSPM)
    Company:       Accenture LTDA
    Description:   Methods executed by BI_SUB_PedidoSubsidio Triggers 
    
    History:
     
    <Date>                  <Author>                			<Change Description>
    23/02/2018              Geraldine Sofía Perez Montes           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

public with sharing class BI_SUB_PedidoSubsidioMethods {

    

	public BI_SUB_PedidoSubsidioMethods() {
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Geraldine Sofía Perez Montes (GSPM)
    Company:       Accenture LTDA
    Description:   Metodo para actualizar los campos en despacho y enviar dependiendo del estado del pedido
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    22/02/2018                      GSPM                         Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void UpdateCaseSubsidio(List<BI_SUB_Pedido_Subsidio__c> lstPedNew,List<BI_SUB_Pedido_Subsidio__c> lstPedOld)
    {
        try
        {
            List<NE__OrderItem__c> lstOI = new List<NE__OrderItem__c>();
            NE__OrderItem__c objOIF = new NE__OrderItem__c();
            List<BI_SUB_Pedido_Subsidio__c> lstPedSub = new List<BI_SUB_Pedido_Subsidio__c>();
            List<BI_SUB_ProductosOI_PedidoSubsidio__c> lstOIxPS = new List<BI_SUB_ProductosOI_PedidoSubsidio__c>();
            List<BI_SUB_ProductosOI_PedidoSubsidio__c> lstOIxPSDel = new List<BI_SUB_ProductosOI_PedidoSubsidio__c>();
            List<NE__OrderItem__c> lstToUpdate = new List<NE__OrderItem__c>();
            set<Id> setIdOpp = new set<Id>();
            set<Id> setIdSub = new set<Id>();
            set<Id> setIdPxSP = new set<Id>();
            if(!lstPedNew.IsEmpty()){            
                //for(BI_SUB_Pedido_Subsidio__c objOINew: lstPedNew){
                for(Integer i=0; i < lstPedNew.size(); i++){
                system.debug('>Estadoo old< ' +lstPedOld[i].BI_SUB_Estado__c );
                system.debug('>Estadoo new< ' +lstPedNew[i].BI_SUB_Estado__c );
                    if(lstPedNew[i].BI_SUB_Estado__c != lstPedOld[i].BI_SUB_Estado__c ) //&& (lstPedNew[i].BI_SUB_Estado__c == 'Enviado' || lstPedNew[i].BI_SUB_Estado__c == 'Completo' || lstPedNew[i].BI_SUB_Estado__c == 'Rechazado' || lstPedNew[i].BI_SUB_Estado__c == 'Cancelado')
                    {
                        system.debug('>dentro del if< ' );
                        //setIdOpp.add(lstPedNew[i].BI_SUB_Oportunidad__c);
                        setIdSub.add(lstPedNew[i].Id);
                        
                    }
                }
           
                system.debug('\n >setIdSub< ' + setIdSub);
                lstOIxPS = [select Id, BI_SUB_Junction_Cantidad_Enviada__c, BI_SUB_Junction_Configuration_Item__c, BI_SUB_Junction_Pedido_Subsidio__c 
                from BI_SUB_ProductosOI_PedidoSubsidio__c
                where BI_SUB_Junction_Pedido_Subsidio__c =:setIdSub]; 

                for(BI_SUB_ProductosOI_PedidoSubsidio__c onjOIxPS: lstOIxPS)
                {
                    setIdPxSP.add(onjOIxPS.BI_SUB_Junction_Configuration_Item__c);
                }
                system.debug('>setIdPxSP< ' +setIdPxSP);

                lstOI = [Select Id, name, BI_SUB_Disponible__c, BI_SUB_EnDespacho__c,BI_SUB_AEnviar__c,BI_SUB_Enviados__c,NE__OrderId__c, NE__OrderId__r.NE__OptyId__c, 
                                NE__OrderId__r.NE__OptyId__r.BI_Country__c,NE__OrderId__r.NE__OptyId__r.BI_SUB_Subsidio__c
                         from NE__OrderItem__c where Id =:setIdPxSP];
                system.debug('>lstOI< ' +lstOI);
                if(!lstPedNew.IsEmpty() && !lstOI.IsEmpty())
                {
                    for(BI_SUB_Pedido_Subsidio__c objPedSub:lstPedNew)
                    {
                        for(BI_SUB_ProductosOI_PedidoSubsidio__c  objOIxPS:lstOIxPS)
                        {
                            for(NE__OrderItem__c objOI: lstOI)
                            {                                
                                if(objOI.Id == objOIxPS.BI_SUB_Junction_Configuration_Item__c)
                                {
                                    system.debug('>estado< '+objPedSub.BI_SUB_Estado__c);
                                    if(objPedSub.BI_SUB_Estado__c == 'Completo')
                                    {
                                        system.debug('>Completo< ');
                                        objOIF = new NE__OrderItem__c();
                                        objOIF.Id = objOI.Id;
                                        objOIF.BI_SUB_Enviados__c = objOI.BI_SUB_Enviados__c + objOIxPS.BI_SUB_Junction_Cantidad_Enviada__c;
                                        if(objOI.BI_SUB_EnDespacho__c > 0)
                                        {
                                            objOIF.BI_SUB_EnDespacho__c = objOI.BI_SUB_EnDespacho__c -  objOIxPS.BI_SUB_Junction_Cantidad_Enviada__c;
                                        }else{
                                            objOIF.BI_SUB_EnDespacho__c = objOIxPS.BI_SUB_Junction_Cantidad_Enviada__c;
                                        }                                        
                                        lstToUpdate.add(objOIF);
                                    }else if(objPedSub.BI_SUB_Estado__c == 'Rechazado' || objPedSub.BI_SUB_Estado__c == 'Cancelado')
                                    {
                                        system.debug('>rechazado o Cancelado< ');
                                        objOIF = new NE__OrderItem__c();
                                        objOIF.Id = objOI.Id;
                                        objOIF.BI_SUB_Disponible__c = objOI.BI_SUB_Disponible__c + objOIxPS.BI_SUB_Junction_Cantidad_Enviada__c;
                                        objOIF.BI_SUB_EnDespacho__c = objOI.BI_SUB_EnDespacho__c -  objOIxPS.BI_SUB_Junction_Cantidad_Enviada__c;
                                        lstToUpdate.add(objOIF);
                                        lstOIxPSDel = lstOIxPS;
                                    } 
                                }                  
                            }

                        }

                    }
                      System.debug('> lstOIxPSDel <' + lstOIxPSDel); 
                    Database.SaveResult[] srList = Database.update(lstToUpdate, false);
                    for(Database.SaveResult srOI: srList)
                    {
                        if(srOI.isSuccess())
                        {
                            System.debug('> Exitoso <' + srOI.getId());                                    
                        }
                    }
                    if(!lstOIxPSDel.isEmpty())
                    {
                            
                         System.debug('> Delete <');   
                        Delete lstOIxPSDel;
                    }
                }
            }
        }catch(Exception e){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Geraldine Sofía Perez Montes (GSPM)
    Company:       Accenture LTDA
    Description:   Metodo para evitar que un usuario no propietario del pedido (Usuario o cola) pueda modificar el registro
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    22/02/2018                      GSPM                         Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void NoCreatePedido(List<BI_SUB_Pedido_Subsidio__c> lstPedNew, List<BI_SUB_Pedido_Subsidio__c> lstOIOld)
    {
    	Id userId = UserInfo.getUserId();
    	//Group g = [select Id from Group where Name='My Group Name' AND Type = 'Queue'];
    	List<GroupMember> lstGM = [select GroupId, UserOrGroupId, Group.DeveloperName, Group.Name from GroupMember where Group.DeveloperName  = 'BI_SUB_TBD'];
    	integer intUsu = 0;
    	integer intGroup = 0;
    	system.debug('>lstOIOld< ' + lstOIOld);
    	if(!lstOIOld.isEmpty()){    		
	    	for(Integer i = 0; i < lstOIOld.size() ; i++){  		
	    			
    			system.debug('\n>lstOIOld[i].OwnerId< ' + lstOIOld[i].OwnerId + '\n >userId< ' + userId);
    			if(lstOIOld[i].OwnerId != userId && lstOIOld[i].Id == lstPedNew[i].Id)
    			{
    				system.debug('>intUsu entro< ' );
    				intUsu++;
    				
		    		lstPedNew[i].addError('Usted no tiene permisos para actualizar este registro');
		    		Break;
    			}	    		

	    	}
    	}else if(!lstGM.isEmpty())
    	{
    		system.debug('>lstGM<' + lstGM);    			
    		for(Integer i = 0; i < lstGM.size() ; i++)
    		{
    			system.debug('\n >userId< ' + userId + '\n >lstGM[i].UserOrGroupId< '+ lstGM[i].UserOrGroupId);    			
    			if(lstGM[i].UserOrGroupId == userId && lstOIOld[i].Id == lstPedNew[i].Id)
    			{
		    		lstPedNew[i].addError('Usted no tiene permisos para actualizar este registro');
    				system.debug('>intGroup entro< ' );
    				
    				
    			}
	    		
    		}

    	}
    }


    
}