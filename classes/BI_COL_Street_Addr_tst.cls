/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for BI_COL_Street_Addr_cls
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-27      Raul Mora (RM)				    Create Class      
*************************************************************************************/
@isTest
private class BI_COL_Street_Addr_tst 
{	
	public List<BI_Sede__c> createData()
	{		
		BI_bypass__c objBibypass = new BI_bypass__c();
		objBibypass.BI_migration__c=true;
		insert objBibypass;
			
		BI_Col_Ciudades__c objCity = new BI_Col_Ciudades__c();
		objCity.Name = 'Test City';
		objCity.BI_COL_Pais__c = 'Test Country';
		objCity.BI_COL_Codigo_DANE__c = 'TestCDa';
		insert objCity;
		
		BI_Sede__c objSede = new BI_Sede__c();
		objSede.BI_COL_Ciudad_Departamento__c = objCity.Id;
		objSede.BI_Direccion__c = 'Test Street 123 Number 321';
		objSede.BI_Localidad__c = 'Test Local';
		objSede.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor4EstadoCallejero;
		objSede.BI_COL_Sucursal_en_uso__c = 'Libre';
		objSede.BI_Country__c = 'Colombia';
		objSede.Name = 'Test Street 123 Number 321, Test Local Colombia';
		objSede.BI_Codigo_postal__c = '12356';		
		insert objSede;
		
		BI_Sede__c objSede2 = new BI_Sede__c();
		objSede2.BI_COL_Ciudad_Departamento__c = objCity.Id;
		objSede2.BI_Direccion__c = 'Test Street 123 Number 321';
		objSede2.BI_Localidad__c = 'Test Local';
		objSede2.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor3EstadoCallejero;
		objSede2.BI_COL_Sucursal_en_uso__c = 'Libre';
		objSede2.BI_Country__c = 'Colombia';
		objSede2.Name = 'Test Street 123 Number 321, Test Local Colombia';
		objSede2.BI_Codigo_postal__c = '12356';		
		insert objSede2;
		
		BI_Sede__c objSede3 = new BI_Sede__c();
		objSede3.BI_COL_Ciudad_Departamento__c = objCity.Id;
		objSede3.BI_Direccion__c = 'Test Street 123 Number 321';
		objSede3.BI_Localidad__c = 'Test Local';
		objSede3.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor3EstadoCallejero;
		objSede3.BI_COL_Sucursal_en_uso__c = 'Libre';
		objSede3.BI_Country__c = 'Colombia';
		objSede3.Name = 'Test Street 123 Number 321, Test Local Colombia';
		objSede3.BI_Codigo_postal__c = '12356';		
		insert objSede3;
		
		List<BI_Sede__c> lstReturn = new List<BI_Sede__c>();
		lstReturn.add( objSede );
		lstReturn.add( objSede2 );
		return lstReturn;
		
		
	}

    static testMethod void myUnitTest() 
    {
        BI_COL_Street_Addr_tst testClass = new BI_COL_Street_Addr_tst();
		List<BI_Sede__c> lstSede = testClass.createData();
		List<Id> lstIdSede = new List<Id>();
		lstIdSede.add( lstSede.get(0).Id );
		lstIdSede.add( lstSede.get(1).Id );
		String strRespWS = '<root><entrada><dir>'+
						'<id>'+lstIdSede.get(0)+'</id>'+
						'<Direccion>Cra 42 # 142 - 142</Direccion>'+
						'<DirSplit>-</DirSplit>'+
						'<Placa>142 142</Placa>'+
						'<Complemento1></Complemento1>'+
						'<Complemento2></Complemento2>'+
						'<CodBar>12334</CodBar>'+
						'<DesBar>12333</DesBar>'+
						'<Zona>1</Zona>'+
						'<EstadoRef>1</EstadoRef>'+
						'<NSE>1</NSE>'+
						'<DirNueva>Cra 42 # 142 - 142</DirNueva>'+
						'<CODIR>1</CODIR>'+
					'</dir></entrada></root>';
		
		
		Test.startTest();
			BI_COL_Street_Addr_cls.EjecutaCallejero( lstIdSede );
			BI_COL_Street_Addr_cls.ActualizaSucursalError( lstSede.get(0), 'TestClass');
			BI_COL_Street_Addr_cls.ActualizaSucursal( lstIdSede, lstSede, strRespWS );
			strRespWS = '<root><entrada><dir>'+
						'<id>'+lstIdSede.get(1)+'</id>'+
						'<Direccion>Cra 42 # 142 - 142</Direccion>'+
						'<DirSplit>-</DirSplit>'+
						'<Placa>142 142</Placa>'+
						'<Complemento1>Test Complement 1</Complemento1>'+
						'<Complemento2>Te:st Complement:2</Complemento2>'+
						'<CodBar>12334</CodBar>'+
						'<DesBar>12333</DesBar>'+
						'<Zona>1</Zona>'+
						'<EstadoRef>1</EstadoRef>'+
						'<NSE>1</NSE>'+
						'<DirNueva>Cra 42 # 142 - 142</DirNueva>'+
						'<CODIR>1</CODIR>'+
					'</dir></entrada></root>';
			BI_COL_Street_Addr_cls.ActualizaSucursal( lstIdSede, lstSede, strRespWS );
			BI_COL_Street_Addr_cls.validateDelete( lstSede );
			BI_COL_Street_Addr_cls.oLstOpcion lstOpc = new BI_COL_Street_Addr_cls.oLstOpcion( strRespWS );
			BI_COL_Street_Addr_cls.oOpcion objOOp = lstOpc.lstOpcion[0]; 
			BI_COL_Street_Addr_cls.crearOpcion( objOOp );
		Test.stopTest();
    }
}