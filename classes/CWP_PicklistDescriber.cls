/*
Copyright (c) 2012 tgerm.com
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, 
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
Class which client code needs to interact with. Call any of the overloaded describe method,
make sure you are passing all the params in the method signature  
@author Abhinav 
*/
public class CWP_PicklistDescriber {
    static final Pattern OPTION_PATTERN = Pattern.compile('<option.+?>(.+?)</option>'); 
    
    
    /**
Describe a picklist field for a SobjectType, its given record type developer name and the picklist field
example usage : 
List<String> options = CWP_PicklistDescriber.describe('Account', 'Record_Type_1', 'Industry'));
*/
    public static List<String> describe(String sobjectType, String recordTypeName, String pickListFieldAPIName) {
        return parseOptions(
            new Map<String, String>	{
                'sobjectType' => sobjectType,
                    'recordTypeName' => recordTypeName,
                    'pickListFieldName'=> pickListFieldAPIName
                    }
        );
    }
    
    
    
    /*
Internal method to parse the OPTIONS
*/
    static List<String> parseOptions(Map<String, String> params) {
        Pagereference pr = Page.CWP_PicklistDesc;
        // to handle development mode, if ON
        pr.getParameters().put('core.apexpages.devmode.url', '1');
        
        for (String key : params.keySet()) {
            pr.getParameters().put(key, params.get(key));	
        }
        String xmlContent;
        if(!Test.isRunningTest()){
            xmlContent= pr.getContent().toString();
             system.debug('\n\n'+ xmlContent);
        }
        else {
           xmlContent='<option value="Administrative Request">Administrative Request</option>'
+'<option value="Attention Complaint">Attention Complaint</option>'
+'<option value="Billing Complaint">Billing Complaint</option>'
+'<option value="Commercial Query">Commercial Query</option>'
+'<option value="Commercial Request">Commercial Request</option>'
+'<option value="Provision Complaint">Provision Complaint</option>'
+'<option value="Technical Query">Technical Query</option>';
        }
            
        Matcher mchr = OPTION_PATTERN.matcher(xmlContent);
        List<String> options = new List<String>();
        while(mchr.find()) {
            options.add(mchr.group(1));
        } 
        // remove the --None-- element
        if (!options.isEmpty()) options.remove(0);
        return options;
    }
}