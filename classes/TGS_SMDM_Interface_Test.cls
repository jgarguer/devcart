/*------------------------------------------------------------
Author:         Diego Sarsa 
Company:        Deloitte
Description:    This class contains unit tests for validating the behavior of
                TGS_SMDM_Interface Apex class
History
<Date>          <Author>        <Change Description>
11-feb-2015     Diego Sarsa     Initial Version
------------------------------------------------------------*/
@isTest
private class TGS_SMDM_Interface_Test {
    
    static boolean isDataSet = false;
    
    /*------------------------------------------------------------
    Author:         Diego Sarsa 
    Company:        Deloitte
    Description:    Method that test an email sent from the Mobile Iron provider
    History
    <Date>          <Author>        <Change Description>
    11-feb-2015     Diego Sarsa     Initial Version
    ------------------------------------------------------------*/
    static testMethod void handleMobileIronProvider() {
    
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        System.runAs(userTest){
        
            System.debug('***** Executing handleMobileIronProvider *****');
            
            /* Root Serial Number */ 
            String serialNumber = 'c4f371075';
            /* Number of devices in the attachment */
            Integer totalItems = 2;
            /* Header of the csv file */
            String header = 'reportmessage;action;principal;emailAddress;currentphoneNumber;platform;model_universal;os_version;' +
                            'SerialNumber;device_id;registeredat;lastConnectedAt;status\r\r\n';
              
            String email1 = 'john.doe@telefonica.com';
            String email2 = 'jane.doe@telefonica.com';
                          
            /* Generate the file's content */
            String content = '';
            for (Integer i=0; i<totalItems; i++) {
                content +=  'appname;report;wmejmm04;' + (math.mod(i, 2) == 0 ? email1 : email2) + ';680604528;Android 4.3;GT-I9505;18;' + 
                            serialNumber + i.format() + ';bec53;24/06/2014;2014-07-24T13:09:08Z;ACTIVE\r\r\n';
            }
            
            Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
            attachment.body = Blob.valueOf(header + content);
            attachment.fileName = 'provider.csv';
            attachment.mimeTypeSubType = 'text/plain';
    
            /* Call to the method for email handling */
            handleEmail('reportesmdm@gmail.com', attachment);
        }
    }
    
    
    /*------------------------------------------------------------
    Author:         Diego Sarsa 
    Company:        Deloitte
    Description:    Method that test an email sent from the Air Watch provider
    History
    <Date>          <Author>        <Change Description>
    11-feb-2015     Diego Sarsa     Initial Version
    ------------------------------------------------------------*/
    static testMethod void handleAirWatchProvider() {
    
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        System.runAs(userTest){
        
            System.debug('***** Executing handleAirWatchProvider *****');
            
            /* Root Serial Number */ 
            String serialNumber = '7W0511XAA4S';
            /* Number of devices in the attachment */
            Integer totalItems = 2;
            /* Header of the csv file */
            String header = 'OrganizationGroup,Location,FriendlyName,PhoneNumber,Platform,Model,OSVersion,CorporateEmployeeShared,' +
                            'SerialNumber,IMEIESN,UDID,MACAddress,SIMNumber,Carrier,DateEnrolled,LastSeen,UserName,EmailUserName,' +
                            'EmailAddress,ActiveUsers,EnrolledDevice,LDAPDomain,WithContent,TotalStorageCapacity,' +
                            'AvailableStorageCapacity\r\n';
            
            /* Generate the file's content */
            String content = '';
            for (Integer i=0; i<totalItems; i++) {
                content +=  'TELEFONICA INTERNATIONAL WHOLESALE SERVICES SL,,jtrinidad iPhone iOS 6.1.3 AA4S,,Apple,' +
                            'iPhone 4 GSM (Unlocked) (16 GB),Apple 6.1.3,Undefined,' + serialNumber + i.format() + ',012544000190044,' +
                            '77289A175A2A4DE0C78A41818C8BF710FAA86C94,58:1F:AA:03:D9:F6,8934075600002854098,movistar,' +
                            '17/05/2014,9/7/2014 2:13:27 PM,jtrinidad,,john.doe@telefonica.com,YES,1,,NO,' +
                            '14.00GB,13.00GB\r\n';
            }
            
            Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
            attachment.body = Blob.valueOf(header + content);
            attachment.fileName = 'provider.csv';
            attachment.mimeTypeSubType = 'text/plain';
    
            /* Call to the method for email handling */
            handleEmail('noreply@air-watch.com', attachment);
       }     
        
    }
    
    
    /*------------------------------------------------------------
    Author:         Diego Sarsa 
    Company:        Deloitte
    Description:    Method that test the sending of an email
    History
    <Date>          <Author>        <Change Description>
    11-feb-2015     Diego Sarsa     Initial Version
    ------------------------------------------------------------*/
    static void handleEmail(String fromAddress, Messaging.InboundEmail.BinaryAttachment attach) {
        /*    
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';      
        insert userTest;
        System.runAs(userTest){
        */
            /* Create a new email and envelope object */
            Messaging.InboundEmail email  = new Messaging.InboundEmail();
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
            
            setData();
            
            /* Create the email body */
            email.plainTextBody = 'This is the plain text of the body';
            email.fromAddress = fromAddress;
    
            /* Set the attachment */
            email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attach };
            
            TGS_SMDM_Interface tgsSMDMInterface = new TGS_SMDM_Interface();
            
            Test.startTest();
            Messaging.InboundEmailResult result = tgsSMDMInterface.handleInboundEmail(email, env);
            Test.stopTest();
            
            //System.assert (result.success, 'InboundEmailResult returned a failure message');
            
            /*
            Account [] accDb = [select ID from Account where name=:email.subject];
            System.assertEquals (1, accDb.size(),'Account was not inserted');
            Contact [] cDb = [select firstname,lastname from Contact where email=:contactEmail];
            System.assertEquals (1, cDb.size(),'Contact was not inserted!');
            Contact c = CDb[0];
            System.assertEquals ('Jon', c.firstName);
            System.assertEquals ('Smith', c.LastName);
            Note [] nDb = [select body from Note where ParentID=:accDb[0].id];
            System.assertEquals (1,nDb.size(), 'A note should have been attached');
            System.assertEquals (email.plainTextBody, nDb[0].body);
            */
           /* 
          }  
          */
        
    }
    
    
    /*------------------------------------------------------------
    Author:         Diego Sarsa 
    Company:        Deloitte
    Description:    Method that test the sending of an email
    History
    <Date>          <Author>        <Change Description>
    19-feb-2015     Diego Sarsa     Initial Version
    ------------------------------------------------------------*/
    static testMethod void handleEmailWithErrors() {
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        System.runAs(userTest){
            System.debug('***** Executing handleEmailWithErrors *****');
            
            /* Create a new email and envelope object */
            Messaging.InboundEmail email  = new Messaging.InboundEmail();
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
            
            /* Create the email body */
            email.subject = 'abcdef';
            
            /* Create the email body */
            email.plainTextBody = 'Plain text of the body';
            email.fromAddress = 'dummy@test.com';
            
            TGS_SMDM_Interface tgsSMDMInterface = new TGS_SMDM_Interface();
            
            Test.startTest();
            
            /* Test a not valid email address */
            Messaging.InboundEmailResult result = tgsSMDMInterface.handleInboundEmail(email, env);
           
            /* Test an email without any attachments */
            email.fromAddress = 'reportesmdm@gmail.com'; 
            Messaging.InboundEmailResult result2 = tgsSMDMInterface.handleInboundEmail(email, env);
            
            /* Test an invalid content */
            /* Establish the attachment */
            String content = 'reportmessage;action;principal;emailAddress;currentphoneNumber;platform;model_universal;' +
                             'SerialNumber;device_id;registeredat;status\r\r\nappname;report;wmejmm04;' +
                             'abc@telefonica.com;680604528;Android 4.3;GT-I9505;18;bec53;24/06/2014;ACTIVE\r\r\n';

            
            Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
            attachment.body = Blob.valueOf(content);
            attachment.fileName = 'providerError.csv';
            attachment.mimeTypeSubType = 'text/plain';
            
            /* Set the attachment */
            email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            Messaging.InboundEmailResult result3 = tgsSMDMInterface.handleInboundEmail(email, env);
            
            /* Test an account that does not linked to a user */
            /* Establish the attachment */
            content = 'reportmessage;action;principal;emailAddress;currentphoneNumber;platform;model_universal;os_version;' +
                      'SerialNumber;device_id;registeredat;lastConnectedAt;status\r\r\nappname;report;wmejmm04;' +
                      'abc@telefonica.com;680604528;Android 4.3;GT-I9505;18;123456789;bec53;24/06/2014;' +
                      '2014-07-24T13:09:08Z;ACTIVE\r\r\n';

            attachment.body = Blob.valueOf(content);
            
            /* Set the attachment */
            email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            Messaging.InboundEmailResult result4 = tgsSMDMInterface.handleInboundEmail(email, env);
            
            Test.stopTest();
            
            System.assert (!result.success, 'InboundEmailResult1 finish successfully and an error was expected.');
            System.assert (!result2.success, 'InboundEmailResult2 finish successfully and an error was expected.');
            System.assert (!result3.success, 'InboundEmailResult3 finish successfully and an error was expected.');
            System.assert (!result4.success, 'InboundEmailResult3 finish successfully and an error was expected.');
         }   
    }
    
    
    /*------------------------------------------------------------
    Author:         Diego Sarsa 
    Company:        Deloitte
    Description:    Method that test a couple email sent from the Mobile Iron provider  
    History
    <Date>          <Author>        <Change Description>
    13-feb-2015     Diego Sarsa     Initial Version
    ------------------------------------------------------------*/
    static testMethod void handleMobileIronProviderWithUpdate() {
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        System.runAs(userTest){
            System.debug('***** Executing handleMobileIronProviderWithUpdate *****');
            
            /* Root Serial Number */ 
            String serialNumber = 'c4f371076';
            /* Number of devices in the attachment. The second email will have one item less than the first!! */
            Integer totalItems = 2;
            /* Header of the csv file */
            String header = 'reportmessage;action;principal;emailAddress;currentphoneNumber;platform;model_universal;os_version;' +
                            'SerialNumber;device_id;registeredat;lastConnectedAt;status\r\r\n';
                      
            /* Generate the file's content for the first email */
            String content = '';
            for (Integer i=0; i<totalItems; i++) {
                content +=  'appname;report;wmejmm04;john.doe@telefonica.com;680604528;Android 4.3;GT-I9505;18;' + 
                            serialNumber + i.format() + ';acb753;24/06/2014;2014-07-24T13:09:08Z;ACTIVE\r\r\n';
            }
                 
            /* Generate the file's content for the second email */
            String content2 = '';
            for (Integer j=0; j<totalItems-1; j++) {
                content2 += 'appname;report;abcdef04;john.doe@telefonica.com;680604528;Android 4.4;GT-I123;18;' + 
                            serialNumber + j.format() + ';acb753;02/02/2015;2015-07-24T17:22:09Z;ACTIVE\r\r\n';
            }
            
            Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
            attachment.body = Blob.valueOf(header + content);
            attachment.fileName = 'provider1.csv';
            attachment.mimeTypeSubType = 'text/plain';
            
            Messaging.InboundEmail.BinaryAttachment attachment2 = new Messaging.InboundEmail.BinaryAttachment();
            attachment2.body = Blob.valueOf(header + content2);
            attachment2.fileName = 'provider2.csv';
            attachment2.mimeTypeSubType = 'text/plain';
    
            /* Call to the method for email handling */
            handleSomeEmails('reportesmdm@gmail.com', attachment, attachment2);
        }
    }
    
    
    /*------------------------------------------------------------
    Author:         Diego Sarsa 
    Company:        Deloitte
    Description:    Method that test the sending of multiple emails
    History
    <Date>          <Author>        <Change Description>
    11-feb-2015     Diego Sarsa     Initial Version
    ------------------------------------------------------------*/
    static void handleSomeEmails(String fromAddress, Messaging.InboundEmail.BinaryAttachment attach,
                                 Messaging.InboundEmail.BinaryAttachment attach2) {
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        System.runAs(userTest){
            /* Create a new email and envelope object */
            Messaging.InboundEmail email  = new Messaging.InboundEmail();
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
            Messaging.InboundEmail email2  = new Messaging.InboundEmail();
            Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
            
            setData();
            
            /* Create the email body */
            email.plainTextBody = 'Plain text of the body';
            email2.plainTextBody = 'Plain text of the body2';
            email.fromAddress = fromAddress;
            email2.fromAddress = fromAddress;
    
            /* Set the attachment */
            email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attach };
            email2.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attach2 };
            
            TGS_SMDM_Interface tgsSMDMInterface = new TGS_SMDM_Interface();
            
            Test.startTest();
            Messaging.InboundEmailResult result = tgsSMDMInterface.handleInboundEmail(email, env);
            Messaging.InboundEmailResult result2 = tgsSMDMInterface.handleInboundEmail(email2, env2);
            Test.stopTest();
            
            //System.assert (result.success, 'First email returned a failure message');
            //System.assert (result2.success, 'Second email returned a failure message');
        }  
    }
    
    
    /*------------------------------------------------------------
    Author:         Diego Sarsa 
    Company:        Deloitte
    Description:    Method that sets the data necessary for the test
    History
    <Date>          <Author>        <Change Description>
    26-feb-2015     Diego Sarsa     Initial Version
    ------------------------------------------------------------*/
    static void setData() {
        /*
        User userTest =  TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        System.runAs(userTest){
        */
            if (!isDataSet) {
                
                isDataSet = true;
            
                /******************
                 * SET DATA BEGINS
                 ******************/
                
                Account legalEntity = TGS_Dummy_Test_Data.dummyHierarchy();
                Account costCenter;
                List<Account> listAcc = TGS_Portal_Utils.getLevel5(legalEntity.Id, 1);
                if(listAcc.size()>0) {
                    costCenter = listAcc[0];
                }
                
                TGS_SMDM_Data_Collect__c sMDMDataCollect1 = new TGS_SMDM_Data_Collect__c();
                sMDMDataCollect1.TGS_Enduser_Email__c = 'john.doe@telefonica.com';
                sMDMDataCollect1.TGS_Product__c = Constants.PRODUCT_SMDM_INVENTORY;
                sMDMDataCollect1.TGS_Cost_Center__c = costCenter.Id;
                insert sMDMDataCollect1;
                
                TGS_SMDM_Data_Collect__c sMDMDataCollect2 = new TGS_SMDM_Data_Collect__c();
                sMDMDataCollect2.TGS_Enduser_Email__c = 'jane.doe@telefonica.com';
                sMDMDataCollect2.TGS_Product__c = Constants.PRODUCT_SMDM_INVENTORY;
                sMDMDataCollect2.TGS_Cost_Center__c = costCenter.Id;
                insert sMDMDataCollect2;
                
                TGS_SMDM_Data_Collect__c sMDMDataCollect3 = new TGS_SMDM_Data_Collect__c();
                sMDMDataCollect3.TGS_Enduser_Email__c = 'jane.doe@telefonica.com';
                sMDMDataCollect3.TGS_Product__c = Constants.PRODUCT_SMDM_MANAGMENT;
                sMDMDataCollect3.TGS_Cost_Center__c = costCenter.Id;
                insert sMDMDataCollect3;
                
                
                /* Additional information about catalog, category, subcategories and products */
                //NE__Order__c testOrder = new NE__Order__c(RecordTypeId = listRecordTypes[0].Id, NE__BillAccId__c = accountId);
                //insert testOrder;
                /* Catalog */
                NE__Catalog__c testCatalog = new NE__Catalog__c(Name=Constants.CATALOG_TGSOL_CATALOG);
                insert testCatalog;
                /* Category */
                NE__Catalog_Category__c testCategory = new NE__Catalog_Category__c(Name='Enterprise Managed Mobility', NE__CatalogId__c = testCatalog.Id);
                insert testCategory;
                /* Subcategory 1 */
                NE__Catalog_Category__c testSubCategory = new NE__Catalog_Category__c(Name='Universal WIFI', NE__CatalogId__c = testCatalog.Id, NE__Parent_Category_Name__c = testCategory.Id);
                insert testSubCategory;
                /* Subcategory 2 */
                NE__Catalog_Category__c testSubCategory2 = new NE__Catalog_Category__c(Name='Secure Mobile Device, Apps and Content Management', NE__CatalogId__c = testCatalog.Id, NE__Parent_Category_Name__c = testCategory.Id);
                insert testSubCategory2;
            
                /* Product */
                //testOrder = new NE__Order__c(RecordTypeId = listRecordTypes[0].Id);
                //insert testOrder;
                NE__Product__c testProduct = new NE__Product__c(Name=Constants.PRODUCT_SMDM_INVENTORY);
                insert testProduct;
                /* Family property */
                NE__Family__c family = new NE__Family__c(Name = 'SMDM Management Family');
                insert family;
                NE__DynamicPropertyDefinition__c property = new NE__DynamicPropertyDefinition__c(Name = 'Device Serial Number', NE__Type__c = 'String');
                insert property;
                NE__ProductFamilyProperty__c familyProp = new NE__ProductFamilyProperty__c(NE__FamilyId__c = family.Id, NE__PropId__c = property.Id, NE__Required__c = 'No', TGS_Is_key_attribute__c = true);
                insert familyProp;
                NE__ProductFamily__c productFamily = new NE__ProductFamily__c(NE__ProdId__c = testProduct.Id, NE__FamilyId__c = family.Id);
                insert productFamily;
            
                /* OrderItem */
                //testOrderItem = new NE__OrderItem__c(NE__OrderId__c = testOrder.Id, NE__ProdId__c = testProduct.Id, NE__Qty__c=1, NE__Account__c = accountId, TGS_Service_Status__c = 'Deployed', NE__Status__c = 'In Progress');
                //insert testOrderItem;
                /* Attributes of the OrderItem*/
                //attribute = new NE__Order_Item_Attribute__c(Name = 'Device Serial Number', NE__Value__c = '000', NE__Order_Item__c = testOrderItem.Id);
                //insert attribute;
                            
                /******************
                 * SET DATA ENDS
                 ******************/
            }
            /*
        }
        */
    }
        
}