@IsTest
public with sharing class TGS_Survey_Launcher_Handler_Test {

    static Id uId;
    static Id myCaseId;
    
    static testMethod void checkSendEmailPoll() {
        String profile = 'TGS Integration User';
        
       Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
                TGS_User_Org__c uO = new TGS_User_Org__c();
                uO.TGS_Is_TGS__c = True;
                uO.SetupOwnerId = miProfile.Id;
                insert uO;
                //[Micah Burgos]Comentado por error en subida de PL. Al tener el (SeeAllData=true) y el valor en el sistema salta duplicado
        }
        
        
        User u = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        u.BI_Permisos__c = 'TGS';
        insert u;
                               
        System.runAs(u) {
                                               
            Case myCase = TGS_Dummy_Test_Data.dummyCaseTGS('TGS_Complaint', 'Support Agent');
            Contact myContact = TGS_Dummy_Test_Data.dummyContactTGS('Test contact');
            myContact.Email = 'dummycontacttgsol@gmail.com';
            myContact.TGS_of_Closed_Cases__c = 9 ;
            insert myContact;
            myCase.ContactId = myContact.Id;
            
            try{
                //myCase.TGS_Acting_as__c = 'On Behalf';   Eliminacion Acting As
                myCase.Subject = 'aabanas';
                myCase.Description = 'banana';
                myCase.Origin = 'Email';
                                                               
                Test.startTest();
                    insert myCase;
                Test.stopTest();
            }catch(Exception e){
                system.debug('Error al insertar el Case');
            }
            
            myCase.Status = 'In Progress';
            myCase.TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
            update myCase;
            
            
            myCase.Status = 'Resolved';
            myCase.TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
            update myCase;
            
            myCase.Status = 'Closed';
            myCase.TGS_Casilla_Desarrollo__c = true; //SNM 07/10/2016
            //myCase.TGS_Acting_as__c = 'On Behalf';    Eliminacion Acting as
            update myCase;
            
        }
    }
    
  /*  static testMethod void changeStatusInProgress() {
        User myU = [Select Id, Name From User Where Id = :uId Limit 1];
        System.runAs(myU) {
            Case miCase = [Select Id, Status From Case Where Id = :myCaseId Limit 1];
            try{
                miCase.Status = 'In Progress';
                update miCase;
            }catch(Exception e){}
            system.debug('Test Case Status: ' + miCase.Status);
        }
    }
            
    static testMethod void changeStatusResolved() {
        User myU = [Select Id, Name From User Where Id = :uId Limit 1];
        System.runAs(myU) {
            Case miCase = [Select Id, Status From Case Where Id = :myCaseId Limit 1];
            try{
                miCase.Status = 'Resolved';
                update miCase;
            }catch(Exception e){}
            system.debug('Test Case Status: ' + miCase.Status);
        }
    }
            
    static testMethod void changeStatusClosed() {
        User myU = [Select Id, Name From User Where Id = :uId Limit 1];
        System.runAs(myU) {
            Case miCase = [Select Id, Status From Case Where Id = :myCaseId Limit 1];
            try{
                miCase.Status = 'Closed';
                miCase.TGS_Acting_as__c = 'On Behalf';
                update miCase;
            }catch(Exception e){}
            system.debug('Test Case Status: ' + miCase.Status);
            system.debug('Stop Test Case Status: ' + miCase.Status);
        }
    }*/
}