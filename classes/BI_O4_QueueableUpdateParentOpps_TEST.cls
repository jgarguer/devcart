/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Pedro Jesús Párraga Bórnez
 Company:       New Energy Aborda
 Description:   
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 21/09/2016          Pedro Párraga	             Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class BI_O4_QueueableUpdateParentOpps_TEST {

	static{
        BI_TestUtils.throw_exception = false;
    }
	
	@isTest static void BI_O4_QueueableUpdateParentOpps() {
		List<Opportunity> lstOpp = new List<Opportunity>();

	    Opportunity opp = new Opportunity(Name = 'Test' + system.now(),
                      					  CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                      					  BI_Ciclo_ventas__c = Label.BI_Completo  
                      					  );


	    lstOpp.add(opp);
	    Test.startTest();
	    insert lstOpp;
	    Test.stopTest();
	}
}