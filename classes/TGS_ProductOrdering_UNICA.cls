/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Antonio Pardo
     Company:       Accenture
     Description:   
                   
     Test Class:    TGS_ProductOrdering_UNICA_TEST
     History:
     
     <Date>            <Author>             <Description>
                        Antonio Pardo        First version
                        Antonio Mendivil     Added the different callouts
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

@RestResource(urlMapping='/v1/ordering/*')
global class TGS_ProductOrdering_UNICA {

    @HttpPost
    global static void createOrder(){
        
        NETriggerHelper.setTriggerFired('TGS_ProductOrdering_UNICA.createOrder');

        System.debug('CreateOrder INNIT:::AccountId' + RestContext.request.requestURI.split('/')[4]);
        
        
        //Recogiendo la respuesta y pasando del tipo de dato Blob a String, para el parseo al wrapper
        RestRequest req = RestContext.request;
        Blob blob_json = req.requestBody; 
        String string_json = blob_json.toString();
        System.debug(string_json);

        //Parseo del JSON recibido a un wrapper
        TGS_ProductOrdering_Helper.OrderJSON order = TGS_ProductOrdering_Helper.parse(string_json);
        //Creando la respuesta, se pasa al helper para que gestione los errores en cada catch
        RestResponse res = RestContext.response;   
        
        //Antes de la creación de Orders creamos un savePoint al que volveremos si hay algún fallo en la transacción
        Savepoint svp = Database.setSavepoint();
        
        try{
            Map<String, Object> map_emailBinding = new Map<String, Object>{
                'fullStackId' => order.correlationId
            };
            TGS_ProductOrdering_Helper.map_emailBinding = map_emailBinding;
            Exception integrationException;
            TGS_ProductOrdering_Helper.integrationException = integrationException;

            TGS_ProductOrdering_Helper.setresHelper(res);
            
            //Mapa con todos los recordtypes necesarios, para la creación de orders, caso o contacto
            Map<String,String> mapRTs=TGS_ProductOrdering_Helper.getRTs();
            
            //Correlation Id sólo será comprobado en Asset, si existe una Order que contenga correlationId indica que es una modificación/terminación, de lo contrario, creación
            Boolean isNew = [SELECT TGS_CorrelationId__c FROM NE__Order__c WHERE RecordTypeId =:mapRTs.get('Asset') AND TGS_CorrelationId__c = :order.correlationId LIMIT 1].isEmpty() ? true : false;
            Map<String, NE__Order__c> map_oldOrders;
            System.debug('Es nueva ? ' + isNew);
            //control de errores sobre correlationId y el estado
            if(checkCorrelationId(order.productOrderType, isNew, res, order.correlationId)){
                
                //Mapa con order y asset accesible para los distintos métodos del helper
                Map<String,NE__Order__c>mapOrders=new Map<String,NE__Order__c>();
                //Si es una modificación rellenamos un mapa con los valores de las orders del sistema
                if(!isNew){
                    //Caso usado para filtrar la última order
                    Map<String, Map<String, NE__Order__c>> map_caseOr = new Map<String, Map<String, NE__Order__c>>();

                    for(NE__Order__c oldOr : [SELECT Id, Case__c, NE__Configuration_Type__c ,recordType.DeveloperName, Cost_Center__r.TGS_IntegrationID__c, NE__Asset__c, Site__c, (SELECT Id, IsRoot__c, TEMPEXT_ID__c ,TGS_CSUID1__c, NE__OneTimeFeeOv__c, NE__CatalogItem__c,NE__RecurringChargeFrequency__c, NE__RecurringChargeOv__c,  NE__ProdId__c, NE__ProdId__r.NE__Source_Product_Id__c, Installation_Point__c, Installation_point__r.TEMPEXT_ID__c, Installation_point__r.Name, NE__Qty__c,CurrencyIsoCode,NE__Billing_Account__r.TGS_IntegrationID__c, NE__Billing_Account__r.Name, TGS_billing_start_date__c, TGS_Billing_end_date__c,NE__Status__c FROM NE__Order_Items__r) FROM NE__Order__c WHERE TGS_CorrelationId__c =:order.correlationId]){
                    
                        if(map_caseOr.containsKey(oldOr.Case__c)){
                            map_caseOr.get(oldOr.Case__c).put(oldOr.recordType.DeveloperName, oldOr);
                        }else{
                            map_caseOr.put(oldOr.Case__c, new Map<String, NE__Order__c>{oldOr.recordType.DeveloperName => oldOr});
                        }
                        
                    }
                    map_oldOrders = new Map<String, NE__Order__c>();
                    for(Map<String, NE__Order__c> mapoldor : map_caseOr.values()){
                        System.debug('SIZEEE' + mapoldor.size());
                        if(mapoldor.size() == 2){
                            //añadimos ya el asset a la terminación
                            mapOrders.put('Asset', mapoldor.get('Asset'));
                            map_oldOrders.putAll(mapoldor);
                        }
                        
                    }
                    TGS_ProductOrdering_Helper.setmapOld(map_oldOrders);
                }

                /***** DATOS UTILIZADOS EN NEW, CHANGE Y DISCONNECT *****/
                //Query del site a partir de la id externa
                BI_Punto_de_Instalacion__c site=TGS_ProductOrdering_Helper.getSite(order.OrderItem[0]);
                
                //Mapa con las cuentas recogidas con una query a partir de la id externa
                Map<String, Account> map_account = TGS_ProductOrdering_Helper.getAccounts(RestContext.request.requestURI.split('/')[4], order.orderItem[0].billingAccount[0].id);
                
                //Map con información de contractHeader y contractAccount
                System.debug('ID***-->'+map_account.get('TGS_Cost_Center').TGS_Aux_Legal_Entity__c); 
                Map<String,Id> mapContractInfo=TGS_ProductOrdering_Helper.getContractInfo(map_account.get('TGS_Cost_Center').TGS_Aux_Legal_Entity__c);
                
                //Máquina de estados que devolverá los estados para Order y OrderItem dependiendo de productOrderType
                Map<String, String> map_status = TGS_ProductOrdering_Helper.getStatus(order.productOrderType);
                
                //Nombre e id de itemHeader para sacar la estructura de catálogo
                Map<String, Object> map_itemHeader = TGS_ProductOrdering_Helper.getItemHeaderFromProduct(order.OrderItem[0]);
                if(map_itemHeader == null){
                    res.statusCode = 400;
                    String jsonResponse = '{\n"error": "no se pudo recuperar la estructura del catálogo"\n}';
                    res.responseBody = blob.valueOf(jsonResponse);
                    return;
                }

                if(order.productOrderType == 'new'){

                    NE__Order__c orderToInsert=TGS_ProductOrdering_Helper.createOrder(map_status.get('OrderStatus'), order,site,mapRTs.get('Order'),map_account.get('TGS_Cost_Center'),mapContractInfo);
                    System.debug(orderToInsert);
                    mapOrders.put('Order',orderToInsert);
                    
                    //se crea el Asset
                    NE__Order__c orderAssetToInsert=TGS_ProductOrdering_Helper.createAsset(map_status.get('AssetStatus'), orderToInsert,mapRTs.get('Asset'));
                    mapOrders.put('Asset',orderAssetToInsert);
                    
                    //crear o actulizar commercial asset con las nuevas ids y estados
                    TGS_ProductOrdering_Helper.updateAssetEnterpriseId(mapOrders, map_status);

                    //información del catálogo, necesaria para la creación de order items
                    Map<String, Object> map_productTree = TGS_CatalogHelper.getProductTree(new Set<Id>{(Id)map_itemHeader.get('IdItemHeader')}, 'TGSOL Catalog');
                    
                    //Creación de caso si no existiera, o recoger uno ya existente
                    Contact contacto=TGS_ProductOrdering_Helper.createDummyContact(map_account.get('TGS_Legal_Entity').Id, order.relatedParty);
                    
                    //Creación del caso necesario para la order
                    Case caseManagement = TGS_ProductOrdering_Helper.createCase(contacto,mapOrders,(String)map_itemHeader.get('Name'),order.priority,mapRTs.get('Order_Management_Case'), date.valueOf(order.orderItem[0].validFor.startDateTime));

                    System.debug(mapOrders);
                    Map<String,Map<String,NE__OrderItem__c>> map_oi = TGS_ProductOrdering_Helper.createOrderItems(map_status,mapOrders,order.OrderItem,map_productTree,(String)map_itemHeader.get('IdItemHeader')); 
                    //Actualización de caso necesaria para ejecutar métodos del trigger
                    progressCase(mapOrders, caseManagement, res);

                }else if(order.productOrderType == 'change'){
                    List<TGS_ProductOrdering_Helper.OrderItem> lst_newOiAsset = TGS_ProductOrdering_Helper.compareOrders(map_oldOrders.get('Asset'), order);
                    NE__Order__c orderToInsert=TGS_ProductOrdering_Helper.createOrder(map_status.get('OrderStatus'), order,site,mapRTs.get('Order'),map_account.get('TGS_Cost_Center'),mapContractInfo);
                    mapOrders.put('Order',orderToInsert);
                    
                    //se crea el Asset
                    //NE__Order__c orderAssetToInsert=TGS_ProductOrdering_Helper.createAsset(map_status.get('AssetStatus'), orderToInsert,mapRTs.get('Asset'));
                    //mapOrders.put('Asset',orderAssetToInsert);
                    //modifyAsset(map_status, mapOrders.get('Asset'), Map<String, NE__OrderItem__c> map_tempex_orderOi, TGS_CatalogHelper.ProductDataWrapper catalogRoot);
                    //crear o actulizar commercial asset con las nuevas ids y estados
                    TGS_ProductOrdering_Helper.updateAssetEnterpriseId(mapOrders, map_status);
                    //información del catálogo, necesaria para la creación de order items
                    Map<String, Object> map_productTree = TGS_CatalogHelper.getProductTree(new Set<Id>{(Id)map_itemHeader.get('IdItemHeader')}, 'TGSOL Catalog');
                    
                    //Creación de caso si no existiera, o recoger uno ya existente
                    Contact contacto=TGS_ProductOrdering_Helper.createDummyContact(map_account.get('TGS_Legal_Entity').Id, order.relatedParty);
                    
                    //Creación del caso necesario para la order
                    Case caseManagement = TGS_ProductOrdering_Helper.createCase(contacto,mapOrders,(String)map_itemHeader.get('Name'),order.priority,mapRTs.get('Order_Management_Case'), date.valueOf(order.orderItem[0].validFor.startDateTime));
                    
                    TGS_ProductOrdering_Helper.fillAssetOrderItems((Id)map_itemHeader.get('IdItemHeader'), map_status, lst_newOiAsset, mapOrders, map_productTree);
                    NE__Order__c orAss = [SELECT Id, Case__c, NE__Configuration_Type__c ,recordType.DeveloperName, Cost_Center__r.TGS_IntegrationID__c, NE__Asset__c, Site__c, (SELECT Id, IsRoot__c, TEMPEXT_ID__c ,TGS_CSUID1__c, NE__OneTimeFeeOv__c, NE__CatalogItem__c,NE__RecurringChargeFrequency__c, NE__RecurringChargeOv__c,  NE__ProdId__c, NE__ProdId__r.NE__Source_Product_Id__c, Installation_Point__c, Installation_point__r.TEMPEXT_ID__c, Installation_point__r.Name, NE__Qty__c,CurrencyIsoCode,NE__Billing_Account__r.TGS_IntegrationID__c, NE__Billing_Account__r.Name, TGS_billing_start_date__c, TGS_Billing_end_date__c,NE__Status__c FROM NE__Order_Items__r) FROM NE__Order__c WHERE Id =:mapOrders.get('Asset').Id];
                    mapOrders.put('Asset', orAss);
                    System.debug(mapOrders);

                    Map<String,Map<String,NE__OrderItem__c>> map_oi = TGS_ProductOrdering_Helper.updateOrderItems(map_status,mapOrders,order.OrderItem,map_productTree,(String)map_itemHeader.get('IdItemHeader')); 

                    System.debug(mapOrders);
                    //Map<String,Map<Id,NE__OrderItem__c>> map_oi = TGS_ProductOrdering_Helper.createOrderItems(map_status,mapOrders,order.OrderItem,map_productTree,(String)map_itemHeader.get('IdItemHeader')); 
                    //Actualización de caso necesaria para ejecutar métodos del trigger
                    progressCase(mapOrders, caseManagement, res);
                    
                
                }else if(order.productOrderType == 'disconnect'){

                    System.debug('Tipo de Order: ' + order.productOrderType );
                    if(TGS_ProductOrdering_Helper.checkValidAccount(map_oldOrders.get('Order'), order)){
                        System.debug('Cuenta válida');
                        System.debug('Cuenta válida:::map_oldOrders'+map_oldOrders.get('Order'));
                        System.debug('Cuenta válida:::order'+order);
                        if(TGS_ProductOrdering_Helper.checkTerminationOrder(map_oldOrders.get('Order'), order)){
                            System.debug('Terminación');
                            //Creación de Order de terminación
                            NE__Order__c orderToInsert=TGS_ProductOrdering_Helper.createOrder(map_status.get('OrderStatus'), order,site,mapRTs.get('Order'),map_account.get('TGS_Cost_Center'),mapContractInfo);
                            mapOrders.put('Order',orderToInsert);
                            
                            //crear o actulizar commercial asset con las nuevas ids y estados
                            TGS_ProductOrdering_Helper.updateAssetEnterpriseId(mapOrders, map_status);

                            //información del catálogo, necesaria para la creación de order items
                            Map<String, Object> map_productTree = TGS_CatalogHelper.getProductTree(new Set<Id>{(Id)map_itemHeader.get('IdItemHeader')}, 'TGSOL Catalog');
                            
                            //Creación de caso si no existiera, o recoger uno ya existente
                            Contact contacto=TGS_ProductOrdering_Helper.createDummyContact(map_account.get('TGS_Legal_Entity').Id, order.relatedParty);
                            
                            //Creación del caso necesario para la order
                            Case caseManagement = TGS_ProductOrdering_Helper.createCase(contacto,mapOrders,(String)map_itemHeader.get('Name'),order.priority,mapRTs.get('Order_Management_Case'), date.valueOf(order.orderItem[0].validFor.startDateTime));
                            
                            System.debug(mapOrders);
                            Map<String,Map<String,NE__OrderItem__c>> map_oi = TGS_ProductOrdering_Helper.updateOrderItems(map_status,mapOrders,order.OrderItem,map_productTree,(String)map_itemHeader.get('IdItemHeader')); 
                            //Actualización de caso necesaria para ejecutar métodos del trigger
                            progressCase(mapOrders, caseManagement, res);
                        }else{
                            Database.rollback(svp);
                            return;
                        }

                    }
                }

                //Al llegar al final de la ejecución
                if(TGS_ProductOrdering_Helper.getresHelper().statusCode == null){
                    TGS_ProductOrdering_Helper.sendEmail(map_emailBinding);
                    res.statusCode = 200;
                    res.responseBody = blob.valueOf(JSON.serializePretty(mapOrders));
                    TGS_ErrorIntegrations_Helper.createSuccessLog('OrderingUNICA', order.productOrderType, order, mapOrders.get('Asset').Id);
                }else{
                    //SI no es null es porque saltó un error y hay que hacer rollback
                    if(integrationException!=null){
                        TGS_ErrorIntegrations_Helper.createExceptionLog('OrderingUNICA', order.productOrderType, integrationException, order, order.correlationId);
                    }
                    Database.rollback(svp);
                }

            }
        }catch(Exception exc){
            res.statusCode = 400;
            String jsonResponse = '{\n"error":' + '"' + exc.getTypeName() + '"\n "message": "'+exc.getMessage() + '"\n}';
            res.responseBody = blob.valueOf(jsonResponse);
            Database.rollback(svp);
            TGS_ErrorIntegrations_Helper.createExceptionLog('OrderingUNICA', order.productOrderType, exc, order, order.correlationId);
        }
        return;
        
    }
    
    private static Boolean checkCorrelationId(String orType, Boolean isNew, RestResponse res, String correlationId){
        if(orType == 'new' || orType == 'change' || orType == 'disconnect'){

            if(orType == 'New' && !isNew){
                res.statusCode = 400;
                String jsonResponse = '{\n"error": "Ya existe una Order con esta ID ⛄ ' + correlationId +'"\n}';
                res.responseBody = blob.valueOf(jsonResponse);
                return false;

            }else if(orType == 'change' && isNew || orType == 'disconnect' && isNew){
                res.statusCode = 400;
                String jsonResponse = '{\n"error": "No existe una order con Id : ' + correlationId +'"\n}';
                res.responseBody = blob.valueOf(jsonResponse);
                return false;
            }

        }else{
            res.statusCode = 400;
            String jsonResponse = '{\n"error": "No se puede realizar una operación del tipo : ' + orType + ', operaciones permitidas : new, change, disconenct "\n}';
            res.responseBody = blob.valueOf(jsonResponse);
            return false;   
        }
        
        return true;
    }

    private static void progressCase(Map<String, NE__Order__c> map_orders, Case caseManagement, RestResponse res){
        try{
            BI_MigrationHelper.skipAllTriggers();
            caseManagement.Status = 'Resolved';
            caseManagement.TGS_Status_Reason__c = '';
            update caseManagement;
            BI_MigrationHelper.cleanSkippedTriggers();
            
            Constants.firstRunBilling = true;
            NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
            caseManagement.Status = 'Closed';
            caseManagement.Asset__c = map_orders.get('Asset').Id;
            update caseManagement;

        }catch(Exception exc){
            res.statusCode = 400;
            String jsonResponse = '{"error":\'' + exc.getMessage() + '\'}';
            res.responseBody = blob.valueOf(jsonResponse);
            BI_LogHelper.generate_BILog('TGS_ProductOrdering_UNICA.updateCase', 'TGS', exc, 'Apex Class');
        }
    }
}