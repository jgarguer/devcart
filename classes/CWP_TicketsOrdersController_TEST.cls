/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for testing CWP_TicketsOrdersController
    Test Class:    CWP_TicketsOrdersController
    
    History:
     
    <Date>                  <Author>                <Change Description>
    19/04/2017              Everis                    Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class CWP_TicketsOrdersController_TEST {
    
       
    @testSetup 
    private static void dataModelSetup() {  
        
       
        //USER
        UserRole rolUsuario;
        User unUsuario;
        User otroUsuario;
        Profile profileTGS;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            profileTGS = CWP_TestDataFactory.getProfile('TGS System Administrator');
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, profileTGS.Id);
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
            //ACCOUNT
            map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
        
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        Contact contactTest1 = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest1');
        
        contactTest.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest;
        contactTest1.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest1;
        
        User usuario;
        User usuarioNoTGS;
        
         //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 't1';
        producto.TGS_CWP_Tier_2__c = 't2';
        insert producto;
        list<NE__Catalog_Item__c> ciList = new list<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
        ciList.add(catalogoItem);
        
        NE__Catalog_Item__c catalogoItem1 = CWP_TestDataFactory.createCatalogoItem('ProductoTB', catalogCategoryChildren.id, catalogo.id, producto.id);
        catalogoItem1.NE__Technical_Behaviour__c = 'Service with pre-approved changes';
        ciList.add(catalogoItem1);
        insert ciList;
        
        NE__Contract_Header__c newCH = new NE__Contract_Header__c(
            NE__name__c = 'theContract'
        );
        insert newCH;
        
        NE__Contract_Account_Association__c newCAA = new NE__Contract_Account_Association__c(
            NE__Contract_Header__c = newCH.id,
            NE__Account__c = accLegalEntity.id
        );
        insert newCAA;
        
        NE__Contract__c newC =new  NE__Contract__c(
            NE__Contract_Header__c = newCH.id
        );
        insert newC;
        
        NE__Contract_Line_Item__c newCII = new NE__Contract_Line_Item__c(
            NE__Commercial_Product__c = producto.id,
            NE__Contract__c = newC.id
        );
        insert newCII;
        
        
        
        
          //ORDER
        
        NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        insert testOrder;
        //createOrderItem(id accountId, id orderId, id productId, id catalogItem, integer quantity){
        NE__OrderItem__c newOI;
        list<NE__OrderItem__c> oiList = new list<NE__OrderItem__c>();
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
        oiList.add(newOI);
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[1].id, 1);
        newOI.NE__City__c = 'Gondor';
        newOI.NE__city__c = 'Middle-earth';
        oiList.add(newOI);
        insert oiList;
        
        
        //CASE
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        insert newCase;       
        
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');       
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
         
        CaseComment Comment = new CaseComment(ParentId = newCase.Id, CommentBody = 'new comment');
        insert Comment;
        
         Market_Place_PP__c newMarketplace = new Market_Place_PP__c();
        insert newMarketplace;
     
     }
    
        
      @isTest
    private static void getCaseListTest() {     
        system.debug('test 1');
         
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
       
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_TicketsOrdersController.getCaseList();   
            Test.stopTest();
        }       
        
    } 
    
    
      @isTest
    private static void addNewCommentTest() {     
        system.debug('test 2');
         
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        CaseComment Comment = [SELECT CommentBody, ParentId FROM CaseComment WHERE CommentBody =: 'new comment'];
        Case caso =[SELECT Id FROM Case LIMIT 1];
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_TicketsOrdersController.addNewComment(caso.Id, Comment.CommentBody);   
            Test.stopTest();
        }       
        
    } 
    
    
      @isTest
    private static void saveTheFileTest() {     
        system.debug('test 3');
         
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        Market_Place_PP__c newMarketplace =[SELECT Id FROM Market_Place_PP__c LIMIT 1];
        
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_TicketsOrdersController.saveTheFile(newMarketplace.Id, 'filename', 'base64Data', 'tipo de contenido');   
            Test.stopTest();
        }       
        
    } 

    
      @isTest
    private static void getCaseCommentTest() {     
        system.debug('test 4');
         
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];
        
        Case caso =[SELECT Id FROM Case LIMIT 1];
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_TicketsOrdersController.getCaseComment(caso.Id);   
            Test.stopTest();
        }       
        
    } 

    
      @isTest
    private static void getTableHeaderTest() {     
        system.debug('test 5');
         
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
      
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_TicketsOrdersController.getTableHeader();   
            Test.stopTest();
        }       
        
    } 

    
      @isTest
    private static void saveNewCaseTest() {     
        system.debug('test 6');
         
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];
      
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_TicketsOrdersController.saveNewCase('subject', 'reason', 'description');   
            Test.stopTest();
        }       
        
    }     

}