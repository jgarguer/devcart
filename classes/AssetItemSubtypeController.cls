public with sharing class AssetItemSubtypeController {
    
    public Apexpages.StandardController controller {get;set;}
    public Boolean adminMode {get;set;}
    public list<NE__OrderItem__c> ordItems {get;set;}
    public list<OrderItemWrapper> listOfOiWrappers {get;set;}
    public list<SelectOption> allSubtype {get;set;}
    public String stSelected {get;set;}
    
    
    public AssetItemSubtypeController(ApexPages.StandardController stdController) {
        
        this.controller = stdController;
        String ordId = ApexPages.currentPage().getParameters().get('id');
        String isparque = ApexPages.currentPage().getParameters().get('parque');
        if(ApexPages.currentPage().getParameters().get('adminmode') == 'true')
            adminMode = true;
        else
            adminMode = false;
        
        if(adminMode)
        {
	        ordItems = [SELECT Id, (SELECT id FROM NE__Order_Items_Conf__r),NE__OrderId__c, NE__OrderId__r.NE__Type__c, NE__OrderId__r.NE__OptyId__c, NE__CatalogItem__r.NE__Disconnect_Subtype__c, NE__CatalogItem__r.NE__Change_Subtype__c, NE__CatalogItem__r.NE__New_Subtype__c, NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name, Order_Item_Code_Linea_Pedido__c, NE__ProdId__c, Configuration_Type__c, Configuration_SubType__c, NE__Status__c, NE__FulfilmentStatus__c 
	                    FROM NE__OrderItem__c 
	                    WHERE NE__OrderId__c =: ordId AND NE__Root_Order_Item__c = null];
        }
        else
        {
	        ordItems = [SELECT Id, (SELECT id FROM NE__Order_Items_Conf__r),NE__OrderId__c, NE__OrderId__r.NE__Type__c, NE__OrderId__r.NE__OptyId__c, NE__CatalogItem__r.NE__Disconnect_Subtype__c, NE__CatalogItem__r.NE__Change_Subtype__c, NE__CatalogItem__r.NE__New_Subtype__c, NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name, Order_Item_Code_Linea_Pedido__c, NE__ProdId__c, Configuration_Type__c, Configuration_SubType__c, NE__Status__c, NE__FulfilmentStatus__c 
	                    FROM NE__OrderItem__c 
	                    WHERE NE__OrderId__c =: ordId];        	
        }
        
        stSelected = '';
        listOfOiWrappers = new list<OrderItemWrapper>();
        
        // listOfOiWrappers saves information about order items; Configuration_SubType__c takes the value
        // of CatalogItem picklist (NE__Change_Subtype__c or NE__Disconnect_Subtype__c)
        for(NE__OrderItem__c oi : ordItems) {
            OrderItemWrapper oiw = new OrderItemWrapper();
            oiw.isparque = isparque;
            oiw.oi = oi;
            listOfOiWrappers.add(oiw);
        }
        
        list<SelectOption> listOfSubtypes	=	new list<SelectOption>();
        allSubtype							=	new list<SelectOption>();
        Set<SelectOption> allSubtypeSet 	= 	new Set<SelectOption>();
        for(OrderItemWrapper wr : listOfOiWrappers) {
            for(SelectOption ls : wr.subTypeList) {
                if(ls.getValue() != '')
                    listOfSubtypes.add(new SelectOption(ls.getValue(),ls.getValue()));
            }
        }
        
        allSubtypeSet.addAll(listOfSubtypes);
        allSubtype.addAll(allSubtypeSet);        
    }
    
    
    public PageReference save() {
        upsert ordItems;
        PageReference page;
		
        if(ordItems[0].NE__OrderId__r.NE__Type__c == 'ChangeOrder') {
            String oppId = ordItems[0].NE__OrderId__r.NE__OptyId__c;
            page = new PageReference('/'+oppId.substring(0,15)+'/e?retURL=%2F'+oppId.substring(0,15));
        } else if(ordItems[0].NE__OrderId__r.NE__Type__c == 'Disconnection')
            page = new PageReference('/' + ordItems[0].NE__OrderId__c);
        else
            page = new PageReference('/' + ordItems[0].NE__OrderId__c);
            
        if(adminMode	==	true)
        {
        	for(OrderItemWrapper oiwrapper:listOfOiWrappers)
        	{
        		//If is complex then create the asset
        		system.debug('size of complex: '+oiwrapper.oi.NE__Order_Items_Conf__r.size());
        		if(oiwrapper.oi.NE__Order_Items_Conf__r.size() > 0 && (oiwrapper.oi.NE__Status__c == 'RFS' || oiwrapper.oi.NE__Status__c == 'RFB' || oiwrapper.oi.NE__Status__c == 'Procesado con cambios'))
        		{
        			for(NE__OrderItem__c oiIt:oiwrapper.oi.NE__Order_Items_Conf__r)
        				oiIt.NE__Status__c	=	oiwrapper.oi.NE__Status__c;
        			
        			update oiwrapper.oi.NE__Order_Items_Conf__r;
        			
                    /*
			        NE.DataMap mp = new NE.DataMap();
			        
			        Map <String , String> result	=	new map<String,String>();
		        	result 					= 	mp.GenerateMapObjects('Order2Asset', oiwrapper.oi.NE__OrderId__c);
		        	String errorMessage    	=   result.get('ErrorMessage');
		        	String errorCode       	=   result.get('ErrorCode');         
		        	String assetId			=	result.get('ParentId'); 			
					
					system.debug('result: '+result);
					
					// Call datamapper
					NE.DataMap.DataMapRequest dReq 	= new NE.DataMap.DataMapRequest();
					dReq.mapName 					= 'Order2Order';
					dReq.sourceId 					= oiwrapper.oi.NE__OrderId__c;
					Map<String,String> mapOfq 		= new Map<String,String>();
					list<String> listOfph 			= new list<String>();
					listOfph.add(oiwrapper.oi.id);
					
					String customQuery = 'NE__Root_Order_Item__c =: {!0} OR id =: {!0}';
					mapOfq.put('NoPromoOrderItem',customQuery);
					mapOfq.put('PromoOrderItem',customQuery);
					mapOfq.put('OrderItem',customQuery);
					
					dReq.mapOfCustomQueries = mapOfq;
					dReq.listOfPlaceHolders = listOfph;
					
					NE.DataMap obj = new NE.DataMap();
					NE.DataMap.DataMapResponse dResp = obj.callDataMap(dReq);
					
					system.debug('dResp: '+dResp);
					
					if(dResp.errorCode	==	'0')
					{
						RecordType rec 	=	[select Name,Id from RecordType WHERE Name = 'Asset' AND (SObjectType = 'Order__c' OR SObjectType = 'NE__Order__c') LIMIT 1];  						
						NE__Order__c newOrd = [SELECT NE__AccountId__c,NE__Asset_Account__c,NE__BillAccId__c,NE__Billing_Account_Asset__c,NE__Service_Account_Asset__c,NE__ServAccId__c,Id,Name,NE__Asset_Configuration__c,NE__Version__c,RecordTypeId,NE__OrderStatus__c,NE__SerialNum__c,NE__Type__c,(SELECT NE__Asset_Item_Account__c,NE__Billing_Account_Asset_Item__c,NE__Service_Account_Asset_Item__c,NE__Billing_Account__c,NE__Service_Account__c,NE__Account__c,NE__Status__c,NE__Action__c,Id FROM NE__Order_Items__r) FROM NE__Order__c WHERE Id =: dResp.parentId];

				        newOrd.RecordTypeId 				= 	rec.Id;
				        newOrd.NE__OrderStatus__c   		= 	'Active';
						newOrd.NE__Asset__c					=	assetId;
						newOrd.NE__Type__c					=	'Asset';
						newOrd.NE__Version__c				=	1;
						newOrd.NE__Asset_Configuration__c	=	assetId;
						
						newOrd.NE__Asset_Account__c			=	newOrd.NE__AccountId__c;
						newOrd.NE__Billing_Account_Asset__c	=	newOrd.NE__BillAccId__c;
						newOrd.NE__Service_Account_Asset__c	=	newOrd.NE__ServAccId__c;					
						
				        update newOrd;  
				        
				        for(NE__OrderItem__c oi:newOrd.NE__Order_Items__r)
				        {
							oi.NE__Asset_Item_Account__c			=	oi.NE__Account__c;
							oi.NE__Billing_Account_Asset_Item__c	=	oi.NE__Billing_Account__c;
							oi.NE__Service_Account_Asset_Item__c	=	oi.NE__Service_Account__c;
							oi.NE__Status__c						=	'Active';
				        }
				        
				        update newOrd.NE__Order_Items__r;							
					}*/
        		}
        		else if(oiwrapper.oi.NE__Order_Items_Conf__r.size() > 0)
        		{
        			for(NE__OrderItem__c oiIt:oiwrapper.oi.NE__Order_Items_Conf__r)
        				oiIt.NE__Status__c	=	oiwrapper.oi.NE__Status__c;
        			
        			update oiwrapper.oi.NE__Order_Items_Conf__r;     			
        		}

        	}
        }
            
        page.setRedirect(false);
        return page;
    }
    
    
    public Pagereference setAllChanges() {
    	system.debug('*LV subtype selected = '+stSelected);
    	
    	for(OrderItemWrapper wr : listOfOiWrappers) {
			for(SelectOption ls : wr.subTypeList) {
				if(ls.getValue() == stSelected)
					wr.oi.Configuration_SubType__c = stSelected;
            }
        }     
        return null;
    }
    
    
    public class OrderItemWrapper {
        
        public NE__OrderItem__c oi {get;set;}
        public String isparque {get;set;}
        
        public OrderItemWrapper() {

        }
        
        public list<SelectOption> subTypeList {
            get
            {
                list<SelectOption> pick = new list<SelectOption>();
                pick.add(new SelectOption('',''));
                
                list<String> listsub = new list<String>();
                if(isparque != 'true') {
	                if(oi.NE__OrderId__r.NE__Type__c == 'ChangeOrder' && oi.NE__CatalogItem__r.NE__Change_Subtype__c != null)
	                    listsub = oi.NE__CatalogItem__r.NE__Change_Subtype__c.split(';');
	                else if(oi.NE__OrderId__r.NE__Type__c == 'Disconnection' && oi.NE__CatalogItem__r.NE__Disconnect_Subtype__c != null)
	                    listsub = oi.NE__CatalogItem__r.NE__Disconnect_Subtype__c.split(';');
                } else {
                	if(oi.NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name == 'Parque Modification' && oi.NE__CatalogItem__r.NE__Change_Subtype__c != null)
                		listsub = oi.NE__CatalogItem__r.NE__Change_Subtype__c.split(';');
                	else if(oi.NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name == 'Parque Baja' && oi.NE__CatalogItem__r.NE__Disconnect_Subtype__c != null)
	                    listsub = oi.NE__CatalogItem__r.NE__Disconnect_Subtype__c.split(';');
	                else if(oi.NE__CatalogItem__r.NE__New_Subtype__c != null)
	                	listsub = oi.NE__CatalogItem__r.NE__New_Subtype__c.split(';');
                }
                
                for(String ls : listsub) {
                    pick.add(new SelectOption(ls,ls));
                }
				
                return pick;
            }
            set;
        }
    }

}