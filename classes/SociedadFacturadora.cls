public  class SociedadFacturadora {
	// constructor
	public SociedadFacturadora() {
		
	}

	public void inicio (List<Factura__c> lsFactura){
		Set<String> setIDFact = new Set<String> ();
		Map<string, Factura__c > mapaFactura = new Map<String, Factura__c>();
		List<Programacion__c> lsUpdateProg = new List<Programacion__c>();

		for (Factura__c fac01:lsFactura ){
			setIDFact.add(fac01.id);
			mapaFactura.put (fac01.id,fac01);	
		}

		for (Programacion__c prog : [SELECT ID, Name,  Facturado__c,Factura__c ,Sociedad_Facturadora_Real__c 
										FROM Programacion__c
										WHERE Facturado__c = false  AND
											Factura__c IN:setIDFact]){
				if(mapaFactura.get (prog.Factura__c) != null){
					prog.Sociedad_Facturadora_Real__c = mapaFactura.get(prog.Factura__c).Sociedad_Facturadora__c;
					lsUpdateProg.add (prog);		
				}
				
		}

		if (lsUpdateProg != null  && lsUpdateProg.size() >0 ){
			update lsUpdateProg;
		}
	}
}