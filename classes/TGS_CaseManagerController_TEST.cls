@isTest 
private class TGS_CaseManagerController_TEST
{
  static testMethod void myUnitTest()
    {
        //Id holdingAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Holding').getRecordTypeId();
        //Account acc = new Account(Name='Account', RecordTypeId=holdingAccountRecordTypeId);
        
       Id recordtypeHId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
       Account acc = new Account(Name='Account', RecordTypeId=recordtypeHId);
      insert acc;
      Contact c = TGS_Dummy_Test_Data.dummyContact(acc.Id);
      RecordType RTProd = [SELECT Id FROM RecordType WHERE (SobjectType = 'NE__Product__c' OR SobjectType = 'Product__c') AND Name = 'Standard'];
      NE__Product__c prod1 = new NE__Product__c(Name='Product 1', RecordTypeId=RTProd.Id);
      insert prod1;
        
        //BCRs 21/06/2017
        Bit2WinHUB__Bulk_Configuration_Request__c bcr1 = new Bit2WinHUB__Bulk_Configuration_Request__c( Bit2WinHUB__Request_Id__c='req-0123456', Bit2WinHUB__Status__c='Completed', Bit2WinHUB__Description__c='');
        insert bcr1;
        Bit2WinHUB__Bulk_Configuration_Request__c bcr2 = new Bit2WinHUB__Bulk_Configuration_Request__c( Bit2WinHUB__Request_Id__c='req-0123456', Bit2WinHUB__Status__c='New', Bit2WinHUB__Description__c='');
        insert bcr2;
        Bit2WinHUB__Bulk_Configuration_Request__c bcr3 = new Bit2WinHUB__Bulk_Configuration_Request__c( Bit2WinHUB__Request_Id__c='req-0123456', Bit2WinHUB__Status__c='New', Bit2WinHUB__Description__c='');
        insert bcr3;
        Bit2WinHUB__Bulk_Configuration_Request__c bcr4 = new Bit2WinHUB__Bulk_Configuration_Request__c( Bit2WinHUB__Request_Id__c='req-0123456', Bit2WinHUB__Status__c='Failed', Bit2WinHUB__Description__c='');
        insert bcr4;
        Bit2WinHUB__Bulk_Configuration_Request__c bcr5 = new Bit2WinHUB__Bulk_Configuration_Request__c( Bit2WinHUB__Request_Id__c='req-0123457', Bit2WinHUB__Status__c='New', Bit2WinHUB__Description__c='');
        insert bcr5;   
        
      NE__Order__c ord1 = new NE__Order__c(NE__AccountId__c=acc.Id, Bit2WinHUB__BulkConfigurationRequest__c=bcr1.Id);
      insert ord1;
      NE__Order__c ord2 = new NE__Order__c(NE__AccountId__c=acc.Id, Bit2WinHUB__BulkConfigurationRequest__c=bcr2.Id);
      insert ord2;
      NE__Order__c ord3 = new NE__Order__c(NE__AccountId__c=acc.Id, Bit2WinHUB__BulkConfigurationRequest__c=bcr3.Id);
      insert ord3;
      NE__Order__c ord4 = new NE__Order__c(NE__AccountId__c=acc.Id, Bit2WinHUB__BulkConfigurationRequest__c=bcr4.Id);
      insert ord4;
      NE__Order__c ord5 = new NE__Order__c(NE__AccountId__c=acc.Id, Bit2WinHUB__BulkConfigurationRequest__c=bcr5.Id);
      insert ord5;
        Id caseOrderManagementRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RTYPE_ORDER_MNGMNT).getRecordTypeId();
      Case case1 = new Case(Subject=Constants.CASE_RTYPE_ORDER_MNGMNT, AccountId=acc.Id, Order__c=ord1.Id, Status='Assigned', TGS_Service__c=prod1.Name, Type='New', RecordTypeId=caseOrderManagementRecordTypeId);
        insert case1;
        Case case2 = new Case(Subject=Constants.CASE_RTYPE_ORDER_MNGMNT, AccountId=acc.Id, Order__c=ord2.Id, Status='Assigned', TGS_Service__c=prod1.Name, Type='New', RecordTypeId=caseOrderManagementRecordTypeId);
      insert case2;
        Case case3 = new Case(Subject=Constants.CASE_RTYPE_ORDER_MNGMNT, AccountId=acc.Id, Order__c=ord3.Id, Status='Assigned', TGS_Service__c=prod1.Name, Type='New', RecordTypeId=caseOrderManagementRecordTypeId);
      insert case3;
        Case case4 = new Case(Subject=Constants.CASE_RTYPE_ORDER_MNGMNT, AccountId=acc.Id, Order__c=ord4.Id, Status='Assigned', TGS_Service__c=prod1.Name, Type='New', RecordTypeId=caseOrderManagementRecordTypeId);
      insert case4;
        Case case5 = new Case(Subject=Constants.CASE_RTYPE_ORDER_MNGMNT, AccountId=acc.Id, Order__c=ord5.Id, Status='In Progress', TGS_Status_reason__c='In Provision', TGS_Service__c=prod1.Name, Type='New', RecordTypeId=caseOrderManagementRecordTypeId, ContactId = c.Id);
    insert case5;
    Id pf = UserInfo.getProfileId();
    Profile miProfile = [SELECT Id, Name FROM Profile Where Id = :pf Limit 1];
        TGS_Status_machine__c sm = new TGS_Status_machine__c(TGS_Prior_Status__c='Assigned', TGS_Prior_Status_reason__c='', TGS_Service__c=prod1.Id, TGS_Status__c='In Progress', TGS_Status_reason__c='In Provision', TGS_Profile__c = miProfile.Name);
        insert sm;
        TGS_Status_machine__c sm2 = new TGS_Status_machine__c(TGS_Prior_Status__c='Assigned', TGS_Prior_Status_reason__c='', TGS_Service__c=prod1.Id, TGS_Status__c='In Progress', TGS_Status_reason__c='In Provision');
        insert sm2;
        TGS_Status_machine__c sm3 = new TGS_Status_machine__c(TGS_Prior_Status__c='In Progress', TGS_Prior_Status_reason__c='In Provision', TGS_Service__c=prod1.Id, TGS_Status__c='Resolved', TGS_Status_reason__c='');
        insert sm3;
        
        TGS_CaseManagerController caseMngr = new TGS_CaseManagerController();
        
        caseMngr.filterObject.AccountId = acc.Id;
        caseMngr.filterObject.Order__c = ord1.Id;
        caseMngr.filterRequestId='req-0123456' ;//AAP 21/06
        caseMngr.filterStatus = 'Assigned';
        caseMngr.addFilter();
        caseMngr.filterObject = new Case();
    	caseMngr.filterStatus = '';
        caseMngr.filterCaseNumber ='';//AAP 21/06
        caseMngr.filterReason = '';//AAP 21/06
        
    	caseMngr.addFilter();
        
        caseMngr.goToNextPage();
        caseMngr.goToPreviousPage();
        
        caseMngr.goToChangeStatusSection();

      Test.setCurrentPageReference(new PageReference('Page.TGS_CaseManager')); 
      System.currentPageReference().getParameters().put('contChange','0');
      caseMngr.calculateNewStatusReasonList();

        
        TGS_CaseManagerController.UpdateStatus updStatus = new TGS_CaseManagerController.UpdateStatus();
        updStatus.priorStatus = 'Assigned';
        updStatus.priorStatusReason = '';
        updStatus.newStatus = 'In Progress';
        updStatus.newStatusReason = 'In Provision';
        
        
        caseMngr.listStatusToChange.add(updStatus); 

        
        caseMngr.updateCases();
        
        caseMngr.goBack();

        caseMngr.filterStatus = 'In Progress';
        caseMngr.filterCaseNumber ='03215564';//AAP 21/06
        caseMngr.filterReason = 'In Provision';//AAP 21/06

        
        caseMngr.addFilter();


        



        TGS_CaseManagerController caseMngr2 = new TGS_CaseManagerController();
        
        caseMngr2.filterObject.AccountId = acc.Id;
        caseMngr2.filterObject.Order__c = ord1.Id;
        caseMngr2.filterRequestId='req-0123457' ;//AAP 21/06
        caseMngr2.filterStatus = 'In Progress';
        caseMngr2.addFilter();
        caseMngr2.filterObject = new Case();
      caseMngr2.filterStatus = '';
        caseMngr2.filterCaseNumber ='';//AAP 21/06
        caseMngr2.filterReason = '';//AAP 21/06
        
      caseMngr2.addFilter();
        
        caseMngr2.goToNextPage();
        caseMngr2.goToPreviousPage();
        
        caseMngr2.goToChangeStatusSection();

      Test.setCurrentPageReference(new PageReference('Page.TGS_CaseManager')); 
      System.currentPageReference().getParameters().put('contChange','0');
      caseMngr2.calculateNewStatusReasonList();

      TGS_CaseManagerController.UpdateStatus updStatus2 = new TGS_CaseManagerController.UpdateStatus();
        updStatus2.priorStatus = 'In Progress';
        updStatus2.priorStatusReason = 'In Provision';
        updStatus2.newStatus = 'Resolved';
        updStatus2.newStatusReason = '';
        
        
        caseMngr2.listStatusToChange.add(updStatus2);
        caseMngr2.updateCases();

    }

}