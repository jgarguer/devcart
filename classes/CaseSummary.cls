public with sharing class CaseSummary 
{
    public    String orderId {get;set;}
    public    String recordTypeId {get;set;}
    
    public CaseSummary(ApexPages.StandardController controller) 
    {
         String caseId  		= Apexpages.Currentpage().getParameters().get('Id');
         Case CaseToLook		= [SELECT Order__c FROM case WHERE id =: caseId];
         orderId				= CaseToLook.Order__c;
         RecordType recordlist	= [select Id,Name FROM RecordType WHERE SobjectType= 'NE__Order__c' and Name = 'Order' LIMIT 1]; 
         recordTypeId 			= recordlist.Id ; 
    }

    
}