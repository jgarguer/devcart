/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:                         Fábio Mogo
Company:                        Everis 
Description:                    Tratamiento de la integración de Perú para controlar la salida de trazas y ejecución de procesos con el fin de evitar duplicidades. 

History:
<Date>                          <Author>                        <Change Description>
11/04/2017                      Fábio Mogo               		Versión inicial
<
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class FS_CORE_Fullstack_CheckRecursive{
    private static Boolean run = true;
    
    public static Boolean runOnce(){
        if(run){
            run = false;
            return true;
        }else{
            return run;
        }
    }
}