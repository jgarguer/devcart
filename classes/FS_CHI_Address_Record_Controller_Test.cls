/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Paloma De La Lastra Pinacho
Company:						Everis España
Description:					Clase de prueba: FS_CHI_Address_Record_Controller_Test

History:

<Date>							<Author>						<Change Description>
07/06/2017						Paloma De La Lastra Pinacho			Versión inicial
---------------------------------------------------------------------------------------*/
@isTest
public class FS_CHI_Address_Record_Controller_Test {
    
    @testSetup 
    private static void init() {
        /* Sede */
        BI_Sede__c sede = new BI_Sede__c(Estado__c = 'Validado', Name = 'Test-Address', BI_Direccion__c ='Manoteras',
                                         BI_Numero__c ='13', BI_Provincia__c ='Santiago', BI_Country__c ='Chile',
                                         BI_Codigo_postal__c = '28050', BI_Distrito__c = 'Chile',
                                         BI_Localidad__c = 'Santiago', BI_Piso__c = '2', BI_Apartamento__c ='A',
                                         FS_CHI_Bloque__c = '10', FS_CHI_Tipo_Complejo_Habitacional__c = 'A', FS_CHI_Nombre_Complejo_Habitacional__c = 'A',
                                         FS_CHI_Numero_de_Casilla__c = '10');
        insert sede;
    }
    
    @isTest private static void SearchAddress() {
        /* Test */
        System.Test.startTest();
        
        try{
            System.assertEquals(1, FS_CHI_Address_Record_Controller.SearchAddress([SELECT Id FROM BI_Sede__c LIMIT 1][0].Id).size());
        }catch(Exception exc){
            System.assert(false);
        }     
    } 
    @isTest private static void getNormalizedAddress() {
        /* Test */
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba2';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba2';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba2';
        insert config_integracion;
        System.Test.startTest();
        
        try{
            List<BI_Sede__c[]> sedes = (List<BI_Sede__c[]>) JSON.deserialize(FS_CHI_Address_Record_Controller.getNormalizedAddress([SELECT Id FROM BI_Sede__c LIMIT 1][0].Id), List<BI_Sede__c[]>.class);
            System.assertEquals(sedes.size(), 2);
        }catch(Exception exc){
            System.assert(false);
        }
    } 
     
    @isTest private static void updateAddress() {
        /* Test */
        System.Test.startTest();
        
        try{
            System.assert([SELECT Estado__c FROM BI_Sede__c LIMIT 1][0].Estado__c == 'Validado');
            FS_CHI_Address_Record_Controller.updateAddress([SELECT Id FROM BI_Sede__c LIMIT 1][0].Id ,'Test', 'Chile', 'Santiago', 'Chile', 'Chile','Test2', '3', '28050', 'xx', 'xx', '3A', 'B', '32', '48', '3', 'Test', 'Test','3', 'Red', 'listaNegra');
            System.assert([SELECT Estado__c FROM BI_Sede__c LIMIT 1][0].Estado__c == 'Pendiente de validar');
        }catch(Exception exc){
            System.assert(false);
        }  
    }  
}