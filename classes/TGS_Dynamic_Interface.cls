/*------------------------------------------------------------  
Author:         Marta Laliena   
Company:        Deloitte
Description:    Class to call INF2_Dinamic_Class_CreateAD WS
History
<Date>          <Author>        <Change Description>
09-jul-2015     Marta Laliena   Initial Version 
------------------------------------------------------------*/
public class TGS_Dynamic_Interface {

    
    public static String createConfigurationItem(NE__OrderItem__c configItem){
        System.debug('## dynamic integration createConfigItem');
        TGS_inf2DinamicClassCreatead.InterfaceSoap interfaceSoap = new TGS_inf2DinamicClassCreatead.InterfaceSoap();
        // AuthenticationInfo
        TGS_inf2DinamicClassCreatead.AuthenticationInfo authInfo = new TGS_inf2DinamicClassCreatead.AuthenticationInfo();
        authInfo.userName = 'Integration_user';
        authInfo.password = '273cr18J';
        //authInfo.authentication = '';
        //authInfo.locale = '';
        //authInfo.timeZone = '';
        interfaceSoap.parameters = authInfo;
        // AD_element
        List<NE__Order_Item_Attribute__c> listOrderItemAtt = configItem.NE__Order_Item_Attributes__r;
        List<TGS_inf2DinamicClassCreatead.AD_element> listAD = new List<TGS_inf2DinamicClassCreatead.AD_element>();
        for(NE__Order_Item_Attribute__c orderItemAtt : listOrderItemAtt){
            if(orderItemAtt.NE__FamPropId__r.TGS_Remedy_Integration__c){
                System.debug('## dynamic integration createConfigItem attribute: '+orderItemAtt.Name+' -> '+orderItemAtt.NE__Value__c);
                TGS_inf2DinamicClassCreatead.AD_element element = new TGS_inf2DinamicClassCreatead.AD_element();
                element.CIService = configItem.Name;
                element.Attribute = orderItemAtt.Name;
                element.AttributeID = orderItemAtt.NE__FamPropId__r.NE__Source_Product_Family_Property_Id__c;
                element.Value = orderItemAtt.NE__Value__c;
                element.Category = orderItemAtt.NE__FamPropId__r.TGS_Category__c;
                listAd.add(element);
            }
        }
        System.debug('## dynamic integration llamo WS');
        interfaceSoap.CreateAD(configItem.Name, listAD);
        
        return '';
    }
}