@isTest
public class CWP_CasesController_Lightning_Test{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    *
    * Author:        Belén Coronado
    *
    * Company:       everis
    *
    * Description:   ApexClass Test to CWP_ExportTicketsOrders_ctrl controller
    * History:
    * <Date>                          <Author>                                <Code>              <Change Description>
    * 25/05/2017                      Belén Coronado                                              Initial version
    */
    
    @testSetup 
    private static void dataModelSetup() {               
        
        //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{
            'Account', 
                'Case',
                'NE__Order__c',
                'NE__OrderItem__c'
                };
                    
                    //ACCOUNT
                    map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            //rtMapBydevName.put(i.DeveloperName, i.id);
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }          
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        insert contactTest;
        
        User usuario;
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 'Catalogo padre';
        producto.TGS_CWP_Tier_2__c = 'Catalogo hijo';
        insert producto;
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Product', catalogCategoryChildren.id, catalogo.id, producto.id);
        insert catalogoItem;
        
        TGS_GME_GM_Product_Categorization__c aTGS_GME= new  TGS_GME_GM_Product_Categorization__c();
        aTGS_GME.name='001';  
        aTGS_GME.TGS_Service__c='Producto Comercial';
        aTGS_GME.TGS_Categorization_tier_1__c = 'Catalogo padre';
        aTGS_GME.TGS_Categorization_tier_2__c = 'Catalogo hijo';
        aTGS_GME.TGS_Categorization_tier_3__c = 'Producto Comercial';
        insert aTGS_GME;
        
        // CONTRACT
        // Contract header
        NE__Contract_Header__c header = new NE__Contract_Header__c();
        header.NE__Name__c = 'contract header name';
        insert header;
        // Contract
        NE__Contract__c contract = new NE__Contract__c();
        contract.NE__Contract_Header__c = header.Id;
        insert contract;
        // Contract Account Association
        NE__Contract_Account_Association__c cAccount = new NE__Contract_Account_Association__c();
        cAccount.NE__Account__c = accLegalEntity.id;
        cAccount.NE__Contract_Header__c = header.Id;
        insert cAccount;     
        // Contract line item
        NE__Contract_Line_Item__c cLine = new NE__Contract_Line_Item__c();
        cLine.NE__Contract__c = contract.Id;
        cLine.NE__Commercial_Product__c = producto.Id;
        insert cLine;
        //ORDER
        
        //NE__Asset__c testAsset = CWP_TestDataFactory.createAsset();
        //insert testAsset;
        //Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
        //NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId, NE__Type__c = 'Asset');        
        //NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        //insert testOrder;
        
        
        
        //private final set<string> REQUESTRT = new set<string>{'Order_Management_Case'};
        //private final set<string> CASERT = new set<string>{'TGS_Billing_Inquiry', 'TGS_Change', 'TGS_Complaint', 'TGS_Incident', 'TGS_Problem', 'TGS_Query'};
        
        //CASO     
        list <Case> listaDeCasos = new list <Case>();                
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.contactId = contactTest.id;        
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Billing_Inquiry'), 'sub2', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Change'), 'sub3', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Complaint'), 'sub4', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Problem'), 'sub5', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Query'), 'sub6', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub7', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;                
        listaDeCasos.add(newCase);
        insert listaDeCasos;        
        
        
        // ORDER   
        NE__Order__c testOrder1= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        testOrder1.Case__c = listaDeCasos[1].id;
        insert testOrder1;
        
        NE__Order__c testOrder2 = CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        testOrder2.Case__c = listaDeCasos[0].id;
        insert testOrder2;        
        
        // ORDER ITEM
        NE__OrderItem__c newOI;        
        newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder1.id, producto.id, catalogoItem.id,  1);
        insert newOI;       
        
        newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder2.id, producto.id, catalogoItem.id,  2);
        newOI.NE__Status__c='Activo';
        insert newOI;      
        
        NE__Family__c family = CWP_TestDataFactory.createFamily('corleone');
        insert family;
        
        NE__DynamicPropertyDefinition__c dynProp = CWP_TestDataFactory.createDynamiyProperty('dynamic');
        insert dynProp;
        
        NE__ProductFamilyProperty__c famProp = CWP_TestDataFactory.createFamilyProperty(family.id, dynProp.id); 
        famProp.TGS_Is_key_attribute__c = true;
        insert famProp;
        
        NE__Order_Item_Attribute__c oia1 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        oia1.Name = 'picked';
        oia1.NE__Value__c = 'filter';
        insert oia1;
        NE__Order_Item_Attribute__c oia3 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        oia3.Name = 'picked2';
        oia3.NE__Value__c = 'filter';
        insert oia3;
        
        NE__Order_Item_Attribute__c oia2 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        insert oia2;      
        
        
        
    }
    
    @isTest
    private static void Test1() { 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();
            // Init Tickets
            String tContextString = CWP_CasesController.getTicketsContextController('init');
            
            CWP_CasesController.LightningContextTicketsAndOrders tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            // getAdvancedFilters
            tContextString = CWP_CasesController.getAdvancedFiltersValuesForTickets(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            
            // Filtro por familia
            tContext.pickListFamilyServiceValue = 'Catalogo padre';
            tContextString = CWP_CasesController.getTicketsContextController(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            // Filtro por servicio
            tContext.pickListServiceValue = 'Catalogo hijo';
            tContext.pickListServiceUnitValue = 'Producto Comercial';
            tContext.pickListStatusValue = 'Assigned';
            tContext.pickListCountryValue = 'Spain';
            tContext.pickListCityValue = 'Madrid';
            tContext.pickListShowValue = usuario.id;
            tContext.pickListCaseTypeValue = 'TGS_Incident';
            tContext.caseNumberFilter = '0000000';
            tContextString = CWP_CasesController.getTicketsContextController(JSON.serialize(tContext));
            
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.isForExport = false;
            tContextString = CWP_CasesController.getTicketsContextController(JSON.serialize(tContext));
            // Clear Advance Filters
            tContextString = CWP_CasesController.clearAdvancedFilters(JSON.serialize(tContext));
            // Paginación
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pagination = 'forward';
            tContextString = CWP_CasesController.getNewTableController(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pagination = 'backward';
            tContextString = CWP_CasesController.getNewTableController(JSON.serialize(tContext));
            Test.stopTest();
        }
    
    }
    
    @isTest
    private static void Test2() { 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();
            String tContextString = CWP_CasesController.getTicketsContextController('init');
            // loadPickListExcelModal
            CWP_CasesController.LightningContextTicketsAndOrders tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.TGS_CWP_STAT;
            tContextString = CWP_CasesController.loadPickListExcelModal(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.CWP_CaseType;
            tContextString = CWP_CasesController.loadPickListExcelModal(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.CWP_OpeningDate;
            tContextString = CWP_CasesController.loadPickListExcelModal(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.TGS_CWP_SERVICE;
            tContextString = CWP_CasesController.loadPickListExcelModal(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.TGS_CWP_T3;
            tContextString = CWP_CasesController.loadPickListExcelModal(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.TGS_CWP_COUNT;
            tContextString = CWP_CasesController.loadPickListExcelModal(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.TGS_CWP_CITY;
            tContextString = CWP_CasesController.loadPickListExcelModal(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.TGS_CWP_SB;
           
            tContextString = CWP_CasesController.loadPickListExcelModal(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.CWP_KeyValue;
            tContextString = CWP_CasesController.loadPickListExcelModal(JSON.serialize(tContext));
            Test.stopTest();
        }
    }
    
    @isTest
    private static void Test3() { 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();
            // Filtros excel
            String tContextString = CWP_CasesController.getTicketsContextController('init');
            CWP_CasesController.LightningContextTicketsAndOrders tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContextString = CWP_CasesController.getAdvancedFiltersValuesForTickets(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.TGS_CWP_STAT;
            tContext.inputExcelFilterValue = 'Assigned';
            tContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.CWP_CaseType;
            tContext.inputExcelFilterValue = 'Incident';
            tContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(tContext));
            tContext.pickedRow = Label.TGS_CWP_SERVICE;
            tContext.pickListServiceValue = 'Catalogo hijo';
            tContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(tContext));
            
            tContext.pickedRow = Label.TGS_CWP_COUNT;
            tContext.pickListCountryValue = 'Spain';
            tContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(tContext));
            tContext.pickedRow = Label.TGS_CWP_CITY;
            tContext.pickListCityValue = 'Madrid';
            tContext.pickListShowValue = usuario.id;
            tContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(tContext));
            tContext.pickedRow = Label.CWP_CaseNumber;
            tContext.caseNumberFilter = '0000000';
            tContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(tContext));
           
            
            
            tContextString = CWP_CasesController.quitaFiltroExcel(JSON.serialize(tContext));
            Test.stopTest();
            
        }
    }
    
    @isTest
    private static void Test4() { 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        Case caso = [SELECT Id, CaseNumber FROM Case LIMIT 1];    
        // Case Comment
        CaseComment caseCom = new CaseComment();
        caseCom.ParentId = caso.Id;
        caseCom.IsPublished = true;
        caseCom.CommentBody = 'comment body';
        insert caseCom;       
        System.runAs(usuario){
            Test.startTest();
            // Exportación
            String tContextString = CWP_CasesController.getTicketsContextController('init');
            CWP_CasesController.LightningContextTicketsAndOrders tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            CWP_CasesController.exportExcelPDF(JSON.serialize(tContext));
            
            // getIntegraRoD
            CWP_CasesController.getIntegraRoD(caso.CaseNumber);
            
            // Cambios de estado
            //CWP_CasesController.setAcceptTicket(caso.CaseNumber);
            CWP_CasesController.setCancelTicket(caso.CaseNumber);
            //CWP_CasesController.setReopenTicket(caso.CaseNumber);
            
            // History
            CWP_CasesController.getCaseHistoryList(caso.CaseNumber);
            
            // Case detail
            CWP_CasesController.getCaseDetails(caso.CaseNumber);
            CWP_CasesController.getCaseIdFromNumber(caso.CaseNumber);
            // Attachment
            CWP_CasesController.getAttachmentsCaseDetails(caso.CaseNumber);
            CWP_CasesController.saveTheFile(caso.Id, 'filename', 'base64Data', 'tipo de contenido', '');
            CWP_CasesController.saveTheChunk('filename', 'fileDescription', 'base64Data', 'contentType', CWP_CasesController.saveTheFile(caso.Id, 'filename', 'base64Data', 'tipo de contenido',''), 'caso');
            // Comments
            CWP_CasesController.getCommentsCaseDetails(caso.CaseNumber);
            CWP_CasesController.saveCommentCase(caso.CaseNumber, 'New comment body');
            
            
            Test.stopTest();
        }
    }
    
    @isTest
    private static void Test5() { 
        // Orders
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        Case caso = [SELECT CaseNumber FROM Case LIMIT 1];           
        System.runAs(usuario){
            Test.startTest();
            String orderContextString = CWP_CasesController.getTicketsContextController('init orders');
            CWP_CasesController.LightningContextTicketsAndOrders oContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(orderContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            orderContextString = CWP_CasesController.getAdvancedFiltersValuesForTickets(JSON.serialize(oContext));
            oContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(orderContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            oContext.pickListCaseTypeValue = 'TGS_Incident';
            orderContextString = CWP_CasesController.getTicketsContextController(JSON.serialize(oContext));
            oContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(orderContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            Test.stopTest();
        }
    }
    
    @isTest
    private static void Test6() { 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();
            // Filtros excel
            String tContextString = CWP_CasesController.getTicketsContextController('init orders');
            CWP_CasesController.LightningContextTicketsAndOrders tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContextString = CWP_CasesController.getAdvancedFiltersValuesForTickets(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.TGS_CWP_STAT;
            tContext.inputExcelFilterValue = 'Assigned';
            tContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(tContext));
            tContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(tContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            tContext.pickedRow = Label.CWP_CaseType;
            tContext.inputExcelFilterValue = 'Incident';
            tContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(tContext));
            tContext.pickedRow = Label.TGS_CWP_T3;
            tContext.pickListServiceUnitValue = 'Producto Comercial';
            tContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(tContext));
            tContext.pickedRow = Label.TGS_CWP_CITY;
            tContext.pickListCityValue = 'Madrid';
            tContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(tContext));
            tContext.pickListShowValue = usuario.id;
            tContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(tContext));
            
            tContext.pickedRow = Label.TGS_CWP_SB;
            tContext.caseNumberFilter = 'subject';
            tContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(tContext));
                       
            Test.stopTest();
        }
    }
    
    @isTest
    private static void Test7() { 
        // Orders by key value
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        Case caso = [SELECT CaseNumber FROM Case LIMIT 1];           
        System.runAs(usuario){
            Test.startTest();
            String orderContextString = CWP_CasesController.getTicketsContextController('init orders');
            CWP_CasesController.LightningContextTicketsAndOrders oContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(orderContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            orderContextString = CWP_CasesController.getAdvancedFiltersValuesForTickets(JSON.serialize(oContext));
            oContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(orderContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            oContext.isForExport = false;
            oContext.sortedRow = 'CWP_KeyValue__c';
            oContext.sortedOrder = 'ASC';
            orderContextString = CWP_CasesController.getTicketsContextController(JSON.serialize(oContext));
            oContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(orderContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            oContext.keyValueFilter = 'filter';
            orderContextString = CWP_CasesController.getTicketsContextController(JSON.serialize(oContext));
            oContext = (CWP_CasesController.LightningContextTicketsAndOrders)JSON.deserialize(orderContextString, CWP_CasesController.LightningContextTicketsAndOrders.class);
            oContext.pickedRow = Label.CWP_OpeningDate;
            oContext.inputExcelFilterValue = '2016-02-16';
            orderContextString = CWP_CasesController.addExcelFilterValueCtr(JSON.serialize(oContext));
            
            Test.stopTest();
        }
    }
}