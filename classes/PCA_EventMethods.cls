public class PCA_EventMethods {
		 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Methods executed by Event Triggers 
    
    History:
    
    <Date>					  <Author> 	       		 <Description>
    01/02/2014 				Jorge Longarela			Initial Version	
    03/07/2014               Pablo OLiva             PCA_Event v2
    22/08/2014				 Ana Escrich			 sendEmail
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ana Escrich Acero
	    Company:       Salesforce.com
	    Description:   Send email to event portal users (after insert)
	    
    	History: 
	    
	    <Date>                  <Author>                <Change Description>
  	    22/08/2014              Ana Escrich Acero           Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	/*public static void sendEmail(List <Event> news){
		try{
			//filter events by createdById
			List<Id> events = new List<Id>();
			for(Event item: news){
				events.add(item.Id);
			}
			List<EventRelation> eRelations1 = [SELECT EventId,Id,RelationId FROM EventRelation WHERE EventId in :events];
			system.debug('eRelations1: '+eRelations1);
			List<Event> SFDC_Events = [select Id, CreatedBy.ProfileId, Description, Subject, StartDateTime, EndDateTime, DurationInMinutes, ActivityDateTime from Event where Id in :events];
			Map<Id, Event> mapEvents = new Map<Id, Event>();
			Profile profile = [select Id from Profile where Name = 'Customer Community User Clone' limit 1];
			events = new List<Id>();
			for(Event item: SFDC_Events){
				if(profile!=null && profile.Id!=item.CreatedBy.ProfileId){
					events.add(item.Id);
					mapEvents.put(item.Id, item);
				}
			}
			system.debug('events: '+events);
			List<EventRelation> eRelations = [SELECT EventId,Id,RelationId FROM EventRelation WHERE EventId in :events];
			
			system.debug('eRelations: '+eRelations);
			if(!eRelations.isEmpty()){
				List<Id> contactRelations = new List<Id>();
				Map<Id, List<EventRelation>> mapEventRelations = new Map<Id, List<EventRelation>>();
				for(EventRelation item: eRelations){
					if(mapEventRelations.containsKey(item.EventId)){
						mapEventRelations.get(item.EventId).add(item);
					}
					else{
						mapEventRelations.put(item.EventId, new List<EventRelation>{item});
					}
					contactRelations.add(item.RelationId);
				}
				
				List<Contact> contactRelated = [select Id, Name, Email from Contact where Id in :contactRelations];
				Map<Id, String> mapContacts= new Map<Id, String>();
				for(Contact item: contactRelated){
					mapContacts.put(item.Id, item.Email);
				}
				
				List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
				List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject, DeveloperName, Body from EmailTemplate where DeveloperName='BI_Comunicacion_nuevo_evento'];
				Map<Id, List<String>> mapEmails = new Map<Id, List<String>>(); 
				for(Event item: news){
					String[] toaddress = new String[]{};
					if(mapEventRelations.containsKey(item.Id)){
						for(EventRelation eRelation: mapEventRelations.get(item.Id)){
		    				if(mapContacts.containsKey(eRelation.RelationId)){
		    					toaddress.add(mapContacts.get(eRelation.RelationId));
		    				}
		    			}
		    			Messaging.SingleEmailMessage singleMail = sendEmail(templates[0], toaddress);
		    			emails.add(singleMail);
					}
					
				}
				Messaging.sendEmail(emails);
			
					//set object Id
					/*singleMail.saveAsActivity=false;
					singleMail.setToAddresses(toaddress);
					 //set template Id
					singleMail.setTemplateId(templates[0].Id);
					if(item.Parent.ContactId!=null){
						singleMail.setTargetObjectId(item.Parent.ContactId);
					}
					else{
						singleMail.setTargetObjectId(portalContacts[0].BI_Contacto__c);
					}
					singleMail.setWhatId(item.ParentId);
					emails.add(singleMail);*/
			
				//Messaging.sendEmail(emails);
			/*}
			
		}
		catch(exception e){
			
			BI_LogHelper.generateLog('BI_CaseMethods.sendEmail', 'Portal Platino', e.getMessage()+', '+e.getLineNumber()+', '+e.getCause()+', '+e.getTypeName(), 'Class');
		}
	}*/
	public static void sendEmail(List <Event> news, List <Event> olds){
		Map<Id, Event> newsIds = new Map<Id, Event>(); 
		Map<Id, Event> oldsIds = new Map<Id, Event>(); 
		for(Event item : news){
			newsIds.put(item.Id, item);
		}
		for(Event item : olds){
			oldsIds.put(item.Id, item);
		}
		for(Event item : news){
			if(oldsIds.containsKey(item.Id)){
				Event old = oldsIds.get(item.Id);//StartDateTime, EndDateTime,
				if(item.ActivityDateTime!=old.ActivityDateTime || item.StartDateTime!=old.StartDateTime || item.EndDateTime!=old.EndDateTime){
					item.BI_Correo_electronico_enviado__c = false;
				}
			}
		}
		/*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(toaddress);
		mail.setHtmlBody(template.HTMLValue);
		mail.setSubject(template.Subject);
		mail.setPlainTextBody(template.Body);
		return mail;*/
	}
	
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ana Escrich Acero
	    Company:       Salesforce.com
	    Description:   Insert or update events
	    
    	History: 
	    
	    <Date>                  <Author>                <Change Description>
  	    21/07/2014              Ana Escrich Acero           Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	/*public static Messaging.SingleEmailMessage sendEmail(EmailTemplate template,String[] toaddress){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(toaddress);
		mail.setHtmlBody(template.HTMLValue);
		mail.setSubject(template.Subject);
		mail.setPlainTextBody(template.Body);
		return mail;
	}*/

}