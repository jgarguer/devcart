/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Pardo Corral
Company:       Accenture
Description:   Controller for the component BI_SF1_RecordDetail CustomMetadata BI_SF1_Layout_assignments_SF1__mdt is the core of this class,
                any cofiguration for this component is stored there.

History: 
<Date>                  <Author>                <Change Description>
30/11/2017              Antonio Pardo corral    Initial Version 
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_SF1_RecordDetail_Ctrl {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Pardo Corral
    Company:       Accenture
    Description:   Returns the layout information and the object information needed to print the component recordDetail

    IN:             String recordId - Id of the record
                    
    OUT:            Map<String, Object> - Map with the object info and the layout info, both stored in "objInfo" and "layoutConfig", the objInfo contains
                    the object retrieved by the query with no manipulation, the header and description showed at the top of the component.
                    layoutConfig contains the info of the customMetadata BI_SF1_Layout_assignments_SF1__mdt.
                    null - when no custommetadata is found

    History: 
    <Date>                  <Author>                <Change Description>
    30/11/2017              Antonio Pardo corral    Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static Map<String, Object> getLayoutConfig(Id recordId){

        Map<String, Object> map_return = new Map<String, Object>();
        List<Map<String, String>> actions = new List<Map<String, String>>();
        Map<String, Profile> map_profiles = new Map<String, Profile>([SELECT Id, Name FROM Profile WHERE Id =:UserInfo.getProfileId() LIMIT 1]);


        String recordTypeQuery = 'SELECT RecordType.DeveloperName FROM ' + recordId.getSobjectType().getDescribe().getName() + ' WHERE Id = \'' + recordId + '\' LIMIT 1 FOR VIEW';
        System.debug(recordTypeQuery);

        String rtDevName = String.valueOf(Database.query(recordTypeQuery)[0].getSObject('RecordType').get('DeveloperName'));
        System.debug(rtDevName);
        
        String userRt = (map_profiles.get(UserInfo.getProfileId()).Name == 'System administrator') ? 'Administrador del sistema' : map_profiles.get(UserInfo.getProfileId()).Name;

        //Query to retrieve the layout configuration of the component, for Profile, Object and Object recordType
        List<BI_SF1_Layout_assignments_SF1__mdt> layoutConfig = [SELECT DeveloperName, MasterLabel, Language, NamespacePrefix, Label, QualifiedApiName, BI_SF1_Perfil__c, BI_SF1_Objeto__c, BI_SF1_Conjunto_de_campos__c,
                                                BI_SF1_Icono_de_la_cabecera__c, BI_SF1_Campos_titulo_cabecera__c, BI_SF1_Campos_subtitulo_cabecera__c, BI_SF1_Tipo_de_registro__c, BI_SF1_Campos_query_Id__c,
                                                (SELECT DeveloperName, MasterLabel, Label, QualifiedApiName, BI_SF1_Nombre_lista_relacionada__c, BI_SF1_Icono__c, BI_SF1_Orden__c FROM Related_lists_configuration__r ORDER BY BI_SF1_Orden__c),
                                                (SELECT DeveloperName, MasterLabel, Label, BI_SF1_Icono__c, BI_SF1_accion__c, BI_SF1_Campos_por_defecto__c,BI_SF1_Orden__c FROM quickAction_SF1__r ORDER BY BI_SF1_Orden__c)
                                                FROM BI_SF1_Layout_assignments_SF1__mdt WHERE BI_SF1_Objeto__c = :recordId.getSobjectType().getDescribe().getName() AND BI_SF1_Perfil__c = :userRt AND BI_SF1_Tipo_de_registro__c = :rtDevName LIMIT 1];
        
        //If there's no configuration for that profile and given object
        if(layoutConfig.isEmpty()){
            //There's another query to retrieve the layout configuration for a given object with no filter over recordType or Profile, but BI_SF1_DefaultConfig__c set to true
            layoutConfig = [SELECT DeveloperName, MasterLabel, Language, NamespacePrefix, Label, QualifiedApiName, BI_SF1_Perfil__c, BI_SF1_Objeto__c, BI_SF1_Conjunto_de_campos__c,
                                                BI_SF1_Icono_de_la_cabecera__c, BI_SF1_Campos_titulo_cabecera__c, BI_SF1_Campos_subtitulo_cabecera__c, BI_SF1_Tipo_de_registro__c, BI_SF1_Campos_query_Id__c,
                                                (SELECT DeveloperName, MasterLabel, Label, QualifiedApiName, BI_SF1_Nombre_lista_relacionada__c, BI_SF1_Icono__c, BI_SF1_Orden__c FROM Related_lists_configuration__r ORDER BY BI_SF1_Orden__c),
                                                (SELECT DeveloperName, MasterLabel, Label, BI_SF1_Icono__c, BI_SF1_accion__c, BI_SF1_Campos_por_defecto__c,BI_SF1_Orden__c FROM quickAction_SF1__r ORDER BY BI_SF1_Orden__c)
                                                FROM BI_SF1_Layout_assignments_SF1__mdt WHERE BI_SF1_Objeto__c = :recordId.getSobjectType().getDescribe().getName() AND BI_SF1_DefaultConfig__c = true LIMIT 1];
        }
        if(layoutConfig!=null && !layoutConfig.isEmpty()){
            
            map_return.put('layoutConfig', layoutConfig[0]);
            
            String fieldsToQuery = '';
            //retrieving fields that can contain multiple Api names separated by ';'
            //This API names are used in the query to get the values used to print the values on the component
            if(layoutConfig[0].BI_SF1_Campos_subtitulo_cabecera__c.contains(';')){
                fieldsToQuery += layoutConfig[0].BI_SF1_Campos_subtitulo_cabecera__c.replaceAll(';', ',') + ',';
            }else{
                fieldsToQuery += layoutConfig[0].BI_SF1_Campos_subtitulo_cabecera__c + ',';
            }

            if(layoutConfig[0].BI_SF1_Campos_titulo_cabecera__c.contains(';')){
                fieldsToQuery += layoutConfig[0].BI_SF1_Campos_titulo_cabecera__c.replaceAll(';', ',') + ',';
            }else{
                fieldsToQuery += layoutConfig[0].BI_SF1_Campos_titulo_cabecera__c + ',';
            }
        
            //Field apiName that are included on the fieldSet
            for(String fieldLabel : fieldSetFields(layoutConfig[0].BI_SF1_Conjunto_de_campos__c,recordId.getSobjectType().getDescribe().getName())){
                if(!fieldsToQuery.contains(fieldLabel)){

                    fieldsToQuery += fieldLabel + ',';
                }
            }

            //Additional fields mostly used to query Ids to link records created or whatever to the main record
            //BE AWARE OF DUPLICATE VALUES IN THE QUERY CAUSED BY LAYOUTCONFIG MISCONFIGURATION
            fieldsToQuery += layoutConfig[0].BI_SF1_Campos_query_Id__c;

            String objectQuery = 'SELECT ' + fieldsToQuery + ' FROM ' + layoutConfig[0].BI_SF1_Objeto__c + ' WHERE Id = \'' + recordId + '\' LIMIT 1';
            System.debug(objectQuery);
            
            //Dynamic query to get requested record values
            sObject recordInfo = Database.query(objectQuery)[0];
            
            //to make easiar JavaScritp coding, I call this transfotmSobject
            map_return.put('objInfo', transformSobject(layoutConfig[0].BI_SF1_Objeto__c, recordInfo, layoutConfig[0]));

            return map_return;
        }else{
            //If there's no custom metadata for the given object the value is null
            return null;
        }

    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Pardo Corral
    Company:       Accenture
    Description:   returns a modified version of an object just to bind the variables to the component

    IN:             String objectType - Object api name
                    Sobject record - sObject retrieved by query
                    BI_SF1_Layout_assignments_SF1__mdt config - custom metadata corresponding to the object
                    
    OUT:            Map<String, Object> - Map with object info, header title and subtitle

    History: 
    <Date>                  <Author>                <Change Description>
    30/11/2017              Antonio Pardo corral    Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static Map<String, Object> transformSobject(String objectType, Sobject record , BI_SF1_Layout_assignments_SF1__mdt config){

        System.debug('transformSobjectRecordDetail');
        
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(objectType);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();

     
        Map<String, Object> map_objectInfo = new Map<String, Object>();
        map_objectInfo.put('recordInfo', record);

        List<Map<String, String>> list_descriptionInfo = new List<Map<String, String>>();
        
        String title = '';

        if(config.BI_SF1_Campos_titulo_cabecera__c.contains(';')){     

            for(String fieldName : config.BI_SF1_Campos_titulo_cabecera__c.split(';')){
                if(fieldName.contains('.')){
                    List<String> fieldRelation = fieldName.split('\\.');
                     title += record.getSobject(fieldRelation[0]).get(fieldRelation[1])!=null ? record.getSobject(fieldRelation[0]).get(fieldRelation[1]) + ' ' : '';
                }else{
                    title += (String)record.get(fieldName)!=null ? (String)record.get(fieldName) + ' ' : '';
                }
          
            }
            map_objectInfo.put('headerTitle', title);
        
        }else{

            if(config.BI_SF1_Campos_titulo_cabecera__c.contains('.')){
            List<String> fieldRelation = config.BI_SF1_Campos_titulo_cabecera__c.split('\\.');
                    title += record.getSobject(fieldRelation[0]).get(fieldRelation[1])!=null ? record.getSobject(fieldRelation[0]).get(fieldRelation[1])  + ' ' : '';
            }else{
                title += (String)record.get(config.BI_SF1_Campos_titulo_cabecera__c)!=null ? (String)record.get(config.BI_SF1_Campos_titulo_cabecera__c) + ' ' : '';
            }


            map_objectInfo.put('headerTitle', title.removeEnd(' '));
        
        }
        
        String subtitle = ''; 

        if(config.BI_SF1_Campos_subtitulo_cabecera__c.contains(';')){     

            for(String fieldName : config.BI_SF1_Campos_subtitulo_cabecera__c.split(';')){
                
                if(fieldName.contains('.')){
                    List<String> fieldRelation = fieldName.split('\\.');
                    subtitle += record.getSobject(fieldRelation[0]).get(fieldRelation[1])!=null ? record.getSobject(fieldRelation[0]).get(fieldRelation[1]) + ' ' : '';
                }else{
                    subtitle += (String)record.get(fieldName)!=null ? (String)record.get(fieldName) + ' ' : '';
                }
          
            }
            map_objectInfo.put('headerSubtitle', subtitle);

        
        }else{
            if(config.BI_SF1_Campos_subtitulo_cabecera__c.contains('.')){
                List<String> fieldRelation = config.BI_SF1_Campos_subtitulo_cabecera__c.split('\\.');
                subtitle += record.getSobject(fieldRelation[0]).get(fieldRelation[1])!=null ? record.getSobject(fieldRelation[0]).get(fieldRelation[1]) + ' ' : '';
            }else{
                subtitle += (String)record.get(config.BI_SF1_Campos_subtitulo_cabecera__c)!=null ?  (String)record.get(config.BI_SF1_Campos_subtitulo_cabecera__c) + ' ' : '';
            }

            map_objectInfo.put('headerSubtitle', subtitle.removeEnd(' '));
          
            
        }
        return map_objectInfo;

    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Pardo Corral
    Company:       Accenture
    Description:   Given a fieldset name retrieves the apiNames

    IN:             String fieldSet - fieldset name
                    String objName -  object api name
                    
    OUT:            List<String> - apiName of the fields in the fieldSet

    History: 
    <Date>                  <Author>                <Change Description>
    30/11/2017              Antonio Pardo corral    Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static List<String> fieldSetFields(String fieldSet, String objName){
        
        List<String> lstLabel = new List<String>();
        
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSet);

         for (Schema.FieldSetMember fld : fieldSetObj.getFields()){
               
            lstLabel.add(fld.getFieldPath());
            System.debug(fld.getFieldPath());
            
        }

        return lstLabel;
    }
    
}