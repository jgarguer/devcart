global with sharing class GenerateOrder {

    WebService static String generateOrder (String optyId) 
    { 
        String newOrdId;
        String errorCode;
        String urlPage;
        
        try {
          //To skip the querys of an invalid id
          if(optyId != null && optyId.length() > 8){
            System.debug('generateOrder optyId valida');
            List<NE__Order__c> oldOrd = [SELECT currencyIsoCode,
                                 NE__Contract_Account__c,
                                 NE__Contract_Header__c,
                                 NE__ServAccId__c, 
                                 NE__BillAccId__c,
                                 NE__AccountId__c, 
                                 NE__Type__c,  
                                 NE__ConfigurationStatus__c, 
                                 Id,
                                 Name,
                                 NE__OpportunityId__c,
                                 NE__OptyId__c,
                                 NE__Version__c,
                                 RecordTypeId,
                                 NE__OrderStatus__c,
                                 NE__SerialNum__c
                            FROM NE__Order__c 
                            WHERE NE__OptyId__c =: optyId 
                            order BY NE__Version__c desc limit 1];
            
            Opportunity opty    =   [SELECT Pedido__c,
                                      Generate_Acuerdo_Marco__c,
                                      BI_Acuerdo_Marco__c 
                                     FROM Opportunity 
                                     WHERE id =: optyId];   
            
            if(oldOrd.size() > 0) 
            {
                if(opty.Generate_Acuerdo_Marco__c == false)
                {
                    NE.DataMap mp = new NE.DataMap();
                    Map<String,String> gen_map = mp.GenerateMapObjects('Order2Order', oldOrd[0].Id);
                    system.debug('*** gen_map = '+gen_map);
                    
                    if(gen_map != null) {
                        newOrdId = gen_map.get('ParentId');
                        errorCode = gen_map.get('ErrorCode'); 
                        system.debug('*ERRORCODE ' + errorCode);
                        
                        if(errorCode.equalsIgnoreCase('0')) {                       
                            
                            NE__Order__c ord = oldOrd[0];
                            RecordType recType = [SELECT Id 
                                    FROM RecordType 
                                    WHERE (SobjectType = 'Order__c' OR SobjectType = 'NE__Order__c') 
                                    AND Name = 'Order' limit 1];

                            ord = [SELECT currencyIsoCode, 
                                       NE__Contract_Account__c,
                                       NE__Contract_Header__c,
                                       RecordTypeId,
                                       NE__ServAccId__c, 
                                       NE__BillAccId__c, 
                                       NE__OrderStatus__c,
                                       NE__AccountId__c, 
                                       NE__Type__c,  
                                       NE__ConfigurationStatus__c,
                                       NE__OpportunityId__c,
                                       NE__OptyId__c 
                                   FROM NE__Order__c 
                                   WHERE Id =: newOrdId];
                            
                            ord.NE__OrderStatus__c          =   'Pending';
                            ord.NE__ConfigurationStatus__c  =   'Valid';
                            ord.RecordTypeId                =   recType.Id;
                            ord.NE__OpportunityId__c        =   optyId;
                            ord.NE__OptyId__c               =   optyId;
                            
                            NE__Order_Header__c oh = new NE__Order_Header__c( 
                                        NE__ServAccId__c = ord.NE__ServAccId__c, 
                                        NE__BillAccId__c = ord.NE__BillAccId__c, 
                                        NE__AccountId__c = ord.NE__AccountId__c,
                                        NE__OrderId__c = ord.Id, 
                                        NE__OrderStatus__c = ord.NE__OrderStatus__c,
                                        NE__Type__c = ord.NE__Type__c);   
                            insert oh;      
                                                                            
                            opty.Pedido__c      =   ord.id;
                                            
                            update  opty; 
                            update  ord;
                            
                            urlPage = newOrdId;
                        }
                    }
                }
                else if(opty.Generate_Acuerdo_Marco__c == true && opty.BI_Acuerdo_Marco__c == null)
                {
                    NE__Order__c ord    =   oldOrd[0];
                    String contractId   =   NE.JS_RemoteMethods.GenerateContractFromQuote(ord.id);
                    
                    system.debug('ord.NE__AccountId__c: '+ord.NE__AccountId__c+' contractHId: '+contractId);
                    
                    //Retrieve the account contract
                    NE__Contract__c contract                        =   [SELECT id, NE__Contract_Policy__c,currencyIsoCode, NE__Contract_Header__c, (SELECT id, NE__Configuration_Item__r.NE__OneTimeFeeOv__c, NE__Configuration_Item__r.NE__RecurringChargeOv__c,NE__Base_OneTime_Fee__c, NE__Base_Recurring_Charge__c, NE__Configuration_Item__c,NE__Configuration_Item__r.currencyIsoCode,currencyIsoCode FROM NE__Contract_Line_Items__r) FROM NE__Contract__c WHERE id =: contractId];                                                     
                    NE__Contract_Account_Association__c contrAcc    =   [SELECT id, currencyIsoCode FROM NE__Contract_Account_Association__c WHERE NE__Account__c =: ord.NE__AccountId__c AND NE__Contract_Header__c =: contract.NE__Contract_Header__c];                           
                    NE__Contract_Header__c contrHeader              =   [SELECT id, NE__Order_Agreement__c , currencyIsoCode FROM NE__Contract_Header__c WHERE Id =: contract.NE__Contract_Header__c];                          
                    
                    opty.BI_Acuerdo_Marco__c                        =   contrAcc.id;
                    //ord.NE__Contract_Account__c                   =   contrAcc.id;                                
                    //ord.NE__Contract_Header__c                    =   contract.NE__Contract_Header__c;            
                    
                    contract.currencyIsoCode                        =   ord.currencyIsoCode;
                    contract.NE__Contract_Policy__c                 =   'Pricing';
                    contrAcc.currencyIsoCode                        =   ord.currencyIsoCode;
                    contrHeader.currencyIsoCode                     =   ord.currencyIsoCode;
                    contrHeader.NE__Order_Agreement__c              =   ord.id;
                    
                    for(NE__Contract_Line_Item__c cli:contract.NE__Contract_Line_Items__r)
                    {   
                        cli.currencyIsoCode                 =   cli.NE__Configuration_Item__r.currencyIsoCode;
                        cli.NE__Base_Recurring_Charge__c    =   cli.NE__Configuration_Item__r.NE__RecurringChargeOv__c;
                        cli.NE__Base_OneTime_Fee__c         =   cli.NE__Configuration_Item__r.NE__OneTimeFeeOv__c;
                    }   
                    
                    update  contract.NE__Contract_Line_Items__r; 
                    update  contract;
                    update  contrAcc;
                    update  contrHeader;    
                    update  opty; 
                    
                    urlPage =   contract.id;    
                }                           
            }
            
          }//end if skipping querys  
        }
        catch(Exception e)
        {
            system.debug('Exception: '+e);
            urlPage = '';
        }
        
        system.debug('urlPage: ' + urlPage);

        return urlPage; 
    } 

}