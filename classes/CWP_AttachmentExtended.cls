public with sharing class CWP_AttachmentExtended {
	public Attachment attachment;
	public String message;
	public CWP_AttachmentExtended(Attachment attachment, String message) {
		attachment = attachment;
		message = message;
	}
}