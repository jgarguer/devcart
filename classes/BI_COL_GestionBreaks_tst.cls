/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Test Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                             Descripción
*           -----   ----------      --------------------              ---------------
* @version   1.0    2015-03-24      Manuel Esthiben Mendez Devia (MEMD)     Test Class
*************************************************************************************************************/
@isTest
private class BI_COL_GestionBreaks_tst 
{

    Public static list <UserRole> 									lstRoles;
	Public static User 												objUsuario;
    public static Opportunity                                       objOppty;
      
    public static Account                                           objCuenta;

    public static List <BI_COL_Modificacion_de_Servicio__c>          lstModSer = new List <BI_COL_Modificacion_de_Servicio__c>();
    public static BI_COL_Modificacion_de_Servicio__c                objModSer;

    public static List<BI_COL_Descripcion_de_servicio__c>           lstDesSer;
    public static BI_COL_Descripcion_de_servicio__c                 objDesSer;
    public static BI_COL_Seguimiento_Quiebre__c      objSegMien;
    public static BI_Log__c           objLog;

    public static List<RecordType>                                  rtBI_FUN;
    public static List<String>          lstIdquiebre = new List<String>();         
    public static String Enviada;
    public static String Respuesta;



   static void crearData()
    {
        //List<Profile> lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
        ////list<User> lstUsuarios = [Select Id FROM User where country != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

        ////lstRol
        //lstRoles = new list <UserRole>();
        //lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        ////ObjUsuario
        //objUsuario = new User();
        //objUsuario.Alias = 'standt';
        //objUsuario.Email ='pruebas@test.com';
        //objUsuario.EmailEncodingKey = '';
        //objUsuario.LastName ='Testing';
        //objUsuario.LanguageLocaleKey ='en_US';
        //objUsuario.LocaleSidKey ='en_US'; 
        //objUsuario.ProfileId = lstPerfil.get(0).Id;
        //objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        //objUsuario.UserName ='pruebas@test.com';
        //objUsuario.EmailEncodingKey ='UTF-8';
        //objUsuario.UserRoleId = lstRoles.get(0).Id;
        //objUsuario.BI_Permisos__c ='Sucursales';
        //objUsuario.Pais__c='Colombia';
        //insert objUsuario;
        
        //list<User> lstUsuarios = new list<User>();
        //lstUsuarios.add(objUsuario);

			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass;
			objCuenta                     = new Account();
			objCuenta.Name                  = 'prueba';
			objCuenta.BI_Country__c             = 'Colombia';
			objCuenta.TGS_Region__c             = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c  = 'NIT';
			objCuenta.CurrencyIsoCode             = 'GTQ';
			insert objCuenta;
			System.debug('\n\n\n ======= CUENTA ======\n '+ objCuenta );
			objOppty                    = new Opportunity();
			objOppty.Name                   = 'TEST AVANXO OPPTY';
			objOppty.AccountId                = objCuenta.Id;
			//objOppty.TGS_Region__c            = 'América';
			objOppty.BI_Country__c              = 'Colombia';
			objOppty.CloseDate                = system.today().addDays(1);
			objOppty.StageName                = 'F6 - Prospecting';
			objOppty.BI_Ciclo_ventas__c           = Label.BI_Completo;
			insert objOppty;
			objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c                     = objOppty.Id;
			//objDesSer.CurrencyIsoCode                           = 'COP';
			insert objDesSer;
			System.debug('\n\n\n ======= objDesSer '+ objDesSer);
            objModSer                                   = new BI_COL_Modificacion_de_Servicio__c();
            objModSer.BI_COL_Codigo_unico_servicio__c   = objDesSer.Id;
            objModSer.BI_COL_Clasificacion_Servicio__c  = 'ALTA';
            objModSer.BI_COL_Estado__c                  = 'Pendiente';
            objModSer.BI_COL_Fecha_instalacion_servicio_RFS__c  = system.today().addDays(1);
            objModSer.BI_COL_Oportunidad__c             = objOppty.Id;
            //objModSer.BI_COL_Direccion_IP__c            = i+'';
            //insert objModSer;
            lstModSer.add(objModSer);      
            insert lstModSer;
            System.debug('\n\n\n ======= lstModSer '+ lstModSer);
			objSegMien          = new BI_COL_Seguimiento_Quiebre__c();
			objSegMien.BI_COL_Observaciones__c   = 'Prueba avanxo';
			objSegMien.BI_COL_Modificacion_de_Servicio__c = lstModSer[0].Id;
			objSegMien.BI_COL_Tipo_gestion__c   = 'Informativo';
			objSegMien.BI_COL_Transaccion__c   =null;
			objSegMien.BI_COL_Aplazar_MS__c    =true;
			objSegMien.BI_COL_Codigo_Quiebre_Sisgot__c ='Fallido Conexion';
			objSegMien.BI_COL_Estado_de_Gestion__c  ='Abierto';
			objSegMien.BI_COL_Estado_envio__c   ='Creado SFDC';
			insert objSegMien;
			lstIdquiebre.add(objSegMien.Id);
			
			objLog              = new BI_Log__c();
			objLog.BI_COL_Informacion_recibida__c      = 'OK, Respuesta Procesada';
			//objLog.BI_COL_Contacto__c          = lstContacts[0].Id;
			objLog.BI_COL_Estado__c          = 'FALLIDO';
			objLog.BI_COL_Interfaz__c         = 'NOTIFICACIONES';
			objLog.BI_COL_Informacion_Enviada__c      = Enviada;
			objLog.BI_COL_Informacion_recibida__c      = Respuesta;
			
		   //lstLog.add(objLog); 
		   insert objLog; 
        

    }

    static testmethod void test1()
   {
	   objUsuario=BI_COL_CreateData_tst.getCreateUSer();

        System.runAs(objUsuario)
        {
		   crearData();
		   ID IDquiebre = objSegMien.Id;
		   String accion= 'crear';
		   String observacion = null;
		   Test.startTest();
		   Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );
		   Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
		   Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws() );
		   BI_COL_GestionBreaks_ctr.gestionQuiebres(objSegMien,accion,observacion);
		   BI_COL_GestionBreaks_ctr.registroLog(Enviada,IDquiebre,Respuesta);
		   BI_COL_GestionBreaks_ctr.actualizarMs(IDquiebre);
		   
		   objSegMien.BI_COL_Observaciones__c = 'ObservacionesPrueba';
		   objSegMien.BI_COL_Actualizacion_gestion__c = 'Actualización Gestion';
		   objSegMien.BI_COL_Observaciones__c = 'ObservacionesPrueba';
		   objSegMien.BI_COL_Estado_de_Gestion__c = 'Cerrado';
		   update objSegMien;
		   
		   BI_COL_GestionBreaks_ctr.gestionQuiebres(objSegMien, 'Creado SFDC', 'ObservacionesPrueba');
		   
		   objSegMien.BI_COL_Estado_de_Gestion__c = 'Cerrado';
		   BI_COL_GestionBreaks_ctr.gestionQuiebres(objSegMien, 'Creado SFDC', 'ObservacionesPrueba');
		  
		   Test.stopTest();
		}
  }
 
  static testmethod void test2()
  {
  		objUsuario=BI_COL_CreateData_tst.getCreateUSer();

		System.runAs(objUsuario)
		{
		  	crearData();
		    Test.startTest();
		  	Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws() );
		  	BI_COL_GestionBreaks_ctr.envioQuiebreSW(''+objSegMien.Id, 'anteriorObservacion','crear',System.today().addDays(2));
		    Test.stopTest();
		}
  }

  static testmethod void test3()
   {
	   objUsuario=BI_COL_CreateData_tst.getCreateUSer();

		System.runAs(objUsuario)
		{
		   crearData();
		   objSegMien.BI_COL_Fecha_contactar_cliente__c=system.today().addDays(3);
		   update objSegMien;
		   Test.startTest();
		   Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws() );
		   BI_COL_GestionBreaks_ctr.envioQuiebreSW(''+objSegMien.Id, 'anteriorObservacion','crear',System.today().addDays(2));
		   Test.stopTest();
		}
  }

	static testmethod void test4()
   {
	   objUsuario=BI_COL_CreateData_tst.getCreateUSer();

		System.runAs(objUsuario)
		{
		   crearData();
		   objSegMien.BI_COL_Fecha_contactar_cliente__c=system.today().addDays(-1);
		   update objSegMien;
		   Test.startTest();
		   Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws() );
		   BI_COL_GestionBreaks_ctr.envioQuiebreSW(''+objSegMien.Id, 'anteriorObservacion','modificar',System.today().addDays(2));
		   Test.stopTest();
		}
  }
	static testmethod void test5()
   {
	   objUsuario=BI_COL_CreateData_tst.getCreateUSer();

		System.runAs(objUsuario)
		{
		   crearData();
		   objSegMien.BI_COL_Fecha_contactar_cliente__c=system.today().addDays(-1);
		   update objSegMien;
		   Test.startTest();
		   Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws() );
		   BI_COL_GestionBreaks_ctr.envioQuiebreSW(''+objSegMien.Id, 'anteriorObservacion',label.BI_COL_LbCreadoSFDC,System.today().addDays(2));
		   Test.stopTest();
		}
  }
}