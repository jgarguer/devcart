@isTest
private class NEA_B2W_GENERAR_DOCUMENTOS_TEST
{
		 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Pachas y Alejandro Pantoja  
    Company:       New Energy Aborda
    Description:   Test Class of NEA_B2W_GENERAR_DOCUMENTOS class
    History: 

    <Date>                  <Author>                <Change Description>
    19/05/2016              Alejandro Pantoja        Initial Version 
    27/09/2017              Jaime Regidor            Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static List<NE__OrderItem__c> listOrdersItems;
    static List<NE__Order__c> listOrders;
    static List<NE__Product__c> listProducts;
    static NE__Order__c orderAux;
    static NE__Product__c productAux;
    static NE__OrderItem__c oiAux;
    static Account cuenta;
    static NE__Catalog__c catalog;


    @isTest static void Generate_Order()
    {
    	catalog=new NE__Catalog__c();
    	insert catalog;

    	cuenta=new Account();
    	cuenta.Name = 'PruebaAccount';
        cuenta.BI_Segment__c = 'test'; //27/09/2017
        cuenta.BI_Subsegment_Regional__c = 'test'; //27/09/2017
        cuenta.BI_Territory__c = 'test'; //27/09/2017
    	insert cuenta;

		listOrders = new List<NE__Order__c>();
		listOrdersItems=new List<NE__OrderItem__c>();
		listProducts=new List<NE__Product__c>();
		
		/*Create Orders*/
    	for(Integer i=0;i<4;i++)
    	{
    		orderAux= new NE__Order__C();
    		orderAux.NE__CatalogId__c=catalog.Id;
    		orderAux.NE__AccountId__c=cuenta.Id;
    		listOrders.add(orderAux);
    	}
    	insert listOrders;

    	/*Create Comercial Products*/
    	for(Integer i=0;i<4;i++)
    	{
    		productAux=new NE__Product__c();
    		productAux.Family_Local__c='VDC';
    		productAux.Name='PRODUCTO '+i;
    		listProducts.add(productAux);
    		i++;
    		productAux=new NE__Product__c();
    		productAux.Family_Local__c='MOVIL';
    		productAux.Name='PRODUCTO '+i;
    		listProducts.add(productAux);
    	}

    		productAux=new NE__Product__c();
    		productAux.Family_Local__c='DIFERENTE';
    		productAux.Name='PRODUCTO '+4;
    		listProducts.add(productAux);

    	insert listProducts;

    	/*Create Order Items*/
    		oiAux =new NE__OrderItem__c();
    		oiAux.NE__ProdId__c=listProducts.get(0).Id;
    		oiAux.NE__OrderId__c=listOrders.get(0).Id;
    		oiAux.NE__Qty__c=5;    		
    		listOrdersItems.add(oiAux);

    		oiAux =new NE__OrderItem__c();
    		oiAux.NE__ProdId__c=listProducts.get(1).Id;
    		oiAux.NE__OrderId__c=listOrders.get(1).Id;
    		oiAux.NE__Qty__c=5;    		
    		listOrdersItems.add(oiAux);

    		oiAux =new NE__OrderItem__c();
    		oiAux.NE__ProdId__c=listProducts.get(2).Id;
    		oiAux.NE__OrderId__c=listOrders.get(2).Id;
    		oiAux.NE__Qty__c=5;    		
    		listOrdersItems.add(oiAux);

    		oiAux =new NE__OrderItem__c();
			oiAux.NE__ProdId__c=listProducts.get(3).Id;
    		oiAux.NE__OrderId__c=listOrders.get(2).Id;
    		oiAux.NE__Qty__c=5;
    		listOrdersItems.add(oiAux);

    		oiAux =new NE__OrderItem__c();
			oiAux.NE__ProdId__c=listProducts.get(4).Id;
    		oiAux.NE__OrderId__c=listOrders.get(3).Id;
    		oiAux.NE__Qty__c=5;    		
    		listOrdersItems.add(oiAux);

    		insert listOrdersItems;

    		NEA_B2W_GENERAR_DOCUMENTOS.CapturarCategorizacionProductos(listOrders.get(0).Id);
    		NEA_B2W_GENERAR_DOCUMENTOS.CapturarCategorizacionProductos(listOrders.get(1).Id);
    		NEA_B2W_GENERAR_DOCUMENTOS.CapturarCategorizacionProductos(listOrders.get(2).Id);
    		NEA_B2W_GENERAR_DOCUMENTOS.CapturarCategorizacionProductos(listOrders.get(3).Id);

    		Test.startTest();
    		Test.stopTest();
    } 


	
}