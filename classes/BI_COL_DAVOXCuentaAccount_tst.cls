/**
* Avanxo Colombia
* @author           Geraldine Sofía Pérez Montes href=<gperez@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class 
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-08-20      GSPM          			Test Class created
*            1.1	2017-01-02		Gawron, Julián E.		Add testSetup
*            1.2    23-02-2017      Jaime Regidor           Adding Campaign Values, Adding Catalog Country 
					13/03/2017		Marta Gonzalez(Everis)	REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
					20/09/2017      Angel F. Santaliestra   Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/

@isTest
Global class BI_COL_DAVOXCuentaAccount_tst {
	Public static User 													objUsuario = new User();
	public static Account 									objCuenta;
	public static Account 									objCuenta2;
	public static Contact 									objContacto;
	public static Campaign 									objCampana;
	public static Contract 									objContrato;
	public static BI_COL_Anexos__c 							objAnexos;
	public static Opportunity 								objOportunidad;
	public static BI_Col_Ciudades__c 						objCiudad;
	public static BI_Sede__c 								objSede;
	public static BI_Punto_de_instalacion__c 				objPuntosInsta;
	public static BI_COL_Descripcion_de_servicio__c 		objDesSer;
	public static BI_COL_Modificacion_de_Servicio__c 		objMS;
	public static BI_COL_Generate_OT_ctr.WrapperMS    		objWrapperMS;
	
	public static List <Profile> 							lstPerfil;
	public static List <User>	 							lstUsuarios;
	public static List <UserRole> 							lstRoles;
	public static List<Campaign> 							lstCampana;
	public static List<BI_COL_manage_cons__c > 				lstManege;
	public static List<BI_COL_Modificacion_de_Servicio__c>	lstMS;
	public static List<BI_COL_Descripcion_de_servicio__c>	lstDS;
	public static List<recordType>							lstRecTyp;
	public static List<BI_COL_Generate_OT_ctr.WrapperMS>    lstWrapperMS;
	public static List<BI_COL_Anexos__c>    				lstAnexos;

	@testSetup public static void CrearData() 
	{

		//perfiles
		//lstPerfil             			= new List <Profile>();
		lstPerfil               		= [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
		system.debug('datos Perfil '+lstPerfil);
				
		//usuarios
		//lstUsuarios = [Select Id FROM User where Pais__c = 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

		//Roles
			//lstRoles                = new List <UserRole>();
			lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
			system.debug('datos Rol '+lstRoles);
			
		//ObjUsuario
        objUsuario = BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);
        System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
			insert objUsuario;
		}
		System.runAs(objUsuario)
		{	
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass;	
			
			String nRandom = String.valueOf((Integer)(Math.random()*100));
			//Cuentas
			objCuenta 										= new Account();      
			objCuenta.Name                                  = 'prueba' + BI_DataLoad.generateRandomString(4);
			objCuenta.BI_Country__c                         = 'Argentina';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Fraude__c                          = false;
			objCuenta.AccountNumber							= '123456' + nRandom;
			objCuenta.BI_No_Identificador_fiscal__c			= '1234566' + nRandom;
			objCuenta.Phone 								= '1233421';
			objCuenta.BI_Segment__c                         = 'test';
			objCuenta.BI_Subsegment_Regional__c             = 'test';
			objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			system.debug('datos Cuenta '+objCuenta);

			//Ciudad
			objCiudad = new BI_Col_Ciudades__c ();
			objCiudad.Name 						= 'Test City';
			objCiudad.BI_COL_Pais__c 			= 'Test Country';
			objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
			insert objCiudad;
			System.debug('Datos Ciudad ===> '+objSede);
			
			//Contactos
			objContacto                                     = new Contact();
			objContacto.LastName                        	= 'Test';
			objContacto.BI_Country__c             		    = 'Argentina';
			objContacto.CurrencyIsoCode           		    = 'COP'; 
			objContacto.AccountId                 		    = objCuenta.Id;
			objContacto.BI_Tipo_de_contacto__c				= 'Administrador Canal Online';
			objContacto.Phone 								= '1234567';
			objContacto.MobilePhone							= '3104785925';
			objContacto.Email								= 'pruebas@pruebas1.com';
			objContacto.BI_COL_Direccion_oficina__c			= 'Dirprueba';
			objContacto.BI_COL_Ciudad_Depto_contacto__c		= objCiudad.Id;
            //REING-INI
            //objContacto.BI_Tipo_de_contacto__c          = 'Autorizado';
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
       		objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
            objContacto.BI_Tipo_de_contacto__c          = 'General';
            objContacto.FS_CORE_Referencias_Funcionales__c          = 'Administrativo';
       		//REING_FIN
			
			Insert objContacto;
			
			////catalogo
			//NE__Catalog__c objCatalogo 						= new NE__Catalog__c();
			//objCatalogo.Name 								= 'prueba Catalogo';
			//objCatalogo.BI_country__c                       = 'Argentina';
			//insert objCatalogo; 
			
			////Campaña
			//objCampana 										= new Campaign();
			//objCampana.Name 								= 'Campaña Prueba';
			//objCampana.BI_Catalogo__c 						= objCatalogo.Id;
			//objCampana.BI_COL_Codigo_campana__c 			= 'prueba1';
			//insert objCampana;
			//lstCampana 										= new List<Campaign>();
			//lstCampana 										= [select Id, Name from Campaign where Id =: objCampana.Id];
			//system.debug('datos Cuenta '+lstCampana);
			
			//Tipo de registro
			lstRecTyp 										= [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
			system.debug('datos de FUN '+lstRecTyp);
			
			//Contrato
			objContrato 									= new Contract ();
			objContrato.AccountId 							= objCuenta.Id;
			objContrato.StartDate 							=  System.today().addDays(+5);
			objContrato.BI_COL_Formato_Tipo__c 				= 'Condiciones Generales';
			objContrato.BI_COL_Presupuesto_contrato__c 		= 1300000;
			objContrato.StartDate							= System.today().addDays(+5);
			objContrato.BI_Indefinido__c 					= 'No';
			objContrato.BI_COL_Monto_ejecutado__c			= 5;
			objContrato.ContractTerm						= 12;
			objContrato.BI_COL_Duracion_Indefinida__c		= true;
			objContrato.Name 								= 'prueba';
			objContrato.BI_COL_Cuantia_Indeterminada__c		= true;
		
			insert objContrato;
			Contract objCont = [select ContractNumber, BI_COL_Saldo_contrato__c from Contract limit 1];

			System.debug('datos Contratos ===>'+objContrato);
			
			//FUN
			objAnexos               						= new BI_COL_Anexos__c();
			objAnexos.Name          						= 'FUN-0041414';
			objAnexos.RecordTypeId  						= lstRecTyp[0].Id;
			objAnexos.BI_COL_Contrato__c   					= objContrato.Id;
			objAnexos.BI_COL_Estado__c 						= 'Activo';

			insert objAnexos;
			System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
			
			//Configuracion Personalizada
			BI_COL_manage_cons__c objConPer 				= new BI_COL_manage_cons__c();
			objConPer.Name                  				= 'Viabilidades';
			objConPer.BI_COL_Numero_Viabilidad__c 			= 46;
			objConPer.BI_COL_ConsProyecto__c     			= 1;
			objConPer.BI_COL_ValorConsecutivo__c  			=1;
			lstManege                      					= new List<BI_COL_manage_cons__c >();        
			lstManege.add(objConPer);
			insert lstManege;
			System.debug('======= configuracion personalizada ======= '+lstManege);
			
			//Oportunidad
			objOportunidad                                      	= new Opportunity();
			objOportunidad.Name                                   	= 'prueba opp';
			objOportunidad.AccountId                              	= objCuenta.Id;
			objOportunidad.BI_Country__c                          	= 'Argentina';
			objOportunidad.CloseDate                              	= System.today().addDays(+5);
			objOportunidad.StageName                              	= 'F5 - Solution Definition';
			objOportunidad.CurrencyIsoCode                        	= 'COP';
			objOportunidad.Certa_SCP__contract_duration_months__c 	= 12;
			objOportunidad.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
			objOportunidad.OwnerId                                	= objUsuario.id;
			insert objOportunidad;
			system.debug('Datos Oportunidad'+objOportunidad);
			List<Opportunity> lstOportunidad 	= new List<Opportunity>();
			lstOportunidad 						= [select Id from Opportunity where Id =: objOportunidad.Id];
			system.debug('datos ListaOportunida '+lstOportunidad);
			
			
			//Direccion
			objSede 								= new BI_Sede__c();
			objSede.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
			objSede.BI_Direccion__c 				= 'Test Street 123 Number 321';
			objSede.BI_Localidad__c 				= 'Test Local';
			objSede.BI_COL_Estado_callejero__c 		= System.Label.BI_COL_LbValor3EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c 		= 'Libre';
			objSede.BI_Country__c 					= 'Colombia';
			objSede.Name 							= 'Test Street 123 Number 321, Test Local Colombia';
			objSede.BI_Codigo_postal__c 			= '12356';
			insert objSede;
			System.debug('Datos Sedes ===> '+objSede);
			
			//Sede
			objPuntosInsta 						= new BI_Punto_de_instalacion__c ();
			objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
			objPuntosInsta.BI_Sede__c          = objSede.Id;
			objPuntosInsta.BI_Contacto__c      = objContacto.id;
			objPuntosInsta.Name 				='Prueba sucursal';
			insert objPuntosInsta;
			System.debug('Datos Sucursales ===> '+objPuntosInsta);
			
			//DS
			objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
			objDesSer.CurrencyIsoCode               			= 'COP';
			insert objDesSer;
			System.debug('Data DS ====> '+ objDesSer);
			System.debug('Data DS ====> '+ objDesSer.Name);
			lstDS 	= [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
			System.debug('Data lista DS ====> '+ lstDS);
			
			//MS
			objMS                         				= new BI_COL_Modificacion_de_Servicio__c();
			objMS.BI_COL_FUN__c           				= objAnexos.Id;
			objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
			objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c 					= false;
			objMS.BI_COL_Estado__c 						= 'Activa';//label.BI_COL_lblActiva;
			objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
			objMs.BI_COL_Medio_Preferido__c 			= 'Cobre';
			objMS.BI_COL_Clasificacion_Servicio__c		= 'Alta demo';
			objMS.BI_COL_TRM__c 						= 5;
			objMS.BI_COL_Oportunidad__c					= objOportunidad.id;
			insert objMS;
			system.debug('Data objMs ===>'+objMs);
			BI_COL_Modificacion_de_Servicio__c objr = [select Id, BI_COL_Monto_ejecutado__c from BI_COL_Modificacion_de_Servicio__c where id =: objMS.id];
			system.debug('\n@-->Data consulta ===>'+objr);	
		
		}
	}
		
	@isTest static void cuentaCesionContr() 
	{
		
		CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);
		list<string> lstStrAcc = new list<string>();
		lstStrAcc.add('abcdefg');
		system.debug('\n@-->lstStrAcc'+lstStrAcc);

		Test.startTest();
		pageRef.getParameters().put('id',objCuenta.Id);
		pageRef.getParameters().put('idMS',objMS.Id);
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();
		Test.setMock(WebServiceMock.class, new MockResponseGenerator1());
		objcontroller.cuentaCesionContr();
		objcontroller.cuentas = objCuenta.Id;
		objcontroller.fnCargarDatos();
		objcontroller.lstCuentas = lstStrAcc;
		objcontroller.armarOpciones();
		objcontroller.CuentaDx = objCuenta.Id;
		objcontroller.guardar();		
		Test.stopTest();
	}
	@isTest static void armarOpcionesMayor2() 
	{
		
		CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);
		list<string> lstStrAcc = new list<string>();
		lstStrAcc.add('abcdefg');
		system.debug('\n@-->lstStrAcc'+lstStrAcc);

		Test.startTest();
		pageRef.getParameters().put('id',objCuenta.Id);
		pageRef.getParameters().put('idMS',objMS.Id);
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();
		Test.setMock(WebServiceMock.class, new MockResponseGenerator3());
		objcontroller.cuentaCesionContr();
		objcontroller.cuentas = objCuenta.Id;
		objcontroller.fnCargarDatos();
		objcontroller.lstCuentas = lstStrAcc;
		objcontroller.armarOpciones();
		objcontroller.guardar();		
		Test.stopTest();
	}

	@isTest static void armarOpcionesNoMayor1() 
	{
		
		CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);
		list<string> lstStrAcc = new list<string>();
		lstStrAcc.add('abcdefg');
		system.debug('\n@-->lstStrAcc'+lstStrAcc);

		Test.startTest();
		pageRef.getParameters().put('id',objCuenta.Id);
		pageRef.getParameters().put('idMS',objMS.Id);
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();
		Test.setMock(WebServiceMock.class, new MockResponseGenerator4());
		objcontroller.cuentaCesionContr();
		objcontroller.cuentas = objCuenta.Id;
		objcontroller.fnCargarDatos();
		objcontroller.lstCuentas = lstStrAcc;
		objcontroller.armarOpciones();
		objcontroller.guardar();		
		Test.stopTest();
	}


	@isTest static void getPageNumber() 
	{
		
		//CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();	
		objcontroller.getPageNumber();		
		Test.stopTest();
	}

	@isTest static void getPageSize() 
	{
		
		//CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();	
		objcontroller.getPageSize();		
		Test.stopTest();
	}

	@isTest static void getPreviousButtonEnabled() 
	{
		
		//CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();	
		objcontroller.getPreviousButtonEnabled();		
		Test.stopTest();
	}

	@isTest static void getNextButtonDisabled() 
	{
		
		//CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();	
		objcontroller.getNextButtonDisabled();		
		Test.stopTest();
	}

	@isTest static void getTotalPageNumber() 
	{
		
		//CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();	
		objcontroller.getTotalPageNumber();		
		Test.stopTest();
	}

	@isTest static void previousBtnClick() 
	{
		
		//CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();	
		objcontroller.previousBtnClick();		
		Test.stopTest();
	}

	@isTest static void nextBtnClick() 
	{
		
		//CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();	
		objcontroller.nextBtnClick();		
		Test.stopTest();
	}

	@isTest static void ViewData() 
	{
		
		//CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();	
		objcontroller.ViewData();		
		Test.stopTest();
	}

	@isTest static void getCuentaDx() 
	{
		
		//CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();	
		objcontroller.getCuentaDx();		
		Test.stopTest();
	}

	@isTest static void setCuentaDx() 
	{
		
		//CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();	
		objcontroller.setCuentaDx('Prueba');		
		Test.stopTest();
	}

	@isTest static void BindData() 
	{
		
		//CrearData();
		PageReference pageRef           = Page.BI_COL_DAVOXCuentaAccount_pag;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		BI_COL_DAVOXCuentaAccount_ctr objcontroller = new BI_COL_DAVOXCuentaAccount_ctr();	
		objcontroller.BindData(5);		
		Test.stopTest();
	}
	global class MockResponseGenerator3 implements WebServiceMock {
    	
	    global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) 
	    {    
	    	String strXmlResp = '';

	    	strXmlResp = '<WS>';
		    	strXmlResp += '<USER>USER</USER>';
		    	strXmlResp += '<PASSWORD>PASSWORD</PASSWORD>';
		    	strXmlResp += '<DATEPROCESS>2015-08-24</DATEPROCESS>';
		    	strXmlResp += '<PACKCODE>ASD654AS</PACKCODE>';
		    	strXmlResp += '<'+Label.BI_COL_LbSeparator+'>Õ</'+Label.BI_COL_LbSeparator+'>';
		    	strXmlResp += '<TYPE>CONSULTA_CUENTA</TYPE>';
		    	strXmlResp += '<COUNT>1</COUNT>';
		    	strXmlResp += '<'+label.BI_COL_LbValues+'>';	 
		    		strXmlResp += '<'+label.BI_COL_LbValue+'>value_testÕvalue_test2Õvalue_test3</'+label.BI_COL_LbValue+'>';
		 		    	strXmlResp +=  '</'+label.BI_COL_LbValues+'>';  	
	    	strXmlResp += '</WS>';
	    	system.debug('@-->strXmlResp'+strXmlResp);  
	    	
	    	BI_COL_Davox_wsdl.findResponse_element objResp = new BI_COL_Davox_wsdl.findResponse_element();
	    	objResp.findReturn = strXmlResp;

			response.put( 'response_x', objResp);
		}
	}

	global class MockResponseGenerator4 implements WebServiceMock {
    	
	    global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) 
	    {    
	    	String strXmlResp = '';

	    	strXmlResp = '<WS>';
		    	strXmlResp += '<USER>USER</USER>';
		    	strXmlResp += '<PASSWORD>PASSWORD</PASSWORD>';
		    	strXmlResp += '<DATEPROCESS>2015-08-24</DATEPROCESS>';
		    	strXmlResp += '<PACKCODE>ASD654AS</PACKCODE>';
		    	strXmlResp += '<'+Label.BI_COL_LbSeparator+'>Õ</'+Label.BI_COL_LbSeparator+'>';
		    	strXmlResp += '<TYPE>CONSULTA_CUENTA</TYPE>';
		    	strXmlResp += '<COUNT>1</COUNT>';
		    	strXmlResp += '<'+label.BI_COL_LbValues+'>';	 
		    		strXmlResp += '<'+label.BI_COL_LbValue+'>value_test</'+label.BI_COL_LbValue+'>';
		 		    	strXmlResp +=  '</'+label.BI_COL_LbValues+'>';  	
	    	strXmlResp += '</WS>';
	    	system.debug('@-->strXmlResp'+strXmlResp);  
	    	
	    	BI_COL_Davox_wsdl.findResponse_element objResp = new BI_COL_Davox_wsdl.findResponse_element();
	    	objResp.findReturn = strXmlResp;

			response.put( 'response_x', objResp);
		}
	}


	global class MockResponseGenerator1 implements WebServiceMock {
    	
	    global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) 
	    {    
	    	String strXmlResp = '';

	    	strXmlResp = '<WS>';
		    	strXmlResp += '<USER>USER</USER>';
		    	strXmlResp += '<PASSWORD>PASSWORD</PASSWORD>';
		    	strXmlResp += '<DATEPROCESS>2015-08-24</DATEPROCESS>';
		    	strXmlResp += '<PACKCODE>ASD654AS</PACKCODE>';
		    	strXmlResp += '<'+Label.BI_COL_LbSeparator+'>Õ</'+Label.BI_COL_LbSeparator+'>';
		    	strXmlResp += '<TYPE>CONSULTA_CUENTA</TYPE>';
		    	strXmlResp += '<COUNT>1</COUNT>';
		    	strXmlResp += '<'+label.BI_COL_LbValues+'>';	 
		    		strXmlResp += '<'+label.BI_COL_LbValue+'>value_testÕvalue_test2Õvalue_test3Õvalue_test4ÕSCÕVEÕvalue_test7Õvalue_test8Õvalue_test9Õvalue_test10Õvalue_test11Õvalue_test12 </'+label.BI_COL_LbValue+'>';
		 		    	strXmlResp +=  '</'+label.BI_COL_LbValues+'>';  	
	    	strXmlResp += '</WS>';
	    	system.debug('@-->strXmlResp'+strXmlResp);  
	    	
	    	BI_COL_Davox_wsdl.findResponse_element objResp = new BI_COL_Davox_wsdl.findResponse_element();
	    	objResp.findReturn = strXmlResp;

			response.put( 'response_x', objResp);
		}
	}

	global class MockResponseGenerator2 implements WebServiceMock {
    	
	    global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) 
	    {       
	    	BI_COL_Davox_wsdl.findResponse_element objResp = new BI_COL_Davox_wsdl.findResponse_element();
	    	objResp.findReturn = '<'+Label.BI_COL_LbSeparator+'><'+label.BI_COL_LbValues+'>test</'+label.BI_COL_LbValues+'></FAIL>';

			response.put( 'response_x', objResp);
		}
	}
	
}