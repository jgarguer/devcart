public class BI_Milestone1TaskMethods {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by Account Triggers 
    Test Class:    Milestone1TaskMethods
    
    History:
     
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void createTaskProject(List <Milestone1_Task__c> news){
    	try{	    	
	    	if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
	    	
	    	Set <Id> set_mtasks = new Set <Id>();
	    	
	    	List <Task> lst_tsk = new List <Task>();
	    	
	    	for (Milestone1_Task__c mtask:news){
	    		Task StTsk = new Task (WhatId = mtask.Id,
										Subject = mtask.Name,
										OwnerId = mtask.Assigned_To__c,
										Priority = Label.BI_Priority_Normal,
										ActivityDate = mtask.Due_Date__c);
											
								
				if (mtask.Complete__c == true){
					StTsk.Status= Label.BI_TaskStatus_Completed;
					//mtask.Task_Stage__c = 'Resolved';
				}else if(mtask.Task_Stage__c == 'None'){
					StTsk.Status= Label.BI_TaskStatus_NotStarted;
				}else if(mtask.Task_Stage__c == 'In Progress' || mtask.Task_Stage__c == 'Close'){
					StTsk.Status=Label.BI_TaskStatus_InProgress;
				}
				lst_tsk.add(StTsk);		
	    	}
	    	system.debug('%&%&%&'+lst_tsk);
	    	insert lst_tsk;
	    	system.debug('%&%&%&2'+lst_tsk);
    	
    	}catch (exception Exc){
		  	BI_LogHelper.generate_BILog('BI_Milestone1TaskMethods.createTaskProject', 'BI_EN', Exc, 'Trigger');
		}
    /*	for (Milestone1_Task__c mtask1:news){
    		for (Task tsk1:lst_tsk){
    			if(tsk1.WhatId==mtask1.Id)
    			mtask1.BI_Tarea_asignada__c = '/' + string.valueof(tsk1.Id);
    		}
    	}
    	update news?;*/
    }
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that assigns PermissionSet when inserting users with profile BI_Partner Communities, when updating BI_Permisos__c field or updating Country. 
    			   If the user has already assigned a permissionset it's deleted
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 	 
    
    <Date>                  <Author>                <Change Description>
   29/09/2014             Ignacio Llorca		        Initial Version
   12/01/2017			Alberto Fernández				Añadida funcionalidad para que la Tarea se actualice con los datos de la Tarea de Proyecto asociada
   18/01/2017			Alvaro García					Añadido que el campo Task_Stage__c se actualice a 'Closed'
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateTaskProjectStatus(List <Milestone1_Task__c> news, List <Milestone1_Task__c> olds){
	    try{
	    	NETriggerHelper.setTriggerFired('BI_Milestone1TaskMethods.updateProjectTask');
				
	    	Set <Id> set_mtasks = new Set <Id>();    
	    	for (Milestone1_Task__c mstoneTsk:news){
	    		set_mtasks.add(mstoneTsk.Id);
	    	}	
	    	system.debug('%%%%%%'+set_mtasks);
	    	List <Task> lst_StTask = [SELECT Id, WhatId, Status, OwnerId, ActivityDate FROM Task WHERE WhatId IN :set_mtasks]; 
	    	system.debug('%%%%%%'+lst_StTask);
	    	Integer i=0;
	    	List <Task> lst_task = new List <Task>();
	    	for (Milestone1_Task__c mtask:news){

		    	if (mtask.Complete__c == true){
		    		mtask.Task_Stage__c = 'Closed';
		    		
	    			for(Task StTask:lst_StTask){
	    				
		    			if(StTask.WhatId == mtask.Id){
		    				StTask.Status = Label.BI_TaskStatus_Completed;
		    				StTask.OwnerId = mtask.Assigned_To__c;
		    				StTask.ActivityDate = mtask.Due_Date__c;			    				
		    			}
		    			lst_task.add(StTask);
		    		}	    		
	    		}

	    		else if(mtask.Complete__c == false) {
	    			for(Task StTask:lst_StTask){
	    				
		    			if(StTask.WhatId == mtask.Id){
		    				StTask.Status = mtask.Task_Stage__c;
		    				StTask.OwnerId = mtask.Assigned_To__c;
		    				StTask.ActivityDate = mtask.Due_Date__c;
		    				StTask.BI_Fecha_de_Cierre__c = null;			    				
		    			}
		    			lst_task.add(StTask);
		    		}
	    		}

	    		i++;
	    	}
	    	system.debug('%%%%%%'+lst_task);
	    	update lst_task;
    	}catch (exception Exc){
		  	BI_LogHelper.generate_BILog('BI_Milestone1TaskMethods.createTaskProject', 'BI_EN', Exc, 'Trigger');
		}
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that assigns PermissionSet when inserting users with profile BI_Partner Communities, when updating BI_Permisos__c field or updating Country. 
    			   If the user has already assigned a permissionset it's deleted
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 	 
    
    <Date>                  <Author>                <Change Description>
   29/09/2014             Ignacio Llorca		        Initial Version
   
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   /* public static void updateAssignedTask(List <Milestone1_Task__c> news){
    	Set <Id> set_mtasks = new Set <Id>();    
    	for (Milestone1_Task__c mstoneTsk:news){
    		set_mtasks.add(mstoneTsk.Id);
    	}	
    	
    	List <Task> lst_tsk = [SELECT Id, WhatId, Status FROM Task WHERE WhatId IN :set_mtasks]; 
    	Integer i=0;
    	
    	for (Milestone1_Task__c mtask:news){
    		for (Task tsk:lst_tsk){
    			if(tsk.WhatId==mtask.Id)
    			mtask.BI_Tarea_asignada__c = '/' + string.valueof(tsk.Id);
    		}
    	}
    }*/
}