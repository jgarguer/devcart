public with sharing class BI_Punto_de_InstalacionMethods {


   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer
    Company:       NEAborda

    History

    <Date>                  <Author>                    <Description>
    ??/??/??                Fco.Javier Sanz             Initial version
    01/08/2016              Humberto Nunes              Se limito a 20 Caracteres el Rellenado del Codigo Postal en la direccion del Cliente.
    31/08/2016              Alfonso Alvarez(AJAE)       Se corrige error en código de limite a 20 Caracteres el Rellenado del Codigo Postal en la direccion del Cliente. 
    05/09/2016              Alfonso Alvarez(AJAE)       Se corrige error en TEST.

    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateUsershippingAddressFromSede(list<BI_Punto_de_instalacion__c> news, list<BI_Punto_de_instalacion__c>olds ) {
        
        map<id,id> map_accoutId_direccionId = new map<id,id> ();
        set<id> set_updatedSedes = new set<id>();
        
        for (integer i = 0; i <  news.size(); i++) {
            
            if ((olds == null || (olds!= null && olds[i].BI_Tipo_de_sede__c != 'Comercial Principal'))
                         && news[i].BI_Tipo_de_sede__c == 'Comercial Principal') {
            //UPDATE RELATTED USERSHIPPING ADDRESS
                set_updatedSedes.add(news[i].id);
                map_accoutId_direccionId.put(news[i].BI_Cliente__c,news[i].BI_Sede__c);
                    
            }
                
        }
        
        if (!map_accoutId_direccionId.isEmpty()) {
            
            //[selcet id from BI_Punto_de_instalacion__c where id in:set_updatedSedes]
            
            map<id,Account> map_id_accountToUpdate = new map<id,Account>([select id,
                                                             ShippingCity,ShippingCountry,
                                                             ShippingLatitude,ShippingLongitude,
                                                             ShippingPostalCode,
                                                             ShippingState,ShippingStreet
                                                                from Account
                                                                where id IN :map_accoutId_direccionId.keyset()]);
            if(!map_id_accountToUpdate.isEmpty()) {
                
                map<id,BI_Sede__c> map_id_direccionToCopy = new map<id,BI_Sede__c>(
                                                [select id, BI_Localidad__c,BI_Country__c,BI_Longitud__Latitude__s,
                                                BI_Longitud__Longitude__s,BI_Distrito__c,BI_Provincia__c,BI_Codigo_postal__c,
                                                toLabel(BI_Tipo_Via__c),
                                                BI_Direccion__c,BI_Numero__c,BI_Piso__c,BI_Apartamento__c 
                                                from BI_Sede__c
                                                where id IN :map_accoutId_direccionId.values()]);
                                                
                if(!map_id_direccionToCopy.isEmpty()) {
                    
                    list<Account> list_accountToUpdate = new list<Account>();
                    
                    for (id accountToupdateId : map_accoutId_direccionId.keyset()) {
                        
                        if(map_id_accountToUpdate.containsKey(accountToupdateId) && 
                            
                            map_id_direccionToCopy.containskey(map_accoutId_direccionId.get(accountToupdateId)) ) {
                            
                            Account accountToUpdate = map_id_accountToUpdate.get(accountToupdateId);    
                            BI_Sede__c directionAccount = map_id_direccionToCopy.get(map_accoutId_direccionId.get(accountToupdateId)); 
                            
                            String provincia,codigoP;
                            String direccion,numero,piso,apartamento,via;
                            
                            accountToUpdate.ShippingCity = directionAccount.BI_Localidad__c;
                            accountToUpdate.ShippingCountry = directionAccount.BI_Country__c;
                            accountToUpdate.ShippingLatitude = (directionAccount.BI_Longitud__Latitude__s == null) ? 0 : directionAccount.BI_Longitud__Latitude__s;
                            accountToUpdate.ShippingLongitude = (directionAccount.BI_Longitud__Longitude__s == null) ? 0 : directionAccount.BI_Longitud__Longitude__s; 
                            //AJAE 31/08/2016: Se cambia directionAccount.BI_Distrito__c.substring(0); por directionAccount.BI_Distrito__c.substring(0,19);
                            //AJAE 05/09/2016: Se corrige error en TEST
                               accountToUpdate.ShippingPostalCode = (directionAccount.BI_Distrito__c == null) ? '' : ( (directionAccount.BI_Distrito__c.length() <20 ) ? directionAccount.BI_Distrito__c : directionAccount.BI_Distrito__c.substring(0,19));   
                            provincia = (directionAccount.BI_Provincia__c == null) ? '' : directionAccount.BI_Provincia__c;
                            codigoP = (directionAccount.BI_Codigo_postal__c == null) ? '' : directionAccount.BI_Codigo_postal__c;
                            accountToUpdate.ShippingState = provincia +' '+ codigoP;
        
                            via = (directionAccount.BI_Provincia__c == null) ? '' : directionAccount.BI_Provincia__c;
                            direccion = (directionAccount.BI_Direccion__c == null) ? '' : directionAccount.BI_Direccion__c;
                            numero = (directionAccount.BI_Numero__c == null) ? '' : directionAccount.BI_Numero__c;
                            piso = (directionAccount.BI_Piso__c == null) ? '' : directionAccount.BI_Piso__c;
                            apartamento = (directionAccount.BI_Apartamento__c == null) ? '' : directionAccount.BI_Apartamento__c;
                            accountToUpdate.ShippingStreet = via + ' ' +
                                                             direccion + ' ' +
                                                             numero  + ' ' +
                                                             piso  + ' ' +
                                                             apartamento  + ' ';
                                                            
                            list_accountToUpdate.add(accountToUpdate);
                            }
                        
                        
                    } 
                    if (!list_accountToUpdate.isEmpty()) {
                        
                        update list_accountToUpdate;
                        
                    }
                    
                }       
                
            }
            
                        
                        
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Pardo
    Company:       NEAborda

    History

    <Date>                  <Author>                    <Description>
    05/09/2016              Antonio Pardo               Initial version - Prevent Sede delete if associated MS
    11/05/2018              Mariano García              Remove subquery on deleted relationship
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void prevenirBorradoSede(Map<Id, BI_Punto_de_instalacion__c> olds){

        Set<Id> set_toProcess = new Set<Id>();
        //Filtro solo para sedes de colombia
        for(BI_Punto_de_instalacion__c sede : olds.values()){
            if(sede.Pais__c == 'Colombia'){
                set_toProcess.add(sede.Id);
            }
        }
        if(!set_toProcess.isEmpty()){
            for(BI_Punto_de_instalacion__c sede : [SELECT Id, (SELECT Id FROM Modificacion_de_Servicio1__r), (SELECT Id FROM Modificaci_n_de_Servicio__r) FROM BI_Punto_de_instalacion__c WHERE Id IN : set_toProcess]){

                if(sede.BI_COL_Sede_Destino_Sede__r.isEmpty() || sede.Modificacion_de_Servicio1__r.isEmpty() || sede.Modificaci_n_de_Servicio__r.isEmpty()){
                    olds.get(sede.Id).addError(Label.BI_COL_Error_Eliminar_Sede);
                }
            }       
        }
    }
}