/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Helper class for before trigger of the object Catalog Item Configuration
    Test Class:    CWP_CatItemConfig_Util_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    23/02/2017              Everis                    Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class CWP_CatItemConfig_Util {
    
    /*
    Developer Everis
    Function: method used to receive the list of new elements, clasify them by their recordType and send the output lists to the appropiate methods
    */
    public static void receiveList(list<CWP_CatalogItemConfiguration__c> rcvList, boolean isInsert){
        map<Id, RecordType> rtMap= new map<id, RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE sObjectType =: 'CWP_CatalogItemConfiguration__c']);
        map<string, list<CWP_CatalogItemConfiguration__c>> configMap = new map<string, list<CWP_CatalogItemConfiguration__c>>();
        
        for(RecordType i : rtMap.values()){
            configMap.put(i.DeveloperName, new list<CWP_CatalogItemConfiguration__c>());
        }
        
        for(CWP_CatalogItemConfiguration__c i : rcvList){
            system.debug('item para añadir al mapa  ' +i);
            configMap.get(rtMap.get(i.RecordTypeId).DeveloperName).add(i);
        }
        system.debug('configMap creado  ' + configMap);
        processCustomQuery(configMap.get('CWP_CustomQuery'), isInsert);
                
        list<CWP_CatalogItemConfiguration__c> orderAndConfigList = new list<CWP_CatalogItemConfiguration__c>();
        orderAndConfigList.addAll(configMap.get('CWP_OrderField'));
        if(isInsert){
            validateUnicity(orderAndConfigList);
        }
        
    }
    
    /*
    Developer Everis
    Function: method used validate the custom query which has been created
    */
    private static void processCustomQuery(list<CWP_CatalogItemConfiguration__c> rcvList, boolean isInsert){
        string main = '\'abc%\'';
        string mainQuery = 'SELECT Name FROM NE__Order_Item_Attribute__c WHERE Name LIKE ';
        for(CWP_CatalogItemConfiguration__c i : rcvList){
            if(i.CWP_Prequery_Type__c=='Attribute_Prequery'){
                if(isInsert){
                    i.CWP_Query__c = ' AND ' + i.CWP_Query__c;
                }
                string queryExtended = mainQuery + main + i.CWP_Query__c;
                try{
                    list<NE__Order_Item_Attribute__c> tryList = new list<NE__Order_Item_Attribute__c>();
                    tryList = dataBase.Query(queryExtended);
                }catch(exception e){
                    i.addError(e.getMessage());
                }
            }
            else{
                mainQuery='SELECT Name FROM NE__Order_Item_Attribute__c WHERE Name LIKE';
            }
        }
    }
    
    /*
    Developer Everis
    Function: method used to check that for each catalog item there is one and only one record of Catalog Item Configuration with the record type OrderField
    */
    private static void validateUnicity(list<CWP_CatalogItemConfiguration__c> rcvList){
        set<id> ciAlreadyConfigured = new set<id>();
        map<id, integer> timesCICAdded = new map<id, integer>();
        map<id, integer> timesCIOAdded = new map<id, integer>();
        for(CWP_CatalogItemConfiguration__c i : rcvList){
            system.debug('item para comprobar   ' + i);
            if(i.CWP_CatalogItemOrderField__c!=null){
                timesCIOAdded = getCountTimesMap(timesCIOAdded, i.CWP_CatalogItemOrderField__c);
                ciAlreadyConfigured.add(i.CWP_CatalogItemOrderField__c);
            }
        }
        system.debug('mapa de ocurrencias de cics antes de sacar los de base de datos   ' + timesCICAdded);
        system.debug('mapa de ocurrencias de cios antes de sacar los de base de datos   ' + timesCIOAdded);
        list<CWP_CatalogItemConfiguration__c> cicList = new list<CWP_CatalogItemConfiguration__c>([SELECT Id, RecordTypeId, CWP_CatalogItemOrderField__c FROM CWP_CatalogItemConfiguration__c WHERE CWP_CatalogItemOrderField__c IN: ciAlreadyConfigured AND  RecordType.DeveloperName =: 'CWP_OrderField']);
        for(CWP_CatalogItemConfiguration__c i : cicList){
            if(i.CWP_CatalogItemOrderField__c!=null){
                timesCIOAdded = getCountTimesMap(timesCIOAdded, i.CWP_CatalogItemOrderField__c);
            }
        }
        system.debug('mapa de ocurrencias de cics despues de sacar los de base de datos ' + timesCICAdded);
        system.debug('mapa de ocurrencias de cios despues de sacar los de base de datos ' + timesCIOAdded);
        for(CWP_CatalogItemConfiguration__c i : rcvList){
            if(i.CWP_CatalogItemOrderField__c != null && timesCIOAdded.get(i.CWP_CatalogItemOrderField__c)>1){
                system.debug('error en cio   ' + timesCIOAdded.get(i.CWP_CatalogItemOrderField__c));
                i.addError('This catalog item already has a Order Field list record, please, edit this record instead of creating a new one');
            }
        }
    }
    
    /*
    Developer Everis
    Function: method used to count the times that an id appears
    */
    private static map<id, integer> getCountTimesMap(map<id, integer> rcvMap, id idToCount){
        if(rcvMap.containskey(idToCount)){
            integer theInt = rcvMap.get(idToCount) +1;
            rcvMap.put(idToCount, theInt);
        }else{
            rcvMap.put(idToCount, 1);
        }
        return rcvMap;
    }
    
}