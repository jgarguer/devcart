public class BI_GlobalVariables {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Global variables for triggers control.
    History:
    
    <Date>			  <Author> 	        <Description>
    14/04/2014        Pablo Oliva     	Initial version
    26/02/2015        Pablo Lozón     	Add stopTriggerIMPExcel
    23/03/2015        Álvaro Hernando   New variable to control Case triggers
    2970872015        Francisco Ayllon  New variable to control queueable methods on campaign
	17/10/2017		  Oscar Bartolo		New variable to execute the caseAssignment (CaseMethods) method two times
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static Boolean stopPreventUpdate = false;
	public static Boolean stopTriggerEventGuest = false;
	public static Boolean stopTriggerEvent = false;
	
    public static Boolean stopTriggereHelpCase = false; // variable to execute the caseAssignment (CaseMethods) method just once
	public static Boolean stopWorkflow = false; // variable to execute the caseAssignment (CaseMethods) method two times
    
	public static Boolean stopTriggerISC = false;
	public static final Set<String> Permisos;
	public static Boolean stopTriggerIMPExcel = false;
	public static Boolean stopTriggerAccHierarchy = false;
	public static Boolean stopTriggerDesc = false;

    public static Boolean isCloned = false; //Used in BI_Clone class when a Oppty is cloned.
    public static Boolean stopUpdateOrder = false; //Used in BI_NEOrderItemMethods class.
    public static Boolean stopUpdateOpp = false; //Used in BI_OrderMethods class.

    public static Integer count_Order_Updates = 0; //Used in TGS_Order_Trigger trigger.
    public static Integer count_Oppty_Updates = 0; //Used in Opportunity trigger.

    static{
        
        Permisos = new Set<String>();
        
        Set<Id> customPermIds = new Set<Id>();
        
        for(SetupEntityAccess sea : [SELECT SetupEntityId FROM SetupEntityAccess WHERE SetupEntityType = 'CustomPermission' AND ParentId IN (SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId = :UserInfo.getUserId())])
            customPermIds.add(sea.SetupEntityId);

        if(!customPermIds.isEmpty()){
            for(CustomPermission customPerms : [SELECT Id, DeveloperName, MasterLabel FROM CustomPermission WHERE Id IN :customPermIds])
                Permisos.add(customPerms.DeveloperName);
        }
            
    }
    
}