@isTest
private class PCA_CustomCalendarController_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_CustomCalendarController class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    13/08/2014              Ana Escrich             Initial Version
	21/04/2017              Jose Miguel Fierro		Increased coverage
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for PCA_CustomCalendarController.loadInfo
            
    History: 
    
     <Date>                     <Author>                <Change Description>
    13/08/2014              Ana Escrich             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void loadInfoTest() {
        PCA_CustomCalendarController constructor = new PCA_CustomCalendarController();
        BI_TestUtils.throw_exception = false;
        constructor.loadInfo();
        constructor.next();
        constructor.prev();
        /*List<User> users = BI_DataLoad.loadUsers(1, BI_DataLoad.searchAdminProfile(), Label.BI_Administrador);
        
        List<Account> accounts = BI_DataLoad.loadAccounts(1, BI_DataLoad.loadPais(1));
        List<RecordType> rT = [select id, name from Recordtype where name='Concepto factura'];
        BI_Cobranza__c cobranzaObject = new BI_Cobranza__c(BI_clientes__c=accounts[0].Id, recordTypeId=rT[0].Id);
        insert cobranzaObject;
        system.runAs(userTest){
             
             PageReference pageRef = new PageReference('PCA_Cobranza_PopUpDetail');
             Test.setCurrentPage(pageRef);
             ApexPages.currentPage().getParameters().put('Id', cobranzaObject.Id);
             
             PCA_Cobranza_PopUpDetail controller = new PCA_Cobranza_PopUpDetail();
             controller.loadInfo();
        }*/
    }
    
    static testMethod void loadInfoTest1() {
        
        List<User> users = BI_DataLoad.loadUsers(1, BI_DataLoad.searchAdminProfile(), Label.BI_Administrador);
        
        List<Account> accounts = BI_DataLoad.loadAccounts(1, BI_DataLoad.loadPaisFromPickList(1));
        
        List<Contact> lst_Contacts = BI_DataLoad.loadContacts(1, accounts);
        
        BI_Permisos_PP__c permis = new BI_Permisos_PP__c(BI_Country__c=accounts[0].BI_Country__c, BI_Segment__c=accounts[0].BI_Segment__c);
        insert permis;
        
        BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(
            OwnerId=users[0].Id,
            BI_User__c=users[0].Id,
            BI_Contacto__c=lst_Contacts[0].Id,
        	BI_Cliente__c=accounts[0].Id,
        	BI_Permisos_PP__c=permis.Id);
        insert ccp;
        
        
        
        system.runAs(users[0]){
            DateTime hoy = System.now();
        	Event eve = new Event(StartDateTime= hoy, IsVisibleInSelfService=true, subject='Other', description='', EndDateTime=hoy.addDays(1), BI_ParentId__c=null);
            insert eve;
            
            List<BI_Contact_Customer_Portal__c> my_ccp= [SELECT BI_Contacto__c FROM BI_Contact_Customer_Portal__c WHERE OwnerId=:UserInfo.getUserId()];
            EventRelation relatEvent = new EventRelation(EventId=eve.Id, RelationId=my_ccp[0].BI_Contacto__c);
            insert relatEvent;
            
             PageReference pageRef = new PageReference('PCA_CustomCalendar');
             Test.setCurrentPage(pageRef);
             //ApexPages.currentPage().getParameters().put('day', Date.today());
             
             PCA_CustomCalendarController constructor = new PCA_CustomCalendarController();
             BI_TestUtils.throw_exception = false;
             constructor.loadInfo();
        }
    }
    static testMethod void loadInfoTest2() {
        
        User users = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        
        //List<Account> accounts = BI_DataLoad.loadAccounts(1, BI_DataLoad.loadPais(1));
        
        
         system.runAs(users){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(2);
            
            List<Account> accounts2 = BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: accounts2){
                    item.OwnerId = users.Id;
                    accList.add(item);
            }
            update accList;
            List<Contact> con = BI_DataLoad.loadPortalContacts(2, accounts2);
             User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());
             User user2 = BI_DataLoad.loadSeveralPortalUsers(con[1].Id, BI_DataLoad.searchPortalProfile());  
             system.debug('user1: ' +user1);
             system.debug('contact1: ' +con[0]);
             system.debug('account1: ' +accounts2[0].id);
             system.debug('account1: ' +accounts2[0]);
             system.debug('account1.owner: ' +accounts2[0].ownerId);

             DateTime day = Datetime.NOW();
             DateTime day10 = day.AddMinutes(10);
             
                    
             PageReference pageRef = new PageReference('PCA_CustomCalendar');
             Test.setCurrentPage(pageRef);
             
             Event__c Evento1;
             
             system.runAs(user2){
                 Event__c Evento2 = new Event__c(Subject__c='Nuevo Evento', description__c='Test Evento', ownerId = accounts2[0].ownerId, ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day);
                 insert Evento2;
                 Evento1 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = accounts2[0].ownerId,ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day);
                 insert Evento1;
                
                 //constructor.getweeks();
             }
              /*EventRelation EvRel1 = new EventRelation(RelationId=con[1].Id,EventId=Evento1.Id);
                 insert EvRel1;*/
             system.runAs(user2){
                    
                 PCA_CustomCalendarController constructor2 = new PCA_CustomCalendarController();
                 BI_TestUtils.throw_exception = false;
                 constructor2.checkPermissions();
                 constructor2.loadInfo();
                 constructor2.next();
                 constructor2.prev();
                 constructor2.getPCA_CustomCalendarMonth();
                 constructor2.getweeks();
                     constructor2.getWeekdayNames();
                    
             }
             system.runAs(user1) {
                 
                 Event__c Evento2 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = accounts2[0].ownerId, ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day);
                 insert Evento2;    
                 
                    
                 ApexPages.currentPage().getParameters().put('idUser', user1.Id);   
                 ApexPages.currentPage().getParameters().put('nameUser', user1.Name);
                 ApexPages.currentPage().getParameters().put('idUserStr', user1.Id);
                 ApexPages.currentPage().getParameters().put('nameUserStr', user1.Name);      
                 
                 //ApexPages.currentPage().getParameters().put('day', Date.today());
                 
                 PCA_CustomCalendarController constructor3 = new PCA_CustomCalendarController();
                 BI_TestUtils.throw_exception = false;
                 constructor3.checkPermissions();
                 constructor3.loadInfo();
                 constructor3.next();
                 constructor3.prev();
                 constructor3.getPCA_CustomCalendarMonth();
                 constructor3.getweeks();
                 system.assertEquals(user1.Id, ApexPages.currentPage().getParameters().get('idUser'));
             }
             system.runAs(user1){
                PCA_CustomCalendarController constructor4 = new PCA_CustomCalendarController();
                 BI_TestUtils.throw_exception = true;
                 constructor4.checkPermissions();
             }
             system.runAs(user1){
                PCA_CustomCalendarController constructor5 = new PCA_CustomCalendarController();
                 constructor5.checkPermissions();
                 BI_TestUtils.throw_exception = true;
                 constructor5.loadInfo();
             }
             system.runAs(user1){
                PCA_CustomCalendarController constructor5 = new PCA_CustomCalendarController();
                 constructor5.checkPermissions();
                 constructor5.loadInfo();
                 BI_TestUtils.throw_exception = true;
                 constructor5.next();
             }
             system.runAs(user1){
                PCA_CustomCalendarController constructor5 = new PCA_CustomCalendarController();
                 constructor5.checkPermissions();
                 constructor5.loadInfo();
                 BI_TestUtils.throw_exception = true;
                 constructor5.prev();
             }
             system.runAs(user1){
                PCA_CustomCalendarController constructor5 = new PCA_CustomCalendarController();
                 constructor5.checkPermissions();
                 constructor5.loadInfo();
                 BI_TestUtils.throw_exception = true;
                 constructor5.getWeekdayNames();
             }
             system.runAs(user1){
                PCA_CustomCalendarController constructor5 = new PCA_CustomCalendarController();
                 constructor5.checkPermissions();
                 constructor5.loadInfo();
                 BI_TestUtils.throw_exception = true;
                 constructor5.getPCA_CustomCalendarMonth();
             }
             system.runAs(user1){
                PCA_CustomCalendarController constructor5 = new PCA_CustomCalendarController();
                 constructor5.checkPermissions();
                 constructor5.loadInfo();
                 BI_TestUtils.throw_exception = true;
                 constructor5.getWeeks();
             }
        }
    }       
    static testMethod void loadInfoTest3() {
        //List<User> users = BI_DataLoad.loadUsers(1, BI_DataLoad.searchAdminProfile(), Label.BI_Administrador);
        User user = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        System.runAs(user){
            Event ev = new Event(Subject='Asunto Prueba',
                                 Description='Descripcion',
                                BI_Temas_Tratados__c = 'Tema1',
                                OwnerId=user.Id,
                                DurationInMinutes=1,
                                ActivityDateTime=System.now());
            insert ev;
            
            List<User> sub_usu = BI_DataLoad.loadUsers(1, BI_DataLoad.searchAdminProfile(), Label.BI_Administrador);
            System.runAs(sub_usu[0]) {
                Event eve = new Event(StartDateTime= System.now(), IsVisibleInSelfService=true, subject='Other', description='', EndDateTime=system.now().addDays(1), OwnerId=user.Id);
                insert eve;
            }
            ApexPages.currentPage().getParameters().put('idUser', user.Id);
            PCA_CustomCalendarController constructor = new PCA_CustomCalendarController();
            BI_TestUtils.throw_exception = false;
            constructor.loadInfo();
           
            constructor.next();
        }
    }
}