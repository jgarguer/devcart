/*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        Pedro Párraga
  Company:       New Energy Aborda
  Description:   Test class for BI_O4_ActivateQuoteController_TEST
  
  History:
  
  <Date>          <Author>        <Change Description>
  26/09/2016        Pedro Párraga     Initial version
  20/09/2017                       Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
  25/09/2017		Jaime Regidor	  Fields BI_Subsegment_Regional__c, BI_Subsector__c and BI_Sector__c added
  ------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_ActivateQuoteController_TEST {

  static{
    BI_TestUtils.throw_exception = false;
  }
	
	@isTest static void test_method_one() {
  
		Map<String,Id> MAP_NAME_RT = new Map<String,Id>();

		for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'NE__Order__c']){
			MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
		}

    	Account acc1 = new Account(Name = 'test',
                                	BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                	BI_Activo__c = Label.BI_Si,
                                	BI_Segment__c = 'Empresas',
                                	BI_Subsector__c = 'test', //25/09/2017
									BI_Sector__c = 'test', //25/09/2017
                                	BI_Subsegment_local__c = 'Top 1', 
                                	BI_Subsegment_Regional__c = 'test', //25/09/2017
                                	BI_Territory__c = 'test'
                                	);                 
		insert acc1;               

    	Opportunity opp = new Opportunity(Name = 'Test',
                                     	 CloseDate = Date.today(),
                                       	 StageName = Label.BI_F6Preoportunidad,
                                      	 AccountId = acc1.Id,
                                       	 BI_Ciclo_ventas__c = Label.BI_Completo 
                                       	 ); 
		insert opp;

		List <NE__Order__c> lst_order = new List <NE__Order__c>();
		NE__Order__c ne_order = new NE__Order__c(NE__OptyId__c = opp.Id, RecordTypeId = MAP_NAME_RT.get('Quote'), NE__orderStatus__c = Label.BI_Active);
		lst_order.add(ne_order);
		NE__Order__c ne_order2 = new NE__Order__c(NE__OptyId__c = opp.Id);
		lst_order.add(ne_order2);
		insert lst_order;

		ApexPages.StandardController standardC = new ApexPages.StandardController(ne_order2); 
		BI_O4_ActivateQuoteController controller = new BI_O4_ActivateQuoteController(standardC);
		controller.activateQuote();
		controller.cancel();

	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Aborda
    Description:   Manage coverage for catchs

    History: 
    
    <Date>                      <Author>                 <Change Description>
   	22/11/2016                  Alvaro García           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   @isTest static void cathExceptions(){

   		Account acc1 = new Account(Name = 'test',
									BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
									BI_Activo__c = Label.BI_Si,
									BI_Segment__c = 'Empresas',
									BI_Subsector__c = 'test', //25/09/2017
									BI_Sector__c = 'test', //25/09/2017
									BI_Subsegment_local__c = 'Top 1',
									BI_Subsegment_Regional__c = 'test', //25/09/2017
									BI_Territory__c = 'test'
									);
		insert acc1;            

   		Opportunity opp = new Opportunity(Name = 'Test',
											CloseDate = Date.today(),
											StageName = Label.BI_F6Preoportunidad,
											AccountId = acc1.Id,
											BI_Ciclo_ventas__c = Label.BI_Completo 
											);
		insert opp; 

		NE__Order__c ne_order2 = new NE__Order__c(NE__OptyId__c = opp.Id);
		insert ne_order2;
		
		BI_TestUtils.throw_exception = true;
		ApexPages.StandardController standardC = new ApexPages.StandardController(ne_order2); 
		BI_O4_ActivateQuoteController controller = new BI_O4_ActivateQuoteController(standardC);
		controller.activateQuote();
		controller.cancel();
    }
}