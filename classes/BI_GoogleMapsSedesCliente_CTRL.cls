public with sharing class BI_GoogleMapsSedesCliente_CTRL {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Controller class for BI_GoogleMapsSedesCliente page
    
    History:
    
    <Date>            <Author>          <Description>
    16/06/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	public Account acc { get; set; }
	public List<BI_GoogleMapsWrapper> lst_address { get; set; }
	
	public class BI_GoogleMapsWrapper{
		public String dir { get; set; }
		public String nam { get; set; }
	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Constructor Method
    
    IN:            ApexPages.StandardController (Account)
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    16/06/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public BI_GoogleMapsSedesCliente_CTRL(ApexPages.StandardController controller){
		try{
			acc = (Account)controller.getRecord();
			
			lst_address = new List<BI_GoogleMapsWrapper>();
			
			for(BI_Punto_de_instalacion__c pdi:[select Id, BI_Sede__c, BI_Sede__r.BI_Numero__c, BI_Sede__r.BI_Direccion__c, BI_Sede__r.BI_Localidad__c, BI_Sede__r.BI_Codigo_postal__c, 
												BI_Sede__r.BI_Country__c, BI_Sede__r.Name from BI_Punto_de_instalacion__c where BI_Cliente__c = :acc.Id])
			{
				String address = '';
				
				if(pdi.BI_Sede__r.BI_Direccion__c != null)
					address += pdi.BI_Sede__r.BI_Direccion__c + ',';
				if(pdi.BI_Sede__r.BI_Numero__c != null)
					address += pdi.BI_Sede__r.BI_Numero__c + ',';
				if(pdi.BI_Sede__r.BI_Localidad__c != null)
					address += pdi.BI_Sede__r.BI_Localidad__c + ',';
				if(pdi.BI_Sede__r.BI_Codigo_postal__c != null)
					address += pdi.BI_Sede__r.BI_Codigo_postal__c + ',';
				if(pdi.BI_Sede__r.BI_Country__c != null)
					address += pdi.BI_Sede__r.BI_Country__c + ',';
					
				if(address != ''){
					address = address.substring(0, address.length() - 1);
					
					BI_GoogleMapsWrapper gmw = new BI_GoogleMapsWrapper();
					gmw.dir = address;
					gmw.nam = pdi.BI_Sede__r.Name.replace('(','');
					gmw.nam = gmw.nam.replace(')','');
					
					lst_address.add(gmw);
				}
					
			}
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('BI_GoogleMapsSedesCliente_CTRL.BI_GoogleMapsSedesCliente_CTRL', 'BI_EN', Exc, 'Trigger');
		}
	}

}