@IsTest
public class PCA_Service_Detail_Controller_TEST {
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test class to manage the coverage code for PCA_Service_Detail_Controller class 

    <Date>                  <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_Service_Detail_Controller.init
            
     <Date>                 <Author>                <Change Description>
    02/03/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testmethod void loadServiceTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);

        System.runAs(userTest){
            Test.startTest();
            NE__OrderItem__c testOrderItem = TGS_Dummy_test_Data.createDummyConfigurationComplexProduct(userTest.Contact.AccountId, userTest.ContactId);
            testOrderItem.TGS_Billing_end_date__c = Date.today();
            update testOrderItem;
            /* Attributes of the OrderItem */
            NE__Order_Item_Attribute__c attribute1 = new NE__Order_Item_Attribute__c(Name = 'AttributeName1', NE__Value__c = '0', NE__Order_Item__c = testOrderItem.Id);
            insert attribute1;
            /* Attributes of the OrderItem */
            NE__Order_Item_Attribute__c attribute2 = new NE__Order_Item_Attribute__c(Name = 'AttributeName2', NE__Value__c = '-', NE__Order_Item__c = testOrderItem.Id);
            insert attribute2;
            
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_Service_Detail_Controller controller  = new PCA_Service_Detail_Controller();
            controller.site = 'test';
            controller.RFBdate = 'test';
            controller.RFSdate = 'test';
            controller.billingEndDate = 'test';
            controller.myAttachmentList = new List<Attachment>();
            controller.myAttachment = new Attachment();
            Test.stopTest();
        }
    }
    
    static testmethod void newModificationTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);        
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.createDummyConfigurationComplexProduct(userTest.Contact.AccountId, userTest.ContactId);
            testOrderItem.NE__Status__c = 'Pending';
            update testOrderItem;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            System.assertNotEquals(testOrderItem.Id, null);
            PCA_Service_Detail_Controller controller  = new PCA_Service_Detail_Controller();
            controller.newModification();
            Test.stopTest();
            System.assertNotEquals(controller.returnPage, 'KO');
        }
    }
    
    static testmethod void newModificationKOTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.createDummyConfigurationComplexProduct(userTest.Contact.AccountId, userTest.ContactId);
            testOrderItem.NE__Status__c = 'Pending';
            update testOrderItem;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_Service_Detail_Controller controller  = new PCA_Service_Detail_Controller();  
            controller.suId = null;
            controller.newModification();
            Test.stopTest();
            System.assertEquals(controller.returnPage, 'KO');
        }
    }
    /*
	static Case createDummyCase(Id confId) {
        List<RecordType> listRecordTypes = [SELECT Id, Name FROM RecordType WHERE Name LIKE 'Order Management Case'];
        Case testCase;
        if(listRecordTypes.size()>0){
            testCase = new Case(RecordTypeId = listRecordTypes[0].Id, Subject = 'Test Subject', Status='Assigned', Order__c = confId, Asset__c= confId, TGS_Service__c='Universal WIFI');
            upsert testCase;
        }
        return testCase;
    } 
    
    */
    
     static testmethod void newTerminationTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.createDummyConfigurationComplexProduct(userTest.Contact.AccountId, userTest.ContactId);
            testOrderItem.NE__Status__c = 'Pending';
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_Service_Detail_Controller controller  = new PCA_Service_Detail_Controller();  
            controller.newTermination();
            Test.stopTest();
            System.assertNotEquals(controller.returnPage, 'KO');
        }
    }
    
    static testmethod void newTerminationKOTest() {
        User userTest=TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.createDummyConfigurationComplexProduct(userTest.Contact.AccountId, userTest.ContactId);
            testOrderItem.NE__Status__c = 'Pending';
            Test.startTest();
            ApexPages.currentPage().getParameters().put('suId', testOrderItem.Id);
            PCA_Service_Detail_Controller controller  = new PCA_Service_Detail_Controller(); 
            controller.suId = null;
            controller.newTermination();
            Test.stopTest();
            System.assertEquals(controller.returnPage, 'KO');
        }
    }
}