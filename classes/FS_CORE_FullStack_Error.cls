/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Generación de tareas para los errores de sincronización.

History:
<Date>							<Author>						<Change Description>
10/04/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
global with sharing class FS_CORE_FullStack_Error{
    /* SALESFORCE VARIABLE DEFINITION */
    /* Empty */
    
    webservice static void sync(String taskId){
        /* Retrieve Data */
        Task task = [SELECT Id, WhoId, WhatId, CallObject, FS_CORE_Bypass__c, FS_CORE_Single_Record__c FROM Task WHERE Id =: taskId][0];
        delete task;
        
        FS_CORE_Fullstack_Manager.callout(task.WhatId, Integer.valueOf(task.CallObject), task.FS_CORE_Bypass__c, task.FS_CORE_Single_Record__c);
    }
    
    public static void createTask(Integer status, String description, String objectType, Id sObjectId, Boolean isBypass, Integer step, Id singleRecordId){
        Task task = new Task();
        
        /* Task Configuration */
        task.Status = 'Código de error: ' + status;
        task.CallObject = '' + FS_CORE_FullStack_Error.getRecoveryStep(step);
        task.FS_CORE_Bypass__c = isBypass;
        
        /* Salesforce Object */
        if(objectType == 'Contact') sObjectId = [SELECT AccountId FROM Contact WHERE Id =: sObjectId][0].accountId;
        if(singleRecordId != null) task.FS_CORE_Single_Record__c = singleRecordId;
        
        Account account = [SELECT Id, OwnerId FROM Account WHERE Id =: sObjectId][0];
        task.WhatId = account.Id;
        task.OwnerId = account.OwnerId;
        
        task.Subject = 'Integraciones Fullstack: Error en ' + FS_CORE_Fullstack_Error.getMethodName(step);
        Map<String, Object> root;
        try{
             root = (Map<String, Object>) JSON.deserializeUntyped(description);
        }catch(Exception e){
            root = new Map<String, Object>();
            task.Certa_SCP__Description__c = 'EmptyBody';
        }
        
        
        if(root.get('Error') != null){
            Map<String,Object> levelError = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(root.get('Error')));
            if(levelError.get('description') != null) task.Certa_SCP__Description__c = (String) levelError.get('description');
        }
        
        task.FS_CORE_FechaHoraTarea__c = datetime.now();
        task.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Error Datos').getRecordTypeId();
        
        /* Comit */
        insert task;
    }
    
    private static String getMethodName (Integer method){
        /* Variable Configuration */
        String result;
        
        if(method == 0) result = 'en la consulta de clientes';
        else if(method == 1) result = 'en la creación del cliente';
        else if(method == 2) result = 'en la actualización del cliente';
        else if(method == 5) result = 'en la creación del contacto';
        else if(method == 6) result = 'en la actualización del contacto';
        else if(method == 3) result = 'en la asociación del usuario';
        else if(method == 4) result = 'en la desasociación del usuario';
        else if(method == 7) result = 'en la asociación del contacto';
        else if(method == 8) result = 'en la desasociación del contacto';
        
        return result;
    }
    
    private static Integer getRecoveryStep (Integer method){
        /* Variable Configuration */
        Integer result;
        
        if(method == 0) result = 0;
        else if(method == 1 || method == 2) result = 1;
        else if(method == 3 || method == 4) result = 2;
        else if(method == 5 || method == 6) result = 3;
        else if(method == 7) result = 4;
        else if(method == 8) result = 5;
        
        return result;
    }
}