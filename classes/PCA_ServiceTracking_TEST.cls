@isTest 
private class PCA_ServiceTracking_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_ServiceTracking class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    30/09/2014              Micah Burgos            Initial Version 
    15/02/2017              Pedro Párraga           Increase coverage
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @IsTest(SeeAllData='true')
    static void PCA_ServiceTracking1() {
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
       
        
        if(rec1!=null){
            ima1.RecordTypeId =rec1.Id; 
            ima1.BI_Titulo__c = 'Test Title';
            ima1.BI_Informacion_Menu__c = 'Test Text';
            ima1.BI_Segment__c = 'Negocios';
            ima1.BI_Pestana__c = 'Service Tracking';
        insert ima1;
        }
            //List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            /*Set<String> countryCantBe = new Set<String>();
             countryCantBe.add('Argentina');
            countryCantBe.add('Chile');
            countryCantBe.add('México');
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickListNotBeing(1, countryCantBe);*/
            
            List<String> lst_pais = new List<String>();
            lst_pais.add('Guatemala');
            
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            List<BI_Cliente_PP__c> cliList = new List<BI_Cliente_PP__c>();
            List<Reportes_PP__c> repoList = new List<Reportes_PP__c>();
            
            List<Report> reports = [SELECT Id FROM Report WHERE IsDeleted = false Order by lastmodifieddate Desc LIMIT 5];
            Integer pos = 0;
            System.debug('+*+*acc+*+*: ' + acc);
            for(Account item: acc){
                item.OwnerId = usr.Id;
                item.BI_Segment__c = 'Negocios';
                item.BI_Subsegment_Local__c = null;
                accList.add(item);
                Reportes_PP__c repo = new Reportes_PP__c(BI_Activo__c=true,BI_Descripcion__c='Descrip',BI_Country__c=item.BI_Country__c,BI_Posicion__c=++pos,BI_Segment__c=item.BI_Segment__c);
                if(reports.size() > 0){
                    repo.BI_URL_del_reporte__c = String.valueOf(reports.get(0).Id);
                    System.debug('++**reportId: ' + String.valueOf(reports.get(0).Id));
                }
                repoList.add(repo);
            }
            update accList;
            insert repoList;
            for(Integer i = 0; i < accList.size(); i++) {
                BI_Cliente_PP__c cli = new BI_Cliente_PP__c(BI_Activo__c=true,BI_Cliente__c=accList.get(i).Id,BI_Imagen_inicio_PP__c=accList.get(i).BI_Imagen_Inicio_PP__c,BI_Reportes__c=repoList.get(i).Id);
                cliList.add(cli);
            }
            insert cliList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
           Attachment attach=new Attachment(Name='Unit Test Attachment',
                                            body=bodyBlob,
                                            parentId=ima1.Id, 
                                            ContentType='image/png');  
            insert attach;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            List<Profile> idsPlus = [select id, name from Profile where Name = 'BI_Customer_Community_Plus' limit 1];
            if(idsPlus.size() <1)
                return;
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, idsPlus[0].Id);          
            system.runAs(user1){
                BI_TestUtils.throw_exception = false;
                
                PageReference pageRef = new PageReference('PCA_ServiceTracking');
                 Test.setCurrentPage(pageRef);

                PCA_ServiceTracking tmpContr = new PCA_ServiceTracking();
                PCA_ServiceTracking.WrapperReport wr = new PCA_ServiceTracking.WrapperReport();
                wr.reportID = 'reportID';
                wr.name = 'name';
                wr.description = 'description';
                wr.lastModified = 'lastModified';
                PageReference ret_page =  tmpContr.checkPermissions();
                tmpContr.loadinfo();
                system.assert(ret_page != null);
                
                
                BI_TestUtils.throw_exception = true;
                                
                tmpContr = new PCA_ServiceTracking();
                tmpContr.checkPermissions();
    
            }
        }
    }
    static testMethod void PCA_ServiceTracking2() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
        
        if(rec1!=null){
            ima1.RecordTypeId =rec1.Id; 
            ima1.BI_Titulo__c = 'Test Title';
            ima1.BI_Informacion_Menu__c = 'Test Text';
            ima1.BI_Segment__c = 'Negocios';
            ima1.BI_Pestana__c = 'Service Tracking';
        insert ima1;
        }
            //List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            
            List<String> lst_pais = new List<String>();
            lst_pais.add('Guatemala');
            
            
            List<Account> acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                item.BI_Segment__c = 'test';
                item.BI_Subsegment_Local__c = 'test';
                accList.add(item);
            }
            update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
           Attachment attach=new Attachment(Name='Unit Test Attachment',
                                            body=bodyBlob,
                                            parentId=ima1.Id, 
                                            ContentType='image/png');  
            insert attach;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
            system.runAs(user1){
                BI_TestUtils.throw_exception = false;
                
                PageReference pageRef = new PageReference('PCA_ServiceTracking');
                 Test.setCurrentPage(pageRef);

                PCA_ServiceTracking tmpContr = new PCA_ServiceTracking();
                PageReference ret_page =  tmpContr.checkPermissions();
                tmpContr.loadinfo();
                system.assert(ret_page != null);
                
                
                BI_TestUtils.throw_exception = true;
                                
                tmpContr = new PCA_ServiceTracking();
                tmpContr.checkPermissions();
    
            }
        }
    }
    static testMethod void PCA_ServiceTracking3() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
        BI_ImageHome__c ima1 = new BI_ImageHome__c();
        RecordType rec1 = [SELECT DeveloperName,Id FROM RecordType where DeveloperName='BI_General'];
       
        if(rec1!=null){
            ima1.RecordTypeId =rec1.Id; 
            ima1.BI_Titulo__c = 'Test Title';
            ima1.BI_Informacion_Menu__c = 'Test Text';
            ima1.BI_Segment__c = 'Empresas';
            ima1.BI_Pestana__c = 'Service Tracking';
        insert ima1;
        }
            //List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            
            List<String> lst_pais = new List<String>();
            lst_pais.add('Guatemala');
             
            List<Account> acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
           Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
           Attachment attach=new Attachment(Name='Unit Test Attachment',
                                            body=bodyBlob,
                                            parentId=ima1.Id, 
                                            ContentType='image/png');  
            insert attach;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
            system.runAs(user1){
                BI_TestUtils.throw_exception = false;
                
                PageReference pageRef = new PageReference('PCA_ServiceTracking');
                 Test.setCurrentPage(pageRef);

                PCA_ServiceTracking tmpContr = new PCA_ServiceTracking();
                PageReference ret_page =  tmpContr.checkPermissions();
                tmpContr.loadinfo();
                system.assert(ret_page != null);
                
                if(tmpContr.tokenApex!= null) {}
                if(tmpContr.RelationImage != null) {}
                
                BI_TestUtils.throw_exception = true;
                                
                tmpContr = new PCA_ServiceTracking();
                tmpContr.checkPermissions();
    
            }
        }
    }
    
    static testMethod void TEST_Reportes() {
       /* TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        //Report rep=new Report();
        //insert rep;
        Reportes_PP__c line =new Reportes_PP__c(
            BI_Activo__c=true,
            BI_Country__c='Argentina',
            BI_Posicion__c=1,
            BI_Segment__c='Empresas',
            BI_URL_del_reporte__c='adfasdfasdf',
            Name='Probando'
        );
        insert line;
        
                BI_TestUtils.throw_exception = false;
                
                PageReference pageRef = new PageReference('PCA_ServiceTracking');
                Test.setCurrentPage(pageRef);

                PCA_ServiceTracking tmpContr = new PCA_ServiceTracking();
                PageReference ret_page =  tmpContr.checkPermissions();
                tmpContr.loadinfo();
                system.assert(ret_page != null);
                
                
                BI_TestUtils.throw_exception = true;
                                
                tmpContr = new PCA_ServiceTracking();
                tmpContr.checkPermissions();
    
               
    }

}