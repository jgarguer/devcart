/*------------------------------------------------------------
 Author:        	Jose Lopez
 Company:       	Deloitte
 Description:   	Class to manage the recordType access in the application, contains a cache with the recordTypes previously obtained
 Test Class:    	TGS_RecordTypes_Util_TEST
 History
 <Date>      	<Author>     	<Change Description>
 16-Jun-2015    Jose Lopez      Initial Version
 07-Feb-2017    Jose M Fierro   Added performance fixes, and removed unnecessary querying and duplicate code
------------------------------------------------------------*/
public class TGS_RecordTypes_Util {
    // local cache
    private static Map<String, Map<String, Id>> rtypesCache; // Map<SObjectName, Map<RTDevName, RTId>>
    
    static {
        rtypesCache = new Map<String, Map<String, Id>>();
    }


    /*------------------------------------------------------------
     Method to load the cache of RecordTypes Objects from a list of Objects.
	 Use: loadRecordTypes(new List<Schema.SObjectTypes){Account.SObjectType,Case.SObjectType})
	 Execute 1 describeCall for Object and only 1 SOQL for call
	------------------------------------------------------------*/    
    public static void loadRecordTypes(List<Schema.SObjectType> tokens) {
        Map<String, Map<Id, Schema.RecordTypeInfo>> infos =new Map<String, Map<Id, Schema.RecordTypeInfo>>();

        String objName;
        Schema.DescribeSObjectResult descrip = null;
        Set<String> set_names = new Set<String>();

        for(Schema.SObjectType token : tokens) {
            descrip = token.getDescribe();
            objName = String.escapeSingleQuotes(descrip.getName()); // Does this add any benefits in this particular situation?

            // Ensure that we have not processed this SOjectType in a prior call
            if(!set_names.contains(objName) && !rtypesCache.containsKey(objName)) {
                set_names.add(objName);
                infos.put(descrip.getName(), descrip.getRecordTypeInfosByID());
            }
        }
        System.debug(LoggingLevel.FINEST, 'TGS_RecordTypes_Util: names='+JSON.serialize(set_names));

        if(!set_names.isEmpty()) {
            try {
                objName = null;
                for (RecordType rt : [SELECT Id, DeveloperName, SObjectType FROM RecordType WHERE SObjectType IN :set_names AND IsActive = TRUE])
                {
                    objName = rt.SObjectType;
                    if (!infos.get(objName).get(rt.Id).isAvailable()) {
                        System.debug(LoggingLevel.FINEST, 'TGS_RecordTypes_Util: RT: ' + objName+'.'+rt.DeveloperName+ ' no disponible');
                    }
                    if(!rtypesCache.containsKey(objName)) {
                        rtypesCache.put(objName, new Map<String, Id>());
                    }
                    rtypesCache.get(objName).put(rt.DeveloperName, rt.Id);
                }
            } catch (Exception ex) {                
                System.debug('TGS_RecordTypes_Util: Excepcion:' + ex);
            }
        }
    }


    /*------------------------------------------------------------
     Method to retrieve the RecordTypes Objects from an Object.
	 Use: 
	 	Map<String, Id> map=getRecordTypeIdsByDeveloperName(Account.SObjectType);
	 	map.get('DeveloperName') retrieve the same as SObjectType.Account.getRecordTypeInfosByName().get(LabelName)
	 	but is indexed by developerName independent of the languaje.
	------------------------------------------------------------*/
    public static Map<String, Id> getRecordTypeIdsByDeveloperName(Schema.SObjectType token) {
        String objName = token.getDescribe().getName();
        
        if(!rtypesCache.containsKey(objName)) {
            loadRecordTypes(new Schema.SObjectType[] { token });
        }
        return rtypesCache.get(objName);
    }
	

    public static Id getRecordTypeId(Schema.SObjectType objectType,String devName){
        Map<String,Id> ids = getRecordTypeIdsByDeveloperName(objectType);
        return ids.get(devName);
    }
}