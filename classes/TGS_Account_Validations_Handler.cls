public class TGS_Account_Validations_Handler {

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Angel Galindo, Carlos Campillo
    Company:       Deloitte
    Description:   This class contains al methods related with Account object.
                    Class invoked by the trigger Account.
    
    History

    <Date>            <Author>                      <Description>
    13/01/2015        Miguel Angel Galindo          Initial version
    19/05/2015        Miguel Angel Galindo          Changed Checkbox TGS_Active__c by picklist BI_Activo__c
    22/05/2015        Carlos Campillo Gállego       Deleted "With Sharing" and added ADM check.
    25/05/2015        Carlos Campillo Gállego       Added ADM check.
    08/11/2017        Álvaro López                  Added preventAccountToBeDeleted method
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    
    private static String msg = '';
    
    public static void checkActiveAccount(List<Account> listAccountNew, List<Account> listAccountOld){

        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
        Boolean ADM = userTGS.TGS_Is_Admin__c;

        if(!ADM){
            for(Account ao :listAccountOld){
                system.debug('Account: '+ao);
                for(Account ac :listAccountNew){
                    if(ao.Id == ac.Id){
                        if(ao.BI_Activo__c == Label.BI_Si){
                            if(!(ac.BI_Activo__c == Label.BI_Si)){
                                boolean deactivate = searchSons(ac);
                                if(!deactivate){
                                    try{
                                        ac.BI_Activo__c.addError('This Account can not be deactivated '+msg);
                                    }catch(Exception e){
                                        System.debug('Exception: '+e);
                                    }
                                }
                            }
                        }else{
                            ac.addError('This Account can not be edited because is inactive');
                        }
                    }
                }
            }
        }
    }
    
    /* This Method checks if the parent Id of an Account has been changed */
    public static void checkAccountStructure(List<Account> listAccountNew, List<Account> listAccountOld){
        
        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
        Boolean ADM = userTGS.TGS_Is_Admin__c;

        
        if(!ADM){
        
                List<Id> newParent = new List<Id>();
                List<Id> oldParent = new List<Id>();
                for(Account ac :listAccountNew){
                    newParent.add(ac.ParentId);
                }
                for(Account ac :listAccountOld){
                    oldParent.add(ac.ParentId);
                }
                
                integer i = 0;
                while(i<newParent.size()){
                    if(newParent.get(i)!= oldParent.get(i)){
                        for(Account ac :listAccountNew){
                            if(ac.ParentId==newParent.get(i)){
                                try{
                                    ac.addError('Account structure can not be modified');
                                }catch(Exception e){
                                    System.debug('Exception '+e);
                                }
                                break;
                            }
                        }
                    }
                    i++;
                }
        }
    }
    
    /* This Method search sons of the account on the system*/
    private static boolean searchSons(Account account){
        
        List<Account> accountSons = [Select Id, BI_Activo__c 
                                    From Account
                                    Where ParentId = :account.Id];
        if(accountSons.size()>0){
            for(Account ac :accountSons){
                if(ac.BI_Activo__c == Label.BI_Si){
                    msg = 'because it has some related accounts.';
                    return false;
                }else{
                    return searchSons(ac);
                }
            }
        }else{
            return comprobarCI(account);
        }
        return false;
    }
    
    /* This method checks if the account has some configuration Item associated to it*/
    public static boolean comprobarCI(Account account){

        List<NE__OrderItem__c> activeCIsList = 
                [select Id, TGS_Service_status__c 
                 from NE__OrderItem__c 
                 where TGS_Service_status__c <> :Constants.CI_TGS_SERVICE_STATUS_DELETED and 
                 NE__Billing_Account_Asset_Item__c = :account.Id];
        if(activeCIsList.size()>0){
            msg = 'because it has some installed Service Units.';
            return false;
        }
        return true;    
    }

    public static void preventAccountToBeDeleted(List<Account> olds){

        User user = [SELECT Profile.Name FROM User WHERE Id =: UserInfo.getUserId()];
        System.debug('User: ' + user);

        for(Account acc : olds){
            if(user.Profile.Name == Constants.PROFILE_TGS_PERFIL_1 || user.Profile.Name == Constants.PROFILE_TGS_ORDER_HANDLING){
                acc.addError('You have not permissions to delete this record.');
            }
        }
    }
}