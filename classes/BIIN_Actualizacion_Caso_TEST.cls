@isTest(seeAllData = false)
public class BIIN_Actualizacion_Caso_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        José María Martín
Company:       Deloitte
Description:   Test method to manage the code coverage for BIIN_Actualizacion_Caso

<Date>                 <Author>               <Change Description>
11/2015              José María Martín          Initial Version
11/02/2016           Micah BUrgos               Fix Bug (BI_Id_del_caso_legado__c = 'INC000000001022')
08/02/2017           Pedro Párraga              Increase coverage
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static List<Map<String, String>> ladditionalData;
    private static User usr;

    public static void createData(){
        BI_TestUtils.throw_exception=false;
        usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             

        RestContext.request = req;
        RestContext.response = res;

        ladditionalData = new  List<Map<String, String>>();
            
        Map<String, String> mapId1 = new Map<String, String>();   
        mapId1.put( 'key', 'fechaRadicacion');
        mapId1.put('value', '2222222');
        ladditionalData.add(mapId1);

        Map<String, String> mapId2 = new Map<String, String>(); 
        mapId2.put( 'key', 'nombreContactoFinal');
        mapId2.put('value', 'PRUEBAS');
        ladditionalData.add(mapId2);

        Map<String, String> mapId3 = new Map<String, String>(); 
        mapId3.put( 'key', 'apellidoContactoFinal');
        mapId3.put('value', 'BAO');
        ladditionalData.add(mapId3);

        Map<String, String> mapId4 = new Map<String, String>(); 
        mapId4.put( 'key', 'emailContactoFinal');
        mapId4.put('value', '');
        ladditionalData.add(mapId4);

        Map<String, String> mapId5 = new Map<String, String>(); 
        mapId5.put( 'key', 'urgencia');
        mapId5.put('value', '2000'); 
        ladditionalData.add(mapId5);

        Map<String, String> mapId6 = new Map<String, String>();
        mapId6.put( 'key', 'subStatus');
        mapId6.put('value', '27000'); 
        ladditionalData.add(mapId6);     
    }

    static testMethod void test_BIIN_Actualizacion_Caso_TEST() {
        createData();
        System.runAs(usr){             
          
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                item.Name = 'ACME';
                item.BI_Country__c='Peru';
                item.BI_Tipo_de_identificador_fiscal__c='RUC';
                item.BI_No_Identificador_fiscal__c ='12345678910';
                accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            
            Contact ContactTest = new Contact(
                                            AccountId = accList[0].Id,
                                            FirstName = 'PRUEBAS',
                                            LastName = 'BAO',
                                            Email = '');
            insert ContactTest;

            BI_TestUtils.throw_exception = false;
            List<RecordType> rT = [select id, name from Recordtype where DeveloperName = 'BIIN_Incidencia_Tecnica'];
            Case caseTest = new Case(RecordTypeId=rT[0].Id, 
                                     ContactId = con[0].Id,
                                     AccountId = accList[0].Id,
                                     BI_Id_del_caso_legado__c = 'INC000000001022',
                                     BI2_fecha_hora_de_resolucion__c = DateTime.newInstance(2015,12,28,10,0,0),
                                     BI_Otro_Tipo__c = 'test', 
                                     Status='Nuevo',
                                     Priority='Media', 
                                     Type='Reclamo',
                                     BI_Country__c = Label.BI_Peru,
                                     Subject='Test',
                                     CurrencyIsoCode='ARS', 
                                     Origin='Portal Cliente',
                                     BI_Confidencial__c=false, 
                                     TGS_Impact__c = '3-Moderate/Limited',
                                     BI_Assigned_Group__c = 'SupportGroup',
                                     Reason='Reclamos administrativos', Description='testdesc');
            insert caseTest;

            Case c = [SELECT caseNumber FROM Case WHERE BI_Id_del_caso_legado__c = 'INC000000001022' LIMIT 1];
            
            BIIN_Actualizacion_Caso.TicketEntrada entrada = new BIIN_Actualizacion_Caso.TicketEntrada( );
            entrada.ticketId = caseTest.caseNumber; //CaseNumber
            entrada.correlatorId = 'INC000000001022';//BI_Id_del_caso_legado__c
            entrada.subject = 'PRUEBAAAA123';//Subject
            entrada.description = '';//Description
            entrada.country = 'United States';//BI_Country__c
            entrada.customerId = usr.id;
            entrada.accountId = 'PER_RUC_12345678910';//BI_Validador_Fiscal__c 
            entrada.creationDate = '1445870250';//CreatedDate
            entrada.severity = '2000';//TGS_Impact__c
            entrada.priority = '1';//Priority
            entrada.ticketType = '0';
            entrada.source = '1000';//Origin
            entrada.parentTicket = 'PID001';//Parent
            entrada.ticketStatus = '6';//Status
            entrada.statusChangeDate = '1446048563';
            entrada.statusChangeReason = '';//LastModifiedDate
            entrada.targetResolutionDate = '';
            entrada.resolutionDate = '1234124123';//BI2_fecha_hora_de_resolucion__c
            entrada.resolution = '';//TGS_Resolution__c
            entrada.responsibleParty = 'REMEDY1';//BI_Assigned_Group__c*/            
            
            BIIN_Actualizacion_Caso.TicketEntrada entradaElse = new BIIN_Actualizacion_Caso.TicketEntrada( );
            entradaElse.ticketId = c.CaseNumber; //CaseNumber
            entradaElse.correlatorId = 'INC000000001022';//BI_Id_del_caso_legado__c
            entradaElse.subject = 'PRUEBAAAA123';//Subject
            entradaElse.description = '';//Description
            entradaElse.country = 'United States';//BI_Country__c
            entradaElse.customerId = usr.id;
            entradaElse.accountId = 'PER_RUC_12345678910';//BI_Validador_Fiscal__c 
            entradaElse.creationDate = '1445870250';//CreatedDate
            entradaElse.severity = '2000';//TGS_Impact__c
            entradaElse.priority = '1';//Priority
            entradaElse.ticketType = '0';
            entradaElse.source = '1000';//Origin
            entradaElse.parentTicket = 'PID001';//Parent
            entradaElse.ticketStatus = '6';//Status
            entradaElse.statusChangeDate = '1446048563';
            entradaElse.statusChangeReason = '';//LastModifiedDate
            entradaElse.targetResolutionDate = '';
            entradaElse.resolutionDate = '';//BI2_fecha_hora_de_resolucion__c
            entradaElse.resolution = '';//TGS_Resolution__c
            entradaElse.responsibleParty = 'REMEDY1';//BI_Assigned_Group__c*/     
            
            BIIN_Actualizacion_Caso.TicketEntrada entradaAccountCatch = new BIIN_Actualizacion_Caso.TicketEntrada( );
            entradaAccountCatch.accountId='';

            entrada.additionalData =  ladditionalData;
            entradaElse.additionalData = ladditionalData;  
            
            Test.startTest(); 
                Test.setMock(HttpCalloutMock.class, new TGS_MockHttpResponseGenerator());   
                BIIN_Actualizacion_Caso controller = new BIIN_Actualizacion_Caso();
                BIIN_Actualizacion_Caso.actualizarCaso(entrada);
                BIIN_Actualizacion_Caso.actualizarCaso(entradaElse);  
                BIIN_Actualizacion_Caso.actualizarCaso(entradaAccountCatch);
            Test.stopTest();
        }       
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that manage the code coverage of BIIN_Helper class.
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        27/01/2016                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void BIIN_Helper_TEST(){

        BIIN_Tabla_Correspondencia__c tabla = new BIIN_Tabla_Correspondencia__c(
            BIIN_RoD_ID__c = '1',
            BIIN_Valor_Salesforce__c = 'TestSf',
            BIIN_Valor_Remedy__c = 'TestRoD',
            BIIN_Tipo_Tabla__c = 'TablaTest',
            Name = 'Test');
        insert tabla;

        BIIN_Tabla_Correspondencia__c tabla2 = new BIIN_Tabla_Correspondencia__c(
            BIIN_RoD_ID__c = '2',
            BIIN_Valor_Salesforce__c = 'TestSf2',
            BIIN_Valor_Remedy__c = 'TestRoD2',
            BIIN_Tipo_Tabla__c = 'TablaTest',
            Name = 'Test1');
        insert tabla2;

        BIIN_Helper.traducirValor('TablaTest', '1');
        BIIN_Helper.traducirValor('TablaTest', 'TestSf');
        BIIN_Helper.traducirValor('TablaTest', 'TestRoD');

    }

  static testMethod void test_BIIN_Actualizacion_Caso_TEST_Two() {
        createData();        
        
        System.runAs(usr){             
              
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                item.Name = 'ACME';
                item.BI_Country__c='Peru';
                item.BI_Tipo_de_identificador_fiscal__c='RUC';
                item.BI_No_Identificador_fiscal__c ='12345678910';
                accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            
            Contact ContactTest = new Contact(
                                            AccountId = accList[0].Id,
                                            FirstName = 'PRUEBAS',
                                            LastName = 'BAO',
                                            Email = '');
            insert ContactTest;
            

            BI_TestUtils.throw_exception = false;
            List<RecordType> rT = [select id, name from Recordtype where DeveloperName = 'BI_Caso_Comercial_Abierto'];
            Case caseTest = new Case(RecordTypeId=rT[0].Id, 
                                     ContactId = con[0].Id,
                                     AccountId = accList[0].Id,
                                     BI_Id_del_caso_legado__c = 'INC000000001022',
                                     BI2_fecha_hora_de_resolucion__c = DateTime.newInstance(2015,12,28,10,0,0),
                                     BI_Otro_Tipo__c = 'test', 
                                     Status='Nuevo',
                                     Priority='Media', 
                                     Type='Reclamo',
                                     BI_Country__c = Label.BI_Peru,
                                     Subject='Test',
                                     CurrencyIsoCode='ARS', 
                                     Origin='Portal Cliente',
                                     BI_Confidencial__c=false, 
                                     TGS_Impact__c = '3-Moderate/Limited',
                                     BI_Assigned_Group__c = 'SupportGroup',
                                     Reason='Reclamos administrativos', Description='testdesc');
            insert caseTest;
            
            BIIN_Actualizacion_Caso.TicketEntrada entrada = new BIIN_Actualizacion_Caso.TicketEntrada( );
            entrada.ticketId = caseTest.caseNumber; //CaseNumber
            entrada.correlatorId = 'INC000000001022';//BI_Id_del_caso_legado__c
            entrada.subject = 'PRUEBAAAA123';//Subject
            entrada.description = '';//Description
            entrada.country = 'United States';//BI_Country__c
            entrada.customerId = usr.id;
            entrada.accountId = 'PER_RUC_12345678910';//BI_Validador_Fiscal__c 
            entrada.creationDate = '1445870250';//CreatedDate
            entrada.severity = '2000';//TGS_Impact__c
            entrada.priority = '1';//Priority
            entrada.ticketType = '0';
            entrada.source = '1000';//Origin
            entrada.parentTicket = 'PID001';//Parent
            entrada.ticketStatus = '6';//Status
            entrada.statusChangeDate = '1446048563';
            entrada.statusChangeReason = '';//LastModifiedDate
            entrada.targetResolutionDate = '';
            entrada.resolutionDate = '1234124123';//BI2_fecha_hora_de_resolucion__c
            entrada.resolution = '';//TGS_Resolution__c
            entrada.responsibleParty = 'REMEDY1';//BI_Assigned_Group__c*/            

            entrada.additionalData =  ladditionalData;
            
            Test.startTest(); 
                Test.setMock(HttpCalloutMock.class, new TGS_MockHttpResponseGenerator());   
                BIIN_Actualizacion_Caso controller = new BIIN_Actualizacion_Caso();
                BIIN_Actualizacion_Caso.actualizarCaso(entrada);

                entrada.correlatorId = null;
                BIIN_Actualizacion_Caso.actualizarCaso(entrada);

            Test.stopTest();
        }       
    }

}