/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       27-Abr-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
public with sharing class BI_COL_Midas_Version_cls 
{
    
    //guarda en cache USD antes de usar esta variable hay que llamar el metodo estatico MidasCreation_cls.getUSD();
    //public static Decimal monedaUSD = null;
    
    //public static void getUSD()
    //{
    //    //Si monedaUSD es diferente de nulo evita que se ejecute el resto del codigo (Solo se ejecuta 1 vez por contexto)
    //    if( monedaUSD != null )return;
        
    //    list<CurrencyType> curr = new list<CurrencyType>([SELECT ConversionRate FROM CurrencyType WHERE IsoCode = 'USD' LIMIT 1]);
        
    //    if( !curr.isEmpty() && monedaUSD == null )
    //        monedaUSD = curr[0].ConversionRate;
    //    else
    //        monedaUSD = 0;
    //}
    
    //public void validarPrefactibilidad (list<BI_COL_MIDAS__c> midas)
    //{
    //    set<Id> listaOportunidades = new set<Id>();
        
    //    for( BI_COL_MIDAS__c m : midas )
    //        listaOportunidades.add( m.BI_COL_Proyecto__c );
        
    //    map<Id,Opportunity> oporPrefactibilidad = new map<Id,Opportunity>([
    //        select  Id,
    //                (SELECT Id, Name, BI_COL_Proyecto__c, BI_COL_Registro_aprobado__c, BI_COL_Fecha_aprovacion_prefactibilidad__c 
    //                FROM    PreFactibilidad__r 
    //                where   BI_COL_Proyecto__c IN: listaOportunidades 
    //                and     BI_COL_Registro_aprobado__c = 'Aprobada' 
    //                ORDER BY CreatedDate DESC NULLS FIRST limit 1) 
    //        from    Opportunity 
    //        where   Id IN: listaOportunidades]);
    //    system.debug('\n\n##oporPrefactibilidad:'+ oporPrefactibilidad +'\n\n');
    //    for( BI_COL_MIDAS__c m2 : midas )
    //    {
    //        Opportunity preFact = oporPrefactibilidad.get( m2.BI_COL_Proyecto__c );
    //        if( preFact != null && !preFact.PreFactibilidad__r.isEmpty() )
    //        {
    //            if( preFact.PreFactibilidad__r[0].BI_COL_Registro_aprobado__c != 'Aprobada' && 
    //                preFact.PreFactibilidad__r[0].BI_COL_Fecha_aprovacion_prefactibilidad__c != null )
    //                m2.addError( 'La ultima Prefactibilidad creada no se encuentra en estado Aprobada');        
    //            m2.BI_COL_Fecha_aprobacionprefactibilidad__c = preFact.PreFactibilidad__r[0].BI_COL_Fecha_aprovacion_prefactibilidad__c;
    //        }
    //        else
    //            m2.addError('No se encontro una prefactibilidad en estado Aprobado');
    //    }
    //}
    
    //public void actualizarMidas( list<Id> midasLstId )
    //{
    //    list<BI_COL_MIDAS__c> lstMidas = new list<BI_COL_MIDAS__c> ([
    //        select  Id, BI_COL_Estado_de_la_aprobacion__c 
    //        from    BI_COL_MIDAS__c 
    //        where   Id =: midasLstId]);
        
    //    for(BI_COL_MIDAS__c m: lstMidas)
    //    {
    //        if( m.BI_COL_Estado_de_la_aprobacion__c == 'Aprobado' )
    //            m.BI_COL_Estado_de_la_aprobacion__c='Aprobado - Inactivo';          
    //        else
    //            m.BI_COL_Estado_de_la_aprobacion__c='Rechazado - Inactivo';         
    //    }
    //    update lstMidas;
    //}

    //public static void midastgr(list <BI_COL_MIDAS__c> listatrigger)

    //{
    //    BI_COL_Midas_Version_cls mdcr = new BI_COL_Midas_Version_cls();
    //    //if(!bypass.BI_migration__c)
    //    //{ 
    //        for( BI_COL_MIDAS__c midasObj : listatrigger )
    //        {
    //            //midasObj.BI_COL_Estado_de_la_aprobacion__c = null;
    //            midasObj.BI_COL_Version__c = -1;
                
    //            list<AggregateResult> Mvercion= [SELECT MAX(BI_COL_Version__c) maxVersion FROM BI_COL_MIDAS__c WHERE BI_COL_Proyecto__c = : midasObj.BI_COL_Proyecto__c];
                
    //            List<AggregateResult> listMidas =[
    //                SELECT  Id, MAX(BI_COL_Version__c) maxVersion, BI_COL_Bloqueado__c bloqueado, BI_COL_Estado_de_la_aprobacion__c estado, BI_COL_Proyecto__r.StageName st
    //                FROM    BI_COL_MIDAS__c
    //                WHERE   BI_COL_Proyecto__c =: midasObj.BI_COL_Proyecto__c
    //                GROUP BY id, BI_COL_Bloqueado__c, BI_COL_Estado_de_la_aprobacion__c, BI_COL_Proyecto__r.StageName ];
    //            //Asigna La TRM Cargada se debe cargar previamente MidasCreation_cls.getUSD();                          
    //            midasObj.BI_COL_TRM_SFDC__c = BI_COL_Midas_Version_cls.monedaUSD;
    //            //Valida que la ultima prefactibilidad este aprobada
    //            mdcr.validarPrefactibilidad( listatrigger );
                
    //            ID id = null;
    //            Integer maxVersion = -1;
    //            Boolean bloqueado = true;
    //            String estadoAprobacion = 'Rechazado - Inactivo';
    //            Boolean control = true;
    //            //Almacena el Id del midas que esta Aprobado o nulo
    //            list<Id> midasAN = new list<Id>(); 
    //            for( AggregateResult ar : listMidas )
    //            {
    //                id = (ID)ar.get('id');
                    
    //                //Si el registro esta bloqueado actualiza la variable
    //                //if( Boolean.valueOf( ar.get('bloqueado') ) )
    //                    //bloqueado = Boolean.valueOf( ar.get('bloqueado') );
                    
    //                estadoAprobacion = String.valueOf( ar.get('estado') );
    //                System.debug('\n\n estadoAprobacion= '+estadoAprobacion+'\n\n');
    //                //Validamos si se puede crear o no un nuevo MIDAS
    //                //if( //( ar.get('st') != 'F1 - Cerrada/Legalizada') && 
    //                    //estadoAprobacion != 'Rechazado - Inactivo' || estadoAprobacion != 'Aprobado - Inactivo')
    //                    if(estadoAprobacion == 'Pendiente Aprobacion')
    //                {
    //                    control = false;
    //                    midasAN.add(ar.id);
    //                }

    //            }
                
    //            if( Mvercion[0].get('maxVersion') == null )
    //                midasObj.BI_COL_Version__c = 1;     
    //            else
    //                midasObj.BI_COL_Version__c = 1 + integer.valueOf( Mvercion[0].get('maxVersion') );
                
    //            if(!control)
    //            {
    //                //if(bloqueado)
    //                    midasObj.addError('No se puede crear un nuevo MIDAS porque existe uno en proceso de aprobacion');
    //            }
    //             else
    //            {
    //                mdcr.actualizarMidas(midasAN);
    //                if( maxVersion > -1)
    //                    midasObj.BI_COL_Version__c = maxVersion + 1;
    //            }
    //        }    

    //    //}    

    //}

    ///*-------------------------------------------------------------------------------------------------------------------------------------------------------
    //    Author:        Guillermo Muñoz
    //    Company:       NEAborda
    //    Description:   Method that validate the posible mosifications of BI_COL_Estado_de_la_aprobacion__c field. Created for the 'Demanda 252'
        
    //    History: 
        
    //    <Date>                          <Author>                    <Change Description>
    //    02/09/2016                      Guillermo Muñoz             Initial version
    //--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    //public static void validateStatus(Map <Id, BI_COL_MIDAS__c> news, Map <Id,BI_COL_MIDAS__c> olds){

    //    try{

    //        if(BI_TestUtils.isRunningTest())
    //            throw new BI_Exception('test');

    //        List <BI_COL_MIDAS__c> toProcess = new List <BI_COL_MIDAS__c>();
    //        List <BI_COL_MIDAS__c> errors = new List <BI_COL_MIDAS__c>();

    //        for(Id idMidas : news.Keyset()){
    //            //Midas que pasan de Aprobado a Rechazado
    //            if(news.get(idMidas).BI_COL_Estado_de_la_aprobacion__c == 'Rechazado - Inactivo' && olds.get(idMidas).BI_COL_Estado_de_la_aprobacion__c == 'Aprobado'){

    //                toProcess.add(news.get(idMidas));
    //            }
    //            //Midas que están en estado aprobado y no cambian a rechazado
    //            else if(olds.get(idMidas).BI_COL_Estado_de_la_aprobacion__c == 'Aprobado'){
    //                errors.add(news.get(idMidas));
    //            }
    //            //Midas que están en estado rechazado
    //            if(olds.get(idMidas).BI_COL_Estado_de_la_aprobacion__c == 'Rechazado - Inactivo'){

    //                errors.add(news.get(idMidas));
    //            }
    //        }
    //        if(!toProcess.isEmpty()){

    //            String permisos = [SELECT BI_Permisos__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1].BI_Permisos__c;
    //            //Solo los usuarios con permisos de Ingeniería/Preventa pueden modificar el estado del MIDAS
    //            if(permisos == Label.BI_Ingenieria_Preventa){

    //                Map<String, Schema.SObjectField> map_fields = BI_COL_MIDAS__c.sObjectType.getDescribe().fields.getMap();
    //                //Comprobamos que solo cambia el campo BI_COL_Estado_de_la_aprobacion__c
    //                for(Schema.SObjectField field : map_fields.values()){

    //                    for(BI_COL_MIDAS__c midas : toProcess){

    //                        String fieldName = field.getDescribe().getName();
    //                        if(fieldName != 'BI_COL_Estado_de_la_aprobacion__c'){

    //                            if(news.get(midas.Id).get(fieldName) != olds.get(midas.Id).get(fieldName)){

    //                                midas.addError('Sólo se puede modificar el campo Estado de la aprobación del MIDAS');
    //                                break;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            else{
    //                //Si no tienes permisos
    //                for(BI_COL_MIDAS__c midas : toProcess){

    //                    midas.addError('No tiene permisos para realizar la modificación');
    //                }
    //            }
    //        }

    //        if(!errors.isEmpty()){
                
    //            for(BI_COL_MIDAS__c midas : errors){
    //                //Si el midas está en estado rechazado
    //                if(midas.BI_COL_Estado_de_la_aprobacion__c == 'Rechazado - Inactivo'){
    //                    midas.addError('No se puede modificar un MIDAS rechazado, debe crear uno nuevo');
    //                }
    //                //Si el midas está en estado aprobado
    //                else{
    //                    midas.addError('No se puede modificar un MIDAS aprobado');
    //                }
    //            }
    //        }
    //    }catch(Exception exc){
    //        BI_LogHelper.generate_BILog('BI_COL_Midas_Version_cls.validateStatus', 'BI_EN', Exc, 'Trigger');
    //    }
    //}

     //19/10/2016 START GSPM: Obtirene el usurio d ela sesión
     //31/01/2017 START GSPM: 
     //27/02/2017 START GSPM: 
    public static void SendEmail(List<BI_COL_MIDAS__c> lstNewRecords, List<BI_COL_MIDAS__c> lstOldRecords){

        NETriggerHelper.setTriggerFired('BI_COL_Midas_Version_cls.SendEmail'); 
        List<Messaging.Email> lstmessages = new List<Messaging.Email>();

        set<Id> setIdMIDAS = new set<Id>();
        for(Integer i = 0; i < lstNewRecords.size(); i++)
        {
            
           if(lstNewRecords[i].BI_COL_Estado_de_la_aprobacion__c != lstOldRecords[i].BI_COL_Estado_de_la_aprobacion__c)
           {               
    		  setIdMIDAS.add(lstNewRecords[i].Id);
            }               
            
        }       

        system.debug('lstNewRecords>>>>> ' + lstNewRecords + 'lstOldRecords>>>>> ' + lstOldRecords);
       
        
        if(!setIdMIDAS.isEmpty())
        {
            
            ProcessInstance objUser = [SELECT Id,CreatedById,CreatedDate, TargetObjectId, Status, LastActor.name  
                                FROM ProcessInstance where TargetObjectId IN: setIdMIDAS limit 1];
                            
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();    
                               
            for(BI_COL_MIDAS__c objNewRecords : lstNewRecords)
            {        
        				 
        		system.debug('objUser.CreatedById>>>>> ' + objUser.CreatedById);
        		message.setTargetObjectId(objUser.CreatedById);
        		//message.setToAddresses(toAddresses); 
        		message.setSenderDisplayName('Alerta Aprobación Midas');
        		message.setSubject('Alerta Aprobación Midas');
        		message.setUseSignature(false);
                string strURL = label.BI_COL_UrlMIDAS+objNewRecords.Id;
        		
              	String plainTextBody = objUser.LastActor.name +' ha dado respuesta : ' + objNewRecords.BI_COL_Estado_de_la_aprobacion__c + '  a su solicitud de aprobación, para el MIDAS: '+ objNewRecords.name+
                ' en el siguiente elemento: '+strURL+' '+ '\nHaga clic en este vínculo para observar este registro. \n\n'+'Gracias, \n'+'Salesforce.';      
                        
        		message.setPlainTextBody(plainTextBody);		
        		lstmessages.add(message);
        		message.setSaveAsActivity(false);
            }
            if(!Test.isRunningTest()){

    	        Messaging.SendEmailResult[] results = Messaging.sendEmail( lstmessages );
            }
        }        
    }
    //27/02/2017 END GSPM: 
    //31/01/2017 END GSPM: 
}