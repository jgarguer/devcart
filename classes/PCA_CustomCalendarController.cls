public without sharing class PCA_CustomCalendarController extends PCA_HomeController {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Class to show events of users 
    
    History:
    
    <Date>            <Author>          	<Description>
    11/06/2014        Ana Escrich      	 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public String literalCalendarHeader {get;set;}
	public String addItemArguments {get;set;}
	private id idUserCalendar; 
	public List<eventWrapper> wrapperList	{get;set;}
	public List<Event> listEvent {get; set;}
  	private List<Event__c> events;
  	private PCA_CustomCalendarMonth pCA_CustomCalendarMonth;

	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>          	<Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public PageReference checkPermissions (){
		try{
			PageReference page = enviarALoginComm();
			system.debug('page: ' +page);
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			if(page == null){
				loadInfo();
			}
			return page;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_CustomCalendarController.checkPermissions', 'Portal Platino', Exc, 'Class');
		   return null;
		}
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   load events in the calendar
    
    History:
    
    <Date>            <Author>          	<Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public void loadInfo (){
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			Date day = system.today();  // default to today 
		    //Integer mo = day.month(); 
		    
		    //String m_param = System.currentPageReference().getParameters().get('mo');
		    //String m_param = String.escapeSingleQuotes(System.currentPageReference().getParameters().get('mo'));
		    //String y_param = System.currentPageReference().getParameters().get('yr');
		    //String y_param = String.escapeSingleQuotes(System.currentPageReference().getParameters().get('yr'));
		   
		    String idUserStr = 'idUser';
		    String nameUserStr = 'nameUser';
		    
		    String idUserP = (System.currentPageReference().getParameters().get(idUserStr)!=null)?String.escapeSingleQuotes(System.currentPageReference().getParameters().get(idUserStr)):null;
		    //String idUserP = String.escapeSingleQuotes(System.currentPageReference().getParameters().get(idUserStr));
		    String nameUserP =  (System.currentPageReference().getParameters().get(nameUserStr)!=null)?String.escapeSingleQuotes(System.currentPageReference().getParameters().get(nameUserStr)):null;    
		   	system.debug('3');
		    /** Defining user calendar: if is fill that is the user calendar, else, user calendar was UserInfo.getUserId()*/
		    this.idUserCalendar = idUserP!=null?idUserP:UserInfo.getUserId();
		    system.debug('this.idUserCalendar'+this.idUserCalendar);
			//system.debug('idUserCalendar: '+idUserCalendar);
			/** Defining arguments to invoke PCA_CustomEventDetail, overall if is for an "equipo cliente" user meeting*/
			this.addItemArguments = idUserP!=null?'&'+idUserStr+'='+idUserP+'&'+nameUserStr+'='+nameUserP:null;	
			//this.addItemArguments = idUserCalendar!=null?'&idUser='+idUserCalendar+'&nameUser='+nameUserP:null;
		    this.literalCalendarHeader = nameUserP!=null?Label.BI_LabelCalendario+' ' + nameUserP:Label.BI_LabelCalendario1;	
		   	// allow a month to be passed in on the url as mo=10
			/*if (m_param != null) { 
		    	Integer monthIndex = Integer.valueOf(m_param); 
		        if (monthIndex > 0 && monthIndex <= 12) {
		          day = Date.newInstance(day.year(),monthIndex,day.day());
		        }
		   	}
	   		// and year as yr=2008
	   		if (y_param != null) { 
		        Integer year = Integer.valueOf(y_param); 
		        day = Date.newInstance(year, day.month(), day.day());
	   		}*/
			System.debug('## MONTH EVAL: day:' +day );
		   	setPCA_CustomCalendarMonth(day);
		   	system.debug('addItemArguments: '+addItemArguments);
		   	system.debug('addItemArguments: '+this.addItemArguments);
	 	}catch (exception Exc){
	   		BI_LogHelper.generate_BILog('PCA_CustomCalendarController.PCA_CustomCalendarController', 'Portal Platino', Exc, 'Class');
	    }
  	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Constructor
    
    History:
    
    <Date>            <Author>          	<Description>
    11/06/2014        Ana Escrich      	 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public PCA_CustomCalendarController() {
	
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Add one month to the index of calendar
    
    History:
    
    <Date>            <Author>          	<Description>
    11/06/2014        Ana Escrich      	 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public void next() { 
  		try{
	    	addMonth(1);
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
	  	}catch (exception Exc){
	   		BI_LogHelper.generate_BILog('PCA_CustomCalendarController.next', 'Portal Platino', Exc, 'Class');
	    }
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Descreases one month to the index of calendar
    
    History:
    
    <Date>            <Author>          	<Description>
    11/06/2014        Ana Escrich      	 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public void prev() { 
	  	try{
	    	addMonth(-1);
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
	  	}catch (exception Exc){
	   		BI_LogHelper.generate_BILog('PCA_CustomCalendarController.prev', 'Portal Platino', Exc, 'Class');
	    }
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Retrieves weeks of current month with respective events
    
    History:
    
    <Date>            <Author>          	<Description>
    12/06/2014        Ana Escrich      	 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public List<PCA_CustomCalendarMonth.Week> getWeeks() {  
	  	try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
		    system.assert(pCA_CustomCalendarMonth!=null,'month is null');
		    List<PCA_CustomCalendarMonth.Week> weeks1 = pCA_CustomCalendarMonth.getWeeks();
		    system.debug('weeks1: '+weeks1);
		    for(PCA_CustomCalendarMonth.Week w: weeks1){
		    	system.debug('days1: '+ w.days);
		    }
		    return pCA_CustomCalendarMonth.getWeeks();
	  	}catch (exception Exc){
	   		BI_LogHelper.generate_BILog('PCA_CustomCalendarController.getWeeks', 'Portal Platino', Exc, 'Class');
	   		return null;
	    }
  	}
  
  	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Retrieves the events of the month 
    
    History:
    
    <Date>            <Author>          	<Description>
    11/06/2014        Ana Escrich      	 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public PCA_CustomCalendarMonth getPCA_CustomCalendarMonth() {
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			return pCA_CustomCalendarMonth;
		}catch (exception Exc){
	   		BI_LogHelper.generate_BILog('PCA_CustomCalendarController.getPCA_CustomCalendarMonth', 'Portal Platino', Exc, 'Class');
	   		return null;
	    } 
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       Accenture NEA
     Description:   Set events to calendar based on the input date 
     
     History:
     
     <Date>            <Author>          	<Description>
     21/04/2017        Jose Miguel Fierro 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public String[] getWeekdayNames() {
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
		    system.assert(pCA_CustomCalendarMonth!=null,'month is null');
		    //String[] weekdayName = pCA_CustomCalendarMonth.getWeekdayNames();
		    //system.debug('weekstart: '+weekdayName[0]);
		    return new String[] {
				Label.PCA_Domingo, Label.PCA_Lunes, Label.PCA_Martes, Label.PCA_Miercoles, Label.PCA_Jueves, Label.PCA_Viernes, LAbel.PCA_Sabado
			};
	  	}catch (exception Exc){
	   		BI_LogHelper.generate_BILog('PCA_CustomCalendarController.getWeekdayNames', 'Portal Platino', Exc, 'Class');
	   		return null;
	    }
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Set events to calendar based on the input date 
    
    History:
    
    <Date>            <Author>          	<Description>
    11/06/2014        Ana Escrich      	 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private void setPCA_CustomCalendarMonth(Date d) { 
	  	try{
		    pCA_CustomCalendarMonth = new PCA_CustomCalendarMonth(d);  
		    system.assert(pCA_CustomCalendarMonth != null); 
		
		    Date[] day = pCA_CustomCalendarMonth.getValidDateRange();  // gather events that fall in this month
		    System.debug('## Query conditions: day[0]: ' + day[0] + '. day[1]: ' + day[1]+ '. this.idUserCalendar:'+this.idUserCalendar);
			
			//Ana Escrich
			List<BI_Contact_Customer_Portal__c> customerContacts = [SELECT BI_Activo__c,BI_Cliente__c,BI_Contacto__c,BI_User__c,Id FROM BI_Contact_Customer_Portal__c where BI_User__c=:this.idUserCalendar];
		    List<Id> contacts = new List<Id>();
		    for(BI_Contact_Customer_Portal__c item: customerContacts){
		    	contacts.add(item.BI_Contacto__c);
		    }
		    String idUser = (System.currentPageReference().getParameters().get('idUser')!=null)?String.escapeSingleQuotes(System.currentPageReference().getParameters().get('idUser')):null;
			if(idUser!=null){
				// Calendar of other user 
				listEvent = new List<Event>();
				listEvent = [Select StartDateTime, IsVisibleInSelfService, subject, description, id, EndDateTime,CreatedById,OwnerId, BI_ParentId__c From Event where OwnerId = :this.idUserCalendar];
				wrapperList = new List<eventWrapper>();
				List<Id> eventIds = new List<Id>();
				for(Event item: listEvent){
					eventWrapper evW;
					if(item.createdById ==usuarioPortal.Id){
						evW = new eventWrapper(item.subject, item.Description, item.StartDateTime, item.EndDateTime, true);
						evW.setEditable(true);
						evW.setVisible(true);
					}
					else{
						evW = new eventWrapper(item.subject, item.Description, item.StartDateTime, item.EndDateTime, false);
						evW.setEditable(false);
						evW.setVisible(false);
					}
		        	evW.setEventId(item.Id);
		        	wrapperList.add(evW);
					eventIds.add(item.Id);
				}
				system.debug('wrapperList: '+wrapperList);       
			
			}else{// portal user Events
				system.debug('contacts: '+contacts);
				listEvent = new List<Event>();
			    List<EventRelation> relationEvents = [SELECT EventId,Id,RelationId FROM EventRelation WHERE RelationId in :contacts];
			    system.debug('relationEvents: '+relationEvents);
			    List<Id> eventRelatedIds = new List<Id>();
			    for(EventRelation evR: relationEvents){
			    	eventRelatedIds.add(evR.EventId);
			    }
			    //listEvent = [Select StartDateTime, IsVisibleInSelfService, subject, description, id, EndDateTime,CreatedById,OwnerId From Event where OwnerId = :this.idUserCalendar or Id in :eventRelatedIds or createdById = :this.idUserCalendar];
			    Id accId = BI_AccountHelper.getCurrentAccountId();
		        Account acc = [select Id, OwnerId from Account where Id=:accId limit 1];
		        if(acc!=null){
		        	listEvent = [Select StartDateTime, IsVisibleInSelfService, subject, description, id, EndDateTime,CreatedById,OwnerId, BI_ParentId__c From Event where OwnerId = :acc.OwnerId and createdById = :this.idUserCalendar];
		        }
		        else{
			    	listEvent = [Select StartDateTime, IsVisibleInSelfService, subject, description, id, EndDateTime,CreatedById,OwnerId, BI_ParentId__c From Event where createdById = :this.idUserCalendar];//  or createdById = :this.idUserCalendar];
		        }
		        wrapperList = new List<eventWrapper>();
		        Set<Id> idEvents = new Set<Id>();
		        for(Event item: listEvent){
		        	if(!idEvents.contains(item.BI_ParentId__c)){
			        	eventWrapper evW = new eventWrapper(item.subject, item.Description, item.StartDateTime, item.EndDateTime, true);
			        	evW.setEventId(item.Id);
			        	evW.setVisible(true);
			        	wrapperList.add(evW);
			        	evW.setEditable(true);
			        	idEvents.add(item.Id);
		        	}
		        }
		        system.debug('idEvents: '+idEvents);
		        listEvent = [Select StartDateTime, IsVisibleInSelfService, subject, description, id, EndDateTime, CreatedById, OwnerId, BI_ParentId__c From Event where Id in :eventRelatedIds ];
		        for (Event item: listEvent){
		        	if(!idEvents.contains(item.BI_ParentId__c)){
			        	eventWrapper evW = new eventWrapper(item.subject, item.Description, item.StartDateTime, item.EndDateTime, false);
			        	evW.setEventId(item.Id);
			        	evW.setVisible(true);
			        	wrapperList.add(evW);
			        	evW.setEditable(false);
			        	idEvents.add(item.Id);
		        	}
		        }
		        system.debug('idEvents1: '+idEvents);
		 		system.debug(wrapperList);     
			}
			
			if(wrapperList!=null && wrapperList.size() > 0)
		    	pCA_CustomCalendarMonth.setEventsStd(wrapperList);  // merge those events into the month class
		    	
		    	
		    system.debug('events: '+events);
	  	}catch (exception Exc){
	   		BI_LogHelper.generate_BILog('PCA_CustomCalendarController.setPCA_CustomCalendarMonth', 'Portal Platino', Exc, 'Class');
	    }
	}
  
  	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Add new month to the calendar
    
    History:
    
    <Date>            <Author>          	<Description>
    11/06/2014        Ana Escrich      	 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private void addMonth(Integer value) { 
	  	try{
		    Date day = pCA_CustomCalendarMonth.getFirstDate();
		    System.debug('## PCA_CustomCalendarController:addMonth:Date d: ' + day);
		    day = day.addMonths(value);
		    System.debug('## PCA_CustomCalendarController:addMonth:Date d after addMonths: ' + day);
		    setPCA_CustomCalendarMonth(day);
	  	}catch(exception Exc){
		   		BI_LogHelper.generate_BILog('PCA_CustomCalendarController.addMonth', 'Portal Platino', Exc, 'Class');
		}
	}

  


 
}