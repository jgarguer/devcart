/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       09-Abr-2015     Javier Tibamoza              Creación de la Clase Prueba
    1.1       19-08-2015      OJCB                         Subir cobertura
    1.1       2015-08-19      Jeisson Rojas (JR)           Cambio realizado a la Query de Usuarios del campo Country por Pais__c
    1.2       20-Nov-2015     Carlos Carvajal              Ajuste cambio de plan
    1.3       26-Ene-2017     Pedro Párraga                Added several things
    1.4       23-02-2017      Jaime Regidor                Adding Catalog Country , Adding Contact Values
		13/03/2017	Marta Gonzalez(Everis)	   REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
*             20/09/2017      Antonio Mendivil Azagra      Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
              27/09/2017      Jaime Regidor                Add the mandatory fields on account creation: BI_Subsector__c and BI_Sector__c
****************************************************************************************************/
@isTest
private class BI_COL_SelectClassificationServices_tst {
    public static BI_COL_ChangeMassiveBranch_ctr objCMassBranch;
    public static BI_COL_Modificacion_de_Servicio__c objMS;
    public static Account objCuenta;
    public static Account objCuenta2;
    public static Contact objContacto;
    public static Opportunity objOportunidad;
    public static BI_COL_Anexos__c objAnexos;
    public static BI_COL_Descripcion_de_servicio__c objDesSer;
    public static Contract objContrato;
    public static Campaign objCampana;
    public static BI_Col_Ciudades__c objCiudad;
    public static BI_Sede__c objSede;
    public static User objUsuario;
    public static BI_Punto_de_instalacion__c objPuntosInsta;
    public static BI_Log__c objBiLog;

    public static list <Profile> lstPerfil;
    public static list <UserRole> lstRoles;
    public static list <Campaign> lstCampana;
    public static list<BI_COL_manage_cons__c> lstManege;
    public static List<RecordType> lstRecTyp;
    public static List<User> lstUsuarios;
    public static List<BI_COL_Modificacion_de_Servicio__c> lstMS;
    public static List<BI_COL_Descripcion_de_servicio__c> lstDS;
    public static List<BI_COL_SessionContract_ctr.wrapperServicios> lstWraperServ;
    public static list<BI_Log__c> lstLog = new list <BI_Log__c>();
    public static NE__OrderItem__c NEInsert2;

    public static void crearData() {
        //Cuentas
        objCuenta                                       = new Account();
        objCuenta.Name                                  = 'prueba';
        objCuenta.BI_Country__c                         = 'Colombia';
        objCuenta.TGS_Region__c                         = 'América';
        objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
        objCuenta.CurrencyIsoCode                       = 'COP';
        objCuenta.BI_Fraude__c                          = false;
        objCuenta.BI_Segment__c                         = 'test';
        objCuenta.BI_Subsegment_Regional__c             = 'test';
        objCuenta.BI_Territory__c                       = 'test';
        insert objCuenta;
        system.debug('datos Cuenta ' + objCuenta);

        objCuenta2                                       = new Account();
        objCuenta2.Name                                  = 'prueba';
        objCuenta2.BI_Country__c                         = 'Colombia';
        objCuenta2.TGS_Region__c                         = 'América';
        objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
        objCuenta2.CurrencyIsoCode                       = 'COP';
        objCuenta2.BI_Fraude__c                          = true;
        objCuenta2.BI_Segment__c                         = 'test';
        objCuenta2.BI_Subsegment_Regional__c             = 'test';
        objCuenta2.BI_Territory__c                       = 'test';
        insert objCuenta2;

        //Ciudad
        objCiudad = new BI_Col_Ciudades__c ();
        objCiudad.Name                      = 'Test City';
        objCiudad.BI_COL_Pais__c            = 'Test Country';
        objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
        insert objCiudad;

        //Contactos
        objContacto                                     = new Contact();
        objContacto.LastName                            = 'Test';
        objContacto.BI_Country__c                       = 'Colombia';
        objContacto.CurrencyIsoCode                     = 'COP';
        objContacto.AccountId                           = objCuenta.Id;
        objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
        objContacto.Phone                               = '12345678';
        objContacto.MobilePhone                         = '3104785925';
        objContacto.Email                               = 'pruebas@pruebas1.com';
        objContacto.BI_COL_Direccion_oficina__c         = 'Dirprueba 34550';
        objContacto.BI_COL_Ciudad_Depto_contacto__c     = objCiudad.Id;
        objContacto.BI_Tipo_de_documento__c             = 'RUT';    
        objContacto.BI_Numero_de_documento__c           = '123456789';
        //REING-INI
        //objContacto.BI_Tipo_de_contacto__c          = 'Autorizado';
        objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
        objContacto.BI_Tipo_de_contacto__c          = 'General';
        objContacto.FirstName                            = 'TT';
        //REING_FIN
        Insert objContacto;

        //catalogo
        NE__Catalog__c objCatalogo                      = new NE__Catalog__c();
        objCatalogo.Name                                = 'prueba Catalogo';
        objCatalogo.BI_Country__c                       =  'Colombia';
        insert objCatalogo;

        //Campaña
        /*objCampana                                      = new Campaign();
        objCampana.Name                                 = 'Campaña Prueba';
        objCampana.BI_Catalogo__c                       = objCatalogo.Id;
        objCampana.BI_COL_Codigo_campana__c             = 'prueba1';
        insert objCampana;
        lstCampana                                      = new List<Campaign>();
        lstCampana                                      = [select Id, Name from Campaign where Id = : objCampana.Id];
        system.debug('datos Cuenta ' + lstCampana);*/

        //Tipo de registro
        lstRecTyp                                       = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
        system.debug('datos de FUN ' + lstRecTyp);

        //Contrato
        objContrato                                     = new Contract ();
        objContrato.AccountId                           = objCuenta.Id;
        objContrato.StartDate                           =  System.today().addDays(+5);
        objContrato.BI_COL_Formato_Tipo__c              = 'Contrato Marco';
        objContrato.BI_COL_Tipo_contrato__c             = 'Contrato Telefónica';
        objContrato.ContractTerm                        = 12;
        objContrato.BI_COL_Presupuesto_contrato__c      = 1000000;
        objContrato.BI_Indefinido__c                    = 'Si';
        objContrato.BI_COL_Cuantia_Indeterminada__c     = true;
        objContrato.BI_COL_Duracion_Indefinida__c       = true;
        objContrato.StartDate                           = System.today().addDays(+5);
        insert objContrato;
        System.debug('datos Contratos ===>' + objContrato);

        //FUN
        objAnexos                                       = new BI_COL_Anexos__c();
        objAnexos.Name                                  = 'FUN-0041414';
        objAnexos.RecordTypeId                          = lstRecTyp[0].Id;
        objAnexos.BI_COL_Contrato__c                    = objContrato.Id;
        insert objAnexos;
        System.debug('\n\n\n Sosalida objAnexos ' + objAnexos + '\n ====> objAnexos.Id ' + objAnexos.Id);

        //Configuracion Personalizada
        BI_COL_manage_cons__c objConPer                 = new BI_COL_manage_cons__c();
        objConPer.Name                                  = 'Viabilidades';
        objConPer.BI_COL_Numero_Viabilidad__c           = 46;
        objConPer.BI_COL_ConsProyecto__c                = 1;
        objConPer.BI_COL_ValorConsecutivo__c            = 1;
        lstManege                                       = new list<BI_COL_manage_cons__c >();
        lstManege.add(objConPer);
        insert lstManege;
        System.debug('======= configuracion personalizada ======= ' + lstManege);

        //Oportunidad
        objOportunidad                                          = new Opportunity();
        objOportunidad.Name                                     = 'prueba opp';
        objOportunidad.AccountId                                = objCuenta.Id;
        objOportunidad.BI_Country__c                            = 'Colombia';
        objOportunidad.CloseDate                                = System.today().addDays(+5);
        objOportunidad.StageName                                = 'F6 - Prospecting';
        objOportunidad.CurrencyIsoCode                          = 'COP';
        objOportunidad.Certa_SCP__contract_duration_months__c   = 12;
        objOportunidad.BI_Plazo_estimado_de_provision_dias__c   = 0 ;
        //objOportunidad.OwnerId                                  = lstUsuarios[0].id;
        insert objOportunidad;
        system.debug('Datos Oportunidad' + objOportunidad);
        list<Opportunity> lstOportunidad    = new list<Opportunity>();
        lstOportunidad                      = [select Id from Opportunity where Id = : objOportunidad.Id];
        system.debug('datos ListaOportunida ' + lstOportunidad);

        //Ciudad
        objCiudad = new BI_Col_Ciudades__c ();
        objCiudad.Name                      = 'Test City';
        objCiudad.BI_COL_Pais__c            = 'Test Country';
        objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
        insert objCiudad;
        System.debug('Datos Ciudad ===> ' + objSede);

        //Direccion
        objSede                                 = new BI_Sede__c();
        objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
        objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
        objSede.BI_Localidad__c                 = 'Test Local';
        objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
        objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
        objSede.BI_Country__c                   = 'Colombia';
        objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
        objSede.BI_Codigo_postal__c             = '12356';
        insert objSede;
        System.debug('Datos Sedes ===> ' + objSede);

        //Sede
        objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
        objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
        objPuntosInsta.BI_Sede__c          = objSede.Id;
        objPuntosInsta.BI_Contacto__c      = objContacto.id;
        objPuntosInsta.Name                 = 'Ptuebas sucursales';
        insert objPuntosInsta;
        System.debug('Datos Sucursales ===> ' + objPuntosInsta);


        NE__Product__c testProduct = new NE__Product__c(Name = Constants.PRODUCT_SMDM_INVENTORY, BI_COL_TipoRegistro__c = [select id from recordtype where sobjecttype = 'NE__Product__c' limit 1][0].Id);
        insert testProduct;
        NE__Order__c NEORDER =  new NE__Order__c();
        NEORDER.NE__OptyId__c = objOportunidad.Id;
        NEORDER.NE__OrderStatus__c = 'Active';
        insert NEORDER;

        NEInsert2  = new NE__OrderItem__c ();
        NEInsert2.NE__OrderId__c = NEORDER.id;
        NEInsert2.CurrencyIsoCode = 'USD';
        NEInsert2.NE__OneTimeFeeOv__c = 100;
        NEInsert2.NE__RecurringChargeOv__c = 100;
        NEInsert2.Fecha_de_reasignaci_n_a_factura__c = date.today();
        NEInsert2.NE__Qty__c = 1;
        NEInsert2.NE__ProdId__c = testProduct.Id;
        insert NEInsert2;
        //DS
        objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
        objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
        objDesSer.CurrencyIsoCode                           = 'COP';
        objDesSer.BI_COL_Producto_Telefonica__c             = NEInsert2.id;
        insert objDesSer;
        System.debug('Data DS ====> ' + objDesSer);
        System.debug('Data DS ====> ' + objDesSer.Name);
        lstDS   = [select id, name , BI_COL_Autoconsumo__c, BI_COL_Producto_Telefonica__c from BI_COL_Descripcion_de_servicio__c];
        System.debug('Data lista DS ====> ' + lstDS);

        //MS
        objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
        objMS.BI_COL_FUN__c                         = objAnexos.Id;
        objMS.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
        objMS.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
        objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
        objMS.BI_COL_Bloqueado__c                   = false;
        objMS.BI_COL_Estado__c                      = 'Activa';//label.BI_COL_lblActiva;
        objMS.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
        objMs.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
        objMS.RecordtypeId                          = [select id from recordType where SobjectType = 'BI_COL_Modificacion_de_Servicio__c' limit 1][0].Id;
        insert objMS;
        system.debug('Data objMs ===>' + objMs);
        lstMS = [select Name, BI_COL_Codigo_unico_servicio__c, BI_COL_Producto__r.Name, BI_COL_Descripcion_Referencia__c,
                         BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Sucursal_de_Facturacion__c, BI_COL_Codigo_unico_servicio__r.BI_COL_Sede_Destino__c,
                         BI_COL_Sucursal_Origen__c, /*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c, BI_COL_Clasificacion_Servicio__c, BI_COL_Oportunidad__c, RecordtypeId 
                 from BI_COL_Modificacion_de_Servicio__c
                ]; //10/11/2016 ANRC: Se comentó el campo antiguo y se adiciono nuevo campo davox1
        System.debug('datos lista MS ===> ' + lstMS);

        objBiLog                                            = new BI_Log__c();
        objBiLog.BI_COL_Informacion_recibida__c                     = 'OK, Respuesta Procesada';
        //objBiLog.BI_COL_Contacto__c                                   = lstContacts[0].Id;
        objBiLog.BI_COL_Estado__c                                       = 'FALLIDO';
        objBiLog.BI_COL_Interfaz__c                                 = 'NOTIFICACIONES';
        objBiLog.BI_COL_Informacion_Enviada__c                      = 'INFO Enviada';
        lstLog.add(objBiLog);
        insert lstLog;

        // }
    }
    @isTest static void test_method_one() {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) {
            // Implement test code
            crearData();
            Test.startTest();
            Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
            ApexPages.StandardController objStandardController;
            ApexPages.currentPage().getParameters().put('IdOpp', objOportunidad.id);
            BI_COL_SelectClassificationServices_ctr objController = new BI_COL_SelectClassificationServices_ctr(objStandardController);
            System.debug(objController.mWrapper);
            System.debug(objController.mapRT);
            System.debug(objController.mapRTMS);
            System.debug(objController.OrderItemLst);
            System.debug(objController.lstModificacion_de_Servicio);
            System.debug(objController.objModificacion_de_Servicio);

            System.debug(objController.IdOpp);
            System.debug(objController.sFiltro);
            System.debug(objController.sValorPopUp);
            System.debug(objController.sNameValorPopUp);
            System.debug(objController.sIdSede);
            System.debug(objController.sIdwrp);
            System.debug(objController.sCurrency);
            System.debug(objController.sOppName);
            System.debug(objController.sDMeses);
            System.debug(objController.oOpp);
            Test.stopTest();
        }
    }

    @isTest static void fnReLoadWrapper() {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) {
            // Implement test code
            crearData();
            Test.startTest();
            Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
            ApexPages.StandardController objStandardController;
            ApexPages.currentPage().getParameters().put('IdOpp', objOportunidad.id);
            BI_COL_SelectClassificationServices_ctr objController = new BI_COL_SelectClassificationServices_ctr(objStandardController);
            objController.fnReLoadWrapper();
            Test.stopTest();
        }
    }
    @isTest static void fnSave() {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) {
            // Implement test code
            crearData();
            Test.startTest();
            Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
            ApexPages.StandardController objStandardController;
            ApexPages.currentPage().getParameters().put('IdOpp', objOportunidad.id);
            BI_COL_SelectClassificationServices_ctr objController = new BI_COL_SelectClassificationServices_ctr(objStandardController);

            BI_COL_SelectClassificationServices_ctr.wrapperTable oWrapper = new BI_COL_SelectClassificationServices_ctr.wrapperTable( NEInsert2, 1 );
            oWrapper.oMS.BI_COL_Oportunidad__c = objOportunidad.Id;
            oWrapper.oMS.CurrencyIsoCode = '';
            oWrapper.sDivisa = NEInsert2.CurrencyIsoCode;
            oWrapper.oMS.BI_COL_Duracion_meses__c = null;
            oWrapper.oMS.BI_COL_Clasificacion_Servicio__c = 'ALTA';
            oWrapper.blVerLupaDS = false;
            oWrapper.blValEsDemo = NEInsert2.NE__ProdId__r.BI_COL_Es_Demo__c;

            objController.lstTable.add( oWrapper );

            objController.fnSave();
            Test.stopTest();
        }
    }

    @isTest static void fnValidarSucursalCliente() {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) {
            // Implement test code
            crearData();
            Test.startTest();
            Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
            ApexPages.StandardController objStandardController;
            ApexPages.currentPage().getParameters().put('IdOpp', objOportunidad.id);
            BI_COL_SelectClassificationServices_ctr objController = new BI_COL_SelectClassificationServices_ctr(objStandardController);
            objController.fnValidarSucursalCliente();
            Test.stopTest();
        }
    }

    @isTest static void fnCreateDS() {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) {
            // Implement test code
            crearData();
            Test.startTest();
            Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
            ApexPages.StandardController objStandardController;
            ApexPages.currentPage().getParameters().put('IdOpp', objOportunidad.id);
            BI_COL_SelectClassificationServices_ctr objController = new BI_COL_SelectClassificationServices_ctr(objStandardController);
            list<BI_COL_Modificacion_de_Servicio__c> lstMser = new list<BI_COL_Modificacion_de_Servicio__c>();
            objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
            objMS.BI_COL_FUN__c                         = objAnexos.Id;
            objMS.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objMS.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
            objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
            objMS.BI_COL_Bloqueado__c                   = false;
            objMS.BI_COL_Estado__c                      = 'Activa';//label.BI_COL_lblActiva;
            objMS.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objMs.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            objMs.RecordtypeId                          = [select id from recordType where SobjectType = 'BI_COL_Modificacion_de_Servicio__c' and name = 'BI MS Aplicaciones' limit 1][0].Id;
            objMs.BI_COL_Producto__c                    = NEInsert2.Id;

            BI_COL_Modificacion_de_Servicio__c objMS1   = new BI_COL_Modificacion_de_Servicio__c();
            objMS1.BI_COL_FUN__c                         = objAnexos.Id;
            objMS1.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objMS1.BI_COL_Clasificacion_Servicio__c      = 'SERVICIO INGENIERIA';//'Cambios servicio alta';
            objMS1.BI_COL_Oportunidad__c                 = objOportunidad.Id;
            objMS1.BI_COL_Bloqueado__c                   = false;
            objMS1.BI_COL_Estado__c                      = 'Pendiente';
            objMS1.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objMs1.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            objMs1.RecordtypeId                          = [select id from recordType where SobjectType = 'BI_COL_Modificacion_de_Servicio__c' and name = 'BI MS Aplicaciones' limit 1][0].Id;
            objMs1.BI_COL_Producto__c                    = NEInsert2.Id;
            lstMser.add(objMS);
            lstMser.add(objMS1);
            objController.fnCreateDS(lstMser);
            objController.fnCancel();
            BI_COL_SelectClassificationServices_ctr.EstadoMS(objMS);
            objController.cerradasPerdidasMSDS();
            System.debug(objController.asignValuePopUp());
            objController.fnReLoadWrapper();
            set<String> lstKey = objController.mWrapper.keySet();
            String key = '';
            for (String lkey : lstKey)
                key = lkey;
            ApexPages.currentPage().getParameters().put('sIdwrp', key);
            System.debug(objController.fnUpdateTable());
            System.debug(objController.asignValuePopUp());
            System.debug(objController.fnReplicarValor());
            objController.sDMeses = '3';
            objController.objModificacion_de_Servicio.BI_COL_Sucursal_Origen__c = objPuntosInsta.Id;
            objController.fnReplicarValor();
            System.debug(objController.fnEsCambio(''));
            Test.stopTest();
        }

    }
}