/****************************************************************************************************
	Información general
	-------------------
	author: Javier Tibamoza Cubillos
	company: Avanxo Colombia
	Project: Implementación Salesforce
	Customer: TELEFONICA
	Description: 
	
	Information about changes (versions)
	-------------------------------------
	Number    Dates           Author                       Description
	------    --------        --------------------------   -----------
	1.0       24-Abr-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
global class BI_COL_Response_Processing_Viability_ws
{

	public static Boolean isTestMethod = false;
	
	webservice static String procesarRespuestasViatec( String ds, String nroViabilidad, String consecutivoViabilidad, String parametros, 
		String  MedioVendido, String medioDestino, String respuestaDestino )
	{
		parametros += ' ';
		respuestaDestino += '.';
		String respuestaServicio = '';
		String solicitudServicio = '';
		String consecutivoConfiguracion = '';
		String codigoRespuesta = '';
		
		/*Fix bug eHelp 01520202 - Micah Burgos*/

		/*WHERE   BI_COL_Codigo_unico_servicio__r.Name = :ds */

		List<BI_COL_Viabilidad_Tecnica__c> lst_vt = [SELECT Id,Name,BI_COL_Modificacion_de_Servicio__c FROM BI_COL_Viabilidad_Tecnica__c WHERE Name = :consecutivoViabilidad];
		
		List<BI_COL_Modificacion_de_Servicio__c> listMS =  new List<BI_COL_Modificacion_de_Servicio__c>();

		if(!lst_vt.isEmpty() && lst_vt[0].BI_COL_Modificacion_de_Servicio__c != null){

			listMS = [
			SELECT  Id, Name 
			FROM    BI_COL_Modificacion_de_Servicio__c
				WHERE Id = :lst_vt[0].BI_COL_Modificacion_de_Servicio__c
			AND     BI_COL_Estado__c = 'Pendiente'
			AND     IsDeleted = false
			LIMIT 1 ];
				/*WHERE   BI_COL_Codigo_unico_servicio__r.Name = :ds */

		}else{

			BI_Log__c log_error = new BI_Log__c(
				BI_Asunto__c = 'BI_COL_Response_Processing_Viability_ws.procesarRespuestasViatec',
				BI_Bloque_funcional__c = 'BI_EN COL',
				BI_Descripcion__c = 'No se encuentra viabilidad (lst_vt) con autonumerico recibido [' + consecutivoViabilidad + '] o la viabilidad no tiene asociada MS',
				BI_COL_Tipo_Transaccion__c = 'RESPUESTA VIABILIDAD TRS'
			);
			insert log_error;
		}

		/*End Fix*/
		
		if( listMS.size() > 0 )
		{
			//Obtenemos el objeto MS
			BI_COL_Modificacion_de_Servicio__c objMS = listMS[0];
			// dgrajales 09/03/2012 CC actualizar medio vendido
			if( MedioVendido != null && MedioVendido != '' )
			{
				objMS.BI_COL_Medio_Vendido__c = MedioVendido;
				update objMS;
			}
			//Creamos un log de transacciones
			BI_Log__c bitacoraDescripcion = new BI_Log__c();
			
			bitacoraDescripcion.Descripcion__c = 'RTA VIABILIDADES TRS';
			bitacoraDescripcion.BI_COL_Modificacion_Servicio__c = objMS.Id; 
			bitacoraDescripcion.BI_COL_Estado__c = 'Pendiente';
			bitacoraDescripcion.Descripcion__c = objMS.Name;
			bitacoraDescripcion.BI_COL_Tipo_Transaccion__c = 'RESPUESTA VIABILIDAD TRS';
			bitacoraDescripcion.BI_Descripcion__c = 'Los parametros recibidos fueron: ' + parametros + '\n Medio Vendido: ' + MedioVendido + ' \n medio Destino: ' + medioDestino + '\n respuesta Destino: ' + respuestaDestino;
			bitacoraDescripcion.BI_COL_Informacion_recibida__c = '';
			
			//Obtenemos los campos de la configuración personalizada
			List<BI_COL_Configuracion_campos_interfases__c> consultaConfiguracionInterfaz =  [
				SELECT  Id, BI_COL_Calificador_Texto__c, BI_COL_Descripcion__c, BI_COL_Linea__c, Name,  
						BI_COL_Segmento__c, BI_COL_Separador__c, BI_COL_Separador_Decimal__c 
				FROM    BI_COL_Configuracion_campos_interfases__c 
				WHERE   BI_COL_Interfaz__c = 'TRS-VIATEC' 
				AND     BI_COL_Tipo_transaccion__c = 'RESPUESTA VIABILIDAD TECNICA'
				AND     BI_COL_Direccion__c = 'ENTRANTE' 
				ORDER BY CreatedDate DESC
				LIMIT 1 ];
			
			Integer posicionCampo = 0;
			
			Boolean errorInterfaz = false;
			
			if( consultaConfiguracionInterfaz.size() > 0 )
			{
				BI_COL_Configuracion_campos_interfases__c configuracionInterfaz = consultaConfiguracionInterfaz.get(0);
				
				String calificadorTexto = configuracionInterfaz.BI_COL_Calificador_Texto__c;
				consecutivoConfiguracion = configuracionInterfaz.Name;
				String caracterSeparador = configuracionInterfaz.BI_COL_Separador__c;
				String separadorDecimal = configuracionInterfaz.BI_COL_Separador_Decimal__c;
				
				List<BI_COL_Configuracion_campos_interfases__c> camposConfiguracionProducto =  [
					SELECT  BI_COL_Convertir_mayuscula__c, BI_COL_Decimales__c, BI_COL_Formato_Campo__c, BI_COL_Longitud__c,
						BI_COL_Configuracion_Interfaz__r.BI_COL_Segmento__c, BI_COL_Configuracion_Interfaz__r.BI_COL_Linea__c,
						BI_COL_Configuracion_Interfaz__r.BI_COL_Descripcion__c, Name, BI_COL_Nombre_campo__c, BI_COL_Permite_nulos__c,
						BI_COL_Tipo_campo__c, BI_COL_Valor_predeterminado__c 
					FROM BI_COL_Configuracion_campos_interfases__c 
					WHERE BI_COL_Configuracion_Interfaz__c = :configuracionInterfaz.Id
					AND IsDeleted = false
					ORDER BY BI_COL_Orden__c ASC, CreatedDate ASC ];
				
				String camposConsulta = ''; 
					
				if( camposConfiguracionProducto.size() > 0 )
				{
					String[] cadenaSeparada = parametros.split('\\' + caracterSeparador);
					
					if( cadenaSeparada.size() == camposConfiguracionProducto.size() )
					{                                                                                       
						for( posicionCampo = 0; posicionCampo < camposConfiguracionProducto.size(); posicionCampo++ )
						{
							BI_COL_Configuracion_campos_interfases__c configuracionCampo = camposConfiguracionProducto[posicionCampo];
							
							String campoConfigurado = configuracionCampo.BI_COL_Nombre_campo__c;
							
							if (camposConsulta == '')
								camposConsulta = campoConfigurado;
							else
								if( camposConsulta.indexOf(campoConfigurado) == -1 )
									camposConsulta = camposConsulta + ', ' + campoConfigurado;
						}
						String consultaSOQL = 'SELECT ';
						consultaSOQL = consultaSOQL + 'Id, Name, ' + camposConsulta + ' ';
						consultaSOQL = consultaSOQL + 'FROM ';
						consultaSOQL = consultaSOQL + 'BI_COL_Viabilidad_Tecnica__c ';
						consultaSOQL = consultaSOQL + 'WHERE ';
						consultaSOQL = consultaSOQL + 'BI_COL_Modificacion_de_Servicio__c = \'' + objMS.Id + '\' ';
						consultaSOQL = consultaSOQL + 'AND BI_COL_Numero_viabilidad__c = ' + nroViabilidad + ' ';
						consultaSOQL = consultaSOQL + 'AND Name = \'' + consecutivoViabilidad + '\' ';                      
						consultaSOQL = consultaSOQL + 'AND IsDeleted = false ';
						consultaSOQL = consultaSOQL + 'LIMIT 1';
						System.debug('\n\n consultaSOQL= '+consultaSOQL+' \n\n');
						List<BI_COL_Viabilidad_Tecnica__c> consultaSolicitudes = Database.query(consultaSOQL);
							
						if( consultaSolicitudes.size() > 0 )
						{
							BI_COL_Viabilidad_Tecnica__c solicitudDescripcion = consultaSolicitudes.get(0);
							
							Double valorNumero = 0;
							Datetime valorFechaHora = null;
							Date valorFecha = null;
							Boolean valorLogico = false;
							
							for( posicionCampo = 0; posicionCampo < camposConfiguracionProducto.size(); posicionCampo++ )
							{
								BI_COL_Configuracion_campos_interfases__c configuracionCampo = camposConfiguracionProducto[posicionCampo];
								Boolean convertirMayuscula = configuracionCampo.BI_COL_Convertir_mayuscula__c;
								Double numeroDecimales = configuracionCampo.BI_COL_Decimales__c;
								String formatoCampo = configuracionCampo.BI_COL_Formato_Campo__c;
								Double longitudCampo = configuracionCampo.BI_COL_Longitud__c;
								String campoConfigurado = configuracionCampo.BI_COL_Nombre_campo__c;
								Boolean permiteNulos = configuracionCampo.BI_COL_Permite_nulos__c;
								String tipoCampo = configuracionCampo.BI_COL_Tipo_campo__c;
								String valorPredeterminado = configuracionCampo.BI_COL_Valor_predeterminado__c;                         
								String valorCampo = cadenaSeparada[posicionCampo];
								if( (valorCampo == null) || (valorCampo == 'null')) valorCampo = '';
								if( (calificadorTexto != null) && (calificadorTexto != '')) valorCampo = valorCampo.replace(calificadorTexto, '');
								if( (permiteNulos == false) && (valorCampo == '') && (valorPredeterminado != '') && (valorPredeterminado != null)) valorCampo = valorPredeterminado;
								if( valorCampo != '')
								{
									if( tipoCampo == 'Texto' )
									{
										if (convertirMayuscula == true) valorCampo = valorCampo.toUpperCase();
										
										if ((longitudCampo != null) && ((longitudCampo.intValue() - 1) <= valorCampo.length())) valorCampo = valorCampo.substring(0, longitudCampo.intValue());
										
										solicitudDescripcion.put(campoConfigurado, valorCampo);
									}
									else if( tipoCampo == 'Numérico' )
									{
										if( (numeroDecimales != null) && (formatoCampo != '') && (formatoCampo != null) && (separadorDecimal != '')     && (separadorDecimal != null))  
											valorCampo = formatoNumero( Decimal.valueOf(valorCampo), formatoCampo, numeroDecimales.intValue(), separadorDecimal);
										valorNumero = Double.valueOf(valorCampo);
										solicitudDescripcion.put(campoConfigurado, valorNumero);
									}
									else if( tipoCampo == 'Fecha')
									{
										if ((formatoCampo != null) && (formatoCampo != ''))
										{
											
											if((valorPredeterminado == 'Hoy') || (valorPredeterminado == 'Ayer') || (valorPredeterminado == 'Mañana') || (valorPredeterminado == 'Año actual') || (valorPredeterminado == 'Mes actual') || (valorPredeterminado == 'Ahora'))
											{   
												valorCampo = formatoFecha(System.now(), valorPredeterminado, formatoCampo);
											}
											
											valorCampo = convertirFechaSalesforce(valorCampo, formatoCampo);
											
											if (formatoCampo.indexOf('HH:mm:ss') > -1)
											{
												valorFechaHora = Datetime.valueOf(valorCampo);
												solicitudDescripcion.put(campoConfigurado, valorFechaHora);
											}
											else
											{
												valorFecha = Date.valueOf(valorCampo);
												solicitudDescripcion.put(campoConfigurado, valorFecha);
											}
										}                                           
									}
									else if (tipoCampo == 'Lógico')
									{
										if( formatoCampo == '1 : 0' )
										{
											if( valorCampo == '1' )
												valorLogico = true;
											else
												valorLogico = false;
										}
										else if( formatoCampo == 'S : N' )
										{
											if( valorCampo == 'S' )
												valorLogico = true;
											else
												valorLogico = false;
										}
										solicitudDescripcion.put(campoConfigurado, valorLogico);
									}
								}
							}
							update solicitudDescripcion;
							
							respuestaServicio = 'Solicitud de viabilidad recibida desde TRS-VIABILIDADES y actualizada en Salesforce.com\n\r' + 
												'Consecutivo configuración interfaz: ' + consecutivoConfiguracion + '\n\r' +
												'Interfaz: TRS-VIABILIDADES\n\r' +
												'Tipo transacción: RESPUESTA VIABILIDAD TECNICA\n\r' +
												'Dirección: ENTRANTE\n\r' +
												'Consecutivo solicitud viabilidad: ' + solicitudDescripcion.Name;
						
							solicitudServicio = 'Información envíada desde TRS-VIABILIDADES\n\r' +
												'Consecutivo descripción servicio: ' + objMS.Name + '\n\r' +
												'Consecutivo viabilidad: ' + consecutivoViabilidad + '\n\r' +
												'Cadena recibida: ' + parametros;
						
							codigoRespuesta = '1';
						
							bitacoraDescripcion.Descripcion__c = solicitudServicio;
							bitacoraDescripcion.BI_COL_Informacion_recibida__c = respuestaServicio;
							bitacoraDescripcion.BI_COL_Estado__c = 'Exitoso';
							insert bitacoraDescripcion;
						}
						else
						{
							codigoRespuesta = '2';
							
							respuestaServicio = 'No existe el número de viabilidad en Salesforce.com envíado desde TRS-VIABILIDADES\n\r' +
												'Consecutivo configuración interfaz: ' + consecutivoConfiguracion + '\n\r' + 
												'Interfaz: TRS-TRS-VIABILIDADES\n\r' + 
												'Tipo transacción: RESPUESTA VIABILIDAD TECNICA\n\r' +
												'Dirección: ENTRANTE\n\r';
							
							solicitudServicio = 'Información envíada desde TRS-VIABILIDADES\n\r' +
												'Consecutivo descripción servicio: ' + objMS.Name + '\n\r' +
												'Consecutivo viabilidad: ' + consecutivoViabilidad + '\n\r' +
												'Cadena recibida: ' + parametros;
							
							bitacoraDescripcion.Descripcion__c = solicitudServicio;
							bitacoraDescripcion.BI_COL_Informacion_recibida__c = respuestaServicio;
							bitacoraDescripcion.BI_COL_Estado__c = 'Fallido';
							
							insert bitacoraDescripcion;
						}
					}
					else
					{
						
						codigoRespuesta = '2';
						
						respuestaServicio = 'El número de campos envíados desde TRS-VIABILIDADES no coincide con el número de campos esperado en Salesforce.com Origen\n\r' +
											'Consecutivo configuración interfaz: ' + consecutivoConfiguracion + '\n\r' + 
											'Interfaz: TRS-VIATEC\n\r' + 
											'Tipo transacción: RESPUESTA VIABILIDAD TECNICA\n\r' +
											'Dirección: ENTRANTE\n\r';
						
						solicitudServicio = 'Información envíada desde TRS-VIABILIDADES\n\r' +
											'Consecutivo descripción servicio: ' + ds + '\n\r' +
											'Consecutivo viabilidad: ' + consecutivoViabilidad + '\n\r' +
											'Cadena recibida: ' + parametros;
						
						bitacoraDescripcion.Descripcion__c = solicitudServicio;
						bitacoraDescripcion.BI_COL_Informacion_recibida__c = respuestaServicio;
						bitacoraDescripcion.BI_COL_Estado__c = 'Fallido';
						insert bitacoraDescripcion;
					}
				}
				else
				{
					errorInterfaz = true;
				}
			}
			else
			{
				errorInterfaz = true;
			}
			
			Integer posicionCampo2 = 0;
			Boolean errorInterfaz2 = false;
			if(respuestaDestino != null && respuestaDestino != '')
			{
				List<BI_COL_Configuracion_campos_interfases__c> consultaConfiguracionInterfaz2 =  [
					SELECT  Id, BI_COL_Calificador_Texto__c, BI_COL_Descripcion__c, BI_COL_Linea__c, Name,
							BI_COL_Segmento__c, BI_COL_Separador__c, BI_COL_Separador_Decimal__c 
					FROM    BI_COL_Configuracion_campos_interfases__c 
					WHERE   BI_COL_Interfaz__c = 'TRS-VIATEC-DESTINO' 
					AND     BI_COL_Tipo_transaccion__c = 'RESPUESTA VIABILIDAD TECNICA'
					AND     BI_COL_Direccion__c = 'ENTRANTE' 
					ORDER BY CreatedDate DESC
					LIMIT 1 ];
				System.debug('\n\n Consulta primera linea 300 \n\n');  
				if( consultaConfiguracionInterfaz2.size() > 0 )
				{
					BI_COL_Configuracion_campos_interfases__c configuracionInterfaz2 = consultaConfiguracionInterfaz2.get(0);
					
					String calificadorTexto = configuracionInterfaz2.BI_COL_Calificador_Texto__c;
					consecutivoConfiguracion = configuracionInterfaz2.Name;
					String caracterSeparador = configuracionInterfaz2.BI_COL_Separador__c;
					String separadorDecimal = configuracionInterfaz2.BI_COL_Separador_Decimal__c;
					
					List<BI_COL_Configuracion_campos_interfases__c> camposConfiguracionProducto2 = [
						SELECT  BI_COL_Convertir_mayuscula__c, BI_COL_Decimales__c, BI_COL_Formato_Campo__c, BI_COL_Longitud__c,
								BI_COL_Configuracion_Interfaz__r.BI_COL_Segmento__c, BI_COL_Configuracion_Interfaz__r.BI_COL_Linea__c, 
								BI_COL_Configuracion_Interfaz__r.BI_COL_Descripcion__c, Name, BI_COL_Nombre_campo__c, BI_COL_Permite_nulos__c,
								BI_COL_Tipo_campo__c, BI_COL_Valor_predeterminado__c 
						FROM    BI_COL_Configuracion_campos_interfases__c 
						WHERE   BI_COL_Configuracion_Interfaz__c = :configuracionInterfaz2.Id
						AND     IsDeleted = false
						ORDER BY BI_COL_Orden__c ASC, CreatedDate ASC ];
					
					String camposConsulta2 = '';    
						
					if( camposConfiguracionProducto2.size() > 0 )
					{
						String[] cadenaSeparada = respuestaDestino.split('\\' + caracterSeparador);
						
						if( cadenaSeparada.size() == camposConfiguracionProducto2.size() )
						{
							for( posicionCampo2 = 0; posicionCampo2 < camposConfiguracionProducto2.size(); posicionCampo2++ )
							{
								BI_COL_Configuracion_campos_interfases__c configuracionCampo = camposConfiguracionProducto2[posicionCampo2];
								
								String campoConfigurado = configuracionCampo.BI_COL_Nombre_campo__c;
	
								if (camposConsulta2 == '')
									camposConsulta2 = campoConfigurado;
								else
								{
									if (camposConsulta2.indexOf(campoConfigurado) == -1) camposConsulta2 = camposConsulta2 + ', ' + campoConfigurado;
								}
							}
							
							String consultaSOQL = 'SELECT ';
							consultaSOQL = consultaSOQL + 'Id, Name, ' + camposConsulta2 + ' ';
							consultaSOQL = consultaSOQL + 'FROM ';
							consultaSOQL = consultaSOQL + 'BI_COL_Viabilidad_Tecnica__c ';
							consultaSOQL = consultaSOQL + 'WHERE ';
							consultaSOQL = consultaSOQL + 'BI_COL_Modificacion_de_Servicio__c = \'' + objMS.Id + '\' ';
							consultaSOQL = consultaSOQL + 'AND BI_COL_Numero_viabilidad__c = ' + nroViabilidad + ' ';
							consultaSOQL = consultaSOQL + 'AND Name = \'' + consecutivoViabilidad + '\' ';                      
							consultaSOQL = consultaSOQL + 'AND IsDeleted = false ';
							consultaSOQL = consultaSOQL + 'LIMIT 1';
							System.debug('\n\n Consulta destino '+consultaSOQL+'\n\n');
							List<BI_COL_Viabilidad_Tecnica__c> consultaSolicitudes = Database.query(consultaSOQL);
								
							if( consultaSolicitudes.size() > 0 )
							{
								BI_COL_Viabilidad_Tecnica__c solicitudDescripcion = consultaSolicitudes.get(0);
								
								Double valorNumero = 0;
								Datetime valorFechaHora = null;
								Date valorFecha = null;
								Boolean valorLogico = false;
								
								for( posicionCampo2 = 0; posicionCampo2 < camposConfiguracionProducto2.size(); posicionCampo2++ )
								{
									BI_COL_Configuracion_campos_interfases__c configuracionCampo = camposConfiguracionProducto2[ posicionCampo2 ];
									
									Boolean convertirMayuscula = configuracionCampo.BI_COL_Convertir_mayuscula__c;
									Double numeroDecimales = configuracionCampo.BI_COL_Decimales__c;
									String formatoCampo = configuracionCampo.BI_COL_Formato_Campo__c;
									Double longitudCampo = configuracionCampo.BI_COL_Longitud__c;
									String campoConfigurado = configuracionCampo.BI_COL_Nombre_campo__c;
									Boolean permiteNulos = configuracionCampo.BI_COL_Permite_nulos__c;
									String tipoCampo = configuracionCampo.BI_COL_Tipo_campo__c;
									String valorPredeterminado = configuracionCampo.BI_COL_Valor_predeterminado__c;
									
									String valorCampo = cadenaSeparada[posicionCampo2];
									
									if( ( valorCampo == null) || (valorCampo == 'null') ) valorCampo = '';
									
									if( ( calificadorTexto != null ) && ( calificadorTexto != '' ) ) valorCampo = valorCampo.replace( calificadorTexto, '' );
									
		
									if( ( permiteNulos == false ) && ( valorCampo == '' ) && ( valorPredeterminado != '' ) && ( valorPredeterminado != null ) ) valorCampo = valorPredeterminado;
										
									if( valorCampo != '' )
									{
										if( tipoCampo == 'Texto' )
										{
											if( convertirMayuscula == true) valorCampo = valorCampo.toUpperCase();
											
											if( (longitudCampo != null) && ((longitudCampo.intValue() - 1) <= valorCampo.length() ) ) valorCampo = valorCampo.substring( 0, longitudCampo.intValue() );
											
											solicitudDescripcion.put(campoConfigurado, valorCampo);
										}
										else if (tipoCampo == 'Numérico')
										{
											if( (numeroDecimales != null) && (formatoCampo != '') && (formatoCampo != null) && (separadorDecimal != '') && (separadorDecimal != null))  
												valorCampo = formatoNumero(Decimal.valueOf(valorCampo), formatoCampo, numeroDecimales.intValue(), separadorDecimal);
											
											valorNumero = Double.valueOf( valorCampo );
											solicitudDescripcion.put( campoConfigurado, valorNumero );
										}
										else if( tipoCampo == 'Fecha' )
										{
											if( (formatoCampo != null) && (formatoCampo != ''))
											{
												if ((valorPredeterminado == 'Hoy') || (valorPredeterminado == 'Ayer') || (valorPredeterminado == 'Mañana') || (valorPredeterminado == 'Año actual') || (valorPredeterminado == 'Mes actual') || (valorPredeterminado == 'Ahora'))
													valorCampo = formatoFecha( System.now(), valorPredeterminado, formatoCampo);
												
												valorCampo = convertirFechaSalesforce(valorCampo, formatoCampo);
												
												if( formatoCampo.indexOf('HH:mm:ss') > -1 )
												{
													valorFechaHora = Datetime.valueOf(valorCampo);
													solicitudDescripcion.put(campoConfigurado, valorFechaHora);
												}
												else
												{
													valorFecha = Date.valueOf(valorCampo);
													solicitudDescripcion.put(campoConfigurado, valorFecha);
												}
											}                                           
										}
										else if( tipoCampo == 'Lógico' )
										{
											if( formatoCampo == '1 : 0' )
											{
												if( valorCampo == '1' )
													valorLogico = true;
												else
													valorLogico = false;
											}
											else if( formatoCampo == 'S : N')
											{
												if( valorCampo == 'S' )
													valorLogico = true;
												else
													valorLogico = false;
											}
											solicitudDescripcion.put( campoConfigurado, valorLogico );
										}
									}
								}
								update solicitudDescripcion;
								
								respuestaServicio = 'Solicitud de viabilidad recibida desde TRS-VIABILIDADES y actualizada en Salesforce.com\n\r' + 
													'Consecutivo configuración interfaz: ' + consecutivoConfiguracion + '\n\r' +
													'Interfaz: TRS-VIABILIDADES\n\r' +
													'Tipo transacción: RESPUESTA VIABILIDAD TECNICA\n\r' +
													'Dirección: ENTRANTE\n\r' +
													'Consecutivo solicitud viabilidad: ' + solicitudDescripcion.Name;
							
								solicitudServicio = 'Información envíada desde TRS-VIABILIDADES\n\r' +
													'Consecutivo descripción servicio: ' + objMS.Name + '\n\r' +
													'Consecutivo viabilidad: ' + consecutivoViabilidad + '\n\r' +
													'Cadena recibida: ' + respuestaDestino;
							
								codigoRespuesta = '1';
							
								bitacoraDescripcion.Descripcion__c = solicitudServicio;
								bitacoraDescripcion.BI_COL_Informacion_recibida__c = respuestaServicio;
								bitacoraDescripcion.BI_COL_Estado__c = 'Exitoso';
							
								update bitacoraDescripcion;
							}
							else
							{
								codigoRespuesta = '2';
								
								respuestaServicio =+ 'No existe el número de viabilidad en Salesforce.com envíado desde TRS-VIABILIDADES\n\r' +
													'Consecutivo configuración interfaz: ' + consecutivoConfiguracion + '\n\r' + 
													'Interfaz: TRS-TRS-VIABILIDADES\n\r' + 
													'Tipo transacción: RESPUESTA VIABILIDAD TECNICA\n\r' +
													'Dirección: ENTRANTE\n\r';
								
								solicitudServicio =+ 'Información envíada desde TRS-VIABILIDADES\n\r' +
													'Consecutivo descripción servicio: ' + objMS.Name + '\n\r' +
													'Consecutivo viabilidad: ' + consecutivoViabilidad + '\n\r' +
													'Cadena recibida: ' + respuestaDestino;
								
								bitacoraDescripcion.Descripcion__c = solicitudServicio;
								bitacoraDescripcion.BI_COL_Informacion_recibida__c = respuestaServicio;
								bitacoraDescripcion.BI_COL_Estado__c = 'Fallido';
								
								update bitacoraDescripcion;
							}
						}
						else
						{
							
							codigoRespuesta = '2';
							
							respuestaServicio = +'El número de campos envíados desde TRS-VIABILIDADES no coincide con el número de campos esperado en Salesforce.com Destino\n\r' +
												'Consecutivo configuración interfaz: ' + consecutivoConfiguracion + '\n\r' + 
												'Interfaz: TRS-VIATEC\n\r' + 
												'Tipo transacción: RESPUESTA VIABILIDAD TECNICA\n\r' +
												'Dirección: ENTRANTE\n\r';
							
							solicitudServicio =+ 'Información envíada desde TRS-VIABILIDADES\n\r' +
												'Consecutivo descripción servicio: ' + ds + '\n\r' +
												'Consecutivo viabilidad: ' + consecutivoViabilidad + '\n\r' +
												'Cadena recibida: ' + respuestaDestino;
							
							bitacoraDescripcion.Descripcion__c = solicitudServicio;
							bitacoraDescripcion.BI_COL_Informacion_recibida__c = respuestaServicio;
							bitacoraDescripcion.BI_COL_Estado__c = 'Fallido';
							
							update bitacoraDescripcion;
						}
					}
					else
						errorInterfaz2 = true;
				}
			}
			if( errorInterfaz || isTestMethod )
			{
				codigoRespuesta = '2';
				
				respuestaServicio = 'Revisar la configuración de la interfaz y los campos para recibir desde TRS-VIABILIDADES la respuesta de la viabilidad técnica\n\r' +
									'Interfaz: TRS-VIATEC\n\r' +
									'Tipo transacción: RESPUESTA VIABILIDAD TECNICA\n\r' +
									'Dirección: ENTRANTE';
				
				solicitudServicio = 'Información envíada desde TRS-VIABILIDADES\n\r' +
									'Consecutivo descripción servicio: ' + ds + '\n\r' +
									'Consecutivo viabilidad: ' + consecutivoViabilidad + '\n\r' +
									'Cadena recibida: ' + parametros;
				
				bitacoraDescripcion.Descripcion__c = solicitudServicio;
				bitacoraDescripcion.BI_COL_Informacion_recibida__c = respuestaServicio;
				bitacoraDescripcion.BI_COL_Estado__c = 'Fallido';
				
				try
				{
					update bitacoraDescripcion;
				}
				catch(Exception ex)
				{
					insert bitacoraDescripcion;
				}
			}
			
			if( errorInterfaz2 || isTestMethod )
			{
				codigoRespuesta = '2';
				
				respuestaServicio = 'Revisar la configuración de la interfaz y los campos para recibir desde TRS-VIABILIDADES la respuesta de la viabilidad técnica\n\r' +
									'Interfaz: TRS-VIATEC\n\r' +
									'Tipo transacción: RESPUESTA VIABILIDAD TECNICA\n\r' +
									'Dirección: ENTRANTE';
				
				solicitudServicio = 'Información envíada desde TRS-VIABILIDADES\n\r' +
									'Consecutivo descripción servicio: ' + ds + '\n\r' +
									'Consecutivo viabilidad: ' + consecutivoViabilidad + '\n\r' +
									'Cadena recibida: ' + respuestaDestino;
				
				bitacoraDescripcion.Descripcion__c = solicitudServicio;
				bitacoraDescripcion.BI_COL_Informacion_recibida__c = respuestaServicio;
				bitacoraDescripcion.BI_COL_Estado__c = 'Fallido';
				
				try
				{
					update bitacoraDescripcion;
				}
				catch(Exception ex)
				{
					insert bitacoraDescripcion;
				}
			}
		}
		else
		{
			codigoRespuesta = '2';
			respuestaServicio='NO EXISTE LA DS';
		}
		return armarRespuesta(codigoRespuesta, respuestaServicio);
	}
	
	public static String armarRespuesta( String Resultado, String MsgError )
	{
		return '<ActualizarInstalacionDSResponse>' +
			'<Resultado>' + Resultado + '</Resultado>' +
			'<MsgError>' + MsgError + '</MsgError>' +
			'</ActualizarInstalacionDSResponse>';
	}
   
	public static string formatoNumero( Decimal valorNumero, 
										String formatoValor, 
										Integer posicionesDecimales, 
										String separadorDecimal)
	{
		
		String valorCampo = '';
		
		if( formatoValor != null )
		{
			valorNumero = valorNumero.setScale(posicionesDecimales);
			
			if( formatoValor.equalsIgnoreCase('#.###,##') )
			{
				valorCampo = valorNumero.format();
				
				if( separadorDecimal != ',' )
				{
					valorCampo = valorCampo.replace(',', ';');
					valorCampo = valorCampo.replace('.', ',');
					valorCampo = valorCampo.replace(';', separadorDecimal);
				}
			}
			else if( formatoValor.equalsIgnoreCase('###.##') )
			{
				valorCampo = String.valueOf( valorNumero.toPlainString() );
				
				if( separadorDecimal != '.' )
				{
					valorCampo = valorCampo.replace('.', ';');
					valorCampo = valorCampo.replace(',', '.');
					valorCampo = valorCampo.replace(';', separadorDecimal);
				}
			}
			else if (formatoValor.equalsIgnoreCase('###'))
			{
				valorCampo = String.valueOf(valorNumero.toPlainString());
				valorCampo = valorCampo.split('\\.')[0];
			}
		}
		return valorCampo;
	}
	
	public static String formatoFecha( Datetime valorFecha, String funcionEspecial, String formatoValor )
	{
		String valorCampo = '';
		Datetime fechaHoraActual = system.now();
		
		if( ( funcionEspecial != '' ) && ( funcionEspecial != null ) )
		{
			if( funcionEspecial.equalsIgnoreCase('Hoy') )
				valorCampo = fechaHoraActual.format(formatoValor);
			
			if( funcionEspecial.equalsIgnoreCase('Ayer'))
			{
				fechaHoraActual = fechaHoraActual.addDays(-1);
				valorCampo = fechaHoraActual.format(formatoValor);
			}

			if( funcionEspecial.equalsIgnoreCase('Mañana'))
			{
				  fechaHoraActual = fechaHoraActual.addDays(1);
				  valorCampo = fechaHoraActual.format(formatoValor);
			}

			if( funcionEspecial.equalsIgnoreCase('Año actual') )
			  valorCampo = fechaHoraActual.format('yyyy');
			
			if( funcionEspecial.equalsIgnoreCase('Mes actual'))
				valorCampo = fechaHoraActual.format('MM');
			
			if( funcionEspecial.equalsIgnoreCase('Ahora'))
				valorCampo = fechaHoraActual.format(formatoValor);
		}
		else
			valorCampo = valorFecha.format(formatoValor);
		
		return valorCampo;
	}
	
	public static String convertirFechaSalesforce(String valorFecha, String formatoFecha)
	{
		valorFecha = valorFecha.trim();
		
		String ano = '';
		String mes = '';
		String dia = '';
		String hora = '';
		String minutos = '';
		String segundos = '';
		Integer posicionComponente = 0;
		String cadenaFecha = '';
		
		posicionComponente = formatoFecha.indexOf('yyyy');
		
		if (posicionComponente > -1)
		{
			ano = valorFecha.substring(posicionComponente, posicionComponente + 4);
			cadenaFecha = ano;
		}
		
		posicionComponente = formatoFecha.indexOf('MM');
		
		if (posicionComponente > -1)
		{
			mes = valorFecha.substring(posicionComponente, posicionComponente + 2);
			cadenaFecha = cadenaFecha + '-' + mes;
		}
		
		posicionComponente = formatoFecha.indexOf('dd');
		
		if (posicionComponente > -1)
		{
			dia = valorFecha.substring(posicionComponente, posicionComponente + 2);
			cadenaFecha = cadenaFecha + '-' + dia;
		}
		
		posicionComponente = formatoFecha.indexOf('HH');
		
		if (posicionComponente > -1)
		{
			hora = valorFecha.substring(posicionComponente, posicionComponente + 2);
			cadenaFecha = cadenaFecha + ' ' + hora;
		}
		
		posicionComponente = formatoFecha.indexOf('mm');
		
		if (posicionComponente > -1)
		{
			minutos = valorFecha.substring(posicionComponente, posicionComponente + 2);
			cadenaFecha = cadenaFecha + ':' + minutos;
		}
		
		posicionComponente = formatoFecha.indexOf('ss');
		
		if (posicionComponente > -1)
		{
			segundos = valorFecha.substring(posicionComponente, posicionComponente + 2);
			cadenaFecha = cadenaFecha + ':' + segundos;
		}
		
		return cadenaFecha;
	}
	
	global class mensajeLog
	{
		WebService String codDS{get;set;}
		WebService String codViabilidad{get;set;}
		WebService String codError{get;set;}
		WebService String consecutivo{get;set;}
		WebService String idViabilidad{get;set;}
		WebService String respuestaCarga{get;set;}
	}
	
	global class Response 
	{
		webservice boolean isSuccess;
		webservice string Error;
	}
	
	webservice static Response respViabilidad( List<mensajeLog> listRespuesta )
	{
		Response respuesta = new Response();
		List<String> lstCodDs = new List<String>();
		List<String> lstConsecutivo = new List<String>();
		Map<String,String> mEstados = new Map<String,String>();
		List<BI_Log__c> logs = new List<BI_Log__c>();
		Map<String,BI_COL_Viabilidad_Tecnica__c> mapSolictudViab = new Map<String,BI_COL_Viabilidad_Tecnica__c>(); 
		//Armar
		for( mensajeLog msl : listRespuesta )
		{
			lstConsecutivo.add( msl.consecutivo );
			lstCodDs.add( msl.codDS );
			mEstados.put( msl.consecutivo, msl.codError );
		}
		//Actualizacion de las Solicitudes de viabilidad
		List<BI_COL_Viabilidad_Tecnica__c> lstSolicitudViabilidad = [
			SELECT  ID,BI_COL_Estado__c, Name, BI_COL_Modificacion_de_Servicio__r.BI_COL_Codigo_unico_servicio__r.Name,
					BI_COL_Modificacion_de_Servicio__r.ID 
			FROM    BI_COL_Viabilidad_Tecnica__c 
			WHERE   NAME IN: lstConsecutivo ];
		for( BI_COL_Viabilidad_Tecnica__c sc : lstSolicitudViabilidad )
		{
			if( mEstados.containsKey( sc.Name ) )
			{
				if( mEstados.get( sc.Name ) == '0' )
					sc.BI_COL_Estado__c = 'Solicitado';
				else
					sc.BI_COL_Estado__c = 'Fallido';
				mapSolictudViab.put( sc.Name, sc );
			}
		}
		for( mensajeLog msl : listRespuesta )
		{
			String estado = msl.codError == '0' ? 'EXITO' : 'Fallido';
			
			if( mapSolictudViab.containsKey( msl.consecutivo ) )
			{
				logs.add( new BI_Log__c( Descripcion__c = 'RESPUESTA VIABILIDADES', 
					BI_COL_Informacion_recibida__c = msl.respuestaCarga, BI_COL_Tipo_Transaccion__c = 'VIABILIDADES', 
					BI_COL_Modificacion_Servicio__c = mapSolictudViab.get(msl.consecutivo).BI_COL_Modificacion_de_Servicio__r.ID, 
					BI_Descripcion__c = msl.consecutivo ) );
			}
		}
		try
		{
			update lstSolicitudViabilidad;
			insert logs;
			respuesta.Error = 'Actualizacion Correcta';
			respuesta.isSuccess = true;
		}
		catch( Exception ex )
		{
			respuesta.Error = ex.getMessage();
			respuesta.isSuccess = false;
			System.debug( ex.getStackTraceString() );
		}
		return respuesta;
	}
}