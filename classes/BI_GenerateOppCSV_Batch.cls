global class BI_GenerateOppCSV_Batch implements Database.Batchable<sObject> {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Micah Burgos
	    Company:       Aborda
	    Description:   Batch class that generate a csv doc with the cases of the 'Recarterización'
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    25/02/2016                      Micah Burgos             	Initial version
	    25/02/2016						Guillermo Muñoz				End of the develop
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	String query;
	Set<Id> set_all_acc;


	final String DOCUMENT_NAME;
	final String DOC_ID;
	final String DOC_ID2;
	final Id RECARTERIZACION_ID;

	final Set<String> set_normalOpp = new Set<String>{'BI_Preoportunidad','BI_Preoportunidad_renegociacion','BI_Renegociacion','BI_Renegociacion_ciclo_licitaciones','BI_Renegociacion_ciclo_rapido','BI_Ciclo_completo','BI_Ciclo_licitaciones','BI_Ciclo_rapido'};	
	final Set<String> set_agrupaOpp = new Set<String>{'BI_Oportunidad_Regional'};	
	global BI_GenerateOppCSV_Batch(Id rec_param) {

		RECARTERIZACION_ID = rec_param;

		/*
		//SEARCH FOLDER
		FOLDER_ID = [Select Id from Folder where DeveloperName = 'BI_Ficheros_CSV_de_Recarterizaciones'].Id;
		*/

		//GENERATE OPP DOCUMENT 
		Attachment doc = new Attachment();
		doc.Name = 'Recarterización Opportunidades ['+String.valueOf(System.now())+'].csv';
		doc.ParentId = rec_param; 


		//Header CSV
		String csvDocHeader = 'OpportunityId,OldOwner,NewOwner,AccountId,RecarterizacionId,IsProcessed'+'\n';
		doc.Body  = Blob.valueOf(csvDocHeader);  

		//INSERT ATTACHMENT
		insert doc;

		DOC_ID = doc.Id;

		//GENERATE OPP DOCUMENT 
		Attachment doc2 = new Attachment();
		doc2.Name = 'Borrar Equipo de Opportunidades ['+String.valueOf(System.now())+'].csv';
		doc2.ParentId = rec_param; 


		//Header CSV
		String csvDocHeader2 = 'Id,OpportunityId,OpportunityAccessLevel,UserId,TeamMemberRole,Name,RecarterizacionId,Propietario'+'\n';
		doc2.Body  = Blob.valueOf(csvDocHeader2);  

		//INSERT ATTACHMENT
		insert doc2;

		DOC_ID2 = doc2.Id;



		//SEARCH ACCOUNT FROM BI_Linea_de_Recarterizacion__c
		List<BI_Linea_de_Recarterizacion__c> lst_rec =  [SELECT Id,BI_AccountId__c, BI_Recarterizacion__c, BI_Cases_RecordTypes__c, BI_Open_Opp__c, BI_Closed_Opp__c, BI_Open_Agrupacion_Opp__c, BI_Closed_Agrupacion_Opp__c  FROM BI_Linea_de_Recarterizacion__c WHERE BI_Recarterizacion__c = :rec_param];


		set_all_acc = new Set<Id>();
		
		Set<Id> set_ParentsID = new Set<Id>();

		//GENERIC FILTERS FOR ALL OPP 
		Boolean onlyOpenOpp = true;
		Boolean onlyClosedOpp = true;

		Boolean onlyAgrupacionOpp = true;
		Boolean onlyNormalOpp= true;

		for(BI_Linea_de_Recarterizacion__c rec :lst_rec){
			set_all_acc.add(rec.BI_AccountId__c);

			if(rec.BI_Open_Opp__c){
				onlyClosedOpp = false;

				onlyAgrupacionOpp = false;
			}
			if(rec.BI_Closed_Opp__c){
				onlyOpenOpp = false;
				onlyAgrupacionOpp = false;
			}

			if(rec.BI_Open_Agrupacion_Opp__c){
 				onlyNormalOpp= false;
 				onlyClosedOpp = false;
			}

			if(rec.BI_Closed_Agrupacion_Opp__c){
 				onlyNormalOpp= false;
 				onlyOpenOpp = false;
			}
		}

		query = 'SELECT ID, OwnerId, AccountId, RecordType.DeveloperName, IsClosed, (SELECT Id,OpportunityAccessLevel,OpportunityId,TeamMemberRole,UserId,Name FROM OpportunityTeamMembers) FROM Opportunity WHERE AccountId IN :set_all_acc ';

		if(onlyOpenOpp){
			query += ' AND IsClosed = false';
		}
		if(onlyClosedOpp){
			query += ' AND IsClosed = true';
		}

		if(onlyAgrupacionOpp ){
			query += ' AND RecordType.DeveloperName = \'BI_Oportunidad_Regional\'';
		}
		if(onlyNormalOpp){
			query += ' AND RecordType.DeveloperName != \'BI_Oportunidad_Regional\'';
		}

		query += ' order by AccountId';

		System.debug('********QUERY: ' + query);
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		List<Attachment> lst_doc = [SELECT ID, Body FROM Attachment WHERE Id  =: DOC_ID OR Id =: DOC_ID2 ORDER BY CreatedDate ASC];

   		System.debug('******** DOC ID: ' + lst_doc[0].Id);

   		String csvDoc_2 = lst_doc[0].Body.toString();
   		String csvDoc2_2 = lst_doc[1].Body.toString();

   		List<Opportunity> lst_opp = scope;	
		Set<Id> set_accID = new Set<Id>();
		for(Opportunity opp :lst_opp){
			set_accID.add(opp.AccountId);
		}

		//Crear mapa de recarterizaciones 
		List<BI_Linea_de_Recarterizacion__c> lst_rec2 = [SELECT Id, BI_AccountId__c,BI_Borrar_Equipo_de_Cuenta__c,BI_NewOwner__c,BI_OldOwner__c,BI_Status__c,BI_Num_OppProcessed__c, BI_Open_Opp__c, BI_Closed_Opp__c, BI_Open_Agrupacion_Opp__c, BI_Closed_Agrupacion_Opp__c, BI_Borrar_Equipo_de_Oportunidad__c
   													FROM BI_Linea_de_Recarterizacion__c WHERE BI_AccountId__c IN :set_accID AND BI_Recarterizacion__c = :RECARTERIZACION_ID];
   		Map<String, BI_Linea_de_Recarterizacion__c> map_rec = new Map<String, BI_Linea_de_Recarterizacion__c>();

   		for(BI_Linea_de_Recarterizacion__c rec2 : lst_rec2){
			map_rec.put(rec2.BI_AccountId__c, rec2);
   		}

   		for(Opportunity opp :lst_opp){

   			system.debug('map_rec.get(opp.AccountId).BI_Open_Opp__c: '+ map_rec.get(opp.AccountId).BI_Open_Opp__c);
   			system.debug('set_normalOpp.contains(opp.RecordType.DeveloperName): ' + set_normalOpp.contains(opp.RecordType.DeveloperName));
   			system.debug('opp.RecordType.DeveloperName: ' + opp.RecordType.DeveloperName);
   			system.debug('set_agrupaOpp: ' + set_normalOpp);
   			system.debug('opp.IsClosed: ' + opp.IsClosed);

   			if(
   				(map_rec.get(opp.AccountId).BI_Open_Opp__c && set_normalOpp.contains(opp.RecordType.DeveloperName) && !opp.IsClosed )||

   				(map_rec.get(opp.AccountId).BI_Closed_Opp__c && set_normalOpp.contains(opp.RecordType.DeveloperName) && opp.IsClosed ) ||

   				(map_rec.get(opp.AccountId).BI_Open_Agrupacion_Opp__c && set_agrupaOpp.contains(opp.RecordType.DeveloperName) && !opp.IsClosed ) ||

   				(map_rec.get(opp.AccountId).BI_Closed_Agrupacion_Opp__c && set_agrupaOpp.contains(opp.RecordType.DeveloperName) && opp.IsClosed ) 

			){
				csvDoc_2 += opp.Id+','+opp.OwnerId+','+map_rec.get(opp.AccountId).BI_NewOwner__c+','+opp.AccountId+','+map_rec.get(opp.AccountId).Id+',true\n';
				map_rec.get(opp.AccountId).BI_Processed_Opp__c = true;
				map_rec.get(opp.AccountId).BI_Num_OppProcessed__c = (map_rec.get(opp.AccountId).BI_Num_OppProcessed__c == null)?1: map_rec.get(opp.AccountId).BI_Num_OppProcessed__c +1;
				
				if(!opp.OpportunityTeamMembers.isEmpty() && map_rec.get(opp.AccountId).BI_Borrar_Equipo_de_Oportunidad__c){
					for(OpportunityTeamMember otm : opp.OpportunityTeamMembers){
					
						if(otm.UserId == opp.OwnerId){
							csvDoc2_2 += otm.Id + ',' + otm.OpportunityId + ',' + otm.OpportunityAccessLevel + ',' + otm.UserId + ',' + otm.TeamMemberRole + ',' + otm.Name + ',' + RECARTERIZACION_ID + ',true\n';
						}
						else{
							csvDoc2_2 += otm.Id + ',' + otm.OpportunityId + ',' + otm.OpportunityAccessLevel + ',' + otm.UserId + ',' + otm.TeamMemberRole + ',' + otm.Name + ',' + RECARTERIZACION_ID + ',true\n';
						}
					}
				}
   			}
			
		}
		lst_doc[0].Body = Blob.valueOf(csvDoc_2); 
		lst_doc[1].Body = Blob.valueOf(csvDoc2_2);


		update lst_doc;

		update map_rec.values();
	}
	
	global void finish(Database.BatchableContext BC) {

		BI_Recarterizacion__c recarterizacion = new BI_Recarterizacion__c(
			Id = RECARTERIZACION_ID,
			BI_End_batch_opp__c = true
		);

		update recarterizacion;
		
	}
	
}