global class BI2_CancelarViejosPendientes_JOB implements Schedulable {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Scheduled class that cancels cases that haven't had an answer from the client 5 days past BI_Fecha_ltimo_cambio_de_estado__c 

	 History:

	 <Date>				<Author>				<Description>
	 08/10/2015			Jose Miguel Fierro		Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    /**
     * CHANGE THIS VALUE TO THE ESTABLISHED RUN TIME
     */
    public static final Integer RUN_HOUR = 4;
    
	Id lastId;
	public BI2_CancelarViejosPendientes_JOB() {
		this(null);
	}
	public BI2_CancelarViejosPendientes_JOB(Id lastId) {
		this.lastId = lastId;
	}
    
    global void execute(SchedulableContext SC) {
        try {
            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');
            try {System.abortJob(sc.getTriggerId());}catch(exception exc) {}
            final Integer queryLimit = Test.isRunningTest() ? 2 : 10000;
            
            System.debug(LoggingLevel.ERROR, '{...}{...}{...}Running Cron: ' + [SELECT CronExpression FROM CronTrigger WHERE Id=:SC.getTriggerId()]);
            
            List<Case> lst_cases = new List<Case>();
            if(lastId==null) {
                lst_cases.addAll([SELECT Id, Status FROM Case WHERE Status IN ('Pending', 'Pendiente') AND RecordType.DeveloperName IN ('BI2_caso_comercial','BI2_Caso_Padre') AND BI_Fecha_ltimo_cambio_de_estado__c = LAST_N_DAYS:5 LIMIT :queryLimit]);
            } else {
                lst_cases.addAll([SELECT Id, Status FROM Case WHERE Status IN ('Pending', 'Pendiente') AND RecordType.DeveloperName IN ('BI2_caso_comercial','BI2_Caso_Padre') AND BI_Fecha_ltimo_cambio_de_estado__c = LAST_N_DAYS:5 AND Id > :lastId LIMIT :queryLimit]);
            }
            System.debug('{****}{***}: lst_cases:' + lst_cases);
            if(!lst_cases.isEmpty()) {
                lastId = lst_cases.get(lst_cases.size() - 1).Id;
                cancelCases(lst_cases);
                String strCron = generateCronStr(Datetime.now().addSeconds(5));
                System.schedule('BI2_CancelarViejosPendientes_JOB ' + strCron, strCron, new BI2_CountCasesLastMonth_JOB(lastId));
            }
            else {
                String strCron = generateCronStr(((DateTime)Date.today()).addDays(1).addHours(RUN_HOUR)); // Tomorrow
                System.schedule('BI2_CancelarViejosPendientes_JOB ' + strCron, strCron, new BI2_CountCasesLastMonth_JOB(lastId));
            }
        } catch (Exception ex) {
            BI_LogHelper.generate_BILog('BI2_CancelarViejosPendientes_JOB.execute', 'BI_EN', ex, 'Apex Job');
        }
    }
	
    @testVisible private static void cancelCases(List<Case> lst_cases2cancel) {
        for(Case cas : lst_cases2cancel) {
            cas.Status = 'Cancelled';
            cas.TGS_Status_reason__c = 'No responde from applicant – No information';
            cas.BI2_PER_Motivo_inconformidad__c = 'Ninguno';
        }
        update lst_cases2cancel;
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Method to create a cron string. Can this be externalized to a utilities class? This is also repeated in BI2_CountCasesLastMonth_JOB

	 IN:			Schedulable context (Parameters to schedule the job)
	 OUT:			Void

	 History:

	 <Date>				<Author>				<Description>
	 08/10/2015			Jose Miguel Fierro		Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static String generateCronStr(Datetime jobT){
		try{
			if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

		    return jobT.second() + ' ' + jobT.minute() + ' ' + jobT.hour() + ' ' + jobT.day() + ' ' + jobT.month() + ' ? ' + jobT.year();
		}catch (Exception e){
		   return null;
		}
	}
}