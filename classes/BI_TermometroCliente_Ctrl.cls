public with sharing class BI_TermometroCliente_Ctrl {

    public String termometro_recordTypeId {get;set;}

    public BI_TermometroCliente_Ctrl(ApexPages.StandardController stdController) {

        List<RecordType> lst_rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_CHI_Termometro' AND SobjectType = 'BI_ISC2__c'];

        if(!lst_rt.isEmpty()){
            termometro_recordTypeId = lst_rt[0].Id;
        }else{
            termometro_recordTypeId = '';
        }
    }
}