public class QueuableSetAssetAdditionalFields implements Queueable
{
    String assetGeneratedId; 
    String caseId; 
    String confId;
    Boolean caseMP;
    /* START Álvaro López 15/02/2018 - Case Manager fix csuids*/
    public QueuableSetAssetAdditionalFields(String inputAssetGeneratedId, String inputCaseId, String inputConfId, Boolean caseManagerProcess)
    {
        assetGeneratedId	=	inputAssetGeneratedId;
        caseId				=	inputCaseId;
        confId				=	inputConfId;
        caseMP              =   caseManagerProcess;
    }   
    
    public void execute(QueueableContext context) 
    {
        system.debug('Call setAssetAdditionalFields');
        system.debug('assetGeneratedId: '+assetGeneratedId);
        system.debug('caseId: '+caseId);
        system.debug('confId: '+confId);

        if(caseMP){
            NETriggerHelper.setTriggerFired('TGS_fill_CSUID');
        }
        /* END Álvaro López 15/02/2018 - Case Manager fix csuids*/
        Order2AssetWithQueuable.setAssetAdditionalFields(assetGeneratedId, caseId, confId); 

        list<NE__OrderItem__c> confItems = [SELECT Id FROM NE__OrderItem__c WHERE NE__OrderId__c =: assetGeneratedId];

        TGS_CallRodWs.inFutureContext   = false;
        TGS_CallRodWs.inFutureContextCI = false;
         /* Send Integration with in ROD in modification and termination */
        Set<Id> confitemsIds = new Set<Id>();
        for(NE__OrderItem__c ci:confItems)
        {           
            confitemsIds.add(ci.Id);
        }
        //Avoid uncatchable errors during test
        if(Test.isRunningTest() == false)        
        	ID jobID = System.enqueueJob(new QueuableRODOrderItem(confitemsIds));
    }    
}