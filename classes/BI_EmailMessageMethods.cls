public without sharing class BI_EmailMessageMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Álvaro Hernando Gavilán
    Company:       Telefonica Global Technology
    Description:   Methods executed by BI_EmailMessage Triggers 
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    11/06/2015              Álvaro Hernando         Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    
    static{
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'Case']){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Álvaro Hernando Gavilán
    Company:       Telefonica Global Technology
    Description:   Method to fill fields on Email To Case Routing Address "BI ARG Gestión Administrativa Cobranzas"
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    11/06/2015                      Álvaro Hernando             Initial Version
    18/11/2015                      Francisco Ayllon            Fixed bugs due to translations
    04/10/2016                      Antonio Pardo               Check if the adrress is in the ccAddress field too
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void fillFieldsARGCobranzas(List <EmailMessage> news){

        try{
            
            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');
                
            Set<Id> caseIds = new Set<Id>();
            Set<String> caseMails = new Set<String>();
            
            for (EmailMessage mail:news){
                //System.debug('*** AHG *** Incoming: ' + mail.Incoming + ' y Address: ' + mail.ToAddress);
                if(mail.Incoming == true){
                    if(mail.ToAddress != null && mail.ToAddress.contains('pagosempresas.ar@telefonica.com')){
                        caseMails.add(mail.FromAddress); // emails que debemos comprobar si existen en el sistema o no
                        caseIds.add(mail.ParentId);
                    } 
                    else if(mail.CcAddress != null && mail.CcAddress.contains('pagosempresas.ar@telefonica.com')){
                        caseMails.add(mail.FromAddress); // emails que debemos comprobar si existen en el sistema o no
                        caseIds.add(mail.ParentId);
                    } 
                }
            }
            
            if (!caseIds.isEmpty()){ // tenemos casos por Email To Case en recepción
                List<Case> casesList = [SELECT Id,SuppliedEmail,Origin FROM Case WHERE Id IN :caseIds]; // lista de casos a procesar
                List<Account> auxAccount = [SELECT Id,Name FROM Account WHERE Name = 'Gestión Administrativa Cobranzas' LIMIT 1]; // cliente virtual
                
                Set<String> mailsList = new Set<String>(); // construimos un set con los emails de los Contactos que existen en el sistema
                Set<String> duplicatedMails = new Set<String>(); // Set con los Contactos duplicados
                
                if (!caseMails.isEmpty()){
                    
                    //List<Contact> contactLst = [SELECT Id,Email FROM Contact WHERE Email IN :caseMails];
                    for(Contact con :[SELECT Id,Email FROM Contact WHERE Email IN :caseMails]){
                    //for(Contact con :contactLst){
                        System.debug('*** AHG *** Contacto existe!!!');
                        if (mailsList.contains(con.Email)){
                            System.debug('*** AHG *** Contacto duplicado!!!');
                            duplicatedMails.add(con.Email);
                        }
                        mailsList.add(con.Email);
                    }
                }
               
                List<Case> casesToUpdate = new List<Case>();
                for (Case cas:casesList){
                    if (!mailsList.contains(cas.SuppliedEmail) && (cas.Origin == 'Email To Case ARG - Cobranzas'||cas.Origin == 'Caso desde Correo Argentina') && !auxAccount.isEmpty()){ // si el contacto no existe o esta duplicado, asignamos el Cliente Virtual
                        cas.AccountId = auxAccount[0].Id;
                        casesToUpdate.add(cas);
                    }
                    if (duplicatedMails.contains(cas.SuppliedEmail) && (cas.Origin == 'Email To Case ARG - Cobranzas'||cas.Origin == 'Caso desde Correo Argentina') && !auxAccount.isEmpty()){
                        System.debug('*** AHG *** Contacto duplicado modificado!!!');
                        cas.AccountId = auxAccount[0].Id;
                        casesToUpdate.add(cas);
                    }
                    System.debug('*** AHG *** Cliente Virtual no existe!!!');
                }
                
                if (!casesToUpdate.isEmpty()){ // to avoid update cases which already have Account and Contact asociated
                update casesToUpdate;
                }
                
            }
            
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_EmailMessageMethods.fillFieldsARGCobranzas', 'BI_EN', Exc, 'Trigger');
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Guillermo Muñoz
     Company:       Accenture
     Description:   PER Method to fill fields on Case from an Email, and assigning it to a Queue:
                    BI_PER_Atencion_Empresas_No_Identif 
                    BI_PER_AtencionEmpresas

     History:
     <Date>         <Author>                        <Change Description>
     31/10/2016     Guillermo Muñoz                 Initial Vertion
     02/03/2017     Gawron, Julián                  Adding variables for D310
     18/08/2017     Guillermo Muñoz                 Changed to use new BI_MigrationHelper functionality
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void fillE2CFields(List<EmailMessage> news){

        try{

            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }

            List<String> lst_validas = Label.BI_Atencion_Empresas_Peru.split(' ');
            List<Id> lst_casesId = new List<Id>();
            List<String> lst_correos = new List<String>();

            //Primero se separan solo los mensajes desde la direccion que interesa
            for(EmailMessage m : news){
                for(String dirValida : lst_validas){
                    if((m.CcAddress != null && m.CcAddress.contains(dirValida))
                        || (m.ToAddress != null && m.ToAddress.contains(dirValida))){
                        lst_casesId.add(m.ParentId);
                        lst_correos.add(m.FromAddress);

                    }
                }
            }
            if(!lst_correos.isEmpty()){
                //Ahora hay que separar los tres grupos de correos.
                Map<String, Contact> map_valid_mails = new Map<String, Contact>();
                Map<String, Contact> map_invalid_mails = new Map<String, Contact>();
                //Busqueda por contactos
                for(Contact c : [Select Id, Email, BI2_IsAutorized__c, Account.Id from Contact where Email in :lst_correos]){
                    if(c.BI2_IsAutorized__c){
                        map_valid_mails.put(c.Email, c);
                    }else{
                        map_invalid_mails.put(c.Email, c);
                    }
                }
                //Asignación del cliente al caso
                List<Case> lst_cases = [Select Id, AccountId, OwnerId, SuppliedEmail, BI_PER_AtencionEmpresas__c, BI_Notification_Preferences__c from Case where Id in :lst_casesId];
                //Listas que contendrán los correos de cada uno de los tipos de mails
                List<Case> lst_cases_contAuth = new List<Case>();
                List<Case> lst_cases_contNoAuth = new List<Case>();
                List<Case> lst_cases_sinCont = new List<Case>();
                //Obtenemos los id de las dos colas
                List<Group> lst_queue = [Select Id, DeveloperName, Type from Group where DeveloperName in ('BI_PER_Atencion_Empresas_No_Identif', 'BI_PER_AtencionEmpresas')];
                Map<String, Id> map_id_aut_noaut = new Map<String, Id>();
                for(Group g : lst_queue){
                    map_id_aut_noaut.put(g.DeveloperName, g.Id);
                }
                 
                for(Case c : lst_cases) {

                    c.BI_Notification_Preferences__c = 'Case Creation;Status Change;Case Closure;'; //JEG Adding variables for D310

                    //Vemos donde encontramos el mail
                    if(map_valid_mails.containsKey(c.SuppliedEmail)){ //es valido
                        if(map_id_aut_noaut.containsKey('BI_PER_AtencionEmpresas')){
                             c.OwnerId = map_id_aut_noaut.get('BI_PER_AtencionEmpresas');
                             lst_cases_contAuth.add(c);
                        }
                    }else{
                        //es inválido o no tiene contacto
                        if(map_id_aut_noaut.containsKey('BI_PER_Atencion_Empresas_No_Identif')){
                             c.OwnerId = map_id_aut_noaut.get('BI_PER_Atencion_Empresas_No_Identif');
                             lst_cases_contNoAuth.add(c);
                        }else{
                            lst_cases_sinCont.add(c);
                        }               
                    }
                    //Se marca el caso para saber que fue creado por este emailToCase.
                    c.BI_PER_AtencionEmpresas__c = true;
                }

                //Anulando triggers
                Map <Integer, BI_bypass__c> mapa;
                try{
                    mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, false, false, false);
                    BI_MigrationHelper.skipAllTriggers();
                    update lst_cases;
                }
                catch(Exception e){}
                finally{
                    if(mapa != null){
                        BI_MigrationHelper.disableBypass(mapa);
                    }
                    BI_MigrationHelper.cleanSkippedTriggers();
                }
            }//If hay correos
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_EmailMessageMethods.fillE2CFields', 'BI_EN', Exc, 'Trigger');
        }
    } //end fillE2CFields

} // end class