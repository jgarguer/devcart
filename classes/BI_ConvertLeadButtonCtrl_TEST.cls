@isTest 
private class BI_ConvertLeadButtonCtrl_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_ConvertLeadButtonCtrl class
    
    History:
    
    <Date>            <Author>          	<Description>
    05/08/2014        Micah Burgos       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void convertLeadButtonCtrl_test_validId() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	
 		PageReference pageRef = new PageReference('BI_ConvertLeadButtonCtrl');
       	Test.setCurrentPage(pageRef);
       	
   		/*BI_Pickup_Option__c op1 = new BI_Pickup_Option__c (Name = 'RFC', BI_Tipo__c = 'Tipo de Identificador Fiscal');
   		insert op1;*/
   		
   		list<Lead> lst_ld = BI_DataLoad.loadLeads(1);
       	
       	lst_ld[0].BI_Tipo_de_Identificador_Fiscal__c = 'RFC';
       	lst_ld[0].BI_Numero_identificador_fiscal__c = 'VECB380326XXX';
       	
       	update lst_ld;
       	
       	Test.startTest();
       	
       	ApexPages.StandardController sc = new ApexPages.StandardController(lst_ld[0]);
        BI_convertLeadButtonCtrl controller = new BI_convertLeadButtonCtrl(sc);
        
        system.assertEquals(controller.dir, true);
        Test.stopTest();
        
    }
    
    static testMethod void convertLeadButtonCtrl_test_notValidId(){
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	list<Lead> lst_ld = BI_DataLoad.loadLeads(1);
    	
    	Test.startTest();
    	
    	ApexPages.StandardController sc = new ApexPages.StandardController(lst_ld[0]);
        BI_convertLeadButtonCtrl controller = new BI_convertLeadButtonCtrl(sc);
        
        system.assertEquals(controller.dir, false);
        Test.stopTest();
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}