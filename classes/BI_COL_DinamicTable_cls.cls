/****************************************************************************************************
	Información general
	-------------------
	author: Javier Tibamoza Cubillos
	company: Avanxo Colombia
	Project: Implementación Salesforce
	Customer: TELEFONICA
	Description: 
	
	Information about changes (versions)
	-------------------------------------
	Number    Dates           Author                       Description
	------    --------        --------------------------   -----------
	1.0       14-Abr-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
public with sharing class BI_COL_DinamicTable_cls
{
	/***
	* @Author: 
	* @Company: 
	* @Description: 
	* @History: 
	* Date		|	Author	|	Description
	* 
	***/
	public BI_COL_DinamicTable_cls()
	{
		
	}
	/***
	* @Author: Javier Tibamoza
	* @Company: Avanxo
	* @Description: Función que retorna la Tabla Dinamica. Parametros: 
	* 	lstObject: Lista del query (Está lista se arma en el constructor)
	* 	lstApiField (Lista del nombre Api de los campos que se van a utilizar en la Tabla)
	* 	lstLabelField (Lista de las Etiquetes que se quieren que se muestren como columnas de la Tabla)
	* 	Estas listas deben coincidir en la misma posición el nombre API y el Label de los campos.
	* 	Ej: De una lista de campos de Contact 
	* 	list<string> lstApiField = new list<string>{'FirstName','LastName','Birthdate'}
	* 	list<string> lstLabelField = new list<string>{'Nombre','Apellido','Cumpleaños'}
	* @History: 
	* Date 			|	Author		|	Description
	* Abr-14-2015 	|Javier Tibamoza| Creación del metodo.
	***/
	public Component.Apex.PageBlockTable fn_DynamicComponentTable( list<sObject> lstObject, list<String> lstApiField, list<String> lstLabelField )
	{
		//Inicializa la variable de la Tabla
		Component.Apex.PageBlockTable table = new Component.Apex.PageBlockTable( var = 'rec' );
		//Debe existir en el controlador una lista que es la que se va a mostrar con el nombre lstObject
		table.expressions.value = '{!lstObject}';
		//add the field
		Component.Apex.Column column;  
		Component.Apex.OutputField outputField;
		//Recorre la lista de los campos que vienen en la lista
		for( Integer i = 0; i < lstApiField.size(); i++ )
		{
			column = new Component.Apex.Column( headerValue = '' + lstLabelField.get( i ) + '' );
			outputField = new Component.Apex.OutputField();  
			outputField.expressions.value = '{!rec.' + lstApiField.get( i ) + '}';
			column.childComponents.add( outputField );
			table.childComponents.add( column );
		}
		//Retorna la Tabla
		return table;
	}
}