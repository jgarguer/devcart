/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Test for BI_O4_CreateChildCaseQuantityController
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera          Initial version
    20/09/2017              Angel F. Santaliestra   Add the mandatory fields on account creation: BI_Subsegment_Regional__c,BI_Territory__c
    25/09/2017              Jaime Regidor           Fields BI_Subsector__c and BI_Sector__c added 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_CreateChildCaseQuantityCtrl_TEST {

    static{
        BI_TestUtils.throw_exception = false;
    }
    
    @isTest static void test_method_one() {

        BI_TestUtils.throw_exception = false;
        TGS_User_Org__c uO = new TGS_User_Org__c();
        uO.TGS_Is_BI_EN__c = True;
        uO.SetupOwnerId = UserInfo.getUserId();
        insert uO;

    List<Profile> profile = [Select Id from Profile where Name= 'TGS System Administrator' limit 1];
                
    User user = new User(
                    Username = 'abortest@email.com',LastName = 'TestUser',CommunityNickname = 'nick' + String.valueOf(Math.random()),
                    Alias = 'testuser', Email = 'abortest@email.com', Profileid = profile[0].id, TimeZoneSidKey = 'America/New_York',
                    EmailEncodingKey='UTF-8', BI_Permisos__c = 'Atención al Cliente', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');


    insert user;        
    User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser(); 
    Account acc;

    System.runAs(usu){
		RecordType rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];
		       
    	acc = new Account();
    	acc.Name = 'test';
		acc.BI_Envio_a_aprobacion_agrupacion_cliente__c = true;
		acc.BI_Activo__c = Label.BI_Si;
		acc.BI_Segment__c = 'Empresas';
        acc.BI_Subsector__c = 'test'; //25/09/2017
        acc.BI_Sector__c = 'test'; //25/09/2017
        acc.BI_Subsegment_Regional__c = 'test';
        acc.BI_Territory__c = 'test';
		acc.BI_Subsegment_local__c = 'Top 1';
		acc.RecordtypeId = rt.Id;
		acc.BI_Activo__c = Label.BI_Si;
        acc.BI_Country__c = 'Spain';
        
        
        insert acc;
    }

    System.runAs(user){
        RecordType rto = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.CloseDate = Date.today();
        opp.StageName = Label.BI_F6Preoportunidad;
        opp.BI_Ciclo_ventas__c = Label.BI_Completo;
        opp.RecordTypeId = rto.Id;
        opp.NE__HaveActiveLineItem__c = true;
        opp.NE__Order_Generated__c = 'Quote';
        opp.AccountId = acc.Id;
        opp.CurrencyIsoCode = 'EUR';
        insert opp;

        RecordType rtc = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno'];

        Case caso = new Case();
        caso.AccountId = acc.Id;
        caso.RecordTypeId = rtc.Id;
        caso.BI_Nombre_de_la_Oportunidad__c = opp.Id;
        caso.Subject = 'XXX';
        caso.Status = 'Assigned';
        caso.Priority = 'Medium';
        caso.Description = 'ZZZ';
        caso.BI_O4_Quantity__c = 3;
        caso.BI_Department__c = 'Ingeniería preventa';
        caso.BI_Type__c = 'Proyectos Especiales';
        caso.BI_O4_Sales_Channel__c = 'MNC';
        caso.BI_O4_Leading_Channel__c = 'Spain';
        caso.BI_O4_Type_of_Offer__c  = 'PoC';
        caso.BI_O4_Service__c = 'D-Cloud';
        caso.BI_O4_Type_of_Support_Request__c = 'Both';
        caso.BI_O4_Main_Topics__c = 'PS - Other';
        caso.BI_PER_Fecha_de_compromiso_de_atencion__c = datetime.now();
        insert caso;

        Case caso1 = new Case();
        caso1.AccountId = acc.Id;
        caso1.ParentId = caso.Id;
        caso1.RecordTypeId = rtc.Id;
        caso1.BI_Nombre_de_la_Oportunidad__c = opp.Id;
        caso1.Subject = 'CCC';
        caso1.Status = 'Assigned';
        caso1.Priority = 'Medium';
        caso1.Description = 'AAA';
        caso1.BI_O4_Quantity__c = 3;
        caso1.BI_Department__c = 'Ingeniería preventa';
        caso1.BI_Type__c = 'Proyectos Especiales';
        caso1.BI_O4_Sales_Channel__c = 'MNC';
        caso1.BI_O4_Leading_Channel__c = 'Spain';
        caso1.BI_O4_Type_of_Offer__c  = 'PoC';
        caso1.BI_O4_Service__c = 'D-Cloud';
        caso1.BI_O4_Type_of_Support_Request__c = 'Both';
        caso1.BI_O4_Main_Topics__c = 'PS - Other';
        caso1.BI_PER_Fecha_de_compromiso_de_atencion__c = datetime.now();
        insert caso1;


        Test.startTest();

        ApexPages.StandardController standardC = new ApexPages.StandardController(caso); 
        BI_O4_CreateChildCaseQuantityController controller = new BI_O4_CreateChildCaseQuantityController(standardC);

        controller.createChildCase();
        controller.cancel();
        System.assertNotEquals(null, caso.Id);

        Test.stopTest();
    }
    }   
}