public with sharing class PCA_ServiceTracking extends PCA_HomeController{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Controller of service tracking page
    
    History:
    
    <Date>            <Author>              <Description>
    26/06/2014        Micah Burgos       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public String tokenApex{get;set;}
    public BI_ImageHome__c ImgBack {get;set;}
    public Attachment AttImg {get;set;}
    public BI_ImageHome__c RelationImage {get;set;}
    public BI_ImageHome__c RelationImageMovi {get;set;}
    public BI_ImageHome__c RelationImageTel {get;set;}
    public List<Reportes_PP__c> reportePP{get;set;}
    public List<WrapperReport> reports{get;set;}
    public map<string,string> mapVisibility {get; private set;}
    public String ticketsPage {get;set;}
    
    //public String repId { get ; set ; }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Constructor
    History:
    
    <Date>            <Author>              <Description>
    26/06/2014        Micah Burgos          Initial version
    23/03/2017        Everis                Visibility control
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public PCA_ServiceTracking(){
        
    /*  Report repSel = [SELECT Id FROM Report WHERE Id = '00O11000000SlQk'];
        if (repSel!=null)
        repId = string.valueof(repSel.Id);*/
        mapVisibility = PCA_ProfileHelper.getPermissionSet();
        ticketsPage = getTicketsPage();
        getTokenBO();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   method that check permissions
    History:
    
    <Date>            <Author>              <Description>
    26/06/2014        Micah Burgos       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    public PageReference checkPermissions (){
        try{
            
            PageReference page = enviarALoginComm();            
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ServiceTracking.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   method that get token
    History:
    
    <Date>            <Author>              <Description>
    26/06/2014        Micah Burgos       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void getTokenBO(){
        
       /* try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
        
            String randomCus = String.valueOf(Math.random()); 
            System.debug('randomCus: ' + randomCus);
            Http h = new Http();
            String url='http://bo.empresaslatinoamerica.telefonica.com:8080/restproxy/login.jsp?';
             System.debug('url: ' + url);
                    
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url);
            req.setMethod('POST');
            String json = '{rdm='+randomCus+'u='+usuarioCustomerPortal.BI_BO_User__c+',Password='+usuarioCustomerPortal.BI_BO_Pwd__c+'}';
            req.setBody(json);
        
            // Send the request, and return a response 
            HttpResponse response;
            if(!Test.isRunningTest()){
                response = h.send(req);
                if(response!=null)tokenApex = response.getBody();
            }else{
                tokenApex=':"W2K8TELATBOAP01:6400@{3&2=313668,U3}##';
            }
            
            // tratar el body
            if(tokenApex!=null && !tokenApex.equalsIgnoreCase('error') )
            tokenApex = tokenApex.substring((tokenApex.indexOf(':"')+2), (tokenApex.length()-2)); 
            System.debug('tokenApex: ' + tokenApex);
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('PCA_ServiceTracking.getTokenBO', 'Portal Platino', exc, 'Class');
        }*/
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   Return the image and description of the global page
    
    History:
    
    <Date>                    <Author>               <Description>
    17/12/2014                Antonio Moruno         Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public void loadInfo(){
        try{
        Id ActAccId = BI_AccountHelper.getCurrentAccountId();
        List<Account> SegAccount = [Select Id, BI_Segment__c, Name, BI_Country__c FROM Account Where Id=:ActAccId];
        List<BI_ImageHome__c> RelImg = new List<BI_ImageHome__c>();
        if(!SegAccount.IsEmpty()){
            if(SegAccount[0].BI_Segment__c!=null){
                RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'Service Tracking' AND  RecordType.Name= 'General' AND BI_Segment__c=: SegAccount[0].BI_Segment__c LIMIT 1];
                
                //===Consigue los reportes referenciados en los Reportes PP y rellena los wrappers con la info conjunta===
                reports=new List<WrapperReport>();
                Set<Id> IdsClientes = new Set<Id>();
                List<BI_Cliente_PP__c> ClientPP = [Select Id, Name, BI_Activo__c, BI_Cliente__c, BI_Imagen_inicio_PP__c, BI_Reportes__c, BI_Country_2__c, BI_Segment_2__c FROM BI_Cliente_PP__c where BI_Cliente__c =: ActAccId AND BI_Activo__c = true AND (BI_Country_2__c != null OR BI_Segment_2__c != null)];
                for(BI_Cliente_PP__c Client :ClientPP){
                    IdsClientes.add(Client.BI_Reportes__c);
                }
                reportePP=[SELECT BI_Activo__c,BI_Country__c,BI_Descripcion__c,BI_Segment__c,BI_URL_del_reporte__c,Name  FROM Reportes_PP__c WHERE (((BI_Country__c =: SegAccount[0].BI_Country__c OR BI_Country__c = null) AND BI_Activo__c = true AND (BI_Segment__c = :SegAccount[0].BI_Segment__c OR BI_Segment__c= null) AND (BI_Country__c != null OR BI_Segment__c != null)) OR Id =:IdsClientes) ORDER BY BI_Posicion__c ASC, Name ASC];
                
                Set<Id>reportIds = new  Set<Id>();
                for(Reportes_PP__c r: reportePP){
                    reportIds.add(r.BI_URL_del_reporte__c);
                    System.debug('++**r.BI_URL_del_reporte__c: ' + r.BI_URL_del_reporte__c);
                }
                
                Map<Id, Report> mapReports=new Map<Id, Report>([SELECT Id,Name, LastModifiedDate FROM Report WHERE Id IN:reportIds]);
                WrapperReport wrp;
                System.debug('+*+*mapReports+*+*: ' + mapReports);
                for(Reportes_PP__c r: reportePP){
                    wrp= new WrapperReport();
                    //wrp.reportName=mapReports.get(r.BI_URL_del_Reporte__c).Name;
                    System.debug('+*+*R+*+*: ' + r);
                    wrp.reportId=mapReports.get(r.BI_URL_del_Reporte__c).Id;
                    wrp.name=r.Name;
                    wrp.lastModified=mapReports.get(r.BI_URL_del_Reporte__c).LastModifiedDate.format();
                    wrp.description=r.BI_Descripcion__c;
                    //wrp.active=r.BI_Activo__c;
                    //wrp.segment=r.BI_Segment__c;
                    //wrp.country=r.BI_Country__c;
                    reports.add(wrp);
                }
                //=======================
                
                if(!RelImg.IsEmpty()){
                    if(RelImg[0].BI_Segment__c!=null){
                        
                        if(RelImg[0].BI_Segment__c=='Empresas'){
                            RelationImageTel = RelImg[0];
                        }else if(RelImg[0].BI_Segment__c=='Negocios'){
                            RelationImageMovi = RelImg[0];
                        }
                    }
                }
            }else{
                    RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'Service Tracking' AND  RecordType.Name= 'General'  AND BI_Segment__c ='Empresas' LIMIT 1];
                    if(!RelImg.IsEmpty()){
                        RelationImageTel = RelImg[0];
                    }else{
                        RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'Service Tracking' AND  RecordType.Name= 'General'  AND BI_Segment__c ='Negocios' LIMIT 1];
                        if(!RelImg.IsEmpty()){
                            RelationImageMovi = RelImg[0];
                        }
                    
                    }
                }
        }
        if(RelationImageTel!=null){
            
            ImgBack = RelationImageTel;
            
            List<Attachment> AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImageTel.Id Limit 1];
            if(!AttTarget.IsEmpty()){
                AttImg = AttTarget[0];
            }
        }else if(RelationImageMovi!=null){
            
            ImgBack = RelationImageMovi;
            
            List<Attachment> AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImageMovi.Id Limit 1];
            if(!AttTarget.IsEmpty()){
                AttImg = AttTarget[0];
            }
        }/*else if(RelationImage!=null){
            
            ImgBack = RelationImage;
            
            Attachment AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImage.Id Limit 1];
            if(AttTarget!=null){
                AttImg = AttTarget;
            }
        
        }*/
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_ServiceTracking.loadInfo', 'Portal Platino', Exc, 'Class');        
        }
    }
    
    
 /* public PageReference checkController(){
        try{
            PageReference pRef = enviarALoginComm(); 
            if(pRef == null || Test.isRunningTest())// Es decir, esta correctamente logado.
            getTokenBO();
            return pRef;
        }catch (exception Exc){
            
            BI_LogHelper.generate_BILog('PCA_ServiceTracking.checkController', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    public void getTokenBO(){
        try{
        String randomCus = String.valueOf(Math.random()); 
        System.debug('randomCus: ' + randomCus);
        Http htt = new Http();
        //String url='http://bo.empresaslatinoamerica.telefonica.com:8080/restproxy/login.jsp?rdm'+randomCus+'&u='+usuarioPortal.Usuario_BO__c+'&p='+usuarioPortal.pass_BO__c;
        String url='http://bo.empresaslatinoamerica.telefonica.com:8080/restproxy/login.jsp?rdm'+randomCus+'&u='+usuarioCustomerPortal.BI_BO_User__c+'&p='+usuarioCustomerPortal.BI_BO_Pwd__c;
         System.debug('url: ' + url);
                
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET'); 
    
        // Send the request, and return a response 
        HttpResponse response;
        if(!Test.isRunningTest()){
            try {
                response = htt.send(req);
            } catch(System.CalloutException e) {System.debug('Error en llamada web service: ' + Exc);}
            if(response!=null)tokenApex = response.getBody();
        
        }else{
            tokenApex=':"W2K8TELATBOAP01:6400@{3&2=313668,U3}##';
        }
        
        // tratar el body
        if(tokenApex!=null && !tokenApex.equalsIgnoreCase('error') )
        tokenApex = tokenApex.substring((tokenApex.indexOf(':"')+2), (tokenApex.length()-2)); 
        System.debug('tokenApex: ' + tokenApex);
        }catch (exception Exc){
            
            BI_LogHelper.generate_BILog('PCA_ServiceTracking.getTokenBO', 'Portal Platino', Exc, 'Class');
        }
    }*/
         
 // {"logonToken":"W2K8TELATBOAP01:6400@{3&2=313668,U3&p=41493.354247581,Y7&4F=124992,U3&63=secLDAP,0P&66=60,03&68=secLDAP:cn%3Davianca%2C ou%3Dusers%2C ou%3Dplatinum%2C dc%3Dbrashp%2C dc%3Dlocal,0P&qe=100,U3&vz=o1.BamX5CJly6pkJyVhHGK2oI1q7VSus_2mKKFngTRk,UP}"} 
        
    public String getTicketsPage() {
        Id pId = UserInfo.getProfileId();
        Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
        if (p.size()>0){
            String pName = p[0].Name;
            System.debug('###Current profile: ' + pName);
            if (pName.startsWith('TGS')){
                System.debug('###Starts with TGS: true');
                //return 'PCA_Cases';
                return 'CWP_Cases'; //changed return of tha method to open the page developer by everis. - everis 23022017
            }
        }
        return 'PCA_ReclamosPedidos';    
    }

    public class WrapperReport{
        //public String reportName{get;set;}
        public String reportID{get;set;}
        public String name{get;set;}
        public String description{get;set;}
        //public boolean active{get;set;}
        //public String segment{get;set;}
        //public String country{get;set;}
        public String lastModified{get;set;}
    }
}