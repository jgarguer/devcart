/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis 
    Company:       Everis
    Description:   Test Methods executed to test CWP_myServicesController.cls 
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    14/03/2016              Everis                   Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class CWP_myServicesController_TEST {
     
    //public CWP_Helper helper{get; set;}
    
    @testSetup 
    private static void dataModelSetup() {  
        
         //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
        
            set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
           //ACCOUNT
        map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
            
          
         //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        insert contactTest;
        
        User usuario;
              
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
    
    }
    
    @isTest
    private static void getButtonVisibilityTest() {     
        system.debug('test 1 ');
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        BI_TestUtils.throw_exception=false;
        System.runAs(usuario){                          
            Test.startTest();                     
            CWP_myServiceController.getButtonVisibility();                  
            Test.stopTest();
        }       
        
    } 
    
     
    @isTest
    private static void getMainImageTest() {     
        BI_TestUtils.throw_exception=false;
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        system.debug('test 2 '+usuario);
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_myServiceController.getMainImage();                  
            Test.stopTest();
        }       
        
    } 
    
    @isTest
    private static void getUserTest() {     
        BI_TestUtils.throw_exception=false;
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        system.debug('test 3 '+usuario);
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_myServiceController.getUser();                  
            Test.stopTest();
        }       
        
    } 
    
    @isTest
    private static void getPermissionSetTest() {     
        BI_TestUtils.throw_exception=false;
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        system.debug('test 4 '+usuario);
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_myServiceController.getPermissionSet();                  
            Test.stopTest();
        }       
        
    } 
    
  }