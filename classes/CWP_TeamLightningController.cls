public without sharing class CWP_TeamLightningController {
    public static User userName {get;Set;}
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   
    Author:        Alberto Pina
    Company:       Everis
    Description:   Method to obtain AccountTeamMembers
    
    History:
    
    <Date>            <Author>              <Description>
    16/03/2017        Everis        		Initial version
    29/06/2017        Daniel Guzman         Telefonica Account Team Member filter for contact with GAM role
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @auraEnabled
    public static List<hierarchyWrapper> getTeam2(){
        list<hierarchyWrapper> listHierarchyWrapper =new list<hierarchyWrapper>();
        List<User> UsersHierarchy = new List<User> ();        
        list<Hierarchy__c> hierarchyList = new list<Hierarchy__c>(); 
        list<Account> accountOwner = new list<Account>(); 
        list<AccountTeamMember> listAccountTeam = new list<AccountTeamMember>();
        list<User> listUsers = new list<User>();
        Id contactAccountId;
        Id idContactFinal;      

        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            string UserId = UserInfo.getUserId(); 
            string AccountId = BI_AccountHelper.getCurrentAccountId();
            
            User user = [Select Id, Name, Profile.Name, ContactId,aboutme, Email,MobilePhone, phone, FullPhotoUrl, Contact.AccountId, UserRole.Name From User where Id =:UserId];
            list<BI_Contact_Customer_Portal__c> attorney = [SELECT Id, BI_Activo__c, BI_Contacto__c, BI_Perfil__c,BI_User__c, BI_Cliente__c FROM BI_Contact_Customer_Portal__c where BI_User__c =: UserId AND BI_Cliente__c=: AccountId];
            
            boolean isAttorney=false;
            
            if(!attorney.IsEmpty()){
                if(attorney[0].BI_Perfil__c==Label.BI_Apoderado){
                    isAttorney = true;      
                }       
                idContactFinal = attorney[0].BI_Contacto__c;
            }
            system.debug('usuario_'+user);
            if(user.ContactId != null){
                contactAccountId = AccountId;
                //Added GAM Filter on query DGM
                listAccountTeam = [Select UserId, toLabel(TeamMemberRole), Id, AccountId, AccountAccessLevel 
                                   From AccountTeamMember
                                   where AccountId= : contactAccountId AND (TeamMemberRole !='GAM' AND Account.RecordType.DeveloperName !='Legal Entity')];
                List<Id> usuari = new list<Id>(); 
                for(AccountTeamMember h : listAccountTeam){
                    usuari.add(h.UserId);
                }
                listUsers = [Select Name,Phone,MobilePhone,Email,AboutMe,UserRole.Name, SmallPhotoUrl, FullPhotoUrl
                             From  User
                             where Id IN :usuari];
                
                Account AccTeam = BI_AccountHelper.getCurrentAccount(BI_AccountHelper.getCurrentAccountId());
                accountOwner = [SELECT Owner.Id,Owner.Name,Owner.Email,Owner.phone,Owner.MobilePhone,Owner.AboutMe,Owner.Profile.Name,Owner.BI_Permisos__c,Owner.SmallPhotoUrl, Owner.FullPhotoUrl FROM Account where Id =: contactAccountId AND (Owner.Profile.Name != 'BI_Inteligencia Comercial' OR Owner.BI_Permisos__c !=: Label.BI_Inteligencia_Comercial) ]; 
                
                if(!accountOwner.IsEmpty()){
                    for(Account g :accountOwner){
                        hierarchyWrapper hWrapper = new hierarchyWrapper();
                        hWrapper.Username2 = g.Owner.Name;
                        hWrapper.UsernameId = g.Owner.Id;
                        hWrapper.email = g.Owner.Email;
                        hWrapper.phone = g.Owner.phone;
                        hWrapper.phoneMobile = g.Owner.MobilePhone;
                        hWrapper.Role = Label.PCA_Propietario;
                        hWrapper.AboutMe = g.Owner.AboutMe !=null?g.Owner.AboutMe:Label.BI_SinDefinir;
                        hWrapper.photo = g.Owner.SmallPhotoUrl;
                        hWrapper.Bigphoto = g.Owner.FullPhotoUrl;
                        hWrapper.showPersonalData = true;
                        system.debug('yu1: '+hWrapper);
                        listHierarchyWrapper.add(hWrapper);
                    }
                }
                
                for(AccountTeamMember h : listAccountTeam){
                    
                    for(User use : listUsers){
                        if(h.UserId==use.Id){
                            hierarchyWrapper hWrapper = new hierarchyWrapper();
                            ConnectApi.UserDetail userDetail;
                            hWrapper.Username2 = use.Name;
                            hWrapper.UsernameId = h.UserId;
                            hWrapper.email = use.Email;
                            hWrapper.phone = use.Phone;
                            hWrapper.phoneMobile = use.MobilePhone;
                            hWrapper.Role = h.TeamMemberRole;
                            hWrapper.AboutMe = use.AboutMe !=null?use.AboutMe:'sin definir';
                            hWrapper.photo =use.SmallPhotoUrl;
                            hWrapper.Bigphoto = use.FullPhotoUrl;
                            hWrapper.showPersonalData = true;
                            system.debug('yu2: '+hWrapper);
                            listHierarchyWrapper.add(hWrapper);
                        } 
                    }
                }
                system.debug(listHierarchyWrapper);
                
                
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.loadInfo', 'Portal Platino', Exc, 'Class');
        }
        
        return listHierarchyWrapper;
    }
    
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   
    Author:        Alberto Pina
    Company:       Everis
    Description:   Method to obtain Escalation Team
    
    History:
    
    <Date>            <Author>              <Description>
    16/03/2017        Everis        		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static List<hierarchyWrapper> getEscalation(){
        list<hierarchyWrapper> listNivelEscaladoWrapper =new list<hierarchyWrapper>();
        list<Hierarchy__c> hierarchyList = new list<Hierarchy__c>(); 
        List<User> UsersHierarchy = new List<User> (); 
        list<Account> accountOwner = new list<Account>(); 
        string AccountId = BI_AccountHelper.getCurrentAccountId();
        Id contactAccountId;
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            
            User user = [Select Id, Name, Profile.Name, ContactId,aboutme, Email,MobilePhone, phone, FullPhotoUrl, Contact.AccountId, UserRole.Name From User where id = : Userinfo.getUserId()];
            //User user = [Select Id, Name, Profile.Name, ContactId,aboutme, Email,MobilePhone, phone, FullPhotoUrl, Contact.AccountId, UserRole.Name From User where name = 'Demo 1 Varias familias'];
            
            Id idContactFinal;
            //list<BI_Contact_Customer_Portal__c> attorney = [SELECT Id, BI_Activo__c, BI_Contacto__c, BI_Perfil__c,BI_User__c, BI_Cliente__c FROM BI_Contact_Customer_Portal__c where BI_User__c =: Userinfo.getUserId() AND BI_Cliente__c=: AccountId];
            list<BI_Contact_Customer_Portal__c> attorney = [SELECT Id, BI_Activo__c, BI_Contacto__c, BI_Perfil__c,BI_User__c, BI_Cliente__c FROM BI_Contact_Customer_Portal__c where BI_User__c =: user.id AND BI_Cliente__c=: AccountId];
            system.debug('*****attorney' +attorney);
            boolean isAttorney=false;
            
            if(!attorney.IsEmpty()){
                if(attorney[0].BI_Perfil__c==Label.BI_Apoderado){
                    isAttorney = true;      
                }       
                idContactFinal = attorney[0].BI_Contacto__c;
            }
            
            /*contactAccountId = user.Contact.AccountId;
accountOwner = [SELECT Owner.Id,Owner.Name,Owner.Email,Owner.phone,Owner.MobilePhone,Owner.AboutMe,Owner.Profile.Name,Owner.BI_Permisos__c,Owner.SmallPhotoUrl, Owner.FullPhotoUrl FROM Account where Id =: contactAccountId ]; 
if(!accountOwner.IsEmpty()){
for(Account g :accountOwner){
hierarchyWrapper hWrapper = new hierarchyWrapper();
hWrapper.Username2 = g.Owner.Name;
hWrapper.UsernameId = g.Owner.Id;
hWrapper.email = g.Owner.Email;
hWrapper.phone = g.Owner.phone;
hWrapper.phoneMobile = g.Owner.MobilePhone;
hWrapper.Role = Label.PCA_Propietario;
hWrapper.AboutMe = g.Owner.AboutMe !=null?g.Owner.AboutMe:Label.BI_SinDefinir;
hWrapper.photo = g.Owner.SmallPhotoUrl;
hWrapper.Bigphoto = g.Owner.FullPhotoUrl;
hWrapper.showPersonalData = true;
system.debug('yu1: '+hWrapper);
listHierarchyWrapper.add(hWrapper);
}
}*/
            //prueba con datos
            idContactFinal=user.ContactId;
            
            hierarchyList = [Select Id, AboutMe__c, MobilePhone__c, ShowPersonalData__c,  Description__c, User__c, NameUser__c, Contact__c, User__r.SmallPhotoUrl, User__r.FullPhotoUrl,
                             Email__c, Phone__c, Nivel_de_escalado__c, Role__c From Hierarchy__c where Contact__c = : idContactFinal and Nivel_de_escalado__c != null and Nivel_de_escalado__c != '' order by Nivel_de_escalado__c];
            List<Id> IdsUsers = new List<Id>();
            if(!hierarchyList.isEmpty()){
                for(Hierarchy__c h :hierarchyList){
                    IdsUsers.add(h.User__c);                
                }
                UsersHierarchy = [Select Id, Email, Phone, MobilePhone From User Where Id IN :IdsUsers];
            }
            
            listNivelEscaladoWrapper = new list<hierarchyWrapper>();
            if(!hierarchyList.isEmpty()){       
                for(Hierarchy__c h : hierarchyList){
                    for(User user2 :UsersHierarchy){
                        if(user2.Id == h.User__c){
                            //getUserConnectApi(h.User__c);
                            hierarchyWrapper hWrapper = new hierarchyWrapper();
                            
                            hWrapper.Username2 = h.NameUser__c;
                            hWrapper.UsernameId = h.User__c;
                            
                            hWrapper.Description = h.Role__c!=null ?h.Role__c:Label.BI_PorDefinir;
                            if(h.ShowPersonalData__c==false){
                                hWrapper.email = Label.PCA_NoDisponible;
                                hWrapper.phone = Label.PCA_NoDisponible;
                                hWrapper.phoneMobile = Label.PCA_NoDisponible;
                            }
                            else{
                                hWrapper.email = user2.Email;
                                hWrapper.phoneMobile = user2.MobilePhone;
                                hWrapper.phone = user2.Phone;
                            }
                            hWrapper.Role = h.Role__c;
                            //hWrapper.Description = h.Description__c!=null ?h.Description__c:'por definir';
                            hWrapper.AboutMe = h.AboutMe__c;
                            hWrapper.photo = h.User__r.SmallPhotoUrl;
                            hWrapper.Bigphoto = h.User__r.FullPhotoUrl;
                            hWrapper.NivelEscalado = h.Nivel_de_escalado__c;
                            listNivelEscaladoWrapper.add(hWrapper);
                        }
                    }
                }       
            }
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.loadInfo', 'Portal Platino', Exc, 'Class');
        }
        return listNivelEscaladoWrapper;
    }

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   
    Author:        Alberto Pina
    Company:       Everis
    Description:   Method to obtain Escalation Team
    
    History:
    
    <Date>            <Author>              <Description>
    16/03/2017        Everis        		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static String getContact(String iden){
        Contact contacto = [Select Id,Name, Email, Phone, MobilePhone, BI_Activo__c, Title FROM Contact Where Id =:iden limit 1];
        return contacto.Id;
    }    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   
    Author:        Alberto Pina
    Company:       Everis
    Description:   Class from Account Team Members
    
    History:
    
    <Date>            <Author>              <Description>
    16/03/2017        Everis        		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    //Clase del telefónica team
    public class  hierarchyWrapper{
        @auraEnabled
        public String UsernameId { get; set; }
        @auraEnabled
        public String Username2 { get; set; }
        @auraEnabled
        public String Description { get; set; }
        @auraEnabled
        public string email { get; set; }
        @auraEnabled
        public string phone { get; set; }
        @auraEnabled
        public string phoneMobile { get; set; }
        @auraEnabled
        public string photo { get; set; }
        @auraEnabled
        public string Bigphoto { get; set; }
        @auraEnabled
        public string NivelEscalado { get; set; }
        @auraEnabled
        public boolean showPersonalData { get; set; }
        @auraEnabled
        public string AboutMe { get; set; }
        @auraEnabled
        public string Role { get; set; }
    }
  
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   
    Author:        Alberto Pina
    Company:       Everis
    Description:   Class from Escalation Team
    
    History:
    
    <Date>            <Author>              <Description>
    16/03/2017        Everis        		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
      public class  detailsUserWrapper{
        @auraEnabled
        public String UsernameId { get; set; }
        @auraEnabled
        public String Username3 { get; set; }
        @auraEnabled
        public String Description { get; set; }
        @auraEnabled
        public string email { get; set; }
        @auraEnabled
        public string phone { get; set; }
        @auraEnabled
        public string phoneMobile { get; set; }
        @auraEnabled
        public string photo { get; set; }
        @auraEnabled
        public string Bigphoto { get; set; }
        @auraEnabled
        public boolean showPersonalData { get; set; }
        @auraEnabled
        public string AboutMe { get; set; }
        @auraEnabled
        public string Role { get; set; }
    }

}