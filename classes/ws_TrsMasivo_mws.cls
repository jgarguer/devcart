/**
* Avanxo Colombia
* @author 			OJCB
* Project:			Telefonica
* Description:		Clase tipo WebServiceMock del servicio NotificacionesSalesForceWSDL.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2015-08-19		OJCB      Definicion de la clase implments WebServiceMock
*************************************************************************************************************/
@isTest
global class ws_TrsMasivo_mws implements WebServiceMock {

	global Map<String, Object> mapWSResponseBySOAPAction									= new Map<String, Object>();

    global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){
		
		//serviciosSISGOTSOAP
		ws_TrsMasivo.instalacionesResponseArray_element objInstalacionesResponseArray_element =new ws_TrsMasivo.instalacionesResponseArray_element();
		ws_TrsMasivo.InstalacionResponseDataType objInst = new ws_TrsMasivo.InstalacionResponseDataType();
		List<ws_TrsMasivo.InstalacionResponseDataType> lstObjInst =new List<ws_TrsMasivo.InstalacionResponseDataType>(); 
		objInst.codigo='Codigo';
        objInst.error='error';
		lstObjInst.add(objInst);
		objInstalacionesResponseArray_element.instalacionResponse=lstObjInst;
		ws_TrsMasivo.RespuestaProductoPDataType[] lstWSArrayOfRespuestaProductoP	= new List<ws_TrsMasivo.RespuestaProductoPDataType>();
		ws_TrsMasivo.RespuestaProductoPDataType wsArrayOfRespuestaProductoP			= new ws_TrsMasivo.RespuestaProductoPDataType();
		wsArrayOfRespuestaProductoP.cts_name='P - 000100001';
		wsArrayOfRespuestaProductoP.cts_id=1;
		wsArrayOfRespuestaProductoP.codigoError=1;
		wsArrayOfRespuestaProductoP.respuestaAccion='';		
		lstWSArrayOfRespuestaProductoP.add( wsArrayOfRespuestaProductoP );
		
		//serviciosSISGOTSOAP
		//actualizarQuiebre
		ws_TrsMasivo.codigosRespuesta[] lstWScodigosRespuesta	= new List<ws_TrsMasivo.codigosRespuesta>();
		ws_TrsMasivo.codigosRespuesta wscodigosRespuesta		= new ws_TrsMasivo.codigosRespuesta();
        wscodigosRespuesta.codigoTRS=1;
        wscodigosRespuesta.Ds='1';
        wscodigosRespuesta.Ms='1';
        wscodigosRespuesta.codigoError='1';
        wscodigosRespuesta.mensajeError='1';
        lstWScodigosRespuesta.add(wscodigosRespuesta);
		
		//actualizarQuiebre
		ws_TrsMasivo.quiebreResponse_element response_elem = new ws_TrsMasivo.quiebreResponse_element();
		response_elem.respuestaQuiebre = lstWScodigosRespuesta;
		
		//ws_TrsMasivo.ArrayOfRespuestacrearProyecto_element
		ws_TrsMasivo.ArrayOfRespuestacrearProyecto_element objArrayOfRespuestacrearProyecto_element = new ws_TrsMasivo.ArrayOfRespuestacrearProyecto_element();
		ws_TrsMasivo.crearProyectoResponse objCrearProyectoResponse = new ws_TrsMasivo.crearProyectoResponse();
        objCrearProyectoResponse.estado='estado';
        objCrearProyectoResponse.mensajeError='Error';
        List<ws_TrsMasivo.crearProyectoResponse> lstProResp= new List<ws_TrsMasivo.crearProyectoResponse>();
        lstProResp.add(objCrearProyectoResponse);
		objArrayOfRespuestacrearProyecto_element.proyectoRespuesta=lstProResp;
		//ws_TrsMasivo.ArrayOfRespuestacrearProyecto_element
		
		ws_TrsMasivo.ArrayOfRespuestaProductoP_element objArrayOfRespuestaProductoP_element = new ws_TrsMasivo.ArrayOfRespuestaProductoP_element();
		objArrayOfRespuestaProductoP_element.Respuesta = lstWSArrayOfRespuestaProductoP;

		List<ws_TrsMasivo.RespuestaDataType> lstRespuestaDataType = new List<ws_TrsMasivo.RespuestaDataType>();
		ws_TrsMasivo.RespuestaDataType objRespuestaDataType = new ws_TrsMasivo.RespuestaDataType();
        objRespuestaDataType.codigo = '1';
        objRespuestaDataType.error = '1';
        lstRespuestaDataType.add(objRespuestaDataType);

		ws_TrsMasivo.ArrayOfRespuesta_element objArrayOfRespuesta_element = new ws_TrsMasivo.ArrayOfRespuesta_element();
		objArrayOfRespuesta_element.Respuesta = lstRespuestaDataType;

		mapWSResponseBySOAPAction.put( 'ArrayOfCodProductoP', lstWSArrayOfRespuestaProductoP );
		mapWSResponseBySOAPAction.put( 'actualizarQuiebreRequest', response_elem );
		mapWSResponseBySOAPAction.put( 'crearQuiebreRequest', response_elem );
		mapWSResponseBySOAPAction.put( 'instalacionesRequestArray', objInstalacionesResponseArray_element );
		mapWSResponseBySOAPAction.put( 'ArrayOfcrearProyecto', objArrayOfRespuestacrearProyecto_element );
		mapWSResponseBySOAPAction.put( 'ArrayOfProductoP', objArrayOfRespuestaProductoP_element );

		
		response.put( 'response_x', mapWSResponseBySOAPAction.get( requestName ) );
	}
}