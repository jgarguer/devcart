global with sharing class BI2_CountCasesLastMonth_JOB implements Schedulable {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Scheduled class that counts the number of cases of each contact for the current month

	 History:

	 <Date>				<Author>				<Description>
	 16/09/2015			Jose Miguel Fierro		Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	Id lastId;
	public BI2_CountCasesLastMonth_JOB() {
	}
	public BI2_CountCasesLastMonth_JOB(Id lastId) {
		this.lastId = lastId;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Method that executes the job

	 IN:			Schedulable context (Parameters to schedule the job)
	 OUT:			Void

	 History:

	 <Date>				<Author>				<Description>
	 16/09/2015			Jose Miguel Fierro		Initial version.
	 29/10/2015			Jose Miguel Fierro		Added BI2 filter
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global void execute(SchedulableContext sc) {
        try {
            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');
            try {System.abortJob(sc.getTriggerId());}catch(exception exc) {}
    
            List<Contact> lst_contacts = new List<Contact>();
            
            if(lastId==null) {
                lst_contacts.addAll([SELECT Id, BI2_N_Casos_Ultimo_Mes__c, (SELECT Id FROM Cases WHERE CreatedDate=LAST_N_DAYS:30 AND RecordType.DeveloperName IN ('BI2_caso_comercial','BI2_Caso_Padre')) FROM Contact ORDER BY Id ASC LIMIT 2]);
            } else {
                lst_contacts.addAll([SELECT Id, BI2_N_Casos_Ultimo_Mes__c, (SELECT Id FROM Cases WHERE CreatedDate=LAST_N_DAYS:30 AND RecordType.DeveloperName IN ('BI2_caso_comercial','BI2_Caso_Padre')) FROM Contact WHERE Id > :lastId ORDER BY Id ASC LIMIT 2]);
                if(lst_contacts.isEmpty()) {
                    System.abortJob(sc.getTriggerId());
                }
            }
    
            System.debug('***lst_contacts: ' + lst_contacts);
            if(!lst_contacts.isEmpty()) {
                lastId = lst_contacts.get(lst_contacts.size() - 1).Id;
                countCases(lst_contacts);
                System.schedule('BI2_CountCasesLastMonth_JOB ' + System.now(), generateCronStr(Datetime.now().addSeconds(2)), new BI2_CountCasesLastMonth_JOB(lastId));
            }
        } catch (Exception exc) {
            BI_LogHelper.generate_BILog('BI2_CountCasesLastMonth_JOB.execute', 'BI_EN', exc, 'Apex Job');
        }
	}
//
	public static void countCases(List<Contact> lst_contacts) {
		List<Contact> lst_2update = new List<Contact>();
		for(Contact con : lst_contacts) {
			Integer numCas = con.Cases.size();
			con.BI2_N_Casos_Ultimo_Mes__c = numCas;
			lst_2update.add(con);
		}
		update lst_2update;
	}

//
	public static String generateCronStr(Datetime jobT){
		try{
		    return jobT.second() + ' ' + jobT.minute() + ' ' + jobT.hour() + ' ' + jobT.day() + ' ' + jobT.month() + ' ? ' + jobT.year();
		}catch (Exception e){
		   return null;
		}
	}
}