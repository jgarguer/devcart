@isTest
private class PCA_CaseHelper_TEST {

    static testMethod void saveCase() {
    	
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
    	User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
    	//User usr2 = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(2);
	    	List<Account> acc = BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(2, accList);
            system.debug('con: '+con);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());
            User user2 = BI_DataLoad.loadSeveralPortalUsers(con[1].Id, BI_DataLoad.searchPortalProfile());           
            system.debug('user1: '+user1);
            system.debug('user2: '+user2);
	    	system.runAs(user1){
	            BI_TestUtils.throw_exception = false;
	    		String mex = Label.BI_Mexico;
		    	Case Case1 = new Case(AccountId=acc[0].Id,BI_Country__c=mex,BI_Otro_Tipo__c = 'test',Reason='Reporte de avería',Status='Nuevo',Type='Reclamo',BI_Categoria_del_caso__c='Nivel 1',Priority='Media',Origin='Portal cliente',Subject='Asunto prueba');	            	            
	            PCA_CaseHelper.saveCase(Case1);
	           system.assertEquals(Case1.AccountId, acc[0].Id);
	    	} 
	    	system.runAs(user2){	    		     
	            BI_TestUtils.throw_exception = true;
	    		String mex = Label.BI_Mexico;
		    	Case Case2 = new Case(AccountId=acc[1].Id,BI_Country__c=mex,BI_Otro_Tipo__c = 'test',Reason='Reporte de avería2',Status='Nuevo2',Type='Reclamo',BI_Categoria_del_caso__c='Nivel 1',Priority='Media',Origin='Portal cliente',Subject='Asunto prueba2');	            	            
	            PCA_CaseHelper.saveCase(Case2);	       
	    	}
	    	                
	    }  	   
    	
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}