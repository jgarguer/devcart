@isTest
private class BI_Campanas_CuentaMethods_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_Campanas_CuentaMethods class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    13/11/2014              Pablo Oliva		        Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_Campanas_CuentaMethods.updateCampanaField
   
    History:
    
    <Date>                  <Author>                <Change Description>
    13/11/2014              Pablo Oliva             Initial Version   
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void updateCampanaField(){
    	
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List <Account> lst_acc = BI_DataLoad.loadAccounts(3, lst_pais);
        
        Set<Id> set_accounts = new Set<Id>();
        
        for(Account acc:lst_acc)
        	set_accounts.add(acc.Id);
        
        List <Campaign> lst_camp = BI_DataLoad.loadCampaigns(3);
        
        List<BI_Campanas_Cuenta__c> lst_campc = new List<BI_Campanas_Cuenta__c>();
        
        for(Account acco:lst_acc){
        	for(Campaign camp:lst_camp){
        		
        		BI_Campanas_Cuenta__c campc = new BI_Campanas_Cuenta__c(Name = acco.Id+'-'+camp.Id,
        																BI_Quantity__c = 1);
        																
        		lst_campc.add(campc);
        		
        	}
        }
        
        Test.startTest();
        
        insert lst_campc;
        
        for(Account acco2:[select Id, BI_PER_Campana__c from Account where Id IN :set_accounts])
        	system.assertEquals(lst_camp.size(), acco2.BI_PER_Campana__c.split(',').size());
        	
        delete lst_campc;
        
        for(Account acco3:[select Id, BI_PER_Campana__c from Account where Id IN :set_accounts])
        	system.assertEquals(null, acco3.BI_PER_Campana__c);
        
       	Test.stopTest();
    	
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Raul Aguera
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_Campanas_CuentaMethods.updateCampanaField with different campaigns
   
    History:
    
    <Date>                  <Author>                <Change Description>
    05/06/2014              Raul Aguera             Initial Version   
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void updateCampanaRelatedAccount(){
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List <Account> lst_acc = BI_DataLoad.loadAccounts(3, lst_pais);
        
        Set<Id> set_accounts = new Set<Id>();
        
        for(Account acc:lst_acc)
            set_accounts.add(acc.Id);
        
        List <Campaign> lst_camp = BI_DataLoad.loadCampaigns(1);
        
        List<BI_Campanas_Cuenta__c> lst_campc = new List<BI_Campanas_Cuenta__c>();
        
        for(Account acco:lst_acc){
            for(Campaign camp:lst_camp){
                
                BI_Campanas_Cuenta__c campc = new BI_Campanas_Cuenta__c(Name = acco.Id+'-'+camp.Id,
                                                                        BI_Quantity__c = 1);
                                                                        
                lst_campc.add(campc);
                
            }
        }

        Test.startTest();
        
        insert lst_campc;

        for(Account acco2:[select Id, BI_PER_Campana__c from Account where Id IN :set_accounts])
            system.assertEquals(lst_camp.size(), acco2.BI_PER_Campana__c.split(',').size());

        List <Campaign> lst_camp2 = BI_DataLoad.loadCampaigns(1);

        lst_campc = new List<BI_Campanas_Cuenta__c>();
        for(Account acco:lst_acc){
            for(Campaign camp:lst_camp2){
                
                BI_Campanas_Cuenta__c campc = new BI_Campanas_Cuenta__c(Name = acco.Id+'-'+camp.Id,
                                                                        BI_Quantity__c = 1);
                lst_campc.add(campc);
               
            }
        }

        insert lst_campc;

        for(Account acco3:[select Id, BI_PER_Campana__c from Account where Id IN :set_accounts])
            system.assertEquals(lst_camp.size()+lst_camp2.size(), acco3.BI_PER_Campana__c.split(',').size());

        delete lst_campc;
        
        for(Account acco4:[select Id, BI_PER_Campana__c from Account where Id IN :set_accounts])
            system.assertEquals( lst_camp[0].Id, acco4.BI_PER_Campana__c );

        Test.stopTest();
}
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}