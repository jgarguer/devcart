public without sharing class BI_FacturacionMethods {
	 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Methods executed by BI_Facturacion Triggers 
    Test Class:    BI_FacturacionMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    19/05/2014              Pablo Oliva          	Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	 
 	
 	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that creates a Task related to the BI_Facturacion object.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    23/07/2014              Micah Burgos         	Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void createTask(List<BI_Facturacion__c> news){ 
	
		try{
			
			Set<Id> set_acc = new Set<Id>();
			for(BI_Facturacion__c fact :news){
				set_acc.add(fact.BI_Cliente__c);
			}

			
			if(!set_acc.isEmpty()){ 
				
				Map<Id, Account> map_accounts = new Map<Id, Account>([select Id, BI_Country__c, OwnerId
																	  from Account where Id IN :set_acc]);
				
				List<AccountTeamMember> lst_accTM = new List<AccountTeamMember>([SELECT Id,TeamMemberRole,UserId, AccountID, Account.Name, User.Name 
																				FROM AccountTeamMember
																				WHERE AccountID IN :map_accounts.keySet() 
																					AND TeamMemberRole = :Label.BI_RolEjecutivoCobros]);
																					
				System.debug('BI_FacturacionMethods->createTask()->lst_accTM: '+lst_accTM);
			 	
					
																			
				Map<Id, Id> map_AccID_AccTMUser = new Map<Id,Id>();																	
				for(AccountTeamMember accTM :lst_accTM){
					map_AccID_AccTMUser.put(accTM.AccountID,accTM.UserID);
				}
				
				
				
				set<String>set_paisRefAcc = new set<String>();
				Map<Id, Id> map_AccOwnerOfTask = new Map<Id,Id>();
				
				for(Account acc :map_accounts.values() ){
					
					if(map_AccID_AccTMUser.containsKey(acc.ID)){
						if(map_AccID_AccTMUser.get(acc.Id) != null){
							map_AccOwnerOfTask.put(acc.Id,map_AccID_AccTMUser.get(acc.Id));
						}else{
							map_AccOwnerOfTask.put(acc.Id, acc.OwnerId); 
						}
					}else{
						map_AccOwnerOfTask.put(acc.Id, acc.OwnerId);
					}
					
					set_paisRefAcc.add(acc.BI_Country__c);
				}
				
				list<String> lst_paisRefAcc = new List<String>(set_paisRefAcc);
				
				System.debug('BI_FacturacionMethods->createTask()->lst_accTM: '+map_AccOwnerOfTask);
				
				List<BI_Tarea_Obligatoria__c> lst_to = new List<BI_Tarea_Obligatoria__c>([SELECT BI_Asunto__c,BI_Ciclo_ventas__c,
																							BI_Criterio_de_creacion__c,BI_Descripcion__c,BI_Dias_Vencimiento__c,BI_Estado_tarea__c,BI_Etapa__c
																							,BI_Country__c,BI_PK2__c,BI_PK__c,BI_Prioridad__c,BI_Rol_propietario__c,BI_Typology__c,
																							BI_Tipo__c,BI_Tramo_de_deuda__c,BI_Ultrarapida__c,Id,Name 
																							FROM BI_Tarea_Obligatoria__c
																							WHERE BI_Typology__c = :Label.BI_TipologiaTareaDeudas
																						    AND BI_Country__c IN :lst_paisRefAcc]);
				
				
				System.debug('BI_FacturacionMethods->createTask()->set_paisRefAcc: '+set_paisRefAcc);																				
				System.debug('BI_FacturacionMethods->createTask()->lst_to: '+lst_to);
				
																							
				if(!lst_to.isEmpty()){
					/* Modified Raul Aguera 27/05/2015 */
					//map<Id, list<map<String,list<BI_Tarea_Obligatoria__c>>>> map_Pais_Todo = new map<Id, list<map<String,list<BI_Tarea_Obligatoria__c>>>>();
					map<String, list<map<String,list<BI_Tarea_Obligatoria__c>>>> map_Pais_Todo = new map<String, list<map<String,list<BI_Tarea_Obligatoria__c>>>>();					
					/* End Modified Raul Aguera 27/05/2015 */

					for(BI_Tarea_Obligatoria__c tarea_o :lst_to){

						if(map_Pais_Todo.containsKey(tarea_o.BI_Country__c)){
							
							
							Boolean found = false; 
							for(map<String,list<BI_Tarea_Obligatoria__c>> tramo : map_Pais_Todo.get(tarea_o.BI_Country__c)){
								
								if(tramo.containsKey(tarea_o.BI_Criterio_de_creacion__c)){
									tramo.get(tarea_o.BI_Criterio_de_creacion__c).add(tarea_o); 
									found= true;	
								}
							}
							
							if(!found){
								map<String,list<BI_Tarea_Obligatoria__c>> map_add_Tramo = new map<String,list<BI_Tarea_Obligatoria__c>>();
								list<BI_Tarea_Obligatoria__c> list_toAdd = new list<BI_Tarea_Obligatoria__c>();
								list_toAdd.add(tarea_o);
								map_add_Tramo.put(tarea_o.BI_Criterio_de_creacion__c, list_toAdd);
								map_Pais_Todo.get(tarea_o.BI_Country__c).add(map_add_Tramo);
							}
						
	
						}else{
							list<map<String,list<BI_Tarea_Obligatoria__c>>>list_maps = new list<map<String,list<BI_Tarea_Obligatoria__c>>>();
							map<String,list<BI_Tarea_Obligatoria__c>> map_add_Tramo = new map<String,list<BI_Tarea_Obligatoria__c>>();
							list<BI_Tarea_Obligatoria__c> list_toAdd = new list<BI_Tarea_Obligatoria__c>();
							
							list_toAdd.add(tarea_o);
							map_add_Tramo.put(tarea_o.BI_Criterio_de_creacion__c, list_toAdd);
							list_maps.add(map_add_Tramo);
							map_Pais_Todo.put(tarea_o.BI_Country__c, list_maps);
						}	
						
					}
					
					System.debug('BI_FacturacionMethods->createTask()->MAPA DE TAREAS(map_Pais_Todo): '+map_Pais_Todo);			

					String dias30 = 'Con deuda <30 días';
					String dias60 = 'Con deuda 30-60 días';
					String dias90 = 'Con deuda 60-90 días';
					String dias120 = 'Con deuda 90-120 días';
					String dias121 = 'Con deuda >120 días';
					
					List<Task> lst_task = new List<Task>();
					for(BI_Facturacion__c fact :news){

						if(map_Pais_Todo.containsKey(fact.BI_Country__c)){//Si existen plantillas task para éste País
							
							
							for(map<String,list<BI_Tarea_Obligatoria__c>> tramo : map_Pais_Todo.get(fact.BI_Country__c)){
								
								if(fact.BI_Deuda_0_30_dias__c != null && fact.BI_Deuda_0_30_dias__c != 0 && tramo.containsKey(dias30) ){
									
									for(BI_Tarea_Obligatoria__c tarea :tramo.get(dias30)){
										Task tsk = new Task(WhatId = fact.Id,
													  Subject = tarea.BI_Asunto__c,
													  Priority = tarea.BI_Prioridad__c,
													  Description = tarea.BI_Descripcion__c,
													  Status = tarea.BI_Estado_tarea__c,
													  ActivityDate = Date.today().addDays(Integer.valueOf(tarea.BI_Dias_Vencimiento__c)),
													  Type = tarea.BI_Tipo__c, //'Tarea Obligatoria',
													  OwnerId = map_AccOwnerOfTask.get(fact.BI_Cliente__c));
									
										System.debug('BI_FacturacionMethods->createTask()->ADD dias30 tsk: '+tsk);	
										lst_task.add(tsk);
									}
									
								}
								
								if(fact.BI_Deuda_30_60_dias__c  != null && fact.BI_Deuda_30_60_dias__c  != 0 && tramo.containsKey(dias60) ){
									for(BI_Tarea_Obligatoria__c tarea :tramo.get(dias60)){
										Task tsk = new Task(WhatId = fact.Id,
													  Subject = tarea.BI_Asunto__c,
													  Priority = tarea.BI_Prioridad__c,
													  Description = tarea.BI_Descripcion__c,
													  Status = tarea.BI_Estado_tarea__c,
													  ActivityDate = Date.today().addDays(Integer.valueOf(tarea.BI_Dias_Vencimiento__c)),
													  Type = tarea.BI_Tipo__c, //'Tarea Obligatoria',
													  OwnerId = map_AccOwnerOfTask.get(fact.BI_Cliente__c));
									
										System.debug('BI_FacturacionMethods->createTask()->ADD dias60 tsk: '+tsk);	
										lst_task.add(tsk);
									}
									
								}
								
								if(fact.BI_Deuda_60_90_dias__c  != null && fact.BI_Deuda_60_90_dias__c  != 0 && tramo.containsKey(dias90)){
									for(BI_Tarea_Obligatoria__c tarea :tramo.get(dias90)){
										Task tsk = new Task(WhatId = fact.Id,
													  Subject = tarea.BI_Asunto__c,
													  Priority = tarea.BI_Prioridad__c,
													  Description = tarea.BI_Descripcion__c,
													  Status = tarea.BI_Estado_tarea__c,
													  ActivityDate = Date.today().addDays(Integer.valueOf(tarea.BI_Dias_Vencimiento__c)),
													  Type = tarea.BI_Tipo__c, //'Tarea Obligatoria',
													  OwnerId = map_AccOwnerOfTask.get(fact.BI_Cliente__c));
									
										System.debug('BI_FacturacionMethods->createTask()->ADD dias90 tsk: '+tsk);	
										lst_task.add(tsk);
									}
									
								}
								if(fact.BI_Deuda_90_120_dias__c  != null && fact.BI_Deuda_90_120_dias__c  != 0 && tramo.containsKey(dias120)){
									for(BI_Tarea_Obligatoria__c tarea :tramo.get(dias120)){
										Task tsk = new Task(WhatId = fact.Id,
													  Subject = tarea.BI_Asunto__c,
													  Priority = tarea.BI_Prioridad__c,
													  Description = tarea.BI_Descripcion__c,
													  Status = tarea.BI_Estado_tarea__c,
													  ActivityDate = Date.today().addDays(Integer.valueOf(tarea.BI_Dias_Vencimiento__c)),
													  Type = tarea.BI_Tipo__c, //'Tarea Obligatoria',
													  OwnerId = map_AccOwnerOfTask.get(fact.BI_Cliente__c));
									
										System.debug('BI_FacturacionMethods->createTask()->ADD dias120 tsk: '+tsk);	
										lst_task.add(tsk);
									}
									
								}
								if(fact.BI_Deuda_mayor_120_dias__c  != null && fact.BI_Deuda_mayor_120_dias__c  != 0 && tramo.containsKey(dias121) ){
									for(BI_Tarea_Obligatoria__c tarea :tramo.get(dias121)){
										Task tsk = new Task(WhatId = fact.Id,
													  Subject = tarea.BI_Asunto__c,
													  Priority = tarea.BI_Prioridad__c,
													  Description = tarea.BI_Descripcion__c,
													  Status = tarea.BI_Estado_tarea__c,
													  ActivityDate = Date.today().addDays(Integer.valueOf(tarea.BI_Dias_Vencimiento__c)),
													  Type = tarea.BI_Tipo__c, //'Tarea Obligatoria',
													  OwnerId = map_AccOwnerOfTask.get(fact.BI_Cliente__c));
									
										System.debug('BI_FacturacionMethods->createTask()->ADD dias121 tsk: '+tsk);	
										lst_task.add(tsk);
									}
								}
							}
						}
					}
					
					
					System.debug('BI_FacturacionMethods->createTask()->lst_task(to INS): '+lst_task);
					insert lst_task;

				}		
			}
		}catch (exception Exc){
			BI_LogHelper.generate_BILog('BI_FacturacionMethods.createTask', 'BI_EN', Exc, 'Trigger');
		}
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that update field BI_Contabiliza_registro__c to false Before insert the news BI_Facturacion__c sObjects.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    04/09/2014              Micah Burgos         	Initial Version
    18/09/2014				Pablo Oliva
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void checkMaxDate(List<BI_Facturacion__c> news){
		try{
			Map<Id, Date> map_old = new Map<Id, Date>();
			Map<Id, Date> map_new = new Map<Id, Date>();
			Map<Id, Date> map_global = new Map<Id, Date>();
			
			set<Id> accIDs = new set<Id>();
			 
			for(BI_Facturacion__c fact :news){
				//fact.BI_Contabiliza_registro__c = true;
				accIDs.add(fact.BI_Cliente__c); 
				
				if(!map_new.keySet().contains(fact.BI_Cliente__c))
					map_new.put(fact.BI_Cliente__c, fact.BI_Fecha_de_carga__c);
				else{
					if(fact.BI_Fecha_de_carga__c > map_new.get(fact.BI_Cliente__c))
						map_new.put(fact.BI_Cliente__c, fact.BI_Fecha_de_carga__c);
				}
				
			}
			
			List<BI_Facturacion__c> oldsFacts = new List<BI_Facturacion__c>([SELECT Id, Name,BI_Contabiliza_registro__c, BI_Cliente__c, BI_Fecha_de_carga__c FROM BI_Facturacion__c WHERE BI_Cliente__c IN :accIDs ]);
			
			for(BI_Facturacion__c fact_old:oldsFacts){
				if(!map_old.keySet().contains(fact_old.BI_Cliente__c))
					map_old.put(fact_old.BI_Cliente__c, fact_old.BI_Fecha_de_carga__c);
				else{
					if(fact_old.BI_Fecha_de_carga__c > map_old.get(fact_old.BI_Cliente__c))
						map_old.put(fact_old.BI_Cliente__c, fact_old.BI_Fecha_de_carga__c);
				}
			}
			
			for(Id idAcc:accIDs){
				
				if(map_old.get(idAcc) == null)
					map_global.put(idAcc, map_new.get(idAcc));
				else if(map_old.get(idAcc) > map_new.get(idAcc))
					map_global.put(idAcc, map_old.get(idAcc));
				else 
					map_global.put(idAcc, map_new.get(idAcc));
			}
			
			//MAP_GLOBAL OK
			
			system.debug('map global: '+map_global);
			
			for(BI_Facturacion__c fact_new:news){
				
				if(fact_new.BI_Fecha_de_carga__c == map_global.get(fact_new.BI_Cliente__c))
					fact_new.BI_Contabiliza_registro__c = true;
				else
					fact_new.BI_Contabiliza_registro__c = false;
				
			}
			
			list<BI_Facturacion__c> lst_factOldToUpdate = new list<BI_Facturacion__c>();
			
			for(BI_Facturacion__c oldFact:oldsFacts){
				
				if(oldFact.BI_Fecha_de_carga__c == map_global.get(oldFact.BI_Cliente__c))
					oldFact.BI_Contabiliza_registro__c = true;
				else
					oldFact.BI_Contabiliza_registro__c = false;
					
				lst_factOldToUpdate.add(oldFact);
			}
			
			update lst_factOldToUpdate;


		}catch (exception Exc){
			BI_LogHelper.generate_BILog('BI_FacturacionMethods.checkMaxDate', 'BI_EN', Exc, 'Trigger');
		}
	}
}