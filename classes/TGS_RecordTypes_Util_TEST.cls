@isTest
/*------------------------------------------------------------------------
Author:        	Jose Lopez
Company:       	Deloitte
Description:   	Test class to manage the coverage code for the class TGS_RecordTypes_Util
History
<Date>      	<Author>     	<Description>
15-Jun-15		Jose Lopez	Initial Version
----------------------------------------------------------------------------*/

public class TGS_RecordTypes_Util_TEST {
    static testmethod void testGetRecordTypeIdsByDeveloperName(){
        Map<String, Id> result=TGS_RecordTypes_Util.getRecordTypeIdsByDeveloperName(Account.SObjectType);
        System.assert(result.containsKey(Constants.RECORD_TYPE_TGS_HOLDING));
        System.assert(result.containsKey(Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY));
        System.assert(result.containsKey(Constants.RECORD_TYPE_TGS_LEGAL_ENTITY));
        System.assert(result.containsKey(Constants.RECORD_TYPE_TGS_BUSINESS_UNIT));
        System.assert(result.containsKey(Constants.RECORD_TYPE_TGS_COST_CENTER));
        System.assert(result.containsKey(Constants.RECORD_TYPE_TGS_HOLDING));
        result=TGS_RecordTypes_Util.getRecordTypeIdsByDeveloperName(Account.SObjectType);
    }
    
    static testmethod void testloadRecordTypes(){
        TGS_RecordTypes_Util.loadRecordTypes(new List<Schema.SobjectType>{ Account.SObjectType, Case.SObjectType,NE__Order__c.SObjectType});
        System.assert(TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType,Constants.RECORD_TYPE_OPTY)!=null);
        
    }
	
}