public without sharing class FO_DetailRecordPageController {
/*****************************************************
     * Author: Javier Lopez
     * Company: gCTIO
     * DEscription: change the Estado of forecast to Submitted
     * 
     * 
     * 
     * *****************************************************/
    @AuraEnabled
    public static String changeStatus(String Id){
        System.debug('Hemos llegado:'+Id);
        
        FO_Forecast__c estadoPa = [Select FO_Forecast_Padre__c,FO_Forecast_Padre__r.FO_Estado__c,FO_Estado__c from FO_Forecast__c where Id=:Id];
        if(estadoPa.FO_Forecast_Padre__c!=null && estadoPa.FO_Forecast_Padre__r.FO_Estado__c.equals('Enviado'))
            return 'KO';
        if(estadoPa.FO_Estado__c.equals('Enviado'))
            return 'KO2';
        FO_Forecast__c fo = new FO_Forecast__c();
        fo.Id=Id;
        fo.FO_Estado__c='Enviado';
        fo.FO_Fecha_Forecasting__c =Date.today();
        try{
            update(fo);
        }catch(Exception e){
            return 'ERROR: '+e.getMessage();
        }
        
        return 'OK';
    }
    /*******************************
     * Autor: Javier Lopez Andradas
     * Company:gCTIO
     * Description: check if the Forecast is already been submitted or the PArent has been submitted
     * 
     * <date>		<version>		<description>
     * 23/04/2018	1.1				Modified to avoid  null pointer exception
     * *******************************/
    @AuraEnabled
    public static String checkSubmitted(String Id){
        FO_Forecast__c fore = [Select FO_Estado__c,FO_FOrecast_Padre__c,FO_Forecast_Padre__r.FO_Estado__c from FO_Forecast__c where Id=:Id];
        if(fore.FO_Estado__c.equals('Enviado')  )
            return 'KO';
        if(fore.FO_Forecast_Padre__c!=null && fore.FO_Forecast_Padre__r.FO_Estado__c.equals('Enviado'))
            return 'KO';
        return 'OK';
        
    }
    /***********************************
     * Author : Javier Lopez Andradas
     * Company:gCTIO
     * Description: retrieves child Nodes that may or not do its forecasts
     * 
     * 
     * *********************************/
    @AuraEnabled
    public static List<String> validaSub(String Id){
        List<String> lst_str = new List<String>();
        Map<Id,FO_Forecast__c> mp_fore =new Map<Id,FO_Forecast__c> ([Select Id,FO_Nodo__c,RecordType.DeveloperName,FO_Estado__c from FO_Forecast__c where Id=:Id or FO_Forecast_Padre__c=:Id]);
        if(mp_fore.get(Id).RecordType.DeveloperName.equals('FO_Ejecutivo'))
            return null;
        List<BI_FVI_Nodos__c> lst_node = [Select Owner.FirstName,Owner.LastName, Name,Id from BI_FVI_Nodos__c where BI_FVI_NodoPadre__c=:mp_fore.get(Id).FO_Nodo__c];
        if(lst_node.isEmpty())
            return null;
        Map<String,String> mp_statusNode = new Map<String,String>();
        for(FO_Forecast__c fore: mp_fore.values()){
            if(fore.Id.equals(Id)==false){
                mp_statusNode.put(fore.FO_Nodo__c,fore.FO_Estado__c);
            }
        }
        for(BI_FVI_Nodos__c node :lst_node){
            String state = mp_statusNode.get(node.Id);
            if(state==null){
                lst_str.add(node.Name+'|||'+node.Owner.FirstName+'|||'+node.Owner.LastName+'|||'+Label.FO_Sub_not_cre);
            }else if(state.equals('Enviado')==false){
                lst_str.add(node.Name+'|||'+node.Owner.FirstName+'|||'+node.Owner.LastName+'|||'+Label.FO_Sub_not_Subm);
            }
        }
        if(lst_str.isEmpty())
            return null;
        return lst_str;
    }
    /************************************
     * Author: Javier Lopez Andradas
     * Company: gCTIO
     * Description:  Retireves the Opps that are not invcluded in this Forecast
     * 
     * 
     * *********************************/
    @AuraEnabled
    public static List<FO_Forecast_Item__c> getOpps(String Id){
        return [Select FO_Opportunity__r.Name, FO_Cliente__c,FO_NAV__c,CurrencyIsoCode from FO_Forecast_Item__c where FO_Tipo_Forecast__c=null AND FO_Forecast_Padre__c=:Id];
    }
    /************************************
     * Author: Javier Lopez Andradas
     * Company: gCTIO
     * Description:  Retireves the RT of a Forecast
     * 
     * 
     * *********************************/
    @AuraEnabled
    public static String getRT(String Id){
        return [Select RecordType.DEveloperName from FO_Forecast__c where Id=:Id].RecordType.DeveloperName;
    }
    /*****************************************
     * Author: Javier Lopez Andradas
     * Company: gCTIO
     * Description: from FO_Oportunidades_Atrasadasbtn, retrieves the number of Opps retrasadas
     * 
     * <date>		<version>		<description>
     * 16/04/2018	1.0				Initial version
     * 
     * *****************************************/
    @AuraEnabled
    public static Integer returnOppsAtr(Boolean atras){
        System.debug('FORECAST entra returnOppsAtr');
         Schema.DescribeFieldResult stageNameField = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> values = stageNameField.getPicklistValues();
        List<String> val_def = new List<String>();
        String query ='select count() from Opportunity where OwnerId=\''+UserInfo.getUserId()+'\' AND StageName NOT IN (';
        for(Schema.PicklistEntry entry :values){
            if( entry.getValue().StartsWith('F1'))
                query+='\''+entry.getValue()+'\',';
        }
        query=query.substring(0, query.length()-1)+')';
        if(atras==true){
            query+='AND CloseDate<TODAY';
        }else{
            
            Date hoy = Date.today();
            Date next= hoy.addMonths(1);
            System.debug(next);
            String month = String.valueOf(next.month());
            if(month.length()<2)
                month='0'+month;
            String day = String.valueOf(next.day());
            if(day.length()<2)
                day='0'+day;
        	query+='AND CloseDate>TODAY AND CloseDate<'+next.year()+'-'+month+'-'+day;    
        }
     	System.debug('FORECAST query '+query);
        System.debug('FORECAST sale returnOppsAtr');
        return database.countQuery(query);
    }
}