public class TGS_Queue_Case {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta García
    Company:       Deloitte
    Description:   Add error for Assignee in Case
    
    History

    <Date>            <Author>                      <Description>
    26/11/2015        Marta García                  Initial version
	20/01/2016		  Pablo Oliva					Optimization
	16/03/2017		  Guillermo Muñoz				Added try catch to avoid errors with Portal Users
--------------------------------------------------------------------------------------------------------------------------------------------------------*/     
   
    public static void queueCase(List<Case> newCases, Map<id, Case> newMapCases) {

    	try{
        
	        Map<id,set<id>> MapCaseGroupMembers = new Map<id,set<id>>();
	        Map<Id,Id> mapCaseGroup = new  Map<Id,Id>();
	        List<Id> ListGrId = new List<Id>();
	        
	        //POF 20-01-16
	        Boolean ownerGroup = false;
	        
	        //
	        for( Case c : newCases ){
	            
	           mapCaseGroup.put(c.OwnerId, c.id);
	           //POF 20-01-16
	           if (c.OwnerId != null && String.valueOf(c.OwnerId).startsWith('00G')) {
	           	   ownerGroup = true;	
	           } 
	           //END POF
	            
	        }
	        
	        
	        if (ownerGroup) {//POF 20-01-16
	        
		       Set<Id> users = new Set<id>(); 
		       for(Group groups : [SELECT Id, type, (SELECT UserOrGroupId FROM GroupMembers WHERE GroupId IN :mapCaseGroup.keySet()) FROM Group WHERE id IN :mapCaseGroup.keySet() AND type = : 'Queue']){ 
		           
		           //Obtengo los usuarios que pertenecen a la cola
		           for(GroupMember groupMembers : groups.GroupMembers ){
		               users.add(groupMembers.UserOrGroupId);
		           
		           }       
		               MapCaseGroupMembers.put(mapCaseGroup.get(groups.id),users);
		
		        }
		        
		        
		       for(Case cases : newCases ){
		             
		             if(String.isNotBlank(cases.TGS_Assignee__c)){
		                 
		                 if(MapCaseGroupMembers.get(cases.id)!=null){
		                     if(!MapCaseGroupMembers.get(cases.id).contains(cases.TGS_Assignee__c)){
		                     
		                     
		                         newMapCases.get(cases.id).addError('User must belong to the assigned queue.'); 
		                     
		                     
		                     }
		                 
		                }  
		                 
		             }
		        }
	        
	    	}//POF 20-01-16
    	}catch(Exception exc){System.debug(exc);}
    }
    
    

}