@isTest
private class PCA_Reclamo_NewAttachmentCtrl_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_Reclamo_NewAttachmentCtrl class 
    
    History: 
    <Date> 					<Author> 				<Change Description>
    14/08/2014      		Ana Escrich	    		Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Ana Escrich   
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for PCA_Reclamo_NewAttachmentCtrl.loadInfo
		    
 	History: 
 	
 	 <Date> 					<Author> 				<Change Description>
    14/08/2014      		Ana Escrich	    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void loadInfoTest() {
        BI_TestUtils.throw_exception = false;
        Profile prof = [Select Id From Profile Where Name='BI_Administrator'];
        User bi_admin=BI_DataLoad.loadUsers(1, prof.id, 'Administrador del Sistema')[0];
        System.runAs(bi_admin){
 			List<Account> accounts = BI_DataLoad.loadAccounts(1, BI_DataLoad.loadPaisFromPickList(1));
			List<RecordType> rT = [select id, name from Recordtype where name='Caso Comercial'];
			Case caseTest = new Case(RecordTypeId=rT[0].Id, AccountId = accounts[0].Id, BI_Otro_Tipo__c = 'test',Status='Nuevo', Priority='Media', Type='Reclamo', 
									Subject='Test', CurrencyIsoCode='ARS', Origin='Portal Cliente', BI_Confidencial__c=false, 
									Reason='Reclamos administrativos', Description='testdesc');
			insert caseTest;
    	 
	    	PCA_Reclamo_NewAttachmentCtrl controller = new PCA_Reclamo_NewAttachmentCtrl();
    		PageReference pageRef = new PageReference('PCA_Reclamo_NewAttachment');
   			Test.setCurrentPage(pageRef);
   			ApexPages.currentPage().getParameters().put('Id', caseTest.Id);
   			ApexPages.currentPage().getParameters().put('Name', 'test111');
    	
    		delete [select id from BI_Log__c];
    	
    		Test.startTest();
    	
    		controller.loadInfo();
    		
    		system.assert([select Id from BI_Log__c limit 1].isEmpty());
    		
    		Test.stopTest();
		}
	}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Ana Escrich
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for PCA_Reclamo_NewAttachmentCtrl.attachFile
		    
 	History: 
 	
 	 <Date> 					<Author> 				<Change Description>
    14/08/2014      		Ana Escrich	    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void attachFileTest() {
        BI_TestUtils.throw_exception = false;
        Profile prof = [Select Id From Profile Where Name='BI_Administrator'];
        User bi_admin=BI_DataLoad.loadUsers(1, prof.id, 'Administrador del Sistema')[0];
        System.runAs(bi_admin){
 			List<Account> accounts = BI_DataLoad.loadAccounts(1, BI_DataLoad.loadPaisFromPickList(1));
			List<RecordType> rT = [select id, name from Recordtype where name='Caso Comercial'];
			Case caseTest = new Case(RecordTypeId=rT[0].Id, BI_Otro_Tipo__c = 'test', AccountId = accounts[0].Id, Status='Nuevo', Priority='Media', Type='Reclamo', 
									Subject='Test', CurrencyIsoCode='ARS', Origin='Portal Cliente', BI_Confidencial__c=false, 
									Reason='Reclamos administrativos', Description='testdesc');
			insert caseTest;
			
			delete [select id from BI_Log__c];
			
			Test.startTest();
    	 
    		PCA_Reclamo_NewAttachmentCtrl controller = new PCA_Reclamo_NewAttachmentCtrl();
    		controller.myDoc = new Attachment();
    		controller.myDoc.Name='Unit Test Attachment';
        	controller.myDoc.Body=Blob.valueOf('Unit Test Attachment Body');
    		controller.caseId = caseTest.Id;
    		controller.attachFile();
    		controller.myDoc.Name=null;
    		controller.attachFile();
    		
    		//system.assert([select Id from BI_Log__c limit 1].isEmpty());
    		
    		Test.stopTest();
		}
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Ana Escrich
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for PCA_Reclamo_NewAttachmentCtrl.checkPermissions
		    
 	History: 
 	
 	 <Date> 					<Author> 				<Change Description>
    14/08/2014      		Ana Escrich	    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void checkPermissionsTest() {
        BI_TestUtils.throw_exception = false;
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
 		
    	 User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
	    //insert usr;
	    system.debug('usr: '+usr);
	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            
            delete [select id from BI_Log__c];
            
	    	system.runAs(user1){
	    		
    			 Test.startTest();
	    		
	        	 PCA_Reclamo_NewAttachmentCtrl controller = new PCA_Reclamo_NewAttachmentCtrl();
	        	 controller.checkPermissions();
	        	 
	        	 //system.assert([select Id from BI_Log__c limit 1].isEmpty());
    		
    			 Test.stopTest();
			}
	    }
	}


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test method that coverage catch conditions.

    History: 
    
    <Date>                     <Author>                <Change Description>
    03/10/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void catch_test() {
        BI_TestUtils.throw_exception = true;
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        Test.startTest();
        
        PCA_Reclamo_NewAttachmentCtrl controller = new PCA_Reclamo_NewAttachmentCtrl();
        controller.checkPermissions();
        controller.attachFile();
        
        system.assertEquals(false, [select Id from BI_Log__c limit 1].isEmpty());
    		
    	Test.stopTest();
        
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Ignacio Morales
 	Company:       deloitte.com
 	Description:   Test method to manage the code coverage for PCA_Reclamo_NewAttachmentCtrl WS
		    
 	History: 
 	
 	 <Date> 					<Author> 				<Change Description>
    3/11/2015      			 Ignacio Morales    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void PCA_Reclamo_NewAttachmentCtrl_Ws() {
        BI_TestUtils.throw_exception = false;
        Profile prof = [Select Id From Profile Where Name='BI_Administrator'];
        User bi_admin=BI_DataLoad.loadUsers(1, prof.id, 'Administrador del Sistema')[0];
        System.runAs(bi_admin){
 			List<Account> accounts = BI_DataLoad.loadAccounts(1, BI_DataLoad.loadPaisFromPickList(1));
			List<RecordType> rT = [select id, name from Recordtype where name='Caso Comercial'];
			Case caseTest = new Case(RecordTypeId=rT[0].Id, BI_Otro_Tipo__c = 'test', AccountId = accounts[0].Id, Status='Nuevo', Priority='Media', Type='Reclamo', 
									Subject='Test', CurrencyIsoCode='ARS', Origin='Portal Cliente', BI_Confidencial__c=false, 
									Reason='Reclamos administrativos', Description='testdesc');
			insert caseTest;
        
      
        
        	PageReference pageRef = new PageReference('PCA_Reclamo_NewAttachment');
   			Test.setCurrentPage(pageRef);
   			ApexPages.currentPage().getParameters().put('Id', caseTest.Id);
   			ApexPages.currentPage().getParameters().put('Name', 'test111');
        
        
    		Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator());  
        
    		PCA_Reclamo_NewAttachmentCtrl controller = new PCA_Reclamo_NewAttachmentCtrl();
    		controller.myDoc = new Attachment();
    		controller.myDoc.Name='Unit Test Attachment';
        	controller.myDoc.Body=Blob.valueOf('Unit Test Attachment Body');
        	controller.myDoc.ParentId = caseTest.Id;
    		controller.caseId = caseTest.Id;
    		controller.attachFile();
    		controller.myDoc.Name=null;
    		controller.attachFile();
        	PageReference onclickCerrar = controller.onclickCerrar();
            }
	}

}