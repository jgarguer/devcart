@isTest(seeAllData = false)
public class BIIN_SiteSearch_Creacion_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José María Martín
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_SiteSearch_Creacion_Ctrl
    
    <Date>                 <Author>                        <Change Description>
    11/2015                José María Martín               Initial Version
    11/05/2016             José Luis González Beltrán      Adapt TEST to UNICA interface modifications in tested class
    05/07/2016             José Luis González Beltrán      Fix test
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void test_BIIN_SiteSearch_Creacion_TEST() {
        BI_TestUtils.throw_exception = false;
        User actualUser = BIIN_Test_Data_Load.createUSer('BI_Standard_PER', 'Asesor');
        User bi_admin = BIIN_Test_Data_Load.createUSer('BI_Administrator', 'Administrador del Sistema');
        insert bi_admin;
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(), BI_DataLoad.searchAdminProfile());
      	Account account;
        Case caso;
		System.runAs(bi_admin)
        {
            account = BIIN_Test_Data_Load.createAccount('Pachamama');
            insert account;
            caso = new Case(
                            Status              = 'Assigned', 
                            Description         = 'Descripcion test', 
                            accountId           = account.id, 
                            OwnerId             = usr.id,
                            BI_Status_Reason__c = '27000'
                            );
        	insert caso;
        }

        System.runAs(usr)
        {   
			
            //Creacion de account y caso
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item : acc) {
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);   
            List<RecordType> rT = [SELECT Id, Name FROM Recordtype WHERE Name = 'Caso Comercial'];
            
            Case caseTest = new Case(
                                     RecordTypeId       = rT[0].Id, 
                                     AccountId          = accList[0].Id, 
                                     BI_Otro_Tipo__c    = 'test', 
                                     Status             = 'Nuevo', 
                                     Priority           = 'Media', 
                                     Type               = 'Reclamo', 
                                     Subject            = 'Test', 
                                     CurrencyIsoCode    = 'ARS', 
                                     Origin             = 'Portal Cliente', 
                                     BI_Confidencial__c = false, 
                                     Reason             = 'Reclamos administrativos', 
                                     Description        = 'testdesc'
                                     );
            insert caseTest;
            //Fin de creacion de account y caso
            
            Test.startTest();

            PageReference p = Page.BIIN_Detalle_Incidencia;
            p.getParameters().put('inputfield', 'AccountName');
            p.getParameters().put('accountfield', accList[0].id );
            Test.setCurrentPageReference(p);
            
            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('SiteSearch'));
            ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caseTest);
            BIIN_SiteSearch_Creacion_Ctrl ceController = new BIIN_SiteSearch_Creacion_Ctrl();
            ceController.getHttpRequest();
            ceController.getHttpResponse();
            ceController.getSite();
            ceController.HabilitarIr();
            ceController.closePopUp();

            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicketException500'));
            ceController.getSite();
            
            Test.stopTest();
        }
    }
}