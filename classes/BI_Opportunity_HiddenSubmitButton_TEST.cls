@isTest
private class BI_Opportunity_HiddenSubmitButton_TEST{
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the coverage of BI_Opportunity_HiddenSubmitButton_CTRL class
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    10/12/2015                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void testGlobal() {
		
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
        BI_Dataload.set_ByPass(false,false);
        BI_TestUtils.throw_exception=false;
		
        User usu = [SELECT Id FROM User WHERE Profile.Name =: Label.BI_Administrador_Sistema AND IsActive = true Limit 1];

        System.assertNotEquals(usu,null);
		
				  
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Argentina'});
			
		Opportunity opp = new Opportunity(Name = 'Test',
											  CloseDate = Date.today(),
											  BI_Requiere_contrato__c = true,
											  Generate_Acuerdo_Marco__c = false,
											  AccountId = lst_acc[0].id,
											  StageName = Label.BI_F2Negociacion,
											  BI_Ultrarapida__c = true,
											  BI_Duracion_del_contrato_Meses__c = 3,
											  BI_Recurrente_bruto_mensual__c = 100.12,
											  BI_Ingreso_por_unica_vez__c = 12.1,
											  BI_Recurrente_bruto_mensual_anterior__c = 1,
											  BI_Opportunity_Type__c = 'Producto Standard',
											  Amount = 23,
											  BI_Country__c = 'Argentina');
										
		insert opp;

		System.runAs(usu){
			BI_Opportunity_HiddenSubmitButton_CTRL obj = new BI_Opportunity_HiddenSubmitButton_CTRL(new ApexPages.StandardController(opp));
			obj.editable();
			obj.bypass = true;
			obj.casillaDesarrollo = true;

			try{
				obj.save();
				throw new BI_Exception('Test');
			}
			catch(Exception e){}
			obj.bypass = false;
			obj.save();
			obj.bypass = true;
			obj.save();
			obj.cancel();
		}

	}
	
	
	
}