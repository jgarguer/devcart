@isTest
public class programacion_testClass {
	static testMethod void programacion_testMethodClass() {
		
		//GMN 13/06/2017 - Deprecated references to Raz_n_social__c (Optimización)
		Account acc01  = new Account ();
			acc01.Name = 'Cuenta de prueba testCosumos2';
			//acc01.Raz_n_social__c = 'Cuenta de prueba testCosumos SA CV';
		insert acc01; 
				
		Factura__c Factura = new Factura__c ();
			Factura.Activo__c = true;
			Factura.Cliente__c = acc01.Id;
			Factura.Fecha_Inicio__c = Date.today();
			Factura.Fecha_Fin__c = Date.today() + 20;
			Factura.Inicio_de_Facturaci_n__c = Date.today();
			Factura.IVA__c = '16%';
			Factura.Requiere_Anexo__c = 'SI';
			
		insert Factura;		
		
		Programacion__c programacion = new Programacion__c ();
			programacion.Factura__c = Factura.Id;
			programacion.Sociedad_Facturadora_Real__c = 'GTM';
			programacion.Fecha__c = Date.today();
			programacion.Inicio_del_per_odo__c = Date.today();
			programacion.Fin_del_per_odo__c = Date.today().addMonths(1);
			programacion.Fecha_de_inicio_de_cobro__c = Date.today().addDays(2);
		insert programacion;
		
			programacion.Folio_Fiscal__c  = 'FGTDMJMO001';
			programacion.Facturado__c  = true;
			programacion.Exportado__c = true;
			programacion.Numero_de_factura_SD__c = '00001';
		update programacion;
		
		try {
				programacion.Folio_Fiscal__c = 'FGTDMJMO002';
				programacion.Numero_de_factura_SD__c = '00002';
			update programacion;
		}catch (Exception err){
			
		}
		
		String strPerfil = '';
		for (Profile perfil : [SELECT id, Name from Profile Where Name = 'Administrador del sistema']){
			strPerfil = perfil.Id;
		}
		/*
				
		 Profile p = [select id from profile where name='BI_Standard_MEX'];
         User u = new User(alias = 'standt', email='sysadmin@TemmMextest.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='sysadmin@TemmMextest.com');
         System.runAs(u) {
			try{
				delete programacion;
			}catch (Exception err){
			}	
         }	
         */
         
	}
		
}