/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-04-07      Manuel Esthiben Mendez Devia (MEMD)     Cloned Class
*************************************************************************************************************/
public with sharing class BI_COL_Contract_cls 
{

    /**
    *Resta el valor de la ms en el contrato en caso que resulten fallidas
    *
    * @param  msID  lista de id de ms fallidas
    */
    
    public static void msFallida(set<string> msID){
        
        if(msID.isEmpty()){
            return;
        }
        
        map<Id,BI_COL_Modificacion_de_Servicio__c> msCtr= new map<Id,BI_COL_Modificacion_de_Servicio__c>
        ([select id,BI_COL_Total_servicio__c,BI_COL_TRM__c, BI_COL_FUN__r.BI_COL_Contrato__c,BI_COL_Monto_ejecutado__c,
        BI_COL_FUN__r.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c,BI_COL_FUN__r.BI_COL_Contrato__r.BI_COL_Saldo_contrato__c
        from BI_COL_Modificacion_de_Servicio__c where id=:msID]);
        
        map<Id,Contract> contratos=new Map<Id, Contract>();
        
        for(string idC:msCtr.keySet()){
            contratos.put(msCtr.get(idC).BI_COL_FUN__r.BI_COL_Contrato__c,(Contract)msCtr.get(idC).BI_COL_FUN__r.BI_COL_Contrato__r);
            
            BI_COL_Modificacion_de_Servicio__c laMs=msCtr.get(idC);
            Double montoNoejecutado= laMs.BI_COL_Monto_ejecutado__c/laMs.BI_COL_TRM__c;
            
            Contract contratoAct=contratos.get(laMs.BI_COL_FUN__r.BI_COL_Contrato__c);
            contratoAct.BI_COL_Monto_ejecutado__c=contratoAct.BI_COL_Monto_ejecutado__c-montoNoejecutado;
            system.debug('contratoAct.BI_COL_Monto_ejecutado__c'+contratoAct.BI_COL_Monto_ejecutado__c);
        }

        update contratos.values();
    } 
    
    /**
    * Calcula el monto ejecutado cuando es llamado por el batch de ms Fallidas
    *
    * @param  msID  lista de id de ms fallidas
    */
    public static void motoEjecutado(set<string> msID){
        
        if(msID.isEmpty()){
            return;
        }
        
        map<Id,BI_COL_Modificacion_de_Servicio__c> msCtr= new map<Id,BI_COL_Modificacion_de_Servicio__c>
        ([select id,BI_COL_Total_servicio__c,BI_COL_TRM__c, BI_COL_FUN__r.BI_COL_Contrato__c,BI_COL_Monto_ejecutado__c,
        BI_COL_FUN__r.BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c,BI_COL_FUN__r.BI_COL_Contrato__r.BI_COL_Saldo_contrato__c
        from BI_COL_Modificacion_de_Servicio__c where id=:msID]);
        
        map<Id,Contract> contratos=new Map<Id, Contract>();
        
        for(string idC:msCtr.keySet()){
            contratos.put(msCtr.get(idC).BI_COL_FUN__r.BI_COL_Contrato__c,(Contract)msCtr.get(idC).BI_COL_FUN__r.BI_COL_Contrato__r);
            
            BI_COL_Modificacion_de_Servicio__c laMs=msCtr.get(idC);
            System.debug(laMs.id+'  laMs.monto_ejecutado__c'+laMs.BI_COL_Monto_ejecutado__c+' laMs.BI_COL_TRM__c'+laMs.BI_COL_TRM__c);
            Double montoNoejecutado= laMs.BI_COL_Monto_ejecutado__c/laMs.BI_COL_TRM__c;
            
            Contract contratoAct=contratos.get(laMs.BI_COL_FUN__r.BI_COL_Contrato__c);
            System.debug('montoNoejecutado:'+montoNoejecutado);
            system.debug('contratoAct.Monto_ejecutado__c'+contratoAct.BI_COL_Monto_ejecutado__c);
            
            //Si el monto ejecutado es null
            contratoAct.BI_COL_Monto_ejecutado__c=(contratoAct.BI_COL_Monto_ejecutado__c==null)?0:contratoAct.BI_COL_Monto_ejecutado__c;
            
            contratoAct.BI_COL_Monto_ejecutado__c=contratoAct.BI_COL_Monto_ejecutado__c+montoNoejecutado;
            
        }
        

        update contratos.values();
    }
    
    
    
    public static void asignaContrato(list<Contract> nuevoContrato){
        
        for(Integer i = 0; i < nuevoContrato.size(); i++){
            
            //Asigna el valor al campo contrato en base al formato de pagina
            if(nuevoContrato[i].BI_COL_Tipo_contrato__c!=null && nuevoContrato[i].CompanySigned==null){
                string  tipoDeRegistro =Schema.SObjectType.Contract.getRecordTypeInfosByID().get(nuevoContrato[i].BI_COL_Tipo_contrato__c).getName();

                tipoDeRegistro =tipoDeRegistro.remove('Contrato Telefónica').trim();    
                //nuevoContrato[i].CompanySigned=tipoDeRegistro;
            }
            

            
            //Salva el valor inicial del presupuesto del contrato
            if(nuevoContrato[i].Status=='Firmado Activo' &&
                nuevoContrato[i].BI_COL_Presupuesto_contrato__c!=0 && nuevoContrato[i].BI_COL_Presupuesto_contrato__c!=null &&
                (nuevoContrato[i].BI_COL_Presupuesto_inicial_contrato__c==0 || nuevoContrato[i].BI_COL_Presupuesto_inicial_contrato__c==null)
                ){
                    nuevoContrato[i].BI_COL_Presupuesto_inicial_contrato__c=nuevoContrato[i].BI_COL_Presupuesto_contrato__c;                                
                }
            
            

        
        }
    
    }
    
    /*RMORA 02-09-2015 Se comentarea este metodo a peticion de Carlos Carvajal
    public void impideModAnexos(list<Contract> nuevoContrato, list<Contract> anteriorContrato, Set <ID>Idcontratos)
    {
        List<PermissionSetAssignment> permiSet=[select id,PermissionSet.Name from PermissionSetAssignment where AssigneeId=:userinfo.getProfileId() limit 1];
        String idAdmin = [SELECT Id,Name FROM Profile WHERE Name = 'Administrador del sistema' limit 1].id;
        set<String> perfiles=new set<String>{'BI_COL_Perfil_Asesor_Ventas','BI_COL_Ejecutivo de Cobros',idAdmin};       
            if(!perfiles.contains(Userinfo.getProfileId())){
            set<AggregateResult>fun=new set<AggregateResult>([select BI_COL_Contrato__c from BI_COL_Anexos__c where BI_COL_Contrato__c IN :Idcontratos group by BI_COL_Contrato__c]);
            set<string> idContratosConFun=new set<string>();
            
            set<String> anexos=new set<String>{'Anexo_de_TV__c','Acuerdo_Voz__c','Anexos_Valor_Agregado__c','Acuerdo_de_Servicios_Datacenter__c','Acuerdo_Datos__c','ACUERDO_DE_SERVICIO_PDTI__c','Anexo_de_Gestion_PC__c','Anexo_Internet_Seguro__c','Acuerdo_RPV_IPSEC__c','Anexo_de_Centrales__c','Anexo_de_Areas_y_energia__c','Anexo_de_Servicios_Conexos__c'};
            
            if(!fun.isEmpty()){
                for(AggregateResult aggr:fun){
                    idContratosConFun.add(String.valueOf(aggr.get('BI_COL_Contrato__c')));          
                }
            }
            
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType leadSchema = schemaMap.get('Contract');
            Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
            
            
            string error= 'No se puede modificar los campos '; 
            
            for (Integer i = 0; i < nuevoContrato.size(); i++){
                //Evalua si ha cambiado algun campo de la seccion anexos para aquellos contratos que tengan fun.
                if(idContratosConFun.contains(nuevoContrato[i].Id)){
                    string campo='';
                    sobject nuevo=(Sobject)nuevoContrato[i];
                    sobject anterior=(Sobject)anteriorContrato[i];
                    for(string an: anexos){
                        if(((nuevo.get(an)!=anterior.get(an)) && nuevo.get(an)==false) && !perfiles.contains(permiSet[0].PermissionSet.Name)){
                            campo+=fieldMap.get(an).getDescribe().getLabel()+', ';                  
                        }
                    }
                    if(campo!=''){
                        nuevoContrato[i].addError(error+campo+' por favor contacte a Administración de ventas');
                    }
                }       
            }
        }
    
    }*/
    
    public void validaMSFacturacion(list<Contract> lstNewContrato, list<Contract> lstOldContrato)
    {
        Boolean bolValida=true;
        list<String> idContrato=new list<String>();
        list<string> idContratoSinNF=new list<string>();
        System.debug('\n\n Metodo validaMSFacturacion Paso por la linea 156 \n\n');
        for(Contract c:lstNewContrato)
        {
            idContrato.add(c.ID);
        }
        map<ID,Integer> mapModServ=new map<ID,Integer>();
        AggregateResult[] groupedResults= [Select Count(n.BI_COL_Estado__c) CountEstado,n.BI_COL_FUN__r.BI_COL_Contrato__r.ID FROM BI_COL_Modificacion_de_Servicio__c n 
                                            WHERE BI_COL_FUN__r.BI_COL_Contrato__r.ID IN:idContrato 
                                            AND  BI_COL_Autorizacion_facturacion__c =true 
                                            AND  BI_COL_Estado__c ='Activa'
                                            group by n.BI_COL_FUN__r.BI_COL_Contrato__r.ID];

        Integer IntValorCount=0;
        String strIDContrato='';
        for(AggregateResult ar:groupedResults)
        {
            strIDContrato=String.valueof(ar.get('IDContrato'));
            IntValorCount=Integer.valueof(ar.get('CountEstado'));
            mapModServ.put(strIDContrato,IntValorCount);
        }
        Integer intCount=0;
        for(Contract c:lstNewContrato)
        {
            System.debug('\n\n Metodo validaMSFacturacion Paso por la linea 168 c.ContractTerm='+c.ContractTerm +'  lstOldContrato[intCount].ContractTerm '+lstOldContrato[intCount].ContractTerm +
            'c.ContractTerm= '+ c.ContractTerm +' mapModServ.containsKey(c.ID)'+mapModServ.containsKey(c.ID)+' \n\n');
            if(c.ContractTerm!=lstOldContrato[intCount].ContractTerm && mapModServ.containsKey(c.ID) && c.BI_COL_Duracion_Adicional_meses__c==0)
            {
                //c.addError(' No es posible actualizar la duracion del Contrato tiene MS Facturando');
            }
        }
        
        //return idContratoSinNF;
    }   
    
    @InvocableMethod 
  public static void invokeapexcallout(list<Contract> LstContra) 
  {

        Map<id,Contract> cotraRef=new Map<id,Contract>();
        if(Trigger.newMap !=null)
        {
            cotraRef = new Map<id,Contract>([SELECT Account.BI_Subsegment_Regional__c FROM Contract where Id IN :Trigger.newMap.keyset()]);
        }
    List<Contract> contrato = new List<Contract>();
    if(!System.isFuture() && !System.isBatch())
    {
      System.debug('\n\n########------->Ejecución Trigger BI_COL_Contract_cls \n\n');
      for(Contract contra : LstContra)
      {
        contrato.add(contra);
        System.debug('\n\n########------->Ejecución Contrato\n\n'+contrato);
        System.debug('\n\n########------->Ejecución Contrato\n\n'+LstContra);        

      }
      BI_COL_Contract_cls.asignaContrato(contrato);
    }
  } 
    

}