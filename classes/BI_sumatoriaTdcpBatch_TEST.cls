@isTest
private class BI_sumatoriaTdcpBatch_TEST
{
	@isTest
	static void itShould()
	{
        BI_TestUtils.throw_exception=false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        userTGS.TGS_Is_TGS__c = false;
        insert userTGS;

     	Map<Integer, BI_bypass__c> mapa = 
        BI_MigrationHelper.enableBypass(Userinfo.getUserId(), true, true, true, true);

        //Insertamos una tasa
        BI_Tasa_de_cambio_presupuestaria__c t1 = new BI_Tasa_de_cambio_presupuestaria__c(
           BI_Fecha_inicio__c = Date.today(),
           BI_Ratio_divisa_a_euro__c = 1.0,
           CurrencyIsoCode = 'ARS' );
        insert t1;
    
    
        BI_Tasa_de_cambio_presupuestaria__c t2 = new BI_Tasa_de_cambio_presupuestaria__c(
           BI_Fecha_inicio__c = Date.today(),
           BI_Ratio_divisa_a_euro__c = 15.0,
           CurrencyIsoCode = 'USD' );
        insert t2;


    /*
    20/09/2017        Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
    */
        Account acc = new Account(
          Name = 'Test1ARG', 
          Industry = 'Comercio', 
          CurrencyIsoCode = 'ARS', 
          BI_Activo__c = 'Sí', 
          BI_Cabecera__c = 'Sí',  
          BI_Estado__c = true, 
          BI_Country__c = Label.BI_Argentina, 
          BI_Sector__c = 'Comercio', 
          BI_Subsector__c = 'Alimentos y Bebidas', 
          TGS_Es_MNC__c = false, 
          BI_No_Identificador_fiscal__c = '30111111118',
          Sector__c = 'Private',BI_Segment__c = 'test',
        BI_Subsegment_Regional__c = 'test',
        BI_Territory__c = 'test'
        );
        insert acc;
        Opportunity opp = new Opportunity(
            Name = 'OppTest1',
            CloseDate = Date.today(),
            StageName = 'F5 - Solution Definition',
            BI_No_requiere_comite_arg__c = true,
            AccountId = acc.Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            CurrencyIsoCode = 'ARS',
            BI_O4_TdCP__c = 1,
            BI_Licitacion__c = 'No',
            BI_Productos_numero__c = 2
        );
        insert opp;
        Opportunity verLaTasa = [Select Id, BI_O4_TdCP__c from Opportunity where Id = :opp.Id];
        System.debug('updateValPresupuestoOrder_TEST ' + verLaTasa);
        System.debug('updateValPresupuestoOrder test opp'+ opp.Id);
        NE__Order__c ord = new NE__Order__c(NE__AccountId__c = acc.Id,
                                                NE__BillAccId__c = acc.Id,
                                                NE__OptyId__c = opp.Id,
                                                NE__AssetEnterpriseId__c='testEnterprise',
                                                NE__ServAccId__c = acc.Id,
                                                NE__OrderStatus__c = 'Active',
                                                CurrencyIsoCode = 'ARS');
        insert ord;
        System.debug('updateValPresupuestoOrder test ord'+ ord.Id);

        NE__Product__c pord = new NE__Product__c();
         pord.Offer_SubFamily__c = 'a-test;';
         pord.Offer_Family__c = 'b-test';

        
        insert pord; 

        NE__Product__c pord2 = new NE__Product__c();
         pord2.Offer_SubFamily__c = 'a-testUSD;';
         pord2.Offer_Family__c = 'b-testUSD';
        
        insert pord2;

        NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;
        NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id, 
            NE__ProductId__c = pord.Id, 
            NE__Change_Subtype__c = 'test', 
            NE__Currency__c = 'ARS',
            NE__Disconnect_Subtype__c = 'test');
        insert catIt;

        NE__Catalog_Item__c catIt2 = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id, 
            NE__ProductId__c = pord.Id, 
            NE__Change_Subtype__c = 'test', 
            NE__Currency__c = 'USD',
            NE__Disconnect_Subtype__c = 'test');
        insert catIt2;
        System.debug('agregaTasa mapaCatalogItems catIt' + catIt);
      

   		

        //Insertamos el order item asociado a la order
        NE__OrderItem__c oi = new NE__OrderItem__c(
            NE__OrderId__c = ord.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = pord.Id,
            NE__CatalogItem__c = catIt.Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 1000,
            BI_Ingreso_Recurrente_Anterior_Producto__c = 2000,
            NE__RecurringChargeOv__c = 3000
        );
        

        NE__OrderItem__c oi2 = new NE__OrderItem__c(
            NE__OrderId__c = ord.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = pord2.Id,
            NE__CatalogItem__c = catIt2.Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 2000,
            BI_Ingreso_Recurrente_Anterior_Producto__c = 4000,
            NE__RecurringChargeOv__c = 6000,
            BI_O4_TdCP__c = 11.0
        );
      
        List<NE__OrderItem__c> lst_itemToInsert = new List<NE__OrderItem__c>();
        lst_itemToInsert.add(oi);
        lst_itemToInsert.add(oi2);
    
        insert lst_itemToInsert;


        List<NE__OrderItem__c> miItem = [Select Id, 
        NE__RecurringChargeOv__c,
        NE__OneTimeFeeOv__c,
        BI_Ingreso_Recurrente_Anterior_Producto__c,
                    BI_Tasa_RecurringChargeOv__c, 
                    BI_Tasa_OneTimeFeeOv__c,
                    BI_Tasa_Ingreso_Recurrente_Anterior_Prod__c,
                    CurrencyISOCode
                    from NE__OrderItem__c where Id = :oi.Id ];
    
        System.debug('updateValPresupuestoOrder_TEST: ' + miItem);

        BI_MigrationHelper.disableBypass(mapa);

    
        BI_SumatoriaTdcpBatch Batch = new BI_SumatoriaTdcpBatch();
    
       // Batch.query += ' LIMIT 200'; 
        Batch.query = 'Select Id From Opportunity Limit 200';
        Id batchprocessId = Database.executeBatch(Batch, 200);

      

      	List<Opportunity> mi_opty = [Select id,BI_Tasa_SUM_GANANCIA_Neta_Presu__c from Opportunity where Id = :opp.Id];
      	System.assert(mi_opty[0].BI_Tasa_SUM_GANANCIA_Neta_Presu__c != null, 'No se a actualizado la tasa');
      	        System.debug('BI_Tasa_SUM_GANANCIA_Neta_Presu__c: ' + (mi_opty[0].BI_Tasa_SUM_GANANCIA_Neta_Presu__c));
	}
    
    @isTest
  static void itShouldnt()
  {
        BI_TestUtils.throw_exception=false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        userTGS.TGS_Is_TGS__c = false;
        insert userTGS;

      Map<Integer, BI_bypass__c> mapa = 
        BI_MigrationHelper.enableBypass(Userinfo.getUserId(), true, true, true, true);

        //Insertamos una tasa
        BI_Tasa_de_cambio_presupuestaria__c t1 = new BI_Tasa_de_cambio_presupuestaria__c(
           BI_Fecha_inicio__c = Date.today(),
           BI_Ratio_divisa_a_euro__c = 1.0,
           CurrencyIsoCode = 'ARS' );
        insert t1;
    
    
        BI_Tasa_de_cambio_presupuestaria__c t2 = new BI_Tasa_de_cambio_presupuestaria__c(
           BI_Fecha_inicio__c = Date.today(),
           BI_Ratio_divisa_a_euro__c = 15.0,
           CurrencyIsoCode = 'USD' );
        insert t2;


    
        Account acc = new Account(
          Name = 'Test1ARG', 
          Industry = 'Comercio', 
          CurrencyIsoCode = 'ARS', 
          BI_Activo__c = 'Sí', 
          BI_Cabecera__c = 'Sí',  
          BI_Estado__c = true, 
          BI_Country__c = Label.BI_Argentina, 
          BI_Sector__c = 'Comercio', 
          BI_Segment__c = 'Empresas', 
          BI_Subsector__c = 'Alimentos y Bebidas', 
          BI_Subsegment_Regional__c = 'Corporate', 
          TGS_Es_MNC__c = false, 
          BI_No_Identificador_fiscal__c = '30111111118',
          Sector__c = 'Private'
        );
        insert acc;
        Opportunity opp = new Opportunity(
            Name = 'OppTest1',
            CloseDate = Date.today(),
            StageName = 'F5 - Solution Definition',
            BI_No_requiere_comite_arg__c = true,
            AccountId = acc.Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            CurrencyIsoCode = 'ARS',
            BI_O4_TdCP__c = 1,
            BI_Licitacion__c = 'No',
            BI_Productos_numero__c = 2
        );
        insert opp;
        Opportunity verLaTasa = [Select Id, BI_O4_TdCP__c from Opportunity where Id = :opp.Id];
        System.debug('updateValPresupuestoOrder_TEST ' + verLaTasa);
        System.debug('updateValPresupuestoOrder test opp'+ opp.Id);
        NE__Order__c ord = new NE__Order__c(NE__AccountId__c = acc.Id,
                                                NE__BillAccId__c = acc.Id,
                                                NE__OptyId__c = opp.Id,
                                                NE__AssetEnterpriseId__c='testEnterprise',
                                                NE__ServAccId__c = acc.Id,
                                                NE__OrderStatus__c = 'Active',
                                                CurrencyIsoCode = 'ARS');
        insert ord;
        System.debug('updateValPresupuestoOrder test ord'+ ord.Id);

        NE__Product__c pord = new NE__Product__c();
         pord.Offer_SubFamily__c = 'a-test;';
         pord.Offer_Family__c = 'b-test';

        
        insert pord; 

        NE__Product__c pord2 = new NE__Product__c();
         pord2.Offer_SubFamily__c = 'a-testUSD;';
         pord2.Offer_Family__c = 'b-testUSD';
        
        insert pord2;

        NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;
        NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id, 
            NE__ProductId__c = pord.Id, 
            NE__Change_Subtype__c = 'test', 
            NE__Currency__c = 'ARS',
            NE__Disconnect_Subtype__c = 'test');
        insert catIt;

        NE__Catalog_Item__c catIt2 = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id, 
            NE__ProductId__c = pord.Id, 
            NE__Change_Subtype__c = 'test', 
            NE__Currency__c = 'USD',
            NE__Disconnect_Subtype__c = 'test');
        insert catIt2;
        System.debug('agregaTasa mapaCatalogItems catIt' + catIt);
      

      

        //Insertamos el order item asociado a la order
        NE__OrderItem__c oi = new NE__OrderItem__c(
            NE__OrderId__c = ord.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = pord.Id,
            NE__CatalogItem__c = catIt.Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 1000,
            BI_Ingreso_Recurrente_Anterior_Producto__c = 2000,
            NE__RecurringChargeOv__c = 3000
        );
        

        NE__OrderItem__c oi2 = new NE__OrderItem__c(
            NE__OrderId__c = ord.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = pord2.Id,
            NE__CatalogItem__c = catIt2.Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 2000,
            BI_Ingreso_Recurrente_Anterior_Producto__c = 4000,
            NE__RecurringChargeOv__c = 6000,
            BI_O4_TdCP__c = 11.0
        );
      
        List<NE__OrderItem__c> lst_itemToInsert = new List<NE__OrderItem__c>();
        lst_itemToInsert.add(oi);
        lst_itemToInsert.add(oi2);
    
        insert lst_itemToInsert;


        List<NE__OrderItem__c> miItem = [Select Id, 
        NE__RecurringChargeOv__c,
        NE__OneTimeFeeOv__c,
        BI_Ingreso_Recurrente_Anterior_Producto__c,
                    BI_Tasa_RecurringChargeOv__c, 
                    BI_Tasa_OneTimeFeeOv__c,
                    BI_Tasa_Ingreso_Recurrente_Anterior_Prod__c,
                    CurrencyISOCode
                    from NE__OrderItem__c where Id = :oi.Id ];
    
        System.debug('updateValPresupuestoOrder_TEST: ' + miItem);

        BI_MigrationHelper.disableBypass(mapa);

    
        BI_SumatoriaTdcpBatch Batch = new BI_SumatoriaTdcpBatch();
    
       // Batch.query += ' LIMIT 200'; 
        Batch.query = 'Select Id From Opportunity Limit 200';
        Id batchprocessId = Database.executeBatch(Batch, 200);

      

        List<Opportunity> mi_opty = [Select id,BI_Tasa_SUM_GANANCIA_Neta_Presu__c from Opportunity where Id = :opp.Id];
        System.assert(mi_opty[0].BI_Tasa_SUM_GANANCIA_Neta_Presu__c != null, 'No se a actualizado la tasa');
                System.debug('BI_Tasa_SUM_GANANCIA_Neta_Presu__c: ' + (mi_opty[0].BI_Tasa_SUM_GANANCIA_Neta_Presu__c));
  }


    @isTest
  static void itSchedulable()
  {
    Test.StartTest();
    BI_SumatoriaTdcpBatch Batch = new BI_SumatoriaTdcpBatch();
    
       // Batch.query += ' LIMIT 200'; 
        Batch.query = 'Select Id From Opportunity Limit 200';
        Batch.execute(null); 
    
    Test.stopTest(); 
  }


}