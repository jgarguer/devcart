/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Oscar Ena
Company:       Accenture - New Energy Aborda
Description:   Webservice for manage Orders.

History:

<Date>            <Author>          <Description>
30/08/2017        Oscar Ena        Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@RestResource(urlMapping='/tickets/*')
global with sharing class TGS_Ebounding_WS  {

    //public static final String API_VERSION = RestContext.request.requestURI.split('/')[2];
    //public static final String SERVER_ROOT = System.URL.getSalesforceBaseURL().toExternalForm();
    //public static final String API_ROOT = SERVER_ROOT+'/tickets/'+API_VERSION;
    public static final String API_ROOT = System.URL.getSalesforceBaseURL().toExternalForm();
    //https://{apiRoot}/ticketing/v2


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that creates an Order and a Case (Registration/Modification/Termination) according to IN parameters

    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:

    <Date>            <Author>          <Description>
    30/08/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @HttpPost
    global static void createOrder() {

        NE__Order__c oOrder = new NE__Order__c();
        Case oCase = new Case();

        GenericResponse wResponse = new GenericResponse(); 
        
        try {

            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }

            string sserialize = RestContext.request.requestBody.tostring();
            system.debug('TGS_Ebounding_WS.createOrder ## deserialize: ' + sserialize);
            TicketRequestType wOrder = (TicketRequestType)Json.deserialize(sserialize, TicketRequestType.Class);
            system.debug('TGS_Ebounding_WS.createOrder ## wOrder: ' + wOrder);


            if (wOrder != null){

                String sOrderType = wOrder.type;
                Boolean existTGS_Agrupador = TGS_Ebounding_Helper.existTGS_Agrupador(wOrder.parentTicket);

                if (String.IsNotBlank(sOrderType) && sOrderType.equals(Constants.TYPE_NEW)){

                    if (!existTGS_Agrupador){

                        //INSERTAR ID ALLIANZ EN QUERY
                        List<Account> listAccount = [SELECT id FROM Account WHERE Name = 'ALLIANZ' AND RecordTypeId =: TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING) ];
                        //List<Account> listAccount = [SELECT id FROM Account WHERE Name LIKE '%ACME - Spain%' ];
                        system.debug('TGS_Ebounding_WS.createOrder ## listAccount: ' + listAccount);

                        if (!listAccount.isEmpty()){
                            
                            oCase = TGS_Ebounding_Helper.createCase(listAccount.get(0).id, wOrder);
                            wResponse = createPostCaseResponse(oCase);

                            //VALORAR USAR EL ID o EL CASENUMBER
                            //RestContext.response.headers.put('Location', API_ROOT+'/tickets/'+oCase.Id);
                            RestContext.response.headers.put('Location', API_ROOT+'/tickets/'+oCase.CaseNumber);
                            RestContext.response.statusCode = 201;

                        } else {

                            wResponse.error = 'SVC10604';
                            wResponse.message = 'accountId value set invalid ';
                            RestContext.response.statusCode = 404;

                        }

                    } else {

                        //ALREADY EXIST ANOTHER CASE WITH THE SAME TGS_Agrupador
                        wResponse.error = 'SVC1024';
                        wResponse.message = 'Repeated query parameter: '+wOrder.parentTicket;
                        RestContext.response.statusCode = 400;

                    }
               

                } else if ( String.IsNotBlank(sOrderType) && (sOrderType.equals(Constants.TYPE_CHANGE) || sOrderType.equals(Constants.TYPE_DISCONNECT)) ){

                    Boolean existEboundingId = TGS_Ebounding_Helper.existEboundingId(wOrder.correlationId);

                    if (!existEboundingId){
                        
                        if (existTGS_Agrupador){

                            Boolean areGroupCasesClosedOrCancelled = TGS_Ebounding_Helper.areGroupCasesClosedOrCancelled(wOrder.parentTicket);

                            if (areGroupCasesClosedOrCancelled){

                                NE__Asset__c oComercialAsset = TGS_Ebounding_Helper.getComercialAssetFromCase(wOrder.parentTicket); //TGS_Agrupador
                                oOrder = TGS_Ebounding_Helper.createModificationOrTerminationOrder(oComercialAsset, sOrderType);
                            
                                if (oOrder!=null){

                                    oOrder.Case__r.BI_Id_SistemaLegado__c = wOrder.correlationId;
                                    oOrder.Case__r.TGS_Agrupador__c = wOrder.parentTicket;
                                    oOrder.Case__r.Description = wOrder.description;
                                    System.debug('TGS_Ebounding_WS.createOrder :: oOrder.Case__r '+oOrder.Case__r);
                                    update oOrder.Case__r;
                                    
                                    wResponse = createPostCaseResponse(oOrder.Case__r);
            
                                    //VALORAR USAR EL ID o EL CASENUMBER
                                    //RestContext.response.headers.put('Location', API_ROOT+'/tickets/'+oOrder.Case__r.Id);
                                    RestContext.response.headers.put('Location', API_ROOT+'/tickets/'+oOrder.Case__r.CaseNumber);
                                    RestContext.response.statusCode = 201;  
                            
                                } else {
                                    
                                    //REVISAR RESPUESTA DE ERROR PARA ESTE CASO
                                    wResponse.error = 'SVR1000';
                                    wResponse.message = 'Generic Server Error: ORDER COULD NOT BE CREATED';
                                    RestContext.response.statusCode = 500;
                                }

                            } else {

                                wResponse.error = 'SVC0001';
                                wResponse.message = 'Generic Client Error: All Cases grouped by parentTicket '+wOrder.parentTicket+', must be Closed or Cancelled';
                                RestContext.response.statusCode = 400;
                            }

                        } else {

                            wResponse.error = 'SVC1006';
                            wResponse.message = 'Resource '+wOrder.parentTicket+' does not exist';
                            RestContext.response.statusCode = 404; 
                        }

                    } else {

                        wResponse.error = 'SVC1024';
                        wResponse.message = 'Repeated query parameter: '+wOrder.correlationId;
                        RestContext.response.statusCode = 400;
                    }

                } else {

                    wResponse.error = 'SVC0003';
                    wResponse.message = 'Invalid parameter value: '+sOrderType+ '. Possible values are: New, Change or Disconnect';
                    RestContext.response.statusCode = 400;
                }

            } else {

                wResponse.error = 'SVC1023';
                wResponse.message = 'Parser Error: JSON content not well formed"';
                RestContext.response.statusCode = 400;
            }

        } catch (Exception e) {

            system.debug('TGS_Ebounding_WS.createOrder ## EXCEPTION: ' + e.getMessage());
            wResponse.error = 'SVR1000';
            wResponse.message = 'Generic Server Error: ' +e.getMessage();
            RestContext.response.statusCode = 500;
            BI_LogHelper.generate_BILog('TGS_Ebounding_WS.createOrder', 'BI_EN', e, 'Web Service');

        }
        system.debug('TGS_Ebounding_WS.createOrder ## wResponse: ' + wResponse);
        RestContext.response.responseBody = Blob.valueof(Json.serializepretty(wResponse, true));
        System.debug('TGS_Ebounding_WS.createOrder :: RestContext.response ' +RestContext.response);
        //return wResponse;

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that updates an Order and a Case according to IN parameters

    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:

    <Date>            <Author>          <Description>
    26/09/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*@HttpPut
    global static void updateOrder() {

        GenericResponse wResponse = new GenericResponse();

        try {

            string sserialize = RestContext.request.requestBody.tostring();
            system.debug('TGS_Ebounding_WS.updateOrder ## deserialize: ' + sserialize);
            TicketStatusChangeType wStatus = (TicketStatusChangeType)Json.deserialize(sserialize, TicketStatusChangeType.Class);
            system.debug('TGS_Ebounding_WS.updateOrder ## wStatus: ' + wStatus);


            String idCaseEbounding = RestContext.request.requestURI.split('/')[2];

            List<Case> listQueryCase = [SELECT id, Status, CaseNumber, BI_Id_SistemaLegado__c FROM Case WHERE BI_Id_SistemaLegado__c =: idCaseEbounding];

            if (!listQueryCase.isEmpty()) {

                if (wStatus!=null){

                    if ( (String.isNotBlank(wStatus.ticketStatus) ) &&
                            (wStatus.ticketStatus.equals('Assigned') || 
                            wStatus.ticketStatus.equals('In Progress') || 
                            wStatus.ticketStatus.equals('Resolved') || 
                            wStatus.ticketStatus.equals('Closed') || 
                            wStatus.ticketStatus.equals('Cancelled')
                            ) 
                        ) {

                        listQueryCase.get(0).Status = wStatus.ticketStatus;
                        update listQueryCase.get(0);
                        wResponse = createPutCaseResponse(listQueryCase.get(0));

                        RestContext.response.headers.put('Location', API_ROOT+'/tickets/'+listQueryCase.get(0).BI_Id_SistemaLegado__c);
                        RestContext.response.statusCode = 400;


                    } else {

                        wResponse.error = 'SVC0003';
                        wResponse.message = 'Invalid parameter value: '+wStatus.ticketStatus+ '. Possible values are: Assigned, In Progress, Resolved, Closed, Cancelled';
                        RestContext.response.statusCode = 400;

                    }

                } else {
                    
                    wResponse.error = 'SVC1023';
                    wResponse.message = 'Parser Error: JSON content not well formed"';
                    RestContext.response.statusCode = 400;
                }
                
            } else {

                wResponse.error = 'SVC1006';
                wResponse.message = 'Resource '+idCaseEbounding+' does not exist';
                RestContext.response.statusCode = 404;
            }


        } catch (Exception e){

            system.debug('TGS_Ebounding_WS.updateOrder ## EXCEPTION: ' + e.getMessage());
            wResponse.error = 'SVR1000';
            wResponse.message = 'Generic Server Error: ' +e.getMessage();
            RestContext.response.statusCode = 500;
            BI_LogHelper.generate_BILog('TGS_Ebounding_WS.updateOrder', 'BI_EN', e, 'Web Service');

        }

        system.debug('TGS_Ebounding_WS.updateOrder ## wResponse: ' + wResponse);
        RestContext.response.responseBody = Blob.valueof(Json.serializepretty(wResponse, true));
        System.debug('TGS_Ebounding_WS.updateOrder :: RestContext.response ' +RestContext.response);

            

    }*/

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   Method that creates a custom response

    IN:            Case
    OUT:           GenericResponse
    
    History:

    <Date>            <Author>          <Description>
    30/08/2017        Oscar Ena        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static GenericResponse createPostCaseResponse(Case oCase){

        GenericResponse wResponse = new GenericResponse();
        wResponse.ticketDetailType = new TicketDetailType();
        
        //VALORAR USAR EL ID o EL CASENUMBER 
        //wResponse.ticketDetailType.id = oCase.Id
        wResponse.ticketDetailType.id = oCase.CaseNumber;
        
        wResponse.ticketDetailType.href = oCase.Id;
        wResponse.ticketDetailType.description = 'SUCCESSFUL';
        wResponse.ticketDetailType.creationDate = oCase.CreatedDate;
        wResponse.ticketDetailType.severity = 'DUMMY';
        wResponse.ticketDetailType.type = oCase.Type;
        wResponse.ticketDetailType.status = oCase.Status;
        
        return wResponse;
    }


    /*private static GenericResponse createPutCaseResponse(Case oCase){

        GenericResponse wResponse = new GenericResponse();
        wResponse.TicketStatusInfoType = new TicketStatusInfoType();
        
        //VALORAR SI USAR ID O CASENUMBER
        //wResponse.ticketStatusInfoType.ticketId = oCase.id;
        wResponse.ticketStatusInfoType.ticketId = oCase.CaseNumber;
        wResponse.ticketStatusInfoType.statusChangeDate = System.now();
        wResponse.ticketStatusInfoType.ticketstatus = oCase.Status;
        
        return wResponse;

    }*/



    global class GenericResponse{
        
        webservice String error;
        webservice String message;

        webservice TicketDetailType ticketDetailType;
        //webservice TicketStatusInfoType ticketStatusInfoType;
       
    }

    //REQUEST POST
    global class TicketRequestType{

        webservice String correlationId; //SI id externo allianz 
        webservice String subject; // NO
        webservice String description; // SI informacion atributo valor Allianz
        webservice String country; // NO
        webservice String customerId; // NO
        webservice String accountId; // NO 
        webservice DateTime reportedDate; // NO
        webservice String severity; // SI introducir valor dummy o adicional si se precisa 
        webservice Integer priority; // NO 
        webservice String type; // SI new, change, disconnect 
        webservice String source; // NO 
        webservice String parentTicket; // SI Id que identifica el servicio sobre el que se quiere generar la orden  
        webservice String relatedParty; //NO   array of RelatedPartyType 
        webservice String relatedObject; //NO  array of RelatedObjectType 
        webservice List<TicketNoteInfoType> note; // NO 
        webservice List<TicketAttachmentInfoType> attachment; // NO 
        webservice List<KeyValueType> additionalData; // NO 
        webservice String callbackUrl; // NO 
    
    }

    global class TicketNoteInfoType{
        webservice String noteId; 
        webservice DateTime date_1; 
        webservice String author; 
        webservice String text;
        webservice List<KeyValueType> additionalData; 
    }

    global class TicketAttachmentInfoType{
        webservice String attachmentId;
        webservice String creationDate; 
        webservice String author; 
        webservice String name;
        webservice String documentLink;
        webservice List<KeyValueType> additionalData;
    }

    global class KeyValueType{
        webservice String key;
        webservice String value;
    }

    //RESPUESTA OK POST
    global class TicketDetailType{

        webservice String id; // SI ID del caso SF 
        webservice String href; // SI anyURI 
        webservice String correlationId; // NO
        webservice String subject; // NO 
        webservice String description; // SI En el caso de sincronización exitosa podemos enviar el valor “Successful” por ejemplo.
        webservice String country; // NO 
        webservice String customerId; // NO 
        webservice String accountId; // NO
        webservice DateTime reportedDate; // NO 
        webservice DateTime creationDate; // SI Fecha del momento que se realiza la llamada por ejemplo
        webservice String severity; // SI Aquí podemos devolver el mismo valor que se envió en la llamada o un valor Dummy. 
        webservice Integer priority; // NO 
        webservice String requestedSeverity; // NO 
        webservice Integer requestedPriority; // NO 
        webservice String type; // SI new, change, discconect
        webservice String source; // NO 
        webservice String parentTicket; // NO 
        webservice String status; //SI  Aquí podemos mandar el valor “Assigned”, que es el estado con el que se generaría el caso.
        webservice String subStatus; // NO 
        webservice DateTime statusChangeDate; // NO
        webservice String statusChangeReason; // NO 
        webservice DateTime targetResolutionDate; // NO 
        webservice DateTime resolutionDate; // NO 
        webservice String resolution; //NO 
        webservice String responsibleParty; // NO
        webservice List<RelatedPartyType> relatedParty; // NO 
        webservice List<RelatedObjectType> relatedObject; // NO 
        webservice List<TicketNoteInfoType> note; // NO 
        webservice List<TicketAttachmentInfoType> attachment; // NO 
        webservice List<KeyValueType> additionalData; // NO

    }

    global class RelatedPartyType {
        webservice String role;
        webservice String reference; 
        webservice String href;
    }

    global class RelatedObjectType {
        webservice String involvement;
        webservice String reference; 
        webservice String href; 
        webservice TimePeriodType validFor;
    }

    global class TimePeriodType {
        webservice DateTime startDateTime;
        webservice DateTime endDateTime;
    }

    //REQUEST PUT
    global class TicketStatusChangeType{
        webservice String statusChangeReason; // NO
        webservice String ticketStatus; // SI Estado al que se ha cambiado la orden en Salesforce
        webservice String ticketSubstatus; // NO

    }

    //RESPONSE OK PUT
    global class TicketStatusInfoType{
        webservice DateTime statusChangeDate; // SI Fecha del momento que se realiza la llamada por ejemplo
        webservice String statusChangeReason; // NO 
        webservice String ticketId; // SI Id externo de Allianz 
        webservice String ticketStatus; // SI Estado al que se ha actualizado la orden
        webservice String ticketSubstatus; // NO

    }



}