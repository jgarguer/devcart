/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-03-24      Daniel ALexander Lopez (DL)     Cloned Class      
*************************************************************************************/

public with sharing class BI_COL_CreateUserOnTRS_cls {



  @Future (callout=true)
  public static void wsMethodCreacion(List<String> IdCuentas)
  {
		System.debug('*******Cuentas a consultar: '+IdCuentas);

		ws_TrsMasivo.staffSincronizado[] staffInfos = new ws_TrsMasivo.staffSincronizado[0];

     List <Account> usuarioCuenta = [Select id, Name,BI_No_Identificador_fiscal__c,OwnerId,
			Owner.BI_COL_Id_TRS__c,
			Owner.AccountId,
			Owner.EmployeeNumber,
			Owner.UserRoleId,
			Owner.MobilePhone,
			Owner.Email,
			Owner.FirstName,
			(SELECT  UserId,User.Firstname,User.Lastname,TeamMemberRole, User.MobilePhone, User.Email  FROM  AccountTeamMembers WHERE TeamMemberRole ='Service Manager')
			From Account
			Where Id In :IdCuentas limit 1];

	  System.debug('*******Cuentas encontradas: '+usuarioCuenta);

	  for(Account account : usuarioCuenta)
	  {

    List<AccountTeamMember> objAccountTeamMembers = account.AccountTeamMembers;

	  Integer nit=0;

	  try 
    { 
    			nit=Integer.valueOf(account.BI_No_Identificador_fiscal__c);  

		}catch(Exception ex)
      {
		        nit=1; 
		  }

    ws_TrsMasivo.staffSincronizado staffServManag = new ws_TrsMasivo.staffSincronizado();
    Integer idTRSServM=0;

    try
    { 
      if(!objAccountTeamMembers.isEmpty())
      {
            idTRSServM= Integer.valueOf(objAccountTeamMembers[0].User.BI_Peru_STC_Per__c);  
      }
    } 
    catch(Exception ex)
    {
        idTRSServM=1; 
    }
   
    //System.debug('*******Celular : '+objAccountTeamMembers[0].user.MobilePhone);
    if(!objAccountTeamMembers.isEmpty())
    {
		staffServManag.celular           = objAccountTeamMembers[0].user.MobilePhone;
		staffServManag.correoElectronico = objAccountTeamMembers[0].user.Email;
	}


	  staffServManag.nit = nit;
	  staffServManag.tipoStaff = 8;
	  staffServManag.usuarioId = idTRSServM;
			ws_TrsMasivo.staffSincronizado staffAsesorV = new ws_TrsMasivo.staffSincronizado();
	  Integer idTRSSerAsesor=0;

	  try { 
			 idTRSSerAsesor=Integer.valueOf(account.Owner.BI_COL_Id_TRS__c);  
		  }catch(Exception ex)
		  {
			 idTRSSerAsesor=1;
		  }

			staffAsesorV.celular = account.Owner.MobilePhone;
			staffAsesorV.correoElectronico = account.Owner.Email;
			staffAsesorV.nit =nit;
			staffAsesorV.tipoStaff = 5;
			staffAsesorV.usuarioId = idTRSSerAsesor;

			staffInfos.add(staffAsesorV);
			staffInfos.add(staffServManag);
	}

		System.debug('Datos Para consumir servicio sincronizacionStaff: '+staffInfos);

		ws_TrsMasivo.sincronizacionStaffResponse[] respuestas=null;
		try{
		  if(!Test.isRunningTest()){
			  ws_TrsMasivo.serviciosSISGOTSOAP wsSISGOTSOAP = new ws_TrsMasivo.serviciosSISGOTSOAP();
			  respuestas = wsSISGOTSOAP.sincronizacionStaff(staffInfos);
			  system.debug('respuesta: ' +respuestas );
		  }else{
			ws_TrsMasivo.sincronizacionStaffResponse res = new ws_TrsMasivo.sincronizacionStaffResponse();
			res.codigoError='0';
			res.mensajeError='Datos creados correctamenten TRS';
			respuestas = new ws_TrsMasivo.sincronizacionStaffResponse[0];
			respuestas.add(res);
		  }

			if(respuestas!=null && respuestas.size()>0){
			  System.debug('Total Registros enviados: '+staffInfos.size()+' - Total Respuestas recibidas: '+respuestas.size()+'\n');
			  for(ws_TrsMasivo.sincronizacionStaffResponse res : respuestas){
					System.debug('Informacion de la Respuesta: '+res.codigoError+' - '+res.mensajeError);
				}
			}else{
				System.debug('La respuesta para el servicio sincronizacionStaff fue nula');
			}
		}catch(Exception e){
		   System.debug('Fallo la comunicacion. Excepcion: \n'+e);
		}
		
  }
	@InvocableMethod
	public static void invokeapexcallout(list<Account> lstAcc) 
	{
		List<String> idCuentas = new List<String>();
		/*User usu=[Select CompanyName,Profile.Name,Pais__c from User where id=: userinfo.getuserid()];*/
		
		if(!System.isFuture() && !System.isBatch()) //&& usu.Pais__c!=null// && usu.Pais__c=='Colombia')
		{
			System.debug('\n\n########------->Ejecución Trigger crearUsuarioTRS \n\n');
			for(Account acc : lstAcc)
			{
				idCuentas.add(acc.Id);
			}
			BI_COL_CreateUserOnTRS_cls.wsMethodCreacion(idCuentas);
		}
  }

}