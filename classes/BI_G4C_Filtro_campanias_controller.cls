public with sharing class BI_G4C_Filtro_campanias_controller {
	
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Julio Alberto Asenjo García
	    Company:       everis
	    Description:   Controller for the campaign filter in Clients Map (Lightning Component: BI_G4C_Filtro_Campanias)
	    
	    History: 
	    
	    <Date>                          <Author>                    	<Change Description>
	    20/06/2016                      Julio Alberto Asenjo García     Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    @AuraEnabled

    public static String getOptions(){
        
        List<PickListItem> resul = new List<PickListItem>();
            PickListItem pil;
            pil = new PickListItem();
            pil.value='';
            pil.label='Ninguna';
            resul.add(pil);
        for (AggregateResult c: [SELECT CampaignId, Campaign.Name campaignName FROM CampaignMember where Campaign.Status='In Progress' AND ContactId  IN (SELECT Id FROM Contact WHERE OwnerId =:UserInfo.getUserId())GROUP BY CampaignId, Campaign.Name]){
            pil = new PickListItem();
            pil.value=String.valueOf(c.get('CampaignId'));
            pil.label=string.valueof(c.get('campaignName'));
            resul.add(pil);
        }     
        //resul.sort();
        
        String jsonResul = JSON.serialize(resul);        
        return jsonResul;
        
    }
    
    public class PickListItem {

        public String myclass = 'optionClass';
        public String labelClass = 'texto';
        public String label {get; set;}
        public String value {get; set;}   
        public String ptype {get; set;} 
        
    }
    
}