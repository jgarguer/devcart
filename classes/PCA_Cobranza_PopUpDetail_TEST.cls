@isTest
private class PCA_Cobranza_PopUpDetail_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_Cobranza_PopUpDetail class 
    
    History: 
    <Date> 					<Author> 				<Change Description>
    13/08/2014      		Ana Escrich	    		Initial Version
    16/02/2017				Pedro Párraga			Increase coverage
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void getloadInfo() {
 		User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());    	
	    System.runAs(usr){
	 		List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(2);
	        
	        List<Account> accounts = BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais);
	       	List<Account> accList = new List<Account>();

	        for(Account item: accounts){
	            	item.OwnerId = usr.Id;
	            	accList.add(item);
	        }

	        update accList;
	        
	        List<Contact> con = BI_DataLoad.loadPortalContacts(1, accounts);
			User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());
			BI_Facturacion__c cobranzaObject = new BI_Facturacion__c(BI_cliente__c=accounts[0].Id);
			insert cobranzaObject;
						
			 PageReference pageRef = new PageReference('PCA_Cobranza_PopUpDetail');
       		 Test.setCurrentPage(pageRef);
       		 ApexPages.currentPage().getParameters().put('Id', cobranzaObject.Id);
	   		 
        	 PCA_Cobranza_PopUpDetail controller = new PCA_Cobranza_PopUpDetail();        	 
        	 BI_TestUtils.throw_exception = false;
        	 controller.checkPermissions();
        	 controller.loadInfo();

		
		System.runAs(user1){
			 ApexPages.currentPage().getParameters().put('Id', cobranzaObject.Id);
			 BI_TestUtils.throw_exception = true;
			 controller.checkPermissions();
        	 controller.loadInfo();
        	 BI_TestUtils.throw_exception = false;
        	 controller.createTask();
        	 System.assertEquals(cobranzaObject.Id,ApexPages.currentPage().getParameters().get('Id'));
			}
		}
 	}
}