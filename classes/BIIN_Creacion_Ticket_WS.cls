public class BIIN_Creacion_Ticket_WS extends BIIN_UNICA_Base
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jose María Martín Castaño
    Company:       Deloitte
    Description:   Función para la creación de Tickets en RoD.
        
    History:
        
    <Date>             <Author>                             <Description>
    10/12/2015         Jose María Martín Castaño            Initial version
    15/02/2016         Micah Burgos García                  Add BI_LogHelper to catch and save exceptions.
    20/05/2016         José Luis González Beltrán           Adapt to UNICA interface
    21/06/2016         José Luis González Beltrán           Fix attachments inside a note
    29/06/2016         José Luis González Beltrán           Fix ticketRequest additionalData key names & send parent CaseNumber in ticketRequest.parentTicket
    30/06/2016         José Luis González Beltrán           Retrieve additionalData key values
    10/01/2017         Antonio Torres - Avanxo              Se añaden campos para la implementación de BI_EN Fase 2 en Colombia
    22/08/2017         Javier Martínez Sanz                 Se corrige viewAccess por viewAcces en la línea 220
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String StringWS {get; set;}

    private final static String ST_USER_SERVICE_RESTORATION = 'Restauración de servicio a usuario';

    public Boolean crearTicket(String authorizationToken, Case caso, List<Adjunto> ladjuntos, CamposOutsiders co) 
    {
        if(Test.isRunningTest()) 
        {
            StringWS = '';
        } 
        
        // Callout URL
        String url = 'callout:BIIN_WS_UNICA/ticketing/v1/tickets';

        BIIN_UNICA_Pojos.TicketRequestType ticketRequest = new BIIN_UNICA_Pojos.TicketRequestType();

        System.debug(' BIIN_Creacion_Ticket_WS.crearTicket | caso: ' + caso);

        Case ticket = [SELECT CaseNumber, Status, Id, AccountId, ParentId, Parent.CaseNumber, 
        BI_Country__c, BI2_Nombre_Contacto_Final__c, BI2_Apellido_Contacto_Final__c , 
        BI2_Email_Contacto_Final__c, TGS_Impact__c, TGS_Urgency__c, Origin, Subject, 
        BIIN_Id_Producto__c, BIIN_Form_Name__c, BI_COL_Codigo_CUN__c, TGS_Ticket_Site__c, 
        Description, BI_Product_Categorization_Tier_1__c, BI_Product_Categorization_Tier_2__c, 
        BI_Product_Categorization_Tier_3__c, BIIN_Categorization_Tier_1__c, 
        BIIN_Categorization_Tier_2__c, BIIN_Categorization_Tier_3__c, BI_COL_Fecha_Radicacion__c,
        Priority, BI_Line_of_Business__c, BI_Product_Service__c, BI2_PER_Descripcion_de_producto__c
        /*Campos agregados en la consulta para la fase dos de colombia*/
        ,BI_ECU_Telefono_fijo__c,BI_ECU_Telefono_movil__c,BI2_COL_Contacto_de_cierre__c,BI2_COL_Fecha_generacion_CUN__c, BIIN_Site__c, TGS_Site_details_contacts__c
        /*FIN Campos */
        FROM Case 
        WHERE Id =: caso.Id 
        LIMIT 1];

        System.debug(' BIIN_Creacion_Ticket_WS.crearTicket | ticket: ' + ticket);

        Account[] qAccount = [SELECT BI_Validador_Fiscal__c, Name, BI_Country__c, BI_No_Identificador_fiscal__c 
                                FROM Account 
                               WHERE Id =: ticket.AccountId];
        String casoPadre = '';
        if(ticket.ParentId != null) 
        {
            casoPadre = ticket.Parent.CaseNumber; // cp[0].caseNumber;
        }

        ticketRequest.correlatorId      = ticket.CaseNumber;
        ticketRequest.subject           = ticket.Subject;
        ticketRequest.description       = ticket.Description;
        ticketRequest.country           = ticket.BI_Country__c;
        ticketRequest.ticketType        = 'Incident';
        // ticketRequest.perceivedSeverity = ticket.TGS_Impact__c.substring(0, 2) + BIIN_UNICA_Utils.translateFromRemedy('Impacto', ticket.TGS_Impact__c.substring(2));
        ticketRequest.perceivedSeverity = ticket.TGS_Impact__c;

        System.debug(' BIIN_Creacion_Ticket_WS.crearTicket | TGS_Urgency__c: ' + ticket.TGS_Urgency__c);
        try
        {
            ticketRequest.perceivedPriority = Integer.valueOf(BIIN_UNICA_Utils.translateFromRemedyValueToRemedyID('Urgencia', ticket.TGS_Urgency__c.substring(2)));
        }
        catch (Exception e)
        {
            ticketRequest.perceivedPriority = 4000;
        }
        ticketRequest.source            = BIIN_UNICA_Utils.translateFromSalesforce('Origen', ticket.Origin);
        ticketRequest.customerId        = qAccount[0].BI_Validador_Fiscal__c;
        
        ticketRequest.reportedDate      = BIIN_UNICA_Utils.dateTimeToSeconds(ticket.BI_COL_Fecha_Radicacion__c);
        
        ticketRequest.parentTicket      = casoPadre;

        List<BIIN_UNICA_Pojos.KeyValueType> additionalData = new List<BIIN_UNICA_Pojos.KeyValueType>();

        String strCountry = ticketRequest.country;
        if(String.isEmpty(strCountry)) {
            strCountry = qAccount[0].BI_Country__c;
        }

        if(strCountry == Label.BI_Colombia) {
            // Campos añadidos para fase 2 de Colombia
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('nitCompany', qAccount[0].BI_No_Identificador_fiscal__c));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('givenName', 'SEDE'));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('familyName', ticket.BIIN_Site__c));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('email', ticket.TGS_Site_details_contacts__c));
            
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('contactFirstName', ticket.BI2_Nombre_Contacto_Final__c));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('contactLastName', ticket.BI2_Apellido_Contacto_Final__c));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('contactEmail', ticket.BI2_Email_Contacto_Final__c));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('phone', ticket.BI_ECU_Telefono_fijo__c));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('mobile', ticket.BI_ECU_Telefono_movil__c));
            
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('closingContact', ticket.BI2_COL_Contacto_de_cierre__c));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('site', ticket.BIIN_Site__c));
        } else {
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('givenName', ticket.BI2_Nombre_Contacto_Final__c));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('familyName', ticket.BI2_Apellido_Contacto_Final__c));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('email', ticket.BI2_Email_Contacto_Final__c));
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('site', ticket.TGS_Ticket_Site__c));
        }
        
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('serviceType', BIIN_UNICA_Utils.translateFromSalesForce('Tipo de Incidencia', ST_USER_SERVICE_RESTORATION)));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('company', qAccount[0].Name));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('ciName', ticket.BIIN_Id_Producto__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('formName', ticket.BIIN_Form_Name__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('reconciliationId', ticket.BI_COL_Codigo_CUN__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('productCategorization1', caso.BI_Line_of_Business__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('productCategorization2', caso.BI_Product_Service__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('productCategorization3', caso.BI2_PER_Descripcion_de_producto__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('operationalCategorization1', caso.BIIN_Categorization_Tier_1__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('operationalCategorization2', caso.BIIN_Categorization_Tier_2__c));
        additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('operationalCategorization3', caso.BIIN_Categorization_Tier_3__c));

        // Campos añadido para fase 2 de Colombia
        if(!String.isEmpty(ticket.BI_COL_Codigo_CUN__c)) {
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('BAO_ExternalID', ticket.BI_COL_Codigo_CUN__c));
        }
        
        if(ticket.BI2_COL_Fecha_generacion_CUN__c != null) {
            additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('cundDate', BIIN_UNICA_Utils.dateTimeToSeconds(ticket.BI2_COL_Fecha_generacion_CUN__c)));
        }

        ticketRequest.additionalData = additionalData;

        /*
        // ticket attachments in attachments field
        List<BIIN_UNICA_Pojos.TicketAttachmentRequestType> attachments = new List<BIIN_UNICA_Pojos.TicketAttachmentRequestType>();
        BIIN_UNICA_Pojos.TicketAttachmentRequestType ticketAttachmentRequest;
        BIIN_UNICA_Pojos.AttachmentType attachmentType;
        BIIN_UNICA_Pojos.AttachmentSummaryType attachmentSummary;

        if(ladjuntos != null) 
        {
            for(Integer i = 0; i < ladjuntos.size(); i++)
            {
                if(ladjuntos[i].adjId != null) 
                {
                    attachmentSummary = new BIIN_UNICA_Pojos.AttachmentSummaryType();
                    attachmentSummary.name = ladjuntos[i].name;

                    attachmentType = new BIIN_UNICA_Pojos.AttachmentType();
                    attachmentType.content = EncodingUtil.base64Encode(ladjuntos[i].body);
                    attachmentType.attachmentSummary = attachmentSummary;

                    ticketAttachmentRequest = new BIIN_UNICA_Pojos.TicketAttachmentRequestType();
                    ticketAttachmentRequest.name = ladjuntos[i].name;
                    ticketAttachmentRequest.attachment = attachmentType;

                    attachments.add(ticketAttachmentRequest);
                }
            }
        }

        ticketRequest.attachments = attachments;
        */

        // ticket attachments inside a note
        if(ladjuntos != null && !ladjuntos.isEmpty()) 
        {
            ticketRequest.notes = new List<BIIN_UNICA_Pojos.TicketNoteRequestType>();
            BIIN_UNICA_Pojos.TicketNoteRequestType ticketNoteRequest;
            BIIN_UNICA_Pojos.TicketAttachmentRequestType ticketAttachmentRequest;
            BIIN_UNICA_Pojos.AttachmentType attachmentType;
            BIIN_UNICA_Pojos.AttachmentSummaryType attachmentSummary;

            ticketNoteRequest = new BIIN_UNICA_Pojos.TicketNoteRequestType();
            ticketNoteRequest.text = '';
            ticketNoteRequest.attachments = new List<BIIN_UNICA_Pojos.TicketAttachmentRequestType>();

            for(Integer i = 0; i < ladjuntos.size(); i++)
            {
                if(ladjuntos[i].adjId != null) 
                {
                    attachmentSummary = new BIIN_UNICA_Pojos.AttachmentSummaryType();
                    attachmentSummary.name = ladjuntos[i].name;

                    attachmentType = new BIIN_UNICA_Pojos.AttachmentType();
                    attachmentType.content = EncodingUtil.base64Encode(ladjuntos[i].body);
                    attachmentType.attachmentSummary = attachmentSummary;

                    ticketAttachmentRequest = new BIIN_UNICA_Pojos.TicketAttachmentRequestType();
                    ticketAttachmentRequest.name = ladjuntos[i].name;
                    ticketAttachmentRequest.attachment = attachmentType;

                    ticketNoteRequest.attachments.add(ticketAttachmentRequest);
                }
            }

            // Additional Data
            ticketNoteRequest.additionalData = new List<BIIN_UNICA_Pojos.KeyValueType>();

            Account account = getAccount(caso.accountId);
            if (account != null)
            {
                ticketNoteRequest.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('companyId', account.BI_Validador_Fiscal__c));
            }
            ticketNoteRequest.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('viewAcces', 'Public'));
            ticketNoteRequest.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('workLogType', ''));
            ticketNoteRequest.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('workLogDate', String.valueof(System.currentTimeMillis()/1000)));

            ticketRequest.notes.add(ticketNoteRequest);
        }

        // Prepare Request
        HttpRequest req = getRequest(url, 'POST', ticketRequest);

        // Get response
        try 
        {
            BIIN_UNICA_Pojos.ResponseObject response = getResponse(req, BIIN_UNICA_Pojos.ApiTransactionType.class);
            System.debug(' BIIN_Creacion_Ticket_WS.crearTicket | La salida del servicio es (response): ' + response);

            BIIN_UNICA_Pojos.ApiTransactionType apiTransaction = (BIIN_UNICA_Pojos.ApiTransactionType) response.responseObject;
            System.debug(' BIIN_Creacion_Ticket_WS.crearTicket | Api Transaction (apiTransaction): ' + apiTransaction);
            StringWS = 'OK';
            ticket.Status = 'Being processed';
            System.debug('[CREAR_TICKET_WS - El estado del caso es: ] ' + ticket.Status);

            return true;
        }
        catch(IntegrationException ex)
        {
            System.debug('Error al llamar al backend: ' + ex.getMessage());
            BI_LogHelper.generate_BILog('BIIN_Creacion_Ticket_WS.crearTicket', 'BIIN', ex, 'WebService');
                        
            // Categories problem or Product Family?
            if (ex.getMessage().contains('SVC1021'))
            {
                StringWS = ex.getMessage();
                ticket.Status = 'Error de validación';

                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    //##############POJOS   
    public class Adjunto
    {
        public String name;
        public Blob body;
        public String adjId;
        public Adjunto(String name, Blob body)
        {
            this.name = name;
            this.body = body;
        }
        public Adjunto(String name, Blob body, String adjId)
        {
            this.name  = name;
            this.body  = body;
            this.adjId = adjId;
        }
    }
    
    public class CamposOutsiders
    {
        public String directContactFirstName;
        public String directContactLastName;
        public String directContactEmail;
    }
}