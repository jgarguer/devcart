/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Updates proposal items related to local opportunities.
    Test Class:    BI_O4_QueueableUpdateProposalItems_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    05/10/2016              Fernando Arteaga        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class BI_O4_QueueableUpdateProposalItems implements Queueable
{
	List<BI_O4_Proposal_Item__c> listPropItem;

	public BI_O4_QueueableUpdateProposalItems(List<BI_O4_Proposal_Item__c> listProposalItems)
	{
		this.listPropItem = listProposalItems;
	}

	public void execute(QueueableContext context)
	{
		try
		{
			if (!this.listPropItem.isEmpty())
			{
				update this.listPropItem;
			}
		}
		catch(Exception exc)
		{
			System.debug(exc.getMessage());
			BI_LogHelper.generate_BILog('BI_O4_QueueableUpdateProposalItems.execute', 'BI_EN', exc, 'Clase');
		}
	}
}