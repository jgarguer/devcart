@isTest
private class API_AC_Quotations_TEST {

    @testSetup
    static void createStuffTestQuotations() {

        CWP_AC_ServicesCost__c serviceCostTest = new CWP_AC_ServicesCost__c(
            CWP_AC_Country__c='Spain',
            CWP_AC_City__c='Madrid',            
            CWP_AC_Contract_Term__c = 12,
            CWP_AC_ServiceName__c = 'INTERNET ACCESS',
            CWP_AC_Bandwidth__c = 100000,
            CWP_AC_Router__c = 'YES',
            CWP_AC_Specifications__c = 'Test',
            CWP_AC_Technology__c = 'Test',
            CWP_AC_Coste_CPE_MRC__c = 700,
            CWP_AC_Coste_CPE_NRC__c = 700,
            CWP_AC_Coste_MRC__c = 700,
            CWP_AC_Coste_NRC__c = 800,
            CurrencyIsoCode = 'EUR',
            CWP_AC_Precio_Internacional_MRC__c = 700,
            CWP_AC_Precio_Internacional_NRC__c = 700

            );
        insert serviceCostTest;

        CWP_AC_MARGINDISCOUNTS__c marginDiscountTest = new CWP_AC_MARGINDISCOUNTS__c(
            Name = '010101',
            CWP_AC_Margin__c = 0.15,
            CWP_AC_Discount__c= 0.2,
            CWP_AC_ProductName__c='INTERNET ACCESS',            
            CWP_AC_ClientName__c = 'Cliente test CWP',
            CWP_AC_Country__c = 'Spain'

            );
        insert marginDiscountTest;

        Profile standardProfile = [select id from Profile where name = :Label.BI_StandardUser];
        string email = 'user@example'+String.valueOf(Math.random())+'.com';
        User engineer = new User(
            Username = 'testquotations@test.com',
            LastName = 'TestUser',
            CommunityNickname = 'nick' + String.valueOf(Math.random()),
            alias = 'testuser',
            Email = email,
            profileid = standardProfile.id,
            TimeZoneSidKey = 'America/New_York',
            EmailEncodingKey='UTF-8',
            BI_Permisos__c = 'Ingeniería/Preventa',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US');
        insert engineer;

        Account acc01  = new Account ();
				acc01.Name = 'Cliente test CWP';
				acc01.BI_Subsegment_Regional__c = 'test';
				acc01.BI_Territory__c = 'test';
				acc01.BI_Country__c = Label.Bi_Mexico;
				acc01.BI_Segment__c = 'Empresas';
				acc01.BI_Subsector__c = 'test';
				acc01.BI_Sector__c = 'test';
			insert acc01;    
        
    }
    
    
    @isTest 
    public static void testMalformedJSONERROR() {

        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = System.Label.BI_URLAPIQuotations;
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf('');
        RestContext.request = request;
        RestContext.response = response;

        // Call the method to test
        API_AC_Quotations.doPost();
        System.assertEquals('The JSON is malformated.System.JSONException: No content to map to Object due to end of input', response.responseBody.toString());
    }

    @isTest 
    public static void testMalfomedJSONRelatedPartiesErrorDifferentTwo() {

        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = System.Label.BI_URLAPIQuotations;
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf('{ "description":"solicitud de cotización", "relatedParties":[ { "id":"Cliente test CWP", "name":"CLIENTE FINAL", "role":"customer" } ], "quoteItems":[ { "id":"001", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"YES" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"001", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Country", "value":"Spain" }, { "valueType":"String", "@type":"StringType", "name":"City", "value":"Madrid" }, { "valueType":"String", "@type":"StringType", "name":"Contract-Term", "value":"12" }, { "valueType":"String", "@type":"StringType", "name":"Address", "value":"C/ Maria Zambrano" } ] }, "quantity":"1" }, { "id":"002", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"NO" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"002", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Country", "value":"Argentina" }, { "valueType":"String", "@type":"StringType", "name":"City", "value":"Buenos Aires" }, { "valueType":"String", "@type":"StringType", "name":"Contract-Term", "value":"12" }, { "valueType":"String", "@type":"StringType", "name":"Address", "value":"C/ Maria Sambrano" } ] }, "quantity":"1" } ], "callbackUrl":"/Consumerendpoint/callbackurl"}');
        RestContext.request = request;
        RestContext.response = response;

        // Call the method to test
        API_AC_Quotations.doPost();
        System.assertEquals('The JSON is malformated at relatedParties.', response.responseBody.toString());
    }

    @isTest 
    public static void testMalfomedJSONRelatedPartiesUseer() {

        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = System.Label.BI_URLAPIQuotations;
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf('{ "description":"solicitud de cotización", "relatedParties":[ { "id":"testquotations@test.com", "name":"User", "role":"useer" }, { "id":"Cliente test CWP", "name":"CLIENTE FINAL", "role":"customer" } ], "quoteItems":[ { "id":"001", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"YES" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"001", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Country", "value":"Spain" }, { "valueType":"String", "@type":"StringType", "name":"City", "value":"Madrid" }, { "valueType":"String", "@type":"StringType", "name":"Contract-Term", "value":"12" }, { "valueType":"String", "@type":"StringType", "name":"Address", "value":"C/ Maria Zambrano" } ] }, "quantity":"1" }, { "id":"002", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"NO" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"002", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Country", "value":"Argentina" }, { "valueType":"String", "@type":"StringType", "name":"City", "value":"Buenos Aires" }, { "valueType":"String", "@type":"StringType", "name":"Contract-Term", "value":"12" }, { "valueType":"String", "@type":"StringType", "name":"Address", "value":"C/ Maria Sambrano" } ] }, "quantity":"1" } ], "callbackUrl":"/Consumerendpoint/callbackurl"}');
        RestContext.request = request;
        RestContext.response = response;

        // Call the method to test
        API_AC_Quotations.doPost();
        System.assertEquals('The JSON is malformated at relatedParties.', response.responseBody.toString());
    }


    @isTest 
    public static void testMalfomedJSONRelatedPartiesFirstUser() {

        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = System.Label.BI_URLAPIQuotations;
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf('{ "description":"solicitud de cotización", "relatedParties":[ { "id":"testquotations@test.com", "name":"User", "role":"user" }, { "id":"Cliente false CWP", "name":"CLIENTE FINAL", "role":"customer" } ], "quoteItems":[ { "id":"001", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"YES" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"001", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Country", "value":"Spain" }, { "valueType":"String", "@type":"StringType", "name":"City", "value":"Madrid" }, { "valueType":"String", "@type":"StringType", "name":"Contract-Term", "value":"12" }, { "valueType":"String", "@type":"StringType", "name":"Address", "value":"C/ Maria Zambrano" } ] }, "quantity":"1" }, { "id":"002", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"NO" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"002", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Country", "value":"Argentina" }, { "valueType":"String", "@type":"StringType", "name":"City", "value":"Buenos Aires" }, { "valueType":"String", "@type":"StringType", "name":"Contract-Term", "value":"12" }, { "valueType":"String", "@type":"StringType", "name":"Address", "value":"C/ Maria Sambrano" } ] }, "quantity":"1" } ], "callbackUrl":"/Consumerendpoint/callbackurl"}');
        RestContext.request = request;
        RestContext.response = response;

        // Call the method to test
        API_AC_Quotations.doPost();
        System.assertEquals('User or client are not valid.', response.responseBody.toString());
    }

    @isTest 
    public static void testMalfomedJSONRelatedPartiesFirstCustomer() {

        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = System.Label.BI_URLAPIQuotations;
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf('{ "description":"solicitud de cotización", "relatedParties":[ { "id":"Cliente false CWP", "name":"CLIENTE FINAL", "role":"customer" },{ "id":"testquotations@test.com", "name":"User", "role":"user" } ], "quoteItems":[ { "id":"001", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"YES" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"001", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Country", "value":"Spain" }, { "valueType":"String", "@type":"StringType", "name":"City", "value":"Madrid" }, { "valueType":"String", "@type":"StringType", "name":"Contract-Term", "value":"12" }, { "valueType":"String", "@type":"StringType", "name":"Address", "value":"C/ Maria Zambrano" } ] }, "quantity":"1" }, { "id":"002", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"NO" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"002", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Country", "value":"Argentina" }, { "valueType":"String", "@type":"StringType", "name":"City", "value":"Buenos Aires" }, { "valueType":"String", "@type":"StringType", "name":"Contract-Term", "value":"12" }, { "valueType":"String", "@type":"StringType", "name":"Address", "value":"C/ Maria Sambrano" } ] }, "quantity":"1" } ], "callbackUrl":"/Consumerendpoint/callbackurl"}');
        RestContext.request = request;
        RestContext.response = response;

        // Call the method to test
        API_AC_Quotations.doPost();
        System.assertEquals('User or client are not valid.', response.responseBody.toString());
    }
    @isTest 
    public static void testNullParents() {

        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = System.Label.BI_URLAPIQuotations;
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf('{ "description":"solicitud de cotización", "relatedParties":[ { "id":"Cliente test CWP", "name":"CLIENTE FINAL", "role":"customer" },{ "id":"testquotations@test.com", "name":"User", "role":"user" } ], "quoteItems":[ { "id":"001", "action":"add", "productOffering":{}, "quantity":"1" }, { "id":"001", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ {} ] }, "quantity":"1" }, { "id":"002", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"NO" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"002", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"Test", "entityType":"product" }, "product":{}, "callbackUrl":"/Consumerendpoint/callbackurl"}]}');
        RestContext.request = request;
        RestContext.response = response;

        // Call the method to test
        API_AC_Quotations.doPost();
        System.assertEquals('The data is not valid.', response.responseBody.toString());
    }
    @isTest 
    public static void testAllCorrect() {

        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = System.Label.BI_URLAPIQuotations;
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf('{ "description":"solicitud de cotización", "relatedParties":[ { "id":"Cliente test CWP", "name":"CLIENTE FINAL", "role":"customer" },{ "id":"testquotations@test.com", "name":"User", "role":"user" } ], "quoteItems":[ { "id":"001", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"YES" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"001", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Country", "value":"Spain" }, { "valueType":"String", "@type":"StringType", "name":"City", "value":"Madrid" }, { "valueType":"String", "@type":"StringType", "name":"Contract-Term", "value":"12" }, { "valueType":"String", "@type":"StringType", "name":"Address", "value":"C/ Maria Zambrano" } ] }, "quantity":"1" } ], "callbackUrl":"/Consumerendpoint/callbackurl"}');
        RestContext.request = request;
        RestContext.response = response;

        // Call the method to test
        API_AC_Quotations.doPost();
        System.assertEquals( true , response.responseBody.toString().contains('{"units":"eur","amount":"2207.0588235294117"}'));
    }
    @isTest 
    public static void testDataNotValid() {

        // Set up a test request
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = System.Label.BI_URLAPIQuotations;
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf('{ "description":"solicitud de cotización", "relatedParties":[ { "id":"Cliente test CWP", "name":"CLIENTE FINAL", "role":"customer" },{ "id":"testquotations@test.com", "name":"User", "role":"user" } ], "quoteItems":[ { "id":"001", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"NO" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"001", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Country", "value":"Spain" }, { "valueType":"String", "@type":"StringType", "name":"City", "value":"Madrid" }, { "valueType":"String", "@type":"StringType", "name":"Contract-Term", "value":"12" }, { "valueType":"String", "@type":"StringType", "name":"Address", "value":"C/ Maria Zambrano" } ] }, "quantity":"1" } ], "callbackUrl":"/Consumerendpoint/callbackurl"}');
        RestContext.request = request;
        RestContext.response = response;

        // Call the method to test
        API_AC_Quotations.doPost();
        System.assertEquals('The data is not valid.', response.responseBody.toString());
    }
  
    // @isTest 
    // public static void testMalfomedJSONRelatedPartiesError5() {

    //     // createTestServiceCost();
    //     // Set up a test request
    //     RestRequest request = new RestRequest();
    //     RestResponse response = new RestResponse();
    //     request.requestUri = System.Label.BI_URLAPIQuotations;
    //     request.httpMethod = 'POST';
    //     request.requestBody = Blob.valueOf('{ "description":"solicitud de cotización", "relatedParties":[ { "id":"testquotations@test.com", "name":"Usear", "role":"usaer" } ], "quoteItems":[ { "id":"001", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"YES" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"001", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Country", "value":"Spain" }, { "valueType":"String", "@type":"StringType", "name":"City", "value":"Madrid" }, { "valueType":"String", "@type":"StringType", "name":"Contract-Term", "value":"12" }, { "valueType":"String", "@type":"StringType", "name":"Address", "value":"C/ Maria Zambrano" } ] }, "quantity":"1" }, { "id":"002", "action":"add", "productOffering":{ "id":"ID_INTERNET ACCESS", "name":"INTERNET ACCESS", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Router", "value":"NO" }, { "valueType":"String", "@type":"StringType", "name":"Bandwidth", "value":"100000" } ] }, "quantity":"1" }, { "id":"002", "action":"add", "productOffering":{ "id":"ID_PARENT", "name":"PARENT", "entityType":"product" }, "product":{ "characteristics":[ { "valueType":"String", "@type":"StringType", "name":"Country", "value":"Argentina" }, { "valueType":"String", "@type":"StringType", "name":"City", "value":"Buenos Aires" }, { "valueType":"String", "@type":"StringType", "name":"Contract-Term", "value":"12" }, { "valueType":"String", "@type":"StringType", "name":"Address", "value":"C/ Maria Sambrano" } ] }, "quantity":"1" } ], "callbackUrl":"/Consumerendpoint/callbackurl"}');
    //     // request.params.put('category.id', 'Spain');
    //     // request.params.put('category.name', 'Madrid');
    //     // request.params.put('name', '12');
    //     // The JSON is malformated.System.JSONException: No content to map to Object due to end of input
    //     RestContext.request = request;
    //     RestContext.response = response;

    //     // Call the method to test
    //     API_AC_Quotations.doPost();
    //     // System.assertEquals('[{"name":"INTERNET ACCESS","frameworkAgreement":"YES","description":"100000"}]', response.responseBody.toString());
    //     System.debug('testMalfomedJSONRelatedPartiesError ## response.responseBody.toString() => ' + response.responseBody.toString());
    //     System.assertEquals('The JSON is malformated at relatedParties.', response.responseBody.toString());
    // }
    
}