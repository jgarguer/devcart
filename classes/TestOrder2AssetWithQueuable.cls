@isTest(SeeAllData=true)
private class TestOrder2AssetWithQueuable 
{
    static testMethod void Order2AssetWithQueuable()
    {        
        NE__Catalog_Header__c catHeader = new NE__Catalog_Header__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Active__c=true, NE__Enable_Global_Cart__c=false, NE__Name__c='CatalogTest', NE__Start_Date__c=system.today()-1);
        insert catHeader;
        
        NE__Catalog__c catalog = new NE__Catalog__c(CurrencyIsoCode='ARS', Name='TGSOL Catalog', NE__Active__c=true, NE__Catalog_Header__c = catHeader.Id, NE__Enable_for_oppty__c=false,  NE__Version__c=1.0, NE__Visible_web__c=false);
        insert catalog;
        
        NE__Catalog_Category__c catCategory = new NE__Catalog_Category__c(Name='TGSOL Catalog', CurrencyIsoCode='ARS', NE__CatalogId__c=catalog.Id);
        insert catCategory;
        
        NE__Commercial_Model__c commModel = new NE__Commercial_Model__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Catalog_Header__c=catHeader.Id, NE__Offline__c=false, NE__ProgramName__c='CatalogTest', NE__Status__c='Pending');
        insert commModel;
        
        NE__Product__c prod = new NE__Product__c(Name='WIFI', CurrencyIsoCode='ARS');
        insert prod;
        
        NE__Catalog_Item__c catItem = new NE__Catalog_Item__c(NE__ProductId__c=prod.Id, NE__Catalog_Id__c=catalog.Id, NE__Catalog_Category_Name__c=catCategory.Id, NE__Start_Date__c=system.today()-1, NE__BaseRecurringCharge__c=10.00, NE__Base_OneTime_Fee__c=100.00, NE__Min_Qty__c=0, NE__Max_Qty__c=99);
        insert catItem;
        
        RecordType RecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name LIKE '%Holding' LIMIT 1];            
        Account acc = new Account(Name = 'TestAccount', RecordTypeId = RecId.Id);
        insert acc;
        
        
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c='TGS';
        insert u;
        System.runAs(u) 
        {            
            NE__Asset__c asset = new NE__Asset__c(NE__AccountId__c=acc.Id, NE__BillAccId__c=acc.Id, NE__ServAccId__c=acc.Id);
            insert asset;
            asset = [SELECT Id, NE__AccountId__c, NE__BillAccId__c, NE__ServAccId__c FROM NE__Asset__c WHERE Id=:asset.Id];
            
            NE__Order__c ord = new NE__Order__c(NE__Asset__c=asset.Id, NE__AccountId__c=acc.Id, NE__Configuration_Type__c='New', NE__Type__c='InOrder');
            insert ord;
            ord = [SELECT Id, NE__AccountId__c, NE__Configuration_Type__c, NE__Type__c FROM NE__Order__c WHERE Id=:ord.Id];
            
            NE__OrderItem__c ordIt = new NE__OrderItem__c(NE__OrderId__c=ord.Id, NE__CatalogItem__c=catItem.Id, NE__ProdId__c=prod.Id, NE__Qty__c=1);
            insert ordIt;

            NE__Order_Item_Attribute__c ordAttribute = new NE__Order_Item_Attribute__c(NE__Order_Item__c=ordIt.Id, name = 'test', NE__Value__c = 'test');
            insert ordAttribute;
            
            ApexPages.StandardController contr = new ApexPages.StandardController(ord);
            NENewOrderExtension newordpage = new NENewOrderExtension(contr);
            newordpage.next();
            
            //Contact CaseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
            //insert CaseContact;
            
            Case newCase = [SELECT Id, Asset__c, Order__c, Status, Subject, Type, TGS_Service__c, ContactId, AccountId 
                            FROM Case 
                            WHERE Order__c =: ord.Id LIMIT 1];

            Test.startTest();
            
            String assetId  =   Order2AssetWithQueuable.callOrder2AssetMethod(ord.Id, newCase.id);
            assetId         =   Order2AssetWithQueuable.setAssetAdditionalFields(assetId, newCase.id, ord.Id);
            
            Order2AssetWithQueuable.order2Asset(ord.Id, newCase.id);
            
            Test.stopTest();           
        }
    }
    
    static testMethod void testQueuableSetAssetAdditionalFields()
    {
        NE__Catalog_Header__c catHeader = new NE__Catalog_Header__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Active__c=true, NE__Enable_Global_Cart__c=false, NE__Name__c='CatalogTest', NE__Start_Date__c=system.today()-1);
        insert catHeader;
        
        NE__Catalog__c catalog = new NE__Catalog__c(CurrencyIsoCode='ARS', Name='TGSOL Catalog', NE__Active__c=true, NE__Catalog_Header__c = catHeader.Id, NE__Enable_for_oppty__c=false,  NE__Version__c=1.0, NE__Visible_web__c=false);
        insert catalog;
        
        NE__Catalog_Category__c catCategory = new NE__Catalog_Category__c(Name='TGSOL Catalog', CurrencyIsoCode='ARS', NE__CatalogId__c=catalog.Id);
        insert catCategory;
        
        NE__Commercial_Model__c commModel = new NE__Commercial_Model__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Catalog_Header__c=catHeader.Id, NE__Offline__c=false, NE__ProgramName__c='CatalogTest', NE__Status__c='Pending');
        insert commModel;
        
        NE__Product__c prod = new NE__Product__c(Name='WIFI', CurrencyIsoCode='ARS');
        insert prod;
        
        NE__Catalog_Item__c catItem = new NE__Catalog_Item__c(NE__ProductId__c=prod.Id, NE__Catalog_Id__c=catalog.Id, NE__Catalog_Category_Name__c=catCategory.Id, NE__Start_Date__c=system.today()-1, NE__BaseRecurringCharge__c=10.00, NE__Base_OneTime_Fee__c=100.00, NE__Min_Qty__c=0, NE__Max_Qty__c=99);
        insert catItem;
        
        RecordType RecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name LIKE '%Holding' LIMIT 1];            
        Account acc = new Account(Name = 'TestAccount', RecordTypeId = RecId.Id);
        insert acc;
        
        
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c='TGS';
        insert u;
        System.runAs(u) 
        {            
            NE__Asset__c asset = new NE__Asset__c(NE__AccountId__c=acc.Id, NE__BillAccId__c=acc.Id, NE__ServAccId__c=acc.Id);
            insert asset;
            asset = [SELECT Id, NE__AccountId__c, NE__BillAccId__c, NE__ServAccId__c FROM NE__Asset__c WHERE Id=:asset.Id];
            
            NE__Order__c ord = new NE__Order__c(NE__Asset__c=asset.Id, NE__AccountId__c=acc.Id, NE__Configuration_Type__c='New', NE__Type__c='InOrder');
            insert ord;
            ord = [SELECT Id, NE__AccountId__c, NE__Configuration_Type__c, NE__Type__c FROM NE__Order__c WHERE Id=:ord.Id];
            
            NE__OrderItem__c ordIt = new NE__OrderItem__c(NE__OrderId__c=ord.Id, NE__CatalogItem__c=catItem.Id, NE__ProdId__c=prod.Id, NE__Qty__c=1);
            insert ordIt;

            NE__Order_Item_Attribute__c ordAttribute = new NE__Order_Item_Attribute__c(NE__Order_Item__c=ordIt.Id, name = 'test', NE__Value__c = 'test');
            insert ordAttribute;
            
            ApexPages.StandardController contr = new ApexPages.StandardController(ord);
            NENewOrderExtension newordpage = new NENewOrderExtension(contr);
            newordpage.next();
            
            //Contact CaseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
            //insert CaseContact;
            
            Case newCase = [SELECT Id, Asset__c, Order__c, Status, Subject, Type, TGS_Service__c, ContactId, AccountId 
                            FROM Case 
                            WHERE Order__c =: ord.Id LIMIT 1];
    
            Test.startTest();       
            String assetId  =   Order2AssetWithQueuable.callOrder2AssetMethod(ord.Id, newCase.id);            
            //System.enqueueJob(new QueuableSetAssetAdditionalFields(assetId,newCase.id, ord.id));
            Order2AssetWithQueuable.QueuableSetAssetAdditionalFields(assetId,newCase.id, ord.id);//Álvaro López 13/11/2017 - Fix asset error in modifications INC000000123226
            Test.stopTest();         
        }  
    } 

    static testMethod void testQueuableRODOrderItem()
    {
        NE__Catalog_Header__c catHeader = new NE__Catalog_Header__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Active__c=true, NE__Enable_Global_Cart__c=false, NE__Name__c='CatalogTest', NE__Start_Date__c=system.today()-1);
        insert catHeader;
        
        NE__Catalog__c catalog = new NE__Catalog__c(CurrencyIsoCode='ARS', Name='TGSOL Catalog', NE__Active__c=true, NE__Catalog_Header__c = catHeader.Id, NE__Enable_for_oppty__c=false,  NE__Version__c=1.0, NE__Visible_web__c=false);
        insert catalog;
        
        NE__Catalog_Category__c catCategory = new NE__Catalog_Category__c(Name='TGSOL Catalog', CurrencyIsoCode='ARS', NE__CatalogId__c=catalog.Id);
        insert catCategory;
        
        NE__Commercial_Model__c commModel = new NE__Commercial_Model__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Catalog_Header__c=catHeader.Id, NE__Offline__c=false, NE__ProgramName__c='CatalogTest', NE__Status__c='Pending');
        insert commModel;
        
        NE__Product__c prod = new NE__Product__c(Name='WIFI', CurrencyIsoCode='ARS');
        insert prod;
        
        NE__Catalog_Item__c catItem = new NE__Catalog_Item__c(NE__ProductId__c=prod.Id, NE__Catalog_Id__c=catalog.Id, NE__Catalog_Category_Name__c=catCategory.Id, NE__Start_Date__c=system.today()-1, NE__BaseRecurringCharge__c=10.00, NE__Base_OneTime_Fee__c=100.00, NE__Min_Qty__c=0, NE__Max_Qty__c=99);
        insert catItem;
        
        RecordType RecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name LIKE '%Holding' LIMIT 1];            
        Account acc = new Account(Name = 'TestAccount', RecordTypeId = RecId.Id);
        insert acc;
        
        
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c='TGS';
        insert u;
        System.runAs(u) 
        {            
            NE__Asset__c asset = new NE__Asset__c(NE__AccountId__c=acc.Id, NE__BillAccId__c=acc.Id, NE__ServAccId__c=acc.Id);
            insert asset;
            asset = [SELECT Id, NE__AccountId__c, NE__BillAccId__c, NE__ServAccId__c FROM NE__Asset__c WHERE Id=:asset.Id];
            
            NE__Order__c ord = new NE__Order__c(NE__Asset__c=asset.Id, NE__AccountId__c=acc.Id, NE__Configuration_Type__c='New', NE__Type__c='InOrder');
            insert ord;
            ord = [SELECT Id, NE__AccountId__c, NE__Configuration_Type__c, NE__Type__c FROM NE__Order__c WHERE Id=:ord.Id];
            
            NE__OrderItem__c ordIt = new NE__OrderItem__c(NE__OrderId__c=ord.Id, NE__CatalogItem__c=catItem.Id, NE__ProdId__c=prod.Id, NE__Qty__c=1);
            insert ordIt;

            NE__Order_Item_Attribute__c ordAttribute = new NE__Order_Item_Attribute__c(NE__Order_Item__c=ordIt.Id, name = 'test', NE__Value__c = 'test');
            insert ordAttribute;
            
            ApexPages.StandardController contr = new ApexPages.StandardController(ord);
            NENewOrderExtension newordpage = new NENewOrderExtension(contr);
            newordpage.next();
            
            //Contact CaseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
            //insert CaseContact;
            
            Case newCase = [SELECT Id, Asset__c, Order__c, Status, Subject, Type, TGS_Service__c, ContactId, AccountId 
                            FROM Case 
                            WHERE Order__c =: ord.Id LIMIT 1];

            Map <String,String> map_conf_cas = new Map <String, String>();
            map_conf_cas.put(ord.Id, newCase.id);
    
            Test.startTest();
            ID jobID = System.enqueueJob(new Order2AssetWithQueuable.QueableHelper(map_conf_cas)); //Álvaro López 13/11/2017 - Fix asset error in modifications INC000000123226        
            /*String assetId  =   Order2AssetWithQueuable.callOrder2AssetMethod(ord.Id, newCase.id);   
            
            list<NE__OrderItem__c> confItems = [SELECT Id FROM NE__OrderItem__c WHERE NE__OrderId__c =: assetId];
    
            TGS_CallRodWs.inFutureContext   = false;
            TGS_CallRodWs.inFutureContextCI = false;*/
             /* Send Integration with in ROD in modification and termination */
            /*Set<Id> confitemsIds = new Set<Id>();
            for(NE__OrderItem__c ci:confItems)
            {           
                confitemsIds.add(ci.Id);
            }            
                     
            System.enqueueJob(new QueuableRODOrderItem(confitemsIds));*/
            Test.stopTest();         
        }  
    } 


}