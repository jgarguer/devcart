/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis 
Company:       Everis
Description:   Test Methods executed to test CWP_PortalesCtrl.cls 
Test Class:    

History:

<Date>                  <Author>                <Change Description>
11/04/2016              Everis                   Initial Version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class CWP_PortalesCtrl_TEST {
    
    
    @testSetup 
    private static void dataModelSetup() {  
        
        //USER
        UserRole rolUsuario;
        User unUsuario;
        User otroUsuario;
        Profile profileTGS;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            profileTGS = CWP_TestDataFactory.getProfile('TGS System Administrator');
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, profileTGS.Id);
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
            //ACCOUNT
            map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
        
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        Contact contactTest1 = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest1');
        
        contactTest.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest;
        contactTest1.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest1;
        
        User usuario;
        User usuarioNoTGS;
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');       
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        System.runAs(unUsuario){        
            Profile perfil2 = CWP_TestDataFactory.getProfile('BI_Customer Communities');       
            usuarioNoTGS = CWP_TestDataFactory.createUser('nombre3', null, perfil2.id);
            usuarioNoTGS.contactId = contactTest1.id;
            insert usuarioNoTGS;
        }

        BI_Portal_local__c portal1 = new BI_Portal_local__c();
        portal1.Name='portal1';
        portal1.BI_Country__c=usuario.Country;
        portal1.BI_Imagen_del_portal__c = '<img alt="User-added image" src="https://telefonicab2b--CRQDEV--c.cs80.content.force.com/servlet/rtaImage?eid=a3M250000000YCU&amp;feoid=00Nw0000008Xf7n&amp;refid=0EM250000004Imj" style="height: 184px; width: 500px;"></img>';
        portal1.BI_Activo__c = true;
        portal1.CWP_TGS_Users__c = true;
        insert portal1;
        
         BI_Portal_local__c portal2 = new BI_Portal_local__c();
        portal2.Name='portal2';
        portal2.BI_Country__c=usuario.Country;
        portal2.BI_Imagen_del_portal__c = '"imagenes" "de" "los" "portales"';
        insert portal2;
        
    }
    
    @isTest
    private static void testcheckPermissions() {     
        CWP_PortalesCtrl constructor = new CWP_PortalesCtrl();
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];
         BI_TestUtils.throw_exception=false;  
        System.runAs(usuario){                          
            Test.startTest();                      
            constructor.checkPermissions();   
            Test.stopTest();
        }       
        
    }
    
    @isTest
    private static void testloadInfo() { 
        CWP_PortalesCtrl constructor1 = new CWP_PortalesCtrl(); 
        /*BI_Portal_local__c portal1 = [SELECT id FROM BI_Portal_local__c WHERE Name =: 'portal1'];
        BI_Portal_local__c portal2 = [SELECT id FROM BI_Portal_local__c WHERE Name =: 'portal2'];
       
        list<BI_Portal_local__c> lista = new list<BI_Portal_local__c>();
        lista.add(portal1);
        lista.add(portal2);*/
        //constructor1.GetPortals=lista;
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];
         BI_TestUtils.throw_exception=false;  
        System.runAs(usuario){                          
            Test.startTest();  
                    
            constructor1.loadInfo();   
            Test.stopTest();
        }       
        
    }
    
    @isTest
    private static void testloadInfo2() {     
        CWP_PortalesCtrl constructor3 = new CWP_PortalesCtrl();
        constructor3.p=[SELECT id, Name FROM Profile WHERE Name =: 'BI_Customer Communities'];    
        List <String> Image = new List <String>();
        Image.add('portales');
        constructor3.ImageToshow = Image;
        User usuario2 = [SELECT id FROM User WHERE FirstName =: 'nombre3'];
 BI_TestUtils.throw_exception=false;  
        System.runAs(usuario2){                          
            Test.startTest();                      
            constructor3.loadInfo();   
            Test.stopTest();
        }       
        
    }
    
    @isTest
    private static void testgetObj() {     
        CWP_PortalesCtrl constructor2 = new CWP_PortalesCtrl(); 
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];
         BI_TestUtils.throw_exception=false;  
        System.runAs(usuario){                          
            Test.startTest();                      
            constructor2.getObj();   
            Test.stopTest();
        }       
        
    }
    
    @isTest
    private static void testgetObj2() {     
        CWP_PortalesCtrl constructor4 = new CWP_PortalesCtrl(); 
        constructor4.p=[SELECT id, Name FROM Profile WHERE Name =: 'BI_Customer Communities'];
        User usuario2 = [SELECT id FROM User WHERE FirstName =: 'nombre3'];
        
         BI_TestUtils.throw_exception=false;  
        System.runAs(usuario2){                          
            Test.startTest();                      
            constructor4.getObj();   
            Test.stopTest();
        }       
        
    }
    
    @isTest
    private static void testgetPortals() {     
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];
         BI_TestUtils.throw_exception=false;  
        System.runAs(usuario){                          
            Test.startTest();   
            CWP_PortalesCtrl.getPortals();   
            Test.stopTest();
        }       
        
    }
    
    @isTest
    private static void testgetPortals2() {     
        
        User usuario2 = [SELECT id FROM User WHERE FirstName =: 'nombre3'];
         BI_TestUtils.throw_exception=false;  
        System.runAs(usuario2){                          
            Test.startTest();   
            CWP_PortalesCtrl.getPortals();   
            Test.stopTest();
        }       
        
    }
    
    
}