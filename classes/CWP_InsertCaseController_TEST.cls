/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis
Company:       Everis
Description:   Controller class for testing CWP_IncModCreationController controller
Test Class:    CWP_IncModCreationController

History:

<Date>                  <Author>                <Change Description>
14/03/2017              Everis                    Initial Version
23/03/2017              Alberto Pina            Coverage 88%    
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class CWP_InsertCaseController_TEST{  
    @isTest
    private static void insertCaseTest() {  
        Test.startTest();
        PageReference pageRef = Page.CWP_InsertCase;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('why','else');
        String id = ApexPages.currentPage().getParameters().get('why');
        
        CWP_InsertCaseController pru= new CWP_InsertCaseController();
        
        Case caso = new Case(); 
        caso.Status = 'Assigned';
        RecordType record = [SELECT Id,Name FROM RecordType WHERE Name = 'Order Management Case'];
        caso.RecordTypeId=record.Id;
        insert caso;
        
        TGS_Error_Integrations__c errorIntegration = new TGS_Error_Integrations__c();
        insert errorIntegration;
        Attachment adjunto = new Attachment(); 
        adjunto.ParentId =errorIntegration.id;
        adjunto.Name='RequestInfo.json';
        string body1='prueba';
        Blob b = Blob.valueOf(body1);
        adjunto.Body=b;
        adjunto.Description='Description';
        insert adjunto;
        List<Attachment> listAtt = new List<Attachment>();
        listAtt.add(adjunto); 
        
        Cache.Session.put('local.CWPexcelExport.caseToInsert', caso , 3000, Cache.Visibility.ALL, true); 
        Cache.Session.put('local.CWPexcelExport.newComment', 'NewComent Case' , 3000, Cache.Visibility.ALL, true); 
        Cache.Session.put('local.CWPexcelExport.attList',listAtt , 3000, Cache.Visibility.ALL, true); 
        
        ApexPages.currentPage().getParameters().put('cancelticketid',caso.Id);
        
        pru.insertCase();
        
        Test.stopTest();
    }
    @isTest
    private static void ConstructorTest() {  
        Test.startTest();
        PageReference pageRef = Page.CWP_InsertCase;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('why','attachment');
        String id = ApexPages.currentPage().getParameters().get('why');
        
        CWP_InsertCaseController pru= new CWP_InsertCaseController();
        
        //Crear variables en los get
        Case caso = new Case(); 
        caso.Status = 'Assigned';
        caso.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Case' AND DeveloperName = 'TGS_Query' LIMIT 1].Id;
        insert caso;
        TGS_Error_Integrations__c errorIntegration = new TGS_Error_Integrations__c();
        insert errorIntegration;
        Attachment adjunto = new Attachment(); 
        adjunto.ParentId =errorIntegration.id;
        adjunto.Name='RequestInfo.json';
        string body1='prueba';
        Blob b = Blob.valueOf(body1);
        adjunto.Body=b;
        adjunto.Description='Description';
        insert adjunto;
        List<Attachment> listAtt = new List<Attachment>();
        listAtt.add(adjunto);
        
        //Cache.Session.put('local.CWPexcelExport.caseId', caso.id , 3000, Cache.Visibility.ALL, true); 
        Cache.Session.put('local.CWPexcelExport.newComment', 'NewComent Case' , 3000, Cache.Visibility.ALL, true); 
        Cache.Session.put('local.CWPexcelExport.attList',listAtt , 3000, Cache.Visibility.ALL, true); 
        Cache.Session.put('local.CWPexcelExport.caseToInsert', caso , 3000, Cache.Visibility.ALL, true); 
        ApexPages.currentPage().getParameters().put('cancelticketid',caso.Id);
        ApexPages.currentPage().getParameters().put('statusticketid',caso.Status);
        pru.insertCase();
        
        Set<Id> sIds= new Set<Id>();
        sIds.add(caso.Id);
        CWP_CallRoDWSFromCWP.invokeRoDWSFromCWP(sIds);
        CWP_CallRoDWSFromCWP.invokeWebServiceRODFromCWP(sIds,new Map<Id,String>(),new Map<Id,String>(),false);
        
        Test.stopTest();
    }
    @isTest
    private static void crearWorkinfoTest_A() {      
        Test.startTest();
        PageReference pageRef = Page.CWP_InsertCase;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('why','comment');
        String id = ApexPages.currentPage().getParameters().get('why');
        
        CWP_InsertCaseController pru= new CWP_InsertCaseController();
        
        Contact contact = new Contact(TGS_Enviar_Email_Query__c = false, FirstName = 'Test', LastName = 'Test');
        insert contact;
        
        //Crear variables en los get
        Case caso = new Case(); 
        caso.Status = 'Assigned';
        caso.ContactId = contact.Id;
        caso.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Case' AND DeveloperName = 'TGS_Query' LIMIT 1].Id;
        insert caso;
        ApexPages.currentPage().getParameters().put('cancelticketid',caso.Id);
        TGS_Error_Integrations__c errorIntegration = new TGS_Error_Integrations__c();
        insert errorIntegration;
        Attachment adjunto = new Attachment(); 
        adjunto.Name='apply1293741960720.png';
        adjunto.Description='Description';
        adjunto.ContentType ='text/html';
        //insert adjunto;
        List<Attachment> listAtt = new List<Attachment>();
        listAtt.add(adjunto);
        String attachmentListB64 = JSON.serialize(listAtt);
        Cache.Session.put('local.CWPexcelExport.caseId', caso.id , 3000, Cache.Visibility.ALL, true); 
        Cache.Session.put('local.CWPexcelExport.newComment', 'NewComent Case' , 3000, Cache.Visibility.ALL, true); 
        Cache.Session.put('local.CWPexcelExport.attList',listAtt , 3000, Cache.Visibility.ALL, true); 
        Cache.Session.put('local.CWPexcelExport.caseToInsert', caso , 3000, Cache.Visibility.ALL, true); 
        System.debug('>>>'+(String)JSON.serialize(listAtt));
        ApexPages.currentPage().getParameters().put('attachmentListB64',JSON.serialize(listAtt));
        pru.insertCase();
        
        System.assert([SELECT Count() FROM Case] == 1);
        
        Map<String,List<String>> cachedStringsMap = new Map<String,List<String>>();
        List<String> lista = new List<String>();
        lista.add('element');
        cachedStringsMap.put('RequestInfo', lista);
        Cache.Session.put('local.CWPexcelExport.adjuntosMap', cachedStringsMap , 3000, Cache.Visibility.ALL, true);
        
        pru.crearWorkinfo(caso.Id, listAtt);
        
        Test.stopTest();    
    }
    
    @isTest
    private static void crearWorkinfoTest_B() {      
        Test.startTest();
        PageReference pageRef = Page.CWP_InsertCase;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('why','attachment');
        String id = ApexPages.currentPage().getParameters().get('why');
        
        CWP_InsertCaseController pru= new CWP_InsertCaseController();
        
        Contact contact = new Contact(TGS_Enviar_Email_Query__c = false, FirstName = 'Test', LastName = 'Test');
        insert contact;
        
        //Crear variables en los get
        Case caso = new Case(); 
        caso.Status = 'Assigned';
        caso.ContactId = contact.Id;
        caso.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Case' AND DeveloperName = 'TGS_Query' LIMIT 1].Id;
        insert caso;
        ApexPages.currentPage().getParameters().put('cancelticketid',caso.Id);
        TGS_Error_Integrations__c errorIntegration = new TGS_Error_Integrations__c();
        insert errorIntegration;
        Attachment adjunto = new Attachment(); 
        adjunto.Name='apply1293741960720.png';
        adjunto.Description='Description';
        adjunto.ContentType ='text/html';
        //insert adjunto;
        List<Attachment> listAtt = new List<Attachment>();
        listAtt.add(adjunto);
        String attachmentListB64 = JSON.serialize(listAtt);
        Cache.Session.put('local.CWPexcelExport.caseId', caso.id , 3000, Cache.Visibility.ALL, true); 
        Cache.Session.put('local.CWPexcelExport.newComment', 'NewComent Case' , 3000, Cache.Visibility.ALL, true); 
        Cache.Session.put('local.CWPexcelExport.attList',listAtt , 3000, Cache.Visibility.ALL, true); 
        Cache.Session.put('local.CWPexcelExport.caseToInsert', caso , 3000, Cache.Visibility.ALL, true); 
        System.debug('>>>'+(String)JSON.serialize(listAtt));
        ApexPages.currentPage().getParameters().put('attachmentListB64',JSON.serialize(listAtt));
        pru.insertCase();
        
        System.assert([SELECT Count() FROM Case] == 1);
        
        Map<String,List<String>> cachedStringsMap = new Map<String,List<String>>();
        List<String> lista = new List<String>();
        lista.add('element');
        cachedStringsMap.put('RequestInfo', lista);
        Cache.Session.put('local.CWPexcelExport.adjuntosMap', cachedStringsMap , 3000, Cache.Visibility.ALL, true);
        
        pru.crearWorkinfo(caso.Id, listAtt);
        pru.allToRoD = true;
        Set<Id> idSet = new Set<Id>();
        idSet.add(caso.Id);
        pru.casesIdSet = idSet;
        Set<Id> wiIdSet = new Set<Id>();
        Map<Id, TGS_Work_Info__c> mapWi = new Map<Id, TGS_Work_Info__c>([SELECT Id FROM TGS_Work_Info__c WHERE TGS_Case__c IN: idSet]);
        wiIdSet.addAll(mapWi.keySet());
        pru.workInfosIdSet = wiIdSet;
        pru.integraRoD();
        
        
        
        caso.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Case' AND DeveloperName = 'TGS_Change' LIMIT 1].Id;
        update caso;
        Id parent;
        for(Id idWI:wiIdSet){
            parent = idWI;
        }
        adjunto.ParentId = parent;
        adjunto.Body = Blob.valueOf('Unit Test Attachment Body');
        upsert adjunto;
        pru.integraRoD();
        
        CWP_InsertCaseController.customAtt p=new CWP_InsertCaseController.customAtt();
        p.name ='test\\.test';
        p.body='';
        p.description='';
        p.contentType='';
        pru.currentAtt = p;
        
        pru.addAttachment();
        
        Set<Id> sIds= new Set<Id>();
        sIds.add(caso.Id);
        
        CWP_CallRoDWSFromCWP.invokeRoDWSFromCWP(sIds);
        CWP_CallRoDWSFromCWP.invokeWebServiceRODFromCWP(sIds,new Map<Id,String>(),new Map<Id,String>(),false);
        
        CWP_CallRoDWSFromCWP.invokeWebServiceRODWorkInfoFromCWP(wiIdSet,false);
        Test.stopTest();    
    }
    
    @isTest
    private static void crearWorkinfoTest_C() {
        Test.startTest();
        PageReference pageRef = Page.CWP_InsertCase;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('why','test');
        
        CWP_InsertCaseController pru = new CWP_InsertCaseController();
        
        /* Insert Contact */
        Contact contact = new Contact(TGS_Enviar_Email_Query__c = false, FirstName = 'Test', LastName = 'Test');
        insert contact;
        
        /* Insert Case */
        Case caso = new Case(); 
        caso.Status = 'Assigned';
        caso.ContactId = contact.Id;
        caso.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Case' AND DeveloperName = 'TGS_Query' LIMIT 1].Id;
        ApexPages.currentPage().getParameters().put('statusticketid',JSON.serialize(caso));
        
        TGS_Error_Integrations__c errorIntegration = new TGS_Error_Integrations__c();
        insert errorIntegration;
        Attachment adjunto = new Attachment(); 
        adjunto.Name='apply1293741960720.png';
        adjunto.Description='Description';
        adjunto.ContentType ='text/html';
        
        /* Add Attachment */
        List<Attachment> listAtt = new List<Attachment>();
        listAtt.add(adjunto);
        String attachmentListB64 = JSON.serialize(listAtt);
        Cache.Session.put('local.CWPexcelExport.newComment', 'NewComent Case' , 3000, Cache.Visibility.ALL, true); 
        Cache.Session.put('local.CWPexcelExport.attList',listAtt , 3000, Cache.Visibility.ALL, true); 
        
        ApexPages.currentPage().getParameters().put('attachmentListB64',JSON.serialize(listAtt));
        
        pru.insertCase();
        System.assert([SELECT Count() FROM Case] == 1);
        
        Set<Id> sIds= new Set<Id>();
        sIds.add(caso.Id);
        CWP_CallRoDWSFromCWP.invokeRoDWSFromCWP(sIds);
        CWP_CallRoDWSFromCWP.invokeWebServiceRODFromCWP(sIds,new Map<Id,String>(),new Map<Id,String>(),false);
        
        Test.stopTest();
    }
    
    
}