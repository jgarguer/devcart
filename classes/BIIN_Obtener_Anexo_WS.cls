/*------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jose María Martín Castaño
        Company:       Deloitte
        Description:   Clase para la obtención de adjuntos de RoD.
        
        History:
        
        <Date>          <Author>                            <Description>
        10/12/2015      Jose María Martín Castaño           Initial version
        18/05/2016      Julio Laplaza Soria                 Adaptation to UNICA
        22/05/2016      José Luis González Beltrán          Fix obtain attachment Id
------------------------------------------------------------------------------------------------------------------------------------------*/
    
public class BIIN_Obtener_Anexo_WS extends BIIN_UNICA_Base
{
    public String obtenerAnexo(String authorizationToken, Case caso, String adjunto, String worklogId, String posicionWI) 
    {
        String adjId = worklogId + '_' + posicionWI.subString(posicionWI.length() - 2);
        try
        {
            // Get account
            Account account = getAccount(caso.accountId);

            // Prepare URL
            String url = 'callout:BIIN_WS_UNICA/ticketing/v1/tickets/' + caso.BI_Id_del_caso_legado__c + '/attachments/' + adjId;

            // Prepare Request
            HttpRequest req = getRequest(url, 'GET');
            
            // Call and Get Response
            BIIN_UNICA_Pojos.ResponseObject response = getResponse(req, BIIN_UNICA_Pojos.TicketAttachmentInfoType.class);
            BIIN_UNICA_Pojos.TicketAttachmentInfoType attachmentInfoType = (BIIN_UNICA_Pojos.TicketAttachmentInfoType) response.responseObject;

            return BIIN_UNICA_Utils.additionalDataToMap(attachmentInfoType.additionalData).get('content');
        }
        catch(Exception e)
        {
            system.debug ('ERROR: ' + e);
            return null;
        }
    }
}