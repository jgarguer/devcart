/*---------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:			Eduardo Ventura
Company:		Everis España
Description:	RFG-INT-CHI-01: Validación del identificador fiscal.

History:

<Date>                      <Author>                        <Change Description>
10/10/2016                  Eduardo Ventura					Versión Inicial
24/10/2017									   				Ever01:Inclusion de logs
---------------------------------------------------------------------------------------------------------------------------------------------------------------*/

public class FS_CHI_Account_Management {
    /* DECLARACIÓN DE VARIABLES */
    /* Vacío */
    
    public static BI_RestWrapper.CustomersListType Sync(String documentValue){
        System.debug('Nombre de clase: FS_CHI_Account_Management   Nombre de método: Sync   Comienzo del método');
        /* Declaración de variables */
        BI_RestWrapper.CustomersListType customersListWrapper;
        BI_RestWrapper.CustomersListType customerList;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        
        /*REQUEST CONFIGURATION*/
        FS_CHI_Integraciones__c config_integracion = FS_CHI_Integraciones__c.getInstance();        
        request.setEndpoint('callout:FS_CHI_DataPower' + '/customerinfo/v1/customers?nationalID='+EncodingUtil.urlEncode(documentValue,'UTF-8') + '&apikey='+config_integracion.FS_CHI_ApiKey__c);
        request.setMethod('GET');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Authorization', config_integracion.FS_CHI_AuthorizationID__c);
        request.setHeader('Content-Type', 'application/json');
        request.setTimeout(25000);
        
        if(Test.isRunningTest()){response = FS_CHI_Account_Management_Mock.respond(request);}
        else response = http.send(request);
        System.debug('DEBUG: FS_CHI_Account_Management - callout response: '+response.getBody());
        /* Process */
        if(response.getStatusCode() == 200 && response.getBody()!= null) {
            customersListWrapper = (BI_RestWrapper.CustomersListType) JSON.deserialize(response.getBody(), BI_RestWrapper.CustomersListType.class);  
            System.debug('@@customersListWrapper: ' + customersListWrapper);
            customerList=customersListWrapper;  //.CustomersListType
            System.debug('@@customerList: ' + customerList);
            customerList.totalResults=customersListWrapper.totalResults;            
        }else{
            customerList=new BI_RestWrapper.CustomersListType();
            customerList.totalResults=9;
            throw new FS_CHI_Contact_Record_Controller.FS_CHI_IntegrationException('Error:'+response.getStatusCode()+ ' ' + response.getBody());  
        }
        System.debug('Nombre de clase: FS_CHI_Account_Management   Nombre de método: Sync   Finalización del método    Resultado: ' + customerList);
        return customerList;
    }
}