@isTest(SeeAllData=true)
private class TestNECheckOptyFields {
 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        XXXXXXXXXXXXX
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for GenerateOrder

    History: 

    <Date>                  <Author>                <Change Description>
     XXXXXX                  XXXXXX                 Initial Version
    18/02/2017              Guillermo Muñoz         Activate bypass and throw_exception = false in testOpty to avoid limits
    27/09/2017              Jaime Regidor           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c

--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void testOpty(){
       // test.startTest();

        BI_TestUtils.throw_exception = false;

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),true, true, false, false);
        NETriggerHelper.setTriggerFired('NESetDefaultOIFieldsTest');
        NETriggerHelper.setTriggerFired('NECheckOrderItems');

        Account acc = new Account();
        acc.Name = 'Testacc';
        acc.BI_Segment__c = 'test'; //28/09/2017
        acc.BI_Subsegment_Regional__c = 'test'; //28/09/2017
        acc.BI_Territory__c = 'test'; //28/09/2017
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'TestOpp';
        opp.AccountId = acc.Id;
        opp.CloseDate = Date.today();
        opp.StageName = 'F5 - Solution Definition';
        opp.BI_Ciclo_ventas__c = 'Rápido';   
        opp.BI_Analisis_de_riesgo__c = 'Riesgo aprobado';    
        opp.BI_Duracion_del_contrato_Meses__c = 2;
        opp.BI_Oferta_economica__c = 'Descuento aprobado' ;
        opp.BI_Oferta_tecnica__c = 'Oferta técnica aprobada';
        opp.BI_Comite_de_negocios__c= 'Aprobado Comité';    
        opp.BI_Ultrarapida__c = true; 
        opp.Amount = 250;
        opp.BI_Productos_numero__c = 1;
        insert opp;
        
        NE__Order__c ord = new NE__Order__c();
        ord.NE__OptyId__c = opp.Id;
        ord.NE__OrderStatus__c = 'Active';
        RecordType recType = [Select Id From RecordType  Where SobjectType = 'NE__Order__c' and DeveloperName = 'Opty'];
        ord.RecordTypeId = recType.Id;
        insert ord;

        Product2 p = new product2(name='x');
                                  
        insert p;

        Pricebook2 standardPB = [select id from Pricebook2 where isStandard = true];
        
        PricebookEntry pbe = new PricebookEntry(pricebook2id = standardPB.id, 
                                                product2id = p.id, 
                                                unitprice=1.0, 
                                                isActive=true);
                                                
        insert pbe;
        
        List<OpportunityLineItem> lst_olis = new List<OpportunityLineItem>();
        
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = opp.Id,
                                                          Quantity = 3,
                                                          TotalPrice = 2,
                                                          PricebookEntryId = pbe.Id);
                                        
        lst_olis.add(oli);
       
        
        insert lst_olis;    
        
        BI_MigrationHelper.disableBypass(mapa);
        NETriggerHelper.setTriggerFiredTest('NESetDefaultOIFieldsTest', false);
        NETriggerHelper.setTriggerFiredTest('NECheckOrderItems', false);
        
        

        NE__OrderItem__c oi = new NE__OrderItem__c();
        oi.NE__OrderId__c = ord.id;
        oi.NE__Qty__c = 1;
        insert oi;

        Test.startTest();
        
        opp.BI_Ciclo_ventas__c = 'Completo';
        opp.stageName = 'F4 - Offer Development';
        opp.BI_Duracion_del_contrato_Meses__c = 3;  
        update opp;
       
        Test.stopTest();                      
        
    }
    
    
  static testMethod void testOpty2(){
		BI_TestUtils.throw_exception = false;    
        Account acc = new Account();
        acc.Name = 'Testacc';
        acc.BI_Segment__c = 'test'; //28/09/2017
        acc.BI_Subsegment_Regional__c = 'test'; //28/09/2017
        acc.BI_Territory__c = 'test'; //28/09/2017
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'TestOpp';
        opp.AccountId = acc.Id;
        opp.CloseDate = Date.today();
        opp.StageName = 'F2 - Negotiation';
        opp.BI_Ciclo_ventas__c = 'Rápido';   
        opp.BI_Analisis_de_riesgo__c = 'Riesgo aprobado';    
        opp.BI_Duracion_del_contrato_Meses__c = 2;
        opp.BI_Oferta_economica__c = 'Descuento aprobado' ;
        opp.BI_Oferta_tecnica__c = 'Oferta técnica aprobada';
        opp.BI_Comite_de_negocios__c= 'Aprobado Comité';    
        opp.BI_Ultrarapida__c = true; 
        insert opp;
        
        NE__Order__c ord = new NE__Order__c();
        ord.NE__OptyId__c = opp.Id;
        ord.NE__OrderStatus__c = 'Active';
        RecordType recType = [Select Id From RecordType  Where SobjectType = 'NE__Order__c' and DeveloperName = 'Opty'];
        ord.RecordTypeId = recType.Id;
        insert ord;
        
        opp.StageName = 'F1 - Closed Won';
        update opp;
        
    
  }  
  
 
        
    
          
    
 
  
}