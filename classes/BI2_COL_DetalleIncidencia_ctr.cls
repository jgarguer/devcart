/**
* Avanxo Colombia
* @author           Sebastián Ortiz href=<seortiz@avanxo.com>
* Proyect:          Teléfonica BI2 
* Description:      Clase controladora encargada de obtener los campos de casos para la página "BI2_COL_DetalleIncidencia_pag"
* Changes (Version)
* -------------------------------------
*            No.    Fecha           Autor                           Descripción        
*            ----   ----------      ---------------------------     -------------    
* @version   1.0    03-02-2017    	Sebastián Ortiz Niño     		Class Created 
*********************************************************************************************************************************/

public with sharing class BI2_COL_DetalleIncidencia_ctr extends BIIN_BaseController
{
    public BIIN_Obtener_Ticket_WS.ObtenerTicketRespuesta respuesta {get; set;} 
    public List<BIIN_Obtener_Ticket_WS.Worklog> lworklog{get; set;} 
    public List<BIIN_Obtener_Ticket_WS.SLA> lsla{get; set;} 
    public case caso {get; set;}
    public case q {get; set;}
    public String  casoNota{get; set;}   
    public boolean displayPopup {get; set;} 
    public integer ContAdj {get; set;}
    public boolean YaAdjuntado {get; set;}
    public boolean MasDeTres {get; set;}
    public List <String> ListaNombres {get; set;} 
    public boolean displayPopup2 {get; set;}
    public boolean displayPopup3{get; set;}
    public List<Case> lcases {get;set;}
    //public String accountName{get; set;}
    //public String accountNIT{get; set;}
    public String nombreContact{get; set;} 
    public String nombreOwner{get; set;}
    public Boolean displayPopUp4 {get; set;} 
    public Boolean displayPopUp5 {get; set;} 
    public Boolean displayPopUp6 {get; set;} 
    public Boolean displayPopUp7 {get; set;} 
    public Boolean displayPopUp8 {get; set;} 
    public Boolean displayPopUp9 {get; set;}
    public String rutaBmc {get; set;}    
    public String rutaBase {get; set;}
    public boolean ModificarConfidencial {get; set;}
    public string GuardarCasoNota{get;set;}
    public case MostrarNumeroCasoPadre{get;set;}
    public String worklogId{get;set;}
    public String posicionWI{get;set;}
    public String attachName {get; set;}
    public String IdAdjunto {get; set;}
    public Attachment attach {get;set;}//BORRAR???
    public String telefonoFijo {get;set;}
    public String telefonoMovil {get;set;}
    
    public Account account {get;set;}
    
    public Transient  Blob adjunto1 {get; set;}
    public String nombre1 {get; set;}
    public Transient  Blob adjunto2 {get; set;}
    public String nombre2 {get; set;}
    public Transient  Blob adjunto3 {get; set;}
    public String nombre3 {get; set;}
    public List <Attachment> ListaAdjuntos2; 
    BIIN_Crear_WorkInfo_WS.CrearWISalida respuestaWI = new BIIN_Crear_WorkInfo_WS.CrearWISalida();
    public Boolean exito=false; 
    public String caseId;
    public Boolean privado {get; set;}

    ApexPages.standardController controller = null;
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Sebastián Ortiz
        Company:       Avanxo
        Description:   Page reference for cancel button
        
        In:                         None
        Out:                        PageReference
        
        <Date>            <Author>                          <Description>
        07/02/2017       Sebastián Ortiz                    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference doCancel()
    {
        return controller.cancel();
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Sebastián Ortiz
        Company:       Avanxo
        Description:   Class construct for variables initialization.
        
        In:                         Standard Controller (Case)
        Out:                        Void
        
        <Date>            <Author>                          <Description>
        07/02/2017       Sebastián Ortiz                    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI2_COL_DetalleIncidencia_ctr(ApexPages.StandardController controller) 
    {
        this.controller = controller;
        try
        {
            GuardarCasoNota = '';
            ModificarConfidencial = false;
            respuesta  = new BIIN_Obtener_Ticket_WS.ObtenerTicketRespuesta();
            lworklog = new List<BIIN_Obtener_Ticket_WS.Worklog>();
            lsla = new List<BIIN_Obtener_Ticket_WS.SLA>();
            lcases = new List<Case>();
            casoNota = '';
            caso = new case();
            q = new case();
            MostrarNumeroCasoPadre = new case();
            attach = new Attachment();
            ContAdj = 0;
            YaAdjuntado = false;
            MasDeTres = false;       
            ListaAdjuntos2  = new List <Attachment> ();    
            ListaNombres  = new List <String> ();
            displayPopUp9 = false;
            displayPopUp8 = false;
            displayPopUp7 = false;
            displayPopUp6 = false;
            displayPopUp5 = false;
            displayPopUp4 = false;
            NamedCredential nc = [SELECT endpoint FROM NamedCredential WHERE developerName = 'BIIN_RoD' LIMIT 1];
            rutaBmc = nc.endpoint;
            
            caseId = System.currentPagereference().getParameters().get('Idcaso');  
            system.debug('idcaso--->' + caseId );
            // ListaAdjuntos = [SELECT Id, Name FROM Attachment WHERE ParentId  =: caseId];

        }
        catch(Exception ex)
        {
            displayPopupGeneric = true;
            errorMessageGeneric = ex.getMessage();
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Sebastián Ortiz
        Company:       Avanxo
        Description:   public method for calling the remedy web service upon loading Visualforce
        
        In:                         None
        Out:                        Void
        
        <Date>            <Author>                          <Description>
        07/02/2017       Sebastián Ortiz                    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadData()
    {
        BIIN_Obtener_Token_BAO ot = new BIIN_Obtener_Token_BAO();
        BIIN_Obtener_Ticket_WS otc = new BIIN_Obtener_Ticket_WS();
        String authorizationToken = ''; // ot.obtencionToken();
        respuesta = otc.detalleTicket(authorizationToken, caseId);

        if(respuesta!=null)
        {
            caso = respuesta.c;
            q = [SELECT toLabel(Type), BI_Product_Service__c, BI_Line_of_Business__c, ParentId, BI_Confidencial__c, Reason, BI_ECU_Telefono_movil__c, BI_ECU_Telefono_fijo__c, 
                BI2_PER_Descripcion_de_producto__c, RecordType.DeveloperName, ContactId, Contact.Name FROM Case Where Id=:caseId];            
            caso.Type = q.Type; 
            system.debug('Traducir Valor -->' + q.Type);
            caso.BI_Product_Service__c = q.BI_Product_Service__c;
            caso.BI_Line_of_Business__c = q.BI_Line_of_Business__c;
            caso.BI2_PER_Descripcion_de_producto__c = q.BI2_PER_Descripcion_de_producto__c;
            caso.Reason =q.Reason;
            caso.ContactId = q.ContactId;
            caso.Contact = q.Contact;
            telefonoFijo = q.BI_ECU_Telefono_movil__c;
            telefonoMovil = q.BI_ECU_Telefono_fijo__c;
            system.debug('BIIN_Site__c --->' + caso.BIIN_Site__c);
            lworklog = respuesta.lw;
            system.debug('Adjuntos traidos -->' + lworklog);
            lsla = respuesta.lsla;
            system.debug('cuenta: '+caso.AccountId);            
            if(q.ParentId !=null){
                caso.ParentId = q.ParentId; 
                MostrarNumeroCasoPadre =  [SELECT caseNumber FROM case WHERE Id =: caso.ParentId LIMIT 1][0];
                system.debug('ParentId: '+ q.ParentId);
            }
            if(caso.AccountId!=null){
                account = [SELECT Name, BI_No_Identificador_fiscal__c FROM Account WHERE Id =: caso.AccountId LIMIT 1];
                system.debug('cuentaName: '+account.Name);
                system.debug('\n\n BI_No_Identificador_fiscal__c: '+account.Name+'\n\n');
                //accountName=account.Name;
            }
            system.debug('contacto: '+caso.ContactId);
            if(caso.ContactId!=null){
                Contact contact = [SELECT FirstName, LastName  FROM Contact WHERE Id =: caso.ContactId LIMIT 1];
                nombreContact=contact.FirstName+' '+contact.LastName;
            }
            system.debug('propietario: '+caso.OwnerId);
            if(caso.OwnerId != null)
            {
                List<User> owner = [SELECT FirstName, LastName FROM User WHERE Id =: caso.OwnerId LIMIT 1];
                if(!owner.isEmpty() && String.isNotEmpty(owner.get(0).FirstName) && String.isNotEmpty(owner.get(0).LastName))
                {
                    nombreOwner = owner.get(0).FirstName + ' ' + owner.get(0).LastName;
                }
                else
                {
                    List<Group> gr = [SELECT Name FROM Group WHERE Id =: caso.OwnerId LIMIT 1];
                    if(!gr.isEmpty() && String.isNotEmpty(gr.get(0).Name))
                    {
                        nombreOwner = gr.get(0).Name;
                    }
                }
            }

            if(caso.BIIN_Tiempo_Neto_Apertura__c  !=null){
               caso.BIIN_Tiempo_Neto_Apertura__c = caso.BIIN_Tiempo_Neto_Apertura__c.SetSCale(2);
           }
           system.debug('SEGUIMOS');
       }
       system.debug('caso--->' + caso);
   }

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Sebastián Ortiz
        Company:       Avanxo
        Description:   public method for showing details from the attachment asociated in the ticket
        
        In:                         None
        Out:                        Void
        
        <Date>            <Author>                          <Description>
        07/02/2017       Sebastián Ortiz                    Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/


     public void getAttBody()
     {
        String cuerpo;
        String adjId = '';
        
        BIIN_Obtener_Token_BAO ot = new BIIN_Obtener_Token_BAO();
        BIIN_Obtener_Anexo_WS oa = new BIIN_Obtener_Anexo_WS();
        String authorizationToken = ''; // ot.obtencionToken();
        cuerpo = oa.obtenerAnexo(authorizationToken, caso, attachName, worklogId, posicionWI);
        if(String.isNotEmpty(cuerpo))
        {         
            Attachment adjuntoIns = new Attachment();
            
            List<Attachment> adjunto = new List<Attachment>();
            adjunto = [SELECT Name, Body, ParentId, Id FROM Attachment WHERE ParentId=:caso.Id];
            if(adjunto.size() >= 0)
            {
                delete adjunto;
            }

            adjuntoIns.Body     = EncodingUtil.base64Decode(cuerpo);
            adjuntoIns.Name     = attachName;
            adjuntoIns.ParentId = caso.Id;

            if(!Test.isRunningTest())
            {
                insert adjuntoIns;
            }
            
            IdAdjunto = adjuntoIns.Id;
            rutaBase = URL.getSalesforceBaseUrl().toExternalForm();
        }
    } 

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Sebastián Ortiz
        Company:       Avanxo
        Description:   public method for creating the case work info in remedy
        
        In:                         None
        Out:                        Void
        
        <Date>            <Author>                          <Description>
        07/02/2017       Sebastián Ortiz                    Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public void crearWIRoD(){
        system.debug('Es privado: '+privado);
        if(Test.isRunningTest()){
            privado=false;   
        } 
        String viewAccess = (privado ? 'Internal' : 'Public');
        List<BIIN_Crear_WorkInfo_WS.Adjunto> la = new List<BIIN_Crear_WorkInfo_WS.Adjunto>();
        if((caso.Status != 'Cancelado') && (caso.Status != 'Cerrado')){
            if (adjunto1 != null) {
                system.debug('Adjuntamos 1');
                BIIN_Crear_WorkInfo_WS.Adjunto a = new BIIN_Crear_WorkInfo_WS.Adjunto(nombre1,adjunto1);
                la.add(a);
            }
            
            if (adjunto2 != null) {
                system.debug('Adjuntamos 2');
                BIIN_Crear_WorkInfo_WS.Adjunto a = new BIIN_Crear_WorkInfo_WS.Adjunto(nombre2,adjunto2);
                la.add(a);
            }
            
            if (adjunto3 != null) {   
                system.debug('Adjuntamos 3');         
                BIIN_Crear_WorkInfo_WS.Adjunto a = new BIIN_Crear_WorkInfo_WS.Adjunto(nombre3,adjunto3);
                la.add(a);
            } 
            
            if(GuardarCasoNota == '') {
                GuardarCasoNota  = Apexpages.currentPage().getParameters().get('CasoNotaJ');
            }
            BIIN_Obtener_Token_BAO ot=new BIIN_Obtener_Token_BAO();
            BIIN_Crear_WorkInfo_WS cwi=new BIIN_Crear_WorkInfo_WS();
            String authorizationToken = ''; // ot.obtencionToken();
            respuestaWI = cwi.crearWorkInfo(authorizationToken, caso, la, casoNota, viewAccess);
            if(respuestaWI!=null&&respuestaWI.code=='000'){
                displayPopUp = false;
                displayPopUp4 = true;
                system.debug('EXITO: Se ha creado el WI correctamente');
                }else{
                    displayPopUp = false;
                    displayPopUp5 = true;
                    system.debug('ERROR: No se ha creado el WI correctamente');
                }
            //ListaAdjuntos = new List<Attachment>(); 
            ListaAdjuntos2 = new List<Attachment>(); 
            ContAdj = 0;
            YaAdjuntado = false;
            }else{
                system.debug('No se crea en RoD ya que no es un estado válido');
            }
        }

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Sebastián Ortiz
        Company:       Avanxo
        Description:   public method for updating the confidential field in the case record
        
        In:                         None
        Out:                        Void
        
        <Date>            <Author>                          <Description>
        07/02/2017       Sebastián Ortiz                    Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public void GuardarCampoConfidencial(){
        system.debug('Valor confidencial puesto a --->' + q.BI_Confidencial__c );
        update q;
    }
    
    public void ModificarCampoConfidencial(){
        ModificarConfidencial = true;
    }

    public pageReference Gotomenu(){
        return new PageReference('/500/o');
    }
    
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Sebastián Ortiz
        Company:       Avanxo
        Description:   method that control the open and close of the popouts
        
        In:                         None
        Out:                        Void
        
        <Date>            <Author>                          <Description>
        07/02/2017       Sebastián Ortiz                    Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void closePopup()
    {
        displayPopup = false; 
        YaAdjuntado = false;
        ListaAdjuntos2 = new List<Attachment>();
        ContAdj = 0;
    }
    public void ClosePopupError()
    {
        displayPopUp5= false;
    }
    public void showPopup()
    {
        casoNota = '';
        displayPopup = true;
    }
}