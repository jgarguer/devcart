/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:
* Description:
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-08-14      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_GenerateNoveltyBilling_ctr
*			 1.1	2015-08-19		Jeisson Rojas (JR)		Cambio realizado a la Query de Usuarios del campo Country por Pais__c
*			 1.2    23-02-2017      Jaime Regidor           Adding Campaign Values, Adding Catalog Country , Adding Contact Values
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
*************************************************************************************************************/
@isTest
private class BI_COL_GenerateNoveltyBilling_tst {

	public static BI_COL_GenerateNoveltyBilling_ctr objBilling;
	public static BI_COL_Modificacion_de_Servicio__c objMS;
	public static BI_COL_Modificacion_de_Servicio__c objMS2;
	public static Account objCuenta;
	public static Account objCuenta2;
	public static Contact objContacto;
	public static Opportunity objOportunidad;
	public static BI_COL_Anexos__c objAnexos;
	public static BI_COL_Descripcion_de_servicio__c objDesSer;
	//GMN 21/01/2017 - added to avoid errors in test
	public static BI_COL_Descripcion_de_servicio__c objDesSer2;
	//
	public static Contract objContrato;
	public static Campaign objCampana;
	public static BI_Col_Ciudades__c objCiudad;
	public static BI_Sede__c objSede;
	public static User objUsuario;
	public static BI_Punto_de_instalacion__c objPuntosInsta;
	public static BI_Log__c objBiLog;
	public static BI_COL_GenerateNoveltyBilling_ctr.WrapperMS objWrapperMS;


	public static list <Profile> lstPerfil;
	public static list <UserRole> lstRoles;
	public static list <Campaign> lstCampana;
	public static list<BI_COL_manage_cons__c> lstManege;
	public static List<RecordType> lstRecTyp;
	public static List<User> lstUsuarios;
	public static List<BI_COL_GenerateNoveltyBilling_ctr.WrapperMS> lstWrapperMS;
	public static list<BI_Log__c> lstLog = new list <BI_Log__c>();
	public static List<Account> lstCuentas;

	public static void crearData() {

		//Cuentas
		objCuenta 										= new Account();
		objCuenta.Name                                  = 'prueba';
		objCuenta.BI_Country__c                         = 'Colombia';
		objCuenta.TGS_Region__c                         = 'América';
		objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta.CurrencyIsoCode                       = 'COP';
		objCuenta.BI_Fraude__c                          = false;
		//Insert objCuenta;
		lstCuentas = new List<Account>();
		lstCuentas.add(objCuenta);
		system.debug('datos Cuenta ' + objCuenta);

		objCuenta2                                       = new Account();
		objCuenta2.Name                                  = 'prueba';
		objCuenta2.BI_Country__c                         = 'Colombia';
		objCuenta2.TGS_Region__c                         = 'América';
		objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta2.CurrencyIsoCode                       = 'COP';
		objCuenta2.BI_Fraude__c                          = true;
		//Insert objCuenta2;
		lstCuentas = new List<Account>();
		lstCuentas.add(objCuenta);

		Insert lstCuentas;
		System.debug('datos de la lista cuentas' + lstCuentas);

		BI_COL_RespuestasDavox__c rd = new BI_COL_RespuestasDavox__c(BI_COL_Descripcion_Codigo__c = '[IC: ZONA] Error no Homologado',
																	Name = 'Name'
																	);
		insert rd;

		//Ciudad
		objCiudad = new BI_Col_Ciudades__c ();
		objCiudad.Name                      = 'Test City';
		objCiudad.BI_COL_Pais__c            = 'Test Country';
		objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
		insert objCiudad;

		//Contactos
		objContacto                                     = new Contact();
		objContacto.LastName                            = 'Test';
        objContacto.FirstName 							= 'fisrtname';
		objContacto.BI_Country__c                       = 'Colombia';
		objContacto.CurrencyIsoCode                     = 'COP';
		objContacto.AccountId                           = objCuenta.Id;
		objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
		objContacto.Phone                               = '12345678';
		objContacto.MobilePhone                         = '3104785925';
		objContacto.Email                               = 'pruebas@pruebas1.com';
		objContacto.BI_COL_Direccion_oficina__c         = 'Dirprueba 34550';
		objContacto.BI_COL_Ciudad_Depto_contacto__c     = objCiudad.Id;
		//REING_INI
		//objContacto.BI_Tipo_de_contacto__c          = 'Autorizado';
		objContacto.BI_Tipo_de_contacto__c          = 'General';
		objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
		//REING_FIN
		objContacto.BI_Tipo_de_documento__c             = 'RUT';    
        objContacto.BI_Numero_de_documento__c           = '123456789';
		Insert objContacto;


		//catalogo
		NE__Catalog__c objCatalogo 						= new NE__Catalog__c();
		objCatalogo.Name 								= 'prueba Catalogo';
		objCatalogo.BI_country__c                       = 'Colombia';
		insert objCatalogo;

		//Campaña
		objCampana 										= new Campaign();
		objCampana.Name 								= 'Campaña Prueba';
		objCampana.BI_Catalogo__c 						= objCatalogo.Id;
		objCampana.BI_COL_Codigo_campana__c 			= 'prueba1';
		objCampana.BI_Country__c                        = 'Colombia';
        objCampana.BI_Opportunity_Type__c               = 'Fijo';
        objCampana.BI_SIMP_Opportunity_Type__c          = 'Alta';
		insert objCampana;
		lstCampana 										= new List<Campaign>();
		lstCampana 										= [select Id, Name from Campaign where Id = : objCampana.Id];
		system.debug('datos Cuenta ' + lstCampana);

		//Tipo de registro
		lstRecTyp 										= [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
		system.debug('datos de FUN ' + lstRecTyp);

		//Contrato
		objContrato 									= new Contract ();
		objContrato.AccountId 							= lstCuentas.get(0).Id;
		objContrato.StartDate 							=  System.today().addDays(+5);
		objContrato.BI_COL_Formato_Tipo__c 				= 'Contrato Marco';
		objContrato.BI_COL_Tipo_contrato__c             = 'Contrato Telefónica';
		objContrato.ContractTerm 						= 12;
		objContrato.BI_COL_Presupuesto_contrato__c 		= 1000000;
		objContrato.StartDate							= System.today().addDays(+5);
		objContrato.BI_Indefinido__c 					= 'Si';
		objContrato.BI_COL_Cuantia_Indeterminada__c 	= true;
		objContrato.BI_COL_Duracion_Indefinida__c  		= true;
		insert objContrato;
		System.debug('datos Contratos ===>' + objContrato);

		//FUN
		objAnexos               						= new BI_COL_Anexos__c();
		objAnexos.Name          						= 'FUN-0041414';
		objAnexos.RecordTypeId  						= lstRecTyp[0].Id;
		objAnexos.BI_COL_Contrato__c   					= objContrato.Id;
		insert objAnexos;
		System.debug('\n\n\n Sosalida objAnexos ' + objAnexos + '\n ====> objAnexos.Id ' + objAnexos.Id);

		//Configuracion Personalizada
		BI_COL_manage_cons__c objConPer 				= new BI_COL_manage_cons__c();
		objConPer.Name                  				= 'Viabilidades';
		objConPer.BI_COL_Numero_Viabilidad__c 			= 46;
		objConPer.BI_COL_ConsProyecto__c     			= 1;
		objConPer.BI_COL_ValorConsecutivo__c  			= 1;
		lstManege                      					= new list<BI_COL_manage_cons__c >();
		lstManege.add(objConPer);
		insert lstManege;

		System.debug('======= configuracion personalizada ======= ' + lstManege);

		//Oportunidad
		objOportunidad                                      	= new Opportunity();
		objOportunidad.Name                                   	= 'prueba opp';
		objOportunidad.AccountId                              	= lstCuentas.get(0).Id;
		objOportunidad.BI_Country__c                          	= 'Colombia';
		objOportunidad.CloseDate                              	= System.today().addDays(+5);
		objOportunidad.StageName                              	= 'F6 - Prospecting';
		objOportunidad.CurrencyIsoCode                        	= 'COP';
		objOportunidad.Certa_SCP__contract_duration_months__c 	= 12;
		objOportunidad.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
		//objOportunidad.OwnerId                                	= lstUsuarios[0].id;
		insert objOportunidad;

		system.debug('Datos Oportunidad' + objOportunidad);

		list<Opportunity> lstOportunidad 	= new list<Opportunity>();
		lstOportunidad 						= [select Id from Opportunity where Id = : objOportunidad.Id];

		system.debug('datos ListaOportunida ' + lstOportunidad);

		//Ciudad
		objCiudad = new BI_Col_Ciudades__c ();
		objCiudad.Name 						= 'Test City';
		objCiudad.BI_COL_Pais__c 			= 'Test Country';
		objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
		insert objCiudad;

		//Direccion
		objSede 								= new BI_Sede__c();
		objSede.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
		objSede.BI_Direccion__c 				= 'Test Street 123 Number 321';
		objSede.BI_Localidad__c 				= 'Test Local';
		objSede.BI_COL_Estado_callejero__c 		= System.Label.BI_COL_LbValor3EstadoCallejero;
		objSede.BI_COL_Sucursal_en_uso__c 		= 'Libre';
		objSede.BI_Country__c 					= 'Colombia';
		objSede.Name 							= 'Test Street 123 Number 321, Test Local Colombia';
		objSede.BI_Codigo_postal__c 			= '12356';
		insert objSede;

		//Sede
		objPuntosInsta 						= new BI_Punto_de_instalacion__c ();
		objPuntosInsta.BI_Cliente__c       = lstCuentas.get(0).Id;
		objPuntosInsta.BI_Sede__c          = objSede.Id;
		objPuntosInsta.BI_Contacto__c      = objContacto.id;
		objPuntosInsta.Name 				= 'Pruebas pro';
		insert objPuntosInsta;

		//DS
		objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
		objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
		objDesSer.CurrencyIsoCode               			= 'COP';
		insert objDesSer;

		//GMN 21/01/2017 - added to avoid errors in test
		objDesSer2                                           = new BI_COL_Descripcion_de_servicio__c();
		objDesSer2.BI_COL_Oportunidad__c                     = objOportunidad.Id;
		objDesSer2.CurrencyIsoCode               			= 'COP';
		insert objDesSer2;
		//

		System.debug('Data DS ====> ' + objDesSer);
		System.debug('Data DS ====> ' + objDesSer.Name);
		List<BI_COL_Descripcion_de_servicio__c> lstqry 	= [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
		System.debug('Data lista DS ====> ' + lstqry);

		//MS
		objMS                         				= new BI_COL_Modificacion_de_Servicio__c();
		objMS.BI_COL_FUN__c           				= objAnexos.Id;
		objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
		objMS.BI_COL_Clasificacion_Servicio__c  	= 'BAJA';
		objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
		objMS.BI_COL_Bloqueado__c 					= false;
		objMS.BI_COL_Estado__c 						= label.BI_COL_lblActiva;//label.BI_COL_lblActiva;
		objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
		objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
		objMs.BI_COL_Fecha_inicio_de_cobro_RFB__c	= System.today().Adddays(30);
		objMs.BI_COL_Duracion_meses__c 	            = 12;
		insert objMS;
		system.debug('Data objMs ===>' + objMs);

		objMS2                         				= new BI_COL_Modificacion_de_Servicio__c();
		objMS2.BI_COL_FUN__c           				= objAnexos.Id;
		//GMN 21/01/2017 - changed to avoid errors in test
		objMS2.BI_COL_Codigo_unico_servicio__c 		= objDesSer2.Id;
		//
		objMS2.BI_COL_Clasificacion_Servicio__c  	= 'ALTA DEMO';
		objMS2.BI_COL_Oportunidad__c 				= objOportunidad.Id;
		objMS2.BI_COL_Bloqueado__c 					= false;
		objMS2.BI_COL_Estado__c 					= label.BI_COL_lblPendiente;//label.BI_COL_lblActiva;
		objMS2.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
		objMs2.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
		objMs2.BI_COL_Fecha_inicio_de_cobro_RFB__c	= System.today().Adddays(30);
		objMs2.BI_COL_Duracion_meses__c  			= 12;
		insert objMS2;
		system.debug('Data objMs2 ===>' + objMs2);

		//Bi_Log
		objBiLog									= new BI_Log__c();
		objBiLog.BI_COL_Informacion_recibida__c		= 'OK, Respuesta Procesada';
		//objBiLog.BI_COL_Contacto__c 				= lstContacts[0].Id;
		objBiLog.BI_COL_Estado__c					= 'FALLIDO';
		objBiLog.BI_COL_Interfaz__c					= 'NOTIFICACIONES';
		objBiLog.BI_COL_Modificacion_Servicio__c	= objMS2.id;
		objBiLog.BI_COL_Informacion_Enviada__c 				= 'OOPERATIVA DE TRABAJO ASOCIADO DE PROFESIONALES DE LA SALUD DE DON MATIAS PROSAÕÕKR 65 CL 78 00ÕÕÕÕNIÕJUÕ05001000Õ1Õ4ÕCOOPERATIVA DE TRABAJO ASOCIADO DE PROFESIONALES DE LA SALUD DE DON MATIAS PROSAÕa27w0000001INW8AAOÕKR 65 CL 78 73ÕCARIBEÕ05001000Õ7137';
		lstLog.add(objBiLog);
		insert lstLog;

	}

	static testMethod void test_method_1() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		objUsuario.CompanyName = 'Compania';
		update objUsuario;

		System.runAs(objUsuario) {
			crearData();
			List<string> lstMsString = new List<string>();
			objBilling = new BI_COL_GenerateNoveltyBilling_ctr ();
			objBilling.Busqueda = 'prueba';
			objWrapperMS = new BI_COL_GenerateNoveltyBilling_ctr.WrapperMS(objMS, true);
			List<BI_COL_Modificacion_de_Servicio__c> lstMoS = new List<BI_COL_Modificacion_de_Servicio__c>();

			lstMoS = [Select    Id, BI_COL_FUN__c, BI_COL_Codigo_unico_servicio__c, BI_COL_Clasificacion_Servicio__c,
			          BI_COL_Oportunidad__r.Account.id, BI_COL_Bloqueado__c, BI_COL_Estado__c,
			          BI_COL_Sucursal_de_Facturacion__c, BI_COL_Sucursal_Origen__c
			          from BI_COL_Modificacion_de_Servicio__c
			         ];
			List<BI_Log__c> lbilog = new List<BI_Log__c>();
			for (BI_COL_Modificacion_de_Servicio__c ms : lstMoS) {
				BI_Log__c bp = new BI_Log__c();
				bp.BI_COL_Estado__c = 'Pendiente';
				bp.BI_COL_Informacion_Enviada__c='14:56:18�a16Q000000155OjIAI�1534,1371,219,�SERVICIO INGENIERIA&lt';
				bp.BI_COL_Informacion_recibida__c='14:56:18�a16Q000000155OjIAI�1534,1371,219,�SERVICIO INGENIERIA&lt';
				bp.BI_COL_Modificacion_Servicio__c = ms.id;
				lbilog.add(bp);
			}
			insert lbilog;
			Test.startTest();

			//objBilling.listaClientes = lstCuentas;
			List<SelectOption> lstClienteSel = objBilling.listaClientes;
			objBilling.listaClientes = lstClienteSel;

			objBilling.Busqueda = NULL;
			objBilling.msgEnviado = 'Test';
			objBilling.msgNOEnviado = 'TNULL';


			objBilling.getMSSolicitudServicio(objCuenta.Id);
			objWrapperMS.diasServicio = 1;
			objWrapperMS.valorMulta = 1.1;


			objBilling.strIdCliente = objcuenta.Id;
			lstMsString.add(objMs.Id);
			lstMsString.add(objMs2.Id);
			//lstMsString.add(objMs3.Id);

			objBilling.idMsSeleccionada = lstMsString;


			objBilling.lstMS  = lstWrapperMS;
			objBilling.todosMarcados = TRUE;
			objBilling.cambioCliente();
			objBilling.buscarRepresentanteLegal();
			objBilling.ViewData();
			objBilling.getPageNumber();
			objBilling.getTotalPageNumber();
			objBilling.nextBtnClick();
			objBilling.previousBtnClick();
			objBilling.getLstMS();
			objBilling.getPreviousButtonEnabled();
			objBilling.getNextButtonDisabled();
			objBilling.getPageSize();

			try {objBilling.action_generarModServNovedad();
				} catch(Exception ex){}

			try {objBilling.action_generarNovedadFacturacion();
				} catch(Exception ex){}
			
			objBilling.action_seleccionarTodos();


			Test.stopTest();
		}
	}

	static testMethod void test_method_2() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		objUsuario.CompanyName = 'Compania';
		update objUsuario;

		System.runAs(objUsuario) {

			//ApexPages.StandardController  sc                = new ApexPages.StandardController(objCase);
			PageReference                 pageRef           = Page.BI_COL_GenerateNoveltyBilling_pag;
			Test.setCurrentPage(pageRef);
			Test.startTest();

			//pageRef.getParameters().put('Id',string.valueOf(objCase.Id));
			//pageRef.getParameters().put('Accion','Cancelar');
			//BI_COL_LiberationApproval_ctr liberation = new BI_COL_LiberationApproval_ctr(sc);
			//pageRef.getParameters().put('Accion','Aceptar');
			//liberation = new BI_COL_LiberationApproval_ctr(sc);
			//liberation = new BI_COL_LiberationApproval_ctr(objCase.Id);


			// TO DO: implement unit test
			List < String > lstAux = new List<String>();
			List<string> lstMsString = new List<string>();
			crearData();
			objBilling = new BI_COL_GenerateNoveltyBilling_ctr ();
			objBilling.Busqueda = 'prueba, prueba2';
			objWrapperMS = new BI_COL_GenerateNoveltyBilling_ctr.WrapperMS(objMS, true);
			List<BI_COL_Modificacion_de_Servicio__c> lstMoS = new List<BI_COL_Modificacion_de_Servicio__c>();
			lstMoS = [Select Id, BI_COL_FUN__c, BI_COL_Codigo_unico_servicio__c, BI_COL_Clasificacion_Servicio__c,
			          BI_COL_Oportunidad__r.Account.id, BI_COL_Bloqueado__c, BI_COL_Estado__c,
			          BI_COL_Sucursal_de_Facturacion__c, BI_COL_Sucursal_Origen__c
			          from BI_COL_Modificacion_de_Servicio__c
			         ];

			System.debug('datos lista MS ======>' + lstMoS);

			//objBilling.listaClientes = lstCuentas;
			List<SelectOption> lstClienteSel = objBilling.listaClientes;
			objBilling.listaClientes = lstClienteSel;

			//objBilling.getMSSolicitudServicio('prueba');
			//objBilling.lstIdClientes = [Select Id, BI_COL_Oportunidad__c]
			objBilling.getMSSolicitudServicio(objCuenta.Id);
			objWrapperMS.diasServicio = 1;
			objWrapperMS.valorMulta = 1.1;

			objBilling.lstMS  = lstWrapperMS;
			objBilling.cambioCliente();
			lstMsString.add(objMs.Id);
			lstMsString.add(objMs2.Id);
			//lstMsString.add(objMs3.Id);

			objBilling.idMsSeleccionada = lstMsString;
			objBilling.Buscar(lstMoS);
			BI_COL_GenerateNoveltyBilling_ctr.armarXml('Test', lstLog);
			lstAux.add('Teste');
			BI_COL_GenerateNoveltyBilling_ctr.ActualizaNovedades(lstAux, '<ficha><nombre>Gabriel</nombre><apellido>Molina</apellido><SEPARATOR>TestTest</SEPARATOR><direccion>AlfredoVargas#36</direccion></ficha>');
			/*objBilling.ViewData();
			objBilling.getPageNumber();
			objBilling.getTotalPageNumber();
			objBilling.nextBtnClick();
			objBilling.previousBtnClick();
			objBilling.getLstMS();
			objBilling.getPreviousButtonEnabled();
			objBilling.getNextButtonDisabled();
			objBilling.getPageSize();
			objBilling.action_generarModServNovedad();*/
			for (BI_COL_GenerateNoveltyBilling_ctr.WrapperMS w : objBilling.lstMS) {
				w.seleccionado = true;
			}
			objBilling.action_generarNovedadFacturacion();
			objBilling.action_seleccionarTodos();
			objBilling.lstMS  = lstWrapperMS;
			objBilling.cambioCliente();
			objBilling.buscarRepresentanteLegal();
			objBilling.ViewData();
			objBilling.getPageNumber();
			objBilling.getTotalPageNumber();
			objBilling.nextBtnClick();
			objBilling.previousBtnClick();
			objBilling.getLstMS();
			objBilling.getPreviousButtonEnabled();
			objBilling.getNextButtonDisabled();
			objBilling.getPageSize();
			//objBilling.action_generarModServNovedad();

			Test.stopTest();
		}
	}
}