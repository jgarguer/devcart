/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Julio Laplaza
        Company:       HP
        Description:   Clase base par alos controladores
        
        History:
        
        <Date>              <Author>                                        <Description>
        06/06/2016       Julio Laplaza                                      Creation
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
//
public virtual class BIIN_BaseController
{
    public boolean displayPopupGeneric {get;set;}
    public String errorMessageGeneric {get;set;}

    public void openPopupGeneric()
    {
        displayPopUpGeneric = true;
    }

    public void closePopupGeneric()
    {
        displayPopUpGeneric = false;
    }
}