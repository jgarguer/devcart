/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Test Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-03-25      Manuel Esthiben Mendez Devia (MEMD)     Create Test Class
*                    19/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest
public class BI_COL_CrearData_tst 
{

    static testMethod Account testcreateaccount() // crea una nueva cuenta
    {
       List <RecordType> lstTipoRegistro  = new list <RecordType>();      
       lstTipoRegistro = [Select id,Name from RecordType where Name = 'Telefonica'];
         
        Account cuenta = new Account();
        cuenta.RecordType = lstTipoRegistro.get(0);
        cuenta.Name= 'Registro Prueba'; 
        cuenta.CurrencyIsoCode = 'EUR'; //OAJF: EUR- Euro -> EUR
        cuenta.BI_Segment__c                    = 'test';
        cuenta.BI_Subsegment_Regional__c        = 'test';
        cuenta.BI_Territory__c                  = 'test';
        return cuenta;
         
    }
    
    static testMethod void testcreatecontact() // crea un nuevo contacto
    {
        Contact contacto = new Contact();        
        //contacto.FisrtName= 'Registro Prueba';    
        contacto.LastName= 'Prueba Registro';
        contacto.BI_Country__c= 'Colombia';
        contacto.Email= 'prueba@registronuevo.com';
        contacto.CurrencyIsoCode = 'EUR'; //OAJF: EUR- Euro -> EUR
             
    }
    
    static testMethod void testcreacaso() // crea un nuevo caso Comercial
    {
        Account cuentanew = testcreateaccount();
        insert cuentanew;
        
        List <RecordType> lstTipoRegistro  = new list <RecordType>();      
        lstTipoRegistro = [Select id,Name from RecordType where Name = 'Caso Comercial'];
        
        Case casos = new Case();         
        casos.AccountId= cuentanew.id;  
        casos.Status= 'Nuevo';
        casos.BI_Country__c= 'Colombia';
        casos.RecordType = lstTipoRegistro.get(0);
        casos.Type = 'Consulta';
        casos.BI_Categoria_del_caso__c = 'Nivel 1';
        casos.Priority= 'Media';
        casos.Origin= 'Call center';
        casos.Subject= 'Petición';
             
    }
    
    static testMethod void testcreateoportunidad() // crea una nueva oportunidad
    {
        Account cuentanew = testcreateaccount();
        insert cuentanew;
        
        List <RecordType> lstTipoRegistro  = new list <RecordType>();
        //GMN 05/10/2017 - Modificada la query para usar BI_Ciclo_completo en vez de Proportunidad(Borrado)
        lstTipoRegistro = [Select id,Name from RecordType where SObjectType = 'Opportunity' AND DeveloperName = 'BI_Ciclo_completo'];
        
        Opportunity oportunidad = new Opportunity();         
        oportunidad.AccountId= cuentanew.id;    
        oportunidad.Name= 'Prueba oportunidad';
        oportunidad.BI_Country__c= 'Colombia';
        oportunidad.RecordType = lstTipoRegistro.get(0);
        oportunidad.CloseDate = Date.newInstance(2015, 3, 25);
        oportunidad.StageName = 'F6- Preoportunidad';
        oportunidad.BI_Probabilidad_de_exito__c= 'Media';
        oportunidad.BI_Probabilidad_de_exito__c= '25';  
        oportunidad.CurrencyIsoCode= 'EUR- Euro';
             
    }

    //public Account ObjAccount(){
    
    //return new Account(Name ='NEXTANT SUCURSAL COLOMBIA',
    //        Tipo_Id__c= 'NIT',
    //        Identificaci_n__c = '823698522',
    //        Tipo__c = 'Activo',
    //        Aplica_LD__c = true,
    //        Cartera__c = 'OK',
    //        Naturaleza__c = 'JURIDICA',
    //        Marcaci_n_del_cliente__c = '3',
    //        Segmento_Telef_nica__c = 'EMPRESAS',
    //        Sector_Telef_nica__c = 'CORPORATIVO',
    //        Subsegmento_Telef_nica__c = 'FINANCIERO',
    //        Subsector_Telef_nica__c = 'BANCA',
    //        Actividad_Telef_nica__c = 'BANCA COMERCIAL PRIVADA',
    //        Description = 'Nextant',
    //        Gerencia_Comercial__c = 'JOSE LUIS RODRIGUEZ GARCIA',
    //        Mercado_Objetivo__c = true,
    //        Jefatura_Comercial__c = 'FEDERICO PELAEZ',
    //        Pais__c = 'Colombia',
    //        Phone = '12345678');
    //}
    
    
    //public Opportunity ObjOpportunity(){
    
    //return new Opportunity(
    //        Name = 'Oportunidad prueba ',
    //        Prioridad__c = '3',
    //        CloseDate = System.today().addDays(7),
    //        StageName = 'E0 - E3 Desarrollo');
    //}
    
    //public Prefactibilidad_y_complejidad__c ObjPrefactibilidadYComplejidad(){
    
    //return new Prefactibilidad_y_complejidad__c (
    //        Fecha_Aprobacion_Prefactibilidad__c=System.today(),
    //        Registro_aprobado__c='Aprobada',
    //        C1_1__c = 'Lider en su mercado',    // Medio o bajo impacto en su mercado
    //        C1_2__c = 'A alto nivel directivo',    // A nivel técnico
    //        C_1_3__c = 'Requiere venta consultiva',    // Servicios por definir
    //        P1_4__c = 'Alto',
    //        C2_1__c = 'No hay penalidades',    // Penalidades altamente exigentes
    //        C2_2__c = 'Participación activa',    // No se participó
    //        C2_3__c = 'Más de 45 días',    // Menos de 5 días
    //        C2_4__c = 'Decisivo para cumplimientos de metas',    // No decisivo para cumplimiento de meta
    //        C2_5__c = 'Tenemos mas experiencia que competidores',    // Competidores son expertos en estas soluciones
    //        C2_6__c = 'Cumplimos parcialmente',    // No cumplimos
    //        C3_1__c = 'Algunos aliados con acuerdo compras',    // Ningun aliado con acuerdo compras
    //        C3_2__c = 'Media',    // Ninguna
    //        C3_3__c = 'Tenemos alguna experiencia',    // Ninguna experiencia previa
    //        C3_4__c = 'Menos de 30 días',    // Entre 30 y 60 días
    //        C3_5__c = 'Producto en desarrollo',    // Producto no desarrollado
    //        C3_6__c = 'No requiere',    // Posiblemente lo requiera
    //        C4_1__c = 'Target ajustado',    // Target desconocido
    //        C4_2__c = 'Margen entre el 12 y 25%',    // Sin Margen o no se conoce
    //        C4_3__c = 'No requiere CAPEX',    // Menos de 100 millones COP
    //        C4_4__c = 'Menos de 100 millones COP',    // No requiere OPEX
    //        CalculoPorMS__c = true,
    //        Producto__c = 'SI',
    //        //----- Complejidad
    //        //----- Sección Conectividad
    //        numeroppales__c = 10,
    //        numerobackup__c = 10,
    //        numeroRouters__c = 10,
    //        confVozEnc__c = 10,
    //        BWconcentracion__c = 10,
    //        BWsucursales__c = 10,
    //        puntosRurSat__c = 10,
    //        enlaceInternet__c = 'SI',
    //        BWinternet__c = 10,
    //        Solucion_de_CDN__c = 'SI',
    //        Gestion_del_servicio__c = 'SI',
    //        InvolucraSSII__c = 'SI',
    //        //----- Sección Colaboración
    //        numPuertos__c = 10,
    //        ToIp__c = 'SI',
    //        Comunicaciones_Unificadas__c = 'SI',
    //        Troncal_SIP__c = 10,
    //        numeroE1__c = 10,
    //        servTel__c = 'SI',
    //        Numero_de_Puntos_VC_TP_CD__c = 10,
    //        solVideoconf__c = 'SI',
    //        Solucion_de_Telepresencia__c = 'SI',
    //        Solucion_Contact_Center_infraestructura__c = 'SI',
    //        Solucion_Telefonia_en_la_nube__c = 'SI',
    //        Solucion_de_Carteleras_Digitales__c = 'SI',
    //        Otras_aplicaciones__c = 'SI',
    //        Gestion_del_Servicio_Colaboracion__c = 'SI',        
    //        //----- Sección Servicios Moviles
    //        Voz_Movil__c = 10,
    //        Datos_Movil__c = 10,
    //        Internet_Movil__c = 10,
    //        SMS__c = 10,
    //        Aplicaciones_SMS__c = 10,
    //        E1s_Movil_PRI__c = 10,
    //        Gateway_Movil_PRI_SIP__c = 10,
    //        ventaDisMov__c = 10,
    //        Correo_Movil__c = 10,
    //        M2M_Machine_to_Machine_Producto__c = 10,
    //        M2M_Machine_to_Machine_en_Desarrollo__c = 'SI',
    //        solMov__c = 'SI',       //Corresponde al campo Solución Moviles (Aplicación Vertical)
    //        Gestion_del_servicio_Servicios_Moviles__c = 'SI',   
    //        //----- Sección Infraestructura TI
    //        Solucion_Servidores__c = '0–5',     //0-5, 6-20, mas de 20
    //        Solucion_Almacenamiento__c = '0–5', //0-5, 6-20, mas de 20
    //        Solucion_Otros_PC_Laptop_impresora__c = 'SI',
    //        Solucion_Colocation__c = 'SI',
    //        Solucion_Hosting__c = 'SI',
    //        Solucion_Disaster_Recovery_DRP__c = 'SI',
    //        //----- Sección Cloud Computing
    //        IaaS_H2_0_solucion_virtualizada_etc__c = '0–5',     //0-5, 6-20, mas de 20
    //        PaSS_VDI_VDC__c = 'SI',
    //        SaaS__c = 'SI',
    //        Servicios_Gestionados__c = 'SI',
    //        //----- Sección Seguridad
    //        Servicios_End_Point__c = '0-500',       //0-500, 501-2000, mas de 2000 licencias
    //        Solucion_de_Seguridad_HW_SW__c = '0-5', //0-5, mas de 6
    //        Trafico_Seguro__c = 'basic',        //basic, professional, advanced, premium, enterprise
    //        Consultoria_de_Seguridad_de_la_Informaci__c = 'SI',
    //        Gestion_del_servicio_Seguridad__c = 'SI',
    //        //----- Sección Consultoría TI
    //        Servicios_Profesionales_B_sicos__c = 'Instalación',     //Instalación, Soporte, Especializada por horas, Consutoría
    //        Consultoria_Especializada__c = 'SI',
    //        Aplicaciones_Sectoriales__c = 'SI',
    //        Aplicaciones_Empresariales__c = 'SI',
    //        Otros_desarrollo_SW_hetas_de_Gestion__c = 'SI',
    //        //----- Sección Outsorsing
    //        Full_Outsourcing__c = 'SI',
    //        Servicio_de_HelpDesk_Mesa_de_Servicio__c = '0-50 puestos de trabajo',       //0-50 puestos de trabajo, 51-150 puestos de trabajo, mas de 150 puestos de trabajo
    //        Servicio_Especializado__c = 'SI',
    //        Servicio_de_Contact_Center__c = '0-5000',       //0-5000, 5001-10000, mas de 10000 agentes
    //        PDTI__c = '0-50 puestos de trabajo',        //0-50 puestos de trabajo, 51-150 puestos de trabajo, mas de 150 puestos de trabajo
    //        CGP__c = 'SI',
    //        Otros_OS_Impresi_n_OS_puesto_trabajo__c = 'SI',
    //        //----- Sección Networking y Facilidades
    //        Equipos_de_Redes_WAN_LAN_WLAN__c = 'SI',
    //        solAdmBW__c = 'SI',
    //            Solucion_Balanceo_de_Carga__c = 'SI',
    //            Venta_de_Soluciones_de_Energia__c = 'SI',
    //            Venta_de_Seguridad_F_sica__c = 'SI',
    //            ventaCableado__c = 'SI',    
    //            Venta_de_Otros__c = 'SI',
    //            //----- Sección Información Adicional
    //            Oferta_en_Servicio__c = 'SI',
    //            La_oferta__c = 'SI',
    //            Monto_de_contrato_12_meses__c = 'Menos de $300 M',  //Menos de $300 M, Entre $300 M y 600 M, Entre $600 M y 1000 M, Entre $1000 M y 5000 M, Entre $5000 M y 10000 M, Mas de $10000 M
    //            Cantidad_de_proveedores_a_gestionar__c = '1',       //1, 2, 3, 4, mas de 4
    //            Clasificaci_nn_de_la_oferta__c = 'Licitación'   //Licitación, Oferta Comercial, RFI/Cotización
    //        );
    //        }
            
    //        public MIDAS__c objMidas(){
            
    //        return new MIDAS__c(
    //            CAPEX__c=1,    // CAPEX 
    //            Datos_instalaci_n__c=1,    // Datos instalación 
    //            Datos__c=1,    // Datos 
    //            Descripci_n_del_negocio__c='Descripci_n_del_negocio__c',    // Descripción del negocio 
    //            Ebitda__c=50,    // Ebitda 
    //            Estado_de_la_aprobaci_n__c='Pendiente Aprobacion',    // Pendiente Aprobacion 
    //            Fecha_de_solicitud__c=Date.today(),    // Fecha de solicitud 
    //          //OA: campo deprecado  Internet__c=1,    // Internet 
    //            OPEX__c=1,    // OPEX 
    //           //OA: Campo deprecado Servicios_m__c=1,    // Servicios móviles 
    //           //OA: Campo deprecado  Servicios_voz_fija_Instalaci_n__c=1,    // Servicios voz fija Instalación 
    //          //OA: campo deprecado   Servicios_voz_fija__c=1,    // Servicios voz fija 
    //          //OA: Campo deprecado   TI_instalaci_n__c=1,    // TI instalación 
    //           //OA: campo deprecado TI__c=1,    // TI 
    //            //OA: Campo deprecado TV_Contenidos_instalaci_n__c=1,    // TV & Contenidos instalación 
    //           //OA: campo deprecado  TV_Contenidos__c=1,    // TV & Contenidos 
    //            Versi_n__c=0,    // Versión 
    //            VPN_Capex__c=0,    // VPN /Capex 
    //            VPN__c=1,
    //            TIR__c = 50,
    //            PRI__c = 1,
    //            Tipo_de_solicitud__c='Renegociación'
    //            );
    //        }
            
    //    public Sucursal__c objSucursal(ID cliente){
        
    //    return new Sucursal__c(
    //            Cliente__c = cliente,
    //            Name = 'Sucursal Prueba',
    //            Tipo_Sucursal__c = 'PRINCIPAL',
    //            Estado_callejero__c='Validado por callejero',
    //            Direcci_n_Sucursal__c='calle 142 # 22-09',
    //            Ciudad__c = this.objCiudad()
    //            );
    //    }
        
    //    public ID objCiudad(){
            
    //        Ciudad__c ciu =new Ciudad__c(
    //            Name= 'BOGOTA DC / CUNDINAMARCA',
    //            Codigo_DANE__c = '11001000',
    //            Regional__c = 'Centro Sur');
    //        insert ciu;
            
    //    return  ciu.id;
    //    }
        
    //    public Producto_Telefonica__c objProducto(){
        
        
    //    return new Producto_Telefonica__c(
    //            Tipo_registro_DS__c = 'DS-Conectividad',
    //            Segmento__c = 'EMPRESAS',
    //            Producto__c = 'MPLS',
    //            Linea__c = 'BANDA ANCHA',
    //            Familia__c = 'Datos',
    //            Descripcion_Referencia__c = 'VPN IP MPLS DATOS',
    //            Cod_Desc_Referencia__c = 'EQCENO79',
    //            Anchos_de_banda__c = this.objAnchosdeBanda(),
    //            Activo__c = true);
    //    }
        
    //    public ID objAnchosdeBanda(){
    //        Anchos_de_Banda__c ab=new Anchos_de_Banda__c(Name = 'DS1',Ancho_de_Banda__c = 'DS1');
    //        insert ab;
    //        return ab.id;
    //    }
        
    //    public Nueva_DS__c objNuevaDS(){
    //        return new Nueva_DS__c(Estado_servicio__c='Activado');
    //    }
        
    //    public Modificacion_servicio__c objMS(){
        
    //    return new Modificacion_servicio__c(
    //        Clasificaci_n_del_Servicio__c='ALTA',
    //        Autorizacion_Facturacion__c=true,
    //        Tipo_de_Facturaci_n__c ='Anticipada',
    //        Plazo_de_Vencimiento_Factura__c='45 días',
    //        Medio_Vendido__c='DROP AND INSERT D&I',
    //        Cuenta_facturar_davox__c='0',
    //        Estado__c = 'Activa',
    //        RecordTypeId=Schema.SObjectType.Modificacion_servicio__c.getRecordTypeInfosByName().get('MS-Internet Dedicado').getRecordTypeId(),
    //        TRM__c=1,
    //        Cargo_conexion__c=200
    //        );
    //    }
        
    //    public Contract objContrato(){
            
    //        return new Contract(Status='Borrador',Anexos_Valor_Agregado__c =true, StartDate=Date.today(),ContractTerm=5,Anexo_Internet_Seguro__c=true);
    //    }
        
    //    public Contact objContacto(){
        
    //        return new Contact(FirstName='Pedro', LastName='Perez Pereira',MobilePhone='7374864874',Tel_fono_Oficina__c='2837932478'); 
    //    }
        
    //    public FUN__c objFun(){
        
    //        return new Fun__c(); 
    //    }
        
    //    public Contrato_Cliente__c objUniTem(){
        
    //        return new Contrato_Cliente__c(name='union temporal01'); 
    //    }
    
}