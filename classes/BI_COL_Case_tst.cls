/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for class BI_COL_Case_ctr
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-08-20      Raul Mora (RM)				    Create class
					2017-08-18      Guillermo Muñoz         		Changed to use new BI_MigrationHelper functionality
*					20/09/2017      Angel F. Santaliestra			Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private class BI_COL_Case_tst 
{
	Public static list<Profile> 		lstPerfil;
	Public static list <UserRole> 		lstRoles;
	Public static User 					objUsuario = new User();
	public static List<Case> 			lstCase;
	public static Account 				objCuenta;
	public static List<User> 			lstUser;
	
	public static void createData()
	{
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass; 
            BI_MigrationHelper.skipAllTriggers();

			objCuenta                                       = new Account();
			objCuenta.Name                                  = 'prueba';
			objCuenta.BI_Country__c                         = 'Colombia';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = 'NIT';
			objCuenta.CurrencyIsoCode                       = 'GTQ';
			objCuenta.BI_Segment__c                         = 'test';
			objCuenta.BI_Subsegment_Regional__c             = 'test';
			objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			
			lstCase = new List<Case>();
			
			Case objCase = new Case();         
	        objCase.AccountId= objCuenta.Id;  
	        objCase.Status= 'Assigned';
	        objCase.BI_Country__c= 'Colombia';
	        objCase.Type = 'Consulta';
	        objCase.BI_Categoria_del_caso__c = 'Nivel 1';
	        objCase.Priority= 'Media';
	        objCase.Origin= 'Call center';
	        objCase.Subject= 'Petición';
	        objCase.BI_COL_Promesa_Cliente_dias_calendario__c = '1';
	        objCase.BI_COL_Fecha_Radicacion__c = System.now();
	        
	        lstCase.add( objCase );
	        
	        Case objCase2 = new Case();         
	        objCase2.AccountId= objCuenta.Id;  
	        objCase2.Status= 'Assigned';
	        objCase2.BI_Country__c= 'Colombia';
	        objCase2.Type = 'Consulta';
	        objCase2.BI_Categoria_del_caso__c = 'Nivel 1';
	        objCase2.Priority= 'Media';
	        objCase2.Origin= 'Call center';
	        objCase2.Subject= 'Petición';
	        objCase2.BI_COL_Promesa_Cliente_dias_calendario__c = '15';
	        objCase.BI_COL_Fecha_Radicacion__c = System.now();
	        lstCase.add( objCase2 );
	        
	        insert lstCase;
	        BI_MigrationHelper.cleanSkippedTriggers();
	}
	
    static testMethod void myUnitTest() 
    {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) 
		{
			createData();
			BI_COL_Case_ctr.envioCasesLst( lstCase );
		}
    }
}