public class BI_LeadMethods {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Methods executed by Lead Triggers 
    Test Class:    BI_LeadMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    04/08/2014              Ana Escrich             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    final static Map<String,Id> MAP_NAME_PAIS = new Map<String,Id>();
    
    
    static{
        /*for(Region__c region :[SELECT Id, Name FROM Region__c]){
            MAP_NAME_PAIS.put(region.Name, region.Id);
        }*/

        //SET_CAM_PAIS.addAll(new List<Id>(){MAP_NAME_PAIS.get(Label.BI_Nicaragua), MAP_NAME_PAIS.get(Label.BI_Panama),  MAP_NAME_PAIS.get(Label.BI_Salvador),  MAP_NAME_PAIS.get(Label.BI_Guatemala), MAP_NAME_PAIS.get(Label.BI_CostaRica)});
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Method that checks the format of the Fiscal Ids depending on BI_Tipo_de_Identificador_Fiscal_Ref__c.Name.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    04/08/2014              Ana Escrich             Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void validateId(List <Lead> olds, List <Lead> news){
        try{
            Set <String> set_options = new Set <String>();

            Set<String> SET_CAM_PAIS = new Set<String>();
                           
            SET_CAM_PAIS.add(Label.BI_Nicaragua);
            SET_CAM_PAIS.add(Label.BI_Panama);
            SET_CAM_PAIS.add(Label.BI_Salvador);
            SET_CAM_PAIS.add(Label.BI_Guatemala);
            SET_CAM_PAIS.add(Label.BI_CostaRica);


    
            for (Lead item:news){
                if(item.BI_Tipo_de_identificador_fiscal__c != null)
                    set_options.add(item.BI_Tipo_de_identificador_fiscal__c);
            }
        
            //Map<Id, BI_Pickup_Option__c> map_options = new Map<Id, BI_Pickup_Option__c>([select Id, Name from BI_Pickup_Option__c where Id IN :set_options]);

            Integer i = 0;
            for (Lead item:news){
                
                    if((item.BI_Tipo_de_identificador_fiscal__c != null || item.BI_Numero_identificador_fiscal__c != null) 
                                && item.BI_Tipo_de_identificador_fiscal__c != Label.BI_ClienteExterior){
                    
                            if(item.BI_Tipo_de_identificador_fiscal__c == null || item.BI_Numero_identificador_fiscal__c == null){
                        
                                item.addError(Label.BI_ValidacionIdFiscal);
                                
                            //}else if ((olds != null && item.BI_Numero_identificador_fiscal__c != olds[i].BI_Numero_identificador_fiscal__c) || olds == null){
                            }else if (
                                olds == null 
                                ||
                                (olds != null && ((item.BI_Numero_identificador_fiscal__c != olds[i].BI_Numero_identificador_fiscal__c) || (item.BI_Tipo_de_identificador_fiscal__c != olds[i].BI_Tipo_de_identificador_fiscal__c)))
                            ){

                                    if(item.BI_Tipo_de_identificador_fiscal__c != null && item.BI_Numero_identificador_fiscal__c == null){
                                        item.addError(Label.BI_ValidacionIdFiscal);
                                    }
                                    //Argentina
                                    else if(item.BI_Tipo_de_identificador_fiscal__c == 'CUIT' || item.BI_Tipo_de_identificador_fiscal__c == 'CUIL'){ // 11 Numbers only display CUIT/CUIL for ARG
                                        if (!CUITisValid(item.BI_Numero_identificador_fiscal__c)){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    else if(item.BI_Country__c == Label.BI_Argentina && item.BI_Tipo_de_identificador_fiscal__c == 'DNI'){ // 10 Numbers-- Numeric
                                        if (item.BI_Numero_identificador_fiscal__c.length() > 10 || !(item.BI_Numero_identificador_fiscal__c.IsNumeric())){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    else if(item.BI_Country__c == Label.BI_Argentina && (item.BI_Tipo_de_identificador_fiscal__c == 'PASAPORTE'|| item.BI_Tipo_de_identificador_fiscal__c == 'Cliente exterior')){ // Alphaumeric
                                        if (item.BI_Numero_identificador_fiscal__c.length() > 10 || !item.BI_Numero_identificador_fiscal__c.isAlphanumeric()){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    //End Argentina

                                    //Only to Venezuela 
                                    else if (item.BI_Tipo_de_identificador_fiscal__c == 'RIF') { // 1 letter - 9 numbers A123456789
                                        if (item.BI_Numero_identificador_fiscal__c.length() != 10 ||
                                            !item.BI_Numero_identificador_fiscal__c.substring(0,1).isAlpha() || !item.BI_Numero_identificador_fiscal__c.substring(1,10).IsNumeric()){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    //End to Venezuela 

                                    else if (item.BI_Tipo_de_identificador_fiscal__c == 'NIT' && !SET_CAM_PAIS.contains(item.BI_Country__c)) { // 9 numbers 123456789
                                        if (item.BI_Numero_identificador_fiscal__c.length() != 9 ||
                                            !item.BI_Numero_identificador_fiscal__c.substring(0,9).IsNumeric()){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    else if (item.BI_Tipo_de_identificador_fiscal__c == 'NITE') { //10 or 12 numbers 1234567891 OR 123456789101
                                        if ((item.BI_Numero_identificador_fiscal__c.length() != 10 && item.BI_Numero_identificador_fiscal__c.length() != 12) ||
                                            !item.BI_Numero_identificador_fiscal__c.IsNumeric()){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    else if (item.BI_Tipo_de_identificador_fiscal__c == 'RUT' && item.BI_Country__c != Label.BI_Chile) { // 8 numbers - 1 letter 12345678A
                                        if (item.BI_Numero_identificador_fiscal__c.length() != 9 ||
                                            !item.BI_Numero_identificador_fiscal__c.substring(0,8).IsNumeric() || !item.BI_Numero_identificador_fiscal__c.substring(8,9).isAlpha()){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    else if (item.BI_Tipo_de_identificador_fiscal__c == 'CPF') { //11 Numbers 12345678910
                                        if (item.BI_Numero_identificador_fiscal__c.length() != 11 || !item.BI_Numero_identificador_fiscal__c.IsNumeric()){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    else if (item.BI_Country__c != Label.BI_Ecuador && !SET_CAM_PAIS.contains(item.BI_Country__c) && item.BI_Tipo_de_identificador_fiscal__c == 'RUC') { //11 Numbers 12345678910
                                        if (item.BI_Numero_identificador_fiscal__c.length() != 11 || !item.BI_Numero_identificador_fiscal__c.IsNumeric()){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    else if (item.BI_Tipo_de_identificador_fiscal__c == 'RTN') { //14 Numbers 12345678910112
                                        if (item.BI_Numero_identificador_fiscal__c.length() != 14 || !item.BI_Numero_identificador_fiscal__c.IsNumeric()){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    else if (item.BI_Tipo_de_identificador_fiscal__c == 'RFC') { //4 letters 6 numbers 3 characters VECB380326XXX
                                        if (item.BI_Numero_identificador_fiscal__c.length() != 13 || !item.BI_Numero_identificador_fiscal__c.substring(0,4).isAlpha() || !item.BI_Numero_identificador_fiscal__c.substring(4,9).IsNumeric()){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }

                                //Only to Ecuador 
                                if(item.BI_Country__c == Label.BI_Ecuador){
                                    if (item.BI_Tipo_de_identificador_fiscal__c == 'PASAPORTE') { //alphanumeric at least 5 characters
                                        if (item.BI_Numero_identificador_fiscal__c.length() < 5 || !item.BI_Numero_identificador_fiscal__c.isAlphanumeric()){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    else if (item.BI_Tipo_de_identificador_fiscal__c == Label.BI_Cedula) { // 10 numbers
                                        if (item.BI_Numero_identificador_fiscal__c.length() != 10 || !item.BI_Numero_identificador_fiscal__c.IsNumeric()){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    else if (item.BI_Tipo_de_identificador_fiscal__c == 'RUC') { //13 numbers
                                        if (item.BI_Numero_identificador_fiscal__c.length() != 13 || !item.BI_Numero_identificador_fiscal__c.IsNumeric() ){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    else if (item.BI_Tipo_de_identificador_fiscal__c == 'RUC ESPECIAL') { //1 number
                                        if (item.BI_Numero_identificador_fiscal__c.length() != 1 || !item.BI_Numero_identificador_fiscal__c.IsNumeric() ){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                }
                            //End Only to Ecuador

                                //Only to Mexico 
                                if(item.BI_Country__c == Label.BI_Mexico){
                                    item.BI_Numero_identificador_fiscal__c = item.BI_Numero_identificador_fiscal__c.toUpperCase();

                                    if (item.BI_Tipo_de_identificador_fiscal__c == Label.BI_RFC_Moral) { //  12 characters  = 3 alpha +  6 numbers + 3 alphanumeric
                                        if (item.BI_Numero_identificador_fiscal__c.length() != 12 || !item.BI_Numero_identificador_fiscal__c.substring(0,3).isAlpha() || !item.BI_Numero_identificador_fiscal__c.substring(3,9).IsNumeric() || !item.BI_Numero_identificador_fiscal__c.substring(9,12).IsAlphaNumeric()){ 
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                    else if (item.BI_Tipo_de_identificador_fiscal__c == Label.BI_RFC_Fisica) { //  13 characters  = 4 alpha +  6 numbers + 3 alphanumeric
                                        if (item.BI_Numero_identificador_fiscal__c.length() != 13 || !item.BI_Numero_identificador_fiscal__c.substring(0,4).isAlpha() || !item.BI_Numero_identificador_fiscal__c.substring(4,10).IsNumeric() || !item.BI_Numero_identificador_fiscal__c.substring(10,13).IsAlphaNumeric()){ 
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                }
                                //End Only to Mexico

                                //Only to Chile 
                                if(item.BI_Country__c == Label.BI_Chile){
                                    if (item.BI_Tipo_de_identificador_fiscal__c == 'RUT') { //00000000-X  //X= Alphanumeric 0= numeric
                                        if (!BI_ValidateRutHelper.check_RUT_chile(item.BI_Numero_identificador_fiscal__c)){
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                }
                                //End Only to Chile


                                //Only to CAM 

                                    //Nicaragua y Panamá: RUC, NIT, CUC, CEDULA (Alfanumérico 14 Posiciones).
                                    //Salvador y Guatemala: NIT, CUC.
                                    //Costa Rica: RUC, CUC.

                                 if(SET_CAM_PAIS.contains(item.BI_Country__c) ){
                                    if (
                                        item.BI_Tipo_de_identificador_fiscal__c != 'RUC'      &&
                                        item.BI_Tipo_de_identificador_fiscal__c != 'NIT'      &&
                                        item.BI_Tipo_de_identificador_fiscal__c != 'CUC'      &&
                                        item.BI_Tipo_de_identificador_fiscal__c == 'CEDULA') { 

                                        if (item.BI_Numero_identificador_fiscal__c.length() != 14 || !item.BI_Numero_identificador_fiscal__c.IsAlphaNumeric()){ 
                                            item.addError(Label.BI_ValidacionIdFiscal);
                                        }
                                    }
                                 }

                                //End Only to CAM 



                            }                           
                    }
                i++;
                }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_LeadMethods.validateId', 'BI_EN', Exc, 'Trigger');
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if the CUIT Number is valid
    
    IN:            CUIT Number
    OUT:           true, false
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    24/09/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static boolean CUITisValid(String InNroCuit) {

        boolean ret = false;

        Integer position, modCoef, SumTotal, Digit, Factor;

        String CLFSC_ARG = '54327654321';

        Integer CLFSC_ARG_LEN = 11;

        if (InNroCuit.length() != CLFSC_ARG_LEN) {
            return false;
        }
        if(!Pattern.matches('^\\d{11}$', InNroCuit)) {
            return false;
        }
        if(Double.valueOf(InNroCuit) <= 0)
            return false;
        SumTotal = 0;
        position = 0;

        while(position < CLFSC_ARG_LEN) {
            Factor = Integer.valueOf( CLFSC_ARG.substring(position, position+1) );
            Digit = Integer.valueOf( InNroCuit.substring(position, position+1) );
            SumTotal = SumTotal + (Factor * Digit);
            position++;
        }
        modCoef = Math.mod(SumTotal, CLFSC_ARG_LEN);
        if(modCoef != 0) {
            return false;
        }

        return true;
    }
  

         /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    28/03/2015              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void validateSegment(List<Lead> news, List<Lead> olds){
        /*

        PLL_PickList_Refactorizacion_Necesaria
        try{
            Set<Id> set_acc = new Set<Id>();
            Integer i = 0;
            for (Lead cand:news){
                if (olds == null || (olds != null && (cand.BI_Segment__c != olds[i].BI_Segment__c || cand.BI_Territorio__c != olds[i].BI_Territorio__c
                                                    || cand.BI_Country__c != olds[i].BI_Country__c ))){
                    set_acc.add(cand.Id);
                }
                i++;
            }
            Map <String, List <String>> map_pais_seg_sub = new Map <String, List <String>>();
            if(!set_acc.isEmpty()){

                List<BI_Dependencia_Territorio__c> territorio = BI_Dependencia_Territorio__c.getAll().values();
                
                for (BI_Dependencia_Territorio__c seg:territorio){
                    if(!map_pais_seg_sub.containsKey(seg.BI_Pais__c)){
                        List <String> lst_seg = new List <String>();
                        lst_seg.add(seg.BI_Segmento_regional__c + ' - ' + seg.BI_Territorio__c);
                        map_pais_seg_sub.put(seg.BI_Pais__c, lst_seg);
                    }else{
                        map_pais_seg_sub.get(seg.BI_Pais__c).add(seg.BI_Segmento_regional__c + ' - ' + seg.BI_Territorio__c);
                    }
                    
                }
                system.debug('#Debug map_pais_seg_sub'+datetime.now()+ map_pais_seg_sub);
                if(territorio != null){
                    Map <Id, String> map_error = new Map <Id, String>();
                    Set <Id> set_ok = new Set <Id>();
                    for(Lead cand:news){
                        Boolean contiene = false;
                        if(!set_acc.IsEmpty() && set_acc.contains(cand.Id) && !map_pais_seg_sub.KeySet().IsEmpty()){                            
                            for(String segm:map_pais_seg_sub.get(cand.BI_Country__c)){
                                system.debug('#Debug compare '+ cand.BI_Segment__c + ' - ' + cand.BI_Territorio__c);
                                 //system.debug('#Debug compare '+ segm.BI_Segmento_regional__c + ' - ' + segm.BI_Territorio__c);
                                if(cand.BI_Segment__c != null && cand.BI_Territorio__c != null && segm.contains(cand.BI_Segment__c) && segm.contains(cand.BI_Territorio__c)){
                                    contiene = true;
                                }
                            } 
                            if(!contiene && cand.BI_Segment__c != null && cand.BI_Territorio__c != null){
                                map_error.put(cand.Id, 'Los valores válidos de territorio para el país ' + cand.BI_Country__c + ' son ' + map_pais_seg_sub.get(cand.BI_Country__c));
                            }
                        }
                    }
                    system.debug('#Debug map_error'+datetime.now()+ map_error);
                    for(Lead cand:news){
                        if(map_error.containsKey(cand.Id)){
                            cand.addError(map_error.get(cand.Id));
                        }
                    }
                }
                
            }
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_AccountMethods.validateSegment', 'BI_EN', exc, 'Trigger');
        }
        */
    }
}