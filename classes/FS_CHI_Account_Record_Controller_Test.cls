/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Clase de prueba: FS_CHI_Account_Record_Controller

History:
<Date>							<Author>						<Change Description>
29/05/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class FS_CHI_Account_Record_Controller_Test {
    
    @testSetup 
    private static void init() {
        
    }
    
    @isTest private static void createAccount() {
        /* Test */
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba';
        insert config_integracion;
        System.Test.startTest();
        
        try{
            System.assert([SELECT Count() FROM Account] == 0);
            FS_CHI_Account_Record_Controller.createAccount('Chile', 'RUC', '20552003609', 'Test [Integración] - Chile', 'CLP', true);//'6895623147', '123456789',
            System.assert([SELECT Count() FROM Account] == 1);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void reintentar() {
        /* Setup */
        Account account = new Account(Name = 'Test [Integración] - Chile', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20552003609');
        insert account;
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba2';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba2';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba2';
        insert config_integracion;
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CHI_Account_Record_Controller.reintentar(account.Id);
            System.assertEquals('Validado', [SELECT FS_CHI_Validacion_Fiscal__c FROM Account LIMIT 1][0].FS_CHI_Validacion_Fiscal__c);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void reintent() {
        /* Setup */
        Account account = new Account(Name = 'Test2 [Integración] - Chile', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_Country__c = 'Chile', BI_No_Identificador_fiscal__c = '20552003609');
        insert account;
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba3';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba3';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba3';
        insert config_integracion;
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CHI_Account_Record_Controller.reintentarL(account.Id);
            System.assertEquals('Validado', [SELECT FS_CHI_Validacion_Fiscal__c FROM Account LIMIT 1][0].FS_CHI_Validacion_Fiscal__c);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void reintent2() {
        /* Setup */
        Account account = new Account(Name = 'Test3 [Integración] - Chile', BI_Tipo_de_identificador_fiscal__c = 'RUC', FS_CHI_Validacion_Fiscal__c = 'Validado');
        insert account;
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba4';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba4';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba4';
        insert config_integracion;
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CHI_Account_Record_Controller.reintentarL(account.Id);
            System.assertEquals('Validado', [SELECT FS_CHI_Validacion_Fiscal__c FROM Account LIMIT 1][0].FS_CHI_Validacion_Fiscal__c);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void reintent3() {
        /* Setup */
        Account account = new Account(Name = 'Test4 [Integración] - Chile', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_Country__c = 'Chile', BI_No_Identificador_fiscal__c = '120047787');
        insert account;
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba5';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba5';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba5';
        insert config_integracion;
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CHI_Account_Record_Controller.reintentarL(account.Id);
            System.assertEquals('No validado', [SELECT FS_CHI_Validacion_Fiscal__c FROM Account LIMIT 1][0].FS_CHI_Validacion_Fiscal__c);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void reintent4() {
        /* Setup */
        Account account = new Account(Name = 'Test5 [Integración] - Chile', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_Country__c = 'Chile', BI_No_Identificador_fiscal__c = '91600307');
        insert account;
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba6';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba6';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba6';
        insert config_integracion;
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CHI_Account_Record_Controller.doInitController(account.Id);
            System.assertEquals('No validado', [SELECT FS_CHI_Validacion_Fiscal__c FROM Account LIMIT 1][0].FS_CHI_Validacion_Fiscal__c);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void reintent5() {
        /* Setup */
        Account account = new Account(Name = 'Test6 [Integración] - Chile', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_Country__c = 'Chile', BI_No_Identificador_fiscal__c = '234134');
        insert account;
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba7';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba7';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba7';
        insert config_integracion;
        /* Test */
        System.Test.startTest();
        
        try{
            FS_CHI_Account_Record_Controller.reintentarL(account.Id);
            System.assertEquals(null, [SELECT FS_CHI_Validacion_Fiscal__c FROM Account LIMIT 1][0].FS_CHI_Validacion_Fiscal__c);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void createAccountExc() {
        /* Test */
        Account account = new Account(Name = 'Test [Integración] - Chile', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_Country__c = 'Chile', BI_No_Identificador_fiscal__c = '20552003609');
        insert account;
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba';
        insert config_integracion;
        System.Test.startTest();
        
        try{
            //System.assert([SELECT Count() FROM Account] == 0);
            FS_CHI_Account_Record_Controller.createAccount('Chile', 'RUC', '20552003609', 'Test [Integración] - Chile', 'CLP', true);
            //System.assert([SELECT Count() FROM Account] == 1);
        }catch(Exception exc){
            //System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void retrieveJSON1() {
        /* Test */
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba8';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba8';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba8';
        insert config_integracion;
        System.Test.startTest();
        System.debug(' FS_CHI_Account_Record_Controller.retrieveJSON: ' +  FS_CHI_Account_Record_Controller.retrieveJSON('RUC', '20552003609'));
        try{
            String expectedResult = '{"totalResults":1,"customersList":{"customerList":[{"legalId":[{"nationalIDType":"NIF","nationalID":"000000000X","isPrimary":null,"country":"Argentina"}],"customerId":"14785AAE1","customerDetails":{"subsegment":"Masivo","segment":"Empresas","satisfaction":null,"parentCustomer":"0012600000Q8BQUBB3","name":null,"legalId":null,"description":"Example.","customerStatus":"active","customerRank":null,"customerName":"Telefonica","cstType":null,"creditProfiles":[{"creditScore":null,"creditRiskRating":2,"creditProfileDate":"2017-01-20T10:53:58.223Z"}],"correlatorId":"0012600000S3bfn","contacts":null,"contactingModes":null,"address":null,"additionalData":[{"value":"TEF","key":"DenominacionComercial"},{"value":"1-415-555-1212","key":"FaxEmpresa"},{"value":"917358987","key":"TelefonoReferencia"},{"value":"Comunicacionesyteconología","key":"Sector"},{"value":"Cliente","key":"Holding"},{"value":"True","key":"CuentaGlobal"},{"value":"Privado","key":"Ambito"},{"value":"GobiernoNacional","key":"Subsector"},{"value":"www.telefonica.com","key":"Website"},{"value":"2017-01-20T10:53:58.223Z","key":"FechaActivacion"},{"value":"2017-01-30T10:53:58.223Z","key":"FechaDesactivacion"}]},"customerAddresses":[{"region":"Madrid","postalCode":"28050","normalized":null,"municipality":null,"locality":"Madrid","isDangerous":null,"floor":null,"country":"España","coordinates":null,"area":null,"apartment":null,"addressType":null,"addressNumber":{"value":"184","range":null},"addressName":"AvenidaManoteras","addressExtension":null,"additionalData":null}],"contactingModes":[{"isPreferred":null,"details":"emailprueba@gmail.com","contactMode":"email"},{"isPreferred":null,"details":"123456789","contactMode":"phone"},{"isPreferred":null,"details":"123456789","contactMode":"fax"}]}]}}';
            System.assertEquals(expectedResult.length(), FS_CHI_Account_Record_Controller.retrieveJSON('RUC', '20552003609').length());
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    
    @isTest private static void getSelectOptionsAccount() {
        /* Test */
        Account accTest = new Account();
        string fldTest = 'CurrencyIsoCode';
        System.Test.startTest();
        
        try{
            List <String> listTest= FS_CHI_Account_Record_Controller.getSelectOptions(accTest,fldTest);
            integer sizeListTest = listTest.size();
            System.assert(sizeListTest>0);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
}