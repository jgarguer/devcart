/**
* Avanxo Colombia
* @author 			OJCB
* Project:			Telefonica
* Description:		Clase tipo WebServiceMock del servicio ws_wwwXappsTialComCoWebservicesSolicitud.
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	1.0		2015-08-19		OJCB      				Definicion de la clase implments WebServiceMock
*************************************************************************************************************/
global class ws_wwwXappsTialComCoWebservicesSol1_mws implements WebServiceMock
{
	global Map<String, Object> mapWSResponseBySOAPAction									= new Map<String, Object>();

    global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){
		
		//cierre
		ws_wwwXappsTialComCoWebservicesSolicitud.respTG_element wsArrayOfrespTG_element			= new ws_wwwXappsTialComCoWebservicesSolicitud.respTG_element();
		
		wsArrayOfrespTG_element.success=1;
        wsArrayOfrespTG_element.id='id';
        wsArrayOfrespTG_element.msg='msg';
        wsArrayOfrespTG_element.error=0;
        wsArrayOfrespTG_element.fun='fun';
			
		//cierre
		//archivo
		ws_wwwXappsTialComCoWebservicesSolicitud.respARCH_element[] lstWSArrayOfrespARCH_element = new List<ws_wwwXappsTialComCoWebservicesSolicitud.respARCH_element>();
		ws_wwwXappsTialComCoWebservicesSolicitud.respARCH_element wsArrayOfrespARCH_element =new ws_wwwXappsTialComCoWebservicesSolicitud.respARCH_element();
		wsArrayOfrespARCH_element.success=1;
       	wsArrayOfrespARCH_element.id='id';
        wsArrayOfrespARCH_element.msg='Msg';
        wsArrayOfrespARCH_element.error=0;
        wsArrayOfrespARCH_element.fun='fun';
        wsArrayOfrespARCH_element.nreg=1;
        wsArrayOfrespARCH_element.ncol=1;
        wsArrayOfrespARCH_element.data =new ws_wwwXappsTialComCoWebservicesSolicitud.data_element();
        wsArrayOfrespARCH_element.data.url =new List<string>{'url1','url2'};
        wsArrayOfrespARCH_element.data.reg =new List<ws_wwwXappsTialComCoWebservicesSolicitud.reg_element>();
        ws_wwwXappsTialComCoWebservicesSolicitud.reg_element objelement = new ws_wwwXappsTialComCoWebservicesSolicitud.reg_element();
        objelement.id='id';
        objelement.nrodoc='nrodoc';
        objelement.nuip='nuip';
        objelement.nombre='nombre';
        wsArrayOfrespARCH_element.data.reg.add(objelement); 
        lstWSArrayOfrespARCH_element.add(wsArrayOfrespARCH_element);
		//archivo
		//solicitud
		ws_wwwXappsTialComCoWebservicesSolicitud.respSOL_element wsArrayOfrespSOL_element =new ws_wwwXappsTialComCoWebservicesSolicitud.respSOL_element();
		wsArrayOfrespSOL_element.success=1;
        wsArrayOfrespSOL_element.id='id';
        wsArrayOfrespSOL_element.msg='msg';
        wsArrayOfrespSOL_element.error=0;
        wsArrayOfrespSOL_element.fun='fun';
        wsArrayOfrespSOL_element.nreg=1;
        wsArrayOfrespSOL_element.ncol=1;
        wsArrayOfrespSOL_element.data =new ws_wwwXappsTialComCoWebservicesSolicitud.data_element();
         wsArrayOfrespSOL_element.data.url =new List<string>{'url1','url2'};
        wsArrayOfrespSOL_element.data.reg =new List<ws_wwwXappsTialComCoWebservicesSolicitud.reg_element>();
        ws_wwwXappsTialComCoWebservicesSolicitud.reg_element objelement1 = new ws_wwwXappsTialComCoWebservicesSolicitud.reg_element();
        
        objelement1.id='id';
        objelement1.nrodoc='nrodoc';
        objelement1.nuip='nuip';
        objelement1.nombre='nombre';
        wsArrayOfrespSOL_element.data.reg.add(objelement1);
		//solicitud
		
		mapWSResponseBySOAPAction.put( 'inpId', wsArrayOfrespTG_element );
		mapWSResponseBySOAPAction.put( 'parArch', wsArrayOfrespARCH_element );
		mapWSResponseBySOAPAction.put( 'parSol', wsArrayOfrespSOL_element );
		mapWSResponseBySOAPAction.put( 'parLog', wsArrayOfrespTG_element );
		//mapWSResponseBySOAPAction.put( 'http://COCorp.Model/2011/COCorp/ContratoNovedades/novedadEmpresa', wsArrayOfrespTG_element );
		
		System.debug('\n\n#### soapAction:'+soapAction);
		
		response.put( 'response_x', mapWSResponseBySOAPAction.get( requestName ) );
	}
}