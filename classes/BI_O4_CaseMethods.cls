public without sharing class BI_O4_CaseMethods {

	    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Methods executed by BI_O4 Case Triggers
    Test Class:    BI_O4_CaseMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera          Initial version
    20/10/2016              Fernando Arteaga        Added fillPreviousOwner method 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    
    static{
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType IN ('Case', 'Opportunity')]){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Method that asign the case to a GSE asociated client
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera          Initial version
    08/11/2016				Alvaro Garcia			MOVED to BI_CaseMethods.setCaseOwner
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   // public static void caseAsignGSE(List<Case>news){

   // 	try{
			//System.debug('News: ' + news);
	  // 		Set<Id> setBiNombreOpp = new Set<Id>();
	  // 		for(Case item : news){
	  // 			if(item.BI_Nombre_de_la_Oportunidad__c != null)
	  // 				setBiNombreOpp.add(item.BI_Nombre_de_la_Oportunidad__c);
	  // 		}

	  // 		Map<Id, Id> mapOppRt = new Map<Id, Id>();
	  // 		Set<Id> setAccId = new Set<Id>();
	  // 		if(!setBiNombreOpp.isEmpty()){
	  // 			List<Opportunity> lstOpp = [SELECT Id, RecordTypeId, AccountId FROM Opportunity WHERE Id IN: setBiNombreOpp];
	  // 			System.debug('Lista opp' + lstOpp);
		 //  		if(!lstOpp.isEmpty()){
		 //  			for(Opportunity opp : lstOpp){
		 //  				mapOppRt.put(opp.Id, opp.RecordTypeId);
		 //  				setAccId.add(opp.AccountId);
		 //  			}
		 //  		}
		 //  	}

	  // 		Map<Id, List<AccountTeamMember>> mapIdAccTeamMember = new Map<Id, List<AccountTeamMember>>();
	  // 		if(!setAccId.isEmpty()){
		 //  		for(AccountTeamMember item : [SELECT Id, UserId, TeamMemberRole, AccountId FROM AccountTeamMember WHERE AccountId IN: setAccId]){
		 //  			System.debug('Account Id: ' + item.AccountId);
		 //  			if(!mapIdAccTeamMember.containsKey(item.AccountId)){
		 //  				mapIdAccTeamMember.put(item.AccountId, new List<AccountTeamMember>{item});
		 //  				System.debug('Entro If');
		 //  			}
		 //  			else{
		 //  				mapIdAccTeamMember.get(item.AccountId).add(item);
		 //  				System.debug('Entro Else');
		 //  			}
		 //  		}
		 //  	}

	  // 		List<AccountTeamMember> lstAccTeamMember = new List<AccountTeamMember>();
	  // 		for(Case caso : news){
	  // 			System.debug('News2: ' + news);
	  // 			if(!mapIdAccTeamMember.isEmpty()){
		 //  			if(MAP_NAME_RT.get('BI_Caso_Interno') == caso.RecordTypeId && caso.BI_Department__c == 'Ingeniería preventa' && caso.BI_Type__c == 'Solicitar Propuesta'){
		 //  				if(caso.BI_Nombre_de_la_Oportunidad__c != null && MAP_NAME_RT.get('BI_Oportunidad_Regional') == mapOppRt.get(caso.BI_Nombre_de_la_Oportunidad__c)){
	  // 						lstAccTeamMember = mapIdAccTeamMember.get(caso.AccountId);
	  // 						System.debug('lista Acc Team Member: ' + lstAccTeamMember);
	  // 						if(!lstAccTeamMember.isEmpty()){
			//					for(AccountTeamMember item : lstAccTeamMember){
			//						System.debug('Team user GSE: ' + item.UserId);
			//						if(item.TeamMemberRole == 'GSE'){
			//							caso.OwnerId = item.UserId;	
			//						}
			//					}
			//				}	
			//			}
			//		}
			//	}
	  // 		}
	  // 		if(BI_TestUtils.isRunningTest()){throw new BI_Exception('test');}
	  // 	}catch(Exception e){

	  // 		BI_LogHelper.generate_BILog('BI_O4_CaseMethods.caseAsignGSE', 'BI_O4', e, 'Trigger');
	  // 	}		
   // }



	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Method that asign the case to a LSE asociated client
    
    History:
     
    <Date>                  <Author>                <Change Description>
    19/08/2016              Miguel Cabrera          Initial version
    08/11/2016				Alvaro Garcia			MOVED to BI_CaseMethods.setCaseOwner
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	//public static void caseAsignLSE(List<Case>news){

	//	try{
	//		System.debug('News: ' + news);
	//   		Set<Id> setBiNombreOpp = new Set<Id>();
	//   		for(Case item : news){
	//   			if(item.BI_Nombre_de_la_Oportunidad__c != null)
	//   				setBiNombreOpp.add(item.BI_Nombre_de_la_Oportunidad__c);
	//   		}

	//   		Map<Id, Id> mapOppRt = new Map<Id, Id>();
	//   		Set<Id> setAccId = new Set<Id>();
	//   		if(!setBiNombreOpp.isEmpty()){
	//   			List<Opportunity> lstOpp = [SELECT Id, RecordTypeId, AccountId FROM Opportunity WHERE Id IN: setBiNombreOpp];
	//   			System.debug('Lista opp' + lstOpp);
	//	   		if(!lstOpp.isEmpty()){
	//	   			for(Opportunity opp : lstOpp){
	//	   				mapOppRt.put(opp.Id, opp.RecordTypeId);
	//	   				setAccId.add(opp.AccountId);
	//	   			}
	//	   		}
	//	   	}

	//   		Map<Id, List<AccountTeamMember>> mapIdAccTeamMember = new Map<Id, List<AccountTeamMember>>();
	//   		if(!setAccId.isEmpty()){
	//	   		for(AccountTeamMember item : [SELECT Id, UserId, TeamMemberRole, AccountId FROM AccountTeamMember WHERE AccountId IN: setAccId]){
	//	   			System.debug('Account Id: ' + item.AccountId);
	//	   			if(!mapIdAccTeamMember.containsKey(item.AccountId)){
	//	   				mapIdAccTeamMember.put(item.AccountId, new List<AccountTeamMember>{item});
	//	   				System.debug('Entro If');
	//	   			}
	//	   			else{
	//	   				mapIdAccTeamMember.get(item.AccountId).add(item);
	//	   				System.debug('Entro Else');
	//	   			}
	//	   		}
	//	   	}

	//   		List<AccountTeamMember> lstAccTeamMember = new List<AccountTeamMember>();
	//   		for(Case caso : news){
	//   			System.debug('News2: ' + news);
	//   			if(!mapIdAccTeamMember.isEmpty()){
	//	   			if(MAP_NAME_RT.get('BI_Caso_Interno') == caso.RecordTypeId && caso.BI_Department__c == 'Ingeniería preventa' && caso.BI_Type__c == 'Solicitar Cotización'){
	//	   				if(caso.BI_Nombre_de_la_Oportunidad__c != null && MAP_NAME_RT.get('BI_Ciclo_completo') == mapOppRt.get(caso.BI_Nombre_de_la_Oportunidad__c)){
	//   						lstAccTeamMember = mapIdAccTeamMember.get(caso.AccountId);
	//   						System.debug('lista Acc Team Member: ' + lstAccTeamMember);
	//   						if(!lstAccTeamMember.isEmpty()){
	//							for(AccountTeamMember item : lstAccTeamMember){
	//								System.debug('Team user LSE: ' + item.UserId);
	//								if(item.TeamMemberRole == 'LSE'){
	//									caso.OwnerId = item.UserId;	
	//								}
	//							}
	//						}	
	//					}
	//				}
	//			}
	//   		}
	//   		if(BI_TestUtils.isRunningTest()){throw new BI_Exception('test');}
	//   	}catch(Exception e){

	//   		BI_LogHelper.generate_BILog('BI_O4_CaseMethods.caseAsignLSE', 'BI_O4', e, 'Trigger');
	//   	}
	//}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Method that does not allow to close the case if you have associated cases open
    
    History:
     
    <Date>                  <Author>                <Change Description>
    19/08/2016              Miguel Cabrera          Initial version
	21/06/2017				Oscar Bartolo			Exclude case with type 'Genérico Preventa' and close as rejected
	26/09/2017				Jaime Regidor 			Exclude case with BI_Type__c = 'Gestión de Cancelación'
	28/09/2017				Oscar Bartolo			Add status Pendiente Aceptación Cliente
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void preventCloseChildCases(Map<Id, Case> newMap, Map<Id, Case> oldMap){

    	try{
	    	Set<Id> lstIdParent = new Set<Id>();
	    	for(Case item : newMap.values()){
	    		System.debug(loggingLevel.Error, 'Id Padre: ' + item.Id);
	    		System.debug(loggingLevel.Error,'New Status: ' + item.Status);
	    		System.debug(LoggingLevel.Error, oldMap);
	    		System.debug(loggingLevel.Error,'Old Status: ' + oldMap.get(item.Id).Status);
                //OBP-21/06/2017
                if (!(item.Tipo_registro_filtro__c.equalsIgnoreCase('Genérico Preventa') && (item.Status == 'Rejected'  || item.Status == 'Pendiente Aceptación Cliente')) && item.BI_Type__c != 'Gestión de Cancelación' ) {
                    if((item.Status == 'Closed'|| item.Status == 'Cerrado' || item.Status == 'Cancelado' || item.Status == 'Cancelled' || item.Status == 'Finalizado' || item.Status == 'Resolved' || item.Status == 'Rejected' || item.Status == 'New demand' || item.Status == 'Returned' || item.Status == 'Aceptada' || item.Status == 'Pendiente Aceptación Cliente') && (oldMap.get(item.Id).Status != 'Closed'|| oldMap.get(item.Id).Status != 'Cerrado' || oldMap.get(item.Id).Status != 'Cancelado' || oldMap.get(item.Id).Status != 'Cancelled' || oldMap.get(item.Id).Status != 'Finalizado' || oldMap.get(item.Id).Status != 'Resolved' || oldMap.get(item.Id).Status != 'Rejected' || oldMap.get(item.Id).Status != 'New demand' || oldMap.get(item.Id).Status != 'Returned' || oldMap.get(item.Id).Status != 'Aceptada' || oldMap.get(item.Id).Status != 'Pendiente Aceptación Cliente')){
                        lstIdParent.add(item.Id);
                    }
                }
	    	}
	    	if(!lstIdParent.isEmpty()){
	    	List<AggregateResult> lstAgr = [Select ParentId, COUNT(Id) Abiertos From Case Where ParentId IN: lstIdParent And Status not in ('Cancelado', 'Cancelled', 'Cerrado', 'Closed', 'Finalizado', 'Resolved', 'Rejected', 'New demand', 'Returned', 'Aceptada', 'Pendiente Aceptación Cliente')  Group by ParentId];
	    	Map<Id, Integer> mapAbiertos = new Map<Id, Integer>();
			if(!lstAgr.isEmpty()){
				for(AggregateResult aggres : lstAgr) {
					mapAbiertos.put((Id)aggres.get('ParentId'), (Integer)aggres.get('Abiertos'));
				}
			}
	    	
	    	//if(!lstIdParent.isEmpty()){ JRM 06/04/2018 comentado 
		    	for(Id idAbierto : lstIdParent){
					if(mapAbiertos.containsKey(idAbierto)){
						if(mapAbiertos.get(idAbierto) > 0){
							newMap.get(idAbierto).Status.addError('Error: No es posible cerrar un caso si el caso asociado esta en un estado abierto');
						}
					}
				}
			}
			
		}catch(Exception e){

			BI_LogHelper.generate_BILog('BI_O4_CaseMethods.preventCloseChildCases', 'BI_O4', e, 'Trigger');
		}	
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Method that when all children assigned cases to DDOs are in state "Cotizado" the parent case goes to "Cotizado" automatically 
    
    History:
     
    <Date>                  <Author>                <Change Description>
    20/09/2016              Miguel Cabrera          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void ddoCotizadosParentCotizado(List<Case>news, List<Case>olds){

    	Set<Id> setCasosPadre = new Set<Id>();
    	Map<Id, List<Case>> mapParentChildren = new Map<Id, List<Case>>();
    	
    	for(Integer i = 0; i < news.size(); i++){
    		if(news[i].ParentId != null && news[i].Status != olds[i].Status && news[i].Status == 'Cotizada'){
    			setCasosPadre.add(news[i].ParentId);
    		}		
    	}

    	if(!setCasosPadre.isEmpty()){
	    	for(Case caso : [SELECT Id, ParentId, Status, BI_O4_DDO__c, RecordType.DeveloperName, parent.RecordTypeId FROM Case WHERE ParentId IN: setCasosPadre]){
	    		if(caso.parent.RecordTypeId != MAP_NAME_RT.get('BI_Caso_Interno')){
	    			if(!mapParentChildren.containsKey(caso.ParentId))
	    				mapParentChildren.put(caso.ParentId, new List<Case>{caso});
	    			else
	    				mapParentChildren.get(caso.ParentId).add(caso);
	    			}
	    	}
	    }

    	Integer totales = 0;
    	List<Case> lstCasoUpdate = new List<Case>();
    	for(Id parentId : mapParentChildren.keySet()){
    		Integer cotizados = 0;
    		for(Case caso : mapParentChildren.get(parentId)){
    			if(caso.Status == 'Cotizada' && caso.BI_O4_DDO__c != null){
    				cotizados++;
    			}
    			totales++;
    		}	
    		if(totales == cotizados){
    			Case caso = new Case();
    			caso.Id = parentId;
    			caso.Status = 'Cotizada';
   				lstCasoUpdate.add(caso);	
    		}
    	}
    	if(!lstCasoUpdate.isEmpty()){
    		update lstCasoUpdate;
    	}    	    	    	
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Fills BI_O4_PriorOwner__c with previous ownerId when case owner changes
    
    History:
     
    <Date>                  <Author>                <Change Description>
    20/10/2016              Fernando Arteaga        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void fillPreviousOwner(List<Case> newCases, List<Case> oldCases)
    {
    	try
    	{
    		for (Integer i=0; i < newCases.size(); i++)
    		{
    			if (newCases[i].RecordTypeId == MAP_NAME_RT.get('BI_Caso_Interno') && newCases[i].OwnerId != oldCases[i].OwnerId)
    			{
    				newCases[i].BI_O4_PriorOwner__c = oldCases[i].OwnerId;
    			}
    		}

    	}
    	catch (Exception exc)
    	{
			BI_LogHelper.generate_BILog('BI_O4_CaseMethods.fillPreviousOwner', 'BI_O4', exc, 'Trigger');
    	}
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Guillermo Muñoz
 	Company:       New Energy Aborda
 	Description:   Assign Cases with 'Legal' Department when the Case Status changes to 'Assigned'
	
 	In:            
 	Out:
 	
 	History:
 	 
 	<Date>              <Author>                   <Change Description>
 	14/03/2017          Guillermo Muñoz        	   Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void assignLegalCase(List <Case> news, Map <Id,Case> oldMap){

    	try{

    		if (BI_TestUtils.isRunningTest()) {
				throw new BI_Exception('test');
    		}

    		List <Case> toProcess = new List <Case>();
	
    		for(Case cas : news){
	
    			if(
    				(cas.BI_Country__c == 'United States' || cas.BI_Country__c == 'Puerto Rico') && cas.BI_Department__c == 'Legal'
    				&& cas.Status == 'Assigned' && cas.Status != oldMap.get(cas.Id).Status
    			){
	
    				toProcess.add(cas);
    			}
    		}
	
    		if(!toProcess.isEmpty()){
	
    			Id queueId = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName = 'BI_O4_Legal' LIMIT 1].Id;
	
    			for(Case cas : toProcess){
	
    				cas.OwnerId = queueId;
    			}
    		}
    	}catch (Exception exc)    	{
			BI_LogHelper.generate_BILog('BI_O4_CaseMethods.assignLegalCase', 'BI_O4', exc, 'Trigger');
    	}
    	
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Oscar Bartolo
Company:       Aborda
Description:   Fill the field Gen_rico_Preventa_ID and the Responsable_Preventa_Global

History: 

<Date>                          <Author>                    <Change Description>
09/06/2017                      Oscar Bartolo               Initial version
26/06/2017						Oscar Bartolo				Add the field BI_O4_Email_Responsable_Preventa_Global__c
13/11/2017						Javier Almirón García		Coment the DML insertion 
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void fillGenericoPreventaId(List <Case> news){
    
        try{
    
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }
    
            Set <Case> toProcess = new Set <Case>();
            Map <Case, Id> map_casParent = new Map <Case, Id>();
    		List <Case> toUpdate = new List <Case>();
            
            for (Case cas : news) {
                if (cas.Tipo_registro_filtro__c.equalsIgnoreCase('Genérico Preventa')) {
                    if (cas.ParentId != null) {
                        map_casParent.put(cas, cas.ParentId);
                    } else {
                        toProcess.add(cas);
                    }
                } 
            }
    		System.debug('GEN PREV ID ' + map_casParent);
    		System.debug('GEN PREV ID 2 ' + toProcess);
    		List <Case> lst_casParent = [SELECT Id, Tipo_registro_filtro__c, BI_O4_Gen_rico_Preventa_ID__c, BI_O4_Responsable_Preventa_Global__c, BI_O4_Email_Responsable_Preventa_Global__c, Subject FROM Case WHERE Id IN :map_casParent.values()];
            for (Case caseChild : map_casParent.keySet()) {
                for (Case casParent : lst_casParent) {
                    if (caseChild.ParentId == casParent.Id) {
                        if (casParent.Tipo_registro_filtro__c.equalsIgnoreCase('Genérico Preventa')) {
                            caseChild.BI_O4_Gen_rico_Preventa_ID__c = casParent.BI_O4_Gen_rico_Preventa_ID__c;
                            caseChild.Subject = casParent.Subject; /* Álvaro López 04/05/2018 - eHelp 03631168*/
                            caseChild.BI_O4_Responsable_Preventa_Global__c = casParent.BI_O4_Responsable_Preventa_Global__c;
                            caseChild.BI_O4_Email_Responsable_Preventa_Global__c = casParent.BI_O4_Email_Responsable_Preventa_Global__c;
                            toUpdate.add(caseChild);
                            System.debug('ASUNTO HIJO: ' + caseChild.Subject + ' - ' + casParent.Subject);
                        } else {
                            toProcess.add(caseChild);
                        }
                    }
                }
            }
    
            for (Case casProcess : toProcess) {
                casProcess.BI_O4_Gen_rico_Preventa_ID__c = 'A-' + casProcess.CreatedBy.BI_O4_Presales_Unit__c + '-' + casProcess.Account.TGS_Account_Mnemonic__c + '-GEN-';
            	casProcess.Subject = 'A-' + casProcess.CreatedBy.BI_O4_Presales_Unit__c + '-' + casProcess.Account.TGS_Account_Mnemonic__c + '-GEN-';   
                casProcess.BI_O4_Responsable_Preventa_Global__c = UserInfo.getUserId();
                casProcess.BI_O4_Email_Responsable_Preventa_Global__c = UserInfo.getUserEmail();
                toUpdate.add(casProcess);
            }
    		
            //if (!toUpdate.isEmpty()) {
            //    update toUpdate;
            //}
    
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_O4_CaseMethods.fillGenericoPreventaId', 'BI_EN', Exc, 'Trigger');
        }
    }

    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Oscar Bartolo
Company:       Aborda
Description:   Fill the field BI_O4_Fecha_presentaci_n_oferta__c in the child

History: 

<Date>                          <Author>                    <Change Description>
30/05/2017                      Oscar Bartolo               Initial version
02/18/2017						Javier Almirón				Resume 2 queries in 1 query
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updatePresentacionDate(List <Case> news){
    
        try{

            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }

            System.debug ('%$% Cehckpoint 1');
    
            Map <Id, Case> toProcess = new Map <Id, Case>();
            Map <Id, Case> map_casParent = new Map <Id, Case>();
            Set <Id> newSet = new Set <Id>();
            Set <Id> parentNewSet = new Set <Id>();
    
            for (Case cas : news) {
                if (cas.Tipo_registro_filtro__c.equalsIgnoreCase('Genérico Preventa')) {
                    if (cas.ParentId != null) {
                        map_casParent.put(cas.ParentId, cas);
                        newSet.add(cas.Id);
                        parentNewSet.add(cas.ParentId);

                    } else {
                        toProcess.put(cas.Id, cas);
                        newSet.add(cas.Id);
                        parentNewSet.add(cas.ParentId);
                    }
                } 
            }

            System.debug ('%$% Cehckpoint 2');
    
            List <Case> lst_casParent = [SELECT Id, Tipo_registro_filtro__c, ParentId, BI_O4_Fecha_presentaci_n_oferta__c
            							 FROM Case WHERE (Id IN :parentNewSet
            							 			OR parentId IN: newSet)];
    
    	    List <Case> lst_casChild = new List <Case>();
            for (Case casParent : lst_casParent) {
                if (casParent.Tipo_registro_filtro__c.equalsIgnoreCase('Caso Interno')) {
                    
                    toProcess.put(map_casParent.get(casParent.Id).Id, map_casParent.get(casParent.Id));

                }
            }

            for (Case ca: toProcess.values()){
            	for (Case c: lst_casParent){
            		if (c.ParentId == ca.Id){
            			lst_casChild.add(c);
            		}
            	}

            }
  
    
            List <Case> toUpdateChild = new List<Case>();
            for (Case casChild : lst_casChild) {
                Case casParent = toProcess.get(casChild.ParentId);
                casChild.BI_O4_Fecha_presentaci_n_oferta__c = casParent.BI_O4_Fecha_presentaci_n_oferta__c;
                toUpdateChild.add(casChild);
            }
    
            if (!toUpdateChild.isEmpty()) {
                update toUpdateChild;
            }
    
    
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_O4_CaseMethods.updatePresentacionDate', 'BI_EN', Exc, 'Trigger');
        }
	}
    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Bartolo
    Company:       Aborda
    Description:   Close child Case when the parent is close
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    21/06/2017                      Oscar Bartolo               Initial version
    29/09/2017						Oscar Bartolo				Add status Pendiente Aceptación Cliente
    05/10/2015						Guillermo Muñoz				Deprecated BI_TestUtils.isRunningTest to avoid test errors
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void closeChildCase(List<Case> news, List<Case> olds){
		
		/*if(BI_TestUtils.isRunningTest()){
	        throw new BI_Exception('Test');
	    }*/

	    Map<id, Case> map_idParentRejected = new Map<Id, Case>();
	    Map<id, Case> map_idParentClient = new Map<Id, Case>();
	    Map<id, Case> map_idParent = new Map<Id, Case>();
	    for(Integer iter = 0; iter < news.size(); iter++) {
	    	Case cas = news[iter];
	    	if (cas.Tipo_registro_filtro__c == 'Genérico Preventa' && cas.Status == 'Rejected'&& olds[iter].Status != 'Rejected') {
	    		map_idParentRejected.put(cas.Id, cas);
	    		map_idParent.put(cas.Id, cas);
	    	}
	    	if (cas.Tipo_registro_filtro__c == 'Genérico Preventa' && cas.Status == 'Pendiente Aceptación Cliente' && olds[iter].Status != 'Pendiente Aceptación Cliente') {
	    		map_idParentClient.put(cas.Id, cas);
	    		map_idParent.put(cas.Id, cas);
	    	}
	    }
        
        List <Case> lst_casChild = new List <Case>();
        if (!map_idParentRejected.isEmpty()) {
        	List <Case> lst_casChildRejected = [SELECT Id, ParentId FROM Case WHERE ParentId IN : map_idParentRejected.keySet()];
        	lst_casChild.addAll(lst_casChildRejected);
		}
        if (!map_idParentClient.isEmpty()) {
        	List <Case> lst_casChildClient = [SELECT Id, ParentId FROM Case WHERE ParentId IN : map_idParentClient.keySet() And Status not in ('Rejected', 'Vencido', 'Aceptada', 'Pendiente Aceptación Cliente')];
        	lst_casChild.addAll(lst_casChildClient);
        }

        List <Case> toUpdate = new List <Case>();

        for (Case casChild :  lst_casChild) {
            for (Case casParent : map_idParent.values()) {
                if (casChild.ParentId == casParent.Id) {
                    casChild.Status = casParent.Status;
                    //Línea comentada ya que no se requiere que dicho campo se pase de la padre a la hija cuando un caso padre se cierra - Javier Almirón García - 31/10/2017
                    //casChild.BI_O4_Closed_Type__c = casParent.BI_O4_Closed_Type__c;
                    casChild.TGS_Closed_Date__c = casParent.TGS_Closed_Date__c;
                    casChild.BI_Solucion_del_caso__c = casParent.BI_Solucion_del_caso__c;

                    toUpdate.add(casChild);
                }
            }
        }

        if (!toUpdate.isEmpty()) {
            Map<Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(Userinfo.getUserId(), true, false, false, false);
            try{
                BI_GlobalVariables.stopTriggereHelpCase=false;
                update toUpdate;
                BI_GlobalVariables.stopTriggereHelpCase=false;
            }catch(Exception e){
            	BI_LogHelper.generate_BILog('BI_O4_CaseMethods.closeChildCase', 'BI_O4', e, 'Trigger');
            }

            finally{
                if(mapa != null){
                    BI_MigrationHelper.disableBypass(mapa);
                }
            }
        }
        
	}



	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Bartolo
    Company:       Aborda
    Description:   Create Case Team Member
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    30/05/2017                      Oscar Bartolo               Initial version
    26/10/2017                      Alfonso Alvarez             Basar la comprobación 'Genérico Preventa' en el campo RecordTypeId en vez de en el campo fórmula BI
--------------------------------------------------------------------------------------------------------------------------------------------------------*/


	public final static Map<String, Id> MAP_NAME_CTM = new Map<String, Id>();
	static {
	    for (CaseTeamRole casTM : [SELECT Id, Name FROM CaseTeamRole]) {
	        MAP_NAME_CTM.put(casTM.Name, casTM.Id);
	    }
	}
	
	
	public static void createCaseTeamMember(List <Case> news){
	
	    try{
	
	        if(BI_TestUtils.isRunningTest()){
	            throw new BI_Exception('Test');
	        }
	
	        List<CaseTeamMember> toInsert = new List<CaseTeamMember>();
	
	        for (Case cas : news) {
		    // 26-10-2017 Inicio AJAE
            	    if (cas.RecordTypeId == MAP_NAME_RT.get('BI_O4_Gen_rico_Preventa')) {    
	            //if (cas.Tipo_registro_filtro__c.equalsIgnoreCase('Genérico Preventa')) {
	            // 26-10-2017 Fin AJAE
	                CaseTeamMember casTM = new CaseTeamMember();
	
	                casTM.MemberId = UserInfo.getUserId();
	                casTM.ParentId = cas.Id;
	                casTm.TeamRoleId = MAP_NAME_CTM.get('Propietario | Owner Creator');
	                toInsert.add(casTM);
	            } 
	        }
	
	        if (!toInsert.isEmpty()) {
	            insert toInsert;
	        }
	
	
	    }catch(Exception exc){
	        BI_LogHelper.generate_BILog('BI_O4_CaseMethods.createCaseTeamMember', 'BI_EN', Exc, 'Trigger');
	    }
	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Bartolo
    Company:       Aborda
    Description:   Update the field Role_de_Usuario_Creador_del_Caso__c
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    30/05/2017                      Oscar Bartolo               Initial version
    13/11/2017						Javier Almirón García		Coment the DML insertion 
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	
	public final static Map<Id, String> MAP_ROLE_NAME = new Map<Id, String>();
	static {
	    for (UserRole uRole : [SELECT Id, Name FROM UserRole]) {
	        MAP_ROLE_NAME.put(uRole.Id, uRole.Name);
	    }
	}
	
	
	
	public static void updateCreatorRole(List <Case> news){
	
	    try{
	
	        if(BI_TestUtils.isRunningTest()){
	            throw new BI_Exception('Test');
	        }
	
	        List<Case> toUpdate = new List<Case>();
	
	        for (Case cas : news) {
	            if (cas.Tipo_registro_filtro__c.equalsIgnoreCase('Genérico Preventa')) {
	                cas.BI_O4_Role_de_Usuario_Creador_del_Caso__c = MAP_ROLE_NAME.get(UserInfo.getUserRoleId());
	                toUpdate.add(cas);
	            } 
	        }
	
	        //if (!toUpdate.isEmpty()) {
	        //    insert toUpdate;
	        //}
	
	
	    }catch(Exception exc){
	        BI_LogHelper.generate_BILog('BI_O4_CaseMethods.updateCreatorRole', 'BI_EN', Exc, 'Trigger');
	    }
	}


/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Bartolo
    Company:       Aborda
    Description:   Update the field BI_O4_Role_de_Usuario_Asignado_al_Caso__c
    
    History: 
    
    <Date>                      <Author>                <Change Description>
    30/05/2017                  Oscar Bartolo           Initial version
    11/10/2017			Oscar Bartolo		Fix empty list
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void updateAssignUserRole(List <Case> news, List <Case> olds){
	
	    try{
	
	        if(BI_TestUtils.isRunningTest()){
	            throw new BI_Exception('Test');
	        }
	
	        List<Case> toUpdate = new List<Case>();
	        Map<Case, Id> map_cas_Assignee = new Map<Case, Id>();
	
	        for(Integer iter = 0; iter < news.size(); iter++) {
	            Case cas = news[iter];
	            Case casOld = olds[iter];
	
	            if (cas.Tipo_registro_filtro__c.equalsIgnoreCase('Genérico Preventa') && (casOld.TGS_Assignee__c != cas.TGS_Assignee__c)) {
	                if (cas.TGS_Assignee__c == null) {
	                    cas.BI_O4_Role_de_Usuario_Asignado_al_Caso__c = '';
	                    toUpdate.add(cas);
	                } else {
	                    map_cas_Assignee.put(cas, cas.TGS_Assignee__c);
	                }
	            } 
	        }
	
	        List <User> lst_userRole = new List<User>();
	        if (!map_cas_Assignee.isEmpty()) {
	            lst_userRole = [SELECT Id, UserRoleId FROM User WHERE Id IN :map_cas_Assignee.values()];
	        }
	
	        for (User userAssignee : lst_userRole) {
	            for (Case cas : map_cas_Assignee.KeySet()) {
	                cas.BI_O4_Role_de_Usuario_Asignado_al_Caso__c = MAP_ROLE_NAME.get(userAssignee.UserRoleId);
	                toUpdate.add(cas);
	            }
	        }
	
	        if (!toUpdate.isEmpty()) {
	            insert toUpdate;
	        }
	
	
	    }catch(Exception exc){
	        BI_LogHelper.generate_BILog('BI_O4_CaseMethods.updateAssignUserRole', 'BI_EN', Exc, 'Trigger');
	    }
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Oscar Bartolo
        Company:       Aborda
        Description:   Update the field BI_O4_Email_Responsable_Preventa_Global__c when BI_O4_Responsable_Preventa_Global__c changes
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        01/09/2017                      Oscar Bartolo               Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static void updateEmailResponsablePreventa(List <Case> news, List <Case> olds){
    
        try{
    
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }
    
            List <Case> toUpdate = new List <Case>();
            Map <Case, Id> map_cas_IdUser = new Map <Case, Id>();
    
            for(Integer iter = 0; iter < news.size(); iter++) {
                Case cas = news[iter];
                Case casOld = olds[iter];
    
                if (casOld.BI_O4_Responsable_Preventa_Global__c != cas.BI_O4_Responsable_Preventa_Global__c) {
                	map_cas_IdUser.put(cas, cas.BI_O4_Responsable_Preventa_Global__c);
                } 
            }
    
            if (!map_cas_IdUser.isEmpty()) {
    
                List <User> lst_userEmail = [SELECT Id, Email FROM User WHERE Id IN :map_cas_IdUser.values()];
    
                for (Case cas : map_cas_IdUser.keySet()) {
                    for (User user : lst_userEmail) {
                    	if (user.id == map_cas_IdUser.get(cas)){
                            cas.BI_O4_Email_Responsable_Preventa_Global__c = user.Email;
                            toUpdate.add(cas);
                        }
                    }
                }
    
                if (!toUpdate.isEmpty()) {
                    update toUpdate;
                }
            }
    
    
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_O4_CaseMethods.updateEmailResponsablePreventa', 'BI_EN', Exc, 'Trigger');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Oscar Bartolo
        Company:       Aborda
        Description:   Fill the field BI_O4_Cliente_Interno_Nombre__c
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        20/09/2017                      Oscar Bartolo               Initial version
        02/11/2017						Javier Almirón García		Fix version - eHelp 02247249  
        24/04/2018						Iñaki Frial Selas 			Add RecordType Disconnect or Cancel
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void fillClienteInternoNombre(List <Case> news){

        try{

            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }

            Set<Id> setIdCase = new Set<Id>();
            List<Case> caseList = new List<Case>();
            List<Case> caseUpdateList = new List<Case>();

            if (news != null){

            	for (Case cas: news){

            		if (cas.Tipo_registro_filtro__c.equalsIgnoreCase('Genérico Preventa')||cas.Tipo_registro_filtro__c.equalsIgnoreCase('Disconnect or Cancel') ){
            			setIdCase.add(cas.Id);
            		}
            	}


            	if(!setIdCase.isEmpty()){
            		caseList = [SELECT Id, ParentId, BI_O4_Cliente_Interno_Nombre__c, CreatedBy.BI_O4_Presales_Unit__c, Parent.Tipo_registro_filtro__c, Parent.CreatedBy.BI_O4_Presales_Unit__c  
            							FROM Case where Id IN : setIdCase];
            	}


            	if (!caseList.isEmpty()){
            		for (Case ca: caseList){
            			if ( (ca.ParentId == null) || ca.Parent.Tipo_registro_filtro__c.equalsIgnoreCase('Caso Interno') ){
            				ca.BI_O4_Cliente_Interno_Nombre__c = ca.CreatedBy.BI_O4_Presales_Unit__c;
            				caseUpdateList.add(ca);
            			}

            			else if(ca.Parent.Tipo_registro_filtro__c.equalsIgnoreCase('Genérico Preventa')){

            				ca.BI_O4_Cliente_Interno_Nombre__c = ca.Parent.CreatedBy.BI_O4_Presales_Unit__c;
            				caseUpdateList.add(ca);
            			}
            			else if(ca.Parent.Tipo_registro_filtro__c.equalsIgnoreCase('Disconnect or Cancel')){

            				ca.BI_O4_Cliente_Interno_Nombre__c = ca.CreatedBy.BI_O4_Presales_Unit__c;
            				caseUpdateList.add(ca);
            			}
            		}
            	}

            	if (!caseUpdateList.isempty()){
            		update caseUpdateList;
            	}
            }

        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_O4_CaseMethods.fillClienteInternoNombre', 'BI_EN', Exc, 'Trigger');
        }
    }
}