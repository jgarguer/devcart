/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Clase de prueba: FS_CORE_Fullstack_ATM

History:
<Date>							<Author>						<Change Description>
3/05/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class FS_CORE_Fullstack_ATM_Test {
    
    @testSetup 
    private static void init() {
        /* Enable Bypass */
        BI_bypass__c bypass = new BI_bypass__c();
        bypass.BI_migration__c = true;
        bypass.BI_skip_trigger__c = true;
        bypass.BI_stop_job__c = true;
        bypass.BI_skip_assert__c = true;
        bypass.BI_Skip_case_assignations__c = true;
        
        insert bypass;
        
        /* Custom Settings */
        FS_CORE_Integracion__c ORGConfiguration = new FS_CORE_Integracion__c();
        ORGConfiguration.FS_CORE_Segmentos_Cliente__c = 'Top 1 Top 2 Top 3 Top 4 Platino';
        ORGConfiguration.FS_CORE_AccountTeamMember__c = 'Ejecutivo de Cobros Gestor de Reclamos';
        ORGConfiguration.FS_CORE_Autorizaciones_Contacto__c = 'Administrativo Cobranza Facturación General PostVenta Técnico';
        ORGConfiguration.FS_CORE_Peru__c = true;
        ORGConfiguration.FS_CORE_Chile__c = false;
        ORGConfiguration.FS_CORE_SrvName_AddContactToOrganization__c = 'Salesforce.AddContactToOrganization';
        ORGConfiguration.FS_CORE_SrvName_AddUserToOrganization__c = 'Salesforce.AddUserToOrganization';
        ORGConfiguration.FS_CORE_SrvName_CreateContact__c ='Salesforce.CreateContact';
        ORGConfiguration.FS_CORE_SrvName_CreateCustomer__c ='Salesforce.CreateCustomer';
        ORGConfiguration.FS_CORE_SrvName_GetCustomerList__c ='Salesforce.GetCustomerList';
        ORGConfiguration.FS_CORE_SrvName_RemoveContactFromOrg__c ='Salesforce.RemoveContactFromOrg';
        ORGConfiguration.FS_CORE_SrvName_RemoveUserOrganization__c = 'Salesforce.RemoveUserOrganization';
        ORGConfiguration.FS_CORE_SrvName_UpdateCustomer__c = 'Salesforce.UpdateCustomer';
        ORGConfiguration.FS_CORE_VersionCabecera__c = '1.0';
        
        insert ORGConfiguration;
        
        /* Users */
        User user = new User (alias = 'user', LastName = 'lastname', Email = 'email@testorg.com', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',
                              Localesidkey='en_US', ProfileId = [SELECT Id FROM Profile WHERE Name =: Label.BI_System_Administrator_ProfileName][0].Id, BI_Permisos__c = 'Administrador del sistema', Timezonesidkey = 'GMT',
                              UserName= 'alias@testorg.com', BI_CodigoUsuario__c = '00000000', IsActive = true);
        
        insert user;
        
        /* Account */
        Account account = new Account(Name = 'Test [Integración] - Perú', Description = 'null', BI_Activo__c = 'Sí',
                                      BI_Segment__c = 'Empresas', BI_Subsegment_Local__c = 'Top 1', BI_Riesgo__c = null,
                                      BI_Denominacion_comercial__c = 'Test [Integración] - Perú [Denominación comercial]', BI_Sector__c = 'Comercio', TGS_Es_MNC__c = false,
                                      BI_Ambito__c = 'Público', BI_Subsector__c = 'Droguerías', Website = 'www.cliente.pe',
                                      TGS_Fecha_Activacion__c = Date.today(), TGS_Deactivation_Date__c = Date.today(), FS_CHI_Tipo_Credito__c = 'Otras',
                                      FS_CHI_CREDIT_CLASS__c = 'PLUS', FS_CHI_Credit_Score__c = 0, FS_CHI_Limite_de_consumo__c = 0,
                                      FS_CHI_Limite_de_credito__c = 0, FS_CHI_Limite_credito_portabilidad__c = 0, FS_CHI_Fecha_de_ultima_ev__c = Date.today(),
                                      BI_Country__c = 'Peru', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20552003609',
                                      Phone = '6895623147', Fax = '123456789', FS_CORE_Account_Massive__c = false,
                                      FS_CORE_ATM_Massive__c = false, RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity' LIMIT 1][0].Id, BI2_asesor__c = UserInfo.getUserId(),
                                      BI2_service_manager__c = UserInfo.getUserId(), FS_CORE_ID_Cliente_legado__c = '000000000X', FS_CORE_Estado_Sincronizacion__c = 'Sincronizado');
        
        insert account;
        
        /* Opportunity */
        Opportunity opportunity = new Opportunity(Name = 'Test [Integración] - Perú', AccountId = account.Id, BI_Country__c = 'Peru',
                                                  BI_SIMP_Opportunity_Type__c = 'Alta', BI_Licitacion__c = 'No', BI_Opportunity_Type__c = 'Fijo',
                                                  CloseDate = Date.today(), BI_Fecha_de_cierre_real__c = Date.today(), StageName = 'F1 - Closed Won',
                                                  BI_Probabilidad_de_exito__c = '100', BI_Duracion_del_contrato_Meses__c = 10, BI_Plazo_estimado_de_provision_dias__c = 10,
                                                  BI_Fecha_de_entrega_de_la_oferta__c = Date.today(), BI_Fecha_de_vigencia__c = Date.today(), BI_Fecha_vigencia_factibilidad_tecnica__c = Date.today(),
                                                  RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Ciclo_completo' LIMIT 1][0].Id);
        
        insert opportunity;
        
        /* AccountTeamMembers */
        AccountTeamMember accountTeamMember = new AccountTeamMember(AccountAccessLevel = 'Edit', AccountId = account.Id, CaseAccessLevel = 'Edit', ContactAccessLevel = 'Edit', OpportunityAccessLevel = 'Edit', TeamMemberRole = 'Gestor de Reclamos', userId = UserInfo.getUserId());
        
        insert accountTeamMember;
    }
    
    @isTest private static void process() {
        /* Variable Definition */
        Account account = [SELECT Id, OwnerId, BI2_asesor__c, BI2_service_manager__c FROM Account LIMIT 1][0];
        
        /* Test */
        System.Test.startTest();
        
        /* Standard Records -> Custom Records */
        FS_CORE_Fullstack_ATM.process(account);
        
        /* Standard Records >-< Custom Records */
        FS_CORE_Fullstack_ATM.process(account);

        /* Account Team Members */
        /* Update */
        AccountTeamMember standard = [SELECT Id, TeamMemberRole FROM AccountTeamMember LIMIT 1][0];
        standard.TeamMemberRole = 'Ejecutivo de Cobros';
        account.BI2_asesor__c = [SELECT Id FROM User WHERE alias = 'user'][0].Id;
        account.BI2_service_manager__c = [SELECT Id FROM User WHERE alias = 'user'][0].Id;
        update standard;
        update account;
        
        FS_CORE_Fullstack_ATM.process(account);
        
        /* Delete */
        List<FS_CORE_TeamMemberToSincronize__c> custom = [SELECT FS_CORE_TipoRegistro__c FROM FS_CORE_TeamMemberToSincronize__c];
        For(FS_CORE_TeamMemberToSincronize__c item : custom){item.FS_CORE_TipoRegistro__c = 'Sincronizado';}
        update custom;
        
        delete [SELECT Id FROM AccountTeamMember LIMIT 1][0];
        account.BI2_asesor__c = null;
        account.BI2_service_manager__c = null;
        update account;
        
        FS_CORE_Fullstack_ATM.process(account);

        System.Test.stopTest();
    }
}