@isTest
private class ADQ_TEST_FACTURACION {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        
    History:
    
    <Date>            <Author>              <Description>
    ??/??/????        ??                   Initial version
    28/12/2016        Adrián Caro          Validation rule error solved
    13/06/2017        Guillermo Muñoz      Deprecated references to Raz_n_social__c (Optimización)
    29/08/2017        Julio Ortega         Facturas Digitales y Fijas
    25/09/2017        Jaime Regidor        Fields BI_Subsector__c and BI_Sector__c added
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    @isTest static void testOpcionesFacturaController() {
        //JG Migracion UATFIX
        
        // Implement test code
        List<Account> listaCuentas = new List<Account>();
        List<Account> listaCuentasNext = new List<Account>();
        
        Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
        Account acc01  = new Account ();
                acc01.Name = 'Cuenta de prueba JMO';
				acc01.BI_Subsegment_Regional__c = 'test';
				acc01.BI_Territory__c = 'test';
                //acc01.Raz_n_social__c = 'Cuenta de prueba SA CV';
                acc01.BI_Country__c = Label.BI_Mexico;
                acc01.BI_Segment__c = 'Empresas';
                acc01.BI_Subsector__c = 'test'; //25/09/2017
                acc01.BI_Sector__c = 'test'; //25/09/2017
                acc01.RecordTypeId = rtId;
            insert acc01;
            
            listaCuentas.add (acc01);
      
            Opportunity opp01 = new Opportunity (); 
                opp01.BI_Id_de_la_Oportunidad__c = '0067000000ZA5XF';
                opp01.Name = 'ENLACE INTERNET DEDICADO PLANTA';
                opp01.AccountId = acc01.Id;
                opp01.BI_Country__c = Label.BI_Mexico;
                opp01.BI_Ciclo_ventas__c = 'Completo';
                opp01.CloseDate = date.today();
                opp01.StageName = 'F1 - Closed Won';
                opp01.CurrencyIsoCode = 'MXN';
                opp01.BI_Duracion_del_contrato_Meses__c = 36;
                opp01.BI_Ingreso_por_unica_vez__c = 0.0;
                opp01.BI_Recurrente_bruto_mensual__c = 13390.00;
                opp01.BI_Opportunity_Type__c = 'Fijo';
                opp01.BI_Country__c = Label.BI_Mexico;
                opp01.BI_Fecha_de_cierre_real__c = Date.today();

                opp01.Amount = 4000;
            insert opp01;   



            System.debug('updateValPresupuestoOrder test opp'+ opp01.Id);
            NE__Order__c ord = new NE__Order__c(NE__AccountId__c = acc01.Id,
                                                    NE__BillAccId__c = acc01.Id,
                                                    NE__OptyId__c = opp01.Id,
                                                   
                                                    NE__ServAccId__c = acc01.Id,
                                                    NE__OrderStatus__c = 'Active',
                                                    CurrencyIsoCode = 'MXN');
            insert ord;
            

            NE__Product__c pord = new NE__Product__c();
             pord.Offer_SubFamily__c = 'a-test;';
             pord.Offer_Family__c = 'b-test';
             pord.BI_Rama_Digital__c = 'asdfasdf';
            
            insert pord; 

            NE__Catalog__c cat = new NE__Catalog__c();
            insert cat;
            NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(
                NE__Catalog_Id__c = cat.Id, 
                NE__ProductId__c = pord.Id, 
                NE__Change_Subtype__c = 'test', 
                NE__Disconnect_Subtype__c = 'test');
            insert catIt;
                   
            //Insertamos el order item asociado a la order
            NE__OrderItem__c oi = new NE__OrderItem__c(
                NE__OrderId__c = ord.Id,
                NE__Account__c = acc01.Id,
                NE__ProdId__c = pord.Id,
                NE__CatalogItem__c = catIt.Id,
                NE__qty__c = 1,
                NE__OneTimeFeeOv__c = 1000,
               
                NE__RecurringChargeOv__c = 3000,
                CurrencyISOCode = 'MXN'
            );
            insert oi;
      
        

/*

            
            
            NE__OrderItem__c NEInsert2  = new NE__OrderItem__c ();
                NEInsert2.NE__OrderId__c = NEORDER.id; 
                NEInsert2.CurrencyIsoCode = 'MXN';
                NEInsert2.NE__OneTimeFeeOv__c = 100;
                NEInsert2.NE__RecurringChargeOv__c = 100;
                 NEInsert2.NE__Qty__c = 1;
            insert NEInsert2;
        */            
        
        ADQ_OpcionesFacturacionController ofc = new ADQ_OpcionesFacturacionController();
        
        Test.startTest();
        ofc.listaCuentas =  listaCuentas;
        ofc.listaCuentasNext = listaCuentas ;
        ofc.getTotalCuentas();
        
        ofc.getListaCuentasNext();
        ofc.getListaCuentas();
        
        ofc.getListaGenerada();
        ofc.getLabelTabla();

        ofc.Next();
        ofc.Prev();
        ofc.getShownext();
        ofc.getShowprev();
        ofc.getListaGenerada();
        ofc.getLabelTabla();
        ofc.getClientIP();
        
        ofc.cuentaSeleccionada = acc01.Id;
        ofc.refresh();
        Test.stopTest();
        
        
    }
    
    @isTest static void testFacturaController() {
        Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
        Account acc01  = new Account ();
        acc01.Name = 'Cuenta de prueba testCosumos2';
		acc01.BI_Subsegment_Regional__c = 'test';
		acc01.BI_Territory__c = 'test';
        //acc01.Raz_n_social__c = 'Cuenta de prueba testCosumos SA CV';
        acc01.BI_Segment__c = 'Empresas';
        acc01.BI_Subsector__c = 'test'; //25/09/2017
        acc01.BI_Sector__c = 'test'; //25/09/2017
        acc01.RecordTypeId = rtId;
        insert acc01; 
        
        Factura__c f = new Factura__c();
        f.IVA__c = '16%';
        f.CurrencyIsoCode = 'MXN';
        f.Fecha_Inicio__c = System.today();
        f.Inicio_de_Facturaci_n__c = System.today();
        f.Condiciones_de_Pago__c = '30 Días';
        f.Cliente__c = acc01.Id;
        f.Tipo_de_Factura__c = 'Anticipado';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(f);
        
        
        
    }
    
    @isTest static  void ADQ_DesagruparFacturasControllerMod1 () {
            Id rtId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
            Account acc01  = new Account ();
                acc01.Name = 'Cuenta de prueba testCosumos2';
				acc01.BI_Subsegment_Regional__c = 'test';
				acc01.BI_Territory__c = 'test';
                //acc01.Raz_n_social__c = 'Cuenta de prueba testCosumos SA CV';
                acc01.BI_Segment__c = 'Empresas';
                acc01.BI_Subsector__c = 'test'; //25/09/2017
                acc01.BI_Sector__c = 'test'; //25/09/2017
                acc01.RecordTypeId = rtId;
            insert acc01;
            
            Factura__c Factura = new Factura__c ();
                Factura.Activo__c = true;
                Factura.Cliente__c = acc01.Id;
                Factura.Fecha_Inicio__c = Date.today();
                Factura.Fecha_Fin__c = Date.today() + 20;
                Factura.Inicio_de_Facturaci_n__c = Date.today();
                Factura.IVA__c = '16%';
                Factura.Sociedad_Facturadora__c = 'GTM';
                Factura.Requiere_Anexo__c = 'SI';
                RecordType tiporegistro = [SELECT Id FROM RecordType where  SobjectType = 'Factura__c' and DeveloperName  ='Recurrente'];
                Factura.RecordTypeId = tiporegistro.Id;
            insert Factura;
            
                Factura.Sociedad_Facturadora__c = 'PCS';
            update  Factura;
            
            Programacion__c programacion = new Programacion__c ();
                programacion.Factura__c = Factura.Id;
                programacion.CurrencyIsoCode = 'MXN';
                programacion.Sociedad_Facturadora_Real__c = 'GTM';
                programacion.Facturado__c = false;
                programacion.Numero_de_factura_SD__c = '';
                programacion.Folio_fiscal__c = '';
                programacion.Fecha__c = Date.today();
                programacion.Inicio_del_per_odo__c = Date.today();
                programacion.Fin_del_per_odo__c = Date.today().addMonths(1);
                programacion.Fecha_de_inicio_de_cobro__c = Date.today().addDays(2);
                
                
            insert programacion;
            
                programacion.Facturado__c =  true;
                programacion.Exportado__c =  true;
                programacion.Folio_Fiscal__c = 'FGTMD00001';
                programacion.Sociedad_Facturadora_Real__c = 'PCS';
                update programacion;
            try{

                delete Factura;
            }   catch (Exception err){
            }
     

            
        ADQ_DesagruparFacturasControllerMod1 clsvalida = new ADQ_DesagruparFacturasControllerMod1 ();
        
        
        Test.setCurrentPageReference(new PageReference('Page.ADQ_DesagruparFacturas'));
        System.currentPageReference().getParameters().put('Fid', Factura.Id); 
            
        ADQ_DesagruparFacturasControllerMod1.WrapperProgramacion wrapp = new   ADQ_DesagruparFacturasControllerMod1.WrapperProgramacion (programacion, true);
        List<ADQ_DesagruparFacturasControllerMod1.WrapperProgramacion> listaWrapperProgramacion = new List<ADQ_DesagruparFacturasControllerMod1.WrapperProgramacion>();
                
            clsvalida.factura =  Factura;
            
            clsvalida.getListaWrapperProgramaciones();
            clsvalida.desagrupar();
            clsvalida.matriz();
            clsvalida.consulta();
            clsvalida.consultaSelecciona();
            clsvalida.cosultaProgramaciones();  
            clsvalida.eliminaPE();
            clsvalida.DelPedido();
            clsvalida.cancelar();
            
        
            
            Boolean selected = true;
            
            //WrapperProgramacion wrapper1 = new WrapperProgramacion(progra,selected);
        }
        

        /*
        @isTest static void ADQ_FacturaControllerclass (){
            ADQ_FacturaController classtest01 = new ADQ_FacturaController ();
            classtest01.inicio();
            
        }*/
    
}