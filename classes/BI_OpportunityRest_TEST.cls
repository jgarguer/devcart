@isTest
private class BI_OpportunityRest_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_OpportunityRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    18/09/2014              Pablo Oliva             Initial Version
    27/05/2016              Guillermo Muñoz         BI_TestUtils.throw_exception set to false to avoid test errors
    01/06/2016              Antonio Pardo           Removed test methods, moved to BI_Buttons_TEST
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static{
        BI_TestUtils.throw_exception = false;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_OpportunityRest.getOpportunity()
    History:
    
    <Date>              <Author>                <Description>
    18/09/2014          Pablo Oliva             Initial version
    27/05/2016          Guillermo Muñoz         Changed BI_TestUtils.throw_exception values caused by changes in the global value
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getOpportunityTest() {
        /*
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		*/
        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){
            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;
            if(userTGS.Id != null){
                update userTGS;
            } else {
                insert userTGS;
            }
        }
		Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);
        
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');                                                
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        List <Opportunity> lst_opp = BI_DataLoadRest.loadOpportunitiesWithPais(1, lst_acc[0].Id, lst_region[0], null);
        List <NE__Order__c> lst_orders = BI_DataLoadRest.loadOrders(1, lst_acc[0], lst_opp[0]);
        List <NE__OrderItem__c> lst_oi = BI_DataLoadRest.loadOrderItem(lst_acc[0], lst_orders);
        
        for(NE__Order__c orderNE:lst_orders)
            orderNE.NE__OrderStatus__c = 'Active';
            
        update lst_orders;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/opportunities/'+lst_opp[0].Id;  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        BI_MigrationHelper.disableBypass(mapa); // Se inserta esta otra

        Test.startTest();
        
        BI_TestUtils.throw_exception = true;

        //INTERNAL_SERVER_ERROR
        BI_OpportunityRest.getOpportunity();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
        system.assertEquals(500,RestContext.response.statuscode);
        if(lst_log.isEmpty()){
            throw new BI_Exception('Se esperaba la excepcion y no se ha generado');
        }
        
        //NOT FOUND
        BI_TestUtils.throw_exception = false;
        req.requestURI = '/opportunities/idOpp';
        RestContext.request = req;
        BI_OpportunityRest.getOpportunity();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/opportunities/'+lst_opp[0].Id;
        RestContext.request = req;
        BI_OpportunityRest.getOpportunity();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
    }
}