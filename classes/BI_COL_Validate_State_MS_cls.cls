public with sharing class BI_COL_Validate_State_MS_cls 
{
	public map<string, set<string>> opciones = new map<string, set<string>>();
	
	public BI_COL_Validate_State_MS_cls()
	{
		//CONFIGURA VALORES
		opciones.put('RPV 133',new set<string>{'BW_Caudal_Bronce__c','BW_Caudal_Oro__c','BW_Videoconferencia__c','BW_Caudal_Plata__c','BW_Caudal_Platino__c','Canales_Voz__c'});
		opciones.put('MULT132',new set<string>{'BW_Caudal_Bronce__c','BW_Caudal_Oro__c','BW_Videoconferencia__c','BW_Caudal_Plata__c','BW_Caudal_Platino__c','Canales_Voz__c'});
		opciones.put('ACCE136',new set<string>{'C_digo_AGSI__c','ID_Conexion_TIWS__c'});
		opciones.put('GLOB137',new set<string>{'C_digo_AGSI__c','ID_Conexion_TIWS__c'});
		opciones.put('MULT138',new set<string>{'C_digo_AGSI__c','ID_Conexion_TIWS__c'});
		opciones.put('RPV 139',new set<string>{'C_digo_AGSI__c','ID_Conexion_TIWS__c'});
		opciones.put('EXTB 219',new set<string>{'C_digo_AGSI__c','ID_Conexion_TIWS__c'});
		opciones.put('RPV 140',new set<string>{'C_digo_AGSI__c','ID_Conexion_TIWS__c'});
		opciones.put('CORR178',new set<string>{'ID_Conexion_TIWS__c'});
		opciones.put('PALLGER02',new set<string>{'DS_Internet__c'});
		opciones.put('DATPDTI001',new set<string>{'DS_Internet__c'});
		opciones.put('PDTIALLGER',new set<string>{'DS_Internet__c'});
		opciones.put('DATPDTI001',new set<string>{'DS_Internet__c'});
		opciones.put('PCALL085',new set<string>{'DS_Internet__c'});
		opciones.put('DATPDTI001',new set<string>{'DS_Internet__c'});
		opciones.put('PDTIALLOP',new set<string>{'DS_Internet__c'});
		opciones.put('PALLOPE01',new set<string>{'DS_Internet__c'});
		opciones.put('PIPCI081',new set<string>{'DS_Internet__c'});
		opciones.put('PIPCI076',new set<string>{'DS_Internet__c'});
		opciones.put('DATPDTI002',new set<string>{'DS_Internet__c'});
		opciones.put('PIPCI075',new set<string>{'DS_Internet__c'});
		opciones.put('PIPCI080',new set<string>{'DS_Internet__c'});
		opciones.put('DATPDTI003',new set<string>{'DS_Internet__c'});
		opciones.put('PIPCI077',new set<string>{'DS_Internet__c'});
		opciones.put('PIPCI082',new set<string>{'DS_Internet__c'});
		opciones.put('PEDSKGE04',new set<string>{'DS_Internet__c'});
		opciones.put('PEDSKOP03',new set<string>{'DS_Internet__c'});
		opciones.put('PDSKTEC05',new set<string>{'DS_Internet__c'});
		opciones.put('PIPCI078',new set<string>{'DS_Internet__c'});
		opciones.put('DATPDTI004',new set<string>{'DS_Internet__c'});
		opciones.put('PLPTGER06',new set<string>{'DS_Internet__c'});
		opciones.put('PLPTTEC07',new set<string>{'DS_Internet__c'});
		opciones.put('PIPCI083',new set<string>{'DS_Internet__c'});
		opciones.put('PIPCI084',new set<string>{'DS_Internet__c'});
		opciones.put('PEMMTO08',new set<string>{'DS_Internet__c'});
		opciones.put('DATPDTI005',new set<string>{'DS_Internet__c'});
		opciones.put('PINET081',new set<string>{'DS_Internet__c'});
		opciones.put('DATPDTI006',new set<string>{'DS_Internet__c'});
		opciones.put('DATPDTI007',new set<string>{'DS_Internet__c'});
		opciones.put('AEQCOM04',new set<string>{'DS_Internet__c'});
		opciones.put('DESK196',new set<string>{'DS_Internet__c'});
		opciones.put('PIREM082',new set<string>{'DS_Internet__c'});
		opciones.put('IMPR197',new set<string>{'DS_Internet__c'});
		opciones.put('LAPT198',new set<string>{'DS_Internet__c'});
		opciones.put('PIREM082',new set<string>{'DS_Internet__c'});
		opciones.put('ITSIS001',new set<string>{'DS_Internet__c'});
		opciones.put('ITMAR002',new set<string>{'DS_Internet__c'});
		opciones.put('ITMAR001',new set<string>{'DS_Internet__c'});
	}
	
	public list<sObject> getMsOportunity(string idopt)
	{
		list<sObject> Ms = new list<BI_COL_Modificacion_de_Servicio__c>([
			SELECT 	BI_COL_Codigo_unico_servicio__c, BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__c , BI_COL_Descripcion_Referencia__c,
					Id, Name, BI_COL_Oportunidad__c, BI_COL_ID_Conexion_TIWS__c, BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c
					//BW_Caudal_Bronce__c,BW_Caudal_Oro__c,BW_Videoconferencia__c,BW_Caudal_Plata__c,BW_Caudal_Platino__c,Canales_Voz__c,
					//C_digo_AGSI__c,DS_Internet__c,
			FROM 	BI_COL_Modificacion_de_Servicio__c
			where 	BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__c =: idopt 
			and 	BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c IN: opciones.keySet() 
			and 	BI_COL_Estado__c = 'Pendiente' 
			and 	BI_COL_Producto__c != null ] );
		return Ms;
	}
	
	public string procesar( string idopt )
	{
		//Trae el valor del label dinamicamente, fieldMap.get('BW_Caudal_Bronce__c').getDescribe().getLabel()
		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		Schema.SObjectType leadSchema = schemaMap.get('BI_COL_Modificacion_de_Servicio__c');
		Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
		string validaError = '';
		string Error = '';
		list<sObject> msValidar = this.getMsOportunity(idopt);
		if( !msValidar.isEmpty() )
		{
			for( sObject msv : msValidar )
			{
				sobject Produc = msv.getSObject( 'BI_COL_Producto__r' );
				sobject Produc2 = Produc.getSObject( 'NE__ProdId__r' );
				string descripcion = string.valueOf( Produc2.get( 'BI_COL_LegadoID__c' ) );
				validaError = '';
			}
		}
		return Error;
	}
}