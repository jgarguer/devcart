/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro Sevilla
    Company:       NE Aborda
    Description:   Methods executed by  Triggers
    
    History:
    
    <Date>            <Author>          <Description>
    22/07/2016       Alvaro Sevilla     Initial version
    28/07/2016       Alvaro sevilla     Se ajusto la consulta para generar mejor pla plantilla y se dinamizaron otras partes de la misma
    01/08/2016       Alvaro sevilla     Se ajusto para que solo se tuvieran en cuenta las encuestas ISC
    23/11/2016       Alvaro Sevilla     Se cambio el texto y se dinamizo correo en el texto usando NE__Lov__c
    02/02/2017       Humberto Nunes     Se realiza el envio del correo con la cuenta configurada como EmailToCase
    05/05/2017       Humberto Nunes     Se cambio el parrafo final del correo, cerrado de la tabla para que se vea en Outlook y eliminacion de Labels colocando
                                        el style directamente sobre la celda td o th. 
    08/05/2017       Humberto Nunes     Se Subio el Cierre de la Tabla Justo al Terminar los TR.
    09/05/2017       Humberto nunes     Volvieron a cambiar el mensaje al final del correo. 
    01/08/2017       Alvaro Sevilla     Se adiciono el cerrado del caso del cual se dispara la encuesta una vez se envie el email
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_FVI_EmailRespuestasEncuesta {

    public static void EnviarEmailRespuestasEncuesta(list<SurveyQuestionResponse__c> lstSurveryTaker) {

        String nombredestinatario = '';
        set<String> stIdSurvery = new set<String>();
        set<String> stIdContrato = new set<String>();
        set<String> stIdContacto = new set<String>();
        set<String> stEnviarA = new set<String>();
        set<String> stNumContrato = new set<String>();
        set<String> stNomOpp = new set<String>();
        set<String> stIdCaso = new set<String>();
        list<String> lstEmailDestinatario = new list<String>();

        list<Messaging.Emailfileattachment> lstAdjuntos = new list<Messaging.Emailfileattachment>();

        for(SurveyQuestionResponse__c elemResponse : lstSurveryTaker){
            stIdSurvery.add(elemResponse.SurveyTaker__c);
        }       

        list<SurveyQuestionResponse__c> lstSurveryResponse = [select Survey_Question__r.OrderNumber__c, Survey_Question__r.Type__c,
                                                                     Survey_Question__r.Question__c,Survey_Question__r.Name, Response__c,SurveyTaker__r.Survey__r.BI_FVI_Type__c,
                                                                     SurveyTaker__r.BI_FVI_Contrato__c,SurveyTaker__r.BI_FVI_Contrato__r.ContractNumber,
                                                                     SurveyTaker__r.BI_FVI_Contrato__r.BI_Oportunidad__r.Name,SurveyTaker__r.Contact__r.Name,
                                                                     SurveyTaker__r.Contact__r.LastName,SurveyTaker__r.Contact__r.Email,SurveyTaker__r.Case__c 
                                                                from SurveyQuestionResponse__c 
                                                                Where SurveyTaker__c IN :stIdSurvery AND SurveyTaker__r.Survey__r.BI_FVI_Type__c = 'CSI' order by Survey_Question__r.OrderNumber__c];
         
        for(SurveyQuestionResponse__c elemContrato: lstSurveryResponse){
            stIdContrato.add(elemContrato.SurveyTaker__r.BI_FVI_Contrato__c);
            stNumContrato.add(elemContrato.SurveyTaker__r.BI_FVI_Contrato__r.ContractNumber);
            stNomOpp.add(elemContrato.SurveyTaker__r.BI_FVI_Contrato__r.BI_Oportunidad__r.Name);
            stIdCaso.add(elemContrato.SurveyTaker__r.Case__c);
        }

        String numContrato = '';
        for(String numC : stNumContrato){               
            numContrato += numC+',';                                
        }
        String nomOpp = '';
        for(String nomb : stNomOpp){                
            nomOpp += nomb+',';                             
        }
        
        list<Attachment> lstAttchContrato = [select id,Name, Body, BodyLength, Description,ContentType  from Attachment where ParentId IN :stIdContrato and Name Like '%CH.pdf'];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSubject('Mis respuestas a la encuesta de satisfacción del contrato: '+numContrato); 

        // AGREGAD POR HN 2017-02-02
        // SE REALIZA EL ENVIO CON EL CORREO UTILIZADO PARA EL EmailToCase de FVI PERU
        // Esta cuenta se configuro en Administrar --> Administración de correo electrónico --> Direcciones de toda la organización
        // Y tambien se configuro en Compilación --> Personalizar --> Casos --> Correo electrónico para registro de casos :: como "BI FVI PER EmailToCase" 
        OrgWideEmailAddress owe = [SELECT Id,DisplayName FROM OrgWideEmailAddress WHERE DisplayName like '%fvi%per%'];
        mail.setOrgWideEmailAddressId(owe.Id);            

        for (Attachment adjunto : lstAttchContrato){

              Messaging.Emailfileattachment adj = new Messaging.Emailfileattachment();
              adj.setFileName(adjunto.Name);
              adj.setContentType(adjunto.ContentType);
              adj.setInline(false);
              adj.setBody(adjunto.Body);
              lstAdjuntos.add(adj);
        }

        for (SurveyQuestionResponse__c emailDest : lstSurveryResponse) {

            if (emailDest.SurveyTaker__r.Contact__r.Email != null && emailDest.SurveyTaker__r.Contact__r.Name != null && emailDest.SurveyTaker__r.Contact__r.LastName != null) {                
                  // se captura la direccion de correo del destinatario              
                  stEnviarA.add(emailDest.SurveyTaker__r.Contact__r.Email);
                 
            }

        }

        if(!stEnviarA.isEmpty()){

            for(String elem : stEnviarA){
                lstEmailDestinatario.add(elem);
            }

            mail.setToAddresses(lstEmailDestinatario);                  
            // Se define el remitente
            //mail.setReplyTo('alvaro.sevilla@aborda.es');//he puesto mi correo provisionalmente
            // OMITIDO POR HN 2017-02-02
            // mail.setSenderDisplayName('Administración FVI');            
              // opcional personas a quien se pone en copia
              //List<String> ccTo = new List<String>();
              //ccTo.add('humberto.nunes@aborda.es');
              //mail.setCcAddresses(ccTo);                                          
        }   

        
        for (SurveyQuestionResponse__c destinatario : lstSurveryResponse) {

            if (destinatario.SurveyTaker__r.Contact__r.Email != null && destinatario.SurveyTaker__r.Contact__r.Name != null && destinatario.SurveyTaker__r.Contact__r.LastName != null) {                 
                  // captura del nombre del destinatario
                  stIdContacto.add(destinatario.SurveyTaker__r.Contact__r.Name);
                }
        }

        if(!stIdContacto.isEmpty()){

            map<String,list<String>> mpPregRespu = new map<String,list<String>>();
            list<String> lstRespu = new list<String>();
            set<String> stPregMulti = new set<String>();

            for(String dest : stIdContacto){                
                nombredestinatario += dest+',';                             
            }

            for (SurveyQuestionResponse__c respuestas : lstSurveryResponse) {

                if(respuestas.Survey_Question__r.Type__c == 'Multi-Select--Vertical' ){

                    if(!mpPregRespu.containskey(respuestas.Survey_Question__r.Question__c)){
                        lstRespu = new list<String>();
                        lstRespu.add(respuestas.Response__c);
                        mpPregRespu.put(respuestas.Survey_Question__r.Question__c,lstRespu);
                    }else{
                        lstRespu = new list<String>();
                        lstRespu = mpPregRespu.get(respuestas.Survey_Question__r.Question__c);
                        lstRespu.add(respuestas.Response__c);
                        mpPregRespu.put(respuestas.Survey_Question__r.Question__c,lstRespu);

                    }
                }
              
            }

            Document banner = [SELECT Id FROM Document WHERE DeveloperName = 'BI_Cabecera_Plantilla'];

             // OMITIDO POR HN 15/02/2017 NO TIENE SENTIDO PORQUE AHORA DEBERAN RESPONDER DIRECTAMENTE AL EMAIL QUE RECIBEN. 
            // List<NE__Lov__c> lst_lovEmail = [SELECT NE__Value1__c,NE__Value2__c,NE__Value3__c,Text_Area_Value__c,Country__c FROM NE__Lov__c WHERE NE__Type__c = 'EMAIL ENCUESTA ISC'];

            System.debug('--->>>>>< '+UserInfo.getOrganizationId());
            
            String body = '<html><head></head><body>';
            body += '<p>Estimado Señor(a) '+nombredestinatario+'</p><br/>';
            body += '<p>Le agradecemos la confianza depositada en Telefónica. Tras la comprobación de contratación digital del servicio/s, nos complace remitirle su copia del contrato número '+numContrato+' que detalla los servicios contratados y los términos y condiciones de los mismos para su control y custodia. Además, en la parte de debajo de este mensaje le proporcionamos la información obtenida de la llamada telefónica de verificación personal que hemos realizado recientemente con usted.</p><br/>';
            body += '<center><table border="0" cellspacing="10" >';
            if (UserInfo.getOrganizationId() == '00Dw0000000m7dEEAQ'){ //produccion
                body += '<tr><td colspan="2"><img src="https://login.salesforce.com/servlet/servlet.ImageServer?id='+banner.Id+'&oid='+UserInfo.getOrganizationId()+'" width="100%;"></td></tr>';
            }else{
                body += '<tr><td colspan="2"><img src="https://test.salesforce.com/servlet/servlet.ImageServer?id='+banner.Id+'&oid='+UserInfo.getOrganizationId()+'" width="100%;"></td></tr>';
            }
            
            //body += '<tr><td colspan="2"><img src="https://test.salesforce.com/servlet/servlet.ImageServer?id=0157E0000004rZE&oid=00D7E000000D4Br" width="100%;"></td></tr>';
            body += '<tr><th width="500px" style="font-weigth:bold; border-bottom: 1px solid #999">LAS PREGUNTAS</th><th width="100px" style="font-weigth:bold; border-bottom: 1px solid #999">SUS RESPUESTAS</th></tr>';
            //Integer num = 1;           
            for (SurveyQuestionResponse__c respuestas : lstSurveryResponse) {

                if(respuestas.Survey_Question__r.Type__c == 'Multi-Select--Vertical' ){
                    String selecciones ='';

                    if(!stPregMulti.contains(respuestas.Survey_Question__r.Question__c)){

                        System.debug('--->>>>>keysssss< '+mpPregRespu.keySet());

                        for(String clave : mpPregRespu.keySet()){

                            if(respuestas.Survey_Question__r.Question__c == clave){

                                for(String valor : mpPregRespu.get(clave))
                                {
                                    selecciones += valor+',';
                                }

                                selecciones = selecciones.removeEnd(',');
                                body += '<tr><td style="font-weigth:bold; border-bottom: 1px solid #CCC">'+respuestas.Survey_Question__r.Question__c+'</td><td style="border-bottom: 1px solid #CCC">'+selecciones+'</td></tr>';
                            }
                        }

                        stPregMulti.add(respuestas.Survey_Question__r.Question__c);
                    }
                    
                }else{

                    if(respuestas.Response__c != null){
                            body += '<tr><td style="font-weigth:bold; border-bottom: 1px solid #CCC">'+respuestas.Survey_Question__r.Question__c+'</td><td style="border-bottom: 1px solid #CCC">'+respuestas.Response__c+'</td></tr>';
                        }else{
                            body += '<tr><td style="font-weigth:bold; border-bottom: 1px solid #999">'+respuestas.Survey_Question__r.Question__c+'</td><td style="border-bottom: 1px solid #CCC">'+' '+'</td></tr>';
                        }                    
                }
                  
                  //num+=1;               
            }  

            body += '</table>';

            // NUEVO MENSAJE SOLICITADO EN EHELP 02558870
            // 09/05/2017
            body += '<br/><p>En caso de no estar de acuerdo con cualquiera de los aspectos, háganos llegar sus comentarios a la mayor brevedad respondiendo a este correo electrónico, indicando los motivos del desacuerdo y nos pondremos en contacto con usted lo antes posible.</p><br/>';
            
            body += '<br/><p>Un cordial saludo,</p><br/>';  
            body += '<br/><p>Telefonica del Perú</p><br/>'; 
            body += '</center></body></html>';  
            mail.setHtmlBody(body);
            
            if(!lstAdjuntos.isEmpty()){
                System.debug('----->LISTAADJUNTO: '+lstAdjuntos);
                mail.setFileAttachments(lstAdjuntos);
            }
            
            // envío del email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});

            // CAPTURAR CASO DE LA ENCUESTA PARA CERRARLO UNA VEZ SE ENVIE EL EMAIL
            // ASD 01/08/2017
            List<Case> lstCasosEncuestas = [Select Id, Status from Case where Id IN :stIdCaso];

            if(!lstCasosEncuestas.isEmpty()){
                lstCasosEncuestas[0].Status = 'Closed';
                 Update lstCasosEncuestas;
            }

        }

    }
}