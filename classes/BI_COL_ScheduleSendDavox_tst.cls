/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for class BI_COL_ScheduleSendDavox_cls.
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-08-24      Raul Mora (RM)				    Create class.
*************************************************************************************/
@isTest
private class BI_COL_ScheduleSendDavox_tst 
{
	public static List<BI_Log__c> lstLog;
	
	public static void createData()
	{
		BI_bypass__c objBibypass = new BI_bypass__c();
		objBibypass.BI_migration__c=true;
		insert objBibypass;
			
		lstLog = new List<BI_Log__c>();
		
		BI_Log__c objLog = new BI_Log__c();
		objLog.BI_COL_Informacion_Enviada__c = 'Test Info enviada';
		objLog.BI_COL_Informacion_recibida__c = 'Test Info recibi';
		objLog.BI_COL_Estado__c = 'Pendiente';
		objLog.BI_COL_Tipo_Transaccion__c = 'SERV. CORPORATIVO - COBROS PARCIALES';
		lstLog.add( objLog );
		
		BI_Log__c objLog2 = new BI_Log__c();
		objLog2.BI_COL_Informacion_Enviada__c = 'Test2 Info enviada';
		objLog2.BI_COL_Informacion_recibida__c = 'Test2 Info recibi';
		objLog2.BI_COL_Estado__c = 'Devuelto';
		objLog2.BI_COL_Tipo_Transaccion__c = 'SERV. CORPORATIVO - COBROS PARCIALES';
		lstLog.add( objLog2 );
		
		BI_Log__c objLog3 = new BI_Log__c();
		objLog3.BI_COL_Informacion_Enviada__c = 'Test3 Info enviada';
		objLog3.BI_COL_Informacion_recibida__c = 'Test3 Info recibi';
		objLog3.BI_COL_Estado__c = 'Error Conexion';
		objLog3.BI_COL_Tipo_Transaccion__c = 'SERV. CORPORATIVO - COBROS PARCIALES';
		lstLog.add( objLog3 );
		
		BI_Log__c objLog4 = new BI_Log__c();
		objLog4.BI_COL_Informacion_Enviada__c = 'Test4 Info enviada';
		objLog4.BI_COL_Informacion_recibida__c = 'Test4 Info recibi';
		objLog4.BI_COL_Estado__c = 'Procesando';
		objLog4.BI_COL_Tipo_Transaccion__c = 'SERV. CORPORATIVO - COBROS PARCIALES';
		lstLog.add( objLog4 );
		
		insert lstLog;
		
	}

    static testMethod void myUnitTest() {
        createData();
        Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
        Test.startTest();
        	SchedulableContext sc;
			BI_COL_ScheduleSendDavox_cls class_sch = new BI_COL_ScheduleSendDavox_cls();
			BI_COL_ScheduleSendDavox_cls class_sch1 = new BI_COL_ScheduleSendDavox_cls(1);
			BI_COL_ScheduleSendDavox_cls class_sch2 = new BI_COL_ScheduleSendDavox_cls(1,'1');
						
			class_sch.EliminarSch();
			class_sch.execute(sc);	  
			class_sch.getPackageCode();  	
	    Test.stopTest();
    }
}