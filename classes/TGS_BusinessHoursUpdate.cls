public with sharing class TGS_BusinessHoursUpdate {
   
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Angel Galindo, Carlos Campillo
    Company:       Deloitte
    Description:   This class contains all methods related with the custom object TGS_Business_Hours.
					Class invoked by the trigger TGS_BusinessHours_Trigger.
    
    History

    <Date>            <Author>                  	<Description>
    27/01/2015        Miguel Angel Galindo			Initial version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    
    /**
    * Constructor
    * 
    *  Inicialice the custom object with the info of the Business hours related 
    * 
    */
    public TGS_BusinessHoursUpdate( List<TGS_Business_Hours__c> listaBhcNew ){
        
        List<Id> listaId = new List<Id>();
        for (TGS_Business_Hours__c bh : listaBhcNew){
        	listaId.add(bh.lookup_Business_Hours__c);	
        }
        
        List<BusinessHours> bhlist =[Select
                SundayStartTime, MondayStartTime, TuesdayStartTime,
                WednesdayStartTime, ThursdayStartTime, FridayStartTime,
                SaturdayStartTime, SundayEndTime, MondayEndTime,TuesdayEndTime,
                WednesdayEndTime, ThursdayEndTime, FridayEndTime,SaturdayEndTime, Id
                From BusinessHours 
                Where Id IN :listaId]; 
                
        for (BusinessHours bh : bhlist) {
        	for (TGS_Business_Hours__c bhc : listaBhcNew){
        		if (bh.Id == bhc.lookup_Business_Hours__c){
        			if(!(bh.FridayEndTime==null))
	                    bhc.FridayEndTime__c = String.valueOf(bh.FridayEndTime).left(5);
	                if(!(bh.FridayStartTime==null))
	                    bhc.FridayStartTime__c = String.valueOf(bh.FridayStartTime).left(5);
	                if(!(bh.MondayEndTime==null))
	                    bhc.MondayEndTime__c = String.valueOf(bh.MondayEndTime).left(5);
	                if(!(bh.MondayStartTime==null))
	                    bhc.MondayStartTime__c = String.valueOf(bh.MondayStartTime).left(5);
	                if(!(bh.SaturdayEndTime==null))
	                    bhc.SaturdayEndTime__c = String.valueOf(bh.SaturdayEndTime).left(5);
	                if(!(bh.SaturdayStartTime==null))
	                    bhc.SaturdayStartTime__c = String.valueOf(bh.SaturdayStartTime).left(5);
	                if(!(bh.SundayEndTime==null))
	                    bhc.SundayEndTime__c = String.valueOf(bh.SundayEndTime).left(5);
	                if(!(bh.SundayStartTime==null))
	                    bhc.SundayStartTime__c = String.valueOf(bh.SundayStartTime).left(5);
	                if(!(bh.ThursdayEndTime==null))
	                    bhc.ThursdayEndTime__c = String.valueOf(bh.ThursdayEndTime).left(5);
	                if(!(bh.ThursdayStartTime==null))
	                    bhc.ThursdayStartTime__c = String.valueOf(bh.ThursdayStartTime).left(5);
	                if(!(bh.TuesdayEndTime==null))  
	                    bhc.TuesdayEndTime__c = String.valueOf(bh.TuesdayEndTime).left(5);
	                if(!(bh.TuesdayStartTime==null))    
	                    bhc.TuesdayStartTime__c = String.valueOf(bh.TuesdayStartTime).left(5);
	                if(!(bh.WednesdayEndTime==null))    
	                    bhc.WednesdayEndTime__c = String.valueOf(bh.WednesdayEndTime).left(5);
	                if(!(bh.WednesdayStartTime==null))  
	                    bhc.WednesdayStartTime__c = String.valueOf(bh.WednesdayStartTime).left(5);
        		}
        	}
        }    
    }
}