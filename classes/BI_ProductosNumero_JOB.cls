global class BI_ProductosNumero_JOB implements Schedulable {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   CLASE TEMPORAL
    Test Class:    

    History: 
    
     <Date>                     <Author>                <Change Description>
    14/01/2014                  Micah Burgos             Initial Version
    27/09/2015                  Francisco Ayllon         Deprecated
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global Id Id_Last_Order;

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that executes the job, and checks diferences between BI_LastPasswordChange__c and LastPasswordChangeDate

    History: 
    
     <Date>                     <Author>                <Change Description>
    16/01/2015                  Micah Burgos             Initial Version
    20/01/2015                  Micah Burgos             Add BI_bypass__c condition
    27/09/2015                  Francisco Ayllon         Deprecated
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global void execute(SchedulableContext sch) {
        /*try{
            system.abortJob(sch.getTriggerId());
            
    		BI_bypass__c bypass = BI_bypass__c.getInstance();
            if(!bypass.BI_stop_job__c){
    			List <NE__OrderItem__c> lst_order = new List <NE__OrderItem__c>();
    			final Integer LIMITE = (Test.isRunningTest())?1:400;

                lst_order = [SELECT Id  , NE__OrderId__r.NE__OptyId__r.BI_Producto_actualizado__c, NE__OrderId__r.NE__OptyId__r.BI_Productos_numero__c 
                        FROM NE__OrderItem__c 
                        WHERE NE__OrderId__r.NE__OptyId__r.BI_Producto_actualizado__c = false AND (NE__OrderId__r.NE__OptyId__r.BI_Productos_numero__c = null OR NE__OrderId__r.NE__OptyId__r.BI_Productos_numero__c = 1) limit :limite];

                        System.debug('ORDERS QUERY [lst_order]: '+ lst_order);

                if(!lst_order.isEmpty()){

    				//BI_NEOrderItemMethods.updateOppChilds(lst_order, null); //DEPRECATED

    				//Rerun Job
                    System.debug('Limite: ' + limite + 'List size: ' + lst_order.size() );

                    if(lst_order.size() == limite){
                        system.debug('BI_Jobs.updateProductosNumero(false);');
                        BI_Jobs.updateProductosNumero(false);
                    }else{
                        system.debug('BI_Jobs.updateProductosNumero(true);');
                        BI_Jobs.updateProductosNumero(true);
                    }
                    
                }
            }
            system.abortJob(sch.getTriggerId());
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_ProductosNumero_JOB.Schedulable', 'BI_EN', Exc, 'Schedulable Job');
        }*/
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Constructor method
    
    IN:            NE__OrderItem__c Id
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    20/11/2014        Micah Burgos       Initial version.
    27/09/2015        Francisco Ayllon         Deprecated
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*public BI_ProductosNumero_JOB(){
    }*/
}