/****************************************************************************************************
    Información general
    -------------------
    author: OJCB
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       19-08-2015    OJCB                          Creación de la Clase Prueba
    1.1       02-09-2015    Daniel Lopez (DL)             Actualizacion de Clase para subir la cobertura
    1.2       23-02-2017    Jaime Regidor                 Adding Campaign Values, Adding Catalog Country 
	          13/03/2017    Marta Gonzalez(Everis)		  REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
              20/09/2017    Angel F. Santaliestra         Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
****************************************************************************************************/
@isTest
private class BI_COL_CreateProduct_tst {
    
    public static BI_Log__c objBiLog;
    public static list<BI_Log__c> lstLog = new list <BI_Log__c>();
    
    public static BI_COL_Modificacion_de_Servicio__c objMS;
    public static BI_COL_Modificacion_de_Servicio__c objMS2;
    public static BI_COL_Modificacion_de_Servicio__c objMS3;
    public static Account objCuenta;
    public static Account objCuenta2;
    public static Contact objContacto;
    public static Opportunity objOportunidad;
    public static BI_COL_Anexos__c objAnexos;
    public static BI_COL_Descripcion_de_servicio__c objDesSer;
    public static Contract objContrato;
    public static Campaign objCampana;
    public static BI_Col_Ciudades__c objCiudad;
    public static BI_Sede__c objSede;
    public static User objUsuario;
    public static BI_Punto_de_instalacion__c objPuntosInsta;
    public static BI_Log__c objBiLog2;
    public static BI_Log__c objBiLog3;

    
    public static list <Profile> lstPerfil;
    public static list <UserRole> lstRoles;
    public static list <Campaign> lstCampana;
    public static list<BI_COL_manage_cons__c> lstManege;
    public static List<RecordType> lstRecTyp;
    public static List<User> lstUsuarios;
    public static list<BI_Log__c> lstLog2 = new list <BI_Log__c>();
    public static List<Account> lstCuentas;
    public static BI_COL_Homologacion_integraciones__c homologacionProducto;
    public static BI_COL_Homologacion_integraciones__c homologacionProducto2;

    public static void crearData()
    {
        ////perfiles
        //lstPerfil                         = new list <Profile>();
        //lstPerfil                     = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
        //system.debug('datos Perfil '+lstPerfil);
                
        //List<string> lstMsString = new List<string>();
        ////usuarios
        ////lstUsuarios = [Select Id FROM User where Pais__c = 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];
        
        ////lstUsuarios[0].CompanyName = 'Test';
        ////Update lstUsuarios;

        ////Roles
        //  lstRoles                = new list <UserRole>();
        //  lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
        //  system.debug('datos Rol '+lstRoles);

        ////ObjUsuario
  //      objUsuario = new User();
  //      objUsuario.Alias = 'standt';
  //      objUsuario.Email ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey = '';
  //      objUsuario.LastName ='Testing';
  //      objUsuario.LanguageLocaleKey ='en_US';
  //      objUsuario.LocaleSidKey ='en_US'; 
  //      objUsuario.ProfileId = lstPerfil.get(0).Id;
  //      objUsuario.TimeZoneSidKey ='America/Los_Angeles';
  //      objUsuario.UserName ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey ='UTF-8';
  //      objUsuario.UserRoleId = lstRoles.get(0).Id;
  //      objUsuario.BI_Permisos__c ='Sucursales';
  //      objUsuario.Pais__c='Colombia';
  //      insert objUsuario;
        
        //lstUsuarios = new list<User>();
  //      lstUsuarios.add(objUsuario);

        //System.runAs(lstUsuarios[0])
        //{     
        //  //Roles
        //  lstRoles                = new list <UserRole>();
        //  lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
        //  system.debug('datos Rol '+lstRoles);
            
            //Cuentas
            objCuenta                                       = new Account();      
            objCuenta.Name                                  = 'prueba';
            objCuenta.BI_Country__c                         = 'Colombia';
            objCuenta.TGS_Region__c                         = 'América';
            objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
            objCuenta.CurrencyIsoCode                       = 'COP';
            objCuenta.BI_Fraude__c                          = false;
            objCuenta.BI_Segment__c                         = 'test';
            objCuenta.BI_Subsegment_Regional__c             = 'test';
            objCuenta.BI_Territory__c                       = 'test';
            //Insert objCuenta;
            lstCuentas = new List<Account>();
            lstCuentas.add(objCuenta);
            system.debug('datos Cuenta '+objCuenta);

            objCuenta2                                       = new Account();
            objCuenta2.Name                                  = 'prueba';
            objCuenta2.BI_Country__c                         = 'Colombia';
            objCuenta2.TGS_Region__c                         = 'América';
            objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
            objCuenta2.CurrencyIsoCode                       = 'COP';
            objCuenta2.BI_Fraude__c                          = true;
            objCuenta2.BI_Segment__c                         = 'test';
            objCuenta2.BI_Subsegment_Regional__c             = 'test';
            objCuenta2.BI_Territory__c                       = 'test';
            //Insert objCuenta2;
            lstCuentas = new List<Account>();
            lstCuentas.add(objCuenta);
            
            Insert lstCuentas;
            System.debug('datos de la lista cuentas'+lstCuentas);
            
            //Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
            objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = lstCuentas.get(0).Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
        	//REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
            //REING_FIN
            objContacto.BI_Representante_legal__c           = TRUE;
            Insert objContacto;
            
            //catalogo
            NE__Catalog__c objCatalogo                      = new NE__Catalog__c();
            objCatalogo.Name                                = 'prueba Catalogo';
            objCatalogo.BI_country__c                       = 'Colombia';
            insert objCatalogo; 
            
            //Campaña
            objCampana                                      = new Campaign();
            objCampana.Name                                 = 'Campaña Prueba';
            objCampana.BI_Catalogo__c                       = objCatalogo.Id;
            objCampana.BI_COL_Codigo_campana__c             = 'prueba1';
            objCampana.BI_Country__c                        = 'Colombia';
            objCampana.BI_Opportunity_Type__c               = 'Fijo';
            objCampana.BI_SIMP_Opportunity_Type__c          = 'Alta';
            insert objCampana;
            lstCampana                                      = new List<Campaign>();
            lstCampana                                      = [select Id, Name from Campaign where Id =: objCampana.Id];
            system.debug('datos Cuenta '+lstCampana);
            
            //Tipo de registro
            lstRecTyp                                       = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
            system.debug('datos de FUN '+lstRecTyp);
            
            //Contrato
            objContrato                                     = new Contract ();
            objContrato.AccountId                           = lstCuentas.get(0).Id;
            objContrato.StartDate                           =  System.today().addDays(+5);
            objContrato.BI_COL_Formato_Tipo__c              = 'Contrato Marco';
            objContrato.ContractTerm                        = 12;
            objContrato.BI_COL_Presupuesto_contrato__c      = 1000000;
            objContrato.StartDate                           = System.today().addDays(+5);
            insert objContrato;
            System.debug('datos Contratos ===>'+objContrato);
            
            //FUN
            objAnexos                                       = new BI_COL_Anexos__c();
            objAnexos.Name                                  = 'FUN-0041414';
            objAnexos.RecordTypeId                          = lstRecTyp[0].Id;
            objAnexos.BI_COL_Contrato__c                    = objContrato.Id;
            insert objAnexos;
            System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
            
            //Configuracion Personalizada
            BI_COL_manage_cons__c objConPer                 = new BI_COL_manage_cons__c();
            objConPer.Name                                  = 'Viabilidades';
            objConPer.BI_COL_Numero_Viabilidad__c           = 46;
            objConPer.BI_COL_ConsProyecto__c                = 1;
            objConPer.BI_COL_ValorConsecutivo__c            = 1;
            lstManege                                       = new list<BI_COL_manage_cons__c >();        
            lstManege.add(objConPer);
            insert lstManege;

            System.debug('======= configuracion personalizada ======= '+lstManege);
            
            //Oportunidad
            objOportunidad                                          = new Opportunity();
            objOportunidad.Name                                     = 'prueba opp';
            objOportunidad.AccountId                                = lstCuentas.get(0).Id;
            objOportunidad.BI_Country__c                            = 'Colombia';
            objOportunidad.CloseDate                                = System.today().addDays(+5);
            objOportunidad.StageName                                = 'F5 - Solution Definition';
            objOportunidad.CurrencyIsoCode                          = 'COP';
            objOportunidad.Certa_SCP__contract_duration_months__c   = 12;
            objOportunidad.BI_Plazo_estimado_de_provision_dias__c   = 0 ;
            insert objOportunidad;
            
            system.debug('Datos Oportunidad'+objOportunidad);

            list<Opportunity> lstOportunidad                        = new list<Opportunity>();
            lstOportunidad                                          = [select Id from Opportunity where Id =: objOportunidad.Id];
            
            system.debug('datos ListaOportunida '+lstOportunidad);
            
            //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            
            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            
            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = lstCuentas.get(0).Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name                 = 'Prueba los Paso Pablo';
            insert objPuntosInsta;
            
            //DS
            objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
            objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
            objDesSer.CurrencyIsoCode                           = 'COP';
            insert objDesSer;
            System.debug('Data DS ====> '+ objDesSer);
            System.debug('Data DS ====> '+ objDesSer.Name);
            List<BI_COL_Descripcion_de_servicio__c> lstqry  = [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
            System.debug('Data lista DS ====> '+ lstqry);
            
            //MS
            objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
            objMS.BI_COL_FUN__c                         = objAnexos.Id;
            objMS.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objMS.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
            objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
            objMS.BI_COL_Bloqueado__c                   = false;
            objMS.BI_COL_Estado__c                      = label.BI_COL_lblActiva;//label.BI_COL_lblActiva;
            objMS.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objMs.BI_COL_Sucursal_Origen__c                 = objPuntosInsta.Id;
            objMs.BI_COL_Fecha_inicio_de_cobro_RFB__c   = System.today().Adddays(30);
            objMs.BI_COL_Duracion_meses__c               = 12;
            insert objMS;


            objMS2                                      = new BI_COL_Modificacion_de_Servicio__c();
            objMS2.BI_COL_FUN__c                        = objAnexos.Id;
            objMS2.BI_COL_Codigo_unico_servicio__c      = objDesSer.Id;
            objMS2.BI_COL_Clasificacion_Servicio__c     = 'ALTA DEMO';
            objMS2.BI_COL_Oportunidad__c                = objOportunidad.Id;
            objMS2.BI_COL_Bloqueado__c                  = false;
            objMS2.BI_COL_Estado__c                     = label.BI_COL_lblPendiente;//label.BI_COL_lblActiva;
            objMS2.BI_COL_Sucursal_de_Facturacion__c    = objPuntosInsta.Id;
            objMs2.BI_COL_Sucursal_Origen__c            = objPuntosInsta.Id;
            objMs2.BI_COL_Fecha_inicio_de_cobro_RFB__c  = System.today().Adddays(30);
            objMs2.BI_COL_Duracion_meses__c             = 12;
            insert objMS2;

            objMS3                                      = new BI_COL_Modificacion_de_Servicio__c();
            objMS3.BI_COL_FUN__c                        = objAnexos.Id;
            objMS3.BI_COL_Codigo_unico_servicio__c      = objDesSer.Id;
            objMS3.BI_COL_Clasificacion_Servicio__c     = 'BAJA';
            objMS3.BI_COL_Oportunidad__c                = objOportunidad.Id;
            objMS3.BI_COL_Bloqueado__c                  = false;
            objMS3.BI_COL_Estado__c                     = 'Inactiva';//label.BI_COL_lblActiva;
            objMS3.BI_COL_Sucursal_de_Facturacion__c    = objPuntosInsta.Id;
            objMs3.BI_COL_Sucursal_Origen__c            = objPuntosInsta.Id;
            objMs3.BI_COL_Duracion_meses__c             = 12;
            
            insert objMS3;
            system.debug('Data objMs ===>'+objMs);
            
            homologacionProducto = new BI_COL_Homologacion_integraciones__c(
                                                      BI_COL_Descripcion_referencia__c='IMPORTANTE-1236',
                                                      BI_COL_Ancho_banda__c='380 Mbps',
                                                      BI_COL_ID_Colombia__c='1032414648'
                                                      );

            insert homologacionProducto;

            homologacionProducto2 = new BI_COL_Homologacion_integraciones__c(
                                                      BI_COL_Descripcion_referencia__c='IMPORTANTE-1235',
                                                      BI_COL_Ancho_banda__c='380 Mbps',
                                                      BI_COL_ID_Colombia__c='1032419999'
                                                      );

            insert homologacionProducto2;

            //NE__Order__c objConfiguracionitem

            //NE__Order__c objConfiguracion = new NE__Order__c();
            ////objConfiguracion.NE__OrderId__c = 
            //Insert objConfiguracion;

            //NE__OrderItem__c producto = new NE__OrderItem__c();
            //producto.NE__Qty__c       = 12345;
            //producto.NE__OrderId__c   = objConfiguracion.id;
            //Insert producto;

            objBiLog                                    = new BI_Log__c();
            objBiLog.BI_COL_Informacion_recibida__c     = 'OK, Respuesta Procesada';
            objBiLog.BI_COL_Informacion_Enviada__c      = 'OK, Respuesta Procesada';
            objBiLog.BI_COL_Estado__c                   = 'Pendiente';
            objBiLog.BI_COL_Interfaz__c                 = 'NOTIFICACIONES';
            objBiLog.BI_COL_Tipo_Transaccion__c         = 'CREACION PRODUCTO';
            objBiLog.BI_COL_Identificador__c            = 'P - 000100001';
            objBiLog.BI_COL_Modificacion_Servicio__c    = objMS2.id;
            objBiLog.BI_COL_Producto_Telefonica__c      = homologacionProducto2.id;

            lstLog.add(objBiLog);   

            ////Bi_Log
            //objBiLog2                                 = new BI_Log__c();
            //objBiLog2.BI_COL_Informacion_recibida__c      = 'OK, Respuesta Procesada';
            //objBiLog2.BI_COL_Informacion_Enviada__c       = 'OK, Respuesta Procesada';
            //objBiLog2.BI_COL_Estado__c                    = 'FALLIDO';
            //objBiLog2.BI_COL_Interfaz__c                  = 'NOTIFICACIONES';
            //objBiLog2.BI_COL_Modificacion_Servicio__c = objMS2.id;
            //lstLog.add(objBiLog2);

            //objBiLog3                                 = new BI_Log__c();
            //objBiLog3.BI_COL_Informacion_recibida__c      = 'OK, Respuesta Procesada';
            //objBiLog3.BI_COL_Informacion_Enviada__c       = 'OK, Respuesta Procesada';
            //objBiLog3.BI_COL_Estado__c                    = 'FALLIDO';
            //objBiLog3.BI_COL_Interfaz__c                  = 'NOTIFICACIONES';
            //objBiLog3.BI_COL_Modificacion_Servicio__c = objMS2.id;
            //lstLog.add(objBiLog3);    

            insert lstLog;
            
        //}
    }

    @isTest static void test_method_one()
    {
        // Implement test code
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
            //objBiLog                                  = new BI_Log__c();
            //objBiLog.BI_COL_Informacion_recibida__c       = 'OK, Respuesta Procesada';
            //objBiLog.BI_COL_Informacion_Enviada__c        = 'OK, Respuesta Procesada';
            //objBiLog.BI_COL_Estado__c                 = 'Pendiente';
            //objBiLog.BI_COL_Interfaz__c                   = 'NOTIFICACIONES';
            //lstLog.add(objBiLog);
            //insert lstLog;
            
            BI_bypass__c objBibypass = new BI_bypass__c();
            objBibypass.BI_migration__c=true;
            insert objBibypass;

            crearData();

            Test.startTest();
            //Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
            //Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );
            Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws() );
            BI_COL_CreateProduct_cls objCreateProduct =new BI_COL_CreateProduct_cls();
            list<BI_COL_Homologacion_integraciones__c> lstHomologacion =new list<BI_COL_Homologacion_integraciones__c> ();
            lstHomologacion.add(homologacionProducto);
            lstHomologacion.add(homologacionProducto2);
            //lstHomologacion.add(homologacionProducto);
            //lstHomologacion.add(new BI_COL_Homologacion_integraciones__c(BI_COL_Descripcion_referencia__c='IMPORTANTE-1235',BI_COL_Ancho_banda__c='333 Mbps'));
            //insert lstHomologacion;

            objBiLog.BI_COL_Producto_Telefonica__c      = lstHomologacion[0].id;
            update objBiLog;


            BI_COL_CreateProduct_cls.invokeapexcallout( lstHomologacion );
            Test.stopTest();
        }
    }   
}