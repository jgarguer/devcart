@isTest(seeAllData = false)
public class BIIN_Obtener_Anexo_TEST {
/*------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José María Martín
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_Obtener_Anexo_WS
    
    <Date>               <Author>                           <Change Description>
    11/2015              José María Martín                  Initial Version
    18/05/2016           Julio Laplaza                      Adaptation to UNICA
    22/06/2016           José luis González Beltrán         Fix attachment test
------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void test_BIIN_Obtener_Anexo_TEST() 
    {
        BI_TestUtils.throw_exception=false;
        
        String authorizationToken;
        
        Case caso = new Case();
        insert caso;            
        
        Test.startTest(); 

        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerAnexo'));   
        BIIN_Obtener_Anexo_WS controller = new BIIN_Obtener_Anexo_WS();
        controller.obtenerAnexo(authorizationToken, caso, 'prueba.txt', 'Id', 'Pos01');

        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicketException500'));
        controller.obtenerAnexo(authorizationToken, caso, 'prueba.txt', 'Id', 'Pos01');

        Test.stopTest();
    }
}