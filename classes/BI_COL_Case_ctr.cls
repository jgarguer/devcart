/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-06      Daniel ALexander Lopez (DL)     New Controller PB
*************************************************************************************/
public with sharing class BI_COL_Case_ctr {
	
	  public Datetime datefechaInicial;
	  public Datetime datefechaFinal;
	  public Date fechaAuxiliar;
   //Fecha Compromiso Cliente = Fecha de Radicación + dias promesa; dias habiles.
   // 
	
  //  @Future(callout=true)
	public static void CaseGescliente( List<Id> lstIdCases )
	{

	  list<Case> lstCase;

	  try{
			lstCase =[
			Select 	 BI_COL_Fecha_asignacion__c,
					 BI_COL_Fecha_comprometida_oferta__c,
					 BI_COL_Fecha_Radicacion__c,
					 BI_COL_Promesa_Cliente_dias_calendario__c,
					 BI_COL_SLA_Radicacion__c 
			From 	Case 
			where ID In: lstIdCases limit 1];
	  }catch(Exception e)
	  {
		 System.debug('Error en la consulta ====\n'+ lstCase + '\n Error ======  '+e.getMessage());
	  }

	  try
	  {
		Case casoactualizar = new Case();
			System.debug('BI_COL_Promesa_Cliente_dias_calendario__c===== '+ lstCase[0].BI_COL_Promesa_Cliente_dias_calendario__c);
		if(lstCase.size() >0 && lstCase[0].BI_COL_Promesa_Cliente_dias_calendario__c != null && lstCase[0].BI_COL_Promesa_Cliente_dias_calendario__c != '')
		{
			System.debug('BI_COL_Fecha_Radicacion__c ===== '+ lstCase[0].BI_COL_Fecha_Radicacion__c);
			System.debug('BI_COL_Promesa_Cliente_dias_calendario__c===== '+ lstCase[0].BI_COL_Promesa_Cliente_dias_calendario__c);
			//fechaAuxiliar = new date;
			//Calcula el nro de dias en segundos nro dias*nro de horas laborales* 60 minutos * 60 segundos
			Timezone tz = Timezone.getTimeZone('America/Bogota');

			double nroDiasSeg=Integer.valueOf(lstCase[0].BI_COL_Promesa_Cliente_dias_calendario__c)*10*60*60;
			casoactualizar.id = lstCase[0].id;
			casoactualizar.BI_COL_Fecha_Compromiso_Cliente__c = calcularFechaSolucion(lstCase[0].BI_COL_Fecha_Radicacion__c.dateGMT(),nroDiasSeg);
			System.debug('Fecha Calculada ===== '+ casoactualizar.BI_COL_Fecha_Compromiso_Cliente__c);
			update casoactualizar;
			}else	
			{
				System.debug('Error no entro al if\n' +lstCase);
			}

		}catch(Exception e)
		{
			System.debug('Error actualizando el caso ===== '+ e.getMessage());
			System.debug('Error actualizando el caso ===== '+ e.getMessage());
		}

	}


	@InvocableMethod
	public static void envioCasesLst(List<Case> lstCase)
	{
	  List<String> caseIds = new List<String>();
	  for (Case Casos : lstCase) 
	  {
		  caseIds.add(Casos.Id);
	  }
	  CaseGescliente(caseIds);
	}

	public static BusinessHours objBH
	{
		get
		{
			if(objBH == null)
			{
				objBH =
				[
					select id, Name, MondayStartTime, MondayEndTime,
						TuesdayStartTime, TuesdayEndTime, WednesdayStartTime, WednesdayEndTime,
						ThursdayStartTime, ThursdayEndTime, FridayStartTime, FridayEndTime
					from BusinessHours
					where Name = :'Horario oficina COL'
				];
			}
			
			return objBH;
		}
		set;
	}

	public static Datetime calcularFechaSolucion( Datetime dttInicial, Double intNroSegundos )
	{
		Datetime dttRespuesta = null;
		system.debug( '\n\nCALCULAR FECHA DE SOLUCION\n\n\tFecha Incial: ' + dttInicial + '\n\tNro. Segundos: ' + intNroSegundos + '\n\n' );
	
		if( dttInicial != null && intNroSegundos != null )
		{
			try
			{
				Long lonTime = (Long)intNroSegundos * 1000L;
				dttRespuesta = BusinessHours.add( objBH.Id, dttInicial, lonTime  );
			}
			catch( system.exception e )
			{
				//Si se genera una excepción hace el calculo de días calendario para permitir el normal procesamiento
				dttRespuesta = dttInicial.addDays( Integer.valueOf( intNroSegundos / ( 10 * 60 * 60 ) ) );
			}
		
		}
	
		return dttRespuesta;
	}

}