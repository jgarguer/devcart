public class PCA_Chatter_Controller {
    public static final String MOSTRAR_NONE = '0';
    
	public String groupName{get;set;}
    //public String userId{get;set;}
    //public String selectedGroup {get;set;}
    public String groupValue {get;set;}
    //public List<SelectOption> groupList {get;set;}
    //public boolean booleanFeed {get;set;}
    public List<TableRecord> groupFileRecords {get;set;}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        ?
    Company:       New Enery Aborda
    Description:   Load a Chatter Group
    
    History:
    <Date>            <Author>              <Description>
    ??/??/????        ?                     Initial version
	01/02/2016		  Jose Miguel Fierro	Code Fixes & General Improvements
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public PCA_Chatter_Controller(){
        //Inicializar parámetros GET
        groupName = ApexPages.currentPage().getParameters().get('groupName'); 
        system.debug('[PCA_Chatter_Controller controller] groupName: ' +groupName);
        
        
        // NEA-JMF-01/02/2016: Commented out because the results of a query should be placed into a list,
        // not directly into an object, unless one knows for sure the query will *never, ever* return empty
        //CollaborationGroup obtainedGroup = [SELECT Id, Name FROM CollaborationGroup where Name =: groupName LIMIT 1];
        List<CollaborationGroup> obtainedGroups = [SELECT Id, Name FROM CollaborationGroup where Name =: groupName LIMIT 1];
        system.debug('[PCA_Chatter_Controller controller] obtainedGroups: ' +obtainedGroups);
        
        if(obtainedGroups.size() > 0) {
            groupValue = obtainedGroups[0].Id;
            system.debug('[PCA_Chatter_Controller controller] groupValue: ' +groupValue);
            
            List<ContentDocumentLink> groupFilesLinks = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: groupValue];
            system.debug('[PCA_Chatter_Controller controller] groupFilesLinks: ' +groupFilesLinks);
            List<Id> idGroupFilesLinks = new List<Id>();
            for(ContentDocumentLink groupFileLink : groupFilesLinks){
                idGroupFilesLinks.add(groupFileLink.ContentDocumentId);
            }
            
            List<ContentVersion> groupFiles = [SELECT Id, ContentDocumentId, Title FROM ContentVersion WHERE ContentDocumentId =:idGroupFilesLinks];
            system.debug('[PCA_Chatter_Controller controller] groupFiles: ' +groupFiles);
            
            groupFileRecords = new List<TableRecord>();
            groupFileRecords = createTableRecordList(groupFiles);
        }
    }
    
    /**
	 * Method:           createTableRecordList
     * Description:      Obtiene los cases que se va a mostrar en la tabla   
     */
    public List<TableRecord> createTableRecordList(List<ContentVersion> fileList) {
        System.debug('[PCA_Chatter_Controller.createTableRecordList] fileList: ' + fileList);
        
        List<TableRecord> recordList = new List<TableRecord>();
        for (ContentVersion mfile : fileList) {
            recordList.add(caseToTableRecord(mfile));
        }
        System.debug('[PCA_Chatter_Controller.createTableRecordList] recordList: ' + recordList);
        return recordList;
    }
    /**
	 * Method:           caseToTableRecord
     * Description:      Obtiene la información del case que se va a mostrar en la tabla   
     */
    public TableRecord caseToTableRecord(ContentVersion mfile) {
        System.debug('[PCA_Cases.caseToTableRecord] mfile: ' + mfile);
        
        TableRecord tr = new TableRecord(mfile.Id, mfile.Title, mfile.ContentDocumentId);
        
        System.debug('[PCA_Cases.caseToTableRecord] tr: ' + tr);
        return tr;
    }
    
    /***********************************************
     *    INNER CLASSES
     ***********************************************/
    /**
     * Class:            TableRecord
     * Description:      Contiene la información que se va a mostrar en la tabla   
     */
    @testVisible
    class TableRecord {
        public String Id {get; set; }
        public String Title {get; set;}
        public String ContentDocumentId {get; set;}
        
        /*
     	* Method:         TableRecord
     	* Description:    Representa cada registro fijo a mostrar en la tabla de service units.
	    */
        public TableRecord (String Id, String Title, String ContentDocumentId){
            this.Id = Id;
            this.Title = Title;
            this.ContentDocumentId = ContentDocumentId;
        }
    }
}