public with sharing class TGS_Account_Hierarchy {
    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Angel Galindo, Carlos Campillo
    Company:       Deloitte
    Description:   This class contains method related with Account object.
                    Class invoked by the trigger Account.
    
    History

    <Date>            <Author>                      <Description>
    6/04/2015        Miguel Angel Galindo           Initial version
    --/03/2016       Jose Miguel Fierro             Added UpdateSites class and helper updateAllHeirarchySites method
    31/03/2016       Jose Miguel Fierro             Bug fixes for UpdateSites subclass.
    30/11/2016       Alvaro García                  Change that it could be call in update and insert trigger
    16/06/2017        Guillermo Muñoz               Deprecated references to TGS_Aux_Cost_Center__c (Optimización)
--------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    
    /*
     * Method to copy the auxiliar hierarchy 
     */
    public static void copyHierarchy(List<Account> listNew, List<Account> listOld){

        List<Account> listAccountNew = new List<Account>();
        Set<Id> parentIdList = new Set<Id>();

        if (listOld == null) {
            for (Integer i = 0; i<listNew.size(); i++) {
                if (listNew[i].ParentId != null) {
                    listAccountNew.add(listNew[i]);
                    parentIdList.add(listNew[i].ParentId);
                }
            }
        }
        else {
            for (Integer i = 0; i<listNew.size(); i++) {
                if (listNew[i].ParentId != null && listNew[i].ParentId != listOld[i].ParentId) {
                    listAccountNew.add(listNew[i]);
                    parentIdList.add(listNew[i].ParentId);
                }
            }
        }

        if (!listAccountNew.isEmpty()) {

            Id idHolding         = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_HOLDING_ACCOUNT);
            Id idCustomerCountry = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_CUSTOMER_COUNTRY_ACCOUNT);
            Id idLegalEntity     = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_LEGAL_ENTITY_ACCOUNT);
            Id idBusinessUnit    = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_BUSINESS_UNIT_ACCOUNT);
            Id idCostCenter      = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_COST_CENTER_ACCOUNT);

            System.debug('@@@@ListAccountNew: '+listAccountNew.size());
            
            List<Account> listAccUpdate = new List<Account>();

            
            System.debug('@@@@ParentIdList: '+parentIdList);
            
            List<Account> listParentInfoAccount = [SELECT Id, RecordTypeId, TGS_Aux_Holding__c, TGS_Aux_Customer_Country__c,
                                        TGS_Aux_Legal_Entity__c, TGS_Aux_Business_Unit__c/*, TGS_Aux_Cost_Center__c*/
                                        FROM Account
                                        WHERE Id IN :parentIdList];
            
            system.debug('@@@@ListParentInfoAccount: '+listParentInfoAccount);
            


            for(Account accountNew: listAccountNew){
                //if(accountNew.ParentId!=null){ //Checking an Account always have a parent
                    for(Account acParent : listParentInfoAccount){
                        if(accountNew.ParentId == acParent.Id){ //Checking if the current Account belongs to this parent
                            system.debug('@@@@ Copiando Jerarquia');
                            //Setting the ParentId to the parent of the actual account
                            if(acParent.RecordTypeId == idHolding){
                                accountNew.TGS_Aux_Holding__c = acParent.Id;
                                system.debug('@@@@ Copiando Holding');
                            }else if(acParent.RecordTypeId == idCustomerCountry){
                                accountNew.TGS_Aux_Customer_Country__c = acParent.Id;
                                accountNew.TGS_Aux_Holding__c = acParent.TGS_Aux_Holding__c;
                                system.debug('@@@@ Copiando CuCo');
                            }else if(acParent.RecordTypeId == idLegalEntity){
                                accountNew.TGS_Aux_Legal_Entity__c = acParent.Id;
                                accountNew.TGS_Aux_Holding__c = acParent.TGS_Aux_Holding__c;
                                accountNew.TGS_Aux_Customer_Country__c = acParent.TGS_Aux_Customer_Country__c;
                                system.debug('@@@@ Copiando LE');
                            }else if(acParent.RecordTypeId == idBusinessUnit){
                                accountNew.TGS_Aux_Business_Unit__c = acParent.Id;
                                accountNew.TGS_Aux_Holding__c = acParent.TGS_Aux_Holding__c;
                                accountNew.TGS_Aux_Customer_Country__c = acParent.TGS_Aux_Customer_Country__c;
                                accountNew.TGS_Aux_Legal_Entity__c = acParent.TGS_Aux_Legal_Entity__c;
                                system.debug('@@@@ Copiando BU');
                            }else{                        
                                //Copy the Aux Hierarchy to the current Account
                                accountNew.TGS_Aux_Holding__c = acParent.TGS_Aux_Holding__c;
                                accountNew.TGS_Aux_Customer_Country__c = acParent.TGS_Aux_Customer_Country__c;
                                accountNew.TGS_Aux_Legal_Entity__c = acParent.TGS_Aux_Legal_Entity__c;
                                accountNew.TGS_Aux_Business_Unit__c = acParent.TGS_Aux_Business_Unit__c;
                           }    
                            //update acParent;
                            //Provoca una dependencia circular
                        }
                    }
                    
                    System.debug(' Holding: '+accountNew.TGS_Aux_Holding__c+' Customer Country: '+accountNew.TGS_Aux_Customer_Country__c+' LE: '+accountNew.TGS_Aux_Legal_Entity__c+' BU: '+accountNew.TGS_Aux_Business_Unit__c);
                //}
            }
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       New Energy Aborda
     Description:   Modifies searchable information of the sites of the hierarchy so user can find sites

     History: 
    
     <Date>                     <Author>                    <Change Description>
     17/03/2016                 Jose Miguel Fierro          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void fillSitesInformation(Map<Id, Account> olds, Map<Id, Account> news) {
        Map<Id, String> mapAccountNames = new Map<Id, String>(); // Holding.Id => Holding.Name
        Id rtAcc2Check = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, 'TGS_Holding');

        Account nAcc, oAcc;
        for(Id idAcc : news.keySet()) {
            nAcc = news.get(idAcc);
            oAcc = olds != null ? olds.get(idAcc) : new Account(Name = nAcc.Name);

            System.debug('[TGS_Account_Hierarchy.fillSitesInformation] RT: ' + nAcc.RecordTypeId + ' vs Holding (' + rtAcc2Check + ')');
            if(nAcc.RecordTypeId == rtAcc2Check && (nAcc.Name != oAcc.Name) && nAcc.TGS_Es_MNC__c) {
                mapAccountNames.put(nAcc.Id, nAcc.Name);
            }
        }
        System.debug('[TGS_Account_Hierarchy.fillSitesInformation] Accounts: ' + mapAccountNames);
        if(!mapAccountNames.isEmpty()) {
            System.enqueueJob(new UpdateSites(mapAccountNames));
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       New Energy Aborda
     Description:   Class that upddates sites of the given hierarchies, to fill in required information to make them searchable

     History: 
    
     <Date>                     <Author>                    <Change Description>
     31/03/2016                 Jose Miguel Fierro          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public class UpdateSites implements Queueable {
        @testVisible private Map<Id, String> mapAccountNames;
        @testVisible private String lastId;
        @testVisible private Integer maxQueriedSites = 100; //5000; // Adjust to avoid callout limit

        @testVisible
        private UpdateSites(Map<Id, String> mapAccountNames, String lastId) {
            if(mapAccountNames == null) mapAccountNames = new Map<Id, String>();
            this.mapAccountNames = mapAccountNames;
            this.lastId = lastId;
        }
        public UpdateSites(Map<Id, String> mapAccountNames) {
            this(mapAccountNames, '');
        }
        public void execute(QueueableContext qc) {
            System.debug('[TGS_Account_Hierarchy.UpdateSites.execute] Accounts: ' + mapAccountNames);

            Id rtBusinessUnit = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_BUSINESS_UNIT_ACCOUNT);
            Set<Id> setIds = mapAccountNames.keySet();
            List<String> lstNames = mapAccountNames.values();
            String query = 'SELECT Id, TGS_Main_Account_Name__c, BI_Cliente__r.TGS_Aux_Holding__c, BI_Cliente__r.Parent.Parent.ParentId FROM BI_Punto_de_instalacion__c '
                            + 'WHERE (BI_Cliente__r.TGS_Aux_Holding__c IN :setIds OR BI_Cliente__r.Parent.Parent.ParentId = :setIds) '
                            + 'AND BI_Cliente__r.RecordTypeId = :rtBusinessUnit ' // Because if we try to update a Site associated with a non-BU that Site will fail
                            + 'AND TGS_Main_Account_Name__c NOT IN :lstNames ';
            if(lastId != null) {
                query += 'AND Id > :lastId ';
            }/**/
            query += 'ORDER BY Id ASC LIMIT :maxQueriedSites';
            System.debug('Query: ' + query);

            List<BI_Punto_de_instalacion__c> lstSites = Database.query(query);
            System.debug('[TGS_Account_Hierarchy.UpdateSites.execute] Sites to update: ' + lstSites);
            
            List<BI_Punto_de_instalacion__c> lst2Update = new List<BI_Punto_de_instalacion__c>();

            for(BI_Punto_de_instalacion__c site : lstSites) {
                Id accId = site.BI_Cliente__r.TGS_Aux_Holding__c != null? site.BI_Cliente__r.TGS_Aux_Holding__c : site.BI_Cliente__r.Parent.Parent.ParentId;
                if(site.TGS_Main_Account_Name__c != mapAccountNames.get(accId)){
                    site.TGS_Main_Account_Name__c = mapAccountNames.get(accId);
                    lst2Update.add(site);
                }
            }

            List<Database.SaveResult> results = Database.update(lst2Update, false);

            // Capture and report errors
            try {
                // SaveResult does not contain an Id if there was an error -> use lst2Update
                lastId = (lst2Update.size() > 0 && lst2Update.get(lst2Update.size() - 1) != null) ? lst2Update.get(lst2Update.size() - 1).Id : '';
                // JMF 04/04/2016 - Replaced with call to generateMultiple_BILOG()
                /*List<BI_Log__c> lstErrorsLogs = new List<BI_Log__c>();
                for(Integer iter = 0; iter < results.size(); iter++) {
                    Database.SaveResult sres = results[iter];
                    if(!sres.isSuccess()) {
                        System.debug(LoggingLevel.ERROR, 'Failed to update Site (' + sres.getId() + ') Reason:');
                        BI_Log__c log = new BI_Log__c(BI_Asunto__c='TGS_Account_Hierarchy.UpdateSites.execute', BI_Bloque_funcional__c='TGS', BI_Tipo_de_componente__c='Class',
                                                    BI_Descripcion__c='Update failed for Id: (' + lst2Update[iter] + '). Exceptions:');

                        for(Database.Error err : sres.getErrors()) {
                            System.debug(LoggingLevel.ERROR, '\n' + err.getStatusCode() + ': ' + err.getMessage() +(err.getFields()!= null && err.getFields().size() > 0 ? '\n\tAffected by fields: ' + err.getFields():''));
                            log.BI_Descripcion__c += '\n'+err.getStatusCode() +': ' + err.getMessage();
                        }
                        lstErrorsLogs.add(log);
                    }
                }
                if(lstErrorsLogs.size() > 0) insert lstErrorsLogs;*/
                generateMultiple_BILOG(results, lst2Update, 'Update', 'TGS_Account_Hierarchy.UpdateSites.execute', 'TGS', 'Apex Job');
            } catch (Exception ex) {
                System.debug('Exception: ' + ex);
                BI_LogHelper.generate_BILog('TGS_Account_Hierarchy.UpdateSites.execute', 'TGS', ex, 'Apex Job');
            }
            if(lstSites.size() == maxQueriedSites) {
                System.debug('[TGS_Account_Hierarchy.UpdateSites.execute] There are more sites to process!');
                System.debug('Repeating job to process more sites. New job id: ' + System.enqueueJob(new UpdateSites(mapAccountNames, lastId))); // Returns null in test
            } else {
                System.debug('[TGS_Account_Hierarchy.UpdateSites.execute] There are NO more sites to process!');
            }
        }
    }

    // Method to facilitate updating the sites of all hierarchies at once
    public static Id updateAllHeirarchySites() {
        Map<Id, String> mapAccountNames = new Map<Id, String>();
        for(Account acc : [SELECT Id, Name FROM Account WHERE TGS_Es_MNC__c =true AND RecordType.DeveloperName = :Constants.RECORD_TYPE_HOLDING_ACCOUNT]) {
            mapAccountNames.put(acc.Id, acc.Name);
        }
        System.debug('Queried AccountIds: ' + mapAccountNames.keySet());
        return System.enqueueJob(new UpdateSites(mapAccountNames));
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       New Energy Aborda
     Description:   Method that generates multiple BI_Logs at once.
                    TODO: Merge this method into BI_LogHelper

     History: 
    
     <Date>                     <Author>                    <Change Description>
     04/04/2016                 Jose Miguel Fierro          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void generateMultiple_BILOG(Database.SaveResult[] results, List<SObject> objs, String operation, String subject, String fBlock, String component) {
        // Passing in the list that was just proccessed is necessary, because Database.SaveResults doesn't contain the Id if the operation failed
        List<BI_Log__c> lstErrorsLogs = new List<BI_Log__c>();
        for(Integer iter = 0; iter < results.size(); iter++) {
            Database.SaveResult sres = results[iter];
            if(!sres.isSuccess()) {
                String descr = operation + ' failed for Id: (' + objs[iter].Id + '). Exceptions:';
                BI_Log__c log = new BI_Log__c(BI_Asunto__c = subject, BI_Bloque_funcional__c = fBlock, BI_Tipo_de_componente__c = component);

                for(Database.Error err : sres.getErrors()) {
                    descr += '\n'+err.getStatusCode() +': ' + err.getMessage() + (err.getFields()!= null && err.getFields().size() > 0 ? '.: ' + err.getFields():'.');
                }
                System.debug(LoggingLevel.ERROR, descr);
                log.BI_Descripcion__c = descr;
                lstErrorsLogs.add(log);
            }
        }
        if(lstErrorsLogs.size() > 0) insert lstErrorsLogs;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Juan Carlos Terrón
        Company:       New Energy Aborda
        Description:   Generates IntegrationId field on CostCenter Accounts depending on the other CostCenter Accounts nested by Country.
        
        IN:            Trigger.newMap
        OUT:           Void
        
        History:   
        <Date>                  <Author>                <Change Description>        
        19/10/2016              JC Terrón               Initial Version
        27/04/2018              Álvaro López            Changed integration id generation
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    public static void generate_IntegrationId(List<Account> triggerAccounts){

        System.debug('MDN--ENTRO en gen_IntId');
        String  INTEGRATION_ID        = '';
        String  INTEGRATION_ID_STRING = '';
        Integer INTEGRATION_ID_NUMBER = 0;

        Set<Id> set_Holding_ID          = new Set<Id>();
        Set<Id> set_CustomerCountry_ID  = new Set<Id>();

        Map<Id,Account>     newAccounts     = new Map<Id,Account>();
        Map<Id,Account>     map_Accounts    = new Map<Id,Account>();
        Map<String,Integer> map_Autonumeric = new Map<String,Integer>();

        for(Account  ACC : triggerAccounts)
        {
            if(ACC.TGS_Aux_Holding__c!=null && ACC.TGS_Aux_Customer_Country__c!=null && ACC.TGS_IntegrationID__c == null)
            {
                set_Holding_ID.add(ACC.TGS_Aux_Holding__c);
                set_CustomerCountry_ID.add(ACC.TGS_Aux_Customer_Country__c);
            }
        }
        
        for(Account ACC :
            [
                SELECT  Id, TGS_Aux_Holding__r.Name, TGS_Aux_Customer_Country__r.Name,TGS_Aux_Customer_Country__r.BI_Country__c,
                        TGS_IntegrationID__c,TGS_Aux_Holding__c,TGS_Aux_Customer_Country__c, RecordType.DeveloperName
                FROM    Account
                WHERE   TGS_Aux_Holding__c          != null                                     AND
                        TGS_Aux_Customer_Country__c != null                                     AND     
                        TGS_Aux_Holding__c          IN :set_Holding_ID                          AND
                        TGS_Aux_Customer_Country__c IN :set_CustomerCountry_ID                  AND
                        RecordType.DeveloperName    = :Constants.RECORD_TYPE_TGS_COST_CENTER    
            ]
            )
        {
            if(ACC.TGS_IntegrationID__c==null)
            {
                newAccounts.put(ACC.Id, ACC);
            }
            else
            {
                map_Accounts.put(ACC.Id, ACC);
            }
        }
        System.debug('map_Accounts: ' + map_Accounts);
        System.debug('newAccounts: ' + newAccounts);
        if(newAccounts!=null && !newAccounts.isEmpty())
        {
            for(Account ACC : map_Accounts.values())
            {
                if(ACC.TGS_IntegrationId__c.subString(5).isNumeric())
                {
                    if(map_Autonumeric.containsKey(ACC.TGS_Aux_Customer_Country__c))
                    {
                        // Álvaro López 27/04/2018
                        INTEGRATION_ID_NUMBER = Integer.valueOf(ACC.TGS_IntegrationID__c.subString(5));
                        System.debug('INTEGRATION_ID_NUMBER: ' + INTEGRATION_ID_NUMBER);
                        if(map_Autonumeric.get(ACC.TGS_Aux_Customer_Country__c)<INTEGRATION_ID_NUMBER)
                        {
                            map_Autonumeric.put(ACC.TGS_Aux_Customer_Country__c,INTEGRATION_ID_NUMBER);
                        }
                    }
                    else if(!map_Autonumeric.containsKey(ACC.TGS_Aux_Customer_Country__c))
                    {
                        // Álvaro López 27/04/2018
                        INTEGRATION_ID_NUMBER = Integer.valueOf(ACC.TGS_IntegrationID__c.subString(5));
                        System.debug('INTEGRATION_ID_NUMBER 2: ' + INTEGRATION_ID_NUMBER);
                        map_Autonumeric.put(ACC.TGS_Aux_Customer_Country__c,INTEGRATION_ID_NUMBER);
                    }
                }
            }
            for(Account ACC : newAccounts.values())
            {
                if(!map_Autonumeric.containsKey(ACC.TGS_Aux_Customer_Country__c) && ACC.TGS_Aux_Holding__r != null && ACC.TGS_Aux_Customer_Country__r != null && ACC.TGS_Aux_Customer_Country__r.BI_Country__c!= null)
                {
                    INTEGRATION_ID = ACC.TGS_Aux_Holding__r.Name.left(2).toUpperCase() + ACC.TGS_Aux_Customer_Country__r.BI_Country__c.left(2).toUpperCase() + 'C0001';
                    ACC.TGS_IntegrationID__c = INTEGRATION_ID;
                    map_Autonumeric.put(ACC.TGS_Aux_Customer_Country__c, 1);
                }
                else if(ACC.TGS_Aux_Holding__r != null && ACC.TGS_Aux_Customer_Country__r != null && ACC.TGS_Aux_Customer_Country__r.BI_Country__c!= null)
                {
                    INTEGRATION_ID_NUMBER = map_Autonumeric.get(ACC.TGS_Aux_Customer_Country__c) + 1;
                    INTEGRATION_ID_STRING = ACC.TGS_Aux_Holding__r.Name.left(2).toUpperCase() + ACC.TGS_Aux_Customer_Country__r.BI_Country__c.left(2).toUpperCase() + 'C';
                    if(INTEGRATION_ID_NUMBER < 10)
                    {
                        INTEGRATION_ID = INTEGRATION_ID_STRING + '000' + String.valueOf(INTEGRATION_ID_NUMBER);
                    }
                    else if(INTEGRATION_ID_NUMBER >= 10 && INTEGRATION_ID_NUMBER < 100)
                    {
                        INTEGRATION_ID = INTEGRATION_ID_STRING + '00' + String.valueOf(INTEGRATION_ID_NUMBER);
                    }
                    else if(INTEGRATION_ID_NUMBER >= 100 && INTEGRATION_ID_NUMBER < 1000)
                    {
                        INTEGRATION_ID = INTEGRATION_ID_STRING + '0' + String.valueOf(INTEGRATION_ID_NUMBER);
                    }
                    else
                    {
                        INTEGRATION_ID = INTEGRATION_ID_STRING + String.valueOf(INTEGRATION_ID_NUMBER);
                    }
                    map_Autonumeric.put(ACC.TGS_Aux_Customer_Country__c,INTEGRATION_ID_NUMBER);
                    ACC.TGS_IntegrationID__c = INTEGRATION_ID;
                }
            }
        }
        if(newAccounts.size()!=0)update newAccounts.values();
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Juan Carlos Terrón
        Company:       New Energy Aborda
        Description:   Generates IntegrationId field on CostCenter Accounts depending on the other CostCenter Accounts nested by Country.
        
        IN:            Trigger.newMap
        OUT:           Void
        
        History:   
        <Date>                  <Author>                <Change Description>        
        19/10/2016              JC Terrón               Initial Version
        23/03/2018              Álvaro López            Added BU and LE IntegrationId generation
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    /*public static void generate_IntegrationId(List<Account> triggerAccounts){

        System.debug('MDN--ENTRO en gen_IntId');
        String  INTEGRATION_ID        = '';
        String  INTEGRATION_ID_STRING = '';
        Integer INTEGRATION_ID_NUMBER = 0;

        Set<Id> set_Holding_ID          = new Set<Id>();
        Set<Id> set_CustomerCountry_ID  = new Set<Id>();

        Map<Id,Account>     newAccounts     = new Map<Id,Account>();
        Map<Id,Account>     map_Accounts    = new Map<Id,Account>();
        Map<String,Integer> map_Autonumeric = new Map<String,Integer>();

        for(Account  ACC : triggerAccounts)
        {
            if(ACC.TGS_Aux_Holding__c!=null && ACC.TGS_Aux_Customer_Country__c!=null && ACC.TGS_IntegrationID__c == null)
            {
                set_Holding_ID.add(ACC.TGS_Aux_Holding__c);
                set_CustomerCountry_ID.add(ACC.TGS_Aux_Customer_Country__c);
            }
        }
        
        for(Account ACC :
            [
                SELECT  Id, TGS_Aux_Holding__r.Name, TGS_Aux_Customer_Country__r.Name,TGS_Aux_Customer_Country__r.BI_Country__c,
                        TGS_IntegrationID__c,TGS_Aux_Holding__c,TGS_Aux_Customer_Country__c, RecordType.DeveloperName
                FROM    Account
                WHERE   TGS_Aux_Holding__c          != null                                     AND
                        TGS_Aux_Customer_Country__c != null                                     AND     
                        TGS_Aux_Holding__c          IN :set_Holding_ID                          AND
                        TGS_Aux_Customer_Country__c IN :set_CustomerCountry_ID                  AND
                        (RecordType.DeveloperName    = :Constants.RECORD_TYPE_TGS_COST_CENTER   OR
                         RecordType.DeveloperName    = :Constants.RECORD_TYPE_TGS_BUSINESS_UNIT OR
                         RecordType.DeveloperName    = :Constants.RECORD_TYPE_TGS_LEGAL_ENTITY)
            ]
            )
        {
            if(ACC.TGS_IntegrationID__c==null)
            {
                newAccounts.put(ACC.Id, ACC);
            }
            else
            {
                map_Accounts.put(ACC.Id, ACC);
            }
        }
        if(newAccounts!=null && !newAccounts.isEmpty())
        {
            for(Account ACC : map_Accounts.values())
            {
                if(ACC.TGS_IntegrationId__c.right(4).isNumeric())
                {
                    if(map_Autonumeric.containsKey(ACC.TGS_Aux_Customer_Country__c))
                    {
                        INTEGRATION_ID_NUMBER = Integer.valueOf(ACC.TGS_IntegrationID__c.right(4));
                        if(map_Autonumeric.get(ACC.TGS_Aux_Customer_Country__c)<INTEGRATION_ID_NUMBER)
                        {
                            map_Autonumeric.put(ACC.TGS_Aux_Customer_Country__c,INTEGRATION_ID_NUMBER);
                        }
                    }
                    else if(!map_Autonumeric.containsKey(ACC.TGS_Aux_Customer_Country__c))
                    {
                        INTEGRATION_ID_NUMBER = Integer.valueOf(ACC.TGS_IntegrationID__c.right(4));
                        map_Autonumeric.put(ACC.TGS_Aux_Customer_Country__c,INTEGRATION_ID_NUMBER);
                    }
                }
            }
            for(Account ACC : newAccounts.values())
            {
                if(!map_Autonumeric.containsKey(ACC.TGS_Aux_Customer_Country__c) && ACC.TGS_Aux_Holding__r != null && ACC.TGS_Aux_Customer_Country__r != null && ACC.TGS_Aux_Customer_Country__r.BI_Country__c!= null)
                {
                    if(ACC.RecordType.DeveloperName.equals(Constants.RECORD_TYPE_TGS_COST_CENTER)){
                        INTEGRATION_ID = ACC.TGS_Aux_Holding__r.Name.left(2).toUpperCase() + ACC.TGS_Aux_Customer_Country__r.BI_Country__c.left(2).toUpperCase() + 'C0001';
                    }
                    else if(ACC.RecordType.DeveloperName.equals(Constants.RECORD_TYPE_TGS_BUSINESS_UNIT)){
                        INTEGRATION_ID = ACC.TGS_Aux_Holding__r.Name.left(2).toUpperCase() + ACC.TGS_Aux_Customer_Country__r.BI_Country__c.left(2).toUpperCase() + 'B0001';
                    }
                    else if(ACC.RecordType.DeveloperName.equals(Constants.RECORD_TYPE_TGS_LEGAL_ENTITY)){
                        INTEGRATION_ID = ACC.TGS_Aux_Holding__r.Name.left(2).toUpperCase() + ACC.TGS_Aux_Customer_Country__r.BI_Country__c.left(2).toUpperCase() + 'L0001';
                    }
                    ACC.TGS_IntegrationID__c = INTEGRATION_ID;
                    map_Autonumeric.put(ACC.TGS_Aux_Customer_Country__c, 1);
                }
                else if(ACC.TGS_Aux_Holding__r != null && ACC.TGS_Aux_Customer_Country__r != null && ACC.TGS_Aux_Customer_Country__r.BI_Country__c!= null)
                {
                    INTEGRATION_ID_NUMBER = map_Autonumeric.get(ACC.TGS_Aux_Customer_Country__c) + 1;
                    if(ACC.RecordType.DeveloperName.equals(Constants.RECORD_TYPE_TGS_COST_CENTER)){
                        INTEGRATION_ID_STRING = ACC.TGS_Aux_Holding__r.Name.left(2).toUpperCase() + ACC.TGS_Aux_Customer_Country__r.BI_Country__c.left(2).toUpperCase() + 'C';
                    }
                    else if(ACC.RecordType.DeveloperName.equals(Constants.RECORD_TYPE_TGS_BUSINESS_UNIT)){
                        INTEGRATION_ID_STRING = ACC.TGS_Aux_Holding__r.Name.left(2).toUpperCase() + ACC.TGS_Aux_Customer_Country__r.BI_Country__c.left(2).toUpperCase() + 'B';
                    }
                    else if(ACC.RecordType.DeveloperName.equals(Constants.RECORD_TYPE_TGS_LEGAL_ENTITY)){
                        INTEGRATION_ID_STRING = ACC.TGS_Aux_Holding__r.Name.left(2).toUpperCase() + ACC.TGS_Aux_Customer_Country__r.BI_Country__c.left(2).toUpperCase() + 'L';
                    }

                    if(INTEGRATION_ID_NUMBER < 10)
                    {
                        INTEGRATION_ID = INTEGRATION_ID_STRING + '000' + String.valueOf(INTEGRATION_ID_NUMBER);
                    }
                    else if(INTEGRATION_ID_NUMBER >= 10 && INTEGRATION_ID_NUMBER < 100)
                    {
                        INTEGRATION_ID = INTEGRATION_ID_STRING + '00' + String.valueOf(INTEGRATION_ID_NUMBER);
                    }
                    else if(INTEGRATION_ID_NUMBER >= 100 && INTEGRATION_ID_NUMBER < 1000)
                    {
                        INTEGRATION_ID = INTEGRATION_ID_STRING + '0' + String.valueOf(INTEGRATION_ID_NUMBER);
                    }
                    else
                    {
                        INTEGRATION_ID = INTEGRATION_ID_STRING + String.valueOf(INTEGRATION_ID_NUMBER);
                    }
                    map_Autonumeric.put(ACC.TGS_Aux_Customer_Country__c,INTEGRATION_ID_NUMBER);
                    ACC.TGS_IntegrationID__c = INTEGRATION_ID;
                }
            }
        }
        if(newAccounts.size()!=0)update newAccounts.values();
    }*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Guillermo Muñoz
     Company:       Accenture - NEA
     Description:   When legal entity or customer country are inserted or modified, we have to send their contacts and sites to FullStack

     History: 
    
     <Date>                     <Author>                    <Change Description>
     30/08/2017                 Guillermo Muñoz             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
     public static void sendToFullStack(List <Account> news, Map <Id,Account> oldMap){

        try{

            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('Test');
            }

            Id rtLegalEntity  = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_LEGAL_ENTITY_ACCOUNT);
            Id rtCustomerCountry = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_CUSTOMER_COUNTRY_ACCOUNT);
            List <Account> toProccess = new List <Account>();
    
    
            if(oldMap == null){
    
                for(Account acc : news){
    
                    if((acc.RecordTypeId == rtLegalEntity || acc.RecordTypeId == rtCustomerCountry) && acc.TGS_Es_MNC__c && acc.ParentId != null && acc.TGS_Aux_Holding__c != null && acc.BI_Identificador_Externo__c == null){
    
                        toProccess.add(acc);
                    }
                }
            }
            else{
    
                for(Account acc : news){
                    System.debug('!!@@ parents ---->' + acc.ParentId + ' ---- ' + oldMap.get(acc.Id).ParentId);
                    System.debug('!!@@ MNC ---->' + acc.TGS_Es_MNC__c + ' ---- ' + oldMap.get(acc.Id).TGS_Es_MNC__c);
                    if(
                        acc.BI_Identificador_Externo__c == null && (acc.RecordTypeId == rtLegalEntity || acc.RecordTypeId == rtCustomerCountry) && acc.TGS_Aux_Holding__c != null && acc.TGS_Es_MNC__c && acc.ParentId != null &&
                        ((acc.TGS_Es_MNC__c != oldMap.get(acc.Id).TGS_Es_MNC__c) || (acc.ParentId != oldMap.get(acc.Id).ParentId))
                     ){
        
                        toProccess.add(acc);
                    }
                }
            }
    
            System.debug('!!@@ toProccess ----> ' + toProccess.size());
            if(!toProccess.isEmpty()){
    
                List <Account> lst_acc = [SELECT Id, TGS_Aux_Holding__r.BI_Identificador_Externo__c, Parent.TGS_Es_MNC__c, RecordType.DeveloperName FROM Account WHERE Id IN: toProccess];
                SOQLHelper soqlHelp = new SOQLHelper();
                lst_acc = (List <Account>)soqlHelp.getQuery('SELECT Id, TGS_Aux_Holding__r.BI_Identificador_Externo__c, Parent.TGS_Es_MNC__c, RecordType.DeveloperName FROM Account WHERE Id IN: auxiliar1', toProccess);
    
    
                if(!lst_acc.isEmpty()){
    
                    Set <Id> set_idsToProcess = new Set <Id>();
    
                    for(Account acc : lst_acc){
                        
                        if(acc.TGS_Aux_Holding__r.BI_Identificador_Externo__c != null && ((acc.RecordType.DeveloperName == Constants.RECORD_TYPE_LEGAL_ENTITY_ACCOUNT && acc.Parent.TGS_Es_MNC__c) || acc.RecordType.DeveloperName == Constants.RECORD_TYPE_CUSTOMER_COUNTRY_ACCOUNT))
                        {
                            set_idsToProcess.add(acc.Id);
                        }
                    }
                    System.debug('!!@@ set_idsToProcess ----> ' + set_idsToProcess.size());
    
                    Boolean wasFired = NETriggerHelper.getTriggerFired('JobFASTAccount');
                    System.debug('!!@@ wasFired ----> ' + wasFired);
    
                    if(!set_idsToProcess.isEmpty() && !wasFired){
    
                        BI_AccountHelper.prepareHierarchy(set_idsToProcess);
                        NETriggerHelper.setTriggerFired('JobFASTAccount');
                    }
                }
            }
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('TGS_Account_Hierarchy.sendToFullStack', 'BI_EN', exc, 'Trigger');
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture - New Energy Aborda
    Description:   Class to avoid with sharing class definition in SOQL queries
    
    History:
    
    <Date>            <Author>              <Description>
    27/09/2017        Guillermo Muñoz       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public without sharing class SOQLHelper{

        public List <Sobject> getQuery(String query, List <SObject> auxiliar1){

            return Database.query(query);
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián
    Company:       Accenture - New Energy Aborda
    Description:   Method to change TGS_AUX to null
    
    History:
    
    <Date>            <Author>              <Description>
    19/10/2017        Gawron, Julián      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void nullHierarchy(List<Account> news, List<Account> olds){

        try {
            if(BI_TestUtils.isRunningTest()) {  
                    throw new BI_Exception('test');
            }

            for(Integer i = 0; i < news.size(); i++) {
                if ((olds != null)
                    && (news[i].ParentId != olds[i].ParentId)
                    && (news[i].ParentId == null)) {
                        
                    news[i].TGS_Aux_Holding__c = null;
                    news[i].TGS_Aux_Customer_Country__c = null;
                    news[i].TGS_Aux_Legal_Entity__c = null;
                    news[i].TGS_Aux_Business_Unit__c = null;
                        
                }   
            }
        }catch(Exception ex) {
            BI_LogHelper.generate_BILog('TGS_Account_Hierarchy.nullHierarchy', 'BI_EN', ex, 'Trigger');
        }       
    }
}