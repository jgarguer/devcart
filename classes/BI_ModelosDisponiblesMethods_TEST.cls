@isTest
private class BI_ModelosDisponiblesMethods_TEST
{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_ModelosDisponiblesMethods class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    27/03/2015              Fernando Arteaga        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Fernando Arteaga
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_ModelosDisponiblesMethods.checkBolsaDineroHasOnlyOneRecordPerModel
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	27/03/2015				Fernando Arteaga        Initial version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void TestModelosDisponiblesMethods()
    {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    	List<Recordtype> lst_rt = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Libre' AND sObjectType = 'BI_Bolsa_de_Dinero__c'];
	    List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
		List<BI_Modelo__c> lst_modelo = new List<BI_Modelo__c>(); 
		List<BI_Modelos_disponibles__c> lst_modelosDisp = new List<BI_Modelos_disponibles__c>();
		
		BI_Bolsa_de_dinero__c bolsa = new BI_Bolsa_de_dinero__c(BI_Cliente__c = lst_acc[0].Id,
        														BI_Monto_Inicial__c = 1000000,
        														CurrencyIsoCode = 'EUR',
        														RecordtypeId = lst_rt[0].Id);

		insert bolsa;
		
        for (Integer i=0;i<200;i++)
        {
        	BI_Modelo__c modelo = new BI_Modelo__c(Name = 'Modelo' + i);
        	lst_modelo.add(modelo);
        }
        
        insert lst_modelo;
        
        for (Integer i=0;i<200;i++)
        {
	        BI_Modelos_disponibles__c md = new BI_Modelos_disponibles__c(BI_Bolsa_de_dinero__c = bolsa.Id,
			        													 BI_Modelo__c = lst_modelo[i].Id,
			        													 BI_Cantidad__c = 5);

	        lst_modelosDisp.add(md);
    	}
    	
    	insert lst_modelosDisp;
		
		BI_Modelos_disponibles__c md = new BI_Modelos_disponibles__c(BI_Bolsa_de_dinero__c = bolsa.Id,
		        													 BI_Modelo__c = lst_modelo[1].Id,
		        													 BI_Cantidad__c = 5);
		try
		{
			insert md;
		}
		catch (Exception e)
		{
			System.assert(e.getMessage().contains('La bolsa de dinero ya tiene un registro de Modelos Disponibles para ese modelo'));
		}
		
		BI_ModelosDisponiblesMethods.checkBolsaDineroHasOnlyOneRecordPerModel(null, null);

		Integer i = [SELECT count()
					FROM BI_Log__c
					WHERE BI_Asunto__c = 'BI_ModelosDisponiblesMethods.checkBolsaDineroHasOnlyOneRecordPerModel'
					AND BI_Bloque_funcional__c = 'BI_EN'
					AND BI_Tipo_de_componente__c = 'Trigger'
					LIMIT 1];

		System.assertEquals(i, 1);
		
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}