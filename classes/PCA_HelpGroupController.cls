public with sharing class PCA_HelpGroupController extends PCA_HomeController {
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Controller of PCA_ChatterCustom
    
    History:
    
    <Date>            <Author>            <Description>
    11/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public boolean reload {get;set;}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Constructor
    
    History:
    
    <Date>            <Author>            <Description>
    11/06/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  public PCA_HelpGroupController() {
  
    reload = true;
  
  }
  
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>            <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  public PageReference checkPermissions (){
    try{
      if(BI_TestUtils.isRunningTest()){
        throw new BI_Exception('test');
      }
      PageReference page = enviarALoginComm();
      return page;
    }catch (exception Exc){
       BI_LogHelper.generate_BILog('PCA_ChatterCustomController.checkPermissions', 'Portal Platino', Exc, 'Class');
       return null;
    }
  }

}