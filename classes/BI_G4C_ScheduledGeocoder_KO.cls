global class BI_G4C_ScheduledGeocoder_KO implements Schedulable {
    
    global void execute(SchedulableContext ctx) {
        
        
        DateTime fActual= datetime.now();
        fActual.addHours(-4);
        List<Account> accsSFKO = [SELECT Id, name,LastModifiedDate FROM Account WHERE BI_G4C_Estado_Coordenadas__c='Pendiente Regla' AND BI_Activo__c!='No' AND  LastModifiedDate<:fActual LIMIT 100];
        List<Id> listIds= new List<Id>();
        for(Account c: accsSFKO)
            listIds.add(c.Id);
        BI_AccountMethods.callGoogleForGeocoding(listIds);
        
    }
    
}