@isTest
public class CWP_ExportTicketsOrders_ctrl_Test{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    *
    * Author:        Belén Coronado
    *
    * Company:       everis
    *
    * Description:   ApexClass Test to CWP_ExportTicketsOrders_ctrl controller
    * History:
    * <Date>                          <Author>                                <Code>              <Change Description>
    * 23/05/2017                      Belén Coronado                                              Initial version
    */
    
    @testSetup
    static void setup(){
        List<Case> casesList = new List<Case>();
        for(Integer i=0; i<3; i++){
            Case newCase = new Case();
            newCase.Subject = 'subject ' + String.valueOf(i);
            newCase.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'TGS_Incident'].Id;
            newCase.Status = 'Assigned';
            casesList.add(newCase);
        }
        insert casesList;
    }
    @isTest
    static void testConstructor(){
        Test.startTest();
            /*PageReference pageRef = Page.CWP_ExportExcelTicketsOrders_Lightning;
            Test.setCurrentPage(pageRef);*/
            List<Case> cases = [SELECT Id, CaseNumber, RecordType.Name, Type, Status, CWP_textDate__c, TGS_Product_Tier_2__c, TGS_Product_Tier_3__c, CWP_KeyValue__c, Subject  FROM Case];
            Cache.Session.put('local.CWPexcelExport.cachedListaCasosToExport', cases);
            Cache.Session.put('local.CWPexcelExport.areTickets', true);
            Cache.Session.put('local.CWPexcelExport.queryToExport', 'SELECT Id, CreatedDate FROM Case');
            CWP_ExportTicketsOrders_ctrl constructor = new CWP_ExportTicketsOrders_ctrl();
            List<Case> onlyOneCase = new List<Case>();
            onlyOneCase.add(cases.get(0));
            Cache.Session.put('local.CWPexcelExport.areTickets', false);
            Cache.Session.put('local.CWPexcelExport.cachedListaCasosToExport', onlyOneCase);
            CWP_ExportTicketsOrders_ctrl constructorOrders = new CWP_ExportTicketsOrders_ctrl();
        Test.stopTest();
        
    }
}