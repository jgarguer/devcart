@isTest
public class BI_O4_BulkProgressController_TEST {
    
    static testMethod void bulkImportBatchProgressTest() 
    {
		
		CWP_AC_TestGenerateData data = new CWP_AC_TestGenerateData();
			  
		String bulkImportRequestId = System.currentPagereference().getParameters().put('BIRId',  data.bir.Id);
	
		BI_O4_BulkProgressController contr = new BI_O4_BulkProgressController();
		contr.loadBulkImportObject();
		contr.loadStatus();
		
    }
    static testMethod void bulkImportBatchProgressTest2() 
    {
		
		CWP_AC_TestGenerateData data = new CWP_AC_TestGenerateData();
			  
		String bulkImportRequestId = System.currentPagereference().getParameters().put('BIRId',  data.bir2.Id);
		String bulkImportRequestDescriptor = System.currentPagereference().getParameters().put('BIRId',  data.bir2.Bit2WinHUB__Description__c);
	    BI_O4_BulkProgressController contr = new BI_O4_BulkProgressController();
		contr.loadBulkImportObject();
		contr.loadStatus();
		
    }
	static testMethod void bulkImportBatchProgressTest3() 
    {
		
		CWP_AC_TestGenerateData data = new CWP_AC_TestGenerateData();
			  
		String bulkImportRequestId = System.currentPagereference().getParameters().put('BIRId',  data.bir3.Id);
        String bulkImportRequestDescriptor = System.currentPagereference().getParameters().put('BIRId',  data.bir2.Bit2WinHUB__Description__c);
        String bulkImportRequestStatus = System.currentPagereference().getParameters().put('Working',  data.bir2.Bit2WinHUB__Status__c);
        System.debug('==================================================================');
        System.debug('data.bir2');
        System.debug(data.bir2);
        System.debug(System.currentPagereference().getParameters());
        System.debug('=================================================================');
		
		BI_O4_BulkProgressController contr = new BI_O4_BulkProgressController();
		contr.loadBulkImportObject();
		contr.loadStatus();
		
    }
    public class TestData{

    }

}