public class TGS_fill_NeedConfiguration {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Marta García
     Company:       Deloitte
     Description:   Fill field TGS_Needs_Configuration for Case
     
     History

     <Date>            <Author>                      <Description>
     25/11/2015        Marta García                  Initial version
     19/01/2016        Pablo Oliva                   ListStatusMachine converted to constant
     28/03/2016        Jose Miguel Fierro            Added additional conditions in which to avoid recalculating the check
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    
    //POF 19-01-16 Converted to constant
    public static final List<TGS_Status_machine__c> ListStatusMachine = [SELECT TGS_Needs_Configuration__c, TGS_Service__c, TGS_Status__c, TGS_Status_reason__c, TGS_Service__r.Name  FROM TGS_Status_machine__c];
    
    public static void fillNeedConfiguration (List<Case> newCases, List<Case> oldCases){

        //POF 19-01-16 Converted to constant
        //List<TGS_Status_machine__c> ListStatusMachine = [SELECT TGS_Needs_Configuration__c, TGS_Service__c, TGS_Status__c, TGS_Status_reason__c, TGS_Service__r.Name  FROM TGS_Status_machine__c];
        Map<Id, Case> mapOldCases = new Map<Id, Case>(oldCases);

        for(Case cases : newCases) {
            // JMF 28/03/2016 If Case.Status or .StatusReason haven't changed, don't recalculate
            if(cases.Status == mapOldCases.get(cases.Id).Status && cases.TGS_Status_reason__c == mapOldCases.get(cases.Id).TGS_Status_reason__c) {
                continue;
            }
            for(TGS_Status_machine__c StatusMachine : ListStatusMachine) {

                if(String.isNotBlank(StatusMachine.TGS_Status_reason__c)){
                   
                    if((StatusMachine.TGS_Service__r.Name.equals(cases.TGS_Service__c)) &&
                       (StatusMachine.TGS_Status__c.equals(cases.Status)) &&
                       (StatusMachine.TGS_Status_reason__c.equals(cases.TGS_Status_reason__c))&& cases.Type!='Disconnect'){
                        cases.TGS_Needs_Configuration__c = StatusMachine.TGS_Needs_Configuration__c;
                        
                        break; // break for(ListStatusMachine)
                    }
                }else{
                    if((StatusMachine.TGS_Service__r.Name.equals(cases.TGS_Service__c)) && 
                       (StatusMachine.TGS_Status__c.equals(cases.Status))&& cases.Type!='Disconnect'){
                        
                        cases.TGS_Needs_Configuration__c = StatusMachine.TGS_Needs_Configuration__c;
                        break; // break for(ListStatusMachine)
                    }
                }
            }
        }
    }
}