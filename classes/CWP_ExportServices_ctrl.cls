public class CWP_ExportServices_ctrl {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis
Company:       Everis
Description:   Controller class for visualforce pages in charge of exporting results table as excel or pdf
Test Class:    CWP_OrderITemListExcel_ctrl_TEST

History:

<Date>                  <Author>                <Change Description>
23/02/2017              Everis                    Initial Version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public List<CWP_MyService_services_insrvt_controller.tableRecord>listToShow{get; set;}
    public List<List<CWP_MyService_services_insrvt_controller.tableRecord>> listToShowPag{get; set;}
    
    public String serviceUnit{get;set;} 
    
    public String service1{get;set;}
    public String service2{get;set;}
    public String service3{get;set;}
    
    public Boolean showProdCategor {get;Set;}
    public Map<String, String> appliedFilters {get;set;}
    public Map<String, String> mapLabel {get;set;}
    
    
    public list<string> filterByAttribute{get; set;}
    
    public List<String> filterByFieldOrderedList{get;set;}
    
    //public String orientation = System.currentPagereference().getParameters().get('orientationValue');
    public String orientation {get;set;}
    public String headerPhoto{get;set;}
    /*
Developer Everis
Function: constructor
*/
    public CWP_ExportServices_ctrl(){
        
        //INI HEADER
        List<Document> lstDocument = [Select Id,Name,LastModifiedById from Document where Name = 'CWP_TelfonicaLogo' limit 1];
        string strOrgId = UserInfo.getOrganizationId();
        //string orgInst = URL.getSalesforceBaseUrl().getHost();
        string orgInst = URL.getCurrentRequestUrl().toExternalForm();
        orgInst = orgInst.substring(0, orgInst.indexOf('.')) + '.content.force.com';
        if(!lstDocument.isEmpty()){
            headerPhoto= URL.getSalesforceBaseUrl().getProtocol() + '://c.' + orgInst + '/servlet/servlet.ImageServer?id=' + lstDocument[0].Id + '&oid=' + strOrgId;
            headerPhoto= '/servlet/servlet.ImageServer?id=' + lstDocument[0].Id + '&oid=' + strOrgId;
            system.debug('url header: '+headerPhoto);
        }
        //FIN HEADER
        
        orientation = (String)Cache.Session.get('local.CWPexcelExport.pdfOrientation');
        
        list<CWP_MyService_services_insrvt_controller.tableRecord> theList = new list<CWP_MyService_services_insrvt_controller.tableRecord>();
        String query= (String)Cache.Session.get('local.CWPexcelExport.cachedFinalQuery');
        CWP_MyService_services_insrvt_controller.LightningContext c= (CWP_MyService_services_insrvt_controller.LightningContext)Cache.Session.get('local.CWPexcelExport.cachedlightningContext');
        List<NE__OrderItem__c> orderItems =Database.query(query);
        
        Map<Id, Set<CWP_MyService_services_insrvt_controller.SearchField>> dynamicAttMap = CWP_MyService_services_insrvt_controller.getDynamicAttributes(orderItems,c);
        
        for(NE__OrderItem__c oi:orderItems){
            CWP_MyService_services_insrvt_controller.TableRecord newTableRec = new CWP_MyService_services_insrvt_controller.TableRecord();
            newTableRec.identifier = oi.Id;
            newTableRec.name = oi.Name.replace('OI-','');
            if(dynamicAttMap.get(oi.Id)!=null) {
                newTableRec.dynamicAttributes = new List< CWP_MyService_services_insrvt_controller.SearchField>();
                newTableRec.dynamicAttributes.addAll(dynamicAttMap.get(oi.Id));
            }
            newTableRec.technicalBehaviour = oi.NE__CatalogItem__r.NE__Technical_Behaviour__c;
            theList.add(newTableRec);
        }
        
        filterByAttribute=(list<String>)Cache.Session.get('local.CWPexcelExport.cachedfilterByAttribute');
        filterByFieldOrderedList=(list<String>)Cache.Session.get('local.CWPexcelExport.cachedfilterByFieldOrderedList');
        
        
        system.debug('traza recuperada por cache    ' + theList);
        listToShow = new list<CWP_MyService_services_insrvt_controller.tableRecord>();
        listToShow.addAll(theList);
        
        
        //Paginamos la lista de 1000 en 1000
        Integer listMaxSize = 1000;
        listToShowPag= new List<List<CWP_MyService_services_insrvt_controller.tableRecord>>();
        Integer numSublists= listToShow.size()/1000+1;
        if(numSublists==0){
            listToShowPag.add(listToShow);
        }else{
            List<CWP_MyService_services_insrvt_controller.tableRecord>listP= new List<CWP_MyService_services_insrvt_controller.tableRecord>();
            for (integer i=0; i<numSublists;i++){
                for (integer j=0; j<=listMaxSize;j++){
                   
                    if(j+i*listMaxSize <listToShow.size())
                        listP.add(listToShow.get(j+i*listMaxSize));
                }
                listToShowPag.add(listP.clone());
                listP.clear();
            }
        }
        list<NE__OrderItem__c> list1;
        // Elementos provenientes de My Service
        if (!listToShow.isEmpty()) {
            String aux=listToShow[0].identifier;
            list1 = [SELECT Id,NE__CatalogItem__r.NE__ProductId__r.Name,NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name, NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name FROM NE__OrderItem__c WHERE Id =: aux];
        }else {
            list1=new list<NE__OrderItem__c>();
        }
        if(!list1.isEmpty()){
            showProdCategor = true;
            serviceUnit=list1[0].NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name;
            
            service1=list1[0].NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name ;
            service2=list1[0].NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name;
            service3=list1[0].NE__CatalogItem__r.NE__ProductId__r.Name;
        }else{
            showProdCategor = false;
            //Elementos provenientes de Service Tracking
            // El nombre del archivo indicará si son tickets u orders y la fecha de descarga
            serviceUnit = 'Service Tracking_';
            
            if(!theList.isEmpty() && theList.get(0).technicalBehaviour.equalsIgnoreCase('Orders')){
                service3 = 'Orders';
            }else{
                service3 = 'Tickets';
            }
            service3 += '_' + String.valueOf(Date.Today());
            
            // Filtros aplicados
            mapLabel = getMapLabel();
            appliedFilters = (Map<String, String>)Cache.Session.get('local.CWPexcelExport.cachedFilterApplied');
            system.debug('appliedFilters: ' + appliedFilters);
        }
        
        
        
        
        
        /* list<CWP_Installed_Services_Controller.tableRecord> theList = new list<CWP_Installed_Services_Controller.tableRecord>();


list<NE__OrderItem__c> list1 = [SELECT Id, NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name FROM NE__OrderItem__c WHERE Id =: listToShow[0].identifier];
*/
    }
    
    /*
Developer Everis
Function: method used to get the orientation of the page desired for the pdf
*/
    public String getOrientation(){
        system.debug('la orientación seleccionada es: ' + orientation);
        return orientation;
    }
    
    /*
Developer Everis
Function: method used to build a map with the labels of the header of the table
*/
    public Map<String, String> getMapLabel(){
        Map<String, String> mapToRet = new Map<String, String>();
        mapToRet.put('city', Label.TGS_CWP_CITY);
        mapToRet.put('country', Label.TGS_CWP_COUNT);
        mapToRet.put('keyValue', Label.CWP_KeyValue);
        mapToRet.put('lvl1', Label.TGS_CWP_T1);
        mapToRet.put('lvl2', Label.TGS_CWP_T2);
        mapToRet.put('lvl3', Label.TGS_CWP_T3);
        mapToRet.put('status', Label.TGS_CWP_STAT);
        mapToRet.put('type', Label.CWP_Type);
        return mapToRet;
    }
    
    
    
}