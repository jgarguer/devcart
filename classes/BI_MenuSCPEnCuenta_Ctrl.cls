/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Francisco Javier Ayllón Martínez
    Company:       Aborda
    Description:   Controller that manages if the SCP related list displays it's content or not
                   depending if the user is included on the required permissions sets.
    
    History:
    
    <Date>                    <Author>               <Description>
    15/06/2015                Francisco Ayllón       Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

global with sharing class BI_MenuSCPEnCuenta_Ctrl {

    public boolean SCPpermission {get; set;}

    public BI_menuSCPEnCuenta_Ctrl(ApexPages.StandardController controller){
        try{

            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

            List<PermissionSetAssignment> PSA_user = [SELECT PermissionSet.Name FROM PermissionSetAssignment WHERE Assignee.Id =: userInfo.getUserId() AND (PermissionSet.Name='SCP' OR PermissionSet.Name='BI_SCP')];
            if(PSA_user.size()==2){
                SCPpermission = true;
            }
            else{
                SCPpermission = false;
            }
            system.debug('SCPPermission: '+SCPpermission);
        }
        catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_menuSCPEnCuenta', 'BI_EN', exc , 'Controller VF');
        }
    }

}