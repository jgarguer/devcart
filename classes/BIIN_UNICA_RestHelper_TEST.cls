@isTest(seeAllData = false)
private class BIIN_UNICA_RestHelper_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        José Luis González Beltrán
Company:       HPE
Description:   Test method to manage the code coverage for BIIN_UNICA_RestHelper_TEST

<Date>             <Author>                             <Change Description>
17/05/2016         José Luis González Beltrán           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
  private final static String VALIDADOR_FISCAL = 'PER_RUC_UNICA_RestHelper_TEST';
  private final static String LE_NAME = 'TestAccount_RestHelper_TEST';
  private static final String correlatorId = '00000000';
  @testSetup static void dataSetup() 
  {
    BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
    // Test data load
    Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
    insert account;

    Account[] acc = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
    Case ticketTest = BIIN_UNICA_TestDataGenerator.createTicket(acc[0]);
    insert ticketTest; 
  }

  @isTest static void test_BIIN_UNICA_RestHelper_TEST() 
  {
    Test.startTest();

    Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
    Case ticketIncidenciaTecnica = [SELECT Id, CaseNumber, BI_Id_del_caso_legado__c FROM Case WHERE AccountId =: account.Id];

    BIIN_UNICA_Pojos.TicketRequestType emptyTicketRequest = new BIIN_UNICA_Pojos.TicketRequestType();

    BIIN_UNICA_Pojos.TicketRequestType ticketRequest0 = BIIN_UNICA_TestDataGenerator.getTicketRequest(VALIDADOR_FISCAL, correlatorId, 0, '', '1', '1');
    BIIN_UNICA_Pojos.TicketRequestType ticketRequest1 = BIIN_UNICA_TestDataGenerator.getTicketRequest(VALIDADOR_FISCAL, correlatorId, 1, null, '2', '2');
    BIIN_UNICA_Pojos.TicketRequestType ticketRequest2 = BIIN_UNICA_TestDataGenerator.getTicketRequest(VALIDADOR_FISCAL, correlatorId, 2, '', '3', '3');
    BIIN_UNICA_Pojos.TicketRequestType ticketRequest3 = BIIN_UNICA_TestDataGenerator.getTicketRequest(VALIDADOR_FISCAL, correlatorId, 3, null, '4', '4');

    Case emptyTicket = BIIN_UNICA_TicketManager.getTicketByCaseNumber('');
    System.assertEquals(emptyTicket, null);

    Case fullTicket = BIIN_UNICA_TicketManager.getTicketByCaseNumber(ticketIncidenciaTecnica.CaseNumber);

    fullTicket = BIIN_UNICA_TicketManager.getTicketByCorrelatorId('');
    fullTicket = BIIN_UNICA_TicketManager.getTicketByCorrelatorId(ticketIncidenciaTecnica.BI_Id_del_caso_legado__c);

    Case generatedTicket0 = BIIN_UNICA_RestHelper.generateTicket(new Case(), ticketRequest0);
    Case generatedTicket1 = BIIN_UNICA_RestHelper.generateTicket(new Case(), ticketRequest1);
    Case generatedTicket2 = BIIN_UNICA_RestHelper.generateTicket(new Case(), ticketRequest2);
    Case generatedTicket3 = BIIN_UNICA_RestHelper.generateTicket(new Case(), ticketRequest3);

    BIIN_UNICA_Pojos.TicketDetailType ticketDetail0 = BIIN_UNICA_RestHelper.generateTicketDetail(generatedTicket0);
    BIIN_UNICA_Pojos.TicketDetailType ticketDetail1 = BIIN_UNICA_RestHelper.generateTicketDetail(generatedTicket1);
    BIIN_UNICA_Pojos.TicketDetailType ticketDetail2 = BIIN_UNICA_RestHelper.generateTicketDetail(generatedTicket2);
    BIIN_UNICA_Pojos.TicketDetailType ticketDetail3 = BIIN_UNICA_RestHelper.generateTicketDetail(generatedTicket3);

    BIIN_UNICA_RestHelper.checkTicketRequestMandatoryFields(ticketRequest0);
    try {
      BIIN_UNICA_RestHelper.checkTicketRequestMandatoryFields(emptyTicketRequest);
    } catch(BIIN_UNICA_BadRequestException ex) {
      Boolean expectedExceptionThrown =  ex.getMessage().contains('Missing mandatory parameter') ? true : false;
      System.AssertEquals(expectedExceptionThrown, true);
    }

    String caseNumber = BIIN_UNICA_TicketManager.getTicketCaseNumber(ticketIncidenciaTecnica.BI_Id_del_caso_legado__c);
    Boolean expectedCaseNumber = caseNumber != null ? true : false;
    System.AssertEquals(expectedCaseNumber, true);

    Test.stopTest();
    
  }
}