public class TGS_Survey_Launcher_Handler {

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Miguel Angel Galindo, Carlos Campillo
	Company:       Deloitte
	Description:   Check if the case is closed and update the number of cases closed to the user.
					Class invoked by the trigger Bi_Case.
	
	History

	<Date>            <Author>                  	<Description>
	13/01/2015        Miguel Angel Galindo			Initial version
	27/10/2016        Jose Miguel Fierro            Optimized number of Queries and ammount of CPU time
	03/08/2017		  Manuel Ochoa                Fix to avoid nullPointerException when not all the cases are received in closed state
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	public static Boolean firstRun = true;

	public static void surveyLaunchHandler(List<Case> listCaseOld, List <Case> listCaseNew){
		
		/*List<Id> listIdContact = new List<Id>();
		for(Case caseNew : listCaseNew){
			listIdContact.add(caseNew.ContactId);
		}
		
		List<Contact> listContacts = [Select Contact.TGS_of_Closed_Cases__c, Id, Name
										From Contact
										Where Id IN :listIdContact];
		
		system.debug('ListContacts' + listContacts);
		for(Case co :listCaseOld){
			if(!co.IsClosed){
				for(Case cn: listCaseNew){
					if(co.Id==cn.Id){
						if(cn.IsClosed){
							if(!listContacts.isEmpty()){
								for(Contact contact : listContacts){
									if(cn.ContactId == contact.Id){
										if(contact.TGS_of_Closed_Cases__c==null){
											contact.TGS_of_Closed_Cases__c=1;
										}else{
											contact.TGS_of_Closed_Cases__c++;
										}
										System.debug('casos cerrados: '+contact.TGS_of_Closed_Cases__c);
										update contact;
									}
								}
							}
						}
					}
					break;
				}
			}
		}*/

		/// JMF 27/10/2016 - Use maps instead of looping many times over multiple for-loops
		if(!NETriggerHelper.getTriggerFired('TGS_Survey_Launcher_Handler')) {
			Map<Id, Case> mapOld = new Map<Id, Case>(listCaseOld);
			Map<Id, Contact> map_contacts;

			Map<Id, Id> map_caseContacts = new Map<Id, Id>();
			for(Case cas : listCaseNew) {
				if(mapOld.containsKey(cas.Id) != null && mapOld.get(cas.Id).IsClosed != cas.IsClosed && cas.ContactId != null) {
					map_caseContacts.put(cas.Id, cas.ContactId);
				}
			}
			if(!map_caseContacts.isEmpty()) {
				map_contacts = new Map<Id, Contact>([SELECT Id, Name, TGS_of_Closed_Cases__c FROM Contact WHERE Id IN :map_caseContacts.values()]);
				for(Case cas : listCaseNew) {
					Contact con = map_contacts.get(map_caseContacts.get(cas.Id));
					// 03-08-2017 - Manuel Ochoa - Fix to avoid nullPointerException when not all the cases are received in closed state
                    // check if contact is not null, if it's null is not a closed case
                    if(con != null){
                        if(con.TGS_of_Closed_Cases__c == null) {
                            con.TGS_of_Closed_Cases__c = 1;
                        } else {
                            con.TGS_of_Closed_Cases__c++;
                    	}     
                    }
				}
				if(!map_contacts.isEmpty()) update map_contacts.values();
			}
		}
	}

}