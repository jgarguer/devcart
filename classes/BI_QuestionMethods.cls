public without sharing class BI_QuestionMethods {
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Cristina Rodriguez
    Company:       Accenture
    Description:   If priority is hight (not hight priority before) create  newcase
    
    IN:            
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    10/05/2017              Cristina Rodríguez      Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void questionCases(List <Question> news, List <Question> olds ){
      List<Id> questions = new List<Id>();
      Map <Id,Question> oldQuestions = new Map <Id,Question>();
      for(Integer i = 0; i<news.size();i++){
        if(news[i].Priority == 'high' && olds[i].Priority != 'high'){
            questions.add(news[i].Id);
            oldQuestions.put(olds[i].Id, olds[i]);
        }
       }      
      if(!questions.isEmpty()){
          Map <Id,Case> mapQuestionCases = new Map <Id,Case>();
          list<Case> casesToInsert = new list<Case>();   
                for(Question q :[select Id, Title, Body, CommunityId, createdById, createdBy.AccountId, createdBy.ContactId, (Select id from Cases) from Question where Id IN :questions]){            
                    if (q.Cases == null || q.Cases.size() == 0) {              
                        Case newCase = new Case(Origin='Chatter Answers', OwnerId=q.CreatedById, QuestionId=q.Id, CommunityId=q.CommunityId, Subject=q.Title, Description = (q.Body == null? null: q.Body.stripHtmlTags()), AccountId=q.CreatedBy.AccountId, ContactId=q.CreatedBy.ContactId);
                        mapQuestionCases.put(q.Id, newCase);
                        casesToInsert.add(newCase);
                    }                            
                }
                if(!casesToInsert.isEmpty()){
                    Database.SaveResult[] lstSaveRes = Database.insert(casesToInsert, false);
                    

                    for(Integer i = 0; i<lstSaveRes.size();i++){
                        if (!lstSaveRes[i].isSuccess()) {
                            //In case you have issues with code coverage for this section, you can move this to a separate helper class method which can be tested separately
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            mail.setReplyTo('no-reply@salesforce.com');
                            mail.setSenderDisplayName('Salesforce Chatter Answers User');
        
                            // The default sender is the portal user causing this trigger to run, to change this, set an organization-wide address for
                            // the portal user profile, and set the ID in the following line.
                            mail.setToAddresses(new String[] { Site.getAdminEmail() });
                            mail.setSubject('Case Escalation exception in site ' + Site.getName());       
                            String errorMessages ='';
                            for(Database.Error err : lstSaveRes[i].getErrors()) {    
                                errorMessages += err.getMessage() + ', ';
                            }             
                            mail.setPlainTextBody('Case Escalation on Question having ID: ' + casesToInsert[i].QuestionId + ' has failed with the following message/s: ' + errorMessages);
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });   
                        }
                    }                
                }  
        }
    }

    /*******OLD
    for (Question q: Trigger.new) {
        try {
            if (q.Priority == 'high' && (q.Cases == null || q.Cases.size() == 0) && Trigger.oldMap.get(q.id).Priority != 'high') {
                q = [select Id, Title, Body, CommunityId, createdById, createdBy.AccountId, createdBy.ContactId from Question where Id = :q.Id];
                Case newCase = new Case(Origin='Chatter Answers', OwnerId=q.CreatedById, QuestionId=q.Id, CommunityId=q.CommunityId, Subject=q.Title, Description = (q.Body == null? null: q.Body.stripHtmlTags()), AccountId=q.CreatedBy.AccountId, ContactId=q.CreatedBy.ContactId);
                insert newCase;
            }
        } catch (Exception e) {
            //In case you have issues with code coverage for this section, you can move this to a separate helper class method which can be tested separately
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setReplyTo('no-reply@salesforce.com');
            mail.setSenderDisplayName('Salesforce Chatter Answers User');

            // The default sender is the portal user causing this trigger to run, to change this, set an organization-wide address for
            // the portal user profile, and set the ID in the following line.
            // mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
            mail.setToAddresses(new String[] { Site.getAdminEmail() });
            mail.setSubject('Case Escalation exception in site ' + Site.getName());
            mail.setPlainTextBody('Case Escalation on Question having ID: ' + q.Id + ' has failed with the following message: ' + e.getMessage() + '\n\nStacktrace: ' + e.getStacktraceString());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
*/
}