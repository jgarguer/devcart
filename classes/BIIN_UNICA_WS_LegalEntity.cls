public class BIIN_UNICA_WS_LegalEntity extends BIIN_UNICA_Base
{
/*---------------------------------------------------------------------------------------
  Author:        José Luis González
  Company:       HPE
  Description:   

  <Date>                 <Author>                				<Change Description>
  07/04/2016             José Luis González Beltrán      		Initial Version
  01/07/2016             José Luis González Beltrán      		Fix update URL
---------------------------------------------------------------------------------------*/
  
	// Global variables
	private static final String URL = 'callout:BIIN_WS_UNICA/customerinfo/v1/customers';
	
	// Constructor
	public BIIN_UNICA_WS_LegalEntity() 
	{
	}
	
	public void postLegalEntity(Account legalEntity) 
	{
		System.debug(' BIIN_UNICA_WS_LegalEntity.postLegalEntity | legalEntity: ' + legalEntity);
		BIIN_UNICA_Pojos.CustomerRequestType contactDetails = parseAccount(legalEntity, null);
		
		// Set request
		HttpRequest req = getRequest(URL, 'POST', contactDetails);	
		BIIN_UNICA_Pojos.ResponseObject resObject = (BIIN_UNICA_Pojos.ResponseObject) getResponse(req, null);
	}
	
	public void putLegalEntity(Account legalEntity, Account oldLegalEntity) 
	{
		System.debug(' BIIN_UNICA_WS_LegalEntity.putLegalEntity | legalEntity: ' + legalEntity + '; oldLegalEntity: ' + oldLegalEntity);
		BIIN_UNICA_Pojos.CustomerRequestType contactDetails = parseAccount(legalEntity, oldLegalEntity);

		String originalValidadorFiscal = String.isEmpty(oldLegalEntity.BI_Validador_Fiscal__c) ? oldLegalEntity.BI_Validador_Fiscal__c : oldLegalEntity.BI_Validador_Fiscal__c;
		
		// Set request
		HttpRequest req = getRequest(URL + '/' + originalValidadorFiscal, 'PUT', contactDetails);
		BIIN_UNICA_Pojos.ResponseObject resObject = (BIIN_UNICA_Pojos.ResponseObject) getResponse(req, BIIN_UNICA_Pojos.ApiTransactionType.class);
	}
	
	private BIIN_UNICA_Pojos.CustomerRequestType parseAccount(Account legalEntity, Account oldLegalEntity)
	{
		BIIN_UNICA_Pojos.CustomerRequestType customerRequest = new BIIN_UNICA_Pojos.CustomerRequestType();
		customerRequest.legalId = new List<BIIN_UNICA_Pojos.IdentificationType>();
		customerRequest.customerAddresses = new List<BIIN_UNICA_Pojos.AddressInfoType>();
		customerRequest.contactingModes = new List<BIIN_UNICA_Pojos.ContactingModeType>();
		customerRequest.callbackUrl = null;
		
		BIIN_UNICA_Pojos.CustomerInfoType customerdetails = new BIIN_UNICA_Pojos.CustomerInfoType();
		customerdetails.customerName = legalEntity.Name;
		customerdetails.customerStatus = legalEntity.BI_Activo__c == 'Sí' ? 'active' : 'inactive';
		customerdetails.additionalData = new List<BIIN_UNICA_Pojos.KeyValueType>();
		if (oldLegalEntity != null)
		{
			if (oldLegalEntity.Name != legalEntity.Name)
			{
				customerdetails.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('ad_old_value_for_name', oldLegalEntity.Name));
			}
			if (oldLegalEntity.BI_No_Identificador_fiscal__c != legalEntity.BI_No_Identificador_fiscal__c)
			{
				customerdetails.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('ad_old_value_for_identificador_fiscal', oldLegalEntity.BI_No_Identificador_fiscal__c));
			}
			if (oldLegalEntity.BI_Validador_Fiscal__c != legalEntity.BI_Validador_Fiscal__c)
			{
				customerdetails.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('ad_old_value_for_validador_fiscal', oldLegalEntity.BI_Validador_Fiscal__c));
			}
		}
		customerRequest.customerdetails = customerdetails;

		BIIN_UNICA_Pojos.IdentificationType legalId = new BIIN_UNICA_Pojos.IdentificationType();
		legalId.country = legalEntity.BI_Country__c;
		legalId.nationalIDType = legalEntity.BI_Tipo_de_identificador_fiscal__c;
		if(String.isEmpty(legalEntity.BI_Validador_Fiscal__c) 
			&& !String.isEmpty(legalEntity.BI_Tipo_de_identificador_fiscal__c) 
			&& !String.isEmpty(legalEntity.BI_No_Identificador_fiscal__c))
		{
			legalEntity.BI_Validador_Fiscal__c = legalEntity.BI_Codigo_ISO__c + '_' + legalEntity.BI_Tipo_de_identificador_fiscal__c + '_' + legalEntity.BI_No_Identificador_fiscal__c;
		}
		legalId.nationalID = legalEntity.BI_Validador_Fiscal__c;
		customerRequest.legalId.add(legalId);

		BIIN_UNICA_Pojos.AddressInfoType address = new BIIN_UNICA_Pojos.AddressInfoType();
		address.addressName = '';
		address.locality = '';
		address.region = '';
		address.country = '';
		address.postalCode = '';
		customerRequest.customerAddresses.add(address);

		BIIN_UNICA_Pojos.ContactingModeType contactingMode = new BIIN_UNICA_Pojos.ContactingModeType();
		contactingMode.contactMode = '';
		contactingMode.details = '';
		customerRequest.contactingModes.add(contactingMode);

		System.debug(' BIIN_UNICA_WS_LegalEntity.parseAccount | customerRequest: ' + customerRequest);

		return customerRequest;
	}

}