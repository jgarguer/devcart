@RestResource(urlMapping='/opportunities/*')
global class BI_OpportunityRest {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Opportunity (Individual) Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version
    01/06/2016        Antonio Pardo     Removed methods isSuperUsuario and retrieves_F4_StageName, moved to BI_Buttons
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains detailed information stored in the server for a specific opportunity.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.OpportunityDetailedInfoType structure
                   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @HttpGet
    global static BI_RestWrapper.OpportunityDetailedInfoType getOpportunity() {
        
        BI_RestWrapper.OpportunityDetailedInfoType response;
        
        try{
            
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
                
            //ONE OPPORTUNITY
            response = BI_RestHelper.getOneOpportunity(RestContext.request.requestURI.split('/')[2]);
            
            RestContext.response.statuscode = (response == null)?404:200;//404 NOT_FOUND, 200 OK
            
        }catch(Exception Exc){
            
            RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
            RestContext.response.headers.put('errorMessage',Exc.getMessage());
            BI_LogHelper.generate_BILog('BI_OpportunityRest.getOpportunity', 'BI_EN', Exc, 'Web Service');
            
        }
        
        return response;
        
    }
    
    
    
     
    
}