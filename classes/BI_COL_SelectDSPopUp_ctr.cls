/****************************************************************************************************
	Información general
	-------------------
	author: Javier Tibamoza Cubillos
	company: Avanxo Colombia
	Project: Implementación Salesforce
	Customer: TELEFONICA
	Description: 
	
	Information about changes (versions)
	-------------------------------------
	Number    Dates           Author                       Description
	------    --------        --------------------------   -----------
	1.0       08-Abr-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
public class BI_COL_SelectDSPopUp_ctr 
{
	public string sIdwrp {get;set;}
	public string sIdProd {get;set;}
	public string sIdAcc {get;set;}
	public string sSearchName {get;set;}
	public string sCambioServAlta {get;set;}
	public string sNomProd {get;set;}

	public list<BI_COL_Descripcion_de_servicio__c> lstDSQuery {get;set;}
	public list<BI_COL_Descripcion_de_servicio__c> lstDS {get;set;}

	public boolean blFilter {get;set;}
	/***
	* @Author: 
	* @Company: 
	* @Description: 
	* @History: 
	* Date		|	Author	|	Description
	* 
	***/
	public BI_COL_SelectDSPopUp_ctr()
	{
		sIdwrp = ApexPages.currentPage().getParameters().get('sIdwrp');
		sIdProd = ApexPages.currentPage().getParameters().get('IdProd');
		sNomProd = ApexPages.currentPage().getParameters().get('NomProd');
		sIdAcc = ApexPages.currentPage().getParameters().get('Filtro');
		//System.debug();
		sCambioServAlta = ApexPages.currentPage().getParameters().get('CambioAlta');
		blFilter = false;
		fnFindDS();
	}
	/***
	* @Author: 
	* @Company: 
	* @Description: 
	* @History: 
	* Date		|	Author	|	Description
	* 
	***/
	public PageReference fnFilterDS()
	{
		blFilter = true;
		fnFindDS();
		return null;
	}
	/***
	* @Author: 
	* @Company: 
	* @Description: 
	* @History: 
	* Date		|	Author	|	Description
	* 
	***/
	public void fnFindDS()
	{
		Map<string,BI_COL_Descripcion_de_servicio__c> mDS = new Map<string,BI_COL_Descripcion_de_servicio__c>();

		lstDS = new list<BI_COL_Descripcion_de_servicio__c>();
		//Query de las DS
		string sQ = ' select Id, Name, BI_COL_Sede_Origen__c, BI_COL_Sede_Origen__r.Name,  '+
		' (select Id, Name, BI_COL_Estado__c, BI_COL_Producto__r.NE__ProdId__r.Name, BI_COL_Producto__r.NE__ProdId__c, '+
		' BI_COL_Producto_Anterior__r.BI_COL_Descripcion_referencia__c from BI_COL_Codigo_unico_servicio__r where BI_COL_Estado__c=\'Activa\') '+
		' from	BI_COL_Descripcion_de_servicio__c '+
		' where	BI_COL_Oportunidad__r.AccountId =: sIdAcc';
		
		 System.debug('\n lstDS======>>>'+lstDS);
		if( blFilter )
			sQ += ' and Name like \'%' + sSearchName + '%\'';
		sQ+= ' limit 1000';
		lstDSQuery = database.query( sQ );
		System.debug('\n\n sCambioServAlta: '+sCambioServAlta+' \n\n');
		System.debug('\n\n SQL='+sQ+'\n\n');
		System.debug('\n\n lstDSQuery='+lstDSQuery+'\n\n');
		for( BI_COL_Descripcion_de_servicio__c oDS : lstDSQuery )
		{
			
			System.debug('\n ENTRO=============='+oDS.BI_COL_Codigo_unico_servicio__r.size());
			//Valida que tenga MS asociadas a las DS
			if( oDS.BI_COL_Codigo_unico_servicio__r.size() > 0 )
			{
				
				System.debug('\n ENTRO222==============');
				//Recorre las MS
				for( BI_COL_Modificacion_de_Servicio__c oMS : oDS.BI_COL_Codigo_unico_servicio__r )
				{
					//Valida que el estado de la MS sea activa y el producto sea el mismo
					System.debug('### sIdProd --->' + oMS.BI_COL_Producto__r.NE__ProdId__c + '  ' + sIdProd + '  ' + oMS.BI_COL_Producto_Anterior__r.BI_COL_Descripcion_referencia__c + ' '+sNomProd);
					System.debug('\n Entro if prueba===='+oMS.BI_COL_Estado__c+' '+sIdProd+ ' ' + oMS.BI_COL_Producto__r.NE__ProdId__c+' '+oDS.Id);
					if( oMS.BI_COL_Estado__c == 'Activa' && oMS.BI_COL_Producto__r.NE__ProdId__c == sIdProd && !mDS.containsKey( oDS.Id ) )
					{	mDS.put( oDS.Id, oDS );
						System.debug('\n Linea 107 Entro if===='+oMS.BI_COL_Estado__c+' '+oMS.BI_COL_Producto__r.NE__ProdId__c+' ');
					}
					else if( oMS.BI_COL_Estado__c == 'Activa' && oMS.BI_COL_Producto_Anterior__r.BI_COL_Descripcion_referencia__c == sNomProd && !mDS.containsKey( oDS.Id ) )
					{
						mDS.put( oDS.Id, oDS );
					//	System.debug('\n Entro elseif1====');
					}
					else if( oMS.BI_COL_Estado__c == 'Activa' && sCambioServAlta == 'true' && !mDS.containsKey( oDS.Id ) )
					{
						mDS.put( oDS.Id, oDS );
					}
				}
			}
		}
		System.debug('\n\n mDS===='+mDS+'\n\n');
		for( string sId : mDS.keySet() )
			lstDS.add( mDS.get( sId ) );
	}
}