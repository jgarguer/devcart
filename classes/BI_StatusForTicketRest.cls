@RestResource(urlMapping='/ticketing/v1/tickets/*/status')
global class BI_StatusForTicketRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Status Ticket Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    20/08/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains information stored in the server for a specific ticket status.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.TicketStatusInfoType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    20/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.TicketStatusInfoType getStatusForTicket() {
		
		BI_RestWrapper.TicketStatusInfoType res;
		
		try{
		
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
				
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
			
				//OBTAIN STATUS
				res = BI_RestHelper.getStatusForTicket(RestContext.request.requestURI.split('/')[4], RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
				
				RestContext.response.statuscode = (res == null)?404:200;//404 NOT_FOUND, 200 OK
					
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_StatusForTicketRest.getStatusForTicket', 'BI_EN', exc, 'Web Service');
			
		}
		
		return res;
		
	}
	
}