/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true)
private class OrderSummaryCancelButtonTest {

    static testMethod void myUnitTest() 
    {
        BI_TestUtils.throw_exception = false;//JEG
        Account acc= new Account(Name = 'test',NE__Type__c = 'Business', ShippingCity = 'Roma',ShippingPostalCode = '00100',  ShippingCountry = 'Lazio',  ShippingState = 'Italia',  ShippingStreet = 'Garibaldi', BillingPostalCode = '00100', BillingCity = 'Roma', BillingCountry ='Lazio', BillingState = 'Italia', BillingStreet = 'aurelia', BI_Segment__c = 'test', BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test'); // 28/09/2017
        insert acc;
        
        Date myTt = date.today();
        
        Opportunity opp=new Opportunity(Name='opp',AccountId=acc.id,StageName='Prospecting',CloseDate=myTt);
        insert opp;
        
        RecordType rec2     =   [select Name,Id from RecordType where SobjectType = 'NE__Order__c' AND Name = 'Order' LIMIT 1];
        Test.startTest();//JEG
        NE__Order__c ord=new NE__Order__c(NE__AccountId__c=acc.id,NE__BillAccId__c=acc.id,NE__ServAccId__c=acc.id,RecordTypeId=rec2.id,NE__OptyId__c=opp.id,NE__Shipping_City__c='city',NE__Shipping_Country__c='country',NE__Shipping_State_Province__c='SSP',NE__Shipping_Street__c='Street',NE__Shipping_Zip_Postal_Code__c='ZPC',NE__OrderStatus__c='Active');
        insert ord;
        
        NE__Product__c prod=new NE__Product__c(Name='prodotto');
        insert prod;
        NE__Catalog__c cat=new NE__Catalog__c(Name='catalog');
        insert cat;
        NE__Catalog_Category__c category=new NE__Catalog_Category__c(name='category',NE__CatalogId__c=cat.id);
        insert category;
        NE__Catalog_Item__c catit=new NE__Catalog_Item__c(NE__Catalog_Id__c=cat.id, NE__Catalog_Category_Name__c=category.id, NE__ProductId__c=prod.id, NE__Max_Qty__c=1,NE__Base_OneTime_Fee__c=15,NE__BaseRecurringCharge__c=30);
        insert catit;
        
        //NE__OrderItem__c oit=new NE__OrderItem__c(NE__Status__c = 'Active', NE__OrderId__c=ord.id,NE__ProdId__c=prod.id,NE__Qty__c=1,NE__BaseOneTimeFee__c=5,NE__BaseRecurringCharge__c=10,NE__OneTimeFeeOv__c=15,NE__RecurringChargeOv__c=20);
        //insert oit;
        
        OrderSummaryCancelButton.cancelButton(ord.Id);
        Test.stopTest();//JEG
    }
}