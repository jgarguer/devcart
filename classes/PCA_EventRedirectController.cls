public without sharing class PCA_EventRedirectController extends PCA_HomeController{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Class to redirect event link
        
        History:
        
        <Date>            <Author>              <Description>
        23/07/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public string url       {get;set;}
    public string idEvent {get;set;}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Constructor
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_EventRedirectController(){
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            PageReference page = enviarALoginComm();
            if(page == null){
                initialize();
            }
            return page;
        }catch (Exception Exc){
           BI_LogHelper.generate_BILog('PCA_EventRedirectController.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich
        Company:       Salesforce.com
        Description:   Class to form url for redirection of the event
        
        History:
        
        <Date>            <Author>              <Description>
        23/07/2014        Ana Escrich           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void initialize(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            string formatedDate;
            DateTime day;
            idEvent = (System.currentPageReference().getParameters().get('id')!=null)?String.escapeSingleQuotes(System.currentPageReference().getParameters().get('id')):null;
            system.debug('id: '+idEvent);
            List<Event> eventList = [select EndDateTime,Id,StartDateTime from Event where Id = : idEvent];
            system.debug('eventList: '+eventList);
            if(!eventList.isEmpty()){
                Event event = eventList[0];
                system.debug('e.StartDateTime: '+event.StartDateTime);
                day = event.StartDateTime;
                //formatedDate = event.StartDateTime.format('h:mm a') +' - ' + event.EndDateTime.format('h:mm a');
                url = Site.getPrefix()+'/PCA_CustomEventDetail?day='+day+'&edit=true&format='+formatedDate+'&id='+idEvent+'&pastday=false&FromChater=true';
            }else{
                url = '';
            }
            
            system.debug('url: '+url);
        }catch(Exception Exc){
            system.debug('error: '+Exc);
            BI_LogHelper.generate_BILog('PCA_EventRedirectController.PCA_EventRedirectController', 'Portal Platino', Exc, 'Class');
        }
    }
}