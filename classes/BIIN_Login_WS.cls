/*------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Julio Laplaza
        Company:       HP
        Description:   Función para obtener el token de autenticación de RoD.
        
        History:
        
        <Date>              <Author>                                        <Description>
        14/06/2016          Julio Laplaza                                   Implementation
------------------------------------------------------------------------------------------------------------------------------------------*/
public class BIIN_Login_WS extends BIIN_UNICA_Base
{
    public String login(String userName, String password)
    {
        System.debug(' BIIN_Login_WS.login | userName: ' + userName + ', password: ' + password);
        // Prepare URL
        String url = 'callout:BIIN_WS_UNICA/_core/v1/login';

        // Prepare Request
        BIIN_UNICA_Pojos.LoginRequestType loginData = new BIIN_UNICA_Pojos.LoginRequestType();
        loginData.credentials = new BIIN_UNICA_Pojos.Credentials();
        loginData.credentials.username = userName;
        loginData.credentials.password = password;

        // Make request
        HttpRequest req = getRequest(url, 'POST', loginData);
        HttpResponse res = new Http().send(req);

        // Get TOKEN
        String token = res.getHeader('Authentication-Token');

        System.debug(' BIIN_Login_WS.login | token: ' + token);

        return token;
    }
}