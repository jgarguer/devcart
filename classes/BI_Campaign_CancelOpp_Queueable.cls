public without sharing class BI_Campaign_CancelOpp_Queueable implements Queueable {

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Micah Burgos  
Company:       Aborda.es
Description:   Queueable class that cancel preOpp if Campaings

IN:
	Set<Id> setId_param -> Id of Campaings or Opportunities
	Integer option_param -> 1 		=> setId_param: id of Campaings
							!= 1 	=> setId_param: id of Oportunities

	Set<Id> setId_campToEmail_param -> id of Campaings for send mails to Owner and to last modified User.

History: 

<Date>                  <Author>                <Change Description>
13/05/2015             Micah Burgos          	Initial Version   
07/12/2015             Guillermo Muñoz          Prevent the email send in Test class  
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public Set<Id> setId;
    public Integer option;
    final Integer MAX_RECORDS = 200;

    Set<Id> set_id_records_to_process;
    Set<Id> set_id_records_to_future_process;
    Set<Id> setId_campToEmail;

    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    
    static{
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType ='Opportunity']){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }
    }

    public BI_Campaign_CancelOpp_Queueable(Set<Id> setId_param, Integer option_param, Set<Id> setId_campToEmail_param) {
        setId = setId_param;
        //option = option_param;
        system.debug('CONSTRUCTOR : setId_campToEmail_param: ' + setId_campToEmail_param);
        Set<Id> aux = setId_campToEmail_param;
        system.debug('CONSTRUCTOR : aux: ' + setId_campToEmail_param);
        setId_campToEmail = aux;
        system.debug('CONSTRUCTOR : setId_campToEmail: ' + setId_campToEmail);

        if(option_param != 1){//Change CampaignId to Opp Ids.
            List<Opportunity> lst_oppId = [SELECT ID FROM Opportunity WHERE CampaignId IN :setId];
            setId.clear();
            for(Opportunity opp_id :lst_oppId){
                setId.add(opp_id.Id);
            }
        }        

        Map<Integer,Set<Id>> map_records = splitArray(setId, MAX_RECORDS);

        set_id_records_to_process = map_records.get(0);
        set_id_records_to_future_process = map_records.get(1);
    }

    public void execute(QueueableContext context) {
    	try{
    		List<Opportunity> lst_opp = [SELECT Id, StageName FROM Opportunity WHERE StageName = :Label.BI_F6Preoportunidad AND Id IN :set_id_records_to_process];

    		  for(Opportunity item:lst_opp){
                   //if (item.StageName == Label.BI_F6Preoportunidad)
                   //	//|| item.StageName == Label.BI_DefinicionSolucion 
                   //	//|| item.StageName == Label.BI_DesarrolloOferta 
                   //	//|| item.StageName == Label.BI_F3OfertaPresentada
                   //	//|| item.StageName == Label.BI_F2Negociacion)
                   //{
                       item.StageName = Label.BI_F1CanceladaSusp;
                       item.BI_Casilla_desarrollo__c = true;
                   //}
                }
                
                update lst_opp;
        
        }catch (exception  Exc){
            Id log_id = BI_LogHelper.generate_BILog('BI_Campaign_CancelOpp_Queueable', 'BI_EN', Exc, 'Trigger - Queueable');
        }

       
        system.debug('START ***RECALL***');

        try{
            BI_bypass__c bypass = BI_bypass__c.getInstance();
            if(!set_id_records_to_future_process.isEmpty() && !bypass.BI_stop_job__c){
                system.debug('RECALL : set_id_records_to_future_process: ' + set_id_records_to_future_process);
                System.enqueueJob(new BI_Campaign_CancelOpp_Queueable(set_id_records_to_future_process,1, setId_campToEmail));
            }else{
                List<EmailTemplate> lst_tmpl = [SELECT DeveloperName,Id,Name FROM EmailTemplate WHERE DeveloperName = 'BI_End_Cancel_Preopp_campaign'];
                if(!lst_tmpl.isEmpty() && setId_campToEmail != null && !setId_campToEmail.isEmpty()){


                    Contact dummyContact = new Contact(LastName = 'noreply', Email = 'noreply@telefonica.com');
                    List<Messaging.SingleEmailMessage> lst_mails = new List<Messaging.SingleEmailMessage>();
                    
                    insert dummyContact;

                    system.debug('QUERY : setId_campToEmail: ' + setId_campToEmail);
                    List<Campaign> lst_campEmail = [SELECT Id, OwnerId ,Owner.Email, LastModifiedById, LastModifiedBy.Email FROM Campaign WHERE Id IN :setId_campToEmail];

                    for(Campaign camp :lst_campEmail){
                        
                        Messaging.SingleEmailMessage mailOwner = new Messaging.SingleEmailMessage();

                            mailOwner.setTargetObjectId(dummyContact.Id);
                                                        
                            mailOwner.setTemplateId(lst_tmpl[0].Id);
                            mailOwner.setWhatId(camp.Id);  
                            mailOwner.setReplyTo('noreply@telefonica.com');
                            mailOwner.setSenderDisplayName('Telefónica BI_EN');
                            mailOwner.setSaveAsActivity(false);

                            List<String> lst_toBcc = new List<String>();
                            lst_toBcc.add(camp.LastModifiedBy.Email);
                            if(camp.LastModifiedById != camp.OwnerId){
                                lst_toBcc.add(camp.Owner.Email);
                            }
                            mailOwner.setToAddresses(lst_toBcc);
                            
                            lst_mails.add(mailOwner);
                    }
                    //system.debug('lst_mails' +lst_mails);
                    try{
                        if(!Test.isRunningTest()){
                            Messaging.sendEmail(lst_mails);
                        }
                    }catch(exception Exc){
                        BI_LogHelper.generate_BILog('BI_Campaign_CancelOpp_Queueable.sendEmail', 'BI_EN', Exc, 'Trigger - Queueable');
                    }
                    
                    delete dummyContact;
                }
            }
        }catch(Exception exc){
             BI_LogHelper.generate_BILog('BI_Campaign_CancelOpp_Queueable.recall_queueable', 'BI_EN', Exc, 'Trigger - Queueable');
        }
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Method that split list in 2 blocks, the first block size = max_regs
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    12/03/2015                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Map<Integer,Set<Id>> splitArray(Set<Id> lst_param, Integer max_regs) {

        Map<Integer,Set<Id>> map_return = new Map<Integer,Set<Id>>();
        map_return.put(0,new Set<Id>());
        map_return.put(1,new Set<Id>());

        if(lst_param != null && !lst_param.isEmpty()){
            Integer i = 0;
            for(Id var :lst_param){
                if(i < max_regs){
                    map_return.get(0).add(var);
                }else{
                    map_return.get(1).add(var);
                }
                i++;
            }
            
        }
        return map_return;
    }
}