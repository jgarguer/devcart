public class BI_AprobacionMiembrosCampanaMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Methods executed by the CampaignMember trigger.
    History:
    
    <Date>            <Author>          <Description>
    14/04/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that controls the status for the related CampaignMembers, and if the Status is 'Aprobado' unlocks the records
    
    IN:            ApexTrigger.old, ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    14/04/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateCampaignMember(List<BI_Aprobacion_Miembros_Campana__c> news, List<BI_Aprobacion_Miembros_Campana__c> olds){
        try{
	        Map<Id,String> map_cm = new Map<Id,String>();
	        
	        Integer i = 0;
	        for(BI_Aprobacion_Miembros_Campana__c amc:news){
	            if((amc.BI_Estado__c == Label.BI_Aprobado || amc.BI_Estado__c == Label.BI_Rechazado) && amc.BI_Estado__c != olds[i].BI_Estado__c)
	                map_cm.put(amc.BI_Miembro_de_Campana__c, amc.BI_Estado__c);
	            
	            i++;
	        }
	        
	        if(!map_cm.isEmpty()){
	        
	            Set<Id> set_c = new Set<Id>();
	            List<CampaignMember> list_cm = new List<CampaignMember>();
	            for(CampaignMember camp:[select Id, CampaignId from CampaignMember where Id IN :map_cm.keySet()]){
	                camp.BI_Bloqueo__c = false;
	                camp.Status = map_cm.get(camp.Id);
	                list_cm.add(camp);
	                set_c.add(camp.CampaignId);
	            }
	            
	            if(!set_c.isEmpty())
	                BI_CampaignMemberHelper.createCampaignMemberStatus(set_c);
	            
	            //Variable used for trigger control
	            BI_GlobalVariables.stopPreventUpdate = true;
	            update list_cm;
	        }
        }catch (exception exc){
			BI_LogHelper.generate_BILog('BI_AprobacionMiembrosCampanaMethods.updateCampaignMember', 'BI_EN', exc, 'Trigger');
		}
        
    }
    
}