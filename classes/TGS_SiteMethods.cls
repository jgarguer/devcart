public with sharing class TGS_SiteMethods {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Álvaro López
     Company:       Accenture
     Description:   Methods executed by BI_Punto_de_instalacion_tgr Trigger (TGS)
     Test Class:    TGS_SiteMethods_TEST
     
     History:
      
     <Date>                  <Author>               <Change Description>
     30/01/2018              Álvaro López          	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static Boolean isAddressUpdated = false;


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Álvaro López
     Company:       Accenture
     Description:   Set the ISO Code field
     
     History:
      
     <Date>                  <Author>               <Change Description>
     30/01/2018              Álvaro López          	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void setIsoCode(List<BI_Punto_de_instalacion__c> news){

    	Set<String> set_countries = new Set<String>();
    	List<BI_Punto_de_instalacion__c> toProcess = new List<BI_Punto_de_instalacion__c>();

    	Map<String, String> map_isoCodes = new Map<String, String>();

    	for(BI_Punto_de_instalacion__c site :news){
    		set_countries.add(site.Pais__c);
    	}

    	List<TGS_Site_ISOCode__mdt> lst_IsoCodes = [SELECT Id, TGS_Country__c, TGS_IsoCode__c FROM TGS_Site_ISOCode__mdt WHERE TGS_Country__c IN :set_countries];

        try{
        	for(TGS_Site_ISOCode__mdt isoCode :lst_IsoCodes){

        		if(!map_isoCodes.containsKey(isoCode.TGS_Country__c)){
        			map_isoCodes.put(isoCode.TGS_Country__c, isoCode.TGS_IsoCode__c);
        		}
        	}

        	for(BI_Punto_de_instalacion__c site :news){
        		
        		if(map_isoCodes.containsKey(site.Pais__c)){
        			site.TGS_ISO_Code__c = map_isoCodes.get(site.Pais__c);
        			if(isAddressUpdated){
        				System.debug('SITE: ' + site.Pais__c + ' - ' + site.TGS_ISO_Code__c);
        				toProcess.add(site);
        			}
        		}
        	}

        	if(!toProcess.isEmpty()){
        		update toProcess;
        	}
        }
        catch(Exception exc){
            BI_LogHelper.generate_BILog('TGS_SiteMethods.isoCode', 'TGS', exc, 'Apex Class');
        }
    }

    public static void updateIsoCode(List<BI_Sede__c> news){

    	List<BI_Sede__c> toProcess = new List<BI_Sede__c>();

    	Map<String, String> map_isoCodes = new Map<String, String>();

    	List<BI_Punto_de_instalacion__c> lst_sites = [SELECT Id, Pais__c, TGS_ISO_Code__c FROM BI_Punto_de_instalacion__c WHERE BI_Sede__c IN :news];

    	if(!lst_sites.isEmpty()){
    		isAddressUpdated = true;
    		TGS_SiteMethods.setIsoCode(lst_sites);
    	}
    }
}