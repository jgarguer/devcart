/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-04-08      Daniel ALexander Lopez (DL)     Cloned Class      
*************************************************************************************/
public class BI_COL_Send_Proyect_trs_ctr 
{  
  public String strOpptys{get;set;}
  public String strProyecto{get;set;}
  public String strProyectoNuevo{get;set;}
  public Account cliente{get;set;} 
  public Decimal consecutivo {get;set;}
  public boolean stRegistrosTotal {get;set;}
  public boolean stBoton {get;set;}
  public List<WrapOportunidad> lstWrap{get;set;}
  public List<WrapOportunidad> lstImpresion {get;set;}
  public List<Opportunity> lstOportunidades{get;set;}
  private String codigoProySelec='';
  private String nombreProySelec='';

  public Integer pageSize{get;set;}
  public Integer pageNumber{get;set;}
  private Integer totalPageNumber{get;set;}

  /**** OA: Nétodo de creación de log de transacciones para el objeto Oportunidad ****/
  public static void creaLogTransaccion(List<ws_TrsMasivo.crearProyectoResponse> lstResp, List<Opportunity> lstIdOpt)
  {
	List<BI_Log__c> lstLogTran = new List<BI_Log__c>();
	BI_Log__c log = null; 
	String mensaje = lstResp[0].mensajeError;
	
	if( ( mensaje == null || mensaje == '' ) && lstResp[0].estado == 'OK' )
	  mensaje = label.BI_COL_lblRegistro_Enviado_Satisfactoriamente;
	
	for( Opportunity opt : lstIdOpt )
	{
	  log = new BI_Log__c();
	  log.BI_COL_Informacion_recibida__c = mensaje +'PRUEBA ';
	  log.BI_COL_Oportunidad__c = opt.id;
	  lstLogTran.add(log);
	}
	insert lstLogTran;
  }
  /**** OA: Método de creación de log de transacciones fallidas para el objeto Oportunidad ****/
  public static void creaLogTransaccion( String error,String strrespuestaWS, String strInformacionEnviada, List<Opportunity> lstIdOpt)
  {
	List<BI_Log__c> lstLogTran = new List<BI_Log__c>();
	BI_Log__c log = null; 
	
	for( Opportunity suc : lstIdOpt )
	{
	  log = new BI_Log__c();
	  log.BI_COL_Informacion_Enviada__c = strInformacionEnviada;
	  log.BI_COL_Informacion_recibida__c = strrespuestaWS;
	  log.BI_COL_Oportunidad__c=suc.Id;
	  log.BI_COL_Estado__c = error; 
	  log.BI_COL_Tipo_Transaccion__c = 'NOTIFICACIONES - TRS';
	  lstLogTran.add(log);
	}
	insert lstLogTran;
  }
  
  public BI_COL_Send_Proyect_trs_ctr(ApexPages.Standardcontroller ctr)
  {
	String idCliente='';
	stRegistrosTotal=false;
	totalPageNumber=0;
	pageNumber=1;
	pageSize=30;
	lstWrap=new List<WrapOportunidad>();
	getConsecutivo();
	
	if( ctr.getRecord() != null && ctr.getRecord().Id != null )
	{
	  idCliente=ctr.getRecord().Id;
	  cliente=[select Id,Name from Account where Id=:idCliente limit 1];
	}
  }
  
  public List<SelectOption> lstProyectos
  {
	  get
	  {          
		  set<String> setCodProyecto=new set <String>(); 
		  List<SelectOption> lst= new List<SelectOption>();
		  List<Opportunity > lstConsec = [
	  SELECT BI_COL_Nombre_Proyecto__c, BI_COL_Codigo_proyecto__c, BI_Id_de_la_oportunidad__c 
	  FROM Opportunity 
	  WHERE AccountId =: cliente.id ];

		  lst.add( new SelectOption('0', '------' ) );
		  if( lstConsec.size() > 0 )
	{
	  for( Opportunity opt: lstConsec )
	  {
		if(!setCodProyecto.contains(opt.BI_COL_Codigo_proyecto__c))
		{
		  lst.add( new SelectOption( opt.BI_COL_Codigo_proyecto__c + 
			'__' + opt.BI_COL_Nombre_Proyecto__c, '' + opt.BI_COL_Nombre_Proyecto__c ) );
		  setCodProyecto.add( opt.BI_COL_Codigo_proyecto__c  );
		}
	  }
		  }
	return lst;
	  }
	set;
  }
	
  public void consultaOportunidades()
  {
	lstWrap.clear();
	stBoton = false;
	totalPageNumber = 0;
	pageNumber = 1;
	String sql = 'select Id,Name,BI_COL_Nombre_Proyecto__c,BI_COL_Codigo_proyecto__c, BI_Numero_id_oportunidad__c,StageName, Account.Name, Account.BI_No_Identificador_fiscal__c,Account.BI_COL_Segmento_Telefonica__c FROM Opportunity WHERE AccountId=\''+cliente.Id+'\'';
	
	if( strOpptys != null && strOpptys != '' )
	{
	  String dsBuscar='';
	  if(strOpptys.contains(','))
	  {
		List<String> lstDSB=strOpptys.split(',');
		for(String ls:lstDSB)
		{
		  dsBuscar+='\''+ls+'\',';
		}
		dsBuscar = dsBuscar.subString(0,dsBuscar.length()-1);
	  }
	  else
	  {
		dsBuscar = '\''+strOpptys+'\'';
	  }
	  sql += ' AND BI_Id_Interno_de_la_Oportunidad__c IN ('+dsBuscar+') LIMIT 1';
	}
	if(!Test.isRunningTest())
	  lstOportunidades = Database.query( sql );
	
	/***** llenando el Wrapper ******/      
	for(Opportunity opt:lstOportunidades)
	{
	  WrapOportunidad wrap = new WrapOportunidad ();
	  wrap.oportunidad = opt;
	  wrap.seleccionado = false;
	  lstWrap.add( wrap );
	}
	if(lstWrap.size()>0)
	{
	  stBoton = true;
	  BindData(1);
	}
	else
	  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,label.BI_COL_IdInvalido));
  }
	
  public void actualizaOpt()
  {

	if(validarEnvioTRS())
	{
	  List<Id> idOptEnviar = new List<Id>();
	  List<Opportunity> lstOptActualizar = new List<Opportunity>();
	  for(WrapOportunidad wo:lstWrap)
	  {
		if(wo.seleccionado)
		{
		  wo.oportunidad.BI_COL_Nombre_Proyecto__c = nombreProySelec;
		  wo.oportunidad.BI_COL_Codigo_proyecto__c = codigoProySelec;
		  lstOptActualizar.add(wo.oportunidad);
		  idOptEnviar.add(wo.oportunidad.Id);
		}
	  }
	  try
	  {
		update lstOptActualizar;

		BI_COL_manage_cons__c  consec= BI_COL_manage_cons__c.getInstance('ConsecutivoProyecto');
		consec.BI_COL_ConsProyecto__c=getConsecutivo();
		update consec;        

		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Registros actualizados satisfactoriamente en Salesforce, Se ha enviado la información a TRS. Favor consultar el log de transacción de cada oportunidad'));
	  
		if(Test.isRunningTest())
		{
		  idOptEnviar.add(lstOportunidades[0].id);
		}
		
		BI_COL_Send_Proyect_trs_ctr.enviarTRS(codigoProySelec,nombreProySelec,idOptEnviar); 
		
	  }
	  catch(Exception e)
	  {
		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error actualizando información:\n'+e));
	  }
	}
  }
  
  @Future(callout=true)
  public static void enviarTRS(String codigoProySelec, String nombreProySelec, List<Id> lstId)
	{
	List<Opportunity> lstOptEnviar = [
	  Select  Id,Name,BI_COL_Nombre_Proyecto__c,BI_COL_Codigo_proyecto__c, BI_Numero_id_oportunidad__c, 
		  StageName, Account.Name,
		  Account.BI_No_Identificador_fiscal__c,Account.BI_COL_Segmento_Telefonica__c,
		  Account.BI_Segment__c,BI_COL_complejidad_implantacion__c, 
		  ( Select Id,User.BI_Peru_STC_Per__c, TeamMemberRole From OpportunityTeamMembers) 
	  From  Opportunity 
	  WHERE   Id In: lstId ];

	ws_TrsMasivo.datosProyecto swDatosProyecto=new ws_TrsMasivo.datosProyecto();
	swDatosProyecto.codigoproyectosalesforce=lstOptEnviar[0].BI_COL_Codigo_proyecto__c;
	swDatosProyecto.nombreproyecto=nombreProySelec;
	swDatosProyecto.nit=lstOptEnviar[0].Account.BI_No_Identificador_fiscal__c;

	swDatosProyecto.segmento = lstOptEnviar[0].Account.BI_Segment__c != null ? 
	  String.valueOf(lstOptEnviar[0].Account.BI_Segment__c).toUpperCase() : '';
	
	swDatosProyecto.nombreTrs = lstOptEnviar[0].Name;

	ws_TrsMasivo.ArrayOfcodigosoportunidades_element oportunidades = new ws_TrsMasivo.ArrayOfcodigosoportunidades_element();
	ws_TrsMasivo.DatosOportunidades [] lstOportunidades = new ws_TrsMasivo.DatosOportunidades []{};
	swDatosProyecto.responsable = 0;
	swDatosProyecto.gerente = 0;
	
	for( OpportunityTeamMember optTeam : lstOptEnviar[0].OpportunityTeamMembers )
	{
	  if(optTeam.TeamMemberRole=='Project Manager')
	  {
		swDatosProyecto.responsable=Integer.valueOf(optTeam.User.BI_Peru_STC_Per__c);
	  }
	  else if(optTeam.TeamMemberRole=='Presale Engineering')
	  {
		System.debug('ENTRO POR aqui');
		  swDatosProyecto.gerente=Integer.valueOf(optTeam.User.BI_Peru_STC_Per__c);
	  }
	}     

	String complejidad='BAJA';
	for(Opportunity opt:lstOptEnviar)
	{ 
	  ws_TrsMasivo.DatosOportunidades datOpt=new ws_TrsMasivo.DatosOportunidades();
	  datOpt.codigooportunidad=opt.BI_Numero_id_oportunidad__c; 
	  complejidad=(opt.BI_COL_complejidad_implantacion__c!=null && opt.BI_COL_complejidad_implantacion__c!='' 
		&& opt.BI_COL_complejidad_implantacion__c!=' ')?opt.BI_COL_complejidad_implantacion__c:'BAJA';
	  System.debug('Envio Complejidad '+complejidad);
	  /*if(opt.BI_COL_complejidad_implantacion__c!=null && opt.BI_COL_complejidad_implantacion__c!='' 
		&& opt.BI_COL_complejidad_implantacion__c!=' ')
	  {
	   complejidad=opt.BI_COL_complejidad_implantacion__c;      
		   
	  }else
	  { 
		complejidad='BAJA';
	  }*/
	  lstOportunidades.add(datOpt);
	}
	
	swDatosProyecto.complejidad=complejidad;
	  
	oportunidades.datos=lstOportunidades; 
	swDatosProyecto.ArrayOfcodigosoportunidades=oportunidades;
	


	try{
	  ws_TrsMasivo.serviciosSISGOTSOAP sw=new ws_TrsMasivo.serviciosSISGOTSOAP();
	  system.debug('--------------request sw-------->'+swDatosProyecto);
	  sw.timeout_x = 20000;
	  List<ws_TrsMasivo.crearProyectoResponse> lstResp = sw.crearProyecto(new ws_TrsMasivo.datosProyecto[]{swDatosProyecto});
	  system.debug('--------------response sw-------->'+lstResp);
	  for(ws_TrsMasivo.crearProyectoResponse res:lstResp)
	  {
	  System.debug(' RESPUESTA SERVICIO  '+res.estado + res.mensajeError);
	  String strInformacionEnviada='';
	  strInformacionEnviada += '\ncodigoproyectosalesforce: ' + codigoProySelec;
	  strInformacionEnviada += '\nnombreTrs: ' + lstOptEnviar[0].Name;
	  strInformacionEnviada += '\nsegmento: ' +lstOptEnviar[0].Account.BI_Segment__c;
	  strInformacionEnviada += '\nnit: ' + codigoProySelec;
	  strInformacionEnviada += '\ncomplejidad: ' + complejidad;
	  strInformacionEnviada += '\nnombreproyecto: ' + nombreProySelec;
	  //strInformacionEnviada += '\nresponsable: ' + codigoProySelec;
	  //strInformacionEnviada += '\ngerente: ' + codigoProySelec;
	  //strInformacionEnviada += '\ncodigooportunidad: ' + opt.BI_Numero_id_oportunidad__c;
	  if(res.estado!='OK')
	  {
		creaLogTransaccion('FALLIDO ','Respuesta id: '+res.id+'\nEstado: '+res.estado+'\n Mensaje: '+res.mensajeError,strInformacionEnviada+'\n'+swDatosProyecto, lstOptEnviar);  
	  }else{

	  creaLogTransaccion('EXITOSO ','Respuesta id: '+res.id+'\nEstado: '+res.estado+'\n Mensaje: '+res.mensajeError,strInformacionEnviada+'\n'+swDatosProyecto, lstOptEnviar);  
	  }

	  }
	}
	catch( Exception e )
	{
	   creaLogTransaccion(' FALLIDO ','Excepcion: '+e,swDatosProyecto+'',lstOptEnviar);
	}
  }
	
   public Decimal getConsecutivo()
   {
	BI_COL_manage_cons__c consec= BI_COL_manage_cons__c.getInstance('ConsecutivoProyecto');
	if( consec != null && consec.BI_COL_ConsProyecto__c != null )
	  consecutivo=consec.BI_COL_ConsProyecto__c;
	else
	  consecutivo = 0;
	return consecutivo + 1;
	}

  public boolean validarEnvioTRS()
  {
	boolean retorno=true;
	Integer cant=0;
	/******************* validando la selección o escritura del proyecto a asociar ******************************/
	if(Test.isRunningTest())
	{
	  strProyectoNuevo = 'Test';
	}
	if((strProyecto=='0' || strProyecto==null) && (strProyectoNuevo=='' || strProyectoNuevo==null))
	{
	  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Debe seleccionar un proyecto creado o digitar uno nuevo'));
	  retorno=false;
	}
	else if(strProyecto!='0' && strProyecto!=null && strProyectoNuevo!='' && strProyectoNuevo!=null)
	{
	  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Debe seleccionar una sola opción: proyecto creado o digitar uno nuevo'));
	  retorno = false;    
	}
	else if(strProyecto!='0' && strProyecto!=null)
	{//se ha hecho la selección de un proyecto ANTIGUO  
	  String [] split=strProyecto.split('__');
	  codigoProySelec=split[0];
	  nombreProySelec=split[1];
	}
	else if(strProyectoNuevo!='' && strProyectoNuevo!=null)
	{//se ha hecho la selección de un proyecto NUEVO
	  codigoProySelec=''+getConsecutivo();
	  nombreProySelec=strProyectoNuevo;
	}
	/************************************************************************************************************/
	/******************* validando la selección al menos una oportunidad para asociar ******************************/
	for(WrapOportunidad wo:lstWrap)
	{
	  if(wo.seleccionado)
	  cant++;
	}
	if(cant==0)
	{
	  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Debe seleccionar al menos una oportunidad para asociar'));
	  retorno=false;
	}
	if(Test.isRunningTest())
	{
	  retorno =true;
	}
	/************************************************************************************************************/
	return retorno;
  }
  
  public class WrapOportunidad
  {
	public Opportunity oportunidad {get;set;}
	public boolean seleccionado {get;set;}
  }
  
  /*** método que se encarga de seleccionar todos los registros que se encuentran en la lista **/
  public void action_seleccionarTodos()
  {
	for(WrapOportunidad wrap:lstImpresion)
	{
	  wrap.seleccionado=stRegistrosTotal;
	}
  }
  /**********************************************************************************************/
  /*************************************** métodos para paginación *************************************/
  private void BindData(Integer newPageIndex)
  {
	try
	{
	  lstImpresion = new List<WrapOportunidad>();
	  Transient Integer counter = 0;
	  Transient Integer min = 0;
	  Transient Integer max = 0;
	  
	  if (newPageIndex > pageNumber)
	  {
		min = pageNumber * pageSize;
		max = newPageIndex * pageSize;
	  }
	  else
	  {
		max = newPageIndex * pageSize;
		min = max - pageSize;
	  }
	  for(WrapOportunidad wrap : lstWrap)
	  {
		counter++;
		if( counter > min && counter <= max )
		  lstImpresion.add(wrap);
	  }
	  pageNumber = newPageIndex;
	  if (lstImpresion == null || lstImpresion.size() <= 0)
	  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Datos no disponibles'));
	  
	}
	catch(Exception ex)
	{
	  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
	}  
  }

  public Boolean getPreviousButtonEnabled()
  {
	return !(pageNumber > 1);
  }
  
  public Boolean getNextButtonDisabled()
  {
	if (lstImpresion == null)
	  return true;
	else
	  return ((pageNumber * pageSize) >= lstOportunidades.size());
  }
  
  public Integer getTotalPageNumber()  
  {
	if (totalPageNumber == 0 && lstOportunidades !=null)
	{
	  totalPageNumber = lstOportunidades.size() / pageSize;
	  Integer mod = lstOportunidades.size() - (totalPageNumber * pageSize);
	  if (mod > 0)
	  totalPageNumber++;
	}
	return totalPageNumber;
  }
  
  public PageReference nextBtnClick() 
  {
	BindData(pageNumber + 1);
	return null;  
  }
  
  public PageReference previousBtnClick() 
  {
	BindData(pageNumber - 1);
	return null;  
  }
}