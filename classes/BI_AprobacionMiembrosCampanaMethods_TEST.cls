@isTest
private class BI_AprobacionMiembrosCampanaMethods_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_CampaignMemberMethods class 
    
    History: 
    <Date> 					<Author> 				<Change Description>
    16/04/2014      		Ignacio Llorca    		Initial Version
    22/04/2014				Pablo Oliva				Asserts added
    05/06/2014				Pablo Oliva				General: A new user is created for inserting campaigns
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static{
    	
    	BI_TestUtils.throw_exception = false;
    }
    
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Ignacio Llorca
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_CampaignMemberMethods.updateCampaignMember.
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	16/04/2014      		Ignacio Llorca			Initial Version
 	22/04/2014				Pablo Oliva				Asserts added
 	28/11/2016				Gawron, Julian			Asserts updated for new Dataload values
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/		    
	static testMethod void updateCampaignMember() {
        
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
		List <Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);
		       
		List <Contact> lst_con = BI_DataLoad.loadContacts(100, lst_acc);
		       
		List <Lead> lst_ld = BI_DataLoad.loadLeads(100);
		
		List <Campaign> lst_camp = new List<Campaign>();
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name = 'Administrador del sistema' limit 1];
        
		User usertest = new User(alias = 'testu',
						  email = 'tu@testorg.com',
						  emailencodingkey = 'UTF-8',
						  lastname = 'Test',
						  languagelocalekey = 'en_US',
						  localesidkey = 'en_US',
						  ProfileId = prof.Id,
						  BI_Permisos__c = Label.BI_Administrador,
						  timezonesidkey = Label.BI_TimeZoneLA,
						  username = 'usertestuser@testorg.com');
		
		insert usertest;
		
		system.runAs(usertest){
			lst_camp = BI_DataLoad.loadCampaigns(1);
		}
		      
		Campaign[] camp = [SELECT Id FROM Campaign WHERE Id IN:lst_camp];
		
		camp[0].isActive = true;
		camp[0].Status = 'En curso';
		update camp;
		
		List <CampaignMember> lst_cmpm =BI_DataLoad.loadCampaignMembers(lst_camp, lst_ld, lst_con, 100);
		BI_Aprobacion_Miembros_Campana__c [] app = [SELECT Id FROM BI_Aprobacion_Miembros_Campana__c];
		
		Test.startTest();
		
		CampaignMember[] camp2 = [SELECT Id FROM CampaignMember WHERE BI_Bloqueo__c = true];
		
		system.assertequals (4, app.size()); //JEG 28/11/2016
		system.assertequals (4, camp2.size()); //JEG 28/11/2016
		
		Integer j = 0;
		for (BI_Aprobacion_Miembros_Campana__c amc:app){
			if (j<2){
				amc.BI_Estado__c = Label.BI_Aprobado;
			}
			else{
				amc.BI_Estado__c = Label.BI_Rechazado;
			}
			j++;
		}
		update app;
		
		Integer blocked = 0;
		Integer appI = 0;
		Integer ree = 0;
		
		for(CampaignMember cmember:[SELECT Id, BI_Bloqueo__c, Status FROM CampaignMember]){
			if(cmember.BI_Bloqueo__c)
				blocked++;
			if(cmember.Status == Label.BI_Rechazado)
				ree++;
			else if(cmember.Status == Label.BI_Aprobado)
				appI++;
		}
		
		system.assertequals (0, blocked);
		system.assertequals (2, appI); //JEG 28/11/2016
		system.assertequals (2, ree); //JEG 28/11/2016
		
		Test.stopTest();
     }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}