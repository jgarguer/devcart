@isTest 
public with sharing class VE_Venta_Express_Helper_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Fernández
    Company:       Accenture
    Description:   Test class to manage the code coverage for VE_Venta_Express_Helper class
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    23/02/2017              Alberto Fernández        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Fernández
    Company:       Accenture
    Description:   Test method to manage the code coverage for VE_Venta_Express_Helper.getDependentOptionsImpl
   
    History:
    
    <Date>                  <Author>                <Change Description>
    23/02/2017              Alberto Fernández        Initial version D404
    23/05/2017              Alberto Fernández        Added 'Run As'
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void getDependentOptionsImpl_TEST() {

        Profile standardProfile = [select Id from Profile where Name = 'System Administrator' OR Name = 'Administrador del sistema' limit 1];

        User user = new User(
            Username = 'vehelper@email.com',
            LastName = 'TestUser',
            CommunityNickname = 'nick' + String.valueOf(Math.random()),
            Alias = 'testuser', //max length 8
            Email = 'vehelper@email.com',
            ProfileId = standardProfile.Id,
            EmailEncodingKey='UTF-8',
            BI_Permisos__c = 'Administrador del sistema',
            TimeZoneSidKey = ' Europe/Paris',
            LanguageLocaleKey = 'es',
            LocaleSidKey = 'es_ES',
            Pais__c = 'Spain');
        insert user;

        System.runAs(user) {
            
            Boolean valid = false;

            /*El test se lleva a cabo con el campo picklist padre 'BI_Country__c y el campo picklist dependiente 'BI_Opportunity_Type__c' del objeto Opportunity*/
            Map<String,List<String>> allOptions = VE_Venta_Express_Helper.getDependentOptionsImpl('Opportunity', 'BI_Country__c', 'BI_Opportunity_Type__c');

            /*Comprobamos si entre los valores válidos de la picklist hija para el valor 'Argentina' de la picklist padre, se encuentra 'Convergente'*/
            for(String value : allOptions.get('Argentina')) {
                if(value == 'Convergente') {
                    valid = true;
                }
            }
            System.assert(valid);

        }

	}

}