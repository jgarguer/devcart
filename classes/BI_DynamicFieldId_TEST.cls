@isTest
private class BI_DynamicFieldId_TEST {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to cover BI_DynamicFieldId_TEST

    History: 
    
     <Date>                     <Author>                <Change Description>
    27/10/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest static void getDynamicField_TEST() {
		//Load custom setting
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		BI_DynamicFields__c dynamicVar = new BI_DynamicFields__c(
			Name = '001_test1',
			BI_NombreAPI__c = 'test1',
			BI_IdCampo__c = 'Id_Test1',
			BI_IdCampo_cf__c = 'CFId_Test1',
			BI_sObject__c = 'Account',
			BI_KeyPrefix__c = '001'
		);
		insert dynamicVar;

		String objectName = 'Account';
		Set<string> setFieldToId = new Set<String>();
		setFieldToId.add('test1');
		setFieldToId.add('BI_Country__c');
		
		map<String,String> map_ApiName_Id = BI_DynamicFieldId.getDynamicField(objectName, setFieldToId);		

		system.assert(!map_ApiName_Id.isEmpty());
		system.assert(map_ApiName_Id.containsKey('test1'));
		system.assert(map_ApiName_Id.containsKey('BI_Country__c'));
		system.assertEquals( map_ApiName_Id.get('BI_Country__c'),'CF00N11000000gvSV');

		system.assert(BI_DynamicFields__c.getAll().size() > 1);
	}

	@isTest static void getDynamicFieldwithoutCF_TEST() {
		//Load custom setting
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		BI_DynamicFields__c dynamicVar = new BI_DynamicFields__c(
			Name = '001_test1',
			BI_NombreAPI__c = 'test1',
			BI_IdCampo__c = 'Id_Test1',
			BI_IdCampo_cf__c = 'CFId_Test1',
			BI_sObject__c = 'Account',
			BI_KeyPrefix__c = '001'
		);
		insert dynamicVar;

		String objectName = 'Account';
		Set<string> setFieldToId = new Set<String>();
		setFieldToId.add('test1');
		setFieldToId.add('BI_Country__c');
		
		map<String,String> map_ApiName_Id = BI_DynamicFieldId.getDynamicFieldwithoutCF(objectName, setFieldToId);	

		system.assert(!map_ApiName_Id.isEmpty());	
		system.assert(map_ApiName_Id.containsKey('test1'));
		system.assert(map_ApiName_Id.containsKey('BI_Country__c'));

		system.assertEquals( map_ApiName_Id.get('BI_Country__c'),'00N11000000gvSV');

		system.assert(BI_DynamicFields__c.getAll().size() > 1);

	}
	

	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}