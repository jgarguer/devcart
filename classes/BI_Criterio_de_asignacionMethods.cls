public with sharing class BI_Criterio_de_asignacionMethods {
	

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Guillermo Muñoz
	Company:       New Energy Aborda
	Description:   Test method that manage the code coverage from all exceptions thrown in BI_Criterio_de_asignacionMethods.fillCriteriaReference

	History:
	 
	<Date>              <Author>                   <Change Description>
	13/02/2017          Guillermo Muñoz            Initial Version
	-------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void fillCriteriaReference(List <BI_Criterio_de_asignacion__c> news){

		try{

			if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');

			List <BI_Criterio_de_asignacion__c> toProcess = new List <BI_Criterio_de_asignacion__c>();

			for(BI_Criterio_de_asignacion__c crit : news){

				if(crit.BI_Country__c == 'eHelp'){

					toProcess.add(crit);
				}
			}

			if(!toProcess.isEmpty()){

				for(BI_Criterio_de_asignacion__c crit : toProcess){

					String key = '';

					List <String> lst_aux = new List <String>();
		
					key += crit.BI_Country__c + '-';

					key += crit.CoE_eHelp_Priority__c + '-';
			
					if(crit.CoE_eHelp_Type__c != null){

						lst_aux = crit.CoE_eHelp_Type__c.split(';');
						lst_aux.sort();
						key += String.join(lst_aux, ';') + '-';
					}
					else{

						key += ' -';
					}
					
					
					if(crit.CoE_eHelp_Request_type__c != null){

						lst_aux = crit.CoE_eHelp_Request_type__c.split(';');
						lst_aux.sort();
						key += String.join(lst_aux, ';') + '-';
					}
					else{
						
						key += ' -';
					}

					lst_aux = crit.CoE_eHelp_Owner__c.split(';');
					lst_aux.sort();
					key += String.join(lst_aux, ';');

					if(key.length() > 255){

						key = key.abbreviate(255);
					}

					crit.BI_Referencia_del_criterio__c = key;
				}
			}
		}catch(Exception exc){

			BI_LogHelper.generate_BILog('BI_Criterio_de_asignacionMethods.fillCriteriaReference', 'BI_EN', Exc, 'Trigger');
		}
	}
}