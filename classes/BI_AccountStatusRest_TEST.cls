@isTest
private class BI_AccountStatusRest_TEST
{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_AccountStatusRest.updateAccountStatus
    History:
    
    <Date>              <Author>                <Description>
    17/11/2015        	Fernando Arteaga  		BI_EN - CSB Integration
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void updateAccountStatusRest_TestSuccess() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    	Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Chile');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('CHI');
       	
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        lst_acc[0].BI_CSB_TransactionId__c = '1000';
        lst_acc[0].BI_COL_Estado__c = Label.BI_CSB_Pendiente_Suspension;
        update lst_acc[0];

		BI_Log__c trans = new BI_Log__c(BI_CSB_Record_Id__c = String.valueOf(lst_acc[0].Id).substring(0, 15));
		insert trans;
    
    	RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/customeraccounts/v1/accounts/status/'+lst_acc[0].BI_Id_del_cliente__c;  
        req.httpMethod = 'PUT';
        
        Blob body = Blob.valueof('{"transaction_x": null}');
        req.requestBody = body;
        
        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        //BAD REQUEST
        BI_AccountStatusRest.updateAccountStatus();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //OK
        body = Blob.valueof('{"transaction_x": {"transactionId":"1000", "transactionStatus": {"transactionStatus":"Success", "Error":null}}, "resourceData": null}');
        req.requestBody = body;
        
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', 'CHI');
        
        BI_AccountStatusRest.updateAccountStatus();
        
 		body = Blob.valueof('{"transaction_x": {"transactionId":"777", "transactionStatus": {"transactionStatus":"Success", "Error":null}}, "resourceData": {"account":{"correlatorId":""}}}');
        req.requestBody = body;
        
        RestContext.request = req;
        RestContext.response = res;

        //NOT FOUND
        BI_AccountStatusRest.updateAccountStatus();
        system.assertEquals(404,RestContext.response.statuscode);

 		body = Blob.valueof('{"transaction_x"}');
        req.requestBody = body;
        
        RestContext.request = req;
        RestContext.response = res;

        //NOT FOUND
        BI_AccountStatusRest.updateAccountStatus();
        system.assertEquals(500,RestContext.response.statuscode);
        
        Test.stopTest();
    
    }
    
    static testMethod void updateAccountStatusRest_TestFail() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    	Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Chile');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('CHI');
       	
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        lst_acc[0].BI_CSB_TransactionId__c = '1000';
        lst_acc[0].BI_COL_Estado__c = Label.BI_CSB_Pendiente_Reactivacion;
        update lst_acc[0];

		BI_Log__c trans = new BI_Log__c(BI_CSB_Record_Id__c = String.valueOf(lst_acc[0].Id).substring(0, 15));
		insert trans;
    
    	RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/customeraccounts/v1/accounts/status/'+lst_acc[0].BI_Id_del_cliente__c;  
        req.httpMethod = 'PUT';
        
        //OK - FAIL
        Blob body = Blob.valueof('{"transaction_x": {"transactionId":"1000", "transactionStatus": {"transactionStatus":"Fail", "Error": {"exceptionId":"SVC1008", "exceptionText":"Error forzado en prueba"}}}, "resourceData": null}');
        req.requestBody = body;
        
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        
        BI_AccountStatusRest.updateAccountStatus();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
    
    }
}