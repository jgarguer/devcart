@isTest
private class BI_CampaignMethods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for CampaignMethods class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    15/04/2014              Ignacio Llorca          Initial Version
    16/04/2014              Pablo Oliva             Methods added
    16/04/2014              Ignacio Llorca          Methods added
    05/06/2014              Pablo Oliva             General: A new user is created for inserting campaigns
    06/06/2014              Pablo Oliva             Method "createTaskPreOppTest" updated
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static{
        
        BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_CampaignMethods.assignTaskpreOpp, case: There is an opportunity for the account
   
    History:
    
    <Date>                  <Author>                <Change Description>
    16/04/2014              Pablo Oliva             Initial Version   
    06/06/2014              Pablo Oliva             Number of records changed: Apex CPU time limit exception
    26/05/2014    
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createTaskPreOppTest(){  
        ////////////////NUMBER OF RECORDS TO TEST//////////////
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
            Integer K = 2; //x3 RecordTypes = x3 Campaings
        //////////////////////////////////////////////////////
        
        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List<Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);
         
        List<Contact> lst_con = BI_DataLoad.loadContacts(1, lst_acc);
       
        List<Lead> lst_ld = BI_DataLoad.loadLeads(1);
      
        List <Campaign> lst_camp = new List<Campaign>();
        
        /* Modified Raul Aguera 26/05/2015*/
        //Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name =: Label.BI_Administrador limit 1];

        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = BI_DataLoad.searchAdminProfile(),
                          BI_Permisos__c = Label.BI_Administrador,
                          UserPermissionsMarketingUser = true,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com');
        
        insert usertest;
        
        /*
        User usertest_mkt = new User(alias = 'testmkt',
                          email = 'tmkt@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'TestMKT',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = [select Id from Profile where Name LIKE '%Marketing%' limit 1].Id,
                          BI_Permisos__c = 'Marketing',
                          timezonesidkey = Label.BI_TimeZoneLA,
                          UserPermissionsMarketingUser = true,
                          username = 'usertestusermkt@testorg.com');

        insert usertest_mkt;
        */
        system.runAs(usertest){

        lst_camp = BI_DataLoad.loadCampaignsWithRecordType(K, BI_DataLoad.obtainCampaignRecordType());//1 x 3
        
        }

        List<CampaignMember> lst_cm = BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);

        /*
        CampaignMember camp = [select Id, Campaign.Name, Contact.Account.Name, CampaignId, Contact.AccountId from CampaignMember where *Campaign.RecordType.DeveloperName = 'BI_Venta_de_productos' and * ContactId <> null limit 1];
        
        String name_opp = camp.Campaign.Name+'-'+camp.Contact.Account.Name+'-Preoportunidad';
        
        Opportunity opp = new Opportunity(CloseDate = Date.today(),
                                        StageName = Label.BI_F6Preoportunidad,
                                        Probability = 0,
                                        Name = camp.Campaign.Name+'-'+camp.Contact.Account.Name+'-Preoportunidad',
                                        CampaignId = camp.CampaignId,
                                        AccountId = camp.Contact.AccountId);
                                        
        insert opp;*/

        
        
        Set<Id> set_camp = new Set<Id>();
 

        for(Campaign campL:lst_camp){
            campL.EndDate = date.today().addDays(15);
            campL.Status = Label.BI_EnCurso;
            campL.BI_Crear_tareas_prospectos__c = Label.BI_Si;
            campL.BI_Crear_oportunidad_contactos__c = Label.BI_Si ;
            campL.BI_Tipo_de_preoportunidad__c= Label.BI_NuevaVenta;
            set_camp.add(campL.Id);
        }
        
        Test.startTest();
        
        system.debug('***StartTEST-> UPDATE CAMPAIGNS: '+ lst_camp);
        update lst_camp;
        for(Campaign campL:lst_camp){
            campL.IsActive = true; 
        }
		update lst_camp;
        List<CampaignMemberStatus> lst_campMembStatus = [select Id, CampaignId, Label from CampaignMemberStatus where CampaignId IN :set_camp];
        system.assert(!lst_campMembStatus.isEmpty());

        Test.stopTest();
            
        //List<AsyncApexJob> lst_jobInfo = [SELECT ApexClass.Name FROM AsyncApexJob WHERE ApexClass.Name LIKE '%BI_CampaignMember_Queueable%'];
        //system.assert(!lst_jobInfo.isEmpty());

        List<Opportunity> opp2 = [select Id, Name from Opportunity where RecordType.DeveloperName  = :Label.BI_Preoportunidad_renegociacionRT OR RecordType.DeveloperName  = :Label.BI_Preoportunidad ]; 
        
        List<Task> tasks = [select Id from Task where Subject = :Label.BI_Trigger_CampaignMember_createTask];
        
        // *** Modified Raul Aguera 25/05/2015 ***
        //system.assertEquals(tasks.size(), K*2);//x2 because has created one for each recordtype. 
        
        system.assertEquals(opp2.size(), K*2);//x2 because has created one only for two recordtypes.
        
        
        //}
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_CampaignMethods.cancelTask.
   
    History:
    
    <Date>                   <Author>                <Change Description>
    16/04/2014               Ignacio Llorca          Initial Version       
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*static testMethod void cancelTaskTest(){  
        
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1);
              
        List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);
        
        List <Contact> lst_con =BI_DataLoad.loadContacts(50, lst_acc);
       
        List <Lead> lst_ld =BI_DataLoad.loadLeads(1);//50
      
        List <Campaign> lst_camp =BI_DataLoad.loadCampaigns(1);//50
               
        BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name =: Label.BI_Administrador limit 1];
        
        User usr = new User(alias = 'testu',
                            email = 'tu@testorg.com',
                            emailencodingkey = 'UTF-8',
                            lastname = 'Test',
                            languagelocalekey = 'en_US',
                            localesidkey = 'en_US',
                            ProfileId = prof.Id,
                            BI_Permisos__c = Label.BI_Administrador,
                            timezonesidkey = Label.BI_TimeZoneLA,
                            username = 'usertestuser@testorg.com');
        
        insert usr;
        //User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        
        system.runAs(usr){
            set<Id> set_campId = new set<Id>();
            for (Campaign camp:lst_camp){
                camp.IsActive = true;
                camp.Status = Label.BI_EnCurso;
                set_campId.add(camp.Id);
                
            }
            
            //BI_DataLoad.simulate_job_CampaignMember_Queueable(set_campId, 2);
            
            update lst_camp; 
            
            //camp.IsActive==false && olds[i].IsActive && camp.BI_Cancela_tareas_cierre__c  == Label.BI_Si
            for (Campaign camp:lst_camp){
                camp.IsActive = false;
                camp.BI_Cancela_tareas_cierre__c = Label.BI_Si;
                camp.BI_Cancela_oportunidad_cierre__c  = Label.BI_Si;
            }
            
            Test.startTest();
            
            update lst_camp; 
            
            for(Task tsk:[SELECT Id, WhatId, Status FROM Task WHERE WhatId IN :lst_camp])
                system.assertequals (Label.BI_F1CanceladaSusp, tsk.Status);
           
            Test.stopTest();
        }
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_CampaignMethods.cancelOpp
       
    History: 
    
    <Date>                  <Author>                <Change Description>
    16/04/2014              Ignacio Llorca          Initial Version       
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void cancelOppTest(){  

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1);
        system.assert(!lst_pais.isEmpty());
          
        List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);
        system.assert(!lst_acc.isEmpty());

        List <Contact> lst_con =BI_DataLoad.loadContacts(50, lst_acc);
        system.assert(!lst_con.isEmpty());

        List <Lead> lst_ld =BI_DataLoad.loadLeads(50);
        system.assert(!lst_ld.isEmpty());

        List <Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(2, BI_DataLoad.obtainCampaignRecordType());

        system.assert(!lst_camp.isEmpty());

        List<CampaignMember> lst_campMemb = BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);
        system.assert(!lst_campMemb.isEmpty());

        Set<Id> set_campId = new Set<Id>();
        for (Campaign camp:lst_camp){
            set_campId.add(camp.Id);
            camp.Status = Label.BI_EnCurso;
            camp.BI_Cancela_oportunidad_cierre__c = Label.BI_Si;
            camp.BI_Cancela_tareas_cierre__c = Label.BI_Si; // *** Modified Raul Aguera 25/05/2015 ***
        }
        update lst_camp;

		for (Campaign camp:lst_camp){
            camp.IsActive = true;
		}
		update lst_camp;
		
        for (Campaign camp:lst_camp){
            camp.IsActive = false;
        }
   
        List<CampaignMemberStatus> lst_campMembStatus = [select Id, CampaignId, Label from CampaignMemberStatus where CampaignId IN :set_campId];

        Test.startTest();
        update lst_camp; 
        Test.stopTest();

        for(Opportunity opp:[SELECT Id, StageName FROM Opportunity WHERE CampaignId IN :set_campId])
            system.assertequals (Label.BI_F1CanceladaSusp, opp.StageName);

    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_CampaignMethods.createCampaignMemberStatus
       
    History: 
    
    <Date>                  <Author>                <Change Description>
    16/04/2014              Ignacio Llorca          Initial Version       
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createCampaignMemberStatusTest(){  
        /////////////////////////////////////////////////////
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
                            Integer K = 1;
        /////////////////////////////////////////////////////    
        List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1);
          
        List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);
    
        List <Contact> lst_con =BI_DataLoad.loadContacts(1, lst_acc);
   
        List <Lead> lst_ld =BI_DataLoad.loadLeads(1);
        
        List <Campaign> lst_camp = new List<Campaign>();
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name =: Label.BI_Administrador limit 1];
        
        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof.Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com');
        
        insert usertest;
        
        system.runAs(usertest){
            lst_camp = BI_DataLoad.loadCampaigns(K);
        }
   
        BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);
        
        for (Campaign camp:lst_camp){
            camp.IsActive = true;
            camp.Status = Label.BI_EnCurso;
        }
        
        Test.startTest();
        
        update lst_camp;
        
        CampaignMemberStatus [] cms = [SELECT Id, Label FROM CampaignMemberStatus WHERE CampaignId = : lst_camp[0].Id order by sortorder];
        system.assert(!cms.isEmpty());
        
        Test.stopTest();
      
     }

    /*---------------------------------------------------------------------------------------------------------------- 
    Author:        Gawron, Julián
    Company:       Accenture
    Description:   Test method to manage the code coverage for 
                   BI_CampaignMethods.sendNotificationNewTasksToOwner

   
    History:
    <Date>                  <Author>                <Change Description>
    11/11/2016              Gawron, Julián          Initial Version  D317
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   static testMethod void sendNotificationNewTasksToOwner_TEST(){
       
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1);
        system.assert(!lst_pais.isEmpty());
          
        List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);
        system.assert(!lst_acc.isEmpty());

        List <Contact> lst_con =BI_DataLoad.loadContacts(50, lst_acc);
        system.assert(!lst_con.isEmpty());

        List <Lead> lst_ld =BI_DataLoad.loadLeads(50);
        system.assert(!lst_ld.isEmpty());

        List <Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(2, BI_DataLoad.obtainCampaignRecordType());

        system.assert(!lst_camp.isEmpty());

        List<CampaignMember> lst_campMemb = BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);
        system.assert(!lst_campMemb.isEmpty());

        Set<Id> set_campId = new Set<Id>();
        for (Campaign camp:lst_camp){
            set_campId.add(camp.Id);
            camp.Status = Label.BI_EnCurso;
            camp.BI_Crear_oportunidad_contactos__c = Label.BI_Si;
            camp.BI_Crear_tareas_prospectos__c = Label.BI_Si;
            camp.BI_Cancela_oportunidad_cierre__c = Label.BI_Si;
            camp.BI_Cancela_tareas_cierre__c = Label.BI_Si; // *** Modified Raul Aguera 25/05/2015 ***
            camp.BI_Notificar_tareas__c = True;
        }



        Campaign nueva = new Campaign(
                    Name = 'prueba',
                    Status = Label.BI_EnCurso,
                    BI_Crear_oportunidad_contactos__c = Label.BI_No,
                    BI_Crear_Tareas_Contactos__c = Label.BI_Si,
                    BI_Asunto_de_las_tareas__c = 'testConTareas',
                    BI_Cancela_oportunidad_cierre__c = Label.BI_Si,
                    BI_Cancela_tareas_cierre__c = Label.BI_Si,
                    BI_Notificar_tareas__c = True
            );
 
        lst_camp.add(nueva);

        Campaign nueva2 = new Campaign(
                    Name = 'prueba2',
                    Status = Label.BI_EnCurso,
                    BI_Crear_oportunidad_contactos__c = Label.BI_No,
                    BI_Crear_Tareas_Contactos__c = Label.BI_Si,
                    BI_Asunto_de_las_tareas__c = 'testConProspect',
                    BI_Cancela_tareas_cierre__c = Label.BI_Si,
                    BI_Crear_tareas_prospectos__c = Label.BI_Si,
                    BI_Notificar_tareas__c = True
            );

        lst_camp.add(nueva2);

        upsert lst_camp;

        List <Lead> lst_leads1 = BI_DataLoad.loadLeads(2);


        List<CampaignMember> lst_campaignMembers = new List<CampaignMember>();
        lst_campaignMembers.add(new CampaignMember(CampaignId = nueva.Id, ContactId = lst_con[0].id, Status = 'Sent'));
        lst_campaignMembers.add(new CampaignMember(CampaignId = nueva.Id, ContactId = lst_con[1].id, Status = 'Sent')); 

        lst_campaignMembers.add(new CampaignMember(CampaignId = nueva2.Id, LeadId = lst_leads1[0].id, Status = 'Sent'));
        lst_campaignMembers.add(new CampaignMember(CampaignId = nueva2.Id, LeadId = lst_leads1[1].id, Status = 'Sent'));

        Contact contacto = new Contact(LastName='lala');
        insert contacto;

        lst_campaignMembers.add(new CampaignMember(CampaignId = lst_camp[0].Id, ContactId = contacto.id, Status = 'Sent'));
        lst_campaignMembers.add(new CampaignMember(CampaignId = lst_camp[0].Id, LeadId = lst_leads1[1].id, Status = 'Sent'));
        insert lst_campaignMembers;



        for (Campaign camp1:lst_camp){
            camp1.IsActive = true;
        }
        nueva.IsActive = true;
        nueva2.IsActive = true;

        Test.startTest();
        update lst_camp;

        Test.stopTest();

     }

    /*---------------------------------------------------------------------------------------------------------------- 
    Author:        Gawron, Julián
    Company:       Accenture
    Description:   Test method to manage the code coverage for 
                   BI_CampaignMethods.sendNotificationNewTasksToOwner Exceptions

   
    History:
    <Date>                  <Author>                <Change Description>
    11/11/2016              Gawron, Julián          Initial Version  D317
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
     static testMethod void TestExceptions(){
        Campaign nueva2 = new Campaign(
                    Name = 'prueba2',
                    Status = Label.BI_EnCurso,
                    BI_Crear_oportunidad_contactos__c = Label.BI_No,
                    BI_Crear_Tareas_Contactos__c = Label.BI_Si,
                    BI_Asunto_de_las_tareas__c = 'testConProspect',
                    BI_Cancela_tareas_cierre__c = Label.BI_Si,
                    BI_Crear_tareas_prospectos__c = Label.BI_Si,
                    BI_Notificar_tareas__c = True
            );

        insert nueva2;
         BI_TestUtils.throw_exception=true;
         Test.startTest();
         Set<Id> lista = new Set<Id>{nueva2.Id};
        BI_CampaignMethods.sendNotificationNewTasksToOwner(lista);
        Test.stopTest();
     } 
   

     
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}