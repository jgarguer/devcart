public with sharing class BI_CSB_OrderMethods
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Methods related to NE__Order__c object
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    04/12/2015              Fernando Arteaga        Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Checks the order composition (only digital services-->D, only traditional services-->T or both types of services-->B)
    				return a list of NE__OrderItem__c (to create an internal case related to the parent orders)
    
    History:
    
    <Date>            <Author>          	<Description>
    04/12/2015        Fernando Arteaga      Initial version
    26/01/2017		  Guillermo Muñoz		Added List filter to avoid innecesary querys
    23/05/2017		  Álvaro López			Added Internal Case RecordType Label to avoid null object when try to getRecordTypeId in other language.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void createInternalCases(List<NE__Order__c> newOrders, List<NE__Order__c> oldOrders)
	{
		try
		{
			List<Case> listCases = new List<Case>();
			Boolean csbProducts = false;
			Boolean traditProducts = false;
			String rt_casoInterno = Label.BI_O4_Caso_Interno;
			Id RT_ORDER = Schema.SObjectType.NE__Order__c.getRecordTypeInfosByName().get('Order').getRecordTypeId();
			Id RT_OPTY = Schema.SObjectType.NE__Order__c.getRecordTypeInfosByName().get('Opty').getRecordTypeId();
			Id RT_INTERNAL_CASE = Schema.SObjectType.Case.getRecordTypeInfosByName().get(rt_casoInterno).getRecordTypeId();
			System.debug(LoggingLevel.INFO, 'RT_ORDER:' + RT_ORDER);
			
			Set<Id> setOrderIds = new Set<Id>();
			Map<Id, String> mapOrderComposition = new Map<Id, String>();
			Map<Id, NE__OrderItem__c> mapOrders = new Map<Id, NE__OrderItem__c>();
			List<NE__OrderItem__c> listOrders = new List<NE__OrderItem__c>();
			Map<Id, Id> mapOrderCase = new Map<Id, Id>();
		
			for (Integer i=0; i < newOrders.size(); i++)
			{
				System.debug(newOrders[i].RecordTypeId);
				System.debug(oldOrders[i].RecordTypeId);
				if (newOrders[i].RecordTypeId.equals(RT_ORDER) && oldOrders[i].RecordTypeId.equals(RT_OPTY))
				{
					System.debug(LoggingLevel.INFO, 'rt:' + newOrders[i].RecordTypeId);
					setOrderIds.add(newOrders[i].Id);
				}
			}
		
			System.debug(LoggingLevel.INFO, 'setOrderIds:' + setOrderIds);
			//GMN 26/01/2017 - Add list filter to avoid innecesary querys
			if(!setOrderIds.isEmpty() || Test.isRunningTest()){
			
				for (NE__OrderItem__c item: [SELECT Id, Name, NE__Status__c, NE__StartDate__c, NE__EndDate__c, NE__Qty__c, Installation_point__c, NE__SmartPart__c, 
		                                            NE__PromotionId__c, NE__FulfilmentStatus__c, Configuration_Type__c, Configuration_SubType__c, NE__Action__c, 
		                                            NE__RecurringChargeFrequency__c, NE__Penalty__c, NE__Penalty_Activated__c, NE__Commitment__c, NE__Commitment_Period__c, 
		                                            NE__RecurringChargeOv__c, CurrencyIsoCode, NE__OneTimeFeeOv__c, NE__Penalty_Fee__c, NE__Commitment_Expiration_Date__c, 
		                                            NE__Commitment_UOM__c, NE__ProdId__r.Name, NE__OrderId__c, NE__Account__c, NE__CatalogItem__r.BI_CSB_Code__c,
		                                            NE__OrderId__r.NE__OptyId__c, NE__Account__r.BI_Country__c
		                                	 FROM NE__OrderItem__c
											 WHERE NE__OrderId__c IN :setOrderIds
											 ORDER BY NE__OrderId__c])
				{
					System.debug(LoggingLevel.INFO, 'orderItem:' + item);
					mapOrders.put(item.NE__OrderId__c, item);
					
					if (item.NE__CatalogItem__r.BI_CSB_Code__c != null)
					{
						if (mapOrderComposition.containsKey(item.NE__OrderId__c))
						{
							if (mapOrderComposition.get(item.NE__OrderId__c) == null)
								mapOrderComposition.put(item.NE__OrderId__c, 'D');
							else if (mapOrderComposition.get(item.NE__OrderId__c) == 'T')
								mapOrderComposition.put(item.NE__OrderId__c, 'B');
						}
						else
							mapOrderComposition.put(item.NE__OrderId__c, 'D');
					}
					else
					{
						if (mapOrderComposition.containsKey(item.NE__OrderId__c))
						{
							if (mapOrderComposition.get(item.NE__OrderId__c) == null)
								mapOrderComposition.put(item.NE__OrderId__c, 'T');
							else if (mapOrderComposition.get(item.NE__OrderId__c) == 'D')
								mapOrderComposition.put(item.NE__OrderId__c, 'B');
						}
						else
							mapOrderComposition.put(item.NE__OrderId__c, 'T');
					}

				}
			
				if (!mapOrderComposition.isEmpty())
				{
					for (Id orderId: mapOrderComposition.keySet())
					{
						if (mapOrderComposition.get(orderId) == 'B')
							listOrders.add(mapOrders.get(orderId));
					}
				
					Map<String, BI_CSB_Informacion_Caso__c> mapConfigCaso = BI_CSB_Informacion_Caso__c.getAll();
					for (NE__OrderItem__c item: listOrders)
					{
						Case internalCase = new Case(RecordTypeId = RT_INTERNAL_CASE, AccountId = item.NE__Account__c, Status = 'New', Priority = 'Medium', Origin = 'Orden de Servicio', BI_Country__c = item.NE__Account__r.BI_Country__c, 
													 Subject = 'Alta de Servicios', BI_Nombre_de_la_Oportunidad__c = item.NE__OrderId__r.NE__OptyId__c, BI_Oferta_asociada__c = item.NE__OrderId__c, Order__c = item.NE__OrderId__c);
						if (!mapConfigCaso.isEmpty() && mapConfigCaso.get(item.NE__Account__r.BI_Country__c) != null)
						{
							internalCase.BI_Department__c = mapConfigCaso.get(item.NE__Account__r.BI_Country__c).BI_CSB_Department__c;
							internalCase.BI_Subtype__c = mapConfigCaso.get(item.NE__Account__r.BI_Country__c).BI_CSB_Solicitation_Subtype__c;
							internalCase.BI_Type__c = mapConfigCaso.get(item.NE__Account__r.BI_Country__c).BI_CSB_Solicitation_Type__c;
						}								 
						listCases.add(internalCase);
					}
					
					if (!listCases.isEmpty())
					{
						insert listCases;
						// Fill Case field in the order
						for (Case c: listCases)
							mapOrderCase.put(c.Order__c, c.Id);
						
						updateOrderCase(mapOrderCase);
					}
				}
			}
			
		}
		catch (Exception exc)
		{
            BI_LogHelper.generate_BILog('BI_CSB_OrderMethods.createInternalCase', 'BI_EN', exc, 'Trigger');
            //return new PageReference('/' + orderId);
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Updates Case field in the order (for CSB orders)
    
    History:
    
    <Date>            <Author>          	<Description>
    30/03/2016        Fernando Arteaga      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@future(callout=false)
	public static void updateOrderCase(Map<Id, Id> mapOrdCase)
	{
		List<NE__Order__c> listOrder = [SELECT Id, Case__c FROM NE__Order__c WHERE Id IN :mapOrdCase.keySet() FOR UPDATE];
		if (!listOrder.isEmpty())
		{
			listOrder[0].Case__c = mapOrdCase.get(listOrder[0].Id);
			update listOrder[0];
		}
	}
}