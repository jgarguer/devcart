public with sharing class PCA_Reclamo_PopUpNew extends PCA_HomeController{
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Pop up to create a new Reclamo
    
    History:
    
    <Date>            <Author>            <Description>
    26/06/2014        Ana Escrich           Initial version
    23/02/2016        Guillermo Muñoz     Added configuration object for allow to include new countries
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  public Contact ConsultaContact{get;set;}
  public Account ConsultaAccount{get;set;}
  public Case newCase {get; set;}
  public String Tipo {get;set;}
  public boolean haveError  {get;set;}
  public String EqTel {get;set;}  
  public Id IdAttach{get;set;}
  public String prev{get;set;}
  public  boolean adjuntos {get; set;}
  public String siteName {get; set;}
  public Id siteInput {get; set;}
  public String CIname {get; set;}
  public Id CIinput {get; set;}
  public string StringWSCreacionPortal {get; set;}
  public BIIN_Creacion_Ticket_WS.CamposOutsiders co;
  public String country {get; set;}
  public BI_Configuracion_Caso_PP__c ccpp {get; set;}
  public string caseType     {get; set;}
  public String countryName   {get; set;}
  public Map <String, Id> map_rt {get; set;}
  public boolean isTech    {get;set;}  
  public boolean hasChosen    {get;set;}
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Get type of the Case
    
    History:
    
    <Date>            <Author>            <Description>
    26/06/2014        Ana Escrich           Initial version
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
  public List<Schema.FieldSetMember> getFields() {
    try{
      if(BI_TestUtils.isRunningTest()){ 
        throw new BI_Exception('test');
      }

      if(isTech){
            return SObjectType.Case.FieldSets.BIIN_NewCasoTecnico.getFields();
      }else{
            return SObjectType.Case.FieldSets.PCA_NewCaseComercial.getFields();
      }
    }catch (exception Exc){
       BI_LogHelper.generate_BILog('PCA_Reclamo_PopUpNew.getFields', 'Portal Platino', Exc, 'Class');
       return null;
    }
  }
  
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
      Author:        Ana Escrich
      Company:       Salesforce.com
      Description:   check permissions of current user
      
      History:
      
      <Date>            <Author>            <Description>
      26/06/2014        Ana Escrich           Initial version
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
  public PageReference checkPermissions (){
    try{
      if(BI_TestUtils.isRunningTest()){
        throw new BI_Exception('test');
      }

      PageReference page = enviarALoginComm();
      if(page == null){
        loadInfo();
      }
      return page;
    }catch (exception Exc){
       BI_LogHelper.generate_BILog('PCA_Reclamo_PopUpNew.checkPermissions', 'Portal Platino', Exc, 'Class');
       return null;
    }
  }
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Constructor
    
    History:
    
    <Date>            <Author>            <Description>
    05/07/2014        Antonio Moruno        Initial version
    09/10/2015        Jose Miguel Fierro    Added BI_EN II Filter
    23/02/2016        Guillermo Muñoz       Changed for allow to include new countries
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
  public PCA_Reclamo_PopUpNew() {

    countryName = [SELECT Id, Pais__c FROM User WHERE Id = :UserInfo.getUserId()].Pais__c;
    ccpp = loadCC();
    map_rt = loadRecordTypes();    
  }

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info of Reclamos
    
    History:
    
    <Date>            <Author>            <Description>
    05/07/2014        Antonio Moruno       Initial version
    09/10/2015        Jose Miguel Fierro   Added BI_EN II Filter by country
    23/02/2016        Guillermo Muñoz      Changed for allow to include new countries
    12/06/2017        Alvaro Sevilla       Change fixed value per tag
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
  public void loadInfo (){
    try{
      if(BI_TestUtils.isRunningTest()){ 
        throw new BI_Exception('test');
      }
      newCase = new case();
      List <String> lst_rts = ccpp.BI_Tipos_de_registro__c.split(';');
      if(lst_rts.size()==1){        
        caseType = lst_rts[0];
        //no se filtra si existe el tipo de registro en el mapa ya que si no existe queremos que se lance una excepción.
        newCase.RecordTypeId = map_rt.get(lst_rts[0]);
      }
      EqTel = (apexpages.currentpage().getparameters().get('EqTel')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('EqTel')):null; 
      newCase.AccountId = BI_AccountHelper.getCurrentAccountId();
      newCase.ParentId = (apexpages.currentpage().getparameters().get('Id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('Id')):null;
            
      if(EqTel == 'true') {
        newCase.Reason = 'Administrative Request';
        newCase.Subject = System.Label.BI_PCA_AsuntoCasoComercial;//Se cambio valor fijo: "Prueba [TDB]" 12/06/2017
      }
      haveError = true; 
      system.debug('newCaseConstructor: '+ newCase);

      if(caseType != null){
        if(caseType == Label.BI_Caso_comercial_2 || caseType == Label.BI_Caso_agrupador){
          isTech = false;
          hasChosen = true;
        }
        //**************Meter aqui futuros tipos de registro**************//
      }
            
    }catch (exception Exc){
      Id idlog = BI_LogHelper.generate_BILog('PCA_Reclamo_PopUpNew.loadInfo', 'Portal Platino', Exc, 'Class');
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.BI_Error_visualforce + idlog));
    }                                                                                  
  }
  
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        Antonio Moruno
  Company:       Salesforce.com
  Description:   Change Case type
  
  History:
  
  <Date>            <Author>            <Description>
  04/07/2014        Antonio Moruno       Initial version
  09/10/2015        Jose Miguel Fierro   Added BI_EN II Filter
  23/02/2016        Guillermo Muñoz      Changed for allow to include new countries
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
  public void changeCaseType(){
    try{
      if(BI_TestUtils.isRunningTest()){
        throw new BI_Exception('test');
      }
      
      if(caseType == Label.BI_Caso_comercial_2 || caseType == Label.BI_Caso_agrupador){
        isTech = false;
        hasChosen = true;
      }
      else if (caseType==Label.BIIN_TituloSolicInciden){
        isTech = true;
        hasChosen = true;
        newCase.RecordTypeId  = [SELECT Id, Name FROM RecordType WHERE Name =: Label.BIIN_TituloSolicInciden].Id;
        system.debug(caseType + isTech +' LoadInfo<<<<<<<<<<caseType');
        ConsultaContact = new Contact();
        ConsultaAccount = new Account();
        User myUser= [Select ContactId,  Contact.Account.Name from User where Id=:userinfo.getuserId()][0];
        ConsultaContact =  [SELECT Name, FirstName, LastName, Email, AccountId, Id FROM Contact WHERE id =: myUser.ContactId ][0];
        ConsultaAccount =  [SELECT Name, Id FROM Account WHERE id =: ConsultaContact.AccountId ][0];
        newCase.ContactId=myUser.ContactId;
        /*newCase.BI2_Nombre_Contacto_Final__c = ConsultaContact.FirstName;
        newCase.BI2_Apellido_Contacto_Final__c = ConsultaContact.LastName;
        newCase.BI2_Email_Contacto_Final__c = ConsultaContact.Email;*/
        newCase.Reason = Label.BIIN_CaseReason;
        newCase.OwnerId = [select OwnerId from account where id =: ConsultaContact.AccountId  limit 1].OwnerId;
        validarUserBackground();
      }
      else{
        hasChosen = false;
      }
        
      system.debug(caseType+ isTech +' cambiar tipo funcion changeCaseType <<<<<<<<<<caseType');
    }catch (exception Exc){
        BI_LogHelper.generate_BILog('PCA_Reclamo_PopUpNew.changeCaseType', 'Portal Platino', Exc, 'Class');
    }
  }
  
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        Antonio Moruno
  Company:       Salesforce.com
  Description:   Save Case 
  
  History:
  
  <Date>            <Author>            <Description>
  04/07/2014        Antonio Moruno       Initial version
  09/10/2015        Jose Miguel Fierro   Added BI_EN II Filter
  13/01/2015        Jose Miguel Fierro   Added contact assignation if case's recordtype = Caso Agrupador
  20/10/2015        Jose Miguel Fierro   Save cases as non-confidential
  23/02/2016        Guillermo Muñoz      Changed for allow to include new countries
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
  public void saveCase(){
    system.debug('caseType: '+caseType);
    system.debug('Label.BIIN_TituloSolicInciden: '+Label.BIIN_TituloSolicInciden);
    try{
      if(BI_TestUtils.isRunningTest()){
        throw new BI_Exception('test');
      }
      
      List<User> lstUser = [SELECT Id, ContactId FROM User WHERE Id = : UserInfo.getUserId()];

      
      if(newCase.RecordTypeId == null){
        newCase.RecordTypeId = map_rt.get(caseType);
      }
      system.debug('NOMBRE APELLIDO --->' +  newCase.BI2_Nombre_Contacto_Final__c + '---------------- ' +  newCase.BI2_Apellido_Contacto_Final__c + '----ID-----' + newCase.OwnerId );
      if (caseType==Label.BIIN_TituloSolicInciden){
          newCase.BI_Confidencial__c = false;
          newCase.Status='In Progress';
          if(newCase.BI_COL_Fecha_Radicacion__c == null){
              newCase.BI_COL_Fecha_Radicacion__c = Datetime.now();
          }
      }
      //////////////////////Asignación de valores de la configuración//////////////////////
      newCase.BI_Country__c = ccpp.BI_Country__c;
      newCase.Origin = ccpp.BI_Origen__c;
      newCase.Type = ccpp.BI_Type__c;
      newCase.BI_Confidencial__c = ccpp.BI_Confidencial__c;
      if(ccpp.BI_COL_Fecha_Radicacion__c){
        newCase.BI_Col_Fecha_Radicacion__c = Datetime.now();
      }
      /////////////////////////////////////////////////////////////////////////////////////

      haveError = PCA_CaseHelper.saveCase(newCase);
    
    }catch (exception Exc){
      BI_LogHelper.generate_BILog('PCA_Reclamo_PopUpNew.saveCase', 'Portal Platino', Exc, 'Class');
    }  
  }
  
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        Antonio Moruno
  Company:       Salesforce.com
  Description:   get reclamo items
  
  History:
  
  <Date>            <Author>            <Description>
  04/07/2014        Antonio Moruno       Initial version
  09/10/2015        Jose Miguel Fierro   Added BI_EN II Filter
  23/02/2016        Guillermo Muñoz      Changed for allow to include new countries
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  public List<SelectOption> getItems() {
    try{
      if(BI_TestUtils.isRunningTest()){
        throw new BI_Exception('test');
      }
      List<SelectOption> options = new List<SelectOption>();
      List <String> lst_rts = ccpp.BI_Tipos_de_registro__c.split(';');
      if(!lst_rts.isEmpty()){
        for(String str : lst_rts){
          if(str == Label.BI_Caso_agrupador){
            options.add(new SelectOption(str,'Caso agrupador'));
          }
          else if(str == Label.BI_Caso_comercial_2){
            options.add(new SelectOption(str,Label.BI2_Caso_Comercial));
          }          
        }
      }
      else{
        options.add(new SelectOption('fallo','fallo'));
      }
             
      return options;
    }catch (exception Exc){
      BI_LogHelper.generate_BILog('PCA_Reclamo_PopUpNew.getItems', 'Portal Platino', Exc, 'Class');
      return null;
    }     
  }

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda
    Description:   Method that loads the configuration of the new case filtered by country
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    23/02/2016                      Guillermo Muñoz             Initial version
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  public BI_Configuracion_Caso_PP__c loadCC(){

    BI_Configuracion_Caso_PP__c aux = new BI_Configuracion_Caso_PP__c();

    try{

      if(BI_TestUtils.isRunningTest()){
        throw new BI_Exception('test');
      }
          
      aux = [SELECT BI_Confidencial__c, BI_COL_Fecha_Radicacion__c, BI_Type__c, BI_Tipos_de_registro__c, BI_Country__c, BI_Origen__c FROM BI_Configuracion_Caso_PP__c WHERE BI_Country__c =: countryName LIMIT 1];

    }catch (Exception exc){
      BI_LogHelper.generate_BILog('PCA_Reclamo_PopUpNew.loadCC', 'Portal Platino', Exc, 'Class');
    }

    return aux;
  }
   
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda
    Description:   Method that return a map with all cases record types
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    23/02/2016                      Guillermo Muñoz             Initial version
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
  public Map <String, Id> loadRecordTypes(){

    Map <String,Id> mapa = new Map <String,Id>();
    RecordType [] lst_rt = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Case'];
    if(!lst_rt.isEmpty()){
      for(RecordType rt : lst_rt){
        mapa.put(rt.DeveloperName, rt.Id);
      }
    }
    else{
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Se ha producido un error de configuración, contacte con su administrador'));
    }
    return mapa;
  }
    
//************************************************************************
//*VALIDACIÓN CLIENTE BACKGROUND CREACIÓN INCIDENCIA TÉCNICA
//************************************************************************
  public void validarUserBackground(){
    BIIN_Obtener_Token_BAO ot=new BIIN_Obtener_Token_BAO();
    BIIN_Obtener_Contacto_WS cfs=new BIIN_Obtener_Contacto_WS();
    String authorizationToken = ot.obtencionToken();
    BIIN_Obtener_Contacto_WS.ContactSalida salida = new BIIN_Obtener_Contacto_WS.ContactSalida();
    salida = cfs.getContact(authorizationToken, ConsultaContact.FirstName, ConsultaContact.LastName, ConsultaContact.Email, ConsultaAccount.Id);
    system.debug('[USERINFO]: nombre: '+ConsultaContact.FirstName+' apellidos: '+ConsultaContact.LastName+' email: '+ConsultaContact.Email );
    if(salida!=null){
      for(Integer i=0;i<salida.outputs.output.size();i++){
        system.debug('nombre: '+salida.outputs.output[i].lastName+' apellidos: '+salida.outputs.output[i].firstName+' email: '+salida.outputs.output[i].internetEmail);
        newCase.BI2_Apellido_Contacto_Final__c = salida.outputs.output[i].lastName;
        newCase.BI2_Nombre_Contacto_Final__c  = salida.outputs.output[i].firstName;
        newCase.BI2_Email_Contacto_Final__c = salida.outputs.output[i].internetEmail;
      }
    }
  }
  
  //************************************************************************
  //*Llamada al WS de creacion del ticket
  //************************************************************************
  public void crearTicketRoD(){
    if (caseType==Label.BIIN_TituloSolicInciden){//Hacemos la llamada al webService de creación, controlaremos que tengamos adjuntos o no
      co=new BIIN_Creacion_Ticket_WS.CamposOutsiders();
      newCase.BI_Confidencial__c = false;
      newCase.Origin = 'Customer Web Portal';
      BIIN_Obtener_Token_BAO ot=new BIIN_Obtener_Token_BAO();
      BIIN_Creacion_Ticket_WS ct=new BIIN_Creacion_Ticket_WS();
      String authorizationToken = ot.obtencionToken();
      Integer nIntento=0;
      Boolean exito=false;
      system.debug('Tenemos adjuntos: '+adjuntos);
      if(!adjuntos){
        do{
          system.debug('nIntento: '+nIntento+' Entramos en el WS de crearTicket con Caso: '+newCase);
          exito=ct.crearTicket(authorizationToken, newCase, null, co);
          nIntento++;
        }while((nIntento<3)&&(exito==false));
        if(!exito){
          newCase.Status='Confirmation pending';
          StringWSCreacionPortal = 'PENDING';
          system.debug('ERROR: Lanzamos la task');
          Task tarea = new Task();
          tarea.WhatId = newCase.Id;
          tarea.WhoId = newCase.ContactId;
          tarea.Subject = newCase.Subject;
          tarea.Description = newCase.Description;
          //tarea.Status = 'Pdte. Confirmación';
          //tarea.Priority = 'High';
          insert tarea;     
        }
      }
      if(newCase.BI_COL_Fecha_Radicacion__c == null){
        newCase.BI_COL_Fecha_Radicacion__c = Datetime.now();
      }
    }  
  }
  
}