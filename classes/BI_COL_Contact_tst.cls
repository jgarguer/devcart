/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for class BI_COL_MS_Validation_ctr.
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     --------------- 
* @version   1.0    2015-09-03      José Esteban Heredia (JEH)      Create class
*************************************************************************************/
@isTest
private class BI_COL_Contact_tst 
{
    public static List <User>                               lstUsuarios;
    public static User                                      objUsuario;
    public static list <UserRole>                           lstRoles;

    public static void crearData()
    {
        ////perfiles
        //list <Profile> lstPerfil                        = new list <Profile>();
        //lstPerfil                       = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
        //system.debug('datos Perfil '+lstPerfil);
                
        ////usuarios
        ////lstUsuarios = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

        ////lstRol
        //lstRoles = new list <UserRole>();
        //lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        ////ObjUsuario
        //objUsuario = new User();
        //objUsuario.Alias = 'standt';
        //objUsuario.Email ='pruebas@test.com';
        //objUsuario.EmailEncodingKey = '';
        //objUsuario.LastName ='Testing';
        //objUsuario.LanguageLocaleKey ='en_US';
        //objUsuario.LocaleSidKey ='en_US'; 
        //objUsuario.ProfileId = lstPerfil.get(0).Id;
        //objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        //objUsuario.UserName ='pruebas@test.com';
        //objUsuario.EmailEncodingKey ='UTF-8';
        //objUsuario.UserRoleId = lstRoles.get(0).Id;
        //objUsuario.BI_Permisos__c ='Sucursales';
        //objUsuario.Pais__c='Colombia';
        //insert objUsuario;
        
        //lstUsuarios = new list<User>();
        //lstUsuarios.add(objUsuario);

        //System.runAs(lstUsuarios[0])
        //{       
            //Roles
            //list <UserRole> lstRoles                = new list <UserRole>();
            //lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
            //system.debug('datos Rol '+lstRoles);
            
            //Cuentas
            Account objCuenta                                       = new Account();      
            objCuenta.Name                                  = 'prueba';
            objCuenta.BI_Country__c                         = 'Colombia';
            objCuenta.TGS_Region__c                         = 'América';
            objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
            objCuenta.CurrencyIsoCode                       = 'COP';
            objCuenta.BI_Fraude__c                          = false;
            insert objCuenta;
            system.debug('datos Cuenta '+objCuenta);

            Account objCuenta2                                       = new Account();
            objCuenta2.Name                                  = 'prueba';
            objCuenta2.BI_Country__c                         = 'Argentina';
            objCuenta2.TGS_Region__c                         = 'América';
            objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
            objCuenta2.CurrencyIsoCode                       = 'COP';
            objCuenta2.BI_Fraude__c                          = true;
            insert objCuenta2;
            
            //Contactos
            Contract objContrato                                     = new Contract ();
            objContrato.AccountId                           = objCuenta.Id;
            objContrato.StartDate                           =  System.today().addDays(+5);
            objContrato.BI_COL_Formato_Tipo__c              = 'Contrato Marco';
            objContrato.BI_COL_Tipo_contrato__c             = 'Contrato Telefónica';
            objContrato.ContractTerm                        = 12;
            objContrato.BI_COL_Presupuesto_contrato__c      = 1000000;
            objContrato.BI_Indefinido__c                    = 'Si';
            objContrato.BI_COL_Cuantia_Indeterminada__c     = true;
            objContrato.BI_COL_Duracion_Indefinida__c       = true;
//            objContrato.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
            objContrato.StartDate                           = System.today().addDays(+5);

            insert objContrato;
            System.debug('datos Contratos ===>'+objContrato);
        //}
    }

    @isTest static void test_method_one() 
    {
        objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
            Test.startTest();
            Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
            crearData();
            Test.stopTest();
        }
    }
    
}