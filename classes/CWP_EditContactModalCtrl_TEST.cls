/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CWP_EditContactModalCtrl_TEST {
@isTest
    static void test_getloadInfo(){
        Contact cont1 =  new Contact(FirstName='TestgetloadInfo', LastName='TestgetloadInfo');
        insert cont1;
        cont1 = [Select Name, Email, Phone, MobilePhone, BI_Activo__c, Title FROM Contact Where Id =:cont1.Id];
        Contact cont2 = CWP_EditContactModalCtrl.getloadInfo(cont1.Id);
        System.assertEquals(cont1, cont2); 
        delete cont1;
    }
}