public class BI2_COL_SiteSearch_ctr {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Torres
    Company:       Avanxo
    Description:   Clase asociada al Pop Up de búsqueda de la Sede para Colombia, usando la interffaz de consulta de contactos de RoD.
                   Basada en la clase BIIN_ContactoFinalSearch_Ctrl.

    History:

    <Date>           <Author>                                                   <Description>
    07/02/2017       Antonio Torres                                             Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String query1                {get;set;}
    public String query2                {get;set;}
    public String query3                {get;set;}
    public List<Case> lcases            {get;set;}
    public Case caso                    {get;set;}
    public boolean BoolDeshabilitar     {get;set;}
		public boolean mensage     {get;set;}
    public Integer queryLength1         {get;set;}
    public Integer queryLength2         {get;set;}
    public boolean MostrarError         {get;set;}

    private transient HttpRequest req;
    public HttpRequest getHttpRequest () {
        return req;
    }

    private transient HttpResponse res;
    public HttpResponse getHttpResponse () {
        return res;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Constructor, donde se definen las variables globales y se toman los valores heredados de nombre, apellido y contacto de la página padre
                        de creación.
        History:

        <Date>            <Author>                      <Description>
        07/02/2017        Antonio Torres                Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI2_COL_SiteSearch_ctr() {
				mensage=false;
        MostrarError = false;
        query1 = Label.BI2_COL_Valor_parametro_nombre_consulta_sede;
        query2 = ApexPages.currentPage().getParameters().get('siteName');
        query3 = ApexPages.currentPage().getParameters().get('accountfield');

        System.debug('\n\n-=#=-\n' + 'query1' + ': ' + query1 + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'query2' + ': ' + query2 + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'query3' + ': ' + query3 + '\n-=#=-\n');

        queryLength1 = query1.length();
        queryLength2 = query2.length();

        if(queryLength1 >= 3 || queryLength2 >= 3) {
            BoolDeshabilitar= false;
        } else {
            BoolDeshabilitar= true;
        }

        lcases = new List<Case>();
        caso = new Case();
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función que obtiene la lista de contactos.

        History:

        <Date>            <Author>                          <Description>
        07/02/2017        Antonio Torres                Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void getContact() {
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'getContact()' + '   <<<<<<<<<<\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'query1' + ': ' + query1 + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'query2' + ': ' + query2 + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'query3' + ': ' + query3 + '\n-=#=-\n');
        
        mensage      = false;
        MostrarError = false;
        BIIN_Obtener_Token_BAO ot       = new BIIN_Obtener_Token_BAO();
        BIIN_Obtener_Contacto_WS cfs    = new BIIN_Obtener_Contacto_WS();
        String authorizationToken       = ''; // ot.obtencionToken();

        BIIN_Obtener_Contacto_WS.ContactSalida salida = new BIIN_Obtener_Contacto_WS.ContactSalida();
        salida = cfs.getContact(authorizationToken, query1, '', query2, query3);

        System.debug('\n\n-=#=-\n' + 'salida' + ': ' + salida + '\n-=#=-\n');
        
        lcases = new List<Case>();

        if(salida != null && salida.outputs != null && !salida.outputs.output.isEmpty() && salida.outputs.output[0].code == null) {
            if(salida.outputs.output[0].code == null) {
                for(Integer i=0;i<salida.outputs.output.size();i++) {
                    System.debug('\n\n-=#=-\n' + 'salida.outputs.output[i]' + ': ' + salida.outputs.output[i] + '\n-=#=-\n');
                    caso.BIIN_Site__c                   = salida.outputs.output[i].lastName;
                    caso.TGS_Site_details_contacts__c   = salida.outputs.output[i].internetEmail;
                    lcases.add(caso);
                    caso = new Case();
                }
            } else {
                MostrarError = true;
                mensage=true;
                caso.BIIN_Site__c = salida.outputs.output[0].message;
            }
        } else {
             mensage=true;
             MostrarError = true;
        }

        System.debug('\n\n-=#=-\n' + 'lcases' + ': ' + lcases + '\n-=#=-\n');
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Torres
        Company:       Avanxo
        Description:   Función que cuenta el número de caracteres de los campos de búsqueda para habilitar el botón Ir.

        History:

        <Date>            <Author>                          <Description>
        07/02/2017        Antonio Torres                Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void HabilitarIr() {
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'HabilitarIr' + '   <<<<<<<<<<\n-=#=-\n');

        queryLength1 = query1.length();
        queryLength2 = query2.length();

        if((queryLength1 >= 3) || (queryLength2 >= 3)) {
            BoolDeshabilitar= false;
        } else {
            BoolDeshabilitar= true;
        }

        System.debug('\n\n-=#=-\n' + 'queryLength1' + ': ' + queryLength1 + '\n-=#=-\n');
        System.debug('\n\n-=#=-\n' + 'queryLength2' + ': ' + queryLength2 + '\n-=#=-\n');
    }
}