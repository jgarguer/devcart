@isTest
private class TGS_Account_Hierarchy_TEST {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture - New Energy Aborda
    Description:   Test method to manage the coverage code for TGS_Account_Hierachy.sendToFullStack()
    
    History:
    
    <Date>            <Author>              <Description>
    28/09/2017        Guillermo Muñoz       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void sendToFullStack_Insert_TEST() {
        
        BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();

        Account oHolding = new Account();
        oHolding.RecordTypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
        oHolding.Name = 'Test Account';
        oHolding.Fax = '916755532';
        oHolding.Phone = '654337899';
        oHolding.NumberOfEmployees = 467; 
        oHolding.BI_No_Identificador_fiscal__c = '654358756'; 
        oHolding.AnnualRevenue = 2897600;
        oHolding.BI_Country__c = 'Spain';  
        oHolding.BI_Tipo_de_identificador_fiscal__c = 'NIF';
        oHolding.Website = 'test@accenture.com';
        oHolding.BI_Inicio_actividad_del_cliente__c = Date.today();
        oHolding.Rating = 'Cold';
        oHolding.BI_Riesgo__c = '1 - Sin problema';
        oHolding.BI_Activo__c = 'Sí';
        oHolding.BI_Denominacion_comercial__c = 'Test';
        oHolding.TGS_Account_Mnemonic__c = 'Test';
        oHolding.TGS_Account_Category__c = 'Corporate';
        oHolding.TGS_Es_MNC__c = true;
        oHolding.TGS_Account_Leading_MNC_Unit__c = 'SPAIN';
        oHolding.SAP_ID__c = 'E34622';
        oHolding.TGS_Invoice_Language__c = false;
        oHolding.BI_Identificador_Externo__c = 'Test';
        insert oHolding;

        Account oCustCountry = new Account();
        oCustCountry.RecordTypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_CUSTOMER_COUNTRY_ACCOUNT);
        oCustCountry.Name = 'Test Account';
        oCustCountry.Fax = '916755532';
        oCustCountry.Phone = '654337899';
        oCustCountry.NumberOfEmployees = 467; 
        oCustCountry.BI_No_Identificador_fiscal__c = '654358757'; 
        oCustCountry.AnnualRevenue = 2897600;
        oCustCountry.BI_Country__c = 'Spain';  
        oCustCountry.BI_Tipo_de_identificador_fiscal__c = 'NIF';
        oCustCountry.Website = 'test@accenture.com';
        oCustCountry.BI_Inicio_actividad_del_cliente__c = Date.today();
        oCustCountry.Rating = 'Cold';
        oCustCountry.BI_Riesgo__c = '1 - Sin problema';
        oCustCountry.BI_Activo__c = 'Sí';
        oCustCountry.BI_Denominacion_comercial__c = 'Test';
        oCustCountry.TGS_Account_Mnemonic__c = 'Test';
        oCustCountry.TGS_Account_Category__c = 'Corporate';
        oCustCountry.TGS_Es_MNC__c = true;
        oCustCountry.TGS_Account_Leading_MNC_Unit__c = 'SPAIN';
        oCustCountry.SAP_ID__c = 'E34622';
        oCustCountry.TGS_Invoice_Language__c = false;
        oCustCountry.ParentId = oHolding.Id;
        oCustCountry.TGS_aux_holding__c = oHolding.Id;

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Location', 'TestResponse');
    
        //BI_MigrationHelper.cleanSkippedTriggers();

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));
        insert oCustCountry;

        //Desactivación del desarrollo
        BI_MigrationHelper.cleanSkippedTriggers();
        TGS_Account_Hierarchy.sendToFullStack(new List <Account>{oCustCountry}, null);
        //Fin desactivacion del desarrollo

        Test.stopTest();

        Account acc = [SELECT Id, BI_Identificador_Externo__c FROM Account WHERE Id =: oCustCountry.Id LIMIT 1];
        
        System.assertEquals(acc.BI_Identificador_Externo__c, 'TestResponse');
        System.assert([SELECT Id FROM BI_Log__c WHERE BI_Asunto__c = 'TGS_Account_Hierarchy.sendToFullStack'].isEmpty());
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture - New Energy Aborda
    Description:   Test method to manage the coverage code for TGS_Account_Hierachy.sendToFullStack()
    
    History:
    
    <Date>            <Author>              <Description>
    28/09/2017        Guillermo Muñoz       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void sendToFullStack_Update_TEST() {
        
        BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();

        Account oHolding = new Account();
        oHolding.RecordTypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
        oHolding.Name = 'Test Account';
        oHolding.Fax = '916755532';
        oHolding.Phone = '654337899';
        oHolding.NumberOfEmployees = 467; 
        oHolding.BI_No_Identificador_fiscal__c = '654358756'; 
        oHolding.AnnualRevenue = 2897600;
        oHolding.BI_Country__c = 'Spain';  
        oHolding.BI_Tipo_de_identificador_fiscal__c = 'NIF';
        oHolding.Website = 'test@accenture.com';
        oHolding.BI_Inicio_actividad_del_cliente__c = Date.today();
        oHolding.Rating = 'Cold';
        oHolding.BI_Riesgo__c = '1 - Sin problema';
        oHolding.BI_Activo__c = 'Sí';
        oHolding.BI_Denominacion_comercial__c = 'Test';
        oHolding.TGS_Account_Mnemonic__c = 'Test';
        oHolding.TGS_Account_Category__c = 'Corporate';
        oHolding.TGS_Es_MNC__c = true;
        oHolding.TGS_Account_Leading_MNC_Unit__c = 'SPAIN';
        oHolding.SAP_ID__c = 'E34622';
        oHolding.TGS_Invoice_Language__c = false;
        oHolding.BI_Identificador_Externo__c = 'Test';
        insert oHolding;

        Account oCustCountry = new Account();
        oCustCountry.RecordTypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_CUSTOMER_COUNTRY_ACCOUNT);
        oCustCountry.Name = 'Test Account';
        oCustCountry.Fax = '916755532';
        oCustCountry.Phone = '654337899';
        oCustCountry.NumberOfEmployees = 467; 
        oCustCountry.BI_No_Identificador_fiscal__c = '654358757'; 
        oCustCountry.AnnualRevenue = 2897600;
        oCustCountry.BI_Country__c = 'Spain';  
        oCustCountry.BI_Tipo_de_identificador_fiscal__c = 'NIF';
        oCustCountry.Website = 'test@accenture.com';
        oCustCountry.BI_Inicio_actividad_del_cliente__c = Date.today();
        oCustCountry.Rating = 'Cold';
        oCustCountry.BI_Riesgo__c = '1 - Sin problema';
        oCustCountry.BI_Activo__c = 'Sí';
        oCustCountry.BI_Denominacion_comercial__c = 'Test';
        oCustCountry.TGS_Account_Mnemonic__c = 'Test';
        oCustCountry.TGS_Account_Category__c = 'Corporate';
        oCustCountry.TGS_Es_MNC__c = true;
        oCustCountry.TGS_Account_Leading_MNC_Unit__c = 'SPAIN';
        oCustCountry.SAP_ID__c = 'E34622';
        oCustCountry.TGS_Invoice_Language__c = false;
        insert oCustCountry;

        Map<String, String> responseHeaders = new Map<String, String>();
        responseHeaders.put('Location', 'TestResponse');
    
        //BI_MigrationHelper.cleanSkippedTriggers();

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

        Map <Id, Account> oldMap = new Map <Id, Account>{oCustCountry.Id => oCustCountry.clone(true, true, false, false)};

        oCustCountry.ParentId = oHolding.Id;
        oCustCountry.TGS_aux_holding__c = oHolding.Id;
        update oCustCountry;

        //Desactivación del desarrollo
        BI_MigrationHelper.cleanSkippedTriggers();
        TGS_Account_Hierarchy.sendToFullStack(new List <Account>{oCustCountry}, oldMap);
        //Fin desactivación del desarrollo

        Test.stopTest();

        Account acc = [SELECT Id, BI_Identificador_Externo__c FROM Account WHERE Id =: oCustCountry.Id LIMIT 1];
        
        System.assertEquals(acc.BI_Identificador_Externo__c, 'TestResponse');
        System.assert([SELECT Id FROM BI_Log__c WHERE BI_Asunto__c = 'TGS_Account_Hierarchy.sendToFullStack'].isEmpty());
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián
    Company:       Accenture - New Energy Aborda
    Description:   Test method to manage the coverage code for TGS_Account_Hierachy.nullHierachy()
    
    History:
    
    <Date>            <Author>              <Description>
    20/10/2017        Gawron, Julián        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void nullHierarchy_TEST() {

        BI_TestUtils.throw_exception = false; //26/10/2017 JEG

        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){
    
           userTGS.TGS_Is_BI_EN__c = true;
           userTGS.TGS_Is_TGS__c = false;
    
           if(userTGS.Id != null){
               update userTGS;
           }
           else{
               insert userTGS;
           }
        }


        Account acc = TGS_Dummy_Test_Data.dummyHierarchy();

        System.debug(LoggingLevel.ERROR, 'Cuenta0 ====> '+acc.TGS_Aux_Holding__c);

        Test.startTest();
            acc.ParentId = null;
            update acc;
        Test.stopTest();

        Account acc2 = [SELECT Id, TGS_Aux_Holding__c FROM Account WHERE Id = :acc.Id];

        System.assertNotEquals(acc.TGS_Aux_Holding__c, acc2.TGS_Aux_Holding__c);
        System.assertEquals(null, acc2.TGS_Aux_Holding__c);

    }

    static testMethod void exceptionsTest() {
        BI_TestUtils.throw_exception = true;
        
        Test.startTest();
            TGS_Account_Hierarchy.nullHierarchy(null, null);
        Test.stopTest();
  }

}