/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis 
    Company:       Everis
    Description:   Test Methods executed to test CWP_ProfileHelper.cls 
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    14/03/2016              Everis                   Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class CWP_ProfileHelper_TEST {
     
    
    @isTest static void checkAllowed_PermissionSet_TEST () {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/

          //Simula Usuario del portal
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
            BI_Permisos_PP__c permiso = new BI_Permisos_PP__c();
            permiso.BI_Country__c = accList[0].BI_Country__c;
            permiso.BI_Segment__c = accList[0].BI_Segment__c;
            insert permiso;
            BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=user1.Id, BI_User__c=user1.Id, BI_Cliente__c=accList[0].Id, BI_Permisos_PP__c=permiso.Id);
            insert ccp;
            
            system.runAs(user1){
                //END Simula Usuario del portal
                BI_TestUtils.throw_exception = false;
                System.debug('<*-*-*-*-*>');
                system.assertEquals(CWP_ProfileHelper.checkPermissionSet('pca_HOME'),true);
                System.debug('</*-*-*-*-*>');
            }
        }
    }
    
    
    @isTest static void checkDenied_PermissionSet_TEST () {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/

          //Simula Usuario del portal
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());  
            BI_Permisos_PP__c permiso = new BI_Permisos_PP__c();
            permiso.BI_Country__c = accList[0].BI_Country__c;
            permiso.BI_Segment__c = accList[0].BI_Segment__c;
            insert permiso;
            BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(OwnerId=user1.Id, BI_User__c=user1.Id, BI_Cliente__c=accList[0].Id, BI_Permisos_PP__c=permiso.Id);
            insert ccp;
            
            system.runAs(user1){
                //END Simula Usuario del portal
                BI_TestUtils.throw_exception = false;
                System.debug('<-+-+-+-+-+>');
                system.assertEquals(CWP_ProfileHelper.checkPermissionSet('pageDenied'),false);
                System.debug('</-+-+-+-+-+>');
            }
        }
    }
    
    @isTest static void check_PermissionSet_catch_TEST() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_TestUtils.throw_exception = true;
        CWP_ProfileHelper.checkPermissionSet(null);
    }
    

    @isTest static void getPermissionSet_TEST() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
    
          //Simula Usuario del portal
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
            system.debug('**********');
            system.runAs(user1){
                //END Simula Usuario del portal
                BI_TestUtils.throw_exception = false;
                system.debug('++++++++++++');
                system.assertEquals(CWP_ProfileHelper.getPermissionSet().isEmpty(), false);
                system.debug('------------');
            }
        }
    }

    @isTest static void getPermissionSet_catch_TEST() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        BI_TestUtils.throw_exception = true;
        CWP_ProfileHelper.getPermissionSet();
    }   
    
  }