public with sharing class PCA_Report_PopUpDetail extends PCA_HomeController{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Moruno
        Company:       Salesforce.com
        Description:   Class to get report details 
        
        History:
        
        <Date>            <Author>              <Description>
        14/05/2015        Antonio Moruno       Initial version
        
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    
     public String reportId {get;set;}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Moruno
        Company:       Salesforce.com
        Description:   Check permissions of report detail page
        
        History:
        
        <Date>            <Author>              <Description>
        14/05/2015        Antonio Moruno       Initial version
        
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_Report_PopUpDetail.checkPermissions', 'Portal Platino',Exc, 'Class');
           return null;
        }
    }    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Constructor 
    
    History:
    
    <Date>            <Author>              <Description>
    14/05/2015        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public PCA_Report_PopUpDetail(){}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   load info of Report Detail page
    
    History:
    
    <Date>            <Author>              <Description>
    14/05/2015        Antonio Moruno       Initial version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public void loadInfo (){
    try{
        if(BI_TestUtils.isRunningTest()){
            throw new BI_Exception('test');
        }
        reportId = (apexpages.currentpage().getparameters().get('id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('id')):null;
     }catch(Exception Exc){
        BI_LogHelper.generate_BILog('PCA_Report_PopUpDetail.loadInfo', 'Portal Platino', Exc, 'Class');
    }
    }
}