public  class ADQ_Facturacion {
	public Factura__c factura{get; set;}
	public Programacion__c programacion{get;set;}
	public String strSubtotalAnexos{get;set;}
	public String strIVAAnexos{get;set;}
	public String strTotalAnexos{get;set;}
	public String strIEPSAnexos{get;set;}
	public String strTotalIEPSAnexos{get;set;}	
	
	public String strTotalIEPS{get;set;}
	public String strTotalConIEPS{get;set;}
	public String strSubtotalOLI{get;set;}
	public String strIVAOLI{get;set;}
	public String strTotalOLI{get;set;}
	
	private double factorIva;
	private Double total = 0;
	
	public List<AnexoWrapper> listaAnexos{get;set;}
	
	private List<Pedido_PE__c> listaPedidosPE;
	private ADQ_DivisaUtility divisaUtility;
	private List<EquipoWrapper> listaWrapperProductos;
	private Map<String, Double> mapProductoTotal;
	private Map<String, Double> mapProductoTotalIEPS;
	/*
	private Map<String, List<Consumo__c>> mapConsumosPorPedido;
	private Map<String, List<Consumo__c>> mapConsumosPorEquipo;
	*/
	// clas divisa *******************************************
	public CurrencyTypeFacturacion__c corporatecurrency;
	public Map<String, CurrencyTypeFacturacion__c> conversionrates = new Map<String, CurrencyTypeFacturacion__c>();
	public List<CurrencyTypeFacturacion__c> systemcurrencies;
	
	
	
	public ADQ_Facturacion(){
		divisaUtility = new ADQ_DivisaUtility();
		String idProgramacion = ApexPages.currentPage().getParameters().get('pid');
/*		
		mapConsumosPorPedido = new Map<String, List<Consumo__c>>();
		mapConsumosPorEquipo = new Map<String, List<Consumo__c>>();
*/		
		if(idProgramacion != null && idProgramacion != ''){
			try{
				programacion = [SELECT Id, CurrencyIsoCode, Factura__c, Fecha__c, Importe__c, IVA__c, Name, 
								Total__c, factor_IVA__c, Inicio_del_per_odo__c, Fin_del_per_odo__c, Folio_fiscal__c 
								FROM Programacion__c 
								WHERE Id =: idProgramacion LIMIT 1];
				
				factura = [SELECT Tipo_de_Factura__c, Requiere_Anexo__c, Name, Migrado__c, 
							Inicio_de_Facturaci_n__c, Id,IVA__c, Fecha_Inicio__c, Fecha_Fin__c, Descripcion__c, 
							CurrencyIsoCode, Contrato__c, Condiciones_de_Pago__c, Activo__c,
								//datos del cliente (Account)
							Cliente__c, Cliente__r.Owner.City, Cliente__r.Owner.State, 
							Cliente__r.Name, Cliente__r.BI_No_Identificador_fiscal__c, 
							Cliente__r.BillingStreet, Cliente__r.BillingCity,  Cliente__r.BillingState, Cliente__r.BillingPostalCode, Cliente__r.BillingCountry
						FROM Factura__c 
						WHERE Id = :programacion.Factura__c LIMIT 1];
						////31/05/2017        Cristina Rodriguez Commented Motivo_del_Pedido__c (Factura__C) from SELECT
				
				
				listaPedidosPE = [SELECT Id, CurrencyIsoCode, Precio_original__c, Id_producto__c, Precio_convertido__c, 
									Moneda_de_conversi_n__c,  Concepto__c, Nombre_del_producto__c, 
									Producto_en_Sitio__c, Producto_en_Sitio__r.Name, 
									Lleva_IEPS__c, IEPS__c ,
									Producto_en_Sitio__r.Installation_point__r.BI_Sede__r.Name 
									FROM Pedido_PE__c 
									WHERE Programacion__c =: programacion.Id];
				
/*				
				for(Consumo__c co : [SELECT Id, Pedido_PE__c, Tarifa__c, Trafico_usado__c, Trafico_ajustado__c, CurrencyIsoCode, 
										Consumo_neto__c, Dia_de_consumo__c, Equipo_en_Sitio__c 
										FROM Consumo__c 
										where Pedido_PE__c IN: listaPedidosPE order by Dia_de_consumo__c asc]){
					//Los agrupamos en un mapa por pedido
					if(!mapConsumosPorPedido.containsKey(co.Pedido_PE__c)){
						mapConsumosPorPedido.put(co.Pedido_PE__c, new List<Consumo__c>());
					}
					mapConsumosPorPedido.get(co.Pedido_PE__c).add(co);
					
					if(!mapConsumosPorEquipo.containsKey(co.Equipo_en_Sitio__c)){
						mapConsumosPorEquipo.put(co.Equipo_en_Sitio__c, new List<Consumo__c>());
					}
					mapConsumosPorEquipo.get(co.Equipo_en_Sitio__c).add(co);
				}
*/				
				
				factorIva = (programacion.factor_IVA__c!=null)?programacion.factor_IVA__c:0;
				
				
				CurrencyTypeFacturacion__c corporatecurrency = [Select IsoCode__c, IsCorporate__c, Id, ConversionRate__c From CurrencyTypeFacturacion__c Where IsCorporate__c = true and IsActive__c = true limit 1];
				this.corporatecurrency =  corporatecurrency;
				
				//Traemos todas las monedas disponibles en el sistema y su conversionrate
				systemcurrencies = new List<CurrencyTypeFacturacion__c>([Select IsoCode__c, IsCorporate__c, Id, ConversionRate__c from CurrencyTypeFacturacion__c where IsActive__c = true]);
				
				//Corremos por la lista de monedas para ordenarlas en el mapa
				for(CurrencyTypeFacturacion__c c : systemcurrencies){
					conversionrates.put(c.IsoCode__c,c);
				}
				
				
			}
			catch(Exception e){
				factura = null;
			}
		}
		else factura = null;
	}
	
	public void generaAnexos(){
		listaAnexos = new List<AnexoWrapper>();
		//mapWrapperAnexos = new Map<String, List<AnexoWrapper>>();
		mapProductoTotal = new Map<String, Double>();
		mapProductoTotalIEPS = new Map<String, Double>();
		
		String nombreSitio = '';
		Double importe = 0;
		Double ieps = 0;
		
		Double iepsTotal = 0;
		Double subtotalAnexos = 0;
		Double ivaAnexos = 0;
		Double totalAnexos = 0;
		
		Map<String, List<EquipoWrapper>> mapEquipoWrapper = new Map<String, List<EquipoWrapper>>();
		for(Pedido_PE__c pedido : listaPedidosPE){
			//nombreSitio = pedido.Producto_en_Sitio__r.Sitio_Id__r.Name + ' - ' + pedido.Producto_en_Sitio__r.Sitio_Id__r.Nombre_del_sitio__c;
			//nombreSitio = 'Pruebas JMO';
			nombreSitio = pedido.Producto_en_Sitio__r.Installation_point__r.BI_Sede__r.Name;
			//String idServicio = (pedido.Producto_en_Sitio__r.Id_de_servicio__c != null)?'('+pedido.Producto_en_Sitio__r.Id_de_servicio__c+') - ':'';
			String idServicio ='';
			
			if(!mapEquipoWrapper.containsKey(nombreSitio)){
				List<EquipoWrapper> listaEquipos = new List<EquipoWrapper>();
				mapEquipoWrapper.put(nombreSitio, listaEquipos);
			}
			if(pedido.CurrencyIsoCode == pedido.Moneda_de_conversi_n__c){
				importe = pedido.Precio_original__c;
				ieps = (pedido.IEPS__c != null)?pedido.IEPS__c:0;
			}
			else{
				importe = pedido.Precio_convertido__c;
				ieps = (pedido.IEPS__c != null)?pedido.IEPS__c:0;
			}
			subtotalAnexos += importe;
			iepsTotal += ieps;
			
			mapEquipoWrapper.get(nombreSitio).add(new EquipoWrapper(idServicio + pedido.Producto_en_Sitio__r.Name + ' - ' + pedido.Nombre_del_producto__c, importe, importe, ieps, ieps, conversionrates, corporatecurrency));
			//sumamos el monto de todas las apariciones de un mismo equipo para tener el total por factura
			importe += (mapProductoTotal.containsKey(pedido.Id_producto__c))?mapProductoTotal.get(pedido.Id_producto__c):0;
			mapProductoTotal.put(pedido.Id_producto__c, importe);

			system.debug('>>>mapProductoTotal: ' + mapProductoTotal );
			
			ieps += (mapProductoTotalIEPS.containsKey(pedido.Id_producto__c))?mapProductoTotalIEPS.get(pedido.Id_producto__c):0;
			mapProductoTotalIEPS.put(pedido.Id_producto__c, ieps);
		}
		
		//Agrupo los EquipoWrapper en uno solo en caso de que exista más de uno con el mismo nombreProducto
		//Esto pasa cuando se genera el anexo de una factura agrupada
		for(String ns : mapEquipoWrapper.keySet()){
			Map<String, EquipoWrapper> mapEW = new map<String, EquipoWrapper>();
			for(EquipoWrapper ew : mapEquipoWrapper.get(ns)){
				if(!mapEW.containsKey(ew.nombreProducto)){
					mapEw.put(ew.nombreProducto, ew);
				} 
				else{
					Double importeAgrupado = ew.importe + mapEw.get(ew.nombreProducto).importe;
					Double total = importeAgrupado;
					
					Double iepsAgrupado = ew.ieps + mapEw.get(ew.nombreProducto).ieps;
					Double totalIEPS = iepsAgrupado;
					/*
					List<Consumo__c> lc = new List<Consumo__c>();
					if (mapEw.get(ew.nombreProducto).listaConsumo != null){
						lc.addAll(mapEw.get(ew.nombreProducto).listaConsumo);
						lc.addAll(ew.listaConsumo);
					}
					mapEw.put(ew.nombreProducto, new EquipoWrapper(ew.nombreProducto, total, importeAgrupado, totalIEPS, iepsAgrupado, lc, conversionrates,corporatecurrency));
					*/
				}
			}
			listaAnexos.add(new AnexoWrapper(ns, mapEW.values()));
		}
		
		/*for(String ns : mapEquipoWrapper.keySet()){
			listaAnexos.add(new AnexoWrapper(ns, mapEquipoWrapper.get(ns)));
		}*/
		
		ivaAnexos = divisaUtility.round((subtotalAnexos + iepsTotal)* factorIva, 2);
		totalAnexos = subtotalAnexos + iepsTotal + ivaAnexos;
		
		strSubtotalAnexos = divisaUtility.formatCurrency(subtotalAnexos);
		strIEPSAnexos = divisaUtility.formatCurrency(iepsTotal);
		strTotalIEPSAnexos = divisaUtility.formatCurrency(subtotalAnexos + iepsTotal);
		strIVAAnexos = divisaUtility.formatCurrency(ivaAnexos);
		strTotalAnexos = divisaUtility.formatCurrency(totalAnexos);
	}
	
	public List<EquipoWrapper> getListaWrapperProductos(){
		listaWrapperProductos = new List<EquipoWrapper>();
		
		Double subtotalOLI = 0;
		Double subTotalConIEPS = 0;
		Double iepsOLI = 0;
		Double ivaOLI = 0;
		Double totalOLI = 0;
		
		//try{
			generaAnexos();
			Map<ID,NE__Product__c> mapProductos2 = new Map<ID,NE__Product__c>([Select Id, Name From NE__Product__c where Id IN: mapProductoTotal.keySet()]); 
			system.debug('>>> mapProductos2: '+ mapProductos2);
			system.debug('>>> mapProductoTotal: '+ mapProductoTotal);
			if (mapProductoTotal != null){
				if (mapProductos2 != null) {
					for(String product2Id : mapProductoTotal.keySet()){
						String productName = '';
						if (mapProductos2.get(product2Id) != null){
							productName = mapProductos2.get(product2Id).Name;	
						}
						
						//productName = (productName != null)?productName:'';
						Double total = mapProductoTotal.get(product2Id);
						Double ieps = mapProductoTotalIEPS.get(product2Id);
						listaWrapperProductos.add(new EquipoWrapper(productName, divisaUtility.round(total, 2), total, divisaUtility.round(ieps, 2), ieps, conversionrates, corporatecurrency));
						subtotalOLI += total;
						iepsOLI += ieps;
						
					}
				}
			}
			
			ivaOLI = divisaUtility.round((subtotalOLI + iepsOLI) * factorIva, 2);
			subTotalConIEPS = divisaUtility.round(subtotalOLI + iepsOLI, 2);
			
			total = totalOLI = subTotalConIEPS + ivaOLI;
			
			strSubtotalOLI = divisaUtility.formatCurrency(subtotalOLI);
			strTotalIEPS = divisaUtility.formatCurrency(iepsOLI);
			strTotalConIEPS = divisaUtility.formatCurrency(subTotalConIEPS);
			strIVAOLI = divisaUtility.formatCurrency(ivaOLI);
			strTotalOLI = divisaUtility.formatCurrency(totalOLI);
			
		/*
		}
		catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Error: ' + e.getMessage()));
		}
		//*/
		
		return listaWrapperProductos;										
	}
	
	public String getTotalConLetra(){
		String moneda;
		if(factura.CurrencyIsoCode == 'USD'){
			moneda = 'DOLARES';
		}else if(factura.CurrencyIsoCode == 'MXN'){
			moneda = 'PESOS';
		}else if(factura.CurrencyIsoCode == 'EUR'){
			moneda = 'EUROS';
		}else{
			moneda = factura.CurrencyIsoCode;
		}
		ADQ_NumberToText ntt = new ADQ_NumberToText();
		return ((ntt.convertirLetras(total)).toUpperCase()).replace('MONEDA&',moneda);
	}
	
	
	public String getHoy(){
		return Date.today().format();
	}
	
	public String getPeriodo(){
		String periodo = '';
		if(programacion.Inicio_del_per_odo__c!=null){
			periodo += programacion.Inicio_del_per_odo__c.format() + ' al ' + programacion.Fin_del_per_odo__c.format();
		}
		return periodo;
	}
	
	public class EquipoWrapper{
		public String nombreProducto {get; set;}
		public String total {get; set;}
		public Double importe{get;set;}
		public String iepsFormateado{get;set;}
		public Double ieps{get;set;}
		public String importeConIEPS{get;set;}
		//private DivisaUtility divisaUtility = new DivisaUtility();
		//public List<Consumo__c> listaConsumo{get;set;}
		public Boolean tieneConsumos {get;set;} 
		public Map<string,CurrencyTypeFacturacion__c> conversionrates;
		public CurrencyTypeFacturacion__c corporatecurrency;
			
		public EquipoWrapper(String nombreProducto, Double total, Double importe, Double iepsFormateado, Double ieps,  Map<string,CurrencyTypeFacturacion__c> conversionrates, CurrencyTypeFacturacion__c corporatecurrency){
			this.nombreProducto = nombreProducto;
			this.total = formatCurrency(total);
			this.importe = importe;
			this.iepsFormateado = formatCurrency(iepsFormateado);
			this.importeConIEPS = formatCurrency(total+iepsFormateado);
			this.ieps = ieps;
//			this.tieneConsumos = (listaConsumos1 != null);
			//this.listaConsumo = listaConsumos1;
			this.conversionrates = conversionrates;
			this.corporatecurrency = corporatecurrency;
		}
/*		
		
		public Double transformCurrency(String original,String targetdiv,Double value){
			Double result;
			
			if(original == targetdiv){
				result = value;
			}else if(original == this.corporatecurrency.IsoCOde__c){
				//CurrencyType targetcurrency = [select ConversionRate from CurrencyType where IsoCode = :targetdiv and IsActive = true limit 1];
				CurrencyTypeFacturacion__c targetcurrency = conversionrates.get(targetdiv);
				result = value * targetcurrency.ConversionRate__c;
			}else if(targetdiv == this.corporatecurrency.IsoCOde__c){
				//CurrencyType originalcurrency = [select ConversionRate from CurrencyType where IsoCode = :original and IsActive = true limit 1];
				CurrencyTypeFacturacion__c originalcurrency = conversionrates.get(original);
				result = value / originalcurrency.ConversionRate__c;
			}
			else {
				Double d = transformCurrency(original, this.corporatecurrency.IsoCode__c,value);
				result = transformCurrency(this.corporatecurrency.IsoCode__c, targetdiv, d);
			}
			
			return result;
		}
		

	*/
		public Double round(Double qnum, Double qdecimal){
			//qnum = this.truncate(qnum, 3);
			Double d = Math.pow(10, Math.roundToLong(qdecimal));
			return Math.roundToLong(qnum * d) / d;
		}
		
		public String formatCurrency(Double amount){
			String r;
			if(amount == null) { r = '0.00'; }
			else{
				String s = '' + amount;
				if(s.indexOf('.') < 0) { s += '.00'; }
				if(s.indexOf('.') == (s.length() - 2)) { s += '0'; }
				//String[] a = s.split('5', 2);
				
				String d = s.substring(s.indexOf('.'),s.indexOf('.') + 3);
				String n = s.substring(0,s.indexOf('.'));
				
				String cadenaNumero = '';
			
				while(n.length() > 3) {
					String block = n.substring(n.length()-3);
					cadenaNumero = ',' + block + cadenaNumero;
					n = n.substring(0,n.length()-3);
				}
				cadenaNumero = n + cadenaNumero + d;
				r = cadenaNumero;
			}
			return r;
		}
		
		public Double truncate(Double num, Integer numDecimals){
			String strNum = string.valueOf(num);
			
			if(strNum.indexOf('.', 0) > -1){
				String intVal = strNum.substring(0, strNum.indexOf('.', 0));
				String decVal = strNum.substring(strNum.indexOf('.', 0) + 1);
				
				if(decVal.length() > numDecimals){
					strNum = intVal + decVal.substring(0, numDecimals);
				}
			}
			return Double.valueOf(strNum);
		}
	}
	
	public class AnexoWrapper{
		public String nombreSitio{get;set;}
		public List<EquipoWrapper> sens{get;set;}
		
		public AnexoWrapper(String nombreSitio, List<EquipoWrapper> sens){
			this.nombreSitio = nombreSitio;
			this.sens = sens;
		}
	}
	
	//*/
}