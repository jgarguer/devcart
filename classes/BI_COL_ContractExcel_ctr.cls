public class BI_COL_ContractExcel_ctr 
{
    /**Atributos generales de la plantilla */
    public BI_COL_Anexos__c fun {get;set;}
    public String numContrato {get;set;}

    public String razonSocial {get;set;}
    public String nit {get;set;}
    public String direccion {get;set;}
    public String telefono {get;set;}
    public String email {get;set;}
    public List<BI_COL_Modificacion_de_Servicio__c> lstMS {get;set;}
    public Decimal totalInstalacion {get;set;}
    public Decimal totalServicio {get;set;}
    public Decimal totalOrden {get;set;}
    public String tipoMoneda {get;set;}
    public Integer duracionContrato {get;set;}
    public String asesorVtas {get;set;} 
    public Integer cantidadColumnas{get;set;}
    public boolean logoMovistar {get;set;}
    public boolean logoTelefonica {get;set;}
    public String tipoOrden{get;set;}
    private Address addr {get;set;}
    public String direcc {get;set;}
    
    private set<string> anexosValidos=new set<string>();
    private set<string> getAnexos=new set<string>();
    
    public List<WrapperLst> lstGlobal
    {
        get{
            boolean flag=true;
            integer cantInicial=10;
            WrapperLst wrapper=new WrapperLst(); 
            List<WrapperLst> lstGlobal=new List<WrapperLst> ();
            
            List<BI_COL_Modificacion_de_Servicio__c> lst = new List<BI_COL_Modificacion_de_Servicio__c>();
            System.debug('\n\n cantInicial= '+cantInicial+' lstMS.size()'+lstMS.size()+'\n\n');
            if( cantInicial > lstMS.size() )
            {
                cantInicial = lstMS.size();
                if( cantidadColumnas > 11 && cantInicial > 5 )
                    cantInicial=cantInicial-2;      
            }
            /*** Cargando los primeros 8 registros *****/
            for(Integer i=0;i<cantInicial;i++)
            {
                lst.add(lstMS.get(i));
            }           
            wrapper.lista = lst;
            lstGlobal.add( wrapper );
            lst = new List<BI_COL_Modificacion_de_Servicio__c>();
            /********************************************/
            /* cargando los demás registros*/
            for( Integer i = cantInicial; i < lstMS.size(); i++ )
            {
                lst.add( lstMS.get(i) );
                if( math.mod( i - ( cantInicial - 1 ), 16 ) == 0 )
                {
                    wrapper=new WrapperLst();
                    wrapper.lista=lst;
                    lstGlobal.add(wrapper);
                    lst = new List<BI_COL_Modificacion_de_Servicio__c>();
                }
            }
            if( lst.size() > 0 )
            {
                wrapper = new WrapperLst();
                wrapper.lista = lst;
                lstGlobal.add( wrapper );
            }
            /******************************************************/
            /***** validando que el ultimo listado sea menor que nueve registros ****/
            if( lstGlobal.get(lstGlobal.size()-1).lista.size() >= 9 )
            {
                List<BI_COL_Modificacion_de_Servicio__c> lstSplit = lstGlobal.get(lstGlobal.size()-1).lista;
                List<BI_COL_Modificacion_de_Servicio__c> lstNueva = new List<BI_COL_Modificacion_de_Servicio__c>();
                for( Integer i=0; i < 9; i++ )
                {
                    lstNueva.add( lstSplit.get(i) );
                }
                lstGlobal.get(lstGlobal.size()-1).lista = lstNueva;
                lstNueva = new List<BI_COL_Modificacion_de_Servicio__c>();
                for( Integer i = 9; i < lstSplit.size(); i++ )
                {
                    lstNueva.add( lstSplit.get(i) );
                }
                wrapper = new WrapperLst();
                wrapper.lista = lstNueva;
                lstGlobal.add( wrapper );
            }
            /******************************************************/
            for(Integer i=0;i<lstGlobal.size();i++)
            {
                lstGlobal.get(i).salto = 'salto';
                lstGlobal.get(i).visible = '';
                if( i == (lstGlobal.size()- 1 ) )
                {
                    lstGlobal.get(i).salto='';
                    lstGlobal.get(i).visible='invisible';
                }
            }
            return lstGlobal;
        }
        set;
    }
    
    public String anexoServicios
    {
        get
        {
            String cadena='';
            if(ApexPages.currentPage().getParameters().get('anexoServicios')!=null)
            {
                cadena = ApexPages.currentPage().getParameters().get('anexoServicios').replace('[','');
                cadena = cadena.replace(']','');
                if( cadena.contains('N/A') )
                    return 'N/A';
                return cadena;
            }
            return '';
        }
        set;
    }
    
    public String numOrdenServicio 
    {
        get
        {
            String numero=''+fun.Name;
            if( fun.Name != null )
            {
                numero=fun.Name.subString(fun.Name.lastIndexOf('-')+1,fun.Name.length());
            }
            return numero;
        }
        set;
    }
    /** Variables para definir las colunmas que aparecen en el documento FUN ***/
    public String CFM
    {
        get{
            if(ApexPages.currentPage().getParameters().get('CFM')!=null)
            {
                if( Boolean.valueOf( ApexPages.currentPage().getParameters().get('CFM') ) )
                    return '';
            }
            return 'display: none;';
        }
        set;
    }

    public String CNX
    {
        get{
            if( ApexPages.currentPage().getParameters().get('CNX') != null )
            {
                if( Boolean.valueOf( ApexPages.currentPage().getParameters().get( 'CNX' ) ) )
                    return '';
            }
            return 'display: none;';
        }set;
    }
    public String nombreSucursal
    {
        get{
            if( ApexPages.currentPage().getParameters().get('nombreSucursal') != null )
            {
                if(Boolean.valueOf(ApexPages.currentPage().getParameters().get('nombreSucursal') ) )
                 return '';
            }
            return 'display: none;';
        }
        set;
    }
    public String ciudadDestino
    {
        get{
            if( ApexPages.currentPage().getParameters().get('ciudadDestino') != null )
            {
                if( Boolean.valueOf(ApexPages.currentPage().getParameters().get('ciudadDestino') ) )
                    return '';
            }
            return 'display: none;';
        }
        set;
    }
    public String bwPorExceso
    {
        get{
            if( ApexPages.currentPage().getParameters().get('bwPorExceso') != null )
            {
                if( Boolean.valueOf(ApexPages.currentPage().getParameters().get('bwPorExceso') ) )
                    return '';
            }
            return 'display: none;';
        }
        set;
    }
    public String costoBwPorExceso
    {
        get{
            if( ApexPages.currentPage().getParameters().get('costoBwPorExceso') != null )
            {
                if( Boolean.valueOf(ApexPages.currentPage().getParameters().get('costoBwPorExceso') ) )
                    return '';
            }
            return 'display: none;';
        }
        set;
    }
    
    public String demo
    {
        get{
            if( ApexPages.currentPage().getParameters().get('demo') != null )
            {
                demo = ApexPages.currentPage().getParameters().get('demo');
                return demo;
            }
            return 'No';
        }
        set;
    }
    /***************************************************************************/    
    //Constructor por defecto
    public BI_COL_ContractExcel_ctr()
    {
        try
        {
            cantidadColumnas=11;
            String idFUN = ApexPages.currentPage().getParameters().get('idFUN');
            tipoOrden = ApexPages.currentPage().getParameters().get('tipoOrden');
            if( CFM != null && CFM.equals('') )
                cantidadColumnas++;
            if( CNX != null && CNX.equals('') )
                cantidadColumnas++;
            if( nombreSucursal != null && nombreSucursal.equals('') )
                cantidadColumnas++;
            if( ciudadDestino != null && ciudadDestino.equals('') )
                cantidadColumnas++;
            if( bwPorExceso != null && bwPorExceso.equals('') )
                cantidadColumnas++;
            if( costoBwPorExceso != null && costoBwPorExceso.equals('') )
                cantidadColumnas++; 
            System.debug('\n\n idFUN: '+idFUN+'---'+idFUN.indexOf('?')+'\n\n');
            //idFUN=idFUN.substring(0,idFUN.indexOf('?'));
            idFUN = idFUN.indexOf('?') < 2 ? idFUN : idFUN.substring(0,idFUN.indexOf('?'));
            List<BI_COL_Anexos__c> lstFUN =new list<BI_COL_Anexos__c>(
                [Select f.BI_COL_Contrato__c,f.BI_COL_Contrato__r.ContractNumber, f.BI_COL_Contrato__r.Account.Id,
                        f.BI_COL_Contrato__r.Account.BI_No_Identificador_fiscal__c,
                        f.BI_COL_Contrato__r.Account.NE__E_mail__c, f.BI_COL_Contrato__r.Account.Name, 
                     //   f.BI_COL_Contrato__r.Account.BillingAddress, 
                        f.BI_COL_Contrato__r.Account.Phone, f.Id, f.Name,f.BI_COL_Contrato__r.ContractTerm,
                        f.BI_COL_Contrato__r.BI_COL_Numero_de_documento__c,
                        f.BI_COL_Contrato__r.Account.Owner.Name, f.BI_COL_Contrato__r.Account.BI_COL_Segmento_Telefonica__c
                From    BI_COL_Anexos__c f
                Where   f.Id = :idFUN
                Limit 1]);                     

            list<Account> lstac= new list<Account> ([SELECT Name, BillingStreet FROM Account where id =:lstFUN[0].BI_COL_Contrato__r.Account.Id]);
                 for(Account a:lstac)
                    {
                       //Address addr = (Address) a.BillingAddress;
                       direcc=(a.BillingStreet==null)?'':a.BillingStreet;
                       System.debug('\n\n direcc=====>>>>>'+direcc+'\n');                          
                    }
            if( lstFUN.size() > 0  && lstac.size() > 0)
            {
                //anexosValidos=validarAnexos(lstFUN[0]);
                this.fun = lstFUN.get(0);
                this.numContrato = this.fun.BI_COL_Contrato__r.BI_COL_Numero_de_documento__c!=null?this.fun.BI_COL_Contrato__r.BI_COL_Numero_de_documento__c:this.fun.BI_COL_Contrato__r.ContractNumber;
                this.razonSocial = this.fun.BI_COL_Contrato__r.Account.Name;
                this.nit = this.fun.BI_COL_Contrato__r.Account.BI_No_Identificador_fiscal__c ;
                System.debug('\n direccion=======>>>>'+direccion);
                this.direccion = direcc;
                System.debug('\n direccion22=======>>>>'+direccion);
                //this.direccion = String.valueOf(this.fun.BI_COL_Contrato__r.Account.BillingAddress);
                this.telefono = this.fun.BI_COL_Contrato__r.Account.Phone;
                this.email = this.fun.BI_COL_Contrato__r.Account.NE__E_mail__c;
                this.asesorVtas = this.fun.BI_COL_Contrato__r.Account.Owner.Name;
                //'/resource/1341004721000/LogoTelefonicaSmall'
                this.logoMovistar = true;
                this.logoTelefonica = false;
                /*
                if( fun.BI_COL_Contrato__r.Account.BI_COL_Segmento_Telefonica__c.equals('MAYORISTAS') )
                {
                    this.logoMovistar = true;
                    this.logoTelefonica = false;
                }
                else
                {
                    this.logoMovistar = true;
                    this.logoTelefonica = false;
                }
                */
                //Cargar lista de MS
                this.lstMS = getMSSolicitudServicio(this.fun.Id);
                //Obtener el tipo de moneda del contrato
                this.tipoMoneda = (this.lstMS!=null&&this.lstMS.size()>0) ? this.lstMS.get(0).BI_COL_Tipo_de_Moneda__c : '';
                //asignar duración del contrato
                this.duracionContrato = this.fun.BI_COL_Contrato__r.ContractTerm;
                //Calcular totales
                calcularTotales( lstMS );
            }
        }
        Catch( system.exception ex )
        {
            ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.ERROR,ex.getMessage() ) );
        }
    }
    /**
    * Obtiene el listado de MS asociadas a la interfaz FUN
    *
    * @param idFUN Id del FUN asociado a la MS
    * @return  Lista de MS
    */
    public List<BI_COL_Modificacion_de_Servicio__c> getMSSolicitudServicio(String idFUN)
    {
        List<BI_COL_Modificacion_de_Servicio__c> lstMS = new List<BI_COL_Modificacion_de_Servicio__c>();    
    
        lstMS = [
            Select m.Id, m.Name, m.BI_COL_Oportunidad__c, m.BI_COL_Sucursal_Origen__r.BI_Sede__r.BI_COL_Ciudad_Departamento__r.Name,
                   m.BI_COL_Cargos_Conexion_Total__c,BI_COL_Tipo_de_Moneda__c,m.BI_COL_Cargo_BW_exceso__c,m.BI_COL_Codigo_unico_servicio__r.Name,
                   m.BI_COL_Clasificacion_Servicio__c,m.BI_COL_Medio_Vendido__c,/*m.Producto_Telefonica__r.Ancho_de_Banda_Canal_Cliente__c,*/
                   m.BI_COL_Duracion_meses__c,m.BI_COL_Cargo_Fijo_Mes_Total__c,m.BI_COL_Sucursal_Origen__r.BI_Sede__r.Name,m.BI_COL_Sucursal_Origen__r.BI_Sede__r.BI_DireccionNormalizada__c,
                   m.BI_COL_Oportunidad__r.Campaign.Name, m.BI_COL_Oportunidad__r.Name,m.BI_COL_Total_servicio__c,
                   m.BI_COL_Producto__r.NE__ProdId__r.Name,m.BI_COL_Producto__r.NE__ProdId__r.Rama_Local__c,BI_COL_Producto_Anterior__r.BI_COL_Plan__c,
                   m.BI_COL_Sucursal_Origen__r.Name,m.BI_COL_Descripcion_Referencia__c, m.BI_COL_Sucursal_Origen__r.BI_Sede__r.BI_COL_Direccion_sin_espacio__c,
                   m.BI_COL_Producto_Anterior__r.BI_COL_Ancho_banda__c, BI_COL_Producto_Anterior__r.BI_COL_Producto__c, BI_COL_Producto_Anterior__r.BI_COL_BW_Exceso__c,
                   m.BI_COL_Sede_Destino__r.BI_Sede__r.BI_COL_Ciudad_Departamento__r.Name
                   //m.Producto_Telefonica__r.Descripcion_Referencia__c,m.Producto_Telefonica__r.Name, --> ambos son BI_COL_Producto__r.NE__ProdId__r.Name
                   //m.Producto_Telefonica__r.Linea__c,---> BI_COL_Producto__r.NE__ProdId__r.Rama_Local__c
                   //m.Producto_Telefonica__r.Anchos_de_banda__r.Name,       
                   //m.Producto_Telefonica__r.Producto__c, ---> BI_COL_Producto__r.NE__ProdId__r.Name
                   //m.Producto_Telefonica__r.Plan__c, 
                   //Producto_Telefonica__r.BW_Exceso__r.Name
            from BI_COL_Modificacion_de_Servicio__c m
            where BI_COL_FUN__c =: idFUN 
            and BI_COL_Estado__c != 'Cerrada perdida' // GMN 03/08/2016 - modificado por la demanda 230
            //and (Producto_Telefonica__r.Demo_y_o__c='' or Producto_Telefonica__r.Demo_y_o__c='Servicio ocasional')
            order by Name Asc];    
        System.debug('\n\n lstMS: '+lstMS+'\n\n');
        return lstMS;
    }
    /**
    * Calcula el valor total de instalacion y de servicio de las MS
    * @param lstMS Listado de modificación de servicios
    * @return 
    */
    public void calcularTotales(List<BI_COL_Modificacion_de_Servicio__c> lstMS)
    {
        Decimal totalServicio = 0;
        Decimal totalInstalacion = 0;
        Decimal totalOrdenSuma = 0;
    
        for(BI_COL_Modificacion_de_Servicio__c ms : lstMS)
        {
            Decimal vlrServicio = ms.BI_COL_Cargo_Fijo_Mes_Total__c == null ? 0 : ms.BI_COL_Cargo_Fijo_Mes_Total__c;
            Decimal vlrInstalacion = ms.BI_COL_Cargos_Conexion_Total__c == null ? 0 : ms.BI_COL_Cargos_Conexion_Total__c;
            Decimal meses = ms.BI_COL_Duracion_meses__c == null ? 0 : ms.BI_COL_Duracion_meses__c;

            //Acumular el valor de los totales
            totalServicio+=vlrServicio; 
            totalInstalacion += vlrInstalacion;
            totalOrdenSuma+=vlrServicio*meses;
        }
        //Asigar valores a los atributos globales
        this.totalServicio = totalServicio;
        this.totalInstalacion = totalInstalacion;
        this.totalOrden = totalOrdenSuma+totalInstalacion;
    }
  
    public class WrapperLst
    {
        public String salto{get;set;}
        public String visible{get;set;}
        public List<BI_COL_Modificacion_de_Servicio__c> lista{get;set;}
    }
}