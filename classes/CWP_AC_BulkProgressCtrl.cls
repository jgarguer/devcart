public class CWP_AC_BulkProgressCtrl{

    public String bulkImportRequestId {get;set;}
    public Bit2WinHUB__Bulk_Import_Request__c BIR {get;set;}
    public Integer currentTotalBlocks {get;set;}
    public Integer currentProcessedBlocks {get;set;}

    //RNA 23/01/2018 - Set currentRequestId to match later with the Order Id
    public String currentRequestId {get;set;}

    public String currentStep {get;set;}
    public Integer reloadTimes {get;set;}
    public boolean initialProcessing {get;set;}
    public Integer stepNumber {get;set;}
    public Integer progressPercentage {get;set;}

    public String bulkStatus {get;set;}
    public String bulkError {get;set;}
    public String bulkOrderId {get;set;}


    public CWP_AC_BulkProgressCtrl (){

        bulkImportRequestId = Apexpages.currentPage().getParameters().get('BIRId');
        currentTotalBlocks = 0;
        currentProcessedBlocks = 0;
        reloadTimes = 0;
        progressPercentage =0;
        stepNumber = 0;
        initialProcessing = true;
        init();
    }

    public void init(){

        List<Bit2WinHUB__Bulk_Import_Request__c> BIRList = [SELECT id, Bit2WinHUB__Request_Id__c, Bit2WinHUB__Processed_Blocks__c, Bit2WinHUB__Total_Blocks__c, Bit2WinHUB__Step__c FROM Bit2WinHUB__Bulk_Import_Request__c WHERE Id = :bulkImportRequestId LIMIT 1];
        
        if (!BIRList.isEmpty() ){

            BIR = BIRList[0];
            
            currentTotalBlocks = BIR.Bit2WinHUB__Total_Blocks__c.intValue();
            currentProcessedBlocks = BIR.Bit2WinHUB__Processed_Blocks__c.intValue();
            currentStep = BIR.Bit2WinHUB__step__c;
            currentRequestId = BIR.Bit2WinHUB__Request_Id__c;

            if(BIR.Bit2WinHUB__step__c == 'createNTXML'){
                stepNumber = 1;             
            }else if(BIR.Bit2WinHUB__step__c == 'createXML'){
                stepNumber = 2;
            }else if(BIR.Bit2WinHUB__step__c == 'CreateBCRequests'){
                stepNumber = 3;
            }else if(BIR.Bit2WinHUB__step__c == 'CreateOrders'){
                stepNumber = 4;
            }else{
                stepNumber = 0;
            }
        }
    }


    public PageReference loadBulkImportObject(){
        
        List<Bit2WinHUB__Bulk_Import_Request__c> BIRList = [SELECT id,Bit2WinHUB__Request_Id__c,Bit2WinHUB__Processed_Blocks__c,Bit2WinHUB__Total_Blocks__c,Bit2WinHUB__Step__c FROM Bit2WinHUB__Bulk_Import_Request__c WHERE Id = :bulkImportRequestId LIMIT 1];

        if (!BIRList.isEmpty() ){
            BIR = BIRList[0];

            if (BIR.Bit2WinHUB__Step__c == 'CreateOrders'){
                stepNumber = 4;
                    
                if (BIR.Bit2WinHUB__Processed_Blocks__c==BIR.Bit2WinHUB__Total_Blocks__c){

                        //RNA 25/01/2018 - This code is cloned at the end to avoid the original redirection and going through Summary+PDF of the Order
                        List<Bit2WinHUB__Bulk_Configuration_Request__c> orderId = [SELECT Bit2WinHUB__Configuration__r.Id, Bit2WinHUB__Request_Id__c, Bit2WinHUB__Status__c FROM Bit2WinHUB__Bulk_Configuration_Request__c WHERE Bit2WinHUB__Request_Id__c =: currentRequestId ORDER BY CreatedDate DESC];
                        
                        if(orderId[0].Bit2WinHUB__Status__c == 'Completed'){
                            bulkStatus = 'completed';
                            bulkOrderId = orderId[0].Bit2WinHUB__Configuration__r.Id;

                        } else if (orderId[0].Bit2WinHUB__Status__c == 'Failed'){
                            bulkStatus = 'failed';
                            bulkError = 'Error';
                        }
                        //(END)RNA 25/01/2018 - This code is cloned at the end to avoid the original redirection and going through Summary+PDF of the Order

                    PageReference pr = redirect(BIR.Id);
                    System.debug('**** PAGE REDIRECT : '+pr);
                    return pr;
                    
                }else{

                    //Things to do in the fourth step
                    //Bulk_Import_Request_Methods.createOrders(BIR); //parameters
                    List <Bit2WinHUB__Bulk_Configuration_Request__c> totalList = [SELECT Name, Bit2WinHUB__Configuration__c, Bit2WinHUB__Description__c, Id, Bit2WinHUB__Status__c FROM Bit2WinHUB__Bulk_Configuration_Request__c WHERE Bit2WinHUB__Request_Id__c =: BIR.Bit2WinHUB__Request_Id__c];
                    List <Bit2WinHUB__Bulk_Configuration_Request__c> processedList =  [SELECT Name, Bit2WinHUB__Configuration__c, Bit2WinHUB__Description__c, Id, Bit2WinHUB__Status__c FROM Bit2WinHUB__Bulk_Configuration_Request__c WHERE (Bit2WinHUB__Status__c = 'Completed' OR Bit2WinHUB__Status__c = 'Failed') AND Bit2WinHUB__Request_Id__c =: BIR.Bit2WinHUB__Request_Id__c];

                    Integer processed = 0;
                    Integer total = 0;

                    if(!totalList.isEmpty()){
                        total = totalList.size();
                    }

                    if(!processedList.isEmpty()){
                        processed = processedList.size();
                    }

                    System.debug('**************************** CALL CREATE ORDERS: PROC: ' + processed + '; TOTAL: ' + total);

                    BIR.Bit2WinHUB__Processed_Blocks__c = processed; 
                    BIR.Bit2WinHUB__Total_Blocks__c = total;
                    update BIR;

                    //RNA 26/01/2018 - Returning null to override the redirection with the Lightning Component
                    return null;
                }
            }
        }

        return null;
    }

    public PageReference loadStatus(){

        reloadTimes++;
        System.debug('loadStatus method: '+reloadTimes);
        return loadBulkImportObject(); 
    }

    public PageReference redirect(String bulkImportRequestId){

        List<Bit2WinHUB__Bulk_Configuration_Request__c> orderId = [SELECT Bit2WinHUB__Configuration__r.Id, Bit2WinHUB__Request_Id__c FROM Bit2WinHUB__Bulk_Configuration_Request__c WHERE Bit2WinHUB__Request_Id__c =: currentRequestId ORDER BY CreatedDate DESC];
        PageReference pageRef = new PageReference('/s/order/' + orderId[0].Bit2WinHUB__Configuration__r.Id);

            //RNA 25/01/2018 - Throwing data status from BulkProgress to trigger the redirection
            //Getting orderId from the bulkProgress
            List<Bit2WinHUB__Bulk_Configuration_Request__c> orderId2 = [SELECT Bit2WinHUB__Configuration__r.Id, Bit2WinHUB__Request_Id__c, Bit2WinHUB__Status__c FROM Bit2WinHUB__Bulk_Configuration_Request__c WHERE Bit2WinHUB__Request_Id__c =: currentRequestId ORDER BY CreatedDate DESC];
            
            if(orderId2[0].Bit2WinHUB__Status__c == 'Completed'){
                bulkStatus = 'completed';
                bulkOrderId = orderId[0].Bit2WinHUB__Configuration__r.Id;

            } else if (orderId2[0].Bit2WinHUB__Status__c == 'Failed'){
                bulkStatus = 'failed';
                bulkError = 'Error';
            }
            //(END)RNA 25/01/2018 - Throwing data status from BulkProgress to trigger the redirection

        //RNA 26/01/2018 - Throwing null to override the redirection with the Lightning Component
        //return pageRef;
        return null;
    }
}