public class BI_CHI_Formulario_despacho_CTRL {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Alberto Fernández Díaz
Company:       Accenture
Description:   Controller for BI_CHI_Formulario_despacho VF page
Test Class:    BI_CHI_Formulario_despacho_CTRL_TEST

History:
 
<Date>                  <Author>                <Change Description>
06/04/2017             Alberto Fernández        Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public  List<Case> myCase                      {get; set;} 
    public  List<Account> myAccount                {get; set;} 
    public  List<NE__OrderItem__c> myOrderItems    {get; set;}
    public  Id caseId                              {get; set;}

    public BI_CHI_Formulario_despacho_CTRL() {

        getData();

    }

    public void getData(){

        if(!Test.isRunningTest()) {
            caseId = ApexPages.currentPage().getParameters().get('id');
        }
        
        try {

            myCase = [SELECT  Id,
                                    Account.Id,
                                    Contact.Id,
                                    Contact.Name,
                                    Contact.Phone,
                                    BI_Nombre_de_la_Oportunidad__r.Name, 
                                    BI_caso_Id_Oportunidad__c,
                                    BI_COL_Sede__r.Name,
                                    BI_COL_Sede__r.BI_Sede__r.Name,
                                    BI_CHI_Gestion__c,
                                    BI_CHI_Codigo_de_Cliente_Legado__c,
                                    BI_CHI_Plan_Tarifario_Legado__c,
                                    BI_CHI_Codigo_Beneficio_Legado__c,
                                    BI_CHI_Codigo_SGP__c
                                    FROM Case WHERE Id = :caseId];

            myAccount = [SELECT Id,
                                     Name,
                                     BI_Tipo_de_identificador_fiscal__c,
                                     BI_No_Identificador_fiscal__c FROM Account WHERE Id = :myCase[0].Account.Id];

            myOrderItems = [SELECT Id,
                                        Name,
                                        NE__RecurringChargeOv__c,
                                        NE__OneTimeFeeOv__c,
                                        NE__ProdId__r.Name
                                        FROM NE__OrderItem__c WHERE NE__OrderId__r.NE__OptyId__c = :myCase[0].BI_Nombre_de_la_Oportunidad__C];
        }
        catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_CHI_Formulario_despacho_CTRL.BI_CHI_Formulario_despacho_CTRL', 'BI_EN', Exc, 'Trigger');
        }

    }
        
}