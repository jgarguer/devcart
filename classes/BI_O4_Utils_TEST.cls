@isTest
private class BI_O4_Utils_TEST {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Ricardo Pereira
	Company:       New Energy Aborda
	Description:   Test class for BI_O4_Utils
	
	History:
	
	<Date>					<Author>				<Change Description>
	20/09/2016				Ricardo Pereira			Initial version
	02/02/2017				Pedro Párraga			Increase coverage
	------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	static{
        BI_TestUtils.throw_exception = false;
    }	

	@isTest static void getAllFieldsApi_test() {
		Account acc = new Account();
		BI_O4_Utils.getAllFieldsApi(acc.getSObjectType());
	}
	
	@isTest static void getSOQLQueryAllFields_test() {
		RecordType rt = new RecordType();
		BI_O4_Utils.getSOQLQueryAllFields(rt.getSObjectType());
	}
	
	@isTest static void calcGeographicalScope_test() {
		String paises = 'Spain;Venezuela;Argentina;Poland;China';
		BI_O4_Utils.calcGeographicalScope(paises);
	}


	@isTest static void getCustomObjectId() {
		String paises = 'Spain;Venezuela;Argentina;Poland;China';
		BI_O4_Utils.getCustomObjectId('Test');
	}
}