public without sharing class BI_CCP_Methods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Methods executed by BI_Case Triggers 
    Test Class:    BI_CCP_Methods
    
    History:
     
    <Date>                  <Author>                <Change Description>
    25/05/2015              Antonio Moruno          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void insertPermission_CCP(List<BI_Contact_Customer_Portal__c> olds, List<BI_Contact_Customer_Portal__c> news){ 
        Set<Id> IdsUsers = new Set<Id>();
        Map<Id,BI_Contact_Customer_Portal__c> contsCCP = new Map<Id,BI_Contact_Customer_Portal__c>();
        for(BI_Contact_Customer_Portal__c CCPiT :news){
            IdsUsers.add(CCPiT.BI_User__c);
            contsCCP.put(CCPiT.BI_User__c,CCPiT);
        }
        List<User> UsersNews = [Select Id, ProfileId, AccountId, ContactId FROM User where Id IN: IdsUsers];
        Set<Id> accIds = new Set<Id>();
        for(User UsersIte :UsersNews){
            accIds.add(UsersIte.AccountId);
        }
        List<Account> AccOfUser = [Select Id, BI_Country__c, BI_Segment__c FROM Account where Id IN :accIds];
        
        String sql = 'SELECT Id, BI_Agenda_Compartida__c,BI_Comunidad_de_Expertos__c,BI_Country__c,BI_Default__c,BI_Documentacion__c,BI_Equipo_de_Telefonica__c,BI_Escalamiento_en_Telefonica__c,BI_Facturacion_y_cobranzas__c,BI_Ideas_Corner__c,BI_MarketPlace__c,BI_Monitoreo__c,BI_Portales__c,BI_Reclamos_y_pedidos__c,BI_Reportes__c,BI_Segment__c,BI_Servicios_Instalados__c,Name FROM BI_Permisos_PP__c where BI_Default__c = true AND (';
        for(Account AccSegment :AccOfUser){
            sql += '(BI_Segment__c =\''+ AccSegment.BI_Segment__c +'\' and BI_Country__c=\''+ AccSegment.BI_Country__c + '\') OR ';
        }
        sql = sql.substring(0,sql.length()-3);
        sql += ')';
        
        List<BI_Permisos_PP__c> UserPerms = new List<BI_Permisos_PP__c>();
        if(!AccOfUser.IsEmpty()){
            System.debug('Dynamic Query*: '+sql);
            UserPerms = Database.query(sql);
        }
        map<Id,Account> accSegCount = new map<Id,Account>();
        for(User item :UsersNews){
            for(Account accToMap :AccOfUser){
                if(accToMap.Id == item.AccountId){
                    accSegCount.put(item.Id,accToMap);
                }
            }
        }
        for(User item :UsersNews){
            for(BI_Permisos_PP__c perms :UserPerms){
                if(accSegCount.get(item.Id) != null && accSegCount.get(item.Id).BI_Segment__c != null && accSegCount.get(item.Id).BI_Country__c != null){
                    if(perms.BI_Segment__c == accSegCount.get(item.Id).BI_Segment__c && perms.BI_Country__c == accSegCount.get(item.Id).BI_Country__c){
                        contsCCP.get(item.Id).BI_Permisos_PP__c = perms.Id;
                    }
                }
            }
        }
    }




    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos  
    Company:       Aborda.es
    Description:   Method that includes a account team member in accounts related to CCP for allow visibility of Acc and Cases.
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/10/2015              Micah Burgos            Initial Version     
    29/07/2016              Guillermo Muñoz         Commented future call to avoid exceptions in future calls of the method
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void insertAccountShare_CCP( List<BI_Contact_Customer_Portal__c> news){

        try{

            Set<Id> set_id_acc = new Set<Id>();

            for(BI_Contact_Customer_Portal__c ccp :news){
                set_id_acc.add(ccp.BI_Cliente__c);
            }

            //Set<String> set_unique_accTM = new Set<String>();
            //for(AccountTeamMember accTM :[SELECT AccountAccessLevel,AccountId,TeamMemberRole,UserId FROM AccountTeamMember WHERE AccountId IN :set_id_acc]){
            //    set_unique_accTM.add(accTM.UserId + '-'+ accTM.AccountId);
            //}

            //List<AccountTeamMember> lst_insert_ACTM = new List<AccountTeamMember>();

            List<BI_Contact_Customer_Portal__c> news_withData = [SELECT BI_Cliente__c, BI_User__c, BI_User__r.IsActive, BI_Activo__c FROM BI_Contact_Customer_Portal__c WHERE Id IN :news];
            
            List<AccountShare> lst_share_ACTM = new List<AccountShare>();
            for(BI_Contact_Customer_Portal__c ccp :news_withData){
                if(ccp.BI_Cliente__c != null && ccp.BI_User__c != null && ccp.BI_Activo__c == true /*&& !set_unique_accTM.contains(ccp.BI_User__c+'-'+ ccp.BI_Cliente__c)*/){
                    //Access to Accounts
                    //AccountTeamMember AccTM = new AccountTeamMember(AccountId = ccp.BI_Cliente__c,
                    //                                                  UserId = ccp.BI_User__c ,
                    //                                                  TeamMemberRole = Label.BI_RolVisibilidadPortal);

                    //lst_insert_ACTM.add(AccTM);

                    //Access to Cases
                    AccountShare share = new AccountShare();
                        share.AccountId  = ccp.BI_Cliente__c;
                        share.AccountAccessLevel = 'Read';
                        share.OpportunityAccessLevel =  'None';
                        share.CaseAccessLevel =  'Read';
                        share.RowCause = 'Manual' ;
                        share.UserOrGroupId = ccp.BI_User__c ;
                    lst_share_ACTM.add(share);
                }
            }

            //insert lst_insert_ACTM;

            System.debug('lst_share_ACTM: ' + lst_share_ACTM);

            List<Database.SaveResult> srList = Database.insert(lst_share_ACTM, false); 
            List<String> lst_log_msg = new List<String>();

            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        String msg_err = 'getFields() = ' + String.join(err.getFields(), ',') + '\n';
                          msg_err += 'getMessage() = ' + err.getMessage()+ '\n';
                          msg_err += 'getStatusCode() = ' + err.getStatusCode()+ '\n';

                        lst_log_msg.add(msg_err);
                    }
                }
            }
            //insert_BI_Logs(lst_log_msg);

        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_CCP_Methods.insertAccountTeamMember_CCP', 'BI_EN', exc, 'Trigger');
        }
    }
    

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos  
    Company:       Aborda.es
    Description:   Method that delete Account Share when a CCP is deleted.
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    22/10/2015              Micah Burgos            Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void deleteAccountShare_CCP(List<BI_Contact_Customer_Portal__c> olds){

        try{

            Set<Id> set_id_acc = new Set<Id>();
            Set<Id> set_id_usr = new Set<Id>();

            for(BI_Contact_Customer_Portal__c ccp :olds){
                set_id_acc.add(ccp.BI_Cliente__c);
                set_id_usr.add(ccp.BI_User__c);
            }

            //List<BI_Contact_Customer_Portal__c> news_withData = [SELECT BI_Cliente__c, BI_User__c, BI_User__r.IsActive, BI_Activo__c FROM BI_Contact_Customer_Portal__c WHERE Id IN :news];
            
            List<AccountShare> lst_share_CCP = [SELECT AccountAccessLevel,AccountId,CaseAccessLevel,ContactAccessLevel,
                    Id,IsDeleted,LastModifiedById,LastModifiedDate,OpportunityAccessLevel,RowCause,UserOrGroupId 
                    FROM AccountShare WHERE RowCause = 'Manual' AND  AccountId IN :set_id_acc AND UserOrGroupId IN :set_id_usr ];


            delete lst_share_CCP;

        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_CCP_Methods.deleteAccountShare', 'BI_EN', exc, 'Trigger');
        }
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos  
    Company:       Aborda.es
    Description:   Method that insert BI_Logs in future method
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    13/11/2015              Micah Burgos              Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @future
    public static void insert_BI_Logs(List<String> lst_logs_msg){
        try{
            List<BI_Log__c> lst_logs = new List<BI_Log__c>();

            for(String msg_err : lst_logs_msg) {

               BI_Log__c log = new BI_Log__c(BI_Asunto__c = 'BI_CCP_Methods.insertAccountTeamMember_CCP',
                    BI_Bloque_funcional__c = 'BI_EN',
                    BI_NumeroLinea__c = 116,
                    BI_Descripcion__c = 'Insert of Account Share for users failed',
                    //BI_StackTrace__c = null,
                    BI_Tipo_Error__c = 'DML_EXCEPTION',
                    BI_Tipo_de_componente__c = 'Apex Trigger',
                    BI_DML_Info__c = msg_err);

                lst_logs.add(log);
            }

            if(!lst_logs.isEmpty()){
                insert lst_logs;
            }

        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_CCP_Methods.insert_BI_Logs', 'BI_EN', exc, 'Trigger [future method]');
        }
    }
}