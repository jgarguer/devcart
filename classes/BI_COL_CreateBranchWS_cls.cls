/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-03-24      Manuel Esthiben Mendez Devia (MEMD)     Cloned Class
* @Version   1.1    2015-03-24      Jeisson Alexander Rojas Noy  (JARN)     Cambio en las query
*************************************************************************************************************/
public with sharing class BI_COL_CreateBranchWS_cls 
{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        ------------------------
	    Company:       Aborda
	    Description:   ------------------------
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    21/04/2016                      Guillermo Muñoz             Added filter by country.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
     public static void Validate_Addressduplicate(list<BI_Punto_de_instalacion__c> lstNuevo, list<BI_Punto_de_instalacion__c> lstViejo){
    
    
        List<BI_Punto_de_instalacion__c> lstSecundarySede = new List<BI_Punto_de_instalacion__c>();
        //List<BI_Punto_de_instalacion__c> lstactual = new List<BI_Punto_de_instalacion__c>();
        Account acc = [SELECT Id, BI_Country__c FROM Account WHERE Id =: lstNuevo[0].BI_Cliente__c];

        if(acc != null && acc.BI_Country__c == Label.BI_Colombia){

        

        	try{
        	
            /*lstactual = [Select  Id from BI_Punto_de_instalacion__c
                                       where   Id = :lstNuevo[0].Id
                                       limit 1];
            System.debug('resultado primera consulta =================== '+lstactual);*/

            
               lstSecundarySede = [Select  Id from    BI_Punto_de_instalacion__c
                                  where   BI_Cliente__c =: lstNuevo[0].BI_Cliente__c
                                  and     BI_Sede__c =: lstNuevo[0].BI_Sede__c
                                  //and     Id != :lstNuevo[0].Id
                                  limit 1];
               System.debug('\n\n lstSecundarySede.size(): '+lstSecundarySede+' \n\n');

            }catch(Exception e)
                {
                    
                    System.debug('\n\n Error en la consulta '+lstSecundarySede.size()+' \n\n');

                   // lstNuevo[0].BI_DireccionNormalizada__c = Label.MensajeErrorDireccion;
                }
            
              System.debug('\n\n resultado segunda coonsulta =================== \n '+lstSecundarySede);

            if( lstSecundarySede.size() > 0 )
            {   
               try{
                             lstNuevo[0].addError(label.BI_COL_lblErrorSede);
                    }catch(Exception e)
                    {
                             System.debug('\n\n ERROR =======  \n '+E.getMessage());
                    }
            }
        }
     }


@future (callout=true)
    public static void crearSucursalTelefonica (List<String> lstIdSucursales, String accion)
    { 
        map<ID,ws_wwwTelefonicaComNotificacionessalesfo.sucursal> infSucursalEnviad = new map<ID,ws_wwwTelefonicaComNotificacionessalesfo.sucursal>();  
        // Array para enviar al Web Service
        ws_wwwTelefonicaComNotificacionessalesfo.sucursal[] arraySucursal;
        System.debug('\n\n lstIdSucursales: '+lstIdSucursales+'\n\n');
        if(lstIdSucursales!=null && lstIdSucursales.size()>0)
        {
            //Query para traer los datos del objeto Account dependiendo del ID q llega del trigger
            BI_Punto_de_instalacion__c objSucursal = new BI_Punto_de_instalacion__c();
            List<BI_Punto_de_instalacion__c> lstSucursal = [select Id, Name, BI_COL_Tipo_sede__c,BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Regional__c, BI_COL_Codigo_sisgot__c, BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c, BI_Sede__c,
                                                BI_Sede__r.BI_Localidad__c, BI_Sede__r.BI_COL_Longitud__c, BI_Sede__r.BI_COL_Latitud__c, BI_Contacto__r.BI_Tipo_de_documento__c,BI_Sede__r.BI_COL_Direccion_split__c,
                                                BI_COL_Contacto_instalacion__r.BI_Tipo_de_documento__c, BI_COL_Contacto_instalacion__r.Id,BI_Sede__r.BI_COL_Placa__c,
                                                BI_Contacto__r.Id, BI_Cliente__c, BI_Sede__r.BI_COL_Estado_callejero__c,BI_Cliente__r.BI_Tipo_de_identificador_fiscal__c,BI_Cliente__r.BI_No_Identificador_fiscal__c,
                                                LastModifiedById, BI_Sede__r.BI_COL_Direccion_sin_espacio__c
                                            From BI_Punto_de_instalacion__c
                                            Where Id in :lstIdSucursales ALL ROWS];
            
            ws_wwwTelefonicaComNotificacionessalesfo.sucursal sc = new ws_wwwTelefonicaComNotificacionessalesfo.sucursal();
            arraySucursal = new ws_wwwTelefonicaComNotificacionessalesfo.sucursal[]{};
            
            system.debug('-------------->Lista Sedes para revisar'+objSucursal);
            system.debug('-------------->Lista Sedes para revisar222'+lstSucursal);
            
            if(lstSucursal.size()>0)
            {
                //Llamando a funcion en el Ws que espera los datos para Contacto.
                
                
                for(BI_Punto_de_instalacion__c sede : lstSucursal)
                {
                    if(sede.BI_Sede__r.BI_COL_Estado_callejero__c == 'Validado por callejero')
                    {
                        system.debug('..==--->.. objeto result sede.. '+sede);
                        sc = new ws_wwwTelefonicaComNotificacionessalesfo.sucursal();
                        sc.name = sede.Name; 
                        sc.tipoSede = sede.BI_COL_Tipo_sede__c;
                        sc.regional = sede.BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Regional__c;
                        if(sede.BI_COL_Codigo_sisgot__c!=null && sede.BI_COL_Codigo_sisgot__c!='')
                            sc.codigoSisgot = sede.BI_COL_Codigo_sisgot__c;
                        else
                            sc.codigoSisgot = '0';
                        sc.codigoDane = sede.BI_Sede__r.BI_COL_Ciudad_Departamento__r.BI_COL_Codigo_DANE__c;
                        sc.direccionSucursal = sede.BI_Sede__r.BI_COL_Direccion_sin_espacio__c; 
                        sc.barrio = sede.BI_Sede__r.BI_Localidad__c;
                        sc.longitud = sede.BI_Sede__r.BI_COL_Longitud__c;
                        sc.latitud = sede.BI_Sede__r.BI_COL_Latitud__c;
                        sc.tipoDocContactoFacturacion = sede.BI_Contacto__r.BI_Tipo_de_documento__c;
                        sc.idContactoFacturacion = sede.BI_Contacto__r.Id;
                        sc.tipoDocumentoContactoTecnico = sede.BI_COL_Contacto_instalacion__r.BI_Tipo_de_documento__c; 
                        sc.idContactoTecnico = sede.BI_COL_Contacto_instalacion__r.Id;
                        System.debug('TEST ============ ' + sede.BI_Cliente__r.BI_No_Identificador_fiscal__c +'\n==== \n'+sede.BI_Cliente__r.BI_Tipo_de_identificador_fiscal__c);
                        sc.idCliente = sede.BI_Cliente__r.BI_No_Identificador_fiscal__c;
                        sc.tipoDocCliente= sede.BI_Cliente__r.BI_Tipo_de_identificador_fiscal__c;                   //OA 30-07-2012 Se agrega nuevo campo 
            //          sc.idSucursal = suc.Id;                                         OA variable reemplazada por idSalesForceSede
                        sc.idSalesForceSede=sede.Id;                                    //OA 30-07-2012 Se agrega nuevo campo
                        sc.idUsuarioModificacion=Userinfo.getUserId();                  //OA 30-07-2012 Se agrega nuevo campo
                        sc.idSalesForceContactoTecnico=sede.BI_COL_Contacto_instalacion__r.Id;  //OA 30-07-2012 Se agrega nuevo campo
                        sc.accion=accion;                                               //OA 30-07-2012 Se agrega nuevo campo
                        sc.estado=sede.BI_Sede__r.BI_COL_Estado_callejero__c;                               //OA 30-07-2012 Se agrega nuevo campo
                        
                        /*********************************************/ 
                        list<string> split=BI_COL_CreateBranchWS_cls.processSplit(sede.BI_Sede__r.BI_COL_Direccion_split__c); 
                        sc.pathType=split[0].trim();
                        sc.pathNumber=Integer.Valueof(split[1].trim());//-------------
                        sc.firstPathCharacters=split[2].trim();
                        sc.secondPathCharacters=split[3].trim();
                        sc.pathZone=split[4].trim();
                        sc.crossPathType=split[5].trim();
                        sc.crossPathNumber=Integer.Valueof(split[6].trim());//-------------
                        sc.firstCrossPathCharacters=split[7].trim();
                        sc.secondCrossPathCharacters=split[8].trim();
                        sc.crossPathZone=split[9].trim();
                        sc.addressNumber=sede.BI_Sede__r.BI_COL_Placa__c;
                        sc.addressCharacters='';
                        sc.zone=0;
                        sc.sector=0;
                        sc.block=0;
                        sc.blockSide='0';
                        sc.spaceNumber=0;
                        sc.serviceNumber=0;
                        
                        arraySucursal.add(sc);
                    }
                }
                BI_COL_FlagforTGRS_cls.flagTriggers = true;
            }else if(accion=='Eliminar'){

                System.debug('ENTRO A ELIMINAR\n\n sucursal'+lstSucursal );

                for( BI_Punto_de_instalacion__c suc:lstSucursal){
                    sc = new ws_wwwTelefonicaComNotificacionessalesfo.sucursal();
                    sc.idSalesForceSede=suc.Id;
                    sc.accion=accion;
                    sc.idUsuarioModificacion=Userinfo.getUserId();
                    arraySucursal.add(sc);
                    infSucursalEnviad.put(suc.Id,sc);
                }
            }
        try{
            ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] rtaWSSucursal;
            ws_wwwTelefonicaComNotificacionessalesfo.NotificacionesSalesForceSOAP xy = new ws_wwwTelefonicaComNotificacionessalesfo.NotificacionesSalesForceSOAP();
            // Enviando a la funcion registrar sucursales el array de las sucursales
            System.debug('..==.. Antes de envio al SW-->'+arraySucursal);
            rtaWSSucursal = xy.RegistrarSucursales(arraySucursal);          
            System.debug('..==.. Rta Web Service RegistrarSucursales----> ..==.. '+rtaWSSucursal);
            if(accion!='Eliminar')
                creaLogTransaccion(rtaWSSucursal,lstSucursal,infSucursalEnviad);
            }catch(Exception e){
          System.debug('..==.. Error en la conexión con el servicio web de notificaciones ');
          creaLogTransaccionError(''+e,lstSucursal,'NOTIFICACIONES');
            }
        }
    }
    
        public static list<String> processSplit(string spl){
            
            list<String> lstPar;        
            //SPLIT
            if(spl==null){
                lstPar=new String[]{'-','0','-','-','-','-','0','-','-'};

            }else{
                lstPar=spl.split('\\x7c');
            }
            system.debug('lstPar01'+lstPar);

            System.debug('\n\n inicia split-->'+lstPar+' --'+lstPar.size()+'\n'+spl+'\n\n');
            if(lstPar.size()<9)
            {
                lstPar=new String[]{'-','0','-','-','-','-','0','-','-'};
                system.debug('lstPar02'+lstPar);
                
                return lstPar;
            }else{
                //orden = pathType,pathNumber,firstPathCharacters,secondPathCharacters,pathZone,crossPathType,crossPathNumber,firstCrossPathCharacters,secondCrossPathCharacters,crossPathZone
                for(Integer i=0;i<lstPar.size();i++){
                    if(lstPar.get(i).remove(' ')=='' && i!=1 && i!=6){
                        lstPar[i]='-';
                    }
                    
                    if(lstPar.get(i).remove(' ')=='' && (i==1 || i==6)){
                        lstPar[i]='0';                  
                    }               
                }
                
                 
                
                if(lstPar.size()!=10)
                {
                    lstPar.add('-');    
                }
                return lstPar;
                    
            }           
            //END SPLIT
        
    
    } 
    
    public static void creaLogTransaccionError(String error, List<BI_Punto_de_instalacion__c> lstIdSucursales,String infEnviada){
    List<BI_Log__c> lstLogTran=new List<BI_Log__c>();
    BI_Log__c log=null; 
    system.debug('métod de BI Log::::'+error+'\n\n'+lstIdSucursales);
    
    for(BI_Punto_de_instalacion__c suc:lstIdSucursales){
        log=new BI_Log__c(BI_COL_Informacion_recibida__c=error,BI_COL_Sede__c=suc.Id,BI_COL_Estado__c='FALLIDO',BI_COL_Interfaz__c='NOTIFICACIONES',BI_COL_Informacion_Enviada__c=infEnviada); //,BI_Numero_de_documento__c='FALLIDO', Name='Error Servicio Web: '+servicio,
        lstLogTran.add(log);
    }
    
    system.debug('Insertando BI Log::::'+lstLogTran);
    insert lstLogTran;
    }

     /**** OA: 04-09-2012 Se agrega método de creación de log de transacciones para el objeto Cuenta ****/ 
    public static void creaLogTransaccion(ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] arrRespuesta, List<BI_Punto_de_instalacion__c> lstSucursal, map<ID,ws_wwwTelefonicaComNotificacionessalesfo.sucursal> infSucursalEnviad){
        List<BI_Log__c> lstLogTran=new List<BI_Log__c>();
        BI_Log__c log=null; 
        system.debug('---->métod de log de transacciones::::'+arrRespuesta+'\n\n'+lstSucursal);
        system.debug('Iteracion For 1');
        lstLogTran.clear();
        //if(!Test.isRunningTest()) WHCN comentado para completar clases de prueba
            for(BI_Punto_de_instalacion__c suc:lstSucursal){ 
                if(arrRespuesta[0].idError.equals('0'))
                    log=new BI_Log__c(BI_COL_Informacion_recibida__c='OK, Respuesta Procesada',BI_COL_Sede__c=suc.Id,BI_COL_Tipo_Transaccion__c='SALIDA NOTIFICACIONES',BI_COL_Estado__c='EXITOSO',BI_COL_Interfaz__c='NOTIFICACIONES',BI_COL_Informacion_Enviada__c=String.valueOf(infSucursalEnviad.get(suc.id)));
                else
                    log=new BI_Log__c(BI_COL_Informacion_recibida__c='('+arrRespuesta[0].idError+')::'+arrRespuesta[0].descripcionError,BI_COL_Sede__c=suc.Id,BI_COL_Tipo_Transaccion__c='SALIDA NOTIFICACIONES',BI_COL_Estado__c='FALLIDO',BI_COL_Interfaz__c='NOTIFICACIONES',BI_COL_Informacion_Enviada__c=String.valueof(infSucursalEnviad.get(suc.id)));
                lstLogTran.add(log);
            }
        
        system.debug('------->Insertando log de transacciones::::'+lstLogTran);
        insert lstLogTran;
    }
    

}