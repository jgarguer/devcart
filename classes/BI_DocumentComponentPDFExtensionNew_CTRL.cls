/*-------------------------------------------------------------------------------------------------------------------------------------------------------
         Author:        Fabrizio Borrelo
         Company:       Accenture
         Description:   Method that help to generate the document
         
         History: 
         
         <Date>                          <Author>                    <Change Description>
         18/10/2017                    Fabrizio Borrelo                 Initial Version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_DocumentComponentPDFExtensionNew_CTRL {

    public String text {get;set;}
    public String text2 {get;set;}
    public NE.DocumentBuilder docBuilder {get;set;}
    
 public BI_DocumentComponentPDFExtensionNew_CTRL (Apexpages.Standardcontroller stdCon){
        try{
                      
            String optyId = ApexPages.currentPage().getParameters().get('optyId');
            
            List<NE__Order__c> orderIdList = [SELECT Id,NE__OrderStatus__c FROM NE__Order__c WHERE NE__OptyId__c =:optyId  AND NE__OrderStatus__c IN('Active','Active - Locked product','Active - Locked product','Active - Rápido') limit 1];      
            
            if(!orderIdList.isEmpty()){
            
            PageReference pag1 = new PageReference('/apex/ne__DocumentComponentPDF?edit=false&PDF=false&mapName=EEContractDataMap&parentId='+orderIdList[0].Id+'&nameDCH=Contrato_Movil_Peru&dateFormat=dd-MM-yyyy');
            PageReference pag2 = new PageReference('/apex/ne__DocumentComponentPDF?edit=false&PDF=false&mapName=EEContractDataMap2&parentId='+orderIdList[0].Id+'&nameDCH=Contrato_Movil_Peru_2&dateFormat=dd-MM-yyyy');  
             
            Map<String,Object> placeholders = null;
            text = pag1.getContent().toString();
            text2 = pag2.getContent().toString();
            } 
        }catch(Exception e){
            System.debug('ERROR VF FVI_CONTRATO'+e);
        }   
    }   
}