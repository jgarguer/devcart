/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   BI_O4_AsociarAccountToCipShareJOB JOB for asociate account to CIP share
    Test Class:    BI_O4_AsociarAccountToCipShareJOB_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class BI_O4_AsociarAccountToCipShareJOB implements Schedulable {

	global void execute(SchedulableContext ctx){
		
		AsociarAccountToCipShare();
		EliminarAccountToCipShare();
    }
	
	
	public void AsociarAccountToCipShare(){
		
		Date dateStart = Date.today().addDays(-1);
		Date dateEnd = Date.today();
		Set<String> setIdAccount = new Set<String>();
		Map<String, List<String>> mapAccountIdlistUser = new Map<String, List<String>>();
		List<BI_Plan_de_accion__Share> listCIPShareInsert = new List<BI_Plan_de_accion__Share>();
		//buscar registros que se han insertado createdDate
		//que user_TMS != null
		//añadir esos usuarios UserId del AccountTeamMember al CIP_Share de las CIP asociadas a la cuenta
		List<AccountTeamMember> listAccountTeams = new List<AccountTeamMember>();
		if(!System.Test.isRunningTest()){
			listAccountTeams = [SELECT Id, AccountId, UserId FROM AccountTeamMember WHERE CreatedDate  >: dateStart AND CreatedDate <: dateEnd AND UserId != null];
		}
		else{
			listAccountTeams = [SELECT Id, AccountId, UserId FROM AccountTeamMember WHERE CreatedDate >= : dateEnd AND UserId != null];
		}
		if(!listAccountTeams.isEmpty()){
			for(AccountTeamMember l: listAccountTeams){
				setIdAccount.add(l.AccountId);
				List<String> aux = mapAccountIdlistUser.get(l.AccountId);
				if(aux == null){
					aux = new List<String>();
				}
				aux.add(l.UserId);
				mapAccountIdlistUser.put(l.AccountId, aux);
			}
			if(!setIdAccount.isEmpty()){
				List<BI_Plan_de_accion__c> listCIP = [SELECT Id, BI_O4_MNCs_Account__c FROM BI_Plan_de_accion__c WHERE BI_O4_MNCs_Account__c IN : setIdAccount];
				for(BI_Plan_de_accion__c c : listCIP){
					List<String> aux = mapAccountIdlistUser.get(c.BI_O4_MNCs_Account__c);
					if(aux != null){
						for(String s : aux){
							BI_Plan_de_accion__Share cipShare = new BI_Plan_de_accion__Share();
							cipShare.ParentId = c.Id;
							cipShare.UserOrGroupId = s;
							cipShare.AccessLevel = 'Read';
							listCIPShareInsert.add(cipShare);	
						}
					}
				}
			}
		}
		if(!listCIPShareInsert.isEmpty()){
			try{
				insert listCIPShareInsert;
			}catch (Exception e1){
				System.debug(e1);
			}
		}		
	}
	
	public void EliminarAccountToCipShare(){
		
		Set<String> setIdAccount = new Set<String>();
		Set<String> setIdCIP = new Set<String>();
		List<BI_Plan_de_accion__Share> CIPShareEliminar = new List<BI_Plan_de_accion__Share>();
		Map<String, String> mapCIPIdAccount = new Map<string, String>();
		Map<String, List<String>> mapAccountListUser = new Map<String, List<String>>();
		//buscar registros que tengan el check a true y eliminarlos modificados las ultimas 24 horas
		Date dateStart = Date.today().addDays(-1);
		Date dateEnd = Date.today();
		System.debug('dateStart: '+ dateStart + 'dateEnd: ' + dateEnd);

		List<AccountTeamMember> listAccountTeams = new List<AccountTeamMember>();
		
		if(!System.Test.isRunningTest()){
			listAccountTeams = [SELECT Id, AccountId, UserId  FROM AccountTeamMember WHERE LastModifiedDate >: dateStart AND LastModifiedDate <: dateEnd AND UserId != null];
		}else{
			listAccountTeams = [SELECT Id, AccountId, UserId FROM AccountTeamMember WHERE CreatedDate >=: dateEnd AND UserId != null];
		}

		if(!listAccountTeams.isEmpty()){
			for(AccountTeamMember l: listAccountTeams){
				setIdAccount.add(l.AccountId); //Seet de account para buscar en tabla CIP
				List<String> aux = mapAccountListUser.get(l.AccountId); 
				if(aux == null){
					aux = new List<String>();
				}
				aux.add(l.UserId);
				mapAccountListUser.put(l.AccountId, aux); //Mapa de cuentas y lista de usuarios
			}
			List<BI_Plan_de_accion__c> listCIP = [SELECT Id, BI_O4_MNCs_Account__c FROM BI_Plan_de_accion__c WHERE BI_O4_MNCs_Account__c IN : setIdAccount];
			for(BI_Plan_de_accion__c c : listCIP){
				setIdCIP.add(c.Id);
				mapCIPIdAccount.put(c.Id, c.BI_O4_MNCs_Account__c); //Map id del CIP y cuenta relacionada
			}
			List<BI_Plan_de_accion__Share> listCIPShare = [SELECT Id, ParentId, UserOrGroupId FROM BI_Plan_de_accion__Share WHERE ParentId IN : setIdCIP AND RowCause <> 'Owner'];
				system.debug(listCIPShare);
				system.debug(mapCIPIdAccount);
				system.debug(mapAccountListUser);
			for(BI_Plan_de_accion__Share cS : listCIPShare){
				String aux = mapCIPIdAccount.get(cS.ParentId); //Obtengo el Account asociado
				if(aux != null){
					List<String> aux2 = mapAccountListUser.get(aux); //obtengo la lista de usuariosTMS
					if(aux2 != null){
						//Para cada usuario de la lista miramos si coincide con el CPI_Share, si coincide eliminamos CPI_Share
						for(string s: aux2){
							if(s == cS.UserOrGroupId){
								CIPShareEliminar.add(cS);
								break;
							}
						}
					}
				}
			}
		}
		if(!CIPShareEliminar.isEmpty()){
			System.debug(CIPShareEliminar);
			delete CIPShareEliminar;
		}
	}
}