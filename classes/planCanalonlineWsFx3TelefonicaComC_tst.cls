/**
* Avanxo Colombia
* @author           Oscar Alejandro Jimenez Forero
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-08-26      Oscar Alejandro Jimenez Forero (OAJF)   Class Created
*************************************************************************************************************/
@isTest
global class planCanalonlineWsFx3TelefonicaComC_tst {
	
	@isTest
	static void test_method_1() {
		
		planCanalonlineWsFx3TelefonicaComC.PlanCanalOnlineSOAP webser = new planCanalonlineWsFx3TelefonicaComC.PlanCanalOnlineSOAP();
		dtoWsFx3TelefonicaComCo.PlanCanalOnlineMassiveRequest canalOnlineMassiveRequest = new dtoWsFx3TelefonicaComCo.PlanCanalOnlineMassiveRequest();		
		
		Test.startTest();		
		Test.setMock( WebServiceMock.class, new MockResponseGenerator1() );
		webser.queryForeignPlanCallsMassive(canalOnlineMassiveRequest);
		Test.stopTest();		
	}

	@isTest
	static void test_method_2() {
		
		planCanalonlineWsFx3TelefonicaComC.PlanCanalOnlineSOAP webser = new planCanalonlineWsFx3TelefonicaComC.PlanCanalOnlineSOAP();		
		dtoWsFx3TelefonicaComCo.PlanCanalOnlineRequest canalOnlineRequest = new dtoWsFx3TelefonicaComCo.PlanCanalOnlineRequest();		
		
		Test.startTest();		
		Test.setMock( WebServiceMock.class, new MockResponseGenerator2() );		
		webser.queryForeignPlanCalls(canalOnlineRequest);
		Test.stopTest();		
	}
	
	global class MockResponseGenerator1 implements WebServiceMock {
		global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){

			planCanalonlineWsFx3TelefonicaComC.queryForeignPlanCallsMassiveResponse_element objresp = new planCanalonlineWsFx3TelefonicaComC.queryForeignPlanCallsMassiveResponse_element();
			objresp.canalOnlineMassiveResponse = new dtoWsFx3TelefonicaComCo.PlanCanalOnlineMassiveResponse();			
			response.put( 'response_x', objresp);
		}
	}

	global class MockResponseGenerator2 implements WebServiceMock {
		global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){

			planCanalonlineWsFx3TelefonicaComC.queryForeignPlanCallsResponse_element objresp = new planCanalonlineWsFx3TelefonicaComC.queryForeignPlanCallsResponse_element();
			objresp.canalOnlineResponse = new dtoWsFx3TelefonicaComCo.PlanCanalOnlineResponse();			
			response.put( 'response_x', objresp);
		}
	}
}