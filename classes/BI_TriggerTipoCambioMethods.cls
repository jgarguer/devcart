public class BI_TriggerTipoCambioMethods {
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Cristina Rodriguez
    Company:       Accenture
    Description:   Update / Insert list of mapCurrency
    
    IN:            
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    10/05/2017              Cristina Rodríguez      Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void actMapCurrency(List <TipoCambio__c> news){
        Map<String, CurrencyTypeFacturacion__c> mapCurrencys = new Map<String, CurrencyTypeFacturacion__c>();
        //recorro la lista de objetos insertados o actualizados, dejando en un mapa solamente los últimos
        //que se insertaron o actualizaron de cada moneda
        Map<String, TipoCambio__c> mapTC = new Map<String, TipoCambio__c>();
        for(TipoCambio__c tc : news){
            mapTC.put(tc.CodigoDivisa__c, tc);
        }
        
        List<CurrencyTypeFacturacion__c> listaCurrencys = [SELECT Id, IsoCode__c, IsCorporate__c, IsActive__c, ConversionRate__c FROM CurrencyTypeFacturacion__c WHERE IsoCode__c IN :mapTC.keySet()];
        
        //actualizo los currencys que ya existen
        for(CurrencyTypeFacturacion__c ctf : listaCurrencys){
    
            TipoCambio__c copiatc = mapTC.remove(ctf.IsoCode__c);
            ctf.ConversionRate__c = copiatc.Factor__c;
            mapCurrencys.put(ctf.IsoCode__c, ctf);
            if (ctf.IsoCode__c == 'MXN'){
                ctf.IsCorporate__c = true;
            }else{
                ctf.IsCorporate__c = true;
            }
        }
        //creo nuevos currencys si es necesario
        for(TipoCambio__c tc : mapTc.values()){
            CurrencyTypeFacturacion__c ctf = new CurrencyTypeFacturacion__c();
            ctf.IsoCode__c = tc.CodigoDivisa__c;
            ctf.ConversionRate__c = tc.Factor__c;
            if (ctf.IsoCode__c == 'MXN'){
                ctf.IsCorporate__c = true;
            }else{
                ctf.IsCorporate__c = true;
            }
            mapCurrencys.put(ctf.IsoCode__c, ctf);
        } 
        
        //START CRM 10/05/2017 Solve queries With No Where Or Limit Clause  
        for(CurrencyType ct : [Select IsoCode, IsCorporate, Id, DecimalPlaces, ConversionRate,IsActive from CurrencyType WHERE IsoCode IN: mapCurrencys.keyset()]){			
            mapCurrencys.get(ct.IsoCode).IsCorporate__c = ct.IsCorporate;
            mapCurrencys.get(ct.IsoCode).IsActive__c = ct.IsActive;
        }
        /*******OLD 
        for(List<CurrencyType> listaCT : [Select IsoCode, IsCorporate, Id, DecimalPlaces, ConversionRate,IsActive from CurrencyType]){
            for(CurrencyType ct : listaCT){
                if(mapCurrencys.containsKey(ct.IsoCode)){
                    mapCurrencys.get(ct.IsoCode).IsCorporate__c = ct.IsCorporate;
                    mapCurrencys.get(ct.IsoCode).IsActive__c = ct.IsActive;
                }
            }
        }
        */  
        //END CRM 10/05/2017 Solve queries With No Where Or Limit Clause        
        
        upsert mapCurrencys.values();
    }   
}