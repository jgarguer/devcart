public with sharing class BI_BolsaEspecialMethods {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by BI_Bolsas_especiales__c Triggers 
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
	 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if BI_Solicitud_de_plan__c Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void blockEdition(List <BI_Bolsas_especiales__c> news){
        try{
            if(BI_GlobalVariables.stopTriggerDesc == false){
            	Set <Id> set_cat = new Set <Id>();
            	for(BI_Bolsas_especiales__c cat:news){        		
            		set_cat.add(cat.BI_Plan_Requerimiento_comercial__c);        		
            	}

            	List <BI_Solicitud_de_plan__c> lst_plan = [SELECT Id, BI_Estado_solicitud__c FROM BI_Solicitud_de_plan__c WHERE Id IN :set_cat];
            	for (BI_Solicitud_de_plan__c plan:lst_plan){
            		for(BI_Bolsas_especiales__c cat:news){
            			if (plan.Id == cat.BI_Plan_Requerimiento_comercial__c && 
                            (plan.BI_Estado_solicitud__c == label.BI_BolsaDineroCancelado ||
                             plan.BI_Estado_solicitud__c == 'Rechazado' ||
                             plan.BI_Estado_solicitud__c == 'Activado' || 
                             plan.BI_Estado_solicitud__c == 'Aprobado')){
            				cat.addError(label.BI_PlanBloqueado);
            			}	
            		}
            		
            	}
            }
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoModeloMethods.blockEdition', 'BI_EN', exc, 'Trigger');
        }
    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if BI_Solicitud_de_plan__c Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void blockDelete(List <BI_Bolsas_especiales__c> olds){
        try{
        	Set <Id> set_cat = new Set <Id>();
        	for(BI_Bolsas_especiales__c cat:olds){        		
        		set_cat.add(cat.BI_Plan_Requerimiento_comercial__c);        		
        	}

        	List <BI_Solicitud_de_plan__c> lst_plan = [SELECT Id, BI_Estado_solicitud__c FROM BI_Solicitud_de_plan__c WHERE Id IN :set_cat];
        	for (BI_Solicitud_de_plan__c plan:lst_plan){
        		for(BI_Bolsas_especiales__c cat:olds){
        			if (plan.Id == cat.BI_Plan_Requerimiento_comercial__c && 
                        (plan.BI_Estado_solicitud__c == label.BI_BolsaDineroCancelado ||
                         plan.BI_Estado_solicitud__c == 'Rechazado' ||
                         plan.BI_Estado_solicitud__c == 'Activado' || 
                         plan.BI_Estado_solicitud__c == 'Aprobado')){
        				cat.addError(label.BI_BolsaEspecialDelete);
        			}	
        		}
        		
        	}
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoModeloMethods.blockEdition', 'BI_EN', exc, 'Trigger');
        }
    }
}