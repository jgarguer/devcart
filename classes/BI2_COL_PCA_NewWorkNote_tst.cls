/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Rubén Suárez
    Company:        Avanxo
    Description:    Clase contenedora de pruebas unitarias para la clase controlador "BI2_COL_PCA_NewWorkNote_ctr"
    
    History:

    <Date>          <Author>            <Description>
    01/03/2017      Rubén Suárez        Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 
@isTest
private class BI2_COL_PCA_NewWorkNote_tst
{
	@isTest	static void testCrearWIRoD()
	{
        BI_TestUtils.throw_exception=false;
        //Creación del caso de prueba
        Case caso = new Case();
        caso.Type = 'Solicitud Incidencia Técnica';
        caso.RecordTypeId = '012w000000071OcAAI';//ID de Solicitud incidencia tecnica
        caso.Subject = 'Caso Prueba';
        caso.BI_Otro_Tipo__c = 'test';
        caso.BI_Country__c = 'Colombia';
        caso.BusinessHoursId = '01mw0000000DKmgAAG';
        caso.Status = 'Assigned';
        insert caso;    	

        Test.startTest();

    	PageReference pageRef = Page.BI2_COL_PCA_NewWorkNote;
    	Test.setCurrentPage(pageRef);
    	pageRef.getParameters().put('Idcaso', caso.Id);
    	ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
        BI2_COL_PCA_NewWorkNote_ctr newWorkNote = new BI2_COL_PCA_NewWorkNote_ctr(controller);
        newWorkNote.casoNota = 'nota';
        newWorkNote.nombre1 = 'uno';
        newWorkNote.nombre2 = 'dos';
        newWorkNote.nombre3 = 'tres';
        newWorkNote.adjunto1 =  Blob.valueOf('uno');
        newWorkNote.adjunto2 =  Blob.valueOf('dos');
        newWorkNote.adjunto3 =  Blob.valueOf('tres');
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerTicket'));
        newWorkNote.crearWIRoD();
        
        Test.stopTest();
	}
    @isTest
    static void testCrearWIRoDClosed()
    {
        BI_TestUtils.throw_exception=false;
        //Creación del caso de prueba
        Case caso = new Case();
        caso.Type = 'Solicitud Incidencia Técnica';
        caso.RecordTypeId = '012w000000071OcAAI';//ID de Solicitud incidencia tecnica
        caso.Subject = 'Caso Prueba';
        caso.BI_Otro_Tipo__c = 'test';
        caso.BI_Country__c = 'Colombia';
        caso.BusinessHoursId = '01mw0000000DKmgAAG';
        caso.Status = 'Cerrado';
        insert caso;        
        
        Test.startTest();

        PageReference pageRef = Page.BI2_COL_PCA_NewWorkNote;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Idcaso', caso.Id);
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
        BI2_COL_PCA_NewWorkNote_ctr newWorkNote = new BI2_COL_PCA_NewWorkNote_ctr(controller);
        newWorkNote.casoNota = 'nota';
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerTicket'));
        newWorkNote.crearWIRoD();
        
        Test.stopTest();
    }

}