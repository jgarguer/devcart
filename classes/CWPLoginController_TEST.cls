@isTest
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis
Company:       Salesforce.com
Description:   Test Class that manage coverage of PCA_NewCaseCtrl Class

History: 

<Date>                     <Author>                <Change Description>
4/3/2017                  Everis             Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class CWPLoginController_TEST {
    
    @isTest static void testLogin(){
                set <string> setStrings = new set <string>{
            'Account', 
                'Case',
                'NE__Order__c',
                'NE__OrderItem__c'
                };
                    //ACCOUNT
                    map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        /* 06/07/2017 */
        Id holdingRecordType = rtMapByName.get('TGS_Holding');
        Id customerCountryRecordType = rtMapByName.get('TGS_Customer_Country');
        Id legalEntityRecordType = rtMapByName.get('TGS_Legal_Entity');
        Id businessUnitRecordType = rtMapByName.get('TGS_Business_Unit');
        Id costCenterRecordType = rtMapByName.get('TGS_Cost_Center');
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Account holding
        Account ho = CWP_TestDataFactory.createHolding(holdingRecordType, 'Holding Acc');
        insert ho;
        Account cc = CWP_TestDataFactory.createCustomerCountry(customerCountryRecordType, 'Customer Country Acc', ho.Id);
        insert cc;
        Account le = CWP_TestDataFactory.createLegalEntity(legalEntityRecordType, 'Legal Entity Acc', ho.Id, cc.Id);
        insert le;
        Account bu = CWP_TestDataFactory.createBussinesUnit(businessUnitRecordType, 'Business Unit Acc', ho.Id, le.Id);
        insert bu;
        Account costC = CWP_TestDataFactory.createCostCenter(costCenterRecordType, 'Cost Center Acc', ho.Id, bu.Id, bu.Id, cc.Id, le.Id);
        insert costC;
        
        Contact con = CWP_TestDataFactory.createContact(bu.Id, 'ContactoPrueba');
        insert con;
        
        Profile prof = CWP_TestDataFactory.getProfile('TGS System Administrator');
        
        UserRole rol = CWP_TestDataFactory.createRole('Test Rol');
        //insert rol;
        
        User usuario = [SELECT Id,Name,userName,AccountId FROM User WHERE Profile.Name = 'Customer Community User Clone' OR Profile.Name = 'Customer Community Plus Custom' OR Profile.Name = 'BI_Customer Communities' OR Profile.Name = 'BI_Customer_Community_Plus' OR Profile.Name = 'TGS Customer Community' OR Profile.Name = 'TGS Customer Community Plus' OR Profile.Name = 'TGS_Customer_Community_Plus_Login' limit 1];
        
        //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        insert producto;
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
        insert catalogoItem;
        
                
        //CASO     
        list <Case> listaDeCasos = new list <Case>();                
        Case newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.contactId = con.id;        
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('TGS_Billing_Inquiry'), 'sub2', 'Assigned', 'Solicitud de alta');
        newCase.contactId = con.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('TGS_Change'), 'sub3', 'Assigned', 'Solicitud de alta');
        newCase.contactId = con.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('TGS_Complaint'), 'sub4', 'Assigned', 'Solicitud de alta');
        newCase.contactId = con.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('TGS_Problem'), 'sub5', 'Assigned', 'Solicitud de alta');
        newCase.contactId = con.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('TGS_Query'), 'sub6', 'Assigned', 'Solicitud de alta');
        newCase.contactId = con.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub7', 'Assigned', 'Solicitud de alta');
        newCase.contactId = con.id;                
        listaDeCasos.add(newCase);
        insert listaDeCasos;   
        
        
        // ORDER   
        NE__Order__c testOrder1= CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', le.id);
        testOrder1.Case__c = listaDeCasos[1].id;
        insert testOrder1;
        
        NE__Order__c testOrder2 = CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', le.id);
        testOrder2.Case__c = listaDeCasos[0].id;
        insert testOrder2;        
        
        // ORDER ITEM
        NE__OrderItem__c newOI;        
        newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder1.id, producto.id, catalogoItem.id,  1);
        insert newOI;       
        
        newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder2.id, producto.id, catalogoItem.id,  2);
        insert newOI;      
        
        NE__Family__c family = CWP_TestDataFactory.createFamily('corleone');
        insert family;
        
        NE__DynamicPropertyDefinition__c dynProp = CWP_TestDataFactory.createDynamiyProperty('dynamic');
        insert dynProp;
        
        NE__ProductFamilyProperty__c famProp = CWP_TestDataFactory.createFamilyProperty(family.id, dynProp.id); 
        famProp.TGS_Is_key_attribute__c = true;
        famProp.CWP_KeyValue__c = true;
        insert famProp;
        
        NE__Order_Item_Attribute__c oia1 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        insert oia1;
        
        NE__Order_Item_Attribute__c oia2 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        insert oia2;   
        
        String pass = 'Platino.8';
        String retorno = CWPLoginController.getlogin(usuario.Username,pass);
        pass = '';
        retorno = CWPLoginController.getlogin(usuario.Username,pass);
        retorno = CWPLoginController.getlogin('testFail2',pass);
        retorno = CWPLoginController.getlogin('testFail',pass);
        Boolean ret = CWPLoginController.getforgotPasswordApex(usuario.Username);
        ret = CWPLoginController.getforgotPasswordApex('testFail');
        ret = CWPLoginController.getforgotPasswordApex('testFail2');
        ret = CWPLoginController.getcheckUser('testFail');
        ret = CWPLoginController.getcheckPass('testFail');
        ret = CWPLoginController.getcheckUserForgotPassword('testFail');
        ret = CWPLoginController.getcheckUserForgotPassword('testFail2');

    }
    
}