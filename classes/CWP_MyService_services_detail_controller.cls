public without sharing class CWP_MyService_services_detail_controller {
    
    public static final String RECORD_TYPE_NAME= 'Incident';
    public static final String ORIGIN = 'Customer Web Portal';
    
    @AuraEnabled
    public static NE__OrderItem__c getOrderItem(String id) {
        NE__OrderItem__c item = 
            [select 
             NE__ProdId__r.TGS_CWP_Tier_1__c,
             NE__ProdId__r.TGS_CWP_Tier_2__c,
             NE__ProdId__r.Name,
             NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name, 
             NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name,
             NE__ProdName__c, NE__Service_Account__r.Name, NE__Billing_Account__r.Name,
             TGS_Service_status__c, NE__OrderId__r.Site__c, NE__Account__r.TGS_Aux_Holding__r.Name, NE__Account__r.Name,
             TGS_RFS_date__c, TGS_Provisional_RFS_date__c, TGS_Billing_end_date__c, Name from NE__OrderItem__c where Name = :id];
        return item;
    }
    
    public static Id getCaseIdFromNumber(String caso){
        Case item = 
            [select Id from Case where CaseNumber = :caso];
        return item.id;
    }
    
    @AuraEnabled
    public static String getAccountHierarchy(String id) {
        NE__OrderItem__c orig = 
            [select NE__Billing_Account__r.Name, NE__Billing_Account__r.ParentId from NE__OrderItem__c where Name = :id];
        String hierarchy = orig.NE__Billing_Account__r.Name;
        Id aux = orig.NE__Billing_Account__r.ParentId;
        while (aux != null){
            Account auxWhile =
                [select ParentId, Name from Account where id = :aux];
            hierarchy = auxWhile.Name + ' > ' + hierarchy;
            aux = auxWhile.ParentId;
        }        
        return hierarchy;
    }
    
    @AuraEnabled
    
    public static OiWrapper getDetailsAttributes(String id) {
        
        List<NE__Order_Item_Attribute__c[]> ret = new List<NE__Order_Item_Attribute__c[]>();
        Map<String,List<NE__Order_Item_Attribute__c>> oiAttsGroupByFamilyId=new Map<String,List<NE__Order_Item_Attribute__c>>();
        NE__Order_Item_Attribute__c[] items = 
            [SELECT 
             Name,
             NE__Order_Item__c,
             TGS_Status__c,
             TGS_Account_Id__c,
             TGS_Type_Order__c,
             NE__FamPropId__r.NE__FamilyId__c,
             NE__FamPropId__r.NE__FamilyId__r.Name,
             TGS_Key_Attribute__c,
             TGS_CSUID__c,
             NE__FamPropId__r.NE__PropId__r.NE__Description__c,
             NE__Value__c
             FROM
             NE__Order_Item_Attribute__c
             WHERE 
             TGS_Order_Item_Name__c =: id AND NE__Value__c!=null AND NE__FamPropId__r.TGS_Hidden_in_CWP__c=:false];
        for(NE__Order_Item_Attribute__c i : items){
            String fmKey=i.NE__FamPropId__r.NE__FamilyId__r.Name;
            if(oiAttsGroupByFamilyId.containsKey(fmKey))
                oiAttsGroupByFamilyId.get(fmKey).add(i);
            else {
                List<NE__Order_Item_Attribute__c> aux= new List<NE__Order_Item_Attribute__c>();
                aux.add(i);
                oiAttsGroupByFamilyId.put(fmKey, aux);
            }
        }
        OiWrapper retVar= new OiWrapper();        
        // Construimos la estructura que necesitamos en el componente como una matriz. 
        //  1-La primera lista hace referencia a los OIs que se muestran 
        //  2-La segunda es para cada recoger las familias de atributos
        //  3-La tercera hace referencia a los atributos.
        List<List<List<NE__Order_Item_Attribute__c>>> aux1= new List<List<List<NE__Order_Item_Attribute__c>>>();
        List<List<NE__Order_Item_Attribute__c>> aux2= new List<List<NE__Order_Item_Attribute__c>>(); 
        
        for (String field : oiAttsGroupByFamilyId.keySet()){
            aux2.add(oiAttsGroupByFamilyId.get(field));
        }
        aux1.add(aux2);
        retVar.OiList=aux1;
        return retVar;
    }
    
    @AuraEnabled 
    public static Boolean hasSecondLevel(String id) {
        NE__OrderItem__c[] itemId = 
            [SELECT Id FROM NE__OrderItem__c where Name = :id];
        NE__OrderItem__c[] attrs = 
            [SELECT TGS_Product_Name__c, NE__Parent_Order_Item__c FROM NE__OrderItem__c where NE__Parent_Order_Item__c =  :itemId.get(0).Id];
        
        
        return attrs.size() > 0; 
    }    
    
    @AuraEnabled
    public static OiWrapper getDetailsAttributes2(String id) {
        
        List<NE__Order_Item_Attribute__c[]> ret = new List<NE__Order_Item_Attribute__c[]>();
        
        NE__OrderItem__c[] names = 
            [SELECT Name FROM NE__OrderItem__c where NE__Parent_Order_Item__r.Name =: id];
        System.debug('@@ La lista de OI es'+ names);
        List<String>ids=new List<String>();
        
        for (NE__OrderItem__c n:names ){
            ids.add(n.Name);
            
        }
        
        
        
        NE__Order_Item_Attribute__c[] items = 
            [SELECT 
             Name,
             TGS_Order_Item_Name__c,
             NE__Order_Item__c,
             NE__Order_Item__r.TGS_Product_Name__c,
             TGS_Status__c,
             TGS_Account_Id__c,
             TGS_Type_Order__c,
             NE__FamPropId__r.NE__FamilyId__c,
             NE__FamPropId__r.NE__FamilyId__r.Name,
             TGS_Key_Attribute__c,
             TGS_CSUID__c,
             NE__FamPropId__r.NE__PropId__r.NE__Description__c,
             NE__Value__c
             FROM
             NE__Order_Item_Attribute__c
             WHERE NE__Value__c!=null AND NE__FamPropId__r.TGS_Hidden_in_CWP__c=:false AND TGS_Order_Item_Name__c IN: ids];
        
        List<List<List<NE__Order_Item_Attribute__c>>> oiList= new List<List<List<NE__Order_Item_Attribute__c>>>();
        List<List<NE__Order_Item_Attribute__c>> familyList= new List<List<NE__Order_Item_Attribute__c>>();
        List<NE__Order_Item_Attribute__c> oiAttList= new List<NE__Order_Item_Attribute__c>();
        
        Map<String,Map<String,List<NE__Order_Item_Attribute__c>>> oiMap=new Map<String,Map<String,List<NE__Order_Item_Attribute__c>>>();
        Map<String,List<NE__Order_Item_Attribute__c>> familyMap=new  Map<String,List<NE__Order_Item_Attribute__c>>();
        for (NE__Order_Item_Attribute__c at : items){
            // comprobamos si ya tenemos ese elemento en el mapa. 
            if(!oiMap.containsKey(at.TGS_Order_Item_Name__c)){
                //Lo insertamos si no existe. 
                oiMap.put(at.TGS_Order_Item_Name__c, new Map<String,List<NE__Order_Item_Attribute__c>>());
            }
            familyMap=oiMap.get(at.TGS_Order_Item_Name__c);
            //Comprobamos si tenemos la categoría
            if (familyMap.containsKey(at.NE__FamPropId__r.NE__FamilyId__r.Name)){
                //Metemos el atributo
                familyMap.get(at.NE__FamPropId__r.NE__FamilyId__r.Name).add(at);
            }else {
                //Inicializamos la lista y metemos el elemento.
                oiAttList= new List<NE__Order_Item_Attribute__c>();
                oiAttList.add(at);
                familyMap.put(at.NE__FamPropId__r.NE__FamilyId__r.Name, oiAttList);
            }
        }
        System.debug('oiMap->>'+ oiMap);
        //Construido el mapa montamos las listas 
        
        OiWrapper retVar= new OiWrapper();
        retVar.OiList=new List<List<List<NE__Order_Item_Attribute__c>>>();
        
        oiList= new List<List<List<NE__Order_Item_Attribute__c>>>();
        for (String oi :oiMap.keySet()){
            familyList= new List<List<NE__Order_Item_Attribute__c>>();
            oiAttList= new List<NE__Order_Item_Attribute__c>();
            for (String family :oiMap.get(oi).keySet()){
                familyList.add(oiMap.get(oi).get(family));
            }
            retVar.OiList.add(familyList); 
        }
        return retVar;
    }       
    
    @AuraEnabled
    public static NE__OrderItem__c[] getProductsNames(String id) {
        
        Integer i = 0;
        NE__OrderItem__c[] ret = new NE__OrderItem__c[]{};
            NE__OrderItem__c[] itemId = 
            [SELECT Id FROM NE__OrderItem__c where Name = :id];
        NE__OrderItem__c[] names = 
            [SELECT TGS_Product_Name__c, Name FROM NE__OrderItem__c where NE__Parent_Order_Item__c =  :itemId.get(0).Id order by TGS_Product_Name__c];
        
        // Recorre lista de OrderItem (names) y coge los nombres 
	List<String> attributesNames = new List<String>() ;
	for (NE__OrderItem__c attribute :names){
	    attributesNames.add(attribute.Name);
	}
	// Coger Order Item que tengan OrdenItemAttributes, cumpla la condicion... y esten en la lista
	NE__Order_Item_Attribute__c[] oiAttribute =
	    [SELECT TGS_Order_Item_Name__c FROM NE__Order_Item_Attribute__c WHERE NE__Value__c!=null AND NE__FamPropId__r.TGS_Hidden_in_CWP__c=:false AND TGS_Order_Item_Name__c IN: attributesNames];
        
        Set<String> listAttributes = new Set<String>();
	for(NE__Order_Item_Attribute__c attributesOi : oiAttribute){
            listAttributes.add(String.valueOf(attributesOi.TGS_Order_Item_Name__c));
        }
        List<NE__OrderItem__c> recorrOisHijos = new List <NE__OrderItem__c>();
        for(NE__OrderItem__c oi:names){
            if(listAttributes.contains(oi.Name))
                recorrOisHijos.add(oi);
        }
        return  recorrOisHijos;
    }
    
    @AuraEnabled
    public static OiWrapper getDetailsAttributes2Detailed(String id) {
        system.debug('recibo '+id);
        List<NE__Order_Item_Attribute__c[]> ret = new List<NE__Order_Item_Attribute__c[]>();
        List<String> parts = id.split(',');
        system.debug('parts--> '+parts);
        NE__Order_Item_Attribute__c[] items = 
            [SELECT 
             Name,
             TGS_Order_Item_Name__c,
             NE__Order_Item__c,
             NE__Order_Item__r.TGS_Product_Name__c,
             TGS_Status__c,
             TGS_Account_Id__c,
             TGS_Type_Order__c,
             NE__FamPropId__r.NE__FamilyId__c,
             NE__FamPropId__r.NE__FamilyId__r.Name,
             TGS_Key_Attribute__c,
             TGS_CSUID__c,
             NE__FamPropId__r.NE__PropId__r.NE__Description__c,
             NE__Value__c
             FROM
             NE__Order_Item_Attribute__c
             WHERE NE__Value__c!=null AND NE__FamPropId__r.TGS_Hidden_in_CWP__c=:false AND TGS_Order_Item_Name__c IN: parts];
        
        List<List<List<NE__Order_Item_Attribute__c>>> oiList= new List<List<List<NE__Order_Item_Attribute__c>>>();
        List<List<NE__Order_Item_Attribute__c>> familyList= new List<List<NE__Order_Item_Attribute__c>>();
        List<NE__Order_Item_Attribute__c> oiAttList= new List<NE__Order_Item_Attribute__c>();
        
        Map<String,Map<String,List<NE__Order_Item_Attribute__c>>> oiMap=new Map<String,Map<String,List<NE__Order_Item_Attribute__c>>>();
        Map<String,List<NE__Order_Item_Attribute__c>> familyMap=new  Map<String,List<NE__Order_Item_Attribute__c>>();
        for (NE__Order_Item_Attribute__c at : items){
            // comprobamos si ya tenemos ese elemento en el mapa. 
            if(!oiMap.containsKey(at.TGS_Order_Item_Name__c)){
                //Lo insertamos si no existe. 
                oiMap.put(at.TGS_Order_Item_Name__c, new Map<String,List<NE__Order_Item_Attribute__c>>());
            }
            familyMap=oiMap.get(at.TGS_Order_Item_Name__c);
            //Comprobamos si tenemos la categoría
            if (familyMap.containsKey(at.NE__FamPropId__r.NE__FamilyId__r.Name)){
                //Metemos el atributo
                familyMap.get(at.NE__FamPropId__r.NE__FamilyId__r.Name).add(at);
            }else {
                //Inicializamos la lista y metemos el elemento.
                oiAttList= new List<NE__Order_Item_Attribute__c>();
                oiAttList.add(at);
                familyMap.put(at.NE__FamPropId__r.NE__FamilyId__r.Name, oiAttList);
            }
        }
        System.debug('oiMap->>'+ oiMap);
        //Construido el mapa montamos las listas 
        
        OiWrapper retVar= new OiWrapper();
        retVar.OiList=new List<List<List<NE__Order_Item_Attribute__c>>>();
        
        oiList= new List<List<List<NE__Order_Item_Attribute__c>>>();
        for (String oi :oiMap.keySet()){
            familyList= new List<List<NE__Order_Item_Attribute__c>>();
            oiAttList= new List<NE__Order_Item_Attribute__c>();
            for (String family :oiMap.get(oi).keySet()){
                familyList.add(oiMap.get(oi).get(family));
            }
            retVar.OiList.add(familyList); 
        }
        return retVar;
    }   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García

Company:       everis

Description:   method called when Incident modal is called
History:
<Date>                          <Author>                                <Code>              <Change Description>
15/05/2017                      Julio Alberto Asenjo García                                 Initial version

*/
    
    @AuraEnabled
    public static string getPiciklistIncidence() {
        String result='';
        List<list <PickListItem>> resultLists= new List<list <PickListItem>>();
        list <PickListItem> impactList= new List <PickListItem>();
        list <PickListItem> urgencyList= new List <PickListItem>();
        Schema.DescribeFieldResult fieldResult = Case.TGS_Impact__c.getDescribe();
        for (Schema.PicklistEntry p: fieldResult.getPicklistValues()){
            impactList.add(new PickListItem(p.getValue(), p.getLabel()));
        }
        fieldResult = Case.TGS_Urgency__c.getDescribe();
        for (Schema.PicklistEntry p: fieldResult.getPicklistValues()){
            urgencyList.add(new PickListItem(p.getValue(), p.getLabel()));
        }
        
        resultLists.add(impactList);
        resultLists.add(urgencyList);
        result=JSON.serialize(resultLists);
        return result;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García

Company:       everis

Description:   method called to create a new incident case
History:
<Date>                          <Author>                                <Code>              <Change Description>
15/05/2017                      Julio Alberto Asenjo García                                 Initial version

*/
    
    @AuraEnabled
    public static string createCaseInc(list<String> params) {
        String result='FIN';
        String suId= params[0];
        String asunto=params[1];
        String descripc=params[2];
        String impacto=params[3];
        String urgencia=params[4];
        
        Id userId = UserInfo.getUserId();
        
        User usActive = [SELECT Id, AccountId, ProfileId, Profile.Name, Pais__c, ContactId, Contact.BI_Cuenta_activa_en_portal__c FROM User WHERE Id =: userId];
        Account currentUserAccount = [SELECT Id, RecordType.DeveloperName FROM Account WHERE Id =: usActive.Contact.BI_Cuenta_activa_en_portal__c];
        Set<Id> allAccountSet = new set<Id>();
        map<string, map<id, Account>> hierarchyAccMap = CWP_Helper.getAllAccountFromUser(currentUserAccount.Id);
        for(string i : hierarchyAccMap.keySet()){
            allAccountSet.addAll(hierarchyAccMap.get(i).keySet());
        }       
        
        Case mCase = new Case();
        mCase.RecordTypeId = TGS_Portal_Utils.getRecordType(RECORD_TYPE_NAME,'Case')[0].Id;
        mCase.AccountId= usActive.Contact.BI_Cuenta_activa_en_portal__c;
        try {
            BI_Punto_de_instalacion__c siteDummy = [SELECT Id FROM BI_Punto_de_instalacion__c WHERE BI_Cliente__c IN: allAccountSet LIMIT 1];
            mCase.TGS_Ticket_Site__c = siteDummy.Id;
        }catch(exception e){}
        mCase.TGS_Customer_Services__c = suId;
        initializeProductTiersBySID([SELECT NE__ProdId__c FROM NE__OrderItem__c WHERE Id=:suId].NE__ProdId__c, mCase);
        mCase.subject = asunto;
        mCase.description = descripc;
        mcase.TGS_Impact__c = impacto;
        mcase.TGS_Urgency__c = urgencia;
        mCase.origin = ORIGIN;
        //  getAccountIdAndSite(suId, mCase);
        System.debug('Vamos a insertar el caso:--<'+mCase);
        TGS_CallRodWs.inFutureContext= true;
        insert mCase;
        TGS_CallRodWs.inFutureContext= false;
        
        
        
        Case temp = [SELECT CaseNumber FROM Case WHERE Id =: mCase.Id LIMIT 1];
        return temp.CaseNumber;
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García

Company:       everis

Description:   method called to integrate a new incident case
History:
<Date>                          <Author>                                <Code>              <Change Description>
15/05/2017                      Julio Alberto Asenjo García                                 Initial version

*/
    
    @AuraEnabled
    public static String integraCaseInc(String cNumber) {
        Case mCase = [SELECT id FROM Case WHERE CaseNumber =: cNumber LIMIT 1];
        Set<Id> casesIdSet = New Set<Id>();
        casesIdSet.add(mCase.id);
        System.debug('\n\n\n INTEGRAMOS EL CASO \n\n\n');
        CWP_CallRoDWSFromCWP.invokeWebServiceRODFromCWP(casesIdSet, null, null, false);
        return cNumber;
        
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García

Company:       everis

Description:   method called to create a new modification case
History:
<Date>                          <Author>                                <Code>              <Change Description>
15/05/2017                      Julio Alberto Asenjo García                                 Initial version

*/
    
    @AuraEnabled
    public static string createCaseMod(list<String> params) {
        String result='FIN';
        String suId= params[0];
        String descripc=params[1];
        String returnPage = NE.JS_RemoteMethods.requestChangeAssetItem(suId, 'ChangeOrder');
        if (returnPage != 'KO') {
            system.debug('NE.JS_RemoteMethods.requestChangeAssetItem devuelve OK');
            
            String parameters = returnPage.substringAfter('?');
            String orderIdParameter = parameters.split('&').get(1);
            String orderId = orderIdParameter.split('=').get(1);
            
            System.debug('[PCA_Installed_Services_Controller.newModification] orderId: ' + orderId);
            NE__Order__c configuration = TGS_Portal_Utils.getOrder(orderId);
            
            Case createdCase = [SELECT Id, TGS_Customer_Services__c, Description FROM Case WHERE Id =: configuration.Case__c];
            createdCase.TGS_Customer_Services__c= suId;
            createdCase.Description = descripc;
            update createdCase;
            Case temp = new Case();
            temp = [SELECT CaseNumber FROM Case WHERE Id =: configuration.Case__c];
            result = temp.CaseNumber;
        }
        return result;
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García

Company:       everis

Description:   method called to create a new modification case
History:
<Date>                          <Author>                                <Code>              <Change Description>
15/05/2017                      Julio Alberto Asenjo García                                 Initial version

*/
    
    @AuraEnabled
    public static boolean showNewModificationButton(String id) {
        NE__OrderItem__c auxC= [SELECT NE__CatalogItem__c FROM NE__OrderItem__c WHERE Name=: id];
        NE__Catalog_Item__c CatIt  = [SELECT Id, NE__Catalog_Category_Name__c, NE__Technical_Behaviour__c, NE__Catalog_Category_Name__r.NE__Parent_Category_Name__c, NE__ProductId__c, NE__ProductId__r.Name, Name FROM NE__Catalog_Item__c WHERE id=:auxC.NE__CatalogItem__c];
        boolean showNewModificationButton;
        if(CatIt.NE__Technical_Behaviour__c != null){
            showNewModificationButton = !CatIt.NE__Technical_Behaviour__c.contains('Service not available for e-Ordering');
        }
        
        return showNewModificationButton;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García

Company:       everis

Description:   method called to Integrate workinfo
History:
<Date>                          <Author>                                <Code>              <Change Description>
15/05/2017                      Julio Alberto Asenjo García                                 Initial version

*/
    
    @AuraEnabled
    public static boolean integraWorkInfo(String attachId) {
        Attachment a = [SELECT ParentId FROM Attachment WHERE Id =:attachId];
        
        Set<Id> workInfosIdSet = New Set<Id>();
        
        workInfosIdSet.add(a.ParentId);
        System.debug('Se integra workInfosIdSet'+workInfosIdSet);
        CWP_CallRoDWSFromCWP.invokeRoDWSFromCWP(workInfosIdSet);
        
        return true;
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García

Company:       everis

Description:   method called to create a new modification case
History:
<Date>                          <Author>                                <Code>              <Change Description>
15/05/2017                      Julio Alberto Asenjo García                                 Initial version

*/
    
    @AuraEnabled
    public static boolean showNewIncidenceButton(String id) {
        System.debug('METHOD--showNewIncidenceButton');
        boolean showNewIncidenceButton;
        User theUser = [SELECT Id, AccountId, ContactId FROM User WHERE Id =:userinfo.getUserId()];
        Account acc= [SELECT HoldingId__c FROM Account WHERE id=:theUser.AccountId ];
        NE__OrderItem__c item = 
            [SELECT 
             NE__ProdId__r.TGS_CWP_Tier_1__c,
             NE__ProdId__r.TGS_CWP_Tier_2__c,
             NE__ProdId__r.Name,
             TGS_Service_status__c, NE__OrderId__r.Site__c, NE__Account__r.TGS_Aux_Holding__r.Name, NE__Account__r.Name,
             TGS_RFS_date__c, TGS_Provisional_RFS_date__c, TGS_Billing_end_date__c, Name 
             FROM NE__OrderItem__c WHERE Name = :id];
        
        
        String holdingName = [SELECT Name FROM Account WHERE Id =: acc.HoldingId__c LIMIT 1].Name;
        // Para poder coger el tier dependence primero necesito recuperar las categorias regularizadas del GME
        List<TGS_GME_GM_Product_Categorization__c> prodRegularizado = 
            [SELECT TGS_Categorization_tier_1__c, TGS_Categorization_tier_2__c, TGS_Categorization_tier_3__c FROM TGS_GME_GM_Product_Categorization__c WHERE TGS_Service__c =: item.NE__ProdId__r.Name LIMIT 1] ;       
        List<TGS_Categorization_Tier_Dependence__c> tierDep;
        if(prodRegularizado != null && !prodRegularizado.isEmpty()){
            tierDep = [SELECT Id FROM TGS_Categorization_Tier_Dependence__c WHERE TGS_RTDevName__c = 'TGS_Incident' AND TGS_Product_Tier_3__c =: prodRegularizado.get(0).TGS_Categorization_tier_3__c AND TGS_Holding_Name__c =: holdingName];
        }
        if(tierDep != null && !tierDep.isEmpty()){
            
            showNewIncidenceButton = true;
        }else{
            showNewIncidenceButton = false;
        }
        System.debug('METHOD--showNewIncidenceButton-->'+showNewIncidenceButton);
        return showNewIncidenceButton;
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García

Company:       everis

Description:   method called to fill the product tiers
History:
<Date>                          <Author>                                <Code>              <Change Description>
15/05/2017                      Julio Alberto Asenjo García                                 Initial version

*/
    private Static void initializeProductTiersBySID(String sid, Case mCase) {
        System.debug('initializeProductTiersBySID>sid>'+sid);
        NE__Catalog_Item__c[] catalogItems = TGS_Portal_Utils.getCatalogItemsBySId(sid);
        System.debug('initializeProductTiersBySID>catalogItems>'+catalogItems);
        if (catalogItems.size()>0){
            System.debug('Hay catalogItems');
            NE__Catalog_Item__c catalogItem = catalogItems[0];
            mCase.TGS_Product_Tier_1__c = catalogItem.NE__ProductId__r.TGS_CWP_Tier_1__c;
            mCase.TGS_Product_Tier_2__c = catalogItem.NE__ProductId__r.TGS_CWP_Tier_2__c;
            mCase.TGS_Product_Tier_3__c = catalogItem.NE__ProductId__r.Name;
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García

Company:       everis

Description:   method called to fill accountId
History:
<Date>                          <Author>                                <Code>              <Change Description>
15/05/2017                      Julio Alberto Asenjo García                                 Initial version

*/
    public Static void  getAccountIdAndSite(String suid, Case mCase) {
        
        NE__OrderItem__c currentService = [SELECT Id, Name, NE__Billing_Account__c, NE__Billing_Account__r.Name, NE__Billing_Account__r.Parent.Name, NE__Billing_Account__r.Parent.Parent.Name, NE__Billing_Account__r.Parent.Parent.Parent.Name, NE__Billing_Account__r.Parent.Parent.Parent.Parent.Name, Installation_point__c, Installation_point__r.Name FROM NE__OrderItem__c WHERE Id =: suid];
        system.debug('Installation point: ' + currentService.Installation_point__c + ' , ' + currentService.Installation_point__r.Name + '.');
        
        
        if (currentService.Installation_point__c != '' && currentService.Installation_point__c != null) {
            mCase.TGS_Ticket_Site__c = currentService.Installation_point__c;
        }
        System.debug('level 5 Cost Center '+ currentService.NE__Billing_Account__c);
        System.debug('level 4 Business Unit '+ currentService.NE__Billing_Account__r.ParentId);
        System.debug('level 3 Legal Entity '+ currentService.NE__Billing_Account__r.Parent.ParentId);
        System.debug('level 2 Customer Country '+ currentService.NE__Billing_Account__r.Parent.Parent.ParentId);
        System.debug('level 1  Holding'+ currentService.NE__Billing_Account__r.Parent.Parent.Parent.ParentId);
        
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
*
* Author:        Eduardo Trujillo
*
* Company:       everis
*
* Description:   Method to set File data to a Case number
* History:
* <Date>                          <Author>                                <Code>              <Change Description>
* 23/05/2017                      Eduardo Trujillo                                              Initial version
*/    
    @AuraEnabled
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType, String caso) { 
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = new Attachment();
        a.parentId = parentId;
        
        a.Body = EncodingUtil.base64Decode(base64Data);
        a.Name = fileName;
        a.ContentType = contentType;
        system.debug('Datos insert 2: '+parentId);
        system.debug('Datos insert 2: '+fileName);        
        system.debug('Datos insert 2: '+base64Data);        
        system.debug('Datos insert 2: '+contentType);        
        system.debug('Datos insert 2: '+caso);
        TGS_CallRodWs.inFutureContextAttachment= true;
        insert a;
        TGS_CallRodWs.inFutureContextAttachment= false;
        
        
        return a.Id;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
*
* Author:        Eduardo Trujillo
*
* Company:       everis
*
* Description:   Method to set File data to a Case number
* History:
* <Date>                          <Author>                                <Code>              <Change Description>
* 23/05/2017                      Eduardo Trujillo                                              Initial version
*/    
    @AuraEnabled
    public static Id saveTheChunk(String fileName, String fileDescription, String base64Data, String contentType, String fileId, String caso) { 
        system.debug('Datos insert: '+fileName);        
        system.debug('Datos insert: '+base64Data);        
        system.debug('Datos insert: '+contentType);        
        system.debug('Datos insert: '+caso);
        system.debug('Datos insert: '+fileId);
        
        if (fileId == '') {
            //crear workinfo-> el parent Id cambiarlo por el id del workinfo
            TGS_Work_Info__c wi = new TGS_Work_Info__c();
            wi.TGS_Case__c = getCaseIdFromNumber(caso);
            wi.TGS_Description__c=fileDescription;
            wi.TGS_Public__c = true;
            TGS_CallRodWs.inFutureContextWI= true;
            insert wi;
            TGS_CallRodWs.inFutureContextWI= false;
            //fileId = saveTheFile(parentId, fileName, base64Data, contentType, caso);
            fileId = saveTheFile(wi.Id, fileName, base64Data, contentType, caso);
        } else {
            appendToFile(fileId, base64Data);
        }
        
        return Id.valueOf(fileId);
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
*
* Author:        Eduardo Trujillo
*
* Company:       everis
*
* Description:   Method to set File data to a Case number
* History:
* <Date>                          <Author>                                <Code>              <Change Description>
* 23/05/2017                      Eduardo Trujillo                                              Initial version
*/    
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = [
            SELECT Id, Body
            FROM Attachment
            WHERE Id = :fileId
        ];
        
        String existingBody = EncodingUtil.base64Encode(a.Body);
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data); 
        TGS_CallRodWs.inFutureContextAttachment= true;
        update a;
        TGS_CallRodWs.inFutureContextAttachment= false;
        
    }
    
    
    public class PickListItem {
        public String label {get; set;}
        public String value {get; set;}   
        
        public  PickListItem(String value,String label ){
            this.label=label;
            this.value=value;
        }  
        
    }
    /**
* Class:            oiWrappe
*
* Description:      Clase wrapper para los resultados de la tabla del detalle de MyService.  
*/
    
    public class OiWrapper{
        @AuraEnabled
        public List<List<List<NE__Order_Item_Attribute__c>>> OiList {get; set;}
        
        
    }
}