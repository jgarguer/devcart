public class BI_COL_LiberationApproval_ctr {
    
    public Case casoPM{get;set;}
    public String observacion{get;set;}
    public boolean verFormulario{get;set;}
    public boolean verBoton{get;set;}
    public String strSenderName='asevilla@avanxo.com';
    public String accion=''; 
    
    public BI_COL_LiberationApproval_ctr(ApexPages.Standardcontroller controller){
        
        verBoton=true;
        Case casoPM2=null;      
            
        system.debug('id a buscar --------->'+ApexPages.currentPage().getParameters().get('Id'));
        
        this.casoPM=[select Id, OwnerId, Owner.Email, Owner.Name, /*BI_Nombre_de_la_Oportunidad__r.BI_COL_UniqueId__c,*/BI_Nombre_de_la_Oportunidad__r.Name, BI_Nombre_de_la_Oportunidad__r.BI_Numero_id_oportunidad__c,
                            BI_Nombre_de_la_Oportunidad__r.Account.Name, BI_Nombre_de_la_Oportunidad__r.owner.Email, 
                            BI_Estado_SLA__c,BI_COL_Aprobacion_liberacion__c,BI_ECU_Descripcion_problema_voz__c
                            from  case  where RecordType.DeveloperName = 'BI_Caso_Interno' and Id=:ApexPages.currentPage().getParameters().get('Id') limit 1];
       
       system.debug('consulta de registro --------->'+casoPM); 
        
       if(ApexPages.currentPage().getParameters().get('accion')!=null){
        
            accion=ApexPages.currentPage().getParameters().get('accion');
            
            if(accion.equals('cancelar')){
                verFormulario=true;
            }else{
                verFormulario=false;
            }
       }
                    
    }
    
    public BI_COL_LiberationApproval_ctr(String IdCaso){
        
        this.casoPM=[select Id, OwnerId, Owner.Email, Owner.Name/*, BI_Nombre_de_la_Oportunidad__r.BI_COL_UniqueId__c*/,BI_Nombre_de_la_Oportunidad__r.Name, BI_Nombre_de_la_Oportunidad__r.BI_Numero_id_oportunidad__c,
                    BI_Nombre_de_la_Oportunidad__r.Account.Name, BI_Nombre_de_la_Oportunidad__r.owner.Email,BI_COL_Aprobacion_liberacion__c,BI_ECU_Descripcion_problema_voz__c 
                    from Case where RecordType.DeveloperName = 'BI_Caso_Interno' and Id=:IdCaso limit 1];
    }
    
    public PageReference actualizarCaso(){  
            
        boolean redireccionar=true;
        system.debug('accion==='+accion);
        
        if(accion.equals('aceptar')){
            
            system.debug('entra a aceptar');
            this.casoPM.BI_COL_Estado_Aceptacion__c='Aceptado';
            correoAceptacion();
            update casoPM;
             
        }else{
            system.debug('entra a cancelar');
            return null;
        }
        
        system.debug('entra a metodo');
        PageReference paginaInicio=null;
        
        if(redireccionar){
            paginaInicio=volver();
        } 
        
        return paginaInicio;
    }
    
    public PageReference volver(){
        
        system.debug('--retornando a-->'+'/'+this.casoPM.Id);
        PageReference paginaInicio= new PageReference('/'+this.casoPM.Id);
        paginaInicio.setRedirect(true);
         
        return paginaInicio;
    }
    
    
    public void correoAceptacion(){
                
        List<String> strMailPARA = new List<String>();
        // envio Individual de correo a cada dueño de oportunidad del registro que ha sido afectado
        
        if(casoPM.owner.Email!=null){
            
            strSenderName = casoPM.owner.Email;
            strMailPARA.add(casoPM.BI_Nombre_de_la_Oportunidad__r.owner.Email);
            +
            
            system.debug('------ correoAceptacion strMailPARA------->'+strMailPARA);
            
            String strMessage = '<table><tr><td><img height="85" width="156" src="'+Label.BI_COL_lblUrlImageTelefonica+'"></td></tr></table>';
            
            strMessage+='<br><br><table border="1" style="border-style: ridge">'+
                                '<tr><td style="background-color:#084B8A;color:white"><b>Nombre del Project Manager</b></td><td>'+casoPM.Owner.Name+'</td></tr>'+
                                '<tr><td style="background-color:#084B8A;color:white"><b>Consecutivo Oportunidad</b></td><td>'+casoPM.BI_Nombre_de_la_Oportunidad__r.BI_Numero_id_oportunidad__c+'</td></tr>'+
                                '<tr><td style="background-color:#084B8A;color:white"><b>Nombre Oportunidad</b></td><td>'+casoPM.BI_Nombre_de_la_Oportunidad__r.Name+'</td></tr>'+
                                '<tr><td style="background-color:#084B8A;color:white"><b>Cliente</b></td><td>'+casoPM.BI_Nombre_de_la_Oportunidad__r.Account.Name+'</td></tr>';
            strMessage+='</table>';
                        
            strMessage = strMessage+'<br></br><br></br>Para mayor información dirijase a Salesforce:<br></br>';
            strMessage = strMessage+'Gracias <br></br><br></br>';
                        
            String strSubject='Caso de Asignación Project Manager aceptado';
            BI_COL_EmailUseful_cls emailUtil= new BI_COL_EmailUseful_cls(strMailPARA);
            EmailUtil.htmlBody(strMessage)
                    .senderDisplayName(strSenderName)
                    .subject(strSubject)
                    .useSignature(false);       
            
            System.debug(strMailPARA+'   '+strMessage+'<><><>'+strSubject);
            if(EmailUtil.sendEmail())
            {               
                system.debug('----- Correo Asignación enviado Satisfactoriamente ----');
                }else{
                system.debug('----- Correo No se pudo enviar ----');
            }
        }
        
                
    }
    
    public void correoCancelacion(){
        
        List<BI_COL_UsuariosCorreo__c> lstCorreosCF = [select Id_Usuario__c,Email__c from BI_COL_UsuariosCorreo__c where Area__c = 'Oficina Proyectos'];
        List<String> strMailPARA = new List<String>();
        
        system.debug('------ingresa metodo de cancelacion------->');
        casoPM.BI_ECU_Descripcion_problema_voz__c = observacion;
        update casoPM;
        system.debug('------actualiza caso------->'+casoPM);
        // envio de correo de cancelación del caso al dueño del registro  
        strSenderName = casoPM.Owner.Email;
        strMailPARA.clear();
        strMailPARA.add(casoPM.BI_Nombre_de_la_Oportunidad__r.owner.Email);
        
        
        for(BI_COL_UsuariosCorreo__c lst: lstCorreosCF)
            strMailPARA.add(lst.Email__c);
        
        system.debug('------strMailPARA PM------->'+strMailPARA);
        String strMessage = '<table><tr><td><img height="85" width="156" src="'+Label.BI_COL_lblUrlImageTelefonica+'"></td></tr></table>';
        
        strMessage='<br></br><br></br>Caso: '+casoPM.CaseNumber+'<br></br><br></br><br></br>Buen día:<br></br><br></br>Se ha devuelto la asignación para una oportunidad :'; 
        
        
        strMessage+='<br><br><table border="1" style="border-style: ridge">'+
                                '<tr><td style="background-color:#084B8A;color:white"><b>Nombre del Project Manager</b></td><td>'+casoPM.Owner.Name+'</td></tr>'+
                                '<tr><td style="background-color:#084B8A;color:white"><b>Consecutivo Oportunidad</b></td><td>'+casoPM.BI_Nombre_de_la_Oportunidad__r.BI_Numero_id_oportunidad__c+'</td></tr>'+
                                '<tr><td style="background-color:#084B8A;color:white"><b>Cliente</b></td><td>'+casoPM.BI_Nombre_de_la_Oportunidad__r.Account.Name+'</td></tr>'+
                                '<tr><td style="background-color:#084B8A;color:white"><b>Nombre Oportunidad</b></td><td>'+casoPM.BI_Nombre_de_la_Oportunidad__r.Name+'</td></tr>'+
                                '<tr><td style="background-color:#084B8A;color:white"><b>Motivo</b></td><td>'+casoPM.BI_ECU_Descripcion_problema_voz__c+'</td></tr>';
        strMessage+='</table>';
        
        strMessage = strMessage+'<br></br><br></br><br></br>Para mayor información dirijase a Salesforce:<br></br>';
        strMessage = strMessage+'Gracias <br></br><br></br>';

        system.debug('------strMessage PM------->'+strMessage); 
        
        String strSubject='Caso de Asignación Project Manager no aceptado';
        BI_COL_EmailUseful_cls EmailUtil= new BI_COL_EmailUseful_cls(strMailPARA);
        EmailUtil.htmlBody(strMessage)
                .senderDisplayName(strSenderName)
                .subject(strSubject)
                .useSignature(false);       
        
        System.debug(strMailPARA+'>>>>'+strMessage+'>>>>'+strSenderName+'>>>>'+strSubject);
        if(EmailUtil.sendEmail()) 
        {            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Correo enviado Satisfactoriamente'));   
            verBoton=false;
            
            casoPM.BI_COL_Estado_Aceptacion__c='';
            if(!Test.isRunningTest())
            {
                casoPM.OwnerId=lstCorreosCF[0].Id_Usuario__c;
            }
            
            update casoPM;
            
            }else{
                
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'El correo no pudo ser enviado a su destinatario, favor contactar al administrador del sistema'));
            system.debug('----- Correo No se pudo enviar ----');
        }
               
    }
    
    
    /*
    static testMethod void myUnitTest2() {
        Account cliente = new Account(Name ='NEXTANT SUCURSAL COLOMBIA',
                                    Tipo_Id__c= 'NIT',
                                    Identificaci_n__c = '000000001-1',
                                    Tipo__c = 'Activo',
                                    Aplica_LD__c = true,
                                    Cartera__c = 'OK',
                                    Naturaleza__c = 'JURIDICA',
                                    Marcaci_n_del_cliente__c = '3',
                                    Segmento_Telef_nica__c = 'EMPRESAS',
                                    Sector_Telef_nica__c = 'CORPORATIVO',
                                    Subsegmento_Telef_nica__c = 'FINANCIERO',
                                    Subsector_Telef_nica__c = 'BANCA',
                                    Actividad_Telef_nica__c = 'BANCA COMERCIAL PRIVADA',
                                    Description = 'Nextant',
                                    Gerencia_Comercial__c = 'JOSE LUIS RODRIGUEZ GARCIA',
                                    Mercado_Objetivo__c = true,
                                    Jefatura_Comercial__c = 'FEDERICO PELAEZ',
                                    Pais__c = 'Colombia',
                                    Phone = '13450280');
                                    
        insert cliente;
        
        
        Opportunity oppty = new Opportunity(
                                            Name = 'Oportunidad prueba ',
                                            AccountId = cliente.id,
                                            Prioridad__c = '3',
                                            CloseDate = System.today().addDays(7),
                                            StageName = 'F5 - En comentarios o En Estructuración');
        insert oppty;
        
        User inge=[select Id from User where profile.Name='Ingeniero' and isactive=true limit 1];
        User jefe=[select Id from User where profile.Name='Jefe de Ingeniería' and isactive=true limit 1];
        
        User x=new User(FirstName='UserABDC', LastName='basd', Alias='bcdert', CommunityNickname='bujeyeh', Email='b@xxx.com',EmailEncodingKey='ISO-8859-1',LanguageLocaleKey='es',
        LocaleSidKey='es',TimeZoneSidKey='America/Bogota',Username='bbbbaaaayyuutyb@kkkijukb.com',ProfileId='00e30000000lpYC', IsActive=true);
        insert x;
        system.debug('---------insertando usuario x------->'+x);
        
        Usuarios_Correos__c cu=new Usuarios_Correos__c();
        cu.Name='Avanxo';
        cu.area__c='Oficina Proyectos';
        cu.mail__c='avanxo@avanxo.com';
        cu.Id_Usuario__c=x.Id;
        insert cu;
        
        Casos_Project_Manager__c cpm=new Casos_Project_Manager__c();
        cpm.Aprobacion_Liberacion__c=false;
        cpm.Oportunidad__c=oppty.Id;
        cpm.Project_Manager__c=inge.Id;
        cpm.OwnerId=UserInfo.getUserId();
        
        insert cpm;
        
        ApexPages.Standardcontroller controller;
        ApexPages.currentPage().getParameters().put('accion','aceptar');        
        ApexPages.currentPage().getParameters().put('id',cpm.Id);
        AprobacionLiberacion_ctr controlador= new AprobacionLiberacion_ctr(controller);
        AprobacionLiberacion_ctr controlador2= new AprobacionLiberacion_ctr(cpm.Id);
        controlador.actualizarCaso();
        controlador.volver();
        ApexPages.currentPage().getParameters().put('accion','cancelar');
        controlador= new AprobacionLiberacion_ctr(controller);
        controlador.correoCancelacion();
    }*/

}