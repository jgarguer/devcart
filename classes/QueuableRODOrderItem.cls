public class QueuableRODOrderItem implements Queueable, Database.AllowsCallouts
{
	public Set<Id> confitemsIds;
    
    public QueuableRODOrderItem(Set<Id> cItemsId)
    {
        confitemsIds	=	cItemsId;
    }
    
    public void execute(QueueableContext context) 
    {
        system.debug('Call integration with confs id: '+confitemsIds.size());
        
        TGS_CallRodWs.invokeWebServiceRODOrderItem(confitemsIds, false); 
    }
}