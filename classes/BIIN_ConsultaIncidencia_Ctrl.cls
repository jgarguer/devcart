public class BIIN_ConsultaIncidencia_Ctrl extends BIIN_BaseController
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Ignacio Morales Rodríguez, Jose María Martín Castaño
	Company:       Deloitte
	Description:   Clase para consultar las Incidencias Técnicas disponibles en Remedy

	History:

	<Date>           <Author>        							  	            <Description>
	10/12/2015       Ignacio Morales Rodríguez, Jose María Martín     	        Initial version
    06/06/2016       Julio Laplaza                                              onClickBuscar: //caso2.Id = r.BAO_CaseNumberID;
    01/07/2016       José Luis González Beltrán                                 Fix translations when retrieving data
    04/07/2016       José Luis González Beltrán                                 Set default values for Status & Priority
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public List<Case> lcases2 {get;set;}
    public Case caso {get;set;}
    public Case caso2 {get;set;}
    public String case_detailId {get;set;}
    public Case casoConsulta{get;set;}
    public BIIN_Listado_Ticket_WS.TicketSalida salida {get;set;}
    public integer ContadorCasos {get;set;}
		public Boolean CpOcultar {get;set;}

    // public boolean displayPopUp {get;set;}
    // public String errorMessage {get;set;}

    private transient HttpRequest req;
    public HttpRequest getobjHttpRequest () {
        return req;
    }
    private transient HttpResponse res;
    public HttpResponse getHttpResponse () {
        return res;
    }

    public BIIN_ConsultaIncidencia_Ctrl(ApexPages.StandardSetController controller)
    {
				CpOcultar=getOcultarCampo();
				ContadorCasos = 1;
        displayPopupGeneric = false;
        controller.setPageSize(10);
        caso = new Case();
        caso.Status = '';
        caso.Priority = '';
        caso2 = new Case();
        lcases2 = new List<Case> ();
        casoConsulta = new Case();
        String tipoRegistroId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BIIN_Incidencia_Tecnica' limit 1].Id;
        caso.recordtypeid = tipoRegistroId  ;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función que a través del número de caso (Apex Param) lleva al detalle del mismo

	    History:

	    <Date>            <Author>          			<Description>
	    12/12/2015     Ignacio Morales Rodríguez    	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public PageReference printView() {
        system.debug('case_detailId ---> ' + case_detailId);
        return new PageReference ('/'+ case_detailId);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Jose María Castaño
	    Company:       Deloitte
	    Description:   Función que obtiene las incidencias de Remedy.

	    History:

	    <Date>            <Author>          			<Description>
	    12/12/2015     Jose María Castaño 			   	Initial version
        07/06/2016      Julio Laplaza                   try/catch error handling
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public void onClickBuscar()
    {
        try
        {
            BIIN_Obtener_Token_BAO ot = new BIIN_Obtener_Token_BAO();
            BIIN_Listado_Ticket_WS lts = new BIIN_Listado_Ticket_WS();
            String authorizationToken = ''; // ot.obtencionToken();
            system.debug('Fecha inicial y final de busqueda -->' + caso.BIIN_FechaInicial__c + caso.BIIN_FechaFinal__c);
            BIIN_Listado_Ticket_WS.TicketSalida salida = lts.listadoTicket(authorizationToken, caso, caso.BIIN_FechaInicial__c, caso.BIIN_FechaFinal__c);

            system.debug('[CONSULTA INCIDENCIA] El listado del Ticket es: '+salida);
            if(Test.isRunningTest())
            {
                string incidentNumber = '1234';
                String BAO_CaseNumberID = '500g000000Dv8P3';
                String Company  = 'ACME';
                String Priority = 'High';
                String reportedDate = '1446486766';
                String reportedSource = 'Portal Platino';
                String Site = 'LIMA';
                String HPDCI = 'dea';
                String Description = 'Descr';
                String assignedGroup = 'Support';
                String SLMStatus = 'sml';
                String Status = 'Assigned' ;
                BIIN_Listado_Ticket_WS.Output Output = new BIIN_Listado_Ticket_WS.Output();
                Output.incidentNumber = incidentNumber;
                Output.BAO_CaseNumberID = BAO_CaseNumberID;
                Output.Company = Company;
                Output.Priority = Priority;
                Output.reportedDate = reportedDate;
                Output.reportedSource = reportedSource;
                Output.Site = Site;
                Output.HPDCI = HPDCI;
                Output.Description = Description;
                Output.assignedGroup = assignedGroup;
                Output.SLMStatus = SLMStatus;
                Output.Status = Status;
                List<BIIN_Listado_Ticket_WS.Output> listOuput = new List<BIIN_Listado_Ticket_WS.Output>();
                listOuput.add(Output);
                BIIN_Listado_Ticket_WS.Outputs Outputs = new BIIN_Listado_Ticket_WS.Outputs();
                Outputs.output = listOuput;
                BIIN_Listado_Ticket_WS.TicketSalida salidaTest = new BIIN_Listado_Ticket_WS.TicketSalida();
                salidaTest.Outputs = Outputs;
                salida = salidaTest;
            }

            if(salida != null)
            {
                    //Cambiar cuando la salida sea incorrecta
                    List<String> lstring = new List<String>();
                    lcases2 = new List<Case>();
                    for (BIIN_Listado_Ticket_WS.Output r : salida.outputs.output)
                    {
                        caso2.TGS_SLA_Light__c = r.SLMStatus;
                        caso2.BI_Id_del_caso_Legado__c = r.incidentNumber;
                        lstring.add(r.incidentNumber);
                        // caso2.Id = r.BAO_CaseNumberID;
                        caso2.Description = r.Company;
                        caso2.Priority = r.Priority;
                        caso2.BI_COL_Fecha_Radicacion__c  = DateTime.newInstance(Long.valueOf(r.reportedDate)*1000);
                        if(!Test.isRunningTest())
                        {
                            caso2.LastModifiedDate = DateTime.newInstance(Long.valueOf(r.lastModifiedDate)*1000);
                        }
                        caso2.BIIN_Site__c = r.Site;
                        caso2.BIIN_Id_Producto__c = r.HPDCI;
                        caso2.Subject = r.Description;
                        caso2.Status = r.Status;
                        caso2.BI_Assigned_Group__c = r.assignedGroup;
                        lcases2.add(caso2);
                        caso2 = new Case();
                    }

                List<case> lcaseNumberId = new List<case>();
                lcaseNumberId = [SELECT Id, caseNumber, BI_Id_del_caso_Legado__c FROM case WHERE BI_Id_del_caso_Legado__c IN :lstring];
                Map<String, String> mapId = new Map<String, String>();
                for (Case caso : lcaseNumberId)
                {
                    mapId.put(caso.BI_Id_del_caso_Legado__c, caso.Id);
                }

                for(Case caso : lcases2)
                {
                    caso.Id = mapId.get(caso.BI_Id_del_caso_Legado__c);
                }
                system.debug('LISTA ----> ' + lcaseNumberId);
            }

            if(lcases2.isEmpty())
            {
                displayPopupGeneric = true;
                errorMessageGeneric = Label.BIIN_NO_RESULTS_MODIFY_CRITERIA;
            }
        }
        catch(Exception ex)
        {
            displayPopupGeneric = true;
            errorMessageGeneric = ex.getMessage();
        }
    }
		/*******************************************************************************************************
		* @Author:        Carlos Carvajal
		* @Company:       Avanxo
		* @description Retorna falso si el usuario es de colombia de lo contrario verdadero
		* @return retorna falso si el pais del usuario es colombia de lo contrario retorna verdadero
		*/
		public static Boolean getOcultarCampo()
		{
			User us=[Select id,Pais__c from User where id=:UserInfo.getUserId()];
			System.debug(' \n\n us.Pais__c = '+ us.Pais__c +' \n\n');
			if(us.Pais__c=='Colombia')
			{
				return false;
			}
			else
			{
				return true;
			}
		}
}