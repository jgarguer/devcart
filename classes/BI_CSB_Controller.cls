/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Class containing methods to be used from buttons in order to create, update, suspend accounts and create orders in CSB/NEC platform
    
    History:
    
    <Date>            <Author>              <Description>
    18/11/2015        Fernando Arteaga      Initial version
    09/08/2016		  Antonio Pardo 		Changed with sharing for without sharing
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public without sharing class BI_CSB_Controller
{
	private String accId {get; set;}
	private String fromAccount {get; set;}
	private String orderId {get; set;}
	private Integer numIntentos {get; set;}
	private Integer numIntentosOrd {get; set;}
	public Boolean showExit {get; set;}
	private Integer maxIntentos {get; set;}
	private Account acc {get; set;}
	private NE__Order__c ord {get; set;}
	
	public BI_CSB_Controller()
	{
		if (BI_CSB_WS_Retry__c.getAll() != null)
		{
			List<BI_CSB_WS_Retry__c> listAux = BI_CSB_WS_Retry__c.getAll().values();
			this.maxIntentos = listAux[0].BI_CSB_Reintentos__c != null ? listAux[0].BI_CSB_Reintentos__c.intValue() : 5;
		}
		if (this.maxIntentos == null)
			this.maxIntentos = 5;
			
		this.numIntentos = 0;
		this.numIntentosOrd = 0;
		this.showExit = false;
		this.accId = ApexPages.currentPage().getParameters().get('id');
		this.orderId = ApexPages.currentPage().getParameters().get('orderId');
		this.fromAccount = ApexPages.currentPage().getParameters().get('fromAccount');
		List<Account> lstAcc = [SELECT Id, Name, BI_Id_CSB__c FROM Account WHERE Id = :this.accId];
		if (!lstAcc.isEmpty())
		{
			this.acc = lstAcc[0];
			List<NE__Order__c> lstOrd = [SELECT Id, Name, BI_Id_legado__c, NE__OrderStatus__c, BI_CSB_Descripcion_Errores__c
										FROM NE__Order__c
										WHERE Id = :this.orderId];
			if (!lstOrd.isEmpty())
				this.ord = lstOrd[0];
		}
	}
	private void upsertTransaction(String status, String description, String numIntentos, Id recordId, String recordName, String action)
	{
		BI_Log__c tr = new BI_Log__c();
		tr.BI_CSB_Record_Id__c = recordId;
		tr.BI_Asunto__c = Label.BI_CSB_Transaccion;
		tr.BI_Descripcion__c = action + recordName + ' | Id ' + recordId;
		tr.BI_COL_Estado__c = status;
		tr.Descripcion__c = description;
		tr.BI_Tipo_Error__c = numIntentos;
		upsert tr;
	}

	private void updateAccount(String idCSB)
	{
        this.acc.BI_Id_CSB__c = idCSB;
        this.acc.BI_COL_Estado__c = Label.BI_CSB_Activo;
        update this.acc;
	}
/*
	private void updateAccountStatus(String status, String transactionId, String errorText)
	{
		List<Account> listAccount = [SELECT Id, BI_CSB_TransactionId__c
									 FROM Account
									 WHERE BI_Id_CSB__c = :this.accExternalId];
		
		if (!listAccount.isEmpty())
		{
			listAccount[0].BI_CSB_TransactionId__c = transactionId;
			update listAccount[0];
		}
	}
*/

	private void updateOrder(String status, String transactionId, String errorText)
	{
		ord.BI_Id_legado__c = transactionId;
		ord.NE__OrderStatus__c = status;
		ord.BI_CSB_Descripcion_Errores__c = errorText;
		update ord;
		
		List<NE__OrderItem__c> listOrderItems = [SELECT Id, NE__Status__c FROM NE__OrderItem__c WHERE NE__OrderId__c = :this.orderId FOR UPDATE];
		for (NE__OrderItem__c oi: listOrderItems)
			oi.NE__Status__c = status;
			
		if (!listOrderItems.isEmpty())
			update listOrderItems;
			
		List<NE__Order_Header__c> lst_ordHeader = [SELECT Id, NE__OrderStatus__c
												   FROM NE__Order_Header__c
												   WHERE NE__OrderId__c = :ord.Id];
		
		if (!lst_ordHeader.isEmpty())
		{
			lst_ordHeader[0].NE__OrderStatus__c = status;
			update lst_ordHeader[0];
		}
	}
	
	public void doAction()
	{
		Boolean finished = false;
		String idCSB;
		
		try
		{
			BI_RestWrapper.GenericResponse response = new BI_RestWrapper.GenericResponse(); 
			// Envío de la cuenta
			do
			{
				if (this.acc.BI_Id_CSB__c == null)
					response = BI_RestRequestHelper.createAccount(this.accId);
				else
					response = BI_RestRequestHelper.updateAccount(this.accId);
				
				if (response.error == null)
	            {
	            	finished = true;
	            	if (this.acc.BI_Id_CSB__c == null)
	            	{
	            		idCSB = response.message.substring(response.message.lastIndexOf('/') + 1);
	            	}
	            }
	            else
	            {
	            	if (response.error == 'NOT_FOUND' || response.error == 'MISSING REQUIRED FIELDS')
	            	{
	            		// Poner transacción a error y enviar correo para corregir y reenviar cuenta
	            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, response.error == 'NOT_FOUND'? Label.BI_CSB_No_Encontrado : Label.BI_CSB_Error_Identificador));
	            		finished = true;
	            		this.showExit = true;
						upsertTransaction(Label.BI_COL_lblFallido, response.error == 'NOT_FOUND' ? Label.BI_CSB_No_Encontrado : Label.BI_CSB_Error_Identificador, String.valueOf(this.numIntentos), this.acc.Id, this.acc.Name, this.acc.BI_Id_CSB__c == null ? Label.BI_CSB_Creacion : Label.BI_CSB_Actualizacion);
	            	}
	            	else if (response.error != null && response.message != null)
	            	{
	            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, response.error + '. ' + Label.BI_CSB_Revisar_Transacciones));
	            		finished = true;
	            		this.showExit = true;
						upsertTransaction(Label.BI_COL_lblFallido, response.error + '. ', String.valueOf(this.numIntentos), this.acc.Id, this.acc.Name, this.acc.BI_Id_CSB__c == null ? Label.BI_CSB_Creacion : Label.BI_CSB_Actualizacion);
	            	}
	            	else
	            		// Timeouts
	            		this.numIntentos = this.numIntentos + 1;
	            }
				System.debug('numIntentos:' + this.numIntentos);

			}
			while (this.numIntentos < this.maxIntentos && !finished);
				
			// Si llegamos aquí significa que se ha reintentado el num máximo de veces
			if (!finished)
			{
				sendAccountNotFinished(response);return;
				/*
				upsertTransaction(Label.BI_COL_lblFallido, Label.BI_CSB_WS_Max_Intentos + (response.error != null ? ' ' + response.error  + '. ' : ''), String.valueOf(this.numIntentos), this.acc.Id, this.acc.Name, this.acc.BI_Id_CSB__c == null ? Label.BI_CSB_Creacion : Label.BI_CSB_Actualizacion);
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BI_CSB_WS_Max_Intentos + (response.error != null ? ' ' + response.error + '. ' : '') + Label.BI_CSB_Revisar_Transacciones));
				this.showExit = true;
				return;
				*/
			}
			else if (finished && this.showExit)
			{
				return;
			}

			
			if (this.fromAccount != null)
			{
				// Update account after order callouts if first time being set
	        	if(this.acc.BI_Id_CSB__c == null)
	        		updateAccount(idCSB);
	        		
				// Upsert account transaction log after order callouts
	        	upsertTransaction(Label.BI_CSB_Procesado, idCSB != null ? Label.BI_CSB_Crear_Cuenta_OK : Label.BI_CSB_Actualizar_Cuenta_OK, String.valueOf(this.numIntentos), this.acc.Id, this.acc.Name, idCSB != null ? Label.BI_CSB_Creacion : Label.BI_CSB_Actualizacion);
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, idCSB != null ? Label.BI_CSB_Crear_Cuenta_OK : Label.BI_CSB_Actualizar_Cuenta_OK));
            	this.showExit = true;
				return;
			}
				
			finished = false;
			
			// Envío del pedido
			do
			{
				response = BI_RestRequestHelper.createOrder(this.orderId, idCSB);
				
				if (response.error == null)
	            {
	            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.BI_CSB_Crear_Pedido_OK));
	            	finished = true;
	            	this.showExit = true;
	            	// Upsert order transaction log
	            	upsertTransaction(Label.BI_COL_lblPendiente, null, '0', this.orderId, this.ord.Name, Label.BI_CSB_Creacion_Pedido);
	            	// Update order
	            	updateOrder('Sent', response.transactionType.transactionId, null);
	            }
	            else
	            {
	            	if (response.error == 'NOT_FOUND' || response.error == 'MISSING REQUIRED FIELDS')
	            	{
	            		// Poner transacción a error y enviar correo para corregir y reenviar cuenta
	            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, response.error == 'NOT_FOUND'? Label.BI_CSB_No_Encontrado : Label.BI_CSB_Error_Identificador));
	            		finished = true;
	            		this.showExit = true;
						// Upsert order transaction log
						upsertTransaction(Label.BI_COL_lblFallido, (response.error == 'NOT_FOUND'? Label.BI_CSB_No_Encontrado : Label.BI_CSB_Error_Identificador), String.valueOf(this.numIntentosOrd), this.orderId, this.ord.Name, Label.BI_CSB_Creacion_Pedido);
	            		updateOrder('Error', null, (response.error == 'NOT_FOUND'? Label.BI_CSB_No_Encontrado : Label.BI_CSB_Error_Identificador));
	            	}
	            	else if (response.error != null && response.message != null)
	            	{
	            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, response.error + '. ' + Label.BI_CSB_Revisar_Transacciones));
	            		finished = true;
	            		this.showExit = true;
						// Upsert order transaction log
						upsertTransaction(Label.BI_COL_lblFallido, response.error + '. ', String.valueOf(this.numIntentosOrd), this.orderId, this.ord.Name, Label.BI_CSB_Creacion_Pedido);
	            	}
	            	else
	            		// Timeouts
	            		this.numIntentosOrd = this.numIntentosOrd + 1;
	            }
				System.debug('numIntentosOrd:' + this.numIntentosOrd);
			}
			while (this.numIntentosOrd < this.maxIntentos && !finished);
			
        	// Update account after order callouts if first time being set
        	if(this.acc.BI_Id_CSB__c == null)
        	{
        		updateAccount(idCSB);
        	}
			// Upsert account transaction log after order callouts
        	upsertTransaction(Label.BI_CSB_Procesado, this.acc.BI_Id_CSB__c == null ? Label.BI_CSB_Crear_Cuenta_OK : Label.BI_CSB_Actualizar_Cuenta_OK, String.valueOf(this.numIntentos), this.acc.Id, this.acc.Name, this.acc.BI_Id_CSB__c == null ? Label.BI_CSB_Creacion : Label.BI_CSB_Actualizacion);
	            	
			// Si llegamos aquí significa que se ha reintentado más del num máximo de veces
			if (!finished)
			{
				createOrderNotFinished(response);
				/*
				// Upsert order transaction log
				upsertTransaction(Label.BI_COL_lblFallido, Label.BI_CSB_WS_Max_Intentos + (response.error != null ? ' ' + response.error  + '. ' : ''), String.valueOf(this.numIntentosOrd), this.orderId, this.ord.Name, Label.BI_CSB_Creacion_Pedido);
				// Update order
				updateOrder('Error', null, Label.BI_CSB_WS_Max_Intentos + ' ' + response.error);
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BI_CSB_WS_Max_Intentos + (response.error != null ? ' ' + response.error + '. ' : '') + Label.BI_CSB_Revisar_Transacciones));
				this.showExit = true;
				*/
			}
		}
		catch (Exception e)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			this.showExit = true;
		}
	}
	
	public PageReference exit()
	{
		return new PageReference ('/' + (this.fromAccount != null ? this.accId : this.orderId));
	}
	
	public void createOrderNotFinished(BI_RestWrapper.GenericResponse resp)
	{
		upsertTransaction(Label.BI_COL_lblFallido, Label.BI_CSB_WS_Max_Intentos + (resp.error != null ? ' ' + resp.error  + '. ' : ''), String.valueOf(numIntentosOrd), orderId, ord.Name, Label.BI_CSB_Creacion_Pedido);
		// Update order
		updateOrder('Error', null, Label.BI_CSB_WS_Max_Intentos + ' ' + resp.error);
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BI_CSB_WS_Max_Intentos + (resp.error != null ? ' ' + resp.error + '. ' : '') + Label.BI_CSB_Revisar_Transacciones));
		showExit = true;
	}

	public void sendAccountNotFinished(BI_RestWrapper.GenericResponse resp)
	{
		upsertTransaction(Label.BI_COL_lblFallido, Label.BI_CSB_WS_Max_Intentos + (resp.error != null ? ' ' + resp.error  + '. ' : ''), String.valueOf(numIntentos), acc.Id, acc.Name, acc.BI_Id_CSB__c == null ? Label.BI_CSB_Creacion : Label.BI_CSB_Actualizacion);
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.BI_CSB_WS_Max_Intentos + (resp.error != null ? ' ' + resp.error + '. ' : '') + Label.BI_CSB_Revisar_Transacciones));
		showExit = true;
		//return;
	}
}