@isTest
private class BI_PlanDeAccionMethods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:    
    
    History: 
    <Date>                  <Author>                <Change Description>
    03/02/2015              Pablo Oliva             Initial Version
    20/09/2017        Angel F. Santaliestra       Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva  
    Company:       Salesforce.com
    Description:   
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    03/02/2015              Pablo Oliva             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void updateOriginTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        List<RecordType> lst_rt = [select Id from RecordType where DeveloperName = 'BI_Acuerdo_Plan_de_accion' and sObjectType = 'Task' limit 1];
        
        List<String> lst_region = new List<String>{'CHI'};                                                           
        
        List <Account> lst_acc = BI_O4_DisconnectionMethods_TEST.loadAccounts(1, lst_region);
        
        List<BI_Plan_de_accion__c> lst_coor = BI_DataLoad.loadCustomPlanDeAccion(50, lst_acc[0]);
        
        List<BI_Plan_de_accion__c> lst_coor_cloned = new List<BI_Plan_de_accion__c>();
        List<Task> lst_task = new List<Task>();
        List<Task> lst_task_cloned = new List<Task>();
        
        for(BI_Plan_de_accion__c coor:lst_coor){
            lst_coor_cloned.add(new BI_Plan_de_accion__c(Name = 'Test '+String.valueOf(Math.random() * 100000),
                                                              BI_Cliente__c = lst_acc[0].Id,
                                                              BI_Plan_original__c = coor.Id,
                                                              BI_Estado__c = Label.BI_PlanAccion_Estado_En_creacion,
                                                              BI_Todos_los_acuerdos_duplicados__c = true));
                                                              
            lst_task.add(new Task(WhatId = coor.Id,
                                  Subject = 'Test' + String.valueOf(Math.random() * 100000),
                                  ActivityDate = Date.TODAY(),
                                  Type = 'Optional Task',
                                  Status = Label.BI_TaskStatus_InProgress,
                                  RecordTypeId = lst_rt[0].Id));
        }
                                                       
        insert lst_coor_cloned;
        insert lst_task;
        
        Integer i=0;
        for(Task task:lst_task){
            lst_task_cloned.add(new Task(WhatId = lst_coor_cloned[i].Id,
                                         Subject = 'Test' + String.valueOf(Math.random() * 100000),
                                         ActivityDate = Date.TODAY(),
                                         Type = 'Optional Task',
                                         Status = Label.BI_TaskStatus_InProgress,
                                         RecordTypeId = lst_rt[0].Id,
                                         BI_Acuerdo_original__c = task.Id));
                                         
            i++;
        }
        
        insert lst_task_cloned;
        
        Set<Id> set_coor = new Set<Id>();
        for(BI_Plan_de_accion__c coor:lst_coor_cloned){
            coor.BI_Estado__c = Label.BI_PlanAccion_Estado_En_curso;
            set_coor.add(coor.BI_Plan_original__c);
        }
        
        Test.startTest();
        
        update lst_coor_cloned;
        
        for(BI_Plan_de_accion__c coor:[select Id, BI_Estado__c from BI_Plan_de_accion__c where Id IN :set_coor]){
            //system.assertEquals(Label.BI_PlanAccion_Estado_Finalizado, coor.BI_Estado__c);
        }
            
        for(Task task:[select Id, Status from Task where WhatId IN :set_coor]){
            //system.assertEquals(task.Status , Label.BI_TaskStatus_DerivativeReplicate);
        }
            
        Test.stopTest();
        
    }



    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Test method to cover BI_PlanDeAccionMethods.check_HasOpenTask() function

    History: 

    <Date>                          <Author>                    <Change Description>
    04/02/2015                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

 /*   static testMethod void check_HasOpenTask_TEST() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_Dataload.set_ByPass(false,false);

        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'CHL'});
        system.assert(!lst_acc.isEmpty());

        List<BI_Plan_de_accion__c> lst_coord = BI_Dataload.loadCustomPlanDeAccion(2, lst_acc[0]);
        system.assert(!lst_coord.isEmpty());        

        List<Id> lst_whatId = new List<Id>();
        for(BI_Plan_de_accion__c coord :lst_coord){
            lst_whatId.add(coord.Id);
            coord.BI_Estado__c =Label.BI_PlanAccion_Estado_Finalizado;
        }

        List<Recordtype> lst_rt_task = [SELECT ID, sObjectType  FROM RecordType WHERE Developername  = 'BI_Acuerdo_Plan_de_accion'];
        List<Task> lst_task = BI_Dataload.loadTaskWithRT(2,lst_rt_task, lst_whatId, Label.BI_TaskStatus_InProgress);
        system.assert(!lst_task.isEmpty());

        
        Test.startTest();

        try{
            update lst_coord;
            system.assert(false);
        }catch(Exception exc){
            system.assert(exc.getMessage().contains(Label.BI_Msg_NoFinalizarSiHayTareasEnCurso));
        }

        Test.stopTest();

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Test method for BI_PlanDeAccionMethods.preventTaskForActionPlan() 
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    05/02/2015                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 /*   static testMethod void check_status_Cancel_allowed_Test() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_Dataload.set_ByPass(false,false);
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'CHL'});
        system.assert(!lst_acc.isEmpty());

        List<User> lst_usr = BI_DataLoad.loadUsers(2, BI_DataLoad.searchAdminProfile(), Label.BI_Administrador_del_sistema);
        lst_usr[0].ManagerId = lst_usr[1].Id;
        update lst_usr[0];

        List<BI_Plan_de_accion__c> lst_coord = new List<BI_Plan_de_accion__c>();
        system.runAs(lst_usr[0]){
            lst_coord = BI_Dataload.loadCustomPlanDeAccion(2, lst_acc[0]);
            system.assert(!lst_coord.isEmpty());
        }


        List<Id> whatId = new List<Id>();
        for(BI_Plan_de_accion__c coo :lst_coord){
            whatId.add(coo.Id);
        coo.BI_Solucion_Motivo_de_cierre__c ='motivo cierre test';
            coo.BI_Estado__c = Label.BI_PlanAccion_Estado_Cancelado;
        }

//      lst_coord[1].BI_Estado__c = Label.BI_PlanAccion_Estado_Finalizado;


        Test.StartTest();
        system.runAs(lst_usr[1]){
            update lst_coord;       
        }
        Test.StopTest();
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Test method for BI_PlanDeAccionMethods.preventTaskForActionPlan() 
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    05/02/2015                      Micah Burgos                Initial Version
    26/09/2016                      Ricardo Pereira             Changed load Account call
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void check_status_Cancel_denied_Test() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_Dataload.set_ByPass(false,false);
        List<Account> lst_acc = BI_O4_DisconnectionMethods_TEST.loadAccounts(1, new List<String>{'CHL'});
        system.assert(!lst_acc.isEmpty());

        List<BI_Plan_de_accion__c> lst_coord = BI_Dataload.loadCustomPlanDeAccion(2, lst_acc[0]);
        system.assert(!lst_coord.isEmpty());

        List<Id> whatId = new List<Id>();
        for(BI_Plan_de_accion__c coo :lst_coord){
            whatId.add(coo.Id);
            coo.BI_Estado__c = Label.BI_PlanAccion_Estado_Cancelado;
        }



        Test.StartTest();
        try{
            update lst_coord;
            system.assert(false);
        }catch(Exception exc){
            system.assert(exc.getMessage().contains(Label.BI_Plan_de_accion_control_estado_Cancelado));
        }

        Test.StopTest();
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Test method for BI_PlanDeAccionMethods.preventTaskForActionPlan() 
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    05/02/2015                      Micah Burgos                Initial Version
    26/09/2016                      Ricardo Pereira             Changed load Account call
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void check_status_Completed_denied_Test() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_Dataload.set_ByPass(false,false);
        List<Account> lst_acc = BI_O4_DisconnectionMethods_TEST.loadAccounts(1, new List<String>{'CHL'});
        system.assert(!lst_acc.isEmpty());

        List<BI_Plan_de_accion__c> lst_coord = BI_Dataload.loadCustomPlanDeAccion(2, lst_acc[0]);
        system.assert(!lst_coord.isEmpty());

        //List<Id> whatId = new List<Id>();
        //for(BI_Plan_de_accion__c coo :lst_coord){
        //  whatId.add(coo.Id);
        //  coo.BI_Estado__c = Label.BI_PlanAccion_Estado_Pendiente_finalizacion;
        //  coo.BI_Casilla_desarrollo__c = true;
        //}
        //update lst_coord;

        for(BI_Plan_de_accion__c coo :lst_coord){
            //whatId.add(coo.Id);
            coo.BI_Estado__c = Label.BI_PlanAccion_Estado_Finalizado;
        }


        Test.StartTest();
        try{
            update lst_coord;
            system.assert(false);
        }catch(Exception exc){
            system.assert(true);
        }

        Test.StopTest();
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Test Method BI_PlanDeAccionMethods.escalationAfterInsert 
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    13/09/2016                      Miguel Cabrera              Initial Version
    25/09/2017                      Jaime Regidor               Fields BI_Subsector__c and BI_Sector__c added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void escalationAfterInsert(){

        RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];
        List<RecordType> rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_O4_IA' AND SobjectType = 'BI_Plan_de_accion__c'];
        List<RecordType> rt2 = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_O4_CIP' AND SobjectType = 'BI_Plan_de_accion__c'];
        List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  BI_Subsegment_local__c = 'Top 1',
                  RecordTypeId = rt1.Id,
                  BI_Country__c = 'Spain'
                  );    
        lst_acc1.add(acc1);
        insert lst_acc1;

        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = BI_DataLoad.searchAdminProfile(),
                          BI_Permisos__c = Label.BI_Administrador,
                          UserPermissionsMarketingUser = true,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com',
                          Pais__c = 'Spain',
                          IsActive = true);
        
        insert usertest;

        List<BI_Plan_de_accion__c> lst_pda = new List<BI_Plan_de_accion__c>();
        BI_Plan_de_accion__c pla = new BI_Plan_de_accion__c();
        pla.BI_O4_Last_Active_Date_Time__c = Date.today();
        pla.BI_O4_Last_Escalation_Date_Time__c = Date.today();
        pla.BI_O4_Next_Escalated_User__c = usertest.Id;
        pla.BI_O4_Original_CIP_User__c = usertest.Id;
        pla.RecordTypeId = rt[0].Id;
        pla.BI_Estado__c = 'En creación';
        pla.BI_O4_MNCs_Account__c = acc1.Id;

        lst_pda.add(pla);
        insert lst_pda;

        BI_PlanDeAccionMethods.escalationAfterInsert(lst_pda);

        List<Account> lst_acc = new List<Account>();       
        Account acc = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  BI_Subsegment_local__c = 'Top 1',
                  RecordTypeId = rt1.Id,
                  BI_Country__c = 'Spain'
                  );    
        lst_acc.add(acc);
        insert lst_acc;

        User usertest1 = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = BI_DataLoad.searchAdminProfile(),
                          BI_Permisos__c = Label.BI_Administrador,
                          UserPermissionsMarketingUser = true,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser1@testorg.com',
                          Pais__c = 'Spain',
                          IsActive = true);
        
        insert usertest1;

        List<BI_Plan_de_accion__c> lst_pda1 = new List<BI_Plan_de_accion__c>();
        BI_Plan_de_accion__c pla1 = new BI_Plan_de_accion__c();
        pla1.BI_O4_Last_Active_Date_Time__c = Date.today();
        pla1.BI_Estado__c = 'En creación';
        pla1.BI_O4_Last_Escalation_Date_Time__c = Date.today();
        pla1.BI_O4_Next_Escalated_User__c = usertest1.Id;
        pla1.BI_O4_Original_CIP_User__c = usertest1.Id;
        pla1.RecordTypeId = rt2[0].Id;
        pla.BI_O4_MNCs_Account__c = acc.Id;

        lst_pda1.add(pla1);
        insert lst_pda1;

        BI_PlanDeAccionMethods.escalationAfterInsert(lst_pda1);
    }    

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Test Method BI_PlanDeAccionMethods.escalationBeforeAfterUpdate
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    13/09/2016                      Miguel Cabrera              Initial Version
    25/09/2017                      Jaime Regidor               Fields BI_Subsector__c and BI_Sector__c added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void escalationBeforeAfterUpdate(){

        Boolean isBefore = false; 
        Boolean isAfter = true;
        Boolean isUpdate = true;

        RecordType rtAcc = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];
        List<RecordType> rtPla = [SELECT r.Id FROM RecordType r WHERE r.DeveloperName  = 'BI_O4_IA' AND SobjectType = 'BI_Plan_de_accion__c'];
        List<RecordType> rtPla1 = [SELECT r.Id FROM RecordType r WHERE r.DeveloperName  = 'BI_O4_CIP' AND SobjectType = 'BI_Plan_de_accion__c'];

        List<Account> lst_acc = new List<Account>();       
        Account acc = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  BI_Subsegment_local__c = 'Top 1',
                  RecordTypeId = rtAcc.Id,
                  BI_Country__c = 'Spain'
                  );    
        lst_acc.add(acc);
        insert lst_acc;

         User usertest2 = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = BI_DataLoad.searchAdminProfile(),
                          BI_Permisos__c = Label.BI_Administrador,
                          UserPermissionsMarketingUser = true,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuse33r@testorg.com',
                          Pais__c = 'Spain',                    
                          IsActive = true);
        
        insert usertest2;

        User usertest = new User(alias = 'testu2',
                          email = 'tu@testorg2.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test2',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = BI_DataLoad.searchAdminProfile(),
                          BI_Permisos__c = Label.BI_Administrador,
                          UserPermissionsMarketingUser = true,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuse2r@testorg.com',
                          Pais__c = 'Spain',
                          ManagerId = usertest2.Id,
                          IsActive = true);
        
        insert usertest;


        List<BI_Plan_de_accion__c> lst_pda = new List<BI_Plan_de_accion__c>();
        BI_Plan_de_accion__c pla = new BI_Plan_de_accion__c();
        pla.BI_O4_Last_Active_Date_Time__c = Date.today()+2;
        pla.BI_O4_Last_Escalation_Date_Time__c = Date.today()+2;
        pla.BI_O4_Next_Escalated_User__c = usertest.Id;
        pla.BI_O4_Original_CIP_User__c = usertest.Id;
        pla.RecordTypeId = rtPla1[0].Id;
        pla.BI_Estado__c = 'En creación';
        pla.BI_O4_Is_Escalated__c = false;
        pla.BI_O4_CIP_User__c = usertest.Id;
        pla.BI_O4_Complete__c = '50 %';
        pla.BI_O4_Other_Comments__c = 'zzz';

        lst_pda.add(pla);
        insert lst_pda;
        pla.BI_Estado__c = 'Completed';
        update pla;
        
 //------------------------------------------------------------------------------------       

        List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  BI_Subsegment_local__c = 'Top 1',
                  RecordTypeId = rtAcc.Id,
                  BI_Country__c = 'Spain'
                  );    
        lst_acc1.add(acc1);
        insert lst_acc1;

        User usertest1 = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = BI_DataLoad.searchAdminProfile(),
                          BI_Permisos__c = Label.BI_Administrador,
                          UserPermissionsMarketingUser = true,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com',
                          Pais__c = 'Spain', 
                          ManagerId = usertest.Id,                   
                          IsActive = true);
        
        insert usertest1;

        List<BI_Plan_de_accion__c> lst_pda1 = new List<BI_Plan_de_accion__c>();
        lst_pda1.add(pla.clone(true,true,false,true));
        lst_pda1[0].BI_O4_Last_Active_Date_Time__c = Date.today();
        lst_pda1[0].BI_O4_Last_Escalation_Date_Time__c = Date.today();
        lst_pda1[0].BI_O4_Next_Escalated_User__c = usertest1.Id;
        lst_pda1[0].BI_O4_Original_CIP_User__c = usertest1.Id;
        lst_pda1[0].RecordTypeId = rtPla[0].Id;
        lst_pda1[0].BI_O4_CIP_User__c = usertest1.Id;
        lst_pda1[0].BI_Estado__c = 'En creación';
        lst_pda1[0].BI_O4_MNCs_Account__c = acc1.Id;
        lst_pda1[0].BI_O4_Is_Escalated__c = true;
        lst_pda1[0].BI_O4_Complete__c = '25 %';
        lst_pda1[0].BI_O4_Other_Comments__c = 'xxx';


        
        update lst_pda1;
       
        Map<Id, BI_Plan_de_accion__c> oldCIPMap = new Map<Id, BI_Plan_de_accion__c>();
        oldCIPMap.put(lst_pda1[0].Id, lst_pda1[0]);
        
        BI_PlanDeAccionMethods.escalationBeforeAfterUpdate(lst_pda, isBefore, true, true, oldCIPMap);

    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Test Method BI_PlanDeAccionMethods.escalationBeforeInsert
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    13/09/2016                      Miguel Cabrera              Initial Version
    25/09/2017                      Jaime Regidor               Fields BI_Subsector__c and BI_Sector__c added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void escalationBeforeInsert(){

        RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];
        List<RecordType> rt = [SELECT r.Id FROM RecordType r WHERE r.DeveloperName  = 'BI_O4_IA' AND SobjectType = 'BI_Plan_de_accion__c'];

        List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_Regional__c = 'test',
                  BI_Territory__c = 'test',
                  BI_Subsegment_local__c = 'Top 1',
                  RecordTypeId = rt1.Id,
                  BI_Country__c = 'Spain'
                  );    
        lst_acc1.add(acc1);
        insert lst_acc1;

        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = BI_DataLoad.searchAdminProfile(),
                          BI_Permisos__c = Label.BI_Administrador,
                          UserPermissionsMarketingUser = true,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com',
                          Pais__c = 'Spain',
                          IsActive = true);
        
        insert usertest;

        List<BI_Plan_de_accion__c> lst_pda = new List<BI_Plan_de_accion__c>();
        BI_Plan_de_accion__c pla = new BI_Plan_de_accion__c();
        pla.BI_O4_Last_Active_Date_Time__c = Date.today();
        pla.BI_O4_Last_Escalation_Date_Time__c = Date.today();
        pla.BI_O4_Next_Escalated_User__c = usertest.Id;
        pla.BI_O4_Original_CIP_User__c = usertest.Id;
        pla.RecordTypeId = rt[0].Id;
        pla.BI_Estado__c = 'En creación';
        pla.BI_O4_MNCs_Account__c = acc1.Id;

        lst_pda.add(pla);
        Test.startTest();
        insert lst_pda;
        update lst_pda; 
        BI_TestUtils.throw_exception = true;
        Test.stopTest();
        BI_PlanDeAccionMethods.escalationBeforeInsert(lst_pda);  
    }

    static testMethod void asociarUsuariosAccountTeam(){

        RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];
        List<RecordType> rt = [SELECT r.Id FROM RecordType r WHERE r.DeveloperName  = 'BI_O4_IA' AND SobjectType = 'BI_Plan_de_accion__c'];

        List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                  BI_Segment__c = 'Empresas',
                  BI_Subsector__c = 'test', //25/09/2017
                  BI_Sector__c = 'test', //25/09/2017
                  BI_Subsegment_Regional__c = 'test', //25/09/2017
                  BI_Territory__c = 'test', //25/09/2017
                  BI_Subsegment_local__c = 'Top 1',
                  RecordTypeId = rt1.Id,
                  BI_Country__c = 'Spain'
                  );    
        lst_acc1.add(acc1);
        insert lst_acc1;

        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = BI_DataLoad.searchAdminProfile(),
                          BI_Permisos__c = Label.BI_Administrador,
                          UserPermissionsMarketingUser = true,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com',
                          Pais__c = 'Spain',
                          IsActive = true);
        
        insert usertest;

        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountId = acc1.Id;
        atm.TeamMemberRole = 'GSM';
        atm.UserId = usertest.Id;
        insert atm; 

        List<BI_Plan_de_accion__c> lst_pda = new List<BI_Plan_de_accion__c>();
        BI_Plan_de_accion__c pla = new BI_Plan_de_accion__c();
        pla.BI_O4_Last_Active_Date_Time__c = Date.today();
        pla.BI_O4_Last_Escalation_Date_Time__c = Date.today();
        pla.BI_O4_Next_Escalated_User__c = usertest.Id;
        pla.BI_O4_Original_CIP_User__c = usertest.Id;
        pla.RecordTypeId = rt[0].Id;
        pla.BI_O4_MNCs_Account__c = acc1.Id;
        pla.BI_Estado__c = 'En creación';

        lst_pda.add(pla);
        insert lst_pda;
        update lst_pda;
        BI_PlanDeAccionMethods.asociarUsuariosAccountTeam(lst_pda);
    }
}