/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Jesús Martínez
Company:        Everis España
Description:    Autenticación FSARG.

History:

<Date>                      <Author>                        <Change Description>
26/10/2016                  Jesús Martínez                  Versión Inicial.
27/10/2016                  Jesús Martínez                  Incluido método generarContractCorrecto.
31/10/2016                  Miguel Girón                    Inclusión de métodos: XMLOutput y XMLInput para parsear XML
02/11/2016                  Miguel Girón                    Inlcusión de métodos: GenerateXMLmap, XMLResponseGen y XMLOutputList
11/11/2016                  Jesús Martínez                  Inlcusión de métodos: Cambio en XMLOutPut y eliminación del resto de métodos que ya no se usan.
15/03/2017                  Eduardo Ventura                 Reconstrucción de métodos getContract, getAccount y getOpportunity.
15/03/2017                  Eduardo Ventura                 Reconstrucción de métodos isValidStageForLicitation y isValidStageForContract.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class BI_GD_Helper {
    public static String campoVacioLicitation = '';
    public static String campoVacioContract = '';
    public static Boolean authCliente = true;
    
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:                 Eduardo Ventura
Company:                Everis España
Description:            Obtención de contrato en base a un Id.

History:

<Date>                              <Author>                                <Change Description>
15/03/2017                          Eduardo Ventura                         Versión inicial.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Contract getContractFromId (String contractId){
        return [SELECT AccountId,ActivatedById,ActivatedDate,BillingCity,BillingCountry,BillingGeocodeAccuracy,
                BillingLatitude,BillingLongitude,BillingPostalCode,BillingState,BillingStreet,BI_ARG_Complejidad__c,
                BI_ARG_Contrato_con_Terceros__c,BI_ARG_Empresa_Licitacion__c,BI_ARG_Empresa_Local__c,BI_ARG_Estado_Procesal__c,
                BI_ARG_Fecha_Aviso_Vto__c,BI_ARG_Fecha_de_Adjudicacion__c,BI_ARG_Fecha_Emision_Poliza_Adjudicacion__c,
                BI_ARG_Fecha_presentacion_de_sobres__c,BI_ARG_Fecha_Sol_Elaboracion_Rta_Pliego__c,BI_ARG_Fecha_Vencimiento_G_Oferta__c,
                BI_ARG_Garantia_de_Licitacion__c,BI_ARG_Garantia_de_Oferta__c,BI_ARG_Id_Legado__c,BI_ARG_Impuesto_a_los_Sellos__c,
                BI_ARG_Licitacion__c,BI_ARG_Moneda_G_Adjudicacion__c,BI_ARG_Moneda_G_Oferta__c,BI_ARG_Moneda_Impuesto__c,
                BI_ARG_Moneda_Otros_Gastos__c,BI_ARG_Moneda_Pliego__c,BI_ARG_Monetario__c,BI_ARG_Monto_del_Pliego__c,
                BI_ARG_Monto_G_Adjudicacion__c,BI_ARG_Monto_G_Oferta__c,BI_ARG_Monto_Impuesto__c,BI_ARG_Monto_Otros_Gastos__c,
                BI_ARG_Nro_de_Licitacion__c,BI_ARG_Nro_de_Poliza_G_Adjudicacion__c,BI_ARG_Nro_de_Poliza_G_de_Oferta__c,
                BI_ARG_Proveedor_Internacional__c,BI_ARG_Recargo_por_Mora__c,BI_ARG_Rescision_Anticipada__c,BI_ARG_Servicios_Internacionales__c,
                BI_ARG_SLA__c,BI_ARG_Tipo_de_Cambio_Aplicable__c,BI_ARG_Tipo_de_Contratacion__c,BI_ARG_Tope_Tipo_de_Cambio__c,
                BI_ARG_Validez__c,BI_ARG_Valor_Tope__c,BI_ARG_Venta_de_Equipamiento__c,BI_Autorenovable__c,BI_Divisas_del_contrato__c,                
                BI_Fecha_de_creacion__c,BI_Fecha_fin_de_implementacion__c,BI_Fecha_fin_de_servicio__c,BI_Fecha_inicio_implementacion__c,
                BI_Fecha_inicio_servicio__c,BI_GD_Estado_sincronizacion_contrato__c,BI_GD_Estado_sincronizacion_licitacion__c,
                BI_GD_ID_Gestor_Documental_licitacion__c,BI_GD_Ultima_sincronizacion_contrato__c,BI_GD_Ultima_sincronizacion_licitacion__c,
                BI_Impuesto_sellos__c,BI_Indefinido__c,BI_Multa_finalizacion_anticipada_servici__c,BI_Oportunidad__c,BI_Pais__c,
                BI_Tiempo_transcurrido_creacion_contrato__c,BI_Tipo_de_documento_recibido__c,BI_Validez__c,BI_Venta_bruta_total_FCV__c,
                CompanySignedDate,CompanySignedId,ContractNumber,ContractTerm,CreatedById,CreatedDate,CurrencyIsoCode,CustomerSignedDate,CustomerSignedId,
                CustomerSignedTitle,Description,EndDate,Id,IsDeleted,LastActivityDate,LastApprovedDate,LastModifiedById,LastModifiedDate,
                LastReferencedDate,LastViewedDate,Name,OwnerExpirationNotice,OwnerId,RecordTypeId,SpecialTerms,StartDate,
                Status,StatusCode,BI_ARG_Proveedor_Tercero__c FROM Contract WHERE Id =: contractId LIMIT 1][0];
    }
    
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:                 Eduardo Ventura
Company:                Everis España
Description:            Obtención de cuenta en base a un Id.

History:

<Date>                              <Author>                                <Change Description>
15/03/2017                          Eduardo Ventura                         Versión inicial.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Account getAccountFromId (String accountId){
        return [SELECT Name,BI_Tipo_de_identificador_fiscal__c,BI_Codigo_ISO__c,BI_No_Identificador_fiscal__c,BI_Country__c 
                FROM Account Where Id =: accountId Limit 1][0];
    }
    
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:                 Eduardo Ventura
Company:                Everis España
Description:            Obtención de campos referidos a contratos de la oportunidad en base a un Id.

History:

<Date>                              <Author>                                <Change Description>
15/03/2017                          Eduardo Ventura                         Versión inicial.
20/10/2017                          Paloma Lastra                           EVER01 - Se recupera el campo BI_Id_Interno_de_la_Oportunidad__c
---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Opportunity getOpportunityFromId (String opportunityId){
        
        return [SELECT BI_Licitacion__c,BI_Country__c,BI_SIMP_Opportunity_Type__c,BI_O4_Bussiness_Line__c,
                NE__Contract_Duration_Months__c, Name, StageName, CloseDate, BI_Fecha_de_vigencia__c, AccountId, BI_Contrato_con_terceros__c,
                BI_ARG_Proveedor_Tercero__c, BI_O4_Service_Lines__c, BI_Opportunity_Type__c, BI_ARG_Servicios_Internacionales__c, BI_Ingreso_por_unica_vez__c,
                BI_ARG_Fecha_Sol_Elaboracion_Rta_Pliego__c, BI_ARG_Fecha_presentacion_de_sobres__c, BI_ARG_Tipo_de_Contratacion__c, BI_ARG_Nro_de_Licitacion__c,
                //EVER01 - INI
                BI_Id_Interno_de_la_Oportunidad__c
                //EVER01 - FIN
                FROM Opportunity Where Id =: opportunityId Limit 1][0];
    }
    
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:                 Eduardo Ventura
Company:                Everis España
Description:            Determina si una licitación puede sincronizarse en base a la etapa de la oportunidad.

History:

<Date>                              <Author>                                <Change Description>
15/03/2017                          Eduardo Ventura                         Versión inicial.
02/11/2017                                                                  Ever 01: Try catch de logs.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static boolean isValidStageForLicitation (Opportunity opportunity){
        boolean result;
        
        /* Process Data */
        try{
            result = (String.valueOf(opportunity.StageName).contains('F1 - Closed Won') || !String.valueOf(opportunity.StageName).contains('F6')) && !String.valueOf(opportunity.StageName).contains('F1 - Closed Lost');
        } catch(Exception exc){
            //Ever01-INI
            BI_LogHelper.generate_BILog('BI_GD_Helper.isValidStageForLicitation', 'BI_EN', exc, 'Web Service');
            //result = false;
            //Ever01-FIN
        }
        
        return result;
    }
    
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:                 Eduardo Ventura
Company:                Everis España
Description:            Determina si un contrato puede sincronizarse en base a la etapa de la oportunidad.

History:

<Date>                              <Author>                                <Change Description>
15/03/2017                          Eduardo Ventura                         Versión inicial.
02/11/2017                                                                  Ever 01: Try catch de logs.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static boolean isValidStageForContract (Opportunity opportunity){
        boolean result;
        
        /* Process Data */
        try{
            result = String.valueOf(opportunity.StageName).contains('F1 - Closed Won') || String.valueOf(opportunity.StageName).contains('F2');
        }catch(Exception exc){
            //Ever01-INI
            BI_LogHelper.generate_BILog('BI_GD_Helper.isValidStageForContract', 'BI_EN', exc, 'Web Service');
            //result = false;
            //Ever01-FIN
        }
        
        return result;
    }
    /*
public static String getTokenOAuth(String username, String password, String method){
OBTENCIÓN DEL TOKEN DE AUTENTICACIÓN 


String clientId = '3MVG9od6vNol.eBgSuBWDbEjKSemnQLZiXKVf9GPQitjE0aqgj2MVWK38FzUSRnxYfemZTZEkYgN.Id7jtBE0';
String clientSecret = '696349059285514776';


String reqbody = 'grant_type=password&client_id=' + clientId + '&client_secret=' + clientSecret + '&username=' + EncodingUtil.urlEncode(username, 'UTF-8') + '&password=' + EncodingUtil.urlEncode(password, 'UTF-8');

Http httpAuth = new Http();
HttpRequest req = new HttpRequest();
HttpResponse res = new HttpResponse();
//req.setBody(reqbody);
req.setMethod('POST');
req.setMethod(method);
////Note if my domain is set up use the proper domain name else use login.salesforce.com for prod or developer or test.salesforce.com for sandbox instance
req.setEndpoint('https://telefonicab2b--fsarg2.cs81.my.salesforce.com/services/oauth2/token');

if(Test.isRunningTest()){
// if(!Test.isRunningTest()){
BI_GD_AuthMock getMock = new BI_GD_AuthMock();
if (!authCliente) {
getMock.authCliente = false;    
} else {
getMock.authCliente = authCliente; 
}

res = getMock.respond(req);
} else {
res = httpAuth.send(req); 
}

System.debug('mapaJSON: ' + res.getBody());
Map<String, Object> mapaJSON = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());

String access_token = (String) mapaJSON.get('access_token');

return access_token;
}


*/
    /* public static String XMLOutput(Map<String,String> mapaXML){

// ---------- MÉTODO ENCARGADO DE LA CREACIÓN Y FORMATEO DEL XML QUE SERÁ EL BODY DE LA REQUEST ---------        
//Creamos el objeto XML que será serializado y puesto como Body de la Request
String XMLRequest = '', label = '';

//Decidimos si será contrato o licitación
label = mapaXML.get('LABEL');
//else if((mapaXML.get(key)) label = 'LICITACION';
//else label = 'EQUIVALENCIAS';

//Adaptamos el mapa de tipo campo:valor a dos listas
//una con los valores y otra con los mapas
Set<String> mapKeys = mapaXML.keySet();
List<String> fieldKeys = new List<String>();
List<String> fieldAdditional = new List<String>();
List<String> fieldPrice = new List<String>();
List<String> fieldTax = new List<String>();
List<String> mapValuesKeys = new List<String>();
List<String> mapValuesAdditional = new List<String>();
List<String> mapValuesPrice = new List<String>();
List<String> mapValuesTax = new List<String>();
//fieldKeys.addAll(mapKeys);
for(String field :mapaXML.keySet()){
if (field.contains('additionalData.')){
fieldAdditional.add(field);
mapValuesAdditional.add(mapaXML.get(field));
}else if (field.contains('price.')){
fieldPrice.add(field);
mapValuesPrice.add(mapaXML.get(field));
}else if (field.contains('tax.')){
fieldTax.add(field);
mapValuesTax.add(mapaXML.get(field));
}else if (field != 'LABEL' && field!= 'IDCONTRATO' && field!='camposVacios'){
fieldKeys.add(field);
mapValuesKeys.add(mapaXML.get(field));
}
}

//List<String> mapValues = mapaXML.values();

//Instanciamos el objeto encargado de crear el XML
XmlStreamWriter XmlWriter = new XmlStreamWriter();

//Creamos el archivo <etiqueta>valor</etiqueta>
if (label != null) XmlWriter.writeStartElement(null, label, null);
for (Integer i = 0; i < fieldKeys.size(); i++){
XmlWriter.writeStartElement(null, fieldKeys[i].toUpperCase(), null);
//System.debug(fieldKeys[i].toUpperCase() + ':' + mapValuesKeys[i]);
if (label == 'LICITACION') campoVacioLicitation=fieldKeys[i];
if (label == 'CONTRATO') campoVacioContract=fieldKeys[i]; 

if (mapValuesKeys[i]!=null) {
XmlWriter.writeCharacters(mapValuesKeys[i]);
XmlWriter.writeEndElement();	                
}
}

String field= '';
//Inicio Price
if (fieldPrice.size() > 0) XmlWriter.writeStartElement(null, 'PRICE', null);
for (Integer i = 0; i < fieldPrice.size(); i++){
field = fieldPrice[i].toUpperCase();
XmlWriter.writeStartElement(null, field.substringAfter('.'), null);
if (label == 'LICITACION') campoVacioLicitation=fieldPrice[i];
if (label == 'CONTRATO') campoVacioContract=fieldPrice[i];
if (mapValuesPrice[i]!=null) {
XmlWriter.writeCharacters(mapValuesPrice[i]);
XmlWriter.writeEndElement();
}
}
//Cierro Price
if (fieldPrice.size() > 0) XmlWriter.writeEndElement();
//Inicio Tax
if (fieldTax.size() > 0) XmlWriter.writeStartElement(null, 'TAX', null);
for (Integer i = 0; i < fieldTax.size(); i++){
field = fieldTax[i].toUpperCase();
XmlWriter.writeStartElement(null, field.substringAfter('.'), null);
if (label == 'LICITACION') campoVacioLicitation=fieldTax[i];
if (label == 'CONTRATO') campoVacioContract=fieldTax[i];            

if (mapValuesTax[i]!=null) {
XmlWriter.writeCharacters(mapValuesTax[i]);
XmlWriter.writeEndElement();
}
}
//Cierro Tax
if (fieldTax.size() > 0) XmlWriter.writeEndElement();        
//Inicio AdditionalData
if (fieldAdditional.size() > 0) XmlWriter.writeStartElement(null, 'ADDITIONALDATA', null);
for (Integer i = 0; i < fieldAdditional.size(); i++){
field = fieldAdditional[i].toUpperCase();
XmlWriter.writeStartElement(null, field.substringAfter('.'), null);
if (label == 'LICITACION') campoVacioLicitation=fieldAdditional[i];
if (label == 'CONTRATO') campoVacioContract=fieldAdditional[i];
if( mapValuesAdditional[i] != null){
XmlWriter.writeCharacters(mapValuesAdditional[i]);
XmlWriter.writeEndElement(); 
}
//XmlWriter.writeCharacters(mapValuesAdditional[i]);

}
//Cierro AdditionalData
if (fieldAdditional.size() > 0) XmlWriter.writeEndElement();        
//Cierro CONTRATO/LICITACION
if (label != null)XmlWriter.writeEndElement();


//Lo guardamos en un String y lo cerramos
XMLRequest = XmlWriter.getXmlString();
XmlWriter.close();
//System.debug('XMLRequest Prov ---> ' + XMLRequest);

//Instanciamos un nuevo objeto
XmlWriter = new XmlStreamWriter();

//Escribimos el String XML en la etiqueta CDATA
//Y cerramos el documento
XmlWriter.writeStartDocument('UTF-8', '1.0');
XmlWriter.writeStartElement(null, 'REQUEST', null);
XmlWriter.writeCData(XMLRequest);
XmlWriter.writeEndElement();
XmlWriter.writeEndDocument();
XMLRequest = XmlWriter.getXmlString();
XmlWriter.close();
//System.debug('XMLRequest Def ---> ' + XMLRequest);
return XMLRequest;
}*/
    
    
    
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:                 Eduardo Ventura
Company:                Everis España
Description:            Metodo encargado de crear un contrato

History:

<Date>                              <Author>                                <Change Description>
15/03/2017                          Eduardo Ventura                         Versión inicial.
02/11/2017                                                                  Ever 01: Try catch de logs.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Contract generateContrato(){
        boolean result;
        Account acct = new Account(Name = 'Cuenta de prueba Contrato'/*, Raz_n_social__c = 'Razon social'*/, BI_Tipo_de_identificador_fiscal__c = 'NIF',
                                   BI_No_Identificador_fiscal__c='48975632B', BI_Country__c='Argentina',BI_Segment__c = 'Empresas',
                                   BI_Subsegment_Regional__c = 'Corporate',
                                   BI_Territory__c = 'CABA',
                                   BI_Sector__c = 'Comercio',
                                   Sector__c = 'Private',
                                   BI_Subsector__c= 'Banca');
        Contract contract = new Contract();
        acct.Owner = [SELECT ID,Pais__c, name FROM user Where Pais__c <> 'Colombia' Limit 1];
        acct.OwnerId = acct.Owner.Id;
        
        contract.Account = acct;
        contract.AccountId = acct.Id;
        contract.Owner = acct.Owner;
        contract.OwnerId = acct.OwnerId;
        Opportunity opp = new Opportunity(BI_Licitacion__c = 'No',BI_Country__c='Argentina', BI_SIMP_Opportunity_Type__c= 'Proyecto Especial',
                                          BI_O4_Bussiness_Line__c='Digital', NE__Contract_Duration_Months__c=10, Name='Opp de prueba Contrato',
                                          StageName='F6 - Prospect', CloseDate = Date.Today().addDays(10), BI_Fecha_de_vigencia__c = Date.Today().addDays(10),
                                          AccountId=acct.Id, BI_Contrato_con_terceros__c = 'Sí', 
                                          BI_O4_Service_Lines__c= 'Internet', BI_Opportunity_Type__c='Alta',
                                          BI_Ingreso_por_unica_vez__c = 100);
        Product2 p2 = new Product2(Name='Test Product',isActive=true);
        contract.Name = 'Contrato de prueba';
        //contract.BI_Divisas_del_contrato__c = 'EUR - Euro';
        contract.BI_ARG_Monetario__c = 'SI';
        contract.CustomerSignedDate = Date.newInstance(2017, 8, 15);
        contract.ContractTerm = 12;
        contract.BI_ARG_Validez__c = 'Provisorio';
        contract.BI_ARG_Rescision_Anticipada__c = 'SI';
        contract.BI_ARG_Recargo_por_Mora__c ='SI';
        contract.BI_Multa_finalizacion_anticipada_servici__c = True;
        contract.BI_ARG_Tope_Tipo_de_Cambio__c ='SI';
        contract.BI_ARG_Valor_Tope__c = 123456;
        contract.BI_ARG_Tipo_de_Cambio_Aplicable__c ='Efectivo Pago';
        contract.BI_ARG_Venta_de_Equipamiento__c ='Leasing';
        contract.BI_Fecha_inicio_servicio__c = Date.newInstance(2017, 7, 11);
        contract.BI_Fecha_fin_de_servicio__c = Date.newInstance(2017, 7, 19);
        contract.BI_Autorenovable__c ='Sí';
        contract.BI_ARG_Empresa_Local__c ='TASA';
        contract.Description = 'Observaciones';
        contract.BI_Tipo_de_documento_recibido__c = 'Orden de Compra';
        contract.BI_ARG_Moneda_Impuesto__c ='EUR';
        contract.BI_ARG_Impuesto_a_los_Sellos__c ='SI';
        contract.BI_ARG_Monto_Impuesto__c =123;
        contract.BI_ARG_SLA__c = 'STD';
        contract.BI_ARG_Proveedor_Internacional__c ='Everis international';
        contract.BI_Oportunidad__c = opp.Id;
        contract.BI_Oportunidad__r = opp;
        try{
            insert acct;
            //Ever01-INI
        //}catch(Exception exc){
            //System.debug ('Excepcion ' +exc.getMessage());
        //}
        //Ever01-FIN
        
        insert p2;
        insert opp;
        insert contract;
        
        
        
        PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        String standardPriceBookId = pb2Standard.Id;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=p2.Id, UnitPrice=99, isActive=true);
        
        OpportunityLineItem oli = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=opp.Id, Quantity=2, TotalPrice=99);
        
        
        
        insert pbe;
        
        insert oli;
            //Ever01-INI
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_GD_Helper.generateContrato', 'BI_EN', exc, 'Web Service');
        }
        //Ever01-FIN
        return contract;
    }
    
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:                 Eduardo Ventura
Company:                Everis España
Description:            Metodo encargado de crear una licitación

History:

<Date>                              <Author>                                <Change Description>
15/03/2017                          Eduardo Ventura                         Versión inicial.
02/11/2017                                                                  Ever 01: Try catch de logs.
---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Contract generateLicitacion(){
        System.debug('*** paso contract');
        Account acct = new Account(Name = 'Cuenta de prueba Licitacion'/*, Raz_n_social__c = 'Razon social'*/, BI_Tipo_de_identificador_fiscal__c = 'NIF',
                                   BI_No_Identificador_fiscal__c='48849657D', BI_Segment__c = 'Empresas', BI_Country__c='Argentina',
                                   BI_Subsegment_Regional__c = 'Corporate',
                                   BI_Territory__c = 'CABA',
                                   BI_Sector__c = 'Comercio',
                                   Sector__c = 'Private',
                                   BI_Subsector__c= 'Banca');
        Contract contract = new Contract();
        acct.Owner = [SELECT ID,Pais__c, name FROM user Where Pais__c <> 'Colombia' Limit 1];
        acct.OwnerId = acct.Owner.Id;
        
        contract.Account = acct;
        contract.AccountId = acct.Id;
        contract.Owner = acct.Owner;
        contract.OwnerId = acct.OwnerId;
        contract.BI_ARG_Id_Legado__c = '';
        contract.BI_GD_ID_Gestor_Documental_licitacion__c = '';
        contract.CurrencyIsoCode='CLP';
        contract.ContractTerm = 12;
        contract.BI_Divisas_del_contrato__c='CLP - Peso chileno';
        contract.CustomerSignedDate=Date.Today().addDays(10);
        Opportunity opp = new Opportunity(Name='Opp de prueba Licitaion', StageName='F6 - Prospect', CloseDate = Date.Today().addDays(10), 
                                          BI_ARG_Fecha_Sol_Elaboracion_Rta_Pliego__c = Date.newInstance(2017, 10, 11), BI_Fecha_de_vigencia__c = Date.Today().addDays(10),
                                          BI_ARG_Fecha_presentacion_de_sobres__c = Date.newInstance(2017, 10, 11), BI_ARG_Tipo_de_Contratacion__c = 'Concurso Privado',
                                          BI_ARG_Nro_de_Licitacion__c= '123456789');
        
        opp.AccountId = acct.Id;
        opp.Account = acct;
        Product2 p2 = new Product2(Name='Test Product',isActive=true);
        contract.Name = 'Licitacion de prueba';
        contract.BI_ARG_Empresa_Local__c = 'TASA';
        contract.Description = 'Observaciones';
        contract.BI_ARG_Fecha_de_Adjudicacion__c = Date.newInstance(2017, 7, 11);
        contract.BI_ARG_Complejidad__c = 'Baja';
        contract.BI_ARG_Garantia_de_Licitacion__c = 'Cheque';
        contract.BI_ARG_Nro_de_Poliza_G_Adjudicacion__c = '987654321';
        contract.BI_ARG_Fecha_Emision_Poliza_Adjudicacion__c = Date.newInstance(2017, 10, 11);
        contract.BI_ARG_Moneda_G_Adjudicacion__c = 'EUR';
        contract.BI_ARG_Monto_G_Adjudicacion__c = 10.5;
        contract.BI_ARG_Garantia_de_Oferta__c = 'Cheque';
        contract.BI_ARG_Nro_de_Poliza_G_de_Oferta__c='456123789';
        contract.BI_ARG_Monto_G_Oferta__c= 100.25;
        contract.BI_ARG_Moneda_G_Oferta__c = 'EUR';
        contract.BI_ARG_Fecha_Vencimiento_G_Oferta__c = Date.newInstance(2017, 25, 11);
        contract.BI_ARG_Moneda_Otros_Gastos__c = 'SI';
        contract.BI_ARG_Monto_Otros_Gastos__c = 125.23;
        contract.BI_ARG_Moneda_Pliego__c = 'USD';
        contract.BI_ARG_Monto_del_Pliego__c =10.36;
        contract.BI_Oportunidad__c = opp.Id;
        contract.BI_Oportunidad__r = opp;
        try{
            insert acct;
            //Ever01-INI
        //}catch(Exception exc){
            //System.debug ('Excepcion2 ' +exc.getMessage());
        //}
        //Ever01-FIN
        insert p2;
        insert opp;
        insert contract;
        PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        String standardPriceBookId = pb2Standard.Id;
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=p2.Id, UnitPrice=99, isActive=true);
        
        OpportunityLineItem oli = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=opp.Id, Quantity=2, TotalPrice=99);
        
        insert pbe;
        
        insert oli;
        System.debug('*** termino contract');
            //Ever01-INI
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_GD_Helper.generateLicitacion', 'BI_EN', exc, 'Web Service');
        }
        //Ever01-FIN
        
        return contract;
        
        
    }
    
    
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:                 Eduardo Ventura
Company:                Everis España
Description:            Método que valida la etapa en que se encuentra un contrato.

History:

<Date>                              <Author>                                <Change Description>
15/03/2017                          Eduardo Ventura                         Versión inicial.
------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static boolean getStageNameContract(Contract contracts){
        if (contracts.BI_Oportunidad__c != null) {
            Opportunity opp = [SELECT stageName FROM Opportunity  Where ID=:contracts.BI_Oportunidad__c Limit 1];
            
            if (opp.stageName == 'F1 - Ganada' || opp.stageName == 'F1 - Closed Won' ) {
                return true;
            }
            
            String etapa = opp.stageName.subString(0,2);	        
            
            if(etapa == 'F2'){             
                return true;            
            } else{
                return false;   
            }              
        } else {
            return false;
        }
    }
    
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:                 Eduardo Ventura
Company:                Everis España
Description:            Método que valida la etapa en que se encuentra una licitación

History:

<Date>                              <Author>                                <Change Description>
15/03/2017                          Eduardo Ventura                         Versión inicial.
------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static boolean getStageNameLicitacion(Contract contracts){
        if (contracts.BI_Oportunidad__c != null) {
            Opportunity opp = [SELECT stageName FROM Opportunity  Where ID=:contracts.BI_Oportunidad__c Limit 1];
            
            if (opp.stageName == 'F1 - Ganada' || opp.stageName == 'F1 - Closed Won') {
                return true; 
            }
            
            String etapa = opp.stageName.subString(0,2);	        
            
            if(etapa == 'F2' || etapa == 'F3' || etapa == 'F4' || etapa == 'F5'){             
                return true;            
            } else {
                return false;  
            }  
        } else {
            return false;
        }
    }
    
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:                        Alejandro García Olmedo
Company:                       Everis España
Description:                   Determina si en un contrato se ha seleccionado mas de una divisa.

History:

<Date>                        <Author>                                        <Change Description>
23/10/2017                    Alejandro García Olmedo                         Versión inicial.
02/11/2017                                                                    Ever 01: Try catch de logs.
10/01/2017                    Gawron, Julián E.                               Fixing exception on NullObject eHelp 03243699
---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static boolean isMulticurrencyContract (Contract contract){
        boolean result;
        
        /* Process Data */
        try{
            result = (contract != null 
                && contract.BI_Divisas_del_contrato__c != null 
                && String.valueOf(contract.BI_Divisas_del_contrato__c).contains(';')); //JEG eHelp 03243699
        }catch(Exception exc){
            //Ever01-INI
            BI_LogHelper.generate_BILog('BI_GD_Helper.isMulticurrencyContract', 'BI_EN', exc, 'Web Service');
            //result = false;
            //Ever01-FIN
        }
        
        return result;
    }
    
}