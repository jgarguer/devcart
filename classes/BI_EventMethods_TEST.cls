@isTest
private class BI_EventMethods_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_EventMethods class 
    
    History: 
    
    <Date> 					<Author> 				<Change Description>
    14/11/2014      		Pablo Oliva	    		Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static{
    	BI_TestUtils.throw_exception = false;
    }
    
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Pablo Oliva
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_EventMethods.validateContact
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	14/11/2014      		Pablo Oliva				Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/		    
    static testMethod void validateContactTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        User user = [select Id, Pais__c from User where Id = :UserInfo.getUserId() limit 1];
        
        user.Pais__c = Label.BI_Ecuador;
        update user;
        
        system.runas(user){
        
	    	List <String> lst_region = BI_DataLoad.loadPaisFromPickList(1);
	        
	        //BI_Pickup_Option__c pickupOption = BI_DataLoadRest.loadPickUpOptions(lst_region);          							   			 
	        
	        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
	        
	        List <Event> lst_event = new List<Event>();
	        
	        for(Contact con:BI_DataLoadRest.loadContacts(200, lst_acc)){
	        	Event event = new Event(WhatId = lst_acc[0].Id,
	        							StartDateTime = DateTime.now(),
	        							EndDateTime = DateTime.now().addDays(1),
	        							Subject = 'Test'+con.Id);
	        							
	        	lst_event.add(event);
	        }
	        
	        Test.startTest();
	        
	        try{
	        	insert lst_event;
	        	system.assert(false);
	        }catch(Exception exc){
	        	system.assertEquals(true, exc.getMessage().contains(Label.BI_ContactoEvento));
	        }
	        
	        Test.stopTest();
	        
        }   
        
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Test method of BI_EventMethods.check_HasClosedPlanDeAccion()	
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    03/02/2015                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void check_HasClosedPlanDeAccion_Test() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		BI_Dataload.set_ByPass(false,false);
		/*
		Region__c chile = new Region__c(Name = Label.BI_Chile,
									  BI_Codigo_ISO__c = 'CHL');
		insert chile;
		*/
		String chile = Label.BI_Chile;
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{chile});
		system.assert(!lst_acc.isEmpty());

		List<BI_Plan_de_accion__c> lst_coord = BI_Dataload.loadCustomPlanDeAccion(2, lst_acc[0]);
		system.assert(!lst_coord.isEmpty());

		List<Id> whatId = new List<Id>();
		for(BI_Plan_de_accion__c coo :lst_coord){
		    whatId.add(coo.Id);
		    coo.BI_Estado__c = Label.BI_Finalizado;
		    coo.BI_Casilla_desarrollo__c = true;
		    coo.BI_Solucion_Motivo_de_cierre__c ='motivo cierre test';
		}
		List<Recordtype> lst_rt_event = [SELECT ID, sObjectType  FROM RecordType WHERE Developername  = 'BI_Asistentes_Plan_de_accion'];

		List<Event> lst_event = BI_Dataload.loadEventWithRT(1,lst_rt_event, whatId );
		system.assert(!lst_event.isEmpty());

		update lst_coord;

		Test.StartTest();

		for(Event evnt :lst_event){
			evnt.Subject = 'update Subject '+ String.valueOf(Math.random() *100000);	
		}

		try{
			update lst_event;
			system.assert(false);
		}catch(Exception exc){
			system.assert(exc.getMessage().contains(Label.BI_Msg_NoModificarSiActionPlanCerrado));			
		}

		Test.StopTest();
		
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Test method of BI_EventMethods.preventEventForActionPlan()	
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    04/02/2015                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void preventEventForActionPlan_test(){
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
			/*
			BI_Dataload.set_ByPass(false,false);
			Region__c chile = new Region__c(Name = Label.BI_Chile,
										  BI_Codigo_ISO__c = 'CHL');
			insert chile;
			*/
			String chile = Label.BI_Chile;
			List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{chile});
			system.assert(!lst_acc.isEmpty());

			List<BI_Plan_de_accion__c> lst_coord = BI_Dataload.loadCustomPlanDeAccion(2, lst_acc[0]);
			system.assert(!lst_coord.isEmpty());

			List<Id> whatId = new List<Id>();
			for(BI_Plan_de_accion__c coo :lst_coord){
			    whatId.add(coo.Id);
			}
			List<Recordtype> lst_rt_event = [SELECT ID, sObjectType  FROM RecordType WHERE Developername  = 'BI_Asistentes_Plan_de_accion'];

			List<Event> lst_event = BI_Dataload.loadEventWithRT(1,lst_rt_event, whatId);
			system.assert(!lst_event.isEmpty());
			

			try{
				List<Event> lst_event2 = BI_Dataload.loadEventWithRT(2,lst_rt_event, whatId);
				system.assert(false);
			}catch(Exception exc){
				system.assert(exc.getMessage().contains(Label.BI_Eventos_Plan_Accion_Masivo));
			}
		
	}
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Aborda
    Description:   Test method of BI_EventMethods.updateBid_Event()	
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    17/01/2017                      Alvaro García                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	static void updateBid_Event_test(){

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		String argentina = Label.BI_Argentina;
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{argentina});

		List<Opportunity> lst_opp = BI_DataLoad.loadOpportunities(lst_acc[0].Id);

		System.debug('updateBid_Event_test');
		Quote quote = new Quote(Name = 'quote-001', Status = 'Working on proposal', OpportunityId = lst_opp[0].Id);
	   	insert quote; 
		System.debug('updateBid_Event_test');
	   	BI_O4_Thursday_Limit__c thursdayLimit = new BI_O4_Thursday_Limit__c(Name = 'Number of Days', BI_O4_Event_Day__c = 'Thursday', BI_O4_Need_Day_Restriction__c = true,
	   																		BI_O4_Number_of_days__c = 180, BI_O4_Slot_Timing__c = 15);
	   	insert thursdayLimit;
        System.debug('updateBid_Event_test');
		Id rt_approval = [SELECT Id, sObjectType  FROM RecordType WHERE Developername  = 'BI_O4_Gate_3' LIMIT 1].Id;

		BI_O4_Approvals__c app = new BI_O4_Approvals__c(BI_O4_Opportunity__c = lst_opp[0].Id, BI_O4_Proposal__c = quote.Id, RecordTypeId = rt_approval, 
												BI_O4_Gate_request_date__c = System.today().addDays(15), BI_O4_Customer_submission_date__c = System.today().addDays(30), 
												BI_O4_Financial_Gate_Request_comments__c = 'test', BI_O4_DRB_requested_date__c = System.today().addDays(20), 
												BI_O4_DRB_Request_Status__c = 'Slot Reject', BI_O4_Reasons_for_DRB_slot_rejection__c = 'Not enough information', BI_O4_Reason_for_request__c = 'Gate 3 – financial sign off', BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(14));
		insert app;

		Event ev = [SELECT Id, StartDateTime, BI_O4_DRB_Confirmed_Date__c, BI_O4_DRB_Request_Status__c, OwnerID, BI_O4_Record_type__c, BI_O4_fromBID__c, WhatId, BI_O4_Reasons_for_DRB__c
					FROM Event WHERE WhatId = :app.Id LIMIT 1];


		System.debug('updateBid_Event_test ev' + ev);			


		Test.startTest();
		    //ev.OwnerId = '02326000002ODt3';//No asigna el calendario como owner 

			ev.StartDateTime = System.now().addDays(29);
			System.debug('updateBid_Event_test antes up date ev' + ev);
			update ev;
			Event ev1 =[SELECT Id, StartDateTime, BI_O4_DRB_Confirmed_Date__c, BI_O4_DRB_Request_Status__c, OwnerID, BI_O4_Record_type__c, BI_O4_fromBID__c, WhatId, BI_O4_Reasons_for_DRB__c
					FROM Event WHERE Id = :ev.Id LIMIT 1];
		    System.debug('updateBid_Event_test ev1 ' + ev1);

			BI_O4_Approvals__c app_aux = [SELECT BI_O4_DRB_confirmed_DateTime__c FROM BI_O4_Approvals__c WHERE Id = :ev.WhatId LIMIT 1];
			//System.assertEquals(ev.StartDateTime, app_aux.BI_O4_DRB_confirmed_DateTime__c); // JAG Test Errors
			System.assert(ev.StartDateTime >= app_aux.BI_O4_DRB_confirmed_DateTime__c); // JAG Test Errors

			ev.BI_O4_DRB_Confirmed_Date__c = System.now().addDays(22);
			update ev;

			app_aux = [SELECT BI_O4_DRB_confirmed_DateTime__c FROM BI_O4_Approvals__c WHERE Id = :ev.WhatId LIMIT 1];
			//System.assertEquals(ev.BI_O4_DRB_Confirmed_Date__c, app_aux.BI_O4_DRB_confirmed_DateTime__c); // JAG Test Errors
			System.assert(ev.BI_O4_DRB_Confirmed_Date__c >= app_aux.BI_O4_DRB_confirmed_DateTime__c); // JAG Test Errors
			

			ev.BI_O4_DRB_Request_Status__c = 'Slot Reject';
			ev.BI_O4_Reasons_for_DRB__c = 'Not enough information';
			update ev;
			
			app_aux = [SELECT BI_O4_Reasons_for_DRB_slot_rejection__c, BI_O4_DRB_request_status__c FROM BI_O4_Approvals__c WHERE Id = :ev.WhatId LIMIT 1];
			System.assertEquals(ev.BI_O4_DRB_Request_Status__c, app_aux.BI_O4_DRB_request_status__c);
			System.assertEquals(ev.BI_O4_Reasons_for_DRB__c, app_aux.BI_O4_Reasons_for_DRB_slot_rejection__c);
		
		Test.stopTest();
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Aborda
    Description:   Test method of BI_EventMethods.updateEvent_Dates()	
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    18/01/2017                      Alvaro García                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	static void updateEvent_Dates_test(){

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		String argentina = Label.BI_Argentina;
		List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{argentina});

		List<Opportunity> lst_opp = BI_DataLoad.loadOpportunities(lst_acc[0].Id);

		Quote quote = new Quote(Name = 'quote-001', Status = 'Working on proposal', OpportunityId = lst_opp[0].Id);
	   	insert quote; 

	   	BI_O4_Thursday_Limit__c thursdayLimit = new BI_O4_Thursday_Limit__c(Name = 'Number of Days', BI_O4_Event_Day__c = 'Thursday', BI_O4_Need_Day_Restriction__c = true,
	   																		BI_O4_Number_of_days__c = 180, BI_O4_Slot_Timing__c = 15);
	   	insert thursdayLimit;

		Id rt_approval = [SELECT Id, sObjectType  FROM RecordType WHERE Developername  = 'BI_O4_Gate_3' LIMIT 1].Id;

		BI_O4_Approvals__c app = new BI_O4_Approvals__c(BI_O4_Opportunity__c = lst_opp[0].Id, BI_O4_Proposal__c = quote.Id, RecordTypeId = rt_approval, 
												BI_O4_Gate_request_date__c = System.today().addDays(15), BI_O4_Customer_submission_date__c = System.today().addDays(30), 
												BI_O4_Financial_Gate_Request_comments__c = 'test', BI_O4_DRB_requested_date__c = System.today().addDays(20), 
												BI_O4_Reason_for_request__c = 'Gate 3 – financial sign off', BI_O4_DRB_confirmed_DateTime__c = System.now().addDays(14));
		insert app;

		Event ev = [SELECT Id, StartDateTime, BI_O4_DRB_Confirmed_Date__c, BI_O4_DRB_Request_Status__c, OwnerID, BI_O4_Record_type__c, BI_O4_fromBID__c, WhatId, BI_O4_Reasons_for_DRB__c
					FROM Event WHERE WhatId = :app.Id LIMIT 1];

		Test.startTest();
			
			ev.StartDateTime = System.now().addDays(29);
			ev.EndDateTime = System.now().addDays(29);
			update ev;
		
		Test.stopTest();
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Aborda
    Description:   Test to covert the exceptions	
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    18/01/2017                      Alvaro García               Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest
	static void exception_test(){

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_TestUtils.throw_exception = true;

		Test.startTest();
			
			BI_EventMethods.preventEventForActionPlan(null);
			BI_EventMethods.check_HasClosedPlanDeAccion(null);
			BI_EventMethods.updateBid_Event(null, null);
			BI_EventMethods.updateEvent_Dates(null, null);
		    BI_EventMethods.updateAccOfertaVisita(null, null); //JEG

		Test.stopTest();
	}
}