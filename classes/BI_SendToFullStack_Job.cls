/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Victoria Gutierrez
    Company:       Accenture - New Energy Aborda
    Description:   Send objects account, contacts and sites to FullStack
    
  
    
    <Date>            <Author>                              <Description>
    06/06/2017        Victoria Gutierrez                    Initial version.
    27/06/2017		  Victoria Gutierrez          			Versión 2
    04/09/2017		  Guillermo Muñoz						Class overwrited
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public without sharing class BI_SendToFullStack_Job implements Queueable, Database.AllowsCallouts {

	private Map<Id,BI_HierarchyNode> nodos; 
	private Map<Id,Account> customers = new Map<Id,Account>();
	private Map<Id,Contact> contacts = new Map<Id,Contact>();
	private Map<Id,String> sites = new Map<Id,String>();
	private Map<Id,Id> mapa = new Map<Id,Id>();
	private List<BI_Sede__c> sedes = new List<BI_Sede__c>();

	Boolean onlyContacts = false;

	Map <Id, Account> map_holdingToProcess;
    Map <Id, Account> map_customerCountryToProcess;
    Map <Id, Account> map_legalEntityToProcess;
    Map <Id, List <Contact>> map_contacts;
    Map <Id, List <BI_Punto_de_instalacion__c>> map_sites;
    Account currentAccount;
    Map <Id, String> map_idParents;
    Id idCurrentAccount;
	

	public BI_SendToFullStack_Job(Map<Id,BI_HierarchyNode> nodos){
		System.debug('BI_SendToFullStack_Job-->BI_SendToFullStack_Job Constructor del batch');
		this.nodos = nodos;
		//normal = true;
	}
	public BI_SendToFullStack_Job (Map <Id, List <Contact>> map_contacts, Id idCurrentAccount){

		this.onlyContacts = true;
		this.idCurrentAccount = idCurrentAccount;
		this.map_contacts = map_contacts;
	}

	public BI_SendToFullStack_Job(Map <Id, Account> map_holding, Map <Id, Account> map_customerCountry, Map <Id, Account> map_legalEntity, Map <Id, List <Contact>> map_contact, Map <Id, List <BI_Punto_de_instalacion__c>> map_site, Account currentAccount, Map <Id, String> map_idParents){
		this.map_holdingToProcess = map_holding;
		this.map_customerCountryToProcess =  map_customerCountry;
		this.map_legalEntityToProcess =  map_legalEntity;
		this.map_contacts =  map_contact;
		this.map_sites =  map_site;
		this.currentAccount = currentAccount;
		this.map_idParents = map_idParents;
	}

	public void execute(QueueableContext context) {

		if(!onlyContacts){
			//Tratamiento de cuentas
			if(currentAccount == null){
				//Primero se sincronizan los holdings
				if(map_holdingToProcess != null){

					currentAccount = map_holdingToProcess.remove(map_holdingToProcess.values()[0].Id);

					if(map_holdingToProcess.isEmpty()){
						map_holdingToProcess = null;
					}
				}
				//Los Customer Countries se sincronizan despues de los holdings
				else if(map_customerCountryToProcess != null){

					currentAccount = map_customerCountryToProcess.remove(map_customerCountryToProcess.values()[0].Id);

					if(map_customerCountryToProcess.isEmpty()){
						map_customerCountryToProcess = null;
					}
				}
				//Las Legal Entities se sincronizan las últimas
				else if(map_legalEntityToProcess != null){

					currentAccount = map_legalEntityToProcess.remove(map_legalEntityToProcess.values()[0].Id);

					if(map_legalEntityToProcess.isEmpty()){
						map_legalEntityToProcess = null;
					}
				}
				//Una vez seleccionada la cuenta
				if(currentAccount != null){

					String parentExternalId;
					//Sacamos el id externo del padre que se sincronizó previamente o el valor null si se trata de un holding
					if(map_idParents != null && map_idParents.containsKey(currentAccount.ParentId)){
						parentExternalId = map_idParents.get(currentAccount.ParentId);
					}
					//Si el padre estaba previamente sincronizado sacamos su id externo, dado que al no haberse sincronizado en esta ejecución no tenemos el valor en el mapa
					if(parentExternalId == null && currentAccount.RecordType.DeveloperName != Constants.RECORD_TYPE_TGS_HOLDING && currentAccount.Parent.BI_Identificador_Externo__c != null){
						parentExternalId = currentAccount.Parent.BI_Identificador_Externo__c;
					}
					//Si no tenemos id externo del padre y no se trata de un holding o si tenemos id externo del padre y se trata da un holding no se debe lanzar la sincronización debido a que fallará
					if(!(parentExternalId == null && currentAccount.RecordType.DeveloperName != Constants.RECORD_TYPE_TGS_HOLDING) || !(parentExternalId != null && currentAccount.RecordType.DeveloperName == Constants.RECORD_TYPE_TGS_HOLDING)){
						System.debug('!!@@ Entro envío de cliente');
						BI_RestRequestHelper.createCustomerInFullStack(currentAccount, parentExternalId);
					}
					//Si se ha sincronizado de forma correcta, guardamos el id externo generado en el mapa para utilizarlo en futuras ejecuciones
					if(currentAccount.BI_Identificador_Externo__c != null){

						if(map_idParents != null){

							map_idParents.put(currentAccount.Id, currentAccount.BI_Identificador_Externo__c);
						}
						else{

							map_idParents = new Map <Id, String>{currentAccount.Id => currentAccount.BI_Identificador_Externo__c};
						}

						//Volvemos a encolar la clase para que prosiga con el tratamiento de las sedes y los contactos
						if((map_contacts != null && map_contacts.containsKey(currentAccount.Id)) || (map_sites != null && map_sites.containsKey(currentAccount.Id))){
							System.debug('!!@@ relanzando en 1');
							if(!Test.isRunningTest()){
								System.enqueueJob(new BI_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents));
							}
						}
						else if(map_holdingToProcess != null || map_customerCountryToProcess != null || map_legalEntityToProcess != null){

							System.debug('!!@@ relanzando en 6');
							if(!Test.isRunningTest()){
								System.enqueueJob(new BI_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, null, map_idParents));
							}
						}

						

					}
					//Si el cliente no se sincronizó con éxito, borramos la clave del cliente de los mapas de contactos y sites y seguimos procesando el siguiente cliente, si lo hubiere
					else{

						System.debug('!!@@ Fallo de sincronización cliente ----> ' + currentAccount.Id);

						if(map_contacts != null && map_contacts.containsKey(currentAccount.Id)){

							map_contacts.remove(currentAccount.Id);
							
							if(map_contacts.isEmpty()){
								map_contacts = null;
							}
						}

						if(map_sites != null && map_sites.containsKey(currentAccount.Id)){

							map_sites.remove(currentAccount.Id);
							
							if(map_sites.isEmpty()){
								map_sites = null;
							}
						}
						
						if(map_holdingToProcess != null || map_customerCountryToProcess != null || map_legalEntityToProcess != null){
							System.debug('!!@@ relanzando en 2');
							if(!Test.isRunningTest()){
								System.enqueueJob(new BI_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, null, map_idParents));
							}
						}
					}
					
				}			
			}
			//Tratamiento de contactos y sites
			else{
				//Tratamiento de contactos
				if(map_contacts != null && map_contacts.containsKey(currentAccount.Id)){

					Boolean isLastContact = false;
					//Cogemos el contacto a sincronizar
					Contact currentContact = map_contacts.get(currentAccount.Id).remove(0);
					//Si es el último contacto para el cliente, borramos la clave del cliente del mapa
					if(map_contacts.get(currentAccount.Id).isEmpty()){

						map_contacts.remove(currentAccount.Id);
						isLastContact = true;
						//Si no hay mas claves de clientes en el mapa, lo igualamos a null para que sea mas sencillo de procesar
						if(map_contacts.isEmpty()){
							map_contacts = null;
						}
					}

					String parentExternalId;
					//Buscamos el id externo de la cuenta asociada al contacto
					if(map_idParents != null && map_idParents.containsKey(currentAccount.Id)){
						parentExternalId = map_idParents.get(currentAccount.Id);
					}
					//Si la cuenta se sincronizó con éxito, enviamos el contacto
					if(parentExternalId != null){
						System.debug('!!@@ Entro envío de contacto');
						BI_RestRequestHelper.createContactInFullStack(currentContact, parentExternalId);
					}
					//Si se trata del último contacto de la lista y el cliente no tiene sites asociados, pasamos a procesar el siguiente cliente, si los hubiere
					if(isLastContact && (map_sites == null || (map_sites != null && !map_sites.containsKey(currentAccount.Id)))){

						if(map_holdingToProcess != null || map_customerCountryToProcess != null || map_legalEntityToProcess != null){
							System.debug('!!@@ relanzando en 3');
							if(!Test.isRunningTest()){
								System.enqueueJob(new BI_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, null, map_idParents));
							}
						}
					}
					//Sino seguirmos procesando los contactos y sites restantes
					else{
						System.debug('!!@@ relanzando en 4');
						if(!Test.isRunningTest()){
							System.enqueueJob(new BI_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents));
						}
					}
				}
				//Tratamiento de sedes
				else if(map_sites != null && map_sites.containsKey(currentAccount.Id)){

					Boolean isLastSite = false;
					//Cogemos el site a sincronizar
					BI_Punto_de_instalacion__c currentSite = map_sites.get(currentAccount.Id).remove(0);
					//Si es el último site para el cliente, borramos la clave del cliente del mapa
					if(map_sites.get(currentAccount.Id).isEmpty()){

						map_sites.remove(currentAccount.Id);
						isLastSite = true;
						//Si no hay mas claves de clientes en el mapa, lo igualamos a null para que sea mas sencillo de procesar
						if(map_sites.isEmpty()){
							map_sites = null;
						}
					}

					String parentExternalId;
					//Buscamos el id externo de la cuenta asociada al site
					if(map_idParents != null && map_idParents.containsKey(currentAccount.Id)){
						parentExternalId = map_idParents.get(currentAccount.Id);
					}
					//Si la cuenta se sincronizó con éxito, enviamos el Site
					if(parentExternalId != null){
						System.debug('!!@@ Entro envío de site');
						BI_RestRequestHelper.createSiteInFullStack(currentSite, parentExternalId);
					}

					//Si es el último site
					if(isLastSite){
						//Si quedan clientes por procesar, pasamos a procesar el siguiente cliente
						if(map_holdingToProcess != null || map_customerCountryToProcess != null || map_legalEntityToProcess != null){
							System.debug('!!@@ relanzando en 4');
							if(!Test.isRunningTest()){
								System.enqueueJob(new BI_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, null, map_idParents));
							}
						}
					}
					//Si no es el último Site a procesar, seguimos procesando los sites
					else{
						System.debug('!!@@ relanzando en 5');
						if(!Test.isRunningTest()){
							System.enqueueJob(new BI_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents));
						}
					}
				}
				else{

					if(map_holdingToProcess != null || map_customerCountryToProcess != null || map_legalEntityToProcess != null){
						System.debug('!!@@ relanzando en 5');
						if(!Test.isRunningTest()){
							System.enqueueJob(new BI_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, null, map_idParents));
						}
					}
				}
			}
		}
		else{
		
			if(idCurrentAccount == null && map_contacts != null){

				idCurrentAccount = new List <Id>(map_contacts.keySet())[0];
				System.debug('!!@@ idAccount ---> ' + idCurrentAccount);
			}

			Boolean isLastContact = false;

			Contact currentContact = map_contacts.get(idCurrentAccount).remove(0);

			if(map_contacts.get(idCurrentAccount).isEmpty()){

				map_contacts.remove(idCurrentAccount);
				isLastContact = true;
				//Si no hay mas claves de clientes en el mapa, lo igualamos a null para que sea mas sencillo de procesar
				if(map_contacts.isEmpty()){
					map_contacts = null;
				}
			}

			String externalId = currentContact.Account.BI_Identificador_Externo__c;

			if(externalId != null){
				System.debug('!!@@ Entro envío de contacto ---- ' + externalId);
				BI_RestRequestHelper.createContactInFullStack(currentContact, externalId);
			}

			if(isLastContact && map_contacts != null){
				System.debug('!!@@ relanzando solo contactos en 1');
				if(!Test.isRunningTest()){
					System.enqueueJob(new BI_SendToFullStack_Job(map_contacts, null));
				}
			}
			else if(map_contacts != null){
				System.debug('!!@@ relanzando solo contactos en 2');
				if(!Test.isRunningTest()){
					System.enqueueJob(new BI_SendToFullStack_Job(map_contacts, idCurrentAccount));
				}
			}		

		}
	}//End execute
 }