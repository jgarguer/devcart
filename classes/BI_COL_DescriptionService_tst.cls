/**
* 
* @author           
* Proyect:          Telefonica
* Description:      Test class 
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.1    2016-12-28      Gawron, Julián E.       Add testSetup
*            1.1    23-02-2017      Jaime Regidor           Adding Campaign Values, Adding Catalog Country
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
*                   20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/

@isTest
private class BI_COL_DescriptionService_tst {

    Public static User                                          objUsuario = new User();
    public static Account                                   objCuenta;
    public static Account                                   objCuenta2;
    public static Contact                                   objContacto;
    public static Campaign                                  objCampana;
    public static Contract                                  objContrato;
    public static BI_COL_Anexos__c                          objAnexos;
    public static Opportunity                               objOportunidad;
    public static BI_Col_Ciudades__c                        objCiudad;
    public static BI_Sede__c                                objSede;
    public static BI_Punto_de_instalacion__c                objPuntosInsta;
    public static BI_COL_Descripcion_de_servicio__c         objDesSer;
    public static BI_COL_Modificacion_de_Servicio__c        objMS;
    public static BI_COL_GenerateOT_failed_ctr.WrapperMS    objWrapperMS;
    public static BI_COL_Homologacion_integraciones__c      objHomolog;
    
    public static List <Profile>                            lstPerfil;
    public static List <User>                               lstUsuarios;
    public static List <UserRole>                           lstRoles;
    public static List<Campaign>                            lstCampana;
    public static List<BI_COL_manage_cons__c >              lstManege;
    public static List<BI_COL_Modificacion_de_Servicio__c>  lstMS;
    public static List<BI_COL_Descripcion_de_servicio__c>   lstDS;
    public static List<recordType>                          lstRecTyp;
    public static List<BI_COL_GenerateOT_failed_ctr.WrapperMS>    lstWrapperMS;
    public static List<BI_COL_Anexos__c>                    lstAnexos;
    public static List<Opportunity>                         lstOpp;

    public boolean todosMarcados {get;set;}
    public boolean habilitarEnvio {get;set;}
    public boolean showPage{get; set;}

    public Decimal decTotalServiciosMs; 
    public String idFUN;
    public Integer newPageIndex;
    
    @testSetup public static void CrearData() 
    {

        //perfiles
        lstPerfil                       = new List <Profile>();
        lstPerfil                       = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
        system.debug('datos Perfil '+lstPerfil);
                
        //usuarios
        //lstUsuarios = [Select Id, CompanyName FROM User where Pais__c = 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];
        //system.debug('\n @-->datos Usuario '+lstUsuarios);

        //lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        
       // objUsuario = new User();
       // objUsuario.Alias = 'standt';
       // objUsuario.Email ='pruebas@test.com';
       // objUsuario.EmailEncodingKey = '';
       // objUsuario.LastName ='Testing';
       // objUsuario.LanguageLocaleKey ='en_US';
       // objUsuario.LocaleSidKey ='en_US'; 
       // objUsuario.ProfileId = lstPerfil.get(0).Id;
       // objUsuario.TimeZoneSidKey ='America/Los_Angeles';
       // objUsuario.UserName ='pruebas@test.com';
       // objUsuario.EmailEncodingKey ='UTF-8';
       // objUsuario.UserRoleId = lstRoles.get(0).Id;
       // objUsuario.BI_Permisos__c ='Sucursales';
       // objUsuario.Pais__c='Colombia';
        
        objUsuario = BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);
        System.runAs([Select Id FROM User Where Id =:UserInfo.getUserId()][0]){
        insert objUsuario;
        }

        lstUsuarios = new list<User>();
        lstUsuarios.add(objUsuario);

        System.runAs(lstUsuarios[0])
        {     

            BI_bypass__c objBibypass = new BI_bypass__c();
            objBibypass.BI_migration__c=true;
            insert objBibypass;
            
            //Roles
            lstRoles                = new List <UserRole>();
            lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
            system.debug('datos Rol '+lstRoles);

            objHomolog                                  = new  BI_COL_Homologacion_integraciones__c();
            objHomolog.BI_COL_Descripcion_referencia__c     = 'Prueba';
            //objHomolog.CurrencyIsoCode                        = 'EUR - Euro';
            objHomolog.BI_COL_Viaja_TRS__c                  = true;
            insert objHomolog;
            
            //Cuentas
            objCuenta                                       = new Account();      
            objCuenta.Name                                  = 'prueba';
            objCuenta.BI_Country__c                         = 'Argentina';
            objCuenta.TGS_Region__c                         = 'América';
            objCuenta.BI_Tipo_de_identificador_fiscal__c    = '654321';
            objCuenta.BI_No_Identificador_fiscal__c         = '123456';
            objCuenta.CurrencyIsoCode                       = 'COP';
            objCuenta.BI_Fraude__c                          = false;
            objCuenta.AccountNumber                         = '123456';
            //objCuenta.BillingAddress                      = 'dirprueba';
            objCuenta.Phone                                 = '1233421';
            objCuenta.BI_Segment__c                         = 'test';
            objCuenta.BI_Subsegment_Regional__c             = 'test';
            objCuenta.BI_Territory__c                       = 'test';
            insert objCuenta;
            system.debug('datos Cuenta '+objCuenta);

            //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);
            
            //Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
            objContacto.BI_Country__c                       = 'Argentina';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objCuenta.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
            objContacto.Phone                               = '1234567';
            objContacto.MobilePhone                         = '3104785925';
            objContacto.Email                               = 'pruebas@pruebas1.com';
            objContacto.BI_COL_Direccion_oficina__c         = 'Dirprueba';
            objContacto.BI_COL_Ciudad_Depto_contacto__c     = objCiudad.Id;
            
			//REING-INI
            //objContacto.BI_Tipo_de_contacto__c          = 'Autorizado';
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
       		objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
            objContacto.BI_Tipo_de_contacto__c          = 'General';
       		//REING_FIN

            Insert objContacto;
            
            //catalogo
            NE__Catalog__c objCatalogo                      = new NE__Catalog__c();
            objCatalogo.Name                                = 'prueba Catalogo';
            objCatalogo.BI_country__c                       = 'Argentina';
            insert objCatalogo; 
            
            //Campaña
            objCampana                                      = new Campaign();
            objCampana.Name                                 = 'Campaña Prueba';
            objCampana.BI_Catalogo__c                       = objCatalogo.Id;
            objCampana.BI_COL_Codigo_campana__c             = 'prueba1';
            objCampana.BI_Country__c                        = 'Argentina';
            objCampana.BI_Opportunity_Type__c               = 'Fijo';
            objCampana.BI_SIMP_Opportunity_Type__c          = 'Alta';
            insert objCampana;
            lstCampana                                      = new List<Campaign>();
            lstCampana                                      = [select Id, Name from Campaign where Id =: objCampana.Id];
            system.debug('datos Cuenta '+lstCampana);
            
            //Tipo de registro
            lstRecTyp                                       = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
            system.debug('datos de FUN '+lstRecTyp);
            
            //Contrato 
            objContrato                                     = new Contract ();
            objContrato.AccountId                           = objCuenta.Id;
            objContrato.StartDate                           =  System.today().addDays(+5);
            objContrato.BI_COL_Formato_Tipo__c              = 'Condiciones Generales';
            objContrato.BI_COL_Tipo_contrato__c             = 'Contrato Telefónica';
            objContrato.BI_COL_Presupuesto_contrato__c      = 1300000;
            objContrato.StartDate                           = System.today().addDays(+5);
            objContrato.BI_Indefinido__c                    = 'Si';
            objContrato.BI_COL_Monto_ejecutado__c           = 5.00;
            objContrato.ContractTerm                        = 12;
            objContrato.BI_COL_Duracion_Indefinida__c       = true;
            objContrato.Name                                = 'prueba';
            objContrato.BI_COL_Cuantia_Indeterminada__c     = true;
        
            insert objContrato;
            Contract objCont = [select ContractNumber, BI_COL_Saldo_contrato__c from Contract limit 1];

            System.debug('datos Contratos ===>'+objContrato);
            
            //FUN
            objAnexos                                       = new BI_COL_Anexos__c();
            objAnexos.Name                                  = 'FUN-0041414';
            objAnexos.RecordTypeId                          = lstRecTyp[0].Id;
            objAnexos.BI_COL_Contrato__c                    = objContrato.Id;
            objAnexos.BI_COL_Estado__c                      = 'Activo';
            insert objAnexos;
            System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
            
            //Configuracion Personalizada
            BI_COL_manage_cons__c objConPer                 = new BI_COL_manage_cons__c();
            objConPer.Name                                  = 'Viabilidades';
            objConPer.BI_COL_Numero_Viabilidad__c           = 46;
            objConPer.BI_COL_ConsProyecto__c                = 1;
            objConPer.BI_COL_ValorConsecutivo__c            =1;
            lstManege                                       = new List<BI_COL_manage_cons__c >();        
            lstManege.add(objConPer);
            insert lstManege;
            System.debug('======= configuracion personalizada ======= '+lstManege);
            
            //Oportunidad
            objOportunidad                                          = new Opportunity();
            objOportunidad.Name                                     = 'prueba opp';
            objOportunidad.AccountId                                = objCuenta.Id;
            objOportunidad.BI_Country__c                            = 'Argentina';
            objOportunidad.CloseDate                                = System.today().addDays(+5);
            objOportunidad.StageName                                = 'F6 - Prospecting';
            objOportunidad.CurrencyIsoCode                          = 'COP';
            objOportunidad.Certa_SCP__contract_duration_months__c   = 12;
            objOportunidad.BI_Plazo_estimado_de_provision_dias__c   = 0 ;
            objOportunidad.BI_ARG_CAPEX_total__c                    = 5.00;
            objOportunidad.OwnerId                                  = lstUsuarios[0].id;
            insert objOportunidad;
            system.debug('Datos Oportunidad'+objOportunidad);
            List<Opportunity> lstOportunidad    = new List<Opportunity>();
            lstOportunidad                      = [select Id, BI_Numero_id_oportunidad__c  from Opportunity];
            system.debug('datos ListaOportunida '+lstOportunidad);
            
            
            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);
            
            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name                 ='Colombia vs Peru';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);
            
            //DS
            objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
            objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
            objDesSer.CurrencyIsoCode                           = 'COP';
            //objDesSer.Name                                        = 'prueba';
            insert objDesSer;
            System.debug('Data DS ====> '+ objDesSer);
            System.debug('Data DS ====> '+ objDesSer.Name);
            lstDS   = [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
            System.debug('Data lista DS ====> '+ lstDS);
            
            //MS
            objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
            objMS.BI_COL_FUN__c                         = objAnexos.Id;
            objMS.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
            objMS.BI_COL_Bloqueado__c                   = false;
            objMS.BI_COL_Estado__c                      = 'Activa';//label.BI_COL_lblActiva;
            objMS.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objMs.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            objMs.BI_COL_Medio_Preferido__c             = 'Cobre';
            objMS.BI_COL_Delta_modificacion_upgrade__c  = 12;
            objMS.BI_COL_Clasificacion_Servicio__c      = 'SUSPENSION TEMPORAL';
            objMS.BI_COL_TRM__c                         = 5;
            objMS.BI_COL_Medio_Vendido__c               = 'Cobre';
            objMS.BI_COL_Ingeniero_implantador__c       = 'ALDO FRANCISCO HERNADEZ';
            objMS.BI_COL_Fecha_comprometida_baja_tecnica__c  = null;
            objMS.BI_COL_Causal_aplazamiento__c         = 'Técnico - Recursos monetarios';
            objMS.BI_COL_Numero_Abonado_Alternativo_1__c = '23';
            objMS.BI_COL_Routing_Number__c               = '25';
            objMS.BI_COL_Parametros_tecnicos__c          = 'Parametos+parametro+prueba';
            objMS.BI_COL_Ingeniero_instalaciones__c      = 'Jorge Int';
            objMS.BI_COL_Garantia_Equipo__c              = '12';
            objMS.BI_COL_Serial__c                       = '0003';
            objMS.BI_COL_Marca__c                        = 'Marca';
            objMS.BI_COL_Modelo_Pta_Origen__c            = 'Modelo';
            objMS.BI_COL_IMEI__c                         = 'IMEI';
            objMS.BI_COL_Numero_telefonico__c            = '7737121098';
            insert objMS;
            system.debug('Data objMs ===>'+objMs);
            BI_COL_Modificacion_de_Servicio__c objr = [select BI_COL_Monto_ejecutado__c,BI_COL_Numero_Abonado_Alternativo_1__c,BI_COL_Routing_Number__c from BI_COL_Modificacion_de_Servicio__c where id =: objMS.id];
            system.debug('\n@-->Data consulta ===>'+objr);  
        
        }
    } 
    
    @isTest static void test1() 
    {
       // CrearData();
        objMS = [select Id from BI_COL_Modificacion_de_Servicio__c limit 1];
        objOportunidad = [select Id,Name,AccountId,BI_Country__c,CloseDate,
                              StageName, CurrencyIsoCode, Certa_SCP__contract_duration_months__c, BI_Plazo_estimado_de_provision_dias__c,
                              BI_ARG_CAPEX_total__c, OwnerId, BI_Numero_id_oportunidad__c
         from Opportunity limit 1];

        BI_COL_Modificacion_de_Servicio__c MS =[select Name, BI_COL_Codigo_unico_servicio__r.Name,BI_COL_Estado__c,BI_COL_Oportunidad__c from BI_COL_Modificacion_de_Servicio__c where id=:objMS.id limit 1];
        System.debug('Limites: '+MS+'  '+Limits.getQueries());
        
        //Metodos
        BI_COL_DescriptionService_ws.ActualizarInstalacionDS(MS.BI_COL_Codigo_unico_servicio__r.Name, 'Cancelada', string.ValueOf(System.today()), string.ValueOf(System.today()), 'Tecnico - Recursos monetarios', string.ValueOf(System.today()), string.ValueOf(System.today()), 'ALDO FRANCISCO HERNADEZ','123','0180023445+Linea:134567+BW:120 MB+Nodo:NOC_01','Parametos+parametro+prueba','JOSE','PB'+objOportunidad.AccountId,'COBRE', 'MedioDestinoPrueba',MS.name,string.ValueOf(System.today()),string.ValueOf(System.today()),string.ValueOf(System.today()),string.ValueOf(System.today()),'','gama','','','','IMEI','77777777 ');
            System.debug('Limites2: '+Limits.getQueries());

        BI_COL_DescriptionService_ws.ActualizarIngenieroImplantador('Herdwingonza',objOportunidad.BI_Numero_id_oportunidad__c);
            System.debug('Limites4: '+Limits.getQueries());

        BI_COL_DescriptionService_ws.mensajeLog msj = new BI_COL_DescriptionService_ws.mensajeLog();
            msj.cod='0';
            msj.mensaje='PRUEBA';
            msj.mensajeError='Se puede';
            msj.ms = MS.name;
            msj.ordenTrabajo='12145466';

        list<BI_COL_DescriptionService_ws.mensajeLog> lsmsj =new list<BI_COL_DescriptionService_ws.mensajeLog>();
            lsmsj.add(msj);

        BI_COL_DescriptionService_ws.respuestaWebServices(lsmsj);
            lsmsj.clear();
            msj.cod='1';
            lsmsj.add(msj);
        BI_COL_DescriptionService_ws.respuestaWebServices(lsmsj);
          
            MS.BI_COL_Estado__c='Activa'; 
            MS.BI_COL_Oportunidad__c=objOportunidad.id;
            update MS;
        BI_COL_DescriptionService_ws.ActualizarIngenieroImplantador('Herdwingonza',objOportunidad.BI_Numero_id_oportunidad__c);

    }

    @isTest static void test2() 
    {

        //CrearData();
        list<Opportunity> lstOportunidad              = [select Id, BI_Numero_id_oportunidad__c  from Opportunity];

        objPuntosInsta                                = [select Id,BI_Cliente__c,BI_Sede__c,BI_Contacto__c,Name from BI_Punto_de_instalacion__c limit 1];

        BI_COL_Descripcion_de_servicio__c Nds         = new BI_COL_Descripcion_de_servicio__c();
        Nds.BI_COL_Oportunidad__c                     = lstOportunidad[0].Id;
        Nds.CurrencyIsoCode                           = 'COP';
        Nds.BI_COL_Sede_Destino__c                    = objPuntosInsta.id;
        Nds.BI_COL_Sede_Origen__c                     = objPuntosInsta.id;
        Nds.BI_COL_Estado_de_servicio__c              = 'Activado';
        insert Nds;

        system.debug('\n@--->>>lstOportunidad\n'+lstOportunidad);
        system.debug('\n@--->>>Nds\n'+Nds);
        
        BI_COL_Modificacion_de_Servicio__c msS = new BI_COL_Modificacion_de_Servicio__c();
        msS = [select name, id, BI_COL_Estado__c,BI_COL_Codigo_unico_servicio__r.Name,
        BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_Numero_id_oportunidad__c,
        BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_ARG_CAPEX_total__c
        from BI_COL_Modificacion_de_Servicio__c ];
        system.debug('\n@--->>>msS\n'+msS);

        
        msS.BI_COL_Estado__c='Pendiente';
        msS.BI_COL_Fecha_inicio_suspension_temporal__c = system.today();
        msS.BI_COL_Fecha_fin_suspension_temporal__c = system.today()+10;
        msS.BI_COL_Clasificacion_Servicio__c='SERVICIO INGENIERIA';
        
        update msS;
        
        System.debug('Limites: '+Limits.getQueries());
        BI_COL_DescriptionService_ws.actualizarCapexOppty(
        string.valueOf(msS.BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_ARG_CAPEX_total__c),
        string.valueOf(msS.BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_Numero_id_oportunidad__c)
        );

        //BI_COL_DescriptionService_ws.actualizarCapexOppty('2566325','4589632',string.valueOf(msS.BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_Numero_id_oportunidad__c));
        //BI_COL_DescriptionService_ws.actualizarCapexOppty('2566325','4589632','op-236589');
    }

      @isTest static void test3() {
      String numTelefonico='77777777';
      BI_COL_DescriptionService_ws.ActualizarInstalacionDS('','Cancelada',string.ValueOf(System.today()),'','','', '','','','', '', '', '','','', '', '', '', '', '', '', '', '', '', '', '', numTelefonico);
     }
    
     @isTest static void test4() 
    {
       // CrearData();
        list<Opportunity> lstOportunidad               = [select Id, BI_Numero_id_oportunidad__c  from Opportunity];
         objPuntosInsta                                = [select Id,BI_Cliente__c,BI_Sede__c,BI_Contacto__c,Name from BI_Punto_de_instalacion__c limit 1];
         objOportunidad                                = [select Id,Name,AccountId from Opportunity limit 1];

        BI_COL_Descripcion_de_servicio__c Nds         = new BI_COL_Descripcion_de_servicio__c();
        Nds.BI_COL_Oportunidad__c                     = lstOportunidad[0].Id;
        Nds.CurrencyIsoCode                           = 'COP';
        Nds.BI_COL_Sede_Destino__c                    = objPuntosInsta.id;
        Nds.BI_COL_Sede_Origen__c                     = objPuntosInsta.id;
        Nds.BI_COL_Estado_de_servicio__c              = 'Activado';
        insert Nds;

        system.debug('\n@--->>>lstOportunidad\n'+lstOportunidad);
        system.debug('\n@--->>>Nds\n'+Nds);

        BI_COL_Modificacion_de_Servicio__c msS = new BI_COL_Modificacion_de_Servicio__c();
        msS = [select name, id, BI_COL_Estado__c,BI_COL_Codigo_unico_servicio__r.Name,
        BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_Numero_id_oportunidad__c,
        BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_ARG_CAPEX_total__c
        from BI_COL_Modificacion_de_Servicio__c ];
        
        system.debug('\n@--->>>msS\n'+msS);
        
        msS.BI_COL_Estado__c='Pendiente';
        msS.BI_COL_Fecha_inicio_suspension_temporal__c = system.today();
        msS.BI_COL_Fecha_fin_suspension_temporal__c = system.today()+10;
        msS.BI_COL_Clasificacion_Servicio__c='BAJA';
        
        update msS;

        BI_COL_DescriptionService_ws.ActualizarInstalacionDS(msS.BI_COL_Codigo_unico_servicio__r.name, 'EJECUTADA', 
                                                            string.ValueOf(System.today()), string.ValueOf(System.today()),
                                                            'T?cnico - Recursos monetarios', string.ValueOf(System.today()), 
                                                            string.ValueOf(System.today()), 'ALDO FRANCISCO HERNADEZ','123',
                                                            '0180023445+Linea:134567+BW:120 MB+Nodo:NOC_01','Parametos+parametro+prueba',
                                                            'JOSE','PB'+objOportunidad.AccountId,'COBRE', 'MedioDestinoPrueba',msS.name,
                                                            string.ValueOf(System.today()),string.ValueOf(System.today()),
                                                            string.ValueOf(System.today()),string.ValueOf(System.today()),'','','','','','','');

         BI_COL_DescriptionService_ws.actualizarCapexOppty
        (
        string.valueOf(msS.BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_ARG_CAPEX_total__c),
        string.valueOf(msS.BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_Numero_id_oportunidad__c)
        );
    }

     @isTest static void test5() 
    {
       // CrearData();
        list<Opportunity> lstOportunidad              = [select Id, BI_Numero_id_oportunidad__c  from Opportunity];
        objPuntosInsta                                = [select Id,BI_Cliente__c,BI_Sede__c,BI_Contacto__c,Name from BI_Punto_de_instalacion__c limit 1];
        objOportunidad                                = [select Id,Name,AccountId from Opportunity limit 1];

        BI_COL_Descripcion_de_servicio__c Nds         = new BI_COL_Descripcion_de_servicio__c();
        Nds.BI_COL_Oportunidad__c                     = lstOportunidad[0].Id;
        Nds.CurrencyIsoCode                           = 'COP';
        Nds.BI_COL_Sede_Destino__c                    = objPuntosInsta.id;
        Nds.BI_COL_Sede_Origen__c                     = objPuntosInsta.id;
        Nds.BI_COL_Estado_de_servicio__c              = 'Activado';
        insert Nds;

        system.debug('\n@--->>>lstOportunidad\n'+lstOportunidad);
        system.debug('\n@--->>>Nds\n'+Nds);
        list<BI_COL_Modificacion_de_Servicio__c> lstModser = new list<BI_COL_Modificacion_de_Servicio__c>();

        BI_COL_Modificacion_de_Servicio__c msS = new BI_COL_Modificacion_de_Servicio__c();
        msS = [select name, id, BI_COL_Estado__c,BI_COL_Codigo_unico_servicio__r.Name,
        BI_COL_Oportunidad__r.BI_Numero_id_oportunidad__c,
        BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_Numero_id_oportunidad__c,
        BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_ARG_CAPEX_total__c
        from BI_COL_Modificacion_de_Servicio__c ];
        
        system.debug('\n@--->>>msS\n'+msS);
        
        msS.BI_COL_Estado__c='Pendiente';
        msS.BI_COL_Ingeniero_implantador__c = 'Pedro Polulo';
        msS.BI_COL_Estado__c = 'Ativa';
        msS.BI_COL_Fecha_inicio_suspension_temporal__c = system.today();
        msS.BI_COL_Fecha_fin_suspension_temporal__c = system.today()+10;
        msS.BI_COL_Clasificacion_Servicio__c='SERVICIO INGENIERIA';
        lstModser.add(msS);
        
        update lstModser;

        BI_COL_DescriptionService_ws.ActualizarInstalacionDS(msS.BI_COL_Codigo_unico_servicio__r.name, 'Cancelada', 
                                                            string.ValueOf(System.today()), string.ValueOf(System.today()),
                                                            'T?cnico - Recursos monetarios', string.ValueOf(System.today()), 
                                                            string.ValueOf(System.today()), 'ALDO FRANCISCO HERNADEZ','123',
                                                            '0180023445+Linea:134567+BW:120 MB+Nodo:NOC_01','Parametos+parametro+prueba',
                                                            'JOSE','PB'+objOportunidad.AccountId,'COBRE', 'MedioDestinoPrueba',msS.name,
                                                            string.ValueOf(System.today()),string.ValueOf(System.today()),
                                                            string.ValueOf(System.today()),string.ValueOf(System.today()),'','','','','','','');

         BI_COL_DescriptionService_ws.actualizarCapexOppty
        (
        string.valueOf(msS.BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_ARG_CAPEX_total__c),
        string.valueOf(msS.BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.BI_Numero_id_oportunidad__c)
        );
    }
}