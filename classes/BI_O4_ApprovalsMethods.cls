public class BI_O4_ApprovalsMethods {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alvaro García
    Company:       Salesforce.com
    Description:   Methods executed by the BI_O4_Approvals trigger.
    History:
    
    <Date>            <Author>          <Description>
    12/01/2017        Alvaro García       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static Datetime perfectStartDate;
	public static Datetime perfectEndDate;
	public static Boolean changedFlag = true;
	public static Id calendarId;

	static final map<String, Id> MAP_RT_NAME = new map<String, Id>();
    
    static{
        for(RecordType rt : [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType IN ('Event','BI_O4_Approvals__c')]){

            MAP_RT_NAME.put(rt.DeveloperName,rt.Id);
        }
    }
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Alvaro García
	Company:       Aborda
	Description:   Method that create a new event in a valid date
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	12/01/2017                      Alvaro García               Initial Version
	18/02/2017						Guillermo Muñoz				Fix bugs on deploy
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void createEvent(List<BI_O4_Approvals__c> aprobaciones) {
		try
    	{
    		if (BI_TestUtils.isRunningTest()) {
				throw new BI_Exception('test');
    		}

			System.debug('!!!Lista Approval: '+ aprobaciones);    
   
			List<BI_O4_Approvals__c> toProcess = new List<BI_O4_Approvals__c>();
			List<Event> nuevosEventos = new List<Event>();
			Set<Id> approvalIds = new Set<id>();

			//finding the record type
			String gate3RecordType =  MAP_RT_NAME.get('BI_O4_Gate_3');
			
			for(BI_O4_Approvals__c app : aprobaciones)
			{
				System.debug('**record TYpe**' + app);
				
				if(app.RecordTypeId == gate3RecordType.substring(0,15) || app.RecordTypeId == gate3RecordType)//Este es el ID de GATE 3
				{
					toProcess.add(app);
					approvalIds.add(app.id);
				}
			}

			System.debug('************LISTA DE APROVALS: '+ toProcess);

			if (!toProcess.isEmpty()) {

				calendarId = Label.BI_O4_Public_calendar_id;
				//calendarId = [SELECT BI_O4_Calendar_Id__c  FROM BI_O4_Public_calendar_id__c WHERE name = 'Agenda DRB Live' LIMIT 1].BI_O4_Calendar_Id__c;

				for(BI_O4_Approvals__c app : toProcess) {
													
					//obtenemos la siguiente fecha libre a partir de la fecha de solicitud
					getFreeTime(app.BI_O4_DRB_requested_date__c);	
					System.debug('!!!perfectStartDate: ' + perfectStartDate + ' perfectEndDate: ' + perfectEndDate);
					System.debug('!!!app.Id: ' + app.Id);					
					//creamos el evento en la fecha libre encontrada
					Event nuevoEv = new Event();
					if(!Test.isRunningTest()){
					nuevoEv.OwnerID = calendarId;
					}
					nuevoEv.BI_O4_Record_type__c = 'Gate3'; //Campo RecordType
					nuevoEv.RecordTypeId = MAP_RT_NAME.get('BI_O4_Agenda_DRB_Live');
					nuevoEv.WhatId = app.Id; //Campo Related To
					nuevoEv.StartDateTime = perfectStartDate;
					nuevoEv.EndDateTime = perfectEndDate;
					nuevoEv.BI_O4_DRB_gate_request_date__c = app.BI_O4_Gate_request_date__c;
					nuevoEv.Subject = app.BI_O4_Account_Name__c +'-' + app.BI_O4_MNC_leading_Unit__c + '-' + app.BI_O4_Opportunity_name__c;
					nuevoEv.BI_O4_customer_submission_date__c = app.BI_O4_Customer_submission_date__c;
					nuevoEv.BI_O4_Financial_Gate_Request_comments__c = app.BI_O4_Financial_Gate_Request_comments__c;
					//nuevoEv.BI_O4_DRB_Confirmed_Date__c=app.BI_O4_DRB_confirmed_date__c;
					nuevoEv.BI_O4_DRB_Confirmed_Date__c = app.BI_O4_DRB_confirmed_DateTime__c;
					nuevoEv.BI_O4_DRB_Requested_Date__c = app.BI_O4_DRB_requested_date__c;
					nuevoEv.BI_O4_Reason_for_request__c = app.BI_O4_Reason_for_request__c;
					nuevoEv.BI_O4_DRB_Request_Status__c = app.BI_O4_DRB_request_status__c;
					nuevoEv.BI_O4_Reasons_for_DRB__c = app.BI_O4_Reasons_for_DRB_slot_rejection__c;
					nuevoEv.BI_O4_Start__c = perfectStartDate; // fecha de comienzo del evento, obtenida por las funciones auxiliares
					nuevoEv.BI_O4_End__c = perfectEndDate; // fecha de fin del evento, obtenida por las funciones auxiliares
					
					nuevosEventos.add(nuevoEv);
					
				}
			}
			
			//introducimos las nuevos eventos si existen
			if (!nuevosEventos.isEmpty()) {
				System.debug('eventos nuevos: ' + nuevosEventos);
				insert nuevosEventos;
			}

		}catch (Exception exc) {
			BI_LogHelper.generate_BILog('BI_O4_ApprovalsMethods.createEvent', 'BI_EN', Exc, 'Trigger');
    		
    	}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Alvaro García
	Company:       Aborda
	Description:   Method that find for a free date and time to create a new event
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	12/01/2017                      Alvaro García               Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	private static void getFreeTime(Datetime  currentDate){
				
		Event[] publicEvent = [SELECT Id, StartDateTime, EndDateTime, CreatedById FROM Event WHERE OwnerID = : calendarId AND StartDateTime >:currentDate ORDER BY StartDateTime];
		system.debug('publicEvent ='+publicEvent );
		List<Date> thrusDates = ValidDates(currentDate.date());
			
		//Finds the time Zone
		TimeZone tz = UserInfo.getTimeZone();
		TimeZone tzSpain = TimeZone.getTimeZone ('Europe/Amsterdam') ;
		Datetime currdate= datetime.now();

		//horario donde se ejecuta el metodo
		DateTime dtpre = DateTime.newInstanceGMT(currdate.year(), currdate.month(), currdate.day(), 0, 00, 00);
		Integer temp = tz.getOffset(dtpre)  / (1000*60*60);
		Integer hours   = math.mod(temp,24);
		
		//horario español
		DateTime dtpreSpain = DateTime.newInstanceGMT(datetime.now().year(), datetime.now().month(), datetime.now().day(), 0, 00, 00);
		Integer tempSpain = tzSpain.getOffset(dtpreSpain)  / (1000*60*60);
		Integer hoursSpain   = math.mod(tempSpain,24);
		
		system.debug('Display nameSpain: ' + tzSpain.getDisplayName());
		system.debug('DATESpain: ' + dtpreSpain);
		system.debug('OffsetSpain: ' + tzSpain.getOffset(dtpreSpain));
			
		Integer gtm1hours = hours-hoursSpain;
			
		system.debug('gtm1hours ='+gtm1hours );
		system.debug('thrusDates ='+thrusDates );
			
		boolean isPerfectTimefound = false;
		Datetime todayDate = System.now();
		boolean chooseNextDate = false;
		
		for(Date thuDt:thrusDates ){
				
			List<Event> thuEvent = new List<Event>();
			boolean noEvent = true;
			//añadir las horas de diferencia para trabajar con la hora española
			List<Datetime> thusDates = validTime(datetime.newInstance(thuDt.year(), thuDt.month(),thuDt.day()).addHours(gtm1hours));
			
			for(Event ev:publicEvent ){
				
				if(ev.StartDateTime.date().format() == thuDt.format()){
						 
					//ya existe algun evento el jueves que se esta revisando, se rellena una lista con todos los eventos de ese jueves
					noEvent = false;
					
					for(Event thEv:publicEvent ){
						
						if(thEv.StartDateTime.date().format() == thuDt.format()) {
							
							thuEvent.add(thEv); 
						}
					}
					
					break;
				}			
			}

			//si no hay ningun evento en ese jueves entonces se creara el evento el jueves de 16:00 a las 16:15
			 
			system.debug('noEvent ='+noEvent +' thuDt '+thuDt );
			if(noEvent){
					
				system.debug('Best Date-'+thuDt);
				//gtm1hours to get gtm right
				
				perfectStartDate = datetime.newInstance(thuDt.year(), thuDt.month(),thuDt.day()).addHours(16+gtm1hours);
				perfectEndDate = perfectStartDate.addMinutes(15);
				system.debug('dates ='+perfectStartDate + 'enddate' +  perfectEndDate);
				
				break;
			}
				
				
			Datetime eventDate = System.Today();
			
			//for(Integer i=0 ;i<thusDates.size()-2;i++){
			//se recorren todas las horas posibles en un dia
			for(Integer i=0 ;i<=thusDates.size();i++){
			
				boolean eventExist = false;
				
				//se recorren todos los eventos que hay creados ese jueves
				for(Event ev:thuEvent ){
					
					system.debug('thusDates.get(i)==**'+thusDates.get(i)+' todayDate ='+todayDate );
					
					if(ev.EndDateTime <= thusDates.get(0) || ev.StartDateTime > thusDates.get(7)){
							
							system.debug('Skipped this event='+ev.EndDateTime+'::::'+eventDate+':thusDates.get(0)='+thusDates.get(0) );
							continue;
					}

					//la fecha y hora ya ha tenido lugar, por tanto no se puede agendar una reunion
					if(thusDates.get(i) < todayDate ){
							
							system.debug('inside thusDates.get(i)==**'+thusDates.get(i)+' todayDate ='+todayDate );
							continue;                    
					
					}

					else{
						//comrpueba si en esa fecha y hora ya existe un evento
					
						if( (thusDates.get(i) > ev.StartDateTime &&  thusDates.get(i) < ev.EndDateTime ) 
							|| (thusDates.get(i+1) > ev.StartDateTime &&  thusDates.get(i+1) < ev.EndDateTime) 
							|| (thusDates.get(i) == ev.StartDateTime && thusDates.get(i+1) == ev.EndDateTime) ){    
							
							system.debug('Comparsion='+thusDates.get(i)+' >= '+ev.StartDateTime+' &&  '+thusDates.get(i)+' <= '+ev.EndDateTime+' || '+thusDates.get(i+1)+' >= '+ev.StartDateTime+' &&  '+thusDates.get(i+1)+' <= '+ev.EndDateTime);
							eventExist = true;
							
							break;
								 
						}
					}
				}
					 
				system.debug('eventExist ='+eventExist );
				
				if(!eventExist){
				
					system.debug('perfectStartDate-'+thusDates.get(i)+' ::perfectEndDate-'+thusDates.get(i+1));
					
					isPerfectTimefound = true;
					perfectStartDate = thusDates.get(i);
					perfectEndDate = thusDates.get(i+1);
					
					break;
					 
				}
			}
				
			if(isPerfectTimefound ){
				break;   
			}
		}
	}
		
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Alvaro García
	Company:       Aborda
	Description:   Method that find the next thursday since the inputdate
	History: 
	
	<Date>                          <Author>                    <Change Description>
	12/01/2017                      Alvaro García               Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	//devuelve una lista con todos los jueves desde la fecha introducida hasta el dia limite de dias indicado en el custom setting
	
	private static List<Date> ValidDates(Date inputdate) {
		system.debug('iinputDate==='+System.now());
		
		Integer thusLimit = Integer.valueOf(BI_O4_Thursday_Limit__c.getInstance('Number of Days').BI_O4_Number_of_days__c);
		List<Date> listofdate =new List<Date>();    
		Date weekStart = inputDate.toStartofWeek();
		
		//Date firstThursdate=weekStart.addDays(3);
		//Finds the first Thurday no matter the location
		Date firstThursdate=inputDate;
		Datetime dt = DateTime.newInstance(firstThursdate, Time.newInstance(0, 0, 0, 0));
		String dayOfWeek=dt.format('EEEE');
		
		while( dayOfWeek != 'Thursday' ){
			
			firstThursdate = firstThursdate.AddDays(1);
			dt = DateTime.newInstance(firstThursdate, Time.newInstance(0, 0, 0, 0));
			dayOfWeek=dt.format('EEEE');
		}
	 
		 
		
		system.debug('DEBUG VALID DATES p1');
		system.debug('thusLimit ='+thusLimit );
		system.debug('weekStart ='+weekStart );
		system.debug('firstThursdate ='+firstThursdate );
		
		if(firstThursdate >= inputdate){
				
				listofdate.add(firstThursdate);
		}
		
		Date limitdate= firstThursdate.addDays(thusLimit);
		
		while (firstThursdate < limitdate ){
			
			Date temp1 =firstThursdate;
			listofdate.add(temp1.addDays(7));
			firstThursdate=temp1.addDays(7);
			
			system.debug('DEBUG VALID DATES p2');
			system.debug('listofdate ='+listofdate );
			system.debug('firstThursdate ='+firstThursdate );
		}
		
	 
		return listofdate;
	}
		
		
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Alvaro García
	Company:       Aborda
	Description:   Method that find all the possible time for an event
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	12/01/2017                      Alvaro García               Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	//devuelve una lista con todas las horas del día posibles para la reunion
	
	private static List<DateTime> validTime(DateTime thuDt){
		
		List<Datetime> dateList = new List<Datetime>();
		
		//Se introduce en la lista las 16:00
		thuDt= thuDt.addHours(16);
		dateList.add(thuDt);
		system.debug('Hours  ='+thuDt);
		
		//como la primera hora esta ya introducida (16:00), solo es necesario introducir 7 horas mas (16:15, 16:30, 16:45, 17:00, 17:15, 17:30, 17:45)
		for(Integer i=0 ;i<7;i++){
				
				thuDt= thuDt.addMinutes(15);
				dateList.add(thuDt);
				system.debug('time  ='+thuDt);
		
		}

		system.debug('DEBUG VALID TIME p1');
		system.debug('thuDt ='+thuDt );    
		
		return dateList;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Alvaro García
	Company:       Aborda
	Description:   Method that update the date of the event if change the Requested date
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	15/01/2017                      Alvaro García               Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void updateDRB_Event(List<BI_O4_Approvals__c> news, List<BI_O4_Approvals__c> olds){
	
		try{
	
			if (BI_TestUtils.isRunningTest()) {
				throw new BI_Exception('test');
    		}

	        List<BI_O4_Approvals__c> updateApproval = new List<BI_O4_Approvals__c>();
	        Set<Id> approvalsID = new Set<Id>();
	        
	        List<Event> listEvent = new List<Event>();
	        
	    	//record type change
	        
	        String gate3RecordType =  MAP_RT_NAME.get('BI_O4_Gate_3');
	        
	        for (Integer i = 0; i < news.size(); i++) {
	        //for(BI_O4_Approvals__c app : news) {

	            if( news[i].BI_O4_DRB_requested_date__c != olds[i].BI_O4_DRB_requested_date__c 
	            	&& changedFlag
	            	&& (news[i].RecordTypeId == gate3RecordType.substring(0,15) || news[i].RecordTypeId == gate3RecordType)) {
	                
	                updateApproval.add(news[i]);
	                approvalsID.add(news[i].Id);
	            }
	        }
	        
	        listEvent = [SELECT Id, StartDateTime, EndDateTime, WhatID, BI_O4_fromBID__c
	                     FROM Event 
	                     WHERE WhatID IN: approvalsID];
	       
	        for(BI_O4_Approvals__c app : updateApproval) {

	            for(Event evento : listEvent) {

	                if(evento.WhatID == app.ID) {

	                	getFreeTime(app.BI_O4_DRB_requested_date__c);

	                	evento.StartDateTime = perfectStartDate;
	                	evento.EndDateTime = perfectEndDate;
	                	evento.BI_O4_fromBID__c = true;
	                }
	            }
	        }

	        
	       // update listTask;
	        
	        if (!listEvent.isEmpty()) {

	        	update listEvent;
	        	
	        	for(Event evento : listEvent) {
		            
		            evento.BI_O4_fromBID__c = false;
		        }

		        update listEvent;
	        }

 		}catch (Exception exc) {
  
    		BI_LogHelper.generate_BILog('BI_O4_ApprovalsMethods.updateDRB_Event', 'BI_EN', Exc, 'Trigger');
    	}   
    }
   
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Alvaro García
	Company:       Aborda
	Description:   Method that find delete an event if change the status of the BI_O4_approvals to 'Slot Reject'
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	15/01/2017                      Alvaro García               Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void deleteDRB_Event(List<BI_O4_Approvals__c> news, List<BI_O4_Approvals__c> olds){
        
        try {

        	if (BI_TestUtils.isRunningTest()) {
				throw new BI_Exception('test');
    		}

        	//List<BI_O4_Approvals__c> updateApproval = new List<BI_O4_Approvals__c>();
	        List<Event> listEvent = new List<Event>();
	        Set<Id> approvalsID = new Set<Id>();
	    
	    	//record type change
	        String gate3RecordType =  MAP_RT_NAME.get('BI_O4_Gate_3');

	        for (Integer i = 0; i < news.size(); i++) {
	        //for(BI_O4_Approvals__c app : listApproval) {

	            if( news[i].BI_O4_DRB_request_status__c != olds[i].BI_O4_DRB_request_status__c 
					&& news[i].BI_O4_DRB_request_status__c == 'Slot Reject'
	            	&& (news[i].RecordTypeId == gate3RecordType.substring(0,15) || news[i].RecordTypeId == gate3RecordType)) {
	            
	                //updateApproval.add(news[i]);
	                approvalsID.add(news[i].Id);
	            }
	        }
	        
	        listEvent = [SELECT Id FROM Event WHERE WhatID IN: approvalsID];
	       
	        if (!listEvent.isEmpty()) {
	        	delete listEvent;
	        }
	            
        }catch (Exception exc) {
  
    		BI_LogHelper.generate_BILog('BI_O4_ApprovalsMethods.deleteDRB_Event', 'BI_EN', Exc, 'Trigger');
    	} 
        
    }


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Alvaro García
	Company:       Aborda
	Description:   Method that update an event if change the confirmed date of the approval
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	15/01/2017                      Alvaro García               Initial Version
	30/08/2017						Javier Almirón				I add the functionality of this method to update the Event if some of the fields "DRB request status" or "Reasons for DRB slot rejection" 
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void updateEvent_confirmedDate(List<BI_O4_Approvals__c> news, List<BI_O4_Approvals__c> olds){
    
	    try {    

	        List<BI_O4_Approvals__c> updateApproval = new List<BI_O4_Approvals__c>();
	        List<BI_O4_Approvals__c> approvalToUpdate = new List<BI_O4_Approvals__c>();
	        //Set<Id> approvalToUpdateID = new Set<Id>();
	        List<Event> listEvent = new List<Event>();
	        Set<ID> approvalsID = new Set<ID>();
	    
	    	//record type change
	        String gate3RecordType =  MAP_RT_NAME.get('BI_O4_Gate_3');

	        for (Integer i = 0; i < news.size(); i++) {
	        //for(BI_O4_Approvals__c app : listApproval) {

	            if( (news[i].BI_O4_DRB_confirmed_DateTime__c != null || news[i].BI_O4_DRB_request_status__c != null || news[i].BI_O4_Reasons_for_DRB_slot_rejection__c != null)

					&& (olds[i].BI_O4_DRB_confirmed_DateTime__c != news[i].BI_O4_DRB_confirmed_DateTime__c || olds[i].BI_O4_DRB_request_status__c != news[i].BI_O4_DRB_request_status__c  || olds[i].BI_O4_Reasons_for_DRB_slot_rejection__c != news[i].BI_O4_Reasons_for_DRB_slot_rejection__c)
	            	&& (news[i].RecordTypeId == gate3RecordType.substring(0,15) || news[i].RecordTypeId == gate3RecordType)) {

	                updateApproval.add(news[i]);
	                approvalsID.add(news[i].Id);
	            }

	            //if(app.BI_O4_DRB_confirmed_DateTime__c != app.BI_O4_DRB_requested_date__c) {

	            //    approvalToUpdateID.add(app.ID);
	            //}
	        }
	        
	        listEvent = [SELECT ID, StartDateTime, EndDateTime, WhatID, BI_O4_fromBID__c, BI_O4_DRB_Confirmed_Date__c, BI_O4_DRB_Request_Status__c, BI_O4_Reasons_for_DRB__c
	                     FROM Event 
	                     WHERE WhatID IN: approvalsID];
	       
	        for(BI_O4_Approvals__c app : updateApproval) { 
	            
	            for(Event evento : listEvent) {

	                if(evento.WhatID == app.Id) {
	                  /*CHANGE ON 2015-03-16 BY RICARDO DIAS
	                    if(app.DRB_Confirmed_Date__c != app.BI_O4_DRB_requested_date__c){ Remove != so Confirm date can be = to request date*/
	                    if(app.BI_O4_DRB_confirmed_DateTime__c != app.BI_O4_DRB_requested_date__c){
	                  
	                        evento.StartDateTime = datetime.newInstance(app.BI_O4_DRB_confirmed_DateTime__c.year(), app.BI_O4_DRB_confirmed_DateTime__c.month(),app.BI_O4_DRB_confirmed_DateTime__c.day()).addHours(app.BI_O4_DRB_confirmed_DateTime__c.hour()); 
	                        evento.StartDateTime = evento.StartDateTime.addMinutes(app.BI_O4_DRB_confirmed_DateTime__c.minute());
	                        evento.EndDateTime = evento.StartDateTime.addMinutes(15);
	                        evento.BI_O4_fromBID__c = true;
	                        evento.BI_O4_DRB_Confirmed_Date__c = app.BI_O4_DRB_confirmed_DateTime__c;
	                        evento.BI_O4_DRB_Request_Status__c = app.BI_O4_DRB_request_status__c;
	                        evento.BI_O4_Reasons_for_DRB__c = app.BI_O4_Reasons_for_DRB_slot_rejection__c;
	                        
	                        changedFlag = false;
	                    }	                    
	                }	           
	            }

	            if (!listEvent.isEmpty()) {

	            	update listEvent;
		            
		            for(Event evento : listEvent) {
		                
		                evento.BI_O4_fromBID__c = false;
		            }

		            update listEvent;
	            }
	            
	        }

	    }catch (Exception exc) {
  
    		BI_LogHelper.generate_BILog('BI_O4_ApprovalsMethods.UpdateEvent_confirmedDate', 'BI_EN', Exc, 'Trigger');
    	} 
    }

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Alberto Fernández
	Company:       Accenture
	Description:   Sends an email notification when a Gate 3 Approval Request is created.
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	19/05/2017                      Alberto Fernández            Initial Version
	30/05/2017						Gawron, Julián				 Refactoring queries
	24/07/2017						Gawron, Julián				 Deleting TeamMember
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void sendG3Notification(Map<Id, BI_O4_Approvals__c> news){

    	List<BI_O4_Approvals__c> toProcess = new List<BI_O4_Approvals__c>();
    	Id id_bi_o4_gate = MAP_RT_NAME.get('BI_O4_Gate_3');

    	for(BI_O4_Approvals__c appr : news.values()){
    		if(appr.RecordtypeId == id_bi_o4_gate)
    			toProcess.add(appr);
    	}


    	if(!toProcess.isEmpty()){
	    	List<Messaging.SingleEmailMessage> list_emails = new List<Messaging.SingleEmailMessage>();
			List<EmailTemplate> template = [Select Id, Name, HTMLValue, Subject from EmailTemplate where DeveloperName='BI_O4_Gate_3_Approval_Created_Notification2']; //JEG
    		List<User> users = [SELECT Id FROM User 
    				                 WHERE UserRole.DeveloperName = 'BI_O4_TGS_FinancialControllerSpain_OffNet_France' 
    				                 OR UserRole.DeveloperName = 'BI_O4_TNA_PricingManager'];
    		Map<Id, BI_O4_Approvals__c>	approvals = new Map<Id, BI_O4_Approvals__c>([select id,
    									BI_O4_Proposal__r.Name,
    									BI_O4_Proposal__r.Opportunity.Id,
    									BI_O4_Proposal__r.Opportunity.Name,
    									BI_O4_Proposal__r.Opportunity.Account.Name,
    									BI_O4_Proposal__r.Opportunity.Account.TGS_Account_Category__c,
    									BI_O4_Proposal__r.Opportunity.Owner.FirstName,
    									BI_O4_Proposal__r.Opportunity.Owner.LastName,
    									BI_O4_Proposal__r.Opportunity.BI_O4_Opportunity_leading_MNC_unit__c,
    									BI_O4_Proposal__r.Opportunity.Amount,
    									BI_O4_Proposal__r.Opportunity.CloseDate,
    									BI_O4_Proposal__r.BI_O4_URL_to_OneDrive__c,
    									BI_O4_Proposal__r.BI_O4_AM__c,
    									BI_O4_Gate_request_date__c,
    									BI_O4_Commercial_Gate_Approved_by__c
    							from BI_O4_Approvals__c where Id in:news.keySet()]);
    		/*
    		Map<Id, Id> map_approvalsOpps = new Map<Id, Id>();
 			Map<Id, OpportunityTeamMember> map_approvalsUsers = new Map<Id, OpportunityTeamMember>();
     		for(BI_O4_Approvals__c appr : approvals.values()) {
    			map_approvalsOpps.put(appr.BI_O4_Proposal__r.Opportunity.Id, appr.Id);
    		}
    		for(OpportunityTeamMember oppTM : [SELECT User.Name, User.LastName, Opportunity.Id FROM OpportunityTeamMember WHERE TeamMemberRole = 'Global Solutions Engineer Principal' AND Opportunity.Id IN :map_approvalsOpps.keySet()]) {
    			map_approvalsUsers.put(map_approvalsOpps.get(oppTM.Opportunity.Id), oppTM);
    		}
			*/
          String QuoteName = ''; // {!Quote.Name}
          String QuoteAM = ''; //{!Quote.BI_O4_AM__c} 
          String QuoteURLOneDrive = ''; //{!Quote.BI_O4_URL_to_OneDrive__c}
          String OppName = ''; //{!Opportunity.Name}
        //  String OppTeamMem = ''; //{!Opportunity.TeamMember}
          String OppMCNunit = ''; //{!Opportunity.BI_O4_Opportunity_leading_MNC_unit__c}
          String OppAmount = ''; //{!Opportunity.Amount}
          String OppLink = ''; //{!Opportunity.Link}
          String OppCloseDate = '';   //{!Opportunity.CloseDate}      
          String AccName = ''; //{!Account.Name}
          String AccTGS_Cat = ''; //{!Account.TGS_Account_Category__c}
          String ApprovCommercialGate = ''; //{!BI_O4_Approvals__c.BI_O4_Commercial_Gate_Approved_by__c}
          String ApprovReqDate = ''; //{!BI_O4_Approvals__c.BI_O4_Gate_request_date__c}
          String ApprovalLink = ''; //{!BI_O4_Approvals__c.Link}

			for(BI_O4_Approvals__c appr : toProcess){
				//Cada usuario debe recibir un correo por aproval
				for (User usert : users) {
					Messaging.SingleEmailMessage singleEmail = new Messaging.SingleEmailMessage();

			        string body = template[0].HTMLValue;

			         BI_O4_Approvals__c theAppr = approvals.get(appr.Id);

 					 QuoteName = '\''+ theAppr.BI_O4_Proposal__r.Name+ '\''; // {!Quote.Name}
 					 QuoteAM = '\''+ theAppr.BI_O4_Proposal__r.BI_O4_AM__c+ '\''; //{!Quote.BI_O4_AM__c} 
 					 QuoteURLOneDrive = (theAppr.BI_O4_Proposal__r.BI_O4_URL_to_OneDrive__c != null) ? '\''+ theAppr.BI_O4_Proposal__r.BI_O4_URL_to_OneDrive__c +'\'' : ' - '  ; //{!Quote.BI_O4_URL_to_OneDrive__c}
 					 OppName = '\''+theAppr.BI_O4_Proposal__r.Opportunity.Name+'\''; //{!Opportunity.Name}
 					// OppTeamMem = (map_approvalsUsers.containsKey(theAppr.BI_O4_Proposal__r.Opportunity.Id)) ? '\'' + map_approvalsUsers.get(theAppr.BI_O4_Proposal__r.Opportunity.Id)+'\'' : ''; //{!Opportunity.TeamMember}
 					 OppMCNunit = '\''+theAppr.BI_O4_Proposal__r.Opportunity.BI_O4_Opportunity_leading_MNC_unit__c+'\''; //{!Opportunity.BI_O4_Opportunity_leading_MNC_unit__c}
 					 OppAmount = '\''+theAppr.BI_O4_Proposal__r.Opportunity.Amount+'\''; //{!Opportunity.Amount}
 					 OppCloseDate =  '\''+theAppr.BI_O4_Proposal__r.Opportunity.CloseDate+'\'';   //{!Opportunity.CloseDate}      
 					 AccName = '\''+ theAppr.BI_O4_Proposal__r.Opportunity.Account.Name+'\''; //{!Account.Name}
 					 AccTGS_Cat = '\''+ theAppr.BI_O4_Proposal__r.Opportunity.Account.TGS_Account_Category__c+'\''; //{!Account.TGS_Account_Category__c}
 					 ApprovCommercialGate = '\''+ theAppr.BI_O4_Commercial_Gate_Approved_by__c+'\'' ; //{!BI_O4_Approvals__c.BI_O4_Commercial_Gate_Approved_by__c}
 					 ApprovReqDate = '\''+ theAppr.BI_O4_Gate_request_date__c+'\''; //{!BI_O4_Approvals__c.BI_O4_Gate_request_date__c}

    			    body = body.replace('{!Quote.Name}', QuoteName);
			        body = body.replace('{!Quote.BI_O4_URL_to_OneDrive__c}', QuoteURLOneDrive);
			        body = body.replace('{!Opportunity.Name}', OppName);
			        body = body.replace('{!Account.Name}', AccName);
			        body = body.replace('{!Account.TGS_Account_Category__c}', AccTGS_Cat);
			       // body = body.replace('{!Opportunity.TeamMember}', OppTeamMem);
			        body = body.replace('{!Opportunity.BI_O4_Opportunity_leading_MNC_unit__c}', OppMCNunit);
			        body = body.replace('{!Opportunity.Amount}', OppAmount);
			        body = body.replace('{!Quote.BI_O4_AM__c}', QuoteAM);
			        body = body.replace('{!BI_O4_Approvals__c.BI_O4_Gate_request_date__c}', ApprovReqDate);
			        body = body.replace('{!BI_O4_Approvals__c.BI_O4_Commercial_Gate_Approved_by__c}', ApprovCommercialGate);
			        body = body.replace('{!Opportunity.CloseDate}', OppCloseDate);


			        String fullOppURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + theAppr.BI_O4_Proposal__r.Opportunity.Id;
			        String fullApprURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + appr.Id;
			        body = body.replace('{!Opportunity.Link}', '\''+fullOppURL+'\'');
			        body = body.replace('{!BI_O4_Approvals__c.Link}', '\''+fullApprURL+'\'');

			        System.debug('sendG3Notification body:' + body);
			        
			        singleEmail.setHTMLBody(body);
			        singleEmail.Subject = template[0].Subject;
			        singleEmail.saveAsActivity=false;
			        singleEmail.setTemplateId(template[0].Id);
			        //singleEmail.setWhatId(cas.ParentId);
			        singleEmail.setTargetObjectId(usert.Id);
			        
			        list_emails.add(singleEmail);
				}
    		}
			if(!list_emails.isEmpty()){
                Messaging.sendEmail(list_emails);
            }

    	}
    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Javier Almirón
	Company:       Aborda
	Description:   Method that fill the analogous fields from Approval to Opportunity when the associated proposal is synced and the Approval has passed all the gates.
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	10/07/2017                      Javier Almirón               Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void migAppOpp ( List<BI_O4_Approvals__c> news, Map <Id, BI_O4_Approvals__c> oldMap) {


		Set <Id> propSet = new Set <Id>();
		Set <Id> appSet = new Set <Id>();
		List <BI_O4_Approvals__c> appList = new List <BI_O4_Approvals__c> ();
		List <Opportunity> oppList = new List <Opportunity>();
		List <Opportunity> oppToUpdate = new List <Opportunity>();
		List <Quote> propList = new List <Quote>();

		IF(news!=null){

			System.debug('¡! Checkpoint 1');

			

			for (BI_O4_Approvals__c app: news){


				if (app.BI_O4_Gate_status__c=='GO'||app.BI_O4_Gate_status__c == 'Conditional Go'){
						System.debug('¡! Checkpoint 2');
						propSet.add(app.BI_O4_Proposal__c);
						appSet.add(app.Id);
							
				}	

			}

			if (!propSet.isEmpty()){
				System.debug('¡! Checkpoint 3');
				propList = [SELECT ID, (SELECT Id, BI_O4_FCV__c, BI_O4_FCV_Off_Net__c, 
												BI_O4_NRR__c, BI_O4_NRR_TGS__c,
												BI_O4_MRR_TEF__c, BI_O4_MRR_TGS__c,
												BI_O4_NRC_TEF__c, BI_O4_NRC_TGS__c,
												BI_O4_MRC_TEF__c, BI_O4_MRC_TGS__c,
												BI_O4_iCOST_TGS__c, BI_O4_Incremental_Cost__c,
												BI_O4_Incremental_OIBDA__c, BI_O4_oibda_tgs__c,
												BI_O4_Incremental_OIBDA_Percent__c, BI_O4_Incremental_oibda_tgs__c,
												BI_O4_CAPEX__c, BI_O4_CAPEX_TGS__c,
												BI_O4_Incremental_OI__c, BI_O4_iOI_TGS__c,
												BI_O4_Incremental_OI_Percent__c, BI_O4_Incremental_OI_tgs__c,
											   BI_O4_Opportunity__c FROM Approvals__r) 
							FROM Quote  
							WHERE (ID IN: propSet AND IsSyncing = true)];
			}
		

			if (!propList.isEmpty()){
				System.debug('¡! Checkpoint 4');
				for (Quote q: propList){

					for (BI_O4_Approvals__c ap: q.Approvals__r){

						if (appSet.contains(ap.ID)){
							System.debug('¡! Checkpoint 5');
							Opportunity opp = new Opportunity (Id=ap.BI_O4_Opportunity__c, 
															   BI_O4_FCV_Business_Case__c = ap.BI_O4_FCV__c,
															   BI_O4_BC_FCV_TGS__c = ap.BI_O4_FCV_Off_Net__c,
															   BI_O4_Opportunity_Won_BC_NRR_TEF__c = ap.BI_O4_NRR__c,
															   BI_O4_BC_NRR_TGS__c = ap.BI_O4_NRR_TGS__c,
															   BI_O4_Opportunity_Won_BC_MRR_TEF__c = ap.BI_O4_MRR_TEF__c,
															   BI_O4_BC_MRR_TGS__c = ap.BI_O4_MRR_TGS__c,
															   BI_O4_Opportunity_Won_BC_NRC_TEF__c = ap.BI_O4_NRC_TEF__c,
															   BI_O4_BC_NRC_TGS__c = ap.BI_O4_NRC_TGS__c,
															   BI_O4_Opportunity_Won_BC_MRC_TEF__c = ap.BI_O4_MRC_TEF__c,
															   BI_O4_BC_MRC_TGS__c = ap.BI_O4_MRC_TGS__c,
															   BI_O4_Opportunity_Won_BC_iCOST_TEF__c = ap.BI_O4_Incremental_Cost__c,
															   BI_O4_BC_iCOST_TGS__c = ap.BI_O4_iCOST_TGS__c,
															   BI_O4_Opportunity_Won_BC_iCAPEX_TEF__c = ap.BI_O4_CAPEX__c,
															   BI_O4_BC_iCAPEX_TGS__c = ap.BI_O4_CAPEX_TGS__c
															   );
							oppToUpdate.add(opp);
						}
	
					}

					for (Opportunity o: oppToUpdate){
						System.debug('¡! o.Id = ' + o.Id);
					}
					
				}

			}

			if (!oppToUpdate.isEmpty()){
	
				update oppToUpdate;
	
			}
		}

    }

}