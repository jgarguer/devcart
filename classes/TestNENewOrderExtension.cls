@isTest
private class TestNENewOrderExtension {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Jose Miguel Fierro
     Company:       New Energy Aborda
     Description:   Method that tests creating an order from the NENewOrder page
     History:
     
     <Date>            <Author>             <Description>
     03/04/2016        Jose Miguel Fierro   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void myUnitTest() {
        UserRole ur = new UserRole(Name='TEST ROLE');
        insert ur;
        User usr = TGS_Dummy_Test_Data.dummyUserTGS('TGS Order Handling');
        usr.BI_Permisos__c = 'TGS'; // Include this in dummyUserTGS?
        usr.UserRoleId = ur.Id;
        insert usr;

        Account accLE;
        User portalUsr;
        System.runAs(usr) {
            accLE = TGS_Dummy_Test_Data.dummyHierarchy();
            portalUsr = TGS_Dummy_Test_Data.getPortalUserFromAccount(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, accLE, true);
            TGS_Dummy_Test_Data.setUserType(portalUsr.ProfileId, true, false);
        }

        List<Account> lstAccs = [SELECT Id, Name, Parent.Id, Parent.Name FROM Account WHERE Parent.ParentId = :accLE.Id LIMIT 1];
        System.assertNotEquals(0, lstAccs.size(), 'No Cost Centers were created for the dummy hierarchy, or they are not visible');
        Account accCC = lstAccs[0],
                accBU = accCC.Parent;

        Pagereference p = Page.NENewOrder;
        p.getParameters().put('accId', accLE.Id);
        Test.setCurrentPageReference(p);

        System.runAs(portalUsr) {
            ApexPages.StandardController sc = new ApexPages.StandardController(new NE__Order__c());

            Test.startTest();
            NENewOrderExtension contr = new NENewOrderExtension(sc);

            contr.ord.Business_Unit__c = accBU.Id;
            contr.updateBusinessUserId();

            Apexpages.currentPage().getParameters().put('businessUnitId', accBU.Id);
            contr.getCostCenters();
            contr.ord.Cost_Center__c = accCC.Id;

            contr.clearAuthorizedUsers();
            contr.getAuthorizedUsersAndContract();

            contr.selectedUser = portalUsr.Id;
            contr.ord.Authorized_User__c = portalUsr.Id;
            contr.setAuthUser();

            contr.next();
            contr.cancel();

            contr.setAcc();
            Test.stopTest();
        }
    }

    /*static testMethod void myUnitTest() {
        RecordType RecId = [SELECT Id FROM RecordType WHERE DeveloperName <> 'TGS_Legal_Entity' and DeveloperName <> 'TGS_Legal_Entity_Inactive' and SobjectType = 'Account' LIMIT 1]; 
            
        Account acc = new Account(Name = 'TestAccount', RecordTypeId = RecId.Id);
        insert acc;
        
        String nRandom = String.valueOf((Integer)(Math.random()*100));
        String mailName = 'telefonicaTest'+nRandom+'@tefonica.com';
        Contact contactTest = new Contact(LastName = 'contactTest',
                                         // AccountId = acc.Id,
                                          AccountId = acc.Id,
                                          Email = mailName
                                          /*Fax = '123456789',
                                          Phone = '123456789',
                                          MobilePhone = '123456789',
                                          BI_Id_del_contacto__c = 'test1',
                                          BI_Paises_ref__c = account1.BI_Pais_ref__c,
                                          BI_Activo__c = true,
                                          MailingCity = 'city1',
                                          BI_Tipo_de_documento__c = 'Otros',
                                          BI_Numero_de_documento__c = '1',
                                          TGS_Default_Cost_Center__c = account1.Id * /);
        insert contactTest;
        //Falta crear usuario asociado al contacto
        //User testUser=new User();
        //insert testUser;


        Pagereference p = Page.NENewOrder;
        p.getParameters().put('accId', acc.Id);
        Test.setCurrentPageReference(p);
        
        NE__Order__c neword = new NE__Order__c();
        insert neword;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(neword);
        NENewOrderExtension contr = new NENewOrderExtension(sc);
        
        //////////Start Miguel Angel Galindo///////////////
        User user;
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            user = TGS_Dummy_Test_Data.dummyUserTGS('TGS Customer Community Plus');
            user.BI_Permisos__c = 'TGS';
            user.ContactId = contactTest.Id;  
            Test.startTest();
            insert user;
            Test.stopTest();
        }
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_TGS__c = true;
        userTGS.SetupOwnerId=user.profileId;
        insert userTGS;
        
        contr.selectedUser = user.Id;
        System.runAs(user){
            Account ac = TGS_Dummy_Test_Data.dummyHierarchy();
            Account acBU = [Select Id From Account Where ParentId = :ac.Id];
            
            Account acCC = [Select Id,HoldingId__c From Account Where ParentId = :acBU.Id];
            
            NE__Order__c new_ord = new NE__Order__c();
            new_ord.NE__AccountId__c = ac.Id;
            new_ord.HoldingId__c=acCC.HoldingId__c;
            new_ord.Cost_Center__c = acCC.Id;
            new_ord.Business_Unit__c = acBu.Id;
            insert new_ord;
            
            contr.ord = new_ord;
            
            contr.next();
            contr.clearAuthorizedUsers();
            contr.cancel();
            contr.setAcc();
            contr.setUserAcc();
            Apexpages.currentPage().getParameters().put('businessUnitId',acBu.Id);
            contr.getCostCenters();
            contr.getSites();
            //contr.setPayerAcc();
            //contr.setAuthUser();
        }
        
        //
        //////////End Miguel Angel Galindo/////////////////
    }*/
    

        
    
    /*
    static testMethod void myUnitTest2() {
        Profile prof = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = [SELECT Id FROM User WHERE ProfileId =: prof.Id and IsActive = true LIMIT 1];
         
        System.runAs(u) 
        {
            
            RecordType RecId = [SELECT Id FROM RecordType WHERE DeveloperName <> 'TGS_Legal_Entity' and DeveloperName <> 'TGS_Legal_Entity_Inactive' and SobjectType = 'Account' LIMIT 1];

            RecordType RecId2 = [SELECT Id FROM RecordType WHERE name like '%Business Unit' and SobjectType = 'Account' LIMIT 1];
            
            
            PageReference p = Page.NENewOrder;
            Test.setCurrentPageReference(p);
            
            
            
            Account acc = new Account(Name = 'TestAccount2', RecordTypeId = RecId.Id);
            insert acc;
            
            Account acc2 = new Account(Name = 'TestAccount3', RecordTypeId = RecId2.Id, parent = acc);
            insert acc2;
            
            
            NE__Order__c neword = new NE__Order__c();
            neword.NE__AccountId__c = acc.Id;
            neword.Cost_Center__c = acc.Id;
            neword.Business_Unit__c = acc2.Id;
            insert neword;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(neword);
            NENewOrderExtension contr2 = new NENewOrderExtension(sc);
            
            contr2.setAcc();
            contr2.cancel();
        }
    }*/
}