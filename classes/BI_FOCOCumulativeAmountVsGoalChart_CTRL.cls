/*-------------------------------------------------------------------
	Author:         Virgilio Utrera
	Company:        Salesforce.com
	Description:    Custom controller for the FOCO Cumulative Amount vs Goal (Current Year) chart
	Test Class:     BI_FOCOCumulativeAmountVsGoalChart_TEST
	History
	<Date>          <Author>           <Change Description>
	16-Oct-2014     Virgilio Utrera    Initial Version
	20-Oct-2014		Virgilio Utrera    Moved queries to BI_FOCOUtil class and added currency conversion
	11-Mar-2015		Virgilio Utrera    Refactored getChartData to handle large data volumes
-------------------------------------------------------------------*/

public class BI_FOCOCumulativeAmountVsGoalChart_CTRL {

	public transient Boolean renderErrorMessage { get; set; }
	public transient String chartData { get; set; }
	public transient String errorMessage { get; set; }
	public transient String userDefaultCurrency { get; set; }

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Class constructor
		IN:
		OUT:
		History
		<Date>          <Author>           <Change Description>
		16-Oct-2014     Virgilio Utrera    Initial Version
	-------------------------------------------------------------------*/

	public BI_FOCOCumulativeAmountVsGoalChart_CTRL() {
		errorMessage = '';
		chartData = '';
		getChartData();

		if(errorMessage.length() > 0)
			renderErrorMessage = true;
		else
			renderErrorMessage = false;
	}

	/*-------------------------------------------------------------------
		Author:         Virgilio Utrera
		Company:        Salesforce.com
		Description:    Gets the chart data for the running user
		IN:
		OUT:            
		History
		<Date>          <Author>           <Change Description>
		16-Oct-2014     Virgilio Utrera    Initial Version
		20-Oct-2014		Virgilio Utrera    Moved queries to BI_FOCOUtil class
		11-Mar-2015		Virgilio Utrera    Refactored method to handle large data volumes
	-------------------------------------------------------------------*/

	private void getChartData() {
		Decimal amount;
		Decimal cumulativeAmount;
		Decimal cumulativeGoal;
		Decimal goal;
		Id userId;
		List<AggregateResult> focoResults = new List<AggregateResult>();
		List<BI_Objetivo_Comercial__c> goalResults = new List<BI_Objetivo_Comercial__c>();
		List<CurrencyType> currenciesList = new List<CurrencyType>();
		Map<Integer, Decimal> amountsMap = new Map<Integer, Decimal>();
		Map<Integer, Decimal> goalsMap = new Map<Integer, Decimal>();
		Map<Integer, String> monthsMap = new Map<Integer, String>();
		Map<String, Double> conversionRatesMap = new Map<String, Double>();
		Set<Id> subordinateIds;
		String corporateCurrency;

		// Build months map. This map is used to output month names in the VF Page
		monthsMap.put(1, 'Enero');
		monthsMap.put(2, 'Febrero');
		monthsMap.put(3, 'Marzo');
		monthsMap.put(4, 'Abril');
		monthsMap.put(5, 'Mayo');
		monthsMap.put(6, 'Junio');
		monthsMap.put(7, 'Julio');
		monthsMap.put(8, 'Agosto');
		monthsMap.put(9, 'Septiembre');
		monthsMap.put(10, 'Octubre');
		monthsMap.put(11, 'Noviembre');
		monthsMap.put(12, 'Diciembre');

		amount = 0;
		goal = 0;
		cumulativeAmount = 0;
		cumulativeGoal = 0;
		userId = UserInfo.getUserId();
		userDefaultCurrency = UserInfo.getDefaultCurrency();
		subordinateIds = BI_FOCOUtil.getRoleSubordinateUsers(userId);
		currenciesList = BI_FOCOUtil.getCurrenciesList();

		// Fills up map with conversion rates, sets the currency corporate code
		for(CurrencyType currencyType : currenciesList) {
			conversionRatesMap.put(currencyType.IsoCode, currencyType.ConversionRate);

			if(currencyType.IsCorporate)
				corporateCurrency = currencyType.IsoCode;
		}

		// Get Ids from all subordinate users
		subordinateIds = BI_FOCOUtil.getRoleSubordinateUsers(userId);

		if(!subordinateIds.isEmpty())
			// There are subordinates, query FOCO records from user and subordinates for current year
			focoResults = BI_FOCOUtil.getCurrentYearFOCOAmountsByPeriod(userId, subordinateIds);
		else
			// There are no subordinates, query FOCO records from user for current year
			focoResults = BI_FOCOUtil.getCurrentYearFOCOAmountsByPeriod(userId, null);

		// Stop the execution if there are no FOCO records to show
		if(focoResults.isEmpty()) {
			errorMessage = 'No se han encontrado Registros de Datos FOCO para el año en curso';
			System.debug(errorMessage);
			return;
		}

		if(!subordinateIds.isEmpty())
			// There are subordinates, query commercial goals from user and subordinates for current year
			goalResults =  BI_FOCOUtil.getRevenueGoalsFromCurrentYear(userId, subordinateIds);
		else
			// There are no subordinates, query commercial goals from user for current year
			goalResults =  BI_FOCOUtil.getRevenueGoalsFromCurrentYear(userId, null);

		// Stop the execution if there are no commercial goal records to show
		if(goalResults.isEmpty() || goalResults[0].BI_Objetivo__c == null) {    
			errorMessage = 'No se han encontrado Objetivos Comerciales para el año en curso';
			System.debug(errorMessage);
			return;
		}

		// Populate amounts map
		for(AggregateResult ar : focoResults) {
			// FOCO results' BI_Monto__c is automatically returned in the org's currency because the query contains a SUM() aggregate function with a GROUP BY clause
			if(userDefaultCurrency == corporateCurrency)
				// No currency conversion needed
				amount = ((Decimal)ar.get('BI_Monto__c')).setScale(2, System.RoundingMode.CEILING);
			else
				// Convert amount into user's currency
				amount = (((Decimal)ar.get('BI_Monto__c')) * conversionRatesMap.get(userDefaultCurrency)).setScale(2, System.RoundingMode.CEILING);

			if((Date)ar.get('BI_Fecha_Inicio_Periodo__c') != null) {
				if(amountsMap.get(((Date)ar.get('BI_Fecha_Inicio_Periodo__c')).month()) != null)
					amountsMap.put(((Date)ar.get('BI_Fecha_Inicio_Periodo__c')).month(), amountsMap.get(((Date)ar.get('BI_Fecha_Inicio_Periodo__c')).month()) + amount);
				else
					amountsMap.put(((Date)ar.get('BI_Fecha_Inicio_Periodo__c')).month(), amount);
			}
		}

		// Populate goals map
		for(BI_Objetivo_Comercial__c oc : goalResults) {
			if(oc.CurrencyIsoCode == userDefaultCurrency)
				// Currency is the same as user's currency
				goal = oc.BI_Objetivo__c.setScale(2, System.RoundingMode.CEILING);
			else {
				if(oc.CurrencyIsoCode == corporateCurrency)
					// Currency is corporate currency, amount is converted into user's default currency
					goal = (oc.BI_Objetivo__c * conversionRatesMap.get(userDefaultCurrency)).setScale(2, System.RoundingMode.CEILING);
				else
					// Currency is other than user's or corporate
					goal = ((oc.BI_Objetivo__c / conversionRatesMap.get(oc.CurrencyIsoCode)) * conversionRatesMap.get(userDefaultCurrency)).setScale(2, System.RoundingMode.CEILING);
			}

			if(goalsMap.get(oc.BI_Periodo__r.BI_Fecha_Inicio_Periodo__c.month()) != null)
				goalsMap.put(oc.BI_Periodo__r.BI_Fecha_Inicio_Periodo__c.month(), goalsMap.get(oc.BI_Periodo__r.BI_Fecha_Inicio_Periodo__c.month()) + goal);
			else
				goalsMap.put(oc.BI_Periodo__r.BI_Fecha_Inicio_Periodo__c.month(), goal);
		}

		// Build the chart data for each month of the current calendar year
		chartData = '[[\'Mes\', \'Monto\', \'Objetivo\'], ';

		for(Integer i = 1; i <= 12; i++) {
			if(amountsMap.get(i) != null)
				cumulativeAmount = cumulativeAmount + amountsMap.get(i);

			if(goalsMap.get(i) != null) {
				cumulativeGoal = cumulativeGoal + goalsMap.get(i);
			}

			if(i == 12)
				chartData = chartData + '[\'' + monthsMap.get(i) + '\', ' + cumulativeAmount + ', ' + cumulativeGoal + ']]';
			else
				chartData = chartData + '[\'' + monthsMap.get(i) + '\', ' + cumulativeAmount + ', ' + cumulativeGoal + '], ';
		}
	}
}