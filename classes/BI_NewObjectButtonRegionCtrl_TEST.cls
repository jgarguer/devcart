@isTest 
private class BI_NewObjectButtonRegionCtrl_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_NewObjectButtonRegionCtrl class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    29/07/2014              Micah Burgos            Initial Version
    08/04/2015				Juan Santisi			Refactored lst_pais to be of type String. One TODO to fix.  Search for TODO
    20/09/2017                       Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
    25/09/2017              Jaime Regidor           Fields BI_Subsector__c and BI_Sector__c added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void BI_NewObjectButtonRegionWithUserRegion() {
           
        List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1); 
              
        List <Account> lst_acc =BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
        
        List<RecordType> lst_rt = [SELECT Id,DeveloperName FROM RecordType  WHERE DeveloperName = 'BI_Caso_Interno'  limit 1];
        
        Profile p = [select Id from Profile where Name = 'System Administrator' OR Name = :Label.BI_Administrador limit 1];
        User u = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = p.Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = 'America/Los_Angeles',
                          username = 'usertestuser@testorg.com',
                          Pais__c = Label.BI_Argentina);
        
        insert u;

        
        system.runAs(u){
             PageReference pageRef = new PageReference('BI_NewAccountButton');
             Test.setCurrentPage(pageRef);
             ApexPages.currentPage().getParameters().put('RecordType', 'test');
             ApexPages.currentPage().getParameters().put('currencyType', 'EUR');
             ApexPages.currentPage().getParameters().put('regionName', lst_pais[0]);
             ApexPages.currentPage().getParameters().put('opp_Id', '123456789012345');
             ApexPages.currentPage().getParameters().put('opp_Name', 'test');
             ApexPages.currentPage().getParameters().put('opp_Stage', 'test');
             ApexPages.currentPage().getParameters().put('acc_Name', 'test');
             ApexPages.currentPage().getParameters().put('acc_Name', 'test');
             ApexPages.currentPage().getParameters().put('acc_Name', 'test');
             ApexPages.currentPage().getParameters().put('acc_Id', lst_acc[0].Id);
             ApexPages.currentPage().getParameters().put('accid', lst_acc[0].Id);
             ApexPages.currentPage().getParameters().put('def_account_id', lst_acc[0].Id);

             ApexPages.currentPage().getParameters().put('otherParams', 't->es$$y->en');
             
             Account acc = new Account();
             ApexPages.StandardController sc = new ApexPages.StandardController(acc);
             
             BI_NewObjectButtonRegionCtrl controller = new BI_NewObjectButtonRegionCtrl(sc);
             
             controller.user_regionName = 'user_regionName';
             controller.user_regionId = 'user_regionId';
             controller.acc_regionId = 'regionId';
             controller.opp_Name = 'oppName';
             controller.recordTypeName = 'recordTypeName';
             controller.camp_Id = 'camp_Id';
             controller.camp_Name = 'camp_Name';
             controller.def_parent_Name = 'def_parent_Name';
             controller.def_contact_id = 'def_contact_id';
             controller.proyect_Id = 'proyect_id';
             controller.proyect_Name = 'proyect_name';

             controller.actionFunction();
             system.assertEquals(controller.currencyType, 'EUR');
             system.assertEquals(controller.recordType, 'test');
             system.assertEquals(controller.otherParams, 't=es&y=en'); 
             Opportunity opp = new Opportunity();
             ApexPages.StandardController scOpp = new ApexPages.StandardController(opp);
             BI_NewObjectButtonRegionCtrl controllerOpp = new BI_NewObjectButtonRegionCtrl(scOpp);
             controllerOpp.actionFunction();
             
             
             system.assertEquals(controllerOpp.opp_Id, '123456789012345');
             system.assertEquals(controllerOpp.opp_Stage, 'test');
             
             system.assertEquals(controllerOpp.acc_Id, lst_acc[0].Id);
             system.assertEquals(controllerOpp.acc_Name, lst_acc[0].Name);
             
           // TODO - Fix this assertion to be system.assertEquals().  The value of controllerOpp.acc_regionName
           // is null.
             Task otask = new Task();
             ApexPages.StandardController scTask = new ApexPages.StandardController(otask);
             BI_NewObjectButtonRegionCtrl controllerTask = new BI_NewObjectButtonRegionCtrl(scTask);
             controllerTask.actionFunction();

             Campaign camp = new Campaign();
             ApexPages.StandardController scCamp = new ApexPages.StandardController(camp);
             BI_NewObjectButtonRegionCtrl controllerCamp = new BI_NewObjectButtonRegionCtrl(scCamp);
             
             controllerCamp.actionFunction();
             
	    Case caso = new Case();
            caso.Type = 'Caso Interno';
            caso.Subject = 'Caso test';
            caso.BI_Otro_Tipo__c = 'test';
            caso.AccountId = lst_acc[0].Id;
            caso.Status = 'Assigned';
            insert caso;
            
            ApexPages.currentPage().getParameters().put('def_parent_id', caso.Id);
            
             ApexPages.StandardController scCaso = new ApexPages.StandardController(caso);
             BI_NewObjectButtonRegionCtrl controllerCaso = new BI_NewObjectButtonRegionCtrl(scCaso);
             controllerCaso.actionFunction();

             Milestone1_Milestone__c hito = new Milestone1_Milestone__c();
             ApexPages.StandardController scHito = new ApexPages.StandardController(hito);
             BI_NewObjectButtonRegionCtrl controllerHito = new BI_NewObjectButtonRegionCtrl(scHito);
             
             controllerHito.actionFunction();
             
             Milestone1_Project__c mproj = new Milestone1_Project__c();
             ApexPages.StandardController scMPrj = new ApexPages.StandardController(mproj);
             BI_NewObjectButtonRegionCtrl controllerMp = new BI_NewObjectButtonRegionCtrl(scMPrj);
             
             controllerMp.actionFunction(); 
             
             Contact cont = new Contact();
             ApexPages.StandardController scCont = new ApexPages.StandardController(cont);
             BI_NewObjectButtonRegionCtrl controllerCont = new BI_NewObjectButtonRegionCtrl(scCont);
             
             controllerCont.actionFunction(); 

             Lead olead = new Lead();
             ApexPages.StandardController scLead = new ApexPages.StandardController(olead);
             BI_NewObjectButtonRegionCtrl controllerLead = new BI_NewObjectButtonRegionCtrl(scLead);

             controllerLead.actionFunction();

             BI_Punto_de_instalacion__c oPuntInstal = new BI_Punto_de_instalacion__c();
             ApexPages.StandardController scPuntInstal = new ApexPages.StandardController(oPuntInstal);
             BI_NewObjectButtonRegionCtrl controllerPuntI = new BI_NewObjectButtonRegionCtrl(scPuntInstal);

             controllerPuntI.actionFunction();

             BI_Plan_de_accion__c oPlanAcc = new BI_Plan_de_accion__c();
             ApexPages.StandardController scPlanAcc = new ApexPages.StandardController(oPlanAcc);
             BI_NewObjectButtonRegionCtrl controllerPlanAcc = new BI_NewObjectButtonRegionCtrl(scPlanAcc);

             controllerPlanAcc.actionFunction();

        }
    }
    static testMethod void test2(){
        PageReference pageRef = new PageReference('BI_NewAccountButton');
        Test.setCurrentPage(pageRef);
        Account cuenta = new Account(Name = 'test',
                                      BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                      BI_Activo__c = Label.BI_Si,
                                      BI_Country__c = 'Colombia',
                                      BI_Segment__c= 'Empresas',
                                      BI_Subsector__c = 'test', //25/09/2017
                                      BI_Sector__c = 'test', //25/09/2017
                                      BI_Subsegment_Local__c = 'Multinacionales',
                                      BI_Subsegment_Regional__c = 'test',
                                      BI_Territory__c = 'test');
        insert cuenta;
        Case caso = new Case();
        caso.Type = 'Solicitud Incidencia Técnica';
        caso.Subject = 'Caso test';
        caso.BI_Otro_Tipo__c = 'test';
        caso.AccountId = cuenta.Id;
        caso.BI_Country__c = 'Colombia';
        caso.BusinessHoursId = '01mw0000000DKmgAAG';
        caso.Status = 'Assigned';
        insert caso;
        Opportunity opp = new Opportunity();
        opp.Name = 'opportunidad';
        opp.StageName = 'F6 - Preoportunidad';
        opp.CloseDate = Date.newInstance(2017, 12, 12);
        opp.AccountId = cuenta.Id;
        opp.BI_No_Identificador_fiscal__c = '860123';
        insert opp;
        ApexPages.currentPage().getParameters().put('def_parent_id', caso.Id);
        ApexPages.currentPage().getParameters().put('def_contact_id', 'def_contact_id');   
        ApexPages.currentPage().getParameters().put('opp_Id', opp.Id);   
                           

        ApexPages.StandardController scCasoRTypeBI2 = new ApexPages.StandardController(caso);
        BI_NewObjectButtonRegionCtrl controllerCasoBI2 = new BI_NewObjectButtonRegionCtrl(scCasoRTypeBI2);
        controllerCasoBI2.def_parent_id = 'caseId'; 
        controllerCasoBI2.rtype = 'BI2_caso_comercial'; 
        controllerCasoBI2.actionFunction();

        ApexPages.currentPage().getParameters().put('cas14', 'subject');
        ApexPages.currentPage().getParameters().put('cas7', 'status');
        ApexPages.currentPage().getParameters().put('cas11', 'origin');
        ApexPages.currentPage().getParameters().put('cas8', 'piority');
        ApexPages.currentPage().getParameters().put('cas4_lkid', 'id');   
        ApexPages.currentPage().getParameters().put('cas3_lkid', 'id');
                

        ApexPages.StandardController scCasoRTypeBIIN = new ApexPages.StandardController(caso);
        BI_NewObjectButtonRegionCtrl controllerCasoBIIN = new BI_NewObjectButtonRegionCtrl(scCasoRTypeBIIN);
        controllerCasoBIIN.rtype = 'BIIN_Solicitud_Incidencia_Tecnica';      
        controllerCasoBIIN.actionFunction(); 

        insert new TGS_User_Org__c(TGS_Is_Admin__c=false, TGS_Is_BI_EN__c=false, TGS_Is_TGS__c=true);

        ApexPages.StandardController scCasoRTypeOrder = new ApexPages.StandardController(caso);
        BI_NewObjectButtonRegionCtrl controllerCasoOrder = new BI_NewObjectButtonRegionCtrl(scCasoRTypeOrder);
        controllerCasoOrder.rtype = 'Order_Management_Case';   
        controllerCasoOrder.actionFunction();            
             
    }
}