public class BI_CampaignMemberHelper {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Methods executed by the CampaignMember trigger.
    
    History:
    
    <Date>            <Author>          <Description>
    15/04/2014        Pablo Oliva       Initial version
    14/05/2014        Pablo Oliva       Method 'createTaskPreOpp' modified
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Depending on the Campaign's RecordType, and the CampaignMember (Lead or Contact), generates tasks and preopportunities
    
    IN:            setId: Set with CampaignMember ids, or Campaign ids
                   option: 1 (trigger from CampaignMember)
                           2 (trigger from Campaign)
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    15/04/2014        Pablo Oliva       Initial version
    14/05/2014        Pablo Oliva       New recordType for preopportunities
    21/08/2014        Ignacio Llorca    New recordType for preopportunities
    05/09/2014        Ignacio Llorca    Description field added
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*    public static void createTaskPreOpp(Set<Id> setId, Integer option){
        try{
            List<Id> lst_account = new List<Id>();
            List<String> lst_cp = new List<String>();
            List<CampaignMember> lst_cm_opp = new List<CampaignMember>();
            List<Opportunity> lst_toCreateCatalog = new list<Opportunity>();
            
            List<Id> lst_accountReneg = new List<Id>();
            List<String> lst_cpReneg = new List<String>();
            List<CampaignMember> lst_cm_oppReneg = new List<CampaignMember>();
            
            List<Task> lst_tasks = new List<Task>();
            
            //Generates the query depending on the option selected
            Boolean primero = true;
            String set_id = '(';
            for(Id sid:setId){
                if(primero){
                    primero = false;
                    set_id += '\''+sid+'\'';
                }else
                    set_id += ','+'\''+sid+'\'';
            }
            set_id += ')';
            
            String query = 'select Id, CampaignId, Campaign.Name, Campaign.BI_Pais_Ref__c, Campaign.EndDate, Campaign.BI_Tipo_de_preoportunidad__c, Campaign.BI_Crear_tareas_prospectos__c, Campaign.BI_Crear_oportunidad_contactos__c, Campaign.IsActive, Campaign.OwnerId, Campaign.RecordType.DeveloperName, LeadId, ContactId, Contact.AccountId, Contact.Account.Name, Contact.Account.BI_Riesgo__c, Contact.Account.BI_Fraude__c, Contact.Account.BI_Activo__c, Contact.Account.OwnerId, Contact.Account.BI_Pais_ref__c from CampaignMember where '+ ((option == 1)?'Id':'CampaignId') +' IN '+set_id;
            
            system.debug('BI_CampaignMemberHelper.createTaskPreOpp() -> QUERY: '+query);
            
            List<CampaignMember> lst_cm = DataBase.query(query);
            
            for(CampaignMember cmem:lst_cm){
                
                if(cmem.Campaign.IsActive && cmem.Campaign.RecordType.DeveloperName != Label.BI_Evento ){
                    /*
                    if(cm.Campaign.RecordType.DeveloperName.startswith('BI_Fidelizacion')){
                        
                        system.debug('Fidelización');
                        
                        if(cm.Campaign.BI_Crear_tareas_prospectos__c == Label.BI_Si){
                            
                            system.debug('Fidelización 2');
                            
                            if(cm.LeadId != null)
                                lst_tasks.add(createTaskLead(cm.Campaign.Name, cm.Campaign.EndDate, cm.Campaign.OwnerId, cm.LeadId, cm.CampaignId));//Generates the task
                            else if(cm.ContactId != null)
                                lst_tasks.add(createTaskContact(cm.CampaignId, cm.Campaign.EndDate, cm.Contact.Account.OwnerId, cm.ContactId));//Generates the task
                                
                            system.debug('Fidelización 3 '+lst_tasks);    
                            
                        }
                    }else
                    **** 
                    //if(cm.Campaign.RecordType.DeveloperName.startswith('BI_Venta_de_productos')){
                  
                        
                        if(cmem.LeadId != null){
                            
                          
                                if(cmem.Campaign.BI_Crear_tareas_prospectos__c == Label.BI_Si){
                                    
                                    system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->cmem.Campaign.BI_Crear_tareas_prospectos__c: ' + cmem.Campaign.BI_Crear_tareas_prospectos__c);
                                    
                                    lst_tasks.add(createTaskLead(cmem.Campaign.Name, cmem.Campaign.EndDate, cmem.Campaign.OwnerId, cmem.LeadId, cmem.CampaignId));//Generates the task
                                }
                        }else{
                             if(cmem.Contact.Account.BI_Fraude__c != true && cmem.Contact.Account.BI_Riesgo__c != Label.BI_BloqueoCliente){
                                system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->cmem.Contact.Account.BI_Activo__c: ' + cmem.Contact.Account.BI_Activo__c);
                                system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->cmem.Campaign.BI_Crear_oportunidad_contactos__c: ' + cmem.Campaign.BI_Crear_oportunidad_contactos__c);
                                system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->cmem.Campaign.BI_Tipo_de_preoportunidad__c: ' + cmem.Campaign.BI_Tipo_de_preoportunidad__c);
                                
                                if(cmem.Contact.Account.BI_Activo__c == Label.BI_Si && cmem.Campaign.BI_Crear_oportunidad_contactos__c == Label.BI_Si && cmem.Campaign.BI_Tipo_de_preoportunidad__c== Label.BI_NuevaVenta){
                                    
                                    lst_account.add(cmem.Contact.AccountId);
                                    lst_cp.add(cmem.CampaignId);
                                    lst_cm_opp.add(cmem);
                                    
                                }else if(cmem.Contact.Account.BI_Activo__c == Label.BI_Si && cmem.Campaign.BI_Crear_oportunidad_contactos__c == Label.BI_Si && cmem.Campaign.BI_Tipo_de_preoportunidad__c== Label.BI_Preoportunidad_Renegociacion){
                                    lst_accountReneg.add(cmem.Contact.AccountId);
                                    lst_cpReneg.add(cmem.CampaignId);
                                    lst_cm_oppReneg.add(cmem);
                                }
                             }
                        }
                        
                        
                            system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->lst_account: ' + lst_account);
                            system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->lst_cp: ' + lst_cp);
                            system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->lst_cm_opp: ' + lst_cm_opp);
                            
                            system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->lst_accountReneg: ' + lst_accountReneg);
                            system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->lst_cpReneg: ' + lst_cpReneg);
                            system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->lst_cm_oppReneg: ' + lst_cm_oppReneg);
                        
            
                    
                    //}
                    
                }
                
            }

            map<String,Id> map_rect = new map<String,Id>();
            if(!lst_cm_opp.isEmpty() || !lst_cm_oppReneg.isEmpty()){
                for(Recordtype rt :[select Id,DeveloperName from RecordType where SObjectType = 'Opportunity' AND (DeveloperName =:Label.BI_Preoportunidad OR DeveloperName =:Label.BI_Preoportunidad_renegociacionRT)]){
                    map_rect.put(rt.DeveloperName,rt.Id);
                }
                      
            }
            
            Set<String> name_opp = new Set<String>();
            List<Opportunity> lst_o = new List<Opportunity>();
            if(!lst_cm_opp.isEmpty()){//Generates the preOpportunity
                String DescProd ='';
                BI_Promociones__c [] lst_prom = [SELECT Id, Nombre_del_producto__c, BI_Campanas__c FROM BI_Promociones__c WHERE BI_Campanas__c IN :lst_cp];
                name_opp = new Set<String>();
                lst_o = new List<Opportunity>();
                
                for(Opportunity opp:[select Id, AccountId, CampaignId from Opportunity where AccountId IN :lst_account AND CampaignId IN :lst_cp]){
                    name_opp.add(opp.AccountId+'---'+opp.CampaignId);
                }
                    
                 system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->name_opp (list): ' + name_opp);
                 
                for(CampaignMember cm2:lst_cm_opp){
                    DescProd ='';
                    String name_aux = cm2.Contact.AccountId+'---'+cm2.CampaignId;
                    for(BI_Promociones__c prom: lst_prom){
                        if (cm2.CampaignId == prom.BI_Campanas__c){
                            if (DescProd==''){                          
                                DescProd += Label.BI_Productos_campana +' \''+cm2.Campaign.name+'\': '+prom.Nombre_del_producto__c;
                            }else{
                                DescProd += ' | ' + prom.Nombre_del_producto__c;                            
                            }
                        }
                    }
                    system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->name_opp + name_aux: ' + name_opp+'---'+name_aux);
                    
                    if(!name_opp.contains(name_aux)){
                            Opportunity opp = new Opportunity(CloseDate = cm2.Campaign.EndDate,
                                                            StageName = Label.BI_F6Preoportunidad,
                                                            Probability = 0,
                                                            BI_Country__c = cm2.Contact.Account.BI_Country__c,

                                                            OwnerId = cm2.Contact.Account.OwnerId,
                                                            Name = cm2.Campaign.Name+'-'+cm2.Contact.Account.Name+'-Preoportunidad',
                                                            CampaignId = cm2.CampaignId,
                                                            AccountId = cm2.Contact.AccountId,
                                                            RecordTypeId = map_rect.get(Label.BI_Preoportunidad),
                                                            Description = DescProd,
                                                            NE__HaveActiveLineItem__c = true);
                                                            
                            lst_o.add(opp);
                            name_opp.add(name_aux);
                    }
                }
                
                system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->lst_o1: ' + lst_o);
                insert lst_o;
                
                lst_toCreateCatalog.addAll(lst_o);
                
            }
            if(!lst_cm_oppReneg.isEmpty()){//Generates the preOpportunity Reneg
                String DescProdReneg ='';
                BI_Promociones__c [] lst_promReneg = [SELECT Id, Nombre_del_producto__c, BI_Campanas__c FROM BI_Promociones__c WHERE BI_Campanas__c IN :lst_cpReneg];
                
                name_opp = new Set<String>();
                lst_o = new List<Opportunity>();
                
                for(Opportunity opp:[select Id, AccountId, CampaignId from Opportunity where AccountId IN :lst_accountReneg AND CampaignId IN :lst_cpReneg])
                    name_opp.add(opp.AccountId+'---'+opp.CampaignId);
                
                for(CampaignMember cm2:lst_cm_oppReneg){
                    String name_aux = cm2.Contact.AccountId+'---'+cm2.CampaignId;
                    DescProdReneg ='';
                    for(BI_Promociones__c prom: lst_promReneg){
                        if (cm2.CampaignId == prom.BI_Campanas__c){
                            if(DescProdReneg==''){
                                DescProdReneg += Label.BI_Productos_campana +' \''+cm2.Campaign.name+'\': '+prom.Nombre_del_producto__c;
                            }else{
                                DescProdReneg += ' | ' + prom.Nombre_del_producto__c;
                            }
                        }
                    }                   
                    system.debug(name_opp+'---'+name_aux);
                    
                    if(!name_opp.contains(name_aux)){
                            Opportunity opp = new Opportunity(CloseDate = cm2.Campaign.EndDate,
                                                            StageName = Label.BI_F6Preoportunidad,
                                                            Probability = 0,
                                                   //Commented out: BI_Pais__c = cm2.Contact.Account.BI_Pais_ref__c,
                                                            OwnerId = cm2.Contact.Account.OwnerId,
                                                            Name = cm2.Campaign.Name+'-'+cm2.Contact.Account.Name+'-Preoportunidad',
                                                            CampaignId = cm2.CampaignId,
                                                            AccountId = cm2.Contact.AccountId,
                                                            RecordTypeId = map_rect.get(Label.BI_Preoportunidad_renegociacionRT),
                                                            Description=DescProdReneg,
                                                            NE__HaveActiveLineItem__c = true);
                                                            
                            lst_o.add(opp);
                            name_opp.add(name_aux);
                    }
                }
                
                system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->lst_o2: ' + lst_o);
                system.debug(lst_o);
                insert lst_o;
                
                lst_toCreateCatalog.addAll(lst_o);

                
            }
            

            createCatalog(lst_toCreateCatalog);
            system.debug('***BI_CampaignMemberHelper.createTaskPreOpp-->lst_tasks(BEFORE): ' + lst_tasks);
            insert lst_tasks;
        
        }catch (exception  Exc){
            BI_LogHelper.generate_BILog('BI_CampaignMemberHelper.createTaskPreOpp', 'BI_EN', Exc, 'Trigger');
        }
        
    }*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Generates a task with the given parameters
    
    IN:            idCampaign: Related Campaign.Id
                   closeDate:  Related Campaign.EndDate
                   assignTo:   Task owner
    OUT:           Task (individual record)
    
    History:
    
    <Date>            <Author>          <Description>
    15/04/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*    public static Task createTaskContact(Id idCampaign, Date closeDate, Id assignTo, Id idRelated){
        try{
            Task tsk = new Task(Subject = Label.BI_Trigger_CampaignMember_createTask,
                              WhatId = idCampaign,
                              Priority = Label.BI_Normal,
                              ActivityDate = closeDate,
                              OwnerId = assignTo,
                              Status = Label.BI_EnCurso,
                              WhoId = idRelated);
                              
            return tsk;
        }catch (exception  Exc){
                BI_LogHelper.generate_BILog('BI_CampaignMemberHelper.createTaskContact', 'BI_EN', Exc, 'Trigger');
                return null;
        }
    }*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Generates a task with the given parameters
    
    IN:            idCampaign: Related Campaign.Id
                   closeDate:  Related Campaign.EndDate
                   assignTo:   Task owner
                   TaskName: Description + Campaign.Name
                   idCampaign: BI_CampaignId__c
    OUT:           Task (individual record)
    
    History:
    
    <Date>            <Author>              <Description>
    23/04/2014        Ignacio Llorca       Initial version
    03/03/2015        Gawron, Julián        Fix whatId     eHelp 02388950
    28/03/2017        Jaime Regidor        Añadir nombre de la campaña Demanda 376
    21/06/2017        Pablo de Andrés       Modificados valores de Subject y OwnerId
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Task createTaskLead(String TaskName, Date closeDate, Id assignTo, Id idRelated, Id idCampaign, Id ownerId, String asuntoTask){
        System.debug('La campaña es: ' + idCampaign);
        try{
            /*
            Task tsk = new Task(Subject = Label.BI_Trigger_CampaignMember_createTask,
                              //WhatId = idCampaign,   //JEG
                              Priority = Label.BI_Normal,
                              ActivityDate = closeDate,
                              OwnerId = assignTo,
                              Status = Label.BI_EnCurso,
                              WhoId = idRelated,
                              Description = Label.BI_SegCampa_a + ' ' + TaskName,
                              BI_CampaignId__c = idCampaign,
                              BI_Campana__c = idCampaign);
                              
            return tsk;
            */
            
            //Generamos la tarea correspondiente
            Task tsk = new Task(Subject = !String.isEmpty(asuntoTask) ? asuntoTask : Label.BI_Trigger_CampaignMember_createTask,
                              Priority = Label.BI_Normal,
                              ActivityDate = closeDate,
                              OwnerId = ownerId,
                              Status = Label.BI_EnCurso,
                              WhoId = idRelated,
                              Description = Label.BI_SegCampa_a + ' ' + TaskName,
                              BI_CampaignId__c = idCampaign,
                              BI_Campana__c = idCampaign);
            //No se pueden relacionar las tareas de lead a otro objeto que no sea lead en el campo WhatId
            /*if(!campaignL.isEmpty()){
                tsk.WhatId = idCampaign;
            }*/
            return tsk;
        }catch (exception  Exc){
                BI_LogHelper.generate_BILog('BI_CampaignMemberHelper.createTaskLead', 'BI_EN', Exc, 'Trigger');
                return null;
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Generates a CampaignMemberStatus and creates its sortorder depending on the last CampaignMemberStatus created
    
    IN:            setCampaigns
    
    History:
    
    <Date>            <Author>          <Description>
    21/04/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void createCampaignMemberStatus(Set<Id> setCampaigns){
        try{
            List<CampaignMemberStatus> list_cms = [select Id,CampaignId,Label,SortOrder from CampaignMemberStatus where CampaignId IN :setCampaigns];
            List<CampaignMemberStatus> list_cms_insert = new List<CampaignMemberStatus>();
            system.debug (list_cms + 'DEBUG LIST CMS');
            for(Id id:setCampaigns){
                
                Boolean pen = true;
                Boolean apr = true;
                Boolean rech = true;
                
                Integer last_order = 2;
     
                for(CampaignMemberStatus cms:list_cms){
                    if(cms.CampaignId == id ){
                    
                        system.debug('ENTRO '+cms.Label+'---'+cms.CampaignId);
                        
                        if(cms.Label == Label.BI_PendienteAprobacion)
                            pen = false;
                        else if(cms.Label == Label.BI_Aprobado)
                            apr = false;
                        else if(cms.Label == Label.BI_Rechazado)
                            rech = false;
                        
                        if(cms.SortOrder > last_order)
                            last_order = cms.SortOrder;
                    }
                }
                
                system.debug('last_order: '+last_order);
                
                if(pen || apr || rech){
                    if(pen){
                        last_order++;
                        list_cms_insert.add(createCMS(Label.BI_PendienteAprobacion,id,last_order));
                    }
                    if(apr){
                        last_order++;
                        list_cms_insert.add(createCMS(Label.BI_Aprobado,id,last_order));
                    }
                    if(rech){
                        last_order++;
                        list_cms_insert.add(createCMS(Label.BI_Rechazado,id,last_order));
                    }
                }
            }
            
            system.debug(list_cms_insert);
            
            if(!list_cms_insert.isEmpty())
                insert list_cms_insert;
        }catch (exception  Exc){
            BI_LogHelper.generate_BILog('BI_CampaignMemberHelper.createCampaignMemberStatus', 'BI_EN', Exc, 'Trigger');
        }
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Creates a CampaignMemberStatus with the given parameters
    
    IN:            label, id, last_order
    
    History:
    
    <Date>            <Author>          <Description>
    21/04/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static CampaignMemberStatus createCMS(String label, Id id, Integer last_order){
        try{
            CampaignMemberStatus cms = new CampaignMemberStatus(Label = label,
                                                                SortOrder = last_order,
                                                                CampaignId = id);
                                                                
            return cms;
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_CampaignMemberHelper.createCMS', 'BI_EN', Exc, 'Trigger');
            return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Creates a CampaignMemberStatus with the given parameters
    
    IN:            list<Opportunities>
    
    History:
    
    <Date>            <Author>          <Description>
    21/04/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*    public static void createCatalog (list<Opportunity> newsOpp){
        try{
            
            //mapa ID, Opp  
            map<Id,Opportunity> map_newsOpp = new map<Id,Opportunity>();
            set<Id> set_catalogHeader = new Set<Id>();
            
            for(Opportunity oppo:[SELECT AccountId, CampaignId, Campaign.BI_Catalogo__c, Campaign.BI_Catalogo__r.NE__Catalog_Header__c, CurrencyISOCode FROM Opportunity WHERE Id IN :newsOpp]){
              map_newsOpp.put(oppo.Id, oppo);
              set_catalogHeader.add(oppo.Campaign.BI_Catalogo__r.NE__Catalog_Header__c);
            }
            
            if(!map_newsOpp.keySet().isEmpty()){
              
              Map<Id, Id> map_catalog_commercial = new Map<Id, Id>();
              if(!set_catalogHeader.isEmpty()){
                for(NE__Commercial_Model__c comm:[select Id, NE__Catalog_header__c from NE__Commercial_Model__c where NE__Catalog_header__c IN :set_catalogHeader])
                  map_catalog_commercial.put(comm.NE__Catalog_header__c, comm.Id);
              }
              
              List<RecordType> rtOrder = [SELECT DeveloperName, Id, SobjectType FROM RecordType WHERE SobjectType = 'NE__Order__c' AND DeveloperName = 'Opty' limit 1];
            if(!rtOrder.isEmpty()){
          
                list<NE__Order__c> lst_orders = new list<NE__Order__c>();
                set<Id> set_campId = new set<Id>();
                for(Opportunity item :map_newsOpp.values()){
                    
                    NE__Order__c ord = new NE__Order__c();
                    ord.NE__TotalRecurringFrequency__c = 'Monthly';
                    ord.NE__OptyId__c = item.Id;
                    ord.NE__OrderStatus__c = Label.BI_Active;
                    ord.NE__AccountId__c = item.AccountId;
                    ord.NE__Order_date__c = Datetime.now();
                    ord.CurrencyISOCode = item.CurrencyIsoCode;
                    if(item.Campaign != null){
                      ord.NE__CatalogId__c = item.Campaign.BI_Catalogo__c;
                      if(item.Campaign.BI_Catalogo__c != null)
                        ord.NE__CommercialModelId__c = map_catalog_commercial.get(item.Campaign.BI_Catalogo__r.NE__Catalog_Header__c);
                    }
                    ord.RecordTypeId = rtOrder[0].Id;
                      
                    lst_orders.add(ord);
                    
                    set_campId.add(item.CampaignId);
                }
                       
                //Insert NE_ORDER__c
                insert lst_orders;
                
                map<Id,Campaign> map_camp = new map<Id,Campaign>([SELECT Id, (SELECT CurrencyISOcode, BI_Ingreso_por_unica_vez__c, Recurrente_bruto_mensual__c, 
                                          BI_Producto__r.NE__ProductId__c, BI_Producto__c, 
                                          BI_Producto__r.NE__Base_OneTime_Fee__c, BI_Producto__r.NE__BaseRecurringCharge__c FROM BI_Promociones__r)  
                                          FROM Campaign WHERE Id IN :set_campId]);
              
                    system.debug('BI_CampaignMemberHelper->map_camp: ' + map_camp);

                RecordType rtOrdItem = [SELECT Id FROM RecordType WHERE SobjectType = 'NE__OrderItem__c' AND DeveloperName = 'StanStandarddard' limit 1];
                list<NE__OrderItem__c> lst_ordItem = new list<NE__OrderItem__c>();
    
          Map<Id, Double> map_FCV = new Map<Id, Double>();
                
                    system.debug('BI_CampaignMemberHelper->lst_orders: ' + lst_orders);

                for(NE__Order__c ord :lst_orders){
                  
                  Double value = 0;
                        
                  system.debug('BI_CampaignMemberHelper->1: ' + map_newsOpp.get(ord.NE__OptyId__c));
                        system.debug('BI_CampaignMemberHelper->2: ' + map_camp.get(map_newsOpp.get(ord.NE__OptyId__c).CampaignId));
                        
                        ord.NE__One_Time_Fee_Total__c = 0;
                        ord.NE__Recurring_Charge_Total__c = 0;

                    if(!map_camp.get(map_newsOpp.get(ord.NE__OptyId__c).CampaignId).BI_Promociones__r.isEmpty()){
                    
                        for(BI_Promociones__c promo :map_camp.get(map_newsOpp.get(ord.NE__OptyId__c).CampaignId).BI_Promociones__r){
                            
                            NE__OrderItem__c ordItem = new NE__OrderItem__c();
                                                   
                            ordItem.NE__OrderId__c = ord.Id;                        
                            ordItem.NE__Account__c = ord.NE__AccountId__c;          
                            
                            ordItem.RecordTypeId = rtOrdItem.Id;
                            ordItem.NE__Qty__c = 1;
                            ordItem.NE__Status__c = 'Pending';
                            
                            if(promo.BI_Ingreso_por_unica_vez__c != null){ 
                              ordItem.NE__OneTimeFeeOv__c = promo.BI_Ingreso_por_unica_vez__c; 
                              ordItem.NE__RecurringChargeOv__c = promo.Recurrente_bruto_mensual__c;
                              ordItem.NE__Commitment_Period__c = 12;//promo.BI_Permanencia_meses__c;
                          }else{
                            //if(promo.BI_Producto__r)
                            ordItem.NE__OneTimeFeeOv__c = promo.BI_Producto__r.NE__Base_OneTime_Fee__c; 
                              ordItem.NE__RecurringChargeOv__c = promo.BI_Producto__r.NE__BaseRecurringCharge__c;
                              ordItem.NE__Commitment_Period__c = 12;//promo.BI_Permanencia_meses__c;
                          }
                          
                            ordItem.NE__ProdId__c = promo.BI_Producto__r.NE__ProductId__c;
                            ordItem.CurrencyISOcode = promo.CurrencyISOcode;
                            
                            ordItem.NE__CatalogItem__c = promo.BI_Producto__c;
                            
                            Double value_aux = ordItem.NE__OneTimeFeeOv__c + (ordItem.NE__RecurringChargeOv__c * 12);
                            
                            if(ordItem.CurrencyISOcode != ord.CurrencyIsoCode)
                              value_aux = BI_CurrencyHelper.convertCurrency(ordItem.CurrencyISOcode, ord.CurrencyIsoCode, value_aux);
                            
                            value += value_aux;
                            
                            ord.NE__One_Time_Fee_Total__c += BI_CurrencyHelper.convertCurrency(ordItem.CurrencyISOcode, ord.CurrencyIsoCode, Double.valueOf(ordItem.NE__OneTimeFeeOv__c));
                            ord.NE__Recurring_Charge_Total__c += BI_CurrencyHelper.convertCurrency(ordItem.CurrencyISOcode, ord.CurrencyIsoCode, Double.valueOf(ordItem.NE__RecurringChargeOv__c));
                            
                            lst_ordItem.add(ordItem);   
                        }
                    }
                    
                    map_newsOpp.get(ord.NE__OptyId__c).BI_Ingreso_por_unica_vez__c = ord.NE__One_Time_Fee_Total__c;
                    map_newsOpp.get(ord.NE__OptyId__c).BI_Recurrente_bruto_mensual__c = ord.NE__Recurring_Charge_Total__c;
                    map_newsOpp.get(ord.NE__OptyId__c).BI_Duracion_del_contrato_Meses__c = 12;
                    
                    map_newsOpp.get(ord.NE__OptyId__c).BI_casilla_desarrollo__c = true;
                    
                    system.debug('TEST: '+ord.NE__One_Time_Fee_Total__c);
                    system.debug('TEST2: '+ord.NE__Recurring_Charge_Total__c);
                    
                    //map_FCV.put(ord.NE__OptyId__c, value);
                    
                }   
                
                //Insert NE_OrderItems__c 
                insert lst_ordItem;
                
                update lst_orders;
                
                update map_newsOpp.values();
                
                /*List<Opportunity> lst_opp_update = new List<Opportunity>();
                
                for(Id idOpp:map_FCV.keySet()){
                  
                  if(map_FCV.get(idOpp) != null && map_newsOpp.get(idOpp) != null){
                    
                    map_newsOpp.get(idOpp).Amount = map_FCV.get(idOpp);
                    
                    lst_opp_update.add(map_newsOpp.get(idOpp));
                  }
                  
                }
                
                update lst_opp_update;****
                
              }
            
          }    
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_CampaignMemberHelper.createCatalog', 'BI_EN', Exc, 'Trigger');
        }
    }*/
    
}