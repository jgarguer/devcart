public with sharing class BI_ISC_Edit_CTRL {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Ignacio Llorca
Company:       Salesforce.com
Description:   Controller of BI_ISC_Edit page

History:
 
<Date>                  <Author>                <Change Description>
27/03/2015              Ignacio Llorca        	Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public BI_ISC2__c isc {get; set;}
	public String response {get; set;}

	public BI_ISC_Edit_CTRL(ApexPages.StandardController controller) {
		isc = (BI_ISC2__c)controller.getRecord();
		List <BI_Plan_de_accion__c> actionPlan = [SELECT Id, BI_Estado__c FROM BI_Plan_de_accion__c WHERE BI_Termometro_ISC__c =: isc.Id];
		
		if(actionPlan[0].BI_Estado__c == 'Pendiente finalización' || 
			actionPlan[0].BI_Estado__c == 'Finalizado' || 
			actionPlan[0].BI_Estado__c == 'Cancelado'){
			response = Label.BI_ISCEdit;
		}else{
			response = 'ok';
		}
	}
}