@isTest
private class BI_O4_OrderItemMethods_TEST {
	
	static{
        BI_TestUtils.throw_exception = false;
    }
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Pedro Párraga
	Company:       New Energy Aborda
	Description:   Test class for BI_O4_OrderItemMethods
	
	History:
	
	<Date>					<Author>				<Change Description>
	22/09/2016				Pedro Párraga			Initial version
	------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	@isTest static void updateOpptyForApproval() {
		//BI_TestUtils.throw_exception = false;

		List<NE__OrderItem__c> listOrderItemNew = new List<NE__OrderItem__c>();
		List<NE__OrderItem__c> listOrderItemOld = new List<NE__OrderItem__c>();
		Map<Id, NE__OrderItem__c> mapOldOrderItems = new Map<Id, NE__OrderItem__c>();

	    Opportunity opp = new Opportunity(Name = 'OppName',
                      					  CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                      					  BI_Ciclo_ventas__c = Label.BI_Completo,
                      					  BI_O4_Tipo_de_OportunidadTNA__c = 'Acquisition'
                      					 
                      					  );

	    insert opp;

	    NE__Order__c ne_order = new NE__Order__c(NE__OptyId__c = opp.Id);
		insert ne_order;

		NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;
        NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__Change_Subtype__c = 'test', NE__Disconnect_Subtype__c = 'test', BI_O4_Acquisition_Margin__c = 30);
        insert catIt;

		NE__OrderItem__c item = new NE__OrderItem__c(NE__OrderId__c = ne_order.Id,
													NE__Qty__c = 2,
													NE__Status__c = 'Active',
													NE__Recurring_Cost__c = 0,
													NE__One_Time_Cost__c = 0,
													NE__RecurringChargeOv__c = 0,
													NE__OneTimeFeeOv__c = 0,
													BI_O4_Total_price__c = 3000													
													);
		listOrderItemNew.add(item);
		insert listOrderItemNew;

		NE__OrderItem__c itemd = new NE__OrderItem__c(NE__OrderId__c = ne_order.Id,
													NE__Qty__c = 3,
													NE__Status__c = 'Active',
													NE__Recurring_Cost__c = 0,
													NE__One_Time_Cost__c = 0,
													NE__RecurringChargeOv__c = 0,
													NE__OneTimeFeeOv__c = 0
													);		


		insert itemd;
		mapOldOrderItems.put(item.Id, itemd);
		
		Test.startTest();
		BI_O4_OrderItemMethods.updateOpptyForApproval(listOrderItemNew, listOrderItemOld);
		BI_O4_OrderItemMethods.fillInvoicingModelAndUnit(listOrderItemNew,true);
		BI_O4_OrderItemMethods.setTotalPriceAndCost(listOrderItemNew);
		BI_O4_OrderItemMethods.setTotalPriceAndCost(listOrderItemNew, mapOldOrderItems);



		try{
			BI_O4_OrderItemMethods.updateOpptyForApproval(null, null);
		}catch(Exception e){
			System.assert(e.getMessage().Contains('Attempt to de-reference a null object'));
		}

		try{
			BI_O4_OrderItemMethods.bypassEconomicApproval(null);			
		}catch(Exception e){
			System.assert(e.getMessage().Contains('Attempt to de-reference a null object'));
		}

		try{
			BI_O4_OrderItemMethods.fillInvoicingModelAndUnit(null,true);
		}catch(Exception e){
			System.assert(e.getMessage().Contains('Attempt to de-reference a null object'));
		}

		try{
			BI_O4_OrderItemMethods.setTotalPriceAndCost(null);
		}catch(Exception e){
			System.assert(e.getMessage().Contains('Attempt to de-reference a null object'));
		}




		Test.stopTest();
	}

		/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Pedro Párraga
	Company:       New Energy Aborda
	Description:   Test class for BI_O4_OrderItemMethods
	
	History:
	
	<Date>					<Author>				<Change Description>
	22/09/2016				Pedro Párraga			Initial version
	20/09/2017        Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
	------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	@isTest static void bypassEconomicApproval() {
		//BI_TestUtils.throw_exception = false;
		List<RecordType> rts = [SELECT Id FROM RecordType 
        WHERE DeveloperName = 'BI_Ciclo_completo'];

        List<RecordType> rtsAcc = [SELECT Id FROM RecordType 
        WHERE DeveloperName = 'TGS_Holding'];

    	Account acc = new Account(Name = 'AccName', NE__Surname__c = 'AcclastName', RecordTypeId = rtsAcc[0].Id,
							    		       BI_Segment__c = 'test',
							        BI_Subsegment_Regional__c = 'test',
							        BI_Territory__c = 'test');
    	insert acc;

		Opportunity opp = new Opportunity(Name = 'OppNamePadre',
                      					  CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                      					  BI_Ciclo_ventas__c = Label.BI_Completo, 
                      					  RecordTypeId = rts[0].Id,
                      					  AccountId = acc.Id
                      					  );
		insert opp;
		
		Test.startTest();
		BI_O4_OrderItemMethods.bypassEconomicApproval(opp);
		Test.stopTest();	

	}
}