global class CWP_AC_BulkAutoQuotesVFCtrl {

    
    global Boolean enableStandardSalesforce {get;set;} // boolean for custom B2W

    public String strRecordType {get; set;} // orders record type - in this case "Opty" recordtype
    public String strPrdItemTreeJSON {get; set;} // Catalog Items hierarchyeverything at 
    public String strPrdItemJSON {get; set;}// Each catalog item
    public String idCatalogItemId {get; set;} 
    public String stringCatalogName {get; set;}
    public String strAccountId {get; set;}
    public List<sObject> dynLookUpData {get; set;}
    public Component.Apex.PageblockTable dynLookUpTable {get; set;}
    public Map<String,List<String>> mapStrProductsIds;
    public String strOrderType='Quote';
    public String mail;
    public String strCsvData {get; set;}
    public String birId {get; set;}    
    public  static final integer BLOCKSIZE = 100;
    public String jsonSC {get; set;}
    public String jsonMD {get; set;}
    public String jsonCT {get; set;}

    //Client retrieved to get info Margin/Discount from CWP_AC_MARGINDISCOUNTS
    public String accClient {get; set;}

    //RNAwip 09/04/2018 - String to get User-Account from component
    public String useracc {get; set;}

    //04-03-2018 - Manuel Ochoa -  new variables for price calculation
    public NE__Order__c orderToProcess;
    public Id catalogID;
    public Id categoryId;
    public String strItemCodeConnProject;
    public String strProdIdConnProject;
    public String strCountry;
    public NE.UpsertItemsService.Output outputUpsert=new NE.UpsertItemsService.Output();

    // JSON for grid
    public String completeDataListJSON {get;set;} // data for grid
    public String changes {get;set;} // Data changed in the grid
    public String tableChangesJson {get;set;} //Changes in the JSon

    //RNAWip Wrapper Site?
    public class WrapSite {
        public Integer rowGrid;
		public String country;
		public String city;
		public Integer contractTerm;
		public List<Product> product;
	}

	public class Product {
		public String productName;
        public String bandwidth;
        public String router;
	}
    //RNAWip END Wrapper Site
    //Public String stringCatalogName = 'Wholesale Catalog';

    /*------------------------------------------------------------
    Author:        Roberto Niubó
    Company:       Accenture (NEAborda)
    Description:   Method for receiving tableChangesJson and returning a complete list of ServicesCost into JS
    Test Class:    
    
    History
    <Date>      <Author>            <Description>
    23-02-2018  Roberto Niubó       Initial version
    13-03-2018  Roberto Niubó       Adding Margin and Discounts functions 
    ------------------------------------------------------------*/
    public void getServiceCosts(){

        //Strating of query
        String queryFilter = 'SELECT Id, CWP_AC_Country__c, CWP_AC_City__c, CWP_AC_Contract_Term__c, CWP_AC_ServiceName__c, CWP_AC_Bandwidth__c, CWP_AC_Router__c, CWP_AC_Coste_MRC__c, CWP_AC_Coste_CPE_MRC__c	,CWP_AC_Coste_CPE_NRC__c, CWP_AC_Coste_NRC__c, CWP_AC_Precio_Internacional_MRC__c, CurrencyIsoCode, CWP_AC_Technology__c, CWP_AC_Specifications__c FROM CWP_AC_ServicesCost__c WHERE ';
        List<CWP_AC_ServicesCost__c> serviceCosts = new List<CWP_AC_ServicesCost__c>();
        Map<String, List<CWP_AC_ServicesCost__c>> mapSc = new Map<String, List<CWP_AC_ServicesCost__c>>();
        Map<String, Set<Decimal>> mapCT = new Map<String, Set<Decimal>>();
        List<List<String>> listStrTableChanges = (List<List<String>>)JSON.deserialize(tableChangesJson, List<List<String>>.class);
        Boolean firstQueryTryTemp = false;
        Set<String> selectedCountries = new Set<String>();
        List<CWP_AC_MARGINDISCOUNTS__c> lstMarginDiscount = new List<CWP_AC_MARGINDISCOUNTS__c>();

        if(!listStrTableChanges.isEmpty()){
            for(List<String> item : listStrTableChanges){
                if(firstQueryTryTemp == true){
                    queryFilter += ' AND ';
                }
                //queryFilter += 'CWP_AC_Country__c = \'' + item[1] + '\' AND CWP_AC_City__c = \'' + item[2] + '\' AND CWP_AC_Contract_Term__c =' + item[3];
                queryFilter += 'CWP_AC_Country__c = \'' + item[1] + '\' AND CWP_AC_City__c = \'' + item[2] + '\'';
                firstQueryTryTemp = true;  

                //Saving all list from countries in grid to retrieve later margin and discount
                selectedCountries.add(item[1]);                    
            }
        }

        System.debug('queryFilter -> ' + queryFilter);
        serviceCosts = Database.query(queryFilter);
        System.debug('ServiceCosts -> ' + serviceCosts);
        String mountedKey = '';
        String contractTermKey = '';
        List<CWP_AC_ServicesCost__c> mountedSc = new List<CWP_AC_ServicesCost__c>();
        Set<Decimal> setContractTerm = new Set<Decimal>();

        for(CWP_AC_ServicesCost__c sc : serviceCosts){
            mountedKey = sc.CWP_AC_Country__c + '/' + sc.CWP_AC_City__c + '/' + sc.CWP_AC_Contract_Term__c;
            contractTermKey= sc.CWP_AC_Country__c + '/' + sc.CWP_AC_City__c;

            if(mapCT.containsKey(contractTermKey)){
                setContractTerm=mapCT.get(contractTermKey);
                setContractTerm.add(sc.CWP_AC_Contract_Term__c);
                mapCT.put(contractTermKey,setContractTerm);
            }else{
                setContractTerm = new Set<Decimal>();
                setContractTerm.add(sc.CWP_AC_Contract_Term__c);
                mapCT.put(contractTermKey,setContractTerm);
            }
            //mountedKey = sc.CWP_AC_Country__c + '/' + sc.CWP_AC_City__c;
            if(mapSc.containsKey(mountedKey)){
                mountedSc = mapSc.get(mountedKey);
                mountedSc.add(sc);
                mapSc.put(mountedKey,mountedSc);
            } else {
                mountedSc = new List<CWP_AC_ServicesCost__c>();
                mountedSc.add(sc);
                mapSc.put(mountedKey, mountedSc);
            }
        }
        System.debug('RNAdebug - mapSc');
        System.debug(mapSc);
        jsonSC = JSON.serialize(mapSc);
        System.debug(jsonSC);
        jsonCT= JSON.serialize(mapCT);

        //RNA 14/03/2018 - Retrieving AccountName from strACcountId to get Margin and Discounts
        String accQuery = 'SELECT Id, Name FROM Account WHERE Id = ' + '\'' + strAccountId + '\'' + ' LIMIT 1';
        Account accClientRetrieve = Database.query(accQuery);

        accClient = accClientRetrieve.Name;
        System.debug('RNAdebug -> accData: ' + accClient);
        System.debug('RNAdebug -> selectedCountries: ' + selectedCountries);

        String queryMarginDiscount = 'SELECT CWP_AC_Margin__c, CWP_AC_Discount__c, CWP_AC_ProductName__c, CWP_AC_ClientName__c, CWP_AC_Country__c FROM CWP_AC_MARGINDISCOUNTS__c WHERE CWP_AC_ClientName__c = ';
        queryMarginDiscount += '\'' + accClient + '\'';
        for(String country : selectedCountries){
            System.debug(country);
            queryMarginDiscount += ' AND CWP_AC_Country__c = \'' + country + '\'';
        }
        lstMarginDiscount = Database.query(queryMarginDiscount);
        System.debug('RNAdebug - lstMarginDiscount');
        System.debug(lstMarginDiscount);
        jsonMD = JSON.serialize(lstMarginDiscount);
        System.debug(jsonMD);
    }


    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Method to create tree view for a complex product
    Test Class:    
    
    History
    <Date>      <Author>     <Description>
    13-12-2017  Manuel Ochoa    
    ------------------------------------------------------------*/
    public void getProductTree(){
        String stringCatalogName = 'Wholesale Catalog';
        String strItemHeader='PROJECT'; // hardcoded for development
        Set<Id> setIdProduct= new Set<Id>();

        String userId = UserInfo.getUserId();
        User userData = [SELECT Id,Name,AccountId FROM User WHERE Id=:userId LIMIT 1];
        strAccountId = userData.AccountId;

        strRecordType = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Quote');
        
        list<NE__Catalog_Item__c> listCatalogItem= new list<NE__Catalog_Item__c>([SELECT 
                                                        id,name,BI_Nombre_del_Producto__c,NE__Active__c,NE__Catalog_Category_Name__c,
                                                        NE__Catalog_Category_Name__r.name,NE__Catalog_Id__c,NE__Catalog_Id__r.NE__Catalog_Header__C,
                                                        NE__Min_Qty__c, NE__Max_Qty__c,NE__Default_Qty__c,
                                                        NE__ProductId__c,NE__ProductId__r.name,NE__Type__c,NE__Parent_Catalog_Item__c,NE__Root_Catalog_Item__c,
                                                        NE__CheckRoot__c,NE__Complex_Item__c,NE__Item_Header__c,NE__Item_Header__r.name
                                                        FROM NE__Catalog_Item__c 
                                                        WHERE NE__Item_Header__r.Name=:strItemHeader AND NE__Catalog_Id__r.Name=:stringCatalogName
                                                        AND NE__Active__c=true AND NE__Type__c!='Category']);

        for(NE__Catalog_Item__c item : listCatalogItem){
            setIdProduct.add(item.NE__ProductId__c);
        }

        Map<Id,List<ProductAttributeWrapper>> mapIdProductListAttrWrapper = generateAttributesList(setIdProduct);       

        ProductDataWrapper rootProductItem = new ProductDataWrapper();

        if(!listCatalogItem.IsEmpty()){
            //First take root catalog item ID - type=="Product"
            Id idProductTypeId;
            id idRootTypeId;
            List<ProductDataWrapper> listProductItem = new List<ProductDataWrapper>();
            Map<String,ProductDataWrapper> mapIdProductItem = new Map<String,ProductDataWrapper>();
            

            Map<String,ProductDataWrapper> productsFirstLevelMap = new Map<String,ProductDataWrapper>();
            Map<String,ProductDataWrapper> productsSecondLevelMap = new Map<String,ProductDataWrapper>();
            Map<String,ProductDataWrapper> productsThirdLevelMap = new Map<String,ProductDataWrapper>();
            Map<String,ProductDataWrapper> productsFourthLevelMap = new Map<String,ProductDataWrapper>();
            
            for(NE__Catalog_Item__c catalogItem: listCatalogItem){
                if(catalogItem.NE__Type__c=='Product'){
                    idProductTypeId=catalogItem.id;
                    // 05-02-2018 - Manuel Ochoa - added variables for cart methods
                    catalogID=catalogItem.NE__Catalog_Id__c;
                    categoryId=catalogItem.NE__Catalog_Category_Name__c;

                }
                if(catalogItem.NE__Type__c=='Root'){
                    idRootTypeId=catalogItem.id;
                }
            }

            for(NE__Catalog_Item__c catalogItem: listCatalogItem){
                // 'Root' type CatalogItem must be ommited and 2nd level OI's must be linked with 'product' type CatalogItem
                if(catalogItem.NE__Type__c=='Root'){
                    continue;
                }

                // creation of order item data for a product item
                ProductDataWrapper newProductItem = new ProductDataWrapper();
                newProductItem.idCatalog = catalogItem.NE__Catalog_Id__c; // NE__Catalog__c
                newProductItem.catalogItemId = catalogItem.id;// NE__CatalogItem__c
                newProductItem.productId = catalogItem.NE__ProductId__c; // NE__ProdId__c
                newProductItem.productName = catalogItem.NE__ProductId__r.name;             
                // mark 'product' type as root
                newProductItem.decMinQty=catalogItem.NE__Min_Qty__c;
                newProductItem.decMaxQty=catalogItem.NE__Max_Qty__c;
                newProductItem.decDefQty=catalogItem.NE__Default_Qty__c;
                newProductItem.xType='Product';
                
                if(catalogItem.NE__Min_Qty__c>0){
                    newProductItem.bRequired = true;
                }else{
                    newProductItem.bRequired = false;
                }
                if(catalogItem.id == idProductTypeId){
                    newProductItem.bRoot = true;
                }else{
                    newProductItem.bRoot = false;
                }
          
                // link 2nd level oi's with 'product' type catalogItem
                if(catalogItem.NE__Root_Catalog_Item__c == idRootTypeId){
                    newProductItem.strRootId = idProductTypeId;
                }else{
                    newProductItem.strRootId = catalogItem.NE__Root_Catalog_Item__c;
                }               
                
                newProductItem.strParentId = catalogItem.NE__Parent_Catalog_Item__c;
                newProductItem.listAttributes = mapIdProductListAttrWrapper.get(catalogItem.NE__ProductId__c);
                newProductItem.listChildProducts = new List<ProductDataWrapper>();
                newProductItem.mapAvailableProducts = new Map<String,String>();
                
                listProductItem.add(newProductItem);
                mapIdProductItem.put(newProductItem.catalogItemId,newProductItem);

                if(newProductItem.bRoot){
                    rootProductItem=newProductItem;
                }
            }

            // first pass -- root order items
            for(ProductDataWrapper productItem : listProductItem){              
                if(productItem.strRootId == null){
                    productItem.itemLevel = 0;
                    productsFirstLevelMap.put(productItem.catalogItemId,productItem);                   
                }
            }

            // second pass
            for(ProductDataWrapper productItem : listProductItem){              
                if(productItem.strRootId != null && productsFirstLevelMap.containsKey(productItem.strRootId)){
                    productItem.itemLevel=1;
                    productsSecondLevelMap.put(productItem.catalogItemId,productItem);              
                }
            }

            // third pass
            /*for(ProductDataWrapper productItem : listProductItem){              
                if(productItem.strRootId!=null && productsSecondLevelMap.containsKey(productItem.strRootId)){
                    productItem.itemLevel=2;
                    productsThirdLevelMap.put(productItem.catalogItemId,productItem);               
                }
            }

            // fourth pass
            for(ProductDataWrapper productItem : listProductItem){              
                if(productItem.strRootId!=null && productsThirdLevelMap.containsKey(productItem.strRootId)){
                    productItem.itemLevel=3;
                    productsFourthLevelMap.put(productItem.catalogItemId,productItem);              
                }
            }*/

            // fill OiWrapper Maps
            for(string strKeyPrdWrapper :mapIdProductItem.keyset()){
                ProductDataWrapper prdWrapper=mapIdProductItem.get(strKeyPrdWrapper);
                
                if(mapIdProductItem.containsKey(prdWrapper.strRootId)){
                    mapIdProductItem.get(prdWrapper.strRootId).listChildProducts.add(prdWrapper);
                    mapIdProductItem.get(prdWrapper.strRootId).mapAvailableProducts.put(prdWrapper.catalogItemId,prdWrapper.productName);
                }
            }
            
            strPrdItemTreeJSON = JSON.serialize(rootProductItem);           
            strPrdItemJSON = JSON.serialize(mapIdProductItem);          
        }
        //return rootProductItem;
    }

    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Generating list of attributes
    Test Class:    
    
    History
    <Date>      <Author>     <Description>
    13-12-2017  Manuel Ochoa    
    ------------------------------------------------------------*/    
    public Map<Id,List<ProductAttributeWrapper>> generateAttributesList(Set<Id> setIdProduct){

        List<Id> setIdFamilies= new List<Id>();
        Map<Id,NE__ProductFamily__c> mapAuxProdFam=new Map<Id,NE__ProductFamily__c>();
        List<String> listIdEnumProps= new List<String>();
        Map<Id,List<ProductAttributeWrapper>> mapIdProductListAttrWrapper = new Map<Id,List<ProductAttributeWrapper>>();
        Map<Id,List<NE__ProductFamilyProperty__c>> mapListPropertyByFamily = new Map<Id,List<NE__ProductFamilyProperty__c>>();

        Map<Id,ProductAttributeWrapper> mapProductAttr = new Map<Id,ProductAttributeWrapper>();

        Map<Id,NE__ProductFamily__c> mapProductFamilies = new Map<Id,NE__ProductFamily__c>([SELECT NE__FamilyId__c,Name,NE__ProdId__c,NE__ProdId__r.Name,
                                                                                            NE__FamilyId__r.NE__Number_of_attributes__c,NE__Sequence__c
                                                                                            FROM NE__ProductFamily__c
                                                                                            WHERE NE__ProdId__c IN: setIdProduct
                                                                                            ORDER BY NE__Sequence__c]);

        for(NE__ProductFamily__c item : mapProductFamilies.values()){
            mapAuxProdFam.put(item.NE__FamilyId__c,item);
            setIdFamilies.add(item.NE__FamilyId__c);
        }

        //With the ID of Families we get the attributes
        List<NE__ProductFamilyProperty__c> listProductFamilyProperty = new List<NE__ProductFamilyProperty__c>([select Id,Name,NE__PropId__c,NE__PropId__r.Name,NE__PropId__r.NE__Type__c, 
                                                                                                                NE__Required__c,NE__FamilyId__c,NE__FamilyId__r.Name,
                                                                                                                NE__Dynamic_Lookup__c,NE__Sequence__c
                                                                                                                from NE__ProductFamilyProperty__c 
                                                                                                                where NE__FamilyId__c IN :setIdFamilies
                                                                                                                ORDER BY NE__Sequence__c]);

        for(NE__ProductFamilyProperty__c item: listProductFamilyProperty){
            if(item.NE__PropId__r.NE__Type__c == 'Enumerated'){
                listIdEnumProps.add(item.NE__PropId__c);

            }
        }
                
        Map<Id,NE__PropertyDomain__c> mapEnumProps = new Map<Id,NE__PropertyDomain__c>([select Id,Name,NE__PropId__c,NE__PropId__r.Name  
                                                                                        from NE__PropertyDomain__c
                                                                                        where NE__PropId__c IN :listIdEnumProps]);      
        
        for(NE__ProductFamilyProperty__c property : listProductFamilyProperty){
            ProductAttributeWrapper attrWrapper = new ProductAttributeWrapper();
            
            if(property.NE__Required__c == 'Yes'){
                attrWrapper.required = true;
                attrWrapper.selected = true;
            }else{
                attrWrapper.required = false;
                attrWrapper.selected = false;
            }
            attrWrapper.attribute = property.NE__PropId__r.Name;
            attrWrapper.xType = property.NE__PropId__r.NE__Type__c;
            if(attrWrapper.xType=='Dynamic Lookup'){
                attrWrapper.strIdDynLookUp = property.Id;
                attrWrapper.strNameDynLookUp = property.NE__Dynamic_Lookup__c;
            }else{
                attrWrapper.strIdDynLookUp = 'false';
                attrWrapper.strNameDynLookUp = 'false';
            }
            attrWrapper.idFamily = property.NE__FamilyId__c;
            attrWrapper.family = property.NE__FamilyId__r.Name;

            if(mapAuxProdFam.get(property.NE__FamilyId__c).NE__Sequence__c==null){
                attrWrapper.strSequence=property.NE__FamilyId__r.Name + '_' + property.NE__Sequence__c;
            }else{
                    attrWrapper.strSequence=mapAuxProdFam.get(property.NE__FamilyId__c).NE__Sequence__c + '_' + property.NE__Sequence__c;
            }            
            if(attrWrapper.xType == 'Enumerated'){
                List<String> auxValues = new List<String>();
                for(NE__PropertyDomain__c enumProp : mapEnumProps.values()){
                    if(enumProp.NE__PropId__c == property.NE__PropId__c){
                        auxValues.add(enumProp.Name);
                    }
                }
                attrWrapper.enumeratedValues = auxValues;
            }           
            mapProductAttr.put(property.id,attrWrapper);
        }

        for(Id idProduct :setIdProduct){
            List<ProductAttributeWrapper> listAttr = new List<ProductAttributeWrapper>();
            for( NE__ProductFamily__c productFamily :mapProductFamilies.values()){
                if(productFamily.NE__ProdId__c==idProduct){
                    for(ProductAttributeWrapper attrWrapper :mapProductAttr.values()){
                        if(attrWrapper.idFamily==productFamily.NE__FamilyId__c){
                            listAttr.add(attrWrapper);
                        }
                    }
                }
            }
            mapIdProductListAttrWrapper.put(idProduct,listAttr);
        }
        return  mapIdProductListAttrWrapper;
    }


    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Relationships in the grid to get City-Country states from ServicesCost
    Test Class:    
    
    History
    <Date>      <Author>     <Description>
    13-12-2017  Manuel Ochoa    
    ------------------------------------------------------------*/
    public Component.Apex.PageBlockTable getDynamicLookUpTable(){    

        try{

            //Fix 08/02/2018 - Changing query to retrieve only City/Country from Location Wholesale
            //string queryString='SELECT CWP_AC_Country__c, CWP_AC_City__c FROM CWP_AC_ServicesCost__c GROUP BY CWP_AC_Country__c,CWP_AC_City__c ORDER BY CWP_AC_Country__c,CWP_AC_City__c';
            string queryString = 'SELECT CWP_AC_Country__c, CWP_AC_City__c FROM CWP_AC_Location_Wholesale__c ORDER BY CWP_AC_Country__c,CWP_AC_City__c';            

            dynLookUpData = new List<sObject>();
            dynLookUpData=Database.query(queryString);




            Component.Apex.PageblockTable table = new Component.Apex.PageblockTable(var='result');
            table.styleClass='slds-table slds-table--bordered dynLookUp';
            table.headerClass='slds-text-heading--label';
            table.onRowDblClick='applyDynLookUp()';
            table.onRowClick='selectRowDynLookUp(this)';
            table.id='dynLookUpTable';

            table.expressions.value='{!dynLookUpData}';

            if(!dynLookUpData.isEmpty()){               
                Component.Apex.Column column = new Component.Apex.Column(headerValue='Country');
                column.expressions.value = '{!result.CWP_AC_Country__c}';
                table.childComponents.add(column);

                column = new Component.Apex.Column(headerValue='City');
                column.expressions.value = '{!result.CWP_AC_City__c}';
                table.childComponents.add(column);
            }else{
                Component.Apex.Column column = new Component.Apex.Column(headerValue='NO DATA FOUND');
                //column.expressions.value = '{!result.'+ mapValue.NE__Value__c +'}';
                table.childComponents.add(column);
            }
            return table;
            
            //return null;      
        }
        catch(Exception e){
            system.debug('Exception found: ' + e + ' at line:' + e.getLineNumber() ); 
            return null;
        }
    }       

    

    public class ProductDataWrapper{
        public Id idCatalog; // NE__Catalog__c
        public Id catalogItemId; // NE__CatalogItem__c
        public Id productId; // NE__ProdId__c
        public String productName; // product name
        public decimal decMinQty;
        public decimal decMaxQty;
        public decimal decDefQty;
        public Boolean bRequired;
        public String xType ;
        public Boolean bRoot;
        public integer itemLevel;
        public String strRootId;
        public String strParentId;
        public List<ProductAttributeWrapper> listAttributes;
        public List<productDataWrapper> listChildProducts;
        public Map<String,String> mapAvailableProducts;      
    }

    
    public class ProductAttributeWrapper {
        public Boolean selected;
        public String attribute;
        public String idFamily ;
        public String family ;      
        public String strSequence;
        public Boolean required ;
        public String xType ;
        public String strIdDynLookUp ;
        public String strNameDynLookUp {get;set;}
        public Map<String,String> mapDynLookUpValues ;
        public List<String> enumeratedValues ;  
    }


    /**** IMPORT DATA ZONE ****/

    public void echoVal(){
        if(!Test.isRunningTest()){
            String param = strCsvData;            
            birId=importFile(param);
        }
    }


    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Importing file and pushing into cart
    Test Class:    
    
    History
    <Date>      <Author>     <Description>
    13-12-2017  Manuel Ochoa    
    ------------------------------------------------------------*/
    public String importFile(String csvFile){

        Integer linesNumber=0;
        String mail;
    
        Savepoint savePointCheckout = Database.setSavepoint();
      
        try{
            if(csvFile != null){
                List<List<String>> totalBlocks = safeSplit(csvFile, '\n', BLOCKSIZE); 

                //Calculate number of csv lines
                for (List<String> tb: totalBlocks){
                    linesNumber = linesNumber + tb.size();
                }
                linesNumber=linesNumber-totalBlocks.size();

                //Generate requestId 
                String requestId = generateIdentifier('req');

                //Creating BIR
                Bit2WinHUB__Bulk_Import_Request__c BIR = new Bit2WinHUB__Bulk_Import_Request__c();
                BIR.Bit2WinHUB__Request_Id__c = requestId;
                BIR.Bit2WinHUB__Total_Blocks__c= linesNumber;
                BIR.Bit2WinHUB__Processed_Blocks__c=0;

                insert BIR;

                // Creating the Attachment with the CSV to import
                    Attachment taskAttach       =   new Attachment();
                    taskAttach.Body             =   blob.valueOf(csvFile);  
                    taskAttach.ParentId         =   BIR.Id;
                    taskAttach.Name             =   'CSV Load Request: '+requestId+'.csv';                                  
                    insert taskAttach;

                BIR.Bit2WinHUB__Step__c = 'CreateNTXML';
                update BIR;

                if(!Test.isRunningTest()){
                    //call CreateNonTechXMLBatch to start batch process
                    Id batchInstanceId = Database.executeBatch(new Bit2WinHUB.CreateNonTechXMLBatch(BIR.Id,mail), 1);
                }

                return BIR.Id;

            }else{
                return null;
            }

        }catch(Exception e){
            system.debug('Exception found: ' + e + ' at line:' + e.getLineNumber() ); 
            DataBase.rollback(savePointCheckout);
            return null;    
        }
        return null;    
    }

    public static String generateRandomNumber(){
        String randomNumber = generate();

        if (randomNumber.length() < 10) {
            String randomNumber2 = generate();
            randomNumber = randomNumber + randomNumber2.substring(0, 10 - randomNumber.length());
        }
        return randomNumber;
    }

    
    public static String generateIdentifier(String prefix) {
        return prefix + '-' + generateRandomNumber();
    }
    
    public static String generate() {
        return String.valueOf(Math.abs(Crypto.getRandomInteger()));
    }


    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Safe-splitting the CSV file
    
    History
    <Date>      <Author>     <Description>
    13-12-2017  Manuel Ochoa    
    ------------------------------------------------------------*/
    public static List<List<String>> safeSplit(String inStr, String delim, Integer bSize)
    {
        Boolean moreThanOneBlock = false;        
        Integer regexFindLimit = 100;
        Integer regexFindCount = 0;
        List<List<String>> blockOutput = new List<List<String>>();
        List<String> output = new List<String>();
        Matcher m = Pattern.compile(delim).matcher(inStr);    
        Integer lastEnd = 0;
        List<String> header = new List<String>();

        while(!m.hitEnd()){
            while(regexFindCount < regexFindLimit && !m.hitEnd()){  
                if(m.find()){
                    if(inStr.substring(lastEnd, m.start()) != null && inStr.substring(lastEnd, m.start()) !=''){
                        output.add(inStr.substring(lastEnd, m.start())); 
                    } 

                    lastEnd = m.end();
                
                    if (header.isEmpty()){
                        header.add(output[0]);
                    }
                }else{
                    if(inStr.substring(lastEnd) != null && inStr.substring(lastEnd) !=''){
                        output.add(inStr.substring(lastEnd));
                    }
                    lastEnd = inStr.length();
                }
                regexFindCount++;
            }

            m.reset(inStr);        
            m.region(lastEnd, m.regionEnd());        
            regexFindCount = 0;

            if (output.size() == bSize){
                moreThanOneBlock = true;
                if (blockOutput.size()>0){
                    List<String> outputAux = new List<String>();
                    outputAux.addAll(header);
                    outputAux.addAll(output);
                    blockOutput.add(outputAux);     
                    output = new List<String>();
                } else {
                    blockOutput.add(output); 
                    output = new List<String>();
                }
            }
        }   

        if (output.size() > 0) {
            if(moreThanOneBlock){
                List<String> outputAux = new List<String>();
                outputAux.addAll(header);
                outputAux.addAll(output);
                blockOutput.add(outputAux);
            }else{
                blockOutput.add(output);
            }
        }
        return blockOutput; 
    }

    public static String getAccClient(String firstname) {
        System.debug('Ha llegado!!');
        return 'Hello: ' + firstname;

    }
}