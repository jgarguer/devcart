public with sharing class VE_venta_express_ctrl {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Suarez
    Company:       Salesforce.com
    Description:   Lightning Controller for the Venta Express.
    
    History:
    
    <Date>            <Author>          <Description>
    02/02/2016        Javier Suarez      Initial version
    03/03/2017        Alberto Fernández  Duración del contrato por defecto modificada para Opps de Chile (D-450)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @AuraEnabled
    public static Account getAccount(String acctId){
        return [select id, name, bi_country__c,currencyisocode from account where id=:acctId and Owner.isActive = True limit 1];
    }
    
    @AuraEnabled
    public static Opportunity getOpportunity(String optyId){
        return [select id, name, stagename,BI_Fecha_de_vigencia__c,account.Name,VE_Express__c from Opportunity where id=:optyId];
    }
    
    @AuraEnabled
    public static String forwardOpportunity(Opportunity o, String tipoCierre, String fechaRealCierre){
                
        VE_ForwardOpptyResponse fo = new VE_ForwardOpptyResponse();
        
        List<Opportunity> optys = new List<Opportunity>();
        
        system.debug('tipoCierre: '+tipoCierre+' StageName: '+o.StageName);
                
        if (tipoCierre=='F1 - Perdida'){
            o.Probability=0;
            o.StageName='F1 - Closed Lost';
        }
        else{
            if ( tipoCierre == 'F1 - Cancelada | Suspendida'){
                o.Probability=0;
                o.StageName='F1 - Cancelled | Suspended';
            }
            else{
                if ( o.StageName == 'F4 - Desarrollo de la oferta' || o.StageName == 'F4 - Offer Development'){
//                    system.debug('Entramos F3');
                    o.StageName = 'F3 - Offer Presented';
                    String[] dts = fechaRealCierre.split('-');
                    Date d = Date.newInstance(Integer.valueOf(dts[0]), Integer.valueOf(dts[1]), Integer.valueOf(dts[2]));
                    o.BI_Fecha_de_vigencia__c=d;
                }
                else{
                    if ( o.StageName == 'F3 - Presentación de oferta' || o.StageName == 'F3 - Offer Presented'){
//                        system.debug('Entramos F2');
                        o.StageName = 'F2 - Negotiation';
                    }
                    else{
                        if ( o.StageName == 'F2 - Negociación' || o.StageName == 'F2 - Negotiation'){
                            o.StageName='F1 - Closed Won';

                            if ( tipoCierre == 'F1 - Ganada'){
                                system.debug('Entramos F1 Prima');
                                String[] dts = fechaRealCierre.split('-');
                                Date d = Date.newInstance(Integer.valueOf(dts[0]), Integer.valueOf(dts[1]), Integer.valueOf(dts[2]));
                                o.BI_Fecha_de_cierre_real__c  = d;
                                o.BI_Motivo_de_perdida__c='';
                            }
                            o.Probability=100;
                        }                
                    }
                }                                   
            }
        }
            
        optys.add(o);
            
        Database.SaveResult[] srList = new Database.SaveResult[]{};   
                
        try{
//          update o;
                system.debug('La etapa bendita: '+o.StageName);
                system.debug('La fecha de cierre real: '+o.BI_Fecha_de_cierre_real__c);
            
            srList = Database.update(optys, false);

        }

        catch(Exception ex){
            system.debug('Exception Catched');
            fo.o = null;
            fo.m = ex.getMessage();
        }   

            for (Database.SaveResult sr : srList) {

                if (sr.isSuccess()) {
                    System.debug('Successfully updated Opty. Opty ID: ' + sr.getId());
                    fo.o = o;
                    fo.m = 'OK';
                }
                else {             
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity fields that affected this error: ' + err.getFields());

                        if (String.isNotBlank(fo.m)){
                            fo.m = fo.m + '<@>' + err.getMessage();
                        }Else{
                            fo.m = err.getMessage();
                        }
                }
            }
    }
        String jsonResponse = JSON.serialize(fo);
        
        return jsonResponse;
        
    }

    @AuraEnabled
    public static Opportunity newOppty(){
        return new Opportunity();
    }
    
    @AuraEnabled
    public static Opportunity insertOppty(Opportunity o, Account a){
        system.debug('init insertOppty '+o.stagename);
        try{
            o.recordtypeid='012w0000000hzMW';
            o.accountid=a.id;
            o.StageName='F5 - Solution Definition';
            o.ve_Express__c=true;
            o.BI_Probabilidad_de_exito__c = '75';
            o.BI_Plazo_estimado_de_provision_dias__c = 30;
            if(o.BI_Country__c == 'Chile' && o.BI_Opportunity_Type__c == 'Móvil') {
                o.BI_Duracion_del_contrato_Meses__c = 18; /*D-450*/
            }
            else {
                o.BI_Duracion_del_contrato_Meses__c = 12;
            }
            
            insert o;            
        }
  
        catch(Exception e){
            system.debug(e.getMessage());
            
            o.BI_Probabilidad_de_exito__c = e.getMessage();
        }
       
        return o;
    }
    
    @AuraEnabled
    public static String takeOpportunity(Opportunity o, String tipoCierre){

        VE_ForwardOpptyResponse fo = new VE_ForwardOpptyResponse(); 
        
        List<Opportunity> optys = new List<Opportunity>();
        
//        system.debug('Entramos Señalizador');
//        system.debug('o.StageName'+o.StageName);
        
            List<id> ids = new List<id>();

            Map<id,id> mapAcctOwner = new Map<id,id>();

        
        if (tipoCierre=='Aceptar'){
            
            o.StageName = 'F5 - Solution Definition';
//            system.debug('o.AccountId'+ o.AccountId);
        }
        else{
            o.StageName = 'F1 - Cancelled | Suspended';
            o.Probability=0;
        }
        
        optys.add(o);
            
        Database.SaveResult[] srList = new Database.SaveResult[]{};   

        try{
            //update o;
             srList = Database.update(optys, false);
            
        }
        catch(exception ex){
            system.debug('Exception Catched');
            fo.o = null;
            fo.m = ex.getMessage();

            String jsonResponse = JSON.serialize(fo);
        
            return jsonResponse;
        }   

            for (Database.SaveResult sr : srList) {

                if (sr.isSuccess()) {
                    System.debug('Successfully updated Opty. Opty ID: ' + sr.getId());
                    fo.o = o;
                    fo.m = 'OK';
                    
/*
 *             ids.add(o.AccountId);
            
            for (Account a: [select id,OwnerId from account where id in :ids]){
                mapAcctOwner.put(a.Id, a.OwnerId);
            }
 
            for (Opportunity opp: [select id, AccountId, CreatedById, OwnerId from Opportunity where id = :o.Id]){

//                system.debug('Estamos para ver lo del Señalizador');
            
//                system.debug('Parametros: '+ mapAcctOwner.get(o.accountid)+' '+opp.CreatedById+' '+opp.OwnerId);
            
            if((mapAcctOwner.get(o.accountid)==opp.OwnerId) && (opp.OwnerId != opp.CreatedById)){

                boolean hasOtm = False;
            
//              System.debug('******### hasOtm: '+hasOtm);
                List<OpportunityTeamMember> removeSen = new List<OpportunityTeamMember>();
//              System.debug('******### otm.Id: '+otm.Id);
                for(OpportunityTeamMember otm : [Select id, UserId, TeamMemberRole From OpportunityTeamMember where OpportunityID = :o.Id and TeamMemberRole = 'Sales Engineer' and UserId = :opp.CreatedById])
                {
                    if(otm.Id != null) removeSen.add(otm);
                }

//                system.debug('Remove Señalizador '+ removeSen);

                delete removeSen;        
            }                       
                }
*/

                }
                else {             
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity fields that affected this error: ' + err.getFields());

                        if (String.isNotBlank(fo.m)){
                            fo.m = fo.m + '<@>' + err.getMessage();
                        }Else{
                            fo.m = err.getMessage();
                        }
                }
                    fo.o=null;
            }
            }

        String jsonResponse = JSON.serialize(fo);
        
        return jsonResponse;

    }
    
    @AuraEnabled
    public static List <PickListValue> stageValues(){
        Schema.DescribeFieldResult stageNameSchema = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> stageNameSchemaValues = stageNameSchema.getPicklistValues();

        List <PickListValue> stageNameValues = new List <PickListValue>();
        for(Schema.PicklistEntry currentValue :stageNameSchemaValues){
            
            stageNameValues.add(new PickListValue(currentValue.getLabel(), currentValue.getValue()));
        }
        return stageNameValues;
    }
    
    public class PickListValue{
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        
        public PickListValue (String label, String value){
            this.label = label;
            this.value = value;
        }
    }

    @AuraEnabled
    public static List<String> getF1Stages() {
        List<String> options = new List<String>();

        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            if(f.getLabel().startsWith('F1') && f.getLabel() != 'F1 - Partially Won' && f.getLabel() != 'F1 - Ganada Parcial') {
                options.add(f.getLabel());
            }
            
        }
        return options;
    }

    @AuraEnabled
    public static List<String> getSIMPOppType(){
        List<String> options = new List<String>();

        Schema.DescribeFieldResult fieldResult = Opportunity.BI_SIMP_Opportunity_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {      
            options.add(f.getLabel());
        }

        options.add(0, Label.VE_Vacio);

        return options;
    }

    @AuraEnabled
    public static List<String> getOppType(String accId){
        Map<String,List<String>> allOptions = new Map<String,List<String>>();
        List<String> options = new List<String>();

        String accCountry = [SELECT Id, BI_Country__c FROM Account WHERE Id =:accId].get(0).BI_Country__c;

        if(UserInfo.getLanguage() == 'es' && accCountry == 'Mexico') {
            accCountry = 'México';
        }
        else if(UserInfo.getLanguage() == 'es' && accCountry == 'Peru') {
            accCountry = 'Perú';
        }
        else if(UserInfo.getLanguage() == 'es' && accCountry == 'Panama') {
            accCountry = 'Panamá';
        }

        /*Añadida condición en la que, para Venta Exprés de Chile, solamente deben mostrarse 'Móvil', 'CSB' y 'Fijo' como valores disponible en el campo Línea de Negocio*/
        if(accCountry != 'Chile') {
            allOptions = VE_Venta_Express_Helper.getDependentOptionsImpl('Opportunity', 'BI_Country__c', 'BI_Opportunity_Type__c');

            if(allOptions.get(accCountry) != null) {
                options = allOptions.get(accCountry);
            }
        }
        else if(accCountry == 'Chile'){
            options.add('Móvil');
            options.add('CSB');
            options.add('Fijo');
        }        

        if(options.isEmpty()) {
            options.add(Label.VE_Vacio);
        }
        else {
            options.add(0, Label.VE_Vacio);
        }
        return options;
    }

    @AuraEnabled
    public static List<String> getOppLostReason(String stage){
        Map<String,List<String>> allOptions = new Map<String,List<String>>();
        List<String> options = new List<String>();

        allOptions = VE_Venta_Express_Helper.getDependentOptionsImpl('Opportunity', 'StageName', 'BI_Motivo_de_perdida__c');

        if(allOptions.get(stage) != null) {
            options = allOptions.get(stage);
        }

        if(options.isEmpty()) {
            options.add(Label.VE_Vacio);
        }
        else {
            options.add(0, Label.VE_Vacio);
        }
        return options;
    }

}