public class PCA_Order_Detail_Controller{
    // Status field values
    public String CASE_STATUS_RESOLVED {
        get{return Constants.CASE_STATUS_RESOLVED;} 
        set;
    }
    public String CASE_STATUS_CLOSED {
        get{return Constants.CASE_STATUS_CLOSED;} 
        set;
    }
    public String CASE_STATUS_REJECTED {
        get{return Constants.CASE_STATUS_REJECTED;} 
        set;
    }
    public String CASE_STATUS_CANCELLED {
        get{return Constants.CASE_STATUS_CANCELLED;} 
        set;
    }
    public String CASE_STATUS_ASSIGNED {
        get{return Constants.CASE_STATUS_ASSIGNED;} 
        set;
    }
    public String CASE_STATUS_PENDING {
        get{return Constants.CASE_STATUS_PENDING;} 
        set;
    }
    public String CASE_STATUS_IN_PROGRESS {
        get{return Constants.CASE_STATUS_IN_PROGRESS;} 
        set;
    }
    // Status reason field values
    public String CASE_STATUS_REASON_IN_SD_VALIDATION {
        get{return Constants.CASE_STATUS_REASON_IN_SD_VALIDATION;} 
        set;
    }
    public String CASE_STATUS_REASON_IN_CANCELLATION_CLOSE {
        get{return Constants.CASE_STATUS_REASON_IN_CANCELLATION_CLOSE;} 
        set;
    }
    public String CASE_STATUS_REASON_IN_CANCELLATION_NOT_PAID {
        get{return Constants.CASE_STATUS_REASON_IN_CANCELLATION_NOT_PAID;} 
        set;
    }
    public String CASE_STATUS_REASON_IN_CANCELLATION_CUSTOMER_ACCEPTANCE {
        get{return Constants.CASE_STATUS_REASON_IN_CANCELLATION_CUSTOMER_ACCEPTANCE;} 
        set;
    }
    public String CASE_STATUS_REASON_IN_CANCELLATION_OFF_NET {
        get{return Constants.CASE_STATUS_REASON_IN_CANCELLATION_OFF_NET;} 
        set;
    }
    public String CASE_STATUS_REASON_IN_CANCELLATION {
        get{return Constants.CASE_STATUS_REASON_IN_CANCELLATION;} 
        set;
    }
    
    /***********************************************
     *    ATTRIBUTES
     ***********************************************/
     
    public boolean error {get; set;}
    
    public Case currentCase {get; set;}
    public String cRecordType {get; set;}
    public String serviceUnit {get; set;}
    public String companyInfo {get; set;}
    public String substatus {get; set;}
    public List<WorkInfo> workinfos {get; set;}
    public List<Attachment> attachments {get;set;}
    public String newComment {get; set;}
    public List<Attachment> listNewCommentAttachments {get;set;}
    public Attachment newAttachment {
    get {
      if (newAttachment == null)
        newAttachment = new Attachment();
      return newAttachment;
    }
      set;
    }
    
    public blob myAttachedBody {get; set;}
    public blob myAttachedBody2 {get; set;}
    public blob myAttachedBody3 {get; set;}
    
    public String attachmentName1 {get; set;}
    public String attachmentName2 {get; set;}
    public String attachmentName3 {get; set;}
    
    public String attachmentDesc1 {get; set;}
    public String attachmentDesc2 {get; set;}
    public String attachmentDesc3 {get; set;}
    public boolean isReassigned {get; set;}
    public String attributesHTML {get; set;} 
    public Service service {get; set;}
    public String mainCSUID {get; set;}
    public String suId{get; set;}
    public String productName{get;set;}

    public Boolean hasAttachment {get; set;}
    public List<Attachment> myAttachmentList {get; set;}
    
    public Boolean hasMainCSUID {get; set;}
    
    public String suAction {get; set;}
    
    public TGS_Status_machine__c discardCancellationCurrentStatus {get; set;}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Description:  Controller of the order detail view
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_Order_Detail_Controller() {
        if (Test.isRunningTest()){
            CASE_STATUS_RESOLVED='CASE_STATUS_RESOLVED'; 
            CASE_STATUS_CLOSED='CASE_STATUS_CLOSED';
            CASE_STATUS_REJECTED='CASE_STATUS_REJECTED';
            CASE_STATUS_CANCELLED='CASE_STATUS_CANCELLED'; 
            CASE_STATUS_ASSIGNED='CASE_STATUS_ASSIGNED';
            CASE_STATUS_PENDING='CASE_STATUS_PENDING'; 
            CASE_STATUS_IN_PROGRESS='CASE_STATUS_IN_PROGRESS';
            CASE_STATUS_REASON_IN_SD_VALIDATION='CASE_STATUS_REASON_IN_SD_VALIDATION';
            CASE_STATUS_REASON_IN_CANCELLATION_CLOSE='CASE_STATUS_REASON_IN_CANCELLATION_CLOSE';
            CASE_STATUS_REASON_IN_CANCELLATION_NOT_PAID='CASE_STATUS_REASON_IN_CANCELLATION_NOT_PAID';
            CASE_STATUS_REASON_IN_CANCELLATION_CUSTOMER_ACCEPTANCE='CASE_STATUS_REASON_IN_CANCELLATION_CUSTOMER_ACCEPTANCE';
            CASE_STATUS_REASON_IN_CANCELLATION_OFF_NET='CASE_STATUS_REASON_IN_CANCELLATION_OFF_NET';
            CASE_STATUS_REASON_IN_CANCELLATION = 'CASE_STATUS_REASON_IN_CANCELLATION';
        }
        
        //Obtener case
        String mId = ApexPages.currentPage().getParameters().get('caseId');
        currentCase = [SELECT subject, 
                            caseNumber, 
                            Asset__c, 
                            TGS_Status_Reason__c,
                            TGS_Invoice_Number__c,
                            TGS_RFS_Date__c, 
                            TGS_RFB_date__c, 
                            TGS_Impact__c, 
                            TGS_Urgency__c, 
                            priority, Type,
                            RecordType.Name,
                            TGS_Service__c,
                            TGS_Assignee__c,
                            Order__c,
                            Order__r.NE__Asset__c,
                            Order__r.NE__ServAccId__c, 
                            Order__r.NE__BillAccId__c,
                            description, accountId, account.Name,
                            CreatedDate, createdById, createdBy.Name, 
                            LastModifiedDate, status, RecordTypeId, TGS_CWP_Accept_Cancellation__c
                            
                             FROM Case WHERE Id = :mId LIMIT 1];
        
        System.debug('[PCA_Order_Detail_Controller Constructor] currentCase: ' + currentCase);
                           
        if (currentCase.status.equalsIgnoreCase(Constants.CASE_STATUS_ASSIGNED)) {
            CaseHistory[] ch = [SELECT CaseId FROM CaseHistory  WHERE Field = 'Status' AND CaseId =:currentCase.Id ];
            if (ch.size() > 0) {
                isReassigned = true;
            }else{
                isReassigned = false;
            }
        }
        System.debug('[PCA_Order_Detail_Controller Constructor] isReassigned: ' + isReassigned);
        
        if (currentCase.TGS_Status_Reason__c != null && 
            (currentCase.TGS_Status_Reason__c.equalsIgnoreCase(Constants.CASE_STATUS_REASON_WAITING_FOR_QUOTATION_ACCEPTANCE) ||
             currentCase.TGS_Status_Reason__c.equalsIgnoreCase(Constants.CASE_STATUS_REASON_IN_CANCELLATION) ||
             //currentCase.TGS_Status_Reason__c.equalsIgnoreCase(Constants.CASE_STATUS_REASON_IN_CANCELLATION_CUSTOMER_ACCEPTANCE) ||
             (currentCase.TGS_Status_Reason__c.equalsIgnoreCase(Constants.CASE_STATUS_REASON_IN_CANCELLATION_OFF_NET) && currentCase.TGS_CWP_Accept_Cancellation__c == false))) {
                 substatus = ' - ' +CASE_STATUS_REASON_IN_CANCELLATION;
        }
        if(currentCase.TGS_Status_Reason__c != null &&
            currentCase.TGS_Status_Reason__c.equalsIgnoreCase(Constants.CASE_STATUS_REASON_IN_CANCELLATION_CUSTOMER_ACCEPTANCE)) {
            substatus = ' - ' +CASE_STATUS_REASON_IN_CANCELLATION_CUSTOMER_ACCEPTANCE;
        }
        
        if(currentCase.TGS_Status_Reason__c != null && (currentCase.TGS_Status_Reason__c.equalsIgnoreCase(Constants.CASE_STATUS_REASON_IN_CANCELLATION_CLOSE) || 
           (currentCase.TGS_Status_Reason__c.equalsIgnoreCase(Constants.CASE_STATUS_REASON_IN_CANCELLATION_OFF_NET) && currentCase.TGS_CWP_Accept_Cancellation__c == true))){
               substatus = ' - Cancellation Accepted';
        }
        
        System.debug('[PCA_Order_Detail_Controller Constructor] substatus: ' + substatus);
               
        cRecordType = currentCase.RecordType.Name;
        
        System.debug('[PCA_Order_Detail_Controller Constructor] cRecordType: ' + cRecordType);
        
        List<NE__OrderItem__c> suidOrderItems = new List<NE__OrderItem__c>();
        List<NE__OrderItem__c> suidOrderItemsOrder = new List<NE__OrderItem__c>();
        serviceUnit='';
        // Se crea la jerarquía del servicio con sus subservicios y se crea el código HTML que se añadirá en la página.
        if(currentCase.Asset__c != null){ //El caso tiene Asset
            List<NE__OrderItem__c> confItems = [SELECT Name FROM NE__OrderItem__c WHERE NE__OrderId__c = :currentCase.Asset__c];
            System.debug('[PCA_Order_Detail_Controller Constructor] confItems: ' + confItems);
            for(NE__OrderItem__c orderItem : confItems){
                serviceUnit += orderItem.Name + ' - ';
            }
            serviceUnit = serviceUnit.substring(0,serviceUnit.length()-3);
        }
        
        System.debug('[PCA_Order_Detail_Controller Constructor] serviceUnit: ' + serviceUnit);
        
        // Se crea la jerarquía del servicio con sus subservicios y se crea el código HTML que se añadirá en la página.
        suidOrderItems=[SELECT Id, TGS_CSUID1__c, NE__ProdId__c, NE__Action__c FROM NE__OrderItem__c 
                WHERE NE__OrderId__c IN (SELECT ID FROM NE__Order__c WHERE Id =: currentCase.Order__c) AND (NE__Action__c <> 'Remove' OR NE__OrderItem__c.NE__OrderId__r.Case__r.Type =: 'Disconnect')];
        System.debug('[PCA_Order_Detail_Controller Constructor] suidOrderItems: ' + suidOrderItems);
        
        System.debug('[PCA_Order_Detail_Controller Constructor] serviceUnit: ' + serviceUnit);
        
        attributesHTML =  '';
        for(NE__OrderItem__c orderItem : suidOrderItems){
            suId = orderItem.Id;
            suAction= orderItem.NE__Action__c;            
            NE__Product__c myProduct = TGS_Portal_Utils.getProductName(orderItem.NE__ProdId__c);
            if(Test.isRunningTest()){
                productName='';
            }
            else{
                productName=myProduct.Name;   
            }
            String mainCSUID='';
            hasMainCSUID=False;
            if(orderItem.TGS_CSUID1__c!=null && orderItem.TGS_CSUID1__c!=''){
                hasMainCSUID=True;
                mainCSUID=orderItem.TGS_CSUID1__c;   
            }
            if (mainCSUID!=null && mainCSUID.length()>2){
                mainCSUID=mainCSUID.substring(0,mainCSUID.length()-2);
            }
            
            System.debug('[PCA_Order_Detail_Controller Constructor] suId: ' + suId);
            System.debug('[PCA_Order_Detail_Controller Constructor] suAction: ' + suAction);
            System.debug('[PCA_Order_Detail_Controller Constructor] productName: ' + productName);
            System.debug('[PCA_Order_Detail_Controller Constructor] hasMainCSUID: ' + hasMainCSUID);
            System.debug('[PCA_Order_Detail_Controller Constructor] mainCSUID: ' + mainCSUID);
            
            service = new Service(suId, productName, mainCSUID, suAction);
            
            System.debug('[PCA_Order_Detail_Controller Constructor] service: ' + service);
            
            getHTML(service);
            
            List<Attachment> listAttachment = [SELECT Id, Name  FROM Attachment WHERE parentId = :orderItem.Id];
            hasAttachment=false;
            if (listAttachment.size()>0){
                hasAttachment=true;
                myAttachmentList=listAttachment;
            }
            if(hasAttachment){
                attributesHTML += '<apex:outputText><div style="margin-bottom:15px; float:left; width:100%;">' +
                		'<div style="width: 19%; float: left;"><label style="margin-right:10px;">' + Label.TGS_CWP_ATT + ': </label></div>' +
                			'<div style="width: 81%;float: left;">';
                for(Attachment myAttachment : myAttachmentList){
                    String urlAction = '/empresasplatino/servlet/servlet.FileDownload?retURL=%2Fempresasplatino%2Fapex%2FPCA_Order_Detail%3FcaseId%3D' + mId + '&file=' + myAttachment.Id;
                    attributesHTML += '<a href="' + urlAction + '" style="color: #666;">' + myAttachment.Name + '</a>' +
                        '<span  style="padding-right:10px"></span>';
                }
                attributesHTML += '</div>' +
             			'</div>' +
                	'</div></apex:outputText>' + 
                    '<div class="clear"></div>';
        	}
        }
        
        // Obtenemos la jerarquía de la cuenta con más bajo nivel entre serviceAccount Y billingAccount de la Service Unit
        companyInfo = '';
        integer level = TGS_Portal_Utils.getAccountLevelForAccount(currentCase.accountId);
        if(level>=1){companyInfo += (TGS_Portal_Utils.getLevel1(currentCase.accountId,1)[0]).Name;}
        if(level>=2){companyInfo += '  >  ' + (TGS_Portal_Utils.getLevel2(currentCase.accountId,1)[0]).Name;}
        if(level>=3){companyInfo += '  >  ' + (TGS_Portal_Utils.getLevel3(currentCase.accountId,1)[0]).Name;}
        if(level>=4){companyInfo += '  >  ' + (TGS_Portal_Utils.getLevel4(currentCase.accountId,1)[0]).Name;}
        if(level>=5){companyInfo += '  >  ' + (TGS_Portal_Utils.getLevel5(currentCase.accountId,1)[0]).Name;}
        
        System.debug('[PCA_Order_Detail_Controller Constructor] companyInfo: ' + companyInfo);

        //Obtener workinfos
        List<TGS_Work_Info__c> TGS_workinfos = [SELECT Id,Name,TGS_Description__c,CreatedById,CreatedDate FROM TGS_Work_Info__c 
                                                WHERE TGS_Case__c = :mId AND TGS_Public__c = true ORDER BY CreatedDate];
        
        //Obtener attachment
        if(TGS_workinfos.size() != 0){
            String queryAttachments = 'SELECT Id, Name, ParentId FROM Attachment WHERE ParentId IN (';
            for(integer i=0; i < TGS_workinfos.size(); i++){
                queryAttachments += ' \'' + TGS_workinfos[i].Id + '\'';
                if(i < TGS_workinfos.size()-1){
                    queryAttachments += ',';
                }
            }
            queryAttachments += ' )';
            attachments = (List<Attachment>)Database.query(queryAttachments);
        }
        else{
            attachments = new List<Attachment>();
        }
        
        System.debug('[PCA_Order_Detail_Controller Constructor] attachments: ' + attachments);
        
        //Creamos la lista de workinfos con sus attachments
        WorkInfo aux;
        workinfos = new List<WorkInfo>();
        for(integer i=0; i < TGS_workinfos.size(); i++){
            aux = new WorkInfo(TGS_workinfos[i]);
            aux.getCommentAttachments(attachments);
            workinfos.add(aux);
        }
        System.debug('[PCA_Order_Detail_Controller Constructor] workinfos: ' + workinfos);
        
        //Inicializamos la lista de adjuntos y el adjunto
        listNewCommentAttachments = new List<Attachment>();
        newAttachment = new Attachment();
    }
    
    /***********************************************
     *    PUBLIC METHODS
     ***********************************************/
    /**
     * Method:         crearWorkinfo
     * Description:    Crea un SObject TGS_WorkInfo__c con los datos 'newSubject' y 'newDescription' y lo inserta en la base de datos.
     *                 También inserta todos los Attachment que se encuentran 'listNewCommentAttachments' y que se relacionan mediante
     *                 ParentId con el SObject TGS_WorkInfo__c creado. En caso de error, 'error' será verdadero con el fin de mostrar
     *                 un mensaje al usuario.
     */
    public PageReference crearWorkinfo() {
        TGS_Work_Info__c work = new TGS_Work_Info__c();
        work.TGS_Description__c = newComment;
        work.TGS_Case__c = ApexPages.currentPage().getParameters().get('caseId');
        work.TGS_Public__c = true;
        
        System.debug('[PCA_Order_Detail_Controller.crearWorkinfo] work: ' + work);
        
        insert work;
        List<Attachment> listToInsert = new List<Attachment>();
        if (myAttachedBody != null) {
            newAttachment = new Attachment();
            newAttachment.Name = attachmentName1;
            newAttachment.Description = attachmentDesc1;
            newAttachment.Body = myAttachedBody;
            newAttachment.OwnerId = UserInfo.getUserId();
            newAttachment.ParentId = work.Id;
            listToInsert.add(newAttachment);
        }
        
        System.debug('[PCA_Order_Detail_Controller.crearWorkinfo] newAttachment: ' + newAttachment);
        
        if (myAttachedBody2 != null) {
            newAttachment = new Attachment();
            newAttachment.Name = attachmentName2;
            newAttachment.Description = attachmentDesc2;
            newAttachment.Body = myAttachedBody2;
            newAttachment.OwnerId = UserInfo.getUserId();
            newAttachment.ParentId = work.Id;
            listToInsert.add(newAttachment);
        }
        
        System.debug('[PCA_Order_Detail_Controller.crearWorkinfo] newAttachment: ' + newAttachment);
        
        if (myAttachedBody3 != null) {
            newAttachment = new Attachment();
            newAttachment.Name = attachmentName3;
            newAttachment.Description = attachmentDesc3;
            newAttachment.Body = myAttachedBody3;
            newAttachment.OwnerId = UserInfo.getUserId();
            newAttachment.ParentId = work.Id;
            listToInsert.add(newAttachment);
        }
        
        System.debug('[PCA_Order_Detail_Controller.crearWorkinfo] newAttachment: ' + newAttachment);
        System.debug('[PCA_Order_Detail_Controller.crearWorkinfo] listToInsert: ' + listToInsert);
        
        try {
            if (listToInsert.size() > 0) {
                Database.SaveResult[] results = Database.insert(listToInsert);
                
                System.debug('[PCA_Order_Detail_Controller.crearWorkinfo] results: ' + results);
            }
        } catch (DmlException e) {
            System.debug('Error al subir los adjuntos del workinfo del case : ' + e);
            error = true;
            
            System.debug('[PCA_Order_Detail_Controller.crearWorkinfo] error: ' + error);
        }
        
        //Vaciamos los campos
        listNewCommentAttachments = new List<Attachment>();
        newComment = '';
        
        PageReference pr = ApexPages.currentPage();
        pr.getParameters().put('caseId', ApexPages.currentPage().getParameters().get('caseId'));
        pr.setRedirect(true);
        return pr;
    }
    
    /**
     * Method:         uploadAttachment
     * Description:    Da valor a campos de 'newAttachment' y lo añade a la lista 'listNewCommentAttachments'.
     *                 Se crea un nuevo SObject Attachment relacionado a 'newAttachment'.
     */
    public void uploadAttachment() {
        newAttachment.OwnerId = UserInfo.getUserId();
        newAttachment.IsPrivate = true;
        
        System.debug('[PCA_Order_Detail_Controller.uploadAttachment] newAttachment: ' + newAttachment);
        
        listNewCommentAttachments.add(newAttachment);
        newAttachment = new Attachment();
        
        System.debug('[PCA_Order_Detail_Controller.uploadAttachment] listNewCommentAttachments: ' + listNewCommentAttachments);
     }
    
    /********** BOTONES *********/
    /**
     * Method:         acceptResolution
     * Description:    Da valor a campos de 'newAttachment' y lo añade a la lista 'listNewCommentAttachments'.
     *                 Se crea un nuevo SObject Attachment relacionado a 'newAttachment'.
     */
    public PageReference acceptResolution() {
        List<TGS_Status_machine__c> newStatusMachineList = getStatusMachine(currentCase, currentCase.Status, currentCase.TGS_Status_reason__c);
        System.debug('[PCA_Order_Detail_Controller.acceptResolution] newStatusMachineList: ' + newStatusMachineList);
        try {
            for(TGS_Status_machine__c statusMachine : newStatusMachineList){
                if(statusMachine.TGS_Status__c == Constants.CASE_STATUS_CLOSED){
                    currentCase.status = statusMachine.TGS_Status__c;
                    currentCase.TGS_Status_Reason__c = statusMachine.TGS_Status_Reason__c;
                    break;
                }
            }
            
            System.debug('[PCA_Order_Detail_Controller.acceptResolution] currentCase: ' + currentCase);
            NETriggerHelper.setTriggerFiredTest('CallOrder2Asset', false);
            update currentCase;      
        } catch(DmlException e) {
            
            System.debug('[PCA_Order_Detail_Controller.acceptResolution] Error al Actualizar el Status: ' + e.getMessage());
        }               
        PageReference pr = ApexPages.currentPage();
        pr.getParameters().put('caseId', ApexPages.currentPage().getParameters().get('caseId'));
        pr.setRedirect(true);
        return pr;               
    }

    /**
     * Method:          reopen
     * Description:     Método que se lanza cuando se pulsa el botón reopen.
     *                  Cambia el status y TGS_Status_reason__c del order actual. 
     * 					Imita el comportamiento del flujo Reopen en SFDC.
     */
    public PageReference reopen() {
        List<TGS_Status_machine__c> newStatusMachineList = getStatusMachine(currentCase, currentCase.Status, currentCase.TGS_Status_reason__c);
        
        System.debug('[PCA_Order_Detail_Controller.reopen] newStatusMachineList: ' + newStatusMachineList);
        try {
            for (TGS_Status_machine__c statusMachine : newStatusMachineList){
                if(statusMachine.TGS_Status__c == Constants.CASE_STATUS_IN_PROGRESS){
                    currentCase.status = statusMachine.TGS_Status__c;
                    currentCase.TGS_Status_Reason__c = statusMachine.TGS_Status_reason__c;
                    break;
                }
            }
            System.debug('[PCA_Order_Detail_Controller.reopen] currentCase: ' + currentCase);
            
            update currentCase;
        } catch(DmlException e) {
            System.debug('[PCA_Order_Detail_Controller.reopen] Error al Actualizar el Status: ' + e.getMessage());
        }
        PageReference pr = new PageReference('/PCA_Order_Detail');
        pr.getParameters().put('caseId', ApexPages.currentPage().getParameters().get('caseId'));
        pr.setRedirect(true);
        return pr;               
    }
    
    /**
     * Method:          cancelOrder
     * Description:     Método que se lanza cuando se pulsa el botón Cancel Order.
     *                  Cambia el status y TGS_Status_reason__c del order actual.
     */
    public PageReference cancelOrder() {
        List<TGS_Status_machine__c> newStatusMachineList = getStatusMachine(currentCase, currentCase.Status, currentCase.TGS_Status_reason__c);
        System.debug('[PCA_Order_Detail_Controller.cancelOrder] newStatusMachineList: ' + newStatusMachineList);
        try {
            for(TGS_Status_machine__c statusMachine : newStatusMachineList){
                if(statusMachine.TGS_Status__c == Constants.CASE_STATUS_PENDING && statusMachine.TGS_Status_Reason__c == Constants.CASE_STATUS_REASON_IN_CANCELLATION){
                    currentCase.status = statusMachine.TGS_Status__c;
                    currentCase.TGS_Status_Reason__c = statusMachine.TGS_Status_Reason__c;
                    break;
                }
            }
            
            System.debug('[PCA_Order_Detail_Controller.cancelOrder] currentCase: ' + currentCase);
            
            update currentCase; 
        } catch(DmlException e) {
            
            System.debug('[PCA_Order_Detail_Controller.cancelOrder] Error al Actualizar el Status: ' + e.getMessage());
        }                
        PageReference pr = new PageReference('/PCA_Order_Detail');
        pr.getParameters().put('caseId', ApexPages.currentPage().getParameters().get('caseId'));
        pr.setRedirect(true);
        return pr;     
    }
    
    /**
     * Method:          discardCancellation
     * Description:     Método que se lanza cuando se pulsa el botón Discard Cancellation.
     *                  Cambia el status y TGS_Status_reason__c del order actual. 
     * 					Se envia al mismo que en el paso de assigned para mantener el flujo de datos.
     */
    public PageReference discardCancellation() {
        List<TGS_Status_machine__c> assignedStatusMachineList = getStatusMachine(currentCase, Constants.CASE_STATUS_ASSIGNED, '');

        System.debug('[PCA_Order_Detail_Controller.discardCancellation] assignedStatusMachineList: ' + assignedStatusMachineList);
        
        try {
            for (TGS_Status_machine__c assignedStatusMachine : assignedStatusMachineList){
                if(assignedStatusMachine.TGS_Status__c ==  Constants.CASE_STATUS_IN_PROGRESS){
                    currentCase.status = assignedStatusMachine.TGS_Status__c;
                    currentCase.TGS_Status_Reason__c = assignedStatusMachine.TGS_Status_reason__c;
                    currentCase.TGS_CWP_Accept_Cancellation__c = false;
                }
            }
        
            System.debug('[PCA_Order_Detail_Controller.discardCancellation] currentCase: ' + currentCase);
            
            update currentCase; 
        } catch(DmlException e) {
            System.debug('[PCA_Order_Detail_Controller.discardCancellation] Error al Actualizar el Status: ' + e.getMessage());
        }  
        PageReference pr = new PageReference('/PCA_Order_Detail');
        pr.getParameters().put('caseId', ApexPages.currentPage().getParameters().get('caseId'));
        pr.setRedirect(true);
        return pr; 
    }
    
    /**
     * Method:          acceptCancellation
     * Description:     Método que se lanza cuando se pulsa el botón Accept Cancellation.
     *                  Cambia el status y TGS_Status_reason__c del order actual.
     */
    public PageReference acceptCancellation() {
        List<TGS_Status_machine__c> newStatusMachineList = getStatusMachine(currentCase, currentCase.Status, currentCase.TGS_Status_reason__c);
        
        System.debug('[PCA_Order_Detail_Controller.acceptCancellation] newStatusMachineList: ' + newStatusMachineList);
        try {
            for (TGS_Status_machine__c statusMachine: newStatusMachineList){
                if (statusMachine.TGS_Status_reason__c == Constants.CASE_STATUS_REASON_IN_CANCELLATION_OFF_NET){
                    currentCase.status = statusMachine.TGS_Status__c;
                    currentCase.TGS_Status_Reason__c= statusMachine.TGS_Status_reason__c;
                    currentCase.TGS_CWP_Accept_Cancellation__c = true;
                    break;
                }
                else if (statusMachine.TGS_Status_reason__c == Constants.CASE_STATUS_REASON_IN_CANCELLATION_CLOSE){
                    currentCase.status = statusMachine.TGS_Status__c;
                    currentCase.TGS_Status_Reason__c= statusMachine.TGS_Status_reason__c;
                }
            }
            
            System.debug('[PCA_Order_Detail_Controller.acceptCancellation] currentCase: ' + currentCase);
            
            update currentCase;            
        } catch(DmlException e) {
            
            System.debug('[PCA_Order_Detail_Controller.acceptCancellation]  Error al Actualizar el Status: ' + e.getMessage());
        }               
        PageReference pr = ApexPages.currentPage();
        pr.getParameters().put('caseId', ApexPages.currentPage().getParameters().get('caseId'));
        pr.setRedirect(true);
        return pr;               
    }

    /**
     * Method:          getHTML
     * Description:     Método que crea dinámicamente la parte de la vista en donde se muestran los atributos del servicio mostrado.
     */
    public void getHTML(Service s){
        if(s.CSUIDconfItem[0]!=null && s.CSUIDconfItem[0]!=''){
            attributesHTML +='<h2 style=\"border-bottom: solid 1px #0095a7;width:50%;float:left;margin-bottom:15px;\">'
                +s.name+ '</h2><h2 style=\"border-bottom: solid 1px #0095a7;width:50%; float:left;margin-bottom:15px;\">'+ 'CSUID: '+ s.CSUIDconfItem[0]+'</h2>';
        }
        else{
            attributesHTML +='<h2 style=\"border-bottom: solid 1px #0095a7;width:100%;float:left;margin-bottom:15px;\">'
                +s.name+ '</h2>';
        }
        if(s.hasAttributes){ //si tiene atributos, los coge
            for( ItemAttribute attribute : s.listAttributes){
                String attributeWithSpaces = insertSpacesInAttribute(attribute.name);
                attributesHTML += '<apex:outputText">';
                if(attribute.csuid != null){
                    attributesHTML +=  '<div  style=\"margin-bottom:5px; float:left; width:100%;\">';
                    attributesHTML +=   '<div class=\"halfWrapper\" style=\"margin-bottom:5px;\">';
                }
                else{
                    attributesHTML +=   '<div style=\"margin-bottom:5px; width:50%;float:left;\">';   
                }
                attributesHTML +=      '<label style=\"margin-right:5%; width:33%; float: left; word-wrap: break-word;\">'+attributeWithSpaces+': </label>';
                attributesHTML +=      '<div style= \"word-wrap: break-word; margin-right:5%; width:57%; float: left;display:block;\">';
                if(attribute.value != null){
                    attributesHTML += attribute.value;
                }
                else{
                    attributesHTML += '-';
                }
                attributesHTML +=  '</div></div>';
                if(attribute.csuid != null){
                    attributesHTML +=  '<div class=\"halfWrapper\" style=\"margin-bottom:5px;\">';
                    attributesHTML +=      '<label style=\"margin-right:5%; width:25%; float: left; word-wrap: break-word;\">CSUID: </label>';
                    attributesHTML +=      '<div style= \"word-wrap: break-word; width:70%; float: left;display:block;\">'+attribute.csuid;
                    attributesHTML +=  '</div></div>';
                    attributesHTML += '<br></br>';
                    attributesHTML +='</div><div class=\"clear"></div>';
                }
                
                attributesHTML += '</apex:outputText>';   
            }
        }
        System.debug('[PCA_Order_Detail_Controller.getHTML] attributesHTML: ' + attributesHTML);
    }
    
    public String insertSpacesInAttribute(String attributeName){
        List<Integer> attributeNameChars = attributeName.getChars();
        for (Integer i=2; i< attributeNameChars.size(); i++){
            integer previousSize = attributeNameChars.size();
            if(attributeNameChars.get(i) >= 97 && attributeNameChars.get(i) <= 122 && attributeNameChars.get(i-1) >= 65 && attributeNameChars.get(i-1) <= 90){
                attributeNameChars.add(i-1, 32);
            }
            if(attributeNameChars.get(i-1) >= 97 && attributeNameChars.get(i-1) <= 122 && attributeNameChars.get(i) >= 65 && attributeNameChars.get(i) <= 90){
                attributeNameChars.add(i, 32);
            }
            if(previousSize < attributeNameChars.size()){
                i++;
            }
        }
        return String.fromCharArray(attributeNameChars);
    }
        
    /**
     * Method:           getStatusMachine()
     * Description:     Método que obtiene una lista de todos los posibles TGS_Status_machine__c dependiendo del estado
     *                  actual del servicio mostrado.
     */
    public List<TGS_Status_machine__c> getStatusMachine(Case curCase, String curStatus, String curStatusReason) {
        List<TGS_Status_machine__c> listStatusMachine = new List<TGS_Status_machine__c>();
        if(curCase.TGS_Status_reason__c == null) {
            curCase.TGS_Status_reason__c = '';    
        }
        
        // Get all available status transitions from the current Status and Status reason
        try {
            //First list with ALL the status machine transitions for the current status, status reasons and Service
            List<TGS_Status_machine__c> listStatusMachineAux = new List<TGS_Status_machine__c>();
            listStatusMachineAux = TGS_Portal_Utils.getStatusMachineListByCaseForDesiredStatusAndStatusReason(curCase, curStatus, curStatusReason);
            String profileCreatedBy = TGS_Portal_Utils.getProfileOfCreatorOfCase(curCase);
            
            //Try to get all the status machine transitions for the CreatedBy User Profile
            for(TGS_Status_machine__c statusMachineWithProfile : listStatusMachineAux) {
                if((statusMachineWithProfile.TGS_Profile__c == profileCreatedBy) ||  (Test.isRunningTest())) {
                    if(statusMachineWithProfile.TGS_Status_reason__c == null) statusMachineWithProfile.TGS_Status_reason__c = '';
                    listStatusMachine.add(statusMachineWithProfile);
                }
            }
            
            //If there are no status machine transitions for the CreatedBy User Profile, get all the default status machine transitions (the ones without profile)
            if(listStatusMachine.size() == 0) {
                system.debug('[PCA_Order_Detail_Controller.getStatusMachine] No status machine transitions for the CreatedBy profile, lets get the default ones');                
                for(TGS_Status_machine__c statusMachineWithoutProfile : listStatusMachineAux) {
                    if(statusMachineWithoutProfile.TGS_Profile__c == null) {
                        if(statusMachineWithoutProfile.TGS_Status_reason__c == null) statusMachineWithoutProfile.TGS_Status_reason__c = '';
                        listStatusMachine.add(statusMachineWithoutProfile);
                    }
                }
            }
            System.debug('[PCA_Order_Detail_Controller.getStatusMachine] listStatusMachine: ' + listStatusMachine);
        }
        catch(System.QueryException e) {
            
            System.debug('[PCA_Order_Detail_Controller.getStatusMachine] Error: '+ e.getmessage());
        }
        
        return listStatusMachine;
    }
    
     /***********************************************
     *    INNER CLASSES
     ***********************************************/
    /**
     * Class:            ItemAttribute
     * Description:      Contiene la información del atributo.
     */
    private Class ItemAttribute {
        public String name {get; set;}
        public String value {get; set;}
        public String csuid {get; set;}
        public ItemAttribute(String name, String value, String csuid) {
            this.name = name;
            this.value = value;
            this.csuid = csuid;
        }
    }
    
    /**
     * Class:            Service
     * Description:      Contiene la información del servicio y sus subservicios.
     */
    private Class Service{
        public String id {get; set;}
        public String name {get; set;}
        public String csuid {get; set;}
        public List<String> CSUIDconfItem {get;set;}
        public boolean hasAttributes {get; set;}
        public List<ItemAttribute> listAttributes {get; set;}
        public String action {get; set;}
        public Service(String id, String name, String csuid, String action) {
            this.id = id;
            this.name = name;
            this.action=action;
            this.listAttributes = getAttributes(id);
            if(this.listAttributes.size()>0){
                    this.hasAttributes = true;
             }
             else {
                    this.hasAttributes = false;
             }
            
            //this.hasAttributes = false;
            this.CSUIDconfItem=new List<String>();
            if (csuid!=null&&csuid!=''){
                this.CSUIDconfItem.add(CSUID);   
            }
            else {this.CSUIDconfItem.add('');}
        }
        
        //Se obtienen los atributos del servicio.
        public List<ItemAttribute> getAttributes(String id){
            List<NE__Order_Item_Attribute__c> attributes = [SELECT Name, NE__Value__c,TGS_CSUID__c 
                                                            FROM NE__Order_Item_Attribute__c 
                                                            WHERE NE__Order_Item__c =:id 
                                                            AND NE__FamPropId__r.TGS_Hidden_in_CWP__c = false]; //JV: añadido visibilidad
            List<ItemAttribute> listaAtributos = new List<ItemAttribute>();
            for(NE__Order_Item_Attribute__c attr : attributes) {
                //validamos que no vengan caracteres especiales
                String value = attr.NE__Value__c;
                if (value != null && !''.equals(value)){
                    //Where:
                    //\w : A word character, short for [a-zA-Z_0-9]
                    //\W : A non-word character
                    //value = value.replaceAll('\\W', ' '); 
                    //Escapa los caracteres html
                    value = value.escapeHtml4();
                    //Escapa los caracteres unicode
                    value = value.escapeUnicode();
                }

                listaAtributos.add(new ItemAttribute(attr.Name, value,attr.TGS_CSUID__c));
            }
            
            System.debug('[PCA_Order_Detail_Controller.Service.getAttributes] listaAtributos: ' + listaAtributos);
            
            return listaAtributos;
        }
    }
   
    /**
     * Class:            WorkInfo
     * Description:      Contiene la información de un cierto comentario de un Case.
     */
    public class WorkInfo {
        public TGS_Work_Info__c work {get; set;}
        public List<Attachment> listAttachment {get; set;}
        public String userName {get; set;}
        
        public WorkInfo (TGS_Work_Info__c work) {
            this.work = work;
            User user = [SELECT Name FROM User WHERE Id = :work.CreatedById LIMIT 1];
            this.userName = user.Name;
        }
        
        /**
         * Method:         hasAttachments
         * Param:          'attachments' es una lista de adjuntos
         * Description:    Verdadero si el TGS_Work_Info__c tiene archivos adjuntos.
         *                 De lo contrario, falso.
         */
        public Boolean hasAttachments(List<Attachment> attachments){
            Boolean hasAttach = false;
            for(integer i=0; i<attachments.size() && !hasAttach; i++){
                if(attachments[i].ParentId == work.Id){
                    hasAttach = true;
                }
            }
            
            System.debug('[PCA_Order_Detail_Controller.WorkInfo.hasAttach] hasAttach: ' + hasAttach);
            
            return hasAttach;
        }
        
        /**
         * Method:         getCommentAttachments
         * Param:          'attachments' es una lista de adjuntos
         * Description:    Guarda en 'listAttachment' los adjuntos del workinfo.
         */
        public void getCommentAttachments(List<Attachment> attachments){
            listAttachment = new List<Attachment>();
            for(integer i=0;i<attachments.size(); i++){
                if(attachments[i].ParentId == work.Id){
                    listAttachment.add(attachments[i]);
                }
            }
            
            System.debug('[PCA_Order_Detail_Controller.WorkInfo.getCommentAttachments] listAttachment: ' + listAttachment);
        }
    }
}