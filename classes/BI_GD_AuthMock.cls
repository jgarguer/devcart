/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Jesús Martínez
Company:        Everis España
Description:    Autenticación FSARG (MOCK).

History:
 
<Date>                      <Author>                        <Change Description>
26/10/2016                  Jesús Martínez               	Versión Inicial.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class BI_GD_AuthMock implements HttpCalloutMock{

    public  Boolean authCliente;    
      
    global HTTPResponse respond (HttpRequest request) {
        /*AUTH CHECK*/
        String[] header = new List<String> {request.getMethod()};
        HttpResponse response = new HttpResponse();
        //MOCK PARA LA LLAMADA A AUTENTICAR.
        //if(header[0] == 'POST'){
        if(header[0] == 'POST' || header[0] == 'PUT'){
            if(authCliente){
                response.setBody('{"access_token":"TokenDemo"}');
                response.setStatusCode(200);
            }else{
                response.setBody('{"access_token":""}');
                response.setStatusCode(401);
            } 
        } return response;      
    }
}