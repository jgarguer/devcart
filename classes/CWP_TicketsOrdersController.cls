public class CWP_TicketsOrdersController {

    @auraEnabled
    public static list<Case> getCaseList(){
        User currentUser = CWP_LHelper.getCurrentUser(null);
        system.debug('llamada a recuperacion de casos en tickets y pedidos  +  '  + currentUser);
        list<Case> retList = new list<Case>();
        List<string> casesType= new List<String> {'BI_Caso_Comercial_Abierto', 'BI_Caso_Comercial_Cerrado',  'BIIN_Incidencia_Tecnica' , 'BIIN_Solicitud_Incidencia_Tecnica' , 'BI2_caso_comercial' , 'BI2_Caso_Padre' };
        map<id, RecordType> rtMap = new map<id, RecordType>([select Id, DeveloperName from RecordType where DeveloperName in : casesType]);
        system.debug('mapa de recordTypes recuperado    '+rtMap);
        retList = [SELECT 
                   BI2_PER_Caso_Padre__c,
                   CaseNumber, 
                   toLabel(Reason), 
                   BI_COL_Fecha_Radicacion__c, 
                   Subject, 
                   BI2_Usuario_creador_de_la_incidencia__c, 
                   toLabel(Status), 
                   toLabel(Origin), 
                   toLabel(Priority),  
                   CurrencyIsoCode,
                   Description,
                   TGS_Resolution__c,
                   Id, 
                   (SELECT Id, ParentId, Name, CreatedBy.Name, CreatedDate FROM Attachments)
                    FROM Case WHERE 
                   AccountId =: currentUser.AccountId /*AND RecordTypeId IN: rtMap.keySet()  AND BI_Confidencial__c = FALSE */ order by BI_COL_Fecha_Radicacion__c DESC];
        system.debug('casos recuperados:    ' + retList.size() + '  lista:  ' + retList);
        return retList;
    }
    
    @auraEnabled
    public static String addNewComment(String parentId, String commentBody) {
        system.debug('nuevo comentario  ' + commentBody + ' para el caso    ' + parentId);
        CaseComment newComment = new CaseComment(
            ParentId = parentId,
            CommentBody = commentBody
        );
        insert newComment;
        // retornamos el id para comprobar que esta correcto
        return ''+newComment.Id;
    }
    
    @auraEnabled
    public static Id saveTheFile(string parentId, String fileName, String base64Data, String contentType) { 
        system.debug('se va a adjuntar al caso de id    '+parentId);
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = new Attachment();
        a.parentId = parentId;
 
        a.Body = EncodingUtil.base64Decode(base64Data);
        a.Name = fileName;
        a.ContentType = contentType;
        
        insert a;
        
        return a.Id;
    }
    
    @auraEnabled
    public static List<CaseComment> getCaseComment(string parentId) { 
        List<CaseComment> retorno = new List<CaseComment>();
        for(CaseComment cmm: [Select CommentBody, CreatedDate, CreatedBy.Name FROM CaseComment Where ParentId =: parentId order by createdDate DESC]){
            retorno.add(cmm);
        }
        return retorno;
    }
    
    @auraEnabled
    public static map<string, integer> getTableHeader(){
        system.debug('llamada al header ');
        map<string, integer> positionKeyMap = new map<string, integer>{
            'BI2_PER_Caso_Padre__c' => 0,
            'CaseNumber' => 1,
            'Reason' => 2,
            'BI_COL_Fecha_Radicacion__c' => 3,
            'Subject' => 4,
            'BI2_Usuario_creador_de_la_incidencia__c' => 5,
            'Status' => 6,
            'Origin' => 7,
            'Priority' => 8
        };
        map<string, integer> retMap = new map<string, integer>();
            
        map<String, SobjectField> fieldMap = Case.getsObjectType().getDescribe().Fields.getMap();
        system.debug('fielMap recuperado    '+ fieldMap);
        
        for(string i : positionKeyMap.keySet()){
            retMap.put(fieldMap.get(i.toLowerCase()).getDescribe().getLabel(), positionKeyMap.get(i));
        }
        retMap.put(label.CWP_attachments, 9);
        system.debug('mapa de campos y posiciones   ' +retMap);
        return retMap;
    }
    
    @auraEnabled
    public static Case saveNewCase(string subject, string reason, string description){
        system.debug('llamada a creacion de caso; asunto:   ' +subject+'    reason: ' + reason+'    description ' + description);
        User currentUser = CWP_LHelper.getCurrentUser(null);
        list<RecordType> rtList = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Case'];
        list<BI_Configuracion_Caso_PP__c> caseConfiguration = new list<BI_Configuracion_Caso_PP__c>([SELECT BI_Confidencial__c, BI_COL_Fecha_Radicacion__c, BI_Type__c, BI_Tipos_de_registro__c, BI_Country__c, BI_Origen__c FROM BI_Configuracion_Caso_PP__c WHERE BI_Country__c =: currentUser.Pais__c LIMIT 1]);
        system.debug('configuracion de caso ' + caseConfiguration);
        map<string, id> rtMap = new map<string, id>();
        for(Recordtype i : rtList){
            rtMap.put(i.DeveloperName, i.id);
        }
        Case newCase = new Case(
            subject = subject,
            reason = reason,
            description = description,
            status = 'Assigned',
            recordTypeId = rtMap.get('BI_Caso_Comercial_Abierto')
        );
        if(!caseConfiguration.isEmpty()){
            newCase.BI_Country__c = caseConfiguration[0].BI_Country__c;
            if(caseConfiguration[0].BI_COL_Fecha_Radicacion__c){
                newCase.BI_Col_Fecha_Radicacion__c = Datetime.now();
            }
        }
        system.debug('caso para ser insertado   ' + newCase);
        boolean haveError = PCA_CaseHelper.saveCase(newCase);
        Case retCase = new Case();
        if(!haveError){
            retCase = [SELECT 
                   BI2_PER_Caso_Padre__c,
                   CaseNumber, 
                   toLabel(Reason), 
                   BI_COL_Fecha_Radicacion__c, 
                   Subject, 
                   BI2_Usuario_creador_de_la_incidencia__c, 
                   toLabel(Status), 
                   toLabel(Origin), 
                   toLabel(Priority),  
                   CurrencyIsoCode,
                   Description,
                   TGS_Resolution__c,
                   Id FROM Case WHERE Id =: newCase.id];
        }
        system.debug('caso creado   ' + retCase);
        return retCase;
    }
}