public with sharing class BI_COL_Callejero_wsdl 
{

	    public class ParametrosGeo {
        public String id;
        public String departamento;
        public String municipio;
        public String localidad;
        public String barrio;
        public String direccion;
        private String[] id_type_info = new String[]{'id','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] departamento_type_info = new String[]{'departamento','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] municipio_type_info = new String[]{'municipio','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] localidad_type_info = new String[]{'localidad','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] barrio_type_info = new String[]{'barrio','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] direccion_type_info = new String[]{'direccion','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'id','departamento','municipio','localidad','barrio','direccion'};
    }
    public class Interfaz1_element {
        public BI_COL_Callejero_wsdl.ArrayOfParametrosGeo misParametrosGeo;
        private String[] misParametrosGeo_type_info = new String[]{'misParametrosGeo','http://tempuri.org/','ArrayOfParametrosGeo','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'misParametrosGeo'};
    }
    public class WebServiceSoap {
    	BI_COL_ServiciosWeb__c SW = BI_COL_ServiciosWeb__c.getInstance();
        public String endpoint_x = SW.ENDPOINT_CALLEJEROS__c;//'http://tecolserviciostest.telecom.net.co:20002/WSGeoTTSF/WSGeo.asmx';//Label.lblEndPointCallejero;//'http://10.201.179.176/WSGeoTT/WSGeo.asmx' 
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x=120000;
        private String[] ns_map_type_info = new String[]{'http://tempuri.org/', 'BI_COL_Callejero_wsdl'};
        public String Interfaz1(BI_COL_Callejero_wsdl.ArrayOfParametrosGeo misParametrosGeo) {
            BI_COL_Callejero_wsdl.Interfaz1_element request_x = new BI_COL_Callejero_wsdl.Interfaz1_element();
            BI_COL_Callejero_wsdl.Interfaz1Response_element response_x;
            request_x.misParametrosGeo = misParametrosGeo;
            Map<String, BI_COL_Callejero_wsdl.Interfaz1Response_element> response_map_x = new Map<String, BI_COL_Callejero_wsdl.Interfaz1Response_element>();
            response_map_x.put('response_x', response_x);
            system.debug('llegando al SW-->'+endpoint_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://tempuri.org/Interfaz1',
              'http://tempuri.org/',
              'Interfaz1',
              'http://tempuri.org/',
              'Interfaz1Response',
              'BI_COL_Callejero_wsdl.Interfaz1Response_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.Interfaz1Result;
        }
    }
    public class ArrayOfParametrosGeo {
        public BI_COL_Callejero_wsdl.ParametrosGeo[] ParametrosGeo;
        private String[] ParametrosGeo_type_info = new String[]{'ParametrosGeo','http://tempuri.org/','ParametrosGeo','0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'ParametrosGeo'};
    }
    public class Interfaz1Response_element {
        public String Interfaz1Result;
        private String[] Interfaz1Result_type_info = new String[]{'Interfaz1Result','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'Interfaz1Result'};
    }

}