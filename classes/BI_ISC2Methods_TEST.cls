@isTest
private class BI_ISC2Methods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_ISC2Methods class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    07/07/2014              Pablo Oliva             Initial Version
    20/09/2017             Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c   
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for assignISC method and updateAverage method.
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    07/07/2014              Pablo Oliva             Initial Version
    17/11/2016              Gawron, Julian          Add throw_exception=false 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void assignISCTest() {
        BI_TestUtils.throw_exception=false;
        ////////////////////////////////
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
            Integer K = 50;
        ///////////////////////////////
        Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'BI_ISC2__c']){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }

        
        List <String> regionList = BI_DataLoad.loadPaisFromPickList(K);
              
        List <Account> accountList = BI_DataLoad.loadAccounts(K, regionList);
        
        Set<Id> accountSet = new Set<Id>();
        list<BI_ISC2__c> lst_isc = new list<BI_ISC2__c>();
        BI_ISC2__c isc;
        BI_ISC2__c isc_termometro;
        
        for(Account item : accountList){
            accountSet.add(item.Id);
            
            isc = new BI_ISC2__c (BI_Cliente__c = item.Id, 
                                    BI_Fecha__c = Date.today().addDays(-15),
                                    BI_Satisfaccion_general_TEF__c = 9.12,
                                    RecordTypeId = MAP_NAME_RT.get('BI_CORE') );
            lst_isc.add(isc);                                

            isc_termometro = new BI_ISC2__c (BI_Cliente__c = item.Id, 
                        BI_Fecha__c = Date.today().addDays(-16), 
                        BI_CHI_Promedio_del_termometro__c = 2.5,
                        RecordTypeId = MAP_NAME_RT.get('BI_CHI_Termometro'),
                        BI_CHI_10_Proceso_de_facturacion__c = '3.6',
                        BI_CHI_11_Sistema_de_cobranza__c = '3.6',
                        BI_CHI_12_Soporte_tecnico_fijo__c = '3.6',
                        BI_CHI_13_Soporte_tecnico_movil__c = '3.6',
                        BI_CHI_14_Recambio_de_equipos__c = '3.6',
                        BI_CHI_15_Atencion_post_venta_comercial__c = '3.6',
                        BI_CHI_16_Cobertura_movil__c = '3.6',
                        BI_CHI_17_Satisfaccion_general__c = '3.6',
                        BI_CHI_8_Oferta_comercial_entregada__c = '3.6',
                        BI_CHI_9_Proceso_de_instalacion__c = '3.6'
            );
                             lst_isc.add(isc_termometro);           

        }
        
        insert lst_isc;
        
        
         list<BI_ISC2__c> lst_iscNEWS = new list<BI_ISC2__c>();
        for(Account item : accountList){
            isc = new BI_ISC2__c (BI_Cliente__c = item.Id, 
                                    BI_Fecha__c = Date.today(),
                                        BI_Satisfaccion_general_TEF__c = 2.3,
                                        RecordTypeId = MAP_NAME_RT.get('BI_CORE')
                                        );
            
            lst_iscNEWS.add(isc);                                        

        isc_termometro = new BI_ISC2__c (BI_Cliente__c = item.Id, 
                BI_Fecha__c = Date.today().addDays(+1), 
                BI_CHI_Promedio_del_termometro__c = 8.5,
                RecordTypeId = MAP_NAME_RT.get('BI_CHI_Termometro'),
                BI_CHI_10_Proceso_de_facturacion__c = '3.6',
                BI_CHI_11_Sistema_de_cobranza__c = '3.6',
                BI_CHI_12_Soporte_tecnico_fijo__c = '3.6',
                BI_CHI_13_Soporte_tecnico_movil__c = '3.6',
                BI_CHI_14_Recambio_de_equipos__c = '3.6',
                BI_CHI_15_Atencion_post_venta_comercial__c = '3.6',
                BI_CHI_16_Cobertura_movil__c = '3.6',
                BI_CHI_17_Satisfaccion_general__c = '3.6',
                BI_CHI_8_Oferta_comercial_entregada__c = '3.6',
                BI_CHI_9_Proceso_de_instalacion__c = '3.6'
                );
         lst_isc.add(isc_termometro);                                        
        }
        
        Test.startTest();
        
        insert lst_iscNEWS;
                    
        for(Account item:[select Id, BI_ISC__c, BI_CHI_Fecha_del_ultimo_termometro__c, BI_CHI_Promedio_del_ultimo_termometro__c from Account where Id IN :accountSet]){
            system.debug('ASSERT: VAR1['+ item.BI_ISC__c +'] != [null]');
            system.assert(item.BI_ISC__c != null);
            system.debug('ASSERT: VAR2['+ item.BI_CHI_Fecha_del_ultimo_termometro__c +'] != [null]');
            system.assert(item.BI_CHI_Fecha_del_ultimo_termometro__c != null);
            system.debug('ASSERT: VAR3['+ item.BI_CHI_Promedio_del_ultimo_termometro__c +'] != [null]');
            system.assert(item.BI_CHI_Promedio_del_ultimo_termometro__c != null);
        }
            
        Test.stopTest();
        
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Test method to manage the code coverage for createPlandeAccion method.
    
    History:        
    
    <Date>            <Author>          <Description>
    22/09/2016        Miguel Cabrera       Initial version
    30/11/2016        Alvaro García        Modify the team role member
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static testMethod void createPlandeAccion(){

       /* BI_TestUtils.throw_exception = false;
        TGS_User_Org__c uO = new TGS_User_Org__c();
        uO.TGS_Is_BI_EN__c = True;
        uO.SetupOwnerId = UserInfo.getUserId();
        insert uO;*/

        RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];

        List<Account> lst_acc1 = new List<Account>();       
        Account acc1 = new Account(Name = 'test',
                  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                  BI_Activo__c = Label.BI_Si,
                 
                  BI_Subsegment_local__c = 'Top 1',
                  RecordTypeId = rt1.Id,
                  BI_Country__c = 'Spain',
                  BI_Segment__c = 'test',
                    BI_Subsegment_Regional__c = 'test',
                    BI_Territory__c = 'test'
                  );    
        lst_acc1.add(acc1);
        insert lst_acc1;

        Contact con = new Contact(
            AccountId = acc1.Id,
            Lastname = 'Testing12',
            Email = 'estu@testorg.com',
            BI_Country__c = 'Spain'
        );
        insert con;

        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = BI_DataLoad.searchAdminProfile(),
                          BI_Permisos__c = Label.BI_Administrador,
                          UserPermissionsMarketingUser = true,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com',
                          Pais__c = 'Spain',
                          IsActive = true);
        
        insert usertest;

        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountId = acc1.Id;
        atm.TeamMemberRole = 'GSE';
        atm.UserId = usertest.Id;
        insert atm; 

        RecordType rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'BI_O4_Global'];
        List<BI_ISC2__c> lstIsc = new List<BI_ISC2__c>();
        BI_ISC2__c pa = new BI_ISC2__c();
        pa.BI_O4_CSI_TEF_Total__c = 5;
        pa.RecordTypeId = rt.Id;
        pa.BI_Fecha__c = Date.Today();
        pa.BI_Cliente__c = acc1.Id;
        pa.CurrencyIsoCode = 'EUR';
        lstIsc.add(pa);

        Test.startTest();
        insert pa;
        BI_ISC2Methods.createPlandeAccion(lstIsc);
        Test.stopTest();
    }
}