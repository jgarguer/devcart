public with sharing class BI_FVI_RentabilidadNetaMethods 
{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes 
    Company:       ACCENTURE
    Description:   Method Create Record on BI_Registro_Datos_Desempeno__c when a Rentability Record has created
    
    IN:            List<BI_FVI_Rentabilidad_Neta__c > tnew, List<BI_FVI_Rentabilidad_Neta__c > told
    OUT:           
    
    History:
    
    <Date>            	<Author>          	<Description>
    21/09/2017        	Humberto Nunes    	Initial version.
    22/09/2017			Humberto Nunes		Se incremento la funcionalidad para que sea por Cliente o NODO... 
    07/11/2017			Alfonso Alvarez     Se quito la referencia a Cliente. 
    15/03/2018          Humberto Nunes      Se agrego el Tipo de Registro 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    static
    {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'BI_Registro_Datos_Desempeno__c'])
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
    }

    public static void CreateRegistroDesemp(List<BI_FVI_Rentabilidad_Neta__c > tnew, List<BI_FVI_Rentabilidad_Neta__c > told)
    {
        try 
        {
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');

            List<BI_Registro_Datos_Desempeno__c> lstRegDatosDesemp = new List<BI_Registro_Datos_Desempeno__c>();

            for(Integer i=0; i<tnew.size(); i++)
            {
                BI_Registro_Datos_Desempeno__c objRegDatosDesemp = new BI_Registro_Datos_Desempeno__c();
                objRegDatosDesemp.BI_FVI_Fecha__c = tnew[i].BI_FVI_Fecha_Registro__c;
                // 07-11-2017 INICIO AJAE
                // objRegDatosDesemp.BI_FVI_Id_Cliente__c = tnew[i].BI_FVI_Cuenta__c;
                // 07-11-2017 FIN AJAE
                objRegDatosDesemp.BI_FVI_Id_Nodo__c = tnew[i].BI_FVI_Nodo__c;
                objRegDatosDesemp.BI_Tipo__c = 'Rentabilidad Neta'; 
                objRegDatosDesemp.BI_FVI_Valor__c = tnew[i].BI_FVI_Ganancia_Neta_Total__c; 
                objRegDatosDesemp.BI_FVI_IdObjeto__c = tnew[i].Id;
                objRegDatosDesemp.RecordTypeId = MAP_NAME_RT.get('BI_FVI_Desempeno_Nodos'); // HN 15/03/2018
                lstRegDatosDesemp.add(objRegDatosDesemp);
            }

            insert lstRegDatosDesemp;
        }
        catch (exception Exc)
        {
            BI_LogHelper.generate_BILog('BI_FVI_RentabilidadNetaMethods.CreateRegistroDesemp', 'BI_EN', Exc, 'Trigger');
        }
    }
}