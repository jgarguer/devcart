/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis
Company:       Everis
Description:   helper class for new modification creation process
Test Class:    CWP_InstalledServicesController_TEST

History:

<Date>                  <Author>                <Change Description>
23/02/2017              Everis                    Initial Version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public without sharing class CWP_New_Modification_Controller {
    
    public String detailIdNewModification{get;set;}
    public NE__OrderItem__c currentOI {get;set;}
    
    public List<String> listTest {get;set;}
    public Boolean newModifError{get;set;}
    
    //Attachments
    public List<Attachment> listCaseAttachmentsMod {get;set;}
    public List<Attachment> attachmentModificationList {get;set;}
    public map<string,id> wiMap{get;set;}
    
    public Attachment newAttachmentMod {get;set;}
    public blob myAttachedBodyMod {get; set;}
    public blob myAttachedBodyMod2 {get; set;}
    public blob myAttachedBodyMod3 {get; set;}
    
    public String attachmentNameMod1 {get; set;}
    public String attachmentNameMod2 {get; set;}
    public String attachmentNameMod3 {get; set;}
    
    public String attachmentDescMod1 {get; set;}
    public String attachmentDescMod2 {get; set;}
    public String attachmentDescMod3 {get; set;}
    public List<String> listCaseAttachmentsNameMod {get; set;}
    public String caseID{get; set;}
    public String caseDescriptionParam{get; set;}
    public String orderIdForTest;
    
    /*
Developer Everis
Function: constructor related to CWP_Installed_Services_controller, so it can be used as
extension of the visualforce page which has installed services as controller.
*/ 
    public CWP_New_Modification_Controller(CWP_Installed_Services_Controller controller){
        
        
    }
    public CWP_New_Modification_Controller(){
        
        
    }
    
    /*
Developer Everis
Function: method used to get important infomation about the order intem on which the modification is being opened
*/ 
    public void getSelectedIdNewModification(){
        system.debug('newmodification detail id: ' + detailIdNewModification);
        currentOI = [SELECT 
                     Id, 
                     Name, 
                     NE__ProdId__c, 
                     NE__ProdId__r.Name, 
                     NE__ProdId__r.TGS_CWP_Tier_1__c, 
                     NE__ProdId__r.TGS_CWP_Tier_2__c, 
                     TGS_Holding_Name__c,
                     NE__Account__c,
                     NE__Account__r.Name,
                     NE__Account__r.TGS_Aux_Holding__r.Name,
                     NE__Billing_Account__c,
                     NE__Billing_Account__r.Name,
                     NE__Service_Account__c,
                     NE__Service_Account__r.Name,
                     NE__Asset_Item_Account__r.Name, 
                     NE__Billing_Account_Asset_Item__r.Name, 
                     NE__Service_Account_Asset_Item__r.Name,
                     NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name,
                     NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name,                       
                     NE__CatalogItem__r.NE__ProductId__r.Name
                     FROM NE__OrderItem__c WHERE Id =: detailIdNewModification];
        
        currentOI.NE__ProdId__r.TGS_CWP_Tier_1__c= currentOI.NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name;
        currentOI.NE__ProdId__r.TGS_CWP_Tier_2__c= currentOI.NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name;                            
        currentOI.NE__ProdId__r.Name=              currentOI.NE__CatalogItem__r.NE__ProductId__r.Name;
        
        Cache.Session.put('local.CWPexcelExport.tipoAdjunto', 'Modifi');
        
        listTest = new List<String>();
        listTest.add('valor inicial');
        
        system.debug('-------------Holding de la cuenta: ' + currentOI.NE__Account__r.TGS_Aux_Holding__r.Name);
    }
    
    /*
Developer Everis
Function: method used to create the new modification  
*/ 
    public String crearMod(){
        System.debug('[PCA_New_Incident_Controller.crearWorkinfo] $$$ listCaseAttachmentsNameMod: ' + attachmentModificationList+'El order Item es '+detailIdNewModification);
        
        
        
        /*Pagereference toRet;*/
        newModifError = false;
        
        
        
        String desCaseModification=caseDescriptionParam;
        if(String.isEmpty(desCaseModification))
            desCaseModification='Modification order';
        
        System.debug('&&nueva Modificación '+desCaseModification);
        //Calling JS_RemoteMethods
        // Al método NE.JS_RemoteMethods.requestChangeAssetItem hay que pasarle el id del orderItem
        String returnPage = NE.JS_RemoteMethods.requestChangeAssetItem(detailIdNewModification, 'ChangeOrder');
        
        // If is testing, returnPage = null because JS_RemoteMethods cannot be called, so returnPage is mocked
        if(Test.isRunningTest()){
            if(detailIdNewModification != null){
                
                //returnPage = '/apex/ne__productconfigurator?fromSummary=true&ordId='+detailIdNewModification+'&rootItemVId=' + detailIdNewModification;
                returnPage = '/apex/ne__productconfigurator?fromSummary=true&ordId='+orderIdForTest+'&rootItemVId=' + orderIdForTest;
            }
        }
        
        if (returnPage != 'KO') {
            system.debug('NE.JS_RemoteMethods.requestChangeAssetItem devuelve OK');
            /*NE__OrderItem__c[] selectedCIS = TGS_Portal_Utils.getConfigurationItemsBySuId(detailIdNewModification); //[SELECT NE__Status__c FROM NE__OrderItem__c WHERE Id = :selectedServiceUnit];

if (selectedCIS.size() > 0) {
currentOI = selectedCIS[0];
currentOI.NE__Status__c = 'In progress';
update currentOI;
}*/
            String parameters = returnPage.substringAfter('?');
            String orderIdParameter = parameters.split('&').get(1);
            String orderId = orderIdParameter.split('=').get(1);
            
            System.debug('[PCA_Installed_Services_Controller.newModification] orderId: ' + orderId);
            NE__Order__c configuration = TGS_Portal_Utils.getOrder(orderId);
            
            /*toRet = new PageReference('/PCA_Order_Detail?caseId=' + configuration.Case__r.Id);*/
            newModifError = false;
            
            if((attachmentModificationList != null && attachmentModificationList.size() > 0) || desCaseModification!='Modification order'){
                crearWorkinfo(configuration.Case__r.Id, desCaseModification, attachmentModificationList);
            }
            
            
            caseID=configuration.Case__r.CaseNumber;
            Case createdCase = [SELECT Id, TGS_Customer_Services__c, Description FROM Case WHERE Id =: configuration.Case__c];
            createdCase.TGS_Customer_Services__c= detailIdNewModification;
            createdCase.Description = desCaseModification;
            update createdCase;
            system.debug('caso despues de ser actualizado   '+ createdCase);
        }else{
            /*toRet = ApexPages.currentPage();
toRet.getParameters().put('error', 'The modification order has not been created');*/
            newModifError = true;
        }
        return caseID;
        /*return toRet;*/
    }
    
    /*
Developer Everis
Function: method used to create a work info needed for the case 
*/ 
    public void crearWorkinfo(Id id, String description, List<Attachment> listToInsertParam) {
        Attachment newAttachmentLocal;
        list<TGS_Work_Info__c> wfList = new list<TGS_Work_Info__c>();
        wiMap = new Map<string,id>();  
        
        for(Attachment ad:listToInsertParam){
            String workDescription = ad.Name;
            if(ad.Description != null && ad.Description != ''){
                workDescription = ad.Description;
            }
            wfList.add(new TGS_Work_Info__c(
                TGS_Description__c =  workDescription,
                TGS_Case__c = id,                   
                TGS_Public__c = true,
                TGS_Contains_attachments__c = true
            ));
        }
        
        // TGS_CallRodWs.inFutureContextWI = true;
        insert wfList;
        // TGS_CallRodWs.inFutureContextWI = false;
        
        system.debug('se ha creado un nuevo workInfo para crearle los adjuntos  ' + wfList);
        
        integer wiCont = 0;
        String regExp = '[^a-zA-Z0-9]';
        List<String> attNameWOPoints = new List<String>();
        String attNameFormated ;        
        for(Attachment ad:listToInsertParam){
            attNameWOPoints = ad.name.split('\\.');
            attNameFormated = attNameWOPoints.get(0).replaceAll(regExp, '') + '.' + attNameWOPoints.get(1);
            wiMap.put(attNameFormated,wfList[wiCont].Id);
            wiCont ++;          
        }
        system.debug('wiMap:  ' + wiMap);
        Cache.Session.put('local.CWPexcelExport.wiMap'+userInfo.getUserId(), wiMap);
        
        system.debug('POST wiMap');
        
    }
    
}