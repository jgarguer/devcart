/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:          
* Description:         
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-08-19      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_Btn_Generate_MS_ctr
*            1.1	2016-12-28		Gawron, Julián E. 		Add @testStatus to async funct 
*			 1.2	2017-1-17		Pedro Párraga			Method call crearData()
*			 1.3    23-02-2017      Jaime Regidor           Adding Campaign Values, Adding Catalog Country
					13/03/2017		Marta Gonzalez(Everis)	REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)		
					20/09/2017      Angel F. Santaliestra   Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest
private class BI_COL_GenerateNewBilling_tst
{

	Public static User 										objUsuario;
	public static BI_COL_GenerateNewBilling_tst				objGenerMS;
	public static Account 									objCuenta;
	public static Account 									objCuenta2;
	public static Contact 									objContacto;
	public static Campaign 									objCampana;
	public static Contract 									objContrato;
	public static BI_COL_Anexos__c 							objAnexos;
	public static Opportunity 								objOportunidad;
	public static BI_Col_Ciudades__c 						objCiudad;
	public static BI_Sede__c 								objSede;
	public static BI_Punto_de_instalacion__c 				objPuntosInsta;
	public static BI_COL_Descripcion_de_servicio__c 		objDesSer;
	public static BI_COL_Modificacion_de_Servicio__c 		objMS;
	public static BI_COL_Cobro_Parcial__c 					objCobroParcial;
	
	public static List <Profile> 							lstPerfil;
	public static List <User>	 							lstUsuarios;
	public static List <UserRole> 							lstRoles;
	public static List<Campaign> 							lstCampana;
	public static List<BI_COL_manage_cons__c > 				lstManege;
	public static List<BI_COL_Modificacion_de_Servicio__c>	lstMS;
	public static List<BI_COL_Descripcion_de_servicio__c>	lstDS;
	public static List<recordType>							lstRecTyp;
	
	public static void crearData ()
	{
		//perfiles
		lstPerfil               		= [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
		system.debug('datos Perfil '+lstPerfil);
	
		//lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        objUsuario = BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);
        System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
			insert objUsuario;
		}

		System.runAs(objUsuario)
		{		
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass;
			
			//Roles
			lstRoles                = new list <UserRole>();
			lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
			system.debug('datos Rol '+lstRoles);
			
			//Cuentas
			objCuenta 										= new Account();      
			objCuenta.Name                                  = 'prueba';
			objCuenta.BI_Country__c                         = 'Argentina';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Fraude__c                          = false;
			objCuenta.BI_Segment__c                         = 'test';
			objCuenta.BI_Subsegment_Regional__c             = 'test';
			objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			system.debug('datos Cuenta '+objCuenta);

			objCuenta2                                       = new Account();
			objCuenta2.Name                                  = 'prueba';
			objCuenta2.BI_Country__c                         = 'Argentina';
			objCuenta2.TGS_Region__c                         = 'América';
			objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta2.CurrencyIsoCode                       = 'COP';
			objCuenta2.BI_Fraude__c                          = true;
			objCuenta2.BI_Segment__c                         = 'test';
			objCuenta2.BI_Subsegment_Regional__c             = 'test';
			objCuenta2.BI_Territory__c                       = 'test';
			insert objCuenta2;
			
			//Contactos
			objContacto                                     = new Contact();
			objContacto                                     = new Contact();
			objContacto.LastName                        	= 'Test';
			objContacto.BI_Country__c             		    = 'Argentina';
			objContacto.CurrencyIsoCode           		    = 'COP'; 
			objContacto.AccountId                 		    = objCuenta.Id;
			objContacto.BI_Tipo_de_contacto__c				= 'Administrador Canal Online';
			objContacto.Phone 								= '1234567';
			objContacto.MobilePhone							= '31047859';
			objContacto.Email								= 'pruebas@pruebas1.com';
			objContacto.Title								= 'pruebas';
            //REING-INI
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
            //REING_FIN
			Insert objContacto;
			
			//catalogo
			NE__Catalog__c objCatalogo 						= new NE__Catalog__c();
			objCatalogo.Name 								= 'prueba Catalogo';
			objCatalogo.BI_country__c                       = 'Argentina';
			insert objCatalogo; 
			
			//Campaña
			objCampana 										= new Campaign();
			objCampana.Name 								= 'Campaña Prueba';
			objCampana.BI_Catalogo__c 						= objCatalogo.Id;
			objCampana.BI_COL_Codigo_campana__c 			= 'prueba1';
			objCampana.BI_Country__c                        = 'Argentina';
        	objCampana.BI_Opportunity_Type__c               = 'Fijo';
        	objCampana.BI_SIMP_Opportunity_Type__c          = 'Alta';
			insert objCampana;
			lstCampana 										= new List<Campaign>();
			lstCampana 										= [select Id, Name from Campaign where Id =: objCampana.Id];
			system.debug('datos Cuenta '+lstCampana);
			
			//Tipo de registro
			lstRecTyp 										= [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
			system.debug('datos de FUN '+lstRecTyp);
			
			//Contrato
			objContrato 									= new Contract ();
			objContrato.AccountId 							= objCuenta.Id;
			objContrato.StartDate 							=  System.today().addDays(+5);
			objContrato.BI_COL_Formato_Tipo__c 				= 'Contrato Marco';
			objContrato.ContractTerm 						= 12;
			objContrato.BI_COL_Presupuesto_contrato__c 		= 1000000;
			objContrato.StartDate							= System.today().addDays(+5);
			insert objContrato;
			System.debug('datos Contratos ===>'+objContrato);
			
			//FUN
			objAnexos               						= new BI_COL_Anexos__c();
			objAnexos.Name          						= 'FUN-0041414';
			objAnexos.RecordTypeId  						= lstRecTyp[0].Id;
			objAnexos.BI_COL_Contrato__c   					= objContrato.Id;
			insert objAnexos;
			System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
			
			//Configuracion Personalizada
			BI_COL_manage_cons__c objConPer 				= new BI_COL_manage_cons__c();
			objConPer.Name                  				= 'Viabilidades';
			objConPer.BI_COL_Numero_Viabilidad__c 			= 46;
			objConPer.BI_COL_ConsProyecto__c     			= 1;
			objConPer.BI_COL_ValorConsecutivo__c  			=1;
			lstManege                      					= new list<BI_COL_manage_cons__c >();        
			lstManege.add(objConPer);
			insert lstManege;
			System.debug('======= configuracion personalizada ======= '+lstManege);
			
			//Oportunidad
			objOportunidad                                      	= new Opportunity();
			objOportunidad.Name                                   	= 'prueba opp';
			objOportunidad.AccountId                              	= objCuenta.Id;
			objOportunidad.BI_Country__c                          	= 'Argentina';
			objOportunidad.CloseDate                              	= System.today().addDays(+5);
			objOportunidad.StageName                              	= 'F5 - Solution Definition';
			objOportunidad.CurrencyIsoCode                        	= 'COP';
			objOportunidad.Certa_SCP__contract_duration_months__c 	= 12;
			objOportunidad.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
			objOportunidad.OwnerId                                	= objUsuario.id;
			insert objOportunidad;
			system.debug('Datos Oportunidad'+objOportunidad);
			list<Opportunity> lstOportunidad 	= new list<Opportunity>();
			lstOportunidad 						= [select Id from Opportunity where Id =: objOportunidad.Id];
			system.debug('datos ListaOportunida '+lstOportunidad);
			
			//Ciudad
			objCiudad = new BI_Col_Ciudades__c ();
			objCiudad.Name 						= 'Test City';
			objCiudad.BI_COL_Pais__c 			= 'Test Country';
			objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
			insert objCiudad;
			System.debug('Datos Ciudad ===> '+objSede);
			
			//Direccion
			objSede 								= new BI_Sede__c();
			objSede.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
			objSede.BI_Direccion__c 				= 'Test Street 123 Number 321';
			objSede.BI_Localidad__c 				= 'Test Local';
			objSede.BI_COL_Estado_callejero__c 		= System.Label.BI_COL_LbValor3EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c 		= 'Libre';
			objSede.BI_Country__c 					= 'Colombia';
			objSede.Name 							= 'Test Street 123 Number 321, Test Local Colombia';
			objSede.BI_Codigo_postal__c 			= '12356';
			insert objSede;
			System.debug('Datos Sedes ===> '+objSede);
			
			//Sede
			objPuntosInsta 						= new BI_Punto_de_instalacion__c ();
			objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
			objPuntosInsta.BI_Sede__c          = objSede.Id;
			objPuntosInsta.BI_Contacto__c      = objContacto.id;
			objPuntosInsta.Name 				='Prueba sucursal';
			insert objPuntosInsta;
			System.debug('Datos Sucursales ===> '+objPuntosInsta);
			
			//DS
			objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
			objDesSer.CurrencyIsoCode               			= 'COP';
			insert objDesSer;
			System.debug('Data DS ====> '+ objDesSer);
			System.debug('Data DS ====> '+ objDesSer.Name);
			lstDS 	= [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
			System.debug('Data lista DS ====> '+ lstDS);
			
			//MS
			objMS                         				= new BI_COL_Modificacion_de_Servicio__c();
			objMS.BI_COL_FUN__c           				= objAnexos.Id;
			objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
			objMS.BI_COL_Clasificacion_Servicio__c  	= 'ALTA';
			objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c 					= false;
			objMS.BI_COL_Estado__c 						= 'Activa';//label.BI_COL_lblActiva;
			objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
			insert objMS;
			system.debug('Data objMs ===>'+objMs);
			lstMS = [select Name, BI_COL_Codigo_unico_servicio__c,BI_COL_Producto__r.Name,BI_COL_Descripcion_Referencia__c,
							BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Sucursal_de_Facturacion__c, BI_COL_Codigo_unico_servicio__r.BI_COL_Sede_Destino__c, 
							BI_COL_Sucursal_Origen__c, BI_COL_Cuenta_facturar_davox__c from BI_COL_Modificacion_de_Servicio__c
					];
			System.debug('datos lista MS ===> '+lstMS);
			objCobroParcial = new BI_COL_Cobro_Parcial__c();
			objCobroParcial.BI_COL_Fecha_envio__c=System.today();
			objCobroParcial.BI_COL_MS__c=lstMS[0].id;
			objCobroParcial.BI_COL_No_Renovable__c=true;
			objCobroParcial.BI_COL_Procesado__c=false;
			objCobroParcial.BI_COL_Renovable__c=true;
			objCobroParcial.BI_COL_Valor_Cobro__c=3400;
			insert objCobroParcial;
						 					
		}
	}
	
	public static testMethod void test_method_one()
	{
		
		Test.startTest();
			crearData();
			//objOpportunity.Delete__c = true;
			//insert objOpportunity;

			/*String CRON_EXP = '0 0 0 1 1 ? 2025';  
			String jobId = System.schedule('testScheduledApex', CRON_EXP, new BI_COL_ScheduleSendDavox_cls() );

			CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];

			System.assertEquals(CRON_EXP, ct.CronExpression); 
			System.assertEquals(0, ct.TimesTriggered);
			System.assertEquals('2025-01-01 00:00:00', String.valueOf(ct.NextFireTime));*/
			BI_COL_GenerateNewBilling_cls obj=new BI_COL_GenerateNewBilling_cls(false);
			List<BI_COL_Cobro_Parcial__c> lst=[SELECT BI_COL_Fecha_envio__c, BI_COL_MS__c, BI_COL_No_Renovable__c, BI_COL_Procesado__c, BI_COL_Renovable__c, BI_COL_Valor_Cobro__c, Name FROM BI_COL_Cobro_Parcial__c];
			System.assertEquals(lst.size() > 0, true); 
			Id BatchProcessId = Database.executeBatch(obj, 1);
			

		Test.stopTest();

	}
}