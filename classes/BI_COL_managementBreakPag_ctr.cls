/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-04-21      Manuel Esthiben Mendez Devia (MEMD)     Cloned Controller
*************************************************************************************************************/
public with sharing class BI_COL_managementBreakPag_ctr 
{

	public String strIdResponsable{get;set;}
	public List<BI_COL_Seguimiento_Quiebre__c> lstQui{get;set;}
	public boolean todosMarcados{get;set;}
	public List<WrapperGestion> listaQuiebres{get;set;}
	public List<WrapperGestion> lstImpresion{get;set;}
	
	public Integer pageSize{get;set;}
	public Integer pageNumber{get;set;}
	private Integer totalPageNumber{get;set;}
	
	
	//--------------------------->>>> ACA VOY
	
	public List<SelectOption> listaResponsables{
	 	get{
	 		BI_COL_PerfilesUsuario__c perf=BI_COL_PerfilesUsuario__c.getInstance('Ingeniero');
	 		
	 		 List<SelectOption> lst= new List<SelectOption>();
	 		 List<User> lstUsuario = new List<User>();

	 		 if(!Test.isRunningTest())
	 		 {
	 		 	lstUsuario=[select Id, FirstName, LastName, Name from User where ProfileId =: perf.BI_COL_idPerfil__c and IsActive=true order by Name];
	 		 }else{
	 		 	lstUsuario=[select Id, FirstName, LastName, Name from User where IsActive=true order by Name];
	 		 }
	 		 
	 		 if(lstUsuario.size()>0){
                lst.add( new SelectOption('', '------------' ) );                
                for(User ac: lstUsuario){
                	if(ac.FirstName!=null)
                    	lst.add( new SelectOption( ac.Id, ac.Name ) );
                }
             }
	 		 
	 		 return lst;
	 	}set;
	}
	
	/*** método que se encarga de seleccionar todos los registros que se encuentran en la lista **/
	public void action_seleccionarTodos(){
		for(WrapperGestion wrap:lstImpresion){			
			wrap.seleccionado=todosMarcados;
		}
		
	}
		
	/*************************************** métodos para paginación *************************************/
	private void BindData(Integer newPageIndex){
		try{
			lstImpresion = new List<WrapperGestion>();
			Transient Integer counter = 0;
			Transient Integer min = 0;
			Transient Integer max = 0;
			
			if (newPageIndex > pageNumber){
				min = pageNumber * pageSize;
				max = newPageIndex * pageSize;
			}else{
				max = newPageIndex * pageSize;
				min = max - pageSize;
			}
			
			for(WrapperGestion wrap : listaQuiebres){
				counter++;
				if (counter > min && counter <= max){
					lstImpresion.add(wrap);
				}
					
			}
			pageNumber = newPageIndex;
			
			if (lstImpresion == null || lstImpresion.size() <= 0)
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Datos no disponibles'));
		}
		catch(Exception ex){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
		}	
	}
	
	
	public Boolean getPreviousButtonEnabled(){
		return !(pageNumber > 1);
	}
	
	public Boolean getNextButtonDisabled(){
		if (lstImpresion == null){	return true;}
		else{return ((pageNumber * pageSize) >= listaQuiebres.size());}
	
	}
	
	public Integer getTotalPageNumber()	{
		if (totalPageNumber == 0 && listaQuiebres !=null){
			totalPageNumber = listaQuiebres.size() / pageSize;
			Integer mod = listaQuiebres.size() - (totalPageNumber * pageSize);
			if (mod > 0)
			totalPageNumber++;
		}
		return totalPageNumber;
	}
	
		public PageReference nextBtnClick() {
			BindData(pageNumber + 1);
		return null;
	
	}
	
	
	public PageReference previousBtnClick() {
			BindData(pageNumber - 1);
		return null;
	
	}
	
	/***************************************************************************************************/
	
	
	/************************** constructor *****************************/ 
	public BI_COL_managementBreakPag_ctr(ApexPages.StandardController cont){
		pageNumber=1;
		pageSize=50;
		totalPageNumber=0;
		consultaRegistros();
		BindData(pageNumber);
		
	}
	/********************************************************************/
	
	/************** método encargado de consutlar los registros de seguimiento de quiebres y crear la lista del Wrapper ********************/
	public void consultaRegistros(){
		listaQuiebres=new List<WrapperGestion>();
		WrapperGestion wrap=null;
		lstQui=[select Id,BI_COL_Modificacion_de_Servicio__r.Name,BI_COL_Modificacion_de_Servicio__r.BI_COL_Oportunidad__r.Account.Name,Owner.FirstName,Owner.LastName,
		BI_COL_Causal_quiebre_inicial__c from BI_COL_Seguimiento_Quiebre__c  where OwnerId=:userInfo.getUserId() order by Id];
		if(lstQui.size()>0){
			for(BI_COL_Seguimiento_Quiebre__c sq:lstQui){
				wrap=new WrapperGestion(false,sq);
				listaQuiebres.add(wrap);
			}
			todosMarcados=false;			
		}
	}
	/****************************************************************************************************************************************/
		
	/********* Método encargado de hacer la actualización de los registros de Quiebres ********/
	public void modificarResponsable(){
		List<BI_COL_Seguimiento_Quiebre__c> lstActualizar=new List<BI_COL_Seguimiento_Quiebre__c>();
		system.debug('-------->asignando el propietario = '+strIdResponsable);
		 if(strIdResponsable!=null && !strIdResponsable.equals('')){
		 	
			 for(WrapperGestion wrap:lstImpresion){
			 	if(wrap.seleccionado){
			 		wrap.quiebre.OwnerId=strIdResponsable;
			 		lstActualizar.add(wrap.quiebre);
			 	}
			 }
			 if(lstActualizar.size()>0){
			 	update lstActualizar;
			 	consultaRegistros();
			 	BindData(pageNumber);
			 	strIdResponsable='';
			 	todosMarcados=false;
			 }
			 else
			 	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Debe seleccionar al menos un registro para actualizar'));
		 }else{
		 	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Debe seleccionar un responsable para asignar a los registros seleccionados'));
		 }
	}
	/********************************/
	
	public class WrapperGestion{
		
		public boolean seleccionado{get;set;}
		public BI_COL_Seguimiento_Quiebre__c quiebre{get;set;}
		
		public WrapperGestion(boolean seleccionado, BI_COL_Seguimiento_Quiebre__c quiebre){
			this.seleccionado=seleccionado;
			this.quiebre=quiebre;
		}
		
	}

}