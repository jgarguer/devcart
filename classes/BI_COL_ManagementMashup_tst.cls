/**
* Avanxo Colombia
* @author           Geraldine Sofía Pérez Montes href=<gperez@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class 
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-08-20      GSPM                    Test Class created 
*            1.1    23-02-2017      Jaime Regidor           Adding Campaign Values, Adding Catalog Country 
*					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
*                   20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/

@isTest
private class BI_COL_ManagementMashup_tst {

    public static User                                      objUsuario;
    public static Account                                   objCuenta;
    public static Account                                   objCuenta2;
    public static Contact                                   objContacto;
    public static Campaign                                  objCampana;
    public static Contract                                  objContrato;
    public static BI_COL_Anexos__c                          objAnexos;
    public static Opportunity                               objOportunidad;
    public static BI_Col_Ciudades__c                        objCiudad;
    public static BI_Sede__c                                objSede;
    public static BI_Punto_de_instalacion__c                objPuntosInsta;
    public static BI_COL_Descripcion_de_servicio__c         objDesSer;
    public static BI_COL_Modificacion_de_Servicio__c        objMS;
    public static BI_COL_Generate_OT_ctr.WrapperMS          objWrapperMS;
    
    public static List <Profile>                            lstPerfil;
    public static List <User>                               lstUsuarios;
    public static List <UserRole>                           lstRoles;
    public static List<Campaign>                            lstCampana;
    public static List<BI_COL_manage_cons__c >              lstManege;
    public static List<BI_COL_Modificacion_de_Servicio__c>  lstMS;
    public static List<BI_COL_Descripcion_de_servicio__c>   lstDS;
    public static List<recordType>                          lstRecTyp;
    public static List<BI_COL_Generate_OT_ctr.WrapperMS>    lstWrapperMS;
    public static List<BI_COL_Anexos__c>                    lstAnexos;

        public static void CrearData() 
    {

        ////perfiles
        //lstPerfil                         = new List <Profile>();
        //lstPerfil                     = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
        //system.debug('datos Perfil '+lstPerfil);
                
        ////usuarios
        ////lstUsuarios = [Select Id FROM User where Pais__c = 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

        ////lstRol
  //      lstRoles = new list <UserRole>();
  //      lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

  //      //ObjUsuario
  //      objUsuario = new User();
  //      objUsuario.Alias = 'standt';
  //      objUsuario.Email ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey = '';
  //      objUsuario.LastName ='Testing';
  //      objUsuario.LanguageLocaleKey ='en_US';
  //      objUsuario.LocaleSidKey ='en_US'; 
  //      objUsuario.ProfileId = lstPerfil.get(0).Id;
  //      objUsuario.TimeZoneSidKey ='America/Los_Angeles';
  //      objUsuario.UserName ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey ='UTF-8';
  //      objUsuario.UserRoleId = lstRoles.get(0).Id;
  //      objUsuario.BI_Permisos__c ='Sucursales';
  //      objUsuario.Pais__c='Colombia';
  //      insert objUsuario;
        
  //      lstUsuarios = new list<User>();
  //      lstUsuarios.add(objUsuario);

            ////Roles
            //lstRoles                = new List <UserRole>();
            //lstRoles                = [Select id,Name from UserRole where Name = 'Telefónica Global'];
            //system.debug('datos Rol '+lstRoles);

            BI_bypass__c objBibypass = new BI_bypass__c();
            objBibypass.BI_migration__c=true;
            insert objBibypass;
            
            //Cuentas
            objCuenta                                       = new Account();      
            objCuenta.Name                                  = 'prueba';
            objCuenta.BI_Country__c                         = 'Colombia';
            objCuenta.TGS_Region__c                         = 'América';
            objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
            objCuenta.CurrencyIsoCode                       = 'COP';
            objCuenta.BI_Fraude__c                          = false;
            objCuenta.AccountNumber                         = '123456';
            objCuenta.BI_No_Identificador_fiscal__c         = '1234566';
            objCuenta.Phone                                 = '1233421';
            objCuenta.BI_Segment__c                         = 'test';
            objCuenta.BI_Subsegment_Regional__c             = 'test';
            objCuenta.BI_Territory__c                       = 'test';
            insert objCuenta;
            system.debug('datos Cuenta '+objCuenta);

            //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);
            
            //Contactos
            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
            objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objCuenta.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
            objContacto.Phone                               = '12345678';
            objContacto.MobilePhone                         = '3104785925';
            objContacto.Email                               = 'pruebas@pruebas1.com';
            objContacto.BI_COL_Direccion_oficina__c         = 'Dirprueba';
            objContacto.BI_COL_Ciudad_Depto_contacto__c     = objCiudad.Id;
            //REING-INI
            //objContacto.BI_Tipo_de_contacto__c          = 'Autorizado';
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
       		objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
            objContacto.BI_Tipo_de_contacto__c          = 'General';
       		//REING_FIN

            Insert objContacto;
            
            //catalogo
            NE__Catalog__c objCatalogo                      = new NE__Catalog__c();
            objCatalogo.Name                                = 'prueba Catalogo';
            objCatalogo.BI_country__c                       = 'Colombia';
            insert objCatalogo; 
            
            //Campaña
            objCampana                                      = new Campaign();
            objCampana.Name                                 = 'Campaña Prueba';
            objCampana.BI_Catalogo__c                       = objCatalogo.Id;
            objCampana.BI_COL_Codigo_campana__c             = 'prueba1';
            objCampana.BI_Country__c                        = 'Colombia';
            objCampana.BI_Opportunity_Type__c               = 'Fijo';
            objCampana.BI_SIMP_Opportunity_Type__c          = 'Alta';
            insert objCampana;
            lstCampana                                      = new List<Campaign>();
            lstCampana                                      = [select Id, Name from Campaign where Id =: objCampana.Id];
            system.debug('datos Cuenta '+lstCampana);
            
            //Tipo de registro
            lstRecTyp                                       = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
            system.debug('datos de FUN '+lstRecTyp);
            
            //Contrato
            objContrato                                     = new Contract ();
            objContrato.AccountId                           = objCuenta.Id;
            objContrato.StartDate                           =  System.today().addDays(+5);
            objContrato.BI_COL_Formato_Tipo__c              = 'Condiciones Generales';
            objContrato.BI_COL_Presupuesto_contrato__c      = 1300000;
            objContrato.StartDate                           = System.today().addDays(+5);
            objContrato.BI_Indefinido__c                    = 'No';
            objContrato.BI_COL_Monto_ejecutado__c           = 5;
            objContrato.ContractTerm                        = 12;
            objContrato.BI_COL_Duracion_Indefinida__c       = true;
            objContrato.Name                                = 'prueba';
            objContrato.BI_COL_Cuantia_Indeterminada__c     = true;
        
            insert objContrato;
            Contract objCont = [select ContractNumber, BI_COL_Saldo_contrato__c from Contract limit 1];

            System.debug('datos Contratos ===>'+objContrato);
            
            //FUN
            objAnexos                                       = new BI_COL_Anexos__c();
            objAnexos.Name                                  = 'FUN-0041414';
            objAnexos.RecordTypeId                          = lstRecTyp[0].Id;
            objAnexos.BI_COL_Contrato__c                    = objContrato.Id;
            objAnexos.BI_COL_Estado__c                      = 'Activo';

            insert objAnexos;
            System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
            
            //Configuracion Personalizada
            BI_COL_manage_cons__c objConPer                 = new BI_COL_manage_cons__c();
            objConPer.Name                                  = 'Viabilidades';
            objConPer.BI_COL_Numero_Viabilidad__c           = 46;
            objConPer.BI_COL_ConsProyecto__c                = 1;
            objConPer.BI_COL_ValorConsecutivo__c            =1;
            lstManege                                       = new List<BI_COL_manage_cons__c >();        
            lstManege.add(objConPer);
            insert lstManege;
            System.debug('======= configuracion personalizada ======= '+lstManege);
            
            //Oportunidad
            objOportunidad                                          = new Opportunity();
            objOportunidad.Name                                     = 'prueba opp';
            objOportunidad.AccountId                                = objCuenta.Id;
            objOportunidad.BI_Country__c                            = 'Colombia';
            objOportunidad.CloseDate                                = System.today().addDays(+5);
            objOportunidad.StageName                                = 'F6 - Prospecting';
            objOportunidad.CurrencyIsoCode                          = 'COP';
            objOportunidad.Certa_SCP__contract_duration_months__c   = 12;
            objOportunidad.BI_Plazo_estimado_de_provision_dias__c   = 0 ;
            //objOportunidad.OwnerId                                    = objUsuario.id;
            insert objOportunidad;
            system.debug('Datos Oportunidad'+objOportunidad);
            List<Opportunity> lstOportunidad    = new List<Opportunity>();
            lstOportunidad                      = [select Id from Opportunity where Id =: objOportunidad.Id];
            system.debug('datos ListaOportunida '+lstOportunidad);
            
            
            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);
            
            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name                 ='Prueba remedy';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);
            
            //DS
            objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
            objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
            objDesSer.CurrencyIsoCode                           = 'COP';
            insert objDesSer;
            System.debug('Data DS ====> '+ objDesSer);
            System.debug('Data DS ====> '+ objDesSer.Name);
            lstDS   = [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
            System.debug('Data lista DS ====> '+ lstDS);
            
            //MS
            objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
            objMS.BI_COL_FUN__c                         = objAnexos.Id;
            objMS.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
            objMS.BI_COL_Bloqueado__c                   = false;
            objMS.BI_COL_Estado__c                      = 'Activa';//label.BI_COL_lblActiva;
            objMS.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objMs.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            objMs.BI_COL_Medio_Preferido__c             = 'Cobre';
            objMS.BI_COL_Clasificacion_Servicio__c      = 'Alta demo';
            objMS.BI_COL_TRM__c                         = 5;
            insert objMS;
            system.debug('Data objMs ===>'+objMs);
            BI_COL_Modificacion_de_Servicio__c objr = [select BI_COL_Monto_ejecutado__c,BI_COL_Producto__c from BI_COL_Modificacion_de_Servicio__c where id =: objMS.id];
            system.debug('\n@-->Data consulta ===>'+objr);  
        
        
    }
    //Con getParameters
    public static testMethod void consultaCotizacionesTRS1() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        objUsuario.BI_Peru_STC_Per__c = '123456789';
        update objUsuario;
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            pageRef.getParameters().put('Id',objUsuario.id);
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.strClie2 = objCuenta.Id;
            
            objcontroller.strOportunidad = objOportunidad.Id;
            
            objcontroller.consultaCotizacionesTRS();    

            Test.stopTest();
        }
    }

    //Sin getParameters
    public static testMethod void consultaCotizacionesTRS2() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.consultaCotizacionesTRS();        
            Test.stopTest();
        }
    }
    
    public static testMethod void crearCotizacionTRS() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            pageRef.getParameters().put('Id',objUsuario.id);
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.crearCotizacionTRS(); 
            //String cl = objcontroller.strClie2;    
            //String op = objcontroller.strOportunidad;    
            Test.stopTest();
        }
    }

    public static testMethod void crearCotizacionTRS1() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.crearCotizacionTRS();     
            Test.stopTest();
        }
    }

    public static testMethod void productMashup() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            pageRef.getParameters().put('product','EP');
            pageRef.getParameters().put('msInfo',objMS.id);
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.productMashup();      
            Test.stopTest();
        }
    }

    public static testMethod void productMashup1() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            pageRef.getParameters().put('product','MM');
            pageRef.getParameters().put('msInfo',objMS.id);
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.productMashup();      
            Test.stopTest();
        }
    }

    public static testMethod void productMashup2() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            pageRef.getParameters().put('product','SM');
            pageRef.getParameters().put('msInfo',objMS.id);
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.productMashup();      
            Test.stopTest();
        }
    }

    public static testMethod void productMashup3() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            pageRef.getParameters().put('product','TS');
            pageRef.getParameters().put('msInfo',objMS.id);
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.productMashup();      
            Test.stopTest();
        }
    }

    public static testMethod void productMashup4() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            pageRef.getParameters().put('product','ID');
            pageRef.getParameters().put('msInfo',objMS.id);
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.productMashup();      
            Test.stopTest();
        }
    }

    public static testMethod void strEtpOportunidad() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            //BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();    
            BI_COL_ManagementMashup_ctr.strEtpOportunidad('F1 - Cancelled | Suspended');        
            Test.stopTest();
        }
    }

    public static testMethod void strSegCliente() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.strSegCliente('segmento');        
            Test.stopTest();
        }
    }

    public static testMethod void caracteresAleatorios() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.caracteresAleatorios(1,2,true);       
            Test.stopTest();
        }
    }

    public static testMethod void encodingMD5() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            //BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();    
            BI_COL_ManagementMashup_ctr.encodingMD5('cadena');      
            Test.stopTest();
        }
    }

    public static testMethod void getPermisionSetUsuario() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            //BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();    
            BI_COL_ManagementMashup_ctr.getPermisionSetUsuario(objUsuario.id);      
            Test.stopTest();
        }
    }

    public static testMethod void test_method_1() 
    {



        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        objUsuario.BI_Peru_STC_Per__c = '123456789';
        update objUsuario;

        PermissionSet  ps = [select id, Name from PermissionSet where Name = 'BI_COL_Desarrollo_Comercial' limit 1];

        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = objUsuario.Id, PermissionSetId = ps.Id);
        insert psa;

        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            pageRef.getParameters().put('Id',objUsuario.id);
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.strClie2 = objCuenta.Id;
            
            objcontroller.strOportunidad = objOportunidad.Id;
            
            objcontroller.consultaCotizacionesTRS();    

            Test.stopTest();
        }
    }

    public static testMethod void test_method_2() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        objUsuario.BI_Peru_STC_Per__c = '7590298';
        update objUsuario;
        
        System.runAs(objUsuario)
        {
            CrearData();
            PageReference pageRef           = Page.BI_COL_ConsultationPriceQuote_pag;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            pageRef.getParameters().put('Id',objOportunidad.id);
            BI_COL_ManagementMashup_ctr objcontroller = new BI_COL_ManagementMashup_ctr();  
            objcontroller.crearCotizacionTRS(); 
            //String cl = objcontroller.strClie2;    
            //String op = objcontroller.strOportunidad;    
            Test.stopTest();
        }
    }
}