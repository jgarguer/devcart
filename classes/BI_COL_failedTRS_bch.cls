/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       30-Abr-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
global class BI_COL_failedTRS_bch implements Schedulable
{
    public Boolean tst{get;set;}
    
    global BI_COL_failedTRS_bch( Boolean bndTst )
    {
        tst = bndTst;
    }
    
    global BI_COL_failedTRS_bch()
    {
        
    }
    
    global void execute( SchedulableContext sc ) 
    {
        this.EliminarSch();
        
        List<BI_Log__c> listLogTrans = [
            Select  CreatedDate, Id 
            from    BI_Log__c
            where   CreatedDate < LAST_N_DAYS:60 
            Limit 9500];
        
        if( listLogTrans.size() > 0)
        {
            try { delete listLogTrans; }catch(Exception ex){}
        }
        
        List<BI_Log__c> lstNovFact = [
            Select  CreatedDate, BI_COL_Estado__c, Id 
            from    BI_Log__c
            where   BI_COL_Estado__c = 'Cerrado' 
            and     CreatedDate < LAST_N_DAYS:60];
        
        if( lstNovFact.size() > 0 )
        {
            try {delete lstNovFact;}catch(Exception ex){}
        }
        
        List<BI_COL_Modificacion_de_Servicio__c> msFallidasConx = [
            select  Id, Name, BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Estado_orden_trabajo__c 
            from    BI_COL_Modificacion_de_Servicio__c 
            where   BI_COL_Estado_orden_trabajo__c = 'Fallido Conexion'];
        
        List<String> lstIdMS = new List<String>();
        
        for( BI_COL_Modificacion_de_Servicio__c ms : msFallidasConx )
            lstIdMS.add( ms.Id );
        
        BI_COL_Contract_cls.motoEjecutado( new set<string>( lstIdMS ) );
        
        envioDatosSISGOT( lstIdMS );
        
        this.EliminarSch();
    }
    
    public void EliminarSch()
    {
        List<Crontrigger> a = [
            Select  Id, state 
            From    CronTrigger 
            where state = 'DELETED'];
        
        for( CronTrigger ct : a )
            System.abortjob( ct.Id );
    }
    
    @Future (callout=true)
    public static void envioDatosSISGOT(List<String> lstIdMS)
    {
        List<BI_COL_Modificacion_de_Servicio__c> lstMS = [
            select  Id, Name, BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Estado_orden_trabajo__c
            from    BI_COL_Modificacion_de_Servicio__c 
            where   Id In: lstIdMS ];
        //Ejecutar Servicio Web de OT
        try
        {
            BI_COL_InterfaceSISGOTR_cls.EnviarDatosBasicosDS( lstMS );
        }
        catch(Exception e)
        {
            System.debug(':: Se presento un error generando las ordenes de trabajo: '+e.getMessage());
        }
    }
}