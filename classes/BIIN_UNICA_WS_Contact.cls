/*----------------------------------------------------------------------------------------------------------------------
	Author:        Julio Laplaza
	Company:       HPE
	Description:   

	<Date>                 <Author>								<Change Description>
	25/05/2016             Julio Laplaza						Initial Version
	01/07/2016             José Luis González Beltrán      		Fix update URL
----------------------------------------------------------------------------------------------------------------------*/

public class BIIN_UNICA_WS_Contact extends BIIN_UNICA_Base
{
	// Global variables
	static final String url = 'callout:BIIN_WS_UNICA/accountresources/v1/contacts';
	
	// Constructor
	public BIIN_UNICA_WS_Contact() 
	{
	}
	
	public void createAuthorizedUser(Contact contact) 
	{
		BIIN_UNICA_Pojos.ContactDetailsType contactDetails = convertData(null, contact);
		
		// Call REST service
		HttpRequest req = getRequest(url, 'POST', contactDetails);
		if(!Test.isRunningTest())
		{
			BIIN_UNICA_Pojos.ResponseObject resObject = (BIIN_UNICA_Pojos.ResponseObject) getResponse(req, null);
		}
	}

	public void updateAuthorizedUser(Contact contactOld, Contact contactNew) 
	{
		BIIN_UNICA_Pojos.ContactDetailsType contactDetails = convertData(contactOld, contactNew);

		String originalEmail = String.isEmpty(contactOld.Email) ? contactNew.Email : contactOld.Email;

		// Call REST service
		HttpRequest req = getRequest(url + '/' + originalEmail, 'PUT', contactDetails);
		if(!Test.isRunningTest())
		{
			BIIN_UNICA_Pojos.ResponseObject resObject = (BIIN_UNICA_Pojos.ResponseObject) getResponse(req, null);
		}
	}

	private BIIN_UNICA_Pojos.ContactDetailsType convertData(Contact contactOld, Contact contactNew)
	{
		BIIN_UNICA_Pojos.ContactDetailsType contactDetails = new BIIN_UNICA_Pojos.ContactDetailsType();

		contactDetails.customers = new List<String>();
		System.debug(' BIIN_UNICA_WS_Contact.convertData | contactNew: ' + contactNew);
		List<Contact> contacts = [SELECT AccountId FROM Contact WHERE Id =: contactNew.Id];
		System.debug(' BIIN_UNICA_WS_Contact.convertData | contacts: ' + contacts);
		String accId = !contacts.isEmpty() ? contacts.get(0).AccountId : null;

		List<Account> accs = [SELECT BI_Tipo_de_identificador_fiscal__c, BI_Validador_Fiscal__c, BI_Country__c FROM Account WHERE Id =: accId];
		System.debug(' BIIN_UNICA_WS_Contact.convertData | accs: ' + accs);
		contactDetails.customers.add(!accs.isEmpty() ? accs.get(0).BI_Validador_Fiscal__c : '');

		/*
		contactDetails.legalId = new List<BIIN_UNICA_Pojos.IdentificationType>();
		BIIN_UNICA_Pojos.IdentificationType legalId = new BIIN_UNICA_Pojos.IdentificationType();
		legalId.country = !accs.isEmpty() ? accs.get(0).BI_Country__c : '';
		legalId.nationalIDType = !accs.isEmpty() ? accs.get(0).BI_Tipo_de_identificador_fiscal__c : '';
		legalId.nationalID = !accs.isEmpty() ? accs.get(0).BI_Validador_Fiscal__c : '';
		contactDetails.legalId.add(legalId);
		*/

		contactDetails.name = new BIIN_UNICA_Pojos.NameDetailsType();
		contactDetails.name.givenName = !String.isEmpty(contactNew.FirstName) ? contactNew.FirstName : 'Nombre-Vacio';
		contactDetails.name.familyName = contactNew.LastName;
		contactDetails.name.fullName = contactNew.Name;
		contactDetails.contactStatus =  BIIN_UNICA_Utils.stringToEnumValueForContactStatusType(contactNew.Estado__c);
		contactDetails.contactingModes = new List<BIIN_UNICA_Pojos.ContactingModeType>{};
		
		// Email
		BIIN_UNICA_Pojos.ContactingModeType contactingMode = new BIIN_UNICA_Pojos.ContactingModeType();
		contactingMode.isPreferred = true;
		contactingMode.contactMode = 'email';
		contactingMode.details = contactNew.Email;
		contactDetails.contactingModes.add(contactingMode);

		// Additional Data
		contactDetails.additionalData = new List<BIIN_UNICA_Pojos.KeyValueType>(); 
		if (contactOld != null && contactOld.email != null)
		{
			if (contactOld.Email != contactNew.Email)
				contactDetails.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('ad_old_value_for_email', contactOld.Email));
		}

		BI_Punto_de_instalacion__c[] sede = [SELECT Id, Name, BI_Sede__c, Provincia__c, Pais__c FROM BI_Punto_de_instalacion__c 
																					WHERE BI_Contacto__c =: contactNew.Id LIMIT 1];

		System.debug(' BIIN_UNICA_WS_Contact.convertData | sede: ' + sede);

		if (!sede.isEmpty())
		{
			BI_Sede__c[] street = [SELECT Name FROM BI_Sede__c WHERE Id =: sede[0].BI_Sede__c LIMIT 1];
			String ad_Street = street[0].Name != null ? street[0].Name : '';
			contactDetails.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('ad_Site', sede[0].Name));
			contactDetails.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('ad_City', sede[0].Provincia__c));
			contactDetails.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('ad_CountrySite', sede[0].Pais__c));
			contactDetails.additionalData.add(new BIIN_UNICA_Pojos.KeyValueType('ad_Street', ad_Street));
		}

		System.debug(' BIIN_UNICA_WS_Contact.convertData | contactDetails: ' + contactDetails);
		return contactDetails;
	}
}