public class PCA_CaseCommentMethods {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Methods executed by CaseComment Triggers 
    
    History:
    
    <Date>					  <Author> 	       		 <Description>
    19/08/2014 				  Ana Escrich			Initial Version
  	28/09/2015				  Francisco Ayllon		Aggregated isPublished = true condition on casecomment query 
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ana Escrich Acero
	    Company:       Salesforce.com
	    Description:   Method that send email to the owner case of case comment
	    
    	History: 
	    
	    <Date>                  <Author>                <Change Description>
  	    21/07/2014              Ana Escrich Acero       Initial Version
  	    28/09/2015				Francisco Ayllon		Aggregated isPublished = true condition on casecomment query
  	    06/09/2016				Antonio Pardo 			Deprecated     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*public static void sendEmail(List <CaseComment> comments){
    	try{
	    	system.debug('comments: '+comments);
	    	List<Id> cases = new List<Id>();
	    	for(CaseComment item: comments){
	    		cases.add(item.Id);
	    	}
	    	//List<Case> casesToSend = [SELECT Id, Origin, Owner.Email from Case where Id in :cases and Origin = :Label.BI_OrigenCasoPortalCliente];
	    	List<CaseComment> casesToSend = [SELECT Id, Parent.Origin, Parent.CaseNumber, Parent.Owner.Email, CommentBody, Parent.OwnerId, Parent.AccountId, Parent.Account.Name, Parent.ContactId from CaseComment where Id in :cases and Parent.Origin = :Label.BI_OrigenCasoPortalCliente AND IsPublished=true];
	    	system.debug('casesToSend*****: ' +casesToSend);
	    	Map<String, Set<String>> emailsToSend = new Map<String, Set<String>>(); 
	    	List<Id> accountIdCases = new List<Id>();
	    	for(CaseComment item: casesToSend){
	    		if(!emailsToSend.containsKey(item.Parent.AccountId)){
	    			system.debug('item.Parent.AccountId' + item.Parent.AccountId);
	    			system.debug('emailsToSend' +emailsToSend);
	    			accountIdCases.add(item.Parent.AccountId);
					emailsToSend.put(item.Parent.AccountId, new Set<String>());		
				}
	    	}
	    	system.debug('accountIdCases: '+accountIdCases);
	    	List<BI_Contact_Customer_Portal__c> portalContacts = [SELECT BI_Activo__c,BI_Cliente__c, BI_Contacto__c, BI_User__c, BI_Contacto__r.Email,BI_User__r.Email FROM BI_Contact_Customer_Portal__c where BI_Cliente__c in :accountIdCases];
	    	system.debug('portalContacts: '+ portalContacts );
	    	for(BI_Contact_Customer_Portal__c item: portalContacts){
	    		if(item.BI_Contacto__c!=null && item.BI_Contacto__r.Email!=null){
	    			if(!emailsToSend.get(item.BI_Cliente__c).contains(item.BI_Contacto__r.Email))
	    				emailsToSend.get(item.BI_Cliente__c).add(item.BI_Contacto__r.Email);
	    		}
	    		if(item.BI_User__c!=null && item.BI_User__r.Email!=null){
	    			if(!emailsToSend.get(item.BI_Cliente__c).contains(item.BI_User__r.Email))
	    				emailsToSend.get(item.BI_Cliente__c).add(item.BI_User__r.Email);
	    		}
	    	}
	    	List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject from EmailTemplate where DeveloperName='BI_Creacion_de_comentariodecaso_portal_platino'];
	    	if(!templates.isEmpty() && !casesToSend.isEmpty()){
		    	system.debug('casesToSend: '+casesToSend);
		    	system.debug('casesToSend: '+casesToSend[0].Parent.Owner.Email);
		    	List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
		    	for(CaseComment item: casesToSend){
			    	String[] toaddress = new String[]{};
			    	for(String address : emailsToSend.get(item.Parent.AccountId)){
		    			toaddress.add(address);
		    		}
					toaddress.add(item.Parent.Owner.Email);
					system.debug('toaddress: '+toaddress);
					Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
			
					//set object Id
					singleMail.saveAsActivity=false;
					singleMail.setToAddresses(toaddress);
					 //set template Id
					singleMail.setTemplateId(templates[0].Id);
					if(item.Parent.ContactId!=null){
						singleMail.setTargetObjectId(item.Parent.ContactId);
					}
					else{
						singleMail.setTargetObjectId(portalContacts[0].BI_Contacto__c);
					}
					singleMail.setWhatId(item.ParentId);
					emails.add(singleMail);
		    		
		    	}
				Messaging.sendEmail(emails);
	    	}
    	}catch(Exception Exc){
    		BI_LogHelper.generate_BILog('PCA_CaseCommentMethods.sendEmail', 'Portal Platino', Exc, 'Class');
    	}
    }*/

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Antonio Pardo
	Company:       Salesforce.com
	Description:   Method that send email to the owner case of case comment
	    
    History: 
	    
	<Date>                  <Author>                <Change Description>
  	05/10/2016				Antonio Pardo 			Initial version
    03/01/2018              Alfonso Alvarez	        Adaptación por migración a licencias Customer Communities
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
     public static void sendEmail(List <CaseComment> comments){
     	
     	try{
	     	//Plantilla que se enviará al propietario del caso
	     	List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject from EmailTemplate where DeveloperName='BI_Creacion_de_comentariodecaso_portal_platino'];
	     	//Si la plantilla no existe no se enviará correo por lo tanto obviamos todo lo demás
	     	if(!templates.isEmpty()){
	     		//Lista con datos necesarios de caseComment
	     		List<CaseComment> lst_comment = [SELECT Id, ParentId, Parent.Contact.Email ,Parent.ContactId , Parent.CaseNumber, Parent.AccountId, Parent.Description, CreatedById FROM CaseComment WHERE Parent.Origin = :Label.BI_OrigenCasoPortalCliente AND Id IN :comments AND isPublished = true];
	     		//Set de contactos
	     		Set<Id> set_contact = new Set<Id>();
	     		for(CaseComment cas : lst_comment){
	     			set_contact.add(cas.Parent.ContactId);
	     		}
	     		//Contacto y su usuario de portal
	     		Map<Id, User> map_contact_user = new Map<Id, User>();
	     		//Lista de usuarios de portal platino
	     		// 03-01-2018 AJAE : Se quita la referencia directa a un perfil y se deja solo la referencia a través de etiqueta Label.BI_Customer_Portal_Profile.
	     		for (User u : [SELECT Id, ContactId, Email FROM User WHERE ContactId IN :set_contact AND profile.Name = :Label.BI_Customer_Portal_Profile AND isActive = true]){
	     			map_contact_user.put(u.ContactId, u);
	     		}
                // FIN 03-01-2018 AJAE
	     		System.debug('usuariosportal--> ' + map_contact_user.size());
	     		//Lista de mensajes que se enviarán
	     		List<Messaging.SingleEmailMessage> list_emails = new List<Messaging.SingleEmailMessage>();
	     		//Listado de comentarios con los datos del caso necesarios para el envío del correo
	     		for(CaseComment cas : lst_comment){
		  			//Comprueba que el contacto tenga un usuario de portal platino
		  			System.debug('NOMBRE: ' + cas.Parent.Reason);
	     			if(cas.Parent.ContactId != null && map_contact_user.containsKey(cas.Parent.ContactId)){
	     				//Comprobar que el usuario que generó el caso no es el mismo que el creador del comentario
	     				System.debug('Creado por-->' + cas.createdById + 'usuarioquecomenta-->' + map_contact_user.get(cas.Parent.ContactId));
			  			if(map_contact_user.get(cas.Parent.ContactId).Id != cas.CreatedById){
			  				//Creamos el mensaje que se enviará después de asegurar que el propietario no es el que comenta en el caso
			  				Messaging.SingleEmailMessage singleEmail = new Messaging.SingleEmailMessage();
				  			//correo del que generó el caso de portal
				  			//toAddress.add(map_contact_user.get(cas.Parent.ContactId).Email);
				  			//generando el correo a enviar añadiendo plantilla dirección y target
				  			singleEmail.saveAsActivity=false;
				  			//singleEmail.setToAddresses(toAddress);
				  			//System.debug('DIRECCIOOOOON ' + toAddress);
				  			singleEmail.setTemplateId(templates[0].Id);
				  			singleEmail.setWhatId(cas.ParentId);
				  			//Coge el correo del contacto para envio
				  			singleEmail.setTargetObjectId(cas.Parent.ContactId);
				  			
							list_emails.add(singleEmail);
			  			}
			  		}
		  		}
		  		//Envío de emails
			  	if(!list_emails.isEmpty()){
			  		System.debug('lista de correo-->' + list_emails);
			  		Messaging.sendEmail(list_emails);
			  	}
	     	}

		}catch(Exception Exc){
    		BI_LogHelper.generate_BILog('PCA_CaseCommentMethods.sendEmail', 'Portal Platino', Exc, 'Class');
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Patricia Castillo
	Company:       NEA
	Description:   Method that avoids insert comment if BIEN and CaseStatus Closed/Cancelled
	    
    History: 
	    
	<Date>                  <Author>                <Change Description>
  	18/10/2016				Patricia Castillo		Initial version 
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void validateCaseStatusBIEN (List <CaseComment> news) {

		Set<Id> caseIds = new Set<Id>();
		List <Case> caseParent = new List<Case>();
		Set<Id> closedCasesIds = new Set<Id>();

		for(CaseComment cm : news) {
			caseIds.add(cm.ParentId);
		}
		caseParent = [SELECT Id, Status from Case where Id in :caseIds and Account.TGS_Es_MNC__c=false and (Status =: Label.BI_CaseStatus_Closed or Status =: Label.BI_CaseStatus_Cancelled)];
		
		if(!caseParent.isEmpty()) {
			for(Case cp : caseParent) {
				closedCasesIds.add(cp.Id);
			}
		
			for(CaseComment cc: news) {
					if(closedCasesIds.contains(cc.ParentId)) {
						cc.addError('No puede insertar comentarios en un caso Cerrado/Cancelado');
					}
			}
		}

	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Alberto Fernández
        Company:       Accenture
        Description:   Method that reopens a 'Pending' PER Case when an user makes a comment
        
        History: 
        
        <Date>                  <Author>                <Change Description>
        21/04/2017             Alberto Fernández        Initial version
        24/04/2017			   Alberto Fernández        Code optimized
        03/01/2018             Alfonso Alvarez          Cambio debido a la migración a licencias de Portal Customer Communities (CreatedBy.ProfileName)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
	public static void reopenPERCases(Map<Id, CaseComment> news) {

        try {

	        List<Case> lst_casesToUpdate = new List<Case>();

            // 03-01-2018 AJAE : Se quita la referencia directa a un perfil y se deja solo la referencia a través de etiqueta Label.BI_Customer_Portal_Profile.
	        for(Case cas : [SELECT Id, Status, BI_PER_Comentario_nuevo__c, Tipo_registro_filtro__c
	                        FROM Case
	                        WHERE Id IN (SELECT ParentId FROM CaseComment WHERE CreatedBy.Profile.Name = :Label.BI_Customer_Portal_Profile AND Id IN :news.keySet())
	                        AND (Status LIKE '%Pendi%' OR Status LIKE '%pendi%')])
	        {
            // AJAE FIN 03-01-2018
	            cas.Status = 'In Progress';
	            cas.BI_PER_Comentario_nuevo__c = true;
	            lst_casesToUpdate.add(cas);

	        }

	        if(!lst_casesToUpdate.isEmpty()) {
	            update lst_casesToUpdate;
	        }

        }
        catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_CaseCommentMethods.reopenPERCases', 'BI_EN', Exc, 'Trigger');
        }

    }   

}