/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Lanzador para sincronización de la cartera de clientes.

History:
<Date>							<Author>						<Change Description>
19/05/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class FS_CORE_Fullstack_Scheduler implements Schedulable {
    /* SALESFORCE VARIABLE DEFINITION */
    FS_CORE_Integracion__c ORGConfiguration = FS_CORE_Integracion__c.getInstance();
    
    global void Execute(SchedulableContext ctx) {
        /* Variable Definition */
        Set<Id> accountIds = new Set<Id>();
        
        /* Retrieve Accounts & AccountTeamMembers */        
        For(Account item : [SELECT Id FROM Account WHERE SystemModStamp >= LAST_N_DAYS:7]){accountIds.add(item.Id);}
        For(AccountTeamMember item : [SELECT AccountId FROM AccountTeamMember WHERE SystemModStamp >= LAST_N_DAYS:7]){accountIds.add(item.AccountId);}
        
        /* Process */
        For(Account item : [SELECT Id, OwnerId, BI2_asesor__c, BI2_service_manager__c FROM Account WHERE Id IN :accountIds]){FS_CORE_Fullstack_ATM.process(item);}
    }
}