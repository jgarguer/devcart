@isTest
private class PCA_CatalogoProdController_TEST {

    static testMethod void getloadInfo() {

        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());       
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(2);
            
            List<Account> accounts = BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: accounts){
                    item.OwnerId = usr.Id;
                    accList.add(item);
            }
            update accList;
            List<Contact> con = BI_DataLoad.loadPortalContacts(1, accounts);
             User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());
                    
             Market_Place_PP__c Familia1 = new Market_Place_PP__c(
             Name = 'Familia1',
             BI_Country__c = lst_pais[0],
             BI_Posicion__c = 1,
             BI_Titulo__c = 'Familia 1',
             BI_Subtitulo__c = 'Familia 1',
             BI_Texto__c = 'Esto es la familia 1'
             );
             insert Familia1;
             
             BI_Catalogo_PP__c Prod1 = new BI_Catalogo_PP__c(
             Name = 'Producto1',
             BI_Familia_del_producto__c = Familia1.Id
             );
             
             insert Prod1;
             
            PageReference pageRef = Page.PCA_Catalogo_Product;
            Test.setCurrentPage(pageRef);
             
            PCA_CatalogoProdController constructor = new PCA_CatalogoProdController();
            system.runAs(user1){
                Test.startTest();
                BI_TestUtils.throw_exception = false;
                constructor.checkPermissions();
                ApexPages.currentPage().getParameters().put('Id', Prod1.Id);            
                constructor.loadInfo();
                constructor.createTask();
                system.assertEquals(Prod1.Id,ApexPages.currentPage().getParameters().get('Id'));
            } 
            system.runAs(user1){
                BI_TestUtils.throw_exception = true;
                constructor.checkPermissions();
                ApexPages.currentPage().getParameters().put('Id', Prod1.Id);            
                constructor.loadInfo();
                constructor.createTask();
                system.assertEquals(Prod1.Id,ApexPages.currentPage().getParameters().get('Id'));
            } 

        }
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}