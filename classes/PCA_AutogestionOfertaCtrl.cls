public class PCA_AutogestionOfertaCtrl extends PCA_HomeController
{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Class for manage Opportunities in Portal Platino 
    
    History:
    
    <Date>            <Author>              <Description>
    25/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public String nameFS;
    public String nameObject                    {get; set;}
    public String accountFieldName;
    
    public String searchFinal;
    
    public Id AccountId                         {get; set;}
    
    public String searchText                    {get; set;}
    public String searchField                   {get; set;}
    public String firstHeader                   {get; set;}
    
    public Integer index                        {get; set;}
    public Integer pageSize                     {get; set;}
    public Integer totalRegs                    {get; set;}
    
    public List<SelectOption> searchFields      {get; set;}
    
    public List<FieldSetHelper.FieldSetRecord> viewRecords  {get; set;}
    public FieldSetHelper.FieldSetContainer fieldSetRecords {get; set;}
    
    public boolean onlyOrders {get; set;}
    
    public String fieldImage {get;set;}
    public String Mode {get;set;}
    
    public String FieldOrder {get;set;}
    public String Field;    
    public String PageValue {get;set;}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            PageReference page = enviarALoginComm();
              if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
              }
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_AutogestionOfertaCtrl.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Constructor
    
    History:
    
    <Date>            <Author>              <Description>
    25/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_AutogestionOfertaCtrl()
    {}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info of opportunities
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfo (){
        try{
            this.index = 1;
            this.pageSize = 10;
            this.searchFinal = null;        
            this.AccountId = BI_AccountHelper.getCurrentAccountId();
            
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            if(AccountId == null){
                return;
            }
            
            this.nameObject = 'Opportunity';
            this.nameFS = 'PCA_MainTableOportunidad';
            this.accountFieldName = 'AccountId';
            
            defineSearch();
            defineRecords();
            defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_AutogestionOfertaCtrl.PCA_AutogestionOfertaCtrl', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Define records of the search
    
    History:
    
    <Date>            <Author>              <Description>
    25/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void defineRecords()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            String accountValue = this.accountFieldName + ';' + this.AccountId;
        
            this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.searchFinal, accountValue,null);
            //system.debug('this.fieldSetRecords: '+this.fieldSetRecords);
            if (this.fieldSetRecords.regs.size() > 0)
            {
                this.firstHeader = this.fieldSetRecords.regs[0].values[0].field;
            }
            
            this.index = 1; 
            this.totalRegs = this.fieldSetRecords.regs.size();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_AutogestionOfertaCtrl.defineRecords', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Define the view of the results
    
    History:
    
    <Date>            <Author>              <Description>
    25/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void defineViews()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.viewRecords = new List<FieldSetHelper.FieldSetRecord>();
            Integer topValue = ((this.index + this.pageSize) > this.totalRegs) ? this.totalRegs : (this.index + this.pageSize - 1);
            //system.debug('topValue: '+topValue);
            for (Integer i = (this.index - 1); i < topValue; i++)
            {
                //system.debug('this.fieldSetRecords.regs[i]: '+this.fieldSetRecords.regs[i]);
                FieldSetHelper.FieldSetRecord iRecord = this.fieldSetRecords.regs[i]; 
                this.viewRecords.add(iRecord);
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_AutogestionOfertaCtrl.defineViews', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Define search criteria
    
    History:
    
    <Date>            <Author>              <Description>
    25/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void defineSearch()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.searchFields = FieldSetHelper.defineSearchFields(this.nameFS, this.nameObject);    
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_AutogestionOfertaCtrl.defineSearch', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Search result records
    
    History:
    
    <Date>            <Author>              <Description>
    25/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void searchRecords()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            this.searchFinal = this.searchField + ';' + this.searchText;
            defineRecords();
            defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_AutogestionOfertaCtrl.searchRecords', 'Portal Platino', Exc, 'Class');
        }
    }       
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   This method retrieves true if the index is not the first one, false otherwise
    
    History:
    
    <Date>            <Author>              <Description>
    25/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Boolean getHasPrevious()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            return (this.index != 1);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_AutogestionOfertaCtrl.getHasPrevious', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   This method retrieves true if the index is not the last one, false otherwise
    
    History:
    
    <Date>            <Author>              <Description>
    25/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Boolean getHasNext()
    {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            return !((this.index + this.pageSize) > this.totalRegs);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_AutogestionOfertaCtrl.getHasNext', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   This method adds one position to the index variable
    
    History:
    
    <Date>            <Author>              <Description>
    25/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void Next()
    {
        try{
        if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
        this.index += this.pageSize;
        defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_AutogestionOfertaCtrl.Next', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   This method decreases one position to the index variable
    
    History:
    
    <Date>            <Author>              <Description>
    25/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void Previous()
    {
        try{
        if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
        this.pageSize = 10;
        this.index -= this.pageSize;
        defineViews();  
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_AutogestionOfertaCtrl.Previous', 'Portal Platino', Exc, 'Class');
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines records in page
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public List<SelectOption> getItemPage() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('25','25'));
        options.add(new SelectOption('50','50'));
        options.add(new SelectOption('100','100'));
        options.add(new SelectOption('200','200'));
        system.debug('itemPage: '+ options);
        return options;
    }   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Show more values in a page
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void valuesInPage(){
        
        this.pageSize = Integer.valueOf(PageValue);
        system.debug('pageSize*: '+this.pageSize);
        defineViews();
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Order table with field
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public void Order(){
        
        try{/*
            this.searchText = null;
            this.searchFinal = null;
            this.OptionA = this.OptionA ? false : true;
            this.nameFS = (this.nameFS == this.nameFS1) ? this.nameFS2 : this.nameFS1;
            this.nameObject = (this.nameObject == this.nameObject1) ? this.nameObject2 : this.nameObject1;
            this.accountFieldName = (this.accountFieldName == this.accountFieldName1) ? this.accountFieldName2 : this.accountFieldName1;
    */          
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            Mode = '';
            defineSearch();
            fieldImage = FieldOrder;
            String accountValue = this.accountFieldName + ';' + this.AccountId;
            //if(this.nameObject == this.nameObject2){
            //this.searchFinal = '::NE__OrderId__r.RecordType.DeveloperName;Asset;'+FieldOrder;
            //}
            if(Field == FieldOrder){
                this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.FieldOrder, accountValue,  'DESC');
                FieldOrder = '';
                Mode = 'DESC';
            }else{
                this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.FieldOrder, accountValue,  'ASC');
                Mode = 'ASC';
            }
            if (this.fieldSetRecords.regs.size() > 0)
            {
                this.firstHeader = this.fieldSetRecords.regs[0].values[0].field;
            }
            
            this.index = 1; 
            this.totalRegs = this.fieldSetRecords.regs.size();
            
            Field = this.FieldOrder;
            defineViews();
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.Order','Portal Platino', Exc, 'Class');
        }
        
    }   
}