@isTest
public class BI_O4_Project_deleteProject_Cntrl_TEST {

    static{
        BI_TestUtils.throw_exception = false;
    }

    @isTest
    static void test_deleteProject() {
        Milestone1_Project__c proj = new Milestone1_Project__c ();          
                        proj.Name = 'TEST PROJ ' + Datetime.now().getTime();
                        proj.BI_Etapa_del_proyecto__c = 'Inicio';
                        proj.BI_Country__c = Label.BI_Argentina;
                        proj.BI_Complejidad_del_proyecto__c='Complejo';
                        proj.BI_O4_Project_Customer__c  = 'rellenado';
        
        Test.setCurrentPage(Page.BI_O4_Project_deleteProject);
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(proj);
        new BI_O4_Project_deleteProject_Controller(controller);
        
        Test.startTest();
        insert proj;
        Test.stopTest();
        
        controller = new ApexPages.Standardcontroller(proj);
        ApexPages.currentPage().getParameters().put('id', proj.Id);
        
        BI_O4_Project_deleteProject_Controller cnt = new BI_O4_Project_deleteProject_Controller(controller);
        PageReference pr = cnt.deleteProject();
        //System.assertNotEquals(null, pr);
        
        cnt.idProject = null;
       String a = cnt.urlBackId;
        pr = cnt.deleteProject();
        System.assertEquals(null, pr);

    }

     @isTest static void exceptions(){

        BI_TestUtils.throw_exception = true;

         Milestone1_Project__c proj = new Milestone1_Project__c ();          
                        proj.Name = 'TEST PROJ ' + Datetime.now().getTime();
                        proj.BI_Etapa_del_proyecto__c = 'Inicio';
                        proj.BI_Country__c = Label.BI_Argentina;
                        proj.BI_Complejidad_del_proyecto__c='Complejo';
                        proj.BI_O4_Project_Customer__c  = 'rellenado';
                        insert proj;
        
        Test.setCurrentPage(Page.BI_O4_Project_deleteProject);
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(proj);
        new BI_O4_Project_deleteProject_Controller(controller);

        controller = new ApexPages.Standardcontroller(proj);
        ApexPages.currentPage().getParameters().put('id', proj.Id);
        
        BI_O4_Project_deleteProject_Controller cnt = new BI_O4_Project_deleteProject_Controller(controller);
        PageReference pr = cnt.deleteProject();

    }
    
}