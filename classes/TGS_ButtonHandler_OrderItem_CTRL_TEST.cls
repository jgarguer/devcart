@isTest
private class TGS_ButtonHandler_OrderItem_CTRL_TEST {
	
	public TGS_ButtonHandler_OrderItem_CTRL_TEST(){
	}
	
	@isTest static void redirectTEST () {

		TGS_ButtonHandler_OrderItem_CTRL button1= new TGS_ButtonHandler_OrderItem_CTRL();
		NE__OrderItem__c testOi = TGS_Dummy_Test_Data.dummyOrderItem();
		NE__Catalog_Item__c testCi= TGS_Dummy_Test_Data.dummyCatalogItem();
		NE__Catalog_Item__c testCi2= TGS_Dummy_Test_Data.dummyCatalogItem();
		testCi2.NE__Root_Catalog_Item__c= testCi2.Id;
		
		ApexPages.StandardController controller  = new ApexPages.StandardController(testOi);
		TGS_ButtonHandler_OrderItem_CTRL button= new TGS_ButtonHandler_OrderItem_CTRL(controller);
		button.redirect();


		// Implement test code
	}
}