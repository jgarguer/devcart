@isTest
private class PCA_Reclamo_PopUpDetail_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    	Author:        Ana Escrich
    	Company:       Salesforce.com
    	Description:   Test class to manage the coverage code for PCA_Reclamo_PopUpDetail class 
    	
    	History: 
    	<Date> 					<Author> 				<Change Description>
    	14/08/2014      		Ana Escrich	    		Initial Version
        12/07/2016              José Luis González      Fix test for UNICA modifications
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 		Author:        Ana Escrich
 		Company:       Salesforce.com
 		Description:   Test method to manage the code coverage for PCA_Reclamo_PopUpDetail.loadInfo and PCA_Reclamo_PopUpDetail.createComment 
			    
 		History: 
 		
 		<Date> 					<Author> 				<Change Description>
    	13/08/2014      		Ana Escrich	    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void getloadInfo() {

        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        //Simula Usuario del portal
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        System.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
            System.runAs(user1){
                //END Simula Usuario del portal

         		BI_TestUtils.throw_exception = false;
        		//List<Account> accounts = BI_DataLoad.loadAccounts(1, BI_DataLoad.loadPais(1));
        		List<RecordType> rT = [select id, name from Recordtype where name='Caso Comercial'];
        		Case caseTest = new Case(RecordTypeId=rT[0].Id, AccountId = accList[0].Id, BI_Otro_Tipo__c = 'test', Status='Nuevo', Priority='Media', Type='Reclamo', 
        									Subject='Test', CurrencyIsoCode='ARS', Origin='Portal Cliente', BI_Confidencial__c=false, 
        									Reason='Reclamos administrativos', Description='testdesc');
        		insert caseTest;
                CaseComment CasCom_Test = new CaseComment(
                    ParentId = caseTest.Id,
                    Commentbody = 'test'
                    );
                 insert CasCom_Test;
            	/* integer ContAdj;
            	 ContAdj = 1;
                 insert ContAdj;*/
        			
        		 PageReference pageRef = new PageReference('PCA_Reclamo_PopUpDetail');
                 Test.setCurrentPage(pageRef);
                 ApexPages.currentPage().getParameters().put('Id', caseTest.Id);
                 ApexPages.currentPage().getParameters().put('Integer', '1'); 
           		 
           		 
            	 PCA_Reclamo_PopUpDetail controller = new PCA_Reclamo_PopUpDetail();
            	 controller.loadInfo();
                 system.assertEquals(controller.caseId,caseTest.Id);
                 system.assert(controller.CasesComment != null && !controller.CasesComment.isEmpty());
                 controller.inicializeNewComment();
            	 
                 controller.newComment.CommentBody='test comment';
            	 controller.createComment();
            }
		}

        system.assertEquals([SELECT Id FROM CaseComment].size(), 2);
	}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 		Author:        Ana Escrich
 		Company:       Salesforce.com
 		Description:   Test method to manage the code coverage for PCA_Reclamo_PopUpDetail.cancel
			    
 		History: 
 		
 		<Date> 					<Author> 				<Change Description>
    	14/08/2014      		Ana Escrich	    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void cancelTest() {
 		BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	 PCA_Reclamo_PopUpDetail controller = new PCA_Reclamo_PopUpDetail();
    	 controller.cancel();
		//}
	}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 		Author:        Ana Escrich
 		Company:       Salesforce.com
 		Description:   Test method to manage the code coverage for PCA_Reclamo_PopUpDetail.checkPermissions
			    
 		History: 
 		
 		<Date> 					<Author> 				<Change Description>
    	14/08/2014      		Ana Escrich	    		Initial Version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void checkPermissionsTest() {
 		BI_TestUtils.throw_exception = false;
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
    	 User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
	    //insert usr;
	    system.debug('usr: '+usr);
	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            
	    	system.runAs(user1){
	        	 PCA_Reclamo_PopUpDetail controller = new PCA_Reclamo_PopUpDetail();
	        	 controller.checkPermissions();
                 controller.loadInfo();
			}
	    }
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    	Author:        Micah Burgos
    	Company:       Salesforce.com
    	Description:   Test method to manage the code coverage for PCA_Reclamo_PopUpDetail.checkPermissions
    	        
    	History: 
    	
    	<Date>						<Author>                <Change Description>
    	03/10/2014					Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void all_catch_test() {
        
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
         BI_TestUtils.throw_exception = true;

         PCA_Reclamo_PopUpDetail controller = new PCA_Reclamo_PopUpDetail();
         controller.checkPermissions();
         controller.loadInfo();
         controller.createComment();
         controller.inicializeNewComment();
         controller.cancel();
         controller.checkPermissions();
    }
    
	/* Commented to avoid problems deploying in Spring15
		@testSetup static void userOrgCS() {
			TGS_User_Org__c userTGS = new TGS_User_Org__c();
			userTGS.TGS_Is_BI_EN__c = true;
			insert userTGS;
		}
	*/ 
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Morales
    Company:       Deloitte
    Description:   Test method to manage the code coverage for PCA_Reclamo_PopUpDetail Web services method
            
    History: 
    
     <Date>                     <Author>                <Change Description>
    03/11/2015                 Ignacio Morales             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    static testMethod void PCA_Reclamo_PopUpDetail_Ws() {
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
            system.runAs(user1){
                //END Simula Usuario del portal

             		BI_TestUtils.throw_exception = false;
            		//List<Account> accounts = BI_DataLoad.loadAccounts(1, BI_DataLoad.loadPais(1));
            		List<RecordType> rT = [select id, name from Recordtype where name='Incidencia Tecnica'];
            		Case caseTest = new Case(RecordTypeId=rT[0].Id, AccountId = accList[0].Id, BI_Otro_Tipo__c = 'test', Status='Nuevo', Priority='Media', Type='Reclamo', 
            									Subject='Test', CurrencyIsoCode='ARS', Origin='Portal Cliente', BI_Confidencial__c=false, 
            									Reason='Reclamos administrativos', Description='testdesc');
            		 insert caseTest;
                	 string BodyAttach = 'Cuerpo';
                	 Attachment AttachTest = new Attachment(ParentId = caseTest.Id,
                                                            Name = 'Adjunto',
                                                            Body = Blob.valueOf(BodyAttach));

                	insert AttachTest;
                
                	PageReference p = Page.PCA_Reclamo_PopUpDetail;
                    p.getParameters().put('attach_Id', AttachTest.Id);
                	p.getParameters().put('Id', caseTest.Id);
                    Test.setCurrentPageReference(p);
                
                     BI_TestUtils.throw_exception = false;
                     Test.setMock(HttpCalloutMock.class, new TGS_MockHttpResponseGenerator()); 
                      PCA_Reclamo_PopUpDetail controller = new PCA_Reclamo_PopUpDetail();
                     controller.checkPermissions();
                     controller.loadInfo();
                     PageReference showAttachment = controller.showAttachment();
                     controller.closePopup();
                     controller.showPopup();
                     controller.closePopup2();
                     controller.showPopup2();
                     controller.closePopup3();
                     controller.showPopup3();
                     caseTest.status = 'Resolved';
                	 update caseTest;
                     controller.GuardarNotaReabrir();
                     caseTest.status = 'Assigned';
                     update caseTest;
                     update caseTest;
                     controller.GuardarNotaCancelar();
                     controller.GuardarNotaCancelar(); 
                     caseTest.status = 'Resolved';
                     update caseTest;
                     controller.CerrarIncidencia();
                     controller.actualizarEstado('6', '3', 'Abierto',''); 
    		}       
    	}
	}
    
    static testMethod void PCA_Reclamo_PopUpDetail_Buttons() {
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
            system.runAs(user1){
                //END Simula Usuario del portal

         		BI_TestUtils.throw_exception = false;
        		//List<Account> accounts = BI_DataLoad.loadAccounts(1, BI_DataLoad.loadPais(1));
        		List<RecordType> rT = [select id, name from Recordtype where name='Caso Comercial II'];
        		Case caseTest = new Case(RecordTypeId=rT[0].Id, AccountId = accList[0].Id, ContactId = con[0].Id, BI_Otro_Tipo__c = 'test', Status='Nuevo', Priority='Media',
        								Type='Reclamo', Subject='Test', CurrencyIsoCode='ARS', Origin='Portal Cliente', BI_Confidencial__c=false, 
        								Reason='Reclamos administrativos', Description='testdesc',BI2_PER_Tipologia__c = 'Test',BI_Product_Service__c = ' Test',BI2_per_subtipologia__c = 'Test', 
										BI_Line_of_Business__c = 'Test',TGS_Disputed_number_of_lines__c = 120,BI2_PER_Territorio_caso__c = 'Test');
        		 insert caseTest;
            	 string BodyAttach = 'Cuerpo';
            	 Attachment AttachTest = new Attachment(ParentId = caseTest.Id,
                                                        Name = 'Adjunto',
                                                        Body = Blob.valueOf(BodyAttach));

            	insert AttachTest;
                
                PageReference p = Page.PCA_Reclamo_PopUpDetail;
                p.getParameters().put('attach_Id', AttachTest.Id);
                p.getParameters().put('Id', caseTest.Id);
                Test.setCurrentPageReference(p);
                
                BI_TestUtils.throw_exception = false;
                Test.setMock(HttpCalloutMock.class, new TGS_MockHttpResponseGenerator()); 
                PCA_Reclamo_PopUpDetail controller = new PCA_Reclamo_PopUpDetail();
                Test.startTest();
                controller.checkPermissions();
                controller.loadInfo();
                controller.getCaseStatus();
                controller.closeCase();
                controller.getCaseStatus();
                controller.reopenCase();
                controller.getCaseStatus();
                Test.stopTest();
            }
        }
    }
    @isTest public static void test_exceptions() {
        BI_TestUtils.throw_exception = true;
        PCA_Reclamo_PopUpDetail reclamoPop = new PCA_Reclamo_PopUpDetail();

        List<RecordType> rT = [select Id, Name from Recordtype where name='Caso Comercial II' ];
        reclamoPop.drawFields();
        reclamoPop.createComment();
       /* 
        reclamoPop.newComment.Id=rT[0].Id;
        reclamoPop.createComment();
        */
        reclamoPop.getisBIEN2Case();
        reclamoPop.getCaseStatus();
        reclamoPop.closeCase();
        reclamoPop.reopenCase();
        reclamoPop.getFieldValue(null);
        reclamoPop.name = '';
        reclamoPop.Casetype = new List<Case>();
        reclamoPop.attachName = 'test';
        reclamoPop.worklogId = 'test';
        reclamoPop.posicionWI = 'Pos01';
        ///reclamoPop.actualizarEstado('','', '' ,'');
        //Integer ContAdj = 1;
        reclamoPop.ContAdj= 1;
        reclamoPop.ConsultaIncidencia = new Case(Status='');
        //reclamoPop.cancelCase();
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerAnexo'));
        reclamoPop.getAttBody();
        reclamoPop.reorder();
        //reclamoPop.adjunto1;
        reclamoPop.nombre1='';
        //reclamoPop.adjunto2;
        reclamoPop.nombre2='';
        //reclamoPop.adjunto3;
        reclamoPop.nombre3='';
        reclamoPop.caseName='';
        reclamoPop.rutaBmc='';
        reclamoPop.rutaBase='';
        reclamoPop.IdAdjunto='';
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA(''));
        reclamoPop.crearWIRoD();
        reclamoPop.caseParent=new Case();
	}
    
    static testMethod void GuardarNotaGestionarTest(){
        BI_TestUtils.throw_exception = false;
          PCA_Reclamo_PopUpDetail reclamoPop = new PCA_Reclamo_PopUpDetail();
        string BodyAttach = 'Cuerpo';
        Attachment attach = new Attachment( Name = 'Adjunto',
                                             Body = Blob.valueOf(BodyAttach));
        List<Attachment> listAttach= new List<Attachment>();
         List<Attachment> listAttach2= new List<Attachment>();
         Boolean YaAdjuntado = false;
        //List<Attachment> listAttach =[select Body , id, Name from Attachment where BodyLength > 1 LIMIT 1];
        Integer ContAdj= 1; 
        reclamoPop.ContAdj= ContAdj;
        reclamoPop.attach= attach;
        reclamoPop.ListaAdjuntos= listAttach;
        reclamoPop.ListaAdjuntos2= listAttach2;
        reclamoPop.YaAdjuntado= YaAdjuntado;
        Integer ContAdj2= 4;
        reclamoPop.ContAdj=ContAdj2;
        
        reclamoPop.ClosePopupErrorNota();
        reclamoPop.ClosePopupError();
    }

    public static testMethod void test_closeCase() {
        BI_TestUtils.throw_exception = false;

        List<RecordType> lst_rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI2_Caso_Comercial'];

        PCA_Reclamo_PopUpDetail contr = new PCA_Reclamo_PopUpDetail();

        contr.chosenCase = new Case(RecordTypeId=lst_rt[0].Id);

        BI_bypass__c bypass = new BI_bypass__c(BI_skip_trigger__c = true, BI_migration__c=true);
        insert bypass;
        insert contr.chosenCase;
        Case child = new Case(Subject='Test', Status='Assigned', ParentId=contr.chosenCase.Id, RecordTypeId=lst_rt[0].Id);
        insert child;
        delete bypass;

        contr.chosenCase = [SELECT Id, Status, (SELECT Id, Status FROM Cases) FROM Case WHERE Id= :contr.chosenCase.Id];

        Test.startTest();
        contr.chosenCase.TGS_SLA_Light__c = 'Green';
        contr.closeCase();

        contr.chosenCase.TGS_SLA_Light__c = 'Red';
        contr.closeCase();

        bypass = new BI_bypass__c(BI_skip_trigger__c = true, BI_migration__c=true);
        insert bypass;
        child.Status = 'Closed';
        update child;
        delete bypass;

        contr.chosenCase.TGS_SLA_Light__c = 'Green';
        contr.closeCase();
        Test.stopTest();
    }
    
    public static testMethod void test_cancelCase() {
        BI_TestUtils.throw_exception = false;

        PCA_Reclamo_PopUpDetail contr = new PCA_Reclamo_PopUpDetail();

        contr.chosenCase = new Case();
        contr.inicializeNewComment();

        contr.cancelCase();

        contr.newComment.CommentBody = 'Test';
        contr.cancelCase();

        contr.cancelCase();
    }    

    public static testMethod void test_reorder() {
    	BI_TestUtils.throw_exception = false;

    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
    	List<Contact> con = BI_DataLoad.loadContacts(1, acc);
    	User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());

    	System.runAs(user1) {
    		List<RecordType> rT = [select id, name from Recordtype where name='Caso Comercial'];
    		Case caseTest = new Case(RecordTypeId=rT[0].Id, AccountId = acc[0].Id, BI_Otro_Tipo__c = 'test', Status='Nuevo', Priority='Media', Type='Reclamo', 
    									Subject='Test', CurrencyIsoCode='ARS', Origin='Portal Cliente', BI_Confidencial__c=false, 
    									Reason='Reclamos administrativos', Description='testdesc');
    		insert caseTest;

    		insert new Case(RecordTypeId=rT[0].Id, Status='Assigned', Subject='Test', Origin='Portal Cliente', ParentId=caseTest.Id, BI_Confidencial__c=false);

    		PageReference pageRef = new PageReference('PCA_Reclamo_PopUpDetail');
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id', caseTest.Id);

            PCA_Reclamo_PopUpDetail cont = new PCA_Reclamo_PopUpDetail();
            cont.loadInfo();

            Test.startTest();
            cont.child_searchFinal = 'Subject;Test';
            cont.child_fieldOrder = 'CaseNumber';
            cont.child_field = 'CaseNumber';
            cont.reorder();

            Test.stopTest();

            System.assertNotEquals(null, cont.child_firstHeader);
            System.assertNotEquals(null, cont.child_fixedStatuses);
    	}
    }   
                
}