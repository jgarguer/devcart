@RestResource(urlMapping='/customerinfo/v1/customers/*')
global class BI_CustomerRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Customer Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains basic information stored in the server for a specific customer.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.CustomerType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.CustomerType getCustomer() {
		
		BI_RestWrapper.CustomerType response;
		
		try{
			
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
				
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
				
				response = BI_RestHelper.getOneCustomer(RestContext.request.requestURI.split('/')[4], RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
			
				RestContext.response.statuscode = (response == null)?404:200;//404 NOT_FOUND; 200 OK
				
			}
			
		}catch(Exception Exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',Exc.getMessage());
			BI_LogHelper.generate_BILog('BI_CustomerRest.getCustomer', 'BI_EN', Exc, 'Web Service');
			
		}
		
		return response;
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Creates a new customer in the server receiving the request.
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPost
	global static void createCustomer() {
		
		try{
			
			if(RestContext.request.headers.get('countryISO') == null){
						
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
			
				RestRequest req = RestContext.request;
			
				Blob blob_json = req.requestBody; 
			
				String string_json = blob_json.toString(); 
				
				BI_RestWrapper.CustomerRequestType custReqType = (BI_RestWrapper.CustomerRequestType) JSON.deserialize(string_json, BI_RestWrapper.CustomerRequestType.class);
				
				String customerExtId;
				
				if(custReqType.accountDetails != null && custReqType.accountDetails.additionalData != null){
					
					for(BI_RestWrapper.KeyvalueType keyValue:custReqType.accountDetails.additionalData){
						
						if(keyValue.Key == 'customerId'){
							customerExtId = keyValue.value;
							break;
						}
						
					}
					
					if(customerExtId == null){
						
						RestContext.response.statuscode = 400;//BAD_REQUEST
						RestContext.response.headers.put('errorMessage', Label.BI_MissingRequiredFields+' customerId');
						
					}else{
						
						Account cust = new Account();
						
						if(custReqType.accountDetails != null){
							
							cust.BI_Id_del_cliente__c = customerExtId;
							if(RestContext.request.headers.get('systemName') != null)
								cust.BI_Sistema_legado__c = RestContext.request.headers.get('systemName');
							
							//List<Region__c> lst_region = [select Id from Region__c where BI_Codigo_ISO__c = :RestContext.request.headers.get('countryISO') limit 1];
							
							BI_Code_ISO__c biCodeIso = [select BI_Country_Currency_ISO_Code__c, BI_Country_ISO_Code__c, Name from BI_Code_ISO__c where BI_Country_ISO_Code__c =: RestContext.request.headers.get('countryISO')];
							
							
							if(biCodeIso != NULL )
								cust.BI_Country__c = biCodeIso.Name;
							else{
								
								RestContext.response.statuscode = 400;//BAD_REQUEST
								RestContext.response.headers.put('errorMessage', Label.BI_CountryNotValid);
								
							}
							
							cust = BI_RestHelper.generateCustomer(cust, custReqType.accountDetails);
							
						}
						
						insert cust;
						
						RestContext.response.statuscode = 201;
						RestContext.response.headers.put('Location','https://'+System.URL.getSalesforceBaseURL().getHost()+'/services/apexrest/customerinfo/v1/customers/'+cust.BI_Id_del_cliente__c);
						
					}
					
				}else{
					
					RestContext.response.statuscode = 400;//BAD_REQUEST
					RestContext.response.headers.put('errorMessage', Label.BI_MissingRequiredFields+' customerId');
					
				}
				
			}
			
		}catch(Exception Exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR 
			RestContext.response.headers.put('errorMessage',Exc.getMessage());
			BI_LogHelper.generate_BILog('BI_CustomerRest.createCustomer', 'BI_EN', Exc, 'Web Service');
			
		}
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Modifies an existing customer in the server receiving the request.
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    14/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPut
	global static void updateCustomer() {
		
		try{
				
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
				
				String idCust = RestContext.request.requestURI.split('/')[4] + ' '+RestContext.request.headers.get('countryISO');
				if(RestContext.request.headers.get('systemName') != null)
					 idCust += ' '+RestContext.request.headers.get('systemName');
				
				List<Account> lst_cust = [select Id from Account where BI_Validador_Id_de_legado__c = :idCust AND BI_Holding__c = 'Sí' limit 1];
		
				if(!lst_cust.isEmpty()){
					
					RestRequest req = RestContext.request;
			
					Blob blob_json = req.requestBody; 
				
					String string_json = blob_json.toString(); 
					
					BI_RestWrapper.CustomerRequestType custReqType = (BI_RestWrapper.CustomerRequestType) JSON.deserialize(string_json, BI_RestWrapper.CustomerRequestType.class);
				
					Account cust = lst_cust[0];
					
					if(custReqType.accountDetails != null)
						cust = BI_RestHelper.generateCustomer(cust, custReqType.accountDetails);
					
					update cust;
					
					RestContext.response.statuscode = 200;//OK
					
				}else{
					
					RestContext.response.statuscode = 404;//NOT_FOUND
					
				}
				
			}
				
		}catch(Exception Exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR 
			RestContext.response.headers.put('errorMessage',Exc.getMessage());
			BI_LogHelper.generate_BILog('BI_CustomerRest.updateCustomer', 'BI_EN', Exc, 'Web Service');
			
		}
		
	}
	
}