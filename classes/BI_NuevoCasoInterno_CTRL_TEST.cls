@isTest
private class BI_NuevoCasoInterno_CTRL_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:        Guillermo Muñoz
	 Company:       Accenture
	 Description:   Test class to manage the code coverage of BI_NuevoCasoInterno_CTRL
	 
	 History:
	  
	 <Date>              <Author>                   <Change Description>
	 05/10/2017          Guillermo Muñoz            Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	@isTest static void fullTest() {
		
		TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }

        BI_TestUtils.throw_exception = false;
        BI_MigrationHelper.skipAllTriggers();

        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{Label.BI_Argentina});
        Opportunity opp = new Opportunity(
            Name = 'Test 1',
            CloseDate = Date.today(),
            StageName = Label.BI_F6Preoportunidad,
            AccountId = lst_acc[0].id,
            BI_Country__c = Label.BI_Argentina
        );
        insert opp;

        Test.startTest();

        PageReference pag = new PageReference('/');
        pag.getParameters().put('Id', opp.Id);
        Test.setCurrentPageReference(pag);

        BI_NuevoCasoInterno_CTRL obj = new BI_NuevoCasoInterno_CTRL(new ApexPages.StandardSetController(new List <Case>()));
        System.assertEquals(obj.optyId, opp.Id);

        obj.redirect();

        Test.stopTest();
	}	
}