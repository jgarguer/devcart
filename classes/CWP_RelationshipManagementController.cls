public without sharing class CWP_RelationshipManagementController {

    @auraEnabled
    public static map<string, string> getServiceTrackingInfo(){
        system.debug('llamada a recogida de datos pra service tracking');
        
        User currentUser = CWP_LHelper.getCurrentUser(null);
        Account currentAccount = CWP_LHelper.getCurrentAccount(currentUser.Contact.BI_Cuenta_activa_en_portal__c);
        map<string, string> retMap = new map<string, string>{
            'title' => label.PCA_Tab_RelMan_menu,
            'imgId' => null
        };
        system.debug('llamada a la funcion que recoge la imagen del fondo   ' + currentUser);
        map<string, string> rcvMap = CWP_LHelper.getPortalInfo('Relationship Management', currentUser, currentAccount);
        if(!rcvMap.isEmpty()){
            retMap.clear();
            retMap.putAll(rcvMap);
        }
        system.debug('mapa de datos que van de vuelta en relationship management' + retMap);
        return retMap;
    }
    
     @auraEnabled
    public static map<string,string> getPermissionSet(){
        map<string,string> mapVisibility = CWP_ProfileHelper.getPermissionSet();
        return mapVisibility;
    }
}