public with sharing class BI_SendMails_Helper {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos  
	Company:       Aborda.es
	Description:   Class that manage before send emails if exist an user in Org. Check the daily limit before send external emails.
	
	History: 
	
	<Date>                  <Author>                <Change Description>
	03/11/2015              Micah Burgos          	Initial Version     
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	

	//FOCO only uses ToAddress


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos  
	Company:       Aborda.es
	Description:   Method used in FOCO classes:

				   BI_FOCO_OppTriggerHelper
				   BI_FOCOBatchInsert
				   BI_FOCOScheduler
				   BI_FOCOBatchDeleteErrorLog
	
	History: 
	
	<Date>                  <Author>                <Change Description>
	03/11/2015              Micah Burgos          	Initial Version     
	07/12/2015              Guillermo Muñoz         Prevent the email send in Test class 
        06/07/2016              Guillermo Muñoz         Fixed Test BI_FOCODeleteErrorLogs_TEST
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static boolean sendMails(List<Messaging.SingleEmailMessage> lst_mail_param, Boolean send_in_test, String option){
		try{
			Set<String> set_emails_to_search = new Set<String>();
			for(Messaging.SingleEmailMessage mail :lst_mail_param){
				system.debug('ToAddresses: ' +  mail.ToAddresses);

				if(mail.ToAddresses != null && !mail.ToAddresses.isEmpty()){
					set_emails_to_search.addAll(mail.ToAddresses);	
				}
			}

			Map<String,User> map_usr = new Map<String,User>();
			if(!set_emails_to_search.isEmpty()){
				for(User usr :[SELECT Id, Email FROM User WHERE Email IN: set_emails_to_search AND IsActive = true]){ 
					map_usr.put(usr.Email, usr);
				}	
			}
			System.debug('map_usr: ' + map_usr);

			List<Messaging.SingleEmailMessage> lst_usr_emails = new List<Messaging.SingleEmailMessage>();
			List<Messaging.SingleEmailMessage> lst_external_emails = new List<Messaging.SingleEmailMessage>();

			for(Messaging.SingleEmailMessage mail :lst_mail_param){
				System.debug('mail.ToAddresses: ' +mail.ToAddresses);
				Set<String> copy_email_address = new Set<String>();
				copy_email_address.addAll(mail.ToAddresses);
				
				for(String address :mail.ToAddresses){
					System.debug('address: ' +address);
					if(map_usr.containsKey(address)){
						if(!Test.isRunningTest()){ //GM 20160706
								Messaging.SingleEmailMessage cloned_mail = clone_email(mail);
								cloned_mail.setToAddresses(null);
								cloned_mail.setSaveAsActivity(false);
								cloned_mail.settargetObjectId(map_usr.get(address).Id);
								
								System.debug('BEFORE - copy_email_address: ' + copy_email_address);
								System.debug('Se elimina: mail.ToAddresses'+address);
								copy_email_address.remove(address); //Eliminar el email_string que pasa a ser usuario_id.
								System.debug('AFTER: copy_email_address: ' + copy_email_address);
		
								lst_usr_emails.add(cloned_mail);
						}
					}
				}
				System.debug('BEFORE - mail.ToAddresses: ' + mail.ToAddresses);
				List<String> lst_final_external_emails = new List<String>();
				lst_final_external_emails.addAll(copy_email_address);
				mail.setToAddresses(lst_final_external_emails);
				System.debug('AFTER - mail.ToAddresses: ' + mail.ToAddresses);

				if(!mail.ToAddresses.isEmpty()){ //Si quedan emails que no se encuentra usuario, se envía pero comprobando el límite y los test.
					lst_external_emails.add(mail);
				}

			}
			System.debug('Size de lista de emails internos): ' + lst_usr_emails.size());
			System.debug('lista de emails internos): ' + lst_usr_emails);
			System.debug('lista de emails externos): ' + lst_external_emails);

			//Send users emails 
			if(!lst_usr_emails.isEmpty()){
				if(!Test.isRunningTest()){
					Messaging.sendEmail(lst_usr_emails);
				}
			}else{
				BI_LogHelper.generateLog('BI_SendMails_Helper.sendMails_FOCO', 'BI_EN', 'No hay emails INTERNOS para enviar', 'Apex Class');
			}
			

			//Send external emails
			System.debug('Email Limits Status:  ' + Limits.getEmailInvocations() + ' email calls out of ' + Limits.getLimitEmailInvocations() + ' allowed');
			
			
			if(!lst_external_emails.isEmpty() && Test.isRunningTest() && !send_in_test ){
				BI_LogHelper.generateLog('BI_SendMails_Helper.sendMails_FOCO', 'BI_EN', 'Mensajes a usuarios externos no enviados debido a que se ha configurado que no se envíen en test.', 'Apex Class');
				return false;
			}

			//Check límits for external emails.
			try{
				if(!lst_external_emails.isEmpty()){
					System.debug('Reservando(size de lista de emails externos): ' + lst_external_emails.size());
					Messaging.reserveSingleEmailCapacity(lst_external_emails.size());
					if(!Test.isRunningTest()){
						Messaging.sendEmail(lst_external_emails);
					}
				}else{
					System.debug('###No hay emails EXTERNOS para enviar');
				}
			}catch(Exception exc){
				System.debug('###No se pueden enviar más emails externos.');
				BI_LogHelper.generate_BILog('BI_SendMails_Helper.sendMails_FOCO', 'BI_EN', Exc, 'Helper Class');
				return false;
			}

			return true;
		}catch(Exception exc){
			BI_LogHelper.generate_BILog('BI_SendMails_Helper.sendMails_FOCO', 'BI_EN', Exc, 'Helper Class');
			return false;
		}
	}

	//To clone email messages: 

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos  
	Company:       Aborda.es
	Description:   Method that clone email messages.
	
	History: 
	
	<Date>                  <Author>                <Change Description>
	03/11/2015              Micah Burgos          	Initial Version     
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	public static Messaging.SingleEmailMessage clone_email(Messaging.SingleEmailMessage original_email){
		Messaging.SingleEmailMessage cloned_email = new Messaging.SingleEmailMessage();
			//Base email methods:
			cloned_email.setBccSender(original_email.BccSender);
			cloned_email.setReplyTo(original_email.ReplyTo);
			cloned_email.setTemplateID(original_email.TemplateID);
			cloned_email.setSaveAsActivity(original_email.SaveAsActivity);
			cloned_email.setSenderDisplayName(original_email.SenderDisplayName);
			cloned_email.setUseSignature(original_email.UseSignature);

			//Single mail methods
			cloned_email.setBccAddresses(original_email.BccAddresses);
			cloned_email.setCcAddresses(original_email.CcAddresses);
			cloned_email.setCharset(original_email.Charset);
			cloned_email.setDocumentAttachments(original_email.DocumentAttachments);
			//cloned_email.setEntityAttachments(original_email.EntityAttachments); //Can´t clone attachments
			cloned_email.setFileAttachments(original_email.FileAttachments);
			cloned_email.setHtmlBody(original_email.HtmlBody);
			cloned_email.setInReplyTo(original_email.InReplyTo);
			//cloned_email.setOptOutPolicy(original_email.OptOutPolicy);
			cloned_email.setPlainTextBody(original_email.PlainTextBody);
			cloned_email.setOrgWideEmailAddressId(original_email.OrgWideEmailAddressId);
			cloned_email.setReferences(original_email.References);
			cloned_email.setSubject(original_email.Subject);
			cloned_email.setTargetObjectId(original_email.TargetObjectId);
			cloned_email.setToAddresses(original_email.ToAddresses);
			//cloned_email.setTreatBodiesAsTemplate(original_email.TreatBodiesAsTemplate);
			//cloned_email.setTreatTargetObjectAsRecipient(original_email.TreatTargetObjectAsRecipient);
			cloned_email.setWhatId(original_email.WhatId);

		return cloned_email;
	}

}