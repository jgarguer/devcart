public with sharing class BI_CatalogoServicioMethods {
		/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by BI_Catalogo_de_servicio__c Triggers 
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
	 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if BI_Solicitud_de_plan__c Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void blockEdition(List <BI_Catalogo_de_servicio__c> news){
        try{
            if(BI_GlobalVariables.stopTriggerDesc == false){
            	Set <Id> set_cat = new Set <Id>();
            	for(BI_Catalogo_de_servicio__c cat:news){        		
            		set_cat.add(cat.BI_Plan__c);        		
            	}

            	List <BI_Solicitud_de_plan__c> lst_plan = [SELECT Id, BI_Estado_solicitud__c FROM BI_Solicitud_de_plan__c WHERE Id IN :set_cat];
            	for (BI_Solicitud_de_plan__c plan:lst_plan){
            		for(BI_Catalogo_de_servicio__c cat:news){
            			if (plan.Id == cat.BI_Plan__c && 
                            (plan.BI_Estado_solicitud__c == label.BI_BolsaDineroCancelado ||
                             plan.BI_Estado_solicitud__c == 'Rechazado' ||
                             plan.BI_Estado_solicitud__c == 'Activado' || 
                             plan.BI_Estado_solicitud__c == 'Aprobado')){
            				cat.addError(label.BI_BloqueoCatServ);
            			}	
            		}
            		
            	}
            }
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoModeloMethods.blockEdition', 'BI_EN', exc, 'Trigger');
        }
    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if BI_Solicitud_de_plan__c Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void blockDelete(List <BI_Catalogo_de_servicio__c> olds){
        try{
        	Set <Id> set_cat = new Set <Id>();
        	for(BI_Catalogo_de_servicio__c cat:olds){        		
        		set_cat.add(cat.BI_Plan__c);        		
        	}

        	List <BI_Solicitud_de_plan__c> lst_plan = [SELECT Id, BI_Estado_solicitud__c FROM BI_Solicitud_de_plan__c WHERE Id IN :set_cat];
        	for (BI_Solicitud_de_plan__c plan:lst_plan){
        		for(BI_Catalogo_de_servicio__c cat:olds){
        			if (plan.Id == cat.BI_Plan__c && 
                        (plan.BI_Estado_solicitud__c == label.BI_BolsaDineroCancelado ||
                         plan.BI_Estado_solicitud__c == 'Rechazado' ||
                         plan.BI_Estado_solicitud__c == 'Activado' || 
                         plan.BI_Estado_solicitud__c == 'Aprobado')){
        				cat.addError(label.BI_CatServDelete);
        			}	
        		}
        		
        	}
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoModeloMethods.blockEdition', 'BI_EN', exc, 'Trigger');
        }
    }
    
    
    public static void validatePlan(List <BI_Catalogo_de_servicio__c> news){
    	
    	Set<Id> set_plan = new Set<Id>();
    	
    	for(BI_Catalogo_de_servicio__c cat_serv:news)
    		set_plan.add(cat_serv.BI_Plan__c);
    		
    	system.debug(set_plan);
    		
    	if(!set_plan.isEmpty()){
    		Map<Id, BI_Solicitud_de_plan__c> map_sol = new Map<Id, BI_Solicitud_de_plan__c>([select Id, BI_Tipo_de_servicio__c from BI_Solicitud_de_plan__c 
    																						 where Id IN :set_plan]);
    																						 
			system.debug(map_sol);
    		
    		for(BI_Catalogo_de_servicio__c cat_serv2:news){
    			if(map_sol.get(cat_serv2.BI_Plan__c).BI_Tipo_de_servicio__c != 'Por defecto')
    				cat_serv2.addError('No puede asignar servicios si el tipo de servicio no es "Por defecto"');
    		}
    		
    	}
    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Validates a Plan has only one 'Catálogo Servicio' associated to the same 'Servicio' record
    
    IN:            ApexTrigger.New
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    01/04/2015              Fernando Arteaga        Initial Version         
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void validatePlanServiceDuplicated(List <BI_Catalogo_de_servicio__c> news)
    {
		try
		{
	    	Set<Id> setPlanIds = new Set<Id>();
	    	Set<Id> setServEquipoIds = new Set<Id>();
			Map<String, Integer> mapExistingRecords = new Map<String, Integer>();
	
	    	for (BI_Catalogo_de_servicio__c catalogo: news)
	    	{
	    		setPlanIds.add(catalogo.BI_Plan__c);
	    		setServEquipoIds.add(catalogo.BI_Id_de_servicio__c);
	    	}
	    		
			for (AggregateResult aggResult : [SELECT BI_Plan__c, BI_Id_de_servicio__c, COUNT(Id)
								   			  FROM BI_Catalogo_de_servicio__c
								   			  WHERE BI_Plan__c IN :setPlanIds
								   			  AND BI_Id_de_servicio__c IN :setServEquipoIds
								   			  GROUP BY BI_Plan__c, BI_Id_de_servicio__c])
			{
				String key = String.valueOf(aggResult.get('BI_Plan__c')) + String.valueOf(aggResult.get('BI_Id_de_servicio__c'));
				mapExistingRecords.put(key, (Integer) aggResult.get('expr0'));
			}
			
			System.debug('### mapExistingRecords: ' + mapExistingRecords);
				
			for (BI_Catalogo_de_servicio__c md: news)
			{
				if (mapExistingRecords.containsKey(String.valueOf(md.BI_Plan__c) + String.valueOf(md.BI_Id_de_servicio__c)))
					md.addError('No puede asignar el servicio ya que el Plan ya lo tiene asignado');
			}
		}
		catch (Exception exc)
		{
			BI_LogHelper.generate_BILog('BI_CatalogoServicioMethods.validatePlanServiceDuplicated', 'BI_EN', exc, 'Trigger');
		}
    }
    
}