public with sharing class OrderHeaderView {
	
	private ApexPages.StandardController controller;
	NE__Order_Header__c oh{get;set;}
	
	public OrderHeaderView(ApexPages.StandardController controller) 
	{
		this.controller = controller;
		oh 	= 	(NE__Order_Header__c)controller.getRecord();
		oh	=	[SELECT Id, NE__OrderId__c FROM NE__Order_Header__c WHERE id =: oh.id];
	}
	
	public PageReference redirectToOrder()
	{
		PageReference redirectPage	=	new PageReference('/'+oh.NE__OrderId__c);
		
		return redirectPage;
		
	}

}