@isTest 
private class BI_CampaignStatusCtrl_TEST {

    static testMethod void BI_CampaignStatusCtrl() {
    	
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1);
    	
    	List<BI_CampaignAction__c> list_actions = new List<BI_CampaignAction__c>();
	        
      
         BI_CampaignAction__c oppAct_1 = new BI_CampaignAction__c(
         	 Name= 'Test1', 
	         BI_Content__c = 'Test',
	         BI_Label__c = 'label_test',
	         BI_Pais__c = 'CORE',
	         BI_Position__c = 0
         );
         list_actions.add(oppAct_1); 
         
         BI_CampaignAction__c oppAct_2 = new BI_CampaignAction__c(
         	Name= 'Test2',
	         BI_Content__c = 'Test2',
	         BI_Label__c = 'label_test',
	         BI_Pais__c = lst_pais[0],
	         BI_Position__c = 1
         );
         list_actions.add(oppAct_2);
    	
    	insert list_actions;
    	
    	list<Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(2, BI_DataLoad.obtainCampaignRecordType());
    	
    	
    	
    	
    	Test.startTest();
    	
    	
		
		ApexPages.Standardcontroller ctr = new ApexPages.Standardcontroller(lst_camp[0]);
		BI_CampaignStatusCtrl crt2 = new BI_CampaignStatusCtrl(ctr);
		
		ctr = new ApexPages.Standardcontroller(lst_camp[1]);
		crt2 = new BI_CampaignStatusCtrl(ctr);
		
		crt2.getRenderAprobacion();
		crt2.getRenderCancelada();
		crt2.getRenderEnCurso();
		crt2.getRenderFinalizada();
		crt2.getRenderPlanificacion();
		
		system.assert(!crt2.error);
			
		crt2.sendToApproval();
		
		Test.stopTest();
    }
    
     static testMethod void BI_CampaignStatusCtrl_Aprobacion() {
     	list<Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(1, BI_DataLoad.obtainCampaignRecordType());
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
     	
     	lst_camp[0].Status = Label.BI_EnAprobacion;
     	
     	update lst_camp[0];
     	ApexPages.Standardcontroller ctr = new ApexPages.Standardcontroller(lst_camp[0]);
		BI_CampaignStatusCtrl crt2 = new BI_CampaignStatusCtrl(ctr);	

        system.assert(!crt2.error);
     }
     
     static testMethod void BI_CampaignStatusCtrl_EnCurso() {
     	list<Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(1, BI_DataLoad.obtainCampaignRecordType());
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
     	
     	lst_camp[0].Status = Label.BI_EnCurso;
     	
     	update lst_camp[0];
     	ApexPages.Standardcontroller ctr = new ApexPages.Standardcontroller(lst_camp[0]);
		BI_CampaignStatusCtrl crt2 = new BI_CampaignStatusCtrl(ctr);
		
		system.assert(!crt2.error);
		
     }
     
     static testMethod void BI_CampaignStatusCtrl_Finalizada() {
     	list<Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(1, BI_DataLoad.obtainCampaignRecordType());
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
     	
     	lst_camp[0].Status = Label.BI_Finalizada;
     	
     	update lst_camp[0];
     	ApexPages.Standardcontroller ctr = new ApexPages.Standardcontroller(lst_camp[0]);
		BI_CampaignStatusCtrl crt2 = new BI_CampaignStatusCtrl(ctr);
		
		system.assert(!crt2.error);
		
     }
     
     static testMethod void BI_CampaignStatusCtrl_Cancelada() {
     	list<Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(1, BI_DataLoad.obtainCampaignRecordType());
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
     	
     	lst_camp[0].Status = Label.BI_Cancelada;
     	
     	update lst_camp[0];
     	ApexPages.Standardcontroller ctr = new ApexPages.Standardcontroller(lst_camp[0]);
		BI_CampaignStatusCtrl crt2 = new BI_CampaignStatusCtrl(ctr);
		system.assert(!crt2.error);
		
     }
     
     static testMethod void BI_CampaignStatusCtrl_Grey() {
     	list<Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(1, BI_DataLoad.obtainCampaignRecordType());
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
     	
     	lst_camp[0].Status = 'testStatus' ;
     	
     	update lst_camp[0];
     	ApexPages.Standardcontroller ctr = new ApexPages.Standardcontroller(lst_camp[0]);
		BI_CampaignStatusCtrl crt2 = new BI_CampaignStatusCtrl(ctr);
		
		system.assert(!crt2.error);
		
     }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}