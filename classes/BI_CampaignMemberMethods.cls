public class BI_CampaignMemberMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Methods executed by the CampaignMember trigger.
   
    History:
    
    <Date>            <Author>          <Description>
    14/04/2014        Pablo Oliva       Initial version
    15/04/2014        Pablo Oliva       Method  "createTaskPreOpp" added
    03/06/2014        Pablo Oliva       Method  "preventDelete" updated
    05/06/2014        Pablo Oliva       Method  "preventStatusBlockRecord" updated
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that prevents from deleting CampaignMembers 
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    14/04/2014        Pablo Oliva       Initial version
    03/06/2014        Pablo Oliva       Users with Profile 'System Administrator' can delete records
    15/09/2014        Ignacio Llorca    try/catch removed, the method is created to force an error
    22/11/2014        Pablo Oliva       Campaign Owner can delete records
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void preventDelete(Map<Id, CampaignMember> olds){
            
        Map<Id, CampaignMember> map_olds = new Map<Id, CampaignMember>([select Id, CampaignId, Campaign.OwnerId from CampaignMember where Id IN :olds.keySet()]);
        
        String permission = BI_UserMethods.getPermission();
        system.debug('NO PERMISOS PARA BORRAR CAMPAÑA: User.BI_Permisos__c = '+permission+ ' | Permissions allowed: '+ Label.BI_Administrador_del_sistema);
        
        for(CampaignMember cmem:olds.values()){
            if(UserInfo.getUserId() != map_olds.get(cmem.Id).Campaign.OwnerId && permission != Label.BI_Administrador_del_sistema)
                cmem.addError(Label.BI_Trigger_CampaignMember_preventDelete);
        }
            
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that prevents from inserting CampaignMembers with Status 'Aprobado' or 'Rechazado'.
                   Or else, if the related Campaign is active, the CampaignMember is blocked and with status 'Pendiente de Aprobación'

    
    IN:            ApexTrigger.New
    OUT:           Void
    
    History:
    
    <Date>            <Author>                          <Description>
    14/04/2014        Pablo Oliva                       Initial version
    05/06/2014        Pablo Oliva                       Only works if the User is not the owner of the related Campaign
    18/11/2015        Miguel Cabrera/Antonio Pardo      Check BI_Aprobacion_miembros_de_campana__c. If is true Aprove directly the campaign member
    30/11/2015        Miguel Cabrera/Antonio Pardo      Status can be now any
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void preventStatusBlockRecord(List<CampaignMember> news){
        try{
             
             if(BI_TestUtils.isRunningTest()){
                   throw new BI_Exception('test');
              }

            Set<Id> set_camp = new Set<Id>();
            
            for(CampaignMember cm0:news)
                set_camp.add(cm0.CampaignId);
                
            Map<Id, Campaign> map_camp = new Map<Id, Campaign>([SELECT Id, OwnerId, BI_Aprobacion_miembros_de_campana__c FROM Campaign WHERE Id IN :set_camp]);
       
            if(!set_camp.isEmpty()){

                Set<Id> set_c = new Set<Id>();
                
                for(CampaignMember cm2:news){
                    if(map_camp.get(cm2.CampaignId) != null){
                        if(map_camp.get(cm2.CampaignId).BI_Aprobacion_miembros_de_campana__c == false && map_camp.get(cm2.CampaignId).OwnerId != UserInfo.getUserId()){
                            cm2.BI_Bloqueo__c = true;
                            cm2.Status = Label.BI_PendienteAprobacion;
                            set_c.add(cm2.CampaignId);
                            
                        }
                        //GMN 09/02/2015 Deprecated for bux in deploy
                       /* else{
                            cm2.BI_Bloqueo__c = true;
                            set_c.add(cm2.CampaignId);
                        }*/
                        
                    }
                }
                
                 if(!set_c.isEmpty())
                    BI_CampaignMemberHelper.createCampaignMemberStatus(set_c);
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_CampaignMemberMethods.preventStatusBlockRecord', 'BI_EN', Exc, 'Trigger');
        }
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   When a CampaingMember is created, if the related Campaign is active, a new BI_Aprobacion_Miembros_Campana__c record is created and sent to 
                   Approval
    
    IN:            ApexTrigger.New
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    14/04/2014        Pablo Oliva       Initial version
    30/04/2014        Pablo Oliva       Change: BI_Aprobador__c = CampaignMember.Campaign.OwnerId
    05/06/2014        Pablo Oliva       Change: The campaign may be inactive
    05/06/2014        Pablo Oliva       Only works if the User is not the owner of the related Campaign
    10/09/2014        Ignacio Llorca    Only works for certain profiles
    18/09/2014        Ignacio Llorca    Only works for certain permissions
    02/12/2015        Francsico Ayllon  Added condition BI_Aprobacion_miembros_de_campana__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void sendToApproval(List<CampaignMember> news){
        try{
             if(BI_TestUtils.isRunningTest()){
                   throw new BI_Exception('test');
              }
              
            List<BI_Aprobacion_Miembros_Campana__c> list_amc = new List<BI_Aprobacion_Miembros_Campana__c>();
            List<CampaignMember> lst_cm = new List<CampaignMember>();
            
            Set<Id> set_campaign = new Set<Id>();
            for(CampaignMember cm0:news)
                set_campaign.add(cm0.CampaignId);
                
            Map<Id, Campaign> map_campaign = new Map<Id, Campaign>([select Id, OwnerId, BI_Aprobacion_miembros_de_campana__c, Owner.ManagerId, IsActive from Campaign where Id IN:set_campaign]);
            
            String permission = BI_UserMethods.getPermission();
            
            for(CampaignMember cmem:news){
                
                if(map_campaign.get(cmem.CampaignId) != null && UserInfo.getUserId() != map_campaign.get(cmem.CampaignId).OwnerId && (permission != Label.BI_Administrador_del_sistema || permission != Label.BI_Usuario_de_Integracion) && map_campaign.get(cmem.CampaignId).BI_Aprobacion_miembros_de_campana__c == false){
                
                    BI_Aprobacion_Miembros_Campana__c amc = new BI_Aprobacion_Miembros_Campana__c(BI_Nombre_Campana__c = cmem.CampaignId,
                                                                                                  BI_Estado__c = Label.BI_PendienteAprobacion,
                                                                                                  BI_Contacto__c = cmem.ContactId,
                                                                                                  BI_Aprobador__c = map_campaign.get(cmem.CampaignId).OwnerId,
                                                                                                  BI_Miembro_de_Campana__c = cmem.Id,
                                                                                                  BI_Prospecto__c = cmem.LeadId);
                    system.debug('# Inserta miembro de campaña');                                                                                               
                    list_amc.add(amc);
                }
            }
            
            insert list_amc;
            
            List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest> ();
            
            for(BI_Aprobacion_Miembros_Campana__c amc:list_amc){
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments('Submitting request for approval.');
                req1.setObjectId(amc.id);
                
                requests.add(req1);
            }
                 
            List<Approval.ProcessResult> processResults = null;
            
            processResults = Approval.process(requests, true);
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_CampaignMemberMethods.sendToApproval', 'BI_EN', Exc, 'Trigger');
        }
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that prevents from updating CampaignMembers if status is 'Rechazado' or BI_Bloqueo__c is true
    
    IN:            ApexTrigger.old, ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    14/04/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void preventUpdate(List<CampaignMember> news, List<CampaignMember> olds){
        try{
             
             if(BI_TestUtils.isRunningTest()){
                   throw new BI_Exception('test');
              }

            if(!BI_GlobalVariables.stopPreventUpdate){
            
                Integer i = 0;
            
                for(CampaignMember cmem:news){
                    if(olds[i].BI_Bloqueo__c || olds[i].Status == Label.BI_Rechazado || olds[i].BI_Editable__c == false)
                        cmem.addError(Label.BI_Trigger_CampaignMember_preventUpdate);
                        
                    i++;
                }
            
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_CampaignMemberMethods.preventUpdate', 'BI_EN', Exc, 'Trigger');
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   If the CampaignMembers status change to 'Aprobado', a set with the CampaignMembers ids is sent to the method: 
                   BI_CampaignMemberHelper.createTaskPreOpp 
    
    IN:            ApexTrigger.old, ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    15/04/2014        Pablo Oliva       Initial version
    02/11/2015        Micah Burgos      eHelp 01419310 - Do not send a message after insert a CampaingMember.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void createTaskPreOpp(List<CampaignMember> news, List<CampaignMember> olds){
        try{
             
             if(BI_TestUtils.isRunningTest()){
                   throw new BI_Exception('test');
              }

            Set<Id> set_campaignMembers = new Set<Id>();
            Set<Id> set_camp = new Set<Id>();
            
          //Integer i = 0;
          //  system.debug('*** CampaignMember: ' + news);
            for(CampaignMember cm0:news){
                //system.debug('*** New Status: ' + cm0.Status);
                ////system.debug('*** Old Status: ' + olds[i].Status);
                //if(olds == null || cm0.Status != olds[i].Status){
                    set_campaignMembers.add(cm0.Id);
                    set_camp.add(cm0.CampaignId);
              //  } 
              //i++;
            }
            
            if(!set_campaignMembers.isEmpty()){
                //if(!Test.isRunningTest()){
                system.debug('createTaskPreOpp: -> set_campaignMembers = ' + set_campaignMembers);
                    System.enqueueJob(new BI_CampaignMember_Queueable(set_campaignMembers, 1, set_camp));
          //  }else{
              //BI_DataLoad.simulate_job_CampaignMember_Queueable(set_campaignMembers, 1);
          //  }

                    //eHelp 01419310 - Al añadir miembros de campaña no se debe enviar correo.

    /*              List<EmailTemplate> lst_tmpl = [SELECT DeveloperName,Id,Name FROM EmailTemplate WHERE DeveloperName = 'BI_Comienzo_Activacion_Campana'];
                    if(!lst_tmpl.isEmpty()){
                        Contact dummyContact = new Contact(LastName = 'noreply', Email = 'noreply@telefonica.com');
                        List<Messaging.SingleEmailMessage> lst_mails = new List<Messaging.SingleEmailMessage>();
                        
                        insert dummyContact;

                        List<Campaign> lst_campEmail = [SELECT Id, OwnerId ,Owner.Email, LastModifiedById, LastModifiedBy.Email FROM Campaign WHERE Id IN :set_camp];

                        for(Campaign camp :lst_campEmail){
                            
                            Messaging.SingleEmailMessage mailOwner = new Messaging.SingleEmailMessage();

                                //if(map_contacts.containsKey(camp.Owner.Email)){
                                //  mailOwner.setTargetObjectId(map_contacts.get(camp.Owner.Email).Id);
                                //  mailOwner.setWhatId(camp.Id);  
                                //}else{
                                //  lst_toCreateContact.add(camp.Owner.Email);
                                //}
                                //mailOwner.setTargetObjectId(camp.OwnerId);
                                mailOwner.setTargetObjectId(dummyContact.Id);
                                                            
                                mailOwner.setTemplateId(lst_tmpl[0].Id);
                                mailOwner.setWhatId(camp.Id);  
                                //mailOwner.setBccSender(false);
                                //mailOwner.setUseSignature(false);
                                mailOwner.setReplyTo('noreply@telefonica.com');
                                mailOwner.setSenderDisplayName('Telefónica BI_EN');
                                mailOwner.setSaveAsActivity(false);

                                List<String> lst_toBcc = new List<String>();
                                lst_toBcc.add(camp.LastModifiedBy.Email);
                                if(camp.LastModifiedById != camp.OwnerId){
                                    lst_toBcc.add(camp.Owner.Email);
                                }
                                mailOwner.setToAddresses(lst_toBcc);

                                lst_mails.add(mailOwner);
                        }
                        system.debug('lst_mails' +lst_mails);
                        try{
                            Messaging.sendEmail(lst_mails);
                        }catch(exception Exc){
                            BI_LogHelper.generate_BILog('BI_CampaignMemberMethods.createTaskPreOpp', 'BI_EN', Exc, 'Trigger');
                        }
                        
                        delete dummyContact;
                    }else{
                        BI_LogHelper.generateLog('BI_CampaignMemberMethods.createTaskPreOpp.sendEmail', 'BI_EN', 'No se encuentra plantilla de email: BI_Comienzo_Activacion_Campana', 'Trigger');
                    }*/

/*                  if(Test.isRunningTest())
                        BI_DataLoad.simulate_job_CampaignMember_Queueable(set_campaignMembers, 1);*/
                        
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_CampaignMemberMethods.createTaskPreOpp', 'BI_EN', Exc, 'Trigger');
        }
    }
    
    
    public static void manageCampanasCuenta(List<CampaignMember> lst_cmember, Integer option){
        
        try{
             if(BI_TestUtils.isRunningTest()){
                   throw new BI_Exception('test');
              }
              
            Map<String, Integer> map_cmembers = new Map<String, Integer>();
                
            Set<Id> set_contact = new Set<Id>();
            List<CampaignMember> lst_cmember2 = new List<CampaignMember>();
                
            for(CampaignMember cmember:lst_cmember){
                if(cmember.ContactId != null){
                    lst_cmember2.add(cmember);
                    set_contact.add(cmember.ContactId);
                }
            }
                
            if(!lst_cmember2.isEmpty()){
                
                system.debug('***1');
                    
                Map<Id, Id> map_contact = new Map<Id, Id>();
                //Refactored BI_Paises_Ref__r.BI_Codigo_ISO__c = 'PER' to BI_Country__c = 'Peru'
                for(Contact contact:[select Id, AccountId from Contact where Id IN :set_contact AND BI_Country__c = 'Peru'])
                    map_contact.put(contact.Id, contact.AccountId);
                        
                if(!map_contact.isEmpty()){ 
                    
                    system.debug('***2');
                    
                    Map<String, Integer> map_ccuenta = new Map<String, Integer>();
                    for(CampaignMember cmember2:lst_cmember2){
                            
                        system.debug('***5');
                            
                        if(map_contact.containsKey(cmember2.ContactId)){
                            
                            system.debug('***6');
                            
                            String key = map_contact.get(cmember2.ContactId)+'-'+cmember2.CampaignId;
                            if(map_ccuenta.containsKey(key)){
                                
                                system.debug('***7');
                                
                                Integer cont = map_ccuenta.get(key) + 1;
                                map_ccuenta.put(key, cont);
                                
                            }else{
                                    
                                map_ccuenta.put(key, 1);
                                    
                            }
                        }
                            
                    }
                    
                    system.debug('***3 '+map_ccuenta);
                        
                    if(!map_ccuenta.isEmpty()){
                            
                        Map<String, BI_Campanas_Cuenta__c> map_records = new Map<String, BI_Campanas_Cuenta__c>();
                        for(BI_Campanas_Cuenta__c campC:[select Id, BI_Quantity__c, Name from BI_Campanas_Cuenta__c where Name IN :map_ccuenta.keySet()])
                            map_records.put(campC.Name, campC);
                            
                        for(String key2:map_ccuenta.keySet()){
                            if(map_records.containsKey(key2)){
                                if(option == 0)
                                    map_records.get(key2).BI_Quantity__c += map_ccuenta.get(key2);
                                else
                                    map_records.get(key2).BI_Quantity__c -= map_ccuenta.get(key2);
                            }else{
                                    
                                BI_Campanas_Cuenta__c camC = new BI_Campanas_Cuenta__c(Name = key2,
                                                                                       BI_Quantity__c = map_ccuenta.get(key2));
                                                                                           
                                map_records.put(camC.Name, camC);
                                    
                            }
                                        
                        }
                        
                        system.debug('***4 '+map_records);
                            
                        if(option == 0)
                            upsert map_records.values();
                        else{
                            
                            List<BI_Campanas_Cuenta__c> lst_update = new List<BI_Campanas_Cuenta__c>();
                            List<BI_Campanas_Cuenta__c> lst_delete = new List<BI_Campanas_Cuenta__c>();
                            
                            for(BI_Campanas_Cuenta__c caC:map_records.values()){
                                if(caC.BI_Quantity__c > 0)
                                    lst_update.add(caC);
                                else
                                    lst_delete.add(caC);
                            }
                            
                            update lst_update;
                            delete lst_delete;
                            
                        }
                            
                    }
                        
                }
    
            }
        
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_CampaignMemberMethods.manageCampanasCuenta', 'BI_EN', exc, 'Trigger');
        }
        
    } 
    
    
}