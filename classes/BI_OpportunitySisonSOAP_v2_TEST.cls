@isTest
private class BI_OpportunitySisonSOAP_v2_TEST {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test setup
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    31/08/2016                      Guillermo Muñoz             Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@TestSetup static void loadInfo(){

		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        BI_TestUtils.throw_exception = false;

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),true,true,false,false);

        //Inser catalog
		NE__Catalog__c cat = new NE__Catalog__c(
			Name = 'Chile Empresas'
		);
        insert cat;

        //Insert product
        NE__Product__c prod = new NE__Product__c(
        	BI_Id_Producto_Gala__c = 'TestProd',
        	Name = 'TestProd'
       	);
       	insert prod;

       	//Insert catalog item
        NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod.Id, NE__Change_Subtype__c = 'test', NE__Disconnect_Subtype__c = 'test');
        insert catIt;

        //Insert account
       	List <Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Chile});
       	lst_acc[0].BI_Tipo_de_identificador_fiscal__c = 'RUT';
       	lst_acc[0].BI_No_Identificador_fiscal__c = '796878304';
       	update lst_acc[0];

       	//Insert opportunity
       	Opportunity opp = new Opportunity(
            Name = 'Test 1',
            CloseDate = Date.today(),
            StageName = Label.BI_F5DefSolucion,
            AccountId = lst_acc[0].Id,
            BI_Country__c = Label.BI_Chile
        );
        insert opp;

        if(mapa != null){
        	BI_MigrationHelper.disableBypass(mapa);
        }
	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       NEAborda
	    Description:   Test method that manages the code coverage from BI_OpportunitySisonSOAP_v2 requests
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    31/08/2016                      Guillermo Muñoz             Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void allRequestTEST(){

		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        BI_TestUtils.throw_exception = false;

		User integracionCHI = [SELECT Id FROM User WHERE Name = 'Integración chi' lIMIT 1];
		String idOpp = '796878304_' + [SELECT BI_Numero_id_oportunidad__c FROM Opportunity Limit 1].BI_Numero_id_oportunidad__c;
		
		/////////////////////////////////////rellenamos los datos de los productos///////////////////////////////////////////////////////////
		BI_OpportunitySisonSOAP_v2.ProductInfoType pInfoType = new BI_OpportunitySisonSOAP_v2.ProductInfoType();

		pInfoType.Integration_id = '0000000001';
        pInfoType.Divisa = 'CLP';
        pInfoType.Ingreso_por_unica_vez = 234;
        pInfoType.Recurrente_bruto_mensual = 234;
        pInfoType.Ingreso_por_unica_vez_descuento = null;
        pInfoType.Id_del_producto = 'TestProd';
        pInfoType.Cantidad = 1;
        pInfoType.Recurrente_bruto_mensual_descuento = null;
        pInfoType.Linea_padre = null;
        pInfoType.AccionSISON = 'CREATE';
        pInfoType.Recurrente_bruto_mensual_anterior = 0;

        List <BI_OpportunitySisonSOAP_v2.ProductInfoType> lst_pInfoType = new List <BI_OpportunitySisonSOAP_v2.ProductInfoType>();
        lst_pInfoType.add(pInfoType);
        /////////////////////////////////////////////////////////fin productos///////////////////////////////////////////////////////////////

		///////////////////////////////////////////////rellenamos los datos de la order//////////////////////////////////////////////////////
		BI_OpportunitySisonSOAP_v2.ConfigurationInfoType cInfoType = new BI_OpportunitySisonSOAP_v2.ConfigurationInfoType();

		cInfoType.Integration_id = '0000000001';
        cInfoType.Divisa = 'CLP';
        cInfoType.Cuota_total_por_unica_vez = 0;
        cInfoType.Cargo_total_recurrente = 0;
        cInfoType.Catalogo = 'Empresas';
        cInfoType.Modelo_comercial = 'uno';
        cInfoType.AccionSISON = 'CREATE';
        cInfoType.Fecha_Hora_creacion_version = Datetime.now();
        cInfoType.Productos = lst_pInfoType;

        List <BI_OpportunitySisonSOAP_v2.ConfigurationInfoType> lst_cInfoType = new List <BI_OpportunitySisonSOAP_v2.ConfigurationInfoType>();
        lst_cInfoType.add(cInfoType);
        /////////////////////////////////////////////////////////////fin order///////////////////////////////////////////////////////////////

		//////////////////////////////////////////////rellenamos los datos de la oportunidad/////////////////////////////////////////////////
		BI_OpportunitySisonSOAP_v2.OpportunityInfoType oInfoType = new BI_OpportunitySisonSOAP_v2.OpportunityInfoType();

		oInfoType.Fecha_de_cierre = Date.today().addDays(2);
        oInfoType.Etapa = 'F3 - Presentación de oferta';
        oInfoType.Motivo_de_perdida = null;
        oInfoType.Duracion_del_contrato_Meses = 12;
        oInfoType.Fecha_entrega_oferta = Date.today().addDays(20);
        oInfoType.Fecha_de_vigencia = Date.today().addDays(20);
        oInfoType.Comienzo_estimado_de_facturacion = null;
        oInfoType.Ingreso_por_unica_vez = 234;
        oInfoType.Recurrente_bruto_mensual = 234;
        oInfoType.Venta_bruta_total_FCV = 0;
        oInfoType.Probabilidad_de_exito = '50';
        oInfoType.Plazo_estimado_de_provision_dias = 0;
        oInfoType.Id_Oportunidad_SF = idOpp;
        oInfoType.Tasa_de_interes = 1.01097885195017;
        oInfoType.Tipo_de_renegociacion = null;
        oInfoType.Recurrente_bruto_mensual_anterior = 0;
        oInfoType.Tipo_de_nuevo_resgistro = null;
        oInfoType.Divisa = 'CLP';
        oInfoType.Descripcion = 'Test 1';
        oInfoType.Descripcion_cierre = null;
        oInfoType.Tipo_de_oportunidad = null;        
        oInfoType.Versiones = lst_cInfoType;
        //////////////////////////////////////////////////////////fin oportunidad////////////////////////////////////////////////////////////

        
        ////////////////////////INSERT////////////////////////////
        System.runAs(integracionCHI){
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);
    	}
    	Test.startTest();
    	////////////////////////UPDATE///////////////////////////
    	lst_cInfoType[0].Productos[0].AccionSISON = 'UPDATE';
    	lst_cInfoType[0].AccionSISON = 'UPDATE';
    	oInfoType.Versiones = lst_cInfoType;

    	System.runAs(integracionCHI){
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);
    	}

    	////////////////////////DELETE///////////////////////////
    	lst_cInfoType[0].Productos[0].AccionSISON = 'DELETE';
    	oInfoType.Versiones = lst_cInfoType;

    	System.runAs(integracionCHI){
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);
    	}
    	Test.stopTest();
	}
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       NEAborda
	    Description:   Test method that manages the code coverage from the errors in the Opportunity request of BI_OpportunitySisonSOAP_v2
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    31/08/2016                      Guillermo Muñoz             Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void errorsOppTest(){

		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        BI_TestUtils.throw_exception=false;

		User integracionCHI = [SELECT Id FROM User WHERE Name = 'Integración chi' lIMIT 1];
		String idOpp = '796878304_' + [SELECT BI_Numero_id_oportunidad__c FROM Opportunity Limit 1].BI_Numero_id_oportunidad__c;
		
		//////////////////////////////////////////////rellenamos los datos de la oportunidad/////////////////////////////////////////////////
		BI_OpportunitySisonSOAP_v2.OpportunityInfoType oInfoType = new BI_OpportunitySisonSOAP_v2.OpportunityInfoType();

		oInfoType.Fecha_de_cierre = Date.today().addDays(2);
        oInfoType.Etapa = 'F3 - Presentación de oferta';
        oInfoType.Motivo_de_perdida = null;
        oInfoType.Duracion_del_contrato_Meses = 12;
        oInfoType.Fecha_entrega_oferta = Date.today().addDays(20);
        oInfoType.Fecha_de_vigencia = Date.today().addDays(20);
        oInfoType.Comienzo_estimado_de_facturacion = null;
        oInfoType.Ingreso_por_unica_vez = 234;
        oInfoType.Recurrente_bruto_mensual = 234;
        oInfoType.Venta_bruta_total_FCV = 0;
        oInfoType.Probabilidad_de_exito = '50';
        oInfoType.Plazo_estimado_de_provision_dias = 0;
        oInfoType.Id_Oportunidad_SF = idOpp;
        oInfoType.Tasa_de_interes = 1.01097885195017;
        oInfoType.Tipo_de_renegociacion = null;
        oInfoType.Recurrente_bruto_mensual_anterior = 0;
        oInfoType.Tipo_de_nuevo_resgistro = null;
        oInfoType.Divisa = 'CLP';
        oInfoType.Descripcion = 'Test 1';
        oInfoType.Descripcion_cierre = null;
        oInfoType.Tipo_de_oportunidad = null;        
        oInfoType.Versiones = null;
        //////////////////////////////////////////////////////////fin oportunidad////////////////////////////////////////////////////////////

        System.runAs(integracionCHI){
        	
        	//Oportunidad sin Id
        	oInfoType.Id_Oportunidad_SF = null;
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

        	//Oportunidad con Id incorrecto
        	oInfoType.Id_Oportunidad_SF = 'fail_fail';
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

        	//Oportunidad sin fecha de cierre
        	oInfoType.Id_Oportunidad_SF = idOpp;
        	oInfotype.Fecha_de_cierre = null;
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

        	//Oportunidad sin etapa
        	oInfotype.Fecha_de_cierre = Date.today().addDays(2);
        	oInfotype.Etapa = null;
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

        	//Oportunidad sin duración del contrato
        	oInfotype.Etapa = 'F3 - Presentación de oferta';
        	oInfotype.Duracion_del_contrato_Meses = null;
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

        	//Oportunidad sin ingreso por única vez
        	oInfotype.Duracion_del_contrato_Meses = 12;
        	oInfotype.Ingreso_por_unica_vez = null;
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

        	//Oportunidad sin recurrente bruto mensual
        	oInfotype.Ingreso_por_unica_vez = 234;
        	oInfotype.Recurrente_bruto_mensual = null;
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

        	//Oportunidad sin FCV
        	oInfotype.Recurrente_bruto_mensual = 234;
        	oInfotype.Venta_bruta_total_FCV = null;
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);
        }
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       NEAborda
	    Description:   Test method that manages the code coverage from the errors in the Order request of BI_OpportunitySisonSOAP_v2
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    31/08/2016                      Guillermo Muñoz             Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void errorsOrderTest(){

		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        BI_TestUtils.throw_exception=false;

		User integracionCHI = [SELECT Id FROM User WHERE Name = 'Integración chi' lIMIT 1];
		String idOpp = '796878304_' + [SELECT BI_Numero_id_oportunidad__c FROM Opportunity Limit 1].BI_Numero_id_oportunidad__c;
		
		///////////////////////////////////////////////rellenamos los datos de la order//////////////////////////////////////////////////////
		BI_OpportunitySisonSOAP_v2.ConfigurationInfoType cInfoType = new BI_OpportunitySisonSOAP_v2.ConfigurationInfoType();

		cInfoType.Integration_id = '0000000001';
        cInfoType.Divisa = 'CLP';
        cInfoType.Cuota_total_por_unica_vez = 0;
        cInfoType.Cargo_total_recurrente = 0;
        cInfoType.Catalogo = 'Empresas';
        cInfoType.Modelo_comercial = 'uno';
        cInfoType.AccionSISON = 'CREATE';
        cInfoType.Fecha_Hora_creacion_version = Datetime.now();
        cInfoType.Productos = null;

        List <BI_OpportunitySisonSOAP_v2.ConfigurationInfoType> lst_cInfoType = new List <BI_OpportunitySisonSOAP_v2.ConfigurationInfoType>();
        lst_cInfoType.add(cInfoType);
        /////////////////////////////////////////////////////////////fin order///////////////////////////////////////////////////////////////

		//////////////////////////////////////////////rellenamos los datos de la oportunidad/////////////////////////////////////////////////
		BI_OpportunitySisonSOAP_v2.OpportunityInfoType oInfoType = new BI_OpportunitySisonSOAP_v2.OpportunityInfoType();

		oInfoType.Fecha_de_cierre = Date.today().addDays(2);
        oInfoType.Etapa = 'F3 - Presentación de oferta';
        oInfoType.Motivo_de_perdida = null;
        oInfoType.Duracion_del_contrato_Meses = 12;
        oInfoType.Fecha_entrega_oferta = Date.today().addDays(20);
        oInfoType.Fecha_de_vigencia = Date.today().addDays(20);
        oInfoType.Comienzo_estimado_de_facturacion = null;
        oInfoType.Ingreso_por_unica_vez = 234;
        oInfoType.Recurrente_bruto_mensual = 234;
        oInfoType.Venta_bruta_total_FCV = 0;
        oInfoType.Probabilidad_de_exito = '50';
        oInfoType.Plazo_estimado_de_provision_dias = 0;
        oInfoType.Id_Oportunidad_SF = idOpp;
        oInfoType.Tasa_de_interes = 1.01097885195017;
        oInfoType.Tipo_de_renegociacion = null;
        oInfoType.Recurrente_bruto_mensual_anterior = 0;
        oInfoType.Tipo_de_nuevo_resgistro = null;
        oInfoType.Divisa = 'CLP';
        oInfoType.Descripcion = 'Test 1';
        oInfoType.Descripcion_cierre = null;
        oInfoType.Tipo_de_oportunidad = null;        
        oInfoType.Versiones = lst_cInfoType;

        Id accId = [SELECT Id FROM Account LIMIT 1].Id;
        NE__Order__c ord = new NE__Order__c(
        	NE__AccountId__c = accId,
             NE__BillAccId__c = accId,
             NE__OptyId__c = [SELECT Id FROM Opportunity LIMIT 1].Id,
             NE__AssetEnterpriseId__c='test',
             NE__ServAccId__c = accId,
             NE__OrderStatus__c = 'Activo',
             Integration_Id__c = lst_cInfoType[0].Integration_id
        );           
        insert ord;
        //////////////////////////////////////////////////////////fin oportunidad////////////////////////////////////////////////////////////

		system.runAs(integracionCHI){
			//Deshabilitamos los triggers para que no pase por el trigger de oportunidades con cada operación ya que solo queremos recorrer los códigos de error
        	Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),true,true,false,false);
        	Test.startTest();
        	//Oportunidad sin configuraciones
        	oInfoType.Versiones = null;
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

        	//Order sin action sison
        	lst_cInfoType[0].AccionSISON = null;
        	oInfotype.Versiones = lst_cInfoType;
        	BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

        	//Creación de una order ya existente.        	
            lst_cInfoType[0].AccionSISON = 'CREATE';
        	oInfotype.Versiones = lst_cInfoType;
            BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

            //Modificación de una order sin id de integración
            lst_cInfoType[0].AccionSISON = 'UPDATE';
            lst_cInfoType[0].Integration_id = '';
            oInfotype.Versiones = lst_cInfoType;
            BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

            //Modificación de una order sin divisa
            lst_cInfoType[0].Integration_id = ord.Integration_Id__c;
            lst_cInfoType[0].Divisa = null;
            oInfotype.Versiones = lst_cInfoType;
            BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

            //Modificación de una order sin cuota total única vez
            lst_cInfoType[0].Divisa = 'CLP';
            lst_cInfoType[0].Cuota_total_por_unica_vez = null;
            oInfotype.Versiones = lst_cInfoType;
            BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

            //Modificación de una order sin cargo total recurrente
            lst_cInfoType[0].Cuota_total_por_unica_vez = 234;
            lst_cInfoType[0].Cargo_total_recurrente = null;
            oInfotype.Versiones = lst_cInfoType;
            BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

            //Modificación de una order sin catálogo
            lst_cInfoType[0].Cargo_total_recurrente = 234;
            lst_cInfoType[0].Catalogo = '';
            oInfotype.Versiones = lst_cInfoType;
            BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

            //Modificación de una order sin modelo comercial
            lst_cInfoType[0].Catalogo = 'Empresas';
            lst_cInfoType[0].Modelo_comercial = null;
            oInfotype.Versiones = lst_cInfoType;
            BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);

            //Order sin fecha de creación
            lst_cInfoType[0].Modelo_comercial = 'uno';
            lst_cInfoType[0].Fecha_Hora_creacion_version = null;
            oInfotype.Versiones = lst_cInfoType;
            BI_OpportunitySisonSOAP_v2.updateFromSison(oInfoType);
            Test.stopTest();
            if(mapa != null){
        		BI_MigrationHelper.disableBypass(mapa);
        	}
        }
		
	}

}