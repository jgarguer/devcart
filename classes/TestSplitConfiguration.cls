/*---------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga
    Company:       Accenture
    Description:   Test class to manage the coverage code for SplitConfiguration class
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    16/02/2017              Pedro Párraga            Initial version
    17/02/2017              Alvaro Garcia            Added bypass when it is not necessary do intern  operations
    27/05/2017              Jesus Arcones Grande     Fix test error Too Many Queries
    28/09/2017              Jaime Regidor            Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c

    ---------------------------------------------------------------------------------------------------------------------------------*/
@isTest(seeAllData=true)
public class TestSplitConfiguration {   

    private static  Account acc;  
    private static Opportunity oppOld;
    private static Product2 p;
    private static List<RecordType> lst_rt;


    public static void createData(){
        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);
        BI_TestUtils.throw_exception = false;
        acc = new Account();
        acc.Name = 'TestAcc';
        acc.BI_Segment__c= 'test'; //28/09/2017
        acc.BI_Subsegment_Regional__c = 'test'; //28/09/2017
        acc.BI_Territory__c = 'test'; //28/09/2017
        insert acc;
            
        oppOld = new Opportunity();
        oppOld.Name = 'TestOpty';
        oppOld.AccountId = acc.Id;
        oppOld.CloseDate = Date.today();
        oppOld.StageName = 'F6 - Prospecting';
        oppOld.BI_Duracion_del_contrato_Meses__c = 6;
        insert oppOld; 

        p = new product2(name='x');              
        insert p;

        BI_MigrationHelper.disableBypass(mapa);

        lst_rt = [Select Id from RecordType where DeveloperName = 'Opty' limit 1];        
    }

    @isTest static void test_method_one() {

        // JAG 27/05/2017
        BI_TestUtils.throw_exception = false;
        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);
        // Fin JAG 27/05/2017

        createData();

        NE__Order__c oldOrd = new NE__Order__c();
        oldOrd.NE__OrderStatus__c = 'Sent';
        oldOrd.NE__OptyId__c = oppOld.Id;
        oldOrd.NE__OpportunityId__c = oppOld.Id; 
        oldOrd.RecordTypeId = lst_rt[0].Id; 
        oldOrd.currencyIsoCode = 'ARS';
        oldOrd.NE__Version__c = 3;  
        insert oldOrd;

        oldOrd.NE__OrderStatus__c = 'Sent';
        update oldOrd;

        NE__Product__c commProd = new NE__Product__c();
        commProd.Name = 'commProd';
        commProd.NE__ForecastProd__c = p.Id;
        insert commProd;     

        NE__OrderItem__c item = new NE__OrderItem__c();
        item.NE__OrderId__c = oldOrd.Id;
        item.NE__Qty__c = 1;
        item.NE__Status__c = 'No Procesado';
        item.NE__ProdId__c = commProd.Id;
        insert item;

        Test.startTest();
                              
        try{         
            SplitConfiguration.splitOrder(oldOrd.Id);                   
            SplitConfiguration.splitFailedItems(oldOrd.Id);   
            SplitConfiguration.splitSuccessedItems(oldOrd.Id);
            SplitConfiguration.regeneratePriceDefinitions(oldOrd.Id, oldOrd.Id);   
        }catch(Exception e){
            System.debug('*Test Failed! ' + e);         
        }
        Test.stopTest();
    }


    @isTest static void test_method_two() {
        createData();

        List<NE__Order__c> lst_order = new List<NE__Order__c>();

        NE__Order__c oldOrd2 = new NE__Order__c();
        oldOrd2.NE__OrderStatus__c = 'Sent';
        oldOrd2.NE__OptyId__c = oppOld.Id;
        oldOrd2.NE__OpportunityId__c = oppOld.Id;   
        oldOrd2.currencyIsoCode = 'ARS';  
        oldOrd2.RecordTypeId = lst_rt[0].Id;  
        lst_order.add(oldOrd2); 
        insert lst_order;

        List<NE__OrderItem__c> lst_oi = new List<NE__OrderItem__c>();

        NE__Product__c commProd = new NE__Product__c();
        commProd.Name = 'commProd';
        commProd.NE__ForecastProd__c = p.Id;
        insert commProd;

        Test.startTest();

        NE__OrderItem__c item2 = new NE__OrderItem__c();
        item2.NE__OrderId__c = oldOrd2.Id;
        item2.NE__Qty__c = 1;
        item2.NE__Status__c = 'Pending';
        item2.NE__ProdId__c = commProd.Id;
        lst_oi.add(item2);
        insert lst_oi;

        lst_order[0].NE__OrderStatus__c = 'Sent';
        update lst_order;        
                              
        try{        
            SplitConfiguration.splitFailedItems(oldOrd2.Id); 
            SplitConfiguration.splitOrder(oldOrd2.Id);                    
            SplitConfiguration.splitSuccessedItems(oldOrd2.Id);
        }catch(Exception e){
            System.debug('*Test Failed! ' + e);         
        }
        Test.stopTest();
    }

    @isTest static void test_method_three() {
        createData();

        NE__Order__c oldOrd = new NE__Order__c();
        oldOrd.NE__OrderStatus__c = 'Sent';
        oldOrd.NE__OptyId__c = oppOld.Id;
        oldOrd.NE__OpportunityId__c = oppOld.Id; 
        oldOrd.RecordTypeId = lst_rt[0].Id; 
        oldOrd.currencyIsoCode = 'ARS';
        oldOrd.NE__Version__c = 3;  
        insert oldOrd;

        oldOrd.NE__OrderStatus__c = 'Sent';
        update oldOrd;

        NE__Product__c commProd = new NE__Product__c();
        commProd.Name = 'commProd';
        commProd.NE__ForecastProd__c = p.Id;
        insert commProd;

        NE__OrderItem__c item = new NE__OrderItem__c();
        item.NE__OrderId__c = oldOrd.Id;
        item.NE__Qty__c = 1;
        item.NE__Status__c = 'No Procesado';
        item.NE__ProdId__c = commProd.Id;
        insert item;

        Test.startTest();
                              
        try{                           
            SplitConfiguration.splitFailedItems(oldOrd.Id); 
        }catch(Exception e){
            System.debug('*Test Failed! ' + e);         
        }
        Test.stopTest();
        
    }

    @isTest static void test_method_four() {
        createData();

        NE__Order__c oldOrd = new NE__Order__c();
        oldOrd.NE__OrderStatus__c = 'Sent';
        oldOrd.NE__OptyId__c = oppOld.Id;
        oldOrd.NE__OpportunityId__c = oppOld.Id; 
        oldOrd.RecordTypeId = lst_rt[0].Id; 
        oldOrd.currencyIsoCode = 'ARS';
        oldOrd.NE__Version__c = 3;  
        insert oldOrd;

        oldOrd.NE__OrderStatus__c = 'Sent';
        update oldOrd;

        NE__Product__c commProd = new NE__Product__c();
        commProd.Name = 'commProd';
        commProd.NE__ForecastProd__c = p.Id;
        insert commProd;

        NE__OrderItem__c item = new NE__OrderItem__c();
        item.NE__OrderId__c = oldOrd.Id;
        item.NE__Qty__c = 1;
        item.NE__Status__c = 'No Procesado';
        item.NE__ProdId__c = commProd.Id;
        insert item;

        Test.startTest();
                              
        try{                           
            SplitConfiguration.splitSuccessedItems(oldOrd.Id); 
        }catch(Exception e){
            System.debug('*Test Failed! ' + e);         
        }
        Test.stopTest();
    }

    @isTest static void test_exception(){
        try{         
            SplitConfiguration.regeneratePriceDefinitions(null ,null);                           
            SplitConfiguration.splitSuccessedItems(null);       
        }catch(Exception e){
            System.debug('*Test Failed! ' + e);         
        }
    }
}