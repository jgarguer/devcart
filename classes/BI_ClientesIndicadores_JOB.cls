global without sharing class BI_ClientesIndicadores_JOB implements Schedulable{
	
    global Map<String, String> publicGroupChatterMsg;
    global String idInforme;
    
    global void execute(SchedulableContext sch) {
        BI_ClientesIndicadoresJOB_ChatterFeed.createChatterFeedsReport(publicGroupChatterMsg, idInforme);
	}
    
    public BI_ClientesIndicadores_JOB(Map<String, String> publicGroupMsg, String informeId){
    	publicGroupChatterMsg = publicGroupMsg;
        idInforme = informeId;
    }
}