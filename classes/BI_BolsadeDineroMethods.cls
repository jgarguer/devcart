public with sharing class BI_BolsadeDineroMethods {
		/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by Account Triggers 
    Test Class:    BI_AccountMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if Descuento Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void blockEdition(List <BI_Bolsa_de_dinero__c> news, List <BI_Bolsa_de_dinero__c> olds){
        try{        	
        	Integer i = 0;
        	for(BI_Bolsa_de_dinero__c bolsa:olds){
        		if (bolsa.BI_Estado_del_Proceso__c == label.BI_BolsaDineroCancelado){
        			news[i].addError(label.BI_BolsaBloqueada);
        		}
        		i++;
        	}
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_BolsadeDineroMethods.blockEdition', 'BI_EN', exc, 'Trigger');
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if Descuento Status is 'Cancelado' to sum all BI_Movimiento__c from transaccion in .
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void transactionSum(List <BI_Bolsa_de_dinero__c> news, List <BI_Bolsa_de_dinero__c> olds){
        /*try{            
            Integer i = 0;
            Set <Id> set_bolsas = new Set <Id>();
            for(BI_Bolsa_de_dinero__c bolsa:news){
                if (olds[i].BI_Estado_del_Proceso__c != label.BI_BolsaDineroRechazado && bolsa.BI_Estado_del_Proceso__c == label.BI_BolsaDineroRechazado){
                    set_bolsas.add(bolsa.Id);
                }
                i++;
            }

            List <BI_Transaccion_de_bolsa_de_dinero__c> lst_trans = [SELECT Id, BI_Codigo_de_bolsa_de_dinero__c, BI_Movimiento__c 
                                                            FROM BI_Transaccion_de_bolsa_de_dinero__c WHERE BI_Codigo_de_bolsa_de_dinero__c IN :set_bolsas];

            Map <Id, Decimal> map_bolsa_mov = new Map <Id, Decimal>();

            for (BI_Transaccion_de_bolsa_de_dinero__c trans:lst_trans){
                if(!map_bolsa_mov.containskey(trans.BI_Codigo_de_bolsa_de_dinero__c)){
                    map_bolsa_mov.put(trans.BI_Codigo_de_bolsa_de_dinero__c, trans.BI_Movimiento__c);
                }else{
                    Decimal sumAux = map_bolsa_mov.get(trans.BI_Codigo_de_bolsa_de_dinero__c) + trans.BI_Movimiento__c;
                    map_bolsa_mov.put(trans.BI_Codigo_de_bolsa_de_dinero__c, sumAux);
                }
            }

           for(BI_Bolsa_de_dinero__c bolsa:news){
                if (set_bolsas.contains(bolsa.Id)){
                    bolsa.BI_Saldo_disponible__c += map_bolsa_mov.get(bolsa.Id);
                }
            }


            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_BolsadeDineroMethods.transactionSum', 'BI_EN', exc, 'Trigger');
        }*/
    }
}