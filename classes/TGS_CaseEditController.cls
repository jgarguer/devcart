/* Gonzalo Álvaro Barrio 25/02/2015
 * Controller from TGS_CaseEdit VisualForce Page 
 */

public class TGS_CaseEditController 
{
    public String caseID{get;set;}
    public Case caseToEdit;

    public boolean estgsperfil1{get;set;}
    public string userId;
    public UserRecordAccess userAccess;
    public boolean bUserHasEditAccess = false;
    
    public List<TGS_Status_machine__c> listStatusMachine = new List<TGS_Status_machine__c>();
    
    public Map<String, List<String>> mapStatusAndStatusReasonOptions = new Map<String, List<String>>();
    
    public Set<String> setStatusStrings = new Set<String>();
    public List<selectOption> listStatusOptions = new List<selectOption>();
    
    public TGS_CaseEditController(ApexPages.StandardController controller)
    {
        // Get parameters from the current Case
        caseToEdit = (Case)controller.getRecord();
        caseToEdit.TGS_Casilla_Desarrollo__c = true; //SNM 06/10/2016
        getStatusMachine();//Álvaro López 03/11/2017 - Moved the getStatusMachine() call
        checkUserAccessToCase();
        checkAssetBeforeCLosed();//Álvaro López 03/11/2017 - Added the checkAssetBeforeCLosed() call
        
        perfiles();
    } 
    
    public void checkUserAccessToCase()
    {
        userAccess = [SELECT RecordId, HasReadAccess, HasEditAccess, HasDeleteAccess 
                      FROM UserRecordAccess 
                      WHERE UserId = :UserInfo.getUserId() 
                            AND RecordId = :caseToEdit.Id];
        
        bUserHasEditAccess = userAccess.HasEditAccess;
        
        if(!bUserHasEditAccess)
        {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.TGS_Case_user_no_edit_access));            
        }
    }
    
    public boolean getUserAccessToCase()
    {
        return bUserHasEditAccess;
    }
    
    // Get al possible combinations of Status - Status reason based in the current Status - Status reason
    public List<TGS_Status_machine__c> getStatusMachine()
    {           
        if(caseToEdit.TGS_Status_reason__c == null)
        {
            caseToEdit.TGS_Status_reason__c = '';    
        }
        
        // Get all available status transitions from the current Status and Status reason
        try
        {
            // GAB 18/08: First list with ALL the status machine transitions for the current status, status reasons and Service
            List<TGS_Status_machine__c> listStatusMachineAux = new List<TGS_Status_machine__c>();
            
            listStatusMachineAux = [SELECT TGS_Service__c, TGS_Service__r.Name, TGS_Prior_Status__c, TGS_Prior_Status_reason__c, TGS_Status__c, TGS_Status_reason__c, TGS_Profile__c
                                    FROM TGS_Status_machine__c
                                    WHERE TGS_Prior_Status__c = :caseToEdit.Status AND
                                    	TGS_Prior_Status_reason__c = :caseToEdit.TGS_Status_reason__c AND
                                    	TGS_Service__r.Name = :caseToEdit.TGS_Service__c];
            
            String profileCreatedBy = [SELECT Profile.Name 
                                       FROM User 
                                       WHERE Id IN (SELECT CreatedById 
                                                    FROM Case 
                                                    WHERE Id = :caseToEdit.Id)][0].Profile.Name;
            
            system.debug('[TGS_CaseEditController.getStatusMachine] Profile name of CreatedBy User: ' + profileCreatedBy);
            
            // GAB 18/08: Try to get all the status machine transitions for the CreatedBy User Profile
            for(TGS_Status_machine__c statusMachineWithProfile : listStatusMachineAux)
            {
                if(statusMachineWithProfile.TGS_Profile__c == profileCreatedBy)
                {
                    listStatusMachine.add(statusMachineWithProfile);
                }
            }
            
            // GAB 18/08: If there are no status machine transitions for the CreatedBy User Profile, get all the default status machine transitions (the ones without profile)
            if(listStatusMachine.size() == 0)
            {
                system.debug('[TGS_CaseEditController.getStatusMachine] No status machine transitions for the CreatedBy profile, lets get the default ones');                
                for(TGS_Status_machine__c statusMachineWithoutProfile : listStatusMachineAux)
                {
                    if(statusMachineWithoutProfile.TGS_Profile__c == null)
                    {
                        listStatusMachine.add(statusMachineWithoutProfile);
                    }
                }
            }
			
        }
        catch(System.QueryException e)
        {
            system.debug(e.getmessage());
        }
                
        // The current Status and Status reason to the map is added to the possible next Status and Status reason
        mapStatusAndStatusReasonOptions.put(caseToEdit.Status, new List<String>{caseToEdit.TGS_Status_reason__c});
        
        system.debug('[TGS_CaseEditController.getStatusMachine] Current Status - Status reason: ' + caseToEdit.Status + ' - ' + caseToEdit.TGS_Status_reason__c);
        
        // Put in a map all the possible status reason regarding its previous status, status reason and current status
        // Based on the prior Status and Status reason, map will be filled with all the possible current Status and Status reason.
        for(TGS_Status_machine__c statusMachine : listStatusMachine)
        {
            String keyStatus = statusMachine.TGS_Status__c;
            String valueStatusReason = statusMachine.TGS_Status_reason__c;
            
            if(mapStatusAndStatusReasonOptions.containsKey(keyStatus))
            {
                if(valueStatusReason == null) valueStatusReason = '';
                mapStatusAndStatusReasonOptions.get(keyStatus).add(valueStatusReason);
            }
            else
            {
                if(valueStatusReason == null) valueStatusReason = '';
                mapStatusAndStatusReasonOptions.put(keyStatus, new List<String>{valueStatusReason});
            }
            
            system.debug('[TGS_CaseEditController.getStatusMachine] Status transition added: ' + keyStatus + ' - ' + valueStatusReason);
        }
        
        system.debug('[TGS_CaseEditController.getStatusMachine] Filled map: ' + mapStatusAndStatusReasonOptions);
        
        return listStatusMachine;
    }
    
    // Fill the possible next Status
    public List<selectOption> getStatusOptions()
    {
        setStatusStrings = mapStatusAndStatusReasonOptions.keySet();    
        
        for(String statusOption : setStatusStrings)
        {
            listStatusOptions.add(new SelectOption(statusOption, statusOption));
        }
        
        system.debug('[TGS_CaseEditController.getNextStatus] Filled Status list: ' + listStatusOptions);
        
        return listStatusOptions;
    }
    
    // Fill the possible next Status reason options dependant on the next Status
    public List<selectOption> getStatusReasonOptions()
    {   
        List<String> listStatusReasonStrings = new List<String>();
        List<selectOption> listStatusReasonOptions = new List<selectOption>();
        
        listStatusReasonStrings = mapStatusAndStatusReasonOptions.get(caseToEdit.Status);   
        
        system.debug('[TGS_CaseEditController.getNextStatusReason] Current Status: ' + caseToEdit.Status + ', possible Status Reason: ' + listStatusReasonStrings);
        
        for(String statusReasonOption : listStatusReasonStrings)
        {
            listStatusReasonOptions.add(new SelectOption(statusReasonOption, statusReasonOption));
        }
        
        return listStatusReasonOptions;
    }

 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Daniel Guzman Moreno
    Company:       Telefonica
    Description:  Validacion de perfiles (TGS)
    Test Class:    TGS_CaseEditController
    
    History:
     
    <Date>                  <Author>                      <Change Description>
    09/01/2017              Daniel Guzmán Moreno           Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public void perfiles()
        {
            UserId = userinfo.getProfileId();
            List<Profile> perfilname = [select name from Profile where ID=:userID limit 1];

            if(!perfilname.isEmpty()){

                if(perfilname[0].Name =='TGS_Perfil 1'|| perfilname[0].Name == 'TGS Order Handling' || perfilname[0].Name == 'TGS System Administrator' || perfilname[0].Name == 'Soporte' ){
                    estgsperfil1=true;                
                    }else{
                    estgsperfil1=false;
                    }
                }
                system.debug('@@@' +estgsperfil1);
        }

    /*------------------------------------------------------------
     Author:         Álvaro López
     Company:        Accenture
     Description:    The method checks whether the asset has been correctly associated with registration cases before allowing users to change the status to Closed
     Test Class:     
     History
     <Date>          <Author>              <Change Description>
     03/11/2017      Álvaro López           Initial Version
    ------------------------------------------------------------*/
    public void checkAssetBeforeCLosed(){

        if(caseToEdit.Status == Constants.CASE_STATUS_RESOLVED && caseToEdit.Type == Constants.TYPE_NEW){

            if(caseToEdit.Asset__c == null){
                bUserHasEditAccess = false;
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must wait a few seconds for the asset to be associated with the case.'));
            }
        }
    }    
}