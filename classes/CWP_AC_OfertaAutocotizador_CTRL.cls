/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Jorge Sanjuan
	Company:       Accenture
	Description:   Method that help to generate the document
		 
	History: 
		 
	<Date>			<Author>			<Change Description>
	15/01/2018		Jorge Sanjuan		Initial Version
	13/02/2018		Roberto Niubó		Optimizations in ois listing/fetching
	14/02/2018		Roberto Niubó		Avoiding PageReferences calls (fixing Apex CPU Time Limits) and pushing static HTML data
	19/02/2018 		Manuel Ochoa		Added CurrencyIsoCode to query
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class CWP_AC_OfertaAutocotizador_CTRL{

	public String text1 {get;set;}
	public String text2 {get;set;}
	public String text3 {get;set;}
	public String text4 {get;set;}
	public String text5 {get;set;}
	public String text6 {get;set;}
	public List<String> lstText {get;set;}
	public NE.DocumentBuilder docBuilder {get;set;}
	
	public CWP_AC_OfertaAutocotizador_CTRL(Apexpages.Standardcontroller stdCon){
		try{
			lstText = new List<String>();
			String orderId= ApexPages.currentPage().getParameters().get('orderId');
			System.debug(orderId);

	
			// 19-02-2018 - Manuel Ochoa - added CurrencyIsoCode to query
            //Retrieving all the ois from list (it's ordered so root items will be retrieved first)
			List<NE__OrderItem__c> oisTodos = [SELECT Id, NE__Parent_Order_Item__c,NE__ProdName__c,NE__BaseOneTimeFee__c, format(NE__BaseOneTimeFee__c) FormatBaseOneTimeFee, NE__BaseRecurringCharge__c, format(NE__BaseRecurringCharge__c) FormatBaseRecurringCharge,CurrencyIsoCode FROM NE__OrderItem__c WHERE NE__OrderId__c =: orderId Order By NE__Parent_Order_Item__c Asc, NE__ProdName__c Desc];
			
			//RNA 14/02/2018 - Listing all the Attributes from OrderItems
			List<NE__Order_Item_Attribute__c> attTodos = [SELECT NE__Value__c, Name, NE__Order_Item__r.NE__OrderId__c FROM NE__Order_Item_Attribute__c WHERE NE__Order_Item__r.NE__OrderId__c =: orderId];
			
			//Map like <IDRootItem, List<[0]=RootItem, [Others]=ChildrenItems>>
			Map<Id,List<NE__OrderItem__c>> mapOrderItems = new Map<Id,List<NE__OrderItem__c>>();
		
			//RNA 13/02/2018 - Map from OrderItem,Attributes
			Map<Id,List<NE__Order_Item_Attribute__c>> mapAttributes = new Map<Id,List<NE__Order_Item_Attribute__c>>();

			//Construction of mapOrderItems
			if(!oisTodos.isEmpty()){
				//Iterating all ois
				for(NE__OrderItem__c ois : oisTodos ){
					//Looking for root items and making one item from the map with a new list
					if(ois.NE__Parent_Order_Item__c == null){
						mapOrderItems.put(ois.Id, new List<NE__OrderItem__c>{ois});
					//Looking for children items and putting into the map from orderItems
					} else if (mapOrderItems.containsKey(ois.NE__Parent_Order_Item__c)){
						List<NE__OrderItem__c> listaOis = mapOrderItems.get(ois.NE__Parent_Order_Item__c);
						listaOis.add(ois);
						mapOrderItems.put(ois.NE__Parent_Order_Item__c, listaOis);
					}
				}
			}

			//Construction of mapAttributes
			if(!attTodos.isEmpty()){
				for(NE__Order_Item_Attribute__c att : attTodos){
					if(mapAttributes.containsKey(att.NE__Order_Item__c)){
						List<NE__Order_Item_Attribute__c> listaAtts = mapAttributes.get(att.NE__Order_Item__c);
						listaAtts.add(att);
						mapAttributes.put(att.NE__Order_Item__c ,listaAtts);
					}else{
						mapAttributes.put(att.NE__Order_Item__c, new List<NE__Order_Item_Attribute__c>{att});
					}
				}
			}

			Map<String,Object> placeholders = null;
			//Generation of image Header
			PageReference pag1 = new PageReference('/apex/ne__DocumentComponentPDF?edit=false&PDF=false&mapName=OfertaAutocotizador&parentId='+orderId+'&nameDCH=OfertaAutocotizador&dateFormat=dd-MM-yyyy');
			//Generation of Order Attributes
			PageReference pag2 = new PageReference('/apex/ne__DocumentComponentPDF?edit=false&PDF=false&mapName=OfertaAutocotizador&parentId='+orderId+'&nameDCH=OfertaAutocotizador_PRICING&dateFormat=dd-MM-yyyy');
			
			//Testing data
			if (!Test.isRunningTest()){			
				text1 = pag1.getContent().toString();
			} else {
				text1 = 'UNIT TEST PAGE1';
			}

			//Pushing text to Final List
			lstText.add(text1);

			//Testing data
			if (!Test.isRunningTest()) {
				text2 = pag2.getContent().toString();
			} else {
				text2 = 'UNIT TEST PAGE2';
			}

			//Pushing text to Final List
			lstText.add(text2);
			
			//Setting variables to fill HTML data
			String pickedCountry = '';
			String pickedCity = '';
			String pickedAddress = '';
			String pickedContractTerm = '';
			String pickedProduct = '';
			String pickedBandwidth = '';
			String pickedRouter = '';
			String pickedTechnology = '';
			String pickedSpecifications = '';
			String pickedNRC = '';
			String pickedMRC = '';			


            System.debug('MapOrderItems' + mapOrderItems.values());
			//RNA - Iteration of map to get values of lists where Root item is lstOis[0]
			for(List<NE__OrderItem__c> lstOis : mapOrderItems.values()){
                System.debug('ListOis' + lstOis);
				//RNA - Iteration of list where first item (or NE__Parent... == null) is root
				for(NE__OrderItem__c orderItem : lstOis){
                    System.debug('OrderItem' + orderItem);
					//RNA - Retrieving ROOT ITEM and going to pag3
					if(orderItem.NE__Parent_Order_Item__c == null){
						
						List<NE__Order_Item_Attribute__c> pickedAttributes = mapAttributes.get(orderItem.Id);
						System.debug('PickedAttr:' + pickedAttributes);

						//Getting values from list with text values
						for(NE__Order_Item_Attribute__c pickAtt : pickedAttributes){
							if(pickAtt.Name == 'Country'){
								pickedCountry = pickAtt.NE__Value__c;
							} else if(pickAtt.Name == 'City'){
								pickedCity = pickAtt.NE__Value__c;
							} else if(pickAtt.Name == 'Address'){
								pickedAddress = pickAtt.NE__Value__c;
							} else if(pickAtt.Name == 'Contract Term'){
								pickedContractTerm = pickAtt.NE__Value__c;
							}
						}
						//RNA ToDo --> Convert this mess into a formatted String fetched from a Static Resource
						// String placeholder = 'Hello {0}, {1} is cool!';
						// List<String> fillers = new String[]{'Jason','Apex'};
						// String formatted = String.format(placeholder, fillers);

	
						//RNA 14/02/2018 - This is the Pagereference we avoid to retrieve the next HTML data
						//PageReference pag3 = new PageReference('/apex/ne__DocumentComponentPDF?edit=false&PDF=false&mapName=OfertaAutocotizador_Tabla&parentId='+orderItem.Id+'&nameDCH=OfertaAutocotizador_TABLA_PRODUCTOS&dateFormat=dd-MM-yyyy');
						text3 = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><head><script src="/empresasplatino/static/111213/js/perf/stub.js" type="text/javascript"></script><link class="user" href="/empresasplatino/sCSS/42.0/sprites/1517528276000/Theme3/es/gc/zen-componentsCompatible.css" rel="stylesheet" type="text/css" /><link class="user" href="/empresasplatino/sCSS/42.0/sprites/1517528276000/Theme3/es/gc/elements.css" rel="stylesheet" type="text/css" /><link class="user" href="/empresasplatino/sCSS/42.0/sprites/1517528276000/Theme3/es/gc/common.css" rel="stylesheet" type="text/css" /><link class="user" href="/empresasplatino/sCSS/42.0/sprites/1517535714000/Theme3/gc/dStandard.css" rel="stylesheet" type="text/css" /><link class="user" href="/empresasplatino/sCSS/42.0/sprites/1518445079000/Theme3/00D9E0000000uRv/0059E000002yjKZ/gc/dCustom0.css" rel="stylesheet" type="text/css" /><link class="user" href="/empresasplatino/sCSS/42.0/sprites/1518445079000/Theme3/00D9E0000000uRv/0059E000002yjKZ/gc/dCustom1.css" rel="stylesheet" type="text/css" /><link class="user" href="/empresasplatino/sCSS/42.0/sprites/1518445079000/Theme3/00D9E0000000uRv/0059E000002yjKZ/gc/dCustom2.css" rel="stylesheet" type="text/css" /><link class="user" href="/empresasplatino/sCSS/42.0/sprites/1518445079000/Theme3/00D9E0000000uRv/0059E000002yjKZ/gc/dCustom3.css" rel="stylesheet" type="text/css" /><link class="user" href="/empresasplatino/sCSS/42.0/sprites/1518445079000/Theme3/00D9E0000000uRv/0059E000002yjKZ/gc/dCustom4.css" rel="stylesheet" type="text/css" /><link class="user" href="/empresasplatino/sCSS/42.0/sprites/1518445079000/Theme3/00D9E0000000uRv/0059E000002yjKZ/gc/dCustom5.css" rel="stylesheet" type="text/css" /><link class="user" href="/empresasplatino/sCSS/42.0/sprites/1517528276000/Theme3/es/gc/extended.css" rel="stylesheet" type="text/css" /><link class="user" href="/empresasplatino/sCSS/42.0/sprites/1517528276000/Theme3/es/gc/setup.css" rel="stylesheet" type="text/css" /><meta HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE" /><meta HTTP-EQUIV="Expires" content="Mon, 01 Jan 1990 12:00:00 GMT" /><style media="print" type="text/css">@page {size: A4;margin-bottom: 80px;margin-top: 80px;@top-center {content: element(header);}@bottom-left{content: element(footer);}}div.header {padding: 10px;position: running(header);}div.footer {display: block;padding: 5px;position: running(footer);}div.body {display: block;}.pagenumber:before {content: counter(page);}.pagecount:before {content: counter(pages);}</style></head><div class="header"><span id="j_id0:j_id2"></span></div><div class="footer"><span id="j_id0:j_id5" style="margin-bottom:70px"></span></div><hr /><table border="0" bordercolor="#ccc" cellpadding="0" cellspacing="0" style="border-collapse:collapse; width:100%"><tbody><tr><td style="width:20%"><div style="margin-left: 20px;"><span style="font-family:arial,helvetica,sans-serif"><strong><span style="color:#2d5986"><span style="font-size:9px">Site</span></span></strong></span></div></td><td><span style="font-family:arial,helvetica,sans-serif"><span style="color:#2d5986"><span style="font-size:9px"><span style="font-size:9px"><strong>Country:</strong> ';
						text3 += pickedCountry;
						text3 += ' </span><span style="font-size:9px"><strong>City:</strong> ';
						text3 += pickedCity;
						text3 += ' </span><span style="font-size:9px"><strong>Address:</strong> ';
						text3 += pickedAddress;
						text3 += ' </span><span style="font-size:9px"><strong>Contract Term:</strong> ';
						text3 += pickedContractTerm;
						text3 += ' </span></span></span></span></td><td style="text-align:center; width:10%"> </td><td style="text-align:center; width:10%"> </td></tr></tbody></table>';
						
						//Testing data
						if (!Test.isRunningTest()) {
							text3 = text3;
						} else {
							text3 = 'UNIT TEST PAGE3';
						}
						lstText.add(text3);
						System.debug('PageRefContent3: ' + text3);
						
					//Retrieving CHILDREN ITEM ois and going into pag4
					}else{

						//Getting only that Attributes from the exact OrderItem
						List<NE__Order_Item_Attribute__c> pickedAttributes = mapAttributes.get(orderItem.Id);

						//Restarting Strings to avoid getting last OI attributes
						pickedBandwidth = '';
						pickedRouter = '';
						pickedTechnology = '';
						pickedSpecifications = null;
						pickedNRC = '';
						pickedMRC = '';							

						//Getting values from list and filter test
						for(NE__Order_Item_Attribute__c pickAtt : pickedAttributes){
							if(pickAtt.Name == 'Bandwidth' && pickAtt.NE__Value__c != null){
								pickedBandwidth = pickAtt.NE__Value__c;
							} else if(pickAtt.Name == 'Router' && pickAtt.NE__Value__c != null){
								pickedRouter = pickAtt.NE__Value__c;
							} else if(pickAtt.Name == 'Technology' && pickAtt.NE__Value__c != null){
								pickedTechnology = pickAtt.NE__Value__c;
							} else if(pickAtt.Name == 'Specifications' && pickAtt.NE__Value__c != null){
								pickedSpecifications = pickAtt.NE__Value__c;
							} else if(pickAtt.Name == 'Specifications' && pickAtt.NE__Value__c == null ){
								pickedSpecifications = 'N/A';
							}
						}						
						
						//RNA 14/02/2018 - This is the Pagereference we avoid to retrieve the next HTML data						
						//PageReference pag4 = new PageReference('/apex/ne__DocumentComponentPDF?edit=false&PDF=false&mapName=OfertaAutocotizador_Tabla&parentId='+orderItem.Id+'&nameDCH=OfertaAutocotizador_TABLA_PRODUCTOS_CHILDS&dateFormat=dd-MM-yyyy');  		
						text4 = '<table border="0" bordercolor="#ccc" cellpadding="0" cellspacing="0" style="border-collapse:collapse; width:100%"><tbody><tr><td style="width:20%"><div style="margin-left: 20px;"><span style="font-family:arial,helvetica,sans-serif"><span style="color:#2d5986"><span style="font-size:9px">';
						
						//RNA 14/02/2018 - Changing Product Name from MPLS VPN to VPN MPLS						
						if(orderItem.NE__ProdName__c == 'MPLS VPN'){
							text4 += 'VPN MPLS';
						} else if(orderItem.NE__ProdName__c == 'INTERNET ACCESS'){
							text4 += 'INTERNET';
						} else {
							text4 += orderItem.NE__ProdName__c;

						}
												
						text4 += '</span></span></span></div></td><td><span style="font-family:arial,helvetica,sans-serif"><span style="color:#2d5986"><span style="font-size:9px"><span style="font-size:9px"><strong>Bandwidth (kbps):</strong> ';
						text4 += pickedBandwidth;
						text4 += ' </span><span style="font-size:9px"><strong>Router:</strong> ';
						text4 += pickedRouter;
						text4 += ' </span><span style="font-size:9px"><strong>Technology:</strong> ';
						text4 += pickedTechnology;
						text4 += ' </span><span style="font-size:9px"><strong>Specifications:</strong> ';
						text4 += pickedSpecifications;
						text4 += ' </span></span></span></span></td><td style="text-align:center; width:10%"><span style="font-family:arial,helvetica,sans-serif"><span style="color:#2d5986"><span style="font-size:9px"> ';
											
						//If Currency is 0 insted print a 'Contact us' String

						if(orderItem.NE__BaseOneTimeFee__c == 0 && orderItem.NE__BaseRecurringCharge__c == 0){
							text4 += 'Contact us';
						}else{
							//text4 += orderItem.NE__BaseOneTimeFee__c.format() + ' ' + orderItem.CurrencyIsoCode;
							
							// Decimal d2 = orderItem.NE__BaseOneTimeFee__c;
							// d2 = d2.setScale(2);
							// text4 += d2.format() + ' ' + orderItem.CurrencyIsoCode;

							//text4 += orderItem.FormatBaseOneTimeFee;

							text4 += orderItem.get('FormatBaseOneTimeFee');


						}	
						
						text4 += ' </span></span></span></td><td style="text-align:center; width:10%"><span style="font-family:arial,helvetica,sans-serif"><span style="color:#2d5986"><span style="font-size:9px"> ';
						
						if(orderItem.NE__BaseRecurringCharge__c == 0 && orderItem.NE__BaseOneTimeFee__c == 0){
							text4 += 'Contact us';
						}else{
							//text4 += orderItem.NE__BaseRecurringCharge__c.format() + ' ' + orderItem.CurrencyIsoCode;
							
							// Decimal d = orderItem.NE__BaseRecurringCharge__c;
							// d = d.setScale(2);
							// text4 += d.format() + ' ' + orderItem.CurrencyIsoCode;

							text4 += orderItem.get('FormatBaseRecurringCharge');

						}
							
						text4 += '</span></span></span></td></tr></tbody></table>';
					
						//Testing data
						if (!Test.isRunningTest()) {
							//text4 = pag4.getContent().toString();
							text4 = text4;
						} else {
							text4 = 'UNIT TEST PAGE4';
						}
						lstText.add(text4);
						
					}
				}
			}

			//RNA 13/02/2018 - Old Code!
			/*for (NE__OrderItem__c oi : oispadres) {
				PageReference pag3 = new PageReference('/apex/ne__DocumentComponentPDF?edit=false&PDF=false&mapName=OfertaAutocotizador_Tabla&parentId='+oi.Id+'&nameDCH=OfertaAutocotizador_TABLA_PRODUCTOS&dateFormat=dd-MM-yyyy');
				if (!Test.isRunningTest()) {
					text3 = pag3.getContent().toString();
				} else {
					text3 = 'UNIT TEST PAGE3';
				}
				lstText.add(text3);
				System.debug('Imprimo oi padre' + oi);
				for (NE__OrderItem__c oi2 : oishijos) {
					if (oi2.NE__Parent_Order_Item__c == oi.id) {
						PageReference pag4 = new PageReference('/apex/ne__DocumentComponentPDF?edit=false&PDF=false&mapName=OfertaAutocotizador_Tabla&parentId='+oi2.id+'&nameDCH=OfertaAutocotizador_TABLA_PRODUCTOS_CHILDS&dateFormat=dd-MM-yyyy');  
						if (!Test.isRunningTest()) {
							text4 = pag4.getContent().toString();
						} else {
							text4 = 'UNIT TEST PAGE4';
						}
						lstText.add(text4);
						System.debug('Imprimo oi hijo' + oi2);
					}
				}
			}*/

			//Exporting TOTAL DATA and Terms and Conditions
			PageReference pag5 = new PageReference('/apex/ne__DocumentComponentPDF?edit=false&PDF=false&mapName=OfertaAutocotizador&parentId='+orderId+'&nameDCH=OfertaAutocotizador_TERMS_AND_CONDITIONS&dateFormat=dd-MM-yyyy');
			//Exporting References
			PageReference pag6 = new PageReference('/apex/ne__DocumentComponentPDF?edit=false&PDF=false&mapName=OfertaAutocotizador&parentId='+orderId+'&nameDCH=OfertaAutocotizador_REFERENCES&dateFormat=dd-MM-yyyy');  
	
			//Testing data
			if (!Test.isRunningTest()) {
				text5 = pag5.getContent().toString();
			} else {
				text5 = 'UNIT TEST PAGE5';
			}
			
			//Testing data
			if (!Test.isRunningTest()) {
				text6 = pag6.getContent().toString();
			} else {
				text6 = 'UNIT TEST PAGE6';
			}

			lstText.add(text5);
			lstText.add(text6);

		}catch(Exception e){
			System.debug('ERROR VF OfertaComercial'+e);
		}
	}
}