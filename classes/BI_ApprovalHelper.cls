public without sharing class BI_ApprovalHelper {

    public string lastError = null;
    
    private Opportunity oppty;
    
    public static string ERROR_NOT_READY_FOR_TECHNICAL_APPROVAL = Label.BI_NoTecnicalApproval;
    public static string ERROR_NO_SALES_ENGINEER = Label.BI_NoSalesEngineer;
    
    public BI_ApprovalHelper(opportunity opp) {
        this.oppty = opp;
    }

    public boolean getUseStandardTechnicalApproval() {
        return !((oppty.stagename.startsWith('F4') && oppty.BI_Country__c == 'Argentina'));
    }


    public boolean submitTechnicalApprovalRequest() {
        lastError = null;
        //get oppty team's sales engineer(s)
        Opportunity opp = [select id, BI_Nivel_de_aprobacion__c, StageName, BI_Country__c,
                                  (select userid from OpportunityTeamMembers where teammemberrole = 'Sales Engineer') 
                            from Opportunity where id = :oppty.id]; 
        if(!((opp.BI_Nivel_de_aprobacion__c == 'Técnico' && Opp.stagename.startsWith('F4') && Opp.BI_Country__c == 'Argentina'))) {
            lastError = ERROR_NOT_READY_FOR_TECHNICAL_APPROVAL; // should not happen - check useStandardRiskApproval() before calling this method
            return false;
        }
        if(opp.OpportunityTeamMembers.size()==0) {
            lastError = ERROR_NO_SALES_ENGINEER;
            return false;
        }
        //create approval request
        List<Approval.ProcessSubmitRequest> requests = new list<Approval.ProcessSubmitRequest>();
        OpportunityTeamMember member = opp.OpportunityTeamMembers[0];
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setObjectId(opp.Id);
        req.setNextApproverIds(new list<id>{member.userid});
        requests.add(req);
        list<Approval.ProcessResult> results = Approval.process(requests);
        Approval.ProcessResult result = results[0];
        if(result.isSuccess()) {
            return true;
        }
        else {
            lastError = String.join(result.getErrors(), '\n');
            return false;
        }

    }
    
    //Francisco Ayllon - 09/10/2015
    //Method used on BI_OptyManualApprovalProcess_CTRL (with sharing, we need to update without sharing)
    public void updateOppty(){
        update oppty;
    }
}