public class ADQ_NotaCreditoController {

   public NotadeCredito__c notadeCredito;
   public Programacion__c programacion;
   public Boolean existeNotaCredito = false;
   public List<Pedido_PE__c> listaPedidosPE  = new List<Pedido_PE__c>();
   public List<NotaCredito_OLI__c> listaNotasCredito  = new List<NotaCredito_OLI__c>();
    
   //
   public Map<String, String> mapPedidosNombre = new Map<String, String>();
   public Map<String, Double> mapImportePedidos = new Map<String, Double>();
   public Map<String, Double> mapImporteNotasExistentes = new Map<String, Double>();
   public Map<String, Boolean> mapLlevaIEPSPedido = new Map<String, Boolean>(); 
   
   // === inicio ==== declaracion de los mapas para la modificacion de las notas de credito JMO ==================================
   public Map<String, String> mapProdIPS = new Map<String, String>();
   public Map<String, String> mapProdIPSId = new Map<String, String>();
   public Map<String, Boolean> mapIEPSProd = new Map<String, Boolean>();
   // === inicio ==== declaracion de los mapas para la modificacion de las notas de credito JMO ==================================
   
   public List<ConceptoFactura> listaConceptos;
   
   public Boolean showConceptosFactura{get;set;}
   public Boolean showCreaNota{get;set;}
   public Boolean creaNotaParcial{get;set;}
   public Boolean creaNotaTotal{get;set;}
   public Boolean deshabilitaCamposConceptos{get;set;}
   
   public Boolean conceptosCreados = false;
   Boolean deshabilitaBoton = false;
   String idProgramacion; 
   
   ApexPages.Action saveAction = new ApexPages.Action('{!newSave}');
   
   public ADQ_NotaCreditoController(ApexPages.StandardController stdController){
      this.notadeCredito = (NotadeCredito__c)stdController.getRecord();
      programacion = new Programacion__c ();
      showCreaNota = false; 
      creaNotaParcial = false;
      creaNotaTotal = false; 
      deshabilitaCamposConceptos = false;
      showConceptosFactura = false;
      
      //recuperamos el id de la cuenta (si es que se le paso como par�metro) y lo asignamos a la factura
      idProgramacion = (ApexPages.currentPage().getParameters().get('fid')!=null)?ApexPages.currentPage().getParameters().get('fid'):notadeCredito.Factura__c;
      if(idProgramacion != '' && idProgramacion != null){
         for (Programacion__c progra001 : [Select p.Id, p.Importe__c,p.Fecha__c, p.CurrencyIsoCode, 
                     p.Factura__c, p.Factura__r.Cliente__c, p.Numero_de_factura_SD__c,
                     p.Facturado__c, p.Factura__r.IVA__c, p.Factura__r.Condiciones_de_Pago__c,
                      Folio_fiscal__c, Clase_de_IVA__c
                      From Programacion__c p 
                      where id = :idProgramacion]){
            programacion = progra001;
         }
            validaNotaCredito();
      }else{
         deshabilitaBoton = true; 
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Para crear una nota de crédito dirigase a la factura sobre la cual quiere crear la nota de crédito.'));
      }
   }
   
   public void validaNotaCredito(){
      Boolean ncValida = true;
      String notaExistente;
      try{
      notaExistente = [Select n.Id From NotadeCredito__c n where n.Factura__c =:idProgramacion and n.TipoNota__c = 'Total' limit 1].Id;
      }catch(System.QueryException e){}
      if(notaExistente != null){
         ncValida = false;
         existeNotaCredito = true;
         deshabilitaBoton = true;
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ya existe una nota de crédito total para esta factura. No es posible generar otra.'));
      }
      if(ncValida){
         if (programacion != null){
            if(programacion.Folio_fiscal__c == null || programacion.Numero_de_factura_SD__c == null || programacion.Facturado__c == false){
               ncValida = false;
               deshabilitaBoton = true;
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No es posible aplicar una nota de crédito a una pre-factura. No es posible generar la nota de crédito.'));
            }
         }  
      }
   }
   
   public void seleccionaTipoNota(){
      if(notadeCredito.TipoNota__c == 'Parcial'){
         creaNotaParcial = true;
         creaNotaTotal = false;
         generaNotaParcial();
      }
      if(notadeCredito.TipoNota__c == 'Total'){
         creaNotaTotal = true;
         creaNotaParcial = false;
         generaNotaTotal();
      }
   }
   
   public void generaNotaParcial(){
      listaConceptos = new List<ConceptoFactura>();
      //Asignamos los valores por defecto para la nueva factura
      notadeCredito.Factura__c = programacion.Id;
      notadeCredito.Encabezado_de_Facturacion__c = programacion.Factura__c;
      notadeCredito.CurrencyIsoCode = programacion.CurrencyIsoCode;
      notadeCredito.Cliente__c = programacion.Factura__r.Cliente__c;
      notadeCredito.Tipo_de_IVA__c = programacion.Clase_de_IVA__c;
      notadeCredito.Condiciones_de_Pago__c = programacion.Factura__r.Condiciones_de_Pago__c;
      notadeCredito.Importe__c = 0; 
      creaConceptos();
      for(ConceptoFactura cf : listaConceptos){
         cf.importe = String.valueOf(cf.importeNotasCredito); 
      }
   }
   
   public void generaNotaTotal(){
      listaConceptos = new List<ConceptoFactura>();
      //Asignamos los valores por defecto para la nueva factura
      notadeCredito.Factura__c = programacion.Id;
      notadeCredito.Encabezado_de_Facturacion__c = programacion.Factura__c;
      notadeCredito.CurrencyIsoCode = programacion.CurrencyIsoCode;
      notadeCredito.Cliente__c = programacion.Factura__r.Cliente__c;
      notadeCredito.Tipo_de_IVA__c = programacion.Clase_de_IVA__c;
      notadeCredito.Condiciones_de_Pago__c = programacion.Factura__r.Condiciones_de_Pago__c;
      notadeCredito.Importe__c = programacion.Importe__c;
      //notadeCredito.
      if(validaNotaTotal()){
         creaConceptos();
         for(ConceptoFactura cf : listaConceptos){
            cf.activado = true;
            cf.importe = String.valueOf(cf.importeNotasCredito); 
         }
         deshabilitaCamposConceptos = true;
      }else{
         existeNotaCredito = true;
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No es posible crear una nota de crédito total para esta factura, ya que cuenta con al menos una nota de crédito parcial'));
      }
   }
   
   public Boolean validaNotaTotal(){
      String lpe;
      try{
         lpe = [Select p.Id  From NotaCredito_OLI__c p where p.NotaCreditoId__r.Factura__c = :idProgramacion limit 1].id;
      }catch(System.QueryException e){}
      return (lpe == null);
   }

   
      public void creaConceptos(){
         Date anio  = date.newInstance(2010, 1, 1);
         try{
            // === inicio ==== declaracion de los mapas para la modificacion de las notas de credito JMO ==================================MO
            
         listaPedidosPE = [Select  Id, Programacion__c, Precio_convertido__c,
                            Nombre_del_producto__c, Name, Id_producto__c, Concepto__c, Producto_en_Sitio__c,
                            Producto_en_Sitio__r.BI_MEX_Lleva_IEPS__c , Producto_en_Sitio__r.BI_MEX_Producto_Sustituto_Id__c,
                            Producto_en_Sitio__r.BI_MEX_Producto_Sustituto__c , Producto_en_Sitio__r.NE__CatalogItem__c  
                            From Pedido_PE__c 
                            where Programacion__c = :idProgramacion];
         


         listaNotasCredito = [Select Id, NotaCreditoId__c, Producto__c, NotaCreditoId__r.Factura__c, Importe__c From NotaCredito_OLI__c where NotaCreditoId__r.Factura__c =:programacion.Id];
         if(listaPedidosPE.size() > 0){
            for(Pedido_PE__c pe : listaPedidosPE){
               if(mapImportePedidos.containsKey(pe.Producto_en_Sitio__r.NE__CatalogItem__c)){
                  Double num = mapImportePedidos.get(pe.Producto_en_Sitio__r.NE__CatalogItem__c) + pe.Precio_convertido__c;
                  mapImportePedidos.put(pe.Producto_en_Sitio__r.NE__CatalogItem__c, num);
                  //Boolean llevaIEPS02 =pe.Producto_en_Sitio__r.BI_MEX_Lleva_IEPS__c;
                  mapLlevaIEPSPedido.put(pe.Producto_en_Sitio__r.NE__CatalogItem__c, pe.Producto_en_Sitio__r.BI_MEX_Lleva_IEPS__c);
               }
               else {
                  mapImportePedidos.put(pe.Producto_en_Sitio__r.NE__CatalogItem__c, pe.Precio_convertido__c);
                  mapPedidosNombre.put(pe.Producto_en_Sitio__r.NE__CatalogItem__c, pe.Nombre_del_producto__c);
                  mapLlevaIEPSPedido.put(pe.Producto_en_Sitio__r.NE__CatalogItem__c, pe.Producto_en_Sitio__r.BI_MEX_Lleva_IEPS__c);
                  
                  // === inicio ==== declaracion de los mapas para la modificacion de las notas de credito JMO ==================================
                  mapProdIPS.put(pe.Producto_en_Sitio__r.NE__CatalogItem__c, pe.Producto_en_Sitio__r.BI_MEX_Producto_Sustituto__c); //--
                  mapProdIPSId.put(pe.Producto_en_Sitio__r.NE__CatalogItem__c, pe.Producto_en_Sitio__r.BI_MEX_Producto_Sustituto_Id__c); //..
                  mapIEPSProd.put(pe.Producto_en_Sitio__r.NE__CatalogItem__c, pe.Producto_en_Sitio__r.BI_MEX_Lleva_IEPS__c);
                  // === Fin ==== declaracion de los mapas para la modificacion de las notas de credito JMO ==================================

               }
            }
            for(String productId : mapImportePedidos.keySet()){
               System.debug('>>>> mapLlevaIEPSPedido: '+ mapLlevaIEPSPedido + '  >>> mapProdIPS: ' + mapProdIPS.get(productId) + '  >>>  ano: '+anio + ' >>> programacion.Fecha: ' + programacion.Fecha__c + '  >>> mapIEPSProd: '+ mapIEPSProd);

            if  (mapLlevaIEPSPedido.get(productId) != null && mapProdIPS.get(productId) != null && programacion.Fecha__c < anio && mapIEPSProd.get(productId))
               {
                  System.debug('si entra a Producto con IEPS.......===================' +mapProdIPS.get(productId) + '   ====  ');
                  ConceptoFactura cf = new ConceptoFactura(mapProdIPSId.get(productId), mapProdIPS.get(productId), mapImportePedidos.get(productId),  mapImportePedidos.get(productId), 0, false, mapLlevaIEPSPedido.get(productId));
                  listaConceptos.add(cf);
                  showCreaNota = true;
               }else {
               
                  ConceptoFactura cf = new ConceptoFactura(productId, mapPedidosNombre.get(productId), mapImportePedidos.get(productId),  mapImportePedidos.get(productId), 0, false,mapLlevaIEPSPedido.get(productId));
                  listaConceptos.add(cf);
                  showCreaNota = true;
               }
               
            }
            if(listaNotasCredito.size() > 0){
               for(NotaCredito_OLI__c nco : listaNotasCredito){
                  if(mapImporteNotasExistentes.containsKey(nco.Producto__c)){
                     Double suma = mapImporteNotasExistentes.get(nco.Producto__c) + nco.Importe__c;
                     mapImporteNotasExistentes.put(nco.Producto__c, suma);
                     
                  }else mapImporteNotasExistentes.put(nco.Producto__c, nco.Importe__c);
               }
               for(ConceptoFactura cf : listaConceptos){
                  if(mapImporteNotasExistentes.containsKey(cf.idProducto)){
                     cf.importeNotasCredito = cf.importeNotasCredito - mapImporteNotasExistentes.get(cf.idProducto);
                  }
               }
            }
         }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No existen conceptos en la factura a los que se les pueda aplicar una nota de crédito. No es posible generar una nueva nota de crédito'));
            existeNotaCredito = true;
         }
         showConceptosFactura = true;
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '' + listaPedidosPE));
         conceptosCreados = true;
      
      }catch(Exception e){
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
         System.debug('Exception: '+e);
      }
      
      }
         
   public void cambiaImporte(){
      try{
         Double sumaConceptos = 0;
         for(ConceptoFactura cf : listaConceptos){
            if(cf.activado == true){
               if(Double.valueOf(cf.importe) > cf.importeNotasCredito){
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'El importe de ' + cf.nombreProducto + ' en la nota de c�dito no puede superar al importe en factura'));
                  cf.activado = false;
               }
               else if(Double.valueOf(cf.importe)<=0){
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'El importe de ' + cf.nombreProducto + ' en la nota de c�dito no puede ser negativo o cero'));
                  cf.activado = false;
               }
               else sumaConceptos += Double.valueOf(cf.importe); 
            }
         }
         notadeCredito.Importe__c = sumaConceptos;
         //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'El im' + sumaConceptos + notadeCredito.Importe__c + listaConceptos));
      }
      catch(Exception e){
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
         System.debug('Exception: '+e);
      }
   }

   public PageReference guardar(){
      if(!existeNotaCredito){
         if(validaSeleccion() && validaImporte()){
            if(Test.isRunningTest()){
               upsert notadeCredito;
            }else{
               insert notadeCredito;
            }
            actualizaPedidosPE(notadeCredito.Id);
            PageReference acctPage = new PageReference('/' + notadeCredito.id);
            acctPage.setRedirect(true);
            return acctPage;
            //return null;
         }
         else return null;

      }else return null;
      }
      public void cubre (){
         Integer i= 0;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         
         
      }
      public Boolean validaImporte(){
      Boolean valido = false;
      Boolean conceptoenCero = false;
      //Si se trata de una nota parcial
      if(creaNotaParcial){
         Double contador = 0;
         Double a = notadeCredito.Importe__c;
         for(ConceptoFactura cf : listaConceptos){
            if(cf.activado == true){
               contador += Double.valueOf(cf.importe);
               if(Double.valueOf(cf.importe) <= 0){
                  conceptoenCero = true;
               }
            }
         }
         notadeCredito.Importe__c = contador;
         if(!conceptoenCero){valido = true;}
      }
      //Si se trata de una nota total
      else{
         if(notadeCredito.Importe__c > programacion.Importe__c){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'El importe de la nota de crédito no puede superar al importe de la programación.'));
         }else valido = true;
      }
      return valido;
   }
   
   public Boolean validaSeleccion(){
      Boolean valido = false;
      if(creaNotaParcial){
         for(ConceptoFactura cf : listaConceptos){
            if(cf.activado == true){
               valido = true;
            }
         }
         if(!valido) ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Debe seleccionar al menos un concepto de la factura.'));
      }else valido= true;
      return valido;
   }
   
   public void actualizaPedidosPE(String NotaCreditoId){
      try{
         List<NotaCredito_OLI__c> listaNotasCreditoParciales  = new List<NotaCredito_OLI__c>();
         if(creaNotaParcial){
            for(ConceptoFactura cf : listaConceptos){
               if(cf.activado == true){
                  Double CantidadIEPS = (cf.llevaIEPS)?Double.valueOf(cf.importe)*0.03:0;
                  NotaCredito_OLI__c ncp = new NotaCredito_OLI__c(Producto__c = cf.idProducto, Importe__c = Double.valueOf(cf.importe), NotaCreditoId__c = NotaCreditoId, ProgramacionId__c = programacion.Id, CurrencyIsoCode = programacion.CurrencyIsoCode, NombreProducto__c = cf.nombreProducto, Lleva_IEPS__c = cf.llevaIEPS, IEPS__c = CantidadIEPS);
                  listaNotasCreditoParciales.add(ncp);
               }
            }
         }else{
            for(ConceptoFactura cf : listaConceptos){
                  Double CantidadIEPS = (cf.llevaIEPS)?Double.valueOf(cf.importe)*0.03:0;
                  NotaCredito_OLI__c ncp = new NotaCredito_OLI__c(Producto__c = cf.idProducto, Importe__c = Double.valueOf(cf.importe), NotaCreditoId__c = NotaCreditoId, ProgramacionId__c = programacion.Id, CurrencyIsoCode = programacion.CurrencyIsoCode, NombreProducto__c = cf.nombreProducto, Lleva_IEPS__c = cf.llevaIEPS, IEPS__c = CantidadIEPS);
                  listaNotasCreditoParciales.add(ncp);
            }
         }
         insert listaNotasCreditoParciales;
      }
      catch(Exception e){
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
         System.debug('Exception: '+e);
      }
         //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ''+listaPedidosPEActualiza));
   }
 
      public PageReference cancel() {
         PageReference acctPage = new PageReference('/apex/ADQ_OpcionesFacturacion');
         if (programacion != null){
            acctPage = new PageReference('/' + programacion.id);
         }
      
       acctPage.setRedirect(true);
       return acctPage;
      }
 
      public List<ConceptoFactura> getListaConceptos(){
         return listaConceptos;
      }
      
   
      public Boolean getMuestraBloqueSeleccionNC(){return !showCreaNota;}
      
      public Boolean getImporteReadOnly(){return true;}
      
      public Boolean getMuestraBotonContinuar() {return deshabilitaBoton;}
      
      public Boolean getMuestraBoton() {return existeNotaCredito;}
      
   public Programacion__c getProgramacion(){return programacion;}
   
   public String getCliente(){return notadeCredito.Cliente__c;}
      
      public class ConceptoFactura{
      public String idProducto {get;set;}
      public String nombreProducto {get;set;}
      public Double precio {get;set;}
      public Double importeNotasCredito{get;set;}
      public String importe{get;set;}
      public Boolean activado {get;set;}
      public Boolean llevaIEPS {get;set;}
      public Double ieps{get;set;}
      
      public ConceptoFactura(String idProducto, String nombreProducto, Double precio, Double importeNotasCredito, Double importeCapturado, Boolean activado,Boolean llevaIEPS){
         this.idProducto = idProducto;
         this.nombreProducto = nombreProducto;
         this.precio = precio;
         this.importeNotasCredito = importeNotasCredito;
         this.importe = String.valueOf(importeCapturado);
         this.activado = activado;
         this.llevaIEPS = llevaIEPS;
         //this.ieps = 0; 
         this.ieps = (llevaIEPS)?importeCapturado*0.03:0; 
      }
   }
  
}