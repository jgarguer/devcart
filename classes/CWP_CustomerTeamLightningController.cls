public without sharing  class CWP_CustomerTeamLightningController {
    
    public static FieldSetHelper.FieldSetContainer fieldSetRecords {get; set;}
    public static String firstHeader {get; set;}
    public static list <records> lista {get;set;}
    public static Integer totalRegs {get;set;}
    public static Integer index {get;set;}
    public static Integer pageSize {get;set;}
    public static Integer pages {get;set;}
    public static Integer topValue {get;set;}
    public static Id rtId = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'CWP_OrderFromCWP'].Id;
    public static list <integer> pagination {get;set;}
    public static List<SelectOption> searchFields {get; set;}
    public static Boolean RenderTGS{get;set;}
    public String CaseType{get;set;}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Get Account Id Method

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static String getIdent(){
        return BI_AccountHelper.getCurrentAccountId();        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Miguel Girón
Company:       Everis
Description:   Get wether the user is BIEN or TGS profile

History:

<Date>            <Author>              <Description>
04/04/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    @auraEnabled
    public static Boolean getBIENUser(){
        return TGS_User_Org__c.getInstance().TGS_Is_BI_EN__c;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Method to obtain table headers.

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static List<headers> getHeaders(){
        list<headers> listHeaders =new list<headers>();
        
        //FieldSetHelper.FieldSetContainer fieldSetRecords;
        try{
            
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            List<Schema.FieldSetMember> fieldSetMemberList =  readFieldSet('PCA_MainTableContacts','Contact');
            
            for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
            {
                headers header = new headers();
                header.Label=fieldSetMemberObj.getLabel();
                listHeaders.add(header);
            }
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.Order','Portal Platino', Exc, 'Class');
        }
        return listHeaders;
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Method to obtain fields from set.

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName)
    {
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);  
        return fieldSetObj.getFields(); 
    }  
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

    Author:        Alberto Pina
    Company:       Everis
    Description:   Method to obtain Customer Team records
    
    History:
    
    <Date>            <Author>              <Description>
    16/03/2017        Everis                Initial version
    11/07/2017        Daniel Guzman        Filter added control field CWP_Contact_hidden__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static List<records> getRecords(String orderField, String orderDirection){
        list <FieldSetHelper.FieldSetRecord> listRecords =new list<FieldSetHelper.FieldSetRecord>();
        
        String firstHeader;
        if(!(pageSize>0))pageSize=10;
        if(!(index>0))index=1;
        
        ///GET RECORDS
        try{
            String accountFieldName = 'AccountId';
            //TODO
            string AccountId= BI_AccountHelper.getCurrentAccountId();  
            //string AccountId ='0012500000dVHOSAA4';
            //Added Filter Condition
            String condition = accountFieldName + ' = \'' + AccountId + '\' AND BI_Activo__c = true AND CWP_contact_hidden__c = false';
            String orderByString;
            if(orderField != null && orderField != '' && orderDirection != null && orderDirection != ''){
                orderByString = orderField + ';' + orderDirection;
            }
            String nameFS = 'PCA_MainTableContacts';
            String nameObject = 'Contact';
            fieldSetRecords = FieldSetHelper.FieldSetHelperSearchWithCondition(nameFS, nameObject, condition, orderByString);
            if (fieldSetRecords.regs.size() > 0)
            {
                firstHeader = fieldSetRecords.regs[0].values[0].field;
            }
            
            totalRegs = fieldSetRecords.regs.size();
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.defineRecords', 'Portal Platino', Exc, 'Class');
        }
        //FIN GET RECORDS
        lista = new list<records>();
        try{
            listRecords = new List<FieldSetHelper.FieldSetRecord>();
            topValue = ((index + pageSize) > totalRegs) ? totalRegs : (index + pageSize - 1);
                for (Integer i = (index - 1); i < topValue; i++)
            {
                FieldSetHelper.FieldSetRecord iRecord = fieldSetRecords.regs[i]; 
                listRecords.add(iRecord);
            }
            pages=listRecords.size()/pageSize;
            
            
            system.debug('listarecord:'+listRecords);
            //list <records> dev = new list<records> ();
            for(FieldSetHelper.FieldSetRecord k : listRecords){
                records rec = new records();
                rec.Id=k.Id;
                rec.Name=k.values[0].value;
                rec.Title=k.values[1].value;
                rec.Email=k.values[2].value;
                rec.Phone=k.values[3].value;
                rec.MobilePhone=k.values[4].value;
                lista.add(rec);
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.defineViews', 'Portal Platino', Exc, 'Class');
        }  
        
        return lista;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Method to get the first page table.

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    @auraEnabled
    public static String getVista(Integer page){
        list <records> listaux = new list<records> ();
        List<Records> viewRecords = new List<Records>();
        list <records> allRecords = getRecords('','');
        if(page>0)pageSize=page;else pageSize=10;         
        
        try{ 
            topValue = ((index + pageSize) > totalRegs) ? totalRegs : (index + pageSize - 1);
                
                for (Integer i = (index - 1); i < topValue; i++)
            {
                records iRecord = allRecords.get(i); 
                viewRecords.add(iRecord);
            }
            for(records k : viewRecords){
                records rec = new records();
                rec.Id=k.Id;
                rec.Name=k.Name;
                rec.Title=k.Title;
                rec.Email=k.Email;
                rec.Phone=k.Phone;
                rec.MobilePhone=k.MobilePhone;
                listaux.add(rec);
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.defineViews', 'Portal Platino', Exc, 'Class');
        }
        return JSON.serialize(listaux);
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Method to set pagination limits.

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static List<Integer> getPagination(){  
        pagination = new list<integer> ();
        pagination.add(5);
        pagination.add(10);
        pagination.add(25);
        pagination.add(50);
        pagination.add(100);
        pagination.add(200);
        return pagination;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

    Author:        Alberto Pina
    Company:       Everis
    Description:   Method to obtain AccountTeamMembers
    
    History:
    
    <Date>            <Author>              <Description>
    16/03/2017        Everis                Initial version
    11/07/2017        Daniel Guzman         Filter added control field CWP_Contact_hidden__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void defineRecords()
    {
        try{
            String accountFieldName = 'AccountId';
            string AccountId = BI_AccountHelper.getCurrentAccountId();
            //Added condition DGM
            String condition = accountFieldName + ' = \'' + AccountId + '\' AND BI_Activo__c = true AND CWP_contact_hidden__c = false';
            String nameFS = 'PCA_MainTableContacts';
            String nameObject = 'Contact';
            
            fieldSetRecords = FieldSetHelper.FieldSetHelperSearchWithCondition(nameFS, nameObject, condition, null);
            
            if (fieldSetRecords.regs.size() > 0)
            {
                firstHeader = fieldSetRecords.regs[0].values[0].field;
            }
            
            index = 1; 
            totalRegs = fieldSetRecords.regs.size();
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.defineRecords', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Method to obtain next records with pagination.

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static List<records> getNext(Integer indice, Integer numRec, Integer page){
        List <records> listaux; 
        if(getHasNext()){ 
            index+=numRec;
            listaux=(List<records>)System.JSON.deserialize(getVista(page), List<records>.class);
        }else{
            listaux=(List<records>)System.JSON.deserialize(getVista(page), List<records>.class);
        }
        if(listaux.size()>0)return listaux;else return lista;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Method to obtain previous records with pagination

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static List<records> getBack(Integer indice, Integer numRec, Integer page){
        List <records> listaux = new List<records> (); 
        if(getHasBack()){ 
            index-=numRec;
            listaux=(List<records>)System.JSON.deserialize(getVista(page), List<records>.class);
        }
        if(listaux.size()>0)return listaux;else return lista;
        //return listaux;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Pagination method

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static void getAttr(Integer indice, Integer numRec, Integer page){
        
        pageSize=integer.valueOf(page);
        list <records> listaux = new list<records> (); 
        lista=getRecords('','');
        topValue = ((index + pageSize) > totalRegs) ? totalRegs : (index + pageSize - 1);
            integer tmp1 = integer.valueof(indice);
        integer tmp2 = integer.valueof(numRec);
        index = (Integer) tmp1;
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Pagination method

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static Boolean getHasNext(){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            system.debug('siguiente index:'+index+' pageSize:'+pageSize+'totalReg:'+totalRegs+'result'+!((index+pageSize)>totalRegs));
            return !((index + pageSize) > totalRegs);
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.getHasNext', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Pagination method

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static Boolean getHasBack(){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            system.debug('index:'+index+' pageSize:'+pageSize+'result'+((index - pageSize) >= 0));
            return ((index - pageSize) >= 0);
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.getHasNext', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Pagination method

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static Integer getIndice(){
        return index;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Pagination method

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static Integer getNumRecords(){
        return totalRegs;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Pagination method

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static Integer getPages(){
        pages=totalRegs/pageSize;
        return pages;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Create Case

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
04/04/2017        Everis                Keep old functionality for BIEN Profiles
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static String setCase(String status, String reason, String subject, String description, String purpose, String whereFrom){
        Case caseObj = new Case();
        String result = null;
        /*if(RenderTGS){
caseObj.Status = status;
caseObj.Reason = reason;
caseObj.Subject = subject;
caseObj.Description = description;  
}else{*/
        String CaseType = '';
        caseObj.OwnerId= userInfo.getUserId();
        system.debug('User Id: '+ userInfo.getUserId());
        Map <String, Id> map_rt = new Map <String,Id>();
        map_rt = loadRecordTypes();
        BI_Configuracion_Caso_PP__c ccpp = loadCC();
        List<User> lstUser = [SELECT Id, ContactId FROM User WHERE Id = : userInfo.getUserId()];
        List <String> lst_rts;
        if(ccpp != null && ccpp.BI_Tipos_de_registro__c != null) {
            lst_rts = ccpp.BI_Tipos_de_registro__c.split(';');
        }           
        if(lst_rts != null && lst_rts.size()==1){        
            caseType = lst_rts[0];
            //no se filtra si existe el tipo de registro en el mapa ya que si no existe queremos que se lance una excepción.
            caseObj.RecordTypeId = map_rt.get(lst_rts[0]);
        }
        if(caseObj.RecordTypeId == null){
            caseObj.RecordTypeId = map_rt.get(caseType);
        }
        if (caseType==Label.BIIN_TituloSolicInciden){
            caseObj.BI_Confidencial__c = false;
            caseObj.Status='In Progress';
            if(caseObj.BI_COL_Fecha_Radicacion__c == null){
                caseObj.BI_COL_Fecha_Radicacion__c = Datetime.now();
            }
        }else{
            //Añadido a lógica anterior para utilizar los valores que el usuario introduzca por pantalla
            caseObj.Reason = reason;
            caseObj.Status = status;
        }
        caseObj.Description = description;
        caseObj.Subject = subject;
        if(lst_rts!=null){
            if(map_rt.get(lst_rts[0]) != null){ caseObj.RecordTypeId = map_rt.get(lst_rts[0]);}
        }           
        
        caseObj.AccountId = BI_AccountHelper.getCurrentAccountId();
        //caseObj.ParentId = (apexpages.currentpage().getparameters().get('Id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('Id')):null;
        caseObj.ParentId = null;
        if(whereFrom.equalsIgnoreCase('EqTel')) {
            caseObj.Reason = 'Administrative Request';
            caseObj.Subject = 'Prueba [TBD]';
        }
        if(ccpp!=null && ccpp.BI_Country__c != null){
            caseObj.BI_Country__c = ccpp.BI_Country__c;
            caseObj.Origin = ccpp.BI_Origen__c;
            caseObj.Type = ccpp.BI_Type__c;
            caseObj.BI_Confidencial__c = ccpp.BI_Confidencial__c;
            if(ccpp.BI_COL_Fecha_Radicacion__c){
                caseObj.BI_Col_Fecha_Radicacion__c = Datetime.now();
            }
            
        }
        
        //}
        try{
            
            insert caseObj;
            system.debug('Case Id: ' + caseObj);
            System.debug('Insertó Case');   
            if(caseObj.Id != null) {
                result = caseObj.Id;
            }
        } catch (exception Exc) {
            System.debug(Exc.getStackTraceString());
            BI_LogHelper.generate_BILog('CWP_CustomerTeamLightningController.setCase', 'Customer Web Portal', Exc, 'Class');
        }
        return result;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Miguel Girón
Company:       Everis
Description:   Method that returns a map with all case record types

History: 

<Date>                          <Author>                    <Change Description>
04/04/2017                      Miguel Girón                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public static Map <String, Id> loadRecordTypes(){
        
        Map <String,Id> mapa = new Map <String,Id>();
        RecordType [] lst_rt = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Case'];
        if(!lst_rt.isEmpty()){
            for(RecordType rt : lst_rt){
                mapa.put(rt.DeveloperName, rt.Id);
            }
        }
        return mapa;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Miguel Girón
Company:       Everis
Description:   Method to obtain the BIEN Case Configuration Record corresponding to BIEN User's Country

History:

<Date>            <Author>              <Description>
04/04/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static BI_Configuracion_Caso_PP__c loadCC(){
        BI_Configuracion_Caso_PP__c aux = new BI_Configuracion_Caso_PP__c();
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            String countryName = [SELECT Pais__c FROM User WHERE Id = :UserInfo.getUserId()].Pais__c;  
            aux = [SELECT BI_Confidencial__c, BI_COL_Fecha_Radicacion__c, BI_Type__c, BI_Tipos_de_registro__c, BI_Country__c, BI_Origen__c FROM BI_Configuracion_Caso_PP__c WHERE BI_Country__c =: countryName LIMIT 1];
        }catch (Exception exc){
            BI_LogHelper.generate_BILog('PCA_Reclamo_PopUpNew.loadCC', 'Portal Platino', Exc, 'Class');
        }
        return aux;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Miguel Girón
Company:       Everis
Description:   Method to obtain the BIEN Case Configuration Record corresponding to BIEN User's Country

History:

<Date>            <Author>              <Description>
04/04/2017        Everis                Initial version
25/07/2017        Daniel Guzman         Added Filter Condition
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static String getRecordsView(){
        String ret='';
        
        RecordsWrapper rc= new RecordsWrapper();
        rc.iniIndex=1;
        rc.finIndex=10;
        String accountFieldName = 'AccountId';
        String AccountId= BI_AccountHelper.getCurrentAccountId();  
        String condition = accountFieldName + ' = \'' + AccountId + '\' AND BI_Activo__c = true AND CWP_contact_hidden__c = false';
        String queryCount='SELECT count() FROM Contact WHERE '+condition;
        
        rc.numRecords=Integer.valueOf(Database.countQuery(queryCount));
        rc.tableSize=10;
        integer offset=rc.iniIndex-1;
        String query='SELECT  Name, Title, Email, Phone, MobilePhone,  CurrencyIsoCode, Id FROM Contact WHERE '+condition+' LIMIT '+rc.finIndex +' OFFSET '+ offset;
        System.debug(query);
        List<records> lista = new List<records>();
        List<Contact> lContact= Database.query(query);
        for(Contact k : lContact){
            records rec = new records();
            rec.Id=k.Id;
            rec.Name=k.Name;
            rec.Title=k.Title;
            rec.Email=k.Email;
            rec.Phone=k.Phone;
            rec.MobilePhone=k.MobilePhone;
            lista.add(rec);
        }
        rc.recordsList=lista;
        ret=JSON.serialize(rc);
        return ret;
    }
    
   
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García 

Company:       everis

Description:   method called when users wants another page
History:
<Date>                          <Author>                                <Code>              <Change Description>
27/03/2017                      Julio Alberto Asenjo García                                  Initial version
25/07/2017        Daniel Guzman         Added Filter Condition
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @AuraEnabled    
    public Static String getNewTableController(String contextString) {
        RecordsWrapper c = (RecordsWrapper)JSON.deserialize(contextString, RecordsWrapper.class);
        string paginationFlag=c.paginationFlag;
        if (paginationFlag!=null) {
            if(paginationFlag=='forward'){
                c.iniIndex=c.iniIndex+c.tableSize;
                c.finIndex=c.finIndex+c.tableSize;
                if(c.finIndex>c.numRecords)
                    c.finIndex=c.numRecords;
                
            }else if(paginationFlag=='backward'){
                c.iniIndex=c.iniIndex-c.tableSize;
                c.finIndex=c.finIndex-c.tableSize;
                if(c.iniIndex<1)
                    c.iniIndex=1;
            }else if(paginationFlag.isNumeric()){
                c.iniIndex= 1;
                c.tableSize=integer.valueOf(paginationFlag);
                c.finIndex= c.tableSize;
                if(c.finIndex>c.numRecords)
                    c.finIndex=c.numRecords;  
            }
        }
        String accountFieldName = 'AccountId';
        String AccountId= BI_AccountHelper.getCurrentAccountId();
        integer offset=c.iniIndex-1;
        String condition = accountFieldName + ' = \'' + AccountId + '\' AND BI_Activo__c = true AND CWP_contact_hidden__c = false';
        if(c.sortedRow!=null && c.sortedRow!='')
            condition=condition+' ORDER BY '+ c.sortedRow + ' '+c.sortOrder;
        String query='SELECT  Name, Title, Email, Phone, MobilePhone,  CurrencyIsoCode, Id FROM Contact WHERE '+condition+' LIMIT '+(c.finIndex- (c.iniIndex-1))+' OFFSET '+ offset;
        System.debug(query);
        List<records> lista = new List<records>();
        List<Contact> lContact= Database.query(query);
        for(Contact k : lContact){
            records rec = new records();
            rec.Id=k.Id;
            rec.Name=k.Name;
            rec.Title=k.Title;
            rec.Email=k.Email;
            rec.Phone=k.Phone;
            rec.MobilePhone=k.MobilePhone;
            lista.add(rec);
        }
        c.recordsList=lista;
        return JSON.serialize(c);
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Method to obtain reasons in case modal.

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static List<customOption> defineSearch(List<String> pickValues){        
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            Case obj = new Case();
            RecordType record = [SELECT Id,Name FROM RecordType WHERE Name = 'Caso Agrupador'];
            obj.RecordTypeId = record.Id;
            
            List<SelectOption> options = new List<SelectOption>();
            List<customOption> customOptions = new List<customOption>(); 
            
            Schema.DescribeFieldResult fieldResult = Case.Reason.getDescribe();
            
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            system.debug('@@@: '+ple);
            for( String pick : pickValues){
                for(Schema.PicklistEntry f : ple)
                {
                    if(pick==f.getValue()){
                        
                        customOptions.add(new customOption(String.valueOf(f.getLabel()), String.valueOf(f.getValue())));
                    }
                }       
            }
            
            return customOptions;
            
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.defineSearch', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Class from Headers

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    //Clase del telefónica team
    public class  headers{
        @auraEnabled
        public String Label { get; set; }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Asenjo 
Company:       Everis
Description:   Class from table record Wrapper

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public class  RecordsWrapper{
        @auraEnabled
        public String paginationFlag { get; set;}
        @auraEnabled
        public Integer iniIndex { get; set;}
        @auraEnabled
        public Integer finIndex { get; set;}
        @auraEnabled
        public Integer numRecords { get; set;}
        @auraEnabled
        public Integer tableSize { get; set;}
        @auraEnabled
        public String sortOrder { get; set;}
        @auraEnabled
        public String sortedRow { get; set;}
        @auraEnabled
        public List<records> recordsList { get; set;}
        
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Class from table records

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public class  records{
        @auraEnabled
        public String Id { get; set; }
        @auraEnabled
        public String Name { get; set; }
        @auraEnabled
        public String Title { get; set; }
        @auraEnabled
        public String Email { get; set; }
        @auraEnabled
        public String Phone { get; set; }
        @auraEnabled
        public String MobilePhone { get; set; }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Alberto Pina
Company:       Everis
Description:   Method to get AccountTeamMembers

History:

<Date>            <Author>              <Description>
16/03/2017        Everis                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public class customOption{
        @auraEnabled
        String label {get;set;}
        @auraEnabled
        String value {get;set;}
        
        public customOption(String l, String v){
            label = l;
            value = v;
        }
    }
    public static void getTGSUser(){
        RenderTGS = TGS_User_Org__c.getInstance().TGS_Is_TGS__c;
    }
}