@isTest
private class BI_Campaign_EditPageCtrl_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_Campaign_EditPageCtrl class 
    History: 
    <Date>                  <Author>                <Change Description>
    13/06/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   Author:        Ignacio Llorca
   Company:       Salesforce.com
   Description:   Test method to manage the code coverage for BI_Campaign_EditPageCtrl
                 
   
   History: 
   
   <Date>                   <Author>                <Change Description>
   13/06/2014               Ignacio Llorca          Initial Version        
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void EditPageCtrl_TEST() {
        
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        PageReference pageRef = Page.BI_Campaign_EditPage;
    	Test.setCurrentPageReference(pageRef);
        
        Id idProfileA = BI_DataLoad.searchAdminProfile();
                      
        List<User> lst_user = BI_DataLoad.loadUsers(1, idProfileA, Label.BI_Administrador);
     	
     	Id idProfileE = BI_DataLoad.searchEjecutivoProfile();
     	
     	List<Campaign> lst_camp = BI_DataLoad.loadCampaigns(1);
     	
     	Integer j=0;
     	
     	User u = new User (alias = 'sttt'+string.valueof(j),
                                    email=string.valueof(j)+'u2@testorg.com',
                                    emailencodingkey='UTF-8',
                                    lastname='Testing2' +string.valueof(j),
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',
                                    ProfileId = idProfileE,
                                    BI_Permisos__c = Label.BI_Ejecutivo_de_Cliente,
                                    timezonesidkey=Label.BI_TimeZoneLA,
                                    Pais__c = Label.BI_Argentina,
                                    username= string.valueof(j)+'ur3@testorg.com');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(lst_camp[0]);
        
        BI_Campaign_EditPageCtrl clase = new BI_Campaign_EditPageCtrl(sc);
      
        
        system.runAs(u){
	        clase = new BI_Campaign_EditPageCtrl(sc);
	        
	        system.assertEquals(clase.dir,false);
	        system.assert(clase.url == '/' + lst_camp[0].Id);
	       
        }        
       
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}