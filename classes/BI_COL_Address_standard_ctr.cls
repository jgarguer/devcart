public class BI_COL_Address_standard_ctr {
        
        private final ApexPages.Standardcontroller controller;
        
        public BI_Sede__c ODireccion {get;set;}
        
        public List<WrpSucOpcCallejero> lstSucOpCa {get;set;}
        
        public Id IdDireccion {get;set;}
        
        public Boolean blSave {get;set;}
        
        public Boolean blClose {get;set;}
        
        public String lblError {get;set;}
        
        /**
        *       Controlador
        */
        public BI_COL_Address_standard_ctr( ApexPages.StandardController controller ) {
                //Controller
                this.controller = controller;
                //Obtiene el Id de la Sucursal
                ODireccion = (BI_Sede__c)controller.getRecord();
                IdDireccion = ODireccion.Id;
                //Inicializa variables
                lstSucOpCa = new List<WrpSucOpcCallejero>();
                lstSucOpCa=getlstSucOpCa();
                //Pinta el Boton Guardar
                blSave = true;
                //Variable para cerrar
                blClose = true;
        }
        /**
        *       Save
        */
        public PageReference Save()
        {
                PageReference pageRef;
                
                try{
                        System.debug('\n\n##Entro Save##' +  + '\n\n');
                        //Variable para cerrar
                        blClose = true;
                        //Recorre la lista
                        for( WrpSucOpcCallejero o : lstSucOpCa ){
                                
                                WrpOpcCallejero oPcCall = o.OpcCallejero;
                                //Valida que este seleccionado
                                if( oPcCall.strCheck != '0' && oPcCall.strCheck != null )
                                {
                                        System.debug('\n\n##Check##' + oPcCall.strCheck + '\n\n');
                                        System.debug('\n\n##o.IdSucursal##' + o.IdSucursal + '\n\n');
                                        //Busca la Sucursal
                                        
                                        List<BI_Sede__c> oSuc = [ Select  Id, BI_DireccionNormalizada__c, BI_Localidad__c, BI_COL_Latitud__c, BI_COL_Longitud__c,
                                                                                                                BI_COL_Estado_callejero__c,BI_COL_Gestion_callejero__c,BI_COL_Ciudad_Departamento__c,
                                                                                                                BI_COL_Direccion_split__c, BI_COL_Complemento_1__c,BI_COL_Complemento_2__c,BI_COL_Placa__c
                                                                                                from    BI_Sede__c
                                                                                                where   Id =: o.IdSucursal
                                                                                                limit   1 ];
                                        //Busca la Opcion Callejera
                                        List<BI_COL_Opcion_callejero__c> OpCall = [       Select  BI_COL_CodBar__c, BI_COL_CodDir__c, BI_COL_Complemento1__c, BI_COL_Complemento2__c,
                                                                                                                                                BI_COL_DesBar__c, BI_COL_DirSplit__c,BI_COL_Direccion__c, BI_COL_DirNueva__c, 
                                                                                                                                                BI_COL_EstadoRef__c, BI_COL_Latitud__c, BI_COL_Longitud__c, BI_COL_NSE__c, BI_COL_Placa__c,
                                                                                                                                                BI_COL_Direccion_Sede__c,BI_COL_Zona__c 
                                                                                                                        from    BI_COL_Opcion_callejero__c
                                                                                                                        where   Id =: oPcCall.strCheck
                                                                                                                        limit   1 ];
                                        //Valida que las listas tengan registros
                                        System.debug('\n\n##OpCall.size(): ' + OpCall.size()+' ## oSuc.size() ' +oSuc.size()+ '\n\n');
                                        
                                        if( OpCall.size() > 0 && oSuc.size() > 0 ){
                                                //Asigan valores
                                                oSuc[0].Name                        = oPCall[0].BI_COL_Direccion_Sede__c;
                                                oSuc[0].BI_Direccion__c             = oPCall[0].BI_COL_Direccion_Sede__c;    
                                                oSuc[0].BI_DireccionNormalizada__c = OpCall[0].BI_COL_Direccion_Sede__c;
                                                oSuc[0].BI_Localidad__c = OpCall[0].BI_COL_DesBar__c;
                                                oSuc[0].BI_COL_Latitud__c = OpCall[0].BI_COL_Latitud__c;
                                                oSuc[0].BI_COL_Longitud__c = OpCall[0].BI_COL_Longitud__c;
                                                
                                                oSuc[0].BI_COL_Direccion_split__c = OpCall[0].BI_COL_DirSplit__c; //OA:18-09-2012 se agregan campo faltante
                                                oSuc[0].BI_COL_Complemento_1__c = OpCall[0].BI_COL_Complemento1__c; //OA:18-09-2012 se agregan campo faltante
                                                oSuc[0].BI_COL_Complemento_2__c = OpCall[0].BI_COL_Complemento2__c; //OA:18-09-2012 se agregan campo faltante
                                                oSuc[0].BI_COL_Placa__c = OpCall[0].BI_COL_Placa__c; //OA:18-09-2012 se agregan campo faltante
                                                
                                                oSuc[0].BI_COL_Estado_callejero__c = 'Validado por callejero';
                                                oSuc[0].BI_COL_Gestion_callejero__c = true;
                                                
                                                //System.debug('\n\n##Direccion##' + OpCall[0].Direcci_n__c + '\n\n');
                                                //System.debug('\n\n##IdDireccion##' + IdDireccion + '\n\n');
                                                
                                                /*List<BI_Punto_de_instalacion__c> lstSuc = [SELECT BI_Sede__c, BI_Sede__r.BI_COL_Estado_callejero__c
                                                                                                         FROM BI_Punto_de_instalacion__c
                                                                                                        WHERE BI_Sede__r.BI_DireccionNormalizada__c =: OpCall[0].BI_COL_Direccion_Sede__c
                                                                                                        and   BI_Sede__r.BI_COL_Latitud__c=:OpCall[0].BI_COL_Latitud__c
                                                                                                        and   BI_Sede__r.BI_COL_Longitud__c=:OpCall[0].BI_COL_Longitud__c
                                                                                                        and   BI_Cliente__c =: IdDireccion
                                                                                                        limit 1];*/
                                                                                                        
                                                /* consulta inicial pero cambio la lupa vieja a cuentas por el ID de la direccion
                                                List<BI_Sede__c> lstSuc = [Select Id,BI_COL_Estado_callejero__c
                                                                                                        from    BI_Sede__c
                                                                                                        where   BI_DireccionNormalizada__c =: OpCall[0].BI_COL_Direccion_Sede__c
                                                                                                        and             BI_COL_Estado_callejero__c = 'Validado por callejero'
                                                                                                        and             Id =: IdDireccion
                                                                                                        and     BI_COL_Latitud__c=:OpCall[0].BI_COL_Latitud__c
                                                                                                        and     BI_COL_Longitud__c=:OpCall[0].BI_COL_Longitud__c
                                                                                                        limit   1];*/
                                                List<BI_COL_Opcion_callejero__c> lstOpcalle = [ Select  Id, BI_COL_Direccion_Sede__c 
                                                                                                        From BI_COL_Opcion_callejero__c
                                                                                                        where Id =: oPcCall.strCheck
                                                                                                        limit 1
                                                                                              ]; // Se creó la query con el fin de listar las direcciones de Opción callejero


                                                
                                                List<BI_Sede__c> lstSuc = [Select Id,BI_COL_Estado_callejero__c
                                                                                                        from    BI_Sede__c
                                                                                                        where   Id =: IdDireccion  
                                                                                                        and BI_COL_Direccion_sin_espacio__c =:lstOpcalle [0].BI_COL_Direccion_Sede__c 
                                                                                                        limit   1]; // Se añadió validación de BI_COL_Direccion_sin_espacio__c
                                                                                                        
                                                //Valida si tiene otra sucursal con la misma direccion
                                                if( lstSuc.size() > 0 ){
                                                        oSuc[0].BI_COL_Estado_callejero__c = 'Dirección duplicada';
                                                        //oSuc[0].Direcci_n_Sucursal__c = Label.MensajeErrorSucursal;
                                                }
                                                //Verifica el estado
                                                system.debug('\n\n >>>>> ' + oSuc[0].BI_COL_Estado_callejero__c + '\n\n');
                                                if( oSuc[0].BI_COL_Estado_callejero__c <> 'Dirección duplicada' ){
                                                        //Borra las Opciones Callejero
                                                        fnDelete( oSuc[0] );
                                                }
                                                else{
                                                        if( blClose )
                                                                blClose = false;
                                                }
                                        }
                                        //Actualiza
                                        update oSuc[0];
                                
                                        //Validar que si la dreccion es duplicada, se mantenga en la misma pantalla
                                        if(oSuc[0].BI_COL_Estado_callejero__c == 'Dirección duplicada'){
                                                BI_COL_Useful_messages_cls.mostrarMensaje(BI_COL_Useful_messages_cls.ERROR, 'La dirección: ' + oSuc[0].BI_DireccionNormalizada__c +' se encuentra duplicada' );
                                                return null;
                                        }
                                }
                                
                        }
                        
                        pageRef=new PageReference('/'+IdDireccion);
                        pageRef.setRedirect(true);
                }
                catch( System.exception ex ){
                        Apexpages.Message msg = new Apexpages.Message( Apexpages.Severity.ERROR, ex.GetMessage() );
                        Apexpages.addMessage( msg );
                }
                return pageRef;
        }
        /**
        *       Funciona para borrar las Opciones Callejero
        *       @param: Objeto Sucursal__c
        */
        public void fnDelete( BI_Sede__c oS ){
                //Busca todas las Opciones
                List<BI_COL_Opcion_callejero__c> lstOpCall = [    Select  Id
                                                                                                from    BI_COL_Opcion_callejero__c
                                                                                                where   BI_COL_Direccion__c =: oS.Id
                                                                                                limit   100 ];
                //Valida que tenga registros
                if( lstOpCall.size() > 0 ){
                        //Delete
                        delete lstOpCall;
                }
        }
        /**
        *       Save
        */
        public PageReference Draw(){
                return null;
        }
        /**
        *       Busca las Opciones Callejero
        *       @return: List<WrpOpcCallejero>
        */
        public List<WrpSucOpcCallejero> getlstSucOpCa(){
                System.debug('\n\n##Entro##' +  + '\n\n');
                //Valida que la lista tenga registros
                if( lstSucOpCa.size() <= 0 ){
                        //Variables
                        WrpSucOpcCallejero oSucursalOpcionCallejero;
                        //Lista de Opcion callejero
                        List<BI_COL_Opcion_callejero__c> lstOc = new List<BI_COL_Opcion_callejero__c>();
                        //Busca las Sucursales del Cliente
                        
                        List<BI_Sede__c> lstS = [ Select  Id, Name, BI_COL_Estado_callejero__c,
                                                                                                (Select BI_COL_Direccion_Sede__c, Id From BI_COL_Direccion__r)
                                                                                from    BI_Sede__c
                                                                                where   Id =: IdDireccion
                                                                                limit   1000];
                        //Valida si tiene Registros
                        System.debug('\n\n lstS'+lstS+'\n\n');
                        if( lstS.size() > 0 ){
                                //Recorro la lista
                                for( BI_Sede__c oS : lstS ){
                                        //Lista de Opciones callejero
                                        lstOc = oS.BI_COL_Direccion__r;
                                        System.debug('\n\n##oS ##' + oS + '\n\n');
                                        System.debug('\n\n##lstOc ##' + lstOc + '\n\n');
                                        //Valida si tiene Registros
                                        if( lstOc.size() > 0 ){
                                                //Carga el Objeto
                                                oSucursalOpcionCallejero = new WrpSucOpcCallejero( oS , lstOc );
                                                //Agrega a la Lista
                                                lstSucOpCa.add( oSucursalOpcionCallejero );
                                        }
                                }
                        }
                }
                //Return
                return lstSucOpCa;
        }
        /**
        *       
        */
        public class WrpSucOpcCallejero{
                
                public Id IdSucursal {get;set;}
                public String strName {get;set;}
                public String strEstado {get;set;}
                public WrpOpcCallejero OpcCallejero {get;set;}
                
                public WrpSucOpcCallejero(){}
                
                public WrpSucOpcCallejero( BI_Sede__c oS, List<BI_COL_Opcion_callejero__c> lst ){
                        this.IdSucursal = oS.Id;
                        this.strName = oS.Name;
                        this.strEstado = oS.BI_COL_Estado_callejero__c;
                        this.OpcCallejero = new WrpOpcCallejero( lst );
                }
        }
        /**
        *       Objeto Opcion Callejero
        */
        public class WrpOpcCallejero{
                
                public List<SelectOption> Item {get;set;}
                public String strCheck {get;set;}
                
                public WrpOpcCallejero(){}
                
                public WrpOpcCallejero( List<BI_COL_Opcion_callejero__c> lst ){
                        
                        this.strCheck = '';
                        this.Item = new List<SelectOption>();
                        //
                        if( lst.size() > 0 )
                        {
                                //Recorre la lista
                                for( BI_COL_Opcion_callejero__c oOc : lst )
                                {
                                        if(oOc.Id!=null && oOC.BI_COL_Direccion_Sede__c!=null)
                                        {
                                                this.Item.add( new SelectOption( oOc.Id , oOC.BI_COL_Direccion_Sede__c ) );
                                        }
                                }
                        }
                        else{
                                this.Item.add( new SelectOption( '0' , 'No tiene Opciones Reportadas.' ) );
                        }
                        
                }
        }

}