/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for ADQ_TipoCambioController class
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    09/02/2017              Pedro Párraga            Initial Creation
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class ADQ_TipoCambioController_TEST {

	public static testMethod void ADQ_TipoCambioController(){
		for(Integer i = 1;i<=10;i++){
			insert new TipoCambio__c(CurrencyIsoCode = 'USD', CodigoDivisa__c = 'USD');
		}

		TipoCambio__c tc1 = new TipoCambio__c();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(tc1);
		ADQ_TipoCambioController tc = new ADQ_TipoCambioController(sc);		
		tc.isTest = true;
		tc.valueUSD = '1.5';
		tc.valueEUR = '1.5';
		tc.insertDivisa();
		tc.getListaHistorialNext();
		tc.Prev();
		tc.Next();
		tc.getFechaHoy();
		tc.getLabelTabla();
		tc.getShownext();
		tc.getShowprev();
		tc.getListaGenerada();
		tc.getTotalRegistros();
		tc.refresh();
	}
}