@isTest(isParallel=true)
public class TGS_TestDummy_TEST {

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test class to manage the coverage code for TGS_Dummy_Test_Data class 

    <Date>                  <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyHierarchy
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyHierarchyTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(new User(Id = UserInfo.getUserId())) {
            TGS_Dummy_Test_Data.setUserType(userTest.Id, true, false);
        }
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyHierarchy();
            //TGS_Dummy_Test_Data.dummyHierarchy('parameter');
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyContact
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyContactTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            TGS_Dummy_Test_Data.dummyContact(le.Id);
        }
    }
 
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyOrderCase
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyOrderCaseTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyOrderCase();
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyTicketCase and dummyWorkInfo
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyWorkInfoTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Case caseTest = TGS_Dummy_Test_Data.dummyTicketCase();
            TGS_Dummy_Test_Data.dummyWorkInfo(caseTest.Id);
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyCaseWithAttachments
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyCaseWithAttachmentsTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyCaseWithAttachments();
        }
    }
    
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyOrder
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyOrderTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyOrder();
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyCatalogItem
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyCatalogItemTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyCatalogItem();
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyOrderItem
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyOrderItemTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyOrderItem();
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyConfiguration
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyConfigurationTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyConfiguration();
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyConfigurationAsset
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyConfigurationAssetTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyConfigurationAsset();
        }
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyCommercialProduct
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyCommercialProductTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyCommercialProduct();
        }
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createDummyConfigurationEnterpriseManagedMobilityTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact cont = TGS_Dummy_Test_Data.dummyContact(le.Id);
            TGS_Dummy_Test_Data.createDummyConfigurationEnterpriseManagedMobility(le.Id, cont.Id);
        }
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.createDummyConfigurationSecurityServices
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createDummyConfigurationSecurityServicesTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact cont = TGS_Dummy_Test_Data.dummyContact(le.Id);
            TGS_Dummy_Test_Data.createDummyConfigurationSecurityServices(le.Id, cont.Id);
        }
    }
    
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyPuntoInstalacion
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyPuntoInstalacionTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Id businessUnit = TGS_Portal_Utils.getLevel4(userTest.Id, 0)[0].Id;
            TGS_Dummy_Test_Data.dummyPuntoInstalacion(businessUnit);
        }
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.createDummyConfigurationComplexProduct
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createDummyConfigurationComplexProductTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            Contact cont = TGS_Dummy_Test_Data.dummyContact(le.Id);
            TGS_Dummy_Test_Data.createDummyConfigurationComplexProduct(le.Id, cont.Id);
        }
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.createTestRecords
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 /*   static testMethod void createTestRecordsTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.createTestRecords(3,3);
        }
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyUserTGS and setPermissionToUser
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 /*   static testMethod void dummyUserTGSTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
            TGS_Dummy_Test_Data.setPermissionToUser(u, 'TGS');
        }
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyCaseTGS
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyCaseTGSTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyCaseTGS(Constants.RECORD_TYPE_TGS_INCIDENT, 'Authorized user');
        }
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyContactTGS
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyContactTGSTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            TGS_Dummy_Test_Data.dummyContactTGS('Test');
        }
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Dummy_Test_Data.dummyEntitlementTGS
            
     <Date>                 <Author>                <Change Description>
    16/06/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void dummyEntitlementTGSTest() {
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            Account le = TGS_Dummy_Test_Data.dummyHierarchy();
            TGS_Dummy_Test_Data.dummyEntitlementTGS('test', le.Id);
        }
    }
    
    static testMethod void getPortalUserTest() {
        User creator = TGS_Dummy_Test_Data.dummyUserTGS('TGS Order Handling');
        creator.BI_Permisos__c = 'TGS'; // Add to Method?
        insert creator;

        System.runAs(new User(Id = UserInfo.getUserId())) {
            TGS_Dummy_Test_Data.addUsersToPortalGroup(new List<Id>{ creator.Id });
        }
        System.runAs(creator) {
            TGS_Dummy_Test_Data.setUserType(creator.Id, true, false);
        }
        Account le;
        System.runAs(new User(Id = UserInfo.getUserId())) {
            le = TGS_Dummy_Test_Data.dummyHierarchy();
        }

        System.runAs(creator) {
            User usr = TGS_Dummy_Test_Data.getPortalUserFromAccount(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, le, true);
        }
    }
    //25/10/2016 JCT Added calls to non-tested methods.
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Juan Carlos Terrón
        Company:       New Energy Aborda
        Description:   Method to test dummyEndpointsTGS_TEST.

        History:   
        <Date>                  <Author>                <Change Description>        
        25/10/2016              JC Terrón               Initial Version.
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    public static testMethod void dummyEndpointsTGS_TEST(){
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Juan Carlos Terrón
        Company:       New Energy Aborda
        Description:   Method to test setPermissionToUser_TEST.

        History:   
        <Date>                  <Author>                <Change Description>        
        25/10/2016              JC Terrón               Initial Version.
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static testMethod void setPermissionToUser_TEST(){

        /*User user = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        insert user;*/
        User user = [Select Id from User where Id = :UserInfo.getUserId()];
        TGS_Dummy_Test_Data.setPermissionToUser(User,'TGS_User');
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Juan Carlos Terrón
        Company:       New Energy Aborda
        Description:   Method to test dummyConfigurationOrderLookup_TEST.

        History:   
        <Date>                  <Author>                <Change Description>        
        25/10/2016              JC Terrón               Initial Version.
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static testMethod void dummyConfigurationOrderLookup_TEST(){
        TGS_Dummy_Test_Data.dummyConfigurationOrderLookup();            
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Juan Carlos Terrón
        Company:       New Energy Aborda
        Description:   Method to test dummyConfigurationOrderCCAndBU_TEST.

        History:   
        <Date>                  <Author>                <Change Description>        
        25/10/2016              JC Terrón               Initial Version.
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static testMethod void dummyConfigurationOrderCCAndBU_TEST(){
        TGS_Dummy_Test_Data.dummyConfigurationOrderCCAndBU();              
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Juan Carlos Terrón
        Company:       New Energy Aborda
        Description:   Method to test dummyConfigurationOrder_TEST.

        History:   
        <Date>                  <Author>                <Change Description>        
        25/10/2016              JC Terrón               Initial Version.
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static testMethod void dummyConfigurationOrder_TEST(){
        TGS_Dummy_Test_Data.dummyConfigurationOrder();               
    }               

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture - New Energy Aborda
    Description:   Test method to manage the code coverage from TGS_Dummy_Test_Data.insertFullStackData
    
    History:
    
    <Date>            <Author>              <Description>
    27/09/2017        Guillermo Muñoz       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void insertFullStackData_TEST(){
      TGS_Dummy_Test_Data.insertFullStackData();
    }
}