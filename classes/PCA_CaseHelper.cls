public without sharing class PCA_CaseHelper {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Diego Arenas
	    Company:       Salesforce.com
	    Description:   Class that save new case
	    
	    History:
	    
	    <Date>            <Author>          	<Description>
	    24/07/2014        Diego Arenas      	 Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


	public static boolean saveCase(Case newcase){
		boolean haveError = false;
		
		try{
			system.debug('****newCase:'+ newCase);
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			insert newCase;
			return haveError;
		}catch(Exception exc){
			system.debug(exc.getMessage());
			BI_LogHelper.generate_BILog('PCA_CaseHelper.saveCase', 'Portal Platino', exc, 'Class');
			apexpages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.BI_ErrorCrearCaso));
			//errorMessage = '';
			haveError = true;
			return haveError;
		}		
	}

}