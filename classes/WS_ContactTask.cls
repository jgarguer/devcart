/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    22015-06-02      Daniel ALexander Lopez (DL)     Cloned Web service      
*************************************************************************************/
global class WS_ContactTask {

	global class Response 
	{
		webservice boolean isSuccess;
		webservice string Error;
	}
	/***************************************************************************************/
	global class notificacionRequest{
		webservice String id;
		webservice String mensaje;
		webservice String sistema;
		webservice String estado;			
	}
	
	global class respCambioSucursalRequest{
        webservice String IdMs;
        webservice String IdDs;
        webservice String mensaje;
        webservice String codError;
        webservice String cuenta;//cuenta davox
        webservice String idCliente;
    }
    
    webservice static Response respCambioSucursal(List<respCambioSucursalRequest> lista, String sistema){
        Response respuesta=new Response();
        set<String> lstMs=new set<String>();
        set<String> lstDs=new set<String>();
        set<String> lstCuentas=new set<String>();
        set<String> lstClientes=new set<String>();
        map<Id,String> mapaDS=new map<Id,String>();  
        map<Id,String> mapaMS=new map<Id,String>();
		List<BI_COL_Modificacion_de_Servicio__c> lstModServ=null;
		List<BI_COL_Descripcion_de_servicio__c> lstDescServ=null;
		BI_Log__c logTrans=null;
		List<BI_Log__c> lstLogInsertar=new List<BI_Log__c>();
        
        /*********************** buscando los ID de las MS y DS que son afectadas ************************************/
		for(respCambioSucursalRequest req:lista){
			if(req.IdMs!=null && req.IdMs!='' && !lstMs.contains(req.IdMs))
				lstMs.add(req.IdMs);
			if(req.IdDs!=null && req.IdDs!='' && !lstDs.contains(req.IdDs))
				lstDs.add(req.IdDs);
			if(req.cuenta!=null && req.cuenta!='' && !lstCuentas.contains(req.cuenta))
				lstCuentas.add(req.cuenta);
            if(req.IdCliente!=null && req.idCliente!='' && !lstCuentas.contains(req.idCliente))
				lstClientes.add(req.idCliente);
        }
                
		if(sistema=='Davox' || sistema=='davox'){
			system.debug('----sistema davox------->');
			system.debug('\n\n----Datos a consultar:------->'+lstCuentas);
			system.debug('\n\n----Datos a consultar2:------->'+lstClientes);
			lstModServ=[select Id, Name from BI_COL_Modificacion_de_Servicio__c 
						where /*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c IN:lstCuentas 
						and BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c=: lstClientes];//10/11/2016 GSPM: Se comentó el campo antiguo y se adiciono nuevo campo davox1
			system.debug('\n\n--Ms Encontradas--'+lstModServ);
			for(BI_COL_Modificacion_de_Servicio__c req:lstModServ){
				logTrans=new BI_Log__c();
				//logTrans.Name='SW Notificaciones Masivo Sucursales';
				logTrans.BI_COL_Informacion_recibida__c=lista[0].mensaje;
				logTrans.BI_COL_Tipo_Transaccion__c='ENTRADA NOTIFICACIONES - ';
				if(lista[0].codError!=null && lista[0].codError.equals('0'))
					logTrans.BI_COL_Estado__c='Exitoso';
				else
					logTrans.BI_COL_Estado__c='Fallido'; 
					logTrans.BI_COL_Modificacion_Servicio__c=req.Id;
				lstLogInsertar.add(logTrans);
			}
		}
        /*else{
        	
        	lstDescServ=[select Id, Name from Nueva_DS__c where Name IN:lstDs];	
        	lstModServ=[select Id, Name from Modificacion_servicio__c where Name IN:lstMs];    */    	
        	
        	/************** creando el mapa de Id y Name de las MS y DS que fueron encontradas ****************************/
	      /*  map<String,Id> mapIdMs=new map<String,Id>();    
	        map<String,Id> mapIdDs=new map<String,Id>();
	        
	        for(Modificacion_servicio__c ms: lstModServ){
	            mapIdMs.put(ms.Name, ms.Id);
	        }   
	         
	        for(Nueva_DS__c ds: lstDescServ){
	            mapIdDs.put(ds.Name, ds.Id);
	        }*/
	        /**************************************************************************************************************/
        	
        	 /************** inserción de log en los respectivos registros encontrados *************************************/
                
	       /* for(respCambioSucursalRequest req:lista){
	            logTrans=new Log_Transaccion__c();
	            logTrans.Name='SW Notificaciones Masivo Sucursales';
	            logTrans.Respuesta__c=req.mensaje;
	            logTrans.TipoTransaccion__c='ENTRADA NOTIFICACIONES - ';
	            if(req.codError!=null && req.codError.equals('0'))
	                logTrans.Estado__c='Exitoso';
	            else
	                logTrans.Estado__c='Fallido';
	            if(mapIdMs.containskey(req.IdMs))
	                logTrans.Modificacion_servicio__c=mapIdMs.get(req.IdMs);
	            if(mapIdDs.containskey(req.IdDs))
	                logTrans.ds_restructurada__c=mapIdDs.get(req.IdDs); 
	                
	            lstLogInsertar.add(logTrans);
	        }*/
	        /**************************************************************************************************************/
       // }
        /**************************************************************************************************************/
        
       /* try{
            system.debug('----insertando log---'+lstLogInsertar);
            insert lstLogInsertar;
            respuesta.Error='';
            respuesta.isSuccess=true;
        }catch(Exception e){
            respuesta.Error=e.getMessage();
            respuesta.isSuccess=false;
        }    */   
                
        return respuesta;
    }
	
	private static Response insertaLogTransaccion(List<notificacionRequest> lista, String Objeto )
	{
		Response respuesta=new Response();
		BI_Log__c logTrans=null;
		List<BI_Log__c> lstLogInsertar=new List<BI_Log__c>();
	    
		for(notificacionRequest tmp:lista)
		{
			System.debug('\n\n temp: '+tmp+' \n\n');
			logTrans=new BI_Log__c();
	     	// logTrans.Name='SW Notificaciones';
			logTrans.BI_COL_Tipo_Transaccion__c='ENTRADA NOTIFICACIONES - '+tmp.sistema;
	      
			if(tmp.mensaje=='0')
			{
				logTrans.BI_COL_Estado__c='Exitoso';
				logTrans.BI_COL_Informacion_recibida__c='Se Creo/actualizo satisfactoriamente en '+ tmp.sistema;
			}
			else
			{
				logTrans.BI_COL_Estado__c='Fallido';
				logTrans.BI_COL_Informacion_recibida__c='Se presento el siguiente error: '+tmp.mensaje+' '+tmp.estado+ ' En '+tmp.sistema;
			}
	      
			if(objeto.equals('Sucursal'))
				logTrans.BI_COL_Sede__c=tmp.id;
			else if(objeto.equals('Cuenta'))
				logTrans.BI_COL_Cuenta__c=tmp.id;
			else if(objeto.equals('Contacto'))
				logTrans.BI_COL_Contacto__c=tmp.id;
	      lstLogInsertar.add(logTrans);
		}
	    try
	    {
			system.debug('----insertando log---'+lstLogInsertar);
			insert lstLogInsertar;
			respuesta.Error='';
			respuesta.isSuccess=true;
	    }
	    catch(Exception e)
	    {
			respuesta.Error=''+e.getMessage();
			respuesta.isSuccess=false;
		}
		return respuesta;
	} 
		
	webservice static Response NotificacionTRSSucursal(List<notificacionRequest> lista){
		Response respuesta=new Response();
		respuesta=insertaLogTransaccion(lista,'Sucursal');
		return respuesta;
	}
	     
	webservice static Response NotificacionTRSCliente(List<notificacionRequest> lista){
		Response respuesta=new Response();
		respuesta=insertaLogTransaccion(lista,'Cuenta');
		return respuesta;
	}
	    
	    webservice static Response NotificacionTRSContacto(List<notificacionRequest> lista){
	     	Response respuesta=new Response();
	     	respuesta=insertaLogTransaccion(lista,'Contacto');
	     	return respuesta;
     }
	/***************************************************************************************/
	webservice static Response getDatosClientes (String NIT, String IdSFDC, String Sistema, String NombContacto, String Apellidos, String Representante,
											   String Teloficina, String Telefono, String Movil, String Tipoid, String NumId, List<String> TipoContacto,
											   String email, String DirOficina, String ciudad, String pais, String IdContacto, String IdCliente, 
											   String TipoDocCliente, String Estado )
	{
		//Recibe los datos de un cliente para crearse o actualizarse, El id salesforce es existente y con este se consulta en el objeto Account, 
		//Para saber el ownerId de este y asi generarle la tarea a esa persona. en el campo descripcion se envian el resto de los datos recibidos
		//Para que creen o actualizen un registro.
		Response response = new Response();
		Account objCuenta = new Account();
		List<Account> lstCuenta = [Select Id 
						From Account
						Where BI_No_Identificador_fiscal__c =: IdCliente];
		if(lstCuenta.size()>0)
		{
			objCuenta = lstCuenta.get(0);
			//Obtengo el Id del recordType llamado: Gestión Service Manager
			List<RecordType> lstRecord = [Select Id, Name From RecordType]; //Where Name =: 'Gestión Service Manager'];
			RecordType objRecord = new RecordType();
			if(lstRecord.size()>0)
			{
				//objRecord = lstRecord.get(0);
				String tpConc='';
				// Se crea la tarea ahora para el usuario asociado a la cuenta(NIT)
				if(TipoContacto!=null && TipoContacto.size()>0)
				{
					for(String tc:TipoContacto)
					{
						tpConc+=tc+';';
					}
				}
				
				Contact objContact=new Contact();
				objContact.LastName=Apellidos;
				objContact.FirstName=NombContacto;
				objContact.BI_Representante_legal__c=false;
				objContact.Fax=Teloficina;
				objContact.Phone=Telefono;
				objContact.MobilePhone=Movil;
				objContact.BI_Tipo_de_documento__c=Tipoid;
				objContact.BI_Relacion_con_Telefonica__c=NumId;
				objContact.BI_Tipo_de_contacto__c=tpConc;
				objContact.Email=email;
				objContact.BI_COL_Direccion_oficina__c=DirOficina;
				objContact.AccountId=objCuenta.Id;
				objContact.BI_Activo__c=true;
				objContact.BI_Country__c=pais;
				
				 List<BI_Col_Ciudades__c> lstCiudad = [SELECT ID, BI_COL_Codigo_DANE__c 
                    FROM BI_Col_Ciudades__c
                    WHERE BI_COL_Codigo_DANE__c=:ciudad];
				if(lstCiudad.size()>0)
				{
					objContact.BI_COL_Ciudad_Depto_contacto__c=lstCiudad[0].Id;
				}
				
				Database.Saveresult dbsrConta = database.insert(objContact);
				
				if (dbsrConta.isSuccess())
				{
					response.isSuccess = true;
				} 
				else 
				{
					response.isSuccess = false;
					Database.Error e2 =dbsrConta.getErrors()[0];
					response.Error = String.valueOf(e2);
					
				} 
			}
		}
		return response;
	}
	
	webservice static Response getDatosSucursal (String IdSucursal, String ConsecutivoTRS){
		Response response = new Response();
		/*Sucursal__c objSucursal = new Sucursal__c();
		List<Sucursal__c> lstSucursal = [Select Id, C_digo_Sisgot__c 
										 From Sucursal__c
										 Where Id=:IdSucursal];
		if(lstSucursal.size()>0)
		{
			objSucursal = lstSucursal.get(0);
			objSucursal.C_digo_Sisgot__c = ConsecutivoTRS;
			System.debug('..==.. Objeto tarea ..==.. '+objSucursal);
			//Actualizar objSucursal;
			Database.Saveresult dbsr = database.update(objSucursal);
			if (dbsr.isSuccess()){
				response.isSuccess = true;
			} else {
				response.isSuccess = false;
				Database.Error e = dbsr.getErrors()[0];
				response.Error = String.valueOf(e);
				
			} 
		}*/
		return response;
	}
	
}