public class Order2AssetWithQueuable
{
    public Order2AssetWithQueuable() {}
    /* START Álvaro López 15/02/2018 - Case Manager fix csuids*/
    @future (callout = true)
    public static void order2Asset(String confId, String caseId, Boolean caseManagerProcess) 
    {
        //GMN 30/03/2017 - changed to avoid duplicate Assets in MWAN Cases
        /*String assetId;
        List <Case> lst_cas = [SELECT Order__r.NE__Asset__c FROM Case WHERE Id =: caseId AND Order__r.NE__Asset__c != null];

        if(!lst_cas.isEmpty()){

            List<NE__Order__c> lst_ord = [SELECT Id FROM NE__Order__c WHERE NE__Asset__c =: lst_cas[0].Order__r.NE__Asset__c AND RecordType.DeveloperName = 'Asset'];

            if(!lst_ord.isEmpty()){

                assetId = lst_ord[0].Id;
            }
            else{
                assetId  =   Order2AssetWithQueuable.callOrder2AssetMethod(confId,caseId);
            }
        }
        else{

            assetId  =   Order2AssetWithQueuable.callOrder2AssetMethod(confId,caseId);
        }*/
        //END GMN 30/02/2017
        
        String assetId = Order2AssetWithQueuable.callOrder2AssetMethod(confId,caseId); //Álvaro López 13/11/2017 - Fix asset error in modifications INC000000123226
        system.debug('Call integration with asset id: '+assetId);

        //Avoid uncatchable errors during test
        //if(Test.isRunningTest() == false)
            ID jobID = System.enqueueJob(new QueuableSetAssetAdditionalFields(assetId, caseId, confId, caseManagerProcess));
        /* END Álvaro López 15/02/2018 - Case Manager fix csuids*/
    }
    
    
    public Static String callOrder2AssetMethod(String confId, String caseId) 
    {
        /* Álvaro López 08/03/2017 - setTriggerFired for validateMSISDN method */
        NETriggerHelper.setTriggerFired('TGS_NEOrderItemMethods.validateMSISDN');
        
        system.debug('[class.FutureCallOrder2Asset.callOrder2Asset] Call future with order id: ' + confId+ ' caseid: ' + caseId);
        TGS_CallRodWs.inFutureContext   = true;
        TGS_CallRodWs.inFutureContextCI = true;
        TGS_CallRodWs.inFutureContextCIAtt = true;
        NETriggerHelper.setTriggerFired('NECheckOrderItems');
        NETriggerHelper.setTriggerFired('NECheckDeltaQuantity');
        NETriggerHelper.setTriggerFired('NESetDefaultOIFields');
        
        system.debug('[class.FutureCallOrder2Asset.callOrder2Asset] TGS_CallRodWs.inFutureContext: ' + TGS_CallRodWs.inFutureContext);
        //Call setgenerateCase in order to avoid to create more than 1 Case
        NETriggerHelper.setgenerateCase(false);
        String assetGeneratedId = NE.JS_RemoteMethods.order2asset(confId);
        /*START Álvaro López - Case Manager optimization*/  
        Integer tries = 1;
        System.debug(LoggingLevel.INFO, 'Llamada B2W ' + tries + ': ' + assetGeneratedId);
        while(!assetGeneratedId.startsWith('a0e') && tries < 3){
            assetGeneratedId = NE.JS_RemoteMethods.order2asset(confId);
            tries++;
            System.debug(LoggingLevel.INFO, 'Llamada B2W ' + tries + ': ' + assetGeneratedId);
        }
        /*END Álvaro López - Case Manager optimization*/
        NETriggerHelper.setgenerateCase(true);

        TGS_CallRodWs.inFutureContext   = false;
        TGS_CallRodWs.inFutureContextCI = false;
        
        return assetGeneratedId;
    }    
    
    public Static String setAssetAdditionalFields(String assetGeneratedId, String caseId, String confId) 
    {
        system.debug('[class.FutureCallOrder2Asset.callOrder2Asset] Call future with order id: ' + confId+ ' caseid: ' + caseId);
        TGS_CallRodWs.inFutureContext   = true;
        TGS_CallRodWs.inFutureContextCI = true;
        TGS_CallRodWs.inFutureContextCIAtt = true;
        NETriggerHelper.setTriggerFired('NECheckOrderItems');
        NETriggerHelper.setTriggerFired('NECheckDeltaQuantity');
        NETriggerHelper.setTriggerFired('NESetDefaultOIFields');        

        NETriggerHelper.setgenerateCase(true);

        NETriggerHelper.setTriggerFired('CallOrder2Asset');
        Case updCase            =   [SELECT Asset__c, Order__r.TGS_SIGMA_ID__c, Type, Order__c FROM Case WHERE id =: caseId];
        updCase.Asset__c        =   Id.valueOf(assetGeneratedId); //MGC 23/10/2017 - assetGeneratedId converted from String to Id
        Constants.isQueueableContext = true;
        update updCase;
        Constants.isQueueableContext = false;
        NE__Order__c assetGenerated             =   [SELECT Id, Case__c, NE__Asset__c, NE__OrderStatus__c
                                                            //TGS_RFB_date__c, TGS_RFS_date__c Deloitte (GAB) 17/04/2014 - Changed field for a formula one
                                                     FROM NE__Order__c 
                                                     WHERE Id = :assetGeneratedId];
                                                     
        assetGenerated.Case__c                  =   updCase.Id; 
        
        list<NE__OrderItem__c> assetItemGenerated =     [SELECT NE__Status__c, TGS_Service_status__c, NE__Action__c, NE__AssetItemEnterpriseId__c, 
                                                            (SELECT Id, NE__AttrEnterpriseId__c
                                                            FROM NE__Order_Item_Attributes__r)
                                                        FROM NE__OrderItem__c
                                                        WHERE NE__OrderId__c =: assetGenerated.Id];
                                                        
       /*                                                 
       system.debug('assetGeneratedIdassetGeneratedId '+assetGeneratedId);
       
       
         /*OI START                                            
        List<Id> idassetItem    = new  List<Id>();
        
        for(NE__OrderItem__c tmpId: assetItemGenerated )
        {
            system.debug('ID ORDERITEM: '+tmpId.id);
            idassetItem.add(tmpId.id);
        }
                                                        
        list<NE__Order_Item_Attribute__c>    assetItemAttributGenerated    =   [SELECT Id, NE__AttrEnterpriseId__c,NE__Value__c 
                                                                                FROM NE__Order_Item_Attribute__c
                                                                                WHERE NE__Order_Item__c IN: idassetItem]; */
                                                                                
        /*OI STOP*/                                
        
        NE__Order__c config = [SELECT NE__ServAccId__c, NE__BillAccId__c, NE__AssetEnterpriseId__c 
                               FROM NE__Order__c 
                               WHERE Id =: confId];
        
        map<String,NE__OrderItem__c> mapOrdItems = new map<String,NE__OrderItem__c>();
        map<String,NE__Order_Item_Attribute__c> mapOrdItAttr = new map<String,NE__Order_Item_Attribute__c>();
        
        if(config.NE__AssetEnterpriseId__c == null)
        {
            mapOrdItems = new map<String,NE__OrderItem__c>([SELECT Id, NE__AssetItemEnterpriseId__c FROM NE__OrderItem__c WHERE NE__OrderId__c =: confId]);
            mapOrdItAttr = new map<String,NE__Order_Item_Attribute__c>([SELECT Id, NE__AttrEnterpriseId__c, NE__Value__c FROM NE__Order_Item_Attribute__c WHERE NE__Order_Item__c IN: mapOrdItems.keySet()]);
        }
        
        list<NE__OrderItem__c> confItemsToUpd               =   new list<NE__OrderItem__c>();
        list<NE__Order_Item_Attribute__c> confItemAttrToUpd =   new list<NE__Order_Item_Attribute__c>();
        
        System.debug('State Case: '+updCase.Type);
        
        for(NE__OrderItem__c assetItem : assetItemGenerated)
        {
            if(updCase.Type == 'Disconnect')
            {
                assetItem.NE__Status__c = 'Disconnected';
                assetItem.TGS_Service_status__c = 'Deleted';
            }
            else if(updCase.Type == 'Change')
            {
                if(assetItem.NE__Status__c == 'Active')
                {
                    system.debug('CaSe In Change Active');
                    assetItem.TGS_Service_status__c = 'Deployed';
                }
                else
                {   //ALA 14/03/2018 - Existing debug update 
                    system.debug('CaSe In Change Else - CI Status From B2W: ' + assetItem.NE__Status__c);
                    assetItem.NE__Status__c = 'Disconnected';
                    assetItem.NE__Action__c = 'Remove';
                    assetItem.TGS_Service_status__c = 'Deleted';
                }
                
                                
                
               
            }
            else
            {
                assetItem.NE__Status__c = 'Active';
                assetItem.TGS_Service_status__c = 'Received';
                
                NE__OrderItem__c ordIt = mapOrdItems.get(assetItem.NE__AssetItemEnterpriseId__c);
                if(ordIt != null)
                {
                  //  ordIt.NE__AssetItemEnterpriseId__c = assetItem.Id;
                    confItemsToUpd.add(ordIt);
                    
                    for(NE__Order_Item_Attribute__c assetItAtt : assetItem.NE__Order_Item_Attributes__r)
                    {
                        NE__Order_Item_Attribute__c ordItAtt = mapOrdItAttr.get(assetItAtt.NE__AttrEnterpriseId__c);
                        if(ordItAtt != null)
                        {    
                            system.debug('AssetAttribute');
                            ordItAtt.NE__AttrEnterpriseId__c = assetItAtt.NE__AttrEnterpriseId__c;
                            confItemAttrToUpd.add(ordItAtt);
                        }
                    }
                }
            }
            
            assetItem.NE__Service_Account__c = config.NE__ServAccId__c;
            assetItem.NE__Billing_Account__c = config.NE__BillAccId__c;
            
            confItemsToUpd.add(assetItem);
        }
        
        update confItemsToUpd;
        update confItemAttrToUpd;
        
       list<NE__Order_Item_Attribute__c> confItemAttrToUpdInChange =   new list<NE__Order_Item_Attribute__c>();
        
        if(updCase.Type == 'Disconnect')
            assetGenerated.NE__OrderStatus__c = 'Disconnected';
        else if(updCase.Type == 'Change')
        {    
             assetGenerated.NE__OrderStatus__c = 'Active';  
        }
        else
        {
            assetGenerated.NE__OrderStatus__c = 'In progress';
            config.NE__AssetEnterpriseId__c = assetGenerated.Id;
            update config;
        }
        
        assetGenerated.TGS_SIGMA_ID__c = updCase.Order__r.TGS_SIGMA_ID__c;
        
       
        update assetGenerated;
        
        
        NE__Asset__c commAsset          =   [SELECT NE__Disable_Button__c, NE__Status__c, TGS_RFB_date__c, TGS_RFS_date__c 
                                             FROM NE__Asset__c 
                                             WHERE Id = :assetGenerated.NE__Asset__c];
        commAsset.TGS_RFB_date__c = updCase.TGS_RFB_date__c; 
        commAsset.TGS_RFS_date__c = updCase.TGS_RFS_date__c; 
        commAsset.NE__Disable_Button__c =   '';
        if(updCase.Type == 'Disconnect')
            commAsset.NE__Status__c = 'Disconnected';

        update commAsset;

        TGS_CallRodWs.inFutureContext   = false;
        TGS_CallRodWs.inFutureContextCI = false;

        return  assetGeneratedId;         
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture
    Description:   Method that create the asset of the case
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    07/05/2017                      Guillermo Muñoz             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public class QueableHelper implements Queueable{

        private Map <String,String> map_conf_cas;

        public QueableHelper(Map <String,String> map_conf_cas){

            this.map_conf_cas = map_conf_cas;
        }

        public void execute(QueueableContext context) {

            if(map_conf_cas != null && !map_conf_cas.isEmpty()){
                List <String> keys = new List <String>();
                keys.addall(map_conf_cas.keySet());

                String confId  = keys[0];
                String caseId = map_conf_cas.get(confId);

                String assetId  =   Order2AssetWithQueuable.callOrder2AssetMethod(confId,caseId);
                system.debug('Call integration with asset id: '+assetId);
        
                //Avoid uncatchable errors during test
                if(Test.isRunningTest() == false){
                    //ID jobID = System.enqueueJob(new QueuableSetAssetAdditionalFields(assetId, caseId, confId));
                    Order2AssetWithQueuable.QueuableSetAssetAdditionalFields(assetId, caseId, confId);
                }

                if(map_conf_cas.size() > 1){

                    map_conf_cas.remove(confId);
                    ID jobID = System.enqueueJob(new QueableHelper(map_conf_cas));
                }
            }
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture
    Description:   Method that fill values of the Case, Order, Order Items and Commercial Asset when the asset is generated
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    07/05/2017                      Guillermo Muñoz             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @future
    public static void QueuableSetAssetAdditionalFields(String inputAssetGeneratedId, String inputCaseId, String inputConfId)
    {

        String assetGeneratedId; 
        String caseId; 
        String confId;

        assetGeneratedId    =   inputAssetGeneratedId;
        caseId              =   inputCaseId;
        confId              =   inputConfId;

        system.debug('Call setAssetAdditionalFields');
        system.debug('assetGeneratedId: '+assetGeneratedId);
        system.debug('caseId: '+caseId);
        system.debug('confId: '+confId);
        
        Order2AssetWithQueuable.setAssetAdditionalFields(assetGeneratedId, caseId, confId); 

        list<NE__OrderItem__c> confItems = [SELECT Id FROM NE__OrderItem__c WHERE NE__OrderId__c =: assetGeneratedId];

        TGS_CallRodWs.inFutureContext   = false;
        TGS_CallRodWs.inFutureContextCI = false;
         /* Send Integration with in ROD in modification and termination */
        Set<Id> confitemsIds = new Set<Id>();
        for(NE__OrderItem__c ci:confItems)
        {           
            confitemsIds.add(ci.Id);
        }
        //Avoid uncatchable errors during test
        if(Test.isRunningTest() == false)        
            ID jobID = System.enqueueJob(new QueuableRODOrderItem(confitemsIds));
    } 
    
}