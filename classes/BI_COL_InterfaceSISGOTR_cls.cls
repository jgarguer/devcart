/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       20-Abr-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
global class BI_COL_InterfaceSISGOTR_cls 
{
	public static Boolean isTestMethod = false;
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: 
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public static void fnEnviarInfoSisgot( ws_TrsMasivo.InstalacionResponseDataType[] respuestas, Map<String,String> mapInfoEnviar )
	{
		String Resultado = '';
		List<String> idMs = new List<String>();
		map<string,string> resInterno = new map<string,string>();
		List<BI_Log__c> lstLogTran = new List<BI_Log__c>(); 

		List<BI_COL_Modificacion_de_Servicio__c> listModSer = [
			Select 	BI_COL_Estado_orden_trabajo__c, Name, BI_COL_Fecha_liberacion_OT__c, BI_COL_Codigo_unico_servicio__r.Name 
			from 	BI_COL_Modificacion_de_Servicio__c
			where 	Name in:mapInfoEnviar.keyset() ];
		
		for( BI_COL_Modificacion_de_Servicio__c ms : listModSer )
		{
			string RespuestaExitosa = respuestas[0].codigo;
			//si el envio fue exitoso, actualiza estado de la DS
			if( RespuestaExitosa == '0' )
			{
				ms.BI_COL_Estado_orden_trabajo__c = 'Solicitado';
				ms.BI_COL_Estado__c = 'Enviado';
				//Escribir resultado en el log
				lstLogTran.add( GuardarEnLog( 'SISGOT', 'Exitoso', String.valueof( mapInfoEnviar.get( ms.Name ) ) , 
					respuestas[0].error, ms.Name , 'Instalaciones', ms.Id ) );
			}
			else
			{
				lstLogTran.add( GuardarEnLog( 'SISGOT', 'Fallido', String.valueof( mapInfoEnviar.get( ms.Name ) ), 
					respuestas[0].error, ms.Name , 'Instalaciones', ms.Id ) );
				ms.BI_COL_Estado_orden_trabajo__c = 'Fallido';
			}
		}

		insert lstLogTran;

		BI_COL_FlagforTGRS_cls.flagTriggers = true;

		update listModSer;
		
		list<BI_Log__c> logList = new list<BI_Log__c>();

		Database.SaveResult[] lsr = database.update( listModSer, false );
   		
   		for( Database.SaveResult sr : lsr )
   		{
   			if(! sr.isSuccess() )
   			{
   				Database.Error err = sr.getErrors()[0];
   				string errorDetalles = '';

   				for( Database.Error errt:sr.getErrors() )
   					errorDetalles += errt.getMessage();

   				logList.add( new BI_Log__c( 
					BI_Tipo_de_componente__c = 'RESPUESTA TRS',
					BI_COL_Informacion_recibida__c = 'No se pudo actualizar el estado de la MS\n'+errorDetalles,
					BI_COL_Modificacion_Servicio__c = sr.getId(),
					BI_COL_Tipo_Transaccion__c = 'Liberar Ot',
					BI_COL_Estado__c = 'FALLIDO' ) );
   			}   		
   		}
   		insert logList;
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: 
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public static BI_Log__c GuardarEnLog( String SistemaExterno, String Estado, 
		String MensajeEnviado, String MensajeRecibido, String CodObjeto, String TipoTransaccion, String DSId )
	{
		BI_Log__c Log = new BI_Log__c();
		Log.BI_Tipo_de_componente__c = SistemaExterno;
		Log.BI_COL_Estado__c = Estado;	
		Log.BI_COL_Informacion_Enviada__c = MensajeEnviado;	
		Log.BI_COL_Informacion_recibida__c = MensajeRecibido; 
		Log.BI_Descripcion__c = CodObjeto;
		Log.BI_COL_Tipo_Transaccion__c = TipoTransaccion;
		Log.BI_COL_Modificacion_Servicio__c = DSId ;
		return Log;
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: 
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	WebService static String EnviarDatosBasicosDS( List<BI_COL_Modificacion_de_Servicio__c> idMS )
	{
		System.debug('	1 ================>  '+ idMS);
		
		//System.debug('================>  '+ idMS);
		List<String> listInfoEnviar = new List<String>();
		List<BI_COL_Configuracion_campos_interfases__c> listConfIfz = getConfInfz('SISGOT-INSTALACION','INSTALACIONES','SALIENTE');
		List<BI_COL_Configuracion_campos_interfases__c> listCampoInfz;
		List<BI_Log__c> lisLogT = new List<BI_Log__c>();

		Map<String,String> mapInfoEnviar = new Map<String,String>();
		
		String infoEnviar = '';
		String Resultado = '';
		String sqlArmar = 'SELECT ';
		String dsConcat = '';
		
		for( BI_COL_Modificacion_de_Servicio__c s : idMS )
			dsConcat += ( dsConcat == '' ?  '\''+s.Name+'\'' : ',\''+s.Name+'\'' ) ;
			System.debug('	2 ================>  '+ dsConcat);
		
		if( listConfIfz.size() > 0 )
		{
			listCampoInfz = configCampos( listConfIfz[0].id );
			
			for( BI_COL_Configuracion_campos_interfases__c cci:listCampoInfz)
				sqlArmar += ( sqlArmar == 'SELECT ' ?  cci.BI_COL_Nombre_campo__c : ',' + cci.BI_COL_Nombre_campo__c );
			
			sqlArmar += ' from ' + listConfIfz[0].BI_COL_Objeto__c;
			sqlArmar += ' where Name in(' + dsConcat + ')';
			system.debug('\n\n##sqlArmar: '+ sqlArmar +'\n\n');
			list<sObject> consultaArmada = Database.query(sqlArmar);
			BI_COL_Basic_pck_cls basic = new BI_COL_Basic_pck_cls();
			
			for( SObject so : consultaArmada )
			{
				String dsenviar = '';
				infoEnviar = '';

				for( BI_COL_Configuracion_campos_interfases__c cci : listCampoInfz )
				{
					String temp = '';
					Double dec = cci.BI_COL_Decimales__c;
					
					temp = basic.fn_ObtenerValorCampo( so, cci.BI_COL_Nombre_campo__c, cci.BI_COL_Tipo_campo__c, 
							cci.BI_COL_Formato_Campo__c, dec, ',', true, cci.BI_COL_Convertir_mayuscula__c );
					
					if( cci.BI_COL_Nombre_campo__c == 'Name' )
						dsenviar = temp;

					infoEnviar += listConfIfz[0].BI_COL_Separador__c + temp;
				}
				mapInfoEnviar.put( dsenviar, infoEnviar );
			}
		}

		ws_TrsMasivo.serviciosSISGOTSOAP wsTrsMSoap = new ws_TrsMasivo.serviciosSISGOTSOAP();
		wsTrsMSoap.timeout_x = 120000;

		ws_TrsMasivo.InstalacionRequestDataType[] arrParametros = new ws_TrsMasivo.InstalacionRequestDataType[ idMS.size() ];


		ws_TrsMasivo.InstalacionResponseDataType[] respuestas;

		List<String> idS = new List<String>();
		Map<String,String> mapInfoEnviada = new Map<String,String>();
		Map<String,String> mapInfoRecibida = new Map<String,String>();
		Integer cont = 0;

		for( BI_COL_Modificacion_de_Servicio__c infEn : idMS )
		{
			String infEnviaws='';

			if( mapInfoEnviar.containsKey( infEn.Name ) )
				infEnviaws = mapInfoEnviar.get( infEn.Name );

			infEnviaws = fn_ConvertirNs( infEnviaws );
			infEnviaws = fn_QuitarTildes( infEnviaws );
			ws_TrsMasivo.InstalacionRequestDataType Parametro = new ws_TrsMasivo.InstalacionRequestDataType();

			System.debug('\n infEnviaws============>>>>'+infEnviaws);
			Parametro.parametros = infEnviaws;
			arrParametros[cont] = Parametro;
			cont++;
		}

		try
		{
			//Invoca WS de SISGOT
			String RespuestaWS = '';

			if( !BI_COL_InterfaceSISGOTR_cls.isTestMethod )
			System.debug('\n arrParametros============>>>>'+arrParametros);
				respuestas = wsTrsMSoap.crearInstalaciones( arrParametros );
				System.debug('\n respuestas============>>>>'+respuestas);
			fnEnviarInfoSisgot( respuestas, mapInfoEnviar );
		}
		catch( System.Exception e )
		{
			List<BI_Log__c> lstLogTran = new List<BI_Log__c>(); 
			set<string> msID = new set<string>();
			//actualiza estado de la OT en la MS
			for( BI_COL_Modificacion_de_Servicio__c ms : idMS )
			{
				ms.BI_COL_Estado_orden_trabajo__c = 'Fallido Conexion';
				//Escribir resultado en el log
				String infEnviaws = mapInfoEnviar.get( ms.Name );
				
				lstLogTran.add( GuardarEnLog( 'SISGOT', 'Fallido Conexion', 
					infEnviaws,  
					ms.Name + ' No enviada a SISGOT. Conexión no disponible. Error: ' + e.getMessage(), 
					ms.Name, 'Instalaciones', 
					ms.Id ) );

				msID.add( ms.Id );
			}

			update idMS;

			insert lstLogTran;
			
			if( !test.isRunningTest() )
				BI_COL_Contract_cls.msFallida( msID );
			
			Resultado = dsConcat + ' No enviada a SISGOT. Conexión no disponible. Error: ' + e.getMessage();
		}
		//update idMS;
		return dsConcat;
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: 
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public static List<BI_COL_Configuracion_campos_interfases__c> getConfInfz( String interfaz, String TipoTransaccion, String Direccion )
	{
		String consultaSOQL = ' SELECT Id, BI_COL_Calificador_Texto__c, BI_COL_Descripcion__c, BI_COL_Linea__c, ' +
			' Name, BI_COL_Segmento__c, BI_COL_Separador__c, BI_COL_Separador_Decimal__c, BI_COL_Objeto__c ' +  
			' FROM 	BI_COL_Configuracion_campos_interfases__c ' + 
			' WHERE BI_COL_Interfaz__c = \''+interfaz+'\'' +
			' AND 	BI_COL_Tipo_transaccion__c = \'' + TipoTransaccion + '\'' +
			' AND 	BI_COL_Direccion__c = \'' + Direccion + '\'';
			System.debug('	3 ================>  '+ consultaSOQL);
		return Database.query( consultaSOQL );
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: 
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public static List<BI_COL_Configuracion_campos_interfases__c> configCampos( String identificadorInterfaz )
	{
		List<BI_COL_Configuracion_campos_interfases__c> consultaConfiguracionCampos = [
			SELECT 	BI_COL_Convertir_mayuscula__c, BI_COL_Decimales__c, BI_COL_Formato_Campo__c, 
					BI_COL_Longitud__c, 
					BI_COL_Configuracion_Interfaz__r.BI_COL_Segmento__c, 
					BI_COL_Configuracion_Interfaz__r.BI_COL_Linea__c, 
					BI_COL_Configuracion_Interfaz__r.BI_COL_Descripcion__c, 
					Name, BI_COL_Nombre_campo__c, BI_COL_Permite_nulos__c, BI_COL_Tipo_campo__c, 
					BI_COL_Valor_predeterminado__c 
			FROM 	BI_COL_Configuracion_campos_interfases__c 
			WHERE 	BI_COL_Configuracion_Interfaz__c =: identificadorInterfaz 
			AND 	IsDeleted = false 
			ORDER BY BI_COL_Configuracion_Interfaz__c ASC, BI_COL_Orden__c ASC];
		
		return consultaConfiguracionCampos;
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: enviar descripcion referencia
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public static string fn_ConvertirNs( string strFieldValue )
	{
		strFieldValue = strFieldValue.replace('ñ', 'n');
		strFieldValue = strFieldValue.replace('Ñ', 'N');
		return strFieldValue;
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: Función eliminar las tildes de un campo y dejar su caracter normal
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public static string fn_QuitarTildes(string strFieldValue)
	{
		// Elimina las tildes
		strFieldValue = strFieldValue.replaceAll('[\\xc0-\\xc6]','A');
		strFieldValue = strFieldValue.replaceAll('[\\xe0-\\xe6]','a');
		strFieldValue = strFieldValue.replaceAll('[\\xc8-\\xcb]','E');
		strFieldValue = strFieldValue.replaceAll('[\\xe8-\\xeb]','e');
		strFieldValue = strFieldValue.replaceAll('[\\xcc-\\xcf]','I');
		strFieldValue = strFieldValue.replaceAll('[\\xec-\\xef]','i');
		strFieldValue = strFieldValue.replaceAll('[\\xd2-\\xd6]','O');
		strFieldValue = strFieldValue.replaceAll('[\\xf2-\\xf6]','o');
		strFieldValue = strFieldValue.replaceAll('[\\xd9-\\xdc]','U');
		strFieldValue = strFieldValue.replaceAll('[\\xf9-\\xfc]','u');
		return strFieldValue;
	} 
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: interpreta la respuesta del WS de sisgot
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public static Boolean interpretarRespuesta( String respuesta )
	{
		Boolean RespuestaValida = false;

		String[] arrRespuesta = respuesta.split(',');
		
		if ( arrRespuesta[0] == 'true' )
			RespuestaValida = true;
		
		return RespuestaValida;
	}
	/***
	* @Author: 
	* @Company: Avanxo Colombia
	* @Description: determina si la cadena es nula o no, si la es devuelve cadena vacia
	* @History: 
	* Date      |   Author  |   Description
	* 
	***/
	public static Object EsNull( Object cadena )
	{
		if( cadena == null )
			return '';
		else
			return EncodingUtil.urlEncode( String.valueof( cadena ), 'UTF-8');
	}
}