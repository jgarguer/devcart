public without sharing class BI_G4C_Visitas_Controller {
    
    /*--------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Miguel Molina Cruz
Company:       everis
Description:   Controller for the Lightning Components BI_G4C_Visitas_Nueva, BI_G4C_Visitas_Lista, BI_G4C_Visitas_Cualificar

History: 

<Date>            <Author>                            <Code>        <Change Description>
20/06/2016          Miguel Molina Cruz                            Initial version
10/08/2016          Eduardo Ventura Izquierdo Nuñez         E001        Sustitución
26/12/2016          Carlos Andres Bastidas Hernandez        ESTE-F3-01      Nuevo metodo para obtener la Account
13/12/2017                  Antonio Mendivil Azagra(CoE Zaragoza)             Añadidos metodos customsearch,customSearchAndOrder,getEventByAccountOrderASC,
                                              updatesend, sendPDF,
                                              OrderBy,modificacion de getEventByAccount, y cambio en la gestion de la Fecha/hora
                                              para el desarrollo-573
                                              Añadida validacion para gestion de Oportunity
22/03/2018                  Álvaro López                                                    Fix D-573 para eventos sin cuenta asociada (AccountId)
06/04/2018          Antonio Mendivil(CoE Zaragoza)                  Fix D-573 Fecha de Visita para diferentes Zonas horarias
  --------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Id EventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Evento').getRecordTypeId();
    public final Event evt;
    public final List<Account> acc;
    public BI_G4C_Visitas_Controller(ApexPages.StandardController stdController) {
        Event current = (Event)stdController.getRecord();
        this.evt = [SELECT Id, BI_G4C_esVisita__c, AccountId, Location
                    FROM Event
                    WHERE Id =: current.Id
                    LIMIT 1
                   ];
        this.acc = [SELECT ID, Name, BI_Country__c
                    FROM Account
                    WHERE Id =: this.evt.AccountId
                    LIMIT 1
                   ];
        System.debug('@@@account: ' + this.acc);
    }
   public BI_G4C_Visitas_Controller() {

    }

     @AuraEnabled
    public static String geoloc(String eventId,String geoL){
    System.debug('GEOLOC---eventid---> '+eventId+' location---> '+geoL);    
      try{
            Event e = [select id, BI_G4C_GeoLocalizacion__c from event where id=:eventId];
            if (e != null){
                e.BI_G4C_GeoLocalizacion__c=geoL;
                update e;
                System.debug('GEOLOCATION UPDATED--->'+e.BI_G4C_GeoLocalizacion__c); 
            }  
        }catch(Exception ex){
          
        }
        return null;
    }
    
   @AuraEnabled
   public static Event[] customSearchAndOrder(String accountId, String searchKeyWord,Boolean isASC,String fieldToOrder){
       System.debug('Events customSearchAndOrder ACCOUNT ID ::'+accountId);
       String searchKey ='%' + searchKeyWord + '%';
       System.debug('Events SEARCHKEY--> ::'+searchKey);
       String campo='';
       String query='SELECT id, Location, EndDateTime, Description,StartDateTime, Subject, WhatId,BI_FVI_Estado__c,BI_FVI_Resultado__c,Who.Name, BI_G4C_Caducada__c,BI_Identificador_Interno__c,Owner.Name from Event where (recordtypeid=:EventRecordTypeId and whatid=:accountId and BI_G4C_esVisita__c=TRUE) AND (BI_Identificador_Interno__c LIKE: searchKey OR Who.Name LIKE: searchKey OR Owner.Name LIKE: searchKey OR BI_FVI_Estado__c LIKE: searchKey)  order by';
                                  
        if(fieldToOrder.equals('BI_Identificador_Interno__c')){
            campo=' BI_Identificador_Interno__c';
        }else if(fieldToOrder.equals('Contacto')){
             campo=' Who.Name';
        }else if(fieldToOrder.equals('Asunto')){
             campo=' Subject';    
        }else if(fieldToOrder.equals('FechaHora')){
             campo=' StartDateTime';
        }else if(fieldToOrder.equals('Asignado')){
             campo=' Owner.Name';
        }else if(fieldToOrder.equals('Estado')){
              campo=' BI_FVI_Estado__c';
        }
         query+=campo;
        if(isASC){
             query+=' ASC';
        }else{
            query+=' DESC';
        } 
       System.debug('Query ::'+query);
       List <Event> listEvents=database.query(query);
       System.debug('Events customSearchAndOrder ::'+listEvents);
       return listEvents;
    }
    
    @AuraEnabled
   public static Event[] customSearch(String accountId, String searchKeyWord){
        System.debug('Events customSearch ACCOUNT ID-->'+accountId);
       String searchKey ='%' + searchKeyWord + '%';
       System.debug('Events customSearch searchKey-->'+searchKey);
       //Añadir ORs sobre el resto de campos
       String query='SELECT id, Location, EndDateTime, Description,StartDateTime, Subject, WhatId,BI_FVI_Estado__c,BI_FVI_Resultado__c,Who.Name, BI_G4C_Caducada__c,BI_Identificador_Interno__c,Owner.Name from Event '
          +'where (recordtypeid=:EventRecordTypeId and whatid=:accountId and BI_G4C_esVisita__c=TRUE) AND (BI_Identificador_Interno__c LIKE: searchKey OR Who.Name LIKE: searchKey OR Owner.Name LIKE: searchKey OR BI_FVI_Estado__c LIKE: searchKey OR Subject LIKE: searchKey)  order by StartDateTime';
        
       List <Event> listEvents=database.query(query);
          System.debug('Events customSearch SEARCH -->'+listEvents);
        return listEvents;
    } 
    @AuraEnabled
    public static Event[] getEventByAccount(String accountId){
        System.debug('Events getEventByAccount ACCOUNT ID-->'+accountId);
        List <Event> listEvents=[SELECT id, Location, EndDateTime, Description,StartDateTime, Subject, WhatId,BI_FVI_Estado__c,
                BI_FVI_Resultado__c,Who.Name, BI_G4C_Caducada__c,BI_Identificador_Interno__c,Owner.Name,BI_Correo_electronico_enviado__c
                from Event where recordtypeid=:EventRecordTypeId and whatid=:accountId and BI_G4C_esVisita__c=TRUE order by StartDateTime];
          System.debug('Events getEventByAccount-->'+listEvents);
       /*FOR(Event e:listEvents){
            e.StartDateTime=e.StartDateTime
        }*/
        return listEvents;

    } 
    @AuraEnabled
    public static Event[] getEventByAccountOrderASC(String accountId,String fieldToOrder, String dir,Boolean isASC){
            System.debug('Events getEventByAccountOrderASC ACCOUNT ID-->'+accountId);
        String campo='';
         String query='SELECT id, Location, EndDateTime, Description,StartDateTime, Subject, WhatId,BI_FVI_Estado__c,BI_FVI_Resultado__c,Who.Name, BI_G4C_Caducada__c,BI_Identificador_Interno__c,Owner.Name from Event where recordtypeid=:EventRecordTypeId and whatid=:accountId and BI_G4C_esVisita__c=TRUE order by ';
        if(fieldToOrder.equals('BI_Identificador_Interno__c')){
            campo='BI_Identificador_Interno__c';
        }else if(fieldToOrder.equals('Contacto')){
             campo='Who.Name';
        }else if(fieldToOrder.equals('FechaHora')){
             campo='StartDateTime';
        }else if(fieldToOrder.equals('Asunto')){
             campo=' Subject';  
        }else if(fieldToOrder.equals('Asignado')){
             campo='Owner.Name';
        }else if(fieldToOrder.equals('Estado')){
              campo='BI_FVI_Estado__c';
        }
         query+=campo;
        if(isASC){
             query+=+' ASC';
        }else{
            query+=+' DESC';
        }
      System.debug('getEventByAccountOrderASC ISASC-->'+isASC);
       System.debug('getEventByAccountOrderASC Query-->'+query);
       List <Event> listEvents=database.query(query);
       return listEvents; 
    }
    /*INI ESTE-F3-01*/
    @AuraEnabled
    public static Account getAccountById(String accountId) {
        List<Account> accList = [SELECT id, Name, BillingStreet, BI_Country__c  from Account where id=:accountId];
        return accList.get(0);
    }
    

    //JEG
    @AuraEnabled
    public static String getCurrentUserData() {
      User user = [SELECT Id, FirstName, LastName, Profile.Name FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
      return JSON.serialize(user);
    }


    //JEG
    //Sobrecarga para evitar problemas de compatibilidad con clases anteriores.
    @AuraEnabled
    public static String updateEvent(String e, String a){
        return updateEvent(e,a, null);
    }


    /*INI E001*/
    @AuraEnabled
    public static String updateEvent(String e, String a, Id owner){
        
        System.debug('updateEvent--id owner-->'+owner);
        String estadoRet='OK';
        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(e);
        system.debug('updateEvent--El mapa tiene-->'+ m);
        system.debug('updateEvent--owner->'+m.get('Owner'));
        
    
        Event evento = new Event();
        String yD=(String)m.get('Id');
        if(yD!=null && yD.length()>10){
            evento.Id=(Id)m.get('Id');
            Event eo=[Select id,OwnerId From Event where id=: evento.Id Limit 1];
            evento.OwnerId=eo.OwnerId; 
        }else{
           //evento.OwnerID = owner; //JEG To fix assignation to Calendar
        }
        
        /*Adding BI_ECU_Gestion__c*/
        if(m.get('BI_ECU_Gestion__c')!=null){
            evento.BI_ECU_Gestion__c=(String)m.get('BI_ECU_Gestion__c');
        }

        if(m.get('BI_FVI_Estado__c')!=null){
            evento.BI_FVI_Estado__c=(String)m.get('BI_FVI_Estado__c');
        }
        if(m.get('BI_FVI_Resultado__c')!=null){
            evento.BI_FVI_Resultado__c=(String)m.get('BI_FVI_Resultado__c');
        }
        
        if(m.get('BI_G4C_ResultadoVisita__c')!=null){
            evento.BI_G4C_ResultadoVisita__c=(String)m.get('BI_G4C_ResultadoVisita__c');
        }
        evento.WhoId=(Id)m.get('WhoId');
        system.debug('@@@ m es'+ m);
        try{
            String f=(String)m.get('ActivityDateTime');
            
            System.debug('La fecha es '+f);
            //Formato de la fecha esperado:::13-ene-2017 12:30 (Locale España)
            //Formato de la fecha esperado:::2018-03-23T22:00:00.000Z (Locale Argentina)
            String dd;
            String MM;
            Integer mes;
            String yy;
            String hh;
            String mim;
            if (f.endsWith('Z') && f.contains('T')) {
                //Parseamos fecha en formato 2017-02-25T15:28:00.000Z
                String fPart0=f.split('T')[0];
                String fPart1=f.split('T')[1];

                yy=fPart0.split('-')[0];
                MM=fPart0.split('-')[1];
                dd=fPart0.split('-')[2];
                
                hh=fPart1.split(':')[0];
                mim=fPart1.split(':')[1];
                mes = Integer.valueOf(MM);
            }else{
                
                Map<String,Integer> mapaValores=new Map<String,Integer>{'dic' => 12, 'nov' => 11,'oct' => 10,'sept' => 9, 'ago' => 8,'jul' => 7, 'jun' => 6,'may' => 5, 'abr' => 4,'mar' => 3, 'feb' => 2,'ene' =>1};
                
                System.debug('updateEvent--fecha sin parsear-->'+f);
                
                String fPart0=f.split(' ')[0];
                String fPart1=f.split(' ')[1];
                
                System.debug('parte 0-->'+fPart0);
                System.debug('parte 1-->'+fPart1);
                //separar dias/mes/año y sustituir por su respectivo numero
                yy=fPart0.split('-')[2];
                MM=fPart0.split('-')[1];
                dd=fPart0.split('-')[0];
                
                hh=fPart1.split(':')[0];
                mim=fPart1.split(':')[1];
                
                mes = Integer.valueOf(mapaValores.get(MM));
            }
            String userLocale  = getUserDateTimeFormat();
            System.debug('USERLOCALE'+userLocale);
            System.debug('USERLOCALE::date'+DateTime.newInstance(Integer.valueOf(yy),mes ,Integer.valueOf(dd),Integer.valueOf(hh), Integer.valueOf(mim), 0));

           evento.ActivityDateTime= DateTime.newInstance(Integer.valueOf(yy),mes ,Integer.valueOf(dd),Integer.valueOf(hh), Integer.valueOf(mim), 0);

           //JRM -- 16/04/2018
           if(m.get('BI_FVI_Estado__c')== 'Nueva'){
           TimeZone tz = UserInfo.getTimeZone();
           Integer diferencia = tz.getOffset(Datetime.now())/1000/60/60;
           System.debug('updateEvent--Termina-->' + diferencia);
           evento.ActivityDateTime = evento.ActivityDateTime.addHours(diferencia);
           }
           //JRM -- 16/04/2018

           SYSTEM.DEBUG('USERLOCALE:::date-event'+  evento.ActivityDateTime);
           system.debug('updateEvent--Termina-->'+ evento.ActivityDateTime);
        }catch(Exception ex){
            system.debug('Excepción'+ex);
        }
        evento.DurationInMinutes=(Integer)m.get('DurationInMinutes');
        evento.Subject=(String)m.get('Subject');
        evento.WhatId=(Id)m.get('WhatId');    
        evento.BI_G4C_Tipo_de_Visita__c=(String)m.get('BI_G4C_Tipo_de_Visita__c');
        
        
        //
        if(m.get('BI_G4C_Oportunidad__c')==null){
            System.debug('NULL opo');
            evento.BI_G4C_Oportunidad__c=null;
        }else if(((String)m.get('BI_G4C_Oportunidad__c')).equals('aaa') || ((String)m.get('BI_G4C_Oportunidad__c')).equals('')){
             evento.BI_G4C_Oportunidad__c=null;
        }else{
             evento.BI_G4C_Oportunidad__c=(String)m.get('BI_G4C_Oportunidad__c');
        }
        //
       
        System.debug('La nueva OPO es....'+ evento.BI_G4C_Oportunidad__c);
        //if evento.BI_G4C_Tipo_de_Visita__c==BI_G4C_Tipo_de_Visita__c es "Visita  programada - Reunión" o "Visita en frío – Presencial" isvisita==true
        /*
        if(evento.BI_G4C_Tipo_de_Visita__C == 'Visita  programada - Reunión' || evento.BI_G4C_Tipo_de_Visita__C == 'Visita en frío – Presencial'){
            evento.BI_G4C_esVisita__c=true;
        }*/
        evento.Description=(String)m.get('Description');
        //evento.BI_G4C_Oportunidad__c=(String)m.get('BI_G4C_Oportunidad__c');
        
        
        Map<String, Object> auxData = (Map<String, Object>) JSON.deserializeUntyped(a);
        
        String estado = (String) evento.BI_FVI_Estado__c;
        
        if(estado == 'Nueva'){
            evento.BI_FVI_Estado__c = 'Planificada';
            evento.WhoId = (String) auxData.get('ContactId');
            evento.recordtypeid = EventRecordTypeId;
            List<Opportunity> opportunityList = [SELECT Id, CampaignId FROM Opportunity Where Id =: (String)m.get('BI_G4C_Oportunidad__c') AND AccountId =: evento.WhatId Limit 1];
            if(!opportunityList.isEmpty()){
                evento.BI_G4C_Oportunidad__c = (String) opportunityList[0].Id;
                evento.BI_CampaignId__c = opportunityList[0].CampaignId;
            }else{
                evento.BI_G4C_Oportunidad__c = null;
            }
        }
        System.debug('@@Tratamos el estado que es '+estado);
        if(estado == 'Iniciada') {
            evento.BI_FVI_FechaHoraInicioVisita__c = DateTime.now();
        }else if(estado == 'Finalizada') {
            evento.BI_FVI_FechaHoraFinVisita__c = DateTime.now();
        }else if(estado == 'LLamaCualificar') {
            evento.BI_FVI_Estado__c='Finalizada';
            estadoRet='QA';
        }else if(estado == 'Cualificada') {
            system.debug('cualificada');
            evento.BI_FVI_Estado__c='Cualificada';
            estadoRet='QA';
        }else {
            estadoRet='OK';
        }
        System.debug('The event==> ' + evento);
        try{
            upsert evento;
            //JEG To fix assignation to Calendar
            if(owner != null){
              evento.OwnerID = owner;  //JEG 05/03/2018
              update evento; 
            }

        }catch(Exception ex){
             system.debug('error-->'+ex);
             estadoRet='Error';
        }
        return estadoRet;
    }            
    /*FIN E001*/
    
    @AuraEnabled
    public static Opportunity getEvent(){
        Opportunity e = null;
        List<Opportunity> listOpp = [select id, name from Opportunity];
        if(!listOpp.isEmpty()){
            e = listOpp.get(0);
        }
        return e;
    }
    
    @AuraEnabled
    public static string getContactData(String acctId){
        System.debug('getContactData-->'+acctId);
        String jsonResul;
        
        try{
            List<PickListItem> resul = new List<PickListItem>();
            PickListItem pil;
            
            list <contact> cList=[SELECT Id, Name, email, phone FROM Contact WHERE AccountId =: acctId ORDER BY createddate];
            
            if (!clist.isEmpty()){
                
                for(contact c : cList){
                    pil = new PickListItem();
                    pil.value=String.valueOf(c.get('Id'));
                    pil.label=string.valueof(c.get('Name'));
                    pil.email=string.valueof(c.get('email'));
                    pil.phone=string.valueof(c.get('phone'));
                    pil.phone=string.valueof(c.get('phone'));
                    pil.tipo='Contacto';
                    resul.add(pil);
                }
            }
            else{
                pil = new PickListItem();
                pil.value='Este cliente no tiene contactos';
                pil.label='Este cliente no tiene contactos';
                pil.email='-';
                pil.tipo='Contacto';
                resul.add(pil);
            }
            resul.addAll(getOpportunityData(acctId)); 
            jsonResul = JSON.serialize(resul);        
            
        }
        
        catch (Exception e){
            
        }
        
        return jsonResul;
    }
    
    @AuraEnabled
    public static List<PickListItem> getOpportunityData(String acctId){
        String jsonResul;
        List<PickListItem> resul = new List<PickListItem>();
        try{
            PickListItem pil;
            list <Opportunity> oList=[SELECT Id, Name FROM Opportunity WHERE AccountId =: acctId ORDER BY createddate];
            
            if (!olist.isEmpty()){
                //M001 - Start
                pil = new PickListItem();
                pil.value='aaa';
                pil.label='Sin oportunidad';
                pil.tipo='Oportunidad';
                resul.add(pil);
                //M001 - End
                for(Opportunity o : oList){
                    pil = new PickListItem();
                    pil.value=String.valueOf(o.get('Id'));
                    pil.label=string.valueof(o.get('Name'));
                    pil.tipo='Oportunidad';
                    
                    resul.add(pil);
                }
            }
            else{
                pil = new PickListItem();
                pil.value=null;
                pil.label='Este cliente no tiene oportunidades';
                pil.tipo='Oportunidad';
                
                resul.add(pil);
            }
            // jsonResul = JSON.serialize(resul);        
            
        }
        
        catch (Exception e){
            
        }
        return resul;
        
    }
    
    
    public class PickListItem {
        
        public String label {get; set;}
        public String value {get; set;} 
        public String email {get; set;} 
        public String phone {get; set;}
        Public String OpName {get; set;}
        Public String OpId {get; set;}
        Public String tipo {get; set;}
        
        
    }
    
    @AuraEnabled
    public static String datosCuenta(String acctId, String campaignId){
        
        list <contact> cList=[SELECT Id, Name, email, phone, AccountId FROM Contact WHERE AccountId =: acctId ORDER BY createddate limit 1];
        
        List<PickListItem> resul = new List<PickListItem>();
        PickListItem pil;
        
        
        List<Opportunity> op = [SELECT Name, Id From Opportunity Where AccountId=: acctId Limit 1];
        
        String opId = null;
        String opName = null;
        if(!op.isEmpty()){
            opName=op[0].Name;
            opId=op[0].Id; 
        }
        
        
        if (!clist.isEmpty()){
            
            for(contact c : cList){
                pil = new PickListItem();
                pil.value=String.valueOf(c.get('Id'));
                pil.label=string.valueof(c.get('Name'));
                pil.email=string.valueof(c.get('email'));
                pil.phone=string.valueof(c.get('phone'));
                pil.OpName=opName;
                pil.OpId=opId;
                resul.add(pil);
            }
        }
        else{
            pil = new PickListItem();
            pil.value='Este cliente no tiene contactos';
            pil.label='Este cliente no tiene contactos';
            pil.email='-';
            pil.phone='-';
            pil.OpName=opName;
            pil.OpId=opId;
            resul.add(pil);
        }
        
        
        return JSON.serialize(resul); 
        
    }
    @AuraEnabled
    public static String datosContacto(String contactId){        
        list <contact> cList=[SELECT Id, Name, email, phone, AccountId FROM Contact WHERE Id =: contactId  ORDER BY createddate limit 1];
        List<PickListItem> resul = new List<PickListItem>();
        PickListItem pil;
        List<Opportunity> op = new List<Opportunity>();
        if (!clist.isEmpty()){
            op = [SELECT Name, Id From Opportunity Where AccountId=: cList[0].AccountId Limit 1];
        }
        
        String opId = null;
        String opName = null;
        if(!op.isEmpty()){
            if(!Test.isRunningTest()){
                opName=op[0].Name;
                opId=op[0].Id;
            }
            
        }
        
        if (!clist.isEmpty()){
            
            for(contact c : cList){
                pil = new PickListItem();
                pil.value=String.valueOf(c.get('Id'));
                pil.label=string.valueof(c.get('Name'));
                pil.email=string.valueof(c.get('email'));
                pil.phone=string.valueof(c.get('phone'));
                pil.OpName=opName;
                pil.OpId=opId;
                resul.add(pil);
            }
        }
        else{
            pil = new PickListItem();
            pil.value='0';
            pil.label='Este cliente no tiene contactos';
            pil.email='-';
            pil.phone='-';
            pil.OpName=opName;
            pil.OpId=opId;
            resul.add(pil);
        }
        
        
        return JSON.serialize(resul); 
        
    }
    
    
    @AuraEnabled
    public static String deleteVisita(String eventId){
        Event e = [select id from Event where id = :eventId];
        delete e;
        
        return 'OK';
    }
    
    @AuraEnabled
    public static String getEventData(String eventId){
        /*Modificado*/
        List<Event> listaEventos = [select Id,BI_Correo_electronico_enviado__c, BI_ECU_Gestion__c, BI_FVI_Estado__c, whoid, Location, ActivityDateTime, DurationInMinutes, Subject, Description, ReminderDateTime, IsReminderSet,whatid,BI_FVI_FechaHoraInicioVisita__c,BI_FVI_FechaHoraFinVisita__c,BI_G4C_ResultadoVisita__c,BI_G4C_Oportunidad__c, BI_FVI_Resultado__c, BI_G4C_Tipo_de_Visita__c,Owner.Name,BI_Identificador_Interno__c from event where id = :eventId];
        Event e;
        list<Contact> c;
        Opportunity o = new Opportunity();
        
        if(!listaEventos.isEmpty()){
            e = listaEventos[0];
            //TimeZone tz = UserInfo.getTimeZone(); //JRM 16/04/2018
            //Integer diferencia = tz.getOffset(Datetime.now())/1000/60/60;
            //e.ActivityDateTime = e.ActivityDateTime.addHours(diferencia);
            c = [select id, name, email, phone,AccountId from contact where id =: e.whoid limit 1];
            
            list<Opportunity> Opos = [select id, name from Opportunity where id =: e.BI_G4C_Oportunidad__c];
            if(!Opos.isEmpty()){
                o= Opos[0];
            }
        }       
        BI_G4C_Event_Helper eh = new BI_G4C_Event_Helper();
        eh.e=e;
        eh.c=c;
        eh.o=o;
        return JSON.serialize(eh);
    }        
    @AuraEnabled
    public static User getCurrentUser() {
        System.debug('GET CURRENT USER APEX');
        String userId = UserInfo.getUserId();
        System.debug(userId);
        String query =  'SELECT Id, Name FROM User WHERE Id =: userId';
        System.debug(query);
        User oUser = (User) Database.query(query)[0];
        System.debug(oUser);
        return oUser;
    }
    @AuraEnabled
    public static void updatesend(String eid){
        System.debug('updatesendEMAIL-->'+eid);
        Event e=[SELECT  id,BI_Correo_electronico_enviado__c From Event where id=:eid];
        e.BI_Correo_electronico_enviado__c=true;
        update e;
         System.debug('updatesendEMAIL-->'+e.BI_Correo_electronico_enviado__c);
    }
    @AuraEnabled
    public static void sendPdf(ID visitaID,String cid){
        System.debug('sendPdf   visitaID-->'+visitaID+' mail-->'+cid);
        Contact contact=[SELECT Id, Name, email, phone, AccountId FROM Contact WHERE Id =: cid limit 1];
        System.debug('email-->'+contact.email);
        
        PageReference pdf = Page.BI_MinutaReunion_PDF;
        // add parent id to the parameters for standardcontroller
        pdf.getParameters().put('id',visitaID);
        // the contents of the attachment from the pdf
        Blob body;
    
        try {
          // returns the output of the page as a PDF
          body = pdf.getContent();
        } catch (VisualforceException e) {
          body = Blob.valueOf('Some Text');
        }
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        attach.setContentType('application/pdf');
        attach.setFileName('datosVisita.pdf');
        attach.setInline(false);
        attach.Body = body;
    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        mail.setToAddresses(new String[] { contact.Email });
        mail.setSubject('PDF Datos Visita');
        mail.setHtmlBody('Aqui esta el email con la informacion de la visita.');
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 

        // Send the email
        if(!Test.isRunningTest()){
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
      }
    }
    public PageReference getRedirectEsVisita() {
        //Event currentEvent = (Event)stdController.getRecord();

        if (this.evt.BI_G4C_esVisita__c) {
            return null;
        } else {
            PageReference pageRef = new ApexPages.StandardController(this.evt).edit();
            pageRef.setRedirect(true);
            return pageRef;
        }
    }
    public Id accountId {
        get{
            System.debug('AccountId --> ' + this.evt.AccountId);
            
          return this.evt.AccountId;
        }
        set{}
    }
    public Event oEvent {
        get{
            System.debug('Evento --> ' + this.evt);
            return this.evt;
        }
        set{}
    }
    public String accountName {
        get{
            if(!this.acc.isEmpty()){
              System.debug('AccountName --> ' + this.acc[0].Name);
              return this.acc[0].Name;
           }
            else{return null;}
        }
        set{}
    }
    public String accountCountry {
        get{
            if(!this.acc.isEmpty()){
                System.debug('AccountCountry' + this.acc[0].BI_Country__c);
                return this.acc[0].BI_Country__c;
           }
            else{return null;}
        }
        set{}
    }
    public String eventLocation {
        get{
            System.debug('EventLocation' + this.evt.Location);
            return this.evt.Location;
        }
        set{}
    }
    
    
    
    
    
    private static Map<String,String>localeToDateTimeFmtMap;             
//  -------------------------------------------------------
//  getUserDateFormat: e.g. M/d/yyyy
//  -------------------------------------------------------
public static String getUserDateFormat() {
    String      userLocale  = UserInfo.getLocale();
    getLocaleToDateTimeFmtMap();
    if (!localeToDateTimeFmtMap.containsKey(userLocale))    
    return 'yyyy-mm-dd';
    
    return localeToDateTimeFmtMap.get(userLocale).substringBefore(' ');
}

//  -------------------------------------------------------
//  getUserDateTimeFormat: e.g. M/d/yyyy hh.mm.ss
//  -------------------------------------------------------
public static String getUserDateTimeFormat() {
    String      userLocale  = UserInfo.getLocale();
    getLocaleToDateTimeFmtMap();
    if (!localeToDateTimeFmtMap.containsKey(userLocale))    
        return 'yyyy-mm-dd hh.mm.ss';
    
    return localeToDateTimeFmtMap.get(userLocale);
}

//  -------------------------------------------------------
//  getLocaleToDateTimeFmtMap                   : Returns a map of user locale | datetime format for that locale (http://www.interactiveties.com/b_locale_datetime.php)
//  -------------------------------------------------------
public static Map<String,String> getLocaleToDateTimeFmtMap () {
    if (localeToDateTimeFmtMap == null)
        localeToDateTimeFmtMap  = new Map<String,String> {
            'ar'            => 'dd/MM/yyyy hh:mm a',
            'ar_AE'         => 'dd/MM/yyyy hh:mm a',
            'ar_BH'         => 'dd/MM/yyyy hh:mm a',
            'ar_JO'         => 'dd/MM/yyyy hh:mm a',
            'ar_KW'         => 'dd/MM/yyyy hh:mm a',
            'ar_LB'         => 'dd/MM/yyyy hh:mm a',
            'ar_SA'         => 'dd/MM/yyyy hh:mm a',
            'bg_BG'         => 'yyyy-M-d H:mm',
            'ca'            => 'dd/MM/yyyy HH:mm',
            'ca_ES'         => 'dd/MM/yyyy HH:mm',
            'ca_ES_EURO'    => 'dd/MM/yyyy HH:mm',
            'cs'            => 'd.M.yyyy H:mm',
            'cs_CZ'         => 'd.M.yyyy H:mm',
            'da'            => 'dd-MM-yyyy HH:mm',
            'da_DK'         => 'dd-MM-yyyy HH:mm',
            'de'            => 'dd.MM.yyyy HH:mm',
            'de_AT'         => 'dd.MM.yyyy HH:mm',
            'de_AT_EURO'    => 'dd.MM.yyyy HH:mm',
            'de_CH'         => 'dd.MM.yyyy HH:mm',
            'de_DE'         => 'dd.MM.yyyy HH:mm',
            'de_DE_EURO'    => 'dd.MM.yyyy HH:mm',
            'de_LU'         => 'dd.MM.yyyy HH:mm',
            'de_LU_EURO'    => 'dd.MM.yyyy HH:mm',
            'el_GR'         => 'd/M/yyyy h:mm a',
            'en_AU'         => 'd/MM/yyyy HH:mm',
            'en_B'          => 'M/d/yyyy h:mm a',
            'en_BM'         => 'M/d/yyyy h:mm a',
            'en_CA'         => 'dd/MM/yyyy h:mm a',
            'en_GB'         => 'dd/MM/yyyy HH:mm',
            'en_GH'         => 'M/d/yyyy h:mm a',
            'en_ID'         => 'M/d/yyyy h:mm a',
            'en_IE'         => 'dd/MM/yyyy HH:mm',
            'en_IE_EURO'    => 'dd/MM/yyyy HH:mm',
            'en_NZ'         => 'd/MM/yyyy HH:mm',
            'en_SG'         => 'M/d/yyyy h:mm a',
            'en_US'         => 'M/d/yyyy h:mm a',
            'en_ZA'         => 'yyyy/MM/dd hh:mm a',
            'es'            => 'dd/MM/yyyy HH:mm',
            'es_AR'         => 'dd/MM/yyyy HH:mm',
            'es_BO'         => 'dd-MM-yyyy hh:mm a',
            'es_CL'         => 'dd-MM-yyyy hh:mm a',
            'es_CO'         => 'd/MM/yyyy hh:mm a',
            'es_CR'         => 'dd/MM/yyyy hh:mm a',
            'es_EC'         => 'dd/MM/yyyy hh:mm a',
            'es_ES'         => 'd/MM/yyyy H:mm',
            'es_ES_EURO'    => 'd/MM/yyyy H:mm',
            'es_GT'         => 'd/MM/yyyy hh:mm a',
            'es_HN'         => 'MM-dd-yyyy hh:mm a',
            'es_MX'         => 'd/MM/yyyy hh:mm a',
            'es_PE'         => 'dd/MM/yyyy hh:mm a',
            'es_PR'         => 'MM-dd-yyyy hh:mm a',
            'es_PY'         => 'dd/MM/yyyy hh:mm a',
            'es_SV'         => 'MM-dd-yyyy hh:mm a',
            'es_UY'         => 'dd/MM/yyyy hh:mm a',
            'es_VE'         => 'dd/MM/yyyy hh:mm a',
            'et_EE'         => 'd.MM.yyyy H:mm',
            'fi'            => 'd.M.yyyy H:mm',
            'fi_FI'         => 'd.M.yyyy H:mm',
            'fi_FI_EURO'    => 'd.M.yyyy H:mm',
            'fr'            => 'dd/MM/yyyy HH:mm',
            'fr_BE'         => 'd/MM/yyyy H:mm',
            'fr_CA'         => 'yyyy-MM-dd HH:mm',
            'fr_CH'         => 'dd.MM.yyyy HH:mm',
            'fr_FR'         => 'dd/MM/yyyy HH:mm',
            'fr_FR_EURO'    => 'dd/MM/yyyy HH:mm',
            'fr_LU'         => 'dd/MM/yyyy HH:mm',
            'fr_MC'         => 'dd/MM/yyyy HH:mm',
            'hr_HR'         => 'yyyy.MM.dd HH:mm',
            'hu'            => 'yyyy.MM.dd. H:mm',
            'hy_AM'         => 'M/d/yyyy h:mm a',
            'is_IS'         => 'd.M.yyyy HH:mm',
            'it'            => 'dd/MM/yyyy H.mm',
            'it_CH'         => 'dd.MM.yyyy HH:mm',
            'it_IT'         => 'dd/MM/yyyy H.mm',
            'iw'            => 'HH:mm dd/MM/yyyy',
            'iw_IL'         => 'HH:mm dd/MM/yyyy',
            'ja'            => 'yyyy/MM/dd H:mm',
            'ja_JP'         => 'yyyy/MM/dd H:mm',
            'kk_KZ'         => 'M/d/yyyy h:mm a',
            'km_KH'         => 'M/d/yyyy h:mm a',
            'ko'            => 'yyyy. M. d a h:mm',
            'ko_KR'         => 'yyyy. M. d a h:mm',
            'lt_LT'         => 'yyyy.M.d HH.mm',
            'lv_LV'         => 'yyyy.d.M HH:mm',
            'ms_MY'         => 'dd/MM/yyyy h:mm a',
            'nl'            => 'd-M-yyyy H:mm',
            'nl_BE'         => 'd/MM/yyyy H:mm',
            'nl_NL'         => 'd-M-yyyy H:mm',
            'nl_SR'         => 'd-M-yyyy H:mm',
            'no'            => 'dd.MM.yyyy HH:mm',
            'no_NO'         => 'dd.MM.yyyy HH:mm',
            'pl'            => 'yyyy-MM-dd HH:mm',
            'pt'            => 'dd-MM-yyyy H:mm',
            'pt_AO'         => 'dd-MM-yyyy H:mm',
            'pt_BR'         => 'dd/MM/yyyy HH:mm',
            'pt_PT'         => 'dd-MM-yyyy H:mm',
            'ro_RO'         => 'dd.MM.yyyy HH:mm',
            'ru'            => 'dd.MM.yyyy H:mm',
            'sk_SK'         => 'd.M.yyyy H:mm',
            'sl_SI'         => 'd.M.y H:mm',
            'sv'            => 'yyyy-MM-dd HH:mm',
            'sv_SE'         => 'yyyy-MM-dd HH:mm',
            'th'            => 'M/d/yyyy h:mm a',
            'th_TH'         => 'd/M/yyyy, H:mm ?.',
            'tr'            => 'dd.MM.yyyy HH:mm',
            'ur_PK'         => 'M/d/yyyy h:mm a',
            'vi_VN'         => 'HH:mm dd/MM/yyyy',
            'zh'            => 'yyyy-M-d ah:mm',
            'zh_CN'         => 'yyyy-M-d ah:mm',
            'zh_HK'         => 'yyyy-M-d ah:mm',
            'zh_TW'         => 'yyyy/M/d a h:mm'
        };
        return localeToDateTimeFmtMap;
}
}