/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Oscar Ena
Company:       Accenture - New Energy Aborda
Description:   TEST Class for TGS_Ebounding_WS coverage

History:

<Date>            <Author>          <Description>
30/08/2017        Oscar Ena        Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class TGS_Ebounding_TEST {

	private static final String RT_ASSET = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, Constants.RECORD_TYPE_ASSET);
	private static final String RTYPE_ID_ORDER_MANAGEMENT_CASE = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, Constants.CASE_RTYPE_DEVNAME_ORDER_MNGMNT);
	private static final String RT_ACCOUNT_HOLDING = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Oscar Ena
    Company:       Accenture - New Energy Aborda
    Description:   

    IN:            void
    OUT:           void
    
    History:

    <Date>            <Author>          <Description>
    30/08/2017        Oscar Ena         Initial version
    30/09/2017        Guillermo Muñoz	Add userTGS, throw_exception=false, skipAllTriggers()/cleanSkippedTriggers()
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void createRegistrationOrder_TEST() {
		
		TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == true || userTGS.TGS_Is_TGS__c == false){

            userTGS.TGS_Is_BI_EN__c = false;
            userTGS.TGS_Is_TGS__c = true;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }

		BI_Testutils.throw_exception = false;

		BI_MigrationHelper.skipAllTriggers();

		Account oAccount = new Account();

		oAccount.Name = 'ALLIANZ';
		oAccount.RecordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);

		insert oAccount;

		TGS_Ebounding_WS.TicketRequestType wOrder = new TGS_Ebounding_WS.TicketRequestType();

		wOrder.correlationId = '123456789';
    	wOrder.description = 'TEST ORDER';
    	wOrder.severity = 'DUMMY';
    	wOrder.type = Constants.TYPE_NEW;
    	wOrder.parentTicket = '976';

		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();

		restReq.requestURI = '/tickets/';
		restReq.requestBody = Blob.valueOf(JSON.serialize(wOrder));
		RestContext.request = restReq;

		BI_MigrationHelper.cleanSkippedTriggers();

		TGS_Ebounding_WS.createOrder();
		SYSTEM.debug('TGS_Ebounding_TEST.test_createRegistrationOrder :: response ' +RestContext.response.responseBody.tostring());
		TGS_Ebounding_WS.GenericResponse oGenericResponse = (TGS_Ebounding_WS.GenericResponse)Json.deserialize(RestContext.response.responseBody.tostring(), TGS_Ebounding_WS.GenericResponse.Class);
		
		System.assertEquals(201, RestContext.response.statusCode);
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture - New Energy Aborda
    Description:   

    IN:            void
    OUT:           void
    
    History:

    <Date>            <Author>          <Description>
    30/09/2017        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest (SeeAllData = true) static void createModificationOrTerminationOrder_Termination_TEST(){

		BI_Testutils.throw_exception = false;

		BI_MigrationHelper.skipAllTriggers();

		TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == true || userTGS.TGS_Is_TGS__c == false){

            userTGS.TGS_Is_BI_EN__c = false;
            userTGS.TGS_Is_TGS__c = true;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }

        User portUsr;

        System.runAs(new User(Id = UserInfo.getUserId())){
        	portUsr = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        }

        System.runAs(portUsr){
            NE__OrderItem__c tr_CI = TGS_Dummy_Test_Data.dummyConfiguration(UserInfo.getUserId(),null);
            Case newCase = [SELECT Id, AccountId, Order__c, Order__r.NE__Asset__c, RecordTypeId, Type FROM Case WHERE Order__c IN (SELECT NE__OrderId__c FROM NE__OrderItem__c WHERE ID = :tr_CI.Id)];
            NE__Order__c ord = newCase.Order__r;
            ord.NE__Order_Items__r.add(tr_CI);
            newCase.Order__c = tr_CI.NE__OrderId__c;
            newCase.Asset__c = tr_CI.NE__OrderId__c;
            newCase.TGS_Casilla_Desarrollo__c = true;          
            newCase.Status = 'Closed';
            newCase.TGS_Agrupador__c = 'TestAgrupador';
            newCase.TGS_Service__c = 'dummy';
            update newCase;

            NE__Order__c order = new NE__Order__c(Id = tr_CI.NE__OrderId__c, NE__OrderStatus__c = 'Active', NE__Configuration_Type__c = 'New');
            update order;

            TGS_Ebounding_WS.TicketRequestType wOrder = new TGS_Ebounding_WS.TicketRequestType();

			wOrder.correlationId = 'testCorrelation';
    		wOrder.description = 'TEST ORDER';
    		wOrder.severity = 'DUMMY';
    		wOrder.type = 'Disconnect';
    		wOrder.parentTicket = 'TestAgrupador';
	
    		RestRequest restReq = new RestRequest();
			RestContext.response = new RestResponse();
			
	
			restReq.requestURI = '/tickets/';
			restReq.requestBody = Blob.valueOf(JSON.serialize(wOrder));
			RestContext.request = restReq;
			
			BI_MigrationHelper.cleanSkippedTriggers();

			Test.startTest();
			TGS_Ebounding_WS.createOrder();
			Test.stopTest();
			        
			System.assertEquals(201, RestContext.response.statusCode);
        }
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture - New Energy Aborda
    Description:   

    IN:            void
    OUT:           void
    
    History:

    <Date>            <Author>          <Description>
    30/09/2017        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest (SeeAllData = true) static void createModificationOrTerminationOrder_Modification_TEST(){

		BI_Testutils.throw_exception = false;

		BI_MigrationHelper.skipAllTriggers();
		
		TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == true || userTGS.TGS_Is_TGS__c == false){

            userTGS.TGS_Is_BI_EN__c = false;
            userTGS.TGS_Is_TGS__c = true;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }

        User portUsr;

        System.runAs(new User(Id = UserInfo.getUserId())){
        	portUsr = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        }

        System.runAs(portUsr){
            NE__OrderItem__c tr_CI = TGS_Dummy_Test_Data.dummyConfiguration(UserInfo.getUserId(),null);
            Case newCase = [SELECT Id, AccountId, Order__c, Order__r.NE__Asset__c, RecordTypeId, Type FROM Case WHERE Order__c IN (SELECT NE__OrderId__c FROM NE__OrderItem__c WHERE ID = :tr_CI.Id)];
            NE__Order__c ord = newCase.Order__r;
            ord.NE__Order_Items__r.add(tr_CI);

            NE__Order__c order = new NE__Order__c(Id = tr_CI.NE__OrderId__c, NE__OrderStatus__c = 'Active', NE__Configuration_Type__c = 'New', RecordtypeId = [SELECT Id, Name FROM RecordType WHERE Name = 'Asset' AND SObjectType = 'NE__Order__c' LIMIT 1].Id);
            update order;

            newCase.Order__c = ord.Id;
            newCase.Asset__c = ord.Id;
            newCase.TGS_Casilla_Desarrollo__c = true;           
            newCase.Status = 'Closed';
            newCase.TGS_Agrupador__c = 'TestAgrupador2';
            newCase.TGS_Service__c = 'dummy';
            update newCase;

            TGS_Ebounding_WS.TicketRequestType wOrder = new TGS_Ebounding_WS.TicketRequestType();

			wOrder.correlationId = 'testCorrelation';
    		wOrder.description = 'TEST ORDER';
    		wOrder.severity = 'DUMMY';
    		wOrder.type = 'Change';
    		wOrder.parentTicket = 'TestAgrupador2';
	
    		RestRequest restReq = new RestRequest();
			RestContext.response = new RestResponse();
			
	
			restReq.requestURI = '/tickets/';
			restReq.requestBody = Blob.valueOf(JSON.serialize(wOrder));
			RestContext.request = restReq;
			
			BI_MigrationHelper.cleanSkippedTriggers();

			Test.startTest();
			TGS_Ebounding_WS.createOrder();
			Test.stopTest();
			        
			System.assertEquals(201, RestContext.response.statusCode);
        }
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture - New Energy Aborda
    Description:   

    IN:            void
    OUT:           void
    
    History:

    <Date>            <Author>          <Description>
    30/09/2017        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void errors_TEST() {

		BI_Testutils.throw_exception = false;

		BI_MigrationHelper.skipAllTriggers();

		TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == true || userTGS.TGS_Is_TGS__c == false){

            userTGS.TGS_Is_BI_EN__c = false;
            userTGS.TGS_Is_TGS__c = true;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }

		TGS_Ebounding_WS.TicketRequestType wOrder = new TGS_Ebounding_WS.TicketRequestType();

		wOrder.correlationId = '123456789';
    	wOrder.description = 'TEST ORDER';
    	wOrder.severity = 'DUMMY';
    	wOrder.type = null;
    	wOrder.parentTicket = 'Test';

		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();

		restReq.requestURI = '/tickets/';
		restReq.requestBody = Blob.valueOf(JSON.serialize(wOrder));
		RestContext.request = restReq;

		/////////////////////////Registro

		//Sin type		
		TGS_Ebounding_WS.createOrder();
		
		System.assertEquals(400, RestContext.response.statusCode);

		//Sin Cuenta
		wOrder.type = 'New';
		restReq.requestBody = Blob.valueOf(JSON.serialize(wOrder));
		TGS_Ebounding_WS.createOrder();

		System.assertEquals(404, RestContext.response.statusCode);

		//Agrupador que ya existe
		BI_MigrationHelper.skipAllTriggers();
		Account oAccount = new Account();

		oAccount.Name = 'Test';
		oAccount.RecordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);

		insert oAccount;

		TGS_Ebounding_Helper.createCase(oAccount.Id, wOrder);

		TGS_Ebounding_WS.createOrder();

		System.assertEquals(400, RestContext.response.statusCode);

		/////////////////////////Modificación - Terminación
		wOrder.type = 'Change';

        //EboundingId repetido
        wOrder.correlationId = '123456789';
        restReq.requestBody = Blob.valueOf(JSON.serialize(wOrder));
        TGS_Ebounding_WS.createOrder();

        System.assertEquals(400, RestContext.response.statusCode);

        //Orden con el mismo agrupador no cerrada o cancelada
        wOrder.correlationId = '987654321';
        wOrder.parentTicket = 'Test';
        restReq.requestBody = Blob.valueOf(JSON.serialize(wOrder));
        TGS_Ebounding_WS.createOrder();

        System.assertEquals(400, RestContext.response.statusCode);

		//Agrupador que no existe
        wOrder.correlationId = '987654321';
		wOrder.parentTicket = 'Error';
		restReq.requestBody = Blob.valueOf(JSON.serialize(wOrder));
		TGS_Ebounding_WS.createOrder();

		System.assertEquals(404, RestContext.response.statusCode);

		//Orden no se crea
		/*wOrder.parentTicket = 'Test';
		restReq.requestBody = Blob.valueOf(JSON.serialize(wOrder));
		TGS_Ebounding_WS.createOrder();

		System.assertEquals(500, RestContext.response.statusCode);*/

		//Error 500
		BI_Testutils.throw_exception = true;

		TGS_Ebounding_WS.createOrder();

		System.assertEquals(500, RestContext.response.statusCode);
	}

	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture - New Energy Aborda
    Description:   

    IN:            void
    OUT:           void
    
    History:

    <Date>            <Author>          <Description>
    30/09/2017        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	/*@isTest static void sendEboundingNotification_TEST(){

		TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == true || userTGS.TGS_Is_TGS__c == false){

            userTGS.TGS_Is_BI_EN__c = false;
            userTGS.TGS_Is_TGS__c = true;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }


		BI_Testutils.throw_exception = false;

		BI_MigrationHelper.skipAllTriggers();

		Account oAccount = new Account();

		oAccount.Name = 'ALLIANZ';
		oAccount.RecordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);

		insert oAccount;

		TGS_Ebounding_WS.TicketRequestType wOrder = new TGS_Ebounding_WS.TicketRequestType();

		wOrder.correlationId = '123456789';
    	wOrder.description = 'TEST ORDER';
    	wOrder.severity = 'DUMMY';
    	wOrder.type = Constants.TYPE_NEW;
    	wOrder.parentTicket = '976';

		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();

		restReq.requestURI = '/tickets/';
		restReq.requestBody = Blob.valueOf(JSON.serialize(wOrder));
		RestContext.request = restReq;

		TGS_Ebounding_WS.createOrder();

		Case cas = [SELECT Id FROM Case WHERE BI_Id_SistemaLegado__c = '123456789' LIMIT 1];

		System.assertNotEquals(cas, null);

		BI_MigrationHelper.cleanSkippedTriggers();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

    	Test.startTest();
	
    	Test.setMock(HttpCalloutMock.class, new BI_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));
		cas.Status = 'Closed';
		cas.TGS_Casilla_Desarrollo__c = true;
		update cas;

		Test.stopTest();
	}*/
}