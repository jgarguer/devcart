public with sharing class TGS_SLA_Assigner_Handler {

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Miguel Angel Galindo, Carlos Campillo
	Company:       Deloitte
	Description:   Assign the sla according of the account and the service.
				   Class invoked by the trigger Bi_Case.
	
	History

	<Date>            	<Author>                  		<Description>
	18/02/2015      	Miguel Angel Galindo			Initial version
	09/06/2015			Miguel Angel Galindo			incident solved
	19/02/2016			Fernando Arteaga				Optimization of code to decrease cpu time spent
	19/10/2016          Jose Miguel Fierro              Optimization of code to decrease queries, queried rows and cpu spent
	5/11/2017		    Julio Asenjo García				Cambio para asignar los entitlement process a los SLAS pro zonas
	23/01/2018			Iván Ríos Colilla				Modificacion if para asignar los entitlement.
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	

	// JMF 19/10/2016 - SLA to Account assignments are not going to change in mid-transaction
	public static Map<String, Id> map_account2SLA = new Map<String, Id>();
	
	public static void assignerHandlerSLA(List<Case> listCase, Map<Id,Case> mapOldCase){
		/*if(slas.isEmpty()==false){
			for (Case c : listCase){
				for (TGS_SLA_Assigner__c s : slas){
					if(c.AccountId == s.TGS_Account__c && c.TGS_Service__c == s.TGS_Commercial_Product__r.Name){
						c.EntitlementId = s.TGS_Entitlement__c;
						break; // FAR 19/02/2016 - Break when entitlement is assigned
					}
				}
			}
		}*/
		/*loadSLAs();
		for(Case cas : listCase) {
			if(map_account2SLA.containsKey(cas.AccountId + cas.TGS_Service__c)) {
				cas.EntitlementId = map_account2SLA.get(cas.AccountId + cas.TGS_Service__c);
			}
		}*/
         System.debug('\n\n--> listCase'+listCase+ '\n');
        System.debug('\n\n--> mapOldCase'+mapOldCase+ '\n');
		Set<String> set_sla_services = new Set<String>();
		Set<Case> set_Case2Proc = new Set<Case>();
		Map<id, String> mapPreSlas = new Map<id, String>();
		Map<String, Entitlement> myMapEntitl = new Map<String, Entitlement>();        
		
		for(Case cas : listCase) {
             
			if((mapOldCase == null && cas.AccountId != null && String.isNotBlank(cas.TGS_Service__c)) ||
				//Ever01(mapOldCase != null && (cas.AccountId != mapOldCase.get(cas.Id).AccountId || cas.TGS_Service__c != mapOldCase.get(cas.Id).TGS_Service__c))|| mapOldCase != null && cas.EntitlementId == null && String.isNotBlank(cas.TGS_Service__c)) {
				//Ever01-INI
                (mapOldCase != null && (cas.AccountId != mapOldCase.get(cas.Id).AccountId || cas.TGS_Service__c != mapOldCase.get(cas.Id).TGS_Service__c))|| mapOldCase != null && /*cas.EntitlementId == null &&*/ String.isNotBlank(cas.TGS_Service__c)) {
               //Ever01-FIN
                        set_Case2Proc.add(cas);
				set_sla_services.add(cas.AccountId + ' - ' + cas.TGS_Service__c);
				if(cas.TGS_Service__c=='DDI Standard'|| cas.TGS_Service__c=='DDI Resale'){                  
                   if(!String.isEmpty(cas.TGS_DDI_Zone__c) && cas.TGS_DDI_Zone__c!='ZonaErronea') mapPreSlas.put(cas.Id, 'TGS_SLAs_DDI_SN_'+Cas.TGS_DDI_Zone__c);
                    
				}else if(cas.TGS_Service__c=='Special Number' && cas.TGS_SN_Zone__c!='ZonaErronea'){             
                    if(!String.isEmpty(cas.TGS_SN_Zone__c)) mapPreSlas.put(cas.Id, 'TGS_SLAs_DDI_SN_'+cas.TGS_SN_Zone__c);
                     
				}
			}
		}

		if(!set_sla_services.isEmpty()) {
			if(!mapPreSlas.isEmpty()){
				for(Entitlement e : [SELECT Id,Name FROM Entitlement WHERE Name LIKE '%TGS_SLAs_DDI_SN_%'])
					myMapEntitl.put(e.Name, e);
				}

            
			for(TGS_SLA_Assigner__c sla : [SELECT TGS_Account__c, TGS_Commercial_Product__r.Name, TGS_Entitlement__c FROM TGS_SLA_Assigner__c  WHERE TGS_Account_Service__c IN :set_sla_services]) {
				map_account2SLA.put(sla.TGS_Account__c + ' - ' + sla.TGS_Commercial_Product__r.Name, sla.TGS_Entitlement__c);
			}
            
            System.debug('\n\n--> set_Case2Proc'+set_Case2Proc+'\n');
            System.debug('\n\n--> mapPreSlas'+mapPreSlas+'\n');
            System.debug('\n\n--> myMapEntitl'+myMapEntitl+'\n');
            
			for(Case cas : set_Case2Proc) {
				if (!mapPreSlas.isEmpty() && mapPreSlas.containsKey(cas.Id))
					cas.EntitlementId=myMapEntitl.get(mapPreSlas.get(cas.Id)).Id;
				else
					cas.EntitlementId = map_account2SLA.get(cas.AccountId + ' - ' + cas.TGS_Service__c);
			}
		}
	}
	public static void loadSLAs() {
		if(map_account2SLA.isEmpty()) {
			for(TGS_SLA_Assigner__c sla : [SELECT TGS_Account__c, TGS_Commercial_Product__r.Name, TGS_Entitlement__c FROM TGS_SLA_Assigner__c  WHERE TGS_Account__c != null]) {
				map_account2SLA.put(sla.TGS_Account__c + sla.TGS_Commercial_Product__r.Name, sla.TGS_Entitlement__c);
			}
		}
	}
	
	/*public static void assignerOwner(List<Case> listCase, list<AssignmentRule> AR){
		for(Case c: listCase){
			//Creating the DMLOptions for "Assign using active assignment rules" checkbox
			Database.DMLOptions dmlOpts = new Database.DMLOptions();
			dmlOpts.assignmentRuleHeader.assignmentRuleId = AR[0].Id;
			//Setting the DMLOption on Case instance
			c.setOptions(dmlOpts);
		}
	}*/

}