global class BI2_ClosePastCases_Job implements Schedulable  {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Scheduled class that closes cases that have been in the resolved state for at least 5 days

	 History:

	 <Date>				<Author>				<Description>
	 10/09/2015			Jose Miguel Fierro		Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Method that executes the job, and closes cases

	 IN:			Schedulable context (Parameters to schedule the job)
	 OUT:			Void

	 History:

	 <Date>				<Author>				<Description>
	 10/09/2015			Jose Miguel Fierro		Initial version.
	 30/09/2015			Jose Miguel Fierro		Set TGS_Status_Reason__c to 'Automatically Closed' and extended grace period from 5 to 120 days
	 29/10/2015			Jose Miguel Fierro		Added try-catch
	 10/11/2015			Jose Miguel Fierro		Switched from hard-coded grace period to label (BI2_Antiguedad_Caso_Cierre_Automatico)
	 23/11/2015			Jose Miguel Fierro 		Dissallow closing cases with unfilfilled SLAs
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global void execute(SchedulableContext SC) {
		/*
		 AND (
		 	OR (ISPICKVAL([Case].Status, 'Resolved') , ISPICKVAL([Case].Status, 'Resuelto')),
		 	OR ([Case].RecordType.DeveloperName = 'BI2_caso_comercial' , [Case].RecordType.DeveloperName = 'BI2_Caso_Padre'),
		 	TODAY() - [Case].BI_Fecha_ltimo_cambio_de_estado__c >=5
		 )
		*/
        try {
            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');
            
            Date limite = Date.today().addDays(-(Integer.valueOf(Label.BI2_Antiguedad_Caso_Cierre_Automatico)));
            System.debug('limite: ' + limite);
            List<Case> lst_cases = [SELECT Id, BI_Fecha_ltimo_cambio_de_estado__c, TGS_SLA_Light__c, Status, MilestoneStatus, (SELECT Id FROM Cases WHERE Status NOT IN ('Closed', 'Cancelled', 'Cerrado', 'Cancelado')) FROM Case
            						WHERE (Status IN ('Resuelto','Resolved') AND RecordType.DeveloperName IN ('BI2_caso_comercial','BI2_Caso_Padre')) AND BI_Fecha_ltimo_cambio_de_estado__c <= :limite AND TGS_SLA_Light__c != 'Red'];
            
            System.debug('+++++lst_cases: ' + lst_cases);
            List<Case> lst_cases_a_procesar  = new List<Case>();
            //Date today = Date.today();
            for(Case cas : lst_cases) {
                //if(c.BI_Fecha_ltimo_cambio_de_estado__c.daysBetween(today) >= Integer.valueOf(Label.BI2_Antiguedad_Caso_Cierre_Automatico)) {
                if(cas.Cases.size() == 0){
                    lst_cases_a_procesar.add(cas);
                }
                //}
            }
            if(lst_cases_a_procesar.size() > 0) {
                System.debug('lst_cases_a_procesar had values');
                for(Case c : lst_cases_a_procesar) {
                    c.Status = 'Closed';
                    c.TGS_Status_reason__c = 'Automatically Closed';
                    c.BI2_PER_Motivo_inconformidad__c = 'Ninguno';
                    c.TGS_Resolution__c = 'Cierre Automático';
                    system.debug('XCCC:Id: ' + c.Id);
                }
                //System.debug('CurrentUser (in job): ' + UserInfo.getUserId());
                cleanupUpdateErrors(Database.update(lst_cases_a_procesar, false));
            }
        } catch (Exception exc) {
            BI_LogHelper.generate_BILog('BI2_ClosePastCases_Job.execute', 'BI_EN', exc, 'Apex Job');
        }
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	 Author:		Jose Miguel Fierro
	 Company:		Salesforce.com
	 Description:	Reports any error that happened while updating a SObject via Database.update

	 IN:			List<Database.SaveResult>
	 OUT:			Void

	 History:

	 <Date>				<Author>				<Description>
	 19/11/2015			Jose Miguel Fierro		Initial version.
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global static void cleanupUpdateErrors(List<Database.SaveResult> lstResults) {
		try {
			if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');
			
			String errMsg = '';
			for(Database.SaveResult res : lstResults) {
				if(!res.isSuccess()) {
					String msg = '\nERROR UPDATING: ' + res.getId();
					for(Database.Error err : res.getErrors()) {
						msg += '\n\tError: ' + err.getStatusCode();
						if(err.getFields().size() > 0) {
							msg +=   '\tIn Fields: ';
							for(String field : err.getFields()) {
								msg += field +',';
							}
							msg += '\n\tMessage: ' + err.getMessage();
						}
					}

					errMsg += msg;
				}
			}
			if(errMsg != '') {
				System.debug(LoggingLevel.ERROR, 'ErrMsg: ' + errMsg);
				BI_LogHelper.generate_BILog('BI2_ClosePastCases_Job.cleanupUpdateErrors', 'BI_EN', new BI_Exception(errMsg), 'Apex Job');
			}
		} catch (Exception exc) {
			BI_LogHelper.generate_BILog('BI2_ClosePastCases_Job.cleanupUpdateErrors', 'BI_EN', exc, 'Apex Job');
		}
	}
}