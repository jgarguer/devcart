@isTest(seeAllData = false)
public class BIIN_Listado_Ticket_TEST
{
/*----------------------------------------------------------------------------------------------------------------------
    Author:        José María Martín
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_Listado_Ticket_WS

    <Date>                  <Author>                        <Change Description>
    11/2015                 José María Martín               Initial Version
    07/04/2016              José Luis González              Adapt test to UNICA modifications
----------------------------------------------------------------------------------------------------------------------*/
    private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
    private final static String LE_NAME = 'TestAccount_Listado_Ticket_TEST';

    @testSetup static void dataSetup() 
    {
        BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
        Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert account;

        Account[] acc = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketTest = BIIN_UNICA_TestDataGenerator.createTicket(acc[0]);
        insert ticketTest;
    }

    @isTest static void test_BIIN_Listado_Ticket_TEST() 
    {
        String authorizationToken = 'CONSULTA_INCIDENCIA';
        
        BI_TestUtils.throw_exception = false;

        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        List<Case> ticketIncidenciaTecnica = [SELECT Id, BI_Id_del_caso_legado__c, AccountId FROM Case WHERE AccountId =: account.Id LIMIT 1];
        List<Case> ticketIncidenciaTecnicaNoIdLegado = [SELECT Id, AccountId FROM Case WHERE AccountId =: account.Id LIMIT 1];
		ticketIncidenciaTecnicaNoIdLegado.GET(0).BI_Id_del_caso_legado__c = '';
        
        Test.startTest(); 
            
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ListadoTickets'));
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(ticketIncidenciaTecnica.get(0));
        BIIN_Listado_Ticket_WS ceController = new BIIN_Listado_Ticket_WS();
        
        ceController.listadoTicket(authorizationToken, ticketIncidenciaTecnica.get(0), DateTime.newInstance(2015, 10, 10, 0, 0, 0), DateTime.newInstance(2015, 10, 11, 0, 0, 0));

        ApexPages.Standardcontroller controller1 = new ApexPages.Standardcontroller(ticketIncidenciaTecnicaNoIdLegado.get(0));
        BIIN_Listado_Ticket_WS ceController1 = new BIIN_Listado_Ticket_WS();
        
        ceController.listadoTicket(authorizationToken, ticketIncidenciaTecnicaNoIdLegado.get(0), DateTime.newInstance(2015, 10, 10, 0, 0, 0), DateTime.newInstance(2015, 10, 11, 0, 0, 0));

        Test.stopTest();
    }
}