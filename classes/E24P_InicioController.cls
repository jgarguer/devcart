public class E24P_InicioController {
    
    @AuraEnabled
    public static List < Object > doFetchAccPlan() {
        List < Object > returnList = new List < Object > ();
        
        returnList = [Select Id, Name, E24P_Cliente__c, E24P_Cliente__r.Name, E24P_Comercial__c, E24P_Jefe__c, E24P_Ing_Est_Ano_en_Curso_EAI__c, E24P_Priorizado__c
                      FROM E24P_Account_Plan__c];
        
        return returnList;
        
    }
    
        @AuraEnabled
    public static List < Object > doFetchAccPlanPriorizados() {
        List < Object > returnList = new List < Object > ();
        
        returnList = [Select Id, Name, E24P_Cliente__c, E24P_Cliente__r.Name, E24P_Comercial__c, E24P_Jefe__c, E24P_Ing_Est_Ano_en_Curso_EAI__c, E24P_Priorizado__c, LastModifiedDate
                      FROM E24P_Account_Plan__c WHERE E24P_Priorizado__c=true];
        
        return returnList;
        
    }
       		@AuraEnabled
    public static Boolean doUpdateAccPlan(List<E24P_Account_Plan__c> editedAccPlanList){
        try{
            update editedAccPlanList;
            return true;
        } catch(Exception e){
            return false;
        }
    }
}