@isTest
private class Milestone1_repeatCon_TEST {

	final static String ALL_ITEMS;
    final static String MY_TASKS;
    final static String MY_MILESTONES_ONLY;
    final static String MY_PROJECTS_AND_ITEMS;
    final static String MY_TASKS_ONLY;
    final static Date currentDay;

static{
    ALL_ITEMS = system.label.Milestone1_AllItems;
    MY_TASKS = system.label.Milestone1_MyTasksOnly;
    MY_MILESTONES_ONLY = system.label.Milestone1_MyMilestonesOnly;
    MY_PROJECTS_AND_ITEMS = system.label.Milestone1_MyProjectsAndSubProjectItems;
    MY_TASKS_ONLY = System.Label.Milestone1_MyTasksOnly;

 }

	
     @testSetup static void dataLoad() {
		Milestone1_Project__c testProject = Milestone1_Test_Utility.sampleProject('UNIT TEST PROJECT NAME ABC123XYZ UNIQUE' + System.now());
        testProject.Status__c = 'Active';
        testProject.Deadline__c = Date.today();
        insert testProject;

        Milestone1_Milestone__c testMilestone = Milestone1_Test_Utility.sampleMilestone(testProject.Id,null,'UNIT TEST MILESTONE NAME ACB123XYZ UNIQUE' + System.now());
        testMilestone.Deadline__c = Date.today();
        testMilestone.Complete__c = false;
        insert testMilestone;

        Milestone1_Task__c testTask = Milestone1_Test_Utility.sampleTask(testMilestone.Id);
        testTask.Complete__c = false;
        testTask.Start_Date__c = Date.today();
        testTask.Due_Date__c = Date.today();
        insert testTask;
     }



  @isTest static void testController()
  {
  		Milestone1_Project__c testProject = [Select id from Milestone1_Project__c];
  		Milestone1_Milestone__c testMilestone = [Select id from Milestone1_Milestone__c];
  		Milestone1_Task__c testTask = [Select id from Milestone1_Task__c]; 

        test.startTest();
        Milestone1_repeatCon controller = new Milestone1_repeatCon();
        controller.next();
        controller.prev();
        controller.selectedProject = testProject.id;
        List<Milestone1_Month.Week> weeksList = controller.getWeeks();
        test.stopTest();
        Map<ID,Milestone1_Project__c> projMap= controller.getSelectedProject();
        Map<ID,Milestone1_Milestone__c> milestonesMap=controller.getProjectSpecificMilestone();
        system.assertEquals(projMap.containsKey(testProject.id),true);
        system.assertEquals(milestonesMap.containsKey(testMilestone.id),true);
  }
  
  @isTest static void testControllerFilterTasks()
  {

  		Milestone1_Project__c testProject = [Select id from Milestone1_Project__c];
  		Milestone1_Milestone__c testMilestone = [Select id from Milestone1_Milestone__c];
  		Milestone1_Task__c testTask = [Select id from Milestone1_Task__c]; 

        test.startTest();
        Milestone1_repeatCon controller = new Milestone1_repeatCon();
        controller.next();
        controller.prev();
        controller.selectedProject = testProject.id;
        controller.filterItem = MY_TASKS_ONLY;//'My Tasks Only';
        controller.filter();
        List<Milestone1_Month.Week> weeksList = controller.getWeeks();
        test.stopTest();
        Map<ID,Milestone1_Project__c> projMap= controller.getSelectedProject();
        Map<ID,Milestone1_Milestone__c> milestonesMap=controller.getProjectSpecificMilestone();
        system.assertEquals(projMap.containsKey(testProject.id),true);
        system.assertEquals(milestonesMap.containsKey(testMilestone.id),true);
        List<Id> mIds=new List<Id>();
        mIds.add(testMilestone.id);
        List<Milestone1_Task__c> tasksList=[Select Id from Milestone1_Task__c where Assigned_To__c =:Userinfo.getUserId() and Project_Milestone__c in : mIds ];
        for(Milestone1_Task__c task:tasksList)
        {
            if(testTask.id==task.id)
            system.assertEquals(task.id,testTask.id);
        }
  }
  
  @isTest static void testControllerFilterMilestones()
  {

  		Milestone1_Project__c testProject = [Select id from Milestone1_Project__c];
  		Milestone1_Milestone__c testMilestone = [Select id from Milestone1_Milestone__c];
  		Milestone1_Task__c testTask = [Select id from Milestone1_Task__c]; 

        test.startTest();
        Milestone1_repeatCon controller = new Milestone1_repeatCon();
        controller.next();
        controller.prev();
        controller.selectedProject = testProject.id;
        controller.filterItem = MY_MILESTONES_ONLY;//'My Milestones Only';
        controller.filter();
        List<Milestone1_Month.Week> weeksList = controller.getWeeks();
        test.stopTest();
         Map<ID,Milestone1_Project__c> projMap= controller.getSelectedProject();
        Map<ID,Milestone1_Milestone__c> milestonesMap=controller.getProjectSpecificMilestone();
       //system.assertEquals(projMap.containsKey(testProject.id),true);
        system.assertEquals(milestonesMap.containsKey(testMilestone.id),true);
  }
  
  @isTest static void testControllerFilterMyProjects()
  {
  		Milestone1_Project__c testProject = [Select id from Milestone1_Project__c];
  		Milestone1_Milestone__c testMilestone = [Select id from Milestone1_Milestone__c];
  		Milestone1_Task__c testTask = [Select id from Milestone1_Task__c]; 

        /**
         *Modified Date:04-20-2012.
         *Owner:Persistent.
         *Comment: To add an additional project filter.
         *Bug #: 66.
        */ 
        test.startTest();
        Milestone1_repeatCon controller = new Milestone1_repeatCon();
        controller.next();
        controller.prev();
        controller.selectedProject = testProject.id;
        controller.filterItem = MY_PROJECTS_AND_ITEMS;//'My Projects And Sub-Project Items';
        controller.filter();
        List<Milestone1_Month.Week> weeksList = controller.getWeeks();
        test.stopTest();
        Map<ID,Milestone1_Project__c> projMap= controller.getSelectedProject();
        Map<ID,Milestone1_Milestone__c> milestonesMap=controller.getProjectSpecificMilestone();
        system.assertEquals(projMap.containsKey(testProject.id),true);
        system.assertEquals(milestonesMap.containsKey(testMilestone.id),true);
  }
  
   /**
         *Modified Date:04-20-2012.
         *Owner:Persistent.
         *Comment: To add an additional project filter.
         *Bug #: 66.
   */ 
    @isTest static void testControllerFilterAllItems()
    {

  		Milestone1_Project__c testProject = [Select id from Milestone1_Project__c];
  		Milestone1_Milestone__c testMilestone = [Select id from Milestone1_Milestone__c];
  		Milestone1_Task__c testTask = [Select id from Milestone1_Task__c]; 
        
        test.startTest();
        Milestone1_repeatCon controller = new Milestone1_repeatCon();
        controller.next();
        controller.prev();
        controller.selectedProject = testProject.id;
        controller.filterItem = ALL_ITEMS;//'All Items';
        controller.filter();
        List<Milestone1_Month.Week> weeksList = controller.getWeeks();
        test.stopTest();
        Map<ID,Milestone1_Project__c> projMap= controller.getSelectedProject();
        Map<ID,Milestone1_Milestone__c> milestonesMap=controller.getProjectSpecificMilestone();
      
    }
  
  @isTest static void testControllerFilterAllItemsWithBlankProject()
    {

        Milestone1_Project__c testProject1 = [Select id from Milestone1_Project__c];
        Milestone1_Project__c testProject = Milestone1_Test_Utility.sampleProject('Trial Project ABCXYZ' + System.now());
        testProject.Status__c = 'Active';
        testProject.Deadline__c = Date.today();
        system.debug(testProject.name);
        insert testProject;
        
        Milestone1_Milestone__c testMilestone = [Select id from Milestone1_Milestone__c];
        
        Milestone1_Milestone__c testMilestone1 = Milestone1_Test_Utility.sampleMilestone(testProject1.Id,null,'UNIT TEST MILESTONE NAME ACB123XYZ UNIQUE' + System.now());
        testMilestone1.Deadline__c = Date.today();
        testMilestone1.Complete__c = false;
        insert testMilestone1;
        
        Milestone1_Task__c testTask = Milestone1_Test_Utility.sampleTask(testMilestone.Id);
        testTask.Complete__c = false;
        testTask.Start_Date__c = Date.today();
        testTask.Due_Date__c = Date.today();
        insert testTask;
        
        Milestone1_Task__c testTask1 = Milestone1_Test_Utility.sampleTask(testMilestone1.Id);
        testTask1.Complete__c = false;
        testTask1.Start_Date__c = Date.today();
        testTask1.Due_Date__c = Date.today();
        insert testTask1;
        
        test.startTest();
        Milestone1_repeatCon controller = new Milestone1_repeatCon();
        controller.next();
        controller.prev();
        controller.selectedProject = null;
        controller.filterItem = ALL_ITEMS;//'All Items';
        controller.filter();
        List<Milestone1_Month.Week> weeksList = controller.getWeeks();
        test.stopTest();
        Map<ID,Milestone1_Project__c> projMap= controller.getSelectedProject();
        Map<ID,Milestone1_Milestone__c> milestonesMap=controller.getProjectSpecificMilestone();

        System.debug('!!@@ Mapa projectos----> ' + projMap);
        System.debug('!!@@ Mapa milestones----> ' + milestonesMap);


        List <Milestone1_Milestone__c> lst_milestones = [SELECT Id, Project__c, Project__r.Status__c FROM Milestone1_Milestone__c];

        Integer i = 1;
        for(Milestone1_Milestone__c mile : lst_milestones){
          System.debug('!!@@ Milestone ' + i + ' info');
          System.debug('!!@@ Id----> ' + mile.Id);
          System.debug('!!@@ projectId----> ' + mile.Project__c);
          System.debug('!!@@ project status----> ' + mile.Project__r.Status__c);
          System.debug('*************************************************');
        }

        List <Milestone1_Milestone__c> lst_milestones2 = [Select Id from Milestone1_Milestone__c where Project__c in : projMap.keySet()];
        System.debug('!!@@ proyectos----> ' + projMap.keySet());
        System.debug('!!@@ tamaño query----> ' + lst_milestones2.size());

        system.assertEquals(projMap.containsKey(testProject.id),true);
        system.assertEquals(projMap.containsKey(testProject1.id),true);
        system.assertEquals(milestonesMap.containsKey(testMilestone.id),true);
        system.assertEquals(milestonesMap.containsKey(testMilestone1.id),true);
        Milestone1_Task__c task=[Select id,name from Milestone1_Task__c where id =: testTask.id];
        system.assertEquals(testTask.id,task.id);
        
    }


@isTest static void testControllerFilterMyProjectsWithBlankProject()
  {
  	     Milestone1_Project__c testProject1 = [Select id from Milestone1_Project__c];
         Milestone1_Project__c testProject = Milestone1_Test_Utility.sampleProject('Trial Project ABCXYZ' + System.now());
        testProject.Status__c = 'Active';
        testProject.Deadline__c = Date.today();
        system.debug(testProject.name);
        insert testProject;

        Milestone1_Milestone__c testMilestone = Milestone1_Test_Utility.sampleMilestone(testProject.Id,null,'UNIT TEST MILESTONE NAME ACB123XYZ UNIQUE' + System.now());
        testMilestone.Deadline__c = Date.today();
        testMilestone.Complete__c = false;
        insert testMilestone;
        
        Milestone1_Task__c testTask = Milestone1_Test_Utility.sampleTask(testMilestone.Id);
        testTask.Complete__c = false;
        testTask.Start_Date__c = Date.today();
        testTask.Due_Date__c = Date.today();
        insert testTask;
        
        test.startTest();
        Milestone1_repeatCon controller = new Milestone1_repeatCon();
        controller.next();
        controller.prev();
        controller.selectedProject = 'AllProjects';
        controller.filterItem = MY_PROJECTS_AND_ITEMS;//'My Projects And Sub-Project Items';
        controller.filter();
        List<Milestone1_Month.Week> weeksList = controller.getWeeks();
        test.stopTest();
        Map<ID,Milestone1_Project__c> projMap= controller.getSelectedProject();
        Map<ID,Milestone1_Milestone__c> milestonesMap=controller.getProjectSpecificMilestone();

        System.debug('!!@@ Mapa projectos----> ' + projMap);
        System.debug('!!@@ Mapa milestones----> ' + milestonesMap);


        List <Milestone1_Milestone__c> lst_milestones = [SELECT Id, Project__c, Project__r.Status__c, Project__r.Name FROM Milestone1_Milestone__c];

        Integer i = 1;
        for(Milestone1_Milestone__c mile : lst_milestones){
          System.debug('!!@@ Milestone ' + i + ' info');
          System.debug('!!@@ Id----> ' + mile.Id);
          System.debug('!!@@ projectId----> ' + mile.Project__c);
          System.debug('!!@@ project status----> ' + mile.Project__r.Status__c);
          System.debug('!!@@ project name----> ' + mile.Project__r.Name);
          System.debug('*************************************************');
        }

        List <Milestone1_Milestone__c> lst_milestones2 = [Select Id from Milestone1_Milestone__c where Project__c in : projMap.keySet()];
        System.debug('!!@@ proyectos----> ' + projMap.keySet());
        System.debug('!!@@ tamaño query----> ' + lst_milestones2.size());

        system.assertEquals(projMap.containsKey(testProject.id),true);
        system.assertEquals(projMap.containsKey(testProject1.id),true);
        system.assertEquals(milestonesMap.containsKey(testMilestone.id),true);
        Milestone1_Task__c task=[Select id,name from Milestone1_Task__c where id =: testTask.id];
        system.assertEquals(testTask.id,task.id);
  }
  
  @isTest static void testControllerFilterMilestonesWithBlankProject()
  {
        Milestone1_Project__c testProject = Milestone1_Test_Utility.sampleProject('Trial Project ABCXYZ' + System.now());
        testProject.Status__c = 'Active';
        testProject.Deadline__c = Date.today();
        system.debug('testControllerFilterMilestonesWithBlankProject testProject:'+testProject.name);
        insert testProject;
        

        Milestone1_Milestone__c testMilestone = Milestone1_Test_Utility.sampleMilestone(testProject.Id,null,'UNIT TEST MILESTONE NAME ACB123XYZ UNIQUE' + System.now());
        testMilestone.Deadline__c = Date.today();
        testMilestone.Complete__c = false;
        insert testMilestone;
        
        Milestone1_Milestone__c testMilestone1 = Milestone1_Test_Utility.sampleMilestone(testProject.Id,null,'UNIT TEST MILESTONE NAME ACB123XYZ UNIQUE' + System.now());
        testMilestone1.Deadline__c = Date.today();
        testMilestone1.Complete__c = false;
        insert testMilestone1;
        
        Milestone1_Task__c testTask = Milestone1_Test_Utility.sampleTask(testMilestone.Id);
        testTask.Complete__c = false;
        testTask.Start_Date__c = Date.today();
        testTask.Due_Date__c = Date.today();
        insert testTask;
        
        test.startTest();
        Milestone1_repeatCon controller = new Milestone1_repeatCon();
        controller.next();
        controller.prev();
        controller.selectedProject = testProject.id;
        controller.filterItem = MY_MILESTONES_ONLY;//'My Milestones Only';
        controller.filter();
        List<Milestone1_Month.Week> weeksList = controller.getWeeks();
        controller.getMonth(); //JEG
        controller.monthView(); //JEG
        test.stopTest();
        Map<ID,Milestone1_Project__c> projMap= controller.getSelectedProject();
        Map<ID,Milestone1_Milestone__c> milestonesMap=controller.getProjectSpecificMilestone();
        System.debug('testControllerFilterMilestonesWithBlankProject ' + milestonesMap);
        system.assertEquals(milestonesMap.containsKey(testMilestone.id),true);
        system.assertEquals(milestonesMap.containsKey(testMilestone1.id),true);
        
        
  }
  
  @isTest static void testControllerFilterTasksWithBlankProject()
  {
  		Milestone1_Project__c testProject = [Select id from Milestone1_Project__c];
  		Milestone1_Milestone__c testMilestone = [Select id from Milestone1_Milestone__c];
  		Milestone1_Task__c testTask = [Select id from Milestone1_Task__c]; 
        
        Milestone1_Task__c testTask1 = Milestone1_Test_Utility.sampleTask(testMilestone.Id);
        testTask1.Complete__c = false;
        testTask1.Start_Date__c = Date.today();
        testTask1.Due_Date__c = Date.today();
        insert testTask1;
        
        test.startTest();
        Milestone1_repeatCon controller = new Milestone1_repeatCon();
        controller.next();
        controller.prev();
        controller.selectedProject = 'AllProjects';
        controller.filterItem = MY_TASKS;//'My Tasks Only';
        controller.filter();
        List<Milestone1_Month.Week> weeksList = controller.getWeeks();
        test.stopTest();
        Map<ID,Milestone1_Project__c> projMap= controller.getSelectedProject();
        Map<ID,Milestone1_Milestone__c> milestonesMap=controller.getProjectSpecificMilestone();
        Milestone1_Task__c task=[Select id,name from Milestone1_Task__c where id =: testTask.id];
        system.assertEquals(testTask.id,task.id);
        task=[Select id,name from Milestone1_Task__c where id =: testTask1.id];
        system.assertEquals(testTask1.id,task.id);
  }
  
  
	
}