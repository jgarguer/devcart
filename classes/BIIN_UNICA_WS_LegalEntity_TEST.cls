@isTest(seeAllData = false)
public class BIIN_UNICA_WS_LegalEntity_TEST 
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        José Luis González Beltrán
  Company:       HPE
  Description:   Test method to manage the code coverage for BIIN_UNICA_WS_LegalEntity
  
  <Date>             <Author>                        			<Change Description>
  20/06/2016         José Luis González Beltrán           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
  private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
  private final static String VALIDADOR_FISCAL_UPDATE = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(10);
  private final static String LE_NAME = 'TestAccount_WS_LegalEntity_TEST_TEST';
  private final static String LE_NAME_UPDATE = 'TestAccount_WS_LegalEntity_TEST_UPDATE';

  @testSetup static void dataSetup() 
  {
  	BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
    Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
    insert account;

    Account accountModified = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME_UPDATE, VALIDADOR_FISCAL_UPDATE);
    insert accountModified;
  }

  @isTest static void test_BIIN_UNICA_WS_LegalEntity_TEST()
  {
  	Account account = [SELECT Id, Name, BI_Activo__c, BI_Country__c, BI_Segment__c, BI_Subsegment_Local__c, BI_Tipo_de_identificador_fiscal__c, 
  														BI_No_Identificador_fiscal__c, BI_Validador_Fiscal__c, RecordTypeId, BI_RecordType_DevName__c 
  											FROM Account WHERE Name =: LE_NAME LIMIT 1]; //Added field BI_Subsegment_Local__c to query

  	Account accountModified = [SELECT Id, Name, BI_Activo__c, BI_Country__c, BI_Segment__c, BI_Subsegment_Local__c, BI_Tipo_de_identificador_fiscal__c, 
  																		BI_No_Identificador_fiscal__c, BI_Validador_Fiscal__c, RecordTypeId, BI_RecordType_DevName__c 
  															FROM Account WHERE Name =: LE_NAME_UPDATE LIMIT 1]; //Added field BI_Subsegment_Local__c to query

  	Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA(''));
    Test.startTest(); 

    BIIN_UNICA_WS_LegalEntity wsLegalEntity = new BIIN_UNICA_WS_LegalEntity();

    wsLegalEntity.postLegalEntity(account);
    wsLegalEntity.putLegalEntity(account, accountModified);
        
    Test.stopTest();
  }
}