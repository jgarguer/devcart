@isTest
public with sharing class BI_G4C_Filtro_campanias_controller_TEST {
      
      /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Julio Alberto Asenjo García
	    Company:       everis
	    Description:   Test Class for the class BI_G4C_Filtro_campanias_controller
	    
	    History: 
	    
	    <Date>                          <Author>                    	<Change Description>
	    20/06/2016                      Julio Alberto Asenjo García     Initial version
        23/02/2017                      Jaime Regidor                   Adding Campaign Values, Adding Catalog Country
        20/09/2017                  Angel F. Santaliestra               Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
        
      static testMethod void testgetMyAccounts() {
        
        NE__Catalog__c cat = new NE__Catalog__c (Name = 'test catalog', BI_country__c = 'Argentina');
        insert cat;
        Campaign ca = new Campaign(Name = 'test',CurrencyIsoCode = 'USD',EndDate = date.TODAY(),BI_Catalogo__c=cat.Id, BI_country__c = 'Argentina', Status='En Curso', IsActive=true,BI_Opportunity_Type__c = 'Fijo',
        BI_SIMP_Opportunity_Type__c = 'Alta');
        ca.Status='In Progress';
        insert ca;
        Account a = new Account(Name='pruebaTest',BI_Segment__c = 'test',
                                BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test');
        insert a;
        Contact c = new Contact(LastName='cpruebaTest', AccountId=a.Id);
        insert c;
        CampaignMember newMember = new CampaignMember(CampaignId = ca.Id, ContactId = c.Id);
        insert newMember;
        
        BI_G4C_Filtro_campanias_controller.getOptions();
}
}