public with sharing class BI_ProjectStatus_CTRL {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Pablo Oliva
	Company:       Salesforce.com
	Description:   Controller class for BI_ProjectStatus page
	History:
	
	<Date>            <Author>              <Description>
	08/07/2014        Pablo Oliva           Initial version
	05/07/2016        Jose Miguel Fierro    If the project status is not set, show grey
	22/08/2016        Jose Miguel Fierro    Show statuses specific to BI_O4_Provisioning_Steady_State
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	
	public Milestone1_Project__c project { get; set; }
	public Boolean ProvisioningSteadyState {get; set;}

	private final String green = Label.BI_ColorGreen;
	private final String blue = Label.BI_ColorBlue;
	private final String grey = Label.BI_ColorGrey; // 05/07/2016
	
	public String StatusColor1 { get; set; }
	public String StatusColor2 { get; set; }
	public String StatusColor3 { get; set; }
	public String StatusColor4 { get; set; }
	public String StatusColor5 { get; set; }
	public String StatusColor6 { get; set; }
	public String StatusColor7 { get; set; }
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Pablo Oliva
	Company:       Salesforce.com
	Description:   Constructor Method (calculates the stages colors)
	
	IN:            ApexPages.StandardController (Milestone1_Project__c)
	OUT:           
	
	History:
	
	<Date>            <Author>              <Description>
	08/07/2014        Pablo Oliva           Initial version
	05/07/2016        Jose Miguel Fierro    If the project status is not set, show grey
	22/08/2016        Jose Miguel Fierro    Show statuses specific to BI_O4_Provisioning_Steady_State
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public BI_ProjectStatus_CTRL(ApexPages.StandardController controller){
		try{
			project = (Milestone1_Project__c)controller.getRecord();
			String kickoff = 'Kickoff de inicio';
			String init = 'Inicio';
			String planning = 'Planificación';
			String execution = 'Ejecución';
			String control = 'Control';
			String close = 'Cierre';
			String closeKickOff = 'Kickoff de cierre';

			String assign = 'Assign Project Manager';
			String provision = 'Provisioning';
			String review = 'Review Project Manager Remarks';
			String closeMeeting = 'Project Closing Meeting';
			String accept = 'Accept Transition Package';

			project = [select Id, BI_Etapa_del_proyecto__c, RecordType.DeveloperName from Milestone1_Project__c where Id = :project.Id limit 1];
			
			if(project.RecordType.DeveloperName != 'BI_O4_Provisioning_Steady_State') { // JMF 22/08/2016
				if(project.BI_Etapa_del_proyecto__c == kickoff){
					StatusColor1 = green;
					StatusColor2 = blue;
					StatusColor3 = blue;
					StatusColor4 = blue;
					StatusColor5 = blue;
					StatusColor6 = blue;
					StatusColor7 = blue;
				}else if(project.BI_Etapa_del_proyecto__c == init){
					StatusColor1 = green;
					StatusColor2 = green;
					StatusColor3 = blue;
					StatusColor4 = blue;
					StatusColor5 = blue;
					StatusColor6 = blue;
					StatusColor7 = blue;
				}else if(project.BI_Etapa_del_proyecto__c == planning){
					StatusColor1 = green;
					StatusColor2 = green;
					StatusColor3 = green;
					StatusColor4 = blue;
					StatusColor5 = blue;
					StatusColor6 = blue;
					StatusColor7 = blue;
				}else if(project.BI_Etapa_del_proyecto__c == execution){
					StatusColor1 = green;
					StatusColor2 = green;
					StatusColor3 = green;
					StatusColor4 = green;
					StatusColor5 = blue;
					StatusColor6 = blue;
					StatusColor7 = blue;
				}else if(project.BI_Etapa_del_proyecto__c == control){
					StatusColor1 = green;
					StatusColor2 = green;
					StatusColor3 = green;
					StatusColor4 = green;
					StatusColor5 = green;
					StatusColor6 = blue;
					StatusColor7 = blue;
				}else if(project.BI_Etapa_del_proyecto__c == close){
					StatusColor1 = green;
					StatusColor2 = green;
					StatusColor3 = green;
					StatusColor4 = green;
					StatusColor5 = green;
					StatusColor6 = green;
					StatusColor7 = blue;
				}else if(project.BI_Etapa_del_proyecto__c == closeKickOff){
					StatusColor1 = green;
					StatusColor2 = green;
					StatusColor3 = green;
					StatusColor4 = green;
					StatusColor5 = green;
					StatusColor6 = green;
					StatusColor7 = green;
				}
				// START JMF 05/07/2016
				else if(String.isBlank(project.BI_Etapa_del_proyecto__c)){
					StatusColor1 = grey;
					StatusColor2 = grey;
					StatusColor3 = grey;
					StatusColor4 = grey;
					StatusColor5 = grey;
					StatusColor6 = grey;
					StatusColor7 = grey;
				}
				// END JMF 05/07/2016
			}
			// START JMF 22/08/2016
			else {
				ProvisioningSteadyState = true;
				if(project.BI_Etapa_del_proyecto__c == assign){
					StatusColor1 = green;
					StatusColor2 = blue;
					StatusColor3 = blue;
					StatusColor4 = blue;
					StatusColor5 = blue;
					StatusColor6 = grey;
					StatusColor7 = grey;
				}else if(project.BI_Etapa_del_proyecto__c == provision){
					StatusColor1 = green;
					StatusColor2 = green;
					StatusColor3 = blue;
					StatusColor4 = blue;
					StatusColor5 = blue;
					StatusColor6 = grey;
					StatusColor7 = grey;
				}else if(project.BI_Etapa_del_proyecto__c == review){
					StatusColor1 = green;
					StatusColor2 = green;
					StatusColor3 = green;
					StatusColor4 = blue;
					StatusColor5 = blue;
					StatusColor6 = grey;
					StatusColor7 = grey;
				}else if(project.BI_Etapa_del_proyecto__c == closeMeeting){
					StatusColor1 = green;
					StatusColor2 = green;
					StatusColor3 = green;
					StatusColor4 = green;
					StatusColor5 = blue;
					StatusColor6 = grey;
					StatusColor7 = grey;
				}else if(project.BI_Etapa_del_proyecto__c == accept){
					StatusColor1 = green;
					StatusColor2 = green;
					StatusColor3 = green;
					StatusColor4 = green;
					StatusColor5 = green;
					StatusColor6 = grey;
					StatusColor7 = grey;
				}
				// JMF 20/09/2016 - No tiene estado o no es uno de los del tipo de registro
				else /* if(String.isBlank(project.BI_Etapa_del_proyecto__c))*/  {
					StatusColor1 = grey;
					StatusColor2 = grey;
					StatusColor3 = grey;
					StatusColor4 = grey;
					StatusColor5 = grey;
					StatusColor6 = grey;
					StatusColor7 = grey;
				}
				// JMF 20/09/2016 - No tiene estado
			}
			// END JMF 22/08/2016
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('BI_ProjectStatus_CTRL.BI_ProjectStatus_CTRL', 'BI_EN', Exc, 'Trigger');
		}
		
	}
}