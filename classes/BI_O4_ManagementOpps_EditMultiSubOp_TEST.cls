/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Test class for BI_O4_ManagementOppsMultiSubOppSave_TEST
    Test Class:       
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Fernando Arteaga          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_ManagementOpps_EditMultiSubOp_TEST {

  static{
        BI_TestUtils.throw_exception = false;
  }
	
	@isTest static void editSelectedOpportunities() {

		List<RecordType> rts = [SELECT Id FROM RecordType 
        WHERE DeveloperName = 'BI_Oportunidad_Regional'];

	Opportunity oppPadre = new Opportunity(Name = 'OppNamePadre',
                      					  CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                      					  BI_Ciclo_ventas__c = Label.BI_Completo, 
                      					  RecordTypeId = rts[0].Id,
                      					  BI_O4_Finance_approval_check__c = true
                      					  );	
	insert oppPadre;
	
	Opportunity opp = new Opportunity(Name = 'OppName',
                      					  CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                      					  BI_Ciclo_ventas__c = Label.BI_Completo,
                      					  BI_Oportunidad_padre__c = oppPadre.Id,
                      					  BI_O4_Finance_approval_check__c = true
                      					  );
	insert opp;
        
    Ne__Order__c order = new Ne__Order__c(NE__OrderStatus__c = 'Active',
											Ne__OptyId__c = opp.Id
											);
	insert order;

	NE__OrderItem__c orderItem = new NE__OrderItem__c(NE__Qty__c = 1,
                                                  		CurrencyIsoCode = 'EUR',
														Ne__OrderId__c = order.Id
														);

	insert orderItem;

	Test.startTest();
	ApexPages.standardController controller = new ApexPages.standardController(oppPadre);
	BI_O4_ManagementOpps_EditMultiSubOpp mO = new BI_O4_ManagementOpps_EditMultiSubOpp(controller);
	Mo.saveOpportunities();
	mo.wrapperList[0].check = true;
	Mo.editSelectedOpportunities();
	Mo.saveOpportunities();
	Mo.doActionCancel();
	Test.stopTest();

	}
	
}