/**
**************************************************************************************************************
* @company          Avanxo Colombia
* @author           Antonio Torres href=<atorres@avanxo.com>
* @project          Telefonica BI_EN Fase 2 Colombia
* @name             BI2_COL_DeleteTechCaseAttachments_sch
* @description      Schedulable class for masive record deletion, for attachments of technical incidents.
* @dependencies     Class BI2_COL_DeleteRecords_bch
* @changes (Version)
* --------   ---   ----------   ---------------------------   ------------------------------------------------
*            No.   Date         Author                        Description
* --------   ---   ----------   ---------------------------   ------------------------------------------------
* @version   1.0   2017-03-01   Antonio Torres (AT)           Initial version.
**************************************************************************************************************
**/

global class BI2_COL_DeleteTechCaseAttachments_sch implements Schedulable {
    global void execute(SchedulableContext sc) {
        System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'BI2_COL_DeleteTechCaseAttachments_sch' + '   <<<<<<<<<<\n-=#=-\n');

        String strQuery = 'SELECT Id ' +
                            'FROM Attachment ' +
                           'WHERE CreatedDate >= LAST_N_DAYS:' + Label.BI2_COL_Borrar_adjuntos_casos_cantidad_dias + ' ' +
                             'AND ParentId IN ( ' +
                                    'SELECT Id ' +
                                      'FROM Case ' +
                                     'WHERE RecordType.DeveloperName = \'' + Label.BI2_COL_Incidencia_tecnica + '\' ' +
                                 ')';
        
        System.debug('\n\n-=#=-\n' + 'strQuery' + ': ' + strQuery + '\n-=#=-\n');

        BI2_COL_DeleteRecords_bch objDeleteRecordsBatch = new BI2_COL_DeleteRecords_bch(strQuery, Boolean.valueOf(Label.BI2_COL_Borrar_adjuntos_casos_bandera_email));
        database.executeBatch(objDeleteRecordsBatch, Integer.valueOf(Label.BI2_COL_Borrar_adjuntos_casos_cantidad_registros));
    }
}