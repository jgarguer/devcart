/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:			Angelo Salamanca
Company:		Everis Centers Temuco
Description:	RFG-NOR-DIR-004 / RFG-NOR-DIR-005 / RFG-NOR-DIR-006:
Extension apex del controlador de la pagina visualforce que redirecciona segun el pais del objeto direcciones.

History:

<Date>                      <Author>                        <Change Description>
24/11/2016                  Angelo Salamanca				Initial Version
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

public with sharing class FS_CHI_Address_Standard_Redirection_ctr {
    public Id IdDireccion{get;set;}
    public String countryName{get;set;}
    
    ApexPages.StandardController controller;
    
    public FS_CHI_Address_Standard_Redirection_ctr(ApexPages.StandardController controller){
        this.IdDireccion = controller.getId();
        this.controller = controller;
        
        getcountry();
    }
    
    public void getCountry(){
        BI_Sede__c sedeObject = [SELECT Id, BI_Country__c FROM BI_Sede__c WHERE Id = :IdDireccion LIMIT 1];
        countryName = sedeObject.BI_Country__c;
    }
    
    public PageReference redirect(){
        PageReference pageRef;
        
        pageRef = new PageReference('/apex/FS_CHI_Address_Record?addressId='+ IdDireccion);
        pageRef.setRedirect(true);
        
        return pageRef;
    }
    
    
}