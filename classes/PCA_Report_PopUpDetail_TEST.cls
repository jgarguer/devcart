@isTest
public class PCA_Report_PopUpDetail_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jose Miguel Fierro
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_Report_PopUpDetail class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    13/08/2014              Jose Miguel Fierro      Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	@isTest(SeeAllData=true)    
    public static void getLoadInfo1() {
        BI_TestUtils.throw_exception = false;
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        
        system.runas(usr) {
            List<String> lst_pais = new List<String>();
            lst_pais.add('Guatemala');
            //List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalPlusProfile());
            
            System.runAs(user1) {            
                List<Report> lsReport = [SELECT Id FROM Report LIMIT 1];
                system.debug('+Report: ' + lsReport);
                if(!lsReport.isEmpty()) {
                    PageReference pgRef = new PageReference('PCA_ReportDetail');
                    Test.setCurrentPage(pgRef);
                    ApexPages.currentPage().getParameters().put('id', lsReport[0].Id); 
                }
                PCA_Report_PopUpDetail control = new PCA_Report_PopUpDetail();
                system.assertNotEquals(null, control.checkPermissions());
            }
        }
    }
    
    //@isTest(SeeAllData='true') 
    public static testMethod void getLoadInfo2() {
        BI_TestUtils.throw_exception = false;
        List<Report> lsReport = [SELECT Id FROM Report LIMIT 1];
        system.debug('+Report (empty): ' + lsReport);
        if(!lsReport.isEmpty()) {
            PageReference pgRef = new PageReference('PCA_ReportDetail');
            Test.setCurrentPage(pgRef);
            ApexPages.currentPage().getParameters().put('id', lsReport[0].Id); 
        }
        PCA_Report_PopUpDetail control = new PCA_Report_PopUpDetail();
        system.assertEquals(null, control.checkPermissions());
    }
    
    public static testMethod void checkAsTest() {
        BI_TestUtils.throw_exception = true;
        PCA_Report_PopUpDetail cont = new PCA_Report_PopUpDetail();
        cont.checkPermissions();
        cont.loadInfo();
    }
}