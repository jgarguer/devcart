/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for batch BI_COL_BatchDeleteDuplicateDirection_bch
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-27      Raul Mora (RM)				    Create Class      
*************************************************************************************/
@isTest
private class BI_COL_BatchDeleteDuplicateDirection_tst 
{
	Public static list<Profile> 											lstPerfil;
	Public static list <UserRole> 											lstRoles;
	Public static User 													objUsuario;

	public void createData()
	{
		////lstPerfil
		//lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];

		////lstRol
		//lstRoles = new list <UserRole>();
		//lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

		////ObjUsuario
		//objUsuario = new User();
		//objUsuario.Alias = 'standt';
		//objUsuario.Email ='pruebas@test.com';
		//objUsuario.EmailEncodingKey = '';
		//objUsuario.LastName ='Testing';
		//objUsuario.LanguageLocaleKey ='en_US';
		//objUsuario.LocaleSidKey ='en_US'; 
		//objUsuario.ProfileId = lstPerfil.get(0).Id;
		//objUsuario.TimeZoneSidKey ='America/Los_Angeles';
		//objUsuario.UserName ='pruebas@test.com';
		//objUsuario.EmailEncodingKey ='UTF-8';
		//objUsuario.UserRoleId = lstRoles.get(0).Id;
		//objUsuario.BI_Permisos__c ='Sucursales';
		//objUsuario.Pais__c='Colombia';
		
		//list<User> lstUsuarios = new list<User>();
		//lstUsuarios.add(objUsuario);
		//insert lstUsuarios;

		//System.runAs(lstUsuarios[0])
		//{
			BI_Col_Ciudades__c objCity = new BI_Col_Ciudades__c();
			objCity.Name = 'Test City';
			objCity.BI_COL_Pais__c = 'Test Country';
			objCity.BI_COL_Codigo_DANE__c     = 'TestCDa';
			insert objCity;
			
			BI_Sede__c objSede = new BI_Sede__c();
			objSede.BI_COL_Ciudad_Departamento__c = objCity.Id;
			objSede.BI_Direccion__c = 'Test Street 123 Number 321';
			objSede.BI_Localidad__c = 'Test Local';
			objSede.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor4EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c = 'Libre';
			objSede.BI_Country__c = 'Colombia';
			objSede.Name = 'Test Street 123 Number 321, Test Local Colombia';
			objSede.BI_Codigo_postal__c = '12356';
			insert objSede;
		//}
	}
	
    static testMethod void myUnitTest() 
    {
    	objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
	        BI_COL_BatchDeleteDuplicateDirection_tst testClass = new BI_COL_BatchDeleteDuplicateDirection_tst();
			testClass.createData();
			
			Test.startTest();
				BI_COL_BatchDeleteDuplicateDirection_bch class_batch = new BI_COL_BatchDeleteDuplicateDirection_bch();        
		        Id BatchProcessId = Database.executeBatch(class_batch, 1);
			Test.stopTest();
		}
		
    }
     static testMethod void myUnitTest1() 
    {
    	objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
	        BI_COL_BatchDeleteDuplicateDirection_tst testClass = new BI_COL_BatchDeleteDuplicateDirection_tst();
			testClass.createData();
			BI_Col_Ciudades__c objCity = new BI_Col_Ciudades__c();
			objCity.Name = 'Test City';
			objCity.BI_COL_Pais__c = 'Test Country';
			objCity.BI_COL_Codigo_DANE__c     = 'TestCDa';
			insert objCity;
			BI_Sede__c objSede = new BI_Sede__c();
			objSede.BI_COL_Ciudad_Departamento__c = objCity.Id;
			objSede.BI_Direccion__c = 'Test Street 123 Number 321';
			objSede.BI_Localidad__c = 'Test Local';
			objSede.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor4EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c = label.BI_COL_LbValor1SucursalEnUso;
			objSede.BI_Country__c = 'Colombia';
			objSede.Name = 'Test Street 123 Number 321, Test Local Colombia';
			objSede.BI_Codigo_postal__c = '12356';
			insert objSede;
			objSede = new BI_Sede__c();
			objSede.BI_COL_Ciudad_Departamento__c = objCity.Id;
			objSede.BI_Direccion__c = 'Test Street 123 Number 321';
			objSede.BI_Localidad__c = 'Test Local';
			objSede.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor4EstadoCallejero;
			objSede.BI_COL_Sucursal_en_uso__c = label.BI_COL_LbValor1SucursalEnUso;
			objSede.BI_Country__c = 'Colombia';
			objSede.Name = 'Test Street 123 Number 321, Test Local Colombia';
			objSede.BI_Codigo_postal__c = '12356';
			insert objSede;
			Test.startTest();
				BI_COL_BatchDeleteDuplicateDirection_bch class_batch = new BI_COL_BatchDeleteDuplicateDirection_bch();        
		        Id BatchProcessId = Database.executeBatch(class_batch, 100);
			Test.stopTest();
		}
    }
}