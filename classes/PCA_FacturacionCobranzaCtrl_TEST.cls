@isTest
private class PCA_FacturacionCobranzaCtrl_TEST {

    static testMethod void getloadinfo() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
        List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
              item.OwnerId = usr.Id;
              accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
        system.runAs(user1){
          
          
          BI_TestUtils.throw_exception = false;
          
          
          BI_Actualizaciones_integracion__c act_integra = new BI_Actualizaciones_integracion__c(
          Name = 'Test',
          BI_Country__c = lst_pais[0],
          BI_Entidad__c = 'Cobranza',
          BI_UltimaModificacion__c = '25/07/2014'
        );
        
        insert act_integra;

        
        
        
         PageReference pageRef = new PageReference('PCA_FacturacionCobranza');
         Test.setCurrentPage(pageRef);
         //ApexPages.currentPage().getParameters().put('type', 'o');
            
        PCA_FacturacionCobranzaCtrl tmpContr = new PCA_FacturacionCobranzaCtrl();
        tmpContr.checkPermissions();
        tmpContr.loadInfo();
        tmpContr.changeTab();
        tmpContr.defineRecords();
        tmpContr.defineViews();
        tmpContr.defineSearch();
        tmpContr.getItemPage();
        tmpContr.getHasPrevious();
        tmpContr.getHasNext();
        tmpContr.Previous();
        tmpContr.Next();
        tmpContr.valuesInPage();
        tmpContr.Order();
        tmpContr.searchRecords();
        }
      }
    }
    
    static testMethod void PCA_FacturacionCobranzaCtrl_catch_TEST() {
      /*  TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
      BI_TestUtils.throw_exception = true;
                
      PCA_FacturacionCobranzaCtrl tmpContr = new PCA_FacturacionCobranzaCtrl();
        tmpContr.checkPermissions();
        tmpContr.loadInfo();
        tmpContr.changeTab();
        tmpContr.defineRecords();
        tmpContr.defineViews();
        tmpContr.defineSearch();
        tmpContr.getItemPage();
        tmpContr.getHasPrevious();
        tmpContr.getHasNext();
        tmpContr.Previous();
        tmpContr.Next();
        tmpContr.valuesInPage();
        tmpContr.Order();
        tmpContr.searchRecords();
  }
        
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}