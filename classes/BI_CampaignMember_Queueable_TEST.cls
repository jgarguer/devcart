@isTest
private class BI_CampaignMember_Queueable_TEST {

  static{BI_TestUtils.throw_exception = false;}

    /*static testMethod void myUnitTest() {

      TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }
        
        // TO DO: implement unit test
        
        List <String> lst_pais =new List <String>();
        lst_pais.add('Uruguay');
          
        List <Account> lst_acc =BI_DataLoad.loadAccounts(2, lst_pais);
    
        List <Contact> lst_con =BI_DataLoad.loadContacts(2, lst_acc);
   
        List <Lead> lst_ld =BI_DataLoad.loadLeads(2);
  
        List <Campaign> lst_camp = new List<Campaign>();
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name =: Label.BI_Administrador limit 1];
        
        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof.Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com');
        
        insert usertest;
        
        system.runAs(usertest){
            lst_camp = BI_DataLoad.loadCampaigns(2);
        }
        
        Set<Id> set_camp = new Set<Id>();
        
        for (Campaign camp:lst_camp){
            camp.IsActive = true;
            camp.Status = Label.BI_EnCurso;
            set_camp.add(camp.Id);
        }
            
        update lst_camp;
   
        List <CampaignMember> lst_cm = BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);
        system.assert(!lst_cm.isEmpty());
        system.debug('*** lst_cm: ' + lst_cm);
        
        Set<Id> set_c = new Set<Id>();
        
        for (CampaignMember cmem:lst_cm)
            set_c.add(cmem.Id);
        
        BI_Aprobacion_Miembros_Campana__c [] amc = [SELECT Id, BI_Miembro_de_Campana__c FROM BI_Aprobacion_Miembros_Campana__c WHERE BI_Miembro_de_Campana__c IN :set_c];
        
        for (BI_Aprobacion_Miembros_Campana__c amemc:amc)
            amemc.BI_Estado__c = Label.BI_Aprobado;

        update amc;
        
        Test.startTest();        
        System.enqueueJob(new BI_CampaignMember_Queueable(set_camp, 2, set_camp));
        Test.stopTest();
    }*/
    static testMethod void myUnitTest2() {

      TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }
        
        // TO DO: implement unit test
        
        List <String> lst_pais =new List <String>();
        lst_pais.add('Chile');
        List <String> lst_pais2 =new List <String>();
        lst_pais2.add('Uruguay');
          
        List <Account> lst_acc =BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais);
        List <Account> lst_acc2 =BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais2);
        
        List <Contact> lst_con =BI_DataLoad.loadContacts(2, lst_acc);
        List <Contact> lst_con2 =BI_DataLoad.loadContacts(2, lst_acc2);
        List <Lead> lst_ld =BI_DataLoad.loadLeads(2);
  
        List <Campaign> lst_camp = new List<Campaign>();
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name =: Label.BI_Administrador limit 1];
        
        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof.Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com');
        
        insert usertest;
        
        system.runAs(usertest){
            lst_camp = BI_DataLoad.loadCampaignsWithRecordType(2, BI_DataLoad.obtainCampaignRecordType());
        }
        
        Set<Id> set_camp = new Set<Id>();

        NE__Product__c prod = new NE__Product__c(
          Name = 'Test'
        );

        insert prod;

        NE__Catalog__c cat = [SELECT Id, BI_Country__c FROM NE__Catalog__c LIMIT 1];

        NE__Catalog_Category__c catalogCategory = new NE__Catalog_Category__c(
          Name = 'Test',
          NE__CatalogId__c = cat.Id
        );
        insert catalogCategory;

        NE__Catalog_Item__c catalogItem = new NE__Catalog_Item__c(
          NE__ProductId__c = prod.Id,
          NE__Catalog_Id__c = cat.Id,
          NE__Catalog_Category_Name__c = catalogCategory.Id
        );
        insert catalogItem;

        List <BI_Promociones__c> lst_campProd = new List <BI_Promociones__c>();

        for(Campaign camp : lst_camp){

          BI_Promociones__c campProd = new BI_Promociones__c(
            BI_Campanas__c = camp.Id,
            BI_Catalogo_Producto__c = cat.Id,
            BI_Producto__c = catalogItem.Id
          );
          lst_campProd.add(campProd);
        }
        
        insert lst_campProd;
            
        //update lst_camp;
        List<CampaignMember> lst_campaignMembers = new List<CampaignMember>();

            
            for(Campaign item:lst_camp){
                
                CampaignMember camp = new CampaignMember(CampaignId = item.Id,
                                                       ContactId = lst_con2[0].Id,
                                                       BI_Correo_electronico_enviado__c = false ,
                                                       BI_Bloqueo__c = false ,
                                                       Status = 'Sent');
                    
                lst_campaignMembers.add(camp);
                
                CampaignMember camp2 = new CampaignMember(CampaignId = item.Id,
                                                        LeadId = lst_ld[0].Id,
                                                        BI_Correo_electronico_enviado__c = false ,
                                                        BI_Bloqueo__c = false ,
                                                        Status = 'Sent');
                                                        
                lst_campaignMembers.add(camp2);
            }
            
            insert lst_campaignMembers;
        
        Set<Id> set_c = new Set<Id>();
        
        for (CampaignMember cmem:lst_campaignMembers)
            set_c.add(cmem.Id);
        
        BI_Aprobacion_Miembros_Campana__c [] amc = [SELECT Id, BI_Miembro_de_Campana__c FROM BI_Aprobacion_Miembros_Campana__c WHERE BI_Miembro_de_Campana__c IN :set_c];
        
        for (BI_Aprobacion_Miembros_Campana__c amemc:amc)
            amemc.BI_Estado__c = Label.BI_Aprobado;

        update amc;
        for (Campaign camp:lst_camp){
            camp.IsActive = true;
            camp.Status = Label.BI_EnCurso;
            set_camp.add(camp.Id);
        }
        Test.startTest();        
        //System.enqueueJob(new BI_CampaignMember_Queueable(set_c, 1, set_camp));
        update lst_camp;
        Test.stopTest();
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 
	/*static testMethod void myUnitTest3() {

    TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }
		
		List <String> lst_pais =new List <String>();
        lst_pais.add('Chile');
        List <String> lst_pais2 =new List <String>();
        lst_pais2.add('Uruguay');
          
        List <Account> lst_acc =BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais);
        List <Account> lst_acc2 =BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais2);
        
        List <Contact> lst_con =BI_DataLoad.loadContacts(2, lst_acc);
        List <Contact> lst_con2 =BI_DataLoad.loadContacts(2, lst_acc2);
  
        List <Campaign> lst_camp = new List<Campaign>();
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name =: Label.BI_Administrador limit 1];
        
        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof.Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com');
        
        insert usertest;
        
        system.runAs(usertest){
            lst_camp = BI_DataLoad.loadCampaignsWithRecordType(2, BI_DataLoad.obtainCampaignRecordType());
        }
        
        Set<Id> set_camp = new Set<Id>();
        
        
            
        //update lst_camp;
        List<CampaignMember> lst_campaignMembers = new List<CampaignMember>();

            
            for(Campaign item:lst_camp){
                
                CampaignMember camp = new CampaignMember(CampaignId = item.Id,
                                                       ContactId = lst_con2[0].Id,
                                                       BI_Correo_electronico_enviado__c = false ,
                                                       BI_Bloqueo__c = false ,
                                                       Status = 'Sent');
                    
                lst_campaignMembers.add(camp);
            }
            
            insert lst_campaignMembers;
        
        Set<Id> set_c = new Set<Id>();
        
        for (CampaignMember cmem:lst_campaignMembers)
            set_c.add(cmem.Id);
        
        BI_Aprobacion_Miembros_Campana__c [] amc = [SELECT Id, BI_Miembro_de_Campana__c FROM BI_Aprobacion_Miembros_Campana__c WHERE BI_Miembro_de_Campana__c IN :set_c];
        
        for (BI_Aprobacion_Miembros_Campana__c amemc:amc)
            amemc.BI_Estado__c = Label.BI_Aprobado;

        update amc;
        
        for(Campaign camp:lst_camp){
        	camp.BI_Asunto_de_las_tareas__c='Test';
        	camp.BI_Cancela_Tareas_Cierre_Contactos__c=Label.BI_SI;
        	camp.BI_Crear_Tareas_Contactos__c=Label.BI_SI;
        	camp.BI_Crear_oportunidad_contactos__c=Label.BI_NO;
        	camp.BI_Catalogo__c=null;
        }
        update lst_camp;
        for (Campaign camp:lst_camp){
            camp.IsActive = true;
            camp.Status = Label.BI_EnCurso;
        }
        Test.startTest();        
        //System.enqueueJob(new BI_CampaignMember_Queueable(set_c, 1, set_camp));
        update lst_camp;
        for (Campaign camp:lst_camp){
            camp.IsActive = false;
        }
        update lst_camp;
        Test.stopTest();
		
	}*/
}