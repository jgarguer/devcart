public with sharing class TGS_ButtonHandler_Order_CTRL {
	public Boolean render_GlobalRender {get;set;}
	public Boolean render_FoundationDataAssigner {get;set;}
	public Boolean isRelatedSimCard {get;set;}
	public NE__Order__c order {get;set;}
	public String url {get;set;}

	public TGS_ButtonHandler_Order_CTRL() {}
	public TGS_ButtonHandler_Order_CTRL(ApexPages.StandardController controller)
	{
		this.render_GlobalRender = false;
		this.render_FoundationDataAssigner = false;
		this.isRelatedSimCard = false;
		Set<String> set_GMServices = new Set<String>{''};

        // 10/04/2018 - Iñaki Frial - Change filter for all services.
		for(NE__Catalog_Item__c citem :
            [
                SELECT  Id,NE__Root_Catalog_Item__r.NE__Catalog_Category_Name__r.Name,NE__ProductId__r.Name
                FROM    NE__Catalog_Item__c WHERE NE__Catalog_Id__r.Name ='TGSOL Catalog'
                //WHERE   NE__Root_Catalog_Item__r.NE__Catalog_Category_Name__r.Name = :Constants.GLOBAL_MOBILITY_FOR_MNCS OR NE__ProductId__r.Name IN :Constants.FERROVIAL_SERVICES
            ]
            )
        {
            set_GMServices.add(citem.NE__ProductId__r.Name);
        }
		this.order = (NE__Order__c) controller.getRecord();
		this.order = 	[
								SELECT 	Id,Case__r.TGS_Service__c,NE__Configuration_Type__c
								FROM 	NE__Order__c
								WHERE 	Id = :this.order.Id
							];

		/* START Álvaro López 28/03/2017 - Added Related Pooling Bundle validation */
		for(NE__OrderItem__c oi : [
									SELECT Id, NE__ProdId__r.Name,NE__Status__c 
									FROM   NE__OrderItem__c
									WHERE  NE__OrderId__c = :this.order.Id

								  ])
		{
            if(oi.NE__ProdId__r.Name == 'Pooling Bundle Selection'){
            	this.isRelatedSimCard = true;
            }
			/* END Álvaro López 28/03/2017 */
		}
		System.debug('order: '+this.order);
		this.url = '/apex/TGS_Update_Terminate_Product?ordId='+this.order.Id;
		if(set_GMServices.contains(this.order.Case__r.TGS_Service__c) && this.order.NE__Configuration_Type__c != 'Disconnect')
		{
			this.render_FoundationDataAssigner = true;
			//this.render_GlobalRender = true;
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Juan Carlos Terrón
	    Company:       Accenture
	    Description:   Redirects to TGS_Update_Terminate_Product page.
	    
	    IN:            None
	    OUT:           PageReference
	    
	    History:   
	    <Date>                  <Author>                <Change Description>
	    03/01/2017			Juan Carlos Terrón		Initial Version.        
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public void redirect(){
		this.url = '/apex/TGS_Update_Terminate_Product?ordId='+this.order.Id;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Álvaro López
	    Company:       Accenture
	    Description:   Render the BWP warning block.
	    
	    IN:            None
	    OUT:           PageReference
	    
	    History:   
	    <Date>                  <Author>                <Change Description>
	    28/03/2017				Álvaro López			Initial Version.        
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public PageReference render_bwp(){
		this.render_GlobalRender = true;
		this.render_FoundationDataAssigner = false;
		return null;
	}
}