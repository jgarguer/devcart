public class BIIN_Test_Data_Load {
    
    public static Account createAccount(String name){
        String nRandom = String.valueOf((Integer)(Math.random()*100));
        RecordType rt=[Select ID FROM RecordType WHERE DeveloperName='TGS_Legal_Entity' LIMIT 1];
        // 20/09/2017      Angel F. Santaliestra           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
        Account acc = new Account(Name = name,
                                  BI_Envio_a_aprobacion_agrupacion_cliente__c = false,
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Country__c = 'Peru',
                                  BI_Segment__c = 'Empresas',
                                  BI_Subsector__c = 'test', //27/09/2017 Added fields
                                  BI_Sector__c = 'test', //27/09/2017 Added fields
                                  BI_Subsegment_Regional__c = 'test',
                                  BI_Territory__c = 'test',
                                  BI_Tipo_de_identificador_fiscal__c='RUC',
                                  BI_No_Identificador_fiscal__c ='12345678910',
                                  BI_Subsegment_local__c = 'Top 1',
                                  RecordTypeId=rt.id
                                  );
        // END 20/09/2017      Angel F. Santaliestra
        return acc;
    }
    
    public static Contact createContact(String contactLastName,Account acLe){
        Contact contactTest = new Contact(LastName = contactLastName,
                                          AccountId = acLe.Id,
                                          Email = 'dummyBiin@test.com'
        );
        
        return contactTest;
        
    }
    
    public static User createUSer(String profileName, String permission){
        BI_TestUtils.throw_exception=false;
        Profile p = [Select Id From Profile Where Name=:profileName];
            
        String nRandom = String.valueOf((Integer)(Math.random()*100));
        String mailName = 'telefonicaTest'+nRandom+'@tefonica.com';
        
        User user = new User (alias = 'stttt'+string.valueof(nRandom),
                                        email=string.valueof(nRandom)+'u@testorg.com',
                                        emailencodingkey='UTF-8',
                                        lastname='Testing' +string.valueof(nRandom),
                                        languagelocalekey='en_US',
                                        localesidkey='en_US',
                                        ProfileId = p.id,
                                        BI_Permisos__c=permission,
                                        timezonesidkey=Label.BI_TimeZoneLA,
                                        username= string.valueof(nRandom)+'ur23@testorg.com',
                                        BI_CodigoUsuario__c = String.valueOf(nRandom),
                                        IsActive = true);
        return user;
        
    }
}