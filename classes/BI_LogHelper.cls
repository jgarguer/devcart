public without sharing class BI_LogHelper {
       /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for methods related to BI_Log__c 
    
    History:
    
    <Date>            <Author>          <Description>
    05/05/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


       /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that creates the BI_Log records with the given parameters
    
    IN:                String subject: Value for BI_Bloque_funcional__c 
                       String fBlock: Value for BI_Asunto__c
                       String description: Value for BI_Descripcion__c
                       String component: Value for BI_Tipo_de_componente__c         
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    05/05/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void generateLog(String subject, String fBlock, String description, String component){
             
             BI_Log__c log = new BI_Log__c(BI_Asunto__c = subject,
                                                                   BI_Bloque_funcional__c = fBlock,
                                                                   BI_Descripcion__c = description,
                                                                   BI_Tipo_de_componente__c = component);
             
             insert log;
              system.debug('LOG - '+log);
                    
       }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Generates the log in future

    History:     
    
    <Date>                  <Author>                <Change Description>
    28/11/2014              Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @future
    public static void generateLog_future(String subject, String fBlock, String description, String component, Datetime now){
    String final_desc = 'Future Log ['+ now.format() +'] - ' + description;
       generateLog(subject, fBlock, final_desc, component);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that creates the BI_Log records with the given parameters
    
    IN:            String subject: Value for BI_Bloque_funcional__c 
                   String fBlock: Value for BI_Asunto__c
                   Exception exc: Value for generate BI_Descripcion__c with exception data.
                   String component: Value for BI_Tipo_de_componente__c         
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    05/05/2014        Micah Burgos       Initial version
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Id generate_BILog(String subject, String fBlock, Exception exc, String component){
        

        BI_Log__c log = new BI_Log__c(BI_Asunto__c = subject,
                                        BI_Bloque_funcional__c = fBlock,
                                        BI_NumeroLinea__c = Decimal.valueOf(exc.getLineNumber()),
                                        BI_Descripcion__c = exc.getMessage(),
                                        BI_StackTrace__c = exc.getStackTraceString(),
                                        BI_Tipo_Error__c = exc.getTypeName(),
                                        BI_Tipo_de_componente__c = component);
        

        if(exc.getTypeName() == 'System.DmlException'){
            try{
                log = generate_BILog_DML(exc, log);
            }catch(Exception exc_getDML){
                log.BI_DML_Info__c =  exc_getDML.getMessage();
            }
        }
        System.debug('Vamos a ver si me deja desplegar');
        system.debug('GENERATE_LOG ==> '+log);

        insert log;
        
        return log.Id;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos  
    Company:       Aborda.es
    Description:   Method thats puts more info about DML exception on BI_Log__c record.

    History: 

    <Date>                  <Author>                <Change Description>
    21/04/2014              Micah Burgos              Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static BI_Log__c generate_BILog_DML(Exception exc_param, BI_Log__c log ){

        try{
            throw exc_param;//System exception
        }catch(DmlException exc){


            String aux_str = '';


            for (Integer i = 0; i < exc.getNumDml(); i++) {
                //Integer i = exc.getDmlIndex(0);
    
                //aux_str += 'DmlIndex(0) = ' + String.valueOf(i) + ' - ';
                aux_str += 'DmlFieldNames(i) = ' + JSON.serialize(exc.getDmlFieldNames(i)) + ' - ';
                //aux_str += 'DmlFields(i) = ' + exc.getDmlFields(i) + ' - '; //Not be a String 
                aux_str += 'DmlId(i) = ' + exc.getDmlId(i) + ' - ';
                aux_str += 'DmlMessage(i) = ' + exc.getDmlMessage(i) + ' - ';
                aux_str += 'DmlType(i) = ' + exc.getDmlType(i) + ' - // - ';
            }
    
                log.BI_DML_Info__c = (aux_str.length() > 32000 )?aux_str.substring(0,32000):aux_str;
    
                return log;
        }

    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Daniel Leal
    Company:       Salesforce.com
    Description:   Method that creates the BI_Log records with the given parameters
    
     
    History:
    
    <Date>            <Author>          <Description>
    08/11/2017        Daniel Leal       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   public static Id generate_BILog_Generic(String subject, String fBlock, Exception exc, String component, Object obj)
    {
    BI_Log__c log;
        System.debug('DANIEL:generate_BILog_Generic LINE155');
    if (obj instanceof Contract){
       System.debug('DANIEL:generate_BILog_Generic LINE157');
             Contract contr = (Contract) obj;
       System.debug('DANIEL:generate_BILog_Generic LINE159'+contr);     
            log = new BI_Log__c(
            BI_Asunto__c = subject,
            BI_Bloque_funcional__c = fBlock,
            BI_NumeroLinea__c = Decimal.valueOf(exc.getLineNumber()),
            BI_Descripcion__c = exc.getMessage(),
            BI_StackTrace__c = exc.getStackTraceString(),
            BI_Tipo_Error__c = exc.getTypeName(),
            BI_Tipo_de_componente__c = component,
            BI_COL_Contrato__c = contr.Id);
    
    } else if (obj instanceof Account){
       		System.debug('DANIEL:generate_BILog_Generic LINE171');
             Account acc = (Account) obj;
       		System.debug('DANIEL:generate_BILog_Generic LINE173'+acc);
            log = new BI_Log__c(
            BI_Asunto__c = subject,
            BI_Bloque_funcional__c = fBlock,
            BI_NumeroLinea__c = Decimal.valueOf(exc.getLineNumber()),
            BI_Descripcion__c = exc.getMessage(),
            BI_StackTrace__c = exc.getStackTraceString(),
            BI_Tipo_Error__c = exc.getTypeName(),
            BI_Tipo_de_componente__c = component,
            BI_COL_Cuenta__c = acc.Id);
            
    } else if (obj instanceof Contact){
       	 System.debug('DANIEL:generate_BILog_Generic LINE185');
           Contact cont = (Contact) obj;
         System.debug('DANIEL:generate_BILog_Generic LINE159'+cont);
            log = new BI_Log__c(
            BI_Asunto__c = subject,
            BI_Bloque_funcional__c = fBlock,
            BI_NumeroLinea__c = Decimal.valueOf(exc.getLineNumber()),
            BI_Descripcion__c = exc.getMessage(),
            BI_StackTrace__c = exc.getStackTraceString(),
            BI_Tipo_Error__c = exc.getTypeName(),
            BI_Tipo_de_componente__c = component,
            BI_COL_Contacto__c = cont.Id);
    }
    

        if(exc.getTypeName() == 'System.DmlException'){
            try{
                log = BI_LogHelper.generate_BILog_DML(exc, log);
            }catch(Exception exc_getDML){
                log.BI_DML_Info__c =  exc_getDML.getMessage();
            }
        }

        system.debug('GENERATE_LOG ==> '+log);

        insert log;
        
        return log.Id;
    }
}