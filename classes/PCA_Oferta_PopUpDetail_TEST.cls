@isTest
private class PCA_Oferta_PopUpDetail_TEST {
    /*---------------------------------------------------------------------------------------------------------------------------------
 	Author:        Ana Escrich
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for PCA_Oferta_PopUpDetail.loadInfo
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
    13/08/2014      		Ana Escrich	    		Initial Version
    02/10/2014				Ignacio Llorca			Finished Initial Version
    27/05/2017              Jesus Arcones Grande    Fix test error Too Many Queries
 	---------------------------------------------------------------------------------------------------------------------------------*/
 	static testMethod void PCA_Pedido_PopUpDetail_try_TEST() {

 		BI_TestUtils.throw_exception = false;

        // JAG 27/05/2017
        /*
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        */
        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){
            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;
            if(userTGS.Id != null){
                update userTGS;
            } else {
                insert userTGS;
            }
        }
        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);
        // Fin JAG 27/05/2017

		List<Account> accounts = BI_DataLoad.loadAccountsPaisStringRef(1, BI_DataLoad.loadPaisFromPickList(1));
		
        Opportunity opp = new Opportunity(Name = 'Test',
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                                          AccountId = accounts[0].Id,
                                          BI_Ciclo_ventas__c = Label.BI_Completo,
                                          BI_Opportunity_Type__c = 'Producto Standard',
                                          BI_Country__c = accounts[0].BI_Country__c);
	                                              
		insert opp;
			                                              
		NE__Order__c orderC = new NE__Order__c(NE__OptyId__c=opp.Id, 
											   NE__OrderStatus__c = Label.BI_LabelActive);
		insert orderC;
		
		NE__Product__c prod = new NE__Product__c();
        prod.Name = 'TestProd';
        insert prod;
        
        NE__Catalog_Header__c catH = new NE__Catalog_Header__c();
        catH.Name='t';
        catH.NE__Name__c ='t' ;
        insert catH;
        
        NE__Catalog__c cat = new NE__Catalog__c();
        cat.NE__Catalog_Header__c = catH.Id;
        insert cat;
        
        NE__Catalog_Item__c catItem = new NE__Catalog_Item__c();
        catItem.NE__ProductId__c = prod.Id;
        catItem.NE__Catalog_Id__c = cat.Id;
        insert catItem;        
        
        NE__OrderItem__c ordItem = new NE__OrderItem__c();
        ordItem.NE__OrderId__c = orderC.Id;
        ordItem.NE__CatalogItem__c = catItem.Id;
        ordItem.NE__Qty__c = 1;
        ordItem.NE__ProdId__c = prod.Id;
        insert ordItem;

        BI_MigrationHelper.disableBypass(mapa); // JAG 27/05/2017
        Test.startTest(); // JAG 27/05/2017

        opp.StageName = Label.BI_F5DefSolucion;
        update opp;
        orderC.NE__OrderStatus__c = Label.BI_LabelActive;
        update orderC;
			
    	PageReference pageRef = new PageReference('PCA_DetailOportunidad');
   		Test.setCurrentPage(pageRef);
   		ApexPages.currentPage().getParameters().put('Id', opp.Id);
   		 
    	PCA_Oferta_PopUpDetail controller = new PCA_Oferta_PopUpDetail();
    	controller.checkPermissions();    
    	controller.loadInfo();

        Test.stopTest(); // JAG 27/05/2017
 
        system.assert(controller.fieldSetRecords != null);
        system.assert(controller.viewRecords != null && !controller.viewRecords.isEmpty());
 	 
	}

    static testMethod void PCA_Pedido_PopUpDetail_catch_TEST() { 
        BI_TestUtils.throw_exception = true;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        PCA_Oferta_PopUpDetail controller = new PCA_Oferta_PopUpDetail();
        controller.checkPermissions();    
        controller.loadInfo();
    }

}