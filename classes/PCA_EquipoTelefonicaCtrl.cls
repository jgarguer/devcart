public class PCA_EquipoTelefonicaCtrl extends PCA_HomeController{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Class for show equipo telefonica 
    
    History:
    
    <Date>            <Author>              <Description>
    23/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public string ajaxParamValue {get; set;}
    
        
    //table contacts variables
    public String nameFS;
    public String nameObject;
    public Integer totalRegs    {get; set;}
    public String searchFinal;
    public Id AccountId;
    public String accountFieldName;
    public String firstHeader                   {get; set;}
    public List<FieldSetHelper.FieldSetRecord> viewRecords  {get; set;}
    public FieldSetHelper.FieldSetContainer fieldSetRecords {get; set;}
    public String searchField                   {get; set;}
    public String searchText                    {get; set;}
    //GMN 29/12/2015
    public String parentFieldName   {get;set;}
    public List<SelectOption> searchFields      {get; set;}
    public String fieldImage {get;set;}
    public String Mode {get;set;}
    
    public String FieldOrder {get;set;}
    public String Field;     
    public String PageValue {get;set;}
    
    public Integer index                        {get; set;}
    public Integer pageSize                     {get; set;}
    
    public class  hierarchyWrapper{
        public String UsernameId { get; set; }
        public String Username { get; set; }
        public String Description { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string phoneMobile { get; set; }
        public string photo { get; set; }
        public string Bigphoto { get; set; }
        public string NivelEscalado { get; set; }
        public boolean showPersonalData { get; set; }
        public string AboutMe { get; set; }
        public string Role { get; set; }
    }
    
    public class  detailsUserWrapper{
        public String UsernameId { get; set; }
        public String Username { get; set; }
        public String Description { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string phoneMobile { get; set; }
        public string photo { get; set; }
        public string Bigphoto { get; set; }
        public boolean showPersonalData { get; set; }
        public string AboutMe { get; set; }
        public string Role { get; set; }
    }
    
    /*public class soporteWrapper{
        public String Id { get; set; }
        public string Tipo { get; set; }
        public String Pais { get; set; }
        public string CorreoDelCallCenter { get; set; }
        public String CorreoDelGerente { get; set; }
        public string GerenteDelCallCenter { get; set; }
        public string Telefono { get; set; }
        public string TelefonoDelGerente { get; set; }
        public string photo { get; set; }
        public string Bigphoto { get; set; }
        public boolean showPersonalData { get; set; }
        public string Description { get; set; }
        public string Role { get; set; }
    }*/
    
    public list<hierarchyWrapper> listHierarchyWrapper {get; set;}
    public list<hierarchyWrapper> listNivelEscaladoWrapper {get; set;}
    //public list<soporteWrapper> listSoporteWrapper {get; set;}
    public detailsUserWrapper detailsUser {get; set;}
    
    public string emailTo {get; set;}
    public string emailFrom;
    public string emailPhone {get; set;}
    public string emailSubject {get; set;}
    public string emailBody {get; set;}
    public User user {get; set;}
        
    public boolean haveError {get; set;}
    
    public Boolean isTGS {get;set;}
    
    //public map<string,string> mapVisibility {get; private set;}
    
    
    
    
    
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Constructor 
    
    History:
    
    <Date>            <Author>              <Description>
    23/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_EquipoTelefonicaCtrl (){
        
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info of Equipo Telefonica & Escalamiento de Telefonica
    
    History:
    
    <Date>            <Author>              <Description>
    23/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfo (){
        
        List<User> UsersHierarchy = new List<User> ();
        
        list<Hierarchy__c> hierarchyList = new list<Hierarchy__c>(); 
        
        list<Account> accountOwner = new list<Account>(); 
        
        list<AccountTeamMember> listAccountTeam = new list<AccountTeamMember>();
        
        Id contactAccountId;
        
        Id idContactFinal;
        
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            //system.debug(getPhotoConnectApi(Userinfo.getUserId()));
            //ConnectApi.Photo photo = ConnectApi.ChatterUsers.getPhoto(null, usuarioPortal.id);
            //system.debug(photo.smallPhotoUrl);
        
        
            string AccountId = BI_AccountHelper.getCurrentAccountId();
    
            detailsUser = new detailsUserWrapper();
            user = [Select Id, Name, Profile.Name, ContactId,aboutme, Email,MobilePhone, phone, FullPhotoUrl, Contact.AccountId, UserRole.Name From User where id = : Userinfo.getUserId()];
            // Incluyo control isTGS para mostrar/ocultar botón "Change Request"
            if(user.Profile.Name.containsIgnoreCase('TGS')){
                isTGS = true;
            }else{
                isTGS = false;
            }
            
            list<BI_Contact_Customer_Portal__c> attorney = [SELECT Id, BI_Activo__c, BI_Contacto__c, BI_Perfil__c,BI_User__c, BI_Cliente__c FROM BI_Contact_Customer_Portal__c where BI_User__c =: Userinfo.getUserId() AND BI_Cliente__c=: AccountId];
            system.debug('*****attorney' +attorney);
            boolean isAttorney=false;
            
            if(!attorney.IsEmpty()){
                if(attorney[0].BI_Perfil__c==Label.BI_Apoderado){
                    isAttorney = true;      
                }       
                idContactFinal = attorney[0].BI_Contacto__c;
            }
                        
            if(user.ContactId != null){
                contactAccountId = AccountId;
                listHierarchyWrapper = new list<hierarchyWrapper>();
                
                system.debug('*****contactAccountId' +contactAccountId);
                system.debug('*****idContactFinal' +idContactFinal);

                //Add GAM Filter
                listAccountTeam = [Select UserId, User.Name,User.Phone,User.MobilePhone,User.Email, User.AboutMe,User.UserRole.Name, toLabel(TeamMemberRole), Id, AccountId, AccountAccessLevel 
                                    From AccountTeamMember
                                    where AccountId= : contactAccountId  AND (TeamMemberRole !='GAM' AND Account.RecordType.DeveloperName !='Legal Entity')];
                Account AccTeam = BI_AccountHelper.getCurrentAccount(BI_AccountHelper.getCurrentAccountId());
                accountOwner = [SELECT Owner.Id,Owner.Name,Owner.Email,Owner.phone,Owner.MobilePhone,Owner.AboutMe,Owner.Profile.Name,Owner.BI_Permisos__c FROM Account where Id = :BI_AccountHelper.getCurrentAccountId() AND (Owner.Profile.Name != 'BI_Inteligencia Comercial' OR Owner.BI_Permisos__c !=: Label.BI_Inteligencia_Comercial)];
                
                this.index = 1;
                this.pageSize = 10;
                this.searchFinal = null;        
                this.AccountId = AccTeam.Id;
                
                this.nameObject = 'Contact';
                this.nameFS = 'PCA_MainTableContacts';
                this.accountFieldName = 'AccountId';
                defineRecords();
                defineViews();
                
                //fin listado Contactos     
        
                if(!accountOwner.IsEmpty()){
                    for(Account g :accountOwner){
                        hierarchyWrapper hWrapper = new hierarchyWrapper();
                        hWrapper.Username = g.Owner.Name;
                        hWrapper.UsernameId = g.Owner.Id;
                        hWrapper.email = g.Owner.Email;
                        hWrapper.phone = g.Owner.phone;
                        hWrapper.phoneMobile = g.Owner.MobilePhone;
                        hWrapper.Role = Label.PCA_Propietario;
                        hWrapper.AboutMe = g.Owner.AboutMe !=null?g.Owner.AboutMe:Label.BI_SinDefinir;
                        if(getPhotoConnectApi(g.Owner.Id)!=null){
                            list<String> listAux = getPhotoConnectApi(g.Owner.Id); 
                            system.debug('listAux: ' +listAux);
                            if(!listAux.isEmpty()){
                                hWrapper.photo = listAux[0];
                                hWrapper.Bigphoto = listAux[1];
                            }
                        }
                        hWrapper.showPersonalData = true;
                        listHierarchyWrapper.add(hWrapper);
                    }
                }
                
                for(AccountTeamMember h : listAccountTeam){
                    hierarchyWrapper hWrapper = new hierarchyWrapper();
                    ConnectApi.UserDetail userDetail;
                    /*try{
                        userDetail = ConnectApi.ChatterUsers.getUser('0DBb00000008OTDGA2', h.UserId);
                    }catch(Exception Exc){
                        system.debug(Exc);
                    }*/
        
        
                    hWrapper.Username = h.User.Name;
                    hWrapper.UsernameId = h.UserId;
                    
                    hWrapper.email = h.User.Email;
                    hWrapper.phone = h.User.phone;
                    hWrapper.phoneMobile = h.User.MobilePhone;
                    hWrapper.Role = h.TeamMemberRole;
                    hWrapper.AboutMe = h.User.AboutMe !=null?h.User.AboutMe:'sin definir';
                    if(getPhotoConnectApi(h.UserId)!=null){
                        list<String> listAux = getPhotoConnectApi(h.UserId);
                        //system.debug('listAux: ' +listAux);
                        if(!listAux.isEmpty()){
                            hWrapper.photo = listAux[0];
                            hWrapper.Bigphoto = listAux[1];
                        }
                    }
                    hWrapper.showPersonalData = true;
                    listHierarchyWrapper.add(hWrapper);
                    
                }
                system.debug(listHierarchyWrapper);
                
                hierarchyList = [Select Id, AboutMe__c, MobilePhone__c, ShowPersonalData__c,  Description__c, User__c, NameUser__c, Contact__c, 
                                    Email__c, Phone__c, Nivel_de_escalado__c, Role__c From Hierarchy__c where Contact__c = : idContactFinal and Nivel_de_escalado__c != null and Nivel_de_escalado__c != '' order by Nivel_de_escalado__c];
                List<Id> IdsUsers = new List<Id>();
                if(!hierarchyList.isEmpty()){
                    for(Hierarchy__c h :hierarchyList){
                        IdsUsers.add(h.User__c);                
                    }
                    UsersHierarchy = [Select Id, Email, Phone, MobilePhone From User Where Id IN :IdsUsers];
                }
                
                
                listNivelEscaladoWrapper = new list<hierarchyWrapper>();
                if(!hierarchyList.isEmpty()){       
                    for(Hierarchy__c h : hierarchyList){
                        for(User user :UsersHierarchy){
                            if(user.Id == h.User__c){
                                //getUserConnectApi(h.User__c);
                                hierarchyWrapper hWrapper = new hierarchyWrapper();
                                
                                hWrapper.Username = h.NameUser__c;
                                hWrapper.UsernameId = h.User__c;
                                
                                hWrapper.Description = h.Role__c!=null ?h.Role__c:Label.BI_PorDefinir;
                                if(h.ShowPersonalData__c==false){
                                    hWrapper.email = Label.PCA_NoDisponible;
                                    hWrapper.phone = Label.PCA_NoDisponible;
                                    hWrapper.phoneMobile = Label.PCA_NoDisponible;
                                }
                                else{
                                    hWrapper.email = user.Email;
                                    hWrapper.phoneMobile = user.MobilePhone;
                                    hWrapper.phone = user.Phone;
                                }
                                hWrapper.Role = h.Role__c;
                                //hWrapper.Description = h.Description__c!=null ?h.Description__c:'por definir';
                                hWrapper.AboutMe = h.AboutMe__c;
                                if(getPhotoConnectApi(h.User__c)!=null){
                                    list<String> listAux = getPhotoConnectApi(h.User__c);
                                    system.debug('listAux: ' +listAux);
                                    if(!listAux.isEmpty()){
                                        hWrapper.photo = listAux[0];
                                        hWrapper.Bigphoto = listAux[1];
                                    }
                                }
                                hWrapper.NivelEscalado = h.Nivel_de_escalado__c;
                                listNivelEscaladoWrapper.add(hWrapper);
                            }
                        }
                    }       
                }
                
                
                /*listSoporteWrapper = new list<soporteWrapper>();
                set<string> setSuport = new set<string>();
                list<ListContactSuport__c> listContactSuport = [Select Id, Support__c From ListContactSuport__c where Contact__c = : idContactFinal];
                for(ListContactSuport__c item: listContactSuport){
                    setSuport.add(item.Support__c);
                }
                if(!setSuport.isEmpty()){
                    list<Suport__c> listSuport = [Select Id, Name, Correo_del_Call_Center__c, Correo_del_Gerente__c, Gerente_del_Call_Center__c, Pais__c,
                                Photo__c, Telefono__c, Telefono_del_Gerente__c  From Suport__c where Id IN : setSuport];
                    for(Suport__c item: listSuport){
                        soporteWrapper soporteWrapperAux = new soporteWrapper();
                        soporteWrapperAux.Id = item.Id;
                        soporteWrapperAux.Tipo = item.Name;
                        soporteWrapperAux.Pais = item.Pais__c;
                        soporteWrapperAux.CorreoDelCallCenter = item.Correo_del_Call_Center__c;
                        soporteWrapperAux.CorreoDelGerente = item.Correo_del_Gerente__c;
                        soporteWrapperAux.GerenteDelCallCenter = item.Gerente_del_Call_Center__c;
                        soporteWrapperAux.Telefono = item.Telefono__c;
                        soporteWrapperAux.TelefonoDelGerente = item.Telefono_del_Gerente__c;
                        soporteWrapperAux.photo = item.Photo__c;
                        soporteWrapperAux.Description = 'Soporte';
                        soporteWrapperAux.showPersonalData = false;
                        listSoporteWrapper.add(soporteWrapperAux);
                    }
                }*/
                
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.loadInfo', 'Portal Platino', Exc, 'Class');
        }
    }


    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines records to show
    
    History:
    
    <Date>            <Author>              <Description>
    23/06/2014        Antonio Moruno       Initial version
    29/12/2015        Guillermo Muñoz      Filter added to serach only active Contacts
    10/07/2017        Daniel Guzman        Filter added control field CWP_Contact_hidden__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void defineRecords()
    {
        try{
            //Add Control Field CWP_Contact_hidden__c
            String condition = this.accountFieldName + ' = \'' + this.AccountId + '\' AND BI_Activo__c = true AND CWP_contact_hidden__c = false';
            
        
            this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearchWithCondition(this.nameFS, this.nameObject, condition, null);
            
            if (this.fieldSetRecords.regs.size() > 0)
            {
                this.firstHeader = this.fieldSetRecords.regs[0].values[0].field;
            }
            
            this.index = 1; 
            this.totalRegs = this.fieldSetRecords.regs.size();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.defineRecords', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines views to show
    
    History:
    
    <Date>            <Author>              <Description>
    23/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void defineViews()
    {
        try{
            this.viewRecords = new List<FieldSetHelper.FieldSetRecord>();
            Integer topValue = ((this.index + this.pageSize) > this.totalRegs) ? this.totalRegs : (this.index + this.pageSize - 1);
            
            for (Integer i = (this.index - 1); i < topValue; i++)
            {
                FieldSetHelper.FieldSetRecord iRecord = this.fieldSetRecords.regs[i]; 
                this.viewRecords.add(iRecord);
            }
            system.debug('Records: ' +this.viewRecords);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.defineViews', 'Portal Platino', Exc, 'Class');
        }
            
    }
    
    
    /*public void getUserConnectApi (string id){
        //system.debug(ConnectApi.ChatterUsers.getUser(null, Id));
        try{        
            ConnectApi.UserDetail test = ConnectApi.ChatterUsers.getUser(null, Id);
            //ConnectApi.?User?.Detail  UserDet = ConnectApi.ChatterUsers.getUser(null, Id);
            //system.debug(UserSum);
        }catch(Exception Exc){
            BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.getUserConnectApi', 'Portal Platino', Exc, 'Class');
        }
        
    }*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Retrieves chatter photo of a user given id
    
    History:
    
    <Date>            <Author>              <Description>
    23/06/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public list<string> getPhotoConnectApi (string id){
        try{
            system.debug('Network' +Network.getNetworkId());
            system.debug('Id User' +Id);
            ConnectApi.Photo photo; 
                
                photo = ConnectApi.ChatterUsers.getPhoto(null, Id);
                system.debug(photo.smallPhotoUrl);
            
            system.debug(photo);
            list<string> listString = new list<String>();
            if(photo!=null){
                listString.add(photo.smallPhotoUrl);
                listString.add(photo.largePhotoUrl);
            }
            system.debug(listString);
            //return photo.smallPhotoUrl;
            //return photo.largePhotoUrl;
            return listString;
        }catch(Exception Exc){
            BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.getPhotoConnectApi', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Return the visibility of the subsection EquipoTelefonica
    
    History:
    
    <Date>            <Author>              <Description>
    14/08/2014        Micah Burgos          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public boolean getPermissionTo_EquipoTelefonica (){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            return PCA_ProfileHelper.checkPermissionSet('subsection_EquipoTelefonica');
                
        }catch(Exception Exc){
            BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.getPermissionTo_EquipoTelefonica', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }   
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Return the visibility of the subsection Escalamiento
    
    History:
    
    <Date>            <Author>              <Description>
    14/08/2014        Micah Burgos          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public boolean getPermissionTo_Escalamiento (){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            return PCA_ProfileHelper.checkPermissionSet('subsection_Escalamiento');
                
        }catch(Exception Exc){
            BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.getPermissionTo_Escalamiento', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }   
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Return the visibility of the subsection EquipoCliente
    
    History:
    
    <Date>            <Author>              <Description>
    14/08/2014        Micah Burgos          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public boolean getPermissionTo_EquipoCliente (){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            return PCA_ProfileHelper.checkPermissionSet('subsection_EquipoCliente');
                
        }catch(Exception Exc){
            BI_LogHelper.generate_BILog('PCA_EquipoTelefonicaCtrl.getPermissionTo_EquipoCliente', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines search criteria
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void defineSearch(){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
        this.searchFields = FieldSetHelper.defineSearchFields(this.nameFS, this.nameObject);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.defineSearch', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines search 
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void searchRecords(){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            this.searchFinal = this.searchField + ';' + this.searchText;
            defineRecords();
            defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.searchRecords', 'Portal Platino', Exc, 'Class');
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Retrieves true if index of search is not the first one, false otherwise
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Boolean getHasPrevious(){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            return (this.index != 1);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.getHasPrevious', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Retrieves true if index of search is not the last one, false otherwise
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Boolean getHasNext(){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            return !((this.index + this.pageSize) > this.totalRegs);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.getHasNext', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Adds a position to the index of search pages
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void Next(){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            system.debug('pageSize' +pageSize);
            this.index += this.pageSize;
            
            ValuesInPage();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.Next', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Decreases a position to the index of search pages
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void Previous(){
        try{
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            this.pageSize = 10;
            this.index -= this.pageSize;
            defineViews();  
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.Previous', 'Portal Platino', Exc, 'Class');
        }
    }   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Defines records in page
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/      
    public List<SelectOption> getItemPage() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('25','25'));
        options.add(new SelectOption('50','50'));
        options.add(new SelectOption('100','100'));
        options.add(new SelectOption('200','200'));
        system.debug('itemPage: '+ options);
        return options;
    }       
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Show more values in a page
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void valuesInPage(){
        try{
        this.pageSize = Integer.valueOf(PageValue);
        system.debug('pageSize*: '+this.pageSize);
        defineViews();
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_FacturacionCobranzaCtrl.valuesInPage', 'Portal Platino', Exc, 'Class');
        }
    }       
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Order table with field
    
    History:
    
    <Date>            <Author>              <Description>
    17/09/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public void Order(){
        
        try{/*
            this.searchText = null;
            this.searchFinal = null;
            this.OptionA = this.OptionA ? false : true;
            this.nameFS = (this.nameFS == this.nameFS1) ? this.nameFS2 : this.nameFS1;
            this.nameObject = (this.nameObject == this.nameObject1) ? this.nameObject2 : this.nameObject1;
            this.accountFieldName = (this.accountFieldName == this.accountFieldName1) ? this.accountFieldName2 : this.accountFieldName1;
    */          
            if(BI_TestUtils.isRunningTest())
                throw new BI_Exception('test');
            Mode = '';
            fieldImage = FieldOrder;
            String accountValue = this.accountFieldName + ';' + this.AccountId;
            //if(this.nameObject == this.nameObject2){
            //this.searchFinal = '::NE__OrderId__r.RecordType.DeveloperName;Asset;'+FieldOrder;
            //}
            if(Field == FieldOrder){
                this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.FieldOrder, accountValue,  'DESC');
                FieldOrder = '';
                Mode = 'DESC';
            }else{
                this.fieldSetRecords = FieldSetHelper.FieldSetHelperSearch(this.nameFS, this.nameObject, this.FieldOrder, accountValue,  'ASC');
                Mode = 'ASC';
            }
            if (this.fieldSetRecords.regs.size() > 0)
            {
                this.firstHeader = this.fieldSetRecords.regs[0].values[0].field;
            }
            
            this.index = 1; 
            this.totalRegs = this.fieldSetRecords.regs.size();
            
            Field = this.FieldOrder;
            defineViews();
            
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_ReclamosPedidosCtrl.Order','Portal Platino', Exc, 'Class');
        }
        
    }           
    
    
}