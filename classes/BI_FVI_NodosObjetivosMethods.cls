public with sharing class BI_FVI_NodosObjetivosMethods {
    
    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    static
    {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'BI_FVI_Nodos_Objetivos__c'])
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer García             
    Company:       NEAborda
    Description:   Método para crear registros en el objeto BI_FVI_Nodo_Objetivo_Desempeno__c

    History: 

    <Date>                  <Author>                <Change Description>
    10/05/2016              Antonio Masferrer       Initial Version   
    22/09/2017              Humberto Nunes          Contempla Nodo o Cliente...  
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public static void generaNodo_Objetivo_Desempeno(List<BI_FVI_Nodos_Objetivos__c> tnew , List<BI_FVI_Nodos_Objetivos__c> told){
        

        try{

            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

            Set<Id> set_nodoObjetivo = new Set<Id>();

            for(BI_FVI_Nodos_Objetivos__c no: tnew){
                set_nodoObjetivo.add(no.Id);
            }
            system.debug('[[. NodosObjetivos:' + set_nodoObjetivo);

            List<BI_FVI_Nodos_Objetivos__c> lst_tnew = [SELECT Id, 
                                                               BI_FVI_Id_Nodo__c, 
                                                               BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c, 
                                                               BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c, 
                                                               BI_FVI_Objetivo_Comercial__r.BI_Tipo__c 
                                                        FROM BI_FVI_Nodos_Objetivos__c 
                                                        WHERE Id IN: set_nodoObjetivo];

            system.debug('[[. lst_tnew ' + lst_tnew);

            Date max_date = null;
            Date min_date = null;

            Set<Id> set_nodos = new Set<Id>();
            for(BI_FVI_Nodos_Objetivos__c no : lst_tnew)
            {
                if (no.BI_FVI_Id_Nodo__c!=null) 
                    set_nodos.add(no.BI_FVI_Id_Nodo__c);

                if(max_date == null){
                    min_date = no.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c;
                    max_date = no.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c;
                }else{
                    if(no.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c > max_date){
                        max_date = no.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c;
                    }

                    if(no.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c < min_date){
                        min_date = no.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c;
                    }
                }
            }

            system.debug('[[. max_date ' + max_date  + ' min_date ' + min_date);


            List<BI_Registro_Datos_Desempeno__c> lst_rdd = [SELECT Id,
                                                                   BI_FVI_Id_Cliente__c,
                                                                   BI_FVI_Fecha__c,
                                                                   BI_FVI_Valor__c,
                                                                   BI_Tipo__c
                                                            FROM BI_Registro_Datos_Desempeno__c 
                                                            WHERE BI_FVI_Id_Cliente__c 
                                                            IN (SELECT BI_FVI_IdCliente__c FROM BI_FVI_Nodo_Cliente__c WHERE BI_FVI_IdNodo__c IN: set_nodos OR BI_FVI_IdNodo__r.BI_FVI_NodoPadre__c IN: set_nodos) 
                                                            AND BI_FVI_Fecha__c >=: min_date
                                                            AND BI_FVI_Fecha__c <=: max_date];


            Set<Id> set_IdCliente = new Set<Id>();
            for(BI_Registro_Datos_Desempeno__c rdd: lst_rdd)
                if (rdd.BI_FVI_Id_Cliente__c!=null) 
                    set_IdCliente.add(rdd.BI_FVI_Id_Cliente__c);

            List<BI_FVI_Nodo_Cliente__c> lst_nc = [SELECT 	BI_FVI_IdCliente__c, 
                                                   			BI_FVI_IdNodo__c, 
                                                   			BI_FVI_IdNodo__r.BI_FVI_NodoPadre__c,
                                                   			BI_FVI_IdNodo__r.BI_FVI_NodoPadre__r.RecordType.DeveloperName 
                                                   			FROM BI_FVI_Nodo_Cliente__c WHERE BI_FVI_IdCliente__c IN: set_IdCliente AND (BI_FVI_IdNodo__c IN: set_nodos OR BI_FVI_IdNodo__r.BI_FVI_NodoPadre__c IN: set_nodos)];

            // system.debug('[[. lst_nc ' + lst_nc);
            
            List<BI_FVI_Nodo_Objetivo_Desempeno__c> lst_nod = new List<BI_FVI_Nodo_Objetivo_Desempeno__c>();

            
            for(BI_Registro_Datos_Desempeno__c rdd: lst_rdd)
            {
                for(BI_FVI_Nodos_Objetivos__c no : lst_tnew)
                {
                    if (rdd.BI_FVI_Id_Cliente__c!=null) // DESEMPEÑO RELACIONADO CON CUENTA
                    {
                        for(BI_FVI_Nodo_Cliente__c nc : lst_nc)
                        {                                               
                            if  (                        
                                    nc.BI_FVI_IdCliente__c == rdd.BI_FVI_Id_Cliente__c &&
                                    no.BI_FVI_Objetivo_Comercial__r.BI_Tipo__c == rdd.BI_Tipo__c && 
                                    no.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c >= rdd.BI_FVI_Fecha__c &&
                                    no.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c <= rdd.BI_FVI_Fecha__c &&
                                    (nc.BI_FVI_IdNodo__c == no.BI_FVI_Id_Nodo__c || nc.BI_FVI_IdNodo__r.BI_FVI_NodoPadre__r.RecordType.DeveloperName == 'BI_FVI_NCP')
                                )
                            {

                                BI_FVI_Nodo_Objetivo_Desempeno__c nod = new BI_FVI_Nodo_Objetivo_Desempeno__c();
                                nod.BI_FVI_Nodo_Objetivo__c = no.Id;
                                nod.BI_FVI_Registro_Datos_Desempeno__c = rdd.Id;
                                nod.BI_FVI_Valor__c = rdd.BI_FVI_Valor__c;

                                lst_nod.add(nod);                        
                            }                   
                        }
                    }
                    else // DESEMPEÑO RELACIONADO CON NODO... 
                    {
                         if  (                        
                                no.BI_FVI_Objetivo_Comercial__r.BI_Tipo__c == rdd.BI_Tipo__c && 
                                no.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Fin__c >= rdd.BI_FVI_Fecha__c &&
                                no.BI_FVI_IdPeriodo__r.BI_FVI_Fecha_Inicio__c <= rdd.BI_FVI_Fecha__c &&
                                rdd.BI_FVI_Id_Nodo__c == no.BI_FVI_Id_Nodo__c
                            )
                        {

                            BI_FVI_Nodo_Objetivo_Desempeno__c nod = new BI_FVI_Nodo_Objetivo_Desempeno__c();
                            nod.BI_FVI_Nodo_Objetivo__c = no.Id;
                            nod.BI_FVI_Registro_Datos_Desempeno__c = rdd.Id;
                            nod.BI_FVI_Valor__c = rdd.BI_FVI_Valor__c;

                            lst_nod.add(nod);                        
                        }                                   
                    }
                }
            }

            if(!lst_nod.isEmpty()) insert lst_nod;
            
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_FVI_NodosObjetivosMethods.generaNodo_Objetivo_Desempeno', 'BI_FVI', Exc, 'Trigger');
        }
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer García             
    Company:       NEAborda
    Description:   

    History: 

    <Date>                  <Author>                <Change Description>
    10/05/2016              Antonio Masferrer       Initial Version     
    31/01/2018              Humberto Nunes          Se diferencia entre Tipo de Registro de NodoObjetivo Para Rellenar Nodos o Usuarios Jerarquicos
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public static void asignaNiveles(List<BI_FVI_Nodos_Objetivos__c> tnew , List<BI_FVI_Nodos_Objetivos__c> told){
        
        try{

            if(BI_TestUtils.isRunningTest()) throw new BI_Exception('test');

            Map<BI_FVI_Nodos_Objetivos__c,Id> mapa_no_id = new Map<BI_FVI_Nodos_Objetivos__c,Id>();
            Map<BI_FVI_Nodos_Objetivos__c,Id> mapa_no_idUsr = new Map<BI_FVI_Nodos_Objetivos__c,Id>();

            for(BI_FVI_Nodos_Objetivos__c no: tnew)
            {
                if (no.RecordTypeId == MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Nodos'))
                    mapa_no_id.put(no, no.BI_FVI_Id_Nodo__c);

                if (no.RecordTypeId == MAP_NAME_RT.get('BI_OBJ_Nodo_Objetivos_Jerarquicos'))
                    mapa_no_idUsr.put(no, no.BI_FVI_Id_Nodo__c);
            }
            
            List<BI_FVI_Nodos__c> lst_nodos =[SELECT 
                                            Id,
                                            BI_FVI_NodoPadre__c,
                                            BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__c,
                                            BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__c,
                                            BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__c,
                                            BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__c
                                        FROM BI_FVI_Nodos__c
                                        WHERE BI_FVI_NodoPadre__c != null
                                        AND Id IN: mapa_no_id.values()];

 
            Map<BI_FVI_Nodos_Objetivos__c , List<Id>> mapa_no_lst = new Map<BI_FVI_Nodos_Objetivos__c , List<Id>>();

            for(BI_FVI_Nodos_Objetivos__c no : mapa_no_id.keySet()){
                
                List<Id> lst_padres = new List<Id>();
                
                for(BI_FVI_Nodos__c nodo: lst_nodos){
                    
                    if(mapa_no_id.get(no) == nodo.Id){

                        if(nodo.BI_FVI_NodoPadre__c != null){
                            lst_padres.add(nodo.BI_FVI_NodoPadre__c);
                        }
                        if(nodo.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__c != null){
                            lst_padres.add(nodo.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__c);
                        }
                        if(nodo.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__c != null){
                            lst_padres.add(nodo.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__c);
                        }
                        if(nodo.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__c != null){
                            lst_padres.add(nodo.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__c);
                        }
                        if(nodo.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__c != null){
                            lst_padres.add(nodo.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__r.BI_FVI_NodoPadre__c);
                        }
                        if(!lst_padres.isEmpty()){
                            mapa_no_lst.put(no,lst_padres);
                        }
                    }
                }
            }

            for(BI_FVI_Nodos_Objetivos__c no : mapa_no_lst.keySet()){
                
                Integer maxPadres = mapa_no_lst.get(no).size();
                List<Id> lst_id = mapa_no_lst.get(no);


                if(maxPadres == 1){
                    no.BI_FVI_Nivel_1__c = lst_id[0];
                }else if(maxPadres == 2){
                    no.BI_FVI_Nivel_1__c = lst_id[0];
                    no.BI_FVI_Nivel_2__c = lst_id[1];
                }else if(maxPadres == 3){
                    no.BI_FVI_Nivel_1__c = lst_id[0];
                    no.BI_FVI_Nivel_2__c = lst_id[1];
                    no.BI_FVI_Nivel_3__c = lst_id[2];
                }else if(maxPadres == 4){
                    no.BI_FVI_Nivel_1__c = lst_id[0];
                    no.BI_FVI_Nivel_2__c = lst_id[1];
                    no.BI_FVI_Nivel_3__c = lst_id[2];
                    no.BI_FVI_Nivel_4__c = lst_id[3];
                }else{
                    no.BI_FVI_Nivel_1__c = lst_id[0];
                    no.BI_FVI_Nivel_2__c = lst_id[1];
                    no.BI_FVI_Nivel_3__c = lst_id[2];
                    no.BI_FVI_Nivel_4__c = lst_id[3];
                    no.BI_FVI_Nivel_5__c = lst_id[4];
                }
            }


            // ASIGNACION DE JERARQUIAS
            system.debug('mapa_no_idUsr' + mapa_no_idUsr);

            // SE OBTIENEN TODOS LOS ROLES
            Map<Id,UserRole> mapa_id_ur = new Map<Id,UserRole>([Select Id, ParentRoleId, ForecastUserId from UserRole]);
            system.debug('mapa_id_ur' + mapa_id_ur);
 
            // OBTIENTE LOS ROLES DE LOS PROPIETARIOS DE LOS NODOS RELACIONADOS
            Map<ID, ID> mapa_nodo_role = new Map<ID, ID>();

            List<BI_FVI_Nodos__c> lst_nodo_role = [SELECT Id, Owner.UserRoleId FROM BI_FVI_Nodos__c WHERE Id IN: mapa_no_idUsr.values()];
            system.debug('lst_nodo_role' + lst_nodo_role);

            for(BI_FVI_Nodos__c nodo: lst_nodo_role)
                mapa_nodo_role.put(nodo.Id, nodo.Owner.UserRoleId);
            
            system.debug('mapa_nodo_role' + mapa_nodo_role);
          
            // SETEA LOS USUARIOS EN LAS 5 JERARQUIAS PADRES           
            for(BI_FVI_Nodos_Objetivos__c no : mapa_no_idUsr.keySet())
            {
                ID urId = mapa_nodo_role.get(no.BI_FVI_Id_Nodo__c);
                UserRole ur = mapa_id_ur.get(urId);
                ur = mapa_id_ur.get(ur.ParentRoleId);
                no.BI_OBJ_Nivel_Jerarquico_1__c = ur.ForecastUserId;
                ur = mapa_id_ur.get(ur.ParentRoleId);
                no.BI_OBJ_Nivel_Jerarquico_2__c = ur.ForecastUserId;
                ur = mapa_id_ur.get(ur.ParentRoleId);
                no.BI_OBJ_Nivel_Jerarquico_3__c = ur.ForecastUserId;
                ur = mapa_id_ur.get(ur.ParentRoleId);
                no.BI_OBJ_Nivel_Jerarquico_4__c = ur.ForecastUserId;
                ur = mapa_id_ur.get(ur.ParentRoleId);
                no.BI_OBJ_Nivel_Jerarquico_5__c = ur.ForecastUserId;
            }
           

        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_FVI_NodosObjetivosMethods.asignaNiveles', 'BI_FVI', Exc, 'Trigger');
        }
        
    }
}