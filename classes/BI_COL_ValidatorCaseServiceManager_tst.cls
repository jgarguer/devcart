/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for batch BI_COL_ValidatorCaseServiceManager_cls
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-08-24      Raul Mora (RM)                  Create Class  
*            1.1    2017-02-01      Pedro Párraga                   Increase in coverage
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
*************************************************************************************/
@isTest
private class BI_COL_ValidatorCaseServiceManager_tst 
{
    public static Case objCase;
    public static list <UserRole> lstRoles;
    public static User objUsuario = new User();
    
    public static void createData()
    {
        Id idProfile = BI_DataLoad.searchAdminProfile();
        User usu = BI_DataLoad.createRandomUserCOL(idProfile, BI_DataLoad.searchUserRole());
        System.runAs(usu){
            BI_bypass__c objBibypass = new BI_bypass__c();
            objBibypass.BI_migration__c=true;
            insert objBibypass;
            
            Account objAcc = new Account();
            objAcc.Name = 'Test Account';
            objAcc.BI_Country__c = 'Colombia';
            objAcc.TGS_Region__c = 'América';
            objAcc.BI_Tipo_de_identificador_fiscal__c = 'CC';
            objAcc.BI_No_Identificador_fiscal__c = '1234567787';
            objAcc.BI_Activo__c = 'Si';
            insert objAcc;  

            //Contactos
            Contact objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
            objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objAcc.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
            //REING-INI
            objContacto.MobilePhone          				= '1236547890';
            objContacto.Phone						= '11111111';
            objContacto.Email						= 'test@test.com';		
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
            objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
            //REING_FIN
            Insert objContacto; 
            
            objCase = new Case();         
            objCase.AccountId= objAcc.Id;  
            objCase.Status= 'Assigned';
            objCase.BI_Country__c= 'Colombia';
            objCase.Type = 'Consulta';
            objCase.BI_Categoria_del_caso__c = 'Nivel 1';
            objCase.Priority= 'Media';
            objCase.Origin= 'Call center';
            objCase.Subject= 'Petición';
            objCase.BI_COL_Promesa_Cliente_dias_calendario__c = '1';
            objCase.BI_COL_Fecha_Radicacion__c = System.now();
            objCase.BI_COL_Fecha_proyeccion_entrega__c = System.today().addDays( 2 );
            objCase.contactid=objContacto.Id;
            insert objCase;

            objCase=[select id,AccountId,Status,BI_Country__c,Type,BI_Categoria_del_caso__c,Priority,Origin,Subject,BI_COL_Promesa_Cliente_dias_calendario__c,BI_COL_Fecha_Radicacion__c,BI_COL_Fecha_proyeccion_entrega__c,BI_COL_CasoPM__c,contactid,OwnerId from case where id=: objCase.id];
            AccountTeamMember objAccTeamMemb = new AccountTeamMember();
            objAccTeamMemb.AccountId = objAcc.Id;
            objAccTeamMemb.TeamMemberRole ='Service Manager';
            objAccTeamMemb.UserId = UserInfo.getUserId();
            insert objAccTeamMemb;
        }
    }
    
    static testMethod void myUnitTest() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            createData();
            
            List<Profile> lstProfile = [ SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1 ];
            List<User> lstUser = [ Select Id FROM User where Pais__c = 'Colombia' AND ProfileId =: lstProfile[0].Id And isActive = true Limit 1 ];
                System.debug( '\n\n -.-.-. '+objCase.BI_COL_CasoPM__c );
                Test.startTest();
                    BI_COL_ValidatorCaseServiceManager_cls.fnValidate( objCase );
                Test.stopTest();
        }
    }
}