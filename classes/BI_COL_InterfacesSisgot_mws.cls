/**
* Avanxo Colombia
* @author           Dolly Fierro href=<dfierro@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Test Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-08-12      Dolly Fierro (DF)     					New Test Class 
*************************************************************************************************************/
@isTest
global class BI_COL_InterfacesSisgot_mws implements WebServiceMock
{
	global Map<String, Object> mapWSResponseBySOAPAction         = new Map<String, Object>();

    global void doInvoke( 	Object stub, 
    						Object request, 
    						Map<String, Object> response, 
    						String endpoint, 
    						String soapAction, 
    						String requestName, 
    						String responseNS, 
    						String responseName, 
    						String responseType 
    					)
    {

	    mapWSResponseBySOAPAction.put( 'EnviarAplazamiento', 'String de respuesta' );
	    mapWSResponseBySOAPAction.put( 'ConsultarSegmentoUsuario', 'Segunda respuesta' );
	    
	    response.put( 'response_x', mapWSResponseBySOAPAction.get( soapAction ) );
   }
}