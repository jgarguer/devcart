/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Julio Asenjo
Company:       Everis
Description:   Class for CWP_Footer
Test Class:    

History:

<Date>              <Author>                   <Change Description>
24/07/2017          Julio Asenjo            Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/


public without sharing class  CWP_FooterController {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García

Company:       everis

Description:   method invoked to load a filter to the excel modal
History:
<Date>                          <Author>                                <Code>              <Change Description>
20/03/2017                      Julio Alberto Asenjo García                                 Initial version
01/03/2018						Antonio Pardo Corral					46-50				Added condition to show the new footer only to the profiles that start with 'TGS' except 'TGS Carrier Customer Community'
*/    
    @AuraEnabled
    public static String getFooter() {
        LightningContextFooter context = new LightningContextFooter();
        
        context.footerLinks= new List<BI_Multimarca_PP__c> ();
        context.socialNetworks = new List<BI_Multimarca_PP__c> ();
        
        Map<String, Object> map_return = new Map<String, Object>();
        
        User UserActual = [Select Name, Contact.CWP_First_login_CWP__c, ContactId,Contact.BI_Cuenta_activa_en_portal__c, AccountId FROM User where Id =:UserInfo.getUserId()];
        
        Account segAccount = [Select Id, Name,BI_Country__c, BI_Segment__c, CWP_OneDriveAddress__c FROM Account Where Id=:UserActual.Contact.BI_Cuenta_activa_en_portal__c];
        
        context.segmento=segAccount.BI_Segment__c;
        System.debug('la cuenta es: \n \t\t\t\t\t\t\t segAccount '+ segAccount.Name+'\n \t\t\t\t\t\t\t BI_Country__c:'+segAccount.BI_Country__c+'\n \t\t\t\t\t\t\t BI_Segment__c:'+segAccount.BI_Segment__c+'\n ');
        
        /*Calculamos si es TGS */
        Boolean esTGS=false; 
        Id pId = UserInfo.getProfileId();
        Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
        if (p.size()>0){
            String pName = p[0].Name;
            if(pName=='TGS Carrier Customer Community'){
                esTGS = false;
            } else{
                esTGS= pName.startsWith('TGS');
            }
        }else 
            esTGS= false;
        
        map_return.put('esTGS', esTGS);
        map_return.put('firstLogin', UserActual.Contact.CWP_First_login_CWP__c);
        map_return.put('userLang', UserInfo.getLanguage());
        if(UserActual.Contact.CWP_First_login_CWP__c == false){
            UserActual.Contact.CWP_First_login_CWP__c = true;
            Update UserActual.Contact;
        }
        
        
        if(!esTGS){
            if(segAccount.BI_Segment__c != null){
                context.footerLinks = [SELECT Name, BI_Country__c, BI_Segment__c, BI_Posicion__c, BI_URL__c,BI_Activo__c FROM BI_Multimarca_PP__c WHERE BI_Activo__c=true AND BI_Segment__c =: segAccount.BI_Segment__c AND BI_Country__c =:segAccount.BI_Country__c AND RecordType.DeveloperName ='BI_Pie_de_pagina' order by BI_Posicion__c];
                context.socialNetworks = [SELECT Name, BI_Country__c, BI_Segment__c, BI_Posicion__c, BI_URL__c,BI_Activo__c,BI_Facebook__c,BI_Google__c,BI_Instagram__c,BI_Tuenti__c,BI_Twitter__c,BI_Yammer__c FROM BI_Multimarca_PP__c WHERE BI_Activo__c=true AND BI_Segment__c =: segAccount.BI_Segment__c AND BI_Country__c =:segAccount.BI_Country__c AND RecordType.DeveloperName='BI_Redes_sociales'];
            }else{  
                context.footerLinks = [SELECT Name, BI_Country__c, BI_Segment__c, BI_Posicion__c, BI_URL__c,BI_Activo__c FROM BI_Multimarca_PP__c WHERE BI_Activo__c=true AND BI_Segment__c = 'Empresas' AND BI_Country__c =:segAccount.BI_Country__c AND RecordType.DeveloperName='BI_Pie_de_pagina' order by BI_Posicion__c];
                context.socialNetworks = [SELECT Name, BI_Country__c, BI_Segment__c, BI_Posicion__c, BI_URL__c,BI_Activo__c,BI_Facebook__c,BI_Google__c,BI_Instagram__c,BI_Tuenti__c,BI_Twitter__c,BI_Yammer__c FROM BI_Multimarca_PP__c WHERE BI_Activo__c=true AND BI_Segment__c = 'Empresas' AND BI_Country__c =:segAccount.BI_Country__c AND RecordType.DeveloperName='BI_Redes_sociales'];
            }
            if(!context.footerLinks.isEmpty() && !context.socialNetworks.isEmpty())
                System.debug('Se recuperan: ['+context.footerLinks.size()+' footerLinks] y ['+context.socialNetworks.size()+'] socialNetworks');
            System.debug('\n\nFIN ----------CWP_FOOTER\n\n');
            
            return JSON.serialize(context); 
        } else {
            return JSON.serialize(map_return);
        } 
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Pardo Corral

Company:       Accenture

Description:   Method invoked to get the User's language for the new footer
History:
<Date>                          <Author>                                <Code>              <Change Description>
01/03/2018						Antonio Pardo Corral					46-50				Initial version
*/    
    @AuraEnabled
    public static string getUserLang(){
        return UserInfo.getLanguage();
    }
    
    /**
* Class:            LightningContext
* Description:     Aux Class to Wrap ligthning data
*
*/
    
    public class LightningContextFooter{
        @AuraEnabled
        public String segmento {get; set;}
        @AuraEnabled
        public List<BI_Multimarca_PP__c> footerLinks {get; set;}
        @AuraEnabled
        public List<BI_Multimarca_PP__c> socialNetworks {get; set;}
        
        
    }
}