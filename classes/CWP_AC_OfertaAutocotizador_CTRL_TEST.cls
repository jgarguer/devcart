/*------------------------------------------------------------------------
    Author:         Angel F. Santaliestra
    Company:        New Energy Aborda
    Description:    Test of CWP_AC_OfertaAutocotizador_CTRL Apex class.
    IN:             -
    OUT:            -
    
                    History
        <Date>      <Author>                <Description>
    19/01/2018  Angel F. Santaliestra       Initial version. 
------------------------------------------------------------------------*/
@isTest
public class CWP_AC_OfertaAutocotizador_CTRL_TEST {
    @testSetup 
    static void startUp() {
        NE__Document_component_Header__c docCompHeader = new NE__Document_component_Header__c();
        docCompHeader.NE__Name__c = 'TestName';
        insert docCompHeader;

        NE__Document_Component__c docComponent = new NE__Document_Component__c();
        docComponent.NE__Document_Component_Header__c = docCompHeader.Id;
        docComponent.NE__Name__c = 'TestName';
        insert docComponent;

        Id recordtypeCustomerCountry = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY); 

        Account oAccParent = new Account();
        oAccParent.Name =                       'TestAccountParent';
        oAccParent.RecordTypeId =               recordtypeCustomerCountry;
        oAccParent.BI_Segment__c =              'test';
        oAccParent.BI_Subsegment_Regional__c =  'test';
        insert oAccParent;

        Account oAccChild = new Account();
        oAccChild.Name =                        'TestAccountChild';
        oAccChild.ParentId =                    oAccParent.Id;
        oAccChild.RecordTypeId =                recordtypeCustomerCountry;
        oAccChild.BI_Segment__c =               'test';
        oAccChild.BI_Subsegment_Regional__c =   'test';
        insert oAccChild;

        NE__Contract_Header__c contractHead = new NE__Contract_Header__c();
        contractHead.NE__Name__c = 'Contract Header';
        insert contractHead;

        NE__Catalog_Header__c catalogHeader = new NE__Catalog_Header__c();
        catalogHeader.Name =        'Test Catalog';
        catalogHeader.NE__Name__c = 'Test Catalog';
        insert catalogHeader;
       
        NE__Catalog__c  oCatalog = new NE__Catalog__c();
        oCatalog.NE__Catalog_Header__c =    catalogHeader.Id;
        oCatalog.NE__StartDate__c =     Datetime.now();
        oCatalog.NE__Active__c =            true;
        insert oCatalog;
        
        NE__Catalog_Category__c catalogCategory = new NE__Catalog_Category__c();
        catalogCategory.Name =              'Category';
        catalogCategory.NE__CatalogId__c =  oCatalog.Id;
        insert catalogCategory;

        RecordType RTProd = [SELECT Id FROM RecordType WHERE (SobjectType = 'NE__Product__c' OR SobjectType = 'Product__c') AND Name = 'Standard'];
        
        NE__Product__c prod1 = new NE__Product__c();
        prod1.Name='MPLS VPN';
        prod1.RecordTypeId=RTProd.Id;
        insert prod1;
            
        NE__Product__c prod2 = new NE__Product__c();
        prod2.Name='INTERNET ACCESS';
        prod2.RecordTypeId=RTProd.Id;
        insert prod2;

        NE__Order__c oOrder = new NE__Order__c();
        oOrder.NE__AccountId__c =               oAccChild.Id;
        oOrder.NE__BillAccId__c =               oAccChild.Id;
        oOrder.NE__ServAccId__c =               oAccChild.Id;
        oOrder.NE__CatalogId__c =               oCatalog.Id;
        oOrder.NE__OrderStatus__c =             'Pending';
        oOrder.NE__ConfigurationStatus__c =     'Valid';
        oOrder.NE__Contract_Header__c =         contractHead.Id;
        oOrder.NE__Order_date__c =              Datetime.now().AddDays(1);
        oOrder.NE__Type__c =                    'New';
        insert oOrder;

        NE__OrderItem__c orderItemParent = new NE__OrderItem__c();
        orderItemParent.NE__BaseOneTimeFee__c       = 5;
        orderItemParent.NE__BaseRecurringCharge__c  = 5;
        orderItemParent.NE__OrderId__c              = oOrder.Id;
        orderItemParent.NE__Qty__c                  = 5;
        orderItemParent.CurrencyIsoCode             ='EUR';
        insert orderItemParent;

        NE__Order_Item_Attribute__c attr1 = new NE__Order_Item_Attribute__c();
        attr1.Name='Country';
        attr1.NE__Order_Item__c=orderItemParent.Id;
        attr1.NE__Value__c='Country';
        attr1.NE__Action__c='Add';
        insert attr1;

        NE__Order_Item_Attribute__c attr2 = new NE__Order_Item_Attribute__c();
        attr2.Name='City';
        attr2.NE__Order_Item__c=orderItemParent.Id;
        attr2.NE__Value__c='City';
        attr2.NE__Action__c='Add';
        insert attr2;

        NE__Order_Item_Attribute__c attr3 = new NE__Order_Item_Attribute__c();
        attr3.Name='Address';
        attr3.NE__Order_Item__c=orderItemParent.Id;
        attr3.NE__Value__c='Address';
        attr3.NE__Action__c='Add';
        insert attr3;

        NE__Order_Item_Attribute__c attr4 = new NE__Order_Item_Attribute__c();
        attr4.Name='Contract Term';
        attr4.NE__Order_Item__c=orderItemParent.Id;
        attr4.NE__Value__c='12';
        attr4.NE__Action__c='Add';
        insert attr4;

        NE__OrderItem__c orderItemChild1 = new NE__OrderItem__c();
        orderItemChild1.NE__Parent_Order_Item__c     = orderItemParent.Id;
        orderItemChild1.NE__BaseOneTimeFee__c        = 5;
        orderItemChild1.NE__BaseRecurringCharge__c   = 5;
        orderItemChild1.NE__OrderId__c               = oOrder.Id;
        orderItemChild1.NE__Qty__c                   = 5;
        orderItemChild1.NE__ProdId__c                = prod1.Id;
        orderItemChild1.NE__BaseOneTimeFee__c        = 100;
        orderItemChild1.NE__BaseRecurringCharge__c   = 100;
        orderItemChild1.CurrencyIsoCode             ='EUR';
        insert orderItemChild1;

        NE__Order_Item_Attribute__c attr6 = new NE__Order_Item_Attribute__c();
        attr6.Name='Bandwidth';
        attr6.NE__Order_Item__c=orderItemChild1.Id;
        attr6.NE__Value__c='2000';
        attr6.NE__Action__c='Add';
        insert attr6;

        NE__Order_Item_Attribute__c attr7 = new NE__Order_Item_Attribute__c();
        attr7.Name='Router';
        attr7.NE__Order_Item__c=orderItemChild1.Id;
        attr7.NE__Value__c='Router';
        attr7.NE__Action__c='Add';
        insert attr7;

        NE__Order_Item_Attribute__c attr8 = new NE__Order_Item_Attribute__c();
        attr8.Name='Technology';
        attr8.NE__Order_Item__c=orderItemChild1.Id;
        attr8.NE__Value__c='Technology';
        attr8.NE__Action__c='Add';
        insert attr8;
        
        NE__Order_Item_Attribute__c attr9 = new NE__Order_Item_Attribute__c();
        attr9.Name='Specifications';
        attr9.NE__Order_Item__c=orderItemChild1.Id;
        attr9.NE__Value__c='Specifications';
        attr9.NE__Action__c='Add';
        insert attr9;

        NE__OrderItem__c orderItemChild2 = new NE__OrderItem__c();
        orderItemChild2.NE__Parent_Order_Item__c     = orderItemParent.Id;
        orderItemChild2.NE__BaseOneTimeFee__c        = 5;
        orderItemChild2.NE__BaseRecurringCharge__c   = 5;
        orderItemChild2.NE__OrderId__c               = oOrder.Id;
        orderItemChild2.NE__Qty__c                   = 5;
        orderItemChild2.NE__ProdId__c                = prod2.Id;
        orderItemChild2.NE__BaseOneTimeFee__c        = 100;
        orderItemChild2.NE__BaseRecurringCharge__c   = 100;
        orderItemChild2.CurrencyIsoCode             ='EUR';
        insert orderItemChild2;

        NE__Order_Item_Attribute__c attr10 = new NE__Order_Item_Attribute__c();
        attr10.Name='Bandwidth';
        attr10.NE__Order_Item__c=orderItemChild2.Id;
        attr10.NE__Value__c='2000';
        attr10.NE__Action__c='Add';
        insert attr10;

        NE__Order_Item_Attribute__c attr11 = new NE__Order_Item_Attribute__c();
        attr11.Name='Router';
        attr11.NE__Order_Item__c=orderItemChild2.Id;
        attr11.NE__Value__c='Router';
        attr11.NE__Action__c='Add';
        insert attr11;

        NE__Order_Item_Attribute__c attr12 = new NE__Order_Item_Attribute__c();
        attr12.Name='Technology';
        attr12.NE__Order_Item__c=orderItemChild2.Id;
        attr12.NE__Value__c='Technology';
        attr12.NE__Action__c='Add';
        insert attr12;
        
        NE__Order_Item_Attribute__c attr13 = new NE__Order_Item_Attribute__c();
        attr13.Name='Specifications';
        attr13.NE__Order_Item__c=orderItemChild2.Id;
        attr13.NE__Value__c='Specifications';
        attr13.NE__Action__c='Add';
        insert attr13;

        //NE__Order_Item_Attribute__c ordenItemAttribute = new NE__Order_Item_Attribute__c();
        //ordenItemAttribute.NE__Value__c							= '5';
        //ordenItemAttribute.NE__Order_Item__c 					= oOrder.Id;
        //insert ordenItemAttribute;
    }
    
    @isTest
    private static void constructor_test() {
        NE__Document_Component__c docComponent = [  SELECT  Id,
                                                            NE__Document_Component_Header__c,
                                                            NE__Name__c
                                                    FROM    NE__Document_Component__c
                                                    LIMIT   1];
        NE__Order__c oOrder = [ SELECT  Id
                                FROM    NE__Order__c
                                LIMIT   1];
        ApexPages.currentPage().getParameters().put('orderId', oOrder.Id);
        ApexPages.StandardController stdController = new ApexPages.StandardController(docComponent);
        CWP_AC_OfertaAutocotizador_CTRL offertaComercial = new CWP_AC_OfertaAutocotizador_CTRL(stdController);
    }
}