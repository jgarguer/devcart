/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:
* Description:
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-07-24      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_SessionContract_ctr
*			 1.1    2017-01-31		Pedro Párraga			Modify test_method_7()
*			 1.2	23-02-2017      Jaime Regidor           Adding Campaign Values, Adding Catalog Country
			 1.3	13-03-2017		Marta Gonzalez(Everis)	REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
			 		20/09/2017      Angel F. Santaliestra   Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest
private class BI_COL_SessionContract_tst {

	public static BI_COL_SessionContract_ctr objSesContrac;
	public static BI_COL_Modificacion_de_Servicio__c objMS;
	public static Account objCuenta;
	public static Account objCuenta2;
	public static Contact objContacto;
	public static Opportunity objOportunidad;
	public static BI_COL_Anexos__c objAnexos;
	public static BI_COL_Descripcion_de_servicio__c objDesSer;
	public static Contract objContrato;
	public static Campaign objCampana;
	public static BI_Col_Ciudades__c objCiudad;
	public static BI_Sede__c objSede;
	public static User objUsuario;
	public static BI_Punto_de_instalacion__c objPuntosInsta;

	public static list <Profile> lstPerfil;
	public static list <UserRole> lstRoles;
	public static list <Campaign> lstCampana;
	public static list<BI_COL_manage_cons__c> lstManege;
	public static List<RecordType> lstRecTyp;
	public static List<User> lstUsuarios;
	public static List<BI_COL_SessionContract_ctr.wrapperServicios> lstWraperServ;


	public static void crearData() {

		BI_bypass__c objBibypass = new BI_bypass__c();
		objBibypass.BI_migration__c = true;
		insert objBibypass;

		//Roles
		lstRoles                = new list <UserRole>();
		lstRoles                = [Select id, Name from UserRole where Name = 'Telefónica Global'];
		system.debug('datos Rol ' + lstRoles);

		//Cuentas
		objCuenta 										= new Account();
		objCuenta.Name                                  = 'prueba';
		objCuenta.BI_Country__c                         = 'Argentina';
		objCuenta.TGS_Region__c                         = 'América';
		objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta.CurrencyIsoCode                       = 'COP';
		objCuenta.BI_Fraude__c                          = false;
		objCuenta.BI_Segment__c                         = 'test';
		objCuenta.BI_Subsegment_Regional__c             = 'test';
		objCuenta.BI_Territory__c                       = 'test';
		insert objCuenta;
		system.debug('datos Cuenta ' + objCuenta);

		objCuenta2                                       = new Account();
		objCuenta2.Name                                  = 'prueba';
		objCuenta2.BI_Country__c                         = 'Argentina';
		objCuenta2.TGS_Region__c                         = 'América';
		objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta2.CurrencyIsoCode                       = 'COP';
		objCuenta2.BI_Fraude__c                          = true;
		objCuenta.BI_Segment__c                         = 'test';
		objCuenta.BI_Subsegment_Regional__c             = 'test';
		objCuenta.BI_Territory__c                       = 'test';
		insert objCuenta2;

		//Contactos
		objContacto                                     = new Contact();
		objContacto.LastName                        	= 'Test';
		objContacto.BI_Country__c             		    = 'Argentina';
		objContacto.CurrencyIsoCode           		    = 'COP';
		objContacto.AccountId                 		    = objCuenta.Id;
		objContacto.BI_Tipo_de_contacto__c				= 'Administrador Canal Online';
        //REING-INI
	    objContacto.MobilePhone          				= '1236547890';
	    objContacto.Phone						        = '11111111';
	    objContacto.Email						        = 'test@test.com';		
        objContacto.BI_Tipo_de_documento__c 			= 'Otros';
        objContacto.BI_Numero_de_documento__c 			= '00000000X';
        objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true; // Con la nueva lógica un administrador canal online significa tenga este campo a true
        //REING_FIN
		Insert objContacto;

		//catalogo
		NE__Catalog__c objCatalogo 						= new NE__Catalog__c();
		objCatalogo.Name 								= 'prueba Catalogo';
		objCatalogo.BI_country__c 						= 'Argentina';
		insert objCatalogo;

		//Campaña
		objCampana 										= new Campaign();
		objCampana.Name 								= 'Campaña Prueba';
		objCampana.BI_Catalogo__c 						= objCatalogo.Id;
		objCampana.BI_COL_Codigo_campana__c 			= 'prueba1';
		objCampana.BI_Country__c 						= 'Argentina';
        objCampana.BI_Opportunity_Type__c 				= 'Fijo';
        objCampana.BI_SIMP_Opportunity_Type__c 			= 'Alta';
		insert objCampana;
		lstCampana 										= new List<Campaign>();
		lstCampana 										= [select Id, Name from Campaign where Id = : objCampana.Id];
		system.debug('datos Cuenta ' + lstCampana);

		//Tipo de registro
		lstRecTyp 										= [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
		system.debug('datos de FUN ' + lstRecTyp);

		//Contrato
		objContrato 									= new Contract ();
		objContrato.AccountId 							= objCuenta.Id;
		objContrato.StartDate 							=  System.today().addDays(+5);
		objContrato.BI_COL_Formato_Tipo__c 				= 'Contrato Marco';
		objContrato.ContractTerm 						= 12;
		objContrato.BI_COL_Presupuesto_contrato__c 		= 1000000;
		objContrato.StartDate							= System.today().addDays(+5);
		insert objContrato;
		System.debug('datos Contratos ===>' + objContrato);

		//FUN
		objAnexos               						= new BI_COL_Anexos__c();
		objAnexos.Name          						= 'FUN-0041414';
		objAnexos.RecordTypeId  						= lstRecTyp[0].Id;
		objAnexos.BI_COL_Contrato__c   					= objContrato.Id;
		insert objAnexos;
		System.debug('\n\n\n Sosalida objAnexos ' + objAnexos + '\n ====> objAnexos.Id ' + objAnexos.Id);

		//Configuracion Personalizada
		BI_COL_manage_cons__c objConPer 				= new BI_COL_manage_cons__c();
		objConPer.Name                  				= 'Viabilidades';
		objConPer.BI_COL_Numero_Viabilidad__c 			= 46;
		objConPer.BI_COL_ConsProyecto__c     			= 1;
		objConPer.BI_COL_ValorConsecutivo__c  			= 1;
		lstManege                      					= new list<BI_COL_manage_cons__c >();
		lstManege.add(objConPer);
		insert lstManege;
		System.debug('======= configuracion personalizada ======= ' + lstManege);

		//Oportunidad
		objOportunidad                                      	= new Opportunity();
		objOportunidad.Name                                   	= 'prueba opp';
		objOportunidad.AccountId                              	= objCuenta.Id;
		objOportunidad.BI_Country__c                          	= 'Argentina';
		objOportunidad.CloseDate                              	= System.today().addDays(+5);
		objOportunidad.StageName                              	= 'F5 - Solution Definition';
		objOportunidad.CurrencyIsoCode                        	= 'COP';
		objOportunidad.Certa_SCP__contract_duration_months__c 	= 12;
		objOportunidad.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
		objOportunidad.BI_Duracion_del_contrato_Meses__c		= 1.5;
		objOportunidad.OwnerId                                	= objUsuario.id;
		insert objOportunidad;
		system.debug('Datos Oportunidad' + objOportunidad);
		list<Opportunity> lstOportunidad 	= new list<Opportunity>();
		lstOportunidad 						= [select Id from Opportunity where Id = : objOportunidad.Id];
		system.debug('datos ListaOportunida ' + lstOportunidad);

		//Ciudad
		objCiudad = new BI_Col_Ciudades__c ();
		objCiudad.Name 						= 'Test City';
		objCiudad.BI_COL_Pais__c 			= 'Test Country';
		objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
		insert objCiudad;

		//Direccion
		objSede 								= new BI_Sede__c();
		objSede.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
		objSede.BI_Direccion__c 				= 'Test Street 123 Number 321';
		objSede.BI_Localidad__c 				= 'Test Local';
		objSede.BI_COL_Estado_callejero__c 		= System.Label.BI_COL_LbValor3EstadoCallejero;
		objSede.BI_COL_Sucursal_en_uso__c 		= 'Libre';
		objSede.BI_Country__c 					= 'Colombia';
		objSede.Name 							= 'Test Street 123 Number 321, Test Local Colombia';
		objSede.BI_Codigo_postal__c 			= '12356';
		insert objSede;

		//Sede
		objPuntosInsta 						= new BI_Punto_de_instalacion__c ();
		objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
		objPuntosInsta.BI_Sede__c          = objSede.Id;
		objPuntosInsta.BI_Contacto__c      = objContacto.id;
		objPuntosInsta.Name 				= 'Pruebas conenejsdjkn';
		insert objPuntosInsta;

		//DS
		objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
		objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
		objDesSer.CurrencyIsoCode               			= 'COP';
		insert objDesSer;
		System.debug('Data DS ====> ' + objDesSer);
		System.debug('Data DS ====> ' + objDesSer.Name);
		List<BI_COL_Descripcion_de_servicio__c> lstqry 	= [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
		System.debug('Data lista DS ====> ' + lstqry);

		//MS
		objMS                         				= new BI_COL_Modificacion_de_Servicio__c();
		objMS.BI_COL_FUN__c           				= objAnexos.Id;
		objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
		objMS.BI_COL_Clasificacion_Servicio__c  	= 'ALTA';
		objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
		objMS.BI_COL_Bloqueado__c 					= false;
		objMS.BI_COL_Estado__c 						= label.BI_COL_lblActiva;//label.BI_COL_lblActiva;
		objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
		objMs.BI_COL_Sucursal_Origen__c 				= objPuntosInsta.Id;
		insert objMS;
		system.debug('Data objMs ===>' + objMs);
	}


	static testMethod void test_method_1() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_CusContractService_pag;
			Test.setCurrentPage(pageRef);

			List<BI_COL_Modificacion_de_Servicio__c> lstMS 	= new List<BI_COL_Modificacion_de_Servicio__c>();

			Test.startTest();
			pageRef.getParameters().put('id', objContrato.Id);
			lstMS.add(objMs);
			objSesContrac 					= new BI_COL_SessionContract_ctr (controler);
			objSesContrac.procesables 		= lstMS;
			objSesContrac.inProcesables 	= lstMS;
			objSesContrac.noSeCambian 		= lstMS;

			objContrato 			= new Contract ();
			objSesContrac.siguiente3();
			objContrato.AccountId 	= objCuenta.Id;
			objContrato.StartDate 	=  System.today().addDays(+5);
			objSesContrac.ctr 		= objContrato;
			objSesContrac.validarMs();
			objSesContrac.Servicios();

			Test.stopTest();
		}
	}


	static testMethod void test_method_2() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler        			 	= new ApexPages.StandardController(objContrato);
			PageReference                 pageRef          			 	= Page.BI_COL_CusContractService_pag;
			Test.setCurrentPage(pageRef);

			List<BI_COL_Modificacion_de_Servicio__c> lstMS 				= new List<BI_COL_Modificacion_de_Servicio__c>();
			List<BI_Sede__c> lstSede 									= new List<BI_Sede__c>();
			List<Account> lstCuentas 									= new List<Account> ();
			List<BI_Punto_de_instalacion__c> lstPuntosIns 				= new List<BI_Punto_de_instalacion__c>();
			BI_COL_SessionContract_ctr.wrapper objWrapp 				= new BI_COL_SessionContract_ctr.wrapper (true, true, objMS);
			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi 	= new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS);
			BI_COL_SessionContract_ctr.formulario objWrappFrom 			= new BI_COL_SessionContract_ctr.formulario();
			lstWraperServ 												= new List<BI_COL_SessionContract_ctr.wrapperServicios> ();


			Test.startTest();
			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			lstMS.add(objMs);
			objSesContrac 					= new BI_COL_SessionContract_ctr (controler);
			objSesContrac.e 				= 'c';
			objSesContrac.procesables 		= lstMS;
			objSesContrac.inProcesables 	= lstMS;
			objSesContrac.noSeCambian 		= lstMS;

			lstPuntosIns.add(objPuntosInsta);
			objSesContrac.sucursalesPorCrear 	= lstPuntosIns;
			Boolean bln 						= objSesContrac.sucpcr;
			objSesContrac.Busqueda 				= '1';
			objSesContrac.Servicios();
			objSesContrac.getContactoInfo();
			objSesContrac.getContratoInfo();

			lstWraperServ.add(objWrappServi);
			objSesContrac.wrServicios 	= lstWraperServ;
			objSesContrac.Servicios();
			objSesContrac.validarServicios();

			system.debug('objms tow ==>' + objMS);
			objwrappServi.sel 	= true;
			lstWraperServ.add(objWrappServi);
			objSesContrac.wrServicios 	= lstWraperServ;
			objSesContrac.Busqueda    = objDesSer.Name + ',' + objDesSer.Name;
			objSesContrac.Servicios();

			objSesContrac.siguiente();

			objWrappFrom.ctr 	= objContrato;
			objSesContrac.f 	= objWrappFrom;
			objSesContrac.siguiente2();

			objWrappFrom.ctr 			= objContrato;
			objWrappFrom.myMs 			= objMS;
			objWrappFrom.contactoFacId 	= objContacto.id;
			objWrappFrom.contactoInsId 	= objContacto.id;
			objWrappFrom.cuenta       	= objCuenta.id;
			objSesContrac.f 			= objWrappFrom;
			objSesContrac.sucursalesPorCrear = lstPuntosIns;
			objSesContrac.siguiente3();


			objContrato.AccountId 							= objCuenta2.Id;
			update objContrato;

			objContrato 	= [select id, name, Account.BI_Fraude__c, Account.BI_No_Identificador_fiscal__c, BI_COL_Numero_de_contrato_Cliente__c
			                   from Contract       where id = :objContrato.Id].get(0);
			objSesContrac.ctr 		= objContrato;

			objSesContrac.Servicios();
			objSesContrac.validarMs();


			objContrato = new Contract();
			objSesContrac.Servicios();
			objSesContrac.validarMs();
			objSesContrac.ValidaDaboxTrs();
			objSesContrac.cancelar();

			Test.stopTest();
		}
	}

	static testMethod void test_method_3() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler         			= new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           			= Page.BI_COL_CusContractService_pag;
			Test.setCurrentPage(pageRef);

			List<BI_COL_Modificacion_de_Servicio__c> lstMS 				= new List<BI_COL_Modificacion_de_Servicio__c>();
			List<BI_Sede__c> lstSede 									= new List<BI_Sede__c>();
			List<BI_Punto_de_instalacion__c> lstPuntosIns 				= new List<BI_Punto_de_instalacion__c>();
			List<Account> lstCuentas 									= new List<Account> ();
			BI_COL_SessionContract_ctr.wrapper objWrapp 				= new BI_COL_SessionContract_ctr.wrapper (true, true, objMS);
			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi 	= new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS);
			BI_COL_SessionContract_ctr.formulario objWrappFrom 			= new BI_COL_SessionContract_ctr.formulario();

			lstWraperServ = new List<BI_COL_SessionContract_ctr.wrapperServicios> ();

			Test.startTest();
			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			lstMS.add(objMs);
			objSesContrac 					= new BI_COL_SessionContract_ctr (controler);
			objSesContrac.e 				= 'c';
			objSesContrac.procesables 		= lstMS;
			objSesContrac.inProcesables 	= lstMS;
			objSesContrac.noSeCambian 		= lstMS;

			lstPuntosIns.add(objPuntosInsta);
			objSesContrac.sucursalesPorCrear 	= lstPuntosIns;
			Boolean bln 						= objSesContrac.sucpcr;
			objSesContrac.Busqueda 				= '1';
			objSesContrac.Servicios();

			lstWraperServ.add(objWrappServi);
			objSesContrac.ctr 	= objContrato;
			objSesContrac.validarMs();
			objSesContrac.Servicios();

			objwrappServi.sel 			= true;
			objSesContrac.wrServicios 	= lstWraperServ;
			objSesContrac.validarServicios();


			Test.stopTest();
		}
	}

	static testMethod void test_method_4() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler         			= new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           			= Page.BI_COL_CusContractService_pag;
			Test.setCurrentPage(pageRef);

			List<BI_COL_Modificacion_de_Servicio__c> lstMS 				= new List<BI_COL_Modificacion_de_Servicio__c>();
			List<BI_Sede__c> lstSede 									= new List<BI_Sede__c>();
			List<BI_Punto_de_instalacion__c> lstPuntosIns 				= new List<BI_Punto_de_instalacion__c>();
			List<Account> lstCuentas 									= new List<Account> ();
			List<BI_COL_Descripcion_de_servicio__c> lstDS1				= new List<BI_COL_Descripcion_de_servicio__c> ();
			BI_COL_SessionContract_ctr.wrapper objWrapp 				= new BI_COL_SessionContract_ctr.wrapper (true, true, objMS);
			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi 	= new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS);
			BI_COL_SessionContract_ctr.formulario objWrappFrom 			= new BI_COL_SessionContract_ctr.formulario();
			objMS           = new BI_COL_Modificacion_de_Servicio__c();

			objMS.BI_COL_FUN__c           				= objAnexos.Id;
			objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
			objMS.BI_COL_Clasificacion_Servicio__c  	= 'BAJA';
			objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c 					= false;
			objMS.BI_COL_Estado__c 						= label.BI_COL_lblPendiente;
			objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
			insert objMS;
			system.debug('objMs three 1===>' + objMs);

			lstWraperServ = new List<BI_COL_SessionContract_ctr.wrapperServicios> ();

			Test.startTest();
			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			lstMS.add(objMs);
			objSesContrac 					= new BI_COL_SessionContract_ctr (controler);
			objSesContrac.e 				= 'c';
			objSesContrac.procesables 		= lstMS;
			objSesContrac.inProcesables 	= lstMS;
			objSesContrac.noSeCambian 		= lstMS;

			lstPuntosIns.add(objPuntosInsta);
			objSesContrac.sucursalesPorCrear 	= lstPuntosIns;
			Boolean bln 						= objSesContrac.sucpcr;
			objSesContrac.Busqueda 				= objDesSer.Id;


			objSesContrac.Servicios();

			lstWraperServ.add(objWrappServi);
			objSesContrac.ctr 	= objContrato;
			objSesContrac.validarMs();
			objSesContrac.Servicios();

			objwrappServi.sel 			= true;
			objSesContrac.wrServicios 	= lstWraperServ;
			objSesContrac.validarServicios();

			Test.stopTest();
		}
	}

	static testMethod void test_method_4_one() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler         			= new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           			= Page.BI_COL_CusContractService_pag;
			Test.setCurrentPage(pageRef);

			List<BI_COL_Modificacion_de_Servicio__c> lstMS 				= new List<BI_COL_Modificacion_de_Servicio__c>();
			List<BI_Sede__c> lstSede 									= new List<BI_Sede__c>();
			List<BI_Punto_de_instalacion__c> lstPuntosIns 				= new List<BI_Punto_de_instalacion__c>();
			List<Account> lstCuentas 									= new List<Account> ();
			BI_COL_SessionContract_ctr.wrapper objWrapp 				= new BI_COL_SessionContract_ctr.wrapper (true, true, objMS);
			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi 	= new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS);
			BI_COL_SessionContract_ctr.formulario objWrappFrom 			= new BI_COL_SessionContract_ctr.formulario();

			objMS.BI_COL_Clasificacion_Servicio__c  	= 'BAJA';
			objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
			Update objMS;
			system.debug('objMs three 1===>' + objMs);

			lstWraperServ = new List<BI_COL_SessionContract_ctr.wrapperServicios> ();

			Test.startTest();
			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			lstMS.add(objMs);
			objSesContrac 					= new BI_COL_SessionContract_ctr (controler);
			objSesContrac.e 				= 'c';
			objSesContrac.procesables 		= lstMS;
			objSesContrac.inProcesables 	= lstMS;
			objSesContrac.noSeCambian 		= lstMS;

			lstPuntosIns.add(objPuntosInsta);
			objSesContrac.sucursalesPorCrear 	= lstPuntosIns;
			Boolean bln 						= objSesContrac.sucpcr;
			objSesContrac.Busqueda 				= '1';
			objSesContrac.Servicios();

			lstWraperServ.add(objWrappServi);
			objSesContrac.ctr 	= objContrato;
			objSesContrac.validarMs();
			objSesContrac.Servicios();

			objwrappServi.sel 			= true;
			objSesContrac.wrServicios 	= lstWraperServ;
			objSesContrac.validarServicios();

			Test.stopTest();
		}
	}

	static testMethod void test_method_5() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler         			= new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           			= Page.BI_COL_CusContractService_pag;
			Test.setCurrentPage(pageRef);

			List<BI_COL_Modificacion_de_Servicio__c> lstMS 				= new List<BI_COL_Modificacion_de_Servicio__c>();
			List<BI_Sede__c> lstSede 									= new List<BI_Sede__c>();
			List<BI_Punto_de_instalacion__c> lstPuntosIns 				= new List<BI_Punto_de_instalacion__c>();
			List<Account> lstCuentas 									= new List<Account> ();
			BI_COL_SessionContract_ctr.wrapper objWrapp 				= new BI_COL_SessionContract_ctr.wrapper (true, true, objMS);
			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi 	= new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS);
			BI_COL_SessionContract_ctr.formulario objWrappFrom 			= new BI_COL_SessionContract_ctr.formulario();

			objMS.BI_COL_Clasificacion_Servicio__c  	= 'BAJA';
			objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
			Update objMS;
			system.debug('objMs three 1===>' + objMs);

			lstWraperServ = new List<BI_COL_SessionContract_ctr.wrapperServicios> ();

			Test.startTest();
			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			lstMS.add(objMs);
			objSesContrac 					= new BI_COL_SessionContract_ctr (controler);
			objSesContrac.e 				= 'c';
			objSesContrac.procesables 		= lstMS;
			objSesContrac.inProcesables 	= lstMS;
			objSesContrac.noSeCambian 		= lstMS;

			lstPuntosIns.add(objPuntosInsta);
			objSesContrac.sucursalesPorCrear 	= lstPuntosIns;
			Boolean bln 						= objSesContrac.sucpcr;
			objSesContrac.Busqueda 				= '1';
			objSesContrac.Servicios();

			lstWraperServ.add(objWrappServi);
			objSesContrac.ctr 	= objContrato;
			objSesContrac.validarMs();
			objSesContrac.Servicios();

			objwrappServi.sel 			= true;
			objSesContrac.wrServicios 	= lstWraperServ;
			objSesContrac.validarServicios();

			Test.stopTest();
		}
	}

	static testMethod void test_method_5_one () {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler         			= new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           			= Page.BI_COL_CusContractService_pag;
			Test.setCurrentPage(pageRef);

			List<BI_COL_Modificacion_de_Servicio__c> lstMS 				= new List<BI_COL_Modificacion_de_Servicio__c>();
			List<BI_Sede__c> lstSede 									= new List<BI_Sede__c>();
			List<BI_Punto_de_instalacion__c> lstPuntosIns 				= new List<BI_Punto_de_instalacion__c>();
			List<Account> lstCuentas 									= new List<Account> ();
			BI_COL_SessionContract_ctr.wrapper objWrapp 				= new BI_COL_SessionContract_ctr.wrapper (true, true, objMS);
			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi 	= new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS);
			BI_COL_SessionContract_ctr.formulario objWrappFrom 			= new BI_COL_SessionContract_ctr.formulario();
			objMS           = new BI_COL_Modificacion_de_Servicio__c();

			objMS.BI_COL_FUN__c           				= objAnexos.Id;
			objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
			objMS.BI_COL_Clasificacion_Servicio__c  	= 'BAJA';
			objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c 					= false;
			objMS.BI_COL_Estado__c 						= label.BI_COL_lblPendiente;
			objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
			insert objMS;
			system.debug('objMs three 2===>' + objMs);

			lstWraperServ = new List<BI_COL_SessionContract_ctr.wrapperServicios> ();

			Test.startTest();
			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			lstMS.add(objMs);
			objSesContrac 					= new BI_COL_SessionContract_ctr (controler);
			objSesContrac.e 				= 'c';
			objSesContrac.procesables 		= lstMS;
			objSesContrac.inProcesables 	= lstMS;
			objSesContrac.noSeCambian 		= lstMS;

			lstPuntosIns.add(objPuntosInsta);
			objSesContrac.sucursalesPorCrear 	= lstPuntosIns;
			Boolean bln 						= objSesContrac.sucpcr;
			objSesContrac.Busqueda 				= '1';
			objSesContrac.Servicios();

			lstWraperServ.add(objWrappServi);
			objSesContrac.ctr 	= objContrato;
			objSesContrac.validarMs();
			objSesContrac.Servicios();

			objwrappServi.sel 			= true;
			objSesContrac.wrServicios 	= lstWraperServ;
			objSesContrac.validarServicios();

			Test.stopTest();
		}
	}

	static testMethod void test_method_6() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler         			= new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           			= Page.BI_COL_CusContractService_pag;
			Test.setCurrentPage(pageRef);

			List<BI_COL_Modificacion_de_Servicio__c> lstMS 				= new List<BI_COL_Modificacion_de_Servicio__c>();
			List<BI_Sede__c> lstSede 									= new List<BI_Sede__c>();
			List<BI_Punto_de_instalacion__c> lstPuntosIns 				= new List<BI_Punto_de_instalacion__c>();
			List<Account> lstCuentas 									= new List<Account> ();
			BI_COL_SessionContract_ctr.wrapper objWrapp 				= new BI_COL_SessionContract_ctr.wrapper (true, true, objMS);
			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi 	= new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS);
			BI_COL_SessionContract_ctr.formulario objWrappFrom 			= new BI_COL_SessionContract_ctr.formulario();
			objMS           = new BI_COL_Modificacion_de_Servicio__c();

			objMS.BI_COL_FUN__c           				= objAnexos.Id;
			objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
			objMS.BI_COL_Clasificacion_Servicio__c  	= 'BAJA';
			objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c 					= false;
			objMS.BI_COL_Estado__c 						= label.BI_COL_lblPendiente;
			objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
			objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
			insert objMS;
			system.debug('objMs three 3===>' + objMs);

			lstWraperServ = new List<BI_COL_SessionContract_ctr.wrapperServicios> ();

			Test.startTest();
			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			lstMS.add(objMs);
			objSesContrac 					= new BI_COL_SessionContract_ctr (controler);
			objSesContrac.e 				= 'c';
			objSesContrac.procesables 		= lstMS;
			objSesContrac.inProcesables 	= lstMS;
			objSesContrac.noSeCambian 		= lstMS;

			lstPuntosIns.add(objPuntosInsta);
			objSesContrac.sucursalesPorCrear 	= lstPuntosIns;
			Boolean bln 						= objSesContrac.sucpcr;
			objSesContrac.Busqueda 				= '1';
			objSesContrac.Servicios();

			if (objMS.BI_COL_Sucursal_de_Facturacion__c != null ) {
				System.debug('se ingreso al If de MS :D');
				if (objMS.BI_COL_Sucursal_de_Facturacion__r.BI_Sede__c != null) {
					System.debug('se ingreso al If de puntos:D');
				}

			}
			lstWraperServ.add(objWrappServi);
			objSesContrac.ctr 	= objContrato;
			objSesContrac.validarMs();
			objSesContrac.Servicios();

			objwrappServi.sel 			= true;
			objSesContrac.wrServicios 	= lstWraperServ;
			objSesContrac.validarServicios();

			Test.stopTest();
		}
	}

	static testMethod void test_method_7() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			BI_COL_Modificacion_de_Servicio__c objMS2           = new BI_COL_Modificacion_de_Servicio__c();

			objMS2.BI_COL_FUN__c           				= objAnexos.Id;
			objMS2.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
			objMS2.BI_COL_Clasificacion_Servicio__c  	= 'ALTA';
			objMS2.BI_COL_Oportunidad__c 				= objOportunidad.Id;
			objMS2.BI_COL_Bloqueado__c 					= false;
			objMS2.BI_COL_Estado__c 					= 'Pendiente';
		System.debug('_estado1: '+objMS2.BI_COL_Estado__c);
		//Map<Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(Userinfo.getUserId(), true, true, true, true);
		insert objMS2;
		System.debug('_estado2: '+objMS2.BI_COL_Estado__c);
		//BI_MigrationHelper.disableBypass(mapa);
		System.debug('_estado3: '+objMS2.BI_COL_Estado__c);
		objMS2.BI_COL_Estado__c 					= label.BI_COL_lblActiva;
		update objMS2;

			ApexPages.StandardController  controler         			= new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           			= Page.BI_COL_CusContractService_pag;
			Test.setCurrentPage(pageRef);

			List<BI_COL_Modificacion_de_Servicio__c> lstMS 				= new List<BI_COL_Modificacion_de_Servicio__c>();
			List<BI_Sede__c> lstSede 									= new List<BI_Sede__c>();
			List<BI_Punto_de_instalacion__c> lstPuntosIns 				= new List<BI_Punto_de_instalacion__c>();
			List<Account> lstCuentas 									= new List<Account> ();
			BI_COL_SessionContract_ctr.wrapper objWrapp 				= new BI_COL_SessionContract_ctr.wrapper (true, true, objMS2);
			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi 	= new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS2);
			BI_COL_SessionContract_ctr.formulario objWrappFrom 			= new BI_COL_SessionContract_ctr.formulario();

			//objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
			//objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;

			system.debug('objMs three 4===>' + objMs);

			lstWraperServ = new List<BI_COL_SessionContract_ctr.wrapperServicios> ();

			Test.startTest();
			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			lstMS.add(objMS2);
			objSesContrac 					= new BI_COL_SessionContract_ctr (controler);
			objSesContrac.e 				= 'c';
			objSesContrac.procesables 		= lstMS;
			objSesContrac.inProcesables 	= lstMS;
			objSesContrac.noSeCambian 		= lstMS;

			lstPuntosIns.add(objPuntosInsta);
			objSesContrac.sucursalesPorCrear 	= lstPuntosIns;
			Boolean bln 						= objSesContrac.sucpcr;
			objSesContrac.Busqueda 				= objDesSer.Id + ',' + objDesSer.Id;
			objSesContrac.Servicios();

			objWrappFrom.ctr 	= objContrato;
			objSesContrac.f 	= objWrappFrom;
			objSesContrac.siguiente2();

			objWrappFrom.ctr 			= objContrato;
			objWrappFrom.myMs 			= objMS;
			objWrappFrom.contactoFacId 	= objContacto.id;
			objWrappFrom.contactoInsId 	= objContacto.id;
			objWrappFrom.cuenta       	= objCuenta.id;
			objSesContrac.f 			= objWrappFrom;

			lstWraperServ.add(objWrappServi);
			objSesContrac.ctr 	= objContrato;
			objSesContrac.validarMs();
			objSesContrac.Servicios();

			objwrappServi.sel 			= true;
			objSesContrac.wrServicios 	= lstWraperServ;
			objSesContrac.validarServicios();
			objSesContrac.siguiente3();



			Test.stopTest();
		}
	}


	static testMethod void test_method_8() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_CusContractSession_pag;
			Test.setCurrentPage(pageRef);

			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi = new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS);
			lstWraperServ = new List<BI_COL_SessionContract_ctr.wrapperServicios> ();
			lstWraperServ.add(objWrappServi);
			Test.startTest();

			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			objSesContrac = new BI_COL_SessionContract_ctr (controler);
			objSesContrac.getContactoInfo();
			objSesContrac.getContratoInfo();
			objSesContrac.getCuentaInfo();
			objSesContrac.getValidadosTrsDavox();

			Test.stopTest();
		}
	}

	static testMethod void test_method_9() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_CusSessionContractAllocation_pag;
			Test.setCurrentPage(pageRef);

			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi = new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS);
			lstWraperServ = new List<BI_COL_SessionContract_ctr.wrapperServicios> ();
			lstWraperServ.add(objWrappServi);
			Test.startTest();

			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			objSesContrac = new BI_COL_SessionContract_ctr (controler);
			objSesContrac.getContactoInfo();
			objSesContrac.getContratoInfo();

			list<BI_COL_Modificacion_de_Servicio__c> ms = new list<BI_COL_Modificacion_de_Servicio__c>([SELECT BI_COL_Clasificacion_Servicio__c, BI_COL_Codigo_unico_servicio__c , Id, BI_COL_Codigo_unico_servicio__r.Name FROM BI_COL_Modificacion_de_Servicio__c where id = : objMs.id]);
			system.debug('prueba Query =====>' + ms);

			Test.stopTest();
		}
	}
	static testMethod void test_method_10() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_CusSessionContractFinalized_pag;
			Test.setCurrentPage(pageRef);

			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi = new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS);
			lstWraperServ = new List<BI_COL_SessionContract_ctr.wrapperServicios> ();
			lstWraperServ.add(objWrappServi);
			Test.startTest();

			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			objSesContrac = new BI_COL_SessionContract_ctr (controler);
			objSesContrac.getContactoInfo();
			objSesContrac.getContratoInfo();

			list<BI_COL_Modificacion_de_Servicio__c> ms = new list<BI_COL_Modificacion_de_Servicio__c>([SELECT BI_COL_Clasificacion_Servicio__c, BI_COL_Codigo_unico_servicio__c , Id, BI_COL_Codigo_unico_servicio__r.Name FROM BI_COL_Modificacion_de_Servicio__c where id = : objMs.id]);
			system.debug('prueba Query =====>' + ms);

			Test.stopTest();
		}
	}
	static testMethod void test_method_11() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_CusSessionContractValidation_pag;
			Test.setCurrentPage(pageRef);

			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi = new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS);
			lstWraperServ = new List<BI_COL_SessionContract_ctr.wrapperServicios> ();
			lstWraperServ.add(objWrappServi);
			Test.startTest();

			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			objSesContrac = new BI_COL_SessionContract_ctr (controler);
			objSesContrac.getContactoInfo();
			objSesContrac.getContratoInfo();

			list<BI_COL_Modificacion_de_Servicio__c> ms = new list<BI_COL_Modificacion_de_Servicio__c>([SELECT BI_COL_Clasificacion_Servicio__c, BI_COL_Codigo_unico_servicio__c , Id, BI_COL_Codigo_unico_servicio__r.Name FROM BI_COL_Modificacion_de_Servicio__c where id = : objMs.id]);
			system.debug('prueba Query =====>' + ms);

			Test.stopTest();
		}
	}

	static testMethod void test_method_12() {
		// TO DO: implement unit test
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_CusContractService_pag;
			Test.setCurrentPage(pageRef);

			BI_COL_SessionContract_ctr.wrapperServicios objWrappServi = new BI_COL_SessionContract_ctr.wrapperServicios (false , objMS);
			lstWraperServ = new List<BI_COL_SessionContract_ctr.wrapperServicios> ();
			lstWraperServ.add(objWrappServi);
			Test.startTest();

			pageRef.getParameters().put('id', objContrato.Id);
			pageRef.getParameters().put('e', 'c');
			objSesContrac = new BI_COL_SessionContract_ctr (controler);

			list<BI_COL_Modificacion_de_Servicio__c> ms = new list<BI_COL_Modificacion_de_Servicio__c>([SELECT BI_COL_Clasificacion_Servicio__c, BI_COL_Codigo_unico_servicio__c , Id, BI_COL_Codigo_unico_servicio__r.Name FROM BI_COL_Modificacion_de_Servicio__c where id = : objMs.id]);
			system.debug('prueba Query =====>' + ms);

			objSesContrac.getContactoInfo();
			objSesContrac.getContratoInfo();
			Test.stopTest();
		}
	}
}