@isTest
private class PCA_InventoryController_TEST {

    static testMethod void getloadInfo() {

        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
    	 User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());         
	    //insert usr;
	    system.debug('usr: '+usr);
	    User user1;
	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais); 
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            
            user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());	
            
            List<BI_Contact_Customer_Portal__c> CCP = [SELECT Name From BI_Contact_Customer_Portal__c];
            system.debug('CCP^^^: ' +CCP);
    		List <RecordType> RT1 = [SELECT Id, Name FROM RecordType Where Name = 'Asset'];
    		system.debug('** RT1: ' +RT1 );

        Opportunity opp = new Opportunity(Name = 'Test',
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                                          AccountId = accList[0].Id,
                                          BI_Ciclo_ventas__c = Label.BI_Completo,
                                          BI_Country__c = accList[0].BI_Country__c);
	                                              
		insert opp;
		

    		NE__Order__c order1 = new NE__Order__c (NE__Asset_Account__c=acc[0].Id,RecordTypeId=RT1[0].Id, NE__OptyId__c=opp.Id, NE__OrderStatus__c = Label.BI_LabelActive); 
            insert order1;            
			NE__Product__c prod = new NE__Product__c();
	        prod.Name = 'TestProd';
	        insert prod;
        
	        NE__Catalog_Header__c catH = new NE__Catalog_Header__c();
	        catH.Name='t';
	        catH.NE__Name__c ='t' ;
	        insert catH;
			NE__Catalog__c cat = new NE__Catalog__c();
	        cat.NE__Catalog_Header__c = catH.Id;
	        insert cat;        
	        NE__Catalog_Item__c catItem = new NE__Catalog_Item__c();
	        catItem.NE__ProductId__c = prod.Id;
	        catItem.NE__Catalog_Id__c = cat.Id;
	        insert catItem;        
        
	        NE__OrderItem__c ordItem = new NE__OrderItem__c();
	        ordItem.NE__OrderId__c = order1.Id;
	        ordItem.NE__CatalogItem__c = catItem.Id;
	        ordItem.NE__Qty__c = 1;
	        ordItem.NE__ProdId__c = prod.Id;
	        insert ordItem;
			BI_Actualizaciones_integracion__c act_integra = new BI_Actualizaciones_integracion__c(
				Name = 'Test',
				BI_Country__c = lst_pais[0],
				BI_Entidad__c = 'Parque',
				BI_UltimaModificacion__c = '28/07/2014'
			);
			insert act_integra;
	    	system.runAs(user1){
				
			
	    		PageReference pageRef = new PageReference('PCA_Inventory');
	       		Test.setCurrentPage(pageRef);
				PCA_InventoryController tmpContr = new PCA_InventoryController();
                BI_TestUtils.throw_exception = false	;
				tmpContr.PageValue='10';
				tmpContr.checkPermissions();
				tmpContr.loadInfo();

				
				tmpContr.getItemPage();
				tmpContr.getHasPrevious();
				tmpContr.getHasNext();
				tmpContr.Previous();
				tmpContr.Next();
				tmpContr.valuesInPage();
				tmpContr.Order();
				tmpContr.searchRecords();
	    	}
	    	system.runAs(user1){
			
	    		PageReference pageRef = new PageReference('PCA_Inventory');
	       		Test.setCurrentPage(pageRef);
				PCA_InventoryController tmpContr = new PCA_InventoryController();
                BI_TestUtils.throw_exception = true	;
				tmpContr.PageValue='10';
				tmpContr.checkPermissions();
				tmpContr.loadInfo();
				tmpContr.getItemPage();
				tmpContr.getHasPrevious();
				tmpContr.getHasNext();
				tmpContr.Previous();
				tmpContr.Next();
				tmpContr.valuesInPage();
				tmpContr.Order();
				tmpContr.searchRecords();
	    	}
	    }

    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}