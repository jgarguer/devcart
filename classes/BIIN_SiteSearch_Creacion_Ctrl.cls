public class BIIN_SiteSearch_Creacion_Ctrl extends BIIN_UNICA_Base
{

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Ignacio Morales Rodríguez, Jose María Martín Castaño
	Company:       Deloitte
	Description:   Clase asociada al Pop Up de búsqueda de la Ubicación de BIIN_CreacionIncidencia
	    
	History:
	    
	<Date>            	<Author>                                            <Description>
	10/12/2015         Ignacio Morales Rodríguez, Jose María Martín         Initial version
    11/05/2016         José Luis González Beltrán                           Adapt to UNICA interface
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public String siteName {get;set;}
    public String siteId {get;set;}
    public String query {get;set;}
    public String accountName {get;set;}
    public List<Case> lcases {get;set;}
    public boolean BoolDeshabilitar {get;set;} 
    public integer queryLength {get;set;} 
    
    public boolean MostrarError {get;set;}
    public String errorMessage {get;set;}
    
    private transient HttpRequest req;
    public HttpRequest getHttpRequest() {
        return req;
    }
    
    private transient HttpResponse res;
    public HttpResponse getHttpResponse() {
        return res;
    }
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Jose María Martín Castaño
	    Company:       Deloitte
	    Description:   Constructor, donde se definen las variables globales y se toma el valor heredado de la ubicación de la página padre de creación. 
	    History:
	    
	    <Date>            <Author>          			<Description>
	    12/12/2015      Jose María Martín Castaño    	Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
        public BIIN_SiteSearch_Creacion_Ctrl()
        {	
            if(Test.isRunningTest()){
                siteName = '';
                siteId   = '';
            }
            MostrarError = false;
            query        = ApexPages.currentPage().getParameters().get('inputfield');
            accountName  = ApexPages.currentPage().getParameters().get('accountfield');
            queryLength  = query.length();
            if(queryLength < 3) {       
                BoolDeshabilitar = true;
                } else {
                    BoolDeshabilitar = false;  
                }
                system.debug(' BIIN_SiteSearch_Creacion_Ctrl | Busqueda: ' + query);
                lcases  = new List<Case>();
            }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Jose María Martín Castaño
	    Company:       Deloitte
	    Description:   Función que obtiene la lista de ubicaciones.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Jose María Martín Castaño      	Initial version
        --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public void getSite()
        {
            try {
            // Get account
            Account account = getAccount(accountName);

            // Callout URL
            String url = 'callout:BIIN_WS_UNICA/accountresources/v1/sites';
            url = BIIN_UNICA_Utils.addParameterAdditional(url, 'CompanyID', account.BI_Validador_Fiscal__c);
            url = BIIN_UNICA_Utils.addParameter(url, 'siteName', query);

            // Prepare Request
            HttpRequest req = getRequest(url, 'GET');
            

            BIIN_UNICA_Pojos.ResponseObject response = getResponse(req, BIIN_UNICA_Pojos.SitesListType.class);
            system.debug(' BIIN_SiteSearch_Creacion_Ctrl.getSite | La salida del servicio es (response): ' + response);

            BIIN_UNICA_Pojos.SitesListType sitesList = (BIIN_UNICA_Pojos.SitesListType) response.responseObject;
            system.debug(' BIIN_SiteSearch_Creacion_Ctrl.getSite | Lista de Sites disponibles (sitesList): ' + sitesList);
            Case caso = new Case();
            if (sitesList.totalResults > 0) 
            {
                // clean output list
                lcases = new List<Case>();
                for(BIIN_UNICA_Pojos.SiteType site : sitesList.sites) {
                    caso.BIIN_Site__c = site.siteDetails.siteName;
                    lcases.add(caso);
                    caso = new Case();
                }
            }
            else
            {
                MostrarError = true;
                errorMessage = Label.BIIN_NO_RESULTS_MODIFY_CRITERIA;
            }

            system.debug ('SITE-SUCCESS: Salida: ' + lcases);
        }
        catch(Exception e) 
        {
            system.debug ('SITE-ERROR' + e);

            MostrarError = true;
            errorMessage = e.getMessage();
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez	  
	    Company:       Deloitte
	    Description:   Función que cuenta el número de caracteres del campo de búsqueda para habilitar el botón Ir.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez	     	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        public void HabilitarIr()
        {
            queryLength = query.length();
            if(queryLength < 3) 
            {       
                BoolDeshabilitar= true;
                } else 
                {
                    BoolDeshabilitar= false;  
                } 
                system.debug('HA ENTRADO EN HabilitarIr---->' + BoolDeshabilitar + queryLength );
        }

        public void closePopUp()
        {
            MostrarError = false;
        }
}