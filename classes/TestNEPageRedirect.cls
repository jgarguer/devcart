@isTest
private class TestNEPageRedirect {
    
    static testMethod void NEPageRedirect_Test()
    {        
        BI_TestUtils.throw_exception = false;
        User authUs=TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        User us=TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        us.BI_Permisos__c='TGS';
        database.insert(us);
        
        
        
        RecordType RTHolding = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name LIKE '%Holding' LIMIT 1];
        Account accHold = new Account(Name='HoldingAccount', RecordTypeId=RTHolding.Id, TGS_Es_MNC__c = true);
        //insert accHold;
        
        RecordType RTCustomerCountry = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name LIKE '%Customer Country' LIMIT 1];
        Account accCustom = new Account(Name='CustomerCountryAccount', ParentId=accHold.Id, RecordTypeId=RTCustomerCountry.Id, TGS_Es_MNC__c = true);
        //insert accCustom;
        
        RecordType RTLegalEntity = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name LIKE '%Legal Entity' LIMIT 1];
        Account accLE = new Account(Name='LegalEntityAccount', ParentId=accCustom.Id, RecordTypeId=RTLegalEntity.Id, TGS_Es_MNC__c = true);
        //insert accLE;
        
        RecordType RTBusinessUnit = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name LIKE '%Business Unit' LIMIT 1];
        Account accBU = new Account(Name='BusinessUnitAccount', ParentId=accLE.Id, RecordTypeId=RTBusinessUnit.Id, TGS_Es_MNC__c = true);
        //insert accBU;
        
        NE__Catalog_Header__c catHeader = new NE__Catalog_Header__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Active__c=true, NE__Enable_Global_Cart__c=false, NE__Name__c='CatalogTest', NE__Start_Date__c=system.today()-1);
        //insert catHeader;
        
        NE__Catalog__c catalog = new NE__Catalog__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Active__c=true, NE__Catalog_Header__c = catHeader.Id, NE__Enable_for_oppty__c=false,  NE__Version__c=1.0, NE__Visible_web__c=false);
        //insert catalog;
        
        NE__Commercial_Model__c commModel = new NE__Commercial_Model__c(CurrencyIsoCode='ARS', Name='CatalogTest', NE__Catalog_Header__c=catHeader.Id, NE__Offline__c=false, NE__ProgramName__c='CatalogTest', NE__Status__c='Pending');
        //insert commModel;
        
        NE__Catalog_Category__c catCategory = new NE__Catalog_Category__c(Name='Parque Modification', CurrencyIsoCode='ARS', NE__CatalogId__c=catalog.Id);
		//insert catCategory;
        
        NE__Product__c prod = new NE__Product__c(Name='Product', CurrencyIsoCode='ARS');
        //insert prod;
        
        NE__Catalog_Item__c catItem = new NE__Catalog_Item__c(NE__ProductId__c=prod.Id, NE__Catalog_Id__c=catalog.Id, NE__Catalog_Category_Name__c=catCategory.Id, NE__Start_Date__c=system.today()-1, NE__BaseRecurringCharge__c=10.00, NE__Base_OneTime_Fee__c=100.00, NE__Min_Qty__c=0, NE__Max_Qty__c=99);
        //insert catItem;
        
        BI_Sede__c sede = new BI_Sede__c(BI_Direccion__c='Street', BI_Localidad__c='Locality', BI_Provincia__c='Province', BI_Codigo_postal__c='CP', Name='Street, (PC Locality) Province');
        //insert sede;
        
        BI_Punto_de_instalacion__c pdi = new BI_Punto_de_instalacion__c(Name='Automático', BI_Cliente__c=accBU.Id, BI_Sede__c=sede.Id);
        //insert pdi;
        
        Opportunity opp = new Opportunity(Name='Test', CloseDate=Date.today(), StageName=Label.BI_F6Preoportunidad, AccountId=accLE.Id, BI_Ciclo_ventas__c=Label.BI_Completo, BI_Duracion_del_contrato_Meses__c=36);
        //insert opp;
        
        RecordType RTOpty = [SELECT Id FROM RecordType WHERE SobjectType = 'NE__Order__c' AND Name = 'Opty' LIMIT 1]; 
        Test.startTest();
        System.runAs(us){
            insert accHold;
            accCustom.parentId=accHold.Id;
            insert accCustom;
            accLE.ParentId=accCustom.Id;
            insert accLE;
            accBU.ParentId=accLE.Id;
            insert accBU;
            insert catHeader;
            catalog.NE__Catalog_Header__c = catHeader.Id;
            insert catalog;
            commModel.NE__Catalog_Header__c=catHeader.Id;
            insert commModel;
            catCategory.NE__CatalogId__c=catalog.Id;
            insert catCategory;
            insert prod;
            catItem.NE__ProductId__c=prod.Id;
            catItem.NE__Catalog_Id__c=catalog.Id;
            catItem.NE__Catalog_Category_Name__c=catCategory.Id;
            insert catItem;
            insert sede;
            pdi.BI_Cliente__c=accBU.Id;
            pdi.BI_Sede__c=sede.Id;
            insert pdi;
            opp.AccountId=accLE.Id;
            Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(userInfo.getUserId(), true, false, false, false);
            insert opp;
            BI_MigrationHelper.disableBypass(mapa);
        }
        test.stopTest();
        
        //User authUs = [SELECT Id FROM User WHERE ContactId != null LIMIT 1];
		//User authUs = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_data.PortalType.PowerCustomerSuccess, null, true);
        //Profile prof = [SELECT Id FROM Profile WHERE Name LIKE 'TGS System Administrator' LIMIT 1]; 
	    //User us = [SELECT Id FROM User WHERE ProfileId =: prof.Id and IsActive = true LIMIT 1];
	    
        
//        Test.startTest();
        
//	    System.runAs(us) 
//	    {
//            //ord1 RT Opty
//            NE__Order__c ord1 = new NE__Order__c(CurrencyIsoCode='ARS', RecordTypeId=RTOpty.Id, NE__AccountId__c=accBU.Id, NE__BillAccId__c=accBU.Id, NE__CatalogId__c=catalog.Id, NE__CommercialModelId__c=commModel.Id, NE__ConfigurationStatus__c='Valid', NE__Configuration_Type__c='New', NE__One_Time_Fee_Total__c=1000.00, NE__OpportunityId__c=opp.Id, NE__OptyId__c=opp.Id, NE__OrderStatus__c='Active - Rápido', NE__Recurring_Charge_Total__c=400.00, NE__ServaccId__c=accBU.Id, NE__TotalRecurringFrequency__c='Monthly', NE__Type__c='InOrder');
//            insert ord1;
            
//            NE__OrderItem__c ordItem1 = new NE__OrderItem__c(NE__CatalogItem__c=catItem.Id, NE__ProdId__c=prod.Id, NE__OrderId__c=ord1.Id, NE__Qty__c=1, CurrencyIsoCode='ARS');
//            insert ordItem1;
            
//            //ord2 RT Order
//            NE__Order__c ord2 = new NE__Order__c(CurrencyIsoCode='ARS', NE__AccountId__c=accBU.Id, NE__BillAccId__c=accBU.Id, NE__CatalogId__c=catalog.Id, NE__CommercialModelId__c=commModel.Id, NE__ConfigurationStatus__c='Valid', NE__Configuration_Type__c='New', NE__One_Time_Fee_Total__c=1000.00, NE__OrderStatus__c='Active - Rápido', NE__Recurring_Charge_Total__c=400.00, NE__ServaccId__c=accBU.Id, NE__TotalRecurringFrequency__c='Monthly', NE__Type__c='InOrder');
//            insert ord2;
            
//            NE__OrderItem__c ordItem2 = new NE__OrderItem__c(NE__CatalogItem__c=catItem.Id, NE__ProdId__c=prod.Id, NE__OrderId__c=ord2.Id, NE__Qty__c=1, CurrencyIsoCode='ARS');
//            insert ordItem2;
            
            
//            ApexPages.StandardController contr = new ApexPages.StandardController(ord1);
//            ApexPages.currentPage().getParameters().put('orderId', ord1.Id);
//            NEPageRedirect pagered = new NEPageRedirect();
//            pagered.redirect();
            
//            NETriggerHelper.setTriggerFiredTest('NECheckOrderItems', false);
//            ordItem1.NE__OneTimeFeeOv__c = 10.00;
//            update ordItem1;

//			NETriggerHelper.setTriggerFiredTest('NECheckOrderItems', false);            
            
//            contr = new ApexPages.StandardController(ord2);
//            ApexPages.currentPage().getParameters().put('orderId', ord2.Id);
//            ApexPages.currentPage().getParameters().put('site', pdi.Id);
//            ApexPages.currentPage().getParameters().put('authUser', authUs.Id);
//            pagered = new NEPageRedirect();
//            pagered.redirect();
            
//            contr = new ApexPages.StandardController(ord2);
//            ApexPages.currentPage().getParameters().put('orderId', ord2.Id);
//            ApexPages.currentPage().getParameters().put('site', pdi.Id);
//            ApexPages.currentPage().getParameters().put('authUser', authUs.Id);
//            ApexPages.currentPage().getParameters().put('createOpty',opp.Id);
//            pagered = new NEPageRedirect();
//            pagered.redirect();
            
            
//            NETriggerHelper.setTriggerFiredTest('NECheckOrderItems', false);
//            ordItem2.NE__OneTimeFeeOv__c = 10.00;
//            update ordItem2;
        
///*      Comentado porque se ejecutan muchas SOQL para poder descomentarlo hay que revisar las SOQL anteriores y agruparlas
// * 		DElimitar correctamente el Test del resto y este usuario obtenerlo junto con el otro
// * 		Falla en los trigguers de Case alcanzando el maximo
//        prof = [SELECT Id FROM Profile WHERE Name LIKE 'TGS System%' LIMIT 1]; 
//	    us = [SELECT Id FROM User WHERE ProfileId =: prof.Id and IsActive = true LIMIT 1];
        
//        Case c = new Case(AccountId=accBU.Id, Type='Change', Status = 'Assigned');
//        insert c;
        
//	    NE__Order__c ord = new NE__Order__c(OwnerId=us.Id, Case__c=c.Id, CurrencyIsoCode='ARS', RecordTypeId=RTOpty.Id, NE__AccountId__c=accBU.Id, NE__BillAccId__c=accBU.Id, NE__CatalogId__c=catalog.Id, NE__CommercialModelId__c=commModel.Id, NE__ConfigurationStatus__c='Valid', NE__Configuration_Type__c='New', NE__One_Time_Fee_Total__c=1000.00, NE__OpportunityId__c=opp.Id, NE__OptyId__c=opp.Id, NE__OrderStatus__c='Active - Rápido', NE__Recurring_Charge_Total__c=400.00, NE__ServaccId__c=accBU.Id, NE__TotalRecurringFrequency__c='Monthly', NE__Type__c='InOrder');
//		insert ord;
            
//        NE__OrderItem__c ordItem = new NE__OrderItem__c(NE__CatalogItem__c=catItem.Id, NE__ProdId__c=prod.Id, NE__OrderId__c=ord.Id, NE__Qty__c=1, CurrencyIsoCode='ARS');
//        insert ordItem;
//*/
//        }
//        Test.stopTest();
    }

}