global class BI_FOCOScheduler implements Schedulable {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Bejoy Babu
    Company:       Salesforce.com
    Description:   Schedulable class that schedules the load of Registro Datos FOCO table from staging table.
    
    History:
    
    <Date>            <Author>              <Description>
    09/26/2014        Bejoy Babu           Initial version
    26/11/2015        Micah Burgos         Fix bugs from picklist Code refactoring. eHelp 01423701. 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/



    global void execute(SchedulableContext sc) {
 		System.debug('-------------------------------------------BI_FOCOScheduler.execute(): Start processing staging table records');
    	Map<String, BI_FOCO_Pais__c> mcs = BI_FOCO_Pais__c.getAll();
    	Set<String> allCountries = mcs.keySet();
    	Set<String> countries = new Set<String>();

    	//check which countries got loaded
    	//AggregateResult[] ars = [Select count(Id), BI_Pais__c from BI_Registros_Datos_FOCO_Stage__c where BI_Pais__c IN :allCountries group by BI_Pais__c];
        map<string, BI_FOCO_Stage_Countries__c> m = BI_FOCO_Stage_Countries__c.getAll();

        countries = m.keySet();
    	//for(AggregateResult ar: ars ){
    	//	countries.add((String)ar.get('BI_Pais__c'));
    	//}

        list<BI_FOCO_Stage_Countries__c> toDelete = new list<BI_FOCO_Stage_Countries__c>();

    	Integer count=1;
    	for(String country: countries){
			//Check if schedulable job limit has been reached before starting to process next country
			Integer batchJobCount = [SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')];
			if ( batchJobCount <= 1){

				//Call the Staging Table Load class and let it do its stuff
				BI_FOCOStagingManager mgr = new BI_FOCOStagingManager(country);

				try{
					
					mgr.processStageToMain();

                    if (m.get(country) != null && m.get(country).id != null) {
                        toDelete.add(new BI_FOCO_Stage_Countries__c(id=m.get(country).id));
                    }
				} catch (Exception e){
					System.debug('-------------------------------------------Exception in BI_FOCOScheduler.execute()');
					notifyByEmail(e.getMessage(), e.getStackTraceString(), e.getLineNumber(), e.getTypeName(), country);
				}
			} 
			else{
				
				//space each country schedule with a multiplier of 30 mins
				Datetime dt = Datetime.now().addMinutes(30*count++); // i.e. 30 mins
				String timeForScheduler = dt.format('s m H d M \'?\' yyyy');
				BI_FOCOStagingManager mgr = new BI_FOCOStagingManager(country);
				System.debug('-------------------------------------------Scheduling load for '+country+' at timeForScheduler');
				try{
					Id scId = System.Schedule('FOCORetry'+timeForScheduler,timeForScheduler,mgr);

                    if (m.get(country) != null && m.get(country).id != null) {
                        toDelete.add(new BI_FOCO_Stage_Countries__c(id=m.get(country).id));
                    }
				}
				catch(Exception e){
					System.debug('-------------------------------------------Exception in BI_FOCOScheduler.execute()');
					notifyByEmail(e.getMessage(), e.getStackTraceString(), e.getLineNumber(), e.getTypeName(), country);
				}
			}
		}

        if (!toDelete.isEmpty()) {
            delete toDelete;
        }
	}

	public static void notifyByEmail(String eMsg, String eStrc, Integer eLn, String eTy, String c) {
    	String orgId = UserInfo.getOrganizationId();
    	String orgName = UserInfo.getOrganizationName();
    	String user = UserInfo.getUserName();
    	String message = eMsg;
    	String stacktrace = eStrc;
    	String exType = '' + eTy;
    	String line = '' + eLn;
    	String theTime = '' + System.now();

    	String subject = String.format('Batch Process Failed for {0}', new List<String>{ c });
    	String body = String.format('Time: {0}\nMessage: {1}\nStacktrace: {2}\nLine: {3}', new List<String>{ theTime, message, stacktrace, line });

    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

    	Map<String, BI_FOCO_Pais__c> mcs = BI_FOCO_Pais__c.getAll();
    	String toList = mcs.get(c).Notify_Email_List__c;
    	if(toList != null){

    		String[] toAddresses = toList.split(',', -1) ;
    		mail.setToAddresses(toAddresses);
    		mail.setSubject(subject);
    		mail.setUseSignature(false);
    		mail.setPlainTextBody(body);

            //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); //Check límits and target to userId 
            BI_SendMails_Helper.sendMails(new Messaging.SingleEmailMessage[] { mail }, false, 'FOCO');
    	}
	}
}