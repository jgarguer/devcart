@isTest
private class PCA_HomeCompController_TEST {

    static testMethod void myUnitTest() {
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
        system.debug('lst_pais: '+lst_pais);
        List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
              item.OwnerId = usr.Id;
              accList.add(item);
            }
            update accList;
            
           List<Contact> con = BI_DataLoad.loadContacts(2, accList);
           User user1 = new User (alias = 'stttt',
                                email= '0@testorg.com',
                                emailencodingkey='UTF-8',
                                lastname='Testing',
                                languagelocalekey='en_US',
                                localesidkey='en_US',
                                isActive = true,
                                CommunityNickname = '0testUser123',
                                ProfileId = BI_DataLoad.searchPortalProfile(),
                                BI_Permisos__c = Label.BI_PortalPlatino,
                                ContactId = con[0].Id,
                                timezonesidkey=Label.BI_TimeZoneLA,
                                username= '00@testorg.com');
                                //Country_Region__c=lst_pais[0].Id);     
        insert user1;
        User user2 = new User (alias = 'stttt',
                                email= '1@testorg.com',
                                emailencodingkey='UTF-8',
                                lastname='Testing',
                                languagelocalekey='en_US',
                                localesidkey='en_US',
                                isActive = true,
                                CommunityNickname = '1testUser123',
                                ProfileId = BI_DataLoad.searchPortalProfile(),
                                BI_Permisos__c = Label.BI_PortalPlatino,
                                ContactId = con[1].Id,
                                timezonesidkey=Label.BI_TimeZoneLA,
                                username= '10@testorg.com',
                                pais__c=lst_pais[0]);     
        insert user2;
       Date day = date.newInstance(2014, 9, 15);                                
       BI_ImageHome__c image1 = new BI_ImageHome__c(BI_Url__c='www.gml.com',Name='Image1',BI_Fecha_Inicio__c=day, 
                                                    BI_Country__c=lst_pais[0]);
       insert image1;
       
       
       Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');                        
       Attachment attach=new Attachment(Name='Unit Test Attachment',
                                        body=bodyBlob,
                                        parentId=image1.Id, 
                                        ContentType='image/png');  
        insert attach;
                                     
        system.runAs(user1){
            BI_TestUtils.throw_exception = false;
            PCA_HomeCompController home1 = new PCA_HomeCompController();
            system.assertEquals(attach.Name,[SELECT Name FROM Attachment where Name='Unit Test Attachment'][0].Name);
            
            
        }
         system.runAs(user1){
            BI_TestUtils.throw_exception = true;
            PCA_HomeCompController home1 = new PCA_HomeCompController();
            
            List<PCA_HomeCompController.ImageHomeWrapper> lstihw=home1.lImageHomeWrapper;
             
             
        }
        system.runAs(user2){
            BI_TestUtils.throw_exception = false;
            PCA_HomeCompController home1 = new PCA_HomeCompController();
            
            
        }
        }
    }
    
    static testMethod void myUnitTest2() {
        PCA_HomeCompController controller = new PCA_HomeCompController();
        if((controller.esTGS = false) || controller.esTGS) {}
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}