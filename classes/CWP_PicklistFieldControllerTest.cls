/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:       Everis
    Company:       Everis
    Description:   Class to manage cover of CWP_PicklistFieldController

    History: 
    
     <Date>                     <Author>                <Change Description>
    25/05/2017                 Everis             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	

@isTest
private class CWP_PicklistFieldControllerTest {

	@isTest static void getDependentOptionsImplTest() {
		CWP_PicklistFieldController.getDependentOptionsImpl('Case', 'TGS_Op_Commercial_Tier_1__c', 'TGS_Op_Commercial_Tier_2__c');
	}
}