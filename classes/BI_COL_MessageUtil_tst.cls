/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-23      Daniel ALexander Lopez (DL)     Create Class      
*************************************************************************************/
@isTest
private class BI_COL_MessageUtil_tst
{
	
	public static testMethod void test_method_one()
	{
		Test.startTest();

		BI_COL_MessageUtil_cls.mostrarMensaje('ERROR','test');
		BI_COL_MessageUtil_cls.mostrarMensaje('INFO','test');
		BI_COL_MessageUtil_cls.mostrarMensaje('WARNING','test');
		BI_COL_MessageUtil_cls.mostrarMensaje('FATAL','test');
		BI_COL_MessageUtil_cls.mostrarMensaje('CONFIRM','test');

		Test.stopTest();

	}

	public static testMethod void test_method_two()
	{
		Test.startTest();

		BI_COL_Useful_messages_cls.mostrarMensaje('ERROR','test');
		BI_COL_Useful_messages_cls.mostrarMensaje('INFO','test');
		BI_COL_Useful_messages_cls.mostrarMensaje('WARNING','test');
		BI_COL_Useful_messages_cls.mostrarMensaje('FATAL','test');
		BI_COL_Useful_messages_cls.mostrarMensaje('CONFIRM','test');

		Test.stopTest();

	}
}