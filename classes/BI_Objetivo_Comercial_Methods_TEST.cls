@isTest
private class BI_Objetivo_Comercial_Methods_TEST 
{
	final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    static
    {
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'BI_Objetivo_Comercial__c'])
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Humberto Nunes	
    Company:       Accenture.com
    Description:   Class For Test BI_Objetivo_Comercial_Methods Class

    History: 
    
    <Date>                      <Author>                 <Change Description>
    20/02/2018                  Humberto Nunes           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	@isTest static void validateFieldType_TEST() 
	{
		BI_Objetivo_Comercial__c OC = new BI_Objetivo_Comercial__c();
		OC.RecordTypeId = MAP_NAME_RT.get('BI_OBJ_Objetivos_Jerarquicos');
		OC.BI_Tipo__c = 'Oportunidad';
		OC.BI_OBJ_Campo__c = '';
		OC.BI_FVI_Unidad_Medida__c = 'Cantidad';

 		// CAMPO API NO INIDCADO 
        try 
        {
            insert OC;
        }
        catch(Exception e) 
        {
            System.assert(e.getMessage().contains('Debe rellenar el campo "Api del Campo"'));
        }

        // CAMPO API INEXISTENTE
       	OC.BI_OBJ_Campo__c = 'XXX';
 		try 
        {
            insert OC;
        }
        catch(Exception e) 
        {
            System.assert(e.getMessage().contains('El campo "Api del Campo" indicado NO EXISTE en el objeto "Opportunity".'));
        }

        // CAMPO NO NUNMERICO NI DIVISA
       	OC.BI_OBJ_Campo__c = 'Name';
 		try 
        {
            insert OC;
        }
        catch(Exception e) 
        {
            System.assert(e.getMessage().contains('Solo son admitidos'));
        }

        // CAMPO NO NUNMERICO NI DIVISA
       	OC.BI_FVI_Unidad_Medida__c = 'Moneda';
 		try 
        {
            insert OC;
        }
        catch(Exception e) 
        {
            System.assert(e.getMessage().contains('Solo son admitidos'));
        }
	}	
}