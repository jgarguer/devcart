/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:		Carlos Carvajal
	Company:	   Salesforce.com
	Description:   
	
	History:
	
	<Date>			<Author>		  <Description>
	27/03/2015		Carlos Carvajal	Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
public with sharing class BI_COL_ManagementMashup_ctr 
{
	public string urlTrs{get;set;}
	public Boolean visible{get;set;}
	List<Opportunity> lstOport;
	list<User> usu;

	public void consultaCotizacionesTRS()
	{ 
		usu=new list<User>([Select id,BI_Peru_STC_Per__c,CompanyName 
									 from User where id=: userinfo.getuserid() limit 1]);
		BI_COL_ServiciosWeb__c endpoints = BI_COL_ServiciosWeb__c.getOrgDefaults();
		urlTrs=endpoints.URL_COTIZADOR__c;
		String Id = Apexpages.currentPage().getParameters().get('Id');
		List<Account> lstcl=null;
		if(Id!='' && Id!=null)
		{
			lstcl=[Select Id,BI_No_Identificador_fiscal__c from Account o where o.id=:Id limit 1];
			
			if(lstcl.isEmpty())
			{
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'No se encontro cliente') );
				visible=false;
			}
			
		}
		else
		{
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'No se encontro cliente') );
			visible=false;	  
		}
		set<String> permiSet=BI_COL_ManagementMashup_ctr.getPermisionSetUsuario(usu[0].id);
		System.debug('\n\n '+usu[0].BI_Peru_STC_Per__c+'  '+permiSet+' \n\n');
		if(usu[0].BI_Peru_STC_Per__c==null || permiSet.isEmpty())
		{
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'No se obtuvo el id de usuario del Asesor en TRS') );
			visible=false;
			//Es asesor
		}	//Gerente de Ventas o Asesor Ventas
		else if(permiSet!=null && permiSet.contains('BI_COL_Desarrollo_Comercial') || permiSet.contains('BI_COL_Jefe_de_Ventas'))
		{
			//Es asesor
			visible=true;
			String idtRS=usu[0].BI_Peru_STC_Per__c;
			try{

			    urlTrs+=strMD5DateTime+caracteresAleatorios(9,idtRS.length(),false)+idtRS;
				
				}catch(Exception e)
				{
					System.debug('Error prueba '+e.getMessage());
				}
		}	// Administracion de Ventas o Administrador del sistema o Admon Ventas Master
		else if(permiSet!=null /*&& permiSet.contains('BI_COL_Perfil_Admin_Ventas_Master') || permiSet.contains('BI_COL_Perfil_Administrador_de_Ventas') 
			|| permiSet.contains('BI_COL_Perfil_Administrador_del_sistema')*/)
		{
			//Es Administracion de Ventas
			visible=true;
			urlTrs+=strMD5DateTime;//strClie
		}
		else
		{
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'No tiene permisos para visualizar esta pagina') );
			visible=false;
			/*visible=true;
			String idtRS=usu[0].BI_Peru_STC_Per__c;
			urlTrs+=strMD5DateTime+caracteresAleatorios(9,idtRS.length(),false)+idtRS;
			System.debug('\n\n'+urlTrs+'\n\n');	*/
		}
	
	
	}
	
	
	public void crearCotizacionTRS()
	{
		list<User> usu=new list<User>([Select id,BI_Peru_STC_Per__c,CompanyName from User where id=: userinfo.getuserid() limit 1]);
		//Asesor Ventas 00e30000000mR0D, Administracion de Ventas 00e30000000mODYAA2 00e50000000nuOfAAI
		String Id = Apexpages.currentPage().getParameters().get('Id');
		BI_COL_ServiciosWeb__c endpoints = BI_COL_ServiciosWeb__c.getOrgDefaults();
		urlTrs=endpoints.URL_COTIZADOR__c;
		if(Id!='' && Id!=null)
		{
			lstOport =[Select Id,StageName,CreatedDate,Account.BI_No_Identificador_fiscal__c,Account.BI_Segment__c,BI_Numero_id_oportunidad__c, Account.BI_COL_Segmento_Telefonica__c
											from Opportunity o where o.id=:Id limit 1];
			if(lstOport.isEmpty())
			{
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'No se encontro oportunidad') );
				visible=false;		  
			}
			
		}
		else
		{
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'No se encontro oportunidad') );
			visible=false;	  
		}
		
		//VALIDA PERFILES
		System.debug('Perfiles: '+userinfo.getProfileId());
		if(usu[0].BI_Peru_STC_Per__c==null)
		{
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'No se obtuvo el id de usuario de TRS') );
			visible=false;
		//Es asesor va a cotizar
		}
		else
		{
			visible=true;
			//Validacion de la fecha de creacion de la Oportunidad
			Datetime myDateTime = Datetime.newInstance(2014, 04, 11,19,0,0);
			System.debug('\n\n ID TRS'+usu[0].BI_Peru_STC_Per__c+'\n\n');
			String strUserTrs=usu[0].BI_Peru_STC_Per__c;
			strUserTrs=caracteresAleatorios(9,strUserTrs.length(),false)+strUserTrs;
			if(lstOport[0].CreatedDate>myDateTime)
			{
				urlTrs+=strMD5DateTime+strUserTrs+strClie2+strEtpOportunidad(lstOport[0].StageName)+strOportunidad+strSegCliente(lstOport[0].Account.BI_Segment__c);
			}
			else
			{
				urlTrs+=strMD5DateTime+strUserTrs+strClie2+strEtpOportunidad(lstOport[0].StageName)+strOportunidad+strSegCliente(lstOport[0].Account.BI_Segment__c);
				//urlTrs+='cotizadorCRUD.php?'+strMD5DateTime+strUserTrs+strClie2+strEtpOportunidad+strOportunidad+usu[0].CompanyName;
			}
			
			lstOport[0].BI_COL_Cotizador__c=userinfo.getUserId();
			System.debug('\n\n +urlTr= '+urlTrs+ '\n\n' );
			try
			{
				update lstOport[0];
			}
			catch(Exception e)
			{
			
			}
		}
	}
	/*
	EP= Email Protection
	MM= M2M
	SM= SMDM
	TS= Troncal SIP
	ID=Internet Diferencial
	*/
	public void productMashup()
	{
		BI_COL_ServiciosWeb__c endpoints = BI_COL_ServiciosWeb__c.getOrgDefaults();
		string product=ApexPages.currentPage().getParameters().get('product');
		String msId = Apexpages.currentPage().getParameters().get('msInfo');
		System.debug('\n\n lstModServ[0].Name= '+msId+'\n\n');
		//,m.Producto_Telefonica__r.Cod_Desc_Referencia__c 
		List<BI_COL_Modificacion_de_Servicio__c> lstModServ =[Select Name, Id, BI_COL_Clasificacion_Servicio__c, BI_COL_Codigo_unico_servicio__r.name, BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c,
												BI_COL_Codigo_unico_servicio__r.BI_COL_Codigo_paquete__c,BI_COL_Oportunidad__r.StageName, BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c
                                                from BI_COL_Modificacion_de_Servicio__c where ID=:msId limit 1];
		if(product=='EP')
		{
			visible=true;
			BI_COL_EMAIL_PROTECTION__c confCodDescReferencia=BI_COL_EMAIL_PROTECTION__c.getInstance(lstModServ[0].BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c);
			String codDesReferencia=confCodDescReferencia!=null?confCodDescReferencia.BI_COL_CodigoEmailProtection__c:'1';
			
				urlTrs=endpoints.URL_EMAIL_PROTECTION_TRS__c+caracteresAleatorios(8,0,true)+
					strMD5DateTime+
					caracteresAleatorios(20,lstModServ[0].BI_COL_Codigo_unico_servicio__r.name.length(),false) +lstModServ[0].BI_COL_Codigo_unico_servicio__r.name+
					caracteresAleatorios(30,lstModServ[0].Name.length(),false)+lstModServ[0].Name;
					if(!Test.isRunningTest())
					{
						urlTrs += caracteresAleatorios(10,lstModServ[0].BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c.length(),false)+lstModServ[0].BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c;
					}
					urlTrs += codDesReferencia;
			
		}
		else if(product=='MM')
		{
			BI_COL_EMAIL_PROTECTION__c confCodDescReferencia=BI_COL_EMAIL_PROTECTION__c.getInstance(lstModServ[0].BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c);
			System.debug('Prueba ---> '+lstModServ[0].BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c);
			if(confCodDescReferencia!=null && lstModServ[0].BI_COL_Codigo_unico_servicio__r.BI_COL_Codigo_paquete__c !=null)
			{
				visible=true;
				if(confCodDescReferencia.Tipo__c=='Equipos')
				{
					urlTrs=endpoints.URL_M2M_EQUIPOS__c+caracteresAleatorios(8,0,true)+strMD5DateTime+
							caracteresAleatorios(20,lstModServ[0].BI_COL_Codigo_unico_servicio__r.BI_COL_Codigo_paquete__c.length(),false)+lstModServ[0].BI_COL_Codigo_unico_servicio__r.BI_COL_Codigo_paquete__c;
				}
				else if(confCodDescReferencia.Tipo__c=='Aplicaciones')
				{
					urlTrs=endpoints.URL_M2M_APLICACIONES__c+caracteresAleatorios(8,0,true)+strMD5DateTime+
					caracteresAleatorios(20,lstModServ[0].BI_COL_Codigo_unico_servicio__r.name.length(),false) +lstModServ[0].BI_COL_Codigo_unico_servicio__r.name+
					caracteresAleatorios(30,lstModServ[0].Name.length(),false)+lstModServ[0].Name;
					if(!Test.isRunningTest())
					{
						urlTrs += caracteresAleatorios(20,lstModServ[0].BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c.length(),false)+ lstModServ[0].BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c; 
					}
					
				}
			}
		}
		else if(product=='SM')
		{
			visible=true;
			urlTrs=endpoints.URL_SMDM__c+strMD5DateTime+
					caracteresAleatorios(27,lstModServ[0].Name.length(),false)+lstModServ[0].Name+
					caracteresAleatorios(15,strEtpOportunidad(lstModServ[0].BI_COL_Oportunidad__r.StageName).length(),false)+strEtpOportunidad(lstModServ[0].BI_COL_Oportunidad__r.StageName);
		}
		else if(product=='TS')
		{
			visible=true;
			urlTrs=endpoints.URL_TRONCAL_SIP__c+caracteresAleatorios(8,0,true)+strMD5DateTime+
					caracteresAleatorios(20,lstModServ[0].BI_COL_Codigo_unico_servicio__r.name.length(),false) +lstModServ[0].BI_COL_Codigo_unico_servicio__r.name+
					caracteresAleatorios(12,lstModServ[0].BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c.length(),false)+
					lstModServ[0].BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c+
					caracteresAleatorios(50,lstModServ[0].BI_COL_Clasificacion_Servicio__c.length(),false)+
					lstModServ[0].BI_COL_Clasificacion_Servicio__c;

		}
		else if(product=='ID')
		{
			visible=true;
			urlTrs=endpoints.URL_INTERNETDIFERENCIAL__C+caracteresAleatorios(8,0,true)+strMD5DateTime+
			caracteresAleatorios(20,lstModServ[0].BI_COL_Codigo_unico_servicio__r.name.length(),false) +lstModServ[0].BI_COL_Codigo_unico_servicio__r.name+
			caracteresAleatorios(50,lstModServ[0].BI_COL_Clasificacion_Servicio__c.length(),false) +lstModServ[0].BI_COL_Clasificacion_Servicio__c;
			/*List<BI_COL_Modificacion_de_Servicio__c> lstModServ =[Select Name, Id, BI_COL_Clasificacion_Servicio__c, BI_COL_Codigo_unico_servicio__r.name, BI_COL_Producto__r.NE__ProdId__r.BI_COL_LegadoID__c,
												BI_COL_Codigo_unico_servicio__r.BI_COL_Codigo_paquete__c,BI_COL_Oportunidad__r.StageName, BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c
                                                from BI_COL_Modificacion_de_Servicio__c where BI_COL_Codigo_unico_servicio__r.ID=:msId];*/

		}
		System.debug('\n\n urlTrs = '+urlTrs+'\n\n');
	}

	public String strClie2
	{
		get
		{
			String cons=lstOport[0].Account.BI_No_Identificador_fiscal__c;
			return caracteresAleatorios(15,cons.length(),false)+cons;
		}
		set;
	}
	
	public String strOportunidad
	{
		get
		{
			String cons=lstOport[0].BI_Numero_id_oportunidad__c;
			return caracteresAleatorios(10,cons.length(),false)+cons;
		}
		set;
	}

	public static String strEtpOportunidad(String st)
	{
			
		if(st=='F1 - Cancelled | Suspended') //Validar Cerrada Perdida
		{
			st='CP';
		}
		else
		{
			st=st.substring(0, 2);
		}
		return st;
	}
	
	public String strSegCliente(String segmento)
	{
		String sgtr=segmento!=Null?segmento:'';
		return caracteresAleatorios(32,sgtr.length(),false)+sgtr;
	}

	public String strMD5DateTime
	{
		get
		{
			Datetime dtNow = System.now();
			BI_COL_Utility_cls util = new BI_COL_Utility_cls();
			System.debug( '*-*-*-*-*- dtNow.format( yyyy/MM/dd HH:00 ): ' + dtNow.format( 'yyyy/MM/dd HH:00')+' dtNow  '+dtNow+ '  ' +dtNow.dateGmt());
			strMD5DateTime = EncodingMD5( dtNow.format( 'yyyy/MM/dd HH:00' ,'America/Bogota') );
			System.debug( '*-*-*-*-*- strMD5DateTime: ' + strMD5DateTime );
			return strMD5DateTime+caracteresAleatorios(35,strMD5DateTime.length(),false);
		}
		set;
	}

	//maximo devuelve 32 caracteres
	public string caracteresAleatorios(integer total,integer ct,Boolean al)
	{
		integer retorno= total- ct;
		if(retorno<=0)return '';		
		if(al)
		{
			return EncodingMD5(''+Math.random()).subString(0,retorno);
		}
		string zero='0000000000000000000000000000000000000000000000000000';
		return zero.subString(0,retorno);
	}
	
	public static String encodingMD5(String cadena)
	{
		
		Blob bPrehash = Blob.valueof(cadena);
		Blob bHexPrehash = Blob.valueOf(bPrehash.toString());
		Blob bsig = Crypto.generateDigest('MD5', bPrehash);
		String result = EncodingUtil.convertToHex(bsig);
		return result;
	}

	public static set<String> getPermisionSetUsuario(String idUsuario)
	{
		Set<String> usuPermision=new Set<String>();
		for( PermissionSetAssignment psa:[select id,PermissionSet.Name from PermissionSetAssignment where AssigneeId=:idUsuario])
		{
			usuPermision.add(psa.PermissionSet.Name);
		}
		return usuPermision;
	}
}