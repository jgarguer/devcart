/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Eduardo Ventura
Company:						Everis España
Description:					Mock para los servicios de integración del Fullstack.

History:
<Date>							<Author>						<Change Description>
24/04/2017						Eduardo Ventura					Versión inicial
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class FS_CORE_Fullstack_Mock implements HttpCalloutMock{
    /* SALESFORCE VARIABLES */
    private static final FS_CORE_Integracion_Mock__c ORGConfigurationMock = FS_CORE_Integracion_Mock__c.getInstance();
    public static Integer method;
    
    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:			Eduardo Ventura
	Company:		Everis España
	Description:	Mock
	
	History:
	
	<Date>						<Author>						<Change Description>
	24/04/2017					Eduardo Ventura					Initial Version.
	------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global static HttpResponse respond (HttpRequest request){
        /* Variable Configuration */
        HttpResponse response = new HttpResponse();
        
        if(!ORGConfigurationMock.FS_CORE_Server_Status__c){
            /* Header */
            response.setStatusCode(200);
            
            if(method == 1 && ORGConfigurationMock.FS_CORE_Method_1_Status__c){
                if(ORGConfigurationMock.FS_CORE_Method_1_Exist__c) 
                    response.setBody('{"GetCustomersListResponse":{"customersList":{"customerList":[{"customerId":"14785AAE1","customerDetails":{"correlatorId":"0012600000S3bfn","customerName":"Telefonica","description":"Example.","parentCustomer":"0012600000Q8BQUBB3","customerStatus":"active","segment":"Empresas","subsegment":"Masivo","creditProfiles":[{"creditProfileDate":"2017-01-20T10:53:58.223Z","creditRiskRating":2}],"additionalData":[{"key":"DenominacionComercial","value":"TEF"},{"key":"FaxEmpresa","value":"1-415-555-1212"},{"key":"TelefonoReferencia","value":"917358987"},{"key":"Sector","value":"Comunicaciones y teconología"},{"key":"Holding","value":"Cliente"},{"key":"CuentaGlobal","value":"True"},{"key":"Ambito","value":"Privado"},{"key":"Subsector","value":"Gobierno Nacional"},{"key":"Website","value":"www.telefonica.com"},{"key":"FechaActivacion","value":"2017-01-20T10:53:58.223Z"},{"key":"FechaDesactivacion","value":"2017-01-30T10:53:58.223Z"}]},"legalid":[{"country":"Argentina","nationalIDType":"NIF","nationalID":"000000000X"}],"customerAddresses":[{"addressName":"Avenida Manoteras","addressNumber":{"value":"184"},"locality":"Madrid","region":"Madrid","country":"España","postalCode":"28050"}],"contactingModes":[{"contactMode":"email","details":"emailprueba@gmail.com"}]}],"totalResults":1}}}');
                else response.setBody('{"GetCustomersListResponse":{"customerList":[]},"totalResults":0}');
            }
            else if(method == 2 && ORGConfigurationMock.FS_CORE_Method_2_Status__c){response.setHeader('Location', 'https://{apiRoot}/customers/14785AAE1');}
            else if(method == 3 && ORGConfigurationMock.FS_CORE_Method_3_Status__c) {/* Unanswered */}
            else if(method == 4 && ORGConfigurationMock.FS_CORE_Method_4_Status__c) {response.setHeader('Location', 'https://{apiRoot}/contacts/44785AAQ1');}
            else if(method == 5 && ORGConfigurationMock.FS_CORE_Method_5_Status__c) {/* Unanswered */}
            else if(method == 6 && ORGConfigurationMock.FS_CORE_Method_6_Status__c) {/* Unanswered */}
            else if(method == 7 && ORGConfigurationMock.FS_CORE_Method_7_Status__c) {/* Unanswered */}
            else if(method == 8 && ORGConfigurationMock.FS_CORE_Method_8_Status__c) {/* Unanswered */}
            else if(method == 9 && ORGConfigurationMock.FS_CORE_Method_9_Status__c) {/* Unanswered */}
            else {
                response.setStatusCode(400);
                response.setBody('{"Error":{"code":{"service" :"0","operation":"0","layer":null,"tamSystem":null,"legacySystem":null,"api":null,"error":null},"description":"Generic Mock Error","details":{"timeStamp":null,"cause":null}}}');
            }
        } else {
            response.setStatusCode(500);
            response.setBody('{"Error":{"code":{"service" :"0","operation":"0","layer":null,"tamSystem":null,"legacySystem":null,"api":null,"error":null},"description":"Generic Mock Error","details":{"timeStamp":null,"cause":null}}}');
        }
        
        return response;
    } 
}