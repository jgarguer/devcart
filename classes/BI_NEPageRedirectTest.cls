@isTest
private class BI_NEPageRedirectTest{
 
    static testMethod void unitTest1()
    {
       try{
           NE.NewConfigurationController ncc;
          ncc= new NE.NewConfigurationController();
          Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
          User u = [SELECT id FROM User Where ProfileId=:p.id AND IsActive=true LIMIT 1];
          System.runAs(u) {
        
        ////////
             Datetime myT = datetime.newInstance(2012, 5, 1, 12, 30, 2);
        
            NE__Catalog_Header__c ch=new NE__Catalog_Header__c(NE__Name__c='myUnitTest');
            insert ch;
            System.debug('insert ch1');//
            
            NE__Catalog__c ct = new NE__Catalog__c(Name='myUnitTest', NE__StartDate__c=myT, NE__Catalog_Header__c=ch.id);
            insert ct;
            System.debug('insert ct1');//

            
            NE__Catalog_Category__c categoria = new NE__Catalog_Category__c(Name = ct.Name,NE__CatalogId__c = ct.Id);
            insert categoria;
            System.debug('insert categoria1');//
            NE__Commercial_Model__c commodel = new NE__Commercial_Model__c(Name = ct.Name, NE__Catalog_Header__c = ct.NE__Catalog_Header__c,
            NE__ProgramName__c = NE.JS_RemoteMethods.SFDC_HTMLENCODE(ct.Name.replaceAll(' ','_')));
            insert commodel;
            System.debug('insert commodel1');//
            NE__Catalog_Category__c cc=new NE__Catalog_Category__c(Name='Parque Modification',NE__CatalogId__c=ct.id, NE__Parent_Category_Name__c = categoria.Id);
            insert cc;

            System.debug('insert cc1');//
            NE__Product__c prod1=new NE__Product__c(Name='prod1');
            insert prod1;
            NE__Product__c prod2=new NE__Product__c(Name='prod2');
            insert prod2;
            NE__Product__c prod3=new NE__Product__c(Name='prod3');
            insert prod3;
            NE__Product__c prod4=new NE__Product__c(Name='prod4');
            insert prod4;       
            NE__Product__c prod5=new NE__Product__c(Name='prod5');
            insert prod5;
            System.debug('insert prods1');//
            List<NE__Catalog_Item__c> lisct = new List<NE__Catalog_Item__c>();  
            NE__Catalog_Item__c item= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod1.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item;
            lisct.add(item);
            NE__Catalog_Item__c item2= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod2.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item2;
            lisct.add(item2);
            NE__Catalog_Item__c item3= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod3.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item3;
            lisct.add(item3);
            NE__Catalog_Item__c item4= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod4.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item4;
            lisct.add(item4);
            NE__Catalog_Item__c item5= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod5.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item5;
            lisct.add(item5);
            //////
        
        
        
            BI_NEPageRedirect  NePR;
            RecordType rt= [SELECT CreatedById,DeveloperName,Id,Name,NamespacePrefix,SobjectType
              FROM RecordType 
                WHERE (SobjectType=:'NE__Order__c' or SobjectType=:'Order__c') AND Name='Opty' LIMIT 1];
                
            Account acc = new Account(Name='accTest');
            insert acc;
            String probab = Opportunity.BI_Probabilidad_de_exito__c.getDescribe().getPicklistValues().get(0).getValue();
            String curr= Opportunity.CurrencyIsoCode.getDescribe().getPicklistValues().get(0).getValue();
            String stage= Opportunity.stageName.getDescribe().getPicklistValues().get(0).getValue();
            String country= Opportunity.BI_Country__c.getDescribe().getPicklistValues().get(0).getValue();
           System.debug('select stage: '+stage);     
            System.debug('select recordtype: '+rT.Id); 
          Opportunity testOppty = new Opportunity(Name='OpportunityTest',AccountId=acc.id, CloseDate =Date.today(),StageName=stage, BI_Probabilidad_de_exito__c=probab,CurrencyIsoCode=curr,BI_Country__c=country,BI_Duracion_del_contrato_Meses__c=12,BI_Plazo_estimado_de_provision_dias__c=12);
          try{
            insert testOppty;
            }catch(Exception e){}
                          
            NE__Order__c Neord = new NE__Order__c(NE__OrderStatus__c='F1 - Closed Won',NE__OpportunityId__c=testOppty.id, NE__AccountId__c=acc.Id,NE__BillAccId__c=acc.Id,NE__OptyId__c=testOppty.id, RecordTypeId=rT.Id, NE__Version__c=1);
            try{
              insert Neord;
            }catch(Exception e){}
           
            NE__OrderItem__c oi=new NE__OrderItem__c(NE__OrderId__c=Neord.Id,NE__Qty__c=1,NE__CatalogItem__c=item.Id);
            insert oi;

                       
            PageReference pageRef = Page.BI_NEPageRedirect;
          Test.setCurrentPage(pageRef);
          try{
             pageRef.getParameters().put('orderId',Neord.id);
          }catch(Exception e){}
            pageRef.getParameters().put('siteId','sId');
            //pageRef.getParameters().put('createOpty','true');
            
                   
            NePR = new BI_NEPageRedirect();
            NePR.redirect();
            
            pageRef.getParameters().put('createOpty','true');
            NePR = new BI_NEPageRedirect();
            NePR.redirect();
          }
        }
        catch(Exception e){
            System.debug(e);
        }
       
   }
   
    static testMethod void unitTest2()
    {
    
    System.debug('UNITEST2');
       try{
           NE.NewConfigurationController ncc;
          ncc= new NE.NewConfigurationController();
          Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
          User u = [SELECT id FROM User Where ProfileId=:p.id AND IsActive=true LIMIT 1];
          System.runAs(u) {
        
        /////////
             Datetime myT = datetime.newInstance(2012, 5, 1, 12, 30, 2);
        
            NE__Catalog_Header__c ch=new NE__Catalog_Header__c(NE__Name__c='myUnitTest');
            insert ch;
            System.debug('insert ch1');//
            
            NE__Catalog__c ct = new NE__Catalog__c(Name='myUnitTest', NE__StartDate__c=myT, NE__Catalog_Header__c=ch.id);
            insert ct;
            System.debug('insert ct1');//

            
            NE__Catalog_Category__c categoria = new NE__Catalog_Category__c(Name = ct.Name,NE__CatalogId__c = ct.Id);
            insert categoria;
            System.debug('insert categoria1');//
            NE__Commercial_Model__c commodel = new NE__Commercial_Model__c(Name = ct.Name, NE__Catalog_Header__c = ct.NE__Catalog_Header__c,
            NE__ProgramName__c = NE.JS_RemoteMethods.SFDC_HTMLENCODE(ct.Name.replaceAll(' ','_')));
            insert commodel;
            System.debug('insert commodel1');//
            NE__Catalog_Category__c cc=new NE__Catalog_Category__c(Name=ct.Name,NE__CatalogId__c=ct.id, NE__Parent_Category_Name__c = categoria.Id);
            insert cc;

            System.debug('insert cc1');//
            NE__Product__c prod1=new NE__Product__c(Name='prod1');
            insert prod1;
            NE__Product__c prod2=new NE__Product__c(Name='prod2');
            insert prod2;
            NE__Product__c prod3=new NE__Product__c(Name='prod3');
            insert prod3;
            NE__Product__c prod4=new NE__Product__c(Name='prod4');
            insert prod4;       
            NE__Product__c prod5=new NE__Product__c(Name='prod5');
            insert prod5;
            System.debug('insert prods1');//
            List<NE__Catalog_Item__c> lisct = new List<NE__Catalog_Item__c>();  
            NE__Catalog_Item__c item= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod1.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item;
            lisct.add(item);
            NE__Catalog_Item__c item2= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod2.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item2;
            lisct.add(item2);
            NE__Catalog_Item__c item3= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod3.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item3;
            lisct.add(item3);
            NE__Catalog_Item__c item4= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod4.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item4;
            lisct.add(item4);
            NE__Catalog_Item__c item5= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod5.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item5;
            lisct.add(item5);
            ///////
        
        
        
            BI_NEPageRedirect  NePR;
            RecordType rt= [SELECT CreatedById,DeveloperName,Id,Name,NamespacePrefix,SobjectType
              FROM RecordType 
                WHERE (SobjectType=:'NE__Order__c' or SobjectType=:'Order__c') AND Name='Opty' LIMIT 1];
                
            Account acc = new Account(Name='accTest');
            insert acc;
            String probab = Opportunity.BI_Probabilidad_de_exito__c.getDescribe().getPicklistValues().get(0).getValue();
            String curr= Opportunity.CurrencyIsoCode.getDescribe().getPicklistValues().get(0).getValue();
            String stage= Opportunity.stageName.getDescribe().getPicklistValues().get(0).getValue();
            String country= Opportunity.BI_Country__c.getDescribe().getPicklistValues().get(0).getValue();
           System.debug('select stage: '+stage);     
            System.debug('select recordtype: '+rT.Id); 
          Opportunity testOppty = new Opportunity(Name='OpportunityTest',AccountId=acc.id, RecordTypeId='012w0000000hzMWAAY', CloseDate =Date.today(),StageName='F6 - Prospecting', BI_Probabilidad_de_exito__c=probab,CurrencyIsoCode=curr,BI_Country__c=country,BI_Duracion_del_contrato_Meses__c=12,BI_Plazo_estimado_de_provision_dias__c=12);
           System.debug('create op: '+testOppty.Id);
          try{
            insert testOppty;
            }catch(Exception e){              System.debug('excepcionn op: '+e.getMessage());}
                          
            NE__Order__c Neord = new NE__Order__c(NE__OrderStatus__c='F1 - Closed Won',NE__OpportunityId__c=testOppty.id, NE__AccountId__c=acc.Id,NE__BillAccId__c=acc.Id,NE__OptyId__c=testOppty.id, RecordTypeId=rT.Id, NE__Version__c=1);
            try{
              insert Neord;
            }catch(Exception e){
                          System.debug('excepcionn or: '+e.getMessage());}
           
            NE__OrderItem__c oi=new NE__OrderItem__c(NE__OrderId__c=Neord.Id,NE__Qty__c=1,NE__CatalogItem__c=item.Id);
            insert oi;

                       
            PageReference pageRef = Page.BI_NEPageRedirect;
          Test.setCurrentPage(pageRef);
          try{
          System.debug('put neord: '+Neord.id);
             pageRef.getParameters().put('orderId',Neord.id);
          }catch(Exception e){}
            pageRef.getParameters().put('siteId','sId');
            //pageRef.getParameters().put('createOpty','true');
            
                   
            NePR = new BI_NEPageRedirect();
            NePR.redirect();
            
            pageRef.getParameters().put('createOpty','true');
            NePR = new BI_NEPageRedirect();
            NePR.redirect();
          }
        }
        catch(Exception e){
            System.debug(e);
        }
       
   }   
       static testMethod void unitTest3()
    {
    System.debug('UNITEST3');
    
          try{
           NE.NewConfigurationController ncc;
          ncc= new NE.NewConfigurationController();
          Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
          User u = [SELECT id FROM User Where ProfileId=:p.id AND IsActive=true LIMIT 1];
          System.runAs(u) {
        
        /////////
             Datetime myT = datetime.newInstance(2012, 5, 1, 12, 30, 2);
        
            NE__Catalog_Header__c ch=new NE__Catalog_Header__c(NE__Name__c='myUnitTest');
            insert ch;
            System.debug('insert ch1');//
            
            NE__Catalog__c ct = new NE__Catalog__c(Name='myUnitTest', NE__StartDate__c=myT, NE__Catalog_Header__c=ch.id);
            insert ct;
            System.debug('insert ct1');//

            
            NE__Catalog_Category__c categoria = new NE__Catalog_Category__c(Name = ct.Name,NE__CatalogId__c = ct.Id);
            insert categoria;
            System.debug('insert categoria1');//
            NE__Commercial_Model__c commodel = new NE__Commercial_Model__c(Name = ct.Name, NE__Catalog_Header__c = ct.NE__Catalog_Header__c,
            NE__ProgramName__c = NE.JS_RemoteMethods.SFDC_HTMLENCODE(ct.Name.replaceAll(' ','_')));
            insert commodel;
            System.debug('insert commodel1');//
            NE__Catalog_Category__c cc=new NE__Catalog_Category__c(Name=ct.Name,NE__CatalogId__c=ct.id, NE__Parent_Category_Name__c = categoria.Id);
            insert cc;

            System.debug('insert cc1');//
            NE__Product__c prod1=new NE__Product__c(Name='prod1');
            insert prod1;
            NE__Product__c prod2=new NE__Product__c(Name='prod2');
            insert prod2;
            NE__Product__c prod3=new NE__Product__c(Name='prod3');
            insert prod3;
            NE__Product__c prod4=new NE__Product__c(Name='prod4');
            insert prod4;       
            NE__Product__c prod5=new NE__Product__c(Name='prod5');
            insert prod5;
            System.debug('insert prods1');//
            List<NE__Catalog_Item__c> lisct = new List<NE__Catalog_Item__c>();  
            NE__Catalog_Item__c item= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod1.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item;
            lisct.add(item);
            NE__Catalog_Item__c item2= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod2.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item2;
            lisct.add(item2);
            NE__Catalog_Item__c item3= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod3.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item3;
            lisct.add(item3);
            NE__Catalog_Item__c item4= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod4.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item4;
            lisct.add(item4);
            NE__Catalog_Item__c item5= new NE__Catalog_Item__c(NE__Catalog_Id__c=ct.id, NE__Catalog_Category_Name__c=cc.id, NE__ProductId__c=prod5.id, NE__Max_Qty__c=1,NE__Type__c='Product');
            insert item5;
            lisct.add(item5);
            ///////
        
        
        
            BI_NEPageRedirect  NePR;
            RecordType rt= [SELECT CreatedById,DeveloperName,Id,Name,NamespacePrefix,SobjectType
              FROM RecordType 
                WHERE (SobjectType=:'NE__Order__c' or SobjectType=:'Order__c') AND Name='Asset' LIMIT 1];
                
            Account acc = new Account(Name='accTest');
            insert acc;
               
            NE__Order__c Neord = new NE__Order__c(NE__OrderStatus__c='F1 - Closed Won', NE__AccountId__c=acc.Id,NE__BillAccId__c=acc.Id, RecordTypeId=rT.Id, NE__Version__c=1);
            try{
              insert Neord;
            }catch(Exception e){
                          System.debug('excepcionn or: '+e.getMessage());}
           
            NE__OrderItem__c oi=new NE__OrderItem__c(NE__OrderId__c=Neord.Id,NE__Qty__c=1,NE__CatalogItem__c=item.Id);
            insert oi;

                       
            PageReference pageRef = Page.BI_NEPageRedirect;
          Test.setCurrentPage(pageRef);
          try{
          System.debug('put neord: '+Neord.id);
             pageRef.getParameters().put('orderId',Neord.id);
          }catch(Exception e){}
            pageRef.getParameters().put('siteId','sId'); //siteId
            //pageRef.getParameters().put('authUser',u.id); //siteId
            //pageRef.getParameters().put('createOpty','true');
            
                   
            NePR = new BI_NEPageRedirect();
            NePR.redirect();
            
            //pageRef.getParameters().put('createOpty',null);
            NePR = new BI_NEPageRedirect();
            NePR.redirect();
            System.debug('createoptynull');
          }
        }
        catch(Exception e){
            System.debug(e);
        }
    }

    @isTest static void redirect_Test(){

        BI_TestUtils.throw_exception = false;

        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
        userTGS.TGS_Is_BI_EN__c = false;
        userTGS.TGS_Is_TGS__c = false;
        insert userTGS;

        List <RecordType> lst_rt = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'NE__Order__c'];

        Map <String, Id> map_rt = new Map <String, Id>();

        for(RecordType rt : lst_rt){

            map_rt.put(rt.DeveloperName, rt.Id);
        }

        List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Argentina});

        List <Opportunity> lst_opp = new List <Opportunity>();

        Opportunity opp = new Opportunity(
            Name = 'tesssst',
            CloseDate = Date.today().addDays(2),
            StageName = Label.BI_DefinicionSolucion,
            AccountId = lst_acc[0].Id,
            BI_Opportunity_Type__c = 'Móvil',
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            BI_Licitacion__c = 'No'
        );
        insert opp;

        NE__Catalog__c cat = new NE__Catalog__c(Name = 'teset');
        insert cat;

        NE__Catalog_Category__c category = new NE__Catalog_Category__c (Name = 'Parque Modification', NE__CatalogId__c = cat.Id);
        insert category;

        NE__Catalog_Item__c ci = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id,
            NE__Technical_Behaviour_Opty__c = 'siii',
            NE__Catalog_Category_Name__c = category.Id
        );
        insert ci;

        NE__Order__c ord = new NE__Order__c(
            NE__OrderStatus__c = 'Active',
            NE__OptyId__c = opp.Id,
            RecordTypeId = map_rt.get('Opty'),
            NE__Recurring_Charge_Total__c = 0,
            NE__One_Time_Fee_Total__c = 0,
            NE__AccountId__c = lst_acc[0].Id
        );
        insert ord;

        NE__OrderItem__c oi = new NE__OrderItem__c (
            NE__Status__c = 'Active',
            NE__OrderId__c = ord.Id,
            NE__Qty__c = 2,
            NE__CatalogItem__c = ci.Id
        );
        insert oi;

        PageReference pag = new PageReference('/');
        pag.getParameters().put('orderId',ord.Id);
        
        /*ApexPages.currentPage().getParameters().get('site');
        ApexPages.currentPage().getParameters().get('authUser');
        ApexPages.currentPage().getParamete*/
        Test.setCurrentPage(pag);
        BI_NEPageRedirect obj = new BI_NEPageRedirect();
        Test.startTest();
        obj.redirect();
        pag.getParameters().put('createOpty', 'Test');
        obj.redirect();
        pag.getParameters().put('createOpty', null);
        ord.RecordTypeId = map_rt.get('Quote');
        update ord;
        obj.redirect();
        ApexPages.currentPage().getParameters().put('site', 'Test');
        ord.RecordTypeId = map_rt.get('Order');
        update ord;
        obj.redirect();
        ApexPages.currentPage().getParameters().put('site', null);
        obj.redirect();
        Test.stopTest();
    }

    static testMethod void checkEconomicFactiMex_TEST() {

     
            BI_TestUtils.throw_exception = false;

            TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance();
            userTGS.TGS_Is_BI_EN__c = false;
            userTGS.TGS_Is_TGS__c = false;
            insert userTGS;

            List <RecordType> lst_rt = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'NE__Order__c'];

            Map <String, Id> map_rt = new Map <String, Id>();

            for(RecordType rt : lst_rt){
                map_rt.put(rt.DeveloperName, rt.Id);
            }

            List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Argentina});

            List <Opportunity> lst_opp = new List <Opportunity>();

            Opportunity opp = new Opportunity(
                Name = 'tesssst',
                CloseDate = Date.today().addDays(2),
                StageName = Label.BI_DefinicionSolucion,
                AccountId = lst_acc[0].Id,
                BI_Opportunity_Type__c = 'Móvil',
                BI_Ciclo_ventas__c = Label.BI_Completo,
                BI_Country__c = Label.BI_Argentina,
                BI_Licitacion__c = 'No'
            );
            insert opp;

            NE__Catalog__c cat = new NE__Catalog__c(Name = 'teset');
            insert cat;

            NE__Catalog_Category__c category = new NE__Catalog_Category__c (Name = 'Parque Modification', NE__CatalogId__c = cat.Id);
            insert category;

            NE__Product__c pord = new NE__Product__c(
                Offer_SubFamily__c = 'a-test;',
                Offer_Family__c = 'b-test',
                BI_COT_MEX_Analisis_Economico__c = true
            );
            insert pord;

            NE__Catalog_Item__c ci = new NE__Catalog_Item__c(
                NE__Catalog_Id__c = cat.Id,
                NE__ProductId__c = pord.Id,
                NE__Technical_Behaviour_Opty__c = 'siii',
                NE__Catalog_Category_Name__c = category.Id
            );
            insert ci;

            

            NE__Order__c ord = new NE__Order__c(
                NE__OrderStatus__c = 'Active',
                NE__OptyId__c = opp.Id,
                RecordTypeId = map_rt.get('Opty'),
                NE__Recurring_Charge_Total__c = 0,
                NE__One_Time_Fee_Total__c = 0,
                NE__AccountId__c = lst_acc[0].Id
            );
            insert ord;

            NE__OrderItem__c oi = new NE__OrderItem__c (
                NE__Status__c = 'Active',
                NE__OrderId__c = ord.Id,
                NE__Qty__c = 2,
                NE__CatalogItem__c = ci.Id
            );
            insert oi;

            BI_NEPageRedirect obj = new BI_NEPageRedirect();
            Test.startTest();
            obj.checkEconomicFactiMex(new List<NE__OrderItem__c>{oi}, opp);
            Test.stopTest();    
    }
}