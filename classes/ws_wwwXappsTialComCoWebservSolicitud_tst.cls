/**
* Avanxo Colombia
* @author           Oscar Angel
* Project:          Telefonica
* Description:      Clase tipo WebServiceMock del servicio ws_wwwTelefonicaComTelefonicaservices.
*
* Changes (Version)
* -------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    ---------------
* @version  1.0     2015-08-21      Oscar Angel             Creación de clase
*************************************************************************************************************/
@isTest
global class  ws_wwwXappsTialComCoWebservSolicitud_tst implements WebServiceMock {

  global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){
    
  }
  
  public static testMethod void test_method_1(){
    
        ws_wwwXappsTialComCoWebservicesSolicitud.respARCH_element a=new ws_wwwXappsTialComCoWebservicesSolicitud.respARCH_element();
        ws_wwwXappsTialComCoWebservicesSolicitud.inpPas_element b=new ws_wwwXappsTialComCoWebservicesSolicitud.inpPas_element();
        ws_wwwXappsTialComCoWebservicesSolicitud.parSol_element c=new ws_wwwXappsTialComCoWebservicesSolicitud.parSol_element();
        ws_wwwXappsTialComCoWebservicesSolicitud.inpUsu_element d=new ws_wwwXappsTialComCoWebservicesSolicitud.inpUsu_element();
        ws_wwwXappsTialComCoWebservicesSolicitud.data_element e=new ws_wwwXappsTialComCoWebservicesSolicitud.data_element();
        ws_wwwXappsTialComCoWebservicesSolicitud.respTG_element f=new ws_wwwXappsTialComCoWebservicesSolicitud.respTG_element();
        ws_wwwXappsTialComCoWebservicesSolicitud.reg_element g=new ws_wwwXappsTialComCoWebservicesSolicitud.reg_element();
        ws_wwwXappsTialComCoWebservicesSolicitud.wPort h=new ws_wwwXappsTialComCoWebservicesSolicitud.wPort();
        ws_wwwXappsTialComCoWebservicesSolicitud.respSOL_element i=new ws_wwwXappsTialComCoWebservicesSolicitud.respSOL_element();
        ws_wwwXappsTialComCoWebservicesSolicitud.inpNDoc_element j=new ws_wwwXappsTialComCoWebservicesSolicitud.inpNDoc_element();
        ws_wwwXappsTialComCoWebservicesSolicitud.parLog_element k=new ws_wwwXappsTialComCoWebservicesSolicitud.parLog_element();
        ws_wwwXappsTialComCoWebservicesSolicitud.inpIdR_element l=new ws_wwwXappsTialComCoWebservicesSolicitud.inpIdR_element();
        ws_wwwXappsTialComCoWebservicesSolicitud.parArch_element m=new ws_wwwXappsTialComCoWebservicesSolicitud.parArch_element();
        ws_wwwXappsTialComCoWebservicesSolicitud.inpId_element n=new ws_wwwXappsTialComCoWebservicesSolicitud.inpId_element();      
        
  }
  
  public static testMethod void test_method_2()
    {
      
      Test.startTest();
      Test.setMock( WebServiceMock.class, new ws_wwwXappsTialComCoWebservicesSol_mws() );
    //llamar a la clase que consume el método del servicio web:
      ejecutaMetodos(); 
      Test.stopTest();
    }
  
  public static void ejecutaMetodos(){
        ws_wwwXappsTialComCoWebservicesSolicitud.wPort servicio=new ws_wwwXappsTialComCoWebservicesSolicitud.wPort();
        servicio.cierre('id');
        servicio.saludo('id');
        servicio.archivo('id','idR');
        servicio.logueo('id','usuario','clave');
        servicio.solicitud('id','nDoc'); 
  }
  
   public static testMethod void test_method_3()
    {
      
      Test.startTest();
      Test.setMock( WebServiceMock.class, new ws_wwwXappsTialComCoWebservicesSol1_mws() );
    //llamar a la clase que consume el método del servicio web:
      ejecutaMetodos(); 
      Test.stopTest();
    }

}