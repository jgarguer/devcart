/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for class BI_COL_MS_Validation_ctr.
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-08-21      Raul Mora (RM)                  Create class.
* @version   1.1    2015-09-03      José Esteban Heredia (JEH)      Most coverage
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
*                   20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private class BI_COL_MS_Validation_tst 
{
    public static List<BI_COL_Modificacion_de_Servicio__c> lstModServCreate;
    public static List<BI_COL_Modificacion_de_Servicio__c> lstModServSend;
    public static BI_COL_Descripcion_de_servicio__c objDescServ;
    public static BI_COL_Modificacion_de_Servicio__c objModServ67;  
    public static BI_COL_Modificacion_de_Servicio__c objModServ5;
    public static BI_COL_Modificacion_de_Servicio__c objModServ6;
    public static BI_COL_Modificacion_de_Servicio__c objModServ61;
    public static User objUsuario;
    public static list <UserRole> lstRoles;
    public static list<User> lstUser;
    public static Contact                               objContacto;
    public static BI_Col_Ciudades__c                    objCiudad;
    public static BI_Sede__c                            objSede;
    public static BI_Punto_de_instalacion__c            objPuntosInsta;
    public static BI_COL_Modificacion_de_Servicio__c    objModSer;

    public static void createData()
    {
        //List<Profile> lstProfile = [ SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1 ];
        ////List<User> lstUser = [ Select Id FROM User where country != 'Colombia' AND ProfileId =: lstProfile[0].Id And isActive = true Limit 1 ];
        
        ////lstRol
        //lstRoles = new list <UserRole>();
        //lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        ////ObjUsuario
        //objUsuario = new User();
        //objUsuario.Alias = 'standt';
        //objUsuario.Email ='pruebas@test.com';
        //objUsuario.EmailEncodingKey = '';
        //objUsuario.LastName ='Testing';
        //objUsuario.LanguageLocaleKey ='en_US';
        //objUsuario.LocaleSidKey ='en_US'; 
        //objUsuario.ProfileId = lstProfile.get(0).Id;
        //objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        //objUsuario.UserName ='pruebas@test.com';
        //objUsuario.EmailEncodingKey ='UTF-8';
        //objUsuario.UserRoleId = lstRoles.get(0).Id;
        //objUsuario.BI_Permisos__c ='Sucursales';
        //objUsuario.Pais__c='Colombia';
        //insert objUsuario;
        
        //lstUser = new list<User>();
        //lstUser.add(objUsuario);


            Account objAccount = new Account();      
            objAccount.Name = 'prueba';
            objAccount.BI_Country__c = 'Colombia';
            objAccount.TGS_Region__c = 'América';
            objAccount.BI_Tipo_de_identificador_fiscal__c = '';
            objAccount.CurrencyIsoCode = 'COP';
            objAccount.BI_Fraude__c = false;
            objAccount.BI_Segment__c                         = 'test';
            objAccount.BI_Subsegment_Regional__c             = 'test';
            objAccount.BI_Territory__c                       = 'test';
            insert objAccount;
            
            //Ciudad
            objCiudad = new BI_Col_Ciudades__c ();
            objCiudad.Name                      = 'Test City';
            objCiudad.BI_COL_Pais__c            = 'Test Country';
            objCiudad.BI_COL_Codigo_DANE__c     = 'TestCDa';
            insert objCiudad;
            System.debug('Datos Ciudad ===> '+objSede);

            // //Contactos
            //objContacto                                     = new Contact();
            //objContacto.LastName                            = 'Test';
            //objContacto.BI_Country__c                       = 'Colombia';
            //objContacto.CurrencyIsoCode                     = 'COP'; 
            //objContacto.AccountId                           = objAccount.Id;
            //objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
            //Insert objContacto; 

            objContacto                                     = new Contact();
            objContacto.LastName                            = 'Test';
        	objContacto.BI_Country__c                       = 'Colombia';
            objContacto.CurrencyIsoCode                     = 'COP'; 
            objContacto.AccountId                           = objAccount.Id;
            objContacto.BI_Tipo_de_contacto__c              = 'Administrador Canal Online';
            objContacto.Phone                               = '12345678';
            objContacto.MobilePhone                         = '3104785925';
            objContacto.Email                               = 'pruebas@pruebas1.com';
            objContacto.BI_COL_Direccion_oficina__c         = 'Dirprueba 34550';
            objContacto.BI_COL_Ciudad_Depto_contacto__c     = objCiudad.Id;
            //REING-INI
            //objContacto.BI_Tipo_de_contacto__c          = 'Autorizado';
            objContacto.FirstName 							= 'fisrtname';
            objContacto.BI_Tipo_de_documento__c 			= 'Otros';
            objContacto.BI_Numero_de_documento__c 			= '00000000X';
       		objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
            objContacto.BI_Tipo_de_contacto__c          = 'General';
       		//REING_FIN
            Insert objContacto;
            

            //Direccion
            objSede                                 = new BI_Sede__c();
            objSede.BI_COL_Ciudad_Departamento__c   = objCiudad.Id;
            objSede.BI_Direccion__c                 = 'Test Street 123 Number 321';
            objSede.BI_Localidad__c                 = 'Test Local';
            objSede.BI_COL_Estado_callejero__c      = System.Label.BI_COL_LbValor3EstadoCallejero;
            objSede.BI_COL_Sucursal_en_uso__c       = 'Libre';
            objSede.BI_Country__c                   = 'Colombia';
            objSede.Name                            = 'Test Street 123 Number 321, Test Local Colombia';
            objSede.BI_Codigo_postal__c             = '12356';
            insert objSede;
            System.debug('Datos Sedes ===> '+objSede);

            //Sede
            objPuntosInsta                      = new BI_Punto_de_instalacion__c ();
            objPuntosInsta.BI_Cliente__c       = objAccount.Id;
            objPuntosInsta.BI_Sede__c          = objSede.Id;
            objPuntosInsta.BI_Contacto__c      = objContacto.id;
            objPuntosInsta.Name      = 'QA Erroro';
            insert objPuntosInsta;
            System.debug('Datos Sucursales ===> '+objPuntosInsta);

            Opportunity objOpp = new Opportunity();
            objOpp.Name = 'prueba opp';
            objOpp.AccountId = objAccount.Id;
            objOpp.BI_Country__c = 'Colombia';
            objOpp.CloseDate = System.today().addDays(+5);
            //objOpp.StageName = 'F5 - Solution Definition';
            objOpp.StageName = 'F6 - Prospecting';
            objOpp.CurrencyIsoCode = 'COP';
            objOpp.Certa_SCP__contract_duration_months__c = 12;
            objOpp.BI_Plazo_estimado_de_provision_dias__c = 0 ;
            //objOpp.OwnerId = lstUser[0].id;
            insert objOpp;      

            NE__Order__c objOrder =  new NE__Order__c();
            objOrder.NE__OptyId__c = objOpp.Id; 
            objOrder.NE__OrderStatus__c='Active';
            insert objOrder;    

            RecordType objRecTyp = [ Select Id From RecordType Where sObjectType = 'NE__Product__c' Limit 1 ];

            NE__Product__c objProd = new NE__Product__c();
            objProd.Name = Constants.PRODUCT_SMDM_INVENTORY;
            objProd.BI_COL_TipoRegistro__c = objRecTyp.Id;
            insert objProd;

            NE__OrderItem__c objOrdItm  = new NE__OrderItem__c();
            objOrdItm.NE__OrderId__c = objOrder.Id; 
            objOrdItm.CurrencyIsoCode = 'USD';
            objOrdItm.NE__OneTimeFeeOv__c = 100;
            objOrdItm.NE__RecurringChargeOv__c = 100;
            objOrdItm.Fecha_de_reasignaci_n_a_factura__c = date.today();
            objOrdItm.NE__Qty__c = 1;
            objOrdItm.NE__ProdId__c = objProd.Id;
            insert objOrdItm;   

            objDescServ = new BI_COL_Descripcion_de_servicio__c();
            objDescServ.BI_COL_Codigo_paquete__c = '123456789';
            objDescServ.BI_COL_Producto_Telefonica__c = objOrdItm.Id;
            objDescServ.BI_COL_Estado_de_servicio__c = 'Activa';
            objDescServ.BI_COL_Oportunidad__c = objOpp.Id;
            insert objDescServ; 

            BI_COL_Descripcion_de_servicio__c objDescServ2 = new BI_COL_Descripcion_de_servicio__c();
            objDescServ2.BI_COL_Codigo_paquete__c = '987654321';
            objDescServ2.BI_COL_Producto_Telefonica__c = objOrdItm.Id;
            objDescServ2.BI_COL_Oportunidad__c = objOpp.Id;
            insert objDescServ2;        

            lstModServCreate = new List<BI_COL_Modificacion_de_Servicio__c>();
            lstModServSend = new List<BI_COL_Modificacion_de_Servicio__c>();

            BI_COL_Modificacion_de_Servicio__c objModServ4 = new BI_COL_Modificacion_de_Servicio__c();
            objModServ4.BI_COL_Estado__c = 'Enviado';
            objModServ4.BI_COL_Clasificacion_Servicio__c = 'SUSPENSION TEMPORAL';
            objModServ4.BI_COL_Cargo_fijo_mes__c = 100;
            objModServ4.BI_COL_Delta_modificacion_upgrade__c = 50;
            objModServ4.BI_COL_Producto__c = objOrdItm.Id;
            objModServ4.BI_COL_Codigo_unico_servicio__c = objDescServ.Id;
            objModServ4.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModServ4.BI_COL_Bloqueado__c                   = false;            
            objModServ4.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModServ4.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            lstModServCreate.add( objModServ4 );
            
            objModServ5 = new BI_COL_Modificacion_de_Servicio__c();
            objModServ5.BI_COL_Estado__c = 'Pendiente';
            objModServ5.BI_COL_Clasificacion_Servicio__c = 'SERVICIO INGENIERIA';
            objModServ5.BI_COL_Cargo_fijo_mes__c = 100;
            objModServ5.BI_COL_Delta_modificacion_upgrade__c = 50;
            objModServ5.BI_COL_Producto__c = objOrdItm.Id;
            objModServ5.BI_COL_Codigo_unico_servicio__c = objDescServ.Id;
            objModServ5.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModServ5.BI_COL_Bloqueado__c                   = false;            
            objModServ5.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModServ5.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            //lstModServSend.add( objModServ5 );

            //objModServ67 = new BI_COL_Modificacion_de_Servicio__c();
            //objModServ67.BI_COL_Estado__c = 'Enviado';
            //objModServ67.BI_COL_Clasificacion_Servicio__c = 'RENEGOCIACION';
            //objModServ67.BI_COL_Cargo_fijo_mes__c = 100;
            //objModServ67.BI_COL_Delta_modificacion_upgrade__c = 50;
            //objModServ67.BI_COL_Producto__c = objOrdItm.Id;
            //objModServ67.BI_COL_Codigo_unico_servicio__c = objDescServ.Id;
            
                
            //objModServ6 = new BI_COL_Modificacion_de_Servicio__c();
            //objModServ6.BI_COL_Estado__c = 'Activa';
            //objModServ6.BI_COL_Clasificacion_Servicio__c = 'ALTA POR NORMALIZACION';
            //objModServ6.BI_COL_Cargo_fijo_mes__c = 100;
            //objModServ6.BI_COL_Delta_modificacion_upgrade__c = 50;
            //objModServ6.BI_COL_Producto__c = objOrdItm.Id;
            //objModServ6.BI_COL_Codigo_unico_servicio__c = objDescServ.Id;

            objModServ61 = new BI_COL_Modificacion_de_Servicio__c();
            objModServ61.BI_COL_Estado__c = 'Activa';
            //objModServ61.BI_COL_Estado__c = 'Enviado';
            objModServ61.BI_COL_Clasificacion_Servicio__c = 'ALTA POR NORMALIZACION';
            objModServ61.BI_COL_Cargo_fijo_mes__c = 100;
            objModServ61.BI_COL_Delta_modificacion_upgrade__c = 50;
            //objModServ61.BI_COL_Producto__c = objOrdItm.Id; //JEG
            objModServ61.BI_COL_Oportunidad__c                 = objOpp.Id;
            objModServ61.BI_COL_Bloqueado__c                   = false;
            objModServ61.BI_COL_Codigo_unico_servicio__c = objDescServ.Id;
            objModServ61.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
            objModServ61.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
            
            
   //         objModSer                                       = new BI_COL_Modificacion_de_Servicio__c();
   //         //objModSer.BI_COL_FUN__c                         = objAnexos.Id;
   //         objModSer.BI_COL_Codigo_unico_servicio__c       = objDescServ.Id;
   //         objModSer.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
   //         objModSer.BI_COL_Oportunidad__c                 = objOpp.Id;
   //         objModSer.BI_COL_Bloqueado__c                   = false;
   //         objModSer.BI_COL_Estado__c                      = 'Activa';//label.BI_COL_lblActiva;
   //         objModSer.BI_COL_Sucursal_de_Facturacion__c     = objPuntosInsta.Id;
   //         objModSer.BI_COL_Sucursal_Origen__c             = objPuntosInsta.Id;
   //         insert objModSer;
            
        
    }

    static testMethod void myUnitTest() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            createData();
            lstModServCreate.add( objModServ61 );
            //lstModServCreate.get(0).BI_COL_Estado__c = 'Activa';
            insert lstModServCreate;
            Test.startTest();
            Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
                lstModServSend.add( objModServ5 );
                insert lstModServSend;
                lstModServSend.get(0).BI_COL_Estado__c = 'Enviado';
                update lstModServSend;

                system.debug( 'prueba-->'+lstModServCreate );
                system.debug( 'prueba-->'+objDescServ.BI_COL_Codigo_unico_servicio__r  +  ' -->' + [SELECT Id,(SELECT ID FROM BI_COL_Codigo_unico_servicio__r) FROM BI_COL_Descripcion_de_servicio__c]);

                
                //BI_COL_MS_Validation_cls.validation( lstModServSend, true );          
                BI_COL_MS_Validation_cls.validation( lstModServCreate, true );          

                BI_COL_MS_Validation_cls.servicioIngenieria( lstModServCreate );
            Test.stopTest();
        }
    }
     static testMethod void myUnitTest3() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            createData();
            lstModServCreate.add( objModServ61 );
            lstModServCreate.get(1).BI_COL_Estado__c = 'Activa';
            insert lstModServCreate;
            Test.startTest();
            Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
                lstModServSend.add( objModServ5 );
                insert lstModServSend;
                lstModServSend.get(0).BI_COL_Estado__c = 'Activa';
                update lstModServSend;

                system.debug( 'prueba-->'+lstModServCreate );
                system.debug( 'prueba-->'+objDescServ.BI_COL_Codigo_unico_servicio__r  +  ' -->' + [SELECT Id,(SELECT ID FROM BI_COL_Codigo_unico_servicio__r) FROM BI_COL_Descripcion_de_servicio__c]);

                
                //BI_COL_MS_Validation_cls.validation( lstModServSend, true );          
                BI_COL_MS_Validation_cls.validation( lstModServCreate, true );          

                BI_COL_MS_Validation_cls.servicioIngenieria( lstModServCreate );
            Test.stopTest();
        }
    }

static testMethod void myUnitTest4() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            createData();
            lstModServCreate.add( objModServ61 );
            //lstModServCreate.get(1).BI_COL_Estado__c = 'Pendiente'; //JEG
            insert lstModServCreate;
            Test.startTest();
            Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
                lstModServSend.add( objModServ5 );
                insert lstModServSend;

                system.debug( 'prueba-->'+lstModServCreate );
                system.debug( 'prueba-->'+objDescServ.BI_COL_Codigo_unico_servicio__r  +  ' -->' + [SELECT Id,(SELECT ID FROM BI_COL_Codigo_unico_servicio__r) FROM BI_COL_Descripcion_de_servicio__c]);

                
                //BI_COL_MS_Validation_cls.validation( lstModServSend, true );          
                BI_COL_MS_Validation_cls.validation( lstModServCreate, true );          

                BI_COL_MS_Validation_cls.servicioIngenieria( lstModServCreate );
            Test.stopTest();
        }
    }
    static testMethod void myUnitTest5() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            createData();
            //lstModServCreate.add( objModServ67 );
            insert lstModServCreate;
            lstModServCreate.get(0).BI_COL_Clasificacion_Servicio__c = 'RENEGOCIACION';
            update lstModServCreate;
            Test.startTest();
            Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
                lstModServSend.add( objModServ5 );
                insert lstModServSend;

                system.debug( 'prueba-->'+lstModServCreate );
                system.debug( 'prueba-->'+objDescServ.BI_COL_Codigo_unico_servicio__r  +  ' -->' + [SELECT Id,(SELECT ID FROM BI_COL_Codigo_unico_servicio__r) FROM BI_COL_Descripcion_de_servicio__c]);

                
                //BI_COL_MS_Validation_cls.validation( lstModServSend, true );          
                BI_COL_MS_Validation_cls.validation( lstModServCreate, true );          

                BI_COL_MS_Validation_cls.servicioIngenieria( lstModServCreate );
            Test.stopTest();
        }
    }
    static testMethod void myUnitTest6() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            createData();
            //lstModServCreate.add( objModServ67 );
            insert lstModServCreate;
            lstModServCreate.get(0).BI_COL_Clasificacion_Servicio__c = 'RENEGOCIACION';
            lstModServCreate.get(0).BI_COL_Estado__c = 'Pendiente';
            update lstModServCreate;
            Test.startTest();
            Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
                lstModServSend.add( objModServ5 );
                insert lstModServSend;

                system.debug( 'prueba-->'+lstModServCreate );
                system.debug( 'prueba-->'+objDescServ.BI_COL_Codigo_unico_servicio__r  +  ' -->' + [SELECT Id,(SELECT ID FROM BI_COL_Codigo_unico_servicio__r) FROM BI_COL_Descripcion_de_servicio__c]);

                
                //BI_COL_MS_Validation_cls.validation( lstModServSend, true );          
                BI_COL_MS_Validation_cls.validation( lstModServCreate, true );          

                BI_COL_MS_Validation_cls.servicioIngenieria( lstModServCreate );
            Test.stopTest();
        }
    }
    static testMethod void myUnitTest7() 
    {
        objUsuario=BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario)
        {
            createData();
            //lstModServCreate.add( objModServ67 );
            insert lstModServCreate;
            lstModServCreate.get(0).BI_COL_Clasificacion_Servicio__c = 'RENEGOCIACION';
            lstModServCreate.get(0).BI_COL_Estado__c = 'Activa';
            update lstModServCreate;
            Test.startTest();
            Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
                lstModServSend.add( objModServ5 );
                insert lstModServSend;

                system.debug( 'prueba-->'+lstModServCreate );
                system.debug( 'prueba-->'+objDescServ.BI_COL_Codigo_unico_servicio__r  +  ' -->' + [SELECT Id,(SELECT ID FROM BI_COL_Codigo_unico_servicio__r) FROM BI_COL_Descripcion_de_servicio__c]);

                
                //BI_COL_MS_Validation_cls.validation( lstModServSend, true );          
                BI_COL_MS_Validation_cls.validation( lstModServCreate, true );          

                BI_COL_MS_Validation_cls.servicioIngenieria( lstModServCreate );
            Test.stopTest();
        }
    }

}