/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.  	Fecha           Autor                           Descripción        
*           -----	----------      ---------------------------     ---------------    
* @version   1.0	2015-04-07      Daniel ALexander Lopez (DL)     Cloned Trigger   
             1.1	2017-02-02      Marta Gonzalez                  REING-001 - Reingeniería de Contactos
             1.2	2017-11-28	Jaime Regidor			Añadir un filtro para la llamada del metodo futuro 
************************************************************************************************************/

public with sharing class BI_COL_CreateContactWS_cls {
 /**
		* Llama al servicio web de Telefonica que crea el contacto
		* @param lstIdContactos Id de contactos a crear
		*
		* @return
		*/
		public static void crearContactoTelefonica (List<Contact> news, List<Contact> olds, String accion){
			List<Contact> tocheck = news != null?news:olds;
			List<String> lstIdContactos = new List<String>();
			if(tocheck.size()>0){
				for(Contact contacto : tocheck){
					if(contacto.BI_Country__c!= null && contacto.BI_Country__c == 'Colombia'){
						lstIdContactos.add(contacto.Id);
					}

				}
				if(lstIdContactos.size()>0){
					System.debug('Entro '+lstIdContactos);
					BI_COL_CreateContactWS_cls.crearContactoTelefonica_fut(lstIdContactos,accion);
				}
			}

		}

		@future (callout=true)
		public static void crearContactoTelefonica_fut (List<String> lstIdContactos, String accion)
		{
			String pais='Colombia';
			map<ID,ws_wwwTelefonicaComNotificacionessalesfo.contacto> infContactoEnviad=new map<ID,ws_wwwTelefonicaComNotificacionessalesfo.contacto>();
				// Array para enviar al Web Service
			ws_wwwTelefonicaComNotificacionessalesfo.contacto[] arrayContacto;
				//Si existe contactos a crear
			if(lstIdContactos!=null&&lstIdContactos.size()>0)
			{
				//Consultar contactos creados/modificados
				Contact objContacto = new Contact();
				system.debug('lista contactos a buscar ID--->'+lstIdContactos);
				List<Contact> lstContactos = [Select id, Name, LastName,  Fax,
														Phone, MobilePhone,
														BI_Tipo_de_documento__c ,
																						BI_Relacion_con_Telefonica__c,
																						RecordType.Name,
																						email,
																						BI_Regalos_empresariales__c,
																						RecordTypeId,
																						BI_Numero_de_documento__c,
																						 HasOptedOutOfEmail,
																						Account.BI_No_Identificador_fiscal__c,
																						Account.BI_COL_Segmento_Telefonica__c,
																						Account.BI_Tipo_de_identificador_fiscal__c,
																						FirstName,
																						LastModifiedById,BI_COL_Rol__c,
																						BI_Tipo_de_contacto__c,
																						BI_Activo__c,
																						BI_COL_Ciudad_Depto_contacto__r.BI_COL_Codigo_DANE__c,
																						BI_Country__c,
																						//MailingAddress,
																						BI_COL_Direccion_oficina__c
																						//,Contador_Contacto__c
																						//REING-001-INI
																						,FS_CORE_Referencias_Funcionales__c
                                              											,BI_Representante_legal__c
																						,FS_CORE_Acceso_a_Portal_Platino__c  
																						//REING-001-FIN
																						from Contact 
																						where id in :lstIdContactos AND BI_Country__c=:pais ALL ROWS ];
						system.debug('lista contactos encontrados--->'+lstContactos);
						//YNaranjo
						ws_wwwTelefonicaComNotificacionessalesfo.contacto x = new ws_wwwTelefonicaComNotificacionessalesfo.contacto();
						arrayContacto = new ws_wwwTelefonicaComNotificacionessalesfo.contacto[]{};
						if(lstContactos.size()>0)
						{
								//Llamando a funcion en el Ws que espera los datos para Contacto.
								
								
								ws_wwwTelefonicaComNotificacionessalesfo.TiposContactoType tipos = new ws_wwwTelefonicaComNotificacionessalesfo.TiposContactoType();
								List<ws_wwwTelefonicaComNotificacionessalesfo.TiposContactoType> lstTipos = new List<ws_wwwTelefonicaComNotificacionessalesfo.TiposContactoType>();
								List<String> strTipos = new List<String>();
								 
								for(Contact c : lstContactos)
								{
                                    //REING-001-INI
                                    
                                    // Autorizaciones
                                    if(!String.isEmpty(c.BI_Tipo_de_contacto__c)){
                                        if(c.BI_Tipo_de_contacto__c.contains(';')){
                                            strTipos = c.BI_Tipo_de_contacto__c.split(';');
                                            tipos.tipoContacto = strTipos;
                                        }else{
                                            strTipos.add(c.BI_Tipo_de_contacto__c);
                                            tipos.tipoContacto = strTipos;
                                        }
                                    }else if(!String.isEmpty(c.FS_CORE_Referencias_Funcionales__c)){
                                        if(c.FS_CORE_Referencias_Funcionales__c.contains(';')){
                                            strTipos = c.FS_CORE_Referencias_Funcionales__c.split(';');
                                            tipos.tipoContacto = strTipos;
                                        }else{
                                            strTipos.add(c.FS_CORE_Referencias_Funcionales__c);
                                            tipos.tipoContacto = strTipos;
                                        }
                                    }
                                    
                                    // Representante legal
                                    if(c.BI_Representante_legal__c){
                                        strTipos.add('Representante legal');
                                        tipos.tipoContacto = strTipos;
                                    }
                                    
                                    // Portal platino
                                    if(c.FS_CORE_Acceso_a_Portal_Platino__c){
                                        strTipos.add('Administrador Canal Online');
                                        tipos.tipoContacto = strTipos;
                                    }
                                    System.debug('> tipos: '+JSON.serialize(tipos));
                                    System.debug('> tipos.tipoContacto: '+JSON.serialize(tipos.tipoContacto));
                                    
                                    /* DEPRECATED */
									/*
                                    if(c.BI_Tipo_de_contacto__c!=null && c.BI_Tipo_de_contacto__c.contains(';')){
                                        strTipos = c.BI_Tipo_de_contacto__c.split(';');
                                        tipos.tipoContacto = strTipos;
                                    }
                                    
                                    else
                                    {
                                        strTipos.add(c.BI_Tipo_de_contacto__c);
                                        tipos.tipoContacto = strTipos;
                                    }
                                    
                                    if (c.FS_CORE_Acceso_a_Portal_Platino__c){
                                        strTipos.add('Administrador Canal Online');
                                        tipos.tipoContacto = strTipos;
                                    }
                                    */
                                    
                                    //REING-001-FIN
                                        
										x = new ws_wwwTelefonicaComNotificacionessalesfo.contacto();
										x.Nombre = c.FirstName;
										x.apellidos = c.LastName;
										
										x.telefonoOficina = c.Fax;
										x.telefono = c.Phone;
										x.celular = c.MobilePhone;
										x.tipoDocumento = c.BI_Tipo_de_documento__c ;
										x.identificacion = c.BI_Numero_de_documento__c;
										x.tiposContacto = tipos;
										x.email = c.email;
										x.direccionOficina = c.BI_COL_Direccion_oficina__c;
									 // if(c.Ciudad_Depto_contacto__c!=null)
										x.ciudadDepartamento = c.BI_COL_Ciudad_Depto_contacto__r.BI_COL_Codigo_DANE__c;                   
										x.pais = c.BI_Country__c;
										x.idContacto = c.Id;
										x.tipoId = c.Account.BI_Tipo_de_identificador_fiscal__c;
										x.identificacionContacto=c.Account.BI_No_Identificador_fiscal__c;
										x.estado = c.BI_Activo__c?'Activo':'Inactivo';
										x.idUsuarioModificacion=c.LastModifiedById;         //OA 30-07-2012 Se agrega nuevo campo 
										x.cargo=c.BI_COL_Rol__c;                            //OA 30-07-2012 Se agrega nuevo campo
										//x.consecutivoContacto=''+c.Contador_Contacto__c;  //OA 30-07-2012 Se agrega nuevo campo
										x.accion=accion; 
										arrayContacto.add(x);

										System.debug('\n\n x.idContacto: '+x.idContacto+'\n\n');
								}
						}
						else if(accion==label.BI_COL_LbEliminar)
						{
							for(Contact c:lstContactos)
							{
								x = new ws_wwwTelefonicaComNotificacionessalesfo.contacto();
								x.idContacto=c.Id;
								x.accion=accion;
								x.idUsuarioModificacion=Userinfo.getUserId();
								arrayContacto.add(x);
								infContactoEnviad.put(c.Id,x);

							}
						}
						try
						{
							//ws_wwwTelefonicaComNotificacionessalesfo.RegistrarContactoResponse_element rtaWSContactos;
							ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] rtaWSContactos;
							ws_wwwTelefonicaComNotificacionessalesfo.NotificacionesSalesForceSOAP xy = new ws_wwwTelefonicaComNotificacionessalesfo.NotificacionesSalesForceSOAP();
							//Enviando a variable de respuesta, el array de contactos.
							system.debug('------->request--->'+arrayContacto); 
							if(arrayContacto.size()>0) 
							{
								rtaWSContactos = xy.RegistrarContactos(arrayContacto); 
								System.debug('..==.. Rta Web Service RegistrarContactos ..==.. '+rtaWSContactos);
								if(accion!=label.BI_COL_LbEliminar)
									creaLogTransaccion(rtaWSContactos,lstContactos,infContactoEnviad);
							}
						}
						catch(Exception e)
						{
							System.debug('..==.. Error en la conexión con el servicio web de notificaciones ');
							creaLogTransaccionError(''+e,lstContactos,'NOTIFICACIONES');
						}
				}
		}
		
		/**** OA: 04-09-2012 Se agrega método de creación de BI Log para el objeto Contacto ****/
		public static void creaLogTransaccionError(String error, List<Contact> lstIdContactos,String infEnviada){
			List<BI_Log__c> lstLogTran=new List<BI_Log__c>();
			BI_Log__c log=null; 
			system.debug('métod de BI Log::::'+error+'\n\n'+lstIdContactos);
		
		for(Contact contacto:lstIdContactos){
				log=new BI_Log__c(BI_COL_Informacion_recibida__c=error,BI_COL_Contacto__c=contacto.Id,BI_COL_Estado__c='FALLIDO',BI_COL_Interfaz__c='NOTIFICACIONES',BI_COL_Informacion_Enviada__c=infEnviada); //,BI_Numero_de_documento__c='FALLIDO', Name='Error Servicio Web: '+servicio,
				lstLogTran.add(log);
		}
		
		system.debug('Insertando BI Log::::'+lstLogTran);
		insert lstLogTran;
		}
		/**** OA: 04-09-2012 Se agrega método de creación de BI Log para el objeto Contacto ****/
		public static void creaLogTransaccion(ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] arrRespuesta, List<Contact> lstContactos,map<ID,ws_wwwTelefonicaComNotificacionessalesfo.contacto> infContactoEnviad){
			List<BI_Log__c> lstLogTran=new List<BI_Log__c>();
			BI_Log__c log=null; 
			system.debug('métod de BI Log::::'+arrRespuesta+'\n\n'+lstContactos);
		
		for(Contact contacto:lstContactos){
			if(arrRespuesta[0].idError.equals('0'))
			{
				log = new BI_Log__c( BI_COL_Informacion_recibida__c = 'OK, Respuesta Procesada',BI_COL_Contacto__c=contacto.Id,BI_COL_Tipo_Transaccion__c='SALIDA NOTIFICACIONES',BI_COL_Estado__c ='Exitoso',BI_COL_Informacion_Enviada__c=String.valueOf(infContactoEnviad.get(contacto.id))); //Name='SW Notificaciones',
			}
			else
			{
				log=new BI_Log__c(BI_COL_Informacion_recibida__c='('+arrRespuesta[0].idError+')::'+arrRespuesta[0].descripcionError,BI_COL_Contacto__c=contacto.Id,BI_COL_Tipo_Transaccion__c='SALIDA NOTIFICACIONES',BI_COL_Estado__c='Exitoso',BI_COL_Interfaz__c='NOTIFICACIONES',BI_COL_Informacion_Enviada__c=String.valueof(infContactoEnviad.get(contacto.id))); //Name='SW Notificaciones',
			}
			lstLogTran.add(log);
		}

		
		system.debug('Insertando BI Log::::'+lstLogTran);
		insert lstLogTran;
		}
}