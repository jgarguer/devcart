public class ADQ_SelectorOpcionesController{
	//private Map<String, String> mapPaginas = new Map<String, String>();
	public String paginaSiguiente{get;set;}
	
	private String accid;
	
	/*
	* Valores de ps (paginaSiguiente)
	* df = DefinicionFactura
	*/
	
	public ADQ_SelectorOpcionesController(){
		paginaSiguiente = ApexPages.currentPage().getParameters().get('ps');
		accid = ApexPages.currentPage().getParameters().get('accid');
	}
	
	/***********************   PageRefences de Definición de Factura   ***********************************/
	public PageReference getNuevaFacturaRecurrente(){
		String url = '/apex/ADQ_DefinicionFactura';
		url += (accid!=null)?'?accid=' + accid+'&type=RE':'';
		return new PageReference(url);
	}
	public PageReference getNuevaFacturaCargoUnico(){
		/*
		String url = '/apex/ADQ_DefinicionFacturaCargoUnico';
		url += (accid!=null)?'?accid=' + accid:'';
		return new PageReference(url);
		*/
		String url = '/apex/ADQ_DefinicionFactura';
		url += (accid!=null)?'?accid=' + accid+'&type=CU':'';
		return new PageReference(url);
	}
	/*
	public PageReference getNuevaFacturaOnDemand(){
		String url = '/apex/DefinicionFacturaOnDemand';
		url += (accid!=null)?'?accid=' + accid:'';
		return new PageReference(url);
	}
	*/
	
	/***********************   PageRefences de cancelar  ***********************************/
	public PageReference cancelar(){
    	return new Apexpages.Pagereference('/apex/ADQ_OpcionesFacturacion');
    }
    
    /***********************************************************************************************
	*	Método de Prueba
	***********************************************************************************************/
	/*
	public static testMethod void testFacturaController(){
		
		ADQ_SelectorOpcionesController s = new ADQ_SelectorOpcionesController();
		
		s.getNuevaFacturaCargoUnico();
		s.getNuevaFacturaRecurrente();
		s.getNuevaFacturaOnDemand();
		s.cancelar();
	}
	*/
}