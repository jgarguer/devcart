/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:          
* Description:         
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-07-24      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_CustomerAccount_ctr
*			 1.1	2015-08-19		Jeisson Rojas (JR)		Cambio realizado a la Query de Usuarios del campo Country por Pais__c
*                   20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest
global class BI_COL_CustomerAccount_tst implements WebServiceMock{
	
	/*	
	global Map<String, BI_COL_Davox_wsdl.findResponse_element> mapWSResponseBySOAPAction = new Map<String, BI_COL_Davox_wsdl.findResponse_element	>();
	global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){
		//BI_COL_Callejero_wsdl.Interfaz1Response_element objResp = new BI_COL_Callejero_wsdl.Interfaz1Response_element();
    	//objResp.Interfaz1Result = 'resp';
    	BI_COL_Davox_wsdl.findResponse_element fndEle = new BI_COL_Davox_wsdl.findResponse_element();
    	
    	fndEle.findReturn = '<document><result>success</result><item><SEPARATOR>@</SEPARATOR><Value>573@537</Value></item></document>';
		//mapWSResponseBySOAPAction.put('response_x',fndEle);
		response.put( 'response_x',mapWSResponseBySOAPAction );
	}*/

	    global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) 
	    {    
	    	String strXmlResp = '';

	    	strXmlResp = '<WS>';
		    	strXmlResp += '<USER>USER</USER>';
		    	strXmlResp += '<PASSWORD>PASSWORD</PASSWORD>';
		    	strXmlResp += '<DATEPROCESS>2015-08-24</DATEPROCESS>';
		    	strXmlResp += '<PACKCODE>ASD654AS</PACKCODE>';
		    	strXmlResp += '<'+Label.BI_COL_LbSeparator+'>Õ</'+Label.BI_COL_LbSeparator+'>';
		    	strXmlResp += '<TYPE>CONSULTA_CUENTA</TYPE>';
		    	strXmlResp += '<COUNT>1</COUNT>';
		    	strXmlResp += '<'+label.BI_COL_LbValues+'>';	 
		    		strXmlResp += '<'+label.BI_COL_LbValue+'>value_testÕvalue_test2Õvalue_test3Õvalue_test4ÕSCÕVEÕvalue_test7Õvalue_test8Õvalue_test9Õvalue_test10Õvalue_test11Õvalue_test12 </'+label.BI_COL_LbValue+'>';
		 		    	strXmlResp +=  '</'+label.BI_COL_LbValues+'>';  	
	    	strXmlResp += '</WS>';
	    	system.debug('@-->strXmlResp'+strXmlResp);  
	    	
	    	BI_COL_Davox_wsdl.findResponse_element objResp = new BI_COL_Davox_wsdl.findResponse_element();
	    	objResp.findReturn = strXmlResp;

			response.put( 'response_x', objResp);
		}
	

	Public static User 									objUsuario = new User();
	public static BI_COL_Modificacion_de_Servicio__c 	objMS;
	public static Account 								objCuenta;
	public static BI_Log__c 							objBiLog;
	public static BI_Col_Ciudades__c 					objCiudad;
	public static Opportunity 							objOportunidad;
	public static BI_COL_Anexos__c 						objAnexos;
	public static BI_COL_Descripcion_de_servicio__c 	objDesSer;
	public static Contract 								objContrato;
	public static BI_COL_CustomerAccount_ctr 			objCustomerAccount;
	
	public static list <Profile> lstPerfil;
	public static list <UserRole> lstRoles;
	public static List<User> lstUsuarios;
	public static List<RecordType> lstRecTyp;
	
    public static void crearData()
    {
    	lstPerfil             			= new list <Profile>();
		lstPerfil               		= [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
		system.debug('datos Perfil '+lstPerfil);
		
		//lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = new User();
        objUsuario.Alias = 'standt';
        objUsuario.Email ='pruebas@test.com';
        objUsuario.EmailEncodingKey = '';
        objUsuario.LastName ='Testing';
        objUsuario.LanguageLocaleKey ='en_US';
        objUsuario.LocaleSidKey ='en_US'; 
        objUsuario.ProfileId = lstPerfil.get(0).Id;
        objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        objUsuario.UserName ='pruebas@test.com';
        objUsuario.EmailEncodingKey ='UTF-8';
        objUsuario.UserRoleId = lstRoles.get(0).Id;
        objUsuario.BI_Permisos__c ='Sucursales';
        objUsuario.Pais__c='Colombia';
        insert objUsuario;
        
        lstUsuarios = new list<User>();
        lstUsuarios.add(objUsuario);

		//lstUsuarios = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];
		
		System.runAs(lstUsuarios[0])
		{
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass;
			
			//Ciudad
			objCiudad = new BI_Col_Ciudades__c ();
			objCiudad.Name 						= 'Test City';
			objCiudad.BI_COL_Pais__c 			= 'Test Country';
			objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
			insert objCiudad;
			
			//Cuentas
			objCuenta 										= new Account();      
			objCuenta                                       = new Account();
			objCuenta.Name                                  = 'prueba';
			objCuenta.BI_Country__c                         = 'Argentina';
			objCuenta.TGS_Region__c                         = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
			objCuenta.CurrencyIsoCode                       = 'COP';
			objCuenta.BI_Fraude__c                          = false;
			objCuenta.BI_No_Identificador_fiscal__c         = '88888888';
            objCuenta.BI_Segment__c                         = 'test';
        	objCuenta.BI_Subsegment_Regional__c             = 'test';
          	objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			system.debug('datos Cuenta '+objCuenta);
			
			//Tipo de registro
			lstRecTyp 										= [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
			system.debug('datos de FUN '+lstRecTyp);
			
			//Contrato
			objContrato 									= new Contract ();
			objContrato.AccountId 							= objCuenta.Id;
			objContrato.StartDate 							=  System.today().addDays(+5);
			objContrato.BI_COL_Formato_Tipo__c 				= 'Contrato Marco';
			objContrato.ContractTerm 						= 12;
			objContrato.BI_COL_Presupuesto_contrato__c 		= 1000000;
			objContrato.StartDate							= System.today().addDays(+5);
			insert objContrato;
			System.debug('datos Contratos ===>'+objContrato);
			
			//FUN
			objAnexos               						= new BI_COL_Anexos__c();
			objAnexos.Name          						= 'FUN-0041414';
			objAnexos.RecordTypeId  						= lstRecTyp[0].Id;
			objAnexos.BI_COL_Contrato__c   					= objContrato.Id;
			insert objAnexos;
			System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
			
			//Oportunidad
			objOportunidad                                      	= new Opportunity();
			objOportunidad.Name                                   	= 'prueba opp';
			objOportunidad.AccountId                              	= objCuenta.Id;
			objOportunidad.BI_Country__c                          	= 'Argentina';
			objOportunidad.CloseDate                              	= System.today().addDays(+5);
			objOportunidad.StageName                              	= 'F5 - Solution Definition';
			objOportunidad.CurrencyIsoCode                        	= 'COP';
			objOportunidad.Certa_SCP__contract_duration_months__c 	= 12;
			objOportunidad.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
			objOportunidad.OwnerId                                	= lstUsuarios[0].id;
			insert objOportunidad;
			system.debug('Datos Oportunidad'+objOportunidad);
			list<Opportunity> lstOportunidad 	= new list<Opportunity>();
			lstOportunidad 						= [select Id from Opportunity where Id =: objOportunidad.Id];
			system.debug('datos ListaOportunida '+lstOportunidad);
			
			//DS
			objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
			objDesSer.CurrencyIsoCode               			= 'COP';
			insert objDesSer;
			System.debug('Data DS ====> '+ objDesSer);
			System.debug('Data DS ====> '+ objDesSer.Name);
			List<BI_COL_Descripcion_de_servicio__c> lstqry 		= [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
			System.debug('Data lista DS ====> '+ lstqry);
			
			//MS
			objMS                         			= new BI_COL_Modificacion_de_Servicio__c();
			objMS.BI_COL_FUN__c           			= objAnexos.Id;
			objMS.BI_COL_Codigo_unico_servicio__c 	= objDesSer.Id;
			objMS.BI_COL_Clasificacion_Servicio__c  = 'ALTA';
			objMS.BI_COL_Oportunidad__c 			= objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c 				= false;
			objMS.BI_COL_Estado__c 					= 'Activa';//label.BI_COL_lblActiva;
			insert objMS;
			system.debug('Data objMs ===>'+objMs);
			
			//BI_Log
	    	objBiLog									= new BI_Log__c();
			objBiLog.BI_COL_Informacion_recibida__c		= 'OK, Respuesta Procesada';
			//objBiLog.BI_COL_Contacto__c 				= lstContacts[0].Id;
			objBiLog.BI_COL_Estado__c					= 'FALLIDO';
			objBiLog.BI_COL_Interfaz__c					= 'NOTIFICACIONES';
			//lstLog.add(objLog);
		}	    	    	
    }
    
    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        list<String> info = new list<String>();
        list<string>ids = new list<String>();
        Integer desde=0;
        Integer hasta=0;
        string cuentaNueva='';
        string tipo='';
        string packcode='';
        String queryLike='';
        
        info.add('a');
        info.add('b');
        info.add('c');
        
        string xml='<?xml version="1.0" encoding="utf-8"?>'+
'<resources>'+
    '<string'+
        'name="string_name"'+
        '>text_string</string>'+
'</resources>';
		Test.setMock( WebServiceMock.class, new BI_COL_CustomerAccount_tst() );
        //Test.setMock(WebServiceMock.class, new MockResponseGenerator1());
        crearData();
        List<BI_COL_Modificacion_de_Servicio__c> dst=[Select BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c, BI_COL_Oportunidad__r.Account.Name 
										from BI_COL_Modificacion_de_Servicio__c  limit 1];
		test.startTest();
        ApexPages.currentPage().getParameters().put('id',objCuenta.Id);
        if(dst!=null && dst.size()>0)
        {
        	ApexPages.currentPage().getParameters().put('idMS',dst[0].Id);
        }
        objCustomerAccount = new BI_COL_CustomerAccount_ctr();
        objCustomerAccount.cuentaCesionContr();
        objCustomerAccount.cargarDatos();
        objCustomerAccount.armarOpciones();
        objCustomerAccount.getCuentaDx();
        objCustomerAccount.guardar();
        objCustomerAccount.getPageNumber();
        objCustomerAccount.getPageSize();
        objCustomerAccount.getPreviousButtonEnabled();
        objCustomerAccount.getNextButtonDisabled();
        
        objCustomerAccount.previousBtnClick();
        objCustomerAccount.ViewData();
        BI_COL_CustomerAccount_ctr.armadoXMLMultiple(info,tipo,packcode);
        BI_COL_CustomerAccount_ctr.reservaCuenta(ids,cuentaNueva);
        objCustomerAccount.lstCuentas=new list<String>();
        objCustomerAccount.lstLabelCuenta=new list<String>();
        objCustomerAccount.lstCuentas.add('0');
        objCustomerAccount.lstCuentas.add('1');
        objCustomerAccount.lstLabelCuenta.add('0');
        objCustomerAccount.lstLabelCuenta.add('1');
        objCustomerAccount.totalPageNumber=9;
        objCustomerAccount.getTotalPageNumber(); 
        objCustomerAccount.getItems(desde,hasta);
        objCustomerAccount.qetIDCiudad(queryLike);
        //objCustomerAccount.retornaCadena(xml);
        objCustomerAccount.nextBtnClick();
        
        objCustomerAccount.retornaCadena('<document><result>success</result><item><SEPARATOR>@</SEPARATOR><Value>573@537</Value></item></document>');
        
        objCustomerAccount.respuestaxml= '';
        objCustomerAccount.armarOpciones();
        objCustomerAccount.respuestaxml='a'+objCustomerAccount.separador+'b';
        objCustomerAccount.armarOpciones();
        
        
        objCustomerAccount.totalPageNumber=0;
        objCustomerAccount.getTotalPageNumber(); 
        test.stopTest();
        
        /*
        public void armarOpciones()
    {
        if(respuestaxml!=null)*/

    }
}