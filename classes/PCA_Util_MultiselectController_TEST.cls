@isTest
private class PCA_Util_MultiselectController_TEST {
	
	static testMethod void getloadInfo() {

        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
		 User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());         
	    //insert usr;
	    system.debug('usr: '+usr);
	    User user1;
	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais); 
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
                      
            Event__c Evento2;
            DateTime day = Datetime.NOW();
            DateTime day10 = day.AddMinutes(10); 
            AccountTeamMember AccMem = new AccountTeamMember(UserId=usr.Id,TeamMemberRole='Apoderado',AccountId=acc[0].Id);
            insert AccMem; 
            user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
				PageReference pageRef = new PageReference('PCA_CustomEventDetail');
	       		Test.setCurrentPage(pageRef);
            system.runAs(user1){
	            Evento2 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = acc[0].ownerId, ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day);
	            insert Evento2;
	            Event__c Evento1 = new Event__c(subject__c='Nuevo Evento', description__c='Test Evento', ownerId = acc[0].ownerId, ActivityDateTime__c=DAY,ActivityDate__c=Date.today(),EndDateTime__c=day10,StartDateTime__c=day);
	            insert Evento1; 
            }           
            /*EventRelation EvRel1 = new EventRelation(RelationId=con[0].Id,EventId=Evento2.Id);
            insert EvRel1;    */
            system.runAs(user1){
            	PCA_Util_MultiselectController constructor = new PCA_Util_MultiselectController();
             	BI_TestUtils.throw_exception = false;
            	constructor.leftOptionsHidden = 'test&optionsLeft';
            	constructor.rightOptionsHidden = 'test&optionsRight';            
            }
            system.runAs(user1){
            	PCA_Util_MultiselectController constructor = new PCA_Util_MultiselectController();
             	BI_TestUtils.throw_exception = true;
            	constructor.leftOptionsHidden = 'test&optionsLeft';
            	constructor.rightOptionsHidden = 'test&optionsRight';            
            }            
        }

	} 
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}