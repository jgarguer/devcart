/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Eduardo Ventura
Company:        Everis España
Description:    Generic JSON & XML parser - Test Class.

History:
<Date>                      <Author>                        <Change Description>
13/02/2017                  Eduardo Ventura                 Versión Inicial.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
public class BI_Serializer_Test {
    /* GLOBAL VARIABLES */
    private static final String xml = '<?xml version="1.0" encoding="UTF-8" ?><contractRequest><tax><amount>100</amount><units>ARS</units></tax><price><amount>100</amount><units>ARS</units></price><hasPastDuePenalty>true</hasPastDuePenalty><hasCancelPenalty>true</hasCancelPenalty><allowCancel>true</allowCancel><autoRenewal>true</autoRenewal><serviceEndDate>2017-01-20T10:53:58.223Z</serviceEndDate><serviceStartDate>2017-01-20T10:53:58.223Z</serviceStartDate><contractStartDate>2017-01-20T10:53:58.223Z</contractStartDate><signatureDate>2017-01-20T10:53:58.223Z</signatureDate><legalId><country>Argentina</country><nationalIDType>NIF</nationalIDType><nationalID>000000000X</nationalID></legalId><opportunityId>00626000005B3WuAAK</opportunityId><accountId>0012600000Q8BFUAA3</accountId><status>draft</status><type>No</type><description>Example.</description><name>Telefonica</name><country>ARG</country><correlationId>0012600000Q8BFUAA3</correlationId><additionalData><key>monetary</key><value>Sí</value></additionalData><additionalData><key>contractThirdParts</key><value>Sí</value></additionalData><additionalData><key>providerThirtParts</key><value>Exemplo</value></additionalData><additionalData><key>contractTerm</key><value>1</value></additionalData><additionalData><key>validity</key><value>Provisorio</value></additionalData><additionalData><key>opportunityType</key><value>Alta</value></additionalData><additionalData><key>serviceLines</key><value>Alta</value></additionalData><additionalData><key>topType</key><value>Sí</value></additionalData><additionalData><key>topValue</key><value>100</value></additionalData><additionalData><key>exhangeRateApplicable</key><value>Emisión Factura</value></additionalData><additionalData><key>sla</key><value>Especial</value></additionalData><additionalData><key>salesEquipment</key><value>Leasing</value></additionalData><additionalData><key>company</key><value>TASA</value></additionalData><additionalData><key>stampTax</key><value>Sí</value></additionalData><additionalData><key>internationalService</key><value>Sí</value></additionalData><additionalData><key>internationalSupplier</key><value>Antel</value></additionalData><additionalData><key>documentType</key><value>Nota</value></additionalData></contractRequest>';
    private static final String json = '{"contractRequest":{"additionalData":[{"key":"monetary","value":"Sí"},{"key":"contractThirdParts","value":"Sí"},{"key":"providerThirtParts","value":"Exemplo"},{"key":"contractTerm","value":"1"},{"key":"validity","value":"Provisorio"},{"key":"opportunityType","value":"Alta"},{"key":"serviceLines","value":"Alta"},{"key":"topType","value":"Sí"},{"key":"topValue","value":"100"},{"key":"exhangeRateApplicable","value":"Emisión Factura"},{"key":"sla","value":"Especial"},{"key":"salesEquipment","value":"Leasing"},{"key":"company","value":"TASA"},{"key":"stampTax","value":"Sí"},{"key":"internationalService","value":"Sí"},{"key":"internationalSupplier","value":"Antel"},{"key":"documentType","value":"Nota"}],"correlationId":"0012600000Q8BFUAA3","country":"ARG","name":"Telefonica","description":"Example.","type":"No","status":"draft","accountId":"0012600000Q8BFUAA3","opportunityId":"00626000005B3WuAAK","legalId":{"country":"Argentina","nationalIDType":"NIF","nationalID":"000000000X"},"signatureDate":"2017-01-20T10:53:58.223Z","contractStartDate":"2017-01-20T10:53:58.223Z","serviceStartDate":"2017-01-20T10:53:58.223Z","serviceEndDate":"2017-01-20T10:53:58.223Z","autoRenewal":"true","allowCancel":"true","hasCancelPenalty":"true","hasPastDuePenalty":"true","price":{"amount":"100","units":"ARS"},"tax":{"amount":"100","units":"ARS"}}}';
    
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Eduardo Ventura
    Company:        Everis España
    Description:    Generic XML to JSON parser - Test Method.
    
    History:
    <Date>                      <Author>                        <Change Description>
    13/02/2017                  Eduardo Ventura                 Versión Inicial.
    ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    @isTest public static void xmlToJSON() {
        /* Test */
        Test.startTest();
        System.assertEquals(json.length(), BI_Serializer.parseJSON(xml).length());
        Test.stopTest();
    }
    
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Eduardo Ventura
    Company:        Everis España
    Description:    Generic JSON to XML parser - Test Method.
    
    History:
    <Date>                      <Author>                        <Change Description>
    13/02/2017                  Eduardo Ventura                 Versión Inicial.
    ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    @isTest public static void jsonToXML() {
        /* Test */
        Test.startTest();
        System.assertEquals(xml.length(), BI_Serializer.parseXML(json).length());
        Test.stopTest();
    }
}