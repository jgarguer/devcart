@isTest
private class BI_COL_Proyectos_ColombiaMethods_TEST {


	static{
		BI_TestUtils.throw_exception = false;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       NEAborda
	    Description:   Test SetUp Method
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    08/06/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@TestSetup static void setup(){

		List <Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List <String>{Label.BI_Colombia});

		Opportunity opp = new Opportunity(
            Name = 'Test2',
            CloseDate = Date.today().addDays(2),
            StageName = Label.BI_F6Preoportunidad,
            AccountId = lst_acc[0].Id,
            BI_Opportunity_Type__c = 'Fijo',
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Colombia
        );
        insert opp;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       NEAborda
	    Description:   Test method that manage the code coverage of BI_COL_Proyectos_ColombiaMethods.setOwner()
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    08/06/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void setOwner_TEST(){

		User otherUser = [SELECT Id FROM User WHERE Id !=: UserInfo.getUserId() AND IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema LIMIT 1];
		User me = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];

		Group cola;
		//Insert Queue
		System.runAs(me){
			cola = new Group(
				Type = 'Queue',
				Name = 'Test',
				DeveloperName = 'BI_Test_pc'
			);
			insert cola;
	
	
			QueueSobject qsobj = new QueueSobject(
				QueueId = cola.Id,
				SobjectType = 'BI_COL_Proyectos_Colombia__c'
			);
			insert qsobj;
		}

		List <BI_COL_Asignacion_proyectos__c> lst_cs = new List <BI_COL_Asignacion_proyectos__c>();
		//Insert assignment criteria
		BI_COL_Asignacion_proyectos__c cs1 = new BI_COL_Asignacion_proyectos__c(
			Name = '0001',
			BI_Cola__c = false,
			BI_Id_Usuario_o_cola__c = otherUser.Id,
			BI_Segmento__c = 'Empresas'
		);
		lst_cs.add(cs1);

		BI_COL_Asignacion_proyectos__c cs2 = new BI_COL_Asignacion_proyectos__c(
			Name = '0002',
			BI_Cola__c = true,
			BI_Id_Usuario_o_cola__c = cola.DeveloperName,
			BI_Segmento__c = 'Negocios'
		);
		lst_cs.add(cs2);

		insert lst_cs;

		//Empresas segment assignment
		Account acc = [SELECT Id, BI_Segment__c FROM Account LIMIT 1];
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

		acc.BI_Segment__c = 'Empresas';
		update acc;

		BI_COL_Proyectos_Colombia__c pcol1 = new BI_COL_Proyectos_Colombia__c(
			BI_COL_Oportunidad__c = opp.Id
		);
		insert pcol1;

		System.assertEquals([SELECT OwnerId FROM BI_COL_Proyectos_Colombia__c WHERE Id =: pcol1.Id LIMIT 1].OwnerId, otherUser.Id);

		//Negocios segment assignment
		acc.BI_Segment__c = 'Negocios';
		update acc;

		BI_COL_Proyectos_Colombia__c pcol2 = new BI_COL_Proyectos_Colombia__c(
			BI_COL_Oportunidad__c = opp.Id
		);
		insert pcol2;

		System.assertEquals([SELECT OwnerId FROM BI_COL_Proyectos_Colombia__c WHERE Id =: pcol2.Id LIMIT 1].OwnerId, cola.Id);

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       NEAborda
	    Description:   Test method that manage the code coverage of all the exceptions thrown in BI_COL_Proyectos_ColombiaMethods
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    08/06/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void exceptions(){

		BI_TestUtils.throw_exception = true;

		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

		BI_COL_Proyectos_Colombia__c pcol1 = new BI_COL_Proyectos_Colombia__c(
			BI_COL_Oportunidad__c = opp.Id
		);
		insert pcol1;

	}
	
	
}