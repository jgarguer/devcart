/**
* Avanxo Colombia
* @author 			Manuel Medina href=<mmedina@avanxo.com>
* Project:			Telefonica
* Description:		Clase de rueba que realiza la cobertura del trigger BI_COL_Delete_Attached_tgr
*
* Changes (Version)
* -------------------------------------
*			No.		Date			Author					Description
*			-----	----------		--------------------	---------------
* @version	 1.0	2015-07-29		Manuel Medina (MM)      Definicion de la clase de prueba.
*            1.1	2016-12-29		Gawron, Julian E. 		Add System.runAs
*************************************************************************************************************/
@isTest
private class BI_COL_Delete_Attached_tst {
	
	public static final String strTSTCase1					= 'TSTCase1';
	public static final String strTSTCase2					= 'TSTCase2';
	Public static User 													objUsuario = new User();
	Public static list<Profile> 											lstPerfil;
	Public static list <UserRole> 											lstRoles;

	public static Attachment createAttachment( String strTSTCase ){
		
		String strRTAnexos										= [SELECT Id
																FROM RecordType
																WHERE SobjectType		= 'BI_COL_Anexos__c'
																AND DeveloperName		= 'BI_FUN'
																LIMIT 1
															].Id;
		
		BI_COL_Anexos__c objCOLAnexo						= new BI_COL_Anexos__c();
		objCOLAnexo.Name									= 'FUN-0041414';
		objCOLAnexo.RecordTypeId							= strRTAnexos;
		insert objCOLAnexo;
		
		Attachment objAttachment							= new Attachment();
		objAttachment.Body									= Blob.valueOf( 'TSTAttachment' );
		objAttachment.ParentId								= objCOLAnexo.Id;
		objAttachment.Name									= strTSTCase;
		insert objAttachment;
		
		PermissionSet objPermissionSet						= new PermissionSet();
		objPermissionSet.Name								= 'BI_COL_Perfil_Administrador_de_Ventas';
		objPermissionSet.Label								= 'BI_COL_Perfil_Administrador_de_Ventas';
		insert objPermissionSet;
		
		return objAttachment;
	}
 
    static testMethod void BI_COL_DeleteAttached_TestCase2() {        
        
        //lstPerfil
        lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];

        //lstRol
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);
        System.runAs(new User(Id=UserInfo.getUserId())){
        	       insert objUsuario;
        }

       Test.startTest();

		System.runAs( objUsuario ){	
	        try{        	
	        	delete createAttachment( strTSTCase2 );
	        	
	        }catch( DMLException e ){
	        	for( Integer i=0; i<e.getNumDml(); i++ ){
					System.debug( '\n\n\t<<<<<<<<< DMLException >>>>>>>>> \n\t\t@@--> getDmlFieldNames > ' + e.getDmlFieldNames(i) + '\n\t\t@@--> getDmlId > ' + e.getDmlId(i) + '\n\t\t@@--> getDmlMessage > '+ e.getDmlMessage(i) + '\n\t\t@@--> getDmlType > '+ e.getDmlType(i) + '\n\n' );
					System.assertEquals( 'No se pueden eliminar archivos' , e.getDmlMessage(i), 'TestCase2: No se cubrio el escenario.' );
				}        	
	        }
		}
        Test.stopTest();
    }

        static testMethod void BI_COL_DeleteAttached_TestCase1() {        
        
        //lstPerfil
        lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];

        //lstRol
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);
        System.runAs(new User(Id=UserInfo.getUserId())){
        	       insert objUsuario;
        }

       Test.startTest();

		System.runAs( objUsuario ){	
	        try{        	
	        	delete createAttachment( strTSTCase1 );
	        	
	        }catch( DMLException e ){
	        	for( Integer i=0; i<e.getNumDml(); i++ ){
					System.debug( '\n\n\t<<<<<<<<< DMLException >>>>>>>>> \n\t\t@@--> getDmlFieldNames > ' + e.getDmlFieldNames(i) + '\n\t\t@@--> getDmlId > ' + e.getDmlId(i) + '\n\t\t@@--> getDmlMessage > '+ e.getDmlMessage(i) + '\n\t\t@@--> getDmlType > '+ e.getDmlType(i) + '\n\n' );
					System.assertEquals( 'No se pueden eliminar archivos' , e.getDmlMessage(i), 'TestCase1: No se cubrio el escenario.' );
				}        	
	        }
		}
        Test.stopTest();
    }
}