/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Controller of BI_CoE_OrderItemLayout and BI_CoE_EditOrderItem pages
    
    History:
    
    <Date>            <Author>          <Description>
    22/11/2015        Guillermo Muñoz   Initial version
    13/04/2016        Guillermo Muñoz   Method recurrenteAnteriorEditable() added
    08/07/2016        Jorge Galindo     Promotion price recalculation functionality added
    19/07/2016        Jorge Galindo     Promotion price recalculation Opty and Order added
    19/07/2016        AJAE              Otpy BI_Casilla_desarrollo__c marked when price is changed
    21/07/2016        Jorge Galindo     Added set flag of special offer to true in Opty
 --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_CoE_EditOrderItem_CTRL {

    public NE__OrderItem__c record;
    public Boolean costLayout {get;set;}
    public Boolean editable {get;set;}
    public Boolean noEditable {get;set;}
    // JGL 08/07/2016
    public String offer {get;set;}
    public List<NE__OrderItem__c> lst_OItoUpd;
    
    public Boolean promotionSelectionEnabled {get;set;}
    public Boolean promotionFilled {get;set;}
    public Boolean qtyReadOnly {get;set;}
    public Integer qtyValue {get;set;}
    public String promotionName {get;set;}
    // prices, just one time fee, because we are changing just the Terminal (for Order and Opty recalculation)
    public double totalOneTimeFee;
    public double totalOneTimeCost;
    public List<Opportunity> lst_OptyToUpd;
    public List<NE__Order__c> lst_ORToUpd;
    public String  optyId;
    public String  orderId;
   
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public BI_CoE_EditOrderItem_CTRL(ApexPages.StandardController stdController) {
        record = (NE__OrderItem__c)stdController.getRecord();
        lst_OItoUpd = new List<NE__OrderItem__c>();
        //JGL for Order and Opty recalculation
        lst_OptyToUpd = new List<Opportunity>();
        lst_ORToUpd = new List<NE__Order__c>();
        // End JGL
        costLayout = getCostLayout();
        recurrenteAnteriorEditable();
    }

    
    public boolean getCostLayout(){

        if(record.RecordTypeId == [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Editable_Cost' AND SObjectType = 'NE__OrderItem__c' LIMIT 1].Id)  return true;
         
        return false;

    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Method that redirect the user to the standard edit page of OrderItem or to BI_CoE_OrderItemLayout page
    
    IN:            
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    22/11/2015        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference redirect(){

        String idStr = record.Id;
        Id idval = idStr;
        idStr = idval;
        String toReturn = URL.getSalesforceBaseUrl().toExternalForm() + '/' + record.Id + '/e?retURL=%2F' + idStr + '&nooverride=0';
        Id userProfId = UserInfo.getProfileId();
        Map <Id, Profile> map_profs = new Map<Id,Profile>([SELECT Id,Name FROM Profile WHERE Name LIKE '%TGS%' OR Name =: Label.BI_Administrador_Sistema]);
        if(!map_profs.containsKey(userProfId)){
            toReturn = '/apex/BI_CoE_OrderItemLayout?Id=' + record.Id ;
        }
        
        PageReference page = new PageReference(toreturn);
        page.setRedirect(true);
        return page;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Method that save the record
    
    IN:            
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    22/11/2015        Guillermo Muñoz   Initial version
    08/07/2016        Jorge Galindo     Modified for entering the condition of Valid Promotion Selected
    19/07/2016        Alfonso Alvarez   Modificado por regla de validación sobre Oportunidades
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference save(){
        PageReference page;
        
        if (checkValidPromotion()){
            page = new PageReference('/' + record.Id);
            page.setRedirect(true);
            lst_OItoUpd.add(record);
            update lst_OItoUpd;
            
            //JGL Order and Opty one time fee recalculation
            if(!lst_OptyToUpd.isEmpty()){
                 //PRICES
                totalCalculation();
       
                lst_ORToUpd[0].NE__One_Time_Fee_Total__c = totalonetimefee;
            
                update lst_ORToUpd;
            
                lst_OptyToUpd[0].BI_Ingreso_por_unica_vez__c = totalonetimefee;
                // Modificado AJAE 19/07/2016. Añadido casilla de desarrollo para evitar regla de validación de actualización de Opty
                // que no permite modificar Ingreso por Unica vez ni Recurrente Bruto sobre Opty con productos. 
                lst_OptyToUpd[0].BI_Casilla_desarrollo__c = true;
                // JGL 21/07/2016 added set flag of special offer to true 
                lst_OptyToUpd[0].BI_FVI_Oferta_Especial_Movil__c = true;
                
                update lst_OptyToUpd;
                
            }
           
   
            
            
        }else{
            // Add a custom error message to the page
            ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR,System.Label.BI_CoE_CheckPromotion_Error);
            ApexPages.addMessage(myMsg); 
            page = null;
        }
        
        return page;
    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Method that save the record and redirect to the create orderItem page.
    
    IN:            
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    22/11/2015        Guillermo Muñoz   Initial version
    08/07/2016        Jorge Galindo     Modified for entering the condition of Valid Promotion Selected
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference saveAndNew(){
        
        PageReference page;
        
        if (checkValidPromotion()){
            String idPrefix = NE__OrderItem__c.sobjecttype.getDescribe().getKeyPrefix();
            page = new PageReference('/setup/ui/recordtypeselect.jsp?ent=01Iw0000000ipe4&retURL=%2Fa'+ idPrefix +'1000001ZuYD&save_new_url=%2Fa0a%2Fe%3FretURL%3D%252F' + idPrefix + '11000001ZuYD'); 
            page.setRedirect(true);     
            lst_OItoUpd.add(record);
            update lst_OItoUpd;
            
             //JGL Order and Opty one time fee recalculation
             if(!lst_OptyToUpd.isEmpty()){
                 //PRICES
                totalCalculation();
       
                lst_ORToUpd[0].NE__One_Time_Fee_Total__c = totalonetimefee;
            
                update lst_ORToUpd;
            
                lst_OptyToUpd[0].BI_Ingreso_por_unica_vez__c = totalonetimefee;
                // Modificado AJAE 19/07/2016. Añadido casilla de desarrollo para evitar regla de validación de actualización de Opty
                // que no permite modificar Ingreso por Unica vez ni Recurrente Bruto sobre Opty con productos. 
                lst_OptyToUpd[0].BI_Casilla_desarrollo__c = true; 
                // JGL 21/07/2016 added set flag of special offer to true 
                lst_OptyToUpd[0].BI_FVI_Oferta_Especial_Movil__c = true;
                 
                update lst_OptyToUpd;
                
            }
            
        }else{
            ApexPages.Message myMsg = new  ApexPages.Message(ApexPages.Severity.ERROR,System.Label.BI_CoE_CheckPromotion_Error);
            ApexPages.addMessage(myMsg); 
            page = null;
        }
            return page;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda.es
    Description:   Method that redirect the user to the standard layout of the OrderItem
    
    IN:            
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    22/11/2015        Guillermo Muñoz   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference cancel(){

        PageReference page = new PageReference('/' + record.Id);
        page.setRedirect(true);
        return page;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Aborda
        Description:   Method that enable or disable the edition of BI_Ingreso_Recurrente_Anterior_Producto__c field
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        13/04/2016                      Guillermo Muñoz             Initial version
        25/04/2016                      Guillermo Muñoz             Changed filter to BI_Descuento_bloqueado__c
        08/07/2016          Jorge Galindo
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void recurrenteAnteriorEditable(){

        //JGL 08/07/2016 added a field for OfferID and BI_Opportunity_type
        
        NE__OrderItem__c ord = [SELECT NE__Qty__c,NE__OrderId__c,NE__OrderId__r.NE__OptyId__c,NE__OrderId__r.NE__OptyId__r.BI_Country__c,NE__OrderId__r.NE__OptyId__r.StageName,NE__OrderId__r.NE__OptyId__r.BI_Opportunity_type__c,BI_FVI_PromotionCode__c,BI_FVI_PromotionCode__r.BI_FVI_GenOfferld__c,BI_FVI_PromotionCode__r.Name,NE__Parent_Order_Item__c FROM NE__OrderItem__c WHERE Id =: record.Id LIMIT 1];
        
         //JGL 08/07/2016 added a field for OfferID
        offer = ord.BI_FVI_PromotionCode__r.BI_FVI_GenOfferld__c;
        qtyValue = (integer) ord.NE__Qty__c;
        
        // Order and Opty recalculation
        optyId = ord.NE__OrderId__r.NE__OptyId__c;
        orderId = ord.NE__OrderId__c;
        
        string auxPromotionCode = ord.BI_FVI_PromotionCode__c;
        
        promotionSelectionEnabled  = (ord.NE__OrderId__r.NE__OptyId__r.BI_Opportunity_type__c == 'Móvil' && ord.NE__Parent_Order_Item__c == null)? true : false;
        
         if( promotionSelectionEnabled && auxPromotionCode != null &&  auxPromotionCode !=''){
            promotionFilled = true;
            qtyReadOnly = false;
            promotionName = ord.BI_FVI_PromotionCode__r.Name;
           
        }else{
            promotionFilled = false;
            qtyReadOnly = true;
            promotionName = ord.BI_FVI_PromotionCode__r.Name;
        }
        
        
        System.debug('++++++++++++' + ord.NE__OrderId__r.NE__OptyId__r.StageName);

        if(!costLayout){
            //ANCR 11.28.2017: Se incluye que en etapa F6 pueda editar el campo BI_Ingreso_Recurrente_Anterior_Producto__c el país de Argentina
            if (ord.NE__OrderId__r.NE__OptyId__r.BI_Country__c == Label.BI_Argentina && ord.NE__OrderId__r.NE__OptyId__r.StageName == Label.BI_F6Preoportunidad){
                editable = true;
                noEditable = false;
                System.debug('\n\n +++Pais OPP+++++++++' + ord.NE__OrderId__r.NE__OptyId__r.BI_Country__c + '\n\n++++++++++++' + ord.NE__OrderId__r.NE__OptyId__r.StageName);

            }else if(ord.NE__OrderId__r.NE__OptyId__r.StageName != Label.BI_F5DefSolucion && ord.NE__OrderId__r.NE__OptyId__r.StageName != Label.BI_F4DesarrolloOferta){
                editable = false;
                noEditable = true;
                System.debug('++++++++++++' + ord.NE__OrderId__r.NE__OptyId__r.StageName);
             
            }else{
                editable = true;
                noEditable = false;
            }
        }
        else{
            editable = false;
            noEditable = false;
        }
        
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jorge Galindo
        Company:       New Energy Aborda
        Description:   Method that informs OfferId field in VF page when an BI_FVI_PromotionCode__c is selected
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        08/07/2016                      Jorge Galindo             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  public PageReference fillOfferId(){
      // si se cambia a lookup a medida hay que comentar esta línea
      String promotionName = record.BI_FVI_PromotionCode__c;
      
      
      if(promotionName != null && promotionName != ''){
        offer  = [SELECT BI_FVI_GenOfferld__c from BI_FVI_OfflineCatalog__c where id =:promotionName LIMIT 1].BI_FVI_GenOfferld__c;
        qtyReadOnly = false;          
      }else{
        //record.BI_FVI_PromotionCode__c = null;
        qtyReadOnly = true;  
        offer = '';
      }
      
      qtyValue = (integer) record.NE__Qty__c;
    
      return null;        
  }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jorge Galindo
        Company:       New Energy Aborda
        Description:   If BI_FVI_PromotionCode__c is filled, update the price of the device
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        08/07/2016                      Jorge Galindo             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
      public void updateMobileForPromotion(Ne__OrderItem__c equipment,BI_FVI_OfflineCatalog__c promotion){
          // from the Promotion we have to obtain the ProdId of the equipment and  the catalogItem and the prices and costs
          List<NE__Catalog_Item__c> lst_catItems = [select id,NE__ProductId__c,NE__One_Time_Cost__c  from NE__Catalog_Item__c where NE__ProductId__c IN (:promotion.BI_FVI_P1Product__c,:promotion.BI_FVI_P2Product__c) AND NE__ProductId__r.Payback_Family__c = 'Terminales' AND NE__Type__c ='Child-Product'];
          
          //it has to be only one, then, inform the equipment    
          equipment.NE__ProdId__c = lst_catItems[0].NE__ProductId__c;
          equipment.NE__CatalogItem__c = lst_catItems[0].id;
          
          if(equipment.NE__ProdId__c == promotion.BI_FVI_P1Product__c){
               equipment.NE__OneTimeFeeOv__c = promotion.BI_FVI_P1NRC__c;
          }else if(equipment.NE__ProdId__c == promotion.BI_FVI_P2Product__c){
               equipment.NE__OneTimeFeeOv__c = promotion.BI_FVI_P2NRC__c;
          }
         
          lst_OItoUpd.add(equipment);
          
          //JGL For Order and Opty one time fee recalculation
          lst_OptyToUpd = [SELECT id,BI_Ingreso_por_unica_vez__c FROM Opportunity WHERE id =: optyId];
          lst_ORToUpd = [SELECT id,NE__One_Time_Fee_Total__c FROM NE__Order__c WHERE id =: orderId];
          
      }   

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Humberto Nunes
        Company:       New Energy Aborda
        Description:   If BI_FVI_PromotionCode__c is filled, update the price of the Plan
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        07/04/2017                      Humberto Nunes              Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
      public void updatePlanForPromotion(Ne__OrderItem__c plan,BI_FVI_OfflineCatalog__c promotion){
          // from the Promotion we have to obtain the ProdId of the plan and  the catalogItem and the prices and costs
          List<NE__Catalog_Item__c> lst_catItems = [select id,NE__ProductId__c,NE__One_Time_Cost__c  from NE__Catalog_Item__c where NE__ProductId__c IN (:promotion.BI_FVI_P1Product__c,:promotion.BI_FVI_P2Product__c) AND NE__ProductId__r.Payback_Family__c = 'Planes' AND NE__Type__c ='Child-Product'];
          
          //it has to be only one, then, inform the plan    
          plan.NE__ProdId__c = lst_catItems[0].NE__ProductId__c;
          plan.NE__CatalogItem__c = lst_catItems[0].id;
          
          if(plan.NE__ProdId__c == promotion.BI_FVI_P1Product__c)
          {
               plan.NE__RecurringChargeOv__c = promotion.BI_FVI_P1RC__c;
          }
          else 
              if(plan.NE__ProdId__c == promotion.BI_FVI_P2Product__c)
              {
                   plan.NE__RecurringChargeOv__c = promotion.BI_FVI_P2RC__c;
              }
         
          lst_OItoUpd.add(plan);
          
          //JGL For Order and Opty one time fee recalculation
          lst_OptyToUpd = [SELECT id,BI_Ingreso_por_unica_vez__c FROM Opportunity WHERE id =: optyId];
          lst_ORToUpd = [SELECT id,NE__One_Time_Fee_Total__c FROM NE__Order__c WHERE id =: orderId];
          
      }     
   
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jaime Regidor
        Company:       New Energy Aborda
        Description:   If BI_FVI_PromotionCode__c is filled, update the price of the Fija INFOINTERNE
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        07/04/2017                      Humberto Nunes              Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
      public void updateFijaForPromotion(Ne__OrderItem__c plan,BI_FVI_OfflineCatalog__c promotion){
          // from the Promotion we have to obtain the ProdId of the plan and  the catalogItem and the prices and costs
          List<NE__Catalog_Item__c> lst_catItems = [select id,NE__ProductId__c,NE__One_Time_Cost__c  from NE__Catalog_Item__c where NE__ProductId__c IN (:promotion.BI_FVI_P1Product__c,:promotion.BI_FVI_P2Product__c) AND NE__ProductId__r.Payback_Family__c = 'Equipos' AND NE__Type__c ='Child-Product'];
          
          //it has to be only one, then, inform the plan    
          plan.NE__ProdId__c = lst_catItems[0].NE__ProductId__c;
          plan.NE__CatalogItem__c = lst_catItems[0].id;
          
          if(plan.NE__ProdId__c == promotion.BI_FVI_P1Product__c)
          {
               plan.One_Time_Cost__c = promotion.BI_FVI_P1NRCost__c;
               plan.Recurring_Cost__c = promotion.BI_FVI_P1RC__c;
          }
          else 
              if(plan.NE__ProdId__c == promotion.BI_FVI_P2Product__c)
              {
                   plan.One_Time_Cost__c = promotion.BI_FVI_P2NRCost__c;
                   plan.Recurring_Cost__c = promotion.BI_FVI_P2RC__c;
              }
         
          lst_OItoUpd.add(plan);
          update plan;
          
          //JGL For Order and Opty one time fee recalculation
          lst_OptyToUpd = [SELECT id,BI_Ingreso_por_unica_vez__c FROM Opportunity WHERE id =: optyId];
          lst_ORToUpd = [SELECT id,NE__One_Time_Fee_Total__c FROM NE__Order__c WHERE id =: orderId];
          
      }     
   
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jorge Galindo
        Company:       New Energy Aborda
        Description:   If BI_FVI_PromotionCode__c is removed, update the price of the device to Original
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        08/07/2016                      Jorge Galindo             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
      public void updatePricesToOriginal(Ne__OrderItem__c equipment){
           // we have to retrieve the price of catalog Item corresponding to the device
           // then, compare it with the child OI's price, if they are differents (a promotion has been remove) 
           
          if(equipment.NE__OneTimeFeeOv__c != equipment.NE__CatalogItem__r.NE__Base_OneTime_Fee__c){
               equipment.NE__OneTimeFeeOv__c = equipment.NE__CatalogItem__r.NE__Base_OneTime_Fee__c;    
               lst_OItoUpd.add(equipment);
          }
          
           //JGL For Order and Opty one time fee recalculation
           lst_OptyToUpd = [SELECT id,BI_Ingreso_por_unica_vez__c FROM Opportunity WHERE id =: optyId];
          
           lst_ORToUpd = [SELECT id,NE__One_Time_Fee_Total__c FROM NE__Order__c WHERE id =: orderId];
          
      }   
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jorge Galindo
        Company:       New Energy Aborda
        Description:   If BI_FVI_PromotionCode__c is filled, check that the PLAN in promotion match with the PLAN OI in Order (OIChilds)
                        if it matches, we change the product (and prices) of the Terminal OI
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        08/07/2016                      Jorge Galindo               Initial version
       07/04/2017                      Humberto Nunes              Se Agrego Tambien el Plan Y el PAIS para solo aplicarlo a ARGENTINA
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
      public Boolean checkValidPromotion(){
           
          // if there's any case that promotion is informed, but it's bad data
          if(!promotionSelectionEnabled){
              return true;
          }
          
          
          
          Boolean valid = false;
           List<NE__OrderItem__c> lst_OIChilds = [SELECT id,Name,NE__ProdId__c,NE__OneTimeFeeOv__c,NE__ProdId__r.Payback_Family__c,NE__CatalogItem__r.NE__Base_OneTime_Fee__c, NE__OrderId__r.BI_Country__c, BI_FVI_Country__c,BI_FVI_PromotionCode__c FROM NE__OrderItem__c WHERE NE__Parent_Order_Item__c=:record.id]; //ANCR 12.05.2017 Se incluye el campo BI_FVI_PromotionCode__c por error reportado ehelp 03046537
           List<NE__OrderItem__c> lst_OIChildsForUpd = new List<NE__OrderItem__c>();
           List<NE__OrderItem__c> lst_planOIChilds = new List<NE__OrderItem__c>();
           
           // if you put directly record.BI_FVI_PromotionCode__c in the if conditions is launching an exception ¿¿??? 
          string auxPromotionCode = record.BI_FVI_PromotionCode__c;
      
          if(auxPromotionCode != null && auxPromotionCode != '') {
            // retrieve the Device and the Plan for selected Promotion
            BI_FVI_OfflineCatalog__c promoObj = [SELECT BI_FVI_P1Product__c,BI_FVI_P2Product__c,BI_FVI_P1NRC__c,BI_FVI_P2NRC__c,BI_FVI_P1RC__c,BI_FVI_P2RC__c, BI_FVI_GenCountry__c from BI_FVI_OfflineCatalog__c where id =:auxPromotionCode LIMIT 1]; 
            
            System.debug('++++++++++++ BI_CoE_EditOrderItem_CTRL: checkValidPromotion: P1: ' + promoObj.BI_FVI_P1Product__c + '; P2: ' + promoObj.BI_FVI_P2Product__c);
            // retrieve the Device and the Plan (OI Childs for selected OI)
            
            
            // we have to see if all the products in the childOi are in the promotion  
            
              for(NE__OrderItem__c childItem: lst_OIChilds){
                  if(childItem.NE__ProdId__r.Payback_Family__c == 'Terminales'){
                      
                      lst_OIChildsForUpd.add(childItem);
                  }else if(childItem.NE__ProdId__r.Payback_Family__c == 'Planes'){
                       lst_planOIChilds.add(childItem);
                  }

              }
              
               if(!lst_planOIChilds.isEmpty() && (lst_planOIChilds[0].NE__ProdId__c == promoObj.BI_FVI_P1Product__c || lst_planOIChilds[0].NE__ProdId__c == promoObj.BI_FVI_P2Product__c)){
                      valid = true;
               }
              
               // If it's valid, we change the price
              if(valid && !lst_OIChildsForUpd.isEmpty()){
                  System.debug('++++++++++++ BI_CoE_EditOrderItem_CTRL: checkValidPromotion: ChildProdForUpd: ' + lst_OIChildsForUpd[0].Name);
                  updateMobileForPromotion(lst_OIChildsForUpd[0], promoObj);
              }

            // If it's valid, we change the price
              if(valid && !lst_planOIChilds.isEmpty() && promoObj.BI_FVI_GenCountry__c == 'Argentina') // lst_planOIChilds[0].NE__OrderId__r.BI_Country__c, lst_planOIChilds[0].BI_FVI_Country__c 
              {
                  System.debug('++++++++++++ BI_CoE_EditOrderItem_CTRL: checkValidPromotion: lst_planOIChilds: ' + lst_planOIChilds[0].Name);
                  updatePlanForPromotion(lst_planOIChilds[0], promoObj);
              }


          }else{
            // No promotion ,if removed, check if price is the same that in the configuration item 
            System.debug('++++++++++++ BI_CoE_EditOrderItem_CTRL: checkValidPromotion: Promotion removed. Childs' + lst_OIChilds.size());
              if(promotionFilled){
                for(NE__OrderItem__c childItem: lst_OIChilds){
                  if(childItem.NE__ProdId__r.Payback_Family__c == 'Terminales'){
                      lst_OIChildsForUpd.add(childItem);
                  } 
                }
                  if(!lst_OIChildsForUpd.isEmpty()){
                      updatePricesToOriginal(lst_OIChildsForUpd[0]); 
                  }                 
              }
              valid = true;
          }      
          return valid;
      } 
    
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jorge Galindo
        Company:       New Energy Aborda
        Description:   recalculate the prices when terminal is changed
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        18/07/2016                      Jorge Galindo             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    public void totalCalculation(){
        
        // first we retrieve the root OItems, but just the 
        
        List<NE__OrderItem__c> OItems = [SELECT id,NE__Qty__c,NE__OneTimeFeeOv__c,NE__One_Time_Cost__c,NE__Parent_Order_Item__r.NE__Qty__c FROM NE__OrderItem__c where NE__ProdId__r.Payback_Family__c = 'Terminales' AND NE__Parent_Order_Item__r.NE__OrderId__c  =:orderId];
        
        totalonetimefee = 0;
        totalonetimecost = 0;
        
        // we recalculate the price
        for(NE__OrderItem__c oi: OItems){
            totalonetimefee +=  oi.NE__OneTimeFeeOv__c *  oi.NE__Qty__c * oi.NE__Parent_Order_Item__r.NE__Qty__c;    
            totalonetimecost +=  oi.NE__One_Time_Cost__c *  oi.NE__Qty__c * oi.NE__Parent_Order_Item__r.NE__Qty__c;    
        }
        
    }

    
}