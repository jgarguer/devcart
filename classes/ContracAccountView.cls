public with sharing class ContracAccountView {
	
	private ApexPages.StandardController controller;
	NE__Contract_Account_Association__c cAcc{get;set;}
	
	public ContracAccountView(ApexPages.StandardController controller) 
	{
		this.controller = controller;
		cAcc 	= 	(NE__Contract_Account_Association__c)controller.getRecord();
		cAcc	=	[SELECT Id, NE__Contract_Header__c FROM NE__Contract_Account_Association__c WHERE id =: cAcc.id];
	}

	public PageReference redirectToContract()
	{
		NE__Contract_Header__c contractHeader	=	[SELECT id, (SELECT id FROM NE__Contracts__r ORDER by NE__Version__c desc nulls last limit 1) FROM NE__Contract_Header__c WHERE id =: cAcc.NE__Contract_Header__c];
		
		if(contractHeader.NE__Contracts__r.size() > 0)
			return new PageReference('/'+contractHeader.NE__Contracts__r[0].id);
		else
			return new PageReference('/'+contractHeader.id);
	}
}