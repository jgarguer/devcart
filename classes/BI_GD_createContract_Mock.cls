/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Jesús Martínez
Company:        Everis España
Description:    Migración de Contract: Simulación POST.

History:
 
<Date>                      <Author>                        <Change Description>
26/10/2016                  Jesús Martínez               	Versión Inicial.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class BI_GD_createContract_Mock implements HttpCalloutMock{

    public static boolean error = false;
    public static boolean create = true;
    
         
    global HTTPResponse respond (HttpRequest request) {
        BI_GD_Config__c mc = BI_GD_Config__c.getOrgDefaults();                                                 
           
        /* Variable Configuration */
        HttpResponse response = new HttpResponse();
        
        /* Response Generation */
        if((request.getMethod() == 'POST' || request.getMethod() == 'PUT') &&  mc.BI_GD_system__c == 'CRM'){ 
            /*RESPONSE CREATION*/
            if (request.getMethod() == 'POST'){
                response.setStatusCode(201);
                response.setBody('201 Created');                    
            } else {
                response.setStatusCode(200);
                response.setBody('200 Ok');
            }
            
            response.setHeader('Location', 'https://{apiRoot}/contracts/123456789A');          
        } else {
            response.setStatusCode(500);
            response.setBody('500 Error');
        }
        
        return response;
    }
}