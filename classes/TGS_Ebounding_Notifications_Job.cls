/*----------------------------------------------------------------------------------------------------------
Author:        Oscar Ena
Company:       Accenture - New Energy Aborda
Description:   Send notifications to Ebounding 



<Date>            <Author>                              <Description>
06/06/2017        Oscar Ena                            Initial version.
------------------------------------------------------------------------------------------------------------*/
public class TGS_Ebounding_Notifications_Job implements Queueable, Database.AllowsCallouts {
	
	List <Case> lst_caseToSend = new List <Case>();

	public TGS_Ebounding_Notifications_Job (List <Case> lst_caseToSend){
		this.lst_caseToSend = lst_caseToSend;
	}

	public void execute(QueueableContext context) {

        /*for (Case inCase : mapId_CaseToSend.values()){
        	BI_RestRequestHelper.sendEboundingNotification(inCase.BI_Id_SistemaLegado__c, inCase.Status);	
        }*/
        if(lst_caseToSend != null && !lst_caseToSend.isEmpty()){

        	Case caseToSend = lst_caseToSend.remove(0);
        	BI_RestRequestHelper.sendEboundingNotification(caseToSend.BI_Id_SistemaLegado__c, caseToSend.Status);

        	if(!lst_caseToSend.isEmpty()){

        		if(!Test.isRunningTest()){
        			System.enqueueJob(new TGS_Ebounding_Notifications_Job(lst_caseToSend));
        		}
        	}
        }
	}
}