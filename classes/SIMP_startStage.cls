public class SIMP_startStage {

    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();

    static{

        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'Opportunity' OR sObjectType = 'Case' ]){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Suarez
    Company:       Salesforce.com
    Description:   Change the stage of the opty depending in the owner of the account
    
    History:
    
    <Date>            <Author>          <Description>
    02/02/2016        Javier Suarez      Initial version
    30/05/2016		  Manuel Medina      Amend to check if user belongs to Account team as "Account Manager" to copy cat behavior as owner
	13/06/2016		  Manuel Medina		 Added setOptyMember, to add creator to Opty team if not account owner or Account Manager at Account Team
    28/11/2016        Alvaro García      Add funcionality of the method setStageName
    22/03/2018        Jaime Regidor      Quitar el cambio de etapa dependiendo del owner de la cuenta para usuarios con permisos distintos a Empresas Platino.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void setStageName(List<Opportunity> optys){                
                
        List<id> ids = new List<id>();
        
        List<id> idEjecutivo = new List<id>();
        
		boolean hasAtm = False;
        
        boolean notPortal = True;

        List <Opportunity> toProcess = new List <Opportunity>();

        Set <Id> set_idParent = new Set <Id>();
        List <Opportunity> lst_oppWithParent = new List <Opportunity>();
        
        for (Opportunity o: optys){ 
            if(o.BI_Oportunidad_padre__c != null && o.RecordTypeId == MAP_NAME_RT.get('BI_Ciclo_completo') &&  o.VE_Express__c == true){
                set_idParent.add(o.BI_Oportunidad_padre__c);
                lst_oppWithParent.add(o);
            }
            else{
                ids.add(o.AccountId); 
                idEjecutivo.add(o.OwnerId);
                toProcess.add(o);
            }
        }        
        
        if(!toProcess.isEmpty()){
            Map<id,id> mapAcctOwner = new Map<id,id>();
            
            for (Account a: [select id,OwnerId from account where id in :ids]){
                mapAcctOwner.put(a.Id, a.OwnerId);
            }
            
            for (AccountTeamMember atm : [Select id, UserId, TeamMemberRole From AccountTeamMember where AccountID in :ids and TeamMemberRole = 'Account Manager' and UserId = :idEjecutivo]){
    //        System.debug('******### atm.Id: '+atm.Id);
        		if (atm.Id != null) {
        //            System.debug('******### hasAtm: '+hasAtm);
        			hasAtm = True;
        		}		    	  		
            }

            for (User usrr : [Select BI_Permisos__c from User where id = :idEjecutivo ]){
                if (usrr.BI_Permisos__c =='Empresas Platino') {
                    notPortal = False;
                }
            }   
            
            for (Opportunity o: optys){
                if ((o.stageName == 'F5 - Solution Definition' || o.stageName == 'F6 - Prospecting') && o.CampaignId==null && !notPortal){
                    o.stageName = 'F6 - Prospecting';
                    /*o.StageName= (((mapAcctOwner.get(o.accountid)==o.OwnerId) || hasAtm && o.CampaignId==null && !notPortal)?'F5 - Solution Definition':'F6 - Prospecting';*/ // JRM D-000575
                }
    //              o.StageName=((mapAcctOwner.get(o.accountid)==o.OwnerId)&& (o.CampaignId==null))?'F5 - Solution Definition':'F6 - Prospecting';
            } 
        }

        if(!lst_oppWithParent.isEmpty()){
            Map <Id,Opportunity> map_parents = new Map <Id,Opportunity>([SELECT Id, RecordType.DeveloperName FROM Opportunity WHERE Id IN: set_idParent AND RecordType.DeveloperName = 'BI_Oportunidad_Regional']);

            for(Opportunity opp : lst_oppWithParent){
                if(map_parents.containsKey(opp.BI_Oportunidad_padre__c)){
                    opp.StageName = 'F6 - Prospecting';
                }
            }
        }
}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:       
    Company:       Salesforce.com
    Description:   
    
    History:
    
    <Date>            <Author>          <Description>
    11/05/2017        Oscar Jimenez - Andrea Casteñeda   Se agrega Condición para excluir los usuarios de Colombia de la lógica
    23/02/2018        Julián Gawron     Modification of TeamMemberRole and OpportunityAccessLevel
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void setOptyMember(List<Opportunity> optys){

//    System.debug('******Estamos dentro setOptyMember: ');
        if([SELECT Pais__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Pais__c != Label.BI_COL_lblColombia) //OAJF-ANCR - eHelp 02467556
        {        
            List<id> opty = new List<id>();
            
            List<id> ids = new List<id>();
            
            List<id> idEjecutivo = new List<id>();
            
            List<OpportunityTeamMember> memberList = new List<OpportunityTeamMember>(); 
            
            boolean hasAtm = False;
            
            for (Opportunity o: optys){
                opty.add(o.Id);
                ids.add(o.AccountId); 
                idEjecutivo.add(o.CreatedById);
            }
            
            Map<id,id> mapAcctOwner = new Map<id,id>();
            
            for (Account a: [select id,OwnerId from account where id in :ids]){
                mapAcctOwner.put(a.Id, a.OwnerId);
            }
            
          for (AccountTeamMember atm : [Select id, UserId, TeamMemberRole From AccountTeamMember where AccountID in :ids and TeamMemberRole = 'Account Manager' and UserId = :idEjecutivo]){
    //        System.debug('******### atm.Id: '+atm.Id);
    		if (atm.Id != null) {
    //            System.debug('******### hasAtm: '+hasAtm);
    			hasAtm = True;
    		}		    	  		
        }
            for (Opportunity o: optys){
    //        	System.debug('******Pre-Opty Id: '+o.OwnerId+' '+o.accountid+' '+mapAcctOwner.get(o.id));
              
    //			System.debug('******valores del If OwnerID: '+o.OwnerId+' y CreatedById:'+o.CreatedById+' y el Owner de la cuenta es:'+mapAcctOwner.get(o.accountid));

                if(mapAcctOwner.get(o.accountid)==o.OwnerId && o.OwnerId != o.CreatedById){
                    if(!hasAtm){

    //                    System.debug('******Opty Id: '+o.Id);
                        
    //                    OpportunityTeamMember member = new OpportunityTeamMember();  

    					OpportunityTeamMember member = new OpportunityTeamMember(OpportunityId = o.Id,
                                                                                 UserId = o.CreatedById,
                                                                                 TeamMemberRole = 'Account Manager'); //JEG 23/02/2018

                                                                                  //

                        memberList.add(member);

                    }
                }

            }
            
            insert memberList;

            if(opty.size() > 0) {
                // get all of the team members' sharing records
                        List<OpportunityShare> shares = [select Id, OpportunityAccessLevel,  
                          RowCause from OpportunityShare where OpportunityId IN :opty 
                          and RowCause = 'Team' and UserOrGroupId IN :idEjecutivo];

                        // set all team members access to read/write
                        for (OpportunityShare share : shares)  
                          share.OpportunityAccessLevel = 'Read'; //JEG 23/02/2018

                        update shares;
            }
        }
    }
}