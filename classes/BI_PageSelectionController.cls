public with sharing class BI_PageSelectionController
{
    private Sobject obj {get; set;}
    public String url {get; set;}

    private Map<String, List<String>> mapObjPrefixControllerFields
    {
        get
        {
            Map<String, Schema.SObjectType> mapSobjects = Schema.getGlobalDescribe();
            Map<String, List<String>> myMap = new Map<String, List<String>>();
            myMap.put(mapSobjects.get('BI_Linea_de_Recambio__c').getDescribe().getKeyPrefix(), new List<String>{'BI_Solicitud__c'});
            myMap.put(mapSobjects.get('BI_Linea_de_Venta__c').getDescribe().getKeyPrefix(), new List<String>{'BI_Solicitud__c'});
            myMap.put(mapSobjects.get('BI_Linea_de_Servicio__c').getDescribe().getKeyPrefix(), new List<String>{'BI_Solicitud__c', 'BI_Linea_de_recambio__r.BI_Solicitud__c', 'BI_Linea_de_venta__r.BI_Solicitud__c'});
            myMap.put(mapSobjects.get('BI_Solicitud_envio_productos_y_servicios__c').getDescribe().getKeyPrefix(), new List<String>{});
            
            return myMap;
        }
    }

    public BI_PageSelectionController(ApexPages.standardController controller)
    {
    	if (!Test.isRunningTest())
        	controller.addFields(mapObjPrefixControllerFields.get(controller.getId().substring(0,3)));
        else
        	mapObjPrefixControllerFields.isEmpty();
        obj = controller.getRecord();
    }

    private PageReference getTargetPageReference()
    {
        PageReference pTarget;
        
        if (obj.getSObjectType().getDescribe().getName() == 'BI_Linea_de_Venta__c')
        {
            pTarget = Page.BI_LineaVenta;
            pTarget.getParameters().put('id', (String) obj.get('BI_Solicitud__c'));
            pTarget.getParameters().put('idVen', (String) obj.get('Id'));
        }
        else if (obj.getSObjectType().getDescribe().getName() == 'BI_Linea_de_Recambio__c')
        {
            pTarget = Page.BI_LineaRecambio;
            pTarget.getParameters().put('id', (String) obj.get('BI_Solicitud__c'));
            pTarget.getParameters().put('idRec', (String) obj.get('Id'));
        }
        else if (obj.getSObjectType().getDescribe().getName() == 'BI_Linea_de_Servicio__c')
        {
            pTarget = Page.BI_LineaServicio;
            if (obj.get('BI_Solicitud__c') != null)
                pTarget.getParameters().put('id', (String) obj.get('BI_Solicitud__c'));
            else if (obj.get('BI_Linea_de_recambio__c') != null)
                pTarget.getParameters().put('id', (String) obj.getSObject('BI_Linea_de_recambio__r').get('BI_Solicitud__c'));
            else if (obj.get('BI_Linea_de_venta__c') != null)
                pTarget.getParameters().put('id', (String) obj.getSObject('BI_Linea_de_venta__r').get('BI_Solicitud__c'));
                
            pTarget.getParameters().put('idServ', (String) obj.get('Id'));
        }
        else if (obj.getSObjectType().getDescribe().getName() == 'BI_Solicitud_envio_productos_y_servicios__c')
        {
            pTarget = Page.BI_Solicitud_envio;
            pTarget.getParameters().put('id', (String) obj.get('Id'));
        }
        
        return pTarget;
    }
    
    public void doRedirect()
    {
        PageReference pTarget;
        
        if (userHasPermissionAssigned())
        {
            pTarget = new PageReference('/' + (String) obj.get('Id') + '/e');
            pTarget.getParameters().put('nooverride', '1');
			pTarget.getParameters().put('retURL', System.currentPageReference().getParameters().get('retURL'));
            String baseUrl = System.URL.getSalesforceBaseUrl().toExternalForm();
            url = 'https://' + baseUrl.substring(baseUrl.lastIndexOf('--') + 2).replace('visual.','sales').replace('c.', '') + pTarget.getUrl();
            System.debug('url1:' + url);
        }
        else{
			url = System.URL.getSalesforceBaseUrl().toExternalForm() + getTargetPageReference().getUrl();
            System.debug('url2:' + url);
        }
    }
    
    private Boolean userHasPermissionAssigned()
    {
        Integer i = [SELECT count() from PermissionSetAssignment WHERE AssigneeId = :UserInfo.getUserId() AND PermissionSet.Name = 'BI_CHI_Admin_Agendamientos'];
        return i > 0 ? true : false;            
    }
}