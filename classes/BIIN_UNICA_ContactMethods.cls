public class BIIN_UNICA_ContactMethods 
{
/*----------------------------------------------------------------------------------------------------------------------
  Author:        José Luis González
  Company:       HPE
  Description:   
                 
  IN:            ApexTrigger.new
  OUT:           Void
      
  History: 
  
  <Date>              <Author>                            <Change Description>
  22/06/2016          José Luis González                  Initial versión
  06/07/2016          José Luis González                  Check contacts list size
----------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José Luis González Beltrán
    Company:       HPE
    Description:   Method that assign the Account country
    
    IN:            ApexTrigger.New
    OUT:           Void
    
    History: 
    
    <Date>            <Author>                <Change Description>
    22/06/2016        José Luis González      UNICA integrate Contacts with RoD
    26/10/2016        Antonio Pardo           Previous filter only sending Contacts related with Peru Account
    27/10/2016        Antonio Pardo           Optimización
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void createRoDContact(List<Contact> lst_contacts_new)
    {
    if(!lst_contacts_new.isEmpty()){
      
      Set<Id> set_accId = new Set<Id>();
      
      for(Contact c : lst_contacts_new){
        set_accId.add(c.AccountId);
      }
      
      Map<Id, Account> map_accPer = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN : set_accId AND BI_Country__c = :Label.BI_Peru]);

      if(!map_accPer.isempty()){
        for(Contact contact : lst_contacts_new) {
          if(map_accPer.keySet().contains(contact.AccountId)){
            if(!Test.isRunningTest() && FS_CORE_Fullstack_CheckRecursive.runOnce())
            {
              BIIN_UNICA_ContactHandler.afterInsert(JSON.serialize(contact));  
            }
          }
        }
      }
    }
    }
  
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José Luis González Beltrán
    Company:       HPE
    Description:   Method that assign the Account country
    
    IN:            ApexTrigger.New, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>            <Author>                <Change Description>
    22/06/2016        José Luis González      UNICA integrate Contacts with RoD
    27/10/2016        Antonio Pardo           Previous filter only sending Contacts related with Peru Account 
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateRoDContact(List<Contact> lst_contacts_new, List<Contact> lst_contacts_old)
    {

    Integer j = 0 ;
    Set<Id> set_acc = new Set<Id>();

    for(Contact c : lst_contacts_new){

      if(c.AccountId != lst_contacts_old[j].AccountId){
        set_acc.add(lst_contacts_old[j].AccountId);
      }
        set_acc.add(c.AccountId);
      j++;
    }

    if(!set_acc.isEmpty()){
      Map<Id, Account> map_accPer = new Map<Id, Account>([SELECT Id FROM Account WHERE Id IN : set_acc AND BI_Country__c = :Label.BI_Peru]);
    
    if(!map_accPer.isEmpty()){

      if(!lst_contacts_new.isEmpty()){
          for (Integer i = 0; i < lst_contacts_new.size(); i++)
          {
            if(map_accPer.keySet().contains(lst_contacts_new[i].AccountId) || map_accPer.keySet().contains(lst_contacts_old[i].AccountId)){
              String contactOldStr = JSON.serialize(new Contact());
              if(!lst_contacts_old.isEmpty() && lst_contacts_old[i] != null)
              {
                contactOldStr = JSON.serialize(lst_contacts_old[i]);
              }
                if(!Test.isRunningTest() && FS_CORE_Fullstack_CheckRecursive.runOnce())
                {
                    BIIN_UNICA_ContactHandler.afterUpdate(JSON.serialize(lst_contacts_new[i]), contactOldStr);
                }
            }
          }
        }
      }
    }
    }

}