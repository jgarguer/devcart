@isTest
private class BI_CampaignMemberMethods_TEST2 {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_CampaignMemberMethods (seeAllData needed)
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    27/05/2015              Pablo Oliva             Initial Version
    27/01/2017              Pedro Párraga            Class remakes
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static{
        
        BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga 
    Company:       Salesforce.com
    Description:   New Energy Aborda
       
    History: 
    
    <Date>                  <Author>                <Change Description>
    27/01/2017              Pedro Párraga            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    @isTest static void test_Method_One(){

        List<String> lst_pais = new List<String>();
        lst_pais.add('Peru');
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais);
       
        List <Contact> lst_con =BI_DataLoad.loadContacts(1, lst_acc);
       
        List <Lead> lst_ld =BI_DataLoad.loadLeads(1);

        Id idProfile =BI_DataLoad.searchAdminProfile();
        List<User> lst_user = BI_DataLoad.loadUsers(1, idProfile, Label.BI_Administrador);

        List <Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(1, BI_DataLoad.obtainCampaignRecordType());
        lst_camp[0].OwnerId = lst_user[0].Id;
        update lst_camp;

        List<CampaignMember> lst_cm = BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);

        try{
            update lst_cm;
        }catch(Exception e){}
            
        delete lst_cm; 

        BI_CampaignMemberMethods.preventStatusBlockRecord(null);
        BI_CampaignMemberMethods.sendToApproval(null);
        BI_CampaignMemberMethods.preventUpdate(null, null);
        BI_CampaignMemberMethods.createTaskPreOpp(null, null);
        BI_CampaignMemberMethods.manageCampanasCuenta(null, null);
    }  
}