/*------------------------------------------------------------ 
Author:         Marta Laliena
Company:        Deloitte
Description:    SF -> SIGMA mWan integration
                
History
<Date>          <Author>        <Change Description>
22-Jul-2015     Marta Laliena    Initial Version
------------------------------------------------------------*/
global with sharing class TGS_Salesforce_Ordering_Other {

    public enum Operation_StatusType {Processed, Cancelled, Idle}
      
    global with sharing class OutputMapping1{
        webservice String Error_Code;
        webservice String Error_Msg;
        webservice String Operation_Instance_ID;
        webservice Operation_StatusType Operation_Status;
    } 
    
    
    /* infoCI = Map with key field Name and value field value of the CI */
    public static Map<string, String> infoCI;
    
    
     /*------------------------------------------------------------
    Author:         Marta Laliena
    Company:        Deloitte
    Description:    
    History
    <Date>          <Author>        <Change Description>
    22-Jul-2015     Marta Laliena     Initial Version
    ------------------------------------------------------------*/
    webservice static OutputMapping1 Create(String System_ID, String Operation_Number, String Attributes, String Attachment_Name,Blob Attachment_Data){
        OutputMapping1 result = new OutputMapping1 ();
        result.Operation_Status = Operation_StatusType.processed;
        try{
            infoCI = getInfoCI(Attributes);
            Boolean op = false;
            if(infoCI.containsKey('Status')){
                op = true;
                return pendingClarification(System_ID, Operation_Number, Attributes, Attachment_Name, Attachment_Data);
            }
            if(infoCI.containsKey('Work_Info_Notes')){
                op = true;
                return createEvolutionNote(System_ID, Operation_Number, Attributes, Attachment_Name, Attachment_Data);
            }
            if(!op){
                result = new OutputMapping1 ();
                result.Operation_Status=Operation_StatusType.Cancelled;
                result.Error_Code = '11420';
                result.Error_Msg = 'Error(s) while checking the mandatory fields'; 
                return result;
            }
        }
        catch(Exception e){
            /*if(e.getMessage().contains('Read timed out')){
                System.debug('Error :'+e.getMessage());
                result = Create(System_ID, Operation_Number, Attributes, Attachment_Name, Attachment_Data);
            }
            else{*/
                result.Operation_Status=Operation_StatusType.Cancelled;
                result.Error_Code = '11015';
                result.Error_Msg = 'An unexpected error has occurred while parsing the information: '+e.getMessage();
                //DataBase.rollback(savePointCheckout);
                System.debug('Error :'+e.getMessage()+' '+e.getLineNumber());
            //}
        }
        return result;
    }
    
     /*------------------------------------------------------------
    Author:         Marta Laliena
    Company:        Deloitte
    Description:    
    History
    <Date>          <Author>        <Change Description>
    22-Jul-2015     Marta Laliena     Initial Version
    ------------------------------------------------------------*/
    private static OutputMapping1 pendingClarification (String System_ID, String Operation_Number, String Attributes, String Attachment_Name,Blob Attachment_Data){
        OutputMapping1 result = new OutputMapping1 ();
        result.Operation_Status = Operation_StatusType.processed;
        try{
            String incidentNum = infoCI.get('Incident_Number');
            /* Get Case */
            List<Case> cases = [SELECT Id, CaseNumber, Status, TGS_Status_Reason__c FROM Case WHERE CaseNumber =:incidentNum];
            if(cases.size()==0){
                throw new IntegrationException('Incident is not created');
            }
            /* Update Case status */
            Case caseN = cases[0];
            caseN.Status = infoCI.get('Status');
            caseN.TGS_Status_Reason__c = Constants.CASE_STATUS_REASON_IN_PROVISION;
            update caseN;
            
            result.Operation_Instance_ID = caseN.CaseNumber;
            result.Error_Code = '0';
            
        }catch(Exception e){
            /*if(e.getMessage().contains('Read timed out')){
                System.debug('Error :'+e.getMessage());
                return Create(System_ID, Operation_Number, Attributes, Attachment_Name, Attachment_Data);
            }
            else{*/
                result.Operation_Status=Operation_StatusType.Cancelled;
                result.Error_Code = '11015';
                result.Error_Msg = 'An unexpected error has occurred while parsing the information: '+e.getMessage();
                //DataBase.rollback(savePointCheckout);
                System.debug('Error :'+e.getMessage()+' '+e.getLineNumber());
            //}
        }
        return result;
    }
    
    /*------------------------------------------------------------
    Author:         Marta Laliena
    Company:        Deloitte
    Description:    When a workinfo is created in SF
    History
    <Date>          <Author>        <Change Description>
    22-Jul-2015     Marta Laliena     Initial Version
    ------------------------------------------------------------*/
    private static OutputMapping1 createEvolutionNote (String System_ID, String Operation_Number, String Attributes, String Attachment_Name, Blob Attachment_Data){
        OutputMapping1 result = new OutputMapping1 ();
        result.Operation_Status = Operation_StatusType.processed;
        try{
            String incidentNum = infoCI.get('Incident_Number');
            /* Get Case */
            List<Case> cases = [SELECT Id, CaseNumber, Status, TGS_Status_Reason__c FROM Case WHERE CaseNumber =:incidentNum];
            if(cases.size()==0){
                throw new IntegrationException('Incident is not created');
            }
            /* Update Case status */
            Case caseN = cases[0];
            
            TGS_Work_Info__c workinfo = new TGS_Work_Info__c();
            workinfo.TGS_Case__c = caseN.Id;
            workinfo.TGS_Public__c = false;
            workinfo.TGS_Description__c = infoCI.get('Work_Info_Notes');
            insert workinfo;
            if (Attachment_Data!=null && Attachment_Data.size()!=0){
                Attachment attach = new Attachment();
                attach.Name = Attachment_Name;
                attach.Body = Attachment_Data;
                attach.ParentId = workinfo.Id;
                insert attach;
            }  
            result.Operation_Instance_ID = caseN.CaseNumber;
            result.Error_Code = '0';
            
        }catch(Exception e){
            /*if(e.getMessage().contains('Read timed out')){
                System.debug('Error :'+e.getMessage());
                return Create(System_ID, Operation_Number, Attributes, Attachment_Name, Attachment_Data);
            }
            else{*/
                result.Operation_Status=Operation_StatusType.Cancelled;
                result.Error_Code = '11015';
                result.Error_Msg = 'An unexpected error has occurred while parsing the information: '+e.getMessage();
                //DataBase.rollback(savePointCheckout);
                System.debug('Error :'+e.getMessage()+' '+e.getLineNumber());
            //}
        }    
        return result;
    }
    
     /*------------------------------------------------------------
    Author:         Ana Cirac
    Company:        Deloitte
    Description:    Parses Attributes into a map to store
                    (AttributeName, AttributeValue)
    History
    <Date>          <Author>        <Change Description>
    01-Jun-2015     Ana Cirac     Initial Version
    ------------------------------------------------------------*/
    static Map<String, String> getInfoCI(String Attributes){
        Map<String, String> attrs = new Map<String, String>();
        List<String> listAttributes =  Attributes.split('";');
        System.debug('attributes: '+listAttributes); 
        for(String attribute: listAttributes){
            List<String> att = attribute.split('=');
            attrs.put(att[0],att[1].substring(1));
        }
        return attrs;
    }
    
    
}