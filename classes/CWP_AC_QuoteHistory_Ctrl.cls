/*---------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Pardo
Company:       Accenture
Description:   Controller for the component CWP_AC_QuoteHistory to retrieve Order data
Test class :   CWP_AC_QuoteHistoryCTRL_TEST
History
<Date>      <Author>     	<Description>
26-01-2018	Antonio Pardo	v1.0
------------------------------------------------------------------------------------------------------------------------------*/
public without sharing class CWP_AC_QuoteHistory_Ctrl {
   	/*---------------------------------------------------------------------------------------------------------------------------
	Author:        Antonio Pardo
    Company:       Accenture
    Description:   Retrieves a data structure for the lightning table containing quote information, for the given user and account
	IN:			   	String user - User Id
					String acc - Account Id
					Object recordsPerPage - Number that indicates the number of records to be displayed in the table
    OUT:			Map<String, List<Object>> - Map returning the data, with header info, and a list of pages with all the information
												needed to populate the table
	History
	<Date>      <Author>     	<Description>
	26-01-2018	Antonio Pardo	v1.0
	------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static Map<String, List<Object>> getQuotes(String user, String acc, Object recordsPerPage){
        
        
       	Integer numMaximo = Integer.valueOf(recordsPerPage);
        
        System.debug('useracc' + user + '-' + acc);
        List<sObject> lst_order = [SELECT Id, Name, BI_O4_Total_price_MRC__c, BI_O4_Total_price_NRC__c, format(BI_O4_Total_price_MRC__c) formatMRC, format(BI_O4_Total_price_NRC__c) formatNRC,NE__Recurring_Charge_Total__c, NE__One_Time_Fee_Total__c, CWP_AC_Cliente_Final__c,NE__OrderStatus__c,CurrencyIsoCode,LastModifiedDate, (select Id, Bit2WinHUB__Status__c from Bit2WinHUB__Bulk_Configuration_Requests__r) FROM NE__Order__c WHERE RecordType.DeveloperName = 'Quote' AND NE__AccountId__c = :acc AND OwnerId = :user Order by Name DESC];
        if(!lst_order.isEmpty()){
            
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType leadSchema = schemaMap.get('NE__Order__c');
            Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
            
            //List with the label names translated
            List<String> lst_labels = new List<String>();
           	Map<String, String> map_api_type = new Map<String, String>();
            List<String> fieldNameAux = new List<String>{'Name', 'estado','CWP_AC_Cliente_Final__c','BI_O4_Total_price_NRC__c','BI_O4_Total_price_MRC__c', 'LastModifiedDate'};
                for (String fieldName: fieldNameAux) {
                    if(fieldName == 'BI_O4_Total_price_MRC__c'){
                        lst_labels.add('Total MRC');
                    }else if(fieldName == 'BI_O4_Total_price_NRC__c'){
                        lst_labels.add('Total NRC');
                    }else if(fieldName == 'estado'){
                        lst_labels.add(Label.CWP_AC_Status);
                    }else{  
                        lst_labels.add(fieldMap.get(fieldName).getDescribe().getLabel());
                    }
                    if(fieldName != 'estado' && String.valueOf(fieldMap.get(fieldName).getDescribe().getType())=='CURRENCY' && (fieldName=='BI_O4_Total_price_NRC__c' || fieldName=='BI_O4_Total_price_MRC__c')){
                        map_api_type.put(fieldName, 'STRING');
                    } else {
                        if(fieldName == 'estado'){
                            map_api_type.put(fieldName, 'STRING');
                        }else{
                            map_api_type.put(fieldName, String.valueOf(fieldMap.get(fieldName).getDescribe().getType()));
                        }
                        
                    }
                    
                }
            
            Map<String, List<Object>> map_return = new Map<String, List<Object>>();
            map_return.put('headers', lst_labels);
            System.debug(numMaximo);
            System.debug(map_api_type);
            map_return.put('quotes', valuesForRows(lst_order, map_api_type, numMaximo));
            return map_return;
        }else{
            return null;
        }
    }
    /*---------------------------------------------------------------------------------------------------------------------------
	Author:        Antonio Pardo
    Company:       Accenture
    Description:   Retrieves a data structure for the lightning table containing quote information, values of the fields and the type of the fields
	IN:			   	List<Sobject> lst_order - list of orders retrieved by query
					Map<String, String> - map containing the type of field and the api name of the field
					Integer numMaximo - number of the records to be displayed on the table
	OUT:			List<List<Object>> - List of rows with size numMaximo, containing a list of object, with value and type that indicates
										The component wich type of field should be displayed
	History
	<Date>      <Author>     	<Description>
	26-01-2018	Antonio Pardo	v1.0
	------------------------------------------------------------------------------------------------------------------------------*/
    private static List<List<Object>> valuesForRows(List<NE__Order__c> lst_order, Map<String, String> map_api_type,Integer numMaximo){
		List<List<List<Object>>> lst_pages = new List<List<List<Object>>>();
        Integer i = 1;
        List<List<Object>> lst_values = new List<List<Object>>();
        
        Integer j = 0;
        for(Sobject order : lst_order){
			
            List<Object> lstAux = new List<Object>();
            for(String apiName : map_api_type.keySet()){
                Map<String, Object> map_status = new Map<String, Object>();
                if(apiName == 'estado'){
                    
                    System.debug('entrostatus ' + j++);
                    String status = Label.CWP_AC_StatusCompleted;
                    for(Bit2WinHUB__Bulk_Configuration_Request__c blkConfig : order.getsObjects('Bit2WinHUB__Bulk_Configuration_Requests__r')){
                        System.debug(blkConfig);
                        if(blkConfig.Bit2WinHUB__Status__c != 'Completed'){
                            status = Label.CWP_AC_StatusWorking;
                            break;
                        }
            		}
                    map_status.put('type', 'STRING');
                    map_status.put('value', status);
                    System.debug(map_status);
                    lstAux.add(map_status);
                }else if(apiName == 'Name'){
                    Map<String, Object> map_nameId = new Map<String, Object>();
                    map_nameId.put('id', order.get('Id'));
                    map_nameId.put('name', order.get('Name'));
                    lstAux.add(map_nameId);
                }else{
                    Map<String, Object> map_valueType = new Map<String, Object>();
                    
                    map_valueType.put('type', order.get(apiName) == 0 ? 'STRING' : map_api_type.get(apiName));
                    if(apiName == 'BI_O4_Total_price_MRC__c'){
                        map_valueType.put('value', order.get(apiName)==null ? '' : (order.get(apiName) == 0 ? Label.CWP_AC_Calculating : order.get('formatMRC')));
                        System.debug(lstAux[3]);
                        if((Double)order.get(apiName) == 0){
                            Map<String, Object> mAux = (Map<String, Object>)lstAux[3];
                            mAux.put('value', Label.CWP_AC_Calculating);
                        }
                    }else if(apiName == 'BI_O4_Total_price_NRC__c'){
                    	map_valueType.put('value', order.get(apiName)==null ? '' : order.get('formatNRC'));
  					}else{
                        map_valueType.put('value', order.get(apiName)==null ? '' : order.get(apiName));
                    }
                    if(map_api_type.get(apiName) == 'CURRENCY'){
                        map_valueType.put('isoCode', order.get('CurrencyIsoCode'));
                    }
                    lstAux.add(map_valueType);
                }
            }
            
            lst_values.add(lstAux);
            if(lst_values.size()==numMaximo||i==lst_order.size()){
                lst_pages.add(lst_values);
                lst_values = new List<List<Object>>();
            }
            i++;
        }
        return lst_pages;
    }
    /*---------------------------------------------------------------------------------------------------------------------------
	Author:        Antonio Pardo
    Company:       Accenture
    Description:   Retrieves the Name of the given Order
	IN:			   	String recordId - Id of the quote
	OUT:			String - Name of the quote
	History
	<Date>      <Author>     	<Description>
	26-01-2018	Antonio Pardo	v1.0
	------------------------------------------------------------------------------------------------------------------------------*/
    @auraEnabled
    public static String getQuote(String recordId){
        return [SELECT Name FROM NE__Order__c WHERE Id = :recordId LIMIT 1].Name;
        
    }
    

}