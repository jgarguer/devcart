/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis 
Company:       Everis
Description:   Test Methods executed to comprobe failure coverage 
Test Class:    CWP_Helper.cls


History:

<Date>                  <Author>                <Change Description>
28/02/2016              Everis                  Initial Version
28/03/2017              Alberto Pina            Coverage 88%
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class CWP_Helper_TEST { 
    
    
    @testSetup 
    private static void dataModelSetup() {               
        
        //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{
            'Account', 
                'Case',
                'NE__Order__c',
                'NE__OrderItem__c'
                };
                    
                    //ACCOUNT
                    map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            //rtMapBydevName.put(i.DeveloperName, i.id);
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;
        Account accCustomerCountry;
        Account accLegalEntity;
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;
        }          
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        insert contactTest;
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            User usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        
        //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 't1';
        producto.TGS_CWP_Tier_2__c = 't2';
        insert producto;
        list<NE__Catalog_Item__c> ciList = new list<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
        system.debug('Catalog item creado: '+catalogoItem);
        ciList.add(catalogoItem);
        
        NE__Catalog_Item__c catalogoItem1 = CWP_TestDataFactory.createCatalogoItem('ProductoTB', catalogCategoryChildren.id, catalogo.id, producto.id);
        catalogoItem1.NE__Technical_Behaviour__c = 'Service with pre-approved changes';
        ciList.add(catalogoItem1);
        insert ciList;
        
        NE__Contract_Header__c newCH = new NE__Contract_Header__c(
            NE__name__c = 'theContract'
        );
        insert newCH;
        
        NE__Contract_Account_Association__c newCAA = new NE__Contract_Account_Association__c(
            NE__Contract_Header__c = newCH.id,
            NE__Account__c = accLegalEntity.id
        );
        insert newCAA;
        
        NE__Contract__c newC =new  NE__Contract__c(
            NE__Contract_Header__c = newCH.id
        );
        insert newC;
        
        NE__Contract_Line_Item__c newCII = new NE__Contract_Line_Item__c(
            NE__Commercial_Product__c = producto.id,
            NE__Contract__c = newC.id
        );
        insert newCII;
        
        //ORDER
        
        //NE__Asset__c testAsset = CWP_TestDataFactory.createAsset();
        //insert testAsset;
        //Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
        //NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId, NE__Type__c = 'Asset');        
        
        NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        insert testOrder;
        //createOrderItem(id accountId, id orderId, id productId, id catalogItem, integer quantity){
        NE__OrderItem__c newOI;
        list<NE__OrderItem__c> oiList = new list<NE__OrderItem__c>();
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
        oiList.add(newOI);
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[1].id, 1);
        newOI.NE__City__c = 'Gondor';
        newOI.NE__city__c = 'Middle-earth';
        oiList.add(newOI);
        insert oiList;
        
        list<NE__OrderItem__c> oiChildrenList = new list<NE__OrderItem__c>();
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
        newOI.NE__Parent_Order_Item__c = oiList[0].id;
        oiChildrenList.add(newOI);
        
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[0].id, 1);
        newOI.NE__Parent_Order_Item__c = oiList[0].id;
        oiChildrenList.add(newOI);
        
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[1].id, 1);
        newOI.NE__Parent_Order_Item__c = oiList[1].id;
        oiChildrenList.add(newOI);
        
        newOI = CWP_TestDataFactory.createOrderItem(unUsuario.AccountId, testOrder.id, producto.id, ciList[1].id, 1);
        newOI.NE__Parent_Order_Item__c = oiList[1].id;
        oiChildrenList.add(newOI);
        
        insert oiChildrenList;

        //CASO     
        list <Case> listaDeCasos = new list <Case>();                
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.TGS_Product_Tier_1__c = 't1a';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.contactId = contactTest.id;        
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1.1', 'Assigned', '');
        newCase.TGS_Product_Tier_1__c = 't1a';
        newCase.TGS_Product_Tier_2__c = 't2a';
        newCase.TGS_Product_Tier_3__c = 't3a';
        newCase.contactId = contactTest.id;        
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Billing_Inquiry'), 'sub2', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1';
        newCase.TGS_Product_Tier_2__c = 't2a';
        newCase.TGS_Product_Tier_3__c = 't3aa';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Change'), 'sub3', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1b';
        newCase.TGS_Product_Tier_2__c = 't2b';
        newCase.TGS_Product_Tier_3__c = 't3b';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Complaint'), 'sub4', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1b';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Problem'), 'sub5', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1b';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Query'), 'sub6', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1b';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[0].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub7', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1b';
        newCase.TGS_Product_Tier_2__c = 't2';
        newCase.TGS_Product_Tier_3__c = 't3';
        newCase.TGS_Customer_Services__c = oiList[1].id;
        newCase.contactId = contactTest.id;                
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub8', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1a';
        newCase.TGS_Product_Tier_2__c = 't2a';
        newCase.TGS_Product_Tier_3__c = 't3a';
        newCase.TGS_Customer_Services__c = oiList[1].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub9', 'Assigned', 'Solicitud de alta');
        newCase.TGS_Product_Tier_1__c = 't1a';
        newCase.TGS_Product_Tier_2__c = 't2a';
        newCase.TGS_Product_Tier_3__c = 't3aa';
        newCase.TGS_Customer_Services__c = oiList[1].id;
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        
        insert listaDeCasos;        
        
        NE__Family__c family = CWP_TestDataFactory.createFamily('corleone');
        insert family;
        
        NE__DynamicPropertyDefinition__c dynProp = CWP_TestDataFactory.createDynamiyProperty('dynamic');
        insert dynProp;
        
        NE__ProductFamilyProperty__c famProp = CWP_TestDataFactory.createFamilyProperty(family.id, dynProp.id); 
        famProp.CWP_KeyValue__c = true;
        insert famProp;
        
        list<NE__Order_Item_Attribute__c> oiaList = new list<NE__Order_Item_Attribute__c>();
        NE__Order_Item_Attribute__c oia1 = CWP_TestDataFactory.createOrderItemAttribute(oiList[0].id, famProp.id);
        oiaList.add(oia1);
        
        oia1 = CWP_TestDataFactory.createOrderItemAttribute(oiList[0].id, famProp.id);
        oiaList.add(oia1);
        
        oia1 = CWP_TestDataFactory.createOrderItemAttribute(oiList[0].id, famProp.id);
        oiaList.add(oia1);
        
        oia1 = CWP_TestDataFactory.createOrderItemAttribute(oiChildrenList[0].id, famProp.id);
        oiaList.add(oia1);
        
        oia1 = CWP_TestDataFactory.createOrderItemAttribute(oiChildrenList[0].id, famProp.id);
        oiaList.add(oia1);
        
        insert oiaList;
         
    }
    
    @isTest
    private static void createHelperTest(){
        
        Test.startTest();
        list<NE__OrderItem__c> oiList = new list<NE__OrderItem__c>([SELECT
                                                                    id,
                                                                    TGS_Service_status__c,
                                                                    NE__Status__c,
                                                                    NE__CatalogItem__r.NE__ProductId__r.Name,
                                                                    NE__CatalogItem__r.NE__Catalog_Category_Name__c,
                                                                    NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name,
                                                                    NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__c,
                                                                    NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name,            
                                                                    NE__CatalogItem__r.NE__Technical_Behaviour__c,
                                                                    NE__ProdId__r.Name,
                                                                    Name, 
                                                                    NE__OrderId__r.Case__r.TGS_RFS_date__c,
                                                                    NE__OrderId__r.Case__r.TGS_RFB_date__c,
                                                                    TGS_Billing_end_date__c,
                                                                    Installation_point__c,
                                                                    NE__Catalog__c, 
                                                                    NE__Account__c,
                                                                    NE__CatalogItem__c
                                                                    FROM NE__OrderItem__c]);                      
        CWP_Helper controller  = new CWP_Helper();
        controller  = new CWP_Helper(oiList);                                            
        Test.stopTest();
        
    }
    
    @isTest
    private static void clearFunctionTest() {       
        User usuario = [SELECT id, AccountId FROM User WHERE FirstName =: 'nombre1'];             
        Account currentAccount = [SELECT Id, Name FROM Account WHERE Id =: usuario.AccountId];
        System.runAs(usuario){            
            
            Test.startTest(); 
            CWP_Cases_Controller controller  = new CWP_Cases_Controller();    
            //getFilterFromCatalogItem method
            controller.caseDetail = new CWP_Helper(controller.caseIdForDetail);
            Test.stopTest();
        }
    }
    
    @isTest
    private static void contructorTest() {       
        
        User usuario = [SELECT id, AccountId FROM User WHERE FirstName =: 'nombre1'];             
        Account currentAccount = [SELECT Id, Name FROM Account WHERE Id =: usuario.AccountId];
        System.runAs(usuario){            
            
            Test.startTest();   
            //CWP_Helper constructor
            list <Case> listaDeCasos = [SELECT Id, Status, TGS_Product_Tier_1__c, TGS_Product_Tier_2__c, TGS_Product_Tier_3__c, TGS_customer_Services__c, TGS_customer_Services__r.Name, TGS_customer_Services__r.NE__CatalogItem__c FROM Case WHERE TGS_Product_Tier_1__c = 't1'];    
            CWP_Helper aux = new CWP_Helper(listaDeCasos);
            Test.stopTest();
        }
    }
    
    @isTest
    private static void shortMethodsTest() {       
        
        User usuario = [SELECT id, AccountId FROM User WHERE FirstName =: 'nombre1'];             
        Account currentAccount = [SELECT Id, Name FROM Account WHERE Id =: usuario.AccountId];
        System.runAs(usuario){            
            
            Test.startTest();   
            //CWP_Helper constructor
            list <Case> listaDeCasos = [SELECT Id, Status, TGS_Product_Tier_1__c, TGS_Product_Tier_2__c, TGS_Product_Tier_3__c, TGS_customer_Services__c, TGS_customer_Services__r.Name, TGS_customer_Services__r.NE__CatalogItem__c FROM Case WHERE TGS_Product_Tier_1__c = 't1'];    
            CWP_Helper aux = new CWP_Helper(listaDeCasos);
            map<id, Case> mapa = aux.getAllAccountCase(usuario.id);
            aux.refreshPicklist('CCLIST', 'sel0', 'sel1', 'sel2');
            aux.refreshPicklist('CILIST', 'sel0', 'sel1', 'sel2');
            aux.refreshPicklist('OILIST', 'sel0', 'sel1', 'sel2');
            Test.stopTest();
        }
    }
    
    @isTest
    private static void getCaseDetailTest() {     
        User usuario = [SELECT id, AccountId FROM User WHERE FirstName =: 'nombre1'];             
        Account currentAccount = [SELECT Id, Name FROM Account WHERE Id =: usuario.AccountId];
        System.runAs(usuario){            
            
            Test.startTest();   
            list <Case> listaDeCasos = [SELECT Id, Status, TGS_Product_Tier_1__c, TGS_Product_Tier_2__c, TGS_Product_Tier_3__c, TGS_customer_Services__c, TGS_customer_Services__r.Name, TGS_customer_Services__r.NE__CatalogItem__c FROM Case WHERE TGS_Product_Tier_1__c = 't1'];    
            CWP_Helper aux = new CWP_Helper(listaDeCasos);
            Case casoAux=aux.getCaseDetail(listaDeCasos.get(0).Id);
            Test.stopTest();
        }
    }
    
    @isTest
    private static void creteClassTest() {     
        User usuario = [SELECT id, AccountId FROM User WHERE FirstName =: 'nombre1'];             
        Account currentAccount = [SELECT Id, Name FROM Account WHERE Id =: usuario.AccountId];
        System.runAs(usuario){            
            
            Test.startTest();   
            list <Case> listaDeCasos = [SELECT Id, Status, TGS_Product_Tier_1__c, TGS_Product_Tier_2__c, TGS_Product_Tier_3__c, TGS_customer_Services__c, TGS_customer_Services__r.Name, TGS_customer_Services__r.NE__CatalogItem__c FROM Case WHERE TGS_Product_Tier_1__c = 't1'];    
            CWP_Helper aux = new CWP_Helper(listaDeCasos);
            CWP_Helper.level1wrap level1 = new CWP_Helper.level1wrap('level1');
            CWP_Helper.level2wrap level2 = new CWP_Helper.level2wrap('level2');
            CWP_Helper.level3wrap level3 = new CWP_Helper.level3wrap('label1','value1','label2','value2');
            CWP_Helper.level3wrap level31 = new CWP_Helper.level3wrap(1,'value1','label2');
            CWP_Helper.level3wrap level32 = new CWP_Helper.level3wrap(2,'value1','label2');
            
            level2.addNewLevel3(level3);
            list<CWP_Helper.level3wrap> newElemList = new list<CWP_Helper.level3wrap>();
            newElemList.add(level3);
            level2.addNewLevel3List(newElemList);
            level1.addNewLevel2(level2);
            Test.stopTest();
        }
    }
    @isTest
    private static void getModificationProgressTest() {     
        User usuario = [SELECT id, AccountId FROM User WHERE FirstName =: 'nombre1'];             
        Account currentAccount = [SELECT Id, Name FROM Account WHERE Id =: usuario.AccountId];
        System.runAs(usuario){            
            Test.startTest();   
            list<NE__OrderItem__c> oiList = new list<NE__OrderItem__c>([SELECT
                                                                    id,
                                                                    TGS_Service_status__c,
                                                                    NE__Status__c,
                                                                    NE__CatalogItem__r.NE__ProductId__r.Name,
                                                                    NE__CatalogItem__r.NE__Catalog_Category_Name__c,
                                                                    NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name,
                                                                    NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__c,
                                                                    NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name,            
                                                                    NE__CatalogItem__r.NE__Technical_Behaviour__c,
                                                                    NE__ProdId__r.Name,
                                                                    Name, 
                                                                    NE__OrderId__r.Case__r.TGS_RFS_date__c,
                                                                    NE__OrderId__r.Case__r.TGS_RFB_date__c,
                                                                    TGS_Billing_end_date__c,
                                                                    Installation_point__c,
                                                                    NE__Catalog__c, 
                                                                    NE__Account__c,
                                                                    NE__CatalogItem__c
                                                                    FROM NE__OrderItem__c]);    
            
            list<NE__Catalog_Item__c> oiListCatalog = new list<NE__Catalog_Item__c>([SELECT Id, NE__Catalog_Category_Name__c, NE__Technical_Behaviour__c, NE__Catalog_Category_Name__r.NE__Parent_Category_Name__c, NE__ProductId__c, NE__ProductId__r.Name, Name
                                                                    FROM NE__Catalog_Item__c]); 
            
            map<id, NE__OrderItem__c> oiMap = new map<Id, NE__OrderItem__c>([SELECT
            id, TGS_Service_status__c, NE__Status__c, NE__CatalogItem__r.NE__ProductId__r.Name, NE__CatalogItem__r.NE__Catalog_Category_Name__c,
            NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name, NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__c,
            NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name, NE__CatalogItem__r.NE__Technical_Behaviour__c,
            NE__ProdId__r.Name, Name, NE__OrderId__r.Case__r.TGS_RFS_date__c, NE__OrderId__r.Case__r.TGS_RFB_date__c, TGS_Billing_end_date__c,
            Installation_point__c, NE__Catalog__c, NE__Account__c, NE__CatalogItem__c
            FROM NE__OrderItem__c]);
            
            CWP_Helper controller  = new CWP_Helper();
            controller  = new CWP_Helper(oiList); 
            set<id> accSet = new set<id>();
            map<id, NE__OrderItem__c> rcvMap=new map<id, NE__OrderItem__c>();
            for (NE__OrderItem__c x: oiList){
                accSet.add(x.Id);
                rcvMap.put(x.Id, x);
            }
            
            controller.getOiMapFromAccount(accSet);
            controller.getOiMap(accSet);
            controller.setOIMap(rcvMap);
            set<id> ciIdSet= new set<id>();
            for (NE__Catalog_Item__c x: oiListCatalog){
                ciIdSet.add(x.Id);
            }
            
            map<id, NE__Order__c> rcvMapOrder = new map<id, NE__Order__c>();
            list<NE__Order__c> oList = new list<NE__Order__c>([SELECT   id,
                                                                        RecordTypeId,
                                                                        NE__CatalogId__c,
                                                                        NE__Asset__c,
                                                                        NE__Configuration_Type__c,
                                                                        NE__AccountId__c,
                                                               NE__ServAccId__c,
                                                               NE__BillAccId__c
                                                                        FROM NE__Order__c]);
            for (NE__Order__c x: oList){
                rcvMapOrder.put(x.Id, x);
            }
            controller.setOMap(rcvMapOrder);
            
            map<id, NE__Catalog_Category__c> rcvMapCategory = new map<id, NE__Catalog_Category__c>();
            list<NE__Catalog_Category__c> oListCategory = new list<NE__Catalog_Category__c>([SELECT   id,
                                                                        Name,
                                                                        NE__CatalogId__c,
                                                                        NE__Parent_Category_Name__c
                                                                        FROM NE__Catalog_Category__c]);
            for (NE__Catalog_Category__c x: oListCategory){
                rcvMapCategory.put(x.Id, x);
            }
            controller.setCCCMap(rcvMapCategory);
            controller.setCCPMap(rcvMapCategory);
                
                
            map<id, NE__Catalog_Item__c> mapCatalog= controller.getCiMap(ciIdSet);
            controller.getSelectedId(oiList.get(0).Id);
            
            controller.getServiceDetail('<ALL>');
            String aux = (String)oiList.get(0).Id;
            controller.getServiceDetail(aux);
            
            //AddComent
            list <Case> listaDeCasos = [SELECT Id, Status, TGS_Product_Tier_1__c, TGS_Product_Tier_2__c, TGS_Product_Tier_3__c, TGS_customer_Services__c, TGS_customer_Services__r.Name, TGS_customer_Services__r.NE__CatalogItem__c FROM Case WHERE TGS_Product_Tier_1__c = 't1'];    
            CWP_Helper controller2  = new CWP_Helper(listaDeCasos.get(0).Id);
            controller2.addComment('Comentario');
            
            Test.stopTest();
        }
    }
    @isTest
    private static void creteClassTest2() {    
        User usuario = [SELECT id, AccountId FROM User WHERE FirstName =: 'nombre1'];             
        Account currentAccount = [SELECT Id, Name FROM Account WHERE Id =: usuario.AccountId];
        System.runAs(usuario){   
            List<String> listaAttach = new List<String>();
            BI_ImageHome__c imagenAttach = new BI_ImageHome__c();
            CWP_Helper.imageHomeWrapper image= new CWP_Helper.imageHomeWrapper(imagenAttach, listaAttach, 'www.url.es');
        }
    }
    
     @isTest
    private static void creteClassTest3() {    
        User usuario = [SELECT id, AccountId FROM User WHERE FirstName =: 'nombre1'];             
        Account currentAccount = [SELECT Id, Name FROM Account WHERE Id =: usuario.AccountId];
        System.runAs(usuario){   
            DateTime myDateTime = DateTime.newInstance(2018, 1, 31, 7, 8, 16);
            CWP_Helper.commentWrap obj= new CWP_Helper.commentWrap('content', myDateTime, 'createdByName');
            DateTime myDateTime2 = DateTime.newInstance(2019, 1, 31, 7, 8, 16);
            CWP_Helper.commentWrap obj2= new CWP_Helper.commentWrap('content', myDateTime2, 'createdByName');
            Integer entero = obj.compareTo((Object) obj2);
        }
    }
}