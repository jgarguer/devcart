/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-27      Daniel ALexander Lopez (DL)     Create Class
* @version   1.1    2015-07-30		Manuel Medina (MM)     			Complemento para la cobertura del trigger BI_COL_PreFactibilidad_tgr      
             20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private class BI_COL_SendingEmailPrefeasibility_tst
{
	public static BI_COL_PreFactibilidad__c objPrefactibilidad;
	public list<BI_COL_MatrizP__c> lstConfMatrizP;
	public static Account  objCuenta;
	public static Opportunity objOpp;

	static void crearData()
	{
		BI_bypass__c objBibypass = new BI_bypass__c();
		objBibypass.BI_migration__c=true;
		insert objBibypass;
		
		objCuenta 										= new Account();
        objCuenta.Name 									= 'prueba';
        objCuenta.BI_Country__c 						= 'Colombia';
        objCuenta.TGS_Region__c 						= 'América';
        objCuenta.BI_Tipo_de_identificador_fiscal__c 	= 'NIT';
        objCuenta.CurrencyIsoCode 						= 'GTQ';
        objCuenta.BI_Segment__c                         = 'test';
        objCuenta.BI_Subsegment_Regional__c             = 'test';
        objCuenta.BI_Territory__c                       = 'test';
        insert objCuenta;

        objOpp 											= new Opportunity();
        objOpp.Name 									= 'prueba opp';
        objOpp.AccountId 	 							= objCuenta.Id;
        objOpp.BI_Country__c 							= objCuenta.BI_Country__c;
        objOpp.CloseDate 						 		= System.today().addDays(+5);
        objOpp.StageName 								= 'F5 - Solution Definition';
        objOpp.CurrencyIsoCode							= 'GTQ';
        objOpp.Certa_SCP__contract_duration_months__c 	= 12;
        objOpp.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
        //objOpp.BI_COL_Autoconsumo__c 					= true;
        insert objOpp;

        objPrefactibilidad									= new BI_COL_PreFactibilidad__c();
        objPrefactibilidad.BI_COL_Proyecto__c				= objOpp.Id;
        objPrefactibilidad.BI_COL_Descripcion_negocio__c	= 'PruebaTest';
        objPrefactibilidad.BI_COL_Fecha_solicitud__c		= Date.newInstance(2015, 07, 30);
        objPrefactibilidad.BI_COL_Probabilidad_cierre__c	='50%';
        objPrefactibilidad.BI_COL_Monto_contrato_12_meses__c ='Menos de $300 M';
        objPrefactibilidad.BI_COL_Estado_oferta__c			='Elaboración';
        insert objPrefactibilidad;	   
	}

	static testmethod void myTestMethod()
	{

		crearData();
		Test.startTest();
			BI_COL_SendingEmailPrefeasibility_cls.envioMail_mtd(objPrefactibilidad);
			
			objPrefactibilidad.BI_COL_Enviar_email__c		= false;
			objPrefactibilidad.BI_COL_Enviar_email_cial__c	= false;
			update objPrefactibilidad;
        Test.stopTest();

	}
	static testmethod void myTestMethod2()
	{

		crearData();
		objPrefactibilidad.BI_COL_Enviar_email__c=true;
		update objPrefactibilidad;
		Test.startTest();
			BI_COL_SendingEmailPrefeasibility_cls.envioMail_mtd(objPrefactibilidad);
			BI_COL_SendingEmailPrefeasibility_cls.processAprobal(objPrefactibilidad);
			
			
        Test.stopTest();

	}
	
	static testmethod void myTestMethod3()
	{

		crearData();
		objPrefactibilidad.BI_COL_Enviar_email__c=true;
		objPrefactibilidad.BI_COL_Destinatarios_email__c='alguien@ttdata.com;alguienmas@ttdata.com';
		update objPrefactibilidad;
		Test.startTest();
			BI_COL_SendingEmailPrefeasibility_cls.envioMail_mtd(objPrefactibilidad);
			BI_COL_SendingEmailPrefeasibility_cls.processAprobal(objPrefactibilidad);
			
			objPrefactibilidad.BI_COL_Destinatarios_email__c='alguien@ttdata.com';
			update objPrefactibilidad;
			BI_COL_SendingEmailPrefeasibility_cls.envioMail_mtd(objPrefactibilidad);
        Test.stopTest();

	}
}