/**
* Avanxo Colombia
* @author           Daniel Alexander lopez Sotelo href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    15/05/2015      Daniel Alexander lopez                  Cloned Controller
*************************************************************************************************************/
public with sharing class BI_COL_cup_Selectds_ctr {
    
    /** Parametros VF */
  public boolean mostrarRegresar {get;set;}
  public List<WrapperMS> lstDS {get;set;}
  public boolean todosMarcados {get;set;}
  public String idOpt {get;set;}
  
  /** Paramétros del paginador */
  private List<WrapperMS> pageMS;
  @TestVisible private Integer pageNumber;
  @TestVisible private Integer pageSize;
  @TestVisible private Integer totalPageNumber;
  private Set<String> noCancela = new Set<String>{'Cancelado', 'Ejecutado'};
  
  //Constructor Standard Controller
  public BI_COL_cup_Selectds_ctr()
  {
    idOpt = ApexPages.currentPage().getParameters().get('idOpt');
    String etapaOpt = ApexPages.currentPage().getParameters().get('etapaOpt');
    String NameOpt = ApexPages.currentPage().getParameters().get('Name');
    User usu=[select id,BI_Permisos__c, Name, Profile.Name from user where id =:userinfo.getUserId() limit 1];
    //set<String> permiSet = BI_COL_cup_Selectds_ctr.getPermisionSetUsuario(usu.id);
    System.debug('----> idOpt: ' + idOpt);
    System.debug('----> etapaOpt: ' + etapaOpt);
    System.debug('----> NameOpt: ' + NameOpt);
    
    Boolean mostrar=false;
    if(usu.Profile.Name=='Admon Ventas Master')
    {
      mostrar=true;
    }
    
    //if(etapaOpt!=null && mostrar)
    if(etapaOpt!=null)
    {
      //Estados Inactiva,Activa,Pendiente,Enviado,Cerrada perdida,Suspensión voluntaria
      mostrarRegresar = true;
      List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta = new List<BI_COL_Modificacion_de_Servicio__c>();
      List<String> estados = new List<String>();
      
      boolean BolauxMen = false;
      set<String> permiSet=new set<String>{'Ingeniería/Preventa','Jefe de Ventas','Administrador del sistema','Ejecutivo de Cobros'};
      System.debug('\n\n usu.BI_Permisos__c: '+ usu.BI_Permisos__c+' permiSet.contains(usu.BI_Permisos__c) ='+permiSet.contains(usu.BI_Permisos__c)+'\n\n');
      if(permiSet.contains(usu.BI_Permisos__c))
      //if(etapaOpt=='F1 - Closed Won' || etapaOpt== 'F2 - Negotiation' || etapaOpt== 'F4 - Offer Development' || etapaOpt== 'F5 - Solution Definition' || etapaOpt== 'F6 - Prospecting' || etapaOpt == 'F3 - Offer Presented' && ((usu.BI_Permisos__c=='Ingeniería/Preventa')|| (usu.BI_Permisos__c == 'Jefe de Ventas') || (usu.BI_Permisos__c == 'Administrador del sistema'))) //usu.BI_Permisos__c='BI_COL_Jefe_Ingenieria'||
      {
        estados.add('Enviado');
        //lstMSConsulta = getDSMS(idOpt, estados);
        BolauxMen = true; 
      }
      if(etapaOpt!='F1 - Closed Won' && etapaOpt !='F2 - Negotiation')
      {
        estados.add('Pendiente');
        estados.add('Cerrada perdida');
        
      }
      else if((etapaOpt=='F1 - Closed Won' || etapaOpt =='F2 - Negotiation') && permiSet.contains(usu.BI_Permisos__c) )
      {
        estados.add('Pendiente');
        estados.add('Cerrada perdida');
      }
      System.debug('\n\n estados: '+estados+'\n\n');
      lstMSConsulta = getDSMS(idOpt, estados);
      if(lstMSConsulta.isEmpty() && BolauxMen==false)
      
          {
              BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.ERROR, 'Este perfil no cuenta con los permisos suficientes para modificar esta MS');
              mostrarRegresar = false;        
          }
      
      else if(lstMSConsulta.isEmpty())
      {
        BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.ERROR, 'La oportunidad '+NameOpt+' no tiene Modificaciones de servicio asociadas en estado '+estados);
        mostrarRegresar = false;
      }
      else
      {
        //Llenar listado de MS Wrapper que se muestran en pantalla
        lstDS = new List<WrapperMS>(); 
        for(BI_COL_Modificacion_de_Servicio__c ms: lstMSConsulta)
        {
          boolean estado=false;
          boolean mostrarMotivo=false;
          
          
          if(ms.BI_COL_Estado__c=='Pendiente'){
            System.debug('\n\n ms.BI_COL_Estado__c'+ms.BI_COL_Estado__c+'\n\n');
            estado=true;
            mostrarMotivo=false;
          }
          // && (ms.BI_COL_Estado_orden_trabajo__c=='En Desarrollo' || ms.BI_COL_Estado_orden_trabajo__c=='Aplazada')
          if(ms.BI_COL_Estado__c=='Enviado'){
            estado=true;
            mostrarMotivo=true;
          }
          WrapperMS wMS = new WrapperMS(ms,estado,mostrarMotivo);
          this.lstDS.add(wMS);
        }
        //Inicializar valores del paginador
        pageNumber = 0;
        totalPageNumber = 0;
        pageSize = 10;
        ViewData();
      }
    }
    else
    {                      
      BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.WARNING, 'Solo se puede utilizar si la Oportunidad esta en la Etapa F2 - Ganada o E6 - Ganada');
      mostrarRegresar = false;
    }
  }
  
  /**
  * Genear asociacion de MS a la solicitud de Servicios
  * @return PageReference
  */
  public PageReference action_seleccionarDS()
  {
    List<BI_COL_Modificacion_de_Servicio__c> lstMSSel = new List<BI_COL_Modificacion_de_Servicio__c>();
    
    for(WrapperMS wMS : this.lstDS)
    {
      if(wMS.seleccionado==true)
      {
        lstMSSel.add(wMS.ms);
        System.debug('\n\n'+wMS.ms.Name+'\n\n');
      }
    }
    system.debug('lstMSSel Seleccioinado ------------->'+lstMSSel);
    
    
    string errorMotivo=''; 
    
    
    //List<string> idMS=new List<string>();
    for(BI_COL_Modificacion_de_Servicio__c ms : lstMSSel)
    {
        System.debug('Info +'+ms.BI_COL_Motivo_cancelacion__c);
        System.debug('QBI_COL_Motivo_cancelacion__c '+ms.BI_COL_Motivo_cancelacion__c+'ms.BI_COL_Estado__c'+ms.BI_COL_Estado__c);
        
        if(ms.BI_COL_Estado__c=='Pendiente'){
          
          //if(ms.BI_COL_Motivo_cancelacion__c!=null && ms.BI_COL_Motivo_cancelacion__c!=''){
          ms.BI_COL_Estado__c = 'Cerrada perdida';
          //ms.BI_COL_Estado_orden_trabajo__c = 'Cancelado';
          //ms.BI_COL_Fecha_solicitud_cancelacion__c=system.today();
          //idMS.add(ms.Id);
          //}else{
          //  errorMotivo=' '+ms.Name;        
          //}
          
        }else if(ms.BI_COL_Estado__c=='Cerrada perdida'){
          ms.BI_COL_Estado__c = 'Pendiente';
          ms.BI_COL_Motivo_cancelacion__c=null;
          //idMS.add(ms.Id);
        }else if((ms.BI_COL_Estado__c=='Enviado')){
          
          if(ms.BI_COL_Motivo_cancelacion__c!=null && ms.BI_COL_Motivo_cancelacion__c!=''){
          ms.BI_COL_Estado__c = 'Cerrada perdida';
          ms.BI_COL_Estado_orden_trabajo__c = 'Cancelado';
          ms.BI_COL_Fecha_solicitud_cancelacion__c=system.today();
          //idMS.add(ms.Id);
          }else{
            errorMotivo=errorMotivo+' '+ms.Name;        
          }
        }

      
      
    }
    
    PageReference pageRef;
    Savepoint sp;
    try{
      //Actualizar registros MS
      sp= Database.setSavepoint();
      update lstMSSel;      
      if(errorMotivo!=''){
        BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.ERROR, 'Las siguientes MS no fueron actualizadas por favor asegúrese de ingresar un motivo de cancelación' + errorMotivo);
        return null;      
      }
      
      pageRef = new PageReference('/'+idOpt);
    }
    catch(DMLException e)
    {
      BI_COL_MessageUtil_cls.mostrarMensaje(BI_COL_MessageUtil_cls.ERROR, 'No se pudo actualizar los registros: ' + e.getMessage());
      Database.rollback(sp);
      //pageRef=new PageReference('');
    }
    try{

          pageRef.setRedirect(true);

      }catch(Exception e)
      {
       
      }

    return pageRef;
  }
  
  /**
  * Acción para seleccionar todos los registros de MS
  * @return PageReference
  */
  public PageReference action_seleccionarTodos()
  {
    if(lstDS!=null)
    {
      for(WrapperMS wMS : this.lstDS)
      {
        wMS.seleccionado = this.todosMarcados;
      }
    }
    return null;
  }
  //Clase Wrapper que administra la selección de registros y el estado del motivo de cancelacion 
  public class WrapperMS
  {
    public boolean seleccionado {get;set;}
    public boolean mostrarMotivo {get;set;}
    public BI_COL_Modificacion_de_Servicio__c ms {get;set;}
    
    public WrapperMS(BI_COL_Modificacion_de_Servicio__c ms, boolean seleccionado, boolean mostrarMotivo)
    {
      this.ms = ms;
      this.seleccionado = seleccionado;
      this.mostrarMotivo = mostrarMotivo;
      System.debug('\n\n'+'MS Seleccionada---------->'+seleccionado+'\n\n');
      System.debug('\n\n'+'MS mostrarMotivo---------->'+mostrarMotivo+'\n\n');
    }
  }
  
  /**
  * Obtiene el listado de MS asociadas a la Solicitud de Servicio
  *
  * @param idCliente Id del cliente asociado a la solicitud de servicio
  * @param estado Estado(s) validos para restringir la consultar de la Modificacion de Servicio
  * @return  Lista de MS
  */
  
  public List<BI_COL_Modificacion_de_Servicio__c> getDSMS(String idOpt, List<String> estados)
  {
    List<BI_COL_Modificacion_de_Servicio__c> lstDS = new List<BI_COL_Modificacion_de_Servicio__c>();
    System.debug('\n\n estado: '+estados+'\n\n');
    lstDS = [Select n.Id, n.Name, BI_COL_Producto__r.NE__ProdId__r.Name, BI_COL_Producto__r.NE__ProdId__r.Rama_Local__c, 
              BI_COL_Estado__c, BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Sucursal_Origen__r.Name, BI_COL_Motivo_cancelacion__c, BI_COL_Estado_orden_trabajo__c,BI_COL_Fecha_solicitud_cancelacion__c
         from   BI_COL_Modificacion_de_Servicio__c n
         where  n.BI_COL_Oportunidad__c= :idOpt
         and    BI_COL_Estado__c in : estados
         and  BI_COL_Estado_orden_trabajo__c not in :noCancela
         order by LastModifiedDate Desc limit 1000];
    
    return lstDS;
  }
  
  //IMPLEMENTACION PAGINADOR
  public Integer getPageNumber()
  {
    return pageNumber;
  }

  public Integer getPageSize()
  {
    return pageSize;
  }
  
  public Boolean getPreviousButtonEnabled()
  {
    return !(pageNumber > 1);
  }
  
  public Boolean getNextButtonDisabled()
  {
    if (lstDS == null)
    {  
      return true;
    }
    else
    {
      if(Test.isRunningTest())
      {
        pageNumber = 1;
        pageSize   = 1;
      }
      return ((pageNumber * pageSize) >= lstDS.size());
    }
  
  }
  
  /**
  * Obtiene el numer de paginas de la tabla 
  * @return numero de paginas
  */
  public Integer getTotalPageNumber() 
  {
    if (totalPageNumber == 0 && lstDS !=null)
    {
      totalPageNumber = lstDS.size() / pageSize;
      Integer mod = lstDS.size() - (totalPageNumber * pageSize);
      if (mod > 0)
      totalPageNumber++;
    }
    return totalPageNumber;
  }

  public PageReference ViewData()
  {
    totalPageNumber = 0;
    BindData(1);
    return null;
  }
  
  /**
  * Posiciona el registro segun el numero de pagina
  * @param newPageIndex Indice de la pagina
  * @return 
  */
  @TestVisible 
  private void BindData(Integer newPageIndex)
  {
    try
    {
      pageMS = new List<WrapperMS>();
      Transient Integer counter = 0;
      Transient Integer min = 0;
      Transient Integer max = 0;
      
      if (newPageIndex > pageNumber)
      {
        min = pageNumber * pageSize;
        max = newPageIndex * pageSize;
      }
      else
      {
        max = newPageIndex * pageSize;
        min = max - pageSize;
      }
      
      system.debug('WrapperMS wMS'+lstDS.size()+' '+min+' '+max);
      for(WrapperMS wMS : lstDS)
      { 
        counter++;
        if (counter > min && counter <= max){
          pageMS.add(wMS);
        }
          
      }
      pageNumber = newPageIndex;
      
      if (pageMS == null || pageMS.size() <= 0)
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Data not available for this view.'));
    }
    catch(Exception ex)
    {
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
    }  
  }
  
  public PageReference nextBtnClick() {
      BindData(pageNumber + 1);
    return null;
  
  }
  
  public PageReference previousBtnClick() {
      BindData(pageNumber - 1);
    return null;
  
  }
  
  public List<WrapperMS> getLstMS()
  {
    return pageMS;
  }
  
 
    public static set<String> getPermisionSetUsuario(String idUsuario)
    {
        Set<String> usuPermision=new Set<String>();
        for( PermissionSetAssignment psa:[select id,PermissionSet.Name from PermissionSetAssignment where AssigneeId=:idUsuario])
        {
            usuPermision.add(psa.PermissionSet.Name);
        }
        return usuPermision;
    }
}