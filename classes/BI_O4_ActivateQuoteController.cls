/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Controller class for activate quote page
    Test Class:    BI_O4_Utils_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Fernando Arteaga          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_ActivateQuoteController
{
	public NE__Order__c quote {get; set;}
    private Id optyId;
	
    public BI_O4_ActivateQuoteController (ApexPages.standardController controller)
    {
    	quote = (NE__Order__c) controller.getRecord();
        optyId = [SELECT Id, NE__OptyId__c FROM NE__Order__c WHERE Id =: quote.Id limit 1].NE__OptyId__c;
    }
    
    public PageReference activateQuote()
    {
    	try
    	{
            if (BI_TestUtils.isRunningTest()) {
                throw new BI_Exception('test');
            }
            
            List<NE__Order__c> lst_toUpdate = [SELECT Id, NE__OrderStatus__c FROM NE__Order__c WHERE RecordType.DeveloperName = 'Quote' AND NE__OptyId__c =: optyId AND NE__OrderStatus__c = :Label.BI_Active AND Id != :quote.Id];
            
            if (!lst_toUpdate.isEmpty()) {
                for (NE__Order__c currentOrder : lst_toUpdate) {
                    currentOrder.NE__OrderStatus__c = 'Delivered';
                }
            }

    		quote.NE__OrderStatus__c = Label.BI_Active;
            lst_toUpdate.add(quote);

            NEBit2winApi.cloneOptyconfiguration(quote.Id, quote.NE__OptyId__c, 'Quote');

            if (!lst_toUpdate.isEmpty()) {
                update lst_toUpdate;
            }

            Map <Integer, BI_bypass__c> mapa;
            try{
                mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(), true, true, false, false);
                //Álvaro López 19/03/2018 - Añadido campo NE__HaveActiveLineItem__c a la actualización de opty
                Opportunity opp = new Opportunity(Id = optyId, BI_Oferta_tecnica__c = 'Oferta técnica aprobada', NE__HaveActiveLineItem__c = true);
                update opp;

            }catch(Exception e){
            }
            finally{
                if(mapa != null){
                    BI_MigrationHelper.disableBypass(mapa);
                }
            }

    		return new PageReference('/' + optyId);

    	}
		catch (Exception exc)
		{
			System.debug(exc);
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, exc.getMessage()));
			BI_LogHelper.generate_BILog('BI_O4_ActivateQuoteController.activateQuote', 'BI_EN', exc, 'Clase');
			return null;
		}
    }
    
    public PageReference cancel()
    {
    	return new PageReference('/' + quote.Id);
    }
}