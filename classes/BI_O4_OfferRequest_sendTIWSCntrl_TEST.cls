@isTest
public class BI_O4_OfferRequest_sendTIWSCntrl_TEST {

    static{
        BI_TestUtils.throw_exception = false;
    }

    @isTest
    public static void test_run() {
        
        try{
        BI_O4_Offer_Request__c oreq = new BI_O4_Offer_Request__c();
        insert oreq;
        
        Test.setCurrentPage(Page.BI_O4_OfferRequest_sendTIWS);
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(oreq);
        ApexPages.currentPage().getParameters().put('id', oreq.Id);
        
        Test.startTest();
        BI_TestUtils.throw_exception = false;
        BI_O4_OfferRequest_sendTIWSController tiws = new BI_O4_OfferRequest_sendTIWSController(controller);
		tiws.run();
        BI_TestUtils.throw_exception = true;
        tiws = new BI_O4_OfferRequest_sendTIWSController(controller);
        tiws.run();
        Test.stopTest();
        
        //System.assert(tiws.error == null);
        System.assertNotEquals(tiws.baseURL, null);
        }catch(Exception e){
            
        }
    }
    
}