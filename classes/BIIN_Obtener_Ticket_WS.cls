public class BIIN_Obtener_Ticket_WS extends BIIN_UNICA_Base
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jose María Martín Castaño
    Company:       Deloitte
    Description:   Clase para obtener todos los campos de la Incidencia Técnica de RoD (Detalle de la Incidencia).
	    
    History:
	    
    <Date>           <Author>        							  	<Description>
    10/12/2015       Jose María Martín Castaño			    		Initial version
    12/01/2016       Micah Burgos García                            Fix bug in deserialize JSON with special characters
    16/05/2016       José Luis González Beltrán                     Adapt to UNICA interface
    16/05/2016       José Luis González Beltrán                     Attachment name fix
	29/05/2016       José Luis González Beltrán                     Fix fill BI_Status_Reason__c & BI_Assigned_Group__c
	01/07/2016       José Luis González Beltrán                     Translate field BI_Status_Reason__c
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static final String M_STATUS_MISSED      = 'Missed';
    public static final String M_STATUS_MISSED_GOAL = 'Missed Goal';
    public static final String M_STATUS_WARNING     = 'Warning';

    DateTime startDateTime = DateTime.newInstance(0);
    
    public ObtenerTicketRespuesta detalleTicket(String authorizationToken, String caseId)
    {


     Case[] qCase = [SELECT BI_Country__c, BI_Id_del_caso_legado__c, AccountId, caseNumber FROM Case WHERE id =: caseId LIMIT 1];

            // Callout URL
            String url = 'callout:BIIN_WS_UNICA/ticketing/v1/tickets/' + qCase[0].BI_Id_del_caso_legado__c;

            // Prepare Request
            HttpRequest req = getRequest(url, 'GET');

			// Get response
            BIIN_UNICA_Pojos.ResponseObject response = getResponse(req, BIIN_UNICA_Pojos.TicketDetailType.class);
            system.debug(' BIIN_Obtener_Ticket_WS.detalleTicket | La salida del servicio es (response): ' + response);
            BIIN_UNICA_Pojos.TicketDetailType ticketDetail = (BIIN_UNICA_Pojos.TicketDetailType) response.responseObject;
            system.debug(' BIIN_Obtener_Ticket_WS.detalleTicket | Detalle ticket (ticketDetail): ' + ticketDetail);
            
            ObtenerTicketRespuesta respuesta = new ObtenerTicketRespuesta();
            if(ticketDetail != null) 
            {
                if(Test.isRunningTest())
                {
                    ticketDetail.ticketId = qCase[0].BI_Id_del_caso_legado__c;
                }

                respuesta.c = [SELECT 
                AccountId, 
                Account.BI_Country__c,
                CaseNumber, 
                BI_COL_Fecha_Radicacion__c, 
                CreatedDate, 
                LastModifiedDate, 
                ClosedDate, 
                OwnerId, 
                ContactId, 
                BIIN_Tiempo_Neto_Apertura__c 
                FROM Case                                               /* Álvaro López 09/04/2018 - Integración Colombia*/
                WHERE BI_Id_del_caso_Legado__c =: ticketDetail.ticketId AND Account.BI_Country__c =: ticketDetail.country
                LIMIT 1];
                
                if(ticketDetail.parentTicket != null && ticketDetail.parentTicket != '') 
                {
                    //respuesta.c.ParentId = [SELECT id FROM Case WHERE id =: ticketDetail.parentTicket].Id;
                    respuesta.c.ParentId = [SELECT id FROM Case WHERE CaseNumber =: ticketDetail.parentTicket].Id;
                }

                String strCountry = ticketDetail.country;
                if(String.isEmpty(strCountry)) {
                    strCountry = respuesta.c.Account.BI_Country__c;
                }
                System.debug('\n\n-=#=-\n' + 'strCountry' + ': ' + strCountry + '\n-=#=-\n');

                Map<String, String> additionalData = BIIN_UNICA_Utils.additionalDataToMap(ticketDetail.additionalData);
                
                respuesta.c.BI_Id_del_caso_Legado__c            = ticketDetail.ticketId;

                if(strCountry == Label.BI_Colombia) {
                    // Campos añadido para fase 2 de Colombia
                    respuesta.c.BI2_Nombre_Contacto_Final__c        = additionalData.get('directContactFirstName');
                    respuesta.c.BI2_Apellido_Contacto_Final__c      = additionalData.get('directContactLastName');
                    respuesta.c.BI2_Email_Contacto_Final__c         = additionalData.get('directContactEmail');
                    respuesta.c.BI_ECU_Telefono_fijo__c             = additionalData.get('phone');
                    respuesta.c.BI_ECU_Telefono_movil__c            = additionalData.get('mobile');
                    respuesta.c.BI2_COL_Contacto_de_cierre__c       = additionalData.get('closingContact');
                    respuesta.c.BI_COL_Codigo_CUN__c                = additionalData.get('BAO_ExternalID');
                    if(String.isNotEmpty(additionalData.get('cunDate')) && Integer.ValueOf(additionalData.get('cunDate')) != 0) {
                        respuesta.c.BI2_COL_Fecha_generacion_CUN__c = startDateTime.addSeconds(Integer.ValueOf(additionalData.get('cunDate')));
                    } else {
                        respuesta.c.BI2_COL_Fecha_generacion_CUN__c = null;
                    }
                } else {
                    respuesta.c.BI2_Nombre_Contacto_Final__c        = additionalData.get('givenName');
                    respuesta.c.BI2_Apellido_Contacto_Final__c      = additionalData.get('familyName');
                    respuesta.c.BI2_Email_Contacto_Final__c         = additionalData.get('email');
                }
                
                respuesta.c.BI_COL_Fecha_Radicacion__c          = startDateTime.addSeconds(Integer.ValueOf(ticketDetail.reportedDate));
                respuesta.c.TGS_Impact__c                       = ticketDetail.severity.substring(0, 2) + BIIN_UNICA_Utils.translateFromRemedy('Impacto', ticketDetail.severity.substring(2));
                respuesta.c.TGS_Urgency__c                      = additionalData.get('urgency').substring(0, 2) + BIIN_UNICA_Utils.translateFromRemedy('Urgencia', additionalData.get('urgency').substring(2));
                String cleanStatus                              = ticketDetail.ticketStatus.name().replaceAll('IN_PROGRESS', 'IN PROGRESS').replaceAll('NEW_STAUS', 'NEW');
                respuesta.c.Status                              = BIIN_UNICA_Utils.translateFromRemedy('Estado', cleanStatus);
                respuesta.c.Origin                              = BIIN_UNICA_Utils.translateFromRemedy('Origen', ticketDetail.source);
                respuesta.c.BIIN_Site__c                        = additionalData.get('site');
                respuesta.c.BIIN_Id_Producto__c                 = additionalData.get('ci');
                respuesta.c.BIIN_Categorization_Tier_1__c       = additionalData.get('operationalCategorization1');
                respuesta.c.BIIN_Categorization_Tier_2__c       = additionalData.get('operationalCategorization2');
                respuesta.c.BIIN_Categorization_Tier_3__c       = additionalData.get('operationalCategorization3');
                respuesta.c.BI_Product_Categorization_Tier_1__c = additionalData.get('productCategorization1');
                respuesta.c.BI_Product_Categorization_Tier_2__c = additionalData.get('productCategorization2');
                respuesta.c.BI_Product_Categorization_Tier_3__c = additionalData.get('productCategorization3');
                respuesta.c.Subject                             = ticketDetail.subject;
                respuesta.c.Description                         = ticketDetail.description;
                respuesta.c.TGS_Resolution__c                   = ticketDetail.resolution;
                respuesta.c.BI_Resolution_Category_Tier_1__c    = additionalData.get('resolutionCategory1');
                respuesta.c.BI_Resolution_Category_Tier_2__c    = additionalData.get('resolutionCategory2');
                respuesta.c.BI_Resolution_Category_Tier_3__c    = additionalData.get('resolutionCategory3');
                respuesta.c.Priority                            = BIIN_UNICA_Utils.translateFromRemedyByID('Prioridad', ticketDetail.priority);

                if(!String.isEmpty(ticketDetail.responsibleParty)) 
                {
                    respuesta.c.BI_Assigned_Group__c = ticketDetail.responsibleParty;
                }
                
                if(!String.isEmpty(ticketDetail.statusChangeReason)) 
                {
                    respuesta.c.BI_Status_Reason__c = BIIN_UNICA_Utils.translateFromRemedy('Subestado', ticketDetail.statusChangeReason);
                }
                
                if(additionalData.get('slmStatus') != null && additionalData.get('slmStatus') != '') 
                {
                    respuesta.c.TGS_SLA_Light__c = BIIN_UNICA_Utils.translateFromRemedy('SLM-Status', additionalData.get('slmStatus'));
                }
                
                if(ticketDetail.resolutionDate != null && ticketDetail.resolutionDate != '')
                {
                    respuesta.c.BI2_fecha_hora_de_resolucion__c = startDateTime.addSeconds(Integer.valueOf(ticketDetail.resolutionDate));
                }

                List<Worklog> lwlSalida = new List<Worklog>();
                List<SLA> lslaSalida = new List<SLA>();

                if (ticketDetail.notes != null) 
                {
                    Worklog w;
                    for(BIIN_UNICA_Pojos.TicketNoteInfoType note : ticketDetail.notes) 
                    {
                        w = new Worklog();

                        if (note.additionalData != null)
                        {
                            Map<String, String> noteAD = BIIN_UNICA_Utils.additionalDataToMap(note.additionalData);
                            w.viewAccess               = noteAD.get('viewAccess');
                        }
                        
                        w.WorkLogID           = note.noteId;
                        w.incidentNumber      = ticketDetail.ticketId;
                        w.detailedDescription = note.text;
                        
                        if(note.creationDate != null && note.creationDate != '')
                        {
                            w.workLogDate = string.valueOf(startDateTime.addSeconds(integer.valueOf(note.creationDate)));
                        }

                        if(note.attachments != null)
                        {
                            for (Integer n = 0; n < note.attachments.size(); n++) 
                            {
                                if (String.isNotEmpty(note.attachments.get(n).name))
                                {
                                    String value = note.attachments.get(n).attachmentId;
                                    String name = note.attachments.get(n).name;
                                    if (n == 0)
                                    {
                                        w.z2AFWorkLog01 = name;
                                    }
                                    if (n == 1)
                                    {
                                        w.z2AFWorkLog02 = name;
                                    }
                                    if (n == 2)
                                    {
                                        w.z2AFWorkLog03 = name;
                                    }
                                }
                            }
                        }
                        
                        respuesta.lw.add(w);
                    }
                }

                if (ticketDetail.relatedObjects != null) 
                {
                    Decimal TVencimientoSeg;
                    SLA sla;
                    for(BIIN_UNICA_Pojos.RelatedObjectType relatedObject : ticketDetail.relatedObjects) 
                    {
                        if (relatedObject.relationship == 'SLA Associated')
                        {
                            sla = new SLA();

                            sla.IncId = ticketDetail.ticketId;

                            if(relatedObject.additionalData != null)
                            {
                                Map<String, String> roAdditionalData = BIIN_UNICA_Utils.additionalDataToMap(relatedObject.additionalData);
                                
                                sla.SVTTitle              = roAdditionalData.get('svtTitle');
                                sla.GoalCategoryChar      = roAdditionalData.get('goalCategoryChar');
                                sla.GoalTimeHr            = roAdditionalData.get('goalTimeHr');
                                sla.GoalTimeMin           = roAdditionalData.get('goalTimeMin');
                                sla.WarningDateTimefrmINT = roAdditionalData.get('warningDateTimefrmINT');
                                sla.MeasurementStatus     = BIIN_UNICA_Utils.translateFromRemedy('Measurment Status', roAdditionalData.get('measurementStatus'));

                                if(roAdditionalData.get('svtDueDate') != null && roAdditionalData.get('svtDueDate') != '') 
                                {
                                    sla.SVTDueDate = String.valueOf(startDateTime.addSeconds(Integer.valueOf(roAdditionalData.get('svtDueDate'))));
                                }

                                if(roAdditionalData.get('measurementStatus') == M_STATUS_MISSED 
                                    || roAdditionalData.get('measurementStatus') == M_STATUS_MISSED_GOAL 
                                    || roAdditionalData.get('measurementStatus') == M_STATUS_WARNING) 
                                {
                                    if(roAdditionalData.get('metMissedAmount') != '' && roAdditionalData.get('metMissedAmount') != null) 
                                    {
                                        TVencimientoSeg       = Decimal.valueOf(roAdditionalData.get('metMissedAmount'))/3600;
                                        sla.TiempoVencimiento = String.valueOf(TVencimientoSeg.SetSCale(2));
                                    }
                                } 
                                else 
                                {
                                    if(roAdditionalData.get('upTime') != '' && roAdditionalData.get('upTime') != null) 
                                    {
                                        TVencimientoSeg       = Decimal.valueOf(roAdditionalData.get('upTime'))/3600;
                                        sla.TiempoVencimiento = String.valueOf(TVencimientoSeg.SetSCale(2));
                                    }
                                }
                            }
                            
                            respuesta.lsla.add(sla);
                        }
                    }
                }

                system.debug(' BIIN_Obtener_Ticket_WS.detalleTicket | Respuesta: ' + respuesta);
                respuesta.code = '000';
                return respuesta;
            } 
            else 
            {
                system.debug(' BIIN_Obtener_Ticket_WS.detalleTicket | [OBTENER-TICKET] WARNING: La lista llega vacía');
                respuesta.code = '001';
                return respuesta;
            }

        }

    //##############POJOS
    public class ObtenerTicketRespuesta
    { // Respuesta de nuestro WS. Lo que mandamos a la ventana
        public Case c = new Case();
        public List<Worklog> lw = new List<Worklog>();
        public List<SLA> lsla = new List<SLA>();
        public String code;
    }
    
    public class Worklog
    {
        public String IncId {get;set;}
        public String incidentNumber{get;set;}
        public String detailedDescription{get;set;}
        public String workLogDate{get;set;}
        public String z2AFWorkLog01{get;set;}
        public String z2AFWorkLog02{get;set;}
        public String z2AFWorkLog03{get;set;}
        public String viewAccess{get;set;}
        public String WorkLogID{get;set;}
    }

    public class SLA
    {
        public String IncId {get;set;}
        public String SVTTitle {get;set;}
        public String SVTDueDate {get;set;}
        public String MeasurementStatus{get;set;}
        public String GoalCategoryChar{get;set;}
        public String GoalTimeHr{get;set;}
        public String GoalTimeMin{get;set;}
        public String UpTime{get;set;}
        public String MetMissedAmount{get;set;}
        public String TiempoVencimiento{get;set;}
        public String WarningDateTimefrmINT{get;set;}
    }

}