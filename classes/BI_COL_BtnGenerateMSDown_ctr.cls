/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-04-14      Daniel ALexander Lopez (DL)     Cloned Controller      
*************************************************************************************/
public with sharing class BI_COL_BtnGenerateMSDown_ctr {
  
   /**Atributos*/
  public boolean mostrarRegresar {get;set;}
  public List<WrapperMS> lstMS {get;set;}
  public boolean todosMarcados {get;set;}
  public boolean mostrarValidaciones{get; set;}
  public String Busqueda{get; set;}
  public String picklist {get;set;} 
  public list<SelectOption> ls{get;set;}
  public String idOpport {get; set;}
  public boolean blGenerteMS{get; set;}

  
  /**Parametros del paginador*/
  private Integer pageNumber;
  private Integer totalPageNumber;
  private Integer pageSize;
  private List<WrapperMS> pageMS;
  private String idNewOpport;

  public BI_COL_BtnGenerateMSDown_ctr()
  {
    getValueCalsificacionService();
  }

  /** Constructor */
  //public BtnGenerarMSBaja_ctr()
  public void Buscar()
  {
    
    //if(ApexPages.currentPage().getParameters().get('idCliente') !=null /*&&idCliente.trim().length()>0*/){
    if(ApexPages.currentPage().getParameters().get('idCliente') !=null  /*&&idCliente.trim().length()>0*/)
    {
      if(ApexPages.currentPage().getParameters().get('idOpport') !=null)
      {
       
      System.debug('========= id cliente ========='+ApexPages.currentPage().getParameters().get('idCliente'));
      System.debug('========= id Oportunidad ========='+ApexPages.currentPage().getParameters().get('idOpport'));
      String idCliente = ApexPages.currentPage().getParameters().get('idCliente');
      String idOpport = ApexPages.currentPage().getParameters().get('idOpport');
      idNewOpport = idOpport;
      
      System.debug(':: Carga Cliente: ' + idCliente+ '\n idOpport========='+ idOpport);
      
      //List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta = getMSSolicitudServicio(idCliente);
      
      //List<BI_COL_Modificacion_de_Servicio__c> lstMSProcesar = getMSSolicitudServicio(idCliente,idOpport);
      List<BI_COL_Modificacion_de_Servicio__c> lstMSProcesar = getMSSolicitudServicio(idCliente);
      
      System.debug('\n\n lstMSProcesar:'+ lstMSProcesar);
      //List<BI_COL_Modificacion_de_Servicio__c> lstMSProcesar2 = getMSSolicitudServicio(idOpport);

      List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta =new List<BI_COL_Modificacion_de_Servicio__c>();
      
      Map<String,String> tempMap=new Map<String,String>();
      
      for(BI_COL_Modificacion_de_Servicio__c ms:lstMSProcesar)
      {
        if(ms.BI_COL_Estado__c==label.BI_COL_lblPendiente)
        {
          tempMap.put(ms.BI_COL_Codigo_unico_servicio__r.Name,ms.BI_COL_Estado__c);
        }
      }
      
      for(BI_COL_Modificacion_de_Servicio__c ms:lstMSProcesar)
      {
        if(ms.BI_COL_Estado__c==label.BI_COL_lblActiva && !tempMap.containsKey(ms.BI_COL_Codigo_unico_servicio__r.Name))
        {
          lstMSConsulta.add(ms);
        }
      } 
      
      if(lstMSConsulta != null &&lstMSConsulta.size() > 0)
      {  

        //Llenar listado de MS Wrapper que se muestran en pantalla
        lstMS = new List<WrapperMS>();
        this.mostrarRegresar = true; 
        this.mostrarValidaciones = false; 
        for(BI_COL_Modificacion_de_Servicio__c ms: lstMSConsulta)
        {
          
          WrapperMS wMS = new WrapperMS(ms, false);
          System.debug('\n\n MS========>>>>'+ms);
          
          //Calculo de días restantes del servicio
          if(ms.BI_COL_Fecha_instalacion_servicio_RFS__c != null)
          {
            Integer dMese=ms.BI_COL_Duracion_meses__c!=null?Integer.valueOf(ms.BI_COL_Duracion_meses__c):1;
            Date fechaFinalServicio = ms.BI_COL_Fecha_instalacion_servicio_RFS__c.addMonths(dMese);
            Integer diasDif = System.today().daysBetween(fechaFinalServicio)-1; //Calculo de la diferencia en Dias
            wMS.diasServicio = diasDif;
            Double valorDia = ms.BI_COL_Total_servicio__c / (dMese * 30); //Calculo del valor del servicio por dia
            Double valorMulta = valorDia * diasDif; //Calculo del Valor de la Multa.
            System.debug('\n\n valorMulta='+valorMulta+'\n\n');
            wMS.valorMulta = valorMulta;
          }
          this.lstMS.add(wMS);
          
        }
        
        System.debug('\n\n this.mostrarValidaciones='+this.mostrarValidaciones+'\n\n');
        System.debug('La lista de Wrapper MS es: ' + lstMS);
        
        //Inicializar valores del paginador
        pageNumber = 0;
        totalPageNumber = 0;
        pageSize = 50;
        ViewData();
      }
      else
      {
        BI_COL_Useful_messages_cls.mostrarMensaje(BI_COL_Useful_messages_cls.INFO, 'No existe modificaciones de servicios activas asociadas al cliente');
      }
    
    }else{
      BI_COL_Useful_messages_cls.mostrarMensaje(BI_COL_Useful_messages_cls.ERROR, 'Por favor especifique un cliente');
    }

   } 

  }
  
  public PageReference ViewData()
  {
    totalPageNumber = 0;
    BindData(1);
    return null;
  }
  
  /**
  * Accion para generar MS de Baja
  * @PageReference 
  */
  public PageReference action_generarMSBaja()
  {
    System.debug('\n\n his.blGenerteMS: '+this.mostrarValidaciones+'  picklist'+ picklist+'\n\n');
    if(picklist!='BAJA')
    {
      this.mostrarValidaciones=true;
      this.blGenerteMS=true;
      //this.mostrarRegresar = false;
    }
    List<BI_COL_Modificacion_de_Servicio__c> lstMSSeleccionado = new List<BI_COL_Modificacion_de_Servicio__c>();
    List<String> listIdMS = new List<String>();
    system.debug('action_generarMSBaja:lstMS: '+lstMS);

    Set<String> setId = new Set<String>();
    for(WrapperMS wMS : this.lstMS)
    {
      if(wMS.seleccionado)
      {
        setId.add(wMS.ms.Id);
      }
    }

    Map<String,BI_COL_Modificacion_de_Servicio__c> mapIdOpportunity = new Map<String,BI_COL_Modificacion_de_Servicio__c>([Select Id,BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__c From BI_COL_Modificacion_de_Servicio__c Where id In: setId]);
    for(WrapperMS wMS : this.lstMS)
    {
      if(wMS.seleccionado)
      {
        lstMSSeleccionado.add(wMS.ms);
        listIdMS.add(wMS.ms.Id);
        wMS.ms.BI_COL_Clasificacion_Servicio__c = picklist;
        wMS.ms.BI_COL_Estado__c=label.BI_COL_lblPendiente;
        //wMS.ms.BI_COL_Estado_orden_trabajo__c=label.BI_COL_lblSolicitado;
        wMS.ms.BI_COL_Oportunidad__c=mapIdOpportunity.get(wMS.ms.Id).BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__c;

      }
    }
    
    if(lstMSSeleccionado == null || lstMSSeleccionado.size()==0)
    {
      BI_COL_Useful_messages_cls.mostrarMensaje(BI_COL_Useful_messages_cls.ERROR, 'Debe seleccionar por lo menos un registro');
      return null;
    }
    System.debug('\n\n this.mostrarValidaciones= '+this.mostrarValidaciones+'\n\n');
    //Si no se han mostrado las validaciones, no se puede seguir
    if(this.mostrarValidaciones)
    {
      //Obtener MS de Bajas
      List<BI_COL_Modificacion_de_Servicio__c> lstMSBajas = new List<BI_COL_Modificacion_de_Servicio__c>();
      lstMSBajas = getLstMSBaja(lstMSSeleccionado);
      List<String> regMS = new List<String>();
      try
      {
        System.debug('\n\n lstMSBajasDEVUELTA====>>>>'+lstMSBajas);
          insert lstMSBajas;
          //update lstMSSeleccionado; 
          for(BI_COL_Modificacion_de_Servicio__c ms:lstMSBajas)
          {
            regMS.add(ms.id);
          }
          BI_COL_Useful_messages_cls.mostrarMensaje(BI_COL_Useful_messages_cls.INFO, 'Las Modificaciones de Servicio fueron creadas exitosamente' );
          this.mostrarValidaciones = false;
          //BtnGenerarMSBaja_ctr btn=new BtnGenerarMSBaja_ctr();
        }
        catch(Exception e)
        {
          BI_COL_Useful_messages_cls.mostrarMensaje(BI_COL_Useful_messages_cls.ERROR, 'No se pudo crear la lista de Modificaciones de Servicio de baja: ' + e.getMessage());
        }
        
        this.mostrarRegresar = false; 
        //envioDatosSISGOT(regMS);
        System.debug('\n\n Manda al Metodo Futuro \n\n');
        
    }
    else 
    {
      String validacionesUno = this.validaDSEmpaquetada();
      String validacionesDos = this.validaMSInternet();
      String validacionesTres = this.validaMulta();
      
      this.mostrarValidaciones = true;
      
      if(validacionesUno.equals('') && validacionesDos.equals('') && validacionesTres.equals(''))
      {
        this.action_generarMSBaja();
      }
      else
      {
        String strMensaje = validacionesUno + '\n' + validacionesDos + '\n' + validacionesTres;
        strMensaje += '\n'+label.BI_COL_lblAcuerdo;
        BI_COL_Useful_messages_cls.mostrarMensaje(BI_COL_Useful_messages_cls.INFO, strMensaje);
        
      }
    }
    return null;
  }
  
  
  
  /**IMPLEMENTACION PAGINADOR*/
  public Integer getPageNumber()
  {
    return pageNumber;
  }
  
  /**
  * Obtiene el numer de paginas de la tabla 
  * @return numero de paginas
  */
  public Integer getTotalPageNumber()
  {
    if (totalPageNumber == 0 && lstMS !=null)
    { 
      totalPageNumber = lstMS.size() / pageSize;
      Integer mod = lstMS.size() - (totalPageNumber * pageSize);
      if (mod > 0)
      totalPageNumber++;
    }
    return totalPageNumber;
  }
  
    public PageReference nextBtnClick() {
      BindData(pageNumber + 1);
    return null;
  
  }
  
  
  public PageReference previousBtnClick() {
      BindData(pageNumber - 1);
    return null;
  
  }
  
  public List<WrapperMS> getLstMS()
  {
    return pageMS;
  }
  
  /**
  * Posiciona el registro segun el numero de pagina
  * @param newPageIndex Indice de la pagina
  * @return 
  */
  private void BindData(Integer newPageIndex)
  {
    try
    {
      pageMS = new List<WrapperMS>();
      Transient Integer counter = 0;
      Transient Integer min = 0;
      Transient Integer max = 0;
      
      if (newPageIndex > pageNumber)
      {
        min = pageNumber * pageSize;
        max = newPageIndex * pageSize;
      }
      else
      {
        max = newPageIndex * pageSize;
        min = max - pageSize;
      }
      
      for(WrapperMS wMS : lstMS)
      {
        counter++;
        if (counter > min && counter <= max){
          pageMS.add(wMS);
        }
          
      }
      pageNumber = newPageIndex;
      
      if (pageMS == null || pageMS.size() <= 0)
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Data not available for this view.'));
    }
    catch(Exception ex)
    {
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
    }  
  }
  
  public Boolean getPreviousButtonEnabled()
  {
    return !(pageNumber > 1);
  }
  
  public Boolean getNextButtonDisabled()
  {
    if (lstMS == null){  return true;}
    else{return ((pageNumber * pageSize) >= lstMS.size());}
  
  }
  
  public Integer getPageSize()
  {
    return pageSize;
  }
  
  /**
  * Obtiene el listado de MS activas del cliente
  * 
  * @param idCliente Id del cliente asociado al FUN
  * @return List<BI_COL_Modificacion_de_Servicio__c> modificaciones de servicios activas
  */
  
  //public List<BI_COL_Modificacion_de_Servicio__c> getMSSolicitudServicio(String idCliente, String idOpport){
  public List<BI_COL_Modificacion_de_Servicio__c> getMSSolicitudServicio(String idCliente){
  
    
    System.debug(':: Cliente:' + idCliente + '' + '==== idOpport==='+ idOpport);

   // BI_COL_Producto__r.NE__ProdId__r.Family_Local__c name
    

  //  String consultaSOQL = 'SELECT BI_COL_Modificacion_de_Servicio__c.Id, BI_COL_Modificacion_de_Servicio__c.Name, BI_COL_Modificacion_de_Servicio__c.Oportunidad__c,BI_COL_Modificacion_de_Servicio__c.BI_COL_Producto__r.NE__ProdId__r.Rama_Local__c, BI_COL_Modificacion_de_Servicio__c.BI_COL_Producto__r.NE__ProdId__r.Name,BI_COL_Producto__r.NE__ProdId__r.Family_Local__c,BI_COL_Duracion_meses__c,BI_COL_Codigo_unico_servicio__r.Name,BI_COL_Fecha_instalacion_servicio_RFS__c,BI_COL_Modificacion_de_Servicio__c.BI_COL_Codigo_unico_servicio__r.BI_COL_DS_Medio_Acceso__c, BI_COL_Total_servicio__c,BI_COL_Estado__c FROM BI_COL_Modificacion_de_Servicio__c WHERE BI_COL_Modificacion_de_Servicio__c.BI_COL_Codigo_unico_servicio__r.Oportunidad__r.AccountId = \''+idCliente+'\' and m.BI_COL_Estado__c IN (\'Activa\',\'Pendiente\')';
    
    String consultaSOQL = 'SELECT BI_COL_Modificacion_de_Servicio__c.Id, BI_COL_Modificacion_de_Servicio__c.Name, BI_COL_Modificacion_de_Servicio__c.BI_COL_Oportunidad__c,'+
        'BI_COL_Modificacion_de_Servicio__c.BI_COL_Producto__r.NE__ProdId__r.Rama_Local__c,'+
        'BI_COL_Modificacion_de_Servicio__c.BI_COL_Producto__r.NE__ProdId__r.Name,'+
        'BI_COL_Producto__r.NE__ProdId__r.Family_Local__c,BI_COL_Producto_Anterior__c,'+
        //'Producto_Telefonica__r.Tipo_Anexo__c,'+
        'BI_COL_Duracion_meses__c,  BI_COL_Codigo_unico_servicio__r.Name, '+
        'BI_COL_Fecha_instalacion_servicio_RFS__c,  BI_COL_Modificacion_de_Servicio__c.BI_COL_Codigo_unico_servicio__r.BI_COL_DS_Medio_Acceso__c,'+
        'BI_COL_Porcentaje_aprobar__c, BI_COL_Total_servicio__c, BI_COL_TRM__c, BI_COL_Cargo_Fijo_Mes_Total__c, BI_COL_Cargos_Conexion_Total__c,'+
        'BI_COL_Edad_Provision__c, BI_COL_Autorizacion_facturacion__c, BI_COL_Plazo_Vencimiento_Factura__c, '+
        'BI_COL_Estado__c, BI_COL_Sucursal_Origen__c,BI_COL_FUN__c, BI_COL_Medio_Vendido__c, BI_COL_Medio_Preferido__c, BI_COL_Sucursal_de_Facturacion__c,'+  
        'BI_COL_Tipo_Facturacion__c,BI_COL_Impresion_factura__c,BI_COL_Renovable__c, BI_COL_Modificacion_de_Servicio__c.BI_COL_Autoconsumo__c, '+  
        'BI_COL_Cargo_fijo_mes__c, BI_COL_Cargo_conexion__c, BI_COL_Cuenta_facturar_davox1__c '+
        'FROM '+ 
        'BI_COL_Modificacion_de_Servicio__c'+ 
        ' WHERE '+ 
        ' BI_COL_Modificacion_de_Servicio__c.BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.AccountId = \''+ idCliente +'\''+
       ' and BI_COL_Modificacion_de_Servicio__c.BI_COL_Estado__c IN (\'Activa\') ';
    
    System.debug('consultaSOQL----->>>>'+consultaSOQL+ '---->Busqueda: '+Busqueda);
    
    if(Busqueda!=null && Busqueda!='' )
    {
      String dsBuscar='';
      if(Busqueda.contains(','))
      {
        List<String> lstDSB=Busqueda.split(',');
        for(String ls:lstDSB)
        {
          dsBuscar+='\''+ls+'\',';
        }
        dsBuscar=dsBuscar.subString(0,dsBuscar.length()-1);
      }
      else
      {
        dsBuscar='\''+Busqueda+'\'';
      }
     consultaSOQL+=' AND BI_COL_Codigo_unico_servicio__r.Name IN ('+dsBuscar+')'; 
    }
    
    
    //consultaSOQL+=' and p.Segmento__c=\''+Segmento+'\' and Activo__c = true Limit 10000';
    
    System.debug('----->>>>'+consultaSOQL);
    List<BI_COL_Modificacion_de_Servicio__c> lstMS=new List<BI_COL_Modificacion_de_Servicio__c>();
    System.debug('');
    lstMS=Database.query(consultaSOQL);
  
    System.debug('\n\n ******* La lista de MS de la consulta es: ' + lstMS);
    
    return lstMS;
  }
  
  /**
  * Acción para seleccionar todos los registros de MS
  * @return PageReference
  */
  public PageReference action_seleccionarTodos(){
    if(this.lstMS != null)
    {
      for(WrapperMS wMS : this.lstMS)
      {
        wMS.seleccionado = this.todosMarcados;
      }
    }
    return null;
  }
  
  /**
  * Conviernte un listado de MS de alta en un listado de MS de baja
  * @param  lstMS Listado de MS de Alta
  * @return List<BI_COL_Modificacion_de_Servicio__c> Lista de MS de Baja
  */

  

  public List<BI_COL_Modificacion_de_Servicio__c> getLstMSBaja (List<BI_COL_Modificacion_de_Servicio__c> lstMS){
    List<BI_COL_Modificacion_de_Servicio__c> lstMSBaja = new List<BI_COL_Modificacion_de_Servicio__c>();
    
    System.debug('\n\n idOpport2222=======>>>>>'+idOpport);
    System.debug('\n\n idNewOpportt2222=======>>>>>'+idNewOpport);

    //for(BI_COL_Modificacion_de_Servicio__c ms : lstMSBaja)
    for(BI_COL_Modificacion_de_Servicio__c ms : lstMS)
    {
      BI_COL_Modificacion_de_Servicio__c msBaja = ms.clone(false,true, false, false);
      msBaja.BI_COL_Clasificacion_Servicio__c = picklist; 
      msBaja.BI_COL_Oportunidad__c=idNewOpport;
      msBaja.BI_COL_Autorizacion_facturacion__c=false;
      msBaja.BI_COL_Fecha_inicio_de_cobro_RFB__c=null;
      msBaja.BI_COL_Fecha_instalacion_servicio_RFS__c=null;
      
      if(picklist=='BAJA')
      {
        msBaja.BI_COL_FUN__c=null;
      }
      else if(picklist=='SERVICIO INGENIERIA')
      {
        msBaja.BI_COL_Cargo_conexion__c=0;
        msBaja.BI_COL_Cargo_fijo_mes__c=0;
        msBaja.BI_COL_Duracion_meses__c=1;
      }

      System.debug('\n\n msBaja =====>>>> '+msBaja+'\n\n');
      System.debug('\n\n msBaja.BI_COL_Sucursal_Origen__c= '+msBaja.BI_COL_Sucursal_Origen__c+'\n\n');
      lstMSBaja.add(msBaja);
    }
    
    return lstMSBaja; 
  }
  
  
  public String validaDSEmpaquetada()
  {
    String strMessage = '';
    
    System.debug('\n\n ******** El mensaje de validación es: ' + strMessage);
    
    //Creamos una lista de String para guardar los Ids de las MS seleccioandas
    List<String> listStrMSId = new LIst<String>();
    
    
    //Recorremos la lista de MS para saber cuales fueron seleccionadas y pertenecen a un paquete
    for(WrapperMS objWMS : lstMS)
    {
      if(objWMS.seleccionado ){
        listStrMSId.add(objWMS.ms.Id);
      }
    }
    
    System.debug('\n\n ******* La lista de Id de MS es: ' + listStrMSId);
    
    if(listStrMSId.size() > 0)
    {
      //Obtenemos las MS que están empaquetadas
      List<BI_COL_Modificacion_de_Servicio__c> listMS =
      [
        SELECT Id, Name
        FROM BI_COL_Modificacion_de_Servicio__c
        WHERE Id IN :listStrMSId
      ];
      
      System.debug('\n\n ******* La lista de MS empaquetadas es: ' + listMS);
      
      if(listMS.size() > 0)
      {
        strMessage += Label.BI_COL_lblSiguientesMSEmpaquetadas; 
        
        for(BI_COL_Modificacion_de_Servicio__c objMS : listMS)
        {
          strMessage += objMS.Name + ', ';  
        }
        
        strMessage = strMessage.subString(0, strMessage.length() - 2);
        
        System.debug('\n\n ******** El mensaje de validación es: ' + strMessage);
      }
    }
    
    
    System.debug('\n\n ******** El mensaje de validación es: ' + strMessage);
    
    return strMessage;
  }
  
  
  public String validaMSInternet()
  {
    String strMessage = '';
    
    System.debug('\n\n ******** El mensaje de validación es: ' + strMessage);
    
    //Creamos una lista de String para guardar los Ids de las MS seleccioandas
    List<String> listStrMSId = new LIst<String>();
    
    
    //Recorremos la lista de MS para saber cuales fueron seleccionadas y pertenecen a un paquete
    for(WrapperMS objWMS : lstMS)
    {
      if(objWMS.seleccionado && objWMS.ms.BI_COL_Codigo_unico_servicio__r.BI_COL_DS_Medio_Acceso__c != null){
        listStrMSId.add(objWMS.ms.Id);
      }
    }
    
    System.debug('\n\n ******* La lista de Id de MS es: ' + listStrMSId);
    
    if(listStrMSId.size() > 0)
    {
      //Obtenemos las MS
      List<BI_COL_Modificacion_de_Servicio__c> listMS =
      [SELECT Id, Name, BI_COL_Codigo_unico_servicio__r.BI_COL_DS_Medio_Acceso__c, BI_COL_Codigo_unico_servicio__r.Id,BI_COL_Clasificacion_Servicio__c FROM BI_COL_Modificacion_de_Servicio__c
        WHERE Id IN :listStrMSId
      ];
      
      System.debug('\n\n ******* La lista de MS BI_COL_DS_Medio_Acceso__c es: ' + listMS);
      
      if(listMS.size() > 0)
      {
        strMessage += label.BI_COL_lblEstadoSiguientesMS; 
        
        String strList = '';
        
        for(BI_COL_Modificacion_de_Servicio__c objMS : listMS)
        {
          if(objMS.BI_COL_Codigo_unico_servicio__r.BI_COL_DS_Medio_Acceso__c == objMS.BI_COL_Codigo_unico_servicio__r.Id)
          {
            strList = '\nDS Principal: ' + objMS.BI_COL_Codigo_unico_servicio__r.Id + '\n' + strList;
          }
          else
          {
            strList += objMS.Name + ' - estado: ' + objMS.BI_COL_Clasificacion_Servicio__c +  ' , ';  
          }
        }
        
        strMessage = strMessage + strList;
        
        System.debug('\n\n ******** El mensaje de validación es: ' + strMessage);
        
        strMessage = strMessage.subString(0, strMessage.length() - 2);
        
        System.debug('\n\n ******** El mensaje de validación es: ' + strMessage);
      }
    }
    
    return strMessage;
  }
  
  public String validaMulta()
  {
    String strMessage = '';
    
    System.debug('\n\n ******** El mensaje de validación es: ' + strMessage);
    
    //Recorremos la lista de MS para saber cuales fueron seleccionadas y pertenecen a un paquete
    for(WrapperMS objWMS : lstMS)
    {
      if(objWMS.seleccionado && objWMS.valorMulta != null){
        strMessage += '\n' + objWMS.ms.Name + ': ' + objWMS.valorMulta;
      }
    }
    
    if(!strMessage.equals(''))
    {
      strMessage = label.BI_COL_lblInformaci_nMultas + strMessage;
    }
    
    System.debug('\n\n ******** El mensaje de validación es: ' + strMessage);
    
    return strMessage;
  }
  
  
  /*@Future (callout=true)
  public static void envioDatosSISGOT(List<String> lstIdMS)
  {
    List<BI_COL_Modificacion_de_Servicio__c> lstMS = [select Id, Name, BI_COL_Codigo_unico_servicio__r.Name,BI_COL_Estado_orden_trabajo__c
                        from BI_COL_Modificacion_de_Servicio__c 
                        where id in :lstIdMS];
    try
    {
      //Ejecutar Servicio Web de OT
      InterfazSISGOTR.EnviarDatosBasicosDS(lstMS);
    }catch(Exception e){
      System.debug(':: Se presentó un error dando de baja las modiificaciones de servicio: '+e.getMessage());
    }
  }*/
  
  
  
  /**Clase Wrapper que administra la selección de registros*/
  public class WrapperMS{
    public boolean seleccionado {get;set;}
    public Integer diasServicio {get;set;}
    public Double valorMulta{get; set;}
    public BI_COL_Modificacion_de_Servicio__c ms {get;set;}
    
    public WrapperMS(BI_COL_Modificacion_de_Servicio__c ms, boolean seleccionado){
      this.ms = ms;
      this.seleccionado = seleccionado;
    }
  }
    
  public void getValueCalsificacionService()
  {
     User us=[select id,BI_Permisos__c from user where id =:userinfo.getUserId() limit 1];
     ls= new list<SelectOption>();
      ls.add(new SelectOption('SUSPENSION TEMPORAL','SUSPENSION TEMPORAL'));
      ls.add(new SelectOption('CAMBIO CUENTA','CAMBIO CUENTA'));
      ls.add(new SelectOption('CAMBIO DE NUMERO','CAMBIO DE NUMERO'));
      ls.add(new SelectOption('SERVICIO INGENIERIA','SERVICIO INGENIERIA'));
      ls.add(new SelectOption('ALTA POR NORMALIZACION','ALTA POR NORMALIZACION'));
    if(us.BI_Permisos__c=='Jefe de Ventas' || us.BI_Permisos__c=='Ejecutivo de Cobros')
    {
      
      ls.add(new SelectOption('BAJA','BAJA'));
    }
  }
}