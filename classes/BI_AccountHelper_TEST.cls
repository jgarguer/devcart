@isTest 
private class BI_AccountHelper_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_AccountHelper class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    08/09/2014              Ignacio Llorca          Initial Version
    14/01/2016              Fernando Arteaga        BI_EN - CSB Integration
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_AccountHelper.getAccounts
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    08/09/2014              Ignacio Llorca          Initial Version
    14/01/2016              Fernando Arteaga        BI_EN - CSB Integration
    25/09/2017              Jaime Regidor           Fields BI_Subsector__c and BI_Sector__c added

    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getAccounts_Test() {
       
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
       //INSERT DATA
       
        Id idProfile =BI_DataLoad.searchAdminProfile();
            
        List<User> lst_user = BI_DataLoad.loadUsers(1, idProfile, Label.BI_Administrador);
        
        system.runAs(lst_user[0]){
            
           List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(3);
                  
           List<Account> lst_acc = new List<Account>();
           for(Integer j = 0; j < 3; j++){
                Account acc = new Account(Name = 'test'+ String.valueOf(j),
                                          BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                          BI_Activo__c = Label.BI_Si,
                                          BI_Country__c = lst_pais[j],
                                          BI_Segment__c = 'test',
                                          BI_Subsegment_Regional__c = 'test',
                                          BI_Territory__c = 'test');
                
                lst_acc.add(acc);
           }
           
           insert lst_acc;
        
            List<Contact> lst_contacts = BI_DataLoad.loadContacts(5, lst_acc);
        
            List<BI_Contact_Customer_Portal__c> lst_ccp = new List<BI_Contact_Customer_Portal__c>();

        
            Test.startTest();
            
            //Throw exception
            BI_TestUtils.throw_exception = true; // FAR 14/01/2016
            List<Account> result = BI_AccountHelper.getAccounts();
            system.assertEquals(result, null);
            
            
            //NO DATA
            BI_TestUtils.throw_exception = false;
            system.assertEquals(BI_AccountHelper.getAccounts().isEmpty(), true);
            
            //OK
            for(Contact cont :lst_contacts){
                system.debug('cont.AccountId'+ cont.AccountId);
                BI_Contact_Customer_Portal__c ccp = new BI_Contact_Customer_Portal__c(
                                          BI_Activo__c = true,
                                          BI_Contacto__c = cont.Id,
                                          BI_Cliente__c = cont.AccountId,
                                          BI_User__c = UserInfo.getUserId(),
                                          BI_Perfil__c = Label.BI_Apoderado);
                
                lst_ccp.add(ccp);
            }
            insert lst_ccp;
            
            system.assertEquals(BI_AccountHelper.getAccounts().isEmpty(), false);
            
            Test.stopTest();
             
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for BI_AccountHelper get methods
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    15/09/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
     static testMethod void getCurrentAccount_Test() {
        
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        List<Profile> prof = [select Id from Profile where Name = 'BI_Administrator' limit 1];

        User usertest = new User(alias = 'cctest',
                          email = 'cctest@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof[0].Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'cctest@testorg.com');
        
        insert usertest;
        
        System.runAs(usertest)
        {
	        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	              
	        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
	        
	        Test.startTest();       
	        
	        BI_TestUtils.throw_exception = true; // FAR 14/01/2016
	        Account accN = BI_AccountHelper.getCurrentAccount(null);
	        
	        BI_TestUtils.throw_exception = true; // FAR 14/01/2016
	        String countryF = BI_AccountHelper.getCountryAccount(null);
	        
	        BI_TestUtils.throw_exception = false;
        
          if(lst_acc[0] != null) {
            Account acc = BI_AccountHelper.getCurrentAccount(lst_acc[0].Id);
            system.assertEquals((acc.Owner.Name != ''), true);
          }
	        
	        //Id idAcc =  BI_AccountHelper.getCurrentAccountId();
	        BI_TestUtils.throw_exception = true; // FAR 14/01/2016
	        system.assertEquals(BI_AccountHelper.getCurrentAccountId(), null);
	
	        BI_TestUtils.throw_exception = false; // FAR 14/01/2016
	        system.assertEquals(BI_AccountHelper.getCurrentAccountId(), null);
	        
	        //String country = BI_AccountHelper.getCountryAccount(lst_acc[0].Id);
          if(lst_acc[0] != null) {
	         system.assertEquals(BI_AccountHelper.getCountryAccount(lst_acc[0].Id), lst_pais[0]);
          }
        }
        
        Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julian 
    Company:       Accenture
    Description:   Method that updates the 'Cobertura de Oferta Digital' KPI on Account
    
    History:
    
    <Date>            <Author>              <Description>
    16/02/2017        Gawron, Julian      Initial version D398
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   static testMethod void updateOfertaDigital_TEST(){

         BI_TestUtils.throw_exception=false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_Configuracion_Acc_KPIS__c kpisCs = new BI_Configuracion_Acc_KPIS__c();
        kpisCs.Dias_Oferta_Digital_Empresas__c = 10;
        insert kpisCs;
    
    
        Account acc = new Account(
          Name = 'Test1ARG', 
          Industry = 'Comercio', 
          CurrencyIsoCode = 'ARS', 
          BI_Activo__c = 'Sí', 
          BI_Cabecera__c = 'Sí',  
          BI_Estado__c = true, 
          BI_Country__c = Label.BI_Argentina, 
          BI_Sector__c = 'Comercio', 
          BI_Segment__c = 'Empresas', 
          BI_Subsector__c = 'Alimentos y Bebidas', 
          BI_Subsegment_Regional__c = 'Corporate', 
          TGS_Es_MNC__c = false, 
          BI_No_Identificador_fiscal__c = '30111111118',
          Sector__c = 'Private'
        );
        insert acc;
            

      Test.startTest();  
        Opportunity opp = new Opportunity(
            Name = 'OppTest1',
            CloseDate = Date.today(),
            StageName = 'F5 - Solution Definition',
            BI_No_requiere_comite_arg__c = true,
            AccountId = acc.Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            BI_No_Identificador_fiscal__c = '30111111118',
            CurrencyIsoCode = 'ARS',
            BI_Licitacion__c = 'No',
            VE_Express__c = true
        );
        insert opp;



        System.debug('updateValPresupuestoOrder test opp'+ opp.Id);
        NE__Order__c ord = new NE__Order__c(NE__AccountId__c = acc.Id,
                                                NE__BillAccId__c = acc.Id,
                                                NE__OptyId__c = opp.Id,
                                                NE__AssetEnterpriseId__c='testEnterprise',
                                                NE__ServAccId__c = acc.Id,
                                                NE__OrderStatus__c = 'Active',
                                                CurrencyIsoCode = 'ARS');
        insert ord;
        System.debug('updateValPresupuestoOrder test ord'+ ord.Id);

        NE__Product__c pord = new NE__Product__c();
         pord.Offer_SubFamily__c = 'a-test;';
         pord.Offer_Family__c = 'b-test';
         pord.BI_Rama_Digital__c = 'asdfasdf';
        
        insert pord; 

        NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;
        NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id, 
            NE__ProductId__c = pord.Id, 
            NE__Change_Subtype__c = 'test', 
            NE__Disconnect_Subtype__c = 'test');
        insert catIt;
               
        //Insertamos el order item asociado a la order
        NE__OrderItem__c oi = new NE__OrderItem__c(
            NE__OrderId__c = ord.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = pord.Id,
            NE__CatalogItem__c = catIt.Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 1000,
            BI_Ingreso_Recurrente_Anterior_Producto__c = 2000,
            NE__RecurringChargeOv__c = 3000,
            CurrencyISOCode = 'ARS'
        );
        
      
        List<NE__OrderItem__c> lst_itemToInsert = new List<NE__OrderItem__c>();
        lst_itemToInsert.add(oi);

        insert lst_itemToInsert;

    Test.stopTest();


        Account micuenta = [Select id, BI_Ultima_Oportunidad_Oferta_Digital__c, BI_Cobertura_Oferta_Digital__c from Account where Id = :acc.Id];
        System.debug('actualizarAccountCoberturaDigital micuenta' + micuenta);
         System.assertNotEquals(micuenta.BI_Ultima_Oportunidad_Oferta_Digital__c, null);
         System.assertEquals(true , micuenta.BI_Cobertura_Oferta_Digital__c );

  }

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julian 
    Company:       Accenture
    Description:   Method that updates the 'Cobertura de Oferta Digital' KPI on Account
    
    History:
    
    <Date>            <Author>              <Description>
    16/02/2017        Gawron, Julian      Initial version D398
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   static testMethod void updateOfertaDigitalNeg_TEST(){

        BI_TestUtils.throw_exception=false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_Configuracion_Acc_KPIS__c kpisCs = new BI_Configuracion_Acc_KPIS__c();
        kpisCs.Dias_Oferta_Digital_Empresas__c = 10;
        insert kpisCs;
    
    
        Account acc = new Account(
          Name = 'Test1ARG', 
          Industry = 'Comercio', 
          CurrencyIsoCode = 'ARS', 
          BI_Activo__c = 'Sí', 
          BI_Cabecera__c = 'Sí',  
          BI_Estado__c = true, 
          BI_Country__c = Label.BI_Argentina, 
          BI_Sector__c = 'Comercio', 
          BI_Segment__c = 'Empresas', 
          BI_Subsector__c = 'Alimentos y Bebidas', 
          BI_Subsegment_Regional__c = 'Corporate', 
          TGS_Es_MNC__c = false, 
          BI_No_Identificador_fiscal__c = '30111111118',
          Sector__c = 'Private'
        );
        insert acc;
            

      Test.startTest();  
        Opportunity opp = new Opportunity(
            Name = 'OppTest1',
            CloseDate = Date.today(),
            StageName = 'F5 - Solution Definition',
            BI_No_requiere_comite_arg__c = true,
            AccountId = acc.Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            BI_No_Identificador_fiscal__c = '30111111118',
            CurrencyIsoCode = 'ARS',
            BI_Licitacion__c = 'No',
            VE_Express__c = true
        );
        insert opp;



        System.debug('updateValPresupuestoOrder test opp'+ opp.Id);
        NE__Order__c ord = new NE__Order__c(NE__AccountId__c = acc.Id,
                                                NE__BillAccId__c = acc.Id,
                                                NE__OptyId__c = opp.Id,
                                                NE__AssetEnterpriseId__c='testEnterprise',
                                                NE__ServAccId__c = acc.Id,
                                                NE__OrderStatus__c = 'Active',
                                                CurrencyIsoCode = 'ARS');
        insert ord;
        System.debug('updateValPresupuestoOrder test ord'+ ord.Id);

        NE__Product__c pord = new NE__Product__c();
         pord.Offer_SubFamily__c = 'a-test;';
         pord.Offer_Family__c = 'b-test';
         pord.BI_Rama_Digital__c = 'asdfasdf';
        
        insert pord; 

        NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;
        NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id, 
            NE__ProductId__c = pord.Id, 
            NE__Change_Subtype__c = 'test', 
            NE__Disconnect_Subtype__c = 'test');
        insert catIt;
               
        //Insertamos el order item asociado a la order
        NE__OrderItem__c oi = new NE__OrderItem__c(
            NE__OrderId__c = ord.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = pord.Id,
            NE__CatalogItem__c = catIt.Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 1000,
            BI_Ingreso_Recurrente_Anterior_Producto__c = 2000,
            NE__RecurringChargeOv__c = 3000,
            CurrencyISOCode = 'ARS'
        );
        
      
        List<NE__OrderItem__c> lst_itemToInsert = new List<NE__OrderItem__c>();
        lst_itemToInsert.add(oi);

        insert lst_itemToInsert;


      
        ord.NE__OrderStatus__c = 'No Activo';
        System.debug('actualizarAccountCoberturaDigital noActiv');
        update ord;

    Test.stopTest();

        Account micuenta = [Select id, BI_Ultima_Oportunidad_Oferta_Digital__c, BI_Cobertura_Oferta_Digital__c from Account where Id = :acc.Id];
                 System.debug('actualizarAccountCoberturaDigital micuenta' + micuenta);
        System.assertEquals(null, micuenta.BI_Ultima_Oportunidad_Oferta_Digital__c);
        System.assertEquals(false, micuenta.BI_Cobertura_Oferta_Digital__c );


  }





  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Fernández
    Company:       Accenture
    Description:   Test method to manage the code coverage for BI_AccountHelper.updateOfertaVisita method
    
    History:
    
    <Date>            <Author>             <Description>
    20/02/2017        Alberto Fernández    Initial version D398
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static testMethod void updateOfertaVisita_TEST() { 

      TGS_User_Org__c userTGS = new TGS_User_Org__c();
      userTGS.TGS_Is_BI_EN__c = true;
      insert userTGS;

      BI_Configuracion_Acc_KPIS__c kpisCs = new BI_Configuracion_Acc_KPIS__c();
      kpisCs.Dias_Visita_Empresa__c = 10;
      insert kpisCs;
        
      //Comprobamos que se rellena BI_Ultima_Oportunidad_Visita__c en Cliente, para la creacion de nuevos Eventos del cliente
      Account testAccount = new Account(Name = 'testAcc', BI_Segment__c = 'Empresas',
                                        BI_Subsector__c = 'test', //25/09/2017
                                        BI_Sector__c = 'test', //25/09/2017
                                        BI_Subsegment_Regional__c = 'test',
                                        BI_Territory__c = 'test');
      insert testAccount;

      DateTime dtNow = DateTime.now();

      Event testEvent = new Event(Subject = 'Visita a Cliente', 
                                  Description = 'test description', 
                                  BI_Temas_tratados__c = 'test',
                                  WhatId = testAccount.Id,
                                  BI_FVI_Estado__c = 'Cualificada',
                                  StartDateTime = dtNow, 
                                  EndDateTime = dtNow, 
                                  BI_FVI_FechaHoraFinVisita__c = dtNow.date());

      Test.startTest();
      insert testEvent;

      //Comprobamos que BI_Ultima_Oportunidad_Visita__c se pone a null, cuando el evento pasa a pertenecer a otro Cliente distinto
      Account testAccount2 = new Account(Name = 'testAcc2', BI_Segment__c = 'Empresas',
                                          BI_Subsector__c = 'test', //25/09/2017
                                          BI_Sector__c = 'test', //25/09/2017
                                          BI_Subsegment_Regional__c = 'test',
                                          BI_Territory__c = 'test');
      insert testAccount2;

      testEvent.WhatId = testAccount2.Id;
      update testEvent;

      Test.stopTest();


      List<Account> resultAcc = [SELECT Id, BI_Ultima_Oportunidad_Visita__c, BI_Cobertura_Visita__c FROM Account WHERE Id = :testAccount.Id];
      System.assertEquals(resultAcc.get(0).BI_Ultima_Oportunidad_Visita__c, null);
      System.assertEquals(resultAcc.get(0).BI_Cobertura_Visita__c, false);
        
    }  

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Fernández
    Company:       Accenture
    Description:   Test method to manage the code coverage for BI_AccountHelper.updateOferta method
    
    History:
    
    <Date>            <Author>             <Description>
    20/02/2017        Alberto Fernández    Initial version D398
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static testMethod void updateOferta_TEST() {

      TGS_User_Org__c userTGS = new TGS_User_Org__c();
      userTGS.TGS_Is_BI_EN__c = true;
      insert userTGS;

      BI_Configuracion_Acc_KPIS__c kpisCs = new BI_Configuracion_Acc_KPIS__c();
      kpisCs.Dias_Oferta_Empresas__c = 10;
      insert kpisCs;

      //Comprobamos que se rellena BI_Ultima_Oportunidad_Oferta__c en Cliente, para la creacion de nuevas Oportunidades del cliente
      Account testAccount = new Account(Name = 'testAcc', BI_Country__c = 'Argentina',  BI_Segment__c = 'Empresas',
                                          BI_Subsector__c = 'test', //25/09/2017
                                          BI_Sector__c = 'test', //25/09/2017
                                          BI_Subsegment_Regional__c = 'test',
                                          BI_Territory__c = 'test');
      insert testAccount;

      DateTime dtNow = DateTime.now();
      Test.startTest();
      Opportunity testOpp = new Opportunity(BI_No_Identificador_fiscal__c = '00000000C',
                                            Name = 'testOpp', 
                                            BI_Country__c = 'Argentina', 
                                            BI_SIMP_Opportunity_Type__c = 'Alta', 
                                            CloseDate = Date.today(), 
                                            BI_Probabilidad_de_exito__c = '100', 
                                            CurrencyIsoCode = 'EUR', 
                                            BI_Duracion_del_contrato_Meses__c = 12, 
                                            BI_Plazo_estimado_de_provision_dias__c = 90, 
                                            StageName = Label.BI_DefinicionSolucion,
                                            AccountId = testAccount.Id);
      insert testOpp;

      Test.stopTest();
 
      List<Account> resultAcc = [SELECT Id, BI_Ultima_Oportunidad_Oferta__c, BI_Cobertura_Oferta__c FROM Account WHERE Id = :testAccount.Id];

      System.assertEquals(resultAcc.get(0).BI_Ultima_Oportunidad_Oferta__c, dtNow.date());
      System.assertEquals(resultAcc.get(0).BI_Cobertura_Oferta__c, true);


    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Alberto Fernández
    Company:       Accenture
    Description:   Test method to manage the code coverage for BI_AccountHelper.updateOferta method
    
    History:
    
    <Date>            <Author>             <Description>
    20/02/2017        Alberto Fernández    Initial version D398
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static testMethod void updateOfertaNegativa_TEST() {

      BI_TestUtils.throw_exception=false;

      TGS_User_Org__c userTGS = new TGS_User_Org__c();
      userTGS.TGS_Is_BI_EN__c = true;
      insert userTGS;

      BI_Configuracion_Acc_KPIS__c kpisCs = new BI_Configuracion_Acc_KPIS__c();
      kpisCs.Dias_Oferta_Empresas__c = 10;
      insert kpisCs;

      Account testAccount = new Account(Name = 'testAcc', BI_Country__c = 'Argentina', BI_Segment__c = 'Empresas',
                                          BI_Subsector__c = 'test', //25/09/2017
                                          BI_Sector__c = 'test', //25/09/2017
                                          BI_Subsegment_Regional__c = 'test',
                                          BI_Territory__c = 'test');
      insert testAccount;

      DateTime dtNow = DateTime.now();
      Test.startTest();
      Opportunity testOpp = new Opportunity(BI_No_Identificador_fiscal__c = '00000000C',
                                            Name = 'testOpp', 
                                            BI_Country__c = 'Argentina', 
                                            BI_SIMP_Opportunity_Type__c = 'Alta', 
                                            CloseDate = Date.today(), 
                                            BI_Probabilidad_de_exito__c = '100', 
                                            CurrencyIsoCode = 'EUR', 
                                            BI_Duracion_del_contrato_Meses__c = 12, 
                                            BI_Plazo_estimado_de_provision_dias__c = 90, 
                                            StageName = Label.BI_DefinicionSolucion,
                                            AccountId = testAccount.Id);
      insert testOpp;

      //Comprobamos que BI_Ultima_Oportunidad_Oferta__c se pone a null, cuando la cuenta deja de tener alguna oportunidad que cumpla el KPI
      testOpp.StageName = Label.BI_F1CanceladaSusp;
      testOpp.BI_Motivo_de_perdida__c = 'Desistimiento cliente';
      update testOpp;

      List<Account> resultAcc2 = [SELECT Id, BI_Ultima_Oportunidad_Oferta__c, BI_Cobertura_Oferta__c FROM Account WHERE Id = :testAccount.Id];
      System.assertEquals(resultAcc2.get(0).BI_Ultima_Oportunidad_Oferta__c, null);
      System.assertEquals(resultAcc2.get(0).BI_Cobertura_Oferta__c, false);

      Test.stopTest();

    }



/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture
    Description:   Test method to manage the code coverage for BI_AccountHelper.prepareHierarchy method
    
    History:
    
    <Date>            <Author>             <Description>
    02/10/2017        Guillermo Muñoz      Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void prepareHierarchy_TEST(){

        BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();

        TGS_Dummy_Test_Data.insertFullStackData();

        List <Account> lst_acc = [SELECT Id, RecordType.DeveloperName FROM Account];

        System.assertEquals(lst_acc.size(), 6);

        Set <Id> set_idHolding = new Set <Id>();
        Set <Id> set_idCustCountry = new Set <Id>();
        Set <Id> set_idLegalEntity = new Set <Id>();

        for (Account acc : lst_acc){

            if(acc.Recordtype.DeveloperName == Constants.RECORD_TYPE_TGS_HOLDING){
                set_idHolding.add(acc.Id);
            }
            else if(acc.Recordtype.DeveloperName == Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY){
                set_idCustCountry.add(acc.Id);
            }
            else if(acc.Recordtype.DeveloperName == Constants.RECORD_TYPE_TGS_LEGAL_ENTITY){
                set_idLegalEntity.add(acc.Id);
            }
        }

        System.assertEquals(set_idHolding.size(), 2);
        System.assertEquals(set_idCustCountry.size(), 2);
        System.assertEquals(set_idLegalEntity.size(), 2);

        Test.startTest();

        BI_AccountHelper.prepareHierarchy(set_idHolding);
        BI_AccountHelper.prepareHierarchy(set_idCustCountry);
        BI_AccountHelper.prepareHierarchy(set_idLegalEntity);

        Test.stopTest();
    }
}