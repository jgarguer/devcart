/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Jesús Martínez
Company:        Everis España
Description:    Test para la Clase BI_GD_createContract (creacion de contratos Argentina)

History:

<Date>                      <Author>                        <Change Description>
26/10/2016                  Jesús Martínez               	Versión Inicial.
02/11/2016					Miguel Girón					Añadida compatibilidad con XML Req/Resp
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
//(SeeAllData=true)
public class BI_GD_createContract_Test {
    
    public static String endpoint = '***';
    
     public static Map<String, String> mapaXML = new Map<String, String>();
    
    /* @isTest static void createContractTest_Create_Licitacion() { 
        BI_GD_Helper.authCliente=true;
        BI_GD_createContract.flagCrash = false;
        BI_GD_createContract_Mock.error = false;
        
        Contract contract = BI_GD_Helper.generateLicitacion();
        contract = BI_GD_Helper.getContract(contract.id); 
        test.startTest();
        mapaXML = BI_GD_conversorContract.mapaLicitacion(contract);
        HttpResponse response = BI_GD_createContract.createContract(mapaXML, endpoint, null, 'POST');
        
        BI_GD_CalloutManager.sincronizar(contract.id);
        Test.stopTest();
    } 
    
    @isTest static void BI_GD_CalloutManagerTest2() {
        // Contrato licitacion
        test.startTest();
        BI_GD_CalloutManager.sincronizar('80026000002akF9');
        Test.stopTest();
    }
    @isTest static void BI_GD_CalloutManagerTest() { // pasa oK
        test.startTest();
        BI_GD_CalloutManager.sincronizar('80026000002akFE');
        Test.stopTest();
    }
    
      @isTest static void BI_GD_testLicitacion() { // pasa oK
        test.startTest();
        BI_GD_CalloutManager.sincronizar('80026000002b8rN');
        Test.stopTest();
    }
    
     @isTest static void BI_GD_testLicitacio() { // pasa oK
        test.startTest();
        BI_GD_CalloutManager.sincronizar('80026000002b5uN');
        Test.stopTest();
    }
    
    
    @isTest static void generateContact() { // campos vacios 
        test.startTest();
        // etapa incorrecta licitacion 
        Contract contract = BI_GD_Helper.generateContrato();
        BI_GD_CalloutManager.sincronizar(contract.id);
        Test.stopTest();
    }
    
    @isTest static void generateContact2() { // campos vacios 
        test.startTest();       
        BI_GD_CalloutManager.sincronizar('80026000002axpC');
        Test.stopTest();
    }
    
    @isTest static void BI_GD_CalloutManagerTest3() {
        // Contrato caida
        test.startTest();
        BI_GD_createContract.caida = true;
        BI_GD_CalloutManager.sincronizar('80026000002akF9');
        Test.stopTest();
    }
    @isTest static void BI_GD_CalloutManagerTest4() { // pasa licitacion error 
        test.startTest();
        BI_GD_createContract.caidaContract = true;
        BI_GD_CalloutManager.sincronizar('80026000002akFE');
        Test.stopTest();
    }
    
    @isTest static void BI_GD_AutkMock2(){
        BI_GD_AuthMock Am = new BI_GD_AuthMock();
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint('***');
        Am.authCliente=false;
        Am.respond(request);
        
    }
    //request.getHeader('Authorization'), request.getHeader('Content-Type')
    
    @isTest static void BI_GD_createContract(){
        BI_GD_createContract_Mock CC = new BI_GD_createContract_Mock();
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint('***');
        request.getHeader('TokenDemo');
        request.getHeader('application/xml; charset=UTF-8');
        BI_GD_createContract_Mock.error=true;
        //=false;
        CC.respond(request);
        
    }
       @isTest static void BI_GD_createContract2(){
      
        test.startTest();
        BI_GD_createContract_Mock.error=true;
        BI_GD_CalloutManager.sincronizar('80026000002akF9');
        Test.stopTest();
    }*/
    /*
     @isTest static void BI_GD_createContract2(){
      
        test.startTest();
        BI_GD_CalloutManager.sincronizar('80026000002bCa8AAE');
        Test.stopTest();
    }*/
    
   
    @isTest static void BI_GD_createContract3(){
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario =  BI_GD_createContract_Test.crearRole('rol 0');
            insert rolUsuario;
            
            unUsuario = BI_GD_createContract_Test.crearUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
      
        test.startTest();
       	
        BI_RestWrapper.ContractRequestType contract= new BI_RestWrapper.ContractRequestType();
		
        String method = 'PUT';            
        contract.correlationId = 'asdasd';
        
        System.RunAs(unUsuario){
        
        BI_GD_Config__c mc = new BI_GD_Config__c(BI_GD_lang__c = 'ES', BI_GD_entity__c ='Salesforce', BI_GD_system__c = 'CRM', BI_GD_subsystem__c = 'GestorDocumental', BI_GD_userId__c ='te999999', BI_GD_execId__c ='550e8400-e29b-41d4-a716-446655440001',
                             BI_GD_msgId__c = '550e8400-e29b-41d4-a716-446655440001', BI_GD_clientid__c ='7111df3b-d54a-4e5a-8854-c1eae2acad4f',BI_GD_country__c ='AR',
                             BI_GD_endPoint__c = '/telefonica/sandbox/contractInfo/v2/contracts', BI_GD_nameCertif__c = 'SelfSignedCert_08May2017_ARG');
                insert mc;
        BI_GD_createContract.createContract(contract, method);
            
        } 
        
        Test.stopTest();
    }
    
    

    private static User crearUser (string name, id roleId, id profileId){
        
            User u =  new User( 
            Alias = name,
            Email= name +'@roletest1.com',
            UserRoleId = roleId, 
            LocalesIdKey='en_US', 
            EmailEncodingKey='UTF-8',
            Lastname='Testing', 
            LanguageLocaleKey='en_US', 
            ProfileId = profileId, 
            TimezonesIdKey='America/Los_Angeles',
            FirstName = name,
            UserName= name +'@roletest1.com', 
            BI_Permisos__c = 'TGS', 
            isActive = true,
            BI_CodigoUsuario__c= 'str'
        );
        
        return u;
    }
    
    private static UserRole crearRole(String name){
                UserRole r = new UserRole(Name = name);
        return r;
    }
    
}