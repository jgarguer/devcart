@isTest
private class PCA_DocumentManagementController_TEST {

    static testMethod void loadInfo() {
    	User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        //insert usr;
        system.debug('usr: '+usr);
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;            
            Event Evento1;
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            system.debug('con: '+con);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
            system.runAs(user1){
            	PCA_DocumentManagementController constructor = new PCA_DocumentManagementController();
                 BI_TestUtils.throw_exception = false;
            	 constructor.checkPermissions();
            }
            system.runAs(user1){
            	PCA_DocumentManagementController constructor = new PCA_DocumentManagementController();
                 BI_TestUtils.throw_exception = true;
            	constructor.checkPermissions();
            }
        }
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}