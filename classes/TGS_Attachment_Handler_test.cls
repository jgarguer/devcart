@IsTest
public class TGS_Attachment_Handler_test {

    static testMethod void attachFile() {

        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            System.debug('TGS_Attachment_Handler_test:inserto UserOrg');
            insert uO;   
        }
        
        
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c = 'TGS';
        System.debug('TGS_Attachment_Handler_test:inserto User');
        insert u;
        
        System.runAs(u) {
            TGS_Dummy_Test_Data.dummyEndpointsTGS();
            dummyCaseWithAttachments();
        }    
    }


    public static Case dummyCaseWithAttachments() {
        
        Contact CaseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
        insert CaseContact;

        // Level 1: Holding
        Id recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
        Account holding = new Account(                     
            Name = 'Dummy holding',
            RecordTypeId = recordtypeId,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True);
        insert holding;
        
        // Level 2: Customer Country
        recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY);
        Account customerCountry = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Customer_Country + String.valueOf(Math.random()),
            RecordTypeId = recordtypeId,
            BI_Country__c = 'Spain', // JMF 19/10/2016 - Add Country
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holding.Id,
            ParentId = holding.Id);
        insert customerCountry;
        
        // Level 3: Legal Entity
        recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_LEGAL_ENTITY);
        Account legalEntity = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Legal_Entity + String.valueOf(Math.random()),
            RecordTypeId = recordtypeId,
            ParentId = customerCountry.Id,
            BI_Country__c = 'Spain',
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holding.Id);
        insert legalEntity;
        
        // Level 4: Business Unit
        recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_BUSINESS_UNIT);
        Account businessUnit = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Business_Unit + String.valueOf(Math.random()),
            RecordTypeId = recordtypeId,
            ParentId = legalEntity.Id,
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holding.Id);
        insert businessUnit;

        // Level 5: Cost Center
        recordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_COST_CENTER);
        Account costCenter = new Account(                     
            Name = 'Account Test Name - ' + Label.TGS_Cost_Center + String.valueOf(Math.random()),
            RecordTypeId = recordtypeId,
            ParentId = businessUnit.Id,
            BI_Segment__c = 'Empresas',
            BI_Subsegment_Regional__c='Corporate',
            TGS_Es_MNC__c = True,
            TGS_Aux_Holding__c = holding.Id,
            TGS_Aux_Business_Unit__c = businessUnit.Id,
            TGS_Aux_Customer_Country__c = customerCountry.Id,
            TGS_Aux_Legal_Entity__c = legalEntity.Id
            );
        insert costCenter;

        TGS_Tier_Dependence__c tr_CustomSetting = new TGS_Tier_Dependence__c();
        tr_CustomSetting.Name = 'Test CS Record';
        tr_CustomSetting.TGS_Holding_Name__c = 'Dummy holding';
        tr_CustomSetting.TGS_RTDevName__c = 'TGS_Change';
        tr_CustomSetting.TGS_Product_Tier_1__c = 'Dummy tier 1';
        tr_CustomSetting.TGS_Product_Tier_2__c = 'Dummy tier 2';
        tr_CustomSetting.TGS_Product_Tier_3__c = 'Dummy tier 3';
        insert tr_CustomSetting;


        Case TestCase = TGS_Dummy_Test_Data.dummyCaseTGS(Constants.RECORD_TYPE_TGS_CHANGE, 'On Behalf');
        TestCase.ContactId = CaseContact.id;
        TestCase.AccountId = legalEntity.Id;
        TestCase.TGS_Product_Tier_1__c = 'Dummy tier 1';
        TestCase.TGS_Product_Tier_2__c = 'Dummy tier 2';
        TestCase.TGS_Product_Tier_3__c = 'Dummy tier 3';

        Test.startTest();
            insert TestCase;
        Test.stopTest();
        
        TGS_Work_Info__c workInfoTest = new TGS_Work_Info__c(TGS_Case__c = TestCase.Id , TGS_Description__c = 'Test controller description', TGS_Public__c = false);
        insert workInfoTest;
        try{
            Attachment attachmentTest = new Attachment(Name = 'Test Attachment', Body = Blob.valueOf('Test Body Attachment'), Description = 'Test Description Attachment', ParentId = workInfoTest.Id);
            insert attachmentTest;
            Attachment attachmentTest2 = new Attachment(Name = 'Test Attachment2', Body = Blob.valueOf('Test Body Attachment'), Description = 'Test Description Attachment', ParentId = workInfoTest.Id);
            insert attachmentTest2;
            Attachment attachmentTest3 = new Attachment(Name = 'Test Attachment3', Body = Blob.valueOf('Test Body Attachment'), Description = 'Test Description Attachment', ParentId = workInfoTest.Id);
            insert attachmentTest3;
            Attachment attachmentTest4 = new Attachment(Name = 'Test Attachment4', Body = Blob.valueOf('Test Body Attachment'), Description = 'Test Description Attachment', ParentId = workInfoTest.Id);
            insert attachmentTest4;
        }catch(Exception e){
            System.debug('Exception : '+e);
        }
        return TestCase;
    }

}