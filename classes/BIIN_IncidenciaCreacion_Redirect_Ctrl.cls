public with sharing class BIIN_IncidenciaCreacion_Redirect_Ctrl {

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Clase para hacer redirección del layout de Creación de Solicitud Incidencia a BIIN_CreacionIncidencia 
	    
	    History:
	    
	    <Date>            	<Author>        							  	<Description>
	    10/12/2015       Ignacio Morales Rodríguez			    		 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
private ApexPages.StandardController controller;
public String retURL {get; set;}
public String saveNewURL {get; set;}
public String rType {get; set;}
public String cancelURL {get; set;}
public String ent {get; set;}
public String confirmationToken {get; set;}
public String accountID {get; set;}
public String contactID {get; set;}
public string contacto  {get; set;} 
public string contactoCasoHijo  {get; set;}    
public string AccountCasoHijo   {get; set;}  
public String caseId;

String tipoRegistroId = [SELECT Id FROM RecordType WHERE DeveloperName = 'BIIN_Solicitud_Incidencia_Tecnica' limit 1].Id;     

public BIIN_IncidenciaCreacion_Redirect_Ctrl(ApexPages.StandardController controller) {

    this.controller = controller;   
    retURL = ApexPages.currentPage().getParameters().get('retURL');
    rType = ApexPages.currentPage().getParameters().get('RecordType');
    contacto = ApexPages.currentPage().getParameters().get('CaseId');
    cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
    ent = ApexPages.currentPage().getParameters().get('ent');
    confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
    saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
    accountID = ApexPages.currentPage().getParameters().get('def_account_id');
    contactID = ApexPages.currentPage().getParameters().get('def_contact_id');
    tipoRegistroId = tipoRegistroId.substring(0, 15); 
    
    caseId = controller.getId();
}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función para redireccionar a BIIN_CreacionIncidencia
	    
	    History:
	    
	    <Date>            <Author>          			<Description>
	    12/12/2015     Ignacio Morales Rodríguez    	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
    
public PageReference redirect() {
    PageReference returnURL;    
    IF(rType == tipoRegistroId) {
        
        system.debug('Checkk>>>');
        returnURL = new PageReference('/apex/BIIN_CreacionIncidencia');
        returnURL.getParameters().put('contacto', contacto);
        
    }ELSE {
        system.debug('Checkk3>>>' + rType);
        returnURL = new PageReference('/500/e');
        
    }
    returnURL.getParameters().put('retURL', retURL);
    returnURL.getParameters().put('RecordType', rType);
    returnURL.getParameters().put('cancelURL', cancelURL);
    returnURL.getParameters().put('ent', ent);
    returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
    returnURL.getParameters().put('save_new_url', saveNewURL);
    returnURL.getParameters().put('nooverride', '1');

    IF (accountID != null){
        returnURL.getParameters().put('def_account_id', accountID);
    }
    IF (contactID != null){
        returnURL.getParameters().put('def_contact_id', contactID);
    }
    returnURL.setRedirect(true);
    return returnURL;
}
}