@isTest
private  class ADQ_Miscelanea_TEST {
 @isTest static void testClass_Miscelanea (){
	//JG Migracion a UATFIX
	
	List<NotadeCredito__c> listaNotasDeCredito = new List<NotadeCredito__c>();
	 List<NE__OrderItem__c> listaProductosUnicosAsignar = new List<NE__OrderItem__c>();	
	 List<Pedido_PE__c> listaPedidosPE = new List<Pedido_PE__c> ();

			CurrencyTypeFacturacion__c cctypeUSD = new CurrencyTypeFacturacion__c ();
			cctypeUSD.ConversionRate__c = 	0.065005135;
			cctypeUSD.CurrencyIsoCode = 'EUR';
			cctypeUSD.IsActive__c = true;
			cctypeUSD.IsCorporate__c = false;
			cctypeUSD.IsoCode__c = 'USD';
			
			CurrencyTypeFacturacion__c cctypeEUR = new CurrencyTypeFacturacion__c ();
			cctypeEUR.ConversionRate__c = 	0.06004239;
			cctypeEUR.CurrencyIsoCode = 'EUR';
			cctypeEUR.IsActive__c = true;
			cctypeEUR.IsCorporate__c = false;
			cctypeEUR.IsoCode__c = 'EUR';
			
			CurrencyTypeFacturacion__c cctypeMXN = new CurrencyTypeFacturacion__c ();
			cctypeMXN.ConversionRate__c = 	1;
			cctypeMXN.CurrencyIsoCode = 'EUR';
			cctypeMXN.IsActive__c = true;
			cctypeMXN.IsCorporate__c = true;
			cctypeMXN.IsoCode__c = 'MXN';
			
			insert cctypeUSD;
			insert cctypeEUR;
			insert cctypeMXN;
			
			ADQ_divisautility divisautility = new ADQ_divisautility();
			
			String original = 'MXN';
			String target1 = 'MXN';
			Double value = 5;
			Double result = divisautility.transformCurrency(original, target1, value);
			
			String original2 = 'MXN';
			String target2 = 'USD';
			Double value2 = 5;
			Double result2 = divisautility.transformCurrency(original2, target2, value2);
			
			String original3 = 'USD';
			String target3 = 'MXN';
			Double value3 = 5;
			Double result3 = divisautility.transformCurrency(original3, target3, value3);
			
			String original4 = 'USD';
			String target4 = 'EUR';
			Double value4 = 5;
			Double result4 = divisautility.transformCurrency(original4, target4, value4);
			
			Double qnum = 5.98765;
			Double  qdecimal =  2.2;
			Double  roundedDouble =  2.9872;
			Double rounded = divisautility.round(qnum, qdecimal);
			String currencyFormated = divisautility.formatCurrency(rounded);
			String currencyFormated2 = divisautility.formatCurrency(roundedDouble);
			
			Double num = 123.432938;
			Double doubleTruncate = divisautility.truncate(num, 3);

			//GMN 13/06/2017 - Deprecated references to Raz_n_social__c (Optimización)
			Account acc01  = new Account ();
				acc01.Name = 'Cuenta de prueba JMO';
				acc01.BI_Segment__c = 'test';
				acc01.BI_Subsegment_Regional__c = 'test';
				acc01.BI_Territory__c = 'test';
				//acc01.Raz_n_social__c = 'Cuenta de prueba SA CV';
				acc01.BI_Country__c = Label.BI_Mexico;
			insert acc01;
			
			Opportunity opp01 = new Opportunity ();	
				//opp01.BI_Id_de_la_Oportunidad__c = '0067000000ZA5XF';
				opp01.Name = 'ENLACE INTERNET DEDICADO PLANTA';
				opp01.AccountId = acc01.Id;
				opp01.BI_Country__c = Label.BI_Mexico;
				opp01.BI_Ciclo_ventas__c = 'Completo';
				opp01.CloseDate = date.today();
				opp01.StageName = 'F1 - Ganada';
				opp01.CurrencyIsoCode = 'MXN';
				opp01.BI_Duracion_del_contrato_Meses__c = 36;
				opp01.BI_Ingreso_por_unica_vez__c = 0.0;
				opp01.BI_Recurrente_bruto_mensual__c = 13390.00;
				opp01.BI_Opportunity_Type__c = 'Fijo';
				opp01.Amount = 482040;
			insert opp01;	
			
			NE__Order__c NEORDER =  new NE__Order__c(); 
				NEORDER.NE__OptyId__c = opp01.Id;
				NEORDER.NE__AccountId__c = acc01.Id;
				insert NEORDER;
			
						
			NE__OrderItem__c NEInsert2  = new NE__OrderItem__c ();
				NEInsert2.NE__OrderId__c = NEORDER.id; 
				NEInsert2.CurrencyIsoCode = 'USD';
				NEInsert2.NE__OneTimeFeeOv__c = 100;
				NEInsert2.NE__RecurringChargeOv__c = 100;
				NEInsert2.Fecha_de_reasignaci_n_a_factura__c = date.today();
				NEInsert2.NE__Qty__c = 1;
				insert NEInsert2;
			
			listaProductosUnicosAsignar.add (NEInsert2);
			
			Factura__c Factura = new Factura__c ();
				Factura.Activo__c = true;
				Factura.Cliente__c = acc01.Id;
				Factura.Fecha_Inicio__c = Date.today();
				Factura.Fecha_Fin__c = Date.today() + 20;
				Factura.Inicio_de_Facturaci_n__c = Date.today();
				Factura.IVA__c = '16%';
				Factura.Sociedad_Facturadora__c = 'GTM';
				Factura.Requiere_Anexo__c = 'SI';
				RecordType tiporegistro = [SELECT Id FROM RecordType where  SobjectType = 'Factura__c' and DeveloperName  ='Recurrente'];
				Factura.RecordTypeId = tiporegistro.Id;
			insert Factura;
			
				Factura.Sociedad_Facturadora__c = 'PCS';
			update  Factura;
			
			Programacion__c programacion = new Programacion__c ();
				programacion.Factura__c = Factura.Id;
				programacion.CurrencyIsoCode = 'MXN';
				programacion.Sociedad_Facturadora_Real__c = 'GTM';
				programacion.Facturado__c = false;
				programacion.Fecha__c = date.newInstance(2009 ,12, 25);
				programacion.Inicio_del_per_odo__c = Date.today();
				programacion.Fin_del_per_odo__c = Date.today().addMonths(1);
				programacion.Fecha_de_inicio_de_cobro__c = Date.today().addDays(2);
				programacion.Numero_de_factura_SD__c = null;
				programacion.Folio_fiscal__c = null;
			insert programacion;

			Pedido_PE__c  pediodPE01 = new Pedido_PE__c ();
                pediodPE01.Producto_en_Sitio__c = NEInsert2.Id;
                pediodPE01.CurrencyIsoCode = NEInsert2.CurrencyIsoCode;
                pediodPE01.Precio_original_EQS__c = Double.valueOf(''+NEInsert2.NE__RecurringChargeOv__c);
                pediodPE01.Precio_original__c = 3999.98;
                pediodPE01.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                pediodPE01.Precio_convertido__c = 8990.998;
                pediodPE01.Inicio_del_per_odo__c = date.newInstance(2015, 5, 24);
                pediodPE01.Fin_del_per_odo__c = date.newInstance(2015, 5, 31);
    			pediodPE01.Lleva_IEPS__c = false; 
                pediodPE01.IEPS__c = (pediodPE01.Lleva_IEPS__c)? Double.valueOf(''+pediodPE01.Precio_convertido__c)*0.03:0;
                pediodPE01.Programacion__c = programacion.Id;
	        insert pediodPE01;

	        Pedido_PE__c  pediodPE02 = new Pedido_PE__c ();
                pediodPE02.Producto_en_Sitio__c = NEInsert2.Id;
                pediodPE02.CurrencyIsoCode = NEInsert2.CurrencyIsoCode;
                pediodPE02.Precio_original_EQS__c = Double.valueOf(''+NEInsert2.NE__RecurringChargeOv__c);
                pediodPE02.Precio_original__c = 3999.98;
                pediodPE02.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                pediodPE02.Precio_convertido__c = 8990.998;
                pediodPE02.Inicio_del_per_odo__c = date.newInstance(2015, 5, 24);
                pediodPE02.Fin_del_per_odo__c = date.newInstance(2015, 5, 31);
    			pediodPE02.Lleva_IEPS__c = false; 
                pediodPE02.IEPS__c = (pediodPE02.Lleva_IEPS__c)? Double.valueOf(''+pediodPE02.Precio_convertido__c)*0.03:0;
                pediodPE02.Programacion__c = programacion.Id;
	        insert pediodPE02;

	        listaPedidosPE.add(pediodPE01);
	        listaPedidosPE.add(pediodPE02);

	        List<Pedido_PE__c> listaPedidoPE  = new  List<Pedido_PE__c> ();
	        
	        listaPedidoPE.add(pediodPE01);
	        listaPedidoPE.add(pediodPE02);

				
				
			

			Map<String, NotadeCredito__c> mapNotasDeCredito = new  Map<String, NotadeCredito__c> ();				
			NotadeCredito__c ncp = new NotadeCredito__c();
				ncp.Encabezado_de_Facturacion__c = Factura.Id;
				ncp.TipoNota__c = 'Total';
				ncp.Responsable__c = 'Gustavo Arditti';
				ncp.Factura__c = programacion.Id;
				ncp.Fecha_Solicitud__c = Date.today();
				ncp.Motivo_Pedido_del__c = 'M19-Refacturación';
				ncp.Tipo_de_IVA__c = '16%'; 
				ncp.CurrencyIsoCode = 'MXN';
				ncp.Condiciones_de_Pago__c = '30 Días';
				ncp.Refacturada__c = false;
				ncp.Facturado__c = true;
			insert ncp;

			mapNotasDeCredito.put(ncp.Id,ncp);
			
			listaNotasDeCredito.add (ncp);
			
			NotaCredito_OLI__c NColi = new NotaCredito_OLI__c ();
				NColi.NotaCreditoId__c = ncp.Id;
				NColi.ProgramacionId__c = programacion.Id;
				NColi.Lleva_IEPS__c = false;
				NColi.Importe__c = 2000;
				
			insert 	NColi;
						
			Programacion__c programacionMiscelanea = new Programacion__c ();
				programacionMiscelanea.Factura__c = Factura.Id;
				programacionMiscelanea.CurrencyIsoCode = 'MXN';
				programacionMiscelanea.Sociedad_Facturadora_Real__c = 'GTM';
				programacionMiscelanea.Facturado__c = false;
				programacionMiscelanea.Fecha__c = Date.today();
				programacionMiscelanea.Inicio_del_per_odo__c = Date.today();
				programacionMiscelanea.Fin_del_per_odo__c = Date.today().addMonths(1);
				programacionMiscelanea.Fecha_de_inicio_de_cobro__c = Date.today().addDays(2);
				programacionMiscelanea.Numero_de_factura_SD__c = null;
				programacionMiscelanea.Folio_fiscal__c = null;
				programacionMiscelanea.Miscelanea__c = true;
				
			try{
				insert programacionMiscelanea;
			}catch (Exception err){
					
			}
			
						
			Pedido_PE__c  p = new Pedido_PE__c ();
                p.Producto_en_Sitio__c = NEInsert2.Id;
                p.CurrencyIsoCode = NEInsert2.CurrencyIsoCode;
                p.Precio_original_EQS__c = Double.valueOf(''+NEInsert2.NE__RecurringChargeOv__c);
                p.Precio_original__c = 3999.98;
                p.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                p.Precio_convertido__c = 8990.998;
                p.Inicio_del_per_odo__c = date.newInstance(2015, 5, 24);
                p.Fin_del_per_odo__c = date.newInstance(2015, 5, 31);
    			p.Lleva_IEPS__c = false; 
                p.IEPS__c = (p.Lleva_IEPS__c)? Double.valueOf(''+p.Precio_convertido__c)*0.03:0;
                p.Programacion__c = programacionMiscelanea.Id;
	        insert p;
	        //listaPedidosPE.add(p);

	        Pedido_PE__c  p2 = new Pedido_PE__c ();
                p2.Producto_en_Sitio__c = NEInsert2.Id;
                p2.CurrencyIsoCode = NEInsert2.CurrencyIsoCode;
                p2.Precio_original_EQS__c = Double.valueOf(''+NEInsert2.NE__RecurringChargeOv__c);
                p2.Precio_original__c = 3999.98;
                p2.Moneda_de_conversi_n__c = Factura.CurrencyIsoCode ;
                p2.Precio_convertido__c = 8990.998;
                p2.Inicio_del_per_odo__c = date.newInstance(2015, 5, 24);
                p2.Fin_del_per_odo__c = date.newInstance(2015, 5, 31);
    			p2.Lleva_IEPS__c = false; 
                p2.IEPS__c = (p.Lleva_IEPS__c)? Double.valueOf(''+p.Precio_convertido__c)*0.03:0;
                p2.Programacion__c = programacionMiscelanea.Id;
	        insert p2;
	       
	        
	         Factura_OLI__c f = new Factura_OLI__c();
                f.Factura__c = Factura.Id;
               // f.Commercial_Product__c = ne01.NE__ProdId__c; // relacionamos con el producto comercial
                f.Cantidad__c = 1;
                f.CurrencyIsoCode = 'USD';
                f.Descuento__c = 0.2 * 100;
                f.Plazo__c = 36;  
                f.Precio_Unitario__c = 800.80;
                f.Configuration_Item__c = NEInsert2.Id;
                
          	insert f;
          	List<ProgramacionCurrencyType__c> lsinsertMasivo = new  List<ProgramacionCurrencyType__c> ();
          	for (Integer intconta =1 ; intconta<=200 ; intconta++){	
          	ProgramacionCurrencyType__c pct = new ProgramacionCurrencyType__c();
				pct.Programaci_n__c = programacionMiscelanea.Id;
				pct.IsoCode__c = programacion.CurrencyIsoCode;
				pct.IsCorporate__c = true; 
				pct.ConversionRate__c = (1/15.2656);
			//insert 	pct;
			lsinsertMasivo.add (pct);
			}	

			insert lsinsertMasivo;
			

			delete p2;
		
		
		
        ADQ_miscelanea fmc = new ADQ_miscelanea ();
        ADQ_miscelanea.WrapperPedidoPE wrappePE = new ADQ_miscelanea.WrapperPedidoPE ('EQS00000001','PROD00000001',Factura.Id,200.20, 'USD', NEInsert2.Id,200.30);  
        ADQ_miscelanea.WrapperProductos wrapperPRD = new ADQ_miscelanea.WrapperProductos (NEInsert2, false);
        ADQ_miscelanea.WrapperProductos wrapperPRD2 = new ADQ_miscelanea.WrapperProductos (NEInsert2, true);
        ADQ_miscelanea.WrapperNotaCreditoOLI wppNOLI = new ADQ_miscelanea.WrapperNotaCreditoOLI (NColi );
         
        List<ADQ_miscelanea.WrapperProductos> listaWrapperProductos = new List<ADQ_miscelanea.WrapperProductos>();
        listaWrapperProductos.add(wrapperPRD);
        listaWrapperProductos.add(wrapperPRD2);
        
        
        fmc.listaPedidoPE = listaPedidosPE;
        fmc.listaProductosUnicosAsignar =  listaProductosUnicosAsignar;
        fmc.listaNotasDeCredito = null;
        fmc.listaNotasDeCredito = listaNotasDeCredito;
		fmc.listaPedidosPE = listaPedidosPE;
		
        fmc.creaMapaNotasDeCredito ();
        fmc.mapNotasDeCredito = mapNotasDeCredito;

        fmc.getSelectOptionsNC();
        fmc.getListaNotasDeCredito();
        fmc.getListaConceptosProgramacion();
        fmc.getListaConceptosProgramacionPE();
        fmc.recargaSeccion();
        //fmc.getListaSitiosNext();
       // fmc.filtroOpciones();
        fmc.listaWrapperProductos = listaWrapperProductos; 
        fmc.generaFacturaMiscelanea();
        fmc.getListaProgramacionesGeneradas();
        fmc.creaSetIdsCargosSeleccionados ();
        //fmc.asignaNuevosProductos ();
        fmc.listaProductosUnicosAsignar =  listaProductosUnicosAsignar;
        
        fmc.creaListaPedidosPE ();
        fmc.creaListaPedidos ();
        fmc.calculaImporte ();
      
        fmc.guardar();
        //fmc.generarListasProductos (); 
        
        //fmc.Prev();
        //fmc.Next();
        fmc.getLabelTabla();
        /*
        fmc.getListaMonedas();
        fmc.getListaOportunidades();
        fmc.getListaProductos();
       */
        fmc.getTotalRegistros();
        fmc.getShowprev();
        fmc.getShownext();
        fmc.getMapMonedas();
        fmc.getMapOportunidades();
        fmc.getMapProductos();
      
        fmc.getListaWrapperProductos();
       
        fmc.cancelar();

        fmc.ADQ_DivisaUtilitytest();
			
			String original6 = 'MXN';
			String target16 = 'MXN';
			Double value6 = 5;
			Double result6 = fmc.transformCurrencytest(original, target16, value6);
			
			String original26 = 'MXN';
			String target26 = 'USD';
			Double value26 = 5;
			Double result26 = fmc.transformCurrencytest(original2, target26, value26);
			
			String original36 = 'USD';
			String target36 = 'MXN';
			Double value36 = 5;
			Double result36 = fmc.transformCurrencytest(original3, target36, value36);
			
			String original46 = 'USD';
			String target46 = 'EUR';
			Double value46 = 5;
			Double result46 = fmc.transformCurrencytest(original4, target46, value46);
			
			Double qnum6 = 5.98765;
			Double  qdecimal6 =  2.2;
			Double  roundedDouble6 =  2.9872;
			Double rounded6 = fmc.roundtest(qnum, qdecimal);
			String currencyFormated6 = fmc.formatCurrencytest(rounded6);
			String currencyFormated26 = fmc.formatCurrencytest(roundedDouble6);
			
			Double num6 = 123.432938;
			Double doubleTruncate6 = fmc.truncatetest(num6, 3);

     
        
 }
}