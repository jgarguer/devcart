/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Creates an BI_O4_Approval__c record (with record type based on url parameter) prepopulating some fields. 
 Test Class:    BI_O4_NewGateController_TEST
 
 History:
  
 <Date>              <Author>                   <Change Description>
 28/07/2016          Fernando Arteaga           Initial Version
 07/11/2016          Pedro Párraga              Changed REST API calls for BI_DynamicFieldId functionality on newApproval()
 10/07/2017          Javier Almirón             Some fields are populated (through URL) from Proposal to Approval.
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_NewGateController
{
	public Quote quote {get; set;}
	private String gate {get; set;}
	
	private static final Map<String, RecordType> mapRecordTypes;
	static {
		mapRecordTypes = new Map<String, RecordType>();
		for(RecordType rt : [SELECT Id, DeveloperName
							 FROM RecordType
							 WHERE SObjectType = 'BI_O4_Approvals__c']) {
			mapRecordTypes.put(rt.DeveloperName, rt);
		}
	}
	
	// Constructor
    public BI_O4_NewGateController (ApexPages.standardController controller)
    {
    	quote = (Quote) controller.getRecord();
    	gate = ApexPages.currentPage().getParameters().get('gate');
    }
    
    // Action method called from visualforce page
    public PageReference newApproval()
    {
		if(gate == '3' || gate == '2'){

			List <String> recordTypes = new List <String>{'BI_O4_Gate_' + gate, 'BI_O4_Gate_' + gate + '_Closed'};

			List <BI_O4_Approvals__c> currentGate = [SELECT Id FROM BI_O4_Approvals__c WHERE BI_O4_Proposal__c =: quote.Id AND RecordType.DeveloperName IN: recordTypes AND BI_O4_Gate_status__c NOT IN ('GO', 'Conditional Go') ORDER BY CreatedDate DESC];

			if(!currentGate.isEmpty()){

				return new PageReference('/' + currentGate[0].Id + '/e?retURL=%2F' + quote.Id +'&saveURL=' + quote.Id);
			}
		}
		
		Organization org = [SELECT InstanceName, IsSandbox
							FROM Organization];
		String baseName = System.URL.getSalesforceBaseUrl().toExternalForm().replace('visual.force.com', 'my.salesforce.com').replace('--c.', '.').replace('--e.', '.') + '/services/data/v37.0/tooling/';
		baseName = !org.IsSandbox ? baseName.replace('.' + org.InstanceName.toLowerCase(), '') : baseName;
		
		Id rtId = Schema.SObjectType.BI_O4_Approvals__c.getRecordTypeInfosByName().get('Gate 3').getRecordTypeId();

		Map<String, String> mapFieldId = BI_DynamicFieldId.getDynamicField('BI_O4_Approvals__c', 
																			new Set<String>{'BI_O4_Opportunity__c', 'BI_O4_Proposal__c',
																			'BI_O4_FCV__c','BI_O4_FCV_Off_Net__c',
																			'BI_O4_Incremental_Cost__c','BI_O4_iCOST_TGS__c',
																			'BI_O4_CAPEX__c','BI_O4_CAPEX_TGS__c',
																			'BI_O4_NRR__c', 'BI_O4_NRR_TGS__c',
																			'BI_O4_NRC_TEF__c', 'BI_O4_NRC_TGS__c',
																			'BI_O4_MRR_TEF__c', 'BI_O4_MRR_TGS__c',
																			'BI_O4_MRC_TEF__c', 'BI_O4_MRC_TGS__c'},rtId);		

		String url = '/' + Schema.sObjectType.BI_O4_Approvals__c.getKeyPrefix() + '/e';
		//String cf = 'CF';
		PageReference retPage = new PageReference(url);
		retPage.getParameters().put('RecordType', mapRecordTypes.get('BI_O4_Gate_' + gate).Id);
		
		for (String fieldName : mapFieldId.keySet())
		{
			if (fieldName.equals('BI_O4_Opportunity__c'))
			{
				retPage.getParameters().put( mapFieldId.get(fieldName), quote.Opportunity.Name);
				retPage.getParameters().put( mapFieldId.get(fieldName) + '_lkid', quote.OpportunityId);
			}
			if (fieldName.equals('BI_O4_Proposal__c'))
			{
				retPage.getParameters().put(mapFieldId.get(fieldName), quote.Name);
				retPage.getParameters().put( mapFieldId.get(fieldName) + '_lkid', quote.Id);
			}
			if (fieldName.equals ('BI_O4_FCV__c'))
			{
				if (quote.BI_O4_Proposal_FCV_TEF__c!=null){

					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_FCV_TEF__c).replace('.', ','));
				}
			}
			if (fieldName.equals ('BI_O4_FCV_Off_Net__c'))
			{
				if (quote.BI_O4_Proposal_FCV_TGS__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_FCV_TGS__c).replace('.', ','));
				}
			}
			if (fieldName.equals ('BI_O4_Incremental_Cost__c'))
			{
				if (quote.BI_O4_Proposal_iCOST_TEF__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_iCOST_TEF__c).replace('.', ','));
				}
			}
			if (fieldName.equals ('BI_O4_iCOST_TGS__c'))
			{
				if (quote.BI_O4_Proposal_iCOST_TGS__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_iCOST_TGS__c).replace('.', ','));
				}
			}
			if (fieldName.equals ('BI_O4_CAPEX__c'))
			{
				if (quote.BI_O4_Proposal_iCAPEX_TGS__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_iCAPEX_TEF__c).replace('.', ','));
				}
			}
			if (fieldName.equals ('BI_O4_CAPEX_TGS__c'))
			{
				if (quote.BI_O4_Proposal_iCAPEX_TEF__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_iCAPEX_TGS__c).replace('.', ','));
				}
			}
			if (fieldName.equals ('BI_O4_NRR__c'))
			{
				if (quote.BI_O4_Proposal_NRR_TEF__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_NRR_TEF__c).replace('.', ','));
				}
			}
			if (fieldName.equals ('BI_O4_NRR_TGS__c'))
			{
				if (quote.BI_O4_Proposal_NRR_TGS__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_NRR_TGS__c).replace('.', ','));
				}
			}
			if (fieldName.equals ('BI_O4_NRC_TEF__c'))
			{
				if (quote.BI_O4_Proposal_NRC_TEF__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_NRC_TEF__c).replace('.', ','));
				}
				
			}
			if (fieldName.equals ('BI_O4_NRC_TGS__c'))
			{
				if (quote.BI_O4_Proposal_NRC_TGS__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_NRC_TGS__c).replace('.', ','));}
			}
			if (fieldName.equals ('BI_O4_MRR_TEF__c'))
			{
				if (quote.BI_O4_Proposal_MRR_TEF__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_MRR_TEF__c).replace('.', ','));}
			}
			if (fieldName.equals ('BI_O4_MRR_TGS__c'))
			{
				if (quote.BI_O4_Proposal_MRR_TGS__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_MRR_TGS__c).replace('.', ','));}
			}
			if (fieldName.equals ('BI_O4_MRC_TEF__c'))
			{
				if (quote.BI_O4_Proposal_MRC_TEF__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_MRC_TEF__c).replace('.', ','));}
			}
			if (fieldName.equals ('BI_O4_MRC_TGS__c'))
			{
				if (quote.BI_O4_Proposal_MRC_TGS__c!=null){
					retPage.getParameters().put(mapFieldId.get(fieldName), String.valueOf(quote.BI_O4_Proposal_MRC_TGS__c).replace('.', ','));}
			}
		}
		
		retPage.getParameters().put('saveURL', quote.Id);
		retPage.getParameters().put('retURL', quote.Id);
		retPage.setRedirect(true);
		System.debug('retPage:' + retPage.getUrl());
		return retPage;
    }
}