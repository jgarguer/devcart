@isTest(SeeAllData=true)
private class TestNECheckDeltaQuantity {

    static testMethod void myUnitTest() {
        
        
        
        Account acc = new Account();
        acc.Name = 'Testacc';
        acc.BI_Segment__c = 'test'; // 28/09/2017
        acc.BI_Subsegment_Regional__c = 'test'; // 28/09/2017
        acc.BI_Territory__c = 'test'; // 28/09/2017
        insert acc;
        
        NE__Contract_Header__c header = new NE__Contract_Header__c();
        header.NE__Actual_Qty__c = 3;
        header.NE__Delta_Qty__c = 2;
        header.NE__Name__c ='Header';
        insert header;
        
        NE__Contract__c contract = new NE__Contract__c();
        contract.NE__Contract_Header__c = header.Id;
        contract.NE__Status__c = 'Active';
        insert contract;
        
        NE__Product__c prod = new NE__Product__c();
        prod.Name = 'Prod';
        insert prod;
        
        NE__Contract_Line_Item__c line = new NE__Contract_Line_Item__c();
        line.NE__Contract__c = contract.Id;
        line.NE__Commercial_Product__c = prod.Id;
        line.NE__Allow_Qty_Override__c = false;
        insert line;
        
        NE__Contract_Account_Association__c cA = new NE__Contract_Account_Association__c();
        cA.NE__Account__c = acc.Id;
        cA.NE__Contract_Header__c = header.Id;
        insert cA;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'TestOpp';
        opp.AccountId = acc.Id;
        opp.StageName = 'F6 - Prospecting';
        opp.CloseDate = Date.today();
        opp.BI_Acuerdo_Marco__c = cA.Id;
        insert opp;
               
        NE__Order__c ord = new NE__Order__c();
        ord.NE__OptyId__c = opp.Id;
        insert ord;
        
        opp.Pedido__c = ord.Id;
        
        
        Test.startTest();
        
        //update opp;
        
        NE__OrderItem__c oi = new NE__OrderItem__c();
        oi.NE__Qty__c = 2;
        oi.NE__OrderId__c = ord.Id;
        oi.NE__Status__c = 'RFS';
        //oi.NE__ProdId__c = prod.Id;
        insert oi;
        
        //Test.startTest();
                
        oi.NE__Status__c = 'RFB';
        update oi;
        
        
        
    }
}