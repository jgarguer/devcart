public class BI_Jobs {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:
    
    IN:				Class to manage the scheduled jobs
    
    History:
    
    <Date>            <Author>          <Description>
    15/04/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/   


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Calls createExternalCommunicationJOB
    
    IN:            Campaign Id
    OUT:           void
    
    History:
    
    <Date>            <Author>          <Description>
    25/04/2014        Pablo Oliva       Initial version
    31/10/2014		  Pablo Oliva		Deprecated
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	/*public static void createExternalCommunicationJOB(Id id){
		
		createExternalCommunicationJOB(id, null);
 
	}*/


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Creates the ExternalCommunicationJob for the given Id
    
    IN:            Id, seconds
    OUT:           void
    
    History:
    
    <Date>            <Author>          <Description>
    25/04/2014        Pablo Oliva       Initial version
    31/10/2014		  Pablo Oliva		Deprecated
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	/*public static void createExternalCommunicationJOB(Id id, Integer seconds){
		try{
			String jobName = 'External Communication';
		
			if(seconds == null)
				seconds = 3;
	        
		    System.schedule(generateStrNameProg(Datetime.now().addSeconds(seconds), jobName), generateStrProg(Datetime.now().addSeconds(seconds)), 
		    				new BI_ExternalCommunication_JOB(id));
		}catch (exception e){
		   BI_LogHelper.generate_BILog('BI_Jobs.createExternalCommunicationJOB', 'BI_EN', Exc, 'Trigger');
		}
        
	}*/


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Calls createExternalCommunicationJOB
    
    IN:            Campaign Id
    OUT:           void
    
    History:
    
    <Date>            <Author>          <Description>
    25/04/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void updateCases(){
		
		updateCases(null, false);
        
	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Creates the job to update the cases
    
    IN:            Id, seconds
    OUT:           void
    
    History:
    
    <Date>            <Author>          <Description>
    25/04/2014        Ignacio Llorca      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void updateCases(Id id, Boolean last){
		try{
			String jobName = 'Update Cases';		
			BI_bypass__c bypass = BI_bypass__c.getInstance();
            if(!bypass.BI_stop_job__c){

				if(!last && id !=null){	        
				    System.schedule(generateStrNameProg(Datetime.now().addSeconds(3), jobName), generateStrProg(Datetime.now().addSeconds(3)), 
				    				new BI_Case_JOB(id));
				}else if(!last && id == null){	        
				    System.schedule(generateStrNameProg(Datetime.now().addSeconds(3), jobName), generateStrProg(Datetime.now().addSeconds(3)), 
				    				new BI_Case_JOB(null));
				}else if(last){
					System.schedule(generateStrNameProg(Datetime.now().addSeconds(3), jobName), '0 0 3 * * ?', new BI_Case_JOB(null));			
				}
			}
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('BI_Jobs.updateCases', 'BI_EN', Exc, 'Trigger');
		}
        
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Creates the job to update users

    History: 
    
     <Date>                     <Author>                <Change Description>
    20/11/2014                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateUsers(Id id, Boolean last){
		try{
			System.debug('BI_Jobs.updateUser: ID: ' + id);
			System.debug('BI_Jobs.updateUser: Last: ' + last);

			String jobName = 'Update users';		
		
		    BI_bypass__c bypass = BI_bypass__c.getInstance();
            if(!bypass.BI_stop_job__c){
				if(!last && id !=null){	        
				    System.schedule(generateStrNameProg(Datetime.now().addSeconds(3), jobName), generateStrProg(Datetime.now().addSeconds(3)), new BI_User_JOB(id));
				}else if(!last && id == null){	        
				    System.schedule(generateStrNameProg(Datetime.now().addSeconds(3), jobName), generateStrProg(Datetime.now().addSeconds(3)), new BI_User_JOB(null));
				}else if(last){
					System.schedule(generateStrNameProg(Datetime.now().addSeconds(3), jobName), generateStrProg(Datetime.now().addMinutes(2)), new BI_User_JOB(null));			
				}
			}
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('BI_Jobs.updateCases', 'BI_EN', Exc, 'Trigger');
		}
        
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Generates the Job date
    
    IN:            dt_job: Job datetime
    OUT:           The Job date generated
    
    History:
    
    <Date>            <Author>          <Description>
    25/04/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static String generateStrProg(Datetime dt_job){
		try{
		    String StrProg = dt_job.second() + ' ' + dt_job.minute() + ' ' + dt_job.hour() + ' ' + dt_job.day() + ' ' +dt_job.month() + ' ? ' + dt_job.year();
			return StrProg;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('BI_Jobs.generateStrProg', 'BI_EN', Exc, 'Trigger');
		   return null;
		}
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Generates the job name
    
    IN:            dt_job: job Datetime
    			   jobName: Job Name
    OUT:           The job name generated
    
    History:
    
    <Date>            <Author>          <Description>
    25/04/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static String generateStrNameProg(Datetime dt_job, String jobName){
		try{
			Decimal RND = SYSTEM.Math.random()*1000000;
		    Integer aux = SYSTEM.Math.round(RND);
		    String StrNameProg = jobName + ' - '+ ' '+ aux + ' ' + dt_job.year() + '-' + dt_job.month() + '-' + dt_job.day() + 'T' + dt_job.hour() + ':' + dt_job.minute() + ':' + dt_job.second();
			return StrNameProg;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('BI_Jobs.generateStrNameProg', 'BI_EN', Exc, 'Trigger');
		   return null;
		}
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   TEMPORAL [ONLY USED FOR UPDATE Opp.BI_Productos_numero__c]

    History: 
    
     <Date>                     <Author>                <Change Description>
    14/01/2014                  Micah Burgos            Initial Version
    27/09/2015					Francisco Ayllon		Deprecated
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*public static void updateProductosNumero(Boolean last){
		try{
			System.debug('BI_Jobs.updateProductosNumero: Last: ' + last);

			String jobName = 'Update ProductosNumero';		
			BI_bypass__c bypass = BI_bypass__c.getInstance();

			if(!last && !bypass.BI_stop_job__c){	        
			    System.schedule(generateStrNameProg(Datetime.now().addSeconds(3), jobName), generateStrProg(Datetime.now().addSeconds(3)), new BI_ProductosNumero_JOB());
			}
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('BI_Jobs.updateProductosNumero', 'BI_EN', Exc, 'Trigger');
		}
        
	}
	*/
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Calls BI_Currency_UF_JOB

    History: 
    
    <Date>                     <Author>                <Change Description>
    20/01/2014                 Pablo Oliva	           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateCurrencyUF(){
		
		try{
			
			String jobName = 'update Currency UF';
			
			System.schedule(generateStrNameProg(Datetime.now().addSeconds(3), jobName), generateStrProg(Datetime.now().addSeconds(3)), new BI_Currency_UF_JOB());
	
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('BI_Jobs.updateCurrencyUF', 'BI_EN', Exc, 'Job');
		}
        
	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos
	Company:       Aborda
	Description:   Calls BI_UpdateEntitlementsAcc_JOB
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	17/02/2015                      Micah Burgos                Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void updateEntitlementsAcc(Id id, List<SlaProcess> SLAProcesos, boolean last_round){
		try{
			String jobName = 'Update Accounts Entitlements';		
			BI_bypass__c bypass = BI_bypass__c.getInstance();
			
			if(SLAProcesos != null && !SLAProcesos.isEmpty() && !last_round && !bypass.BI_stop_job__c){     
		    	System.schedule(generateStrNameProg(Datetime.now().addMinutes(1), jobName), generateStrProg(Datetime.now().addMinutes(1)), new BI_UpdateEntitlementsAcc_JOB(id, SLAProcesos));
			}
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('BI_Jobs.updateEntitlementsAcc', 'BI_EN', Exc, 'Trigger');
		}
        
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos
	Company:       Aborda
	Description:   Calls BI_CheckNews_SLAProcess_JOB
	
	History: 
	
	<Date>                          <Author>                    <Change Description>
	24/02/2015                      Micah Burgos                Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public static void start_checkNews_SLAProcess_JOB(Map<Id,SlaProcess> map_old_SLAProceso){
		try{
			BI_bypass__c bypass = BI_bypass__c.getInstance();

			if(!bypass.BI_stop_job__c){
				String jobName = 'Check New SlaProcess';
				System.schedule(generateStrNameProg(Datetime.now(), jobName), generateStrProg(Datetime.now().addHours(3)) , new BI_CheckNews_SLAProcess_JOB(map_old_SLAProceso));	
			}
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('BI_Jobs.updateEntitlementsAcc', 'BI_EN', Exc, 'Trigger');
		}	
	}

	
	
	public static void Linea_JOB(String email, List<Id> lst, Integer posi, Map<Id,String> result, Map<Id,String> mline, Integer option){
		
		String jobName = 'Linea_JOB';		
		
		System.schedule(generateStrNameProg(Datetime.now().addSeconds(3), jobName), generateStrProg(Datetime.now().addSeconds(3)), new BI_Linea_JOB(email, lst, posi, result, mline, option));
        
	}
	
	public static void BackOffice_JOB(String email, List<Id> lst, Integer posi, Map<Id,String> result, Integer option){
		
		String jobName = 'BackOffice_JOB';		
		
		System.schedule(generateStrNameProg(Datetime.now().addSeconds(3), jobName), generateStrProg(Datetime.now().addSeconds(3)), new BI_BackOffice_JOB(email, lst, posi, result, option));
        
	}

}