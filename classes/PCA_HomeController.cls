public without sharing virtual class PCA_HomeController {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Methods to manage home page 
    
    History:
    
    <Date>                    <Author>               <Description>
    24/04/2014              Jorge Longarela         Initial Version 
    17/07/2014              Micah Burgos            V2
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Retrieves Home controller
    
    History:
    
    <Date>                    <Author>               <Description>
    24/04/2014              Jorge Longarela         Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_HomeController getThis() {       
        return this;
    }   
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Retrieves current portal user
    
    History:
    
    <Date>                    <Author>               <Description>
    24/04/2014              Jorge Longarela         Initial Version 
    31/05/2017              Cristina Rodriguez      Commented Usuario_SolarWinds__c, Usuario_BO__c, Sector__c, pass_BO__c, token_SolarWinds__c (User) from SELECT
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public User usuarioPortal{
        get{
            try{
                if(BI_TestUtils.isRunningTest()){
                    throw new BI_Exception('test');
                }
                if(usuarioPortal==null)
                usuarioPortal = [Select id, FullPhotoUrl, name,CompanyName, ContactId, MobilePhone, Contact.Name  from User where id=:UserInfo.getUserId()];
                //Usuario_SolarWinds__c, Usuario_BO__c, Sector__c, pass_BO__c, token_SolarWinds__c
                return usuarioPortal;
            }catch (exception Exc){
                BI_LogHelper.generate_BILog('PCA_HomeController.usuarioPortal', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
        set;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Retrieves Home controller
    
    History:
    
    <Date>                    <Author>               <Description>
    17/07/2014                Micah Burgos           Initial Version 
    11/01/2016                Guillermo Muñoz        Add limit 1 on the query to avoid errors
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI_Contact_Customer_Portal__c usuarioCustomerPortal{
        get{
            try{
                if(BI_TestUtils.isRunningTest()){
                    throw new BI_Exception('test');
                }
                if(usuarioCustomerPortal==null) {
                    //system.debug(BI_AccountHelper.getCurrentAccountId());
                    //system.debug(UserInfo.getUserId());
                    usuarioCustomerPortal = [Select id,BI_BO_Pwd__c,BI_BO_User__c,BI_SW_User__c, BI_SW_Pwd__c, BI_Cliente__r.BI_Country__c from BI_Contact_Customer_Portal__c where BI_User__c=:UserInfo.getUserId() and BI_Cliente__c =: BI_AccountHelper.getCurrentAccountId() LIMIT 1];           
                }
            system.debug('********usuarioCustomerPortal.BI_SW_User__c: ' +usuarioCustomerPortal.BI_SW_User__c);
            system.debug('********usuarioCustomerPortal.BI_SW_Pwd__c: ' +usuarioCustomerPortal.BI_SW_Pwd__c);
                return usuarioCustomerPortal;
            }catch (exception Exc){
                BI_LogHelper.generate_BILog('PCA_HomeController.usuarioCustomerPortal', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
        set;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Retrieves Home controller
    
    History:
    
    <Date>                    <Author>               <Description>
    17/07/2014              Micah Burgos            Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public List<Account> AccountsUser{
        get{
            try{
                if(BI_TestUtils.isRunningTest()){
                    throw new BI_Exception('test');
                }
                return BI_AccountHelper.getAccounts();
            }catch (exception Exc){
                BI_LogHelper.generate_BILog('PCA_HomeController.AccountsUser', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
        set;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Check if current user is valid
    
    History:
    
    <Date>                    <Author>               <Description>
    24/04/2014              Jorge Longarela         Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference enviarALoginComm(){
        
        try{
            if(BI_TestUtils.isRunningTest()){
                    throw new BI_Exception('test');
                }
            List<Profile> prof_CCP = [Select Id FROM Profile where Name = 'BI_Customer_Community_Plus'];    
            System.debug('***HOME CONTROLLER: enviarALoginComm***');
            String pcaLoginStr = (Apexpages.currentPage().getUrl()!=null)?String.escapeSingleQuotes(Apexpages.currentPage().getUrl()):null;
            if(!pcaLoginStr.contains('PCA_Login')){
                // Informativo: System.debug('#### communitiesLanding: ' + Network.communitiesLanding().getUrl());
                List<NetworkMember> nMList = [Select MemberId From NetworkMember where NetworkId= :Network.getNetworkId() and MemberId = : UserInfo.getUserId()];
                if(!nMList.isEmpty()){
                 if(!prof_CCP.IsEmpty() && prof_CCP[0].Id == UserInfo.getProfileId()){
                 if(getCurrentPage() != 'PCA_Home'){
                 if(!PCA_ProfileHelper.checkPermissionSet(getCurrentPage())){
                        System.debug('***HOME CONTROLLER: CHECK PERMISSION REDIRECT ***');
                        return new PageReference(Site.getBaseUrl() + '/PCA_Home');
                  }
                  }
                  }
                        return null;
                }else{
                    //String urlFinal = Test.isRunningTest()?System.Label.PCA_Test_Current_site_url:Site.getBaseUrl();
                    String urlFinal = Site.getBaseUrl() + '/';
                    String currentURL = String.escapeSingleQuotes(System.currentPageReference().getUrl());
                    return new PageReference(urlFinal + 'PCA_Login?retURL='+currentURL.replace('/apex', ''));   
                }
            }else{
                return null;
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_HomeController.enviarALoginComm', 'Portal Platino', Exc, 'Class');
            return null;
        }   
        
        /*
        if(nMList!=null && nMList.size() > 0)
            for(NetworkMember oneNetMember :nMList){
                System.debug('-----> ID usuario portal: ' + UserInfo.getUserId()+ '. ID member network: ' + oneNetMember.MemberId); 
                if(oneNetMember.MemberId ==  UserInfo.getUserId()){
                    return null;
                }
            }
        String urlFinal = Test.isRunningTest()?System.Label.PCA_Test_Current_site_url:Site.getBaseUrl();
        system.debug(urlFinal + 'PCA_Login?retURL='+System.currentPageReference().getUrl().replace('/apex', ''));
        system.debug(System.currentPageReference().getUrl().replace('/apex', ''));
        return new PageReference(urlFinal + 'PCA_Login?retURL='+System.currentPageReference().getUrl().replace('/apex', ''));
        */
            
    }
    public String getCurrentPage(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            String url = Apexpages.currentPage().getUrl();
            String[] pageSplit = url.split('/');
            String pageFinal = '';
            String currentPage = pageSplit[pageSplit.size()-1];
            if(pageSplit[pageSplit.size()-1].contains('?')){
                String[] pageSplit2 = pageSplit[pageSplit.size()-1].split('\\?');
                pageFinal = pageSplit2[0];
            }else{
                pageFinal = pageSplit[pageSplit.size()-1];
            }
            
            return pageFinal;
        }catch(Exception Exc) {
            BI_LogHelper.generate_BILog('PCA_HomeController.getCurrentPage', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    

}