/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-04-20      Manuel Esthiben Mendez Devia (MEMD)     Cloned Controller
*************************************************************************************************************/
public with sharing class BI_COL_GestionBreaks_ctr 
{

	public static List<Id> lstIdMS=new List<Id>();
	public static BI_COL_Seguimiento_Quiebre__c quiebre;

	
	public static void gestionQuiebres(BI_COL_Seguimiento_Quiebre__c segQuiebre, String accion, String observacion)
	{
		
		System.debug('idQuiebre=='+segQuiebre+' accion=='+accion+' observacion=='+observacion);

		quiebre=segQuiebre;

		system.debug('----quiebre encontrado--->'+quiebre);


        if(accion==null || accion.equals(label.BI_COL_LbCreadoSFDC))
        {
            quiebre.BI_COL_Fecha_inicio_aplazamiento__c=system.today();
        } 
        
     //   quiebre.Descripcion_de_servicio_DSR__c=quiebre.BI_COL_Modificacion_de_Servicio__r.BI_COL_Codigo_unico_servicio__c;
        
        
        if(accion.equals(label.BI_COL_Lbcrear) )
        {
        	if(quiebre.BI_COL_Transaccion__c==null ||quiebre.BI_COL_Transaccion__c.equals(label.BI_COL_LbSFDC))
        	{
	        	quiebre.BI_COL_Estado_envio__c=label.BI_COL_LbCreadoSFDC;
        	}
        }  
        else if(accion.equals(label.BI_COL_Lbactualizar))
        {
       		 if(quiebre.BI_COL_Transaccion__c==null ||quiebre.BI_COL_Transaccion__c.equals(label.BI_COL_LbSFDC))
       		 {
        		quiebre.BI_COL_Estado_envio__c=label.BI_COL_LbActualizadoSFDC;
       		 }
        }
        
        if(!quiebre.BI_COL_Observaciones__c.equals(observacion)||accion.equals(label.BI_COL_LbCreadoSFDC))
        {
        
            if(quiebre.BI_COL_Actualizacion_gestion__c!=null)
            {
                quiebre.BI_COL_Actualizacion_gestion__c+='\n'+quiebre.BI_COL_Observaciones__c;
            }     
            else
            {
                quiebre.BI_COL_Actualizacion_gestion__c=quiebre.BI_COL_Observaciones__c;
            }
        }
       
        system.debug('--->quiebre Actualizado--->'+quiebre);
        
        
        /*******************************************************************************************************/               
                        
        
        
    }
    
    public static void actualizarMs(Id idQuiebre)
    {
    	
    	BI_COL_Modificacion_de_Servicio__c ms=null;
    	quiebre=[select Id,BI_COL_Causal_quiebre_inicial__c,BI_COL_Codigo_Quiebre_Sisgot__c,BI_COL_Fecha_inicio_aplazamiento__c,LastModifiedDate,BI_COL_Transaccion__c,
				BI_COL_Modificacion_de_Servicio__r.BI_COL_Codigo_unico_servicio__c, BI_COL_Modificacion_de_Servicio__r.BI_COL_Codigo_unico_servicio__r.Name,BI_COL_Estado_de_Gestion__c,BI_COL_Fecha_fin_aplazamiento__c,OwnerId,Owner.Name,Owner.FirstName,
				Owner.LastName,BI_COL_Observaciones__c,BI_COL_Actualizacion_gestion__c,BI_COL_Modificacion_de_Servicio__r.Name,CreatedDate, Name               
				from BI_COL_Seguimiento_Quiebre__c where Id=: idQuiebre limit 1];
		if(quiebre!=null)
			ms=[select BI_COL_Estado_orden_trabajo__c from BI_COL_Modificacion_de_Servicio__c where Id=:quiebre.BI_COL_Modificacion_de_Servicio__c limit 1];
		 /*************** actualizaci�n de datos de objetos dependientes a la Gestion de quiebre ****************/
        System.debug(' quiebre.BI_COL_Estado_de_Gestion__c ========\n\n '+quiebre.BI_COL_Estado_de_Gestion__c);
        if(quiebre.BI_COL_Estado_de_Gestion__c!=null && (quiebre.BI_COL_Estado_de_Gestion__c.equals(label.BI_COL_LbAbierto) || quiebre.BI_COL_Estado_de_Gestion__c.equals(label.BI_COL_LbAplazada))){
            ms.BI_COL_Estado_orden_trabajo__c=label.BI_COL_LbAplazada;
            ms.BI_COL_Causal_aplazamiento__c=quiebre.BI_COL_Causal_quiebre_inicial__c;
        }
        if(quiebre.BI_COL_Estado_de_Gestion__c!=null && quiebre.BI_COL_Estado_de_Gestion__c.equals(label.BI_COL_LbCerrado)){
            ms.BI_COL_Estado_orden_trabajo__c='En Desarrollo';
           // quiebre.BI_COL_Fecha_fin_aplazamiento__c=quiebre.LastModifiedDate.date();  
        }
        system.debug('--->ms Actualizado--->'+ms);
        
        try{        
        	update ms;
		}
		catch(Exception e)
		{
			registroLog('test ',quiebre.Id,'Error actualizaci�n de MS: '+e);
		}
    }   
    
    @Future(callout=true)
    public static void envioQuiebreSW(String idQuiebre, String anteriorObservacion,String accion,Date fechaRecordar)
	{
		/**************************************** Preparando informaci�n para envio a SW ***************************************/
		quiebre=[select Id,BI_COL_Causal_quiebre_inicial__c,BI_COL_Codigo_Quiebre_Sisgot__c,BI_COL_Fecha_inicio_aplazamiento__c,LastModifiedDate,BI_COL_Transaccion__c,
				BI_COL_Fecha_contactar_cliente__c,	BI_COL_Modificacion_de_Servicio__r.BI_COL_Codigo_unico_servicio__c, BI_COL_Modificacion_de_Servicio__r.BI_COL_Codigo_unico_servicio__r.Name,
				BI_COL_Estado_de_Gestion__c,BI_COL_Fecha_fin_aplazamiento__c,OwnerId,Owner.Name,Owner.FirstName,
				Owner.LastName,BI_COL_Observaciones__c,BI_COL_Actualizacion_gestion__c,BI_COL_Modificacion_de_Servicio__r.Name,CreatedDate,BI_COL_Estado_envio__c                
				from BI_COL_Seguimiento_Quiebre__c where Id=: idQuiebre limit 1];
		
		 /**** llamado al SW de Quiebres ******
        System.debug(quiebre.transaccion__c+'----estao de envio-->'+quiebre.BI_COL_Estado_envio__c);
        if(quiebre.transaccion__c==null){
        	System.debug('---- entra al envio del SW ---');
        	envioQuiebreSW(observacion,accion);
        	quiebre.transaccion__c='FSDC';
        }else if(!quiebre.transaccion__c.contains('TRS')){
        	System.debug('---- entra al envio del SW ---');
        	 envioQuiebreSW(observacion,accion);
        	 quiebre.transaccion__c='FSDC';
        }
        if(quiebre.transaccion__c.equals('TRS'))//validaci�n para evidar la duplicidad de envio de correo por el flujo de trabajo
        	quiebre.transaccion__c=label.BI_COL_LbSFDC;
        *************************************/
        	
        ws_TrsMasivo.datosQuiebre swQuiebre=new ws_TrsMasivo.datosQuiebre();
        swQuiebre.Causal=quiebre.BI_COL_Causal_quiebre_inicial__c;
        swQuiebre.CodTrs=quiebre.BI_COL_Codigo_Quiebre_Sisgot__c;
        swQuiebre.Ds=quiebre.BI_COL_Modificacion_de_Servicio__r.BI_COL_Codigo_unico_servicio__r.Name;
        if(quiebre.BI_COL_Estado_de_Gestion__c.equals(label.BI_COL_LbAbierto))
        {
        	swQuiebre.Estado=label.BI_COL_LbAplazada;
        }
        else
        {
        	swQuiebre.Estado=label.BI_COL_LbCerrado;
        }
        swQuiebre.fechaEstimadaDesaplazamiento=''+quiebre.BI_COL_Fecha_fin_aplazamiento__c;//pendiente por confirmar si es este el campo
        swQuiebre.IdResponsable=quiebre.OwnerId;
        swQuiebre.NombreResponsable=quiebre.Owner.Name;
        swQuiebre.Ms=quiebre.BI_COL_Modificacion_de_Servicio__r.Name;
        
        if(quiebre.BI_COL_Estado_envio__c.equals(label.BI_COL_LbCreadoSFDC))
            swQuiebre.Observaciones=quiebre.BI_COL_Observaciones__c;
        else if(quiebre.BI_COL_Observaciones__c!=null && !quiebre.BI_COL_Observaciones__c.equals(anteriorObservacion))        
            swQuiebre.Observaciones=quiebre.BI_COL_Observaciones__c;
        
        ws_TrsMasivo.datosQuiebre[] sltDatosQuiebre=new ws_TrsMasivo.datosQuiebre[]{swQuiebre};
       /*************************************************************************************************************************/
        
      ws_TrsMasivo.serviciosSISGOTSOAP sw=new ws_TrsMasivo.serviciosSISGOTSOAP();
      sw.timeout_x=120000;
        ws_TrsMasivo.codigosRespuesta[] resp=null;
        System.debug('\n\n sw.endpoint_x'+sw.endpoint_x+' \n\n');
        system.debug('| SW--->'+sltDatosQuiebre);
        String rtaAplazamiento='';
        //try
        //{
	        if(accion.equals('crear'))
	        {
	        	//Enviar aplazar la OT
	        	if(!Test.isRunningTest()){

	        	resp=sw.crearQuiebre(sltDatosQuiebre);
                
                System.debug('\n\n RESPUESTA SERVICIO WEB '+resp+' \n\n');
                }
                rtaAplazamiento=BI_COL_InterfacesSisgot_ctr.EnviarAplazamiento(quiebre.BI_COL_Modificacion_de_Servicio__r.Name,String.valueOf(quiebre.BI_COL_Fecha_inicio_aplazamiento__c),null,quiebre.BI_COL_Causal_quiebre_inicial__c);
                System.debug('\n\n rtaAplazamiento= '+rtaAplazamiento+' \n\n');
            } 
            else
            {
                if(!Test.isRunningTest()){
                    
                System.debug('\n\n MENSAJE DE PRUEBA ======= \n\n');
                resp = sw.actualizarQuiebre(sltDatosQuiebre);
	        	System.debug('\n\n respuesta del servicios ========= \n '+resp+' \n\n');
	        	}
	        	if(swQuiebre.Estado==label.BI_COL_LbCerrado)
	        	{
	        		BI_COL_InterfacesSisgot_ctr.EnviarAplazamiento(quiebre.BI_COL_Modificacion_de_Servicio__r.Name,String.valueOf(quiebre.BI_COL_Fecha_inicio_aplazamiento__c),String.valueOf(quiebre.BI_COL_Fecha_fin_aplazamiento__c),quiebre.BI_COL_Causal_quiebre_inicial__c);
	        		System.debug('\n\n rtaAplazamiento= '+rtaAplazamiento+' \n\n');
	        	}
	        }
	        
			system.debug('\n\n respuesta del SW--->'+resp); 
			
			if(resp!=null && resp.size()>0)
			{
				if(resp[0].codigoTRS!=null)
				{
					quiebre.BI_COL_Codigo_Quiebre_Sisgot__c =''+resp[0].codigoTRS;
					BI_Log__c log = new BI_Log__c();
					log.BI_COL_Seguimiento_Quiebre__c=quiebre.Id;
                    log.BI_COL_Informacion_Enviada__c = quiebre+'';
                    log.BI_COL_Estado__c='EXITOSO';
                    log.BI_COL_Informacion_recibida__c='Enviado por TRS: '+resp[0].mensajeError;
                    insert log;
                }
                else
                {
                    quiebre.BI_COL_Codigo_Quiebre_Sisgot__c='Fallido Conexion';
                    BI_Log__c log = new BI_Log__c();
                    log.BI_COL_Seguimiento_Quiebre__c=quiebre.Id;
                    log.BI_COL_Informacion_Enviada__c = quiebre+'';
                    log.BI_COL_Estado__c='Fallido';
					log.BI_COL_Informacion_recibida__c='Error Enviado por TRS: '+resp[0].mensajeError;
					insert log;
				}
			}
			try
			{
				update quiebre;
				
				System.debug((quiebre.BI_COL_Fecha_contactar_cliente__c!=null)+'>>>'+(quiebre.BI_COL_Fecha_contactar_cliente__c!=fechaRecordar)+'>>>'+(quiebre.BI_COL_Fecha_contactar_cliente__c>=Date.today()));
	        	if(quiebre.BI_COL_Fecha_contactar_cliente__c!=null && (quiebre.BI_COL_Fecha_contactar_cliente__c!=fechaRecordar || accion=='crear') && quiebre.BI_COL_Fecha_contactar_cliente__c<=Date.today())
	        	{

	        	System.debug('Crea tarea');
	        	Task tarea=new Task();
	     		tarea.OwnerId=quiebre.OwnerId;
	     		tarea.Description='Se ha creado el registro de Seguimiento de Quiebre: '+quiebre.id;
	     		tarea.ReminderDateTime=quiebre.BI_COL_Fecha_contactar_cliente__c;
	     		tarea.IsReminderSet=true;

	     		insert tarea;
	     		}
	        		        	
	        }
	        catch(Exception e)
			{
				registroLog(sltDatosQuiebre+'',quiebre.Id,'Error actualizaci�n de quiebre: '+e);
			}
   //     }catch(Exception e)
   //     {
			//quiebre.BI_COL_Codigo_Quiebre_Sisgot__c='Fallido Conexion';
			//registroLog(sltDatosQuiebre+'',quiebre.Id,'Error en el consumo del servicio web de TRS: '+e);
   //     }
   }
	
	public static void registroLog(String Enviado, ID IDquiebre,String Respuesta)
	{
		    BI_Log__c log = new BI_Log__c();

            log.BI_COL_Informacion_Enviada__c  = Enviado;
        	log.BI_COL_Seguimiento_Quiebre__c  = IDquiebre;
        	log.BI_COL_Informacion_recibida__c = Respuesta;
            
        	insert log;
	}

    //public static void creaLogTransaccion(List<ws_TrsMasivo.crearProyectoResponse> lstResp, List<Opportunity> lstIdOpt){
    //  List<BI_Log__c> lstLogTran=new List<BI_Log__c>();
    //  BI_Log__c log=null; 
    //  String mensaje=lstResp[0].mensajeError;
    //  if((mensaje==null || mensaje=='') && lstResp[0].estado=='OK')
    //    mensaje=label.BI_COL_lblRegistro_Enviado_Satisfactoriamente;
    //  system.debug('métod de log de transacciones::SW::\n\n'+lstResp);
    
    //for(Opportunity opt:lstIdOpt){
    //    log=new BI_Log__c(BI_COL_Informacion_recibida__c=mensaje,BI_COL_Oportunidad__c=opt.id);
    //  lstLogTran.add(log);
    //}
  
    //system.debug('Insertando log de transacciones::SW::'+lstLogTran);
    //insert lstLogTran;
    //}

}