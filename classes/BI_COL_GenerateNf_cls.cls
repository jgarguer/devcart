/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-04-16      Manuel Esthiben Mendez Devia (MEMD)     Cloned Controller
*************************************************************************************************************/
public with sharing class BI_COL_GenerateNf_cls 
{

    /**Atributos*/
    public boolean mostrarRegresar {get;set;}
    public List<WrapperMS> lstMS {get;set;}
    public boolean todosMarcados {get;set;}
    public boolean mostrarValidaciones{get; set;}
    public String Busqueda{get; set;}
    public Map<String,Integer> tempMap{get; set;}
    public String idCliente {get; set;}

    /**Parametros del paginador*/
    private Integer pageNumber;
    private Integer totalPageNumber;
    private Integer pageSize;
    private List<WrapperMS> pageMS;
    private static List<BI_COL_Modificacion_de_Servicio__c> lstMSProcesar;
    public List<String> idMsSeleccionada{get;set;}

    /** Constructor */
    public BI_COL_GenerateNf_cls()
    {
        Buscar(); 
        pageNumber=1;  
    }
    public void Buscar()
    {
        if(ApexPages.currentPage().getParameters().get('idAccount') !=null /*&&idCliente.trim().length()>0*/)
        {
            idCliente = ApexPages.currentPage().getParameters().get('idAccount');
            System.debug(':: Carga Cliente: ' + idCliente);

            List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta = new List<BI_COL_Modificacion_de_Servicio__c>();
            //TODO Borrar
            lstMSConsulta= getMSSolicitudServicio(idCliente);
            Integer cont=0;
            tempMap=new Map<String,Integer>();

            if(lstMSConsulta != null && lstMSConsulta.size() > 0)
            {
                //Llenar listado de MS Wrapper que se muestran en pantalla
                lstMS = new List<WrapperMS>();
                this.mostrarRegresar = true; 
                this.mostrarValidaciones = false; 
                for(BI_COL_Modificacion_de_Servicio__c ms: lstMSConsulta)
                {
                    WrapperMS wMS = new WrapperMS(ms, false);
                    this.lstMS.add(wMS);
                    System.debug('Agregando elemento===='+wMs);
                }

                System.debug('\n\n this.mostrarValidaciones='+this.mostrarValidaciones+'\n\n');
                System.debug('La lista de Wrapper MS es: ' + lstMS);

                //Inicializar valores del paginador
                pageNumber = 0;
                totalPageNumber = 0;
                pageSize = 50;
                ViewData();
            }
            else
            {
                BI_COL_Useful_messages_cls.mostrarMensaje(BI_COL_Useful_messages_cls.INFO, 'No existe modificaciones de servicios activas asociadas al cliente');
            }
        }
        else
        {
            BI_COL_Useful_messages_cls.mostrarMensaje(BI_COL_Useful_messages_cls.ERROR, 'Por favor especifique un cliente');
        }
    }

    public PageReference ViewData()
    {
        totalPageNumber = 0;
        BindData(1);
        return null;
    }

    /**
    * Accion para generar MS de Baja
    * @PageReference 
    */
    public PageReference action_generarMSBaja()
    {
        lstMSProcesar = new List<BI_COL_Modificacion_de_Servicio__c>();
        List<Contact> LSconRepr=new List<Contact>();
        String msgEnviado='';
        String msgNOEnviado='';
        String nombreRepLegal='';
        idMsSeleccionada=new List<String>();
        
        LSconRepr=[select Name from Contact WHERE BI_Representante_legal__c = true AND Account.ID=:idCliente LIMIT 1];
        if(LSconRepr.size()<0)
        {
            nombreRepLegal=LSconRepr[0].Name;
        }
        for(WrapperMS w:lstMS)
        {
            System.debug('La var w'+w);
            System.debug('La var w.seleccionado'+w.seleccionado);
            System.debug('La var w.ms'+w.ms);
            if (w != null && w.seleccionado!=null && w.seleccionado)
            {
                lstMSProcesar.add(w.ms);
                idMsSeleccionada.add(w.ms.Id);
            }
        }
        List<String> listaNF=new List<String>();
        List<String> listaMS=new List<String>();
        
        if (lstMSProcesar.size()>0)
        {
            for(BI_COL_Modificacion_de_Servicio__c ms:lstMSProcesar)
            {
                ms.BI_COL_Autorizacion_facturacion__c=true;
                if(ms.BI_COL_Clasificacion_Servicio__c=='BAJA')
                {
                    ms.BI_COL_Fecha_fin__c=ms.BI_COL_Fecha_comprometida_baja_tecnica__c;
                    listaMS.add(ms.id);
                    msgEnviado+=ms.Name+', ';
                }
                else if(ms.BI_COL_Fecha_inicio_de_cobro_RFB__c!=null)
                {
                    Integer intDuracion = Integer.valueOf(String.valueOf(ms.BI_COL_Duracion_meses__c)); 
                    System.debug('\n\n ms.BI_COL_Fecha_inicio_de_cobro_RFB__c ' +ms.BI_COL_Fecha_inicio_de_cobro_RFB__c +'\n\n');
                    Date  dtFechaFinal=Date.valueOf(ms.BI_COL_Fecha_inicio_de_cobro_RFB__c).addMonths(intDuracion).addDays(-1);
                    System.debug('\n\n dtFechaFinal: '+dtFechaFinal+'\n\n');
                    ms.BI_COL_Fecha_fin__c=dtFechaFinal;
                    listaMS.add(ms.id);
                    msgEnviado+=ms.Name+', ';
                }
                else
                {
                    msgNOEnviado+=ms.Name+', ';
                }
                ms.BI_COL_Fecha_de_facturacion__c=System.today();
                ms.BI_COL_RepresentanteLegal__c=nombreRepLegal;

            }
            System.debug('\n\n Antes de actualizar\n\n');

            if(listaMS.size()>0)
            {
                update lstMSProcesar;

                List<BI_Log__c> nFact=[Select n.BI_COL_Modificacion_Servicio__c, n.BI_COL_Estado__c, n.Id, n.BI_COL_Tipo_Transaccion__c 
                                                from BI_Log__c n 
                                                where n.BI_COL_Modificacion_Servicio__c in:listaMS and n.BI_COL_Estado__c not in ('Cerrado','Procesado')];
                
                System.debug('List<Novedad_Fcaturacion_c> nFact = ' + nFact);
                
                for(BI_Log__c nf:nFact)
                {
                    System.debug('Recorriendo la lista nFact');
                    nf.BI_COL_Estado__c='Fallido';
                    System.debug('nf.BI_COL_Estado__c cambiado a cerrado');
                }
                System.debug('Llega a actualizar nFact');
                update nFact;
                System.debug('\n\n Despues de actualizar\n\n');             
                BI_COL_NoveltyBilling_cls.ProcesarServCorporativos_ws(lstMSProcesar,true);
                List<BI_Log__c> scope= [SELECT ID, BI_COL_Informacion_Enviada__c,BI_COL_Informacion_recibida__c,BI_COL_Estado__c
                                            FROM BI_Log__c 
                                            where BI_COL_Modificacion_Servicio__c in:listaMS and BI_COL_Estado__c='Procesado'];
                for(BI_Log__c nf:scope)
                {
                    listaNF.add(nf.id);
                    nf.BI_COL_Estado__c='Pendiente';
                }
                update scope;
                String trama=armarXml('SERV. CORPORATIVO - NOVEDADES',scope);
                GenerarTramaEnviarDavox(listaNF,trama,idMsSeleccionada);
            }
        }
        String armadoMS='';
        if(msgEnviado.length()>1)
        {
            armadoMS='Se enviaron las Siguientes Modificaciones de servicio a Davox '+msgEnviado;
        }
        if(msgNOEnviado.length()>1)
        {
            armadoMS+='\nLa siguientes MS no tienen Fecha de Incio RFB y NO fueron enviadas '+ msgNOEnviado;
        }

        BI_COL_Useful_messages_cls.mostrarMensaje(BI_COL_Useful_messages_cls.INFO, armadoMS);
        this.lstMS=null;
        //PageReference acctPage =new PageReference('/'+idCliente);
        //acctPage.setRedirect(true);
        //return acctPage;
        return null;
    }
    
    
    /**IMPLEMENTACION PAGINADOR*/
    public Integer getPageNumber(){
        return pageNumber;
    }
    
    /**
    * Obtiene el numer de paginas de la tabla 
    * @return numero de paginas
    */
    public Integer getTotalPageNumber() {
        if (totalPageNumber == 0 && lstMS !=null){
            totalPageNumber = lstMS.size() / pageSize;
            Integer mod = lstMS.size() - (totalPageNumber * pageSize);
            if (mod > 0)
            totalPageNumber++;
        }
        return totalPageNumber;
    }
    
        public PageReference nextBtnClick() {
            BindData(pageNumber + 1);
        return null;
    
    }
    
    
    public PageReference previousBtnClick() {
            BindData(pageNumber - 1);
        return null;
    
    }
    
    public List<WrapperMS> getLstMS(){
        return pageMS;
    }
    
    /**
    * Posiciona el registro segun el numero de pagina
    * @param newPageIndex Indice de la pagina
    * @return 
    */
    private void BindData(Integer newPageIndex){
        try{
            pageMS = new List<WrapperMS>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            if (newPageIndex > pageNumber){
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }else{
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
            
            for(WrapperMS wMS : lstMS){
                counter++;
                if (counter > min && counter <= max){
                    pageMS.add(wMS);
                }
                    
            }
            pageNumber = newPageIndex;
            
            if (pageMS == null || pageMS.size() <= 0)
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Data not available for this view.'));
        }
        catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }   
    }
    
    public Boolean getPreviousButtonEnabled(){
        return !(pageNumber > 1);
    }
    
    public Boolean getNextButtonDisabled(){
        if (lstMS == null){ return true;}
        else{return ((pageNumber * pageSize) >= lstMS.size());}
    
    }
    
    public Integer getPageSize(){
        return pageSize;
    }
    
    /**
    * Obtiene el listado de MS activas del cliente
    * 
    * @param idCliente Id del cliente asociado al FUN
    * @return List<BI_COL_Modificacion_de_Servicio__c> modificaciones de servicios activas
    */
    
    public List<BI_COL_Modificacion_de_Servicio__c> getMSSolicitudServicio(String idCliente)
    {
        System.debug(':: Cliente:' + idCliente );
        String consultaSOQL = 'SELECT ' +
                'm.Id, ' +
                'm.Name, ' +
                'm.BI_COL_RepresentanteLegal__c, '+
                'm.BI_COL_Autorizacion_facturacion__c,' +
                'm.BI_COL_Oportunidad__c, ' +
                'm.BI_COL_Oportunidad__r.Account.id,' +
                'm.BI_COL_Producto__r.NE__ProdId__r.Familia_MKTG_Nivel_1__c, ' +
                'm.BI_COL_Producto__r.Name, ' + 
                //'m.BI_COL_Producto__r.Descripcion_Referencia__c, ' +
                'm.BI_COL_Producto__r.NE__ProdId__r.Family_Local__c, '+
                'm.BI_COL_Duracion_meses__c, ' +  
                //'m.BI_COL_Producto__r.Tipo_Anexo__c, ' +
                'm.BI_COL_Codigo_unico_servicio__r.Name, '+
                'm.BI_COL_Fecha_instalacion_servicio_RFS__c, ' +
                'm.BI_COL_FUN__r.BI_COL_Codigo_paquete__c, ' +
                'm.BI_COL_Codigo_unico_servicio__r.BI_COL_DS_Medio_Acceso__c, '+
                'm.BI_COL_Total_servicio__c,'+
                'm.BI_COL_Estado__c, ' +
                'm.BI_COL_Clasificacion_Servicio__c,'+
                //'m.BI_COL_Producto__r.Backup__c, ' +
                //'m.BI_COL_Producto__r.tipo_producto__c, '+
                'm.BI_COL_Fecha_inicio_de_cobro_RFB__c, '+
                'm.BI_COL_Fecha_fin__c, '+
                'm.BI_COL_Estado_orden_trabajo__c, '+
                'm.BI_COL_Fecha_comprometida_baja_tecnica__c '+
            'FROM '+ 
                'BI_COL_Modificacion_de_Servicio__c m '+ 
            'WHERE '+ 
                ' m.BI_COL_Codigo_unico_servicio__r.BI_COL_Oportunidad__r.AccountId = \''+idCliente+'\' ' ;

        consultaSOQL+=' and BI_COL_Autorizacion_facturacion__c=false ';
        consultaSOQL+=' and (BI_COL_Clasificacion_Servicio__c=\'BAJA\' OR (BI_COL_Estado__c=\'Activa\' and BI_COL_Estado_orden_trabajo__c = \'Ejecutado\') )';
        consultaSOQL+=' and BI_COL_Fecha_liberacion_OT__c<>null';
        consultaSOQL+=' and BI_COL_Clasificacion_Servicio__c<>\'CESION CONTRATO\'';

        if(Busqueda!=null && Busqueda!='' ) 
        {
            String dsBuscar='';
            if(Busqueda.contains(','))
            {
                List<String> lstDSB=Busqueda.split(',');
                for(String ls:lstDSB)
                {
                    dsBuscar=dsBuscar.trim();
                    dsBuscar+='\''+ls+'\',';
                }
                dsBuscar=dsBuscar.subString(0,dsBuscar.length()-1);
            }
            else
            {
                dsBuscar=dsBuscar.trim();
                dsBuscar='\''+Busqueda+'\'';
            }
            consultaSOQL+=' AND Name IN ('+dsBuscar+')'; 
        }
        consultaSOQL+=' limit 990';
        System.debug('\n\n ----->>>>'+consultaSOQL+'\n\n');
        List<BI_COL_Modificacion_de_Servicio__c> lstMS=new List<BI_COL_Modificacion_de_Servicio__c>();
        lstMS=Database.query(consultaSOQL);
        System.debug('\n\n ******* La lista de MS de la consulta es: ' + lstMS);
        return lstMS;
    }
    
    /**
    * Acción para seleccionar todos los registros de MS
    * @return PageReference
    */
    public PageReference action_seleccionarTodos(){
        system.debug('Cambiando valor Seleccionado');
        if(this.lstMS != null){
            system.debug('Si hay valores en MS');
            for(WrapperMS wMS : this.lstMS) {
                system.debug('Convirtiendo un valor');
                wMS.seleccionado = this.todosMarcados;
            }
        }
        return null;
    }
    
    /**Clase Wrapper que administra la selección de registros*/
        public class WrapperMS{
        public boolean seleccionado {get;set;}
        public Integer diasServicio {get;set;}
        public Double valorMulta{get; set;}
        public BI_COL_Modificacion_de_Servicio__c ms {get;set;}
        
        public WrapperMS(BI_COL_Modificacion_de_Servicio__c ms, boolean seleccionado)
        {
            this.ms = ms;
            this.seleccionado = seleccionado;
        }
    }
    
    
    
    @Future (callout=true)
    public static void GenerarTramaEnviarDavox(List<String> lstIdNF,String trama, List<String> lstIdMS)
    {
        String strRespuestaServicio='';
        try
        {
            System.debug('inicia cambio de ms GenerarTramaEnviarDavox--->'+lstIdMS);
            BI_COL_Davox_wsdl.BasicServiceWSSOAP ws = new BI_COL_Davox_wsdl.BasicServiceWSSOAP();
            ws.timeout_x=12000;
            System.Debug('\n\n EL XML ENVIADO AL SERVICIO WEB ES: ' + trama+' \n\n');
            strRespuestaServicio = ws.add(trama);
            System.Debug('\n\n ####### Respuesta del XML: ' + strRespuestaServicio+' ####### \n\n');
            ActualizaNovedades(lstIdNF, strRespuestaServicio);
        }
        catch(Exception ex)
        {
            List<BI_Log__c> novedades=[Select BI_COL_Causal_Error__c,BI_COL_Estado__c,BI_COL_Informacion_recibida__c from BI_Log__c where id in:lstIdNF];
            for(BI_Log__c n : novedades)
            {
                n.BI_COL_Causal_Error__c ='Error enviando la Novedad a DAVOX: '+ex.getMessage();
                n.BI_COL_Estado__c='Error Conexion';
                n.BI_COL_Informacion_recibida__c=ex.getMessage();
            }
            update novedades;
            actualizarMsFallo(lstIdMS);
            System.Debug('Error del XML: ' + ex.getMessage());
        }
    }


    public static void actualizarMsFallo(List<String> lstIdMS){
        System.debug('inicia cambio de ms NF1--->'+lstIdMS);
        List<BI_COL_Modificacion_de_Servicio__c> lstMSRespaldo = [select id,BI_COL_Fecha_de_facturacion__c,BI_COL_Autorizacion_facturacion__c from BI_COL_Modificacion_de_Servicio__c where id IN:lstIdMS];
        for(BI_COL_Modificacion_de_Servicio__c ms: lstMSRespaldo){
            ms.BI_COL_Fecha_de_facturacion__c=null;
            ms.BI_COL_Autorizacion_facturacion__c=false;
        }
        System.debug('ms modificadas------>'+lstMSRespaldo);
        update lstMSRespaldo;
    }
    public static String armarXml(String typeX,List<BI_Log__c> novedades)
    {
        System.debug('Entra a armarXml');
        XmlStreamWriter xmlWriter=new XmlStreamWriter();
        xmlWriter.writeStartDocument('utf-8','1.0');
        xmlWriter.writeStartElement(null, 'WS', null);

        xmlWriter.writeStartElement(null, 'USER', null);
        xmlWriter.writeCharacters('USER');
        xmlWriter.writeEndElement();

        xmlWriter.writeStartElement(null, 'PASSWORD', null);
        xmlWriter.writeCharacters('PASSWORD');
        xmlWriter.writeEndElement();
        xmlWriter.writeStartElement(null, 'DATEPROCESS', null);
        xmlWriter.writeCharacters(String.valueOf(System.Today()));
        xmlWriter.writeEndElement();
        xmlWriter.writeStartElement(null, 'PACKCODE', null);
        //xmlWriter.writeCharacters(getPackageCode());
        xmlWriter.writeEndElement();

        xmlWriter.writeStartElement(null, 'SEPARATOR', null);
        xmlWriter.writeCharacters('Õ');
        xmlWriter.writeEndElement();

        xmlWriter.writeStartElement(null, 'TYPE', null);
        xmlWriter.writeCharacters(typeX);
        xmlWriter.writeEndElement();

        xmlWriter.writeStartElement(null, 'VALUES', null);

        for (BI_Log__c n : novedades)
        {
            System.debug(n.BI_COL_Informacion_Enviada__c);
            xmlWriter.writeStartElement(null, 'VALUE', null);
            xmlWriter.writeCharacters(n.BI_COL_Informacion_Enviada__c);
            xmlWriter.writeEndElement();
        }
        xmlWriter.writeEndElement();

        xmlWriter.writeStartElement(null, 'COUNT', null);
        xmlWriter.writeCharacters(String.valueOf(novedades.size()));
        xmlWriter.writeEndElement();

        xmlWriter.writeEndElement();

        string tramaDavox = xmlWriter.getXmlString();
        tramaDavox = tramaDavox.replace('<?xml version="1.0" encoding="utf-8"?>', '');
        xmlWriter = new XmlStreamWriter();

        return tramaDavox;
    }
    
    public static void ActualizaNovedades(List<String> idnov, string strXmlRespuesta)
    {
        List<BI_Log__c> novedades=[Select id,BI_COL_Causal_Error__c,BI_COL_Estado__c,BI_COL_Informacion_recibida__c,BI_COL_Modificacion_Servicio__r.ID 
                            from BI_Log__c 
                            where id in:idnov and BI_COL_Estado__c='Procesando'];
        Map<String,String> mapNovError=new Map<String,String>(); 
        System.debug('Entra a ActuaizarNovedades');
        if (strXmlRespuesta.length() != 0)
        {
            System.debug('La respuesta tiene una longitud superior a 0');
            XmlStreamReader reader = new XmlStreamReader(strXmlRespuesta);
            string strSeparador;
            string RespuestaDavox;
            Map<String, String> CatalogoCodigos = LeeCatalogoCodigos();
            while (reader.hasNext())
            {
                System.debug('Se recorre el XmlStreamReader');
                if (reader.getEventType() == XmlTag.START_ELEMENT)
                {
                    if (reader.getLocalName() == Label.BI_COL_LbSeparator)
                    {
                        strSeparador = Parse(reader);
                    }
                    else if (reader.getLocalName() == label.BI_COL_LbValue)
                    {
                        RespuestaDavox = Parse(reader);
                        System.debug('\n\n RespuestaDavox: '+RespuestaDavox+'\n\n');
                        string[] respuestaNovedad = RespuestaDavox.split(strSeparador);
                        mapNovError.put(respuestaNovedad[0],respuestaNovedad[1]);
                    }
                }
                reader.next();
            }
            if(mapNovError.size()>0)
            {
                System.debug('\n\n Ingreso al if MapNoveda \n\n');
                for(BI_Log__c n : novedades)
                {
                    String rtaDavox=mapNovError.get(n.BI_COL_Modificacion_Servicio__r.ID);
                    n.BI_COL_Informacion_recibida__c=CatalogoCodigos.get(rtaDavox);
                    if(rtaDavox.equals('100'))
                    {
                        n.BI_COL_Estado__c = 'Pendiente Sincronización';
                    }
                    else
                    {
                        n.BI_COL_Estado__c = 'Devuelto';
                    }
                }
                update novedades;
            }
        }
        System.debug('Termina de actualizar novedades');
    }


    private static Map<String,String> LeeCatalogoCodigos()
    {
        Map<String, String> CatalogoCodigos = new Map<String, String>();
        for(BI_COL_RespuestasDavox__c c : [Select Name, BI_COL_Descripcion_Codigo__c from BI_COL_RespuestasDavox__c])
        {
            CatalogoCodigos.put(c.Name, c.BI_COL_Descripcion_Codigo__c);
        }
        return CatalogoCodigos;     
    }
    
    private static String Parse(XmlStreamReader reader)
    {
        System.debug('Entra al Parse de xml');
        String strTextoDevuelve = '';
        while(reader.hasNext())
        {
            System.debug('Se recorre el reader en el Parse');
            if (reader.getEventType() == XmlTag.END_ELEMENT)
            {
                break;
            } 
            else if (reader.getEventType() == XmlTag.CHARACTERS)
            {
                if (reader.getText() != null)
                {
                    strTextoDevuelve = reader.getText();
                }
            }
            reader.next();
        }

        return strTextoDevuelve;
    }

    //-------------------------------->>> Falta PackageCode

    //public Static String getPackageCode()
    //{
    //  PackageCode__c codPaq=[Select Name,Consecutivo__c from PackageCode__c where NombreAplicacion__c='DAVOX' limit 1];
    //  codPaq.Consecutivo__c=Math.floor((Math.random()*10000)+1);//codPaq.Consecutivo__c+1;
    //  codPaq.Name=codPaq.Consecutivo__c+'-ASD654AS'+System.now();
    //  update codPaq; 
    //  return codPaq.Name;
    //}

    
    @Future (callout=true)
    public static void contratoCadenaInterfaz(list<string> idC){
        
        BI_COL_Davox_wsdl.BasicServiceWSSOAP ws = new BI_COL_Davox_wsdl.BasicServiceWSSOAP();
        ws.timeout_x=12000;

        //Informacion para homologar los campos del contrato contra la interfas de MS
        set<string> homologacion=new set<string>{'BI_COL_Oportunidad__r.','BI_COL_FUN__r.BI_COL_Contrato__r.'};
        
        //orden interfaz
        map<integer,estructuraV> estructuraList=new map<integer,estructuraV>();
        
        list<BI_COL_Configuracion_campos_interfases__c> confIntf= new list<BI_COL_Configuracion_campos_interfases__c>([SELECT BI_COL_Nombre_campo__c,BI_COL_Orden__c,BI_COL_Formato_Campo__c,BI_COL_Tipo_campo__c,BI_COL_Decimales__c,BI_COL_Separador_Interno__c,BI_COL_Longitud__c
                    FROM BI_COL_Configuracion_campos_interfases__c
                    WHERE BI_COL_Configuracion_Interfaz__r.BI_COL_Direccion__c = 'SALIENTE' and BI_COL_Configuracion_Interfaz__r.BI_COL_Interfaz__c='DAVOXPLUS' and BI_COL_Configuracion_Interfaz__r.BI_COL_Tipo_transaccion__c='SERV. CORPORATIVO - NOVEDADES' AND 
                    (BI_COL_Nombre_campo__c LIKE 'BI_COL_Oportunidad__r.Account.%' OR BI_COL_Nombre_campo__c LIKE 'BI_COL_FUN__r.BI_COL_Contrato__r.%')]);
        
        //Limpia la relacion para que se pueda buscar en el contrato
        for(BI_COL_Configuracion_campos_interfases__c ci:confIntf){
            string campo=ci.BI_COL_Nombre_campo__c.toLowerCase() ;
            for(string si:homologacion){
                System.debug('Valor:\n'+campo+'para limpiar'+si);
                campo=campo.remove(si);
                System.debug('FINAL:\n'+campo); 
            }

            estructuraList.put((integer)ci.BI_COL_Orden__c,new estructuraV(campo,ci.BI_COL_Orden__c,ci.BI_COL_Formato_Campo__c,ci.BI_COL_Tipo_campo__c,ci.BI_COL_Decimales__c,ci.BI_COL_Separador_Interno__c,ci.BI_COL_Longitud__c));
        }
        //campos adicionales
        estructuraList.put(17,new estructuraV('Owner.BI_DNI_Per__c',17.00,'','Texto',0,'',20.00));
        estructuraList.put(27,new estructuraV('Id',27.00,'','Texto',0,'',20.00));
        estructuraList.put(90,new estructuraV('LastModifiedBy.Username',90.00,'','Texto',0,'',80.00));
        
        string camposContratoString='';
        
        for (integer i:estructuraList.keySet()){
            //ignora duplicados
            if(!camposContratoString.containsIgnoreCase(estructuraList.get(i).nombre))
            camposContratoString+=estructuraList.get(i).nombre +', ';       
        }
        
        /*
        
        for(string s:camposContrato){
            camposContratoString+=s+', ';
        }
        */
        String strAux='(';
        
        for(string s:idC){
            //camposContratoString+=s+', ';
            strAux +='\''+ s+'\',';
        }

        strAux =strAux.removeEnd(',');
        straux+= ')';
        
        camposContratoString =camposContratoString.removeEnd(', ');
        
        camposContratoString='SELECT '+camposContratoString+' FROM Contract WHERE id IN '+strAux;        
        
        //camposContratoString =camposContratoString.removeEnd(', ');
        
        //camposContratoString='SELECT '+camposContratoString+' FROM Contract WHERE id IN :idC';
        
        System.debug('\ncamposContratoString'+camposContratoString);
        
        list<Contract> ctr=Database.query(camposContratoString);
        
        AggregateResult totalPosiciones=[SELECT MAX(BI_COL_Orden__c) total
            FROM BI_COL_Configuracion_campos_interfases__c
            WHERE BI_COL_Configuracion_Interfaz__r.BI_COL_Direccion__c = 'SALIENTE' and BI_COL_Configuracion_Interfaz__r.BI_COL_Interfaz__c='DAVOXPLUS' and BI_COL_Configuracion_Interfaz__r.BI_COL_Tipo_transaccion__c='SERV. CORPORATIVO - NOVEDADES'];
            
        Integer total =Integer.valueOf(totalPosiciones.get('total'));           
        
        list<string>listCadena=new list<string>();
        
        //Se recorren los registros y los campos de los mismos para ser limpiados y formar una cadena para davox
        for(Contract c:ctr){
        string cadenaInterfaz='';

            for(Integer i=1;total>=i;i++){
                string valorC='';
                if(estructuraList.get(i)!=null){
                    if(i==28){
                        estructuraList.get(i).nombre='BI_Numero_de_documento__c';
                    }
                    
                    if(estructuraList.get(i).nombre.contains('.')){
                        if(estructuraList.get(i).nombre.split('\\x2E').size()<=2 && !estructuraList.get(i).nombre.contains(',')){
                            list<string> relacion=estructuraList.get(i).nombre.split('\\x2E');
                            valorC=estructuraList.get(i).getLimpiar(c.getSObject(relacion[0]).get(relacion[1]));
                        }           
                    }else{
                        if(!estructuraList.get(i).nombre.contains(',')){
                            valorC=estructuraList.get(i).getLimpiar(c.get(estructuraList.get(i).nombre));
                        }
                    }
                }
                if(i==30){
                    valorC=label.BI_COL_LbModificacionContra;
                }
                
                cadenaInterfaz+=valorC+'Õ';             
            }
            cadenaInterfaz=cadenaInterfaz.removeEnd('Õ');
            System.debug('cadenaInterfaz'+cadenaInterfaz);
            listCadena.add(cadenaInterfaz);
        }
        
        string pck=Math.floor((Math.random()*10000)+1)+'-ASD654AS'+System.now();
        string xml =BI_COL_CustomerAccount_ctr.armadoXMLMultiple(listCadena,'SERV. CORPORATIVO - NOVEDADES',pck);
        
        try
        {
            system.debug('\nxml'+xml);
            string strRespuestaServicio = ws.add(xml);
            system.debug(strRespuestaServicio);
            
            BI_COL_CustomerAccount_ctr cut=new BI_COL_CustomerAccount_ctr();
            strRespuestaServicio=cut.retornaCadena(strRespuestaServicio);
            
            list<string>logInfo=strRespuestaServicio.split('Õ');
            
            insert new BI_Log__c( BI_COL_Contrato__c=logInfo[0],BI_Descripcion__c=xml,BI_COL_Informacion_recibida__c=strRespuestaServicio,BI_COL_Estado__c='EXITOSO');

        }catch(exception e){
            
            insert new BI_Log__c(BI_COL_Contrato__c=ctr[0].id,BI_Descripcion__c=xml,BI_COL_Informacion_recibida__c='Error:\n'+e.getmessage(),BI_COL_Estado__c='FALLIDO');
        
        }
        
    }
    
    /*public static object multipleCampos(){
    
    }*/
    
    public object relacion(sobject sobj,string campo){
        
        object obj;
        
        
        return obj;
    }
    
    /**
    * Clase interna encargada de almacenar la informacion de
    * los campos de la interfaz y limpiar los mismos
    * segun las necesidades de davox.
    **/
    
    public class estructuraV{
        public string nombre{get;set;}
        public string Orden{get;set;}
        public string Formato_Campo{get;set;}
        public string Tipo_campo{get;set;}
        public double Decimales{get;set;}
        public string Separador_Interno{get;set;}
        public double Longitud{get;set;}
        
        public estructuraV(string nombre,decimal Orden,string Formato_Campo,string Tipo_campo,decimal Decimales,string Separador_Interno,decimal Longitud){
            this.nombre=nombre;
            this.Orden=String.valueOf(Orden);
            this.Formato_Campo=Formato_Campo;
            this.Tipo_campo=Tipo_campo;
            this.Decimales=Double.valueOf(Decimales);
            this.Separador_Interno=Separador_Interno;
            this.Longitud=Longitud;
        }
        
        /**
        * Returns un string limpio segun las necesidades de dabox. 
        *
        * @param  object es un campo para se convertido a string
        * @return      cadena de caracteres acordada con davox
        */
        public string getLimpiar(object obj){
        
            string retorna='';
            
            if(obj==null){
                return retorna;      
            }
                    //BI_COL_Basic_pck_cls.fn_FormatearValor(datoCampo, strTipoCampo, strFormato, dbDecimales, separadorDecimal)
            retorna=BI_COL_Basic_pck_cls.fn_FormatearValor(obj, Tipo_campo, Formato_Campo, Decimales, Separador_Interno);
            
            if(Tipo_campo=='Texto' && nombre!='id'){
                retorna=retorna.toUpperCase();
            }
            
            
            if(retorna.length() > Longitud)
                retorna=retorna.trim().substring(0,Integer.valueOf(Longitud));
        
            return retorna;
        }
        
    
    }
    

    

    
    

}