/**
* Avanxo Colombia
* @author           Oscar Alejandro Jimenez Forero
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-08-19      Oscar Alejandro Jimenez Forero (OAJF)   Class Created
*			 1.1    2017-26-19		Pedro Párraga							Added calls to methods formatoNumero() and formatoFecha()
*			 1.2    23-02-2017      Jaime Regidor                   		Adding Campaign Values, Adding Catalog Country
					13/03/2017		Marta Gonzalez(Everis)					REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
					20/09/2017      Angel F. Santaliestra   				Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest
private class BI_COL_Response_Processing_Viability_tst {

	public static BI_COL_GenerateNewBilling_cls				objGenBil;
	public static Account 									objCuenta;
	public static Account 									objCuenta2;
	public static Contact 									objContacto;
	public static Campaign 									objCampana;
	public static Contract 									objContrato;
	public static BI_COL_Anexos__c 							objAnexos;
	public static Opportunity 								objOportunidad;
	public static BI_Col_Ciudades__c 						objCiudad;
	public static BI_Sede__c 								objSede;
	public static BI_Punto_de_instalacion__c 				objPuntosInsta;
	public static BI_COL_Descripcion_de_servicio__c 		objDesSer;
	public static BI_COL_Modificacion_de_Servicio__c 		objMS;
	public static BI_COL_Cobro_Parcial__c 					objCobroParcial;

	public static List <Profile> 							lstPerfil;
	public static List <User>	 							lstUsuarios;
	public static List <UserRole> 							lstRoles;
	public static List<Campaign> 							lstCampana;
	public static List<BI_COL_manage_cons__c > 				lstManege;
	public static List<BI_COL_Modificacion_de_Servicio__c>	lstMS;
	public static List<BI_COL_Descripcion_de_servicio__c>	lstDS;
	public static List<recordType>							lstRecTyp;
	public static BI_COL_Viabilidad_Tecnica__c 				objViabilidad;
	public static BI_COL_Configuracion_campos_interfases__c objConfCamposInterfases;
	public static BI_COL_Configuracion_campos_interfases__c objConfCamposInterfases1;
	public static BI_COL_Configuracion_campos_interfases__c objConfCamposInterfases2;
	public static BI_COL_Configuracion_campos_interfases__c objConfCamposInterfases3;
	public static List<BI_COL_Viabilidad_Tecnica__c> lstViav;
	public static User objUsuario;

	public static void crearData() {
		//perfiles
		BI_bypass__c objBibypass = new BI_bypass__c();
		objBibypass.BI_migration__c = true;
		insert objBibypass;

		//Roles
		lstRoles                = new List <UserRole>();
		lstRoles                = [Select id, Name from UserRole where Name = 'Telefónica Global'];
		system.debug('datos Rol ' + lstRoles);

		//Cuentas
		objCuenta 										= new Account();
		objCuenta.Name                                  = 'prueba';
		objCuenta.BI_Country__c                         = 'Argentina';
		objCuenta.TGS_Region__c                         = 'América';
		objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta.CurrencyIsoCode                       = 'COP';
		objCuenta.BI_Fraude__c                          = false;
		objCuenta.BI_Segment__c                         = 'test';
		objCuenta.BI_Subsegment_Regional__c             = 'test';
		objCuenta.BI_Territory__c                       = 'test';
		insert objCuenta;
		system.debug('datos Cuenta ' + objCuenta);

		//Ciudad
		objCiudad = new BI_Col_Ciudades__c ();
		objCiudad.Name 						= 'Test City';
		objCiudad.BI_COL_Pais__c 			= 'Test Country';
		objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
		insert objCiudad;
		System.debug('Datos Ciudad ===> ' + objSede);

		//Contactos
		objContacto                                     = new Contact();
		objContacto.LastName                         	= 'Test';
		objContacto.BI_Country__c                   	= 'Argentina';
		objContacto.CurrencyIsoCode                 	= 'COP';
		objContacto.AccountId                       	= objCuenta.Id;
		objContacto.BI_Tipo_de_contacto__c   			= 'Administrador Canal Online';
		objContacto.Phone         						= '1234567';
		objContacto.MobilePhone       					= '3104785925';
		objContacto.Email        						= 'pruebas@pruebas1.com';
		objContacto.BI_COL_Direccion_oficina__c   		= 'Dirprueba';
		objContacto.BI_COL_Ciudad_Depto_contacto__c  	= objCiudad.Id;
		//REING-INI
        //objContacto.BI_Tipo_de_contacto__c         	= 'Autorizado';
        objContacto.BI_Tipo_de_documento__c 			= 'Otros';
        objContacto.BI_Numero_de_documento__c 			= '00000000X';
        objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
        objContacto.BI_Tipo_de_contacto__c          	= 'General';	
        //REING_FIN
		Insert objContacto;

		//catalogo
		NE__Catalog__c objCatalogo 						= new NE__Catalog__c();
		objCatalogo.Name 								= 'prueba Catalogo';
		objCatalogo.BI_country__c 						= 'Argentina';
		insert objCatalogo;

		//Campaña
		objCampana 										= new Campaign();
		objCampana.Name 								= 'Campaña Prueba';
		objCampana.BI_Catalogo__c 						= objCatalogo.Id;
		objCampana.BI_COL_Codigo_campana__c 			= 'prueba1';
		objCampana.BI_Country__c 						= 'Argentina';
        objCampana.BI_Opportunity_Type__c 				= 'Fijo';
        objCampana.BI_SIMP_Opportunity_Type__c 			= 'Alta'; 
		insert objCampana;
		lstCampana 										= new List<Campaign>();
		lstCampana 										= [select Id, Name from Campaign where Id = : objCampana.Id];
		system.debug('datos Cuenta ' + lstCampana);

		//Tipo de registro
		lstRecTyp 										= [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
		system.debug('datos de FUN ' + lstRecTyp);

		//Contrato
		objContrato 									= new Contract ();
		objContrato.AccountId 							= objCuenta.Id;
		objContrato.StartDate 							=  System.today().addDays(+5);
		objContrato.BI_COL_Formato_Tipo__c 				= 'Contrato Marco';
		objContrato.ContractTerm 						= 12;
		objContrato.BI_COL_Presupuesto_contrato__c 		= 1000000;
		objContrato.StartDate							= System.today().addDays(+5);
		insert objContrato;
		System.debug('datos Contratos ===>' + objContrato);

		//FUN
		objAnexos               						= new BI_COL_Anexos__c();
		objAnexos.Name          						= 'FUN-0041414';
		objAnexos.RecordTypeId  						= lstRecTyp[0].Id;
		objAnexos.BI_COL_Contrato__c   					= objContrato.Id;
		insert objAnexos;
		System.debug('\n\n\n Sosalida objAnexos ' + objAnexos + '\n ====> objAnexos.Id ' + objAnexos.Id);

		//Configuracion Personalizada
		BI_COL_manage_cons__c objConPer 				= new BI_COL_manage_cons__c();
		objConPer.Name                  				= 'Viabilidades';
		objConPer.BI_COL_Numero_Viabilidad__c 			= 46;
		objConPer.BI_COL_ConsProyecto__c     			= 1;
		objConPer.BI_COL_ValorConsecutivo__c  			= 1;
		lstManege                      					= new List<BI_COL_manage_cons__c >();
		lstManege.add(objConPer);
		insert lstManege;
		System.debug('======= configuracion personalizada ======= ' + lstManege);

		//Oportunidad
		objOportunidad                                      	= new Opportunity();
		objOportunidad.Name                                   	= 'prueba opp';
		objOportunidad.AccountId                              	= objCuenta.Id;
		objOportunidad.BI_Country__c                          	= 'Argentina';
		objOportunidad.CloseDate                              	= System.today().addDays(+5);
		objOportunidad.StageName                              	= 'F5 - Solution Definition';
		objOportunidad.CurrencyIsoCode                        	= 'COP';
		objOportunidad.Certa_SCP__contract_duration_months__c 	= 12;
		objOportunidad.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
		//objOportunidad.OwnerId                                	= lstUsuarios[0].id;
		insert objOportunidad;
		system.debug('Datos Oportunidad' + objOportunidad);
		List<Opportunity> lstOportunidad 	= new List<Opportunity>();
		lstOportunidad 						= [select Id from Opportunity where Id = : objOportunidad.Id];
		system.debug('datos ListaOportunida ' + lstOportunidad);

		//Direccion
		objSede 								= new BI_Sede__c();
		objSede.BI_COL_Ciudad_Departamento__c 	= objCiudad.Id;
		objSede.BI_Direccion__c 				= 'Test Street 123 Number 321';
		objSede.BI_Localidad__c 				= 'Test Local';
		objSede.BI_COL_Estado_callejero__c 		= System.Label.BI_COL_LbValor3EstadoCallejero;
		objSede.BI_COL_Sucursal_en_uso__c 		= 'Libre';
		objSede.BI_Country__c 					= 'Colombia';
		objSede.Name 							= 'Test Street 123 Number 321, Test Local Colombia';
		objSede.BI_Codigo_postal__c 			= '12356';
		insert objSede;
		System.debug('Datos Sedes ===> ' + objSede);

		//Sede
		objPuntosInsta 						= new BI_Punto_de_instalacion__c ();
		objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
		objPuntosInsta.BI_Sede__c          = objSede.Id;
		objPuntosInsta.BI_Contacto__c      = objContacto.id;
		objPuntosInsta.Name 				= 'Name pruebas';
		insert objPuntosInsta;
		System.debug('Datos Sucursales ===> ' + objPuntosInsta);

		//DS
		objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
		objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
		objDesSer.CurrencyIsoCode               			= 'COP';
		insert objDesSer;
		lstDS 	= [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];
		System.debug('Data lista DS ====> ' + lstDS);

		//MS
		objMS                         				= new BI_COL_Modificacion_de_Servicio__c();
		objMS.BI_COL_FUN__c           				= objAnexos.Id;
		objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
		objMS.BI_COL_Oportunidad__c 				= objOportunidad.Id;
		objMS.BI_COL_Bloqueado__c 					= false;
		objMS.BI_COL_Estado__c 						= 'Pendiente';//label.BI_COL_lblActiva;
		objMS.BI_COL_Sucursal_de_Facturacion__c 	= objPuntosInsta.Id;
		objMs.BI_COL_Sucursal_Origen__c 			= objPuntosInsta.Id;
		objMS.BI_COL_Autorizacion_facturacion__c	= false;
		objMS.BI_COL_Fecha_liberacion_OT__c 		= System.today();
		objMS.BI_COL_Clasificacion_Servicio__c		= 'BAJA';
		objMS.BI_COL_Estado_orden_trabajo__c 		= 'Ejecutado';
		insert objMS;

		objViabilidad = new BI_COL_Viabilidad_Tecnica__c();
		objViabilidad.BI_COL_Modificacion_de_Servicio__c = objMS.Id;
		objViabilidad.BI_COL_Estado__c = 'Solicitado';
		objViabilidad.BI_COL_Numero_viabilidad__c = 1;
		insert objViabilidad;
		lstViav = [select id, name from BI_COL_Viabilidad_Tecnica__c];
		System.debug('Data lista Viabilidad ====> ' + lstViav);

		objConfCamposInterfases = new BI_COL_Configuracion_campos_interfases__c();
		objConfCamposInterfases.BI_COL_Adicion__c = false;
		objConfCamposInterfases.BI_COL_Altas__c = false;
		objConfCamposInterfases.BI_COL_Bajas__c = false;
		objConfCamposInterfases.BI_COL_Calificador_Texto__c = null;
		objConfCamposInterfases.BI_COL_Cambio_Cuenta__c = false;
		objConfCamposInterfases.BI_COL_Cambio_Plan__c = false;
		objConfCamposInterfases.BI_COL_Cambio_servicio_alta__c = false;
		objConfCamposInterfases.BI_COL_Configuracion_Interfaz__c = null;
		objConfCamposInterfases.BI_COL_Convertir_mayuscula__c = false;
		objConfCamposInterfases.BI_COL_Decimales__c = null;
		objConfCamposInterfases.BI_COL_Descripcion__c = 'Envio de Instalaciones';
		objConfCamposInterfases.BI_COL_Direccion__c = 'ENTRANTE';
		objConfCamposInterfases.BI_COL_Downgrade__c = false;
		objConfCamposInterfases.BI_COL_Formato_Campo__c = null;
		objConfCamposInterfases.BI_COL_Funcion_Especial__c = null;
		objConfCamposInterfases.BI_COL_Identificador__c = false;
		objConfCamposInterfases.BI_COL_ID_Colombia__c = 'a3Qg0000000ExpJEAS';
		objConfCamposInterfases.BI_COL_Inactivacion__c = false;
		objConfCamposInterfases.BI_COL_Ingreso__c = false;
		objConfCamposInterfases.BI_COL_Interfaz__c = 'TRS-VIATEC';
		objConfCamposInterfases.BI_COL_Linea__c = null;
		objConfCamposInterfases.BI_COL_Longitud__c = null;
		objConfCamposInterfases.BI_COL_Nombre_campo__c = null;
		objConfCamposInterfases.BI_COL_Objeto__c = 'BI_COL_Modificacion_de_Servicio__c';
		objConfCamposInterfases.BI_COL_Observaciones__c = null;
		objConfCamposInterfases.BI_COL_Orden__c = null;
		objConfCamposInterfases.BI_COL_Permite_nulos__c = false;
		objConfCamposInterfases.BI_COL_Renegociacion__c = false;
		objConfCamposInterfases.BI_COL_Retiro__c = false;
		objConfCamposInterfases.BI_COL_Segmento__c = null;
		objConfCamposInterfases.BI_COL_Separador_Decimal__c = '.';
		objConfCamposInterfases.BI_COL_Separador_Interno__c = '.';
		objConfCamposInterfases.BI_COL_Separador__c = '~';
		objConfCamposInterfases.BI_COL_Servicios_de_IngenierIa__c = false;
		objConfCamposInterfases.BI_COL_Susp_Temporal_Facturacion__c = false;
		objConfCamposInterfases.BI_COL_Tipo_campo__c = null;
		objConfCamposInterfases.BI_COL_Tipo_transaccion__c = 'RESPUESTA VIABILIDAD TECNICA';
		objConfCamposInterfases.BI_COL_Traslado__c = false;
		objConfCamposInterfases.BI_COL_Upgrade__c = false;
		objConfCamposInterfases.BI_COL_Valor_predeterminado__c = null;
		objConfCamposInterfases.Name = 'Cabecera';
		insert objConfCamposInterfases;

		objConfCamposInterfases1 = new BI_COL_Configuracion_campos_interfases__c();
		objConfCamposInterfases1.BI_COL_Adicion__c = false;
		objConfCamposInterfases1.BI_COL_Altas__c = false;
		objConfCamposInterfases1.BI_COL_Bajas__c = false;
		objConfCamposInterfases1.BI_COL_Calificador_Texto__c = null;
		objConfCamposInterfases1.BI_COL_Cambio_Cuenta__c = false;
		objConfCamposInterfases1.BI_COL_Cambio_Plan__c = false;
		objConfCamposInterfases1.BI_COL_Cambio_servicio_alta__c = false;
		objConfCamposInterfases1.BI_COL_Configuracion_Interfaz__c = objConfCamposInterfases.id;
		objConfCamposInterfases1.BI_COL_Convertir_mayuscula__c = false;
		objConfCamposInterfases1.BI_COL_Decimales__c = 1;
		objConfCamposInterfases1.BI_COL_Descripcion__c = 'Envio de Instalaciones';
		objConfCamposInterfases1.BI_COL_Direccion__c = 'Saliente';
		objConfCamposInterfases1.BI_COL_Downgrade__c = false;
		objConfCamposInterfases1.BI_COL_Formato_Campo__c = '#.###,##';
		objConfCamposInterfases1.BI_COL_Funcion_Especial__c = null;
		objConfCamposInterfases1.BI_COL_Identificador__c = false;
		objConfCamposInterfases1.BI_COL_ID_Colombia__c = 'a3Qg0000000ExsKEAS';
		objConfCamposInterfases1.BI_COL_Inactivacion__c = false;
		objConfCamposInterfases1.BI_COL_Ingreso__c = false;
		objConfCamposInterfases1.BI_COL_Interfaz__c = 'SISGOT-INSTALACION';
		objConfCamposInterfases1.BI_COL_Linea__c = null;
		objConfCamposInterfases1.BI_COL_Longitud__c = null;
		objConfCamposInterfases1.BI_COL_Nombre_campo__c = 'BI_COL_Estado__c';
		objConfCamposInterfases1.BI_COL_Objeto__c = '';
		objConfCamposInterfases1.BI_COL_Observaciones__c = null;
		objConfCamposInterfases1.BI_COL_Orden__c = null;
		objConfCamposInterfases1.BI_COL_Permite_nulos__c = false;
		objConfCamposInterfases1.BI_COL_Renegociacion__c = false;
		objConfCamposInterfases1.BI_COL_Retiro__c = false;
		objConfCamposInterfases1.BI_COL_Segmento__c = null;
		objConfCamposInterfases1.BI_COL_Separador_Decimal__c = '.';
		objConfCamposInterfases1.BI_COL_Separador_Interno__c = ',';
		objConfCamposInterfases1.BI_COL_Separador__c = '~';
		objConfCamposInterfases1.BI_COL_Servicios_de_IngenierIa__c = false;
		objConfCamposInterfases1.BI_COL_Susp_Temporal_Facturacion__c = false;
		objConfCamposInterfases1.BI_COL_Tipo_campo__c = 'Texto';
		objConfCamposInterfases1.BI_COL_Tipo_transaccion__c = 'INSTALACIONES';
		objConfCamposInterfases1.BI_COL_Traslado__c = false;
		objConfCamposInterfases1.BI_COL_Upgrade__c = false;
		objConfCamposInterfases1.BI_COL_Valor_predeterminado__c = null;
		objConfCamposInterfases1.Name = 'Cabecera';
		insert objConfCamposInterfases1;

		objConfCamposInterfases2 = new BI_COL_Configuracion_campos_interfases__c();
		objConfCamposInterfases2.BI_COL_Adicion__c = false;
		objConfCamposInterfases2.BI_COL_Altas__c = false;
		objConfCamposInterfases2.BI_COL_Bajas__c = false;
		objConfCamposInterfases2.BI_COL_Calificador_Texto__c = null;
		objConfCamposInterfases2.BI_COL_Cambio_Cuenta__c = false;
		objConfCamposInterfases2.BI_COL_Cambio_Plan__c = false;
		objConfCamposInterfases2.BI_COL_Cambio_servicio_alta__c = false;
		objConfCamposInterfases2.BI_COL_Configuracion_Interfaz__c = null;
		objConfCamposInterfases2.BI_COL_Convertir_mayuscula__c = false;
		objConfCamposInterfases2.BI_COL_Decimales__c = null;
		objConfCamposInterfases2.BI_COL_Descripcion__c = 'Envio de Instalaciones';
		objConfCamposInterfases2.BI_COL_Direccion__c = 'ENTRANTE';
		objConfCamposInterfases2.BI_COL_Downgrade__c = false;
		objConfCamposInterfases2.BI_COL_Formato_Campo__c = null;
		objConfCamposInterfases2.BI_COL_Funcion_Especial__c = null;
		objConfCamposInterfases2.BI_COL_Identificador__c = false;
		objConfCamposInterfases2.BI_COL_ID_Colombia__c = 'a3Qg0000000ExpJEAT';
		objConfCamposInterfases2.BI_COL_Inactivacion__c = false;
		objConfCamposInterfases2.BI_COL_Ingreso__c = false;
		objConfCamposInterfases2.BI_COL_Interfaz__c = 'TRS-VIATEC-DESTINO';
		objConfCamposInterfases2.BI_COL_Linea__c = null;
		objConfCamposInterfases2.BI_COL_Longitud__c = null;
		objConfCamposInterfases2.BI_COL_Nombre_campo__c = null;
		objConfCamposInterfases2.BI_COL_Objeto__c = 'BI_COL_Modificacion_de_Servicio__c';
		objConfCamposInterfases2.BI_COL_Observaciones__c = null;
		objConfCamposInterfases2.BI_COL_Orden__c = null;
		objConfCamposInterfases2.BI_COL_Permite_nulos__c = false;
		objConfCamposInterfases2.BI_COL_Renegociacion__c = false;
		objConfCamposInterfases2.BI_COL_Retiro__c = false;
		objConfCamposInterfases2.BI_COL_Segmento__c = null;
		objConfCamposInterfases2.BI_COL_Separador_Decimal__c = '.';
		objConfCamposInterfases2.BI_COL_Separador_Interno__c = '.';
		objConfCamposInterfases2.BI_COL_Separador__c = '~';
		objConfCamposInterfases2.BI_COL_Servicios_de_IngenierIa__c = false;
		objConfCamposInterfases2.BI_COL_Susp_Temporal_Facturacion__c = false;
		objConfCamposInterfases2.BI_COL_Tipo_campo__c = null;
		objConfCamposInterfases2.BI_COL_Tipo_transaccion__c = 'RESPUESTA VIABILIDAD TECNICA';
		objConfCamposInterfases2.BI_COL_Traslado__c = false;
		objConfCamposInterfases2.BI_COL_Upgrade__c = false;
		objConfCamposInterfases2.BI_COL_Valor_predeterminado__c = null;
		objConfCamposInterfases2.Name = 'Cabecera';
		insert objConfCamposInterfases2;

		objConfCamposInterfases3 = new BI_COL_Configuracion_campos_interfases__c();
		objConfCamposInterfases3.BI_COL_Adicion__c = false;
		objConfCamposInterfases3.BI_COL_Altas__c = false;
		objConfCamposInterfases3.BI_COL_Bajas__c = false;
		objConfCamposInterfases3.BI_COL_Calificador_Texto__c = null;
		objConfCamposInterfases3.BI_COL_Cambio_Cuenta__c = false;
		objConfCamposInterfases3.BI_COL_Cambio_Plan__c = false;
		objConfCamposInterfases3.BI_COL_Cambio_servicio_alta__c = false;
		objConfCamposInterfases3.BI_COL_Configuracion_Interfaz__c = objConfCamposInterfases2.id;
		objConfCamposInterfases3.BI_COL_Convertir_mayuscula__c = false;
		objConfCamposInterfases3.BI_COL_Decimales__c = 1;
		objConfCamposInterfases3.BI_COL_Descripcion__c = 'Envio de Instalaciones';
		objConfCamposInterfases3.BI_COL_Direccion__c = 'Saliente';
		objConfCamposInterfases3.BI_COL_Downgrade__c = false;
		objConfCamposInterfases3.BI_COL_Formato_Campo__c = '#.###,##';
		objConfCamposInterfases3.BI_COL_Funcion_Especial__c = null;
		objConfCamposInterfases3.BI_COL_Identificador__c = false;
		objConfCamposInterfases3.BI_COL_ID_Colombia__c = 'a3Qg0000000ExsKEAU';
		objConfCamposInterfases3.BI_COL_Inactivacion__c = false;
		objConfCamposInterfases3.BI_COL_Ingreso__c = false;
		objConfCamposInterfases3.BI_COL_Interfaz__c = 'TRS-VIATEC-DESTINO';
		objConfCamposInterfases3.BI_COL_Linea__c = null;
		objConfCamposInterfases3.BI_COL_Longitud__c = null;
		objConfCamposInterfases3.BI_COL_Nombre_campo__c = 'BI_COL_Estado__c';
		objConfCamposInterfases3.BI_COL_Objeto__c = '';
		objConfCamposInterfases3.BI_COL_Observaciones__c = null;
		objConfCamposInterfases3.BI_COL_Orden__c = null;
		objConfCamposInterfases3.BI_COL_Permite_nulos__c = false;
		objConfCamposInterfases3.BI_COL_Renegociacion__c = false;
		objConfCamposInterfases3.BI_COL_Retiro__c = false;
		objConfCamposInterfases3.BI_COL_Segmento__c = null;
		objConfCamposInterfases3.BI_COL_Separador_Decimal__c = '.';
		objConfCamposInterfases3.BI_COL_Separador_Interno__c = ',';
		objConfCamposInterfases3.BI_COL_Separador__c = '~';
		objConfCamposInterfases3.BI_COL_Servicios_de_IngenierIa__c = false;
		objConfCamposInterfases3.BI_COL_Susp_Temporal_Facturacion__c = false;
		objConfCamposInterfases3.BI_COL_Tipo_campo__c = 'Texto';
		objConfCamposInterfases3.BI_COL_Tipo_transaccion__c = 'INSTALACIONES';
		objConfCamposInterfases3.BI_COL_Traslado__c = false;
		objConfCamposInterfases3.BI_COL_Upgrade__c = false;
		objConfCamposInterfases3.BI_COL_Valor_predeterminado__c = 'Hoy';
		objConfCamposInterfases3.Name = 'Cabecera';
		insert objConfCamposInterfases3;
	}

	@isTest
	static void test_method_1() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();
			Test.startTest();
			BI_COL_Response_Processing_Viability_ws.procesarRespuestasViatec(lstDS[0].Name, '1', lstViav[0].Name, '', 'test', '', 'test');
			Test.stopTest();
		}
	}

	@isTest
	static void test_method_2() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();
			objConfCamposInterfases1.BI_COL_Nombre_campo__c = 'BI_COL_Numero_prueba__c';
			objConfCamposInterfases1.BI_COL_Tipo_campo__c = 'Numérico';
			update objConfCamposInterfases1;
			objConfCamposInterfases3.BI_COL_Nombre_campo__c = 'BI_COL_Numero_prueba__c';
			objConfCamposInterfases3.BI_COL_Tipo_campo__c = 'Numérico';
			update objConfCamposInterfases3;
			Test.startTest();
			BI_COL_Response_Processing_Viability_ws.procesarRespuestasViatec(lstDS[0].Name, '1', lstViav[0].Name, '10\\~', 'test', '', '10\\~');
			Test.stopTest();
		}
	}

	@isTest
	static void test_method_3() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();
			objConfCamposInterfases1.BI_COL_Nombre_campo__c = 'BI_COL_Fec__c';
			objConfCamposInterfases1.BI_COL_Formato_Campo__c = 'yyyy-MM-dd-HH:mm:ss';
			objConfCamposInterfases1.BI_COL_Tipo_campo__c = 'Fecha';
			update objConfCamposInterfases1;
			objConfCamposInterfases3.BI_COL_Nombre_campo__c = 'BI_COL_Fec__c';
			objConfCamposInterfases3.BI_COL_Formato_Campo__c = 'yyyy-MM-dd-HH:mm:ss';
			objConfCamposInterfases3.BI_COL_Tipo_campo__c = 'Fecha';
			update objConfCamposInterfases3;
			Test.startTest();
			BI_COL_Response_Processing_Viability_ws.procesarRespuestasViatec(lstDS[0].Name, '1', lstViav[0].Name, '2015-08-20-11:45:00', 'test', '', '2015-08-20-11:45:00');
			Test.stopTest();
		}
	}

	@isTest
	static void test_method_4() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();
			objConfCamposInterfases1.BI_COL_Nombre_campo__c = 'BI_COL_Logico_prueba__c';
			objConfCamposInterfases1.BI_COL_Formato_Campo__c = '1 : 0';
			objConfCamposInterfases1.BI_COL_Tipo_campo__c = 'Lógico';
			update objConfCamposInterfases1;
			objConfCamposInterfases3.BI_COL_Nombre_campo__c = 'BI_COL_Logico_prueba__c';
			objConfCamposInterfases3.BI_COL_Formato_Campo__c = '1 : 0';
			objConfCamposInterfases3.BI_COL_Tipo_campo__c = 'Lógico';
			update objConfCamposInterfases3;
			Test.startTest();
			BI_COL_Response_Processing_Viability_ws.procesarRespuestasViatec(lstDS[0].Name, '1', lstViav[0].Name, '1', 'test', '', '1');
			BI_COL_Response_Processing_Viability_ws.procesarRespuestasViatec(lstDS[0].Name, '1', lstViav[0].Name, '0', 'test', '', '0');
			objConfCamposInterfases1.BI_COL_Formato_Campo__c = 'S : N';
			update objConfCamposInterfases1;
			objConfCamposInterfases3.BI_COL_Formato_Campo__c = 'S : N';
			update objConfCamposInterfases3;
			BI_COL_Response_Processing_Viability_ws.procesarRespuestasViatec(lstDS[0].Name, '1', lstViav[0].Name, 'S', 'test', '', 'S');
			BI_COL_Response_Processing_Viability_ws.procesarRespuestasViatec(lstDS[0].Name, '1', lstViav[0].Name, 'N', 'test', '', 'N');
			Test.stopTest();
		}
	}

	@isTest
	static void test_method_5() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();
			delete objViabilidad;
			Test.startTest();
			BI_COL_Response_Processing_Viability_ws.procesarRespuestasViatec(lstDS[0].Name, '1', lstViav[0].Name, '', 'test', '', '');
			Test.stopTest();
		}
	}

	@isTest
	static void test_method_6() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();
			Test.startTest();
			BI_COL_Response_Processing_Viability_ws.procesarRespuestasViatec(lstDS[0].Name, '1', lstViav[0].Name, 'test\\~test1', 'test', '', 'test\\~test1');
			Test.stopTest();
		}
	}

	@isTest
	static void test_method_7() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();
			BI_COL_Response_Processing_Viability_ws.isTestMethod = true;
			Test.startTest();
			BI_COL_Response_Processing_Viability_ws.procesarRespuestasViatec(lstDS[0].Name, '1', lstViav[0].Name, '', 'test', '', 'test');
			Test.stopTest();
		}
	}

	@isTest
	static void test_method_8() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			crearData();
			BI_COL_Response_Processing_Viability_ws.mensajeLog objmsg = new BI_COL_Response_Processing_Viability_ws.mensajeLog();
			objmsg.consecutivo = lstViav[0].Name;
			List<BI_COL_Response_Processing_Viability_ws.mensajeLog> lstobjmsg = new List<BI_COL_Response_Processing_Viability_ws.mensajeLog>();
			BI_COL_Response_Processing_Viability_ws.Response objresp = new BI_COL_Response_Processing_Viability_ws.Response();
			lstobjmsg.add(objmsg);
			Test.startTest();
			objresp = BI_COL_Response_Processing_Viability_ws.respViabilidad(lstobjmsg);
			BI_COL_Response_Processing_Viability_ws.formatoNumero(3.5, '#.###,##', 1, '.');
			BI_COL_Response_Processing_Viability_ws.formatoNumero(3.5, '###.##', 1, ',');
			BI_COL_Response_Processing_Viability_ws.formatoNumero(3.5, '###', 1, '.');
			BI_COL_Response_Processing_Viability_ws.formatoFecha(System.now(), 'Hoy','#.###,##');
			BI_COL_Response_Processing_Viability_ws.formatoFecha(System.now(), 'Ayer','#.###,##');
			BI_COL_Response_Processing_Viability_ws.formatoFecha(System.now(), 'Mañana','#.###,##');
			BI_COL_Response_Processing_Viability_ws.formatoFecha(System.now(), 'Año actual','#.###,##');
			BI_COL_Response_Processing_Viability_ws.formatoFecha(System.now(), 'Mes actual','#.###,##');
			BI_COL_Response_Processing_Viability_ws.formatoFecha(System.now(), 'Ahora','#.###,##');
			Test.stopTest();
		}
	}

}