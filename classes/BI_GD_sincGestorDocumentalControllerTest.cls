/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Borja María
Company:        Everis España
Description:    Test para la Clase BI_GD_CalloutManager

History:

<Date>                      <Author>                        <Change Description>
10/08/2017                 Borja María         		    	Versión Inicial.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
//(SeeAllData=true)

public class BI_GD_sincGestorDocumentalControllerTest {
    
    
    @isTest static void BI_GD_sincGestorDocumentalControllerTest1() {
        Contract contrato = new Contract(BI_FVI_Autoriza__c = true,
                                         BI_FVI_Cobertura__c = false,
                                         BI_FVI_Autoriza_representante__c = false,
                                         BI_FVI_desconoce_CPF__c = false,
                                         BI_FVI_Control_Parental__c = false,
                                         BI_FVI_IdFileTxn__c = null,
                                         BI_COL_Formato_Tipo__c = 'Tipo Cliente',
                                         BI_COL_Tipo_contrato__c = 'Condiciones Uniformes',
                                         BI_COL_Presupuesto_contrato__c  =12000,
                                         BI_FVI_No_de_linea__c = '10',
                                         BI_Indefinido__c = 'No',
                                         StartDate = system.Today());
        Account account = new Account(Name = 'nameAccount',
                                      BI_Country__c = 'ARG',
                                      BI_Tipo_de_identificador_fiscal__c = 'Cliente exterior',BI_Segment__c = 'Empresas',
                                      BI_Subsegment_Regional__c = 'Corporate',
                                      BI_Territory__c = 'CABA',
                                      BI_Sector__c = 'Comercio',
                                      Sector__c = 'Private',
                                      BI_Subsector__c= 'Banca');
        
        insert(account);
        
        
        contrato.AccountId= account.Id;
        insert contrato;
        ApexPages.StandardController stdController = new ApexPages.StandardController(contrato);

        test.startTest();
        BI_GD_sincGestorDocumentalController out = new BI_GD_sincGestorDocumentalController(stdController);
        test.stopTest();
    }
    

}