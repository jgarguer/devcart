@isTest
global class CWP_AC_TestGenerateData { 
	
	public Profile p {get;set;}
	public User u {get;set;}

	public String mail {get;set;}
	public String xmlToLoad{get;set;}

	public Account account{get;set;}
	
	public Asset asset1 {get;set;}
	public NE__Catalog_Header__c catHead {get;set;}
	public NE__Catalog__c cat{get;set;}
	public NE__Commercial_Model__c	commModel{get;set;}
	public NE__Catalog_Category__c catCat{get;set;}
	public RecordType RTProd{get;set;}
	public NE__Product__c prod1{get;set;}
	public NE__Product__c prod2{get;set;}
	public NE__Product__c prod3{get;set;}
	public NE__Product__c prod4{get;set;}
	public NE__Catalog_Item__c catItem1{get;set;}
	public NE__Catalog_Item__c catItem2{get;set;}
	public NE__Catalog_Item__c catItem3{get;set;}
	public NE__Catalog_Item__c catItem4{get;set;}
	public NE__Family__c family{get;set;}
	public NE__Family__c family1{get;set;}

	public NE__DynamicPropertyDefinition__c prop{get;set;}
	public NE__DynamicPropertyDefinition__c prop2{get;set;}
	public NE__DynamicPropertyDefinition__c prop3 {get;set;}
	public NE__ProductFamilyProperty__c	productFamilyProp {get;set;}
	public NE__ProductFamilyProperty__c	productFamilyProp2 {get;set;}
	public NE__ProductFamily__c	prodFamily{get;set;}
	
	public NE__ProductFamilyProperty__c pfp1{get;set;} 
	public NE__ProductFamilyProperty__c pfp2{get;set;}
	public NE__ProductFamily__c pf1{get;set;}
	public NE__ProductFamily__c pf2{get;set;}
	public NE__ProductFamily__c pf3{get;set;}
	public NE__ProductFamily__c pf4{get;set;}

	public NE__Contract_Header__c contractHead{get;set;} 
	public NE__Contract__c contract{get;set;}
	public NE__Contract_Line_Item__c contrLineIt1{get;set;}
	public NE__Contract_Line_Item__c contrLineIt2{get;set;}
	public NE__Contract_Line_Item__c contrLineIt3{get;set;}
	public NE__Contract_Line_Item__c contrLineIt4{get;set;}

				
	global NE__Order__c ord{get;set;}
	global NE__Order__c ord2{get;set;}
	public NE__Order_Header__c ordHeader{get;set;}
	public NE__OrderItem__c ordit1 {get;set;}
	public NE__OrderItem__c ordit2 {get;set;}
	public NE__OrderItem__c ordit1SL {get;set;}
	public NE__OrderItem__c ordit2SL {get;set;}
	public NE__OrderItem__c ordit1TL {get;set;}
	public NE__OrderItem__c ordit2TL {get;set;}
	public NE__OrderItem__c ordit1FL {get;set;}
	
	public NE__Order_Item_Attribute__c attr1 {get;set;}
	public NE__Order_Item_Attribute__c attr2 {get;set;}
	public NE__Order_Item_Attribute__c attr3 {get;set;}
	public NE__Order_Item_Attribute__c attr4 {get;set;}
	public NE__Order_Item_Attribute__c attr5 {get;set;}

	public String reqId{get;set;}
	public Bit2WinHUB__Bulk_Import_Request__c bir {get;set;}
	public Bit2WinHUB__Bulk_Import_Request__c bir2 {get;set;}
	public Bit2WinHUB__Bulk_Import_Request__c bir3 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Request__c bcr {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Request__c bcr2 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Request__c bcr3 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Request__c bcr4 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Request__c bcr5 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Request__c bcr6 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Request__c bcr7 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Request__c bcr8 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Request__c bcr9 {get;set;}

	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir1 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir2 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir3 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir4 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir5 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir6 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir7 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir8 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir9 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir10 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir11 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir12 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir13 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir14 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir15 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir16 {get;set;}
	public Bit2WinHUB__Bulk_Configuration_Item_Request__c bcir17 {get;set;}

	public Attachment att1{get;set;}
	public Attachment att2{get;set;}
	public Attachment att3{get;set;}
	public Attachment att4{get;set;}
	public Attachment att5{get;set;}
	public Attachment att6{get;set;}
	public Attachment att7{get;set;}
	public Attachment att8{get;set;}
	public Attachment att9{get;set;}
	public Attachment att10{get;set;}

	public Task task {get;set;}
	public Task task2 {get;set;}
	public Task task3 {get;set;}

	public Bit2WinHUB__HUB_Settings__c hs {get;set;}
	public Integer blocksize {get;set;} 

	//public NE__MapObject__c adjustmentMatrix {get;set;}
	//public NE__MapObjectItem__c  objetItemsMap {get;set;}

	public CWP_AC_TestGenerateData() 
    {
			
			System.runAs(new User(Id = UserInfo.getUserId())) {
			
			account	=	new Account(name = 'ThisIsATest',
									BI_Segment__c='Empresas',
									BI_Subsegment_Regional__c='Multinacionales',
									Sector__c='Private',
									BI_Sector__c='Industria',
									BI_Subsector__c='Industria Alimentos');
    		insert account;
    		
	    	catHead = new NE__Catalog_Header__c(Name='Test Catalog', NE__Name__c='Test Catalog');
	    	insert catHead;
	    	cat = new NE__Catalog__c(NE__Catalog_Header__c=catHead.Id, NE__StartDate__c=Datetime.now(), NE__Active__c = true);
	    	insert cat;
	    	commModel	=	 new NE__Commercial_Model__c(NE__Catalog_Header__c = catHead.id, NE__CatalogId__c = cat.id);
	    	insert commModel;
	    	catCat = new NE__Catalog_Category__c(Name='Category', NE__CatalogId__c=cat.Id);
	    	insert catCat;
	    	RTProd = [SELECT Id FROM RecordType WHERE (SobjectType = 'NE__Product__c' OR SobjectType = 'Product__c') AND Name = 'Standard'];
	    	prod1 = new NE__Product__c(Name='Test Product', RecordTypeId=RTProd.Id);
	    	insert prod1;
	    	prod2 = new NE__Product__c(Name='Test Product 2', RecordTypeId=RTProd.Id);
	    	insert prod2;
	    	prod3 = new NE__Product__c(Name='Test Product 3', RecordTypeId=RTProd.Id);
	    	insert prod3;
			prod4 = new NE__Product__c(Name='Test Product 4', RecordTypeId=RTProd.Id);
    		insert prod4;
	    	catItem1 = new NE__Catalog_Item__c(NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, NE__Recurring_Charge_Frequency__c='Monthly');
	    	insert catItem1;
	    	catItem2 = new NE__Catalog_Item__c(NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod2.Id, NE__Parent_Catalog_Item__c=catItem1.Id, NE__Root_Catalog_Item__c=catItem1.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=10.00, NE__BaseRecurringCharge__c=0.00, NE__Recurring_Charge_Frequency__c ='Monthly');
	    	insert catItem2;
	    	catItem3 = new NE__Catalog_Item__c(NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod3.Id, NE__Parent_Catalog_Item__c=catItem2.Id, NE__Root_Catalog_Item__c=catItem1.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=10.00, NE__BaseRecurringCharge__c=0.00, NE__Recurring_Charge_Frequency__c ='Monthly');
	    	insert catItem3;
			catItem4 = new NE__Catalog_Item__c(NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=10.00, NE__BaseRecurringCharge__c=0.00);
    		insert catItem4;
	    	family	=	new NE__Family__c(name = 'FamilyTest');
	    	insert family;
			family1 = new NE__Family__c(Name='Test Family 1');
    		insert family1;
    		prop	=	new NE__DynamicPropertyDefinition__c(name = 'TestProperty',NE__Type__c ='Enumerated');
    		insert prop;
			prop2	=	new NE__DynamicPropertyDefinition__c(name = 'TestProperty2');
    		insert prop2;
    		productFamilyProp	=	new NE__ProductFamilyProperty__c(NE__PropId__c = prop.id,NE__FamilyId__c = family.id, NE__Required__c='true');
    		insert productFamilyProp;
			productFamilyProp2	=	new NE__ProductFamilyProperty__c(NE__PropId__c = prop2.id,NE__FamilyId__c = family.id, NE__Required__c='true');
    		insert productFamilyProp2;
    		prodFamily	=	new NE__ProductFamily__c(NE__ProdId__c = prod1.id, NE__FamilyId__c = family.id);
    		insert prodFamily;
    		
			contractHead = new NE__Contract_Header__c(NE__Name__c='Contract Header');
			insert contractHead;
    		ord = new NE__Order__c(NE__AccountId__c=account.Id, NE__BillAccId__c=account.Id, NE__ServAccId__c=account.Id, NE__CatalogId__c=cat.Id, NE__OrderStatus__c='Pending', NE__ConfigurationStatus__c='Valid',NE__Contract_Header__c=contractHead.Id, NE__Order_date__c=Datetime.now().AddDays(1),NE__Type__c='New');
    		insert ord;
			ord2 = new NE__Order__c(NE__AccountId__c=account.Id, NE__BillAccId__c=account.Id, NE__ServAccId__c=account.Id, NE__CatalogId__c=cat.Id, NE__OrderStatus__c='Pending', NE__ConfigurationStatus__c='Valid',NE__Contract_Header__c=contractHead.Id, NE__Order_date__c=Datetime.now().AddDays(1),NE__Type__c='Disconnection');
    		insert ord2;
   			ordHeader = new NE__Order_Header__c(NE__OrderId__c=ord.Id);
   			insert ordHeader;

    		ordit1 = new NE__OrderItem__c(NE__OrderId__c=ord.Id, NE__ProdId__c=prod1.Id,NE__CatalogItem__c=catItem1.Id,NE__RecurringChargeFrequency__c =null, NE__Qty__c=1,NE__Action__c='Add',NE__Root_Order_Item__c=null, NE__Status__c ='In progress');
    		insert ordit1;
    		ordit2 = new NE__OrderItem__c(NE__OrderId__c=ord.Id, NE__ProdId__c=prod2.Id, NE__Qty__c=1, NE__Status__c ='In progress',NE__Root_Order_Item__c=null);
    		insert ordit2;

			ordit1SL = new NE__OrderItem__c(NE__OrderId__c=ord.Id,NE__Parent_Order_Item__c= ordit1.Id, NE__ProdId__c=prod1.Id,NE__CatalogItem__c=catItem2.Id,NE__RecurringChargeFrequency__c =null, NE__Qty__c=1,NE__Action__c='Add',NE__Root_Order_Item__c= ordit1.Id);
    		insert ordit1SL;
			ordit2SL = new NE__OrderItem__c(NE__OrderId__c=ord.Id,NE__Parent_Order_Item__c= ordit2.Id, NE__ProdId__c=prod2.Id, NE__Qty__c=1, NE__Root_Order_Item__c=ordit2.Id);
    		insert ordit2SL;
			ordit1TL = new NE__OrderItem__c(NE__OrderId__c=ord.Id,NE__Parent_Order_Item__c= ordit1SL.Id, NE__ProdId__c=prod1.Id,NE__CatalogItem__c=catItem3.Id,NE__RecurringChargeFrequency__c =null, NE__Qty__c=1,NE__Action__c='Add');
    		insert ordit1TL;
			ordit2TL = new NE__OrderItem__c(NE__OrderId__c=ord.Id,NE__Parent_Order_Item__c= ordit2SL.Id, NE__ProdId__c=prod2.Id, NE__Qty__c=1);
    		insert ordit2TL;
			ordit1FL = new NE__OrderItem__c(NE__OrderId__c=ord.Id,NE__Parent_Order_Item__c= ordit1TL.Id, NE__ProdId__c=prod1.Id,NE__CatalogItem__c=catItem3.Id,NE__RecurringChargeFrequency__c =null, NE__Qty__c=1,NE__Action__c='Add');
    		insert ordit1FL;
    		
			attr1 = new NE__Order_Item_Attribute__c(Name='Test Attribute 1', NE__Order_Item__c=ordit1.Id, NE__FamPropId__c=productFamilyProp.Id, NE__Value__c='Premier Monthly Rate',NE__Action__c='Add');
    		insert attr1;
			attr2 = new NE__Order_Item_Attribute__c(NE__Order_Item__c=ordit2.Id, NE__FamPropId__c=productFamilyProp.Id, NE__Value__c='Premier Monthly Rate');
    		insert attr2;
			attr3 = new NE__Order_Item_Attribute__c(Name='Test Attribute 3', NE__Order_Item__c=ordit1SL.Id, NE__FamPropId__c=productFamilyProp.Id, NE__Value__c='Second Monthly Rate',NE__Action__c='Add');
    		insert attr3;
			attr4 = new NE__Order_Item_Attribute__c(Name='Test Attribute 4', NE__Order_Item__c=ordit1TL.Id, NE__FamPropId__c=productFamilyProp.Id, NE__Value__c='Third Monthly Rate',NE__Action__c='Add');
    		insert attr4;
			attr5 = new NE__Order_Item_Attribute__c(Name='Test Attribute 5', NE__Order_Item__c=ordit1FL.Id, NE__FamPropId__c=productFamilyProp.Id, NE__Value__c='Fourth Monthly Rate',NE__Action__c='Add');
    		insert attr5;


			RecordType RTAttribute = [SELECT Id FROM RecordType WHERE (SobjectType = 'NE__DynamicPropertyDefinition__c' OR SobjectType = 'DynamicPropertyDefinition__c') AND Name = 'Not Enumerated'];
			prop3 = new NE__DynamicPropertyDefinition__c(Name='Test Attribute 3', NE__Type__c='Number', RecordTypeId=RTAttribute.Id);
    		insert prop3;
    		
			pfp1 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=family1.Id, NE__PropId__c=prop.Id);
    		insert pfp1;
    		pfp2 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=family1.Id, NE__PropId__c=prop2.Id);
    		insert pfp2;
    		pf1 = new NE__ProductFamily__c(NE__FamilyId__c=family1.Id, NE__ProdId__c=prod1.Id);
    		insert pf1;
    		pf2 = new NE__ProductFamily__c(NE__FamilyId__c=family1.Id, NE__ProdId__c=prod2.Id);
    		insert pf2;
    		pf3 = new NE__ProductFamily__c(NE__FamilyId__c=family1.Id, NE__ProdId__c=prod3.Id);
    		insert pf3;

			
			contract = new NE__Contract__c(NE__Contract_Header__c=contractHead.Id, NE__Contract_Policy__c='pricing', NE__Start_Date__c=Datetime.now(), NE__Version__c=1);
			insert contract;
			contrLineIt1 = new NE__Contract_Line_Item__c(NE__Contract__c=contract.Id, NE__Base_OneTime_Fee__c=null, NE__Base_Recurring_Charge__c=null, NE__Commercial_Product__c=prod1.Id);
			insert contrLineIt1;
			contrLineIt2 = new NE__Contract_Line_Item__c(NE__Contract__c=contract.Id, NE__Base_OneTime_Fee__c=null, NE__Base_Recurring_Charge__c=null, NE__Commercial_Product__c=prod2.Id);
			insert contrLineIt2;
			contrLineIt3 = new NE__Contract_Line_Item__c(NE__Contract__c=contract.Id, NE__Base_OneTime_Fee__c=null, NE__Base_Recurring_Charge__c=null, NE__Commercial_Product__c=prod3.Id);
			insert contrLineIt3;
			contrLineIt4 = new NE__Contract_Line_Item__c(NE__Contract__c=contract.Id, NE__Base_OneTime_Fee__c=null, NE__Base_Recurring_Charge__c=null, NE__Commercial_Product__c=prod4.Id);
			insert contrLineIt4;


			reqId = 'req-0123456';

			bir = new Bit2WinHUB__Bulk_Import_Request__c (Bit2WinHUB__Request_Id__c='req-0123456',Bit2WinHUB__Status__c='New', Bit2WinHUB__Step__c='CreateNTXML',Bit2WinHUB__Total_Blocks__c =10,Bit2WinHUB__Processed_Blocks__c=0);
			insert bir;
			bir2 = new Bit2WinHUB__Bulk_Import_Request__c (Bit2WinHUB__Request_Id__c='req-0123456',Bit2WinHUB__Status__c='New', Bit2WinHUB__Step__c='CreateOrders',Bit2WinHUB__Total_Blocks__c =10,Bit2WinHUB__Processed_Blocks__c=0);
			insert bir2;
			bir3 = new Bit2WinHUB__Bulk_Import_Request__c (Bit2WinHUB__Request_Id__c='req-0123456',Bit2WinHUB__Status__c='New', Bit2WinHUB__Step__c='CreateOrders',Bit2WinHUB__Total_Blocks__c =10,Bit2WinHUB__Processed_Blocks__c=10);
			insert bir3;
			bcr = new Bit2WinHUB__Bulk_Configuration_Request__c(Bit2WinHUB__Configuration__c=ord2.Id, Bit2WinHUB__Request_Id__c='req-0123456', Bit2WinHUB__Status__c='Completed', Bit2WinHUB__Description__c='');
    		insert bcr;
			bcr2 = new Bit2WinHUB__Bulk_Configuration_Request__c(Bit2WinHUB__Configuration__c=ord.Id, Bit2WinHUB__Request_Id__c='req-0123456', Bit2WinHUB__Status__c='New', Bit2WinHUB__Description__c='');
    		insert bcr2;
			bcr3 = new Bit2WinHUB__Bulk_Configuration_Request__c(Bit2WinHUB__Configuration__c=null, Bit2WinHUB__Request_Id__c='req-0123456', Bit2WinHUB__Status__c='New', Bit2WinHUB__Description__c='');
    		insert bcr3;
			bcr4 = new Bit2WinHUB__Bulk_Configuration_Request__c(Bit2WinHUB__Configuration__c=null, Bit2WinHUB__Request_Id__c='req-0123456', Bit2WinHUB__Status__c='Failed', Bit2WinHUB__Description__c='ERROR');
    		insert bcr4;
			bcr5 = new Bit2WinHUB__Bulk_Configuration_Request__c(Bit2WinHUB__Configuration__c=null, Bit2WinHUB__Request_Id__c='', Bit2WinHUB__Status__c='New', Bit2WinHUB__Description__c='');
    		insert bcr5;
			bcr6 = new Bit2WinHUB__Bulk_Configuration_Request__c(Bit2WinHUB__Configuration__c=null, Bit2WinHUB__Request_Id__c='', Bit2WinHUB__Status__c='Failed', Bit2WinHUB__Description__c='');
    		insert bcr6;
			bcr7 = new Bit2WinHUB__Bulk_Configuration_Request__c(Bit2WinHUB__Configuration__c=ord.Id, Bit2WinHUB__Request_Id__c='req-0123456', Bit2WinHUB__Status__c='Working', Bit2WinHUB__Description__c='');
    		insert bcr7;
    		bcir1 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr.Id, Bit2WinHUB__Configuration_Item__c=ordit1.Id, Bit2WinHUB__Status__c='Completed');
    		insert bcir1;
			bcir2 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr4.Id, Bit2WinHUB__Configuration_Item__c=ordit2.Id, Bit2WinHUB__Status__c='Completed');
    		insert bcir2;
			bcir3 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr2.Id, Bit2WinHUB__Configuration_Item__c=null, Bit2WinHUB__Status__c='Working', Bit2WinHUB__Parent_Configuration_Item_Request__c = null);
    		insert bcir3;
			bcir7 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr2.Id, Bit2WinHUB__Configuration_Item__c=null, Bit2WinHUB__Status__c='New', Bit2WinHUB__Parent_Configuration_Item_Request__c = bcir3.Id);
    		insert bcir7;
			bcir9 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr2.Id, Bit2WinHUB__Configuration_Item__c=null, Bit2WinHUB__Status__c='New', Bit2WinHUB__Parent_Configuration_Item_Request__c = bcir7.Id);
    		insert bcir9;
			bcir10 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr2.Id, Bit2WinHUB__Configuration_Item__c=null, Bit2WinHUB__Status__c='New', Bit2WinHUB__Parent_Configuration_Item_Request__c = bcir9.Id);
    		insert bcir10;
    		bcir4 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr2.Id, Bit2WinHUB__Configuration_Item__c=ordit2.Id, Bit2WinHUB__Status__c='Completed');
    		insert bcir4;
			bcir5 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr6.Id, Bit2WinHUB__Configuration_Item__c=ordit2.Id, Bit2WinHUB__Status__c='Completed');
    		insert bcir5;
			bcir6 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr5.Id, Bit2WinHUB__Configuration_Item__c=ordit2.Id, Bit2WinHUB__Status__c='Working record created');
    		insert bcir6;
			
			//bcir17 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr5.Id, Configuration_Item__c=ordit2SL.Id, Status__c='Working second time');
    		//insert bcir17;
			
			//bcir6 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr5.Id, Configuration_Item__c=ordit2SL.Id, Status__c='Working record created');
    		//insert bcir6;
			bcir8 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr.Id, Bit2WinHUB__Configuration_Item__c=null, Bit2WinHUB__Status__c='Working', Bit2WinHUB__Parent_Configuration_Item_Request__c = null);
    		insert bcir8;

			bcir11 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr7.Id, Bit2WinHUB__Configuration_Item__c=ordit2.Id, Bit2WinHUB__Status__c='Completed', Bit2WinHUB__Parent_Configuration_Item_Request__c =null);
    		insert bcir11;
			bcir12 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr7.Id, Bit2WinHUB__Configuration_Item__c=ordit2SL.Id, Bit2WinHUB__Status__c='Completed', Bit2WinHUB__Parent_Configuration_Item_Request__c = bcir11.Id);
    		insert bcir12;
			bcir13 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr7.Id, Bit2WinHUB__Configuration_Item__c=ordit2TL.Id, Bit2WinHUB__Status__c='Completed', Bit2WinHUB__Parent_Configuration_Item_Request__c = bcir12.Id);
    		insert bcir13;


		    asset1 = new Asset (name='AssetTest', AccountId=account.Id);
			insert asset1;

			task = new Task(Subject='req-0123456');
			insert task;
			task2 = new Task(Subject='Load NTXML Request: req-0123456', Status='Completed', Description='');
			insert task2;
			task3 = new Task(Subject='Load TechXML Request: req-0123456', Status='Completed', Description='');
			insert task3;
			
			String bodyFile = 'Body of my test file';
			String nameFile1 = 'name1';
			String nameFile2 = 'name2';

			String nameFile3 = 'name3';
			String nameFile4 = 'name4';
			
			String head_ConfigurationID = 'Configuration ID';
			String head_Account = 'Account';
			String head_Catalog = 'Catalog';
			String head_Category = 'Category';
			String head_ConfItemRoot = 'Configuration Item Level 1';
			String head_ConfItemChild1 = 'Configuration Item Level 2';
			String head_ConfItemChild2 = 'Configuration Item Level 3';
			String head_Qty = 'Quantity';
			String head_Family = 'Family';
			String head_DynPropDef = 'Dynamic Property Definition';
			String head_Value = 'Value';
			String head_ID = 'ID';
			String head_Configuration = 'Configuration ';


			String csvFile = head_ConfigurationID+';'+head_Account+';'+head_Catalog+';'+head_Category+';'+head_ConfItemRoot+';'+head_Category+';'+head_ConfItemChild1
								+';'+head_Category+';'+head_ConfItemChild2+';'+head_Qty+';'+head_Family+';'+head_DynPropDef+';'+head_Value+';'+head_ID
								+';NE__Description__c;Configuration NE__Description__c\n';
        
			csvFile += 'ORD-001;'+account.Name+';'+catHead.Name+';;;;;;;;;;;;;\n';
			csvFile	+= ';;;'+catCat.Name+';'+catItem1.NE__ProductId__r.Name+';;;;;1;'+family.Name+';'+prop.Name+';This is a value;;OI 1 description;\n';
			csvFile += ';;;'+catCat.Name+';'+catItem2.NE__ProductId__r.Name+';;;;;1;;;;;OI 2 description;\n';
			csvFile += ';;;'+catCat.Name+';'+catItem3.NE__ProductId__r.Name+';;;;;1;;;;;OI 3 description;\n';
			csvFile += 'ORD-002;'+account.Name+';'+catHead.Name+';'+catCat.Name+';'+catItem1.NE__ProductId__r.Name+';;;;;10;;;;;;ORD 2 description\n';
			csvFile += 'ORD-003;'+account.Name+';'+catHead.Name+';'+catCat.Name+';'+catItem1.NE__ProductId__r.Name+';'+catCat.Name+';'+catItem2.NE__ProductId__r.Name+';'+catCat.Name+';'+catItem3.NE__ProductId__r.Name+';1;;;;;;;\n';
			/*csvFile	+= ';;;'+catCat.Name+';'+catItem1.NE__ProductId__r.Name+';;;;;1;'+family.Name+';'+prop.Name+';This is a value;;OI 1 description;\n';
			csvFile += ';;;'+catCat.Name+';'+catItem2.NE__ProductId__r.Name+';;;;;1;;;;;OI 2 description;\n';
			csvFile += ';;;'+catCat.Name+';'+catItem3.NE__ProductId__r.Name+';;;;;1;;;;;OI 3 description;\n';
*/
			String bodyFile2 = csvFile;
	
			xmlToLoad = '<Configuration><Account>'+account.Name+'</Account><ServiceAccount>'+account.Name+'</ServiceAccount><BillingAccount>'+account.Name+'</BillingAccount><Catalog>'+catHead.Name+'</Catalog><Category>'+catCat.Name+'</Category><CommercialModel relObject="NE__Commercial_Model__c" relField="NE__Source_Commercial_Model_Id__c">'+commModel.Id+'</CommercialModel>';
			xmlToLoad += '<ConfigurationItem><Category>'+catCat.Name+'</Category><BillingAccount>'+account.Name+'</BillingAccount><Account>'+account.Name+'</Account><CatalogItem>'+catItem1.Id+'</CatalogItem><Product>'+prod1.Name+'</Product><Quantity>'+1+'</Quantity>';
    		xmlToLoad += '<Attribute><Family>'+family.Name+'</Family><AttributeName>'+prop2.Name+'</AttributeName><Value>Value1</Value><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp2.Name+'</ne__fampropid__c></Attribute>';
			xmlToLoad += '</ConfigurationItem></Configuration>';
			String bodyFile3 = xmlToLoad;

			String bodyFile4 = '<NE__Order__c><Id>'+ord.Id+'</Id><ne__accountid__c>'+account.Id+'</ne__accountid__c><HoldingId__c>'+account.Id+'</HoldingId__c><ne__servaccid__c>'+account.Id+'</ne__servaccid__c><Cost_Center__c>'+account.Id+'</Cost_Center__c><Site__c>'+account.Id+'</Site__c><Authorized_User__c>AU</Authorized_User__c><NE__Contract_Account__c></NE__Contract_Account__c><SessionParameters><Parameter><name></name><value></value></Parameter></SessionParameters><ne__configuration_type__c>New</ne__configuration_type__c><ne__catalogid__c>'+cat.Id+'</ne__catalogid__c><ne__configuration_subtype__c>Standard</ne__configuration_subtype__c><ne__billingprofid__c>'+account.Id+'</ne__billingprofid__c><ne__commercialmodelid__c relObject="NE__Commercial_Model__c" relField="NE__Source_Commercial_Model_Id__c">'+commModel.Id+'</ne__commercialmodelid__c><ne__contract_header__c>'+contractHead.Id+'</ne__contract_header__c>';
			bodyFile4 += '<NE__OrderItem__c><Cost_Center__c>'+account.Id+'</Cost_Center__c><ne__account__c>'+account.Id+'</ne__account__c><ne__catalogitem__c>'+catItem1.Id+'</ne__catalogitem__c><ne__qty__c>1</ne__qty__c><ne__generate_asset_item__c>true</ne__generate_asset_item__c><ne__prodid__c relObject="NE__Product__c" relField="NE__Source_Product_Id__c">'+catItem1.NE__ProductId__c+'</ne__prodid__c><ne__commitment__c>false</ne__commitment__c><ne__service_account__c>'+account.Id+'</ne__service_account__c>';
    		bodyFile4 += '<NE__Order_Item_Attribute__c><ne__value__c>Value 1</ne__value__c><NE__Old_Value__c>Old value 1</NE__Old_Value__c><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp1.Id+'</ne__fampropid__c><name>'+prop.Name+'</name></NE__Order_Item_Attribute__c>';
			bodyFile4 += '<NE__OrderItem__c><Cost_Center__c>'+account.Id+'</Cost_Center__c><ne__recurringchargeov__c>'+catItem2.NE__BaseRecurringCharge__c+'</ne__recurringchargeov__c><ne__account__c>'+account.Id+'</ne__account__c><ne__one_time_cost__c>0.00</ne__one_time_cost__c><ne__catalogitem__c>'+catItem2.Id+'</ne__catalogitem__c><ne__recurringchargefrequency__c>Monthly</ne__recurringchargefrequency__c><ne__qty__c>1</ne__qty__c><ne__optional__c>false</ne__optional__c><ne__penalty__c>false</ne__penalty__c><ne__ispromo__c>false</ne__ispromo__c><ne__onetimefeeov__c>'+catItem2.NE__Base_OneTime_Fee__c+'</ne__onetimefeeov__c><ne__generate_asset_item__c>true</ne__generate_asset_item__c><ne__prodid__c relObject="NE__Product__c" relField="NE__Source_Product_Id__c">'+catItem2.NE__ProductId__c+'</ne__prodid__c><ne__commitment__c>false</ne__commitment__c><ne__service_account__c>'+account.Id+'</ne__service_account__c>';
    		bodyFile4 += '<NE__Order_Item_Attribute__c><ne__value__c>45445460</ne__value__c><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp2.Id+'</ne__fampropid__c><name>'+prop2.Name+'</name></NE__Order_Item_Attribute__c>';
			bodyFile4 += '<NE__OrderItem__c><Cost_Center__c>'+account.Id+'</Cost_Center__c><ne__account__c>'+account.Id+'</ne__account__c><ne__catalogitem__c>'+catItem3.Id+'</ne__catalogitem__c><ne__qty__c>1</ne__qty__c><ne__generate_asset_item__c>true</ne__generate_asset_item__c><ne__prodid__c relObject="NE__Product__c" relField="NE__Source_Product_Id__c">'+catItem3.NE__ProductId__c+'</ne__prodid__c><ne__commitment__c>false</ne__commitment__c><ne__service_account__c>'+account.Id+'</ne__service_account__c>';
    		bodyFile4 += '<NE__Order_Item_Attribute__c><ne__value__c>Value 2</ne__value__c><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp1.Id+'</ne__fampropid__c><name>'+prop.Name+'</name></NE__Order_Item_Attribute__c>';
			bodyFile4 += '<NE__OrderItem__c><Cost_Center__c>'+account.Id+'</Cost_Center__c><ne__account__c>'+account.Id+'</ne__account__c><ne__catalogitem__c>'+catItem4.Id+'</ne__catalogitem__c><ne__qty__c>1</ne__qty__c><ne__generate_asset_item__c>true</ne__generate_asset_item__c><ne__prodid__c relObject="NE__Product__c" relField="NE__Source_Product_Id__c">'+catItem4.NE__ProductId__c+'</ne__prodid__c><ne__commitment__c>false</ne__commitment__c><ne__service_account__c>'+account.Id+'</ne__service_account__c>';
			bodyFile4 += '<NE__Order_Item_Attribute__c><ne__value__c>Value 2</ne__value__c><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp1.Id+'</ne__fampropid__c><name>'+prop.Name+'</name></NE__Order_Item_Attribute__c>';
    		bodyFile4 += '</NE__OrderItem__c></NE__OrderItem__c></NE__OrderItem__c></NE__OrderItem__c></NE__Order__c>';
			bodyFile4 += bodyFile4 + bodyFile4 +bodyFile4 + bodyFile4;
			bodyFile4 += '<NE__OrderItem__c><Cost_Center__c>'+account.Id+'</Cost_Center__c><ne__account__c>'+account.Id+'</ne__account__c><ne__catalogitem__c>'+catItem4.Id+'</ne__catalogitem__c><ne__qty__c>1</ne__qty__c><ne__generate_asset_item__c>true</ne__generate_asset_item__c><ne__prodid__c relObject="NE__Product__c" relField="NE__Source_Product_Id__c">'+catItem4.NE__ProductId__c+'</ne__prodid__c><ne__commitment__c>false</ne__commitment__c><ne__service_account__c>'+account.Id+'</ne__service_account__c>';
			bodyFile4 += '<NE__Order_Item_Attribute__c><ne__value__c>Value 3</ne__value__c><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp1.Id+'</ne__fampropid__c><name>'+prop.Name+'</name></NE__Order_Item_Attribute__c><NE__OrderItem__c><Cost_Center__c>'+account.Id+'</Cost_Center__c><ne__account__c>'+account.Id+'</ne__account__c><ne__catalogitem__c>'+catItem4.Id+'</ne__catalogitem__c><ne__qty__c>1</ne__qty__c><ne__generate_asset_item__c>true</ne__generate_asset_item__c><ne__prodid__c relObject="NE__Product__c" relField="NE__Source_Product_Id__c">'+catItem4.NE__ProductId__c+'</ne__prodid__c><ne__commitment__c>false</ne__commitment__c><ne__service_account__c>'+account.Id+'</ne__service_account__c><NE__Order_Item_Attribute__c><ne__value__c>Value 2</ne__value__c><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp1.Id+'</ne__fampropid__c><name>'+prop.Name+'</name></NE__Order_Item_Attribute__c>';
			bodyFile4 += '<NE__OrderItem__c><Cost_Center__c>'+account.Id+'</Cost_Center__c><ne__account__c>'+account.Id+'</ne__account__c><ne__catalogitem__c>'+catItem4.Id+'</ne__catalogitem__c><ne__qty__c>1</ne__qty__c><ne__generate_asset_item__c>true</ne__generate_asset_item__c><ne__prodid__c relObject="NE__Product__c" relField="NE__Source_Product_Id__c">'+catItem4.NE__ProductId__c+'</ne__prodid__c><ne__commitment__c>false</ne__commitment__c><ne__service_account__c>'+account.Id+'</ne__service_account__c><NE__Order_Item_Attribute__c><ne__value__c>Value 2</ne__value__c><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp1.Id+'</ne__fampropid__c><name>'+prop.Name+'</name></NE__Order_Item_Attribute__c>';
			bodyFile4 += '<NE__OrderItem__c><Cost_Center__c>'+account.Id+'</Cost_Center__c><ne__account__c>'+account.Id+'</ne__account__c><ne__catalogitem__c>'+catItem4.Id+'</ne__catalogitem__c><ne__qty__c>1</ne__qty__c><ne__generate_asset_item__c>true</ne__generate_asset_item__c><ne__prodid__c relObject="NE__Product__c" relField="NE__Source_Product_Id__c">'+catItem4.NE__ProductId__c+'</ne__prodid__c><ne__commitment__c>false</ne__commitment__c><ne__service_account__c>'+account.Id+'</ne__service_account__c><NE__Order_Item_Attribute__c><ne__value__c>Value 2</ne__value__c><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp1.Id+'</ne__fampropid__c><name>'+prop.Name+'</name></NE__Order_Item_Attribute__c></NE__OrderItem__c></NE__OrderItem__c></NE__OrderItem__c><NE__OrderItem__c>';


			att1 = new Attachment(Name=nameFile1, Body=Blob.valueOf(bodyFile), ParentId=task.Id, Description='New');
			insert att1;
			att2 = new Attachment(Name=nameFile2, Body=Blob.valueOf(bodyFile3), ParentId=task2.Id, Description='New');
			insert att2;
			att3 = new Attachment(Name=nameFile3, Body=Blob.valueOf(bodyFile2), ParentId=bir.Id, Description='New');
			insert att3;
			att4 = new Attachment(Name=nameFile4, Body=Blob.valueOf(bodyFile4), ParentId=task3.Id, Description='New');
			insert att4;
			
			String nameFile5 = 'XmlOrder%';
			String bodyFile5 = '<NE__Order__c><Id>'+ord.Id+'</Id><ne__accountid__c>'+account.Id+'</ne__accountid__c><HoldingId__c>'+account.Id+'</HoldingId__c><ne__servaccid__c>'+account.Id+'</ne__servaccid__c><NE__BillAccId__c>'+account.Id+'</NE__BillAccId__c><NE__Order_date__c>03-27-2017</NE__Order_date__c><Site__c>'+account.Id+'</Site__c><Authorized_User__c>AU</Authorized_User__c><NE__Contract_Account__c></NE__Contract_Account__c><SessionParameters><Parameter><name></name><value></value></Parameter></SessionParameters><ne__configuration_type__c>New</ne__configuration_type__c><ne__catalogid__c>'+cat.Id+'</ne__catalogid__c><ne__configuration_subtype__c>Standard</ne__configuration_subtype__c><ne__billingprofid__c>'+account.Id+'</ne__billingprofid__c><ne__commercialmodelid__c relObject="NE__Commercial_Model__c" relField="NE__Source_Commercial_Model_Id__c">'+commModel.Id+'</ne__commercialmodelid__c><ne__contract_header__c>'+contractHead.Id+'</ne__contract_header__c></NE__Order__c>';
			att5 = new Attachment(Name=nameFile5, Body=Blob.valueOf(bodyFile5), ParentId=bcr2.Id, Description='New');
			insert att5;

			String nameFile6 = 'Test Config Item Attach';
			String bodyFile6 = '<NE__OrderItem__c><Id></Id><Cost_Center__c>'+account.Id+'</Cost_Center__c><ne__account__c>'+account.Id+'</ne__account__c><ne__catalogitem__c>'+catItem1.Id+'</ne__catalogitem__c><ne__qty__c>1</ne__qty__c><ne__generate_asset_item__c>true</ne__generate_asset_item__c><ne__prodid__c >'+prod1.Id+'</ne__prodid__c><ne__commitment__c>false</ne__commitment__c><ne__service_account__c>'+account.Id+'</ne__service_account__c>';
			bodyFile6 += '<NE__Order_Item_Attribute__c><ne__value__c>Value 3</ne__value__c><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp1.Id+'</ne__fampropid__c><name>'+prop.Name+'</name></NE__Order_Item_Attribute__c></NE__OrderItem__c>';
			
			String nameFile7 = 'Test Config Item Attach Hijo1';
			String bodyFile7 = '<NE__OrderItem__c><Id></Id><Cost_Center__c>'+account.Id+'</Cost_Center__c><ne__account__c>'+account.Id+'</ne__account__c><ne__catalogitem__c>'+catItem2.Id+'</ne__catalogitem__c><ne__qty__c>1</ne__qty__c><ne__generate_asset_item__c>true</ne__generate_asset_item__c><ne__prodid__c>'+prod2.Id+'</ne__prodid__c><ne__commitment__c>false</ne__commitment__c><ne__service_account__c>'+account.Id+'</ne__service_account__c><NE__Order_Item_Attribute__c><ne__value__c>Value 2</ne__value__c><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp1.Id+'</ne__fampropid__c><name>'+prop.Name+'</name></NE__Order_Item_Attribute__c></NE__OrderItem__c>';
			String nameFile8 = 'Test Config Item Attach Hijo2';
			String bodyFile8 = '<NE__OrderItem__c><Id></Id><Cost_Center__c>'+account.Id+'</Cost_Center__c><ne__account__c>'+account.Id+'</ne__account__c><ne__catalogitem__c>'+catItem3.Id+'</ne__catalogitem__c><ne__qty__c>1</ne__qty__c><ne__generate_asset_item__c>true</ne__generate_asset_item__c><ne__prodid__c >'+prod3.Id+'</ne__prodid__c><ne__commitment__c>false</ne__commitment__c><ne__service_account__c>'+account.Id+'</ne__service_account__c><NE__Order_Item_Attribute__c><ne__value__c>Value 2</ne__value__c><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp1.Id+'</ne__fampropid__c><name>'+prop.Name+'</name></NE__Order_Item_Attribute__c></NE__OrderItem__c>';
			String nameFile9 = 'Test Config Item Attach Hijo3';
			String bodyFile9 = '<NE__OrderItem__c><Id></Id><Cost_Center__c>'+account.Id+'</Cost_Center__c><ne__account__c>'+account.Id+'</ne__account__c><ne__catalogitem__c>'+catItem3.Id+'</ne__catalogitem__c><ne__qty__c>1</ne__qty__c><ne__generate_asset_item__c>true</ne__generate_asset_item__c><ne__prodid__c >'+prod3.Id+'</ne__prodid__c><ne__commitment__c>false</ne__commitment__c><ne__service_account__c>'+account.Id+'</ne__service_account__c><NE__Order_Item_Attribute__c><ne__value__c>Value 2</ne__value__c><ne__fampropid__c relObject="NE__ProductFamilyProperty__c" relField="NE__Source_Product_Family_Property_Id__c">'+pfp1.Id+'</ne__fampropid__c><name>'+prop.Name+'</name></NE__Order_Item_Attribute__c></NE__OrderItem__c>';

			att6 = new Attachment(Name=nameFile6, Body=Blob.valueOf(bodyFile6), ParentId=bcir3.Id, Description='New');
			insert att6;
			att7 = new Attachment(Name=nameFile7, Body=Blob.valueOf(bodyFile7), ParentId=bcir7.Id, Description='New');
			insert att7;
			att8 = new Attachment(Name=nameFile8, Body=Blob.valueOf(bodyFile8), ParentId=bcir9.Id, Description='New');
			insert att8;
			att9 = new Attachment(Name=nameFile9, Body=Blob.valueOf(bodyFile9), ParentId=bcir10.Id, Description='New');
			insert att9;

			Bit2WinHUB__HUB_Settings__c hs = new Bit2WinHUB__HUB_Settings__c();
			hs.Bit2WinHUB__BatchFileLines__c=50;
			hs.Bit2WinHUB__Enable_Log__c = true;
			hs.Bit2WinHUB__Verify_All_Items__c = false;
			insert hs;
			
			blocksize=	hs.Bit2WinHUB__BatchFileLines__c.intValue();

			String nameFile10= 'Csv_Order%';
			String bodyFile10 = 'ACCOUNT:'+account.Id+';CATALOG:'+cat.Id+';HOLDING:'+account.Id+';BU:'+account.Id+';CC:'+account.Id+';';
			bodyFile10 += 'ProductLevel1:'+prod1.Id+':1;Attribute:'+family1.Id+':'+prop.Name+':Value1;';
			bodyFile10 += 'ProductLevel2:'+prod2.Id+':1;Attribute:'+family1.Id+':'+prop.Name+':Value2;';
			bodyFile10 += 'ProductLevel3:'+prod3.Id+':1;Attribute:'+family1.Id+':'+prop.Name+':Value3;';
			
			bcr8 = new Bit2WinHUB__Bulk_Configuration_Request__c(Bit2WinHUB__Configuration__c=null, Bit2WinHUB__Request_Id__c='req-0123456', Bit2WinHUB__Status__c='Failed', Bit2WinHUB__Description__c=' BCR ERROR');
    		insert bcr8;
			bcir14 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr8.Id, Bit2WinHUB__Configuration_Item__c=ordit1.Id, Bit2WinHUB__Status__c='Failed',Bit2WinHUB__Description__c=' BCIR ERROR');
    		insert bcir14;
			att10 = new Attachment(Name=nameFile10, Body=Blob.valueOf(bodyFile10), ParentId=bcr8.Id, Description='New');
			insert att10;

			//bcr9 = new Bit2WinHUB__Bulk_Configuration_Request__c(Configuration__c=ord.Id, Request_Id__c='req-0123456', Status__c='Working', Description__c='');
    		//insert bcr9;
			/*bcir15 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr9.Id, Configuration_Item__c=ordit1.Id, Status__c='Working');
    		insert bcir15;
			bcir16 = new Bit2WinHUB__Bulk_Configuration_Item_Request__c(Bit2WinHUB__Bulk_Configuration_Request__c=bcr9.Id, Configuration_Item__c=ordit1SL.Id, Status__c='Working record created');
    		insert bcir16;*/
			//adjustmentMatrix =new NE__MapObject__c (NE__Map__r.NE__map_name__c='AdjustmentMatrixMap' );
			//objetItemsMap = new NE__MapObjectItem__c ();
		}

	}


}