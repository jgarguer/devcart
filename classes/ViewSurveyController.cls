//18-08-2016: Javier Martínez: Modificado para filtrar el tipo de pregunta "Hidden"
//16-09-2016: Alfonso Alvarez (AJAE): Modificado por incidencia de Mediadiores FVI puedan ejecutar encuesta
//17-09-2016: Alfonso Alvarez (AJAE): Modificado por incidencia de Mediadiores FVI puedan ejecutar encuesta
//22-09-2016: Alfonso Alvarez (AJAE): Se incluyen registros de depuración para error relacionado con parámetro IsWrongUser
//27/12/2016: Humberto Nunes... se coloco la validacion de Usuario erroneo para encuestas IsWrongUser para Departamento FVI
//19/06/2018: Daniel Sánchez (everis): Modificado para implementar la funcionalidad de Visitas (Eventos)

/* Controller associated with pages rendering the survey.
* Used by SurveyPage, ResultsPage, TakeSurvey
*/
global virtual with sharing class ViewSurveyController {
    public String  qQuestion                      {get; set;}
    public Boolean qRequired                      {get; set;} 
    public String  qChoices                       {get; set;}
    public String surveyName                        {get; set;}
    public String surveyHeader                {get; set;}
    public String surveyId {get; set;}
    public String renderSurveyPreview           {get; set;}  
    public String questionName                    {get; set;}  
    public String questionType                    {get; set;}
    public Boolean questionRequired             {get; set;}
    public List<SelectOption> singleOptions   {get; set;} 
    public List<question> allQuestions        {get; set;}
    public List<String> responses                   {get; set;}
    public Integer allQuestionsSize               {get; set;}
    public String  templateURL                {get; set;}
    public String  surveyThankYouText         {get; set;}
    public String surveyContainerCss {get; set;}
    public String surveyThankYouURL          {get; set;}
    public String caseId                     {get; set;}
    public String contactId {get;set;}
    public String contractId {get;set;}
    public String anonymousAnswer {get;set;}
    public List<SelectOption> anonymousOrUser {get;set;}
    public Boolean isInternal {get;set;}
    public String baseURL {get;set;}    
    public String userId{get;set;}
    public String userName{get;set;}
    public String surveyTakerId {get;set;}
    public Boolean thankYouRendered{get;set;}
    public List<String> newOrder {get;set;}
    
    //19/06/2018 Daniel Sánchez (everis)
    public Boolean emptyResponses {get;set;}
    //FIN 19/06/2018 Daniel Sánchez (everis)

	// 20/06/2018 Daniel Sánchez (everis)
    Map<String,List<String>> responsesByKey = new Map<String,List<String>>();  
 
    // FIN 20/06/2018 Daniel Sánchez (everis)
    
    public Boolean IsTaken {get;set;}    
    //16-09-2016 AJAE        
    public Boolean IsWrongUser {get;set;}  
    //FIN 16-09-2016 AJAE            
    /* Retrieves the list of questions, the survey name, after retrieving the 
necessary parameters from the url.
*/
    public Boolean IsClosingCaseSurvey {get;set;} /*Field created for D-000387 (05/09/2017 Alberto Fernandez)*/
    //------------------------------------------------------------------------------// 
    public ViewSurveyController(ApexPages.StandardController stdController) {
        // Get url parameters
        IsTaken = false;
        surveyId = Apexpages.currentPage().getParameters().get('id');
        caseId   = Apexpages.currentPage().getParameters().get('caId');
        contactId = Apexpages.currentPage().getParameters().get('cId'); 
        contractId = Apexpages.currentPage().getParameters().get('coId');
        
        caseId=(caseId==null)?'none':caseId;
        contactId=(contactId==null)?'none':contactId;
        contractId=(contractId==null)?'none':contractId;
        
        // By default the preview is not showing up
        renderSurveyPreview = 'false'; 
        init();       
    } 
    
    public ViewSurveyController(viewShareSurveyComponentController controller)
    {
        surveyId = Apexpages.currentPage().getParameters().get('id');
        caseId   = Apexpages.currentPage().getParameters().get('caId');
        contactId = Apexpages.currentPage().getParameters().get('cId');
        contractId = Apexpages.currentPage().getParameters().get('coId'); 
        
        caseId=(caseId==null)?'none':caseId;
        contactId=(contactId==null)?'none':contactId;
        contractId=(contractId==null)?'none':contractId;
        
        // By default the preview is not showing up
        renderSurveyPreview = 'false';
        init();     
    }
    
    
    public void init()
    {
        /*AGREGADO POR HUMBERTO NUNES: PARA CUBRIR EL REQUERIMIENTO 1 CASO/CONTRATO 1 ENCUESTA */
        system.debug('··········· surveyId: ' + surveyId + '\n contractId->' + contractId + ' contractId.length()=' + contractId.length());
        
        //22-09-2016 AJAE
        system.debug('···········AJAE: caseId: ' + caseId + '\n contactId->' + contactId);
        //FIN 22-09-2016 AJAE
        
        
        IsTaken = false;  
        //16-09-2016 AJAE
        IsWrongUser = false;   
        
        IsClosingCaseSurvey = false;
        
        
        
        if (caseId != 'none')
        {
            List<SurveyTaker__c> LstST1 = [SELECT Id FROM SurveyTaker__c WHERE Case__c =:caseId  AND Survey__c =: surveyId ];
            if (!LstST1.isEmpty()) IsTaken = true;
            
            
            //START CRM 10/05/2017 Solve queries With No Where Or Limit Clause   
            Case C = [Select OwnerId From Case where Id=: caseId Limit 1];
            String ownerId = C.OwnerId;
            list<GroupMember> lst_GroupMember = [select Group.Id, UserOrGroupId  from GroupMember WHERE Group.Id = :ownerId AND UserOrGroupId = :UserInfo.getUserId()];
            
            system.debug('···········AJAE: IsWrongUser pre: ' + IsWrongUser+' C.OwnerId: '+ C.OwnerId+' UserInfo.getUserId(): ' + UserInfo.getUserId() );
            Boolean sizeGroupMember = false;
            if(!lst_GroupMember.isEmpty()){
                sizeGroupMember = true;          
            }          
            system.debug('···········AJAE: setGroupUser.contains(C.OwnerId_UserInfo.getUserId()): ' + sizeGroupMember);
            //FIN 22-09-2016 AJAE
            
            //START AFD 05/09/2017 Users with 'BI_PER_ADM_Encuestas' permissionset can always execute 'BI2 :: Encuesta de cierre de caso' survey (D-000387)
            List<Survey__c> lst_survey = [SELECT Id, Name from Survey__c where Name = 'BI2 :: Encuesta de cierre de caso' AND Id = :surveyId];
            if (!lst_survey.isEmpty()) IsClosingCaseSurvey = true;
            
            if(IsClosingCaseSurvey) {
                List<PermissionSetAssignment> lst_psa = [SELECT id
                                                         FROM PermissionSetAssignment
                                                         WHERE AssigneeId = :UserInfo.getUserId() 
                                                         AND PermissionSet.Name = 'BI_PER_ADM_Encuestas']; 
                IsWrongUser = !(C.OwnerId == UserInfo.getUserId() || (C.OwnerId.getSobjectType().getDescribe().getName() == 'Group' && sizeGroupMember ) || !lst_psa.isEmpty());
            }
            else {
                IsWrongUser = !(C.OwnerId == UserInfo.getUserId() || (C.OwnerId.getSobjectType().getDescribe().getName() == 'Group' && sizeGroupMember ));
            }
            //END AFD 05/09/2017 Users with 'BI_PER_ADM_Encuestas' permissionset can always execute 'BI2 :: Encuesta de cierre de caso' survey (D-000387)
            
            /*******OLD
list<GroupMember> lst_GroupMember = [select Group.Id, UserOrGroupId  from GroupMember];
set<String> setGroupUser = new Set<String>();
for(GroupMember GM : lst_GroupMember)  
setGroupUser.add(GM.Group.Id + '_' + GM.UserOrGroupId);


Case C = [Select OwnerId From Case where Id=: caseId Limit 1];
//22-09-2016 AJAE
system.debug('···········AJAE: IsWrongUser pre: ' + IsWrongUser+' C.OwnerId: '+ C.OwnerId+' UserInfo.getUserId(): ' + UserInfo.getUserId() );
system.debug('···········AJAE: setGroupUser.contains(C.OwnerId_UserInfo.getUserId()): ' + setGroupUser.contains(C.OwnerId + '_' + UserInfo.getUserId()));
//FIN 22-09-2016 AJAE

IsWrongUser = !(C.OwnerId == UserInfo.getUserId() || ( String.valueOf(C.OwnerId).startswith('00G') && setGroupUser.contains(C.OwnerId + '_' + UserInfo.getUserId()) ));
*/  
            //END CRM 10/05/2017 Solve queries With No Where Or Limit Clause         
            //22-09-2016 AJAE
            system.debug('···········AJAE: IsWrongUser post: ' + IsWrongUser);
            //FIN 22-09-2016 AJAE
        }    
        //22-09-2016 AJAE
        system.debug('···········AJAE: IsWrongUser post2: ' + IsWrongUser);  
        //FIN 22-09-2016 AJAE
        if (contractId != 'none')
        {
            List<SurveyTaker__c> LstST2 = [SELECT Id FROM SurveyTaker__c WHERE BI_FVI_Contrato__c =:contractId AND Survey__c =: surveyId ];
            if (!LstST2.isEmpty()) IsTaken = true;
        } 
        
        if(IsTaken)
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.LABS_SF_You_have_already_taken_this_survey));          
            thankYouRendered = true;              
        }          
        
        if(IsWrongUser)
        {
            //22-09-2016 AJAE
            system.debug('···········AJAE: IsWrongUser post3: ' + IsWrongUser);  
            //FIN 22-09-2016 AJAE
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.LABS_SF_WrongUser ));  // + ' ' + UserInfo.getUserId() + ' ' + caseId        
            thankYouRendered = true;              
        }        
        // FIN DE AGREGADO POR HUMBERTO
        //FIN 16-09-2016 AJAE
        
        //17-09-2016: Alfonso Alvarez (AJAE)    
        
        //22-09-2016 AJAE
        system.debug('···········AJAE: surveyId : ' + surveyId );  
        system.debug('···········AJAE: IsTaken : ' + IsTaken );  
        system.debug('···········AJAE: IsWrongUser post4: ' + IsWrongUser);  
        //FIN 22-09-2016 AJAE
        if (surveyId != null && !IsTaken && !IsWrongUser) // && !IsTaken AGREGADO POR HUMBERTO
        {                 
            //FIN 17-09-2016: Alfonso Alvarez (AJAE)
            
            
            //22-09-2016 AJAE 
            system.debug('···········AJAE: IsWrongUser post5: ' + IsWrongUser);  
            //FIN 22-09-2016 AJAE
            
            // Retrieve all necessary information to be displayed on the page
            allQuestions = new List<question>();
            setupQuestionList();
            setSurveyNameAndThankYou(surveyId);
            anonymousOrUser = new List<SelectOption>();
            anonymousOrUser.add(new SelectOption('Anonymous',System.Label.LABS_SF_Anonymous));
            anonymousOrUser.add(new SelectOption('User','User ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName()));
            anonymousAnswer = 'User'; // AGREGADO POR HUMBERTO :: Marca por defecto al usuario NO al ANONIMO.
            isInternal =true;
            newOrder = new List<String>();
            String urlBase = URL.getSalesforceBaseUrl().toExternalForm();
            baseURL = urlBase;
            
            userId = UserInfo.getUserId();
            userName = UserInfo.getName();
            
            String profileId = UserInfo.getProfileId();
            try
            {
                Profile p = [select Id, UserType from Profile where Id=:profileId];
                isInternal =  (p.UserType == 'Guest')?true:false;
            }
            catch (Exception e){
                isInternal = false;
            }
            
            isInternal = false; // AGREGADO POR HUMBERTO :: Evita que salga la opcion de responder como ANONIMO
            thankYouRendered=false;     
        }       
        //22-09-2016 AJAE 
        system.debug('···········AJAE: IsWrongUser post6: ' + IsWrongUser);  
        //FIN 22-09-2016 AJAE
        
    }
    
    
    
    
    //------------------------------------------------------------------------------//  
    private static Testmethod void testViewSurveyController() {
        SurveyTestingUtil tu = new SurveyTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.surveyId); 
        Apexpages.currentPage().getParameters().put('id',tu.surveyId); 
        Apexpages.currentPage().getParameters().put('c',tu.contactId);
        //Error Deploy PLL  Apexpages.currentPage().getParameters().put('ca',tu.caseId); 
        //Error Deploy PLL  Apexpages.currentPage().getParameters().put('co',tu.contractId);  
        Apexpages.Standardcontroller stc;   
        ViewSurveyController vsc = new ViewSurveyController(stc);
        vsc.init();
        System.assert(vsc.allQuestionsSize == 4);
        System.assert(tu.surveyId != null);
        
        
        vsc.submitResults();
        for (question q : vsc.allQuestions)
        {
            q.selectedOption = String.valueof(2);
            q.choices = String.valueof(2);
            q.selectedOptions = new List<String>();
            q.selectedOptions.add(String.valueof(2));
            vsc.submitResults();            
        }
        System.assertEquals(true, vsc.thankYouRendered);
        
        
        //test something
        viewShareSurveyComponentController vSSCC;
        ViewSurveyController vsc2 = new ViewSurveyController(vSSCC);
        vsc2.init();
        
    }
    //----------------------------------------------------------------------------//    
    
    /* Called during the setup of the page. 
Retrieve questions and responses from DB and inserts them in 2 lists. */
    public Integer setupQuestionList(){
        
        getAQuestion();
        return allQuestions.size();
    }
    
    
    /** Sets the survey's name variable
*  param: sID   The survey ID as specified in the DB
*/
    public void setSurveyNameAndThankYou(String sId){
        Survey__c s = [SELECT Name, Id, URL__c, Thank_You_Text__c, thankYouText__c, thankYouLink__c, Survey_Header__c, Survey_Container_CSS__c FROM Survey__c WHERE Id =:sId];
        surveyName = s.Name;
        surveyHeader = s.Survey_Header__c;        
        templateURL = s.URL__c+'id='+sId;//+'&cId={!Contact.Id}'+'&caId='+'{!Case.id}';
        surveyThankYouText = s.Thank_You_Text__c;
        if (surveyThankYouText == null)
        {
            surveyThankYouText = System.Label.LABS_SF_Survey_Submitted_Thank_you;
        }
        surveyThankYouURL = s.thankYouLink__c;
        surveyContainerCss = s.Survey_Container_CSS__c;
    }
    
    //------------------------------------------------------------------------------//   
    public Pagereference updateSurveyName(){
        Survey__c s = [SELECT Name, Id, URL__c, thankYouText__c, thankYouLink__c FROM Survey__c WHERE Id =:surveyId];
        s.Name = surveyName;
        try{
            update s;
        }catch (Exception e){
            Apexpages.addMessages(e);
        }
        return null;
    } 
    
    private static Testmethod void testUpdateSurveyName() {
        SurveyTestingUtil tu = new SurveyTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.surveyId); 
        Apexpages.currentPage().getParameters().put('c',tu.contactId);
        //Error Deploy PLL  Apexpages.currentPage().getParameters().put('ca',tu.caseId); 
        //Error Deploy PLL  Apexpages.currentPage().getParameters().put('co',tu.contractId);  
        Apexpages.Standardcontroller stc; 
        ViewSurveyController vsc = new ViewSurveyController(stc);
        vsc.surveyName = 'new name';
        system.assert(vsc.updateSurveyName() == null);
        
    }
    //------------------------------------------------------------------------------//      
    public Pagereference updateSurveyThankYouAndLink(){
        Survey__c s = [SELECT Name, Id, URL__c, thankYouText__c, thankYouLink__c FROM Survey__c WHERE Id =:surveyId];
        s.thankYouText__c = surveyThankYouText;
        s.thankYouLink__c = surveyThankYouURL;
        try{
            update s;
        }catch(Exception e){
            Apexpages.addMessages(e);
        }
        return null;
    }
    
    private static Testmethod void testupdateSurveyThankYouAndLink() {
        SurveyTestingUtil tu = new SurveyTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.surveyId); 
        Apexpages.currentPage().getParameters().put('c',tu.contactId);
        //Error Deploy PLL  Apexpages.currentPage().getParameters().put('ca',tu.caseId); 
        //Error Deploy PLL  Apexpages.currentPage().getParameters().put('co',tu.contractId); 
        Apexpages.Standardcontroller stc; 
        ViewSurveyController vsc = new ViewSurveyController(stc);
        vsc.surveyThankYouText = 'new stuff';
        vsc.surveyThankYouURL = 'more new stff';
        system.assert(vsc.updateSurveyThankYouAndLink()==null);
    }
    //------------------------------------------------------------------------------//    
    /** When requested from the page - when the user clicks on 'Update Order' -
this function will reorganize the list so that it is displayed in the new order
*/
    public Pagereference refreshQuestionList(){
        setupQuestionList();
        return null;
    }
    
    
    
    //------------------------------------------------------------------------------//      
    
    
    
    //------------------------------------------------------------------------------//    
    private static boolean checkRequired(String response, Survey_Question__c question){
        if(question.Required__c == true){
            if(response == null || response =='NO RESPONSE')
                return false;
        }
        return true;
    } 
    
    /** Redirects the page that displays the detailed results of the survey, 
from all users who took the survey.
*/
    public PageReference resultPage() {
        return new PageReference('/apex/ResultsPage?id='+surveyId);
    }
    
    
    //------------------------------------------------------------------------------//  
    
    //------------------------------------------------------------------------------//  
    
    /** 
*/
    public List<String> getResponses() {
        List<SurveyQuestionResponse__c> qr = [Select Survey_Question__c, SurveyTaker__c, Response__c, Name From SurveyQuestionResponse__c limit 100];
        List<String> resp = new List<String>();
        for (SurveyQuestionResponse__c r : qr) {
            resp.add(r.Response__c);
        }
        
        return resp;
    }  
    
    /** Class: question
*  Retrieves the question information and puts it in the question object
*/      
    public class question{
        public String   name                   {get; set;}
        public String   id                           {get; set;}
        public String   question               {get; set;}
        public String   orderNumber            {get; set;}
        public String   choices                {get; set;}
        public String   selectedOption         {get;set;}
        public List<String> selectedOptions {get;set;}
        public List<SelectOption> singleOptions{get; set;}
        public List<SelectOption> multiOptions {get; set;}
        public Boolean  required               {get; set;}
        public String   questionType           {get; set;}    
        public String   surveyName               {get; set;} 
        public String   renderFreeText             {get; set;}
        public String   renderSelectRadio      {get; set;}
        public String   renderSelectCheckboxes {get; set;} 
        public String   renderSelectRow        {get; set;}
        public List<String> responses              {get; set;}
        public String   singleOptionsForChart  {get; set;}
        public String   qResultsForChart         {get; set;} 
        public List<String> strList              {get; set;} // The question's option as a list of string
        public List<Integer> resultsCounts       {get; set;} // The count of each response to a question's choices
        public List<SelectOption> rowOptions   {get; set;}
        public boolean  noData                 {get; set;}
        // 20/06/2018 Daniel Sánchez (everis)
        public Map<String,Map<String,String>> integerLabel = new Map<String,Map<String,String>>();
        
        /** Fills up the question object
*  param:    Survey_Question__c 
*/     
        public question(Survey_Question__c sq) {
            name = sq.Name;
            id = sq.Id;
            question = sq.Question__c;
            orderNumber = String.valueOf(sq.OrderNumber__c+1);
            choices = sq.Choices__c;
            required = sq.Required__c;
            questionType = sq.Type__c;
            singleOptionsForChart = ' ';
            selectedOption = '';
            selectedOptions = new List<String>();
            surveyName=''; 
            responses=new List<String>(); 
            qResultsForChart=''; 
            resultsCounts=new List<Integer>(); 
            noData=false;
            
            if (sq.Type__c=='Single Select--Vertical'){
                renderSelectRadio='true';
                singleOptions = stringToSelectOptions(choices, sq.Type__c);
                
                renderSelectCheckboxes='false';
                renderFreeText='false';
                renderSelectRow = 'false';
                selectedOption = '';
                selectedOptions = new List<String>();                               
            }
            else if (sq.Type__c=='Multi-Select--Vertical'){        
                renderSelectCheckboxes='true';
                multiOptions = stringToSelectOptions(choices, sq.Type__c);
                renderSelectRadio='false';
                renderFreeText='false';
                renderSelectRow = 'false';
                selectedOption = '';
                selectedOptions = new List<String>();
            }
            else if (sq.Type__c=='Single Select--Horizontal'){   
                renderSelectCheckboxes='false';
                rowOptions = stringToSelectOptions(choices, sq.Type__c);
                renderSelectRadio='false';
                renderFreeText='false';
                renderSelectRow = 'true';
                selectedOption = '';
                selectedOptions = new List<String>();     
            }
            else if (sq.Type__c=='Free Text'){
                renderFreeText='true';
                renderSelectRadio='false';
                renderSelectCheckboxes='false';
                renderSelectRow = 'false';
                choices='';
            }
            //responses= getResponses();
            
        }
        
        /** Splits up the string as given by the user and adds each option
*  to a list to be displayed as option on the Visualforce page
*  param: str   String as submitted by the user
*  returns the List of SelectOption for the visualforce page
*/  
        private List<SelectOption> stringToSelectOptions(String str, String type){
            if (str == '') return new List<SelectOption>();
            strList = str.split('\n');
            
            List<SelectOption> returnVal = new List<SelectOption>();
            Integer i = 0;
            for(String s: strList){
                if (s!='') {    
                    if (s != 'null' && s!= null) {
                        String sBis = s.replace(' ', '%20');
                        singleOptionsForChart += s.trim()+'|';
                        
                        /*RSC2012-02-20
String st = s.replace (' ', '&nbsp;');
returnVal.add(new SelectOption(String.valueOf(i),st));
*/
                        returnVal.add(new SelectOption(String.valueOf(i),s));
                        // 20/06/2018 Daniel Sánchez (everis)
                        // @todo: ARREGLAR PARA MAPEO
                        Map<String,String> auxM = integerLabel.containsKey(type)?integerLabel.get(type):new Map<String,String>();
                        auxM.put(s,String.valueOf(i));
                        integerLabel.put(type, auxM);
                       	// FIN 20/06/2018 Daniel Sánchez (everis)
                        System.debug('*****VALUES: ' + s);
                        i++;
                    }
                }
            }
            singleOptionsForChart = singleOptionsForChart.substring(0, singleOptionsForChart.length()-1);
            return returnVal;
        } 
    }
    
    /** Fills up the List of questions to be displayed on the Visualforce page
*/   
    public List<question> getAQuestion() {
        qRequired = true; 
        questionName = ''; 
        questionType = ''; 
        questionRequired = true; 
        singleOptions = new List<SelectOption>();  
        responses = new List<String>();
        
        qQuestion = '';
        qChoices ='';
        
        //18-08-2016: Javier Martínez: Modificado para filtrar el tipo de pregunta "Hidden"
        //List<Survey_Question__c> allQuestionsObject = 
        //                                [Select s.Type__c, s.Id, s.Survey__c, s.Required__c, s.Question__c, 
        //                                s.OrderNumber__c, s.Name, s.Choices__c 
        //                                From Survey_Question__c s 
        //                                WHERE s.Survey__c =: surveyId ORDER BY s.OrderNumber__c];
        //                                
        //19/06/2018: Daniel Sánchez (everis): Modificado para poder usar la vfp rellena para el proyecto de visitas
        //        String surveyTakerId = ApexPages.currentPage().getParameters().get('surID');
        // List<Survey_Question__c> allQuestionsObject = 
        //    [Select s.Type__c, s.Id, s.Survey__c, s.Required__c, s.Question__c, 
        //     s.OrderNumber__c, s.Name, s.Choices__c
        //     From Survey_Question__c s 
        //     WHERE s.Survey__c =: surveyId and s.Type__c != 'Hidden'
        //     ORDER BY s.OrderNumber__c];
               
        String surveyTakerId = ApexPages.currentPage().getParameters().containsKey('surId')?ApexPages.currentPage().getParameters().get('surId'):'';
        
        System.debug('**surveyTkID: ' + surveyTakerId);
        
        List<Survey_Question__c> allQuestionsObject = !String.isEmpty(surveyTakerId)?
            [Select s.Type__c, s.Id, s.Survey__c, s.Required__c, s.Question__c, 
             s.OrderNumber__c, s.Name, s.Choices__c, (SELECT Id,Response__c,Survey_Question__c,Survey_Question__r.Type__c FROM Survey_Question_Answers__r WHERE SurveyTaker__r.Name = :surveyTakerId) 
             From Survey_Question__c s 
             WHERE s.Survey__c =: surveyId and s.Type__c != 'Hidden'
             ORDER BY s.OrderNumber__c] :
        	[Select s.Type__c, s.Id, s.Survey__c, s.Required__c, s.Question__c, 
             s.OrderNumber__c, s.Name, s.Choices__c 
             From Survey_Question__c s 
             WHERE s.Survey__c =: surveyId and s.Type__c != 'Hidden'
             ORDER BY s.OrderNumber__c];
        System.debug('**lista: ' + allQuestionsObject);
        
        // FIN 19/06/2018: Daniel Sánchez (everis)
        allQuestions = new List<question>();
        
       
        Double old_OrderNumber = 0;
        Double new_OrderNumber;
        Double difference = 0;
        /* Make sure that the order number follow each other (after deleting a question, orders might not do so) */
        for (Survey_Question__c q : allQuestionsObject){ 
            new_OrderNumber = q.OrderNumber__c;
            difference = new_OrderNumber - old_OrderNumber - 1;
            if (difference > 0) {
                Double dd = double.valueOf(difference);
                Integer newOrderInt = dd.intValue();
                q.OrderNumber__c -= Integer.valueOf(newOrderInt); 
            }
            old_OrderNumber = q.OrderNumber__c;
            question theQ = new question(q);
            allQuestions.add(theQ);
            //19/06/2018: Daniel Sánchez (everis). Se añade para mostrar las respuestas en la vpf
            if(!q.Survey_Question_Answers__r.isEmpty()){
                for(SurveyQuestionResponse__c srv : q.Survey_Question_Answers__r){
                    //responses.add(srv.Response__c);
                    List<String> auxL = responsesByKey.containsKey(srv.Survey_Question__r.Type__c)?responsesByKey.get(srv.Survey_Question__r.Type__c):new List<String>();
                    auxL.add(srv.Response__c);
                    responsesByKey.put(srv.Survey_Question__r.Type__c, auxL);
                }
                //emptyResponses = true;
            }
            if(!allQuestions.isEmpty()){
                for(Integer i=0; i<allQuestions.size(); i++){
                    
                }
                for(question qst : allQuestions){
                    if(!responsesByKey.isEmpty()){
                        if(responsesByKey.containsKey('Free Text') && !responsesByKey.get('Free Text').isEmpty() && qst.renderFreeText == 'true'){
                            qst.choices = responsesByKey.get('Free Text')[0];
                        }
                        if(responsesByKey.containsKey('Single Select--Vertical') && !responsesByKey.get('Single Select--Vertical').isEmpty() && qst.renderSelectRadio == 'true'){
                            qst.selectedOption = responsesByKey.get('Single Select--Vertical')[0] == 'No' ? '1':'0';
                            System.debug('**Vertical Select' + qst.selectedOption);
                        }
                        if(qst.questionType == 'Multi-Select--Vertical'){
                            if(responsesByKey.containsKey('Multi-Select--Vertical') && qst.renderSelectCheckboxes == 'true'){
                                for(String saux : responsesByKey.get('Multi-Select--Vertical')){
                                    if(qst.choices.contains(saux)){
                                        System.debug('**entra con cadena ' + saux);
                                        System.debug('**cadena labels: ' + qst.integerLabel.get('Multi-Select--Vertical'));
                                        for(String key : qst.integerLabel.get('Multi-Select--Vertical').keySet()){
                                            System.debug('key: ' + key + 'fin');
                                            System.debug('saux: ' + saux + 'fin');
                                            if(key.contains(saux)){
												qst.selectedoptions.add(qst.integerLabel.get('Multi-Select--Vertical').get(key));                                                
                                            }
                                        }
                                        //qst.selectedoptions.add(qst.integerLabel.get('Multi-Select--Vertical').get(saux));
                                    }
                                }
                            }
                            /*choicesList = qst.choices.split('\n');
                            System.debug('**choicesList: ' + choicesList);
                            System.debug('**choices: ' + qst.choices);
                            if(!choicesList.isEmpty() && qst.renderSelectCheckboxes == 'true'){
                                for(String mop : choicesList){
                                    System.debug('***mop: ' + mop);
                                    List<String> auxList = responsesByKey.get('Multi-Select--Vertical');
                                    System.debug('auxLIST: ' + auxList);
                                    // AQUÍ FALLA-----------------------------------------------------------------------
                                    
                                    
                                    
                                    if(auxList.contains(mop)){
                                        System.debug('segundo IF');
                                        qst.selectedoptions.add(mop);
                                        System.debug('**Multi Select' + qst.selectedOptions);
                                    }
                                }
                            }*/
                        }
                    }
                }
            }

          	//Comprobamos si hay respuestas a mapear
          	/*List<String> choicesList = String.split('/n');
            if(!choicesList.isEmpty()){
                for(String mop : choicesList){
                    if(responsesByKey.get('Multi-Select--Vertical').contains(mop)){
                        selectedoptions.add(mop);
                    }
                }
                }*/
            /*if(!responsesByKey.isEmpty()){
                System.debug('**generated map: ' + responsesByKey);
                /*if(responsesByKey.containsKey('Single Select--Vertical') && !responsesByKey.get('Single Select--Vertical').isEmpty()){
                    selectedOption = responsesByKey.get('Single Select--Vertical')[0];
                }
                if(responsesByKey.containsKey('Free Text') && !responsesByKey.get('Free Text').isEmpty()){
                    choices = responsesByKey.get('Free Text')[0];
                }*/
            //}
            
            //FIN 19/06/2018: Daniel Sánchez (everis)
        }
        allQuestionsSize = allQuestions.size();
        return allQuestions;
    }   
    
    public void submitResults()
    {
        List <SurveyQuestionResponse__c> sqrList = new List<SurveyQuestionResponse__c>();
        
        for (question q : allQuestions)
        {
            SurveyQuestionResponse__c sqr = new SurveyQuestionResponse__c();
            if (q.renderSelectRadio == 'true')
            {
                
                if (q.required &&  (q.selectedOption == null || q.selectedOption == ''))
                {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.LABS_SF_Fillrequired));
                    return;
                }
                
                sqr.Response__c = (q.selectedOption == null || q.selectedOption == '')?'':q.singleOptions.get(Integer.valueOf(q.selectedOption)).getLabel();
                sqr.Survey_Question__c = q.Id;
                sqrList.add(sqr);
            }
            else if (q.renderFreeText == 'true')
            {
                if (q.required && q.choices == '')
                {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,  System.Label.LABS_SF_Fillrequired));
                    return;
                }
                System.debug('*****Select Radio ' + q.choices);
                
                sqr.Response__c = q.choices;
                sqr.Survey_Question__c = q.Id;
                sqrList.add(sqr);
            }
            else if (q.renderSelectCheckboxes == 'true')
            {
                if (q.required && (q.selectedOptions == null || q.selectedOptions.size() == 0))
                {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,  System.Label.LABS_SF_Fillrequired));
                    return;
                }
                
                for (String opt : q.selectedOptions)
                {
                    sqr = new SurveyQuestionResponse__c();
                    sqr.Response__c = (opt == '' || opt == null)?'':q.multiOptions.get(Integer.valueOf(opt)).getLabel();
                    sqr.Survey_Question__c = q.Id;
                    sqrList.add(sqr);
                }
            }
            else if (q.renderSelectRow == 'true')
            {
                if (q.required && (q.selectedOption == null || q.selectedOption == ''))
                {
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,  System.Label.LABS_SF_Fillrequired));
                    return;
                }
                
                sqr.Response__c = (q.selectedOption == null || q.selectedOption == '')?'':q.rowOptions.get(Integer.valueOf(q.selectedOption)).getLabel();
                sqr.Survey_Question__c = q.Id;
                sqrList.add(sqr);
            }
            
            
        }
        if(AddSurveyTaker())
        {
            for (SurveyQuestionResponse__c sqr : sqrList)
            {
                sqr.SurveyTaker__c = surveyTakerId;
            }
            insert sqrList;
            thankYouRendered=true;      
        }
        
        // INICIO 19/06/18 - Daniel Sánchez (everis)
        
        String idVisita = ApexPages.currentPage().getParameters().containsKey('evid')?ApexPages.currentPage().getParameters().get('evid'):'';
        if(String.isEmpty(idVisita)){
            system.debug('Error al obtener el parámetro ID de visita');
        } 
        else{
            Event visita = new Event();
            visita = [SELECT Id, BI_FVI_SurveyTaken__c,BI_FVI_Estado__c From Event Where Id=:idVisita];
            
                visita.BI_FVI_SurveyTaken__c=surveyTakerId;
                visita.BI_FVI_Estado__c='Cualificada';
                update visita;
         
        }
        // FIN 19/06/18 - Daniel Sánchez (everis)
        
    }
    
    
    private Boolean AddSurveyTaker()
    {
        if (surveyId == null) return false;
        
        String userId = (anonymousAnswer != 'Anonymous')?UserInfo.getUserId():null;
        SurveyTaker__c st = new SurveyTaker__c();
        st.Survey__c = surveyId;
        System.debug('CONTACTID ' + contactId);
        if (!String.isBlank(contactId) && contactId!='none') st.Contact__c = contactId; //APC eHelp 02097265 24/10/2016 añadida condición contacto está en blanco
        if (contractId!='none') st.BI_FVI_Contrato__c = contractId;
        if (caseId!='none') st.Case__c = caseId;
        st.Taken__c = 'false';        
        st.User__c = userId;
        insert st;  
        surveyTakerId = st.Id;
        return true;    
    }
    
    
}