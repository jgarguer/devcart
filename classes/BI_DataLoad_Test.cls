@isTest(isParallel=true)
private class BI_DataLoad_Test {
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
   Author:        Pedro Párraga
   Company:       New Energy Aborda
   Description:   Test class to manage the coverage code for BI_DataLoad class 
   
   History: 
   
   <Date>                  <Author>                <Change Description>
   23/01/2017              Pedro Párraga	        Initial Version
   --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	private static List<String> lst_pais;
 	private static List<Account> lst_acc;
 	private static List<Opportunity> lst_opp;
 	private static NE__Catalog__c cat;
 	private static List <Lead> lst_ld;
 	public static List<Contact> lst_cont;
 	private static Id idProfile;
 	private static List<NE__OrderItem__c> lst_oi;
 	private static List<Factura__c> lst_fact;
 	private static List<Programacion__c> lst_progr;
 	private static List<NotadeCredito__c> lst_nota;

    @testSetup
    private static void setupTest(){
		lst_pais = BI_DataLoad.loadPaisFromPickList(1);
		lst_acc = BI_DataLoad.loadAccountsPaisStringRef(2, lst_pais);
		lst_opp = BI_DataLoad.loadOpportunities(lst_acc[0].Id);
		cat = new NE__Catalog__c (Name = 'test catalog', BI_Country__c = 'Argentina');
		insert cat;
		lst_ld = BI_DataLoad.loadLeads(3);
		lst_cont = BI_DataLoad.loadContacts(3, lst_acc);
    	lst_oi = BI_DataLoad.loadOrderItem(3, lst_acc[0]);
    	lst_fact = BI_DataLoad.generateFactura(3, lst_acc[0].Id);
    	lst_progr = BI_DataLoad.generateprogramacion(3, lst_fact[0].Id);
    	lst_nota =  BI_DataLoad.generateNotadeCredito(3, lst_progr[0].Id);
	} 

	private static void getRecords(){
		lst_acc = [Select Id, BI_Country__c FROM Account];
		lst_pais = BI_DataLoad.loadPaisFromPickList(1);
		lst_opp = [Select Id, Name, StageName, AccountId,BI_Opportunity_Type__c, BI_Ciclo_ventas__c, BI_Country__c from Opportunity];
		cat = [Select Id, Name, BI_Country__c from NE__Catalog__c limit 1];
		lst_ld = [Select Id, LastName, CurrencyIsoCode, Company, Status, Email from Lead];
		lst_cont = [Select Id, LastName, AccountId, Email, HasOptedOutOfEmail, BI_Activo__c, BI_Country__c, Phone, BI_Cuenta_activa_en_portal__c from Contact];
		idProfile = BI_DataLoad.searchAdminProfile();
		lst_oi = [Select Id, NE__OrderId__c, NE__Qty__c, NE__Asset_Item_Account__c, NE__Account__c, NE__Status__c from NE__OrderItem__c];
		lst_fact = [Select Id, Activo__c, Cliente__c, Fecha_Inicio__c, Fecha_Fin__c, Inicio_de_Facturaci_n__c, IVA__c, Sociedad_Facturadora__c, Requiere_Anexo__c, RecordTypeId from Factura__c];
		lst_progr = [Select Id, Factura__c, CurrencyIsoCode, Sociedad_Facturadora_Real__c, Facturado__c, Fecha__c, Inicio_del_per_odo__c, Fin_del_per_odo__c, Fecha_de_inicio_de_cobro__c from Programacion__c];
		lst_nota = [Select Id, TipoNota__c, Responsable__c, Factura__c, Fecha_Solicitud__c, Motivo_Pedido_del__c, Tipo_de_IVA__c, CurrencyIsoCode, Condiciones_de_Pago__c, Refacturada__c, Facturado__c from NotadeCredito__c];
	}

   @isTest static void loadPaisFromPickListNotBeing_Test(){ 		
   		lst_pais = BI_DataLoad.loadPaisFromPickListNotBeing(3, new Set<String>());
   }

   @isTest static void createOptyByRecordType_Test(){
   		try{
   			BI_DataLoad.createOptyByRecordType(null, null, null);
   		}catch(Exception Exc){

   		}
   }

   @isTest static void loadAccountsPaisStringRef_Test(){
   		List<String> lst_paises = new List<String>();
   		lst_paises.add('Argentina');
   		lst_Acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_paises);

   		lst_paises.clear();
   		lst_paises.add(Label.BI_Peru);
   		lst_Acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_paises);

   		lst_paises.clear();
   		lst_paises.add('Colombia');
   		lst_Acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_paises);

   		lst_paises.clear();

   		lst_Acc = BI_DataLoad.loadAccountsPaisStringRef(3, lst_paises);
   }

   @isTest static void loadAccountsEcuadorEjecutivoRef_Test(){
   	   	List<String> lst_paises = new List<String>();

   		lst_paises.add('Ecuador');
   		BI_DataLoad.loadAccountsEcuadorEjecutivoRef(3, lst_paises);
   		BI_DataLoad.loadAccountsEcuadorEjecutivoRef(null, null);
   }

   @isTest static void loadAccounts_Test(){	
   		List<String> lst_paises = new List<String>();

   		lst_paises.add('Ecuador');
   		List<Account> lst_acc2 = BI_DataLoad.loadAccounts(3, lst_paises);
   	}
   
   @isTest static void loadAccountsWithCountry_Test(){
   		List<String> lst_paises = new List<String>();

   		lst_paises.add('Ecuador');

   		lst_paises.clear();
   		lst_paises.add('1');
   		BI_DataLoad.loadAccountsWithCountry(3, lst_paises);	
   }
   
   @isTest static void loadPortalAccounts_Test(){
   	   	List<String> lst_paises = new List<String>();

   		lst_paises.add('Argentina');
   		BI_DataLoad.loadPortalAccounts(3, lst_paises , UserInfo.getUserId());
   		BI_DataLoad.loadPortalAccounts(null, null , null);
   }
   
   @isTest static void loadAccountsValidateId_Test(){
   		lst_acc = BI_DataLoad.loadAccountsValidateId();

   }
   
   @isTest static void loadCampaigns_Test(){
   		List<Campaign> lst_camp = BI_DataLoad.loadCampaigns(3);
   		lst_camp = BI_DataLoad.loadCampaigns(null);

   }
   
   @isTest static void loadCampaignsCatalog_Test(){
   		List<Campaign> lst_camp = BI_DataLoad.loadCampaignsCatalog(3);
   		lst_camp = BI_DataLoad.loadCampaignsCatalog(null);

   }
   
   @isTest static void loadCampaigns_CatalogID_Test(){
   		getRecords();
   		List<Campaign> lst_camp = BI_DataLoad.loadCampaigns_CatalogID(3, cat.Id);
   		lst_camp = BI_DataLoad.loadCampaigns_CatalogID(3, null);
   }
   
   @isTest static void loadCampaignsWithRecordType_Test(){
   	   	Map<String, Id> map_camp_rt = BI_DataLoad.obtainCampaignRecordType();
   	   	List<Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(3, map_camp_rt);
   	    lst_camp = BI_DataLoad.loadCampaignsWithRecordType(3, null);
   }

   @isTest static void loadPortalContacts_Test(){
   		getRecords();
   		List<Contact> lst_cont2 = BI_DataLoad.loadPortalContacts(3,lst_acc);
   		lst_cont = BI_DataLoad.loadPortalContacts(3,null);
   }

   @isTest static void loadCampaignMembers_Test(){
   		getRecords();
   		List <Campaign> lst_camp = BI_DataLoad.loadCampaigns(2);
   		List <CampaignMember> lst_cmpm = BI_DataLoad.loadCampaignMembers(lst_camp, lst_ld, lst_cont, 3);
   		lst_cmpm =BI_DataLoad.loadCampaignMembers(null, null, null, 3);
   		try{
   			 lst_cmpm =BI_DataLoad.loadCampaignMembers(lst_camp, lst_ld, lst_cont, 0);
   		}catch(Exception Exc){

   		}
    	lst_cmpm =BI_DataLoad.loadCampaignMembers2(lst_camp, lst_ld, lst_cont);
   		lst_cmpm =BI_DataLoad.loadCampaignMembers2(null, null, null);
   }

 	@isTest static void loadOneCampaignMember_Test(){
 		try{
 			List<CampaignMember> lst_camp = BI_DataLoad.loadOneCampaignMember(null, null, null);
 		}catch(Exception Exc){}
 	}

   @isTest static void loadTwoCampaignMember_Test(){
   		getRecords();         
   		List <Campaign> lst_camp = BI_DataLoad.loadCampaignsWithRecordType(3, BI_DataLoad.obtainCampaignRecordType());
   		List<CampaignMember> lst_cm = BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_cont);
   }

   @isTest static void loadPromociones_Test(){
   		list<Campaign>  lst_camp = BI_DataLoad.loadCampaignsCatalog(1);
   		list<BI_Promociones__c> lst_promo = BI_DataLoad.loadPromociones(3, lst_camp);
   }

   @isTest static void loadPromotions_Test(){
   		getRecords();
   		List<NE__Promotion__c> lst_promotions = BI_DataLoad.loadPromotions(200, cat.Id);
   }

   @isTest static void loadCampaignsforPromo_Test(){
   		getRecords();
   		List<NE__Promotion__c> lst_promotions = BI_DataLoad.loadPromotions(200, cat.Id);
   		List<Campaign> lst_campaigns = BI_DataLoad.loadCampaignsforPromo(lst_promotions);
   		lst_campaigns = BI_DataLoad.loadCampaignsforPromo(null);
   }

   @isTest static void obtainCampaignRecordType_Test(){
   		Map<String, Id> lst_rt = BI_DataLoad.obtainCampaignRecordType();
   }

   @isTest static void loadNECatalogItems_Test(){
   		getRecords();
    	List<NE__Promotion__c> lst_promotions = BI_DataLoad.loadPromotions(200, cat.Id);
   		List<NE__Catalog_Item__c> lst_nci = BI_DataLoad.loadNECatalogItems(200, cat.Id, lst_promotions);
   }

   @isTest static void loadOpportunitiesPaisRef_Test(){
    	try{
    		BI_DataLoad.loadOpportunitiesPaisRef(null);
   		}catch (exception Exc){}  		
   }

   @isTest static void loadOpportunitiesCicloRapido_Test(){
    	try{
    		List<Opportunity> lst_opp2 = BI_DataLoad.loadOpportunitiesCicloRapido(null);
   		}catch (exception Exc){} 
   }

   @isTest static void loadOpportunitiesWithParent_Test(){
   		getRecords();
		RecordType rType = [select Id from RecordType where DeveloperName = 'BI_Oportunidad_Regional' limit 1];
		List <Opportunity> lst_opp2 = BI_DataLoad.loadOpportunitiesWithParent(lst_acc[0].Id, lst_opp[0].Id, lst_pais[0]);
   }

   @isTest static void loadOpportunitiesWithParentStringRef_Test(){
   		getRecords();
   	    RecordType rType = [select Id from RecordType where DeveloperName = 'BI_Oportunidad_Regional' limit 1]; 
   	   List<Opportunity> lst_opp2 = BI_DataLoad.loadOpportunitiesWithParentStringRef(lst_acc[0].Id, lst_opp[0].Id, lst_pais);
   }

   @isTest static void loadOpportunitiesHiddenFields_Test(){
   		getRecords();
		List <Opportunity> lst_opp2 = BI_DataLoad.loadOpportunitiesHiddenFields(lst_acc[0].Id);
   }

   @isTest static void loadOpportunitiesWithStringPais_Test(){
   		getRecords();
   		List <Opportunity> lst_opp2 = BI_DataLoad.loadOpportunitiesWithStringPais(lst_acc[0].Id, lst_pais[0]);
   }

   @isTest static void loadOpportunitiesforButton_Test(){
   		getRecords();
   		List <Opportunity> lst_opp2 = BI_DataLoad.loadOpportunitiesforButton(3, lst_acc[0].Id);
   }

   @isTest static void searchAdminProfile_Test(){
   		Id idProfile2 = BI_DataLoad.searchAdminProfile();
   }


   @isTest static void searchEjecutivoProfile_Test(){
   		Id idProfile2 = BI_DataLoad.searchEjecutivoProfile();
   }

   @isTest static void searchPortalProfile_Test(){
   		Id idProfile2 = BI_DataLoad.searchPortalProfile();
   }


   @isTest static void searchPortalPlusProfile_Test(){
   		Id idProfile2 = BI_DataLoad.searchPortalPlusProfile();
   }

   @isTest static void searchUserRole_Test(){
   		Id idProfile2 = BI_DataLoad.searchUserRole();
   }

   @isTest static void loadUsers_Test(){
   		getRecords();
   		List<User> lst_user = BI_DataLoad.loadUsers(3, idProfile, Label.BI_Administrador);
   }

   @isTest static void loadPortalUserWithRole_Test(){
   		getRecords();
   		User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(), idProfile);
   }

   @isTest static void loadPortalUser_Test(){
   		getRecords();
   		User usr = BI_DataLoad.loadPortalUser(lst_cont[0].Id, idProfile);
   }

   @isTest static void loadSeveralPortalUsers_Test(){
   		getRecords();
   		User usr = BI_DataLoad.loadSeveralPortalUsers(lst_cont[0].Id, idProfile);
   }

    @isTest static void loadOpportunityTeamMembers_Test(){
    	getRecords();
    	List<User> lst_user = BI_DataLoad.loadUsers(2, idProfile, Label.BI_Administrador);
    	List<OpportunityTeamMember> lst_otm = BI_DataLoad.loadOpportunityTeamMembers(2, lst_user[0].Id ,lst_opp);
   }

    @isTest static void loadOpportunityTeamMemberswithRole_Test(){
    	getRecords();
    	List<OpportunityTeamMember> lst_otm = BI_DataLoad.loadOpportunityTeamMemberswithRole(3, UserInfo.getUserId(), lst_opp); 
   }

    @isTest static void loadTasksforAccount_Test(){
    	try{
    		List<Task> lst_task = BI_DataLoad.loadTasksforAccount(null, null, null);
   		}catch (exception Exc){} 
   }

    @isTest static void loadTasksforOpp_Test(){
    	getRecords();
    	List<Task> lst_tsk = BI_DataLoad.loadTasksforOpp(lst_opp);
   }

    @isTest static void loadContracts_Test(){
    	getRecords();
    	List<Contract> lst_ctr = BI_DataLoad.loadContracts(3,lst_acc[0].Id, lst_opp[0].Id);
   }

    @isTest static void loadTareaObligatoria_Test(){
    	getRecords();
    	List<BI_Tarea_Obligatoria__c> lst_to = BI_DataLoad.loadTareaObligatoria(lst_pais);
   }

     @isTest static void loadTareaObligatoriaRol_Test(){
     	getRecords();
     	List<BI_Tarea_Obligatoria__c> lst_to = BI_DataLoad.loadTareaObligatoriaRol(lst_pais);
   }

     @isTest static void recordTypeOppLocal_Test(){
     	Id rtId = BI_DataLoad.recordTypeOppLocal();
   }

     @isTest static void recordTypeOppCicloLicitaciones_Test(){
    	try{
    		Id ids = BI_DataLoad.recordTypeOppCicloLicitaciones();
   		}catch (exception Exc){} 
   }

     @isTest static void recordTypeOppCicloCompleto_Test(){
     	Id idOpp = BI_DataLoad.recordTypeOppCicloCompleto();
   }

     @isTest static void loadISC_Test(){
     	getRecords();
     	List <BI_ISC2__c> lst_isc =	BI_DataLoad.loadISC(lst_acc);
   }

     @isTest static void set_ByPass_Test(){
     	BI_DataLoad.set_ByPass(false,false);
   }

     @isTest static void loadCases_Test(){
     	getRecords();
   		List<RecordType> lst_cas_rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND IsActive = true];
     	List <Case> lst_cas = BI_DataLoad.loadCases(lst_acc, lst_opp, lst_cas_rt);
     	List <Opportunity> lst_opp2 = new List<Opportunity>();
     	Test.startTest();
     	lst_cas = BI_DataLoad.loadCases(lst_acc, lst_opp2, lst_cas_rt);
     	Test.stopTest();
   }

     @isTest static void loadPaises_Test(){
     	Map<String,String> map_pais = BI_DataLoad.loadPaises();
   }

     @isTest static void loadRegions_Test(){
     	Map<String,BI_Code_ISO__c> map_regions = BI_DataLoad.loadRegions();
   }

      @isTest static void loadOpportunitiesContract_Test(){
      	getRecords();
      	lst_opp = BI_DataLoad.loadOpportunitiesContract(3, lst_acc[0]);
     	lst_opp = BI_DataLoad.loadOpportunitiesContract(3, null);
   }

     @isTest static void loadCustomPlanDeAccion_Test(){
     	getRecords();
     	List<BI_Plan_de_accion__c> lst_plan = BI_DataLoad.loadCustomPlanDeAccion(3, lst_acc[0]);
   }

     @isTest static void loadTaskWithRT_Test(){
     	getRecords();
   		List<Id> ids = new List<Id>();
   		for(Account acc : lst_acc){
   			ids.add(acc.Id);
   		}
     	List<RecordType> rect = [select DeveloperName, Id from RecordType where SObjectType = 'Task'];
     	List<Task> lst_task = BI_DataLoad.loadTaskWithRT(3, rect, ids, 'Completed');
   }

     @isTest static void loadEventWithRT_Test(){
     	getRecords();
   		List<Id> ids = new List<Id>();
   		for(Account acc : lst_acc){
   			ids.add(acc.Id);
   		}

     	List<RecordType> rect = [select DeveloperName, Id from RecordType where SObjectType = 'Event'];
     	List<Event>  lst_event = BI_DataLoad.loadEventWithRT(3, rect, ids);
   }

     @isTest static void insertDisconnections_Test(){
     	getRecords();
     	List<BI_O4_Disconnection__c> lst_disc = BI_DataLoad.insertDisconnections(3, lst_acc);
   }

   @isTest static void insertDisconnectionCases_Test(){
       getRecords();
       List<BI_O4_Disconnection__c> lst_disc = BI_DataLoad.insertDisconnections(3, lst_acc);
       
       Test.startTest();
       if(!lst_disc.isEmpty())List<Case> lst_cases = BI_DataLoad.insertDisconnectionCases(3, lst_disc);
       else System.assert(false);
       Test.stopTest();
   }	

     @isTest static void generateRandomString_Test(){
     	String random = BI_DataLoad.generateRandomString(10);
   }

     @isTest static void createRandomUserCOL_Test(){
     	getRecords();
     	User usu = BI_DataLoad.createRandomUserCOL(idProfile, BI_DataLoad.searchUserRole());

   }

     @isTest static void generatePedido_Test(){
     	getRecords();
     	List<Pedido_PE__c> lst_pedido = BI_DataLoad.generatePedido(3, lst_progr[0].Id);
   }

     @isTest static void generateNotadeCreditoOLI_Test(){
     	getRecords();
     	List<NotaCredito_OLI__c> lst_notas = BI_DataLoad.generateNotadeCreditoOLI(3, lst_nota[0].Id);
   }

     @isTest static void generateOrders_Test(){
     	getRecords();
     	BI_DataLoad.generateOrders(3, lst_oi[0].Id, lst_opp[0].Id);
   }

     @isTest static void generateOrderitems_Test(){
     	getRecords();
     	BI_DataLoad.generateOrderitems(3, lst_oi[0].Id, lst_acc[0].Id);
   }
}