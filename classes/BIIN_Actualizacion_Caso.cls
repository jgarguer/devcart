//@RestResource(urlMapping='/ticketing/v1/actualizarCrearCaso/*')
@RestResource(urlMapping='/actualizarCrearCaso/*')

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jose María Martín Castaño
        Company:       Deloitte
        Description:   Clase para actualizar o insertar el ticket que viene de RoD.
        
        History:
        
        <Date>              <Author>                                        <Description>
        10/12/2015          Jose María Martín Castaño                       Initial version
        22/01/2016          Guillermo Muñoz Nieto                           'Categorías operacionales' mapping added
        25/01/2016          Guillermo Muñoz Nieto                           'traducirValor' method deprecated by the creation of BIIN_Helper class
        01/02/2016          Guillermo Muñoz Nieto                           Filter added on BI_Country__c to avoid null values
        11/02/2016          Micah Burgos García                             Add validation code 004. Caso.correlatorId is mandatory in TicketEntrada.
        15/03/2016          Micah Burgos García                             eHelp 01579369 - Added condition to case query in order to prevent duplicates if RoD doesn´t have caseNumber. Condition: OR BI_Id_del_caso_legado__c =:caso.correlatorId
        29/04/2016          Guillermo Muñoz                                 eHelp 01579369- Changed insert for upsert when a case be created
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

global with sharing class BIIN_Actualizacion_Caso {       
    @HttpPost
    global static TicketRespuesta actualizarCaso(TicketEntrada  TicketRequestType) {
        RestContext.response.addHeader('Content-Type', 'application/json; charset=utf-8');
        system.debug('Entrada: '+JSON.serialize(TicketRequestType));
        
        TicketEntrada caso = TicketRequestType;
        TicketRespuesta respuesta = new TicketRespuesta();
        datetime startDateTime = datetime.newInstance(0);
        datetime resolutionDate;
        String subestado='';
        String TIPO_CASO='Primer Contacto';
        String MOTIVO_CASO='Incidencia Técnica';
        Account acc;
        String ORIGEN_REMEDY='13000';
        Id log_id;
        

        List<Map<String, String>> ladditionalData = TicketRequestType.additionalData;
        if(caso.resolutionDate!='' && caso.resolutionDate != null){
            if(integer.valueOf(caso.resolutionDate) != 0){
                resolutionDate = datetime.newInstance(0);
                resolutionDate = startDateTime.addSeconds(integer.valueOf(caso.resolutionDate));
            }
        }

        subestado=getValueFromKey(ladditionalData, 'subStatus');
        if(subestado!=''&&subestado!=null){
            subestado = BIIN_Helper.traducirValor('Subestado',subestado);
        }else{
            subestado='';
        }
        Id recordTypeIncTec = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Incidencia Tecnica').getRecordTypeId();
        system.debug('Recuperamos el recordType: '+recordTypeIncTec);
        try{
            system.debug('Ejecutamos la query');
            acc = [SELECT id, BI_Country__c FROM Account WHERE BI_Validador_Fiscal__c =: caso.accountId];
        }catch(QueryException e){
            system.debug('Error, el validador fiscal no es correcto: '+e);
            respuesta.code='003';
            respuesta.message='El validador Fiscal con valor : "'+caso.accountId+'" enviado no es correcto';
            return respuesta;
        }
        
        if(caso.correlatorId == null || caso.correlatorId == ''){
             system.debug('Error, El valor [correlatorId] es obligatorio. Actualmente aparece a null');
            respuesta.code='004';
            respuesta.message='El valor [correlatorId] es obligatorio. Actualmente aparece a null';
            return respuesta;
        }
        
        try{
          
            Case[] q = [SELECT CaseNumber, ContactId, OwnerId, ParentId, BI_Id_del_caso_legado__c FROM Case WHERE RecordType.DeveloperName IN ('BIIN_Incidencia_Tecnica','BIIN_Solicitud_Incidencia_Tecnica') AND (CaseNumber =: caso.ticketId OR BI_Id_del_caso_legado__c =:caso.correlatorId) ];
            
            if(q.size()<=0){ //La petición es una proactiva, no se ha creado una solicitud técnica previa y se tendrá que hacer una creación del caso
                system.debug('GETKEY' + ladditionalData );
                Case c= new Case(   BI_Id_del_caso_legado__c = caso.correlatorId,     
                                 BI_Confidencial__c=true,
                                 AccountId = acc.Id,
                                 BI2_Nombre_Contacto_Final__c = getValueFromKey(ladditionalData, 'nombreContactoFinal'),
                                 BI2_Apellido_Contacto_Final__c = getValueFromKey(ladditionalData, 'apellidoContactoFinal'),
                                 BI2_Email_Contacto_Final__c = getValueFromKey(ladditionalData, 'emailContactoFinal'),
                                 CreatedDate = startDateTime.addSeconds(integer.valueOf(caso.creationDate)),
                                 BI_COL_Fecha_Radicacion__c = startDateTime.addSeconds(integer.valueOf(getValueFromKey(ladditionalData, 'fechaRadicacion'))),
                                 BI_Assigned_Group__c = caso.responsibleParty,
                                 Status = BIIN_Helper.traducirValor('Estado',caso.ticketStatus),
                                 BI_Status_Reason__c = subestado,
                                 Origin = BIIN_Helper.traducirValor('Origen',ORIGEN_REMEDY),
                                 TGS_Impact__c = BIIN_Helper.traducirValor('Impacto',caso.severity),
                                 TGS_Urgency__c = BIIN_Helper.traducirValor('Urgencia',getValueFromKey(ladditionalData, 'urgencia')),
                                 Priority = BIIN_Helper.traducirValor('Prioridad',caso.priority),

                                 BI_COL_Codigo_CUN__c = getValueFromKey(ladditionalData, 'CodCUN'),
                                 BIIN_Site__c = getValueFromKey(ladditionalData, 'Site'), 
                                 BIIN_Id_Producto__c = getValueFromKey(ladditionalData, 'IdProducto'),
                                 BI_Product_Categorization_Tier_1__c = getValueFromKey(ladditionalData, 'catProducto1'),
                                 BI_Product_Categorization_Tier_2__c = getValueFromKey(ladditionalData, 'catProducto2'),
                                 BI_Product_Categorization_Tier_3__c = getValueFromKey(ladditionalData, 'catProducto3'),
                                 Subject = caso.subject,
                                 Description = caso.description,
                                 LastModifiedDate = startDateTime.addSeconds(integer.valueOf(caso.statusChangeDate)),
                                 BI2_fecha_hora_de_resolucion__c = resolutionDate,
                                 TGS_Resolution__c = caso.resolution,
                                 BI_Resolution_Category_Tier_1__c = getValueFromKey(ladditionalData, 'catResolucion1'), 
                                 BI_Resolution_Category_Tier_2__c = getValueFromKey(ladditionalData, 'catResolucion2'), 
                                 BI_Resolution_Category_Tier_3__c = getValueFromKey(ladditionalData, 'catResolucion3'),   
                                 Type=TIPO_CASO,
                                 Reason=MOTIVO_CASO,
                                 //BIIN_Fecha_Ultima_Resolucion__c =  DateTime.newInstance((integer.valueOf(getValueFromKey(ladditionalData, 'fechaUltimaRes'))))
                                 //BIIN_Fecha_Ultima_WorkInfo__c =  DateTime.newInstance((integer.valueOf(caso.fechaUltimaWorkInfo))*1000)          
                                 BIIN_Categorization_Tier_1__c = getValueFromKey(ladditionalData, 'catOperacional1'),
                                 BIIN_Categorization_Tier_2__c = getValueFromKey(ladditionalData, 'catOperacional2'),
                                 BIIN_Categorization_Tier_3__c = getValueFromKey(ladditionalData, 'catOperacional3')

                                );
                if(getValueFromKey(ladditionalData, 'SLA') != null && getValueFromKey(ladditionalData, 'SLA') != ''){
                   c.TGS_SLA_Light__c = BIIN_Helper.traducirValor('SLM-Status',getValueFromKey(ladditionalData, 'SLA'));//'Green',//getValueFromKey(ladditionalData, 'SLA'),
                }
                else{
                   c.TGS_SLA_Light__c = BIIN_Helper.traducirValor('SLM-Status','1');
                }
                if(getValueFromKey(ladditionalData, 'tiempoNetoApertura')!=''&&getValueFromKey(ladditionalData, 'tiempoNetoApertura')!=null){
                    c.BIIN_Tiempo_Neto_Apertura__c=decimal.valueOf(getValueFromKey(ladditionalData, 'tiempoNetoApertura'))/3600;
                }
                //GMN 01/02/2016 - Filter added to avoid null values
                if(caso.country != null && caso.country != ''){
                    c.BI_Country__c = caso.country;
                }
                else{
                    c.BI_Country__c = acc.BI_Country__c;
                }
                system.debug('Insertamos');
                system.debug('### request---> ' + ladditionalData);
                try{
                    c.RecordTypeId=recordTypeIncTec;
                    upsert c BI_Id_del_caso_legado__c;
                    q = [SELECT CaseNumber FROM Case WHERE BI_Id_del_caso_legado__c =: c.BI_Id_del_caso_legado__c AND RecordType.DeveloperName IN ('BIIN_Incidencia_Tecnica','BIIN_Solicitud_Incidencia_Tecnica') ];
                    respuesta.ticketId=c.BI_Id_del_caso_legado__c;
                    respuesta.caseNumber=q[0].CaseNumber;
                    respuesta.code='000';
                    respuesta.message='El ticket se ha introducido correctamente';
                    system.debug('La inserción ha funcionado: ');
                }catch (DmlException e){
                    respuesta.code='001';
                    log_id = BI_LogHelper.generate_BILog('BIIN_Actualizacion_Caso.actualizarCaso', 'BIIN', e, 'WebService');
                    respuesta.message='Se ha producido un error en la inserción del ticket. Log Id: ' + log_id;
                    system.debug('La inserción ha fallado: '+e);
                }
            }else{ //La petición ya tiene una solicitud técnica creada, con lo que la operación a realizar será una actualización
                q[0].BI_Id_del_caso_legado__c = caso.correlatorId;
                q[0].BI2_Nombre_Contacto_Final__c = getValueFromKey(ladditionalData,'nombreContactoFinal');
                q[0].BI2_Apellido_Contacto_Final__c = getValueFromKey(ladditionalData,'apellidoContactoFinal');
                q[0].BI2_Email_Contacto_Final__c = getValueFromKey(ladditionalData,'emailContactoFinal');
                q[0].BI_Assigned_Group__c = caso.responsibleParty;
                q[0].Status = BIIN_Helper.traducirValor('Estado',caso.ticketStatus);//Interpretar el estado RoD->SF
                q[0].BI_Status_Reason__c = subestado;
                //q[0].Origin = traducirValor(lorigen,caso.source); MBG - Caso eHelp 01498511 
                q[0].TGS_Impact__c = BIIN_Helper.traducirValor('Impacto',caso.severity);
                q[0].TGS_Urgency__c = BIIN_Helper.traducirValor('Urgencia',getValueFromKey(ladditionalData, 'urgencia'));
                q[0].Priority = BIIN_Helper.traducirValor('Prioridad',caso.priority);
                if(getValueFromKey(ladditionalData, 'SLA') != null){
                    q[0].TGS_SLA_Light__c = BIIN_Helper.traducirValor('SLM-Status',getValueFromKey(ladditionalData, 'SLA'));
                }
                q[0].BI_COL_Codigo_CUN__c = getValueFromKey(ladditionalData,'CodCUN');
                q[0].BIIN_Site__c = getValueFromKey(ladditionalData,'Site');
                q[0].BIIN_Id_Producto__c = getValueFromKey(ladditionalData,'IdProducto');
                if(getValueFromKey(ladditionalData, 'tiempoNetoApertura')!=''&&getValueFromKey(ladditionalData, 'tiempoNetoApertura')!=null){
                    q[0].BIIN_Tiempo_Neto_Apertura__c=decimal.valueOf(getValueFromKey(ladditionalData, 'tiempoNetoApertura'))/3600;
                }
                q[0].BI_Product_Categorization_Tier_1__c = getValueFromKey(ladditionalData,'catProducto1');
                q[0].BI_Product_Categorization_Tier_2__c = getValueFromKey(ladditionalData,'catProducto2');
                q[0].BI_Product_Categorization_Tier_3__c = getValueFromKey(ladditionalData,'catProducto3');
                q[0].Subject = caso.subject;
                q[0].Description = caso.description;
                //q[0].LastModifiedDate = startDateTime.addSeconds(integer.valueOf(caso.statusChangeDate));
                q[0].BI2_fecha_hora_de_resolucion__c = resolutionDate;
                q[0].TGS_Resolution__c = caso.resolution;
                q[0].BI_Resolution_Category_Tier_1__c = getValueFromKey(ladditionalData,'catResolucion1');
                q[0].BI_Resolution_Category_Tier_2__c = getValueFromKey(ladditionalData,'catResolucion2');
                q[0].BI_Resolution_Category_Tier_3__c = getValueFromKey(ladditionalData,'catResolucion3');
                q[0].RecordTypeId=recordTypeIncTec; 
                //GMN 01/02/2016 - Filter added to avoid null values
                if(caso.country != null && caso.country != ''){
                    q[0].BI_Country__c = caso.country;
                }
                else{
                    q[0].BI_Country__c = acc.BI_Country__c;
                }
                q[0].BIIN_Categorization_Tier_1__c = getValueFromKey(ladditionalData, 'catOperacional1');
                q[0].BIIN_Categorization_Tier_2__c = getValueFromKey(ladditionalData, 'catOperacional2');
                q[0].BIIN_Categorization_Tier_3__c = getValueFromKey(ladditionalData, 'catOperacional3');

                system.debug('Resolucion: '+q[0].TGS_Resolution__c);

                try{
                    system.debug(''+q[0].Status);
                    update q[0];
                    respuesta.ticketId=q[0].BI_Id_del_caso_legado__c;
                    respuesta.caseNumber=q[0].CaseNumber;
                    respuesta.code='000';
                    respuesta.message='El ticket se ha actualizado correctamente';
                    system.debug('La actualización ha funcionado');
                }catch (DmlException e){
                    respuesta.code='002';
                    log_id = BI_LogHelper.generate_BILog('BIIN_Actualizacion_Caso.actualizarCaso', 'BIIN', e, 'WebService');
                    respuesta.message='Se ha producido un error en la actualización del ticket. Log Id: ' + log_id;
                    system.debug('La actualización ha fallado'+e);
                }
            }
            
        }catch(QueryException e){
            respuesta.code='999';
            log_id = BI_LogHelper.generate_BILog('BIIN_Actualizacion_Caso.actualizarCaso', 'BIIN', e, 'WebService');
            respuesta.message='Se ha producido un error en la inserción del ticket. Log Id: ' + log_id;
            respuesta.message='Se ha producido un error en SF: '+e;
            system.debug('Se ha producido un error en SF'+e);
        }
        return respuesta;
    }
    

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jose María Martín Castaño
        Company:       Deloitte
        Description:   Clase para actualizar o insertar el ticket que viene de RoD.
        
        History:
        
        <Date>              <Author>                                        <Description>
        10/12/2015          Jose María Martín Castaño                       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static String getValueFromKey(List<Map<String, String>> ladditionalData, String key){
        String value;
        for(Integer i=0;i<ladditionalData.size();i++){
            Map<String, String> additionalData = ladditionalData[i];
            if(additionalData.get('key')==key){
                value= additionalData.get('value');
            }
        }
        return value;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Jose María Martín Castaño
        Company:       Deloitte
        Description:   Clase para actualizar o insertar el ticket que viene de RoD.
        
        History:
        
        <Date>              <Author>                                        <Description>
        10/12/2015          Jose María Martín Castaño                       Initial version
        25/01/2016          Guillermo Muñoz                                 Deprecated by the creation of BIIN_Helper class
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*public static String traducirValor(List<BIIN_Tabla_Correspondencia__c> l, String entrada){
        system.debug('entrada: '+ entrada);
        entrada = entrada.replaceAll('^[0-9]-', '');
        Integer i=0;
        String salida='';
        for(i=0;i<l.size();i++){             
            if(l[i].BIIN_RoD_ID__c==entrada){
                salida=l[i].BIIN_Valor_Salesforce__c;
            }else if(l[i].BIIN_Valor_Salesforce__c==entrada){
                salida=l[i].BIIN_RoD_ID__c;
            }else if(l[i].BIIN_Valor_Remedy__c==entrada){
                salida=l[i].BIIN_RoD_ID__c;
            }
        }
        salida= salida.replaceAll('^[0-9]-', '');
        system.debug('salida: '+ salida);
        return salida;
    }  */
    
//#############POJOs##################
    global with sharing class TicketRespuesta {
        String caseNumber;
        String ticketId;
        String code;
        String message;
    }
    
    global with sharing class TicketEntrada  {
        
        public String ticketId; //CaseNumber
        public String correlatorId;//BI_Id_del_caso_legado__c
        public String subject;//Subject
        public String description;//Description
        public String country;//BI_Country__c
        public String customerId;
        public String accountId;//BI_Validador_Fiscal__c 
        public String creationDate;//CreatedDate
        public String severity;//TGS_Impact__c
        public String priority;//Priority
        public String ticketType;
        public String source;//Origin
        public String parentTicket;//Parent
        public String ticketStatus;//Status
        public String statusChangeDate;
        public String statusChangeReason;//LastModifiedDate
        public String targetResolutionDate;
        public String resolutionDate;//BI2_fecha_hora_de_resolucion__c
        public String resolution;//TGS_Resolution__c
        public String responsibleParty;//BI_Assigned_Group__c
        public List <Map<String, String>> additionalData = new List <Map<String, String>> {};
    }
    
    public class AdditionalData {
        public String key;
        public String value;
    }
}