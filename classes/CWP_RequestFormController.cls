/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for lightning component CWP_RequestForm
    Test Class:    CWP_RequestFormControllerTest

    History:

    <Date>                  <Author>                <Change Description>
    22/04/2017              Everis                    Initial Version

    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

public with sharing class CWP_RequestFormController {
  public void metadata() {
    List<SelectOption> options = new List<SelectOption>();
    Schema.DescribeFieldResult fieldResult = Case.TGS_Op_Commercial_Tier_1__c.getDescribe();
   List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

   for( Schema.PicklistEntry f : ple)
   {
     System.debug(f);
      options.add(new SelectOption(f.getLabel(), f.getValue()));
   }

   fieldResult = Case.TGS_Op_Commercial_Tier_2__c.getDescribe();
   ple = fieldResult.getPicklistValues();

  for( Schema.PicklistEntry f : ple)
  {
    System.debug(f);
     options.add(new SelectOption(f.getLabel(), f.getValue()));
  }
  }

}