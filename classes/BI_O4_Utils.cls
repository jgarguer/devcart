/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Infinity W4+5 utility methods used by other classes
    Test Class:    BI_O4_Utils_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
global without sharing class BI_O4_Utils {
	
	 // Pattern to be searched
    private static final Pattern OBJECT_PATTERN = Pattern.compile('<a href="/(\\w*)\\?setupid=CustomObjects">(.+?)</a>');
    private static final String CUSTOM_OBJECT_PAGE_URL = '/p/setup/custent/CustomObjectsPage?setupid=CustomObjects';

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Gets all fields from the sobject passed

    In:            SobjectType: Sobject type
    Out:           List<String>: 
    
    History: 
    
    <Date>          <Author>                <Change Description>
    22/08/2016      Fernando Arteaga        Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global static List<String> getAllFieldsApi(SobjectType t) {
		if(t == null){
			return new List<String>();
		}
		return new List<String>(t.getDescribe().fields.getMap().keyset());
	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Builds a query selecting all fields from the sobject passed

    In:            SobjectType: Sobject type
    Out:           String: query string
    
    History: 
    
    <Date>          <Author>                <Change Description>
    22/08/2016      Fernando Arteaga        Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global static String getSOQLQueryAllFields(SobjectType type) {
		String query = 'SELECT ';
		for(String field : getAllFieldsApi(type)) {
			query += field + ', ';
		}
		query = query.removeEnd(', ') + ' FROM ';
		query += type.getDescribe().getName();
		return query;
	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Calculates geographical scope based on the countries passed

    In:            String: string containing country names separated by ;
    Out:           String: geographical scope calculated
    
    History: 
    
    <Date>          <Author>                <Change Description>
    22/08/2016      Fernando Arteaga        Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global static String calcGeographicalScope(String countries)
	{
		String scope = '';
		Integer numEMEA = 0, numAmerica = 0, numRest = 0;
		Set<String> setAmerica = new Set<String>();
		Set<String> setEMEA = new Set<String>();
		Set<String> setRest = new Set<String>();
		
		for (Schema.PicklistEntry val : Opportunity.BI_O4_Countries_America__c.getDescribe().getPicklistValues())
			setAmerica.add(val.getValue());
		
		for (Schema.PicklistEntry val : Opportunity.BI_O4_Countries_EMEA__c.getDescribe().getPicklistValues())
			setEMEA.add(val.getValue());
		
		for (Schema.PicklistEntry val : Opportunity.BI_O4_Countries_Rest_of_the_world__c.getDescribe().getPicklistValues())
			setRest.add(val.getValue());
			
		for (String country : countries.split(';'))
		{
			if (setAmerica.contains(country))
				numAmerica++;
			else if (setEMEA.contains(country))
				numEMEA++;
			else
				numRest++;
		}
		
		if ((numAmerica > 0 && numEMEA > 0 && numRest > 0) || (numAmerica > 0 && numEMEA > 0 && numRest == 0) || (numEMEA > 0 && numRest > 0 && numAmerica == 0) || (numAmerica > 0 && numRest > 0 && numEMEA == 0))
			scope = 'Global';
		else if ((numAmerica > 1 && numEMEA == 0 && numRest == 0) || (numEMEA > 1 && numAmerica == 0 && numRest == 0) || (numRest > 1 && numAmerica == 0 && numEMEA == 0))
			scope = 'Regional';
		else
			scope = 'Local';
		
		System.debug(LoggingLevel.INFO, 'scope:' + scope);
		
		return scope;
	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Method to get custom object Id based on custom object name

    In:            String: custom object name
    Out:           String: custom object id
    
    History: 
    
    <Date>          <Author>                <Change Description>
    07/10/2016      Fernando Arteaga        Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global static String getCustomObjectId(String custObjName) {
        // Check the parameter
        if(custObjName == null || custObjName == '') {
            return null;
        }
        // find out all listed custom object Ids
        Map<String, String> mapCustObjectIds = findObjectIds();
        
        String customObjectId = mapCustObjectIds.get(custObjName);
        
        // Find custom object Id
        return customObjectId;
    }

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       New Energy Aborda
    Description:   Method to get contents of custom objects setup page and prepare map of custom objects with its Salesforce Ids

    In:
    Out:
    
    History: 
    
    <Date>          <Author>                <Change Description>
    07/10/2016      Fernando Arteaga        Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static map<String, String> findObjectIds() {
        // PageReference instance. NOTE: the URL is standard not supposed to be changed between different orgs
        Pagereference pr = new PageReference(CUSTOM_OBJECT_PAGE_URL);
        
        // Get the Page content and store as String
        String htmlContent = !Test.isRunningTest() ? pr.getContent().toString() : '<html><a href="/myCustomObjectId?setupid=CustomObjects">Opportunity Qualify</a></html>';

        // Matcher for the defined pattern
        Matcher match = OBJECT_PATTERN.matcher(htmlContent);
        
        // Map to store Object Name with its Salesforce ID 
        Map<String, String> mapObjectIds = new Map<String, String>();
        // Iterate the matcher and find out the specified pattern
        while (match.find()) {
            // If matched, Add the custom object and Id to the map
            mapObjectIds.put(match.group(2), match.group(1));
        } 
        
        // Return map of Object Ids
        return mapObjectIds;
    }

}