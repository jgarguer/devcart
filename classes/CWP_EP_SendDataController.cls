/*---------------------------------------------------------------------------------------------------------
Author:        Carlos Vicente
Company:       Accenture.
Description:   Helper class that controls the Asynchronous response for API's II in Quotations

History:

<Date>                  <Author>					<Description>
10/04/2018				Carlos Vicente				Initial Version
08/05/2018				Carlos Vicente				Reshape class in 2 methods
-----------------------------------------------------------------------------------------------------------*/
public class CWP_EP_SendDataController {
	private static final String STATE_COMPLETED			= 'completed';
	private static final String QUOTE_SOLITITUDE		= 'solicitud de cotizacion';

	/*---------------------------------------------------------------------------------------------------------
	Author:			Angel Felipe Santaliestra Pasias
	Company:		Accenture.
	Description:	Method that query all data necessary


	History:

	<Date>                  <Author>					<Description>
	08/05/2018				Carlos Vicente				Initial Version
	-----------------------------------------------------------------------------------------------------------*/

	public static void queryData(List<Bit2WinHUB__Bulk_Configuration_Request__c> listItems, String urlCallback){
		System.debug('##CWP_EP_SendDataController # sendData ');
		System.debug('listItems: ' + listItems);
		System.debug('urlCalback: ' + urlCallback);
		Bit2WinHUB__Bulk_Configuration_Request__c bulkCR = listItems.get(0);
		System.debug('bulkCR: ' + bulkCR);

		List<NE__Order__c> listOrderIds =	[	SELECT
													Id,
													Name,
													Bit2WinHUB__BulkConfigurationRequest__c,
													CWP_AC_Cliente_Final__c,
													OwnerId
												FROM NE__Order__c
												WHERE Bit2WinHUB__BulkConfigurationRequest__c = :bulkCR.id];
		System.debug('listOrderIds: ' + listOrderIds);
		List<NE__OrderItem__c> listOrderItemIds =	[	SELECT
															Id,
															Name,
															NE__ProdName__c,
															NE__BaseRecurringCharge__c,
															NE__BaseOneTimeFee__c,
															NE__OrderId__c,
															NE__CatalogItem__c,
															NE__Parent_Order_Item__c
														FROM NE__OrderItem__c
														WHERE NE__OrderId__c in :listOrderIds];
		System.debug('listOrderItemIds: ' + listOrderItemIds);
		List<NE__Order_Item_Attribute__c> listOrderItemsAttributes = [	SELECT	
																			Id,
																			Name,
																			NE__Value__c,
																			NE__Order_Item__c
																		FROM NE__Order_Item_Attribute__c
																		WHERE NE__Order_Item__c in :listOrderItemIds];

		System.debug('listOrderItemsAttributes: ' + listOrderItemsAttributes);

		
		NE__Order__c orderConfiguration = [Select CWP_AC_Cliente_Final__c,OwnerId,Name from NE__Order__c where Id = :bulkCR.Bit2WinHUB__Configuration__c];
		System.debug('orderConfiguration: ' + orderConfiguration);
		String username = [Select username,id from User where Id = :orderConfiguration.OwnerId limit 1].username;
		API_AC_Quotations.WrpRelatedParty userRelatedParty = new API_AC_Quotations.WrpRelatedParty(username,'user','user');
		API_AC_Quotations.WrpRelatedParty customerRelatedParty = new API_AC_Quotations.WrpRelatedParty(String.valueOf(orderConfiguration.CWP_AC_Cliente_Final__c),'CLIENTE FINAL','customer');
		List<API_AC_Quotations.WrpRelatedParty> listRelatedParties = new List<API_AC_Quotations.WrpRelatedParty>();
		listRelatedParties.add(userRelatedParty);
		listRelatedParties.add(customerRelatedParty);
		System.debug('relatedParties : '+ listRelatedParties);
		System.debug('Serializao relatedParties : ' + JSON.serialize(listRelatedParties));	
		String nameOrder = orderConfiguration.Name;

		sendData(listOrderIds,listOrderItemIds,listOrderItemsAttributes,urlCallback,listRelatedParties,nameOrder);
	}
	/*---------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture.
	Description:	Method that manage the data, wrap the JSON and send the response


	History:

	<Date>                  <Author>					<Description>
	08/05/2018				Carlos Vicente				Initial Version
	10/05/2018				Carlos Vicente				Change the response and the callout
	-----------------------------------------------------------------------------------------------------------*/

	public static void sendData(List<NE__Order__c> listOrderIds,List<NE__OrderItem__c> listOrderItemIds,List<NE__Order_Item_Attribute__c> listOrderItemsAttributes,String urlCallback,List<API_AC_Quotations.WrpRelatedParty> listRelatedParties, String nameOrder) {
		
		Map<String,Set<Id>> mapBulkItem = new Map<String,Set<Id>>();
		Map<Id,API_AC_Quotations.WrpResponseQuoteItem> mapData = new Map<Id,API_AC_Quotations.WrpResponseQuoteItem>();
		
		API_AC_Quotations.WrpResponseQuoteItem wqi;
	
		for(NE__OrderItem__c orderItem : listOrderItemIds){
			
			System.debug('orderItem aqui: ' + orderItem );
			if(orderItem.NE__BaseRecurringCharge__c != 0.00){
				wqi = new API_AC_Quotations.WrpResponseQuoteItem(orderItem.NE__ProdName__c,orderItem.NE__BaseRecurringCharge__c,orderItem.NE__BaseOneTimeFee__c,String.valueOf(orderItem.Id));
				wqi.product = new API_AC_Quotations.WrpProduct();
				for (NE__Order_Item_Attribute__c oia : listOrderItemsAttributes) {
					if(oia.NE__Order_Item__c == orderItem.Id || oia.NE__Order_Item__c == orderItem.NE__Parent_Order_Item__c ){						
						if (oia.Name == API_AC_Quotations.COUNTRY) {
							wqi.product.characteristics.add(new API_AC_Quotations.WrpCharacteristic(API_AC_Quotations.COUNTRY,oia.NE__Value__c));
						} else if (oia.Name == API_AC_Quotations.CITY) {
							wqi.product.characteristics.add(new API_AC_Quotations.WrpCharacteristic(API_AC_Quotations.CITY,oia.NE__Value__c));
						} else if (oia.Name == API_AC_Quotations.ADDRESS) {
							wqi.product.characteristics.add(new API_AC_Quotations.WrpCharacteristic(API_AC_Quotations.ADDRESS,oia.NE__Value__c));
						} else if (oia.Name == API_AC_Quotations.CONTRACT) {
							wqi.product.characteristics.add(new API_AC_Quotations.WrpCharacteristic(API_AC_Quotations.CONTRACT,oia.NE__Value__c));
						} else if (oia.Name == API_AC_Quotations.ROUTER) {
							wqi.product.characteristics.add(new API_AC_Quotations.WrpCharacteristic(API_AC_Quotations.ROUTER,oia.NE__Value__c));
						} else if (oia.Name == API_AC_Quotations.BANDWIDTH) {
							wqi.product.characteristics.add(new API_AC_Quotations.WrpCharacteristic(API_AC_Quotations.BANDWIDTH,oia.NE__Value__c));
						} else if (oia.Name == API_AC_Quotations.SPECIFICATIONS) {
							wqi.product.characteristics.add(new API_AC_Quotations.WrpCharacteristic(API_AC_Quotations.SPECIFICATIONS,oia.NE__Value__c));
						} else if (oia.Name == API_AC_Quotations.TECHNOLOGY) {
							wqi.product.characteristics.add(new API_AC_Quotations.WrpCharacteristic(API_AC_Quotations.TECHNOLOGY,oia.NE__Value__c));
						}
					}
				}
				mapData.put(orderItem.Id,wqi);
				System.debug('mapData##mapData: '+mapData);
			}
		}

		API_AC_Quotations.WrpResponse wr;
		if(!mapData.isEmpty()){

		 	wr = new API_AC_Quotations.WrpResponse();
		 	wr.quoteItems = new List<API_AC_Quotations.WrpResponseQuoteItem>();
			
			wr.id								= nameOrder;
			wr.href								= 'endpoint/' + listOrderIds[0].Id;
			wr.quoteDate						= Datetime.now();
			wr.state							= STATE_COMPLETED;
			wr.description						= QUOTE_SOLITITUDE;
			wr.validFor							= new API_AC_Quotations.WrpValidFor();
			wr.validFor.startDateTime			= Datetime.now();
			wr.validFor.endDateTime				= Datetime.now().addMonths(1);
			wr.callbackUrl						= urlCallback;
			wr.relatedParties 					= listRelatedParties;
			
			for(NE__OrderItem__c orderItem : listOrderItemIds){
				if(orderItem.NE__BaseRecurringCharge__c != 0.00){
		 			wr.quoteItems.add(mapData.get(orderItem.Id));
				}
			}
		 
			System.debug('Serializado wr : ' + JSON.serialize(wr));	
			System.enqueueJob(new AsyncExecutionExample(JSON.serialize(wr),urlCallback));
		 	
		}
	}
	/*---------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente
	Company:		Accenture.
	Description:	Method/Queue that do the callout


	History:

	<Date>                  <Author>					<Description>
	10/05/2018				Carlos Vicente				Initial Version
	-----------------------------------------------------------------------------------------------------------*/
	public class AsyncExecutionExample implements Queueable, Database.AllowsCallouts {
		String wrJSON;
		String urlCallback;

		public AsyncExecutionExample(String wrJSON,String urlCallback){
			this.wrJSON = wrJSON;
			this.urlCallback = urlCallback;

		}
		public void execute(QueueableContext context) {
			
			Http oHttp;
			HttpRequest oHttpReq;
			HttpResponse oHttpRes;
			
			System.debug('wrJSON' + wrJSON);
			oHttp = new Http();
			oHttpReq = new HttpRequest();
			
			oHttpReq.setEndpoint(urlCallback);
			oHttpReq.setMethod('POST');
			
			oHttpReq.setBody(wrJSON);
			oHttpRes = oHttp.send(oHttpReq);
			System.debug(oHttpRes.getBody());
			
		}
	}
}