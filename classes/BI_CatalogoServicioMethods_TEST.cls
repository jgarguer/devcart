@isTest
private class BI_CatalogoServicioMethods_TEST {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando González
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_CatalogoServicioMethods class
    
    History: 
    <Date> 					<Author> 				<Change Description>
    27/03/2015      		Fernando González    	Initial Version		
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Fernando González
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_CatalogoServicioMethods
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	27/03/2015      		Fernando González    	Initial Version	
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/	
    static testMethod void catalogoServiciosMethodsTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    	
        list<BI_Catalogo_de_servicio__c> lstCat = new list<BI_Catalogo_de_servicio__c>();
		list<BI_Catalogo_de_servicio__c> lstCat_del = new list<BI_Catalogo_de_servicio__c>();
		list<BI_Solicitud_de_plan__c> lstSolPlan = new list<BI_Solicitud_de_plan__c>();
		list<BI_Solicitud_de_plan__c> lstSolPlan_del = new list<BI_Solicitud_de_plan__c>();
		list <Recordtype> lst_rt = [SELECT Id, Developername FROM Recordtype where DeveloperName = 'BI_Servicio' AND sObjectType = 'BI_Modelo__c'];
		
		lstSolPlan = BI_DataLoadAgendamientos.loadSolicitudPlan(2);//JEG 19/12/2016
		
		test.startTest();
		
		BI_Modelo__c modelo = new BI_Modelo__c(Name = 'test modelo' ,RecordTypeId = lst_rt[0].Id);
		insert modelo;
		
		
		for (BI_Solicitud_de_plan__c sol : lstSolPlan ) {
			
			BI_Catalogo_de_servicio__c cat = new BI_Catalogo_de_servicio__c(BI_Id_de_servicio__c = modelo.Id,
												               	BI_Plan__c = sol.Id );
			
		    lstCat.Add(cat);										               	
		}
		
		try{
			
    		insert lstCat;
    		
    	}catch(exception e){
    		system.assert(e.getMessage().contains('No se puede modificar el catálogo de servicio en el estado actual del plan'));
    	}
    	
    	
    	lstSolPlan_del = BI_DataLoadAgendamientos.loadSolicitudPlan_SinEstado(2);//JEG 19/12/2016
		
		
		for (BI_Solicitud_de_plan__c sol : lstSolPlan_del ) {
			BI_Catalogo_de_servicio__c cat = new BI_Catalogo_de_servicio__c(BI_Id_de_servicio__c = modelo.Id,
																				BI_Plan__c = sol.Id );
		    
		    lstCat_del.Add(cat);										               	
		}
		
		insert lstCat_del;										                

    	delete lstCat_del;

		test.stopTest();
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Fernando Arteaga
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_CatalogoServicioMethods.validatePlanServiceDuplicated
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	01/04/2015				Fernando Arteaga        Initial version
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void Test_ValidatePlanServiceDuplicated()
    {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List<BI_Catalogo_de_servicio__c> lstCat = new List<BI_Catalogo_de_servicio__c>();
		List<BI_Solicitud_de_plan__c> lstSolPlan = new List<BI_Solicitud_de_plan__c>();
		List<RecordType> lst_rt = [SELECT Id, Developername FROM RecordType where DeveloperName = 'BI_Servicio' AND sObjectType = 'BI_Modelo__c'];

    	Account acc = new Account(Name = 'test Account',
        						  BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                  BI_Activo__c = Label.BI_Si,
                                  BI_Segment__c = 'test',
                                  BI_Subsegment_Regional__c = 'test',
                                  BI_Territory__c = 'test');
        
        insert acc;       

        for(Integer j = 0; j < 2 ; j++)//JEG 19/12/2016
        {
            BI_Solicitud_de_plan__c solicitud = new BI_Solicitud_de_plan__c(BI_Cliente__c = acc.Id,
										                BI_Plan_actual__c = 'test plan',
										                BI_Situacion_actual_del_cliente__c = 'test situacion actual',
										                BI_Oferta_marketing__c = 'test oferta area',
										                BI_Codigo_plan__c = 't',
										                BI_Comentarios_de_operaciones__c = 'test comentarios',
										                BI_Nombre_plan__c = 'test nombre ',
										                BI_Estado_solicitud__c = 'Pendiente',
										                BI_Tipo_de_servicio__c = 'Por defecto');
            
            lstSolPlan.add(solicitud);
        }
        
        insert lstSolPlan;
	            
		Test.startTest();
		
		BI_Modelo__c modelo = new BI_Modelo__c(Name = 'test modelo',
											   RecordTypeId = lst_rt[0].Id);
		insert modelo;
		
		for (BI_Solicitud_de_plan__c sol : lstSolPlan )
		{
			BI_Catalogo_de_servicio__c cat = new BI_Catalogo_de_servicio__c(BI_Id_de_servicio__c = modelo.Id,
												               				BI_Plan__c = sol.Id);
			
		    lstCat.add(cat);
		}
		
		insert lstCat;
		
		BI_Catalogo_de_servicio__c cat = new BI_Catalogo_de_servicio__c(BI_Id_de_servicio__c = modelo.Id,
											               				BI_Plan__c = lstSolPlan[0].Id);

		try
		{
			insert cat;
		}
		catch (Exception e)
		{
			System.assert(e.getMessage().contains('No puede asignar el servicio ya que el Plan ya lo tiene asignado'));
		}
		
		BI_CatalogoServicioMethods.validatePlanServiceDuplicated(null);
		
		Integer i = [SELECT count()
					 FROM BI_Log__c
					 WHERE BI_Asunto__c = 'BI_CatalogoServicioMethods.validatePlanServiceDuplicated'
					 AND BI_Bloque_funcional__c = 'BI_EN'
					 AND BI_Tipo_de_componente__c = 'Trigger'
					 // AND CreatedDate = :System.now() //JEG 19/12/2016
					 LIMIT 1];

		System.assertEquals(i, 1);
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}