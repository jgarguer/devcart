@isTest
private class CWP_InventoryController_TEST {
    @isTest
    public static void getAssetListTest() {
        // Setup test data
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' OR Name = 'Usuario estándar']; 
        User u = new User(Alias = 'standt', Email='standardusertesteveris@testorgeveris.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusertesteveris@testorgeveris.com');

        System.runAs(new User(id=userInfo.getUserId())) {
            // The following code runs as user 'u' 
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
            CWP_InventoryController.getAssetList();
        }
    }
}