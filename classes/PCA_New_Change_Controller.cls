global class PCA_New_Change_Controller {
    public Case mcase {get; set;}
    public Case templateCase {get; set;}
    public String tipoTicket {get; set;}
    public final String ORIGIN = 'Customer Web Portal';
    public final String RECORD_TYPE_NAME= 'Change';
    public final String NONE = Label.TGS_CWP_NONE;
    
    public Integer level{get; set;}
    public Integer templateLevel{get; set;}
    public List<SelectOption> listlevel1{get; set;}
    public List<SelectOption> listlevel2{get; set;}
    public List<SelectOption> listlevel3{get; set;}
    public List<SelectOption> listlevel4{get; set;}
    public List<SelectOption> listlevel5{get; set;}    
    public List<SelectOption> siteList {get; set;}
    
    public String level1{get; set;}
    public String level2{get; set;}
    public String level3{get; set;}
    public String level4{get; set;}
    public String level5{get; set;}
    public String endSite {get; set;}
    
    public String namelevel1{get; set;}
    public String namelevel2{get; set;}
    public String namelevel3{get; set;}
    public String namelevel4{get; set;}
    public String namelevel5{get; set;}
    public String nameSite{get; set;}
    
    public String userId;
    public String sId {get; set;}
    public String suid {get; set;}
    
	//POPUP
    public String lookupSU{get; set;}
    public List<NE__OrderItem__c> lookupServices{get; set;}

	//Attachments
    public List<Attachment> listCaseAttachments {get;set;}
    public Attachment newAttachment {get;set;}
    public blob myAttachedBody {get; set;}
    public blob myAttachedBody2 {get; set;}
    public blob myAttachedBody3 {get; set;}
    
    public String attachmentName1 {get; set;}
    public String attachmentName2 {get; set;}
    public String attachmentName3 {get; set;}
    
    public String attachmentDesc1 {get; set;}
    public String attachmentDesc2 {get; set;}
    public String attachmentDesc3 {get; set;}
    public List<String> listCaseAttachmentsName {get; set;}

    public boolean error {get;set;} 

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Description:  Controller of the new change view
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_New_Change_Controller () {
        if (ApexPages.currentPage().getParameters().get('error') != null) {
            error = true;
        } else {
            error = false;
        }
        
        System.debug('[PCA_New_Change_Controller Constructor] error: ' + error);
        
        listCaseAttachmentsName = new List<String>();
        
    	//Inicialización de información básica del usuario y Case
        userId = UserInfo.getUserId();
        level = TGS_Portal_Utils.getAccountLevel(userId);
        
        System.debug('[PCA_New_Change_Controller Constructor] userId: ' + userId);
        System.debug('[PCA_New_Change_Controller Constructor] level: ' + level);
        
        mcase = new Case();
        mCase.RecordTypeId = TGS_Portal_Utils.getRecordType(RECORD_TYPE_NAME,'Case')[0].Id;
        mCase.AccountId = TGS_Portal_Utils.getUserAccount(userId).Id;
        
    	//Comprobamos si viene de un case //TO DO: sacar al utils
        String templateCaseId = ApexPages.currentPage().getParameters().get('caseId');
        if(templateCaseId!='' && templateCaseId!=null){
            templateCase = TGS_Portal_Utils.getTemplateCaseId(templateCaseId); 
            initializeCaseFields();
        }
        
        System.debug('[PCA_New_Change_Controller Constructor] templateCase: ' + templateCase);
        
    	//Comprobamos si viene de una service unit e inicializamos el valor de los tiers
        suid = ApexPages.currentPage().getParameters().get('suId');
        
        System.debug('[PCA_New_Change_Controller Constructor] suid: ' + suid);
        
        if (suid != null && suid != '') {
            mCase.TGS_Customer_Services__c = suId;
            initializeProductTiers(suid);
        }
        
    	//Comprobamos si viene de un service e inicializamos el valor de los tiers
        sid = ApexPages.currentPage().getParameters().get('sId');
        
        System.debug('[PCA_New_Change_Controller Constructor] sid: ' + sid);
        
        if (sid != null && sid != '') {
           initializeProductTiersBySID(sid);
        }
        
    	//Inicialización de la Jerarquía    
        loadLevel1(); 
        
    	//Inicializamos la lista de adjuntos y el adjunto
        listCaseAttachments = new List<Attachment>();
        newAttachment = new Attachment();
        
        System.debug('[PCA_New_Change_Controller Constructor] mcase: ' + mcase);
    }
    
    //METODOS DE INICIALIZACION
    /**
   	 * Method:         initializeCaseFields
     * Description:    Inicializa los datos del case que se está mostrando.
     */
    private void initializeCaseFields(){
        mcase.TGS_Product_Tier_1__c = templateCase.TGS_Product_Tier_1__c;
        mcase.TGS_Product_Tier_2__c = templateCase.TGS_Product_Tier_2__c;
        mcase.TGS_Product_Tier_3__c = templateCase.TGS_Product_Tier_3__c;
        //RPM Cambio categorizacion
        mcase.TGS_Operational_Tier_1_text__c = templateCase.TGS_Op_Technical_Tier_1__c;
        mcase.TGS_Operational_Tier_2_text__c = templateCase.TGS_Op_Technical_Tier_2__c;
        mcase.TGS_Operational_Tier_3_text__c = templateCase.TGS_Op_Technical_Tier_3__c;
        //mcase.TGS_Op_Technical_Tier_1__c = templateCase.TGS_Op_Technical_Tier_1__c;
        //mcase.TGS_Op_Technical_Tier_2__c = templateCase.TGS_Op_Technical_Tier_2__c;
        //mcase.TGS_Op_Technical_Tier_3__c = templateCase.TGS_Op_Technical_Tier_3__c;
        //END RPM
        mcase.TGS_Impact__c = templateCase.TGS_Impact__c;
        mcase.TGS_Urgency__c = templateCase.TGS_Urgency__c;
        mcase.subject = templateCase.subject;
        mcase.description = templateCase.description;
        mcase.TGS_Customer_Services__c= templateCase.TGS_Customer_Services__c;
        mcase.TGS_Ticket_Site__c= templateCase.TGS_Ticket_Site__c;
        mcase.AccountId = templateCase.AccountId;
       	templateLevel = TGS_Portal_Utils.getAccountLevelForAccount(mCase.AccountId);
        
        System.debug('[PCA_New_Change_Controller.initializeCaseFields] mcase: ' + mcase);
    }
    
    /**
   	 * Method:         initializeProductTiers
     * Description:    Inicializa los tiers del case que se está mostrando.
     */
    public void initializeProductTiers() {
        if(mcase.TGS_Customer_Services__c != null){
            initializeProductTiers(mcase.TGS_Customer_Services__c);
        }

        System.debug('[PCA_New_Change_Controller.initializeCaseFields] mcase: ' + mcase);
    }
	
    /**
   	 * Method:         initializeProductTiers
     * Description:    Inicializa los tiers del case que se está mostrando.
     */
   	@TestVisible
    private void initializeProductTiers(String suid) {
        NE__OrderItem__c[] configurationItems = TGS_Portal_Utils.getConfigurationItemsBySuId(suid);
        if (configurationItems.size()>0){
            NE__OrderItem__c configurationItem = configurationItems[0];

            mCase.TGS_Product_Tier_1__c = configurationItem.NE__ProdId__r.TGS_CWP_Tier_1__c; 
            mCase.TGS_Product_Tier_2__c = configurationItem.NE__ProdId__r.TGS_CWP_Tier_2__c;
            mCase.TGS_Product_Tier_3__c = configurationItem.NE__ProdId__r.Name;
        }
        
        System.debug('[PCA_New_Change_Controller.initializeProductTiers] mcase.TGS_Product_Tier_1__c: ' + mcase.TGS_Product_Tier_1__c);
        System.debug('[PCA_New_Change_Controller.initializeProductTiers] mcase.TGS_Product_Tier_2__c: ' + mcase.TGS_Product_Tier_2__c);
        System.debug('[PCA_New_Change_Controller.initializeProductTiers] mcase.TGS_Product_Tier_3__c: ' + mcase.TGS_Product_Tier_3__c);
    }
    
    /**
   	 * Method:         initializeProductTiersBySID
     * Description:    Inicializa los tiers del case que se está mostrando.
     */
    private void initializeProductTiersBySID(String sid) {
        NE__Catalog_Item__c[] catalogItems = TGS_Portal_Utils.getCatalogItemsBySId(sid);
        if (catalogItems.size()>0){
            NE__Catalog_Item__c catalogItem = catalogItems[0];
            
            mCase.TGS_Product_Tier_1__c = catalogItem.NE__ProductId__r.TGS_CWP_Tier_1__c;
            mCase.TGS_Product_Tier_2__c = catalogItem.NE__ProductId__r.TGS_CWP_Tier_2__c;
            mCase.TGS_Product_Tier_3__c = catalogItem.NE__ProductId__r.Name;
        }
        
        System.debug('[PCA_New_Change_Controller.initializeProductTiersBySID] mcase.TGS_Product_Tier_1__c: ' + mcase.TGS_Product_Tier_1__c);
        System.debug('[PCA_New_Change_Controller.initializeProductTiersBySID] mcase.TGS_Product_Tier_2__c: ' + mcase.TGS_Product_Tier_2__c);
        System.debug('[PCA_New_Change_Controller.initializeProductTiersBySID] mcase.TGS_Product_Tier_3__c: ' + mcase.TGS_Product_Tier_3__c);
    }
    
	//PAGEREFERENCE BUTTONS
	/**
     * Method:         	crearCase
     * Description:    	Método llamado cuando se pulsa el botón Submit.
     * 					Rellena los datos del change, lo inserta en la base de datos y lo envía a RoD.
	 */
    public PageReference crearCase() {
        Case auxCase;
        if (mcase.subject==null || mcase.subject==''){
            PageReference pr = new PageReference('/empresasplatino/PCA_New_Change');
            pr.getParameters().put('error', 'true');
            pr.setRedirect(true);
            return pr;
        }
        
        try {
            mCase.origin = ORIGIN;
            //Site
            if (endSite != '' && endSite != null) {
                mCase.TGS_Ticket_Site__c = endSite;
            }
            if (level5 != '' && level5 != null) {
                mCase.accountId = level5;
            }else if (level4 != '' && level4 != null){
                mCase.accountId = level4;
            }else if (level3 != '' && level3 != null){
                mCase.accountId = level3;
            }else if (level2 != '' && level2 != null){
                mCase.accountId = level2;
            }else if (level1 != '' && level1 != null){
                 mCase.accountId = level1;           
            }
            
            System.debug('[PCA_New_Change_Controller.crearCase] mCase.TGS_Ticket_Site__c: ' + mCase.TGS_Ticket_Site__c);
            System.debug('[PCA_New_Change_Controller.crearCase] mCase.accountId: ' + mCase.accountId);
            
            TGS_CallRodWs.inFutureContext= true;
            insert mCase;
            TGS_CallRodWs.inFutureContext= false;
            auxCase = TGS_Portal_Utils.getCaseNumber(mCase.Id);
            
            //Insertamos los Attachment (si los hay) creando un WorkInfo
            if(myAttachedBody != null || myAttachedBody2 != null || myAttachedBody3 != null){
                crearWorkinfo(mCase.Id);
            }
            
            /* Integracion RoD */
            TGS_IntegrationUtils.sendCaseAndWItoRoD(mCase.Id);
        } catch (Exception e) {
            System.debug(e);
            System.debug('[PCA_New_Change_Controller.crearCase] ERROR. mCase.contact: ' + mCase.contact);
            PageReference pr = new PageReference('/empresasplatino/PCA_Cases');
            pr.getParameters().put('error', 'true');
            pr.setRedirect(true);
            return pr;
        }
        
        System.debug('[PCA_New_Change_Controller.crearCase] mCase.contact: ' + mCase.contact);
        
        PageReference pr = new PageReference('/empresasplatino/PCA_Ticket_Detail?caseId=' + mCase.Id);
        pr.getParameters().put('success', 'true');
        pr.setRedirect(true);
        return pr;
    }
    
    /**
     * Method:         	cancelar
     * Description:    	Método llamado cuando se pulsa el botón Cancel.
	 */
    public PageReference cancelar() {
        PageReference pr = new PageReference('/empresasplatino/PCA_Cases');
        pr.setRedirect(true);
        return pr;          
    }
	
	//FIELD LOAD AND REFRESH
	/**
     * Method:         	loadLevel1
     * Description:    	Loads level1 information and calls level2.
	 */
    public void loadLevel1(){
        List<Account> accountList = new List<Account>();
        listLevel1 = new List<SelectOption>();
        if(templateCase != null){//Viene de un template, sobreescribimos el user para el método 
            accountList = TGS_Portal_Utils.getLevel1(templateCase.AccountId,1);
        }else{
            accountList = TGS_Portal_Utils.getLevel1(userId,0);
        }
        listlevel1.add(new SelectOption('', ''));              
        for (Account acc : accountList) {
            listLevel1.add(new SelectOption(acc.Id, acc.Name));
        }
        if(accountList.size() == 1){
            level1 = accountList[0].Id;
        }
        
        System.debug('[PCA_New_Change_Controller.loadLevel1] level1: ' + level1);
        
        loadLevel2();                     
    }

	/**
     * Method:         	loadLevel2
     * Description:    	Loads level2 information and calls level3.
	 */
    public void loadLevel2(){
        List<Account> accountList = new List<Account>();
        listLevel2 = new List<SelectOption>();       
        if(level1 != '' && level1 != null){
            accountList = TGS_Portal_Utils.getLevel2(level1,1);           
            listlevel2.add(new SelectOption('', ''));        
            for (Account acc : accountList) {
                listLevel2.add(new SelectOption(acc.Id, acc.Name));
            }             
            if(templateCase != null && templateLevel>=2){//Viene de un template 
                level2 = TGS_Portal_Utils.getLevel2(templateCase.AccountId,1)[0].Id;
            }else if(level >=2 ){//Está por debajo en la jerarquía, no puede elegir
                level2 = TGS_Portal_Utils.getLevel2(userId,0)[0].Id;
            }
        }
        
        System.debug('[PCA_New_Change_Controller.loadLevel2] level2: ' + level2);
        
        loadLevel3();
    }
	
	/**
     * Method:         	loadLevel3
     * Description:    	Loads level3 information and calls level4.
	 */
    public void loadLevel3(){
        List<Account> accountList = new List<Account>();
        listLevel3 = new List<SelectOption>();       
        if(level2 != '' && level2 != null){
            accountList = TGS_Portal_Utils.getLevel3(level2,1);            
            listlevel3.add(new SelectOption('', ''));        
            for (Account acc : accountList) {
                listLevel3.add(new SelectOption(acc.Id, acc.Name));
            }             
            if(templateCase != null && templateLevel>=3){//Viene de un template 
                level3 = TGS_Portal_Utils.getLevel3(templateCase.AccountId,1)[0].Id;
            }else if(level >=3 ){
                level3 = TGS_Portal_Utils.getLevel3(userId,0)[0].Id;
            }           
        }
        
        System.debug('[PCA_New_Change_Controller.loadLevel3] level3: ' + level3);
        
        loadLevel4();                     
    }
    
    /**
     * Method:         	loadLevel4
     * Description:    	Loads level4 information and calls level5 and sites
	 */
    public void loadLevel4(){
        List<Account> accountList = new List<Account>();
        listLevel4 = new List<SelectOption>();        
        if(level3 != '' && level3 != null){
            accountList = TGS_Portal_Utils.getLevel4(level3,1);           
            listlevel4.add(new SelectOption('', ''));        
            for (Account acc : accountList) {
                listLevel4.add(new SelectOption(acc.Id, acc.Name));
            }             
            if(templateCase != null && templateLevel>=4){//Viene de un template 
                level4 = TGS_Portal_Utils.getLevel4(templateCase.AccountId,1)[0].Id;
            }else if(level >=4 ){
                level4 = TGS_Portal_Utils.getLevel4(userId,0)[0].Id;
            }           
        }
        
        System.debug('[PCA_New_Change_Controller.loadLevel4] level4: ' + level4);
        
        loadLevel5();
        loadSites();                     
    }
    
    /**
     * Method:         	loadLevel5
     * Description:    	Loads level5 information and calls sites.
	 */
    public void loadLevel5(){
        List<Account> accountList = new List<Account>();
        listLevel5 = new List<SelectOption>();       
        if(level4 != '' && level4 != null){
            accountList = TGS_Portal_Utils.getLevel5(level4,1);           
            listlevel5.add(new SelectOption('', ''));                    
            for (Account acc : accountList) {
                listLevel5.add(new SelectOption(acc.Id, acc.Name));
            }             
            if(templateCase != null && templateLevel>=5){//Viene de un template 
                level5 = TGS_Portal_Utils.getLevel5(templateCase.AccountId,1)[0].Id;
            }else if(level >=5 ){
                level5 = TGS_Portal_Utils.getLevel5(userId,0)[0].Id;
            }           
        }
        
        System.debug('[PCA_New_Change_Controller.loadLevel5] level5: ' + level5);
        
        loadSites();                            
    }
    
    /**
     * Method:         	loadSites
     * Description:    	Loads sites information.
	 */
    public void loadSites(){
        List<BI_Punto_de_instalacion__c> mySites = new List<BI_Punto_de_instalacion__c>();
        siteList = new List<SelectOption>();
        mySites = TGS_Portal_Utils.getSites(level4);
        siteList.add(new SelectOption('', ''));
        for (BI_Punto_de_instalacion__c site : mySites) {
            siteList.add(new SelectOption(site.Id,site.Name));
        }
        if(templateCase != null && templateCase.TGS_Ticket_Site__c != null){//Viene de un template 
            endSite = templateCase.TGS_Ticket_Site__c;
        }
        
        System.debug('[PCA_New_Change_Controller.loadSites] siteList: ' + siteList);
        System.debug('[PCA_New_Change_Controller.loadSites] endSite: ' + endSite);
    }
    
    /**
     * Method:         	loadLookupServices
     * Description:    	Loads lookups.
	 */
    public void loadLookupServices(){
        List<Account> accounts = TGS_Portal_Utils.getAccountsBelowHierarchy(userId);        
        lookupServices = TGS_Portal_Utils.getlookupServiceUnits(accounts);
        
        System.debug('[PCA_New_Change_Controller.loadLookupServices] lookupServices: ' + lookupServices);
    }
    
	//ATTACHMENTS    
    /**
     * Method:         uploadAttachment
     * Description:    Da valor a campos de 'newAttachment' y lo añade a la lista 'listCaseAttachments'.
     *                 Se crea un nuevo SObject Attachment relacionado a 'newAttachment'.
     */
    public void uploadAttachment() {
        try{
            if(myAttachedBody != null && newAttachment.Name != null){
                newAttachment.Body = myAttachedBody;
                newAttachment.OwnerId = UserInfo.getUserId();
                newAttachment.IsPrivate = true;
                listCaseAttachments.add(newAttachment);
                listCaseAttachmentsName.add(newAttachment.Name);
                
        		System.debug('[PCA_New_Change_Controller.uploadAttachment] listCaseAttachments: ' + listCaseAttachments);
        		System.debug('[PCA_New_Change_Controller.uploadAttachment] listCaseAttachmentsName: ' + listCaseAttachmentsName);
                
                newAttachment = new Attachment();
            }
        }catch(Exception e){
            System.Debug('[PCA_New_Change_Controller.uploadAttachment] ERROR: ' + e);
        }
    }
    
    /**
     * Method:         crearWorkinfo
     * Param:          id es el identificador del objeto creado
     * Description:    Crea un SObject TGS_WorkInfo__c relacionado con el Case creado y lo inserta en la base de datos.
     *                 También inserta todos los Attachment que se encuentran 'listCaseAttachments' y que se relacionan
     *                 mediante ParentId con el SObject TGS_WorkInfo__c creado.
     */
    public PageReference crearWorkinfo(Id id) {
        TGS_Work_Info__c work = new TGS_Work_Info__c();
        work.TGS_Description__c = 'Case created';
        work.TGS_Case__c = id;
        work.TGS_Public__c = true;
        
        System.debug('[PCA_New_Change_Controller.crearWorkinfo] work: ' + work);
        
        TGS_CallRodWs.inFutureContextWI = true;
        insert work;
        TGS_CallRodWs.inFutureContextWI = false;
        List<Attachment> listToInsert = new List<Attachment>();
        if (myAttachedBody != null) {
            newAttachment = new Attachment();
            newAttachment.Name = attachmentName1;
            newAttachment.Description = TGS_IntegrationUtils.UNO+attachmentDesc1;
            newAttachment.Body = myAttachedBody;
            newAttachment.OwnerId = UserInfo.getUserId();
            newAttachment.ParentId = work.Id;
            listToInsert.add(newAttachment);
        }
        if (myAttachedBody2 != null) {
            newAttachment = new Attachment();
            newAttachment.Name = attachmentName2;
            newAttachment.Description = TGS_IntegrationUtils.DOS+attachmentDesc2;
            newAttachment.Body = myAttachedBody2;
            newAttachment.OwnerId = UserInfo.getUserId();
            newAttachment.ParentId = work.Id;
            listToInsert.add(newAttachment);
        }
        if (myAttachedBody3 != null) {
            newAttachment = new Attachment();
            newAttachment.Name = attachmentName3;
            newAttachment.Description = TGS_IntegrationUtils.TRES+attachmentDesc3;
            newAttachment.Body = myAttachedBody3;
            newAttachment.OwnerId = UserInfo.getUserId();
            newAttachment.ParentId = work.Id;
            listToInsert.add(newAttachment);
        }
        
        System.debug('[PCA_New_Change_Controller.crearWorkinfo] listToInsert: ' + listToInsert);
        
        try {
            if (listToInsert.size() > 0) { 
                TGS_CallRoDWs.inFutureContextAttachment = true;
                Database.SaveResult[] results = Database.insert(listToInsert);
                TGS_CallRoDWs.inFutureContextAttachment = false;
                
        		System.debug('[PCA_New_Change_Controller.crearWorkinfo] results: ' + results);
            }
        } catch (DmlException e) {
            System.debug('Error al subir los adjuntos del workinfo del case '+id + ': ' + e);
        }
        
        PageReference pr = ApexPages.currentPage();
        String templateCaseId = ApexPages.currentPage().getParameters().get('caseId');
        if(templateCaseId!='' && templateCaseId!=null){
            pr.getParameters().put('caseId', ApexPages.currentPage().getParameters().get('caseId'));
        }
        if (suid != null && suid != '') {
            pr.getParameters().put('suid', ApexPages.currentPage().getParameters().get('suid'));
        }
        if (sid != null && sid != '') {
            pr.getParameters().put('sId', ApexPages.currentPage().getParameters().get('sId'));
        }
        return pr;
    }
}