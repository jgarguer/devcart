@isTest
private class BI_ParqueComercialRest_TEST
{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Class to cover methods from BI_ParqueComercialRest
    
    History:
    
    <Date>            <Author>          	<Description>
    09/12/2015        Fernando Arteaga      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_ParqueComercialRest.insertCommercialAssets()
    History:
    
    <Date>              <Author>                <Description>
    09/12/2015          Fernando Arteaga        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void insertCommercialAssetsTest()
    {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

		List<Profile> listProfile = [SELECT Id FROM Profile WHERE Name = 'BI_Standard'];
		
        User u = new User(profileId = listProfile[0].Id, username = 'user.standard@biencsb.com', email = 'user.standard@biencsb.com', 
               emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
               languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
               alias='ucsb', lastname='user standard bien csb', BI_Permisos__c = 'Jefe de Ventas',
              isActive = True);
              
        insert u;
        
        System.runAs(u)
        {
	        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
	        
	        List<String>lst_region = new List<String>();
	       
	        lst_region.add('Argentina');
	        
	        //retrieve the country
	        BI_Code_ISO__c region = biCodeIso.get('ARG');
	       
	        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
	        List <Contact> lst_con = BI_DataLoadRest.loadContacts(1, lst_acc);
	
			NE__Catalog__c catalog = new NE__Catalog__c(Name = 'Dummy Catalog');
			insert catalog;
			 
	        NE__Product__c prod1 = new NE__Product__c();
	        prod1.Name = 'Prod1';
	        prod1.Rama_Local__c = 'Servicios Digitales CSB';
	        //prod1.CSB_Application_Billing_Code__c = '61';
	        prod1.NE__Recurring_Cost__c = 100;
	        prod1.NE__One_Time_Cost__c = 25;
	        
	        NE__Product__c prod2 = new NE__Product__c();
	        prod2.Name = 'Prod2';
	        //prod2.CSB_Application_Billing_Code__c = '76';
	        prod2.NE__Recurring_Cost__c = 100;
	        prod2.NE__One_Time_Cost__c = 25;
	        
	        NE__Product__c prod3 = new NE__Product__c();
	        prod3.Name = 'Prod3';
	        //prod3.CSB_Application_Billing_Code__c = '666';
	        prod3.NE__Recurring_Cost__c = 100;
	        prod3.NE__One_Time_Cost__c = 25;
	        
	        insert new List<NE__Product__c>{prod1, prod2, prod3};
	        
	        NE__Catalog_Item__c catItem1 = new NE__Catalog_Item__c(NE__Catalog_Id__c = catalog.Id, NE__ProductId__c = prod1.Id, BI_CSB_Code__c = 'CAT1');
	        NE__Catalog_Item__c catItem2 = new NE__Catalog_Item__c(NE__Catalog_Id__c = catalog.Id, NE__ProductId__c = prod2.Id, BI_CSB_Code__c = 'CAT2');
	        NE__Catalog_Item__c catItem3 = new NE__Catalog_Item__c(NE__Catalog_Id__c = catalog.Id, NE__ProductId__c = prod3.Id, BI_CSB_Code__c = 'CAT3');
	        
	        insert new List<NE__Catalog_Item__c>{catItem1, catItem2, catItem3};
	        
	        RestRequest req = new RestRequest();
	        RestResponse res = new RestResponse();
	
	        req.requestURI = '/accountresources/v1/assets/';  
	        req.httpMethod = 'POST';
	
			Test.startTest();
			
	        Blob body = Blob.valueof('{"usageReportDate":"20151210","Usage":null}');
	        req.requestBody = body;
	        RestContext.request = req;
	        RestContext.response = res;
	        
			BI_ParqueComercialRest.insertCommercialAssets();
	        system.assertEquals(400, RestContext.response.statuscode);
	        
	        body = Blob.valueof('{"usageReportDate":"2015-12-10"}');
	        req.requestBody = body;
	        RestContext.request = req;
	        
	        BI_ParqueComercialRest.insertCommercialAssets();
	        system.assertEquals(400, RestContext.response.statuscode);
	        
	        body = Blob.valueof('{"usageReportDate":"2015-12-10T00:00:00Z","Usage":[{"userId":"' + lst_acc[0].BI_Id_CSB__c + '","accountId":"' + lst_acc[0].BI_Id_CSB__c + '","subscriptionId":"100045","productId":"CAT1","consumption":{"characteristic":"product 1","validFor":{"startDateTime":"2015-12-10T00:00:00Z","endDateTime":"2016-12-10T00:00:00Z"},"unitOfMeasure":{"units:":"licenses"},"value":"15"}},{"userId":"' + lst_acc[0].BI_Id_CSB__c + '","accountId":"' + lst_acc[0].BI_Id_CSB__c + '","subscriptionId":"100100","productId":"CAT2","consumption":{"characteristic":"product 2","validFor":{"startDateTime":"2015-12-10T00:00:00Z","endDateTime":"2016-12-10T00:00:00Z"},"unitOfMeasure":{"units:":"licenses"},"value":"50"}},{"userId":"' + lst_acc[0].BI_Id_CSB__c + '","accountId":"' + lst_acc[0].BI_Id_CSB__c + '","subscriptionId":"100045","productId":"CAT3","consumption":{"characteristic":"product 3", "validFor":{"startDateTime":"2015-12-10T00:00:00Z","endDateTime":"2016-12-10T00:00:00Z"},"unitOfMeasure":{"units:":"licenses"},"value":"25"}}]}');
	        req.requestBody = body;
	        BI_ParqueComercialRest.insertCommercialAssets();
	        system.assertEquals(200, RestContext.response.statuscode);
	
	        body = Blob.valueof('{"usageReportDate":"2015-12-10T00:00:00Z","Usage":[{"userId":"' + lst_acc[0].BI_Id_CSB__c + '","accountId":"' + lst_acc[0].BI_Id_CSB__c + '","subscriptionId":"100045","productId":"CAT1","consumption":{"characteristic":"product 1","validFor":{"startDateTime":"2015-12-10T00:00:00Z","endDateTime":"2016-12-10T00:00:00Z"},"unitOfMeasure":{"units:":"licenses"},"value":"15"}},{"userId":"' + lst_acc[0].BI_Id_CSB__c + '","accountId":"' + lst_acc[0].BI_Id_CSB__c + '","subscriptionId":"100100","productId":"CAT2","consumption":{"characteristic":"product 2","validFor":{"startDateTime":"2015-12-10T00:00:00Z","endDateTime":"2016-12-10T00:00:00Z"},"unitOfMeasure":{"units:":"licenses"},"value":"50"}},{"userId":"' + lst_acc[0].BI_Id_CSB__c + '","accountId":"' + lst_acc[0].BI_Id_CSB__c + '","subscriptionId":"100045","productId":"CAT3","consumption":{"characteristic":"product 3","validFor":{"startDateTime":"2015-12-10T00:00:00Z","endDateTime":"2016-12-10T00:00:00Z"},"unitOfMeasure":{"units:":"licenses"},"value":"25"}}]}');
	        req.requestBody = body;
	        BI_ParqueComercialRest.insertCommercialAssets();
	        system.assertEquals(200, RestContext.response.statuscode);
	
	        body = Blob.valueof('');
	        req.requestBody = body;
	        BI_ParqueComercialRest.insertCommercialAssets();
	        system.assertEquals(500, RestContext.response.statuscode);
        }
        
        Test.stopTest();
        
    }

}