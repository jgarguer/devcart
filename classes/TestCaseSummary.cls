@isTest
public class TestCaseSummary{
	
    static testMethod void myUnitTest(){
    	User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c = 'TGS';
        insert u;
    	
        System.runAs(u){
			System.debug('Current User: ' + UserInfo.getUserName());
			System.debug('Current Profile: ' + UserInfo.getProfileId()); 
			
            Case testCase=TGS_Dummy_Test_Data.dummyOrderCase();
            NE__Order__c ord = new NE__Order__c();
            ord.Case__c=testCase.id;
            insert ord;
            testCase.Order__c=ord.id;
            update testCase;
            NE__Product__c prod = new NE__Product__c(Name = 'Service Product');
            insert prod;
            NE__OrderItem__c oi = new NE__OrderItem__c(NE__OrderId__c = ord.Id, NE__ProdId__c = prod.Id, NE__Qty__c=1, CurrencyIsoCode='EUR');
            insert oi;
            
            ord.NE__Configuration_Type__c = 'New';
            update ord;
            
            Case caseOrd = [SELECT Id FROM Case WHERE Order__c = :ord.Id LIMIT 1];
            
            system.assertNotEquals(null, caseOrd);
            
            PageReference p = Page.CaseSummary;
            p.getParameters().put('Id', caseOrd.Id);
            Test.setCurrentPage(p);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(caseOrd);
            CaseSummary contr = new CaseSummary(sc);
            
            
            /*
            system.assertEquals(prod.Name, caseOrd.TGS_Service__c);
            caseOrd.Status = 'In Progress';
            update caseOrd;
            caseOrd.Status = 'Resolved';
            caseOrd.TGS_RFS_date__c = Date.today();
            update caseOrd;
            caseOrd.Status = 'Closed';
            update caseOrd;
            */
        }
    }

}