/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Manuel Ochoa
     Company:       New Energy Aborda
     Description:   Test class for BI_O4_CreateOptyOrders
     Test Class:    
     History:
     
     <Date>            <Author>             <Description>
     02-08-2017         Manuel Ochoa        First version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class BI_O4_CreateOptyOrders_TEST{
	
       static Map<String, Account> mapAccountCntry= new Map<String, Account>();	
        static List<SelectOption> allLineasNegocio = new List<SelectOption>();
	  static List<SelectOption> allServiceLines = new List<SelectOption>();
        static Set<String> setServiceLineId = new Set<String>();
    static Set<String> listServiceLine =new Set<String>();
    static{
        BI_TestUtils.throw_exception = false;
    }

    private Decimal decimalCountOrders = 5; // number of orders that must be inserted
    private Decimal decimalCountOrderItems = 12; // number of order items that must be inserted
    private Decimal decimalCountOrderItemAttr = 24; // number of order item attributes that must be inserted
        
    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Data load for tests
    IN:
    OUT:        
    
    History
    <Date>      <Author>     <Description>
    ------------------------------------------------------------*/
    
    @testSetup 
    static void startUp(){
    //static void test_loadCatalogSimpleProducts() {

        BI_TestUtils.throw_exception = false;

        List<Account> listAccount = BI_O4_DisconnectionMethods_TEST.loadAccounts(1, new String[]{'Spain','France','Italy','Ireland','Belgium'});
        List<Opportunity> listOpportunities = new List<Opportunity>();
                
        // Optys test data
        Opportunity opp = new Opportunity(Name = 'Test1',
                                    CloseDate = Date.today(),
                                    StageName = Label.BI_F6Preoportunidad, 
                                    AccountId = listAccount[0].Id,
                                    BI_Opportunity_Type__c = 'Servicios Internacionales',
                                    BI_Ciclo_ventas__c = Label.BI_Completo,
                                    BI_Country__c = 'Spain',
                                    Description='',
                                    BI_O4_Service_Lines__c = 'Test Product 1'
                                    );

        listOpportunities.add(opp);

        opp = new Opportunity(Name = 'Test2',
                                    CloseDate = Date.today(),
                                    StageName = Label.BI_F6Preoportunidad, 
                                    AccountId = listAccount[0].Id,
                                    BI_Opportunity_Type__c = 'Servicios Internacionales',
                                    BI_Ciclo_ventas__c = Label.BI_Completo,
                                    BI_Country__c = 'France',
                                    Description='',
                                    BI_O4_Service_Lines__c = 'Test Product 1;Test Product 2'
                                    );

        listOpportunities.add(opp);

        opp = new Opportunity(Name = 'Test3',
                                    CloseDate = Date.today(),
                                    StageName = Label.BI_F6Preoportunidad, 
                                    AccountId = listAccount[0].Id,
                                    BI_Opportunity_Type__c = 'Servicios Internacionales',
                                    BI_Ciclo_ventas__c = Label.BI_Completo,
                                    BI_Country__c = 'Italy',
                                    Description='',
                                    BI_O4_Service_Lines__c = 'Test Product 1;Test Product 2;Test Product 3'
                                    );

        listOpportunities.add(opp);

        opp = new Opportunity(Name = 'Test4_renewal',
                                    CloseDate = Date.today(),
                                    StageName = Label.BI_F6Preoportunidad, 
                                    AccountId = listAccount[0].Id,
                                    BI_Opportunity_Type__c = 'Servicios Internacionales',
                                    BI_Ciclo_ventas__c = Label.BI_Completo,
                                    BI_Country__c = 'Ireland',
                                    Description='Is Renewal',
                                    BI_O4_Service_Lines__c = 'Test Product 1'
                                    );

        listOpportunities.add(opp);

        opp = new Opportunity(Name = 'Test5_renewal',
                                    CloseDate = Date.today(),
                                    StageName = Label.BI_F6Preoportunidad, 
                                    AccountId = listAccount[0].Id,
                                    BI_Opportunity_Type__c = 'Servicios Internacionales',
                                    BI_Ciclo_ventas__c = Label.BI_Completo,
                                    BI_Country__c = 'Belgium',
                                    Description='Is Renewal',
                                    BI_O4_Service_Lines__c = 'Test Product 1;Test Product 2'
                                    );

        listOpportunities.add(opp); 

        insert listOpportunities;

        // *********************************
        // Catalog test data - START
        // *********************************
        NE__Catalog_Header__c catHead = new NE__Catalog_Header__c(Name='Test Catalog', 
                                                                    NE__Name__c='Test Catalog');
        insert catHead;
       
        NE__Catalog__c  cat = new NE__Catalog__c(Name='Test Cat',
                                                NE__Catalog_Header__c=catHead.Id, 
                                                NE__StartDate__c=Datetime.now(), 
                                                NE__Active__c = true);
        insert cat;
        
        NE__Catalog_Category__c catCat = new NE__Catalog_Category__c(Name='Test Category', 
                                                                    NE__CatalogId__c=cat.Id);
        insert catCat;

        NE__Commercial_Model__c comModel = new NE__Commercial_Model__c(Name='Test Commercial Model',
                                                                        NE__Catalog_Header__c=catHead.Id);
        insert comModel;

        NE__Family__c family    =   new NE__Family__c(name = 'Test Family');
        insert family;
        
        RecordType RTProd = [SELECT Id FROM RecordType WHERE (SobjectType = 'NE__Product__c' OR SobjectType = 'Product__c') AND Name = 'Standard'];
        NE__Product__c prod1 = new NE__Product__c(Name='Test Product 1', RecordTypeId=RTProd.Id);
        insert prod1;
        NE__Product__c prod2 = new NE__Product__c(Name='Test Product 2', RecordTypeId=RTProd.Id);
        insert prod2;
        NE__Product__c prod3 = new NE__Product__c(Name='Test Product 3', RecordTypeId=RTProd.Id);
        insert prod3;       
        
        
        List<NE__ProductFamily__c> listProdFamily = New List<NE__ProductFamily__c>();
        NE__ProductFamily__c prodFamily =   new NE__ProductFamily__c(NE__ProdId__c = prod1.id, NE__FamilyId__c = family.id);
        listProdFamily.add(prodFamily);
        prodFamily  =   new NE__ProductFamily__c(NE__ProdId__c = prod2.id, NE__FamilyId__c = family.id);
        listProdFamily.add(prodFamily);
        prodFamily  =   new NE__ProductFamily__c(NE__ProdId__c = prod3.id, NE__FamilyId__c = family.id);
        listProdFamily.add(prodFamily);
        insert listProdFamily;
        
        NE__DynamicPropertyDefinition__c prop1  =   new NE__DynamicPropertyDefinition__c(name = 'TestProperty1',NE__Type__c ='Enumerated');
        insert prop1;
         NE__DynamicPropertyDefinition__c prop2 =   new NE__DynamicPropertyDefinition__c(name = 'TestProperty1',NE__Type__c ='Number');
        insert prop2;               

        NE__ProductFamilyProperty__c pfp1 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=family.Id, NE__PropId__c=prop1.Id,NE__Required__c='false');
        insert pfp1;
        NE__ProductFamilyProperty__c pfp2 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=family.Id, NE__PropId__c=prop2.Id,NE__Required__c='false');
        insert pfp2;

        List<NE__Catalog_Item__c> listCatItem = New List<NE__Catalog_Item__c>();
        NE__Catalog_Item__c catItem = new NE__Catalog_Item__c(NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, NE__Recurring_Charge_Frequency__c='Monthly');
        listCatItem.add(catItem);
        catItem = new NE__Catalog_Item__c(NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod2.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, NE__Recurring_Charge_Frequency__c='Monthly');
        listCatItem.add(catItem);
        catItem = new NE__Catalog_Item__c(NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod3.Id, NE__Catalog_Category_Name__c=catCat.Id, NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, NE__Recurring_Charge_Frequency__c='Monthly');
        listCatItem.add(catItem);
        insert listCatItem;

        // *********************************
        // Catalog test data - END
        // *********************************

    }
     
    static List<Decimal> countRecords(){
        List<Decimal> decimalRecords=new List<Decimal>();
        decimalRecords.add(database.countQuery('Select count() from NE__Order__c'));
        decimalRecords.add(database.countQuery('Select count() from NE__orderItem__c'));
        decimalRecords.add(database.countQuery('Select count() from NE__Order_Item_Attribute__c'));
        return decimalRecords;
    }

    
    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Test method to launch synchronous execution
    IN:
    OUT:        
    
    History
    <Date>      <Author>     <Description>
    ------------------------------------------------------------*/
    @isTest static void test_GenerateOrderFromOpty() {
        List<Opportunity> listOpportunities = new List<Opportunity>([SELECT Id,Name,Description,CloseDate,StageName,AccountId,BI_Opportunity_Type__c,BI_Ciclo_ventas__c,BI_Country__c,BI_O4_Service_Lines__c FROM Opportunity]);
        List<NE__Product__c> listProducts = New List<NE__Product__c>([SELECT id,name FROM NE__Product__c]);
        Set<String> setStringProducts=new Set<String>();
        
        for(NE__Product__c productItem: listProducts){
            setStringProducts.add(productItem.id);
        }

        test.startTest();
        BI_O4_CreateOptyOrders orderCreator = new BI_O4_CreateOptyOrders(listOpportunities,setStringProducts, null, false);
        orderCreator.listOptysBatch = new List<Opportunity>();
        orderCreator.listOptysBatch=listOpportunities;
        
        orderCreator.generateOrdersFromOpty();
        test.stopTest();
    }


    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Test method to launch queueable execution
    IN:
    OUT:        
    
    History
    <Date>      <Author>     <Description>
    ------------------------------------------------------------*/
    @isTest static void test_GenerateOrderFromOpt_Queueable() {
        List<Opportunity> listOpportunities = new List<Opportunity>([SELECT Id,Name,Description,CloseDate,StageName,AccountId,BI_Opportunity_Type__c,BI_Ciclo_ventas__c,BI_Country__c,BI_O4_Service_Lines__c FROM Opportunity]);
        List<NE__Product__c> listProducts = New List<NE__Product__c>([SELECT id,name FROM NE__Product__c]);
        Set<String> setStringProducts=new Set<String>();

        for(NE__Product__c productItem: listProducts){
            setStringProducts.add(productItem.id);
        }

        test.startTest();
        BI_O4_CreateOptyOrders orderCreator = new BI_O4_CreateOptyOrders(listOpportunities,setStringProducts, null, true);
        orderCreator.listOptysBatch = new List<Opportunity>();
        orderCreator.listOptysBatch=listOpportunities;
        if(!orderCreator.checkNoCatalogData){
            ID jobID = System.enqueueJob(orderCreator);
        }
        test.stopTest();

        List<Decimal> decimalRecords = countRecords();
        system.debug(decimalRecords);
        System.assertEquals(0,decimalRecords[0]);
        System.assertEquals(0,decimalRecords[1]);
        System.assertEquals(0,decimalRecords[2]);
    }

    
    /*------------------------------------------------------------
    Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Test method to trigger "NoCatalogData" error
    IN:
    OUT:        
    
    History
    <Date>      <Author>     <Description>
    ------------------------------------------------------------*/
    @isTest static void test_GenerateOrderFromOpty_NoCatalogData() {
        List<Opportunity> listOpportunities = new List<Opportunity>([SELECT Id,Name,Description,CloseDate,StageName,AccountId,BI_Opportunity_Type__c,BI_Ciclo_ventas__c,BI_Country__c,BI_O4_Service_Lines__c FROM Opportunity]);
        Set<String> setStringProducts=new Set<String>();
        for(Opportunity productItem: listOpportunities){  // Dummy ids from optys
            setStringProducts.add(productItem.id);
        }

        test.startTest();
        BI_O4_CreateOptyOrders orderCreator = new BI_O4_CreateOptyOrders(listOpportunities,setStringProducts, null, false);
        orderCreator.listOptysBatch = new List<Opportunity>();
        orderCreator.listOptysBatch=listOpportunities;
        orderCreator.generateOrdersFromOpty();
        test.stopTest();

        List<Decimal> decimalRecords = countRecords();
        system.debug(decimalRecords);
        System.assertEquals(0,decimalRecords[0]);
        System.assertEquals(0,decimalRecords[1]);
        System.assertEquals(0,decimalRecords[2]);
    }    
   

    @isTest static void test_GenerateOrderFromOpty_NoCatalogDataException() {
        List<Opportunity> listOpportunities = new List<Opportunity>([SELECT Id,Name,Description,CloseDate,StageName,AccountId,BI_Opportunity_Type__c,BI_Ciclo_ventas__c,BI_Country__c,BI_O4_Service_Lines__c FROM Opportunity]);
        Set<String> setStringProducts=new Set<String>();
        for(Opportunity productItem: listOpportunities){  // Dummy ids from optys
            setStringProducts.add(productItem.id);
        }

        test.startTest();
        BI_O4_CreateOptyOrders orderCreator = new BI_O4_CreateOptyOrders(listOpportunities,setStringProducts, null, false);
        orderCreator.listOptysBatch = new List<Opportunity>();
        orderCreator.listOptysBatch=listOpportunities;
        orderCreator.generateOrdersFromOpty();
        test.stopTest();

        List<Decimal> decimalRecords = countRecords();
        system.debug(decimalRecords);
        System.assertEquals(0,decimalRecords[0]);
        System.assertEquals(0,decimalRecords[1]);
        System.assertEquals(0,decimalRecords[2]);
    }
   
	@isTest static void test_GenerateOrderFromOptyWithItems() {
        
        //////ITEM GENERATION

        NE__Family__c family    =   new NE__Family__c(name = 'Test Family Complex');
        insert family;

        RecordType RTProd = [SELECT Id FROM RecordType WHERE (SobjectType = 'NE__Product__c' OR SobjectType = 'Product__c') AND Name = 'Standard'];
        NE__Product__c prod1 = new NE__Product__c(Name='Test Complex Product 1', RecordTypeId=RTProd.Id);
        insert prod1;
        
        NE__Product__c prod2 = new NE__Product__c(Name='Test Child Product 2', RecordTypeId=RTProd.Id);
        insert prod2;

        List<NE__ProductFamily__c> listProdFamily = New List<NE__ProductFamily__c>();
        NE__ProductFamily__c prodFamily =   new NE__ProductFamily__c(NE__ProdId__c = prod1.id, NE__FamilyId__c = family.id);
        listProdFamily.add(prodFamily);
        prodFamily  =   new NE__ProductFamily__c(NE__ProdId__c = prod2.id, NE__FamilyId__c = family.id);
        listProdFamily.add(prodFamily);
        insert listProdFamily;
        
        NE__DynamicPropertyDefinition__c prop1  =   new NE__DynamicPropertyDefinition__c(name = 'TestProperty1',NE__Type__c ='Enumerated');
        insert prop1;
         NE__DynamicPropertyDefinition__c prop2 =   new NE__DynamicPropertyDefinition__c(name = 'TestProperty1',NE__Type__c ='Number');
        insert prop2;               

        NE__ProductFamilyProperty__c pfp1 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=family.Id, NE__PropId__c=prop1.Id,NE__Required__c='false');
        insert pfp1;
        NE__ProductFamilyProperty__c pfp2 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=family.Id, NE__PropId__c=prop2.Id,NE__Required__c='false');
        insert pfp2;
        
        NE__Catalog_Header__c catHead = new NE__Catalog_Header__c(Name='Catalog Header',
                                                                 NE__Name__C='Catalog Header');
        insert catHead;
        
        NE__Catalog__c  cat = new NE__Catalog__c(Name='TGS Catalog',
                                                NE__Catalog_Header__c=catHead.Id, 
                                                NE__StartDate__c=Datetime.now(), 
                                                NE__Active__c = true);
        insert cat;
        
         NE__Catalog_Category__c catCat = new NE__Catalog_Category__c(Name='Service Lines', 
                                                           NE__CatalogId__c=cat.Id);
        insert catCat;
       
        NE__Catalog_Item__c catItem = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Enumerated',
															NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
															 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
															NE__Recurring_Charge_Frequency__c='Monthly');
        insert catItem;
        
        
        
        NE__Catalog_Item__c catItemC1 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Product',
															NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
															 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
															NE__Recurring_Charge_Frequency__c='Monthly');
        insert catItemC1;
        
        NE__Item_Header__c itemHead = new  NE__Item_Header__c(Name='Item Header',
                                                              NE__Catalog__c=cat.id,
                                                              NE__Catalog_Item__c=catItemC1.id
        													);
        insert itemHead;       
        
        catItemC1.NE__Item_Header__c=itemHead.id;
        update catItemC1;
        
        NE__Catalog_Item__c catItemC2 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Root',
															NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
															 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
															NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                         					NE__Parent_Catalog_Item__c=catItemC1.id);
        insert catItemC2;
        
        NE__Catalog_Item__c catItemC3 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Category',
															NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod1.Id, NE__Catalog_Category_Name__c=catCat.Id,
															 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
															NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                            NE__Parent_Catalog_Item__c=catItemC2.id,
                                                            NE__Root_Catalog_Item__c=catItemC2.id);
        insert catItemC3;
        
        NE__Catalog_Item__c catItemC4 = new NE__Catalog_Item__c(NE__Active__c=true,NE__Type__c='Child-Product',
															NE__Catalog_Id__c=cat.Id, NE__ProductId__c=prod2.Id, NE__Catalog_Category_Name__c=catCat.Id,
															 NE__Start_Date__c=Datetime.now(), NE__Base_OneTime_Fee__c=200.00, NE__BaseRecurringCharge__c=20.00, 
															NE__Recurring_Charge_Frequency__c='Monthly',NE__Item_Header__c=itemHead.id,
                                                            NE__Parent_Catalog_Item__c=catItemC3.id,
                                                            NE__Root_Catalog_Item__c=catItemC2.id);
        insert catItemC4;
        
      
        
        BI_TestUtils.throw_exception = false;

		//List<Account> lstAccount = BI_O4_DisconnectionMethods_TEST.loadAccountsWithOtherRT(1, new String[]{'Spain'});
        List<Account> lstAccount = new List<Account>([SELECT id, name FROM account LIMIT 1]);

		for(Account accItem : lstAccount) {
				mapAccountCntry.put(accItem.Name, accItem);
		}
		
		Opportunity opp = new Opportunity(Name = 'Test',
									CloseDate = Date.today(),
									StageName = Label.BI_F5DefSolucion, //Manuel Medina
									AccountId = lstAccount[0].Id,
									BI_Opportunity_Type__c = 'Global Distributed',
									BI_Ciclo_ventas__c = Label.BI_Completo,
									BI_Country__c = 'Spain',
									BI_O4_Countries_EMEA__c = 'Spain',
                                         BI_O4_Service_Lines__c= string.valueOf(1),
                                         Description='Multiple multi-site'
                                         );
		insert opp;

        List<Opportunity> listOpportunities = new List<Opportunity>();
        listOpportunities.add(opp);
       
        Set<String> setStringProducts=new Set<String>();
        setStringProducts.add(itemHead.id);   

    
        
        Set<String> stringitemID=new Set<String>();
        stringitemID.add(catItem.id);
        stringitemID.add(catItemC1.id);
        stringitemID.add(catItemC2.id);
        stringitemID.add(catItemC3.id);
         stringitemID.add(catItemC4.id);
        
        List<NE__Catalog_Item__c> listCatItem = New List<NE__Catalog_Item__c>();
        listCatItem.add(catItem);
        listCatItem.add(catItemC1);
        listCatItem.add(catItemC2);
        listCatItem.add(catItemC3);
        listCatItem.add(catItemC4);
        
       test.startTest();
        BI_O4_CreateOptyOrders orderCreator = new BI_O4_CreateOptyOrders(listOpportunities,setStringProducts, stringitemID, true);

        orderCreator.listOptysBatch = new List<Opportunity>();
        orderCreator.listOptysBatch=listOpportunities;
        if(!orderCreator.checkNoCatalogData){
            ID jobID = System.enqueueJob(orderCreator);
        }
        // seguir desde aqui
        NE__Contract_Header__c contractHead = new NE__Contract_Header__c(NE__Name__c='Contract Header');
         insert contractHead;
 		NE__Order__c ord = new NE__Order__c(NE__AccountId__c=lstAccount[0].Id, NE__BillAccId__c=lstAccount[0].Id, NE__ServAccId__c=lstAccount[0].Id, NE__CatalogId__c=cat.Id, NE__OrderStatus__c='Pending', NE__ConfigurationStatus__c='Valid',NE__Contract_Header__c=contractHead.Id, NE__Order_date__c=Datetime.now().AddDays(1),NE__Type__c='New');
        insert ord;
        NE__OrderItem__c ordit1 = new NE__OrderItem__c(NE__OrderId__c=ord.Id, NE__ProdId__c=prod1.Id,NE__CatalogItem__c=catItemC2.Id,NE__RecurringChargeFrequency__c =null, NE__Qty__c=1,NE__Action__c='Add',NE__Root_Order_Item__c=null, NE__Status__c ='In progress');
    	insert ordit1;
        
        
		//List<NE__orderItem__c> oiParents=[select id,NE__Parent_Order_Item__c,NE__CatalogItem__c,NE__ProdId__c from NE__orderItem__c WHERE NE__CatalogItem__c=:catItemC2.id];  
		List<NE__orderItem__c> oiParents=new List<NE__orderItem__c>();
        oiParents.add(ordit1);
       	System.debug('TEST OIPARENTS WHERE..NE__CatalogItem__c'+oiParents);
        
        orderCreator.insertChildOrderItems(oiParents);
        test.stopTest();
    }

}