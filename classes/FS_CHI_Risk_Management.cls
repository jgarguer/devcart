/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:			Alejandro Labrin
Company:		Everis Centers Temuco
Description:	Clase para hacer la llamada al webservice 

History:      Creación de operación

<Date>                      <Author>                        <Change Description>
05/11/2016                  Alejandro Labrin				Initial Version
24/10/2017					Daniel Leal				   		Ever01:Inclusion de logs
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

public class FS_CHI_Risk_Management {
    /*SALESFORCE VARIABLE DECLARATION*/
    /*No Entries*/
    
    public static  BI_RestWrapper.CreditInfoType Sync(String tipoCredito, String subSegmento, String numeroIdentificador){
        System.debug('Nombre de clase: FS_CHI_Risk_Management   Nombre de método: Sync   Comienzo del método');
        /*Valiable definition*/
        BI_RestWrapper.CreditInfoType result;        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        BI_RestWrapper.CreditInfoWrapper CreditInfoAux;    
        FS_CHI_Integraciones__c config_integracion = FS_CHI_Integraciones__c.getInstance();
        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(config_integracion.FS_CHI_creditTypeMap__c);
        
        /*REQUEST CONFIGURATION*/
        //Ever01-INI
        String numId = EncodingUtil.urlEncode(numeroIdentificador,'UTF-8');
        String tipoCred = EncodingUtil.urlEncode(String.valueOf(m.get(tipoCredito)),'UTF-8');
        String subSeg ='&ad_companySize=';
        if(String.isEmpty(subSeg)){
            subSeg = subseg + EncodingUtil.urlEncode(subSegmento,'UTF-8');
        }else{
            subSeg = '';
        }
        System.debug('@@@@@@ FS_CHI_Risk_Management   numId: ' + numId+ 'tipoCred: '+tipoCred+'subSeg: '+subSeg);
        request.setEndpoint('callout:FS_CHI_DataPower' + '/customerinfo/v1/customers/'+numId+'/creditInfo?&nationalID='+numId+'&ad_AccreditationType='+tipoCred+subSeg+'&apikey='+config_integracion.FS_CHI_ApiKey__c);
        //Ever01-FIN
        request.setMethod('GET');        
        request.setHeader('Authorization', config_integracion.FS_CHI_AuthorizationID__c);
        request.setTimeout(20000);
        try{
            /*Http Invocation Process*/
            if(Test.isRunningTest()) response = FS_CHI_Risk_Management_Mock.respond(request);
            else response = http.send(request);
            
            /*Response Management*/
            if(response.getStatusCode() == 200){
                /*Successfully Recived*/
                CreditInfoAux = (BI_RestWrapper.CreditInfoWrapper) JSON.deserialize(response.getBody(),  BI_RestWrapper.CreditInfoWrapper.class);
                result =  CreditInfoAux.creditInfo;             
            }
        } catch(Exception except){
            //Ever01-INI
            BI_LogHelper.generate_BILog('FS_CHI_Risk_Management.Sync', 'BI_EN', except, 'Web Service');
            //Ever01-FIN
        }
        System.debug('Nombre de clase: FS_CHI_Risk_Management   Nombre de método: Sync   Finalización del método    Resultado: ' + result + ' '+response.getBody());
        return result;
    }
}