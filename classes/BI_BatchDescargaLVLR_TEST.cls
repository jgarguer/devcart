@isTest
private class BI_BatchDescargaLVLR_TEST {

	final static Integer BI_MAXLIMIT_AGENDAMIENTOS_BUTTONS;
    final static Integer BI_MAXLIMIT_BATCHDESCARGALVLR;
    final static Map<String,Id> MAP_RT_NAME = new Map<String,Id>();

    static{
       BI_MAXLIMIT_AGENDAMIENTOS_BUTTONS = Integer.valueOf(Label.BI_MaxLimit_Agendamientos_Buttons);
       BI_MAXLIMIT_BATCHDESCARGALVLR     = Integer.valueOf(Label.BI_MaxLimit_BatchDescargaLVLR);

       for(Recordtype rt :[SELECT Id, Developername FROM Recordtype where (DeveloperName = 'BI_Modelo' AND sObjectType = 'BI_Modelo__c') OR (DeveloperName = 'BI_Servicio' AND sObjectType = 'BI_Modelo__c' )  ]){
       		MAP_RT_NAME.put(rt.DeveloperName,rt.Id);
       }

       BI_TestUtils.throw_exception = false;
    
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        -------
	    Company:       -------
	    Description:   -------
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    03/02/2016                      Guillermo Muñoz             Fix bugs caused by news validations rules
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void solicitudBOLineaVentaCHI_option_1() {

		List<Account> lst_acc = BI_DataLoad.loadAccounts(1,new List<String>{'Chile'});
		system.assert(!lst_acc.isEmpty());
		User usu = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema AND Id !=: UserInfo.getUserId() LIMIT 1];

		BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(BI_Tipo_solicitud__c = 'Normal',
 		    															BI_Cliente__c = lst_acc[0].Id);
		System.runAs(usu){
			insert sol;
		}
		sol.BI_Lineas_de_venta__c = true;
		sol.BI_Estado_de_ventas__c = 'Aprobado';
		update sol;

        list<BI_Modelo__c> lstModelo = new list<BI_Modelo__c>();	
		
		BI_Modelo__c equipo = new BI_Modelo__c(Name = 'modelo' , RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'Smartphone' );
		BI_Modelo__c servicio = new BI_Modelo__c(Name = 'servicio' , RecordTypeId = MAP_RT_NAME.get('BI_Servicio') );
		BI_Modelo__c sim = new BI_Modelo__c(Name = 'sim', RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'SIMCARD');

		List<BI_Modelo__c> lst_eq_serv = new List<BI_Modelo__c>();
			lst_eq_serv.add(equipo);
			lst_eq_serv.add(servicio);
			lst_eq_serv.add(sim);

		insert lst_eq_serv;

		Map<String,BI_Modelo__c> MAP_MODELO_NAME = new Map<String,BI_Modelo__c>();
		for(BI_Modelo__c mod :lst_eq_serv){
			MAP_MODELO_NAME.put(mod.Name,mod);
		}

		List<BI_Linea_de_Venta__c> lst_lv = new List<BI_Linea_de_Venta__c>();
		
		for(integer i=0; i<50;i++){
	        
	        BI_Linea_de_Venta__c lv = new BI_Linea_de_Venta__c();


	         //FROM BI_Linea_de_Venta__c WHERE BI_Solicitud_descargada_BackOffice__c = false AND BI_Validada__c = true 
	         //AND BI_Estado_de_linea_de_venta__c = \'Entregado\'AND ((BI_Estado_de_courier__c IN (\'Entregado\',\'En Courier\') AND BI_Solicitud__r.BI_Tipo_solicitud__c IN
	        // (\'Normal\',\'Split billing\',\'Contingencia\')) OR (BI_Estado_de_courier__c = \'Entregado\' AND BI_Solicitud__r.BI_Tipo_solicitud__c = \'Portabilidad\'))

			lv.BI_Solicitud_descargada_BackOffice__c = false;
			lv.BI_Estado_de_courier__c = 'Entregado';

			//lv.BI_Solicitud_descargada_Logistica__c = true;
			lv.BI_Validada__c = true;
			lv.BI_Estado_de_linea_de_venta__c = 'Entregado';

			lv.BI_Solicitud__c = sol.Id ;
			lv.BI_Modelo__c = MAP_MODELO_NAME.get('modelo').Id;
			lv.BI_Modelo_TEM__c = String.valueOf(MAP_MODELO_NAME.get('equipo'));
			lv.BI_Codigo_de_cliente_final__c = 'nuevo';
			lv.BI_Modalidad_de_venta__c = 'test modalidad';
			lv.BI_Tipo_de_Contrato__c = 'test contrato';
			lv.BI_Cantidad_de_cuotas__c = '12';
			lv.BI_Valor_de_cuota__c = 50;
			lv.BI_Plan__c = 'test plan';
			lv.BI_Otro_plan__c = 'test otro plan';
			lv.BI_Q_activado__c = 15;
			lv.BI_Cargo_basico__c = 100;
			lv.BI_Inlcuir_tarjeta_SIM__c = 'Si';
			lv.BI_Cantidad_de_equipos__c = 15;
			lv.BI_Precio_de_pie__c = 10;
			lv.BI_Precio_base_de_modelo__c = 10;
			lv.BI_Precio_base_de_SIM__c = 10;
			lv.BI_Precio_del_modelo_definido_manualmen__c = 'Si';
			lv.BI_Precio_de_SIM_definido_manualmente__c = 'Si';
			lv.BI_Ciclo_de_facturacion__c = 'test ciclo';
			lv.BI_Marca_de_cambio__c = false;
			//lv.BI_Beneficio_de_bolsa_de_dinero_de_model__c = 3000000;
			//lv.BI_Beneficio_de_bolsa_de_dinero_de_SIM__c = 1500000;
			lv.BI_Catalogo_de_Servicio_1_TEM__c = 'serv1-serv2';
			lv.BI_Catalogo_de_Servicio_2_TEM__c = 'serv1-serv2';
			lv.BI_Catalogo_de_Servicio_3_TEM__c = 'serv1-serv2';
			lv.BI_Catalogo_de_Servicio_4_TEM__c = 'serv1-serv2';
			lv.BI_Catalogo_de_Servicio_5_TEM__c = 'serv1-serv2';
			//lv.BI_Codigo_de_descuento_de_SIM__c = descSIM.Id;
			lv.BI_ID_de_simcard_TEM__c = String.valueOf(MAP_MODELO_NAME.get('sim').Id);
			//lv.BI_Codigo_de_Descuento_TEM__c = String.valueOf(descModelo.Id);
			//lv.BI_Descuento_SIM_TEM__c = String.valueOf(descSIM.Id);
			lv.BI_ID_de_simcard__c = MAP_MODELO_NAME.get('sim').Id;
				
			lst_lv.add(lv);
	    }
	    
	    BI_GlobalVariables.stopTriggerIMPExcel = true;

		insert lst_lv;
		system.assert(!lst_lv.isEmpty());
		sol.BI_Estado_de_ventas__c = 'Enviando';
		sol.BI_Estado_de_recambios__c = 'Enviando';
		update sol;

		List<User> lst_usr = BI_Dataload.loadUsers(1, BI_Dataload.searchAdminProfile(), Label.BI_Administrador);
		system.assert(!lst_usr.isEmpty());

		Test.startTest();
			Database.executeBatch(new BI_BatchDescargaLVLR(1, lst_usr[0].Id), BI_MAXLIMIT_BATCHDESCARGALVLR);
		Test.stopTest();


		List<BI_Linea_de_Venta__c> lst_lv_assert = [SELECT ID, BI_Usuario_BackOffice__c, BI_Solicitud_descargada_BackOffice__c FROM BI_Linea_de_Venta__c WHERE ID IN :lst_lv ]; 
		system.debug('lst_lv_assert: ' + lst_lv_assert);
		system.assert(!lst_lv_assert.isempty());
		for(BI_Linea_de_Venta__c lv :lst_lv_assert){
			system.assertEquals(lst_usr[0].Id,lv.BI_Usuario_BackOffice__c);
			system.assertEquals(true,lv.BI_Solicitud_descargada_BackOffice__c);
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        -------
	    Company:       -------
	    Description:   -------
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    03/02/2016                      Guillermo Muñoz             Fix bugs caused by news validations rules
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void solicitudlogisticaLineaVentaCHI_option_2() {

		List<Account> lst_acc = BI_DataLoad.loadAccounts(1,new List<String>{'Chile'});
		system.assert(!lst_acc.isEmpty());
		User usu = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema AND Id !=: UserInfo.getUserId() LIMIT 1];

		BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(BI_Tipo_solicitud__c = 'Normal',
 		    															BI_Cliente__c = lst_acc[0].Id);
		System.runAs(usu){
			insert sol;
		}
		sol.BI_Estado_de_ventas__c = 'Aprobado';
		sol.BI_Lineas_de_venta__c = true;
		update sol;

        list<BI_Modelo__c> lstModelo = new list<BI_Modelo__c>();	
		
		BI_Modelo__c equipo = new BI_Modelo__c(Name = 'modelo' , RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'Smartphone' );
		BI_Modelo__c servicio = new BI_Modelo__c(Name = 'servicio' , RecordTypeId = MAP_RT_NAME.get('BI_Servicio') );
		BI_Modelo__c sim = new BI_Modelo__c(Name = 'sim', RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'SIMCARD');

		List<BI_Modelo__c> lst_eq_serv = new List<BI_Modelo__c>();
			lst_eq_serv.add(equipo);
			lst_eq_serv.add(servicio);
			lst_eq_serv.add(sim);

		insert lst_eq_serv;

		Map<String,BI_Modelo__c> MAP_MODELO_NAME = new Map<String,BI_Modelo__c>();
		for(BI_Modelo__c mod :lst_eq_serv){
			MAP_MODELO_NAME.put(mod.Name,mod);
		}

		List<BI_Linea_de_Venta__c> lst_lv = new List<BI_Linea_de_Venta__c>();
		
		for(integer i=0; i<50;i++){
	        
	        BI_Linea_de_Venta__c lv = new BI_Linea_de_Venta__c();


			//'SELECT Id, BI_Usuario_Logistica__c, BI_Solicitud_descargada_Logistica__c FROM BI_Linea_de_Venta__c WHERE BI_Estado_de_linea_de_venta__c 
			//= \'Pendiente\' AND BI_Solicitud_descargada_Logistica__c = false AND BI_Validada__c = true AND BI_Contingencia__c = false';

			lv.BI_Solicitud_descargada_Logistica__c = false;
			lv.BI_Estado_de_courier__c = 'Entregado';

			lv.BI_Validada__c = true;
			lv.BI_Estado_de_linea_de_venta__c = 'Pendiente';

			lv.BI_Solicitud__c = sol.Id ;
			lv.BI_Modelo__c = MAP_MODELO_NAME.get('modelo').Id;
			lv.BI_Modelo_TEM__c = String.valueOf(MAP_MODELO_NAME.get('equipo'));
			lv.BI_Codigo_de_cliente_final__c = 'nuevo';
			lv.BI_Modalidad_de_venta__c = 'test modalidad';
			lv.BI_Tipo_de_Contrato__c = 'test contrato';
			lv.BI_Cantidad_de_cuotas__c = '12';
			lv.BI_Valor_de_cuota__c = 50;
			lv.BI_Plan__c = 'test plan';
			lv.BI_Otro_plan__c = 'test otro plan';
			lv.BI_Q_activado__c = 15;
			lv.BI_Cargo_basico__c = 100;
			lv.BI_Inlcuir_tarjeta_SIM__c = 'Si';
			lv.BI_Cantidad_de_equipos__c = 15;
			lv.BI_Precio_de_pie__c = 10;
			lv.BI_Precio_base_de_modelo__c = 10;
			lv.BI_Precio_base_de_SIM__c = 10;
			lv.BI_Precio_del_modelo_definido_manualmen__c = 'Si';
			lv.BI_Precio_de_SIM_definido_manualmente__c = 'Si';
			lv.BI_Ciclo_de_facturacion__c = 'test ciclo';
			lv.BI_Marca_de_cambio__c = false;
			//lv.BI_Beneficio_de_bolsa_de_dinero_de_model__c = 3000000;
			//lv.BI_Beneficio_de_bolsa_de_dinero_de_SIM__c = 1500000;
			lv.BI_Catalogo_de_Servicio_1_TEM__c = 'serv1-serv2';
			lv.BI_Catalogo_de_Servicio_2_TEM__c = 'serv1-serv2';
			lv.BI_Catalogo_de_Servicio_3_TEM__c = 'serv1-serv2';
			lv.BI_Catalogo_de_Servicio_4_TEM__c = 'serv1-serv2';
			lv.BI_Catalogo_de_Servicio_5_TEM__c = 'serv1-serv2';
			//lv.BI_Codigo_de_descuento_de_SIM__c = descSIM.Id;
			lv.BI_ID_de_simcard_TEM__c = String.valueOf(MAP_MODELO_NAME.get('sim').Id);
			//lv.BI_Codigo_de_Descuento_TEM__c = String.valueOf(descModelo.Id);
			//lv.BI_Descuento_SIM_TEM__c = String.valueOf(descSIM.Id);
			lv.BI_ID_de_simcard__c = MAP_MODELO_NAME.get('sim').Id;
				
			lst_lv.add(lv);
	    }
	    
	    BI_GlobalVariables.stopTriggerIMPExcel = true;

		insert lst_lv;
		system.assert(!lst_lv.isEmpty());

		sol.BI_Estado_de_ventas__c = 'Enviando';				
		sol.BI_Estado_de_recambios__c = 'Enviando';
		update sol;

		List<User> lst_usr = BI_Dataload.loadUsers(1, BI_Dataload.searchAdminProfile(), Label.BI_Administrador);
		system.assert(!lst_usr.isEmpty());

		Test.startTest();
			Database.executeBatch(new BI_BatchDescargaLVLR(2, lst_usr[0].Id), BI_MAXLIMIT_BATCHDESCARGALVLR);
		Test.stopTest();


		List<BI_Linea_de_Venta__c> lst_lv_assert = [SELECT ID, BI_Usuario_Logistica__c, BI_Solicitud_descargada_Logistica__c FROM BI_Linea_de_Venta__c WHERE ID IN :lst_lv ]; 
		system.debug('lst_lv_assert: ' + lst_lv_assert);
		system.assert(!lst_lv_assert.isempty());
		for(BI_Linea_de_Venta__c lv :lst_lv_assert){
			system.assertEquals(lst_usr[0].Id,lv.BI_Usuario_Logistica__c);
			system.assertEquals(true,lv.BI_Solicitud_descargada_Logistica__c);
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        -------
	    Company:       -------
	    Description:   -------
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    17/02/2016                      Guillermo Muñoz             Fix bugs caused by news validations rules
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void solicitudBOLineaRecambioCHI_option_3() {

		User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();
		List<Account> lst_acc = BI_DataLoad.loadAccounts(1,new List<String>{'Chile'});
		system.assert(!lst_acc.isEmpty());

		BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(BI_Tipo_solicitud__c = 'Normal',
																		BI_Estado_de_ventas__c = 'Enviando',
																		BI_Estado_de_recambios__c = 'Vacío',
 		    															BI_Cliente__c = lst_acc[0].Id);
		System.runAs(usu){
			insert sol;
			//List<BI_linea_de_recambio__c> lstRecambio = BI_DataLoadAgendamientos.loadLineaRecambio(1 ,sol.Id ,lst_acc[0]);
		}

        list<BI_Modelo__c> lstModelo = new list<BI_Modelo__c>();	
		
		BI_Modelo__c equipo = new BI_Modelo__c(Name = 'modelo' , RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'Smartphone' );
		BI_Modelo__c servicio = new BI_Modelo__c(Name = 'servicio' , RecordTypeId = MAP_RT_NAME.get('BI_Servicio') );
		BI_Modelo__c sim = new BI_Modelo__c(Name = 'sim', RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'SIMCARD');

		List<BI_Modelo__c> lst_eq_serv = new List<BI_Modelo__c>();
			lst_eq_serv.add(equipo);
			lst_eq_serv.add(servicio);
			lst_eq_serv.add(sim);

		insert lst_eq_serv;

		Map<String,BI_Modelo__c> MAP_MODELO_NAME = new Map<String,BI_Modelo__c>();
		for(BI_Modelo__c mod :lst_eq_serv){
			MAP_MODELO_NAME.put(mod.Name,mod);
		}

		List<BI_Linea_de_Recambio__c> lst_lr = new List<BI_Linea_de_Recambio__c>();
		
		for(integer i=0; i<50;i++){
	        
	        BI_Linea_de_Recambio__c lr = new BI_Linea_de_Recambio__c();


			//query = 'SELECT Id, BI_Usuario_BackOffice__c, BI_Solicitud_descargada_BackOffice__c FROM BI_Linea_de_Recambio__c WHERE BI_Estado_de_linea_de_recambio__c = \'Entregado\' AND BI_Estado_de_courier__c = \'Entregado\'AND BI_Solicitud_descargada_BackOffice__c = false 
			//AND BI_Validada__c = true AND BI_Solicitud__r.BI_Tipo_solicitud__c IN (\'Normal\',\'Split billing\',\'Contingencia\')';

			lr.BI_Solicitud__c = sol.Id ;
			lr.BI_Estado_de_linea_de_recambio__c = 'Entregado';
			lr.BI_Estado_de_courier__c  = 'Entregado';
			lr.BI_Solicitud_descargada_BackOffice__c = false;
			lr.BI_Modelo__c = MAP_MODELO_NAME.get('modelo').Id;
			lr.BI_Precio_final_de_modelo__c = 210;
			lr.BI_Precio_final_de_SIM__c = 100 ;
			lr.BI_Estado__c = 'test estado';
			lr.BI_Razon_para_recambio__c = 'test razon';
			lr.BI_Telefono_de_contacto__c = '666563221';
			lr.BI_Modalidad_de_venta__c = 'test modalidad';
			lr.BI_Tipo_de_contrato__c = 'test tipo contrato';
			lr.BI_Plan__c = 'test plan';
			lr.BI_Validada__c = true;
			lr.BI_Marca_de_cambio__c = true;
			lr.BI_Descuenta_puntos_Movistar__c = 'Sí';
			lr.BI_Descuenta_puntos_Movistar_SIM__c = 'Sí';
			lr.BI_Numero_de_telefono__c = '916102144';
				
			lst_lr.add(lr);
	    }
	    
	    BI_GlobalVariables.stopTriggerIMPExcel = true;

		insert lst_lr;
		system.assert(!lst_lr.isEmpty());

		List<User> lst_usr = BI_Dataload.loadUsers(1, BI_Dataload.searchAdminProfile(), Label.BI_Administrador);
		system.assert(!lst_usr.isEmpty());

		Test.startTest();
			Database.executeBatch(new BI_BatchDescargaLVLR(3, lst_usr[0].Id), BI_MAXLIMIT_BATCHDESCARGALVLR);
		Test.stopTest();


		List<BI_Linea_de_Recambio__c> lst_lr_assert = [SELECT ID, BI_Usuario_BackOffice__c, BI_Solicitud_descargada_BackOffice__c FROM BI_Linea_de_Recambio__c WHERE ID IN :lst_lr ]; 
		system.debug('lst_lr_assert: ' + lst_lr_assert);
		system.assert(!lst_lr_assert.isempty());
		for(BI_Linea_de_Recambio__c lr :lst_lr_assert){
			system.assertEquals(lst_usr[0].Id,lr.BI_Usuario_BackOffice__c);
			system.assertEquals(true,lr.BI_Solicitud_descargada_BackOffice__c);
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        -------
	    Company:       -------
	    Description:   -------
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    17/02/2016                      Guillermo Muñoz             Fix bugs caused by news validations rules
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void solicitudlogisticaLineaRecambioCHI_option_4() {

		User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();
		List<Account> lst_acc = BI_DataLoad.loadAccounts(1,new List<String>{'Chile'});
		system.assert(!lst_acc.isEmpty());

		BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(BI_Tipo_solicitud__c = 'Normal',
																		BI_Estado_de_ventas__c = 'Enviando',
																		BI_Estado_de_recambios__c = 'Vacío',
 		    															BI_Cliente__c = lst_acc[0].Id);
		System.runAs(usu){
			insert sol;
			//List<BI_linea_de_recambio__c> lstRecambio = BI_DataLoadAgendamientos.loadLineaRecambio(1 ,sol.Id ,lst_acc[0]);
		}

        list<BI_Modelo__c> lstModelo = new list<BI_Modelo__c>();	
		
		BI_Modelo__c equipo = new BI_Modelo__c(Name = 'modelo' , RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'Smartphone' );
		BI_Modelo__c servicio = new BI_Modelo__c(Name = 'servicio' , RecordTypeId = MAP_RT_NAME.get('BI_Servicio') );
		BI_Modelo__c sim = new BI_Modelo__c(Name = 'sim', RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'SIMCARD');

		List<BI_Modelo__c> lst_eq_serv = new List<BI_Modelo__c>();
			lst_eq_serv.add(equipo);
			lst_eq_serv.add(servicio);
			lst_eq_serv.add(sim);

		insert lst_eq_serv;

		Map<String,BI_Modelo__c> MAP_MODELO_NAME = new Map<String,BI_Modelo__c>();
		for(BI_Modelo__c mod :lst_eq_serv){
			MAP_MODELO_NAME.put(mod.Name,mod);
		}

		List<BI_Linea_de_Recambio__c> lst_lr = new List<BI_Linea_de_Recambio__c>();
		
		for(integer i=0; i<50;i++){
	        
	        BI_Linea_de_Recambio__c lr = new BI_Linea_de_Recambio__c();


			//'SELECT Id, BI_Usuario_Logistica__c, BI_Solicitud_descargada_Logistica__c FROM BI_Linea_de_Recambio__c 
			//WHERE BI_Estado_de_linea_de_recambio__c = \'Pendiente\' AND BI_Solicitud_descargada_Logistica__c = false AND BI_Validada__c = true  
			//AND BI_Contingencia__c = false ';

			lr.BI_Solicitud__c = sol.Id ;
			lr.BI_Solicitud_descargada_Logistica__c = false;
			lr.BI_Estado_de_linea_de_recambio__c = 'Pendiente';
			//lr.BI_Estado_de_courier__c  = 'Entregado';
			//lr.BI_Solicitud_descargada_BackOffice__c = false;
			lr.BI_Modelo__c = MAP_MODELO_NAME.get('modelo').Id;
			lr.BI_Precio_final_de_modelo__c = 210;
			lr.BI_Precio_final_de_SIM__c = 100 ;
			lr.BI_Estado__c = 'test estado';
			lr.BI_Razon_para_recambio__c = 'test razon';
			lr.BI_Telefono_de_contacto__c = '666563221';
			lr.BI_Modalidad_de_venta__c = 'test modalidad';
			lr.BI_Tipo_de_contrato__c = 'test tipo contrato';
			lr.BI_Plan__c = 'test plan';
			lr.BI_Validada__c = true;
			lr.BI_Marca_de_cambio__c = true;
			lr.BI_Descuenta_puntos_Movistar__c = 'Sí';
			lr.BI_Descuenta_puntos_Movistar_SIM__c = 'Sí';
			lr.BI_Numero_de_telefono__c = '916102144';
				
			lst_lr.add(lr);
	    }
	    
	    BI_GlobalVariables.stopTriggerIMPExcel = true;

		insert lst_lr;
		system.assert(!lst_lr.isEmpty());

		sol.BI_Estado_de_ventas__c = 'Enviando';				
		sol.BI_Estado_de_recambios__c = 'Enviando';
		update sol;

		List<User> lst_usr = BI_Dataload.loadUsers(1, BI_Dataload.searchAdminProfile(), Label.BI_Administrador);
		system.assert(!lst_usr.isEmpty());

		Test.startTest();
			Database.executeBatch(new BI_BatchDescargaLVLR(4, lst_usr[0].Id), BI_MAXLIMIT_BATCHDESCARGALVLR);
		Test.stopTest();


		List<BI_Linea_de_Recambio__c> lst_lr_assert = [SELECT ID, BI_Usuario_Logistica__c, BI_Solicitud_descargada_Logistica__c FROM BI_Linea_de_Recambio__c WHERE ID IN :lst_lr ]; 
		system.debug('lst_lr_assert: ' + lst_lr_assert);
		system.assert(!lst_lr_assert.isempty());
		for(BI_Linea_de_Recambio__c lr :lst_lr_assert){
			system.assertEquals(lst_usr[0].Id,lr.BI_Usuario_Logistica__c);
			system.assertEquals(true,lr.BI_Solicitud_descargada_Logistica__c);
		}

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        -------
	    Company:       -------
	    Description:   -------
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    03/02/2016                      Guillermo Muñoz             Fix bugs caused by news validations rules
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void solicitudBOLineaServicioCHI_option_5() {

		List<Account> lst_acc = BI_DataLoad.loadAccounts(1,new List<String>{'Chile'});
		system.assert(!lst_acc.isEmpty());
		User usu = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema AND Id !=: UserInfo.getUserId() LIMIT 1];

		BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(BI_Tipo_solicitud__c = 'Contingencia',
																		BI_Cliente__c = lst_acc[0].Id);
		System.runAs(Usu){
			insert sol;
		}
		sol.BI_Estado_de_servicios__c = 'Aprobado';
		sol.BI_Lineas_de_servicio__c = true;
		update sol;


        list<BI_Modelo__c> lstModelo = new list<BI_Modelo__c>();	
		
		BI_Modelo__c equipo = new BI_Modelo__c(Name = 'modelo' , RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'Smartphone' );
		BI_Modelo__c servicio = new BI_Modelo__c(Name = 'servicio' , RecordTypeId = MAP_RT_NAME.get('BI_Servicio') );
		BI_Modelo__c sim = new BI_Modelo__c(Name = 'sim', RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'SIMCARD');

		List<BI_Modelo__c> lst_eq_serv = new List<BI_Modelo__c>();
			lst_eq_serv.add(equipo);
			lst_eq_serv.add(servicio);
			lst_eq_serv.add(sim);

		insert lst_eq_serv;

		Map<String,BI_Modelo__c> MAP_MODELO_NAME = new Map<String,BI_Modelo__c>();
		for(BI_Modelo__c mod :lst_eq_serv){
			MAP_MODELO_NAME.put(mod.Name,mod);
		}

		List<BI_Linea_de_Servicio__c> lst_ls = new List<BI_Linea_de_Servicio__c>();
		
		for(integer i=0; i<50;i++){
	        

//query = 'SELECT Id, BI_Usuario_BackOffice__c, BI_Solicitud_descargada_BackOffice__c FROM BI_Linea_de_Servicio__c
// WHERE BI_Solicitud_descargada_BackOffice__c = false AND BI_Estado_de_linea_de_servicio__c = \'Pendiente\'AND BI_Validada__c = true ';

		BI_Linea_de_Servicio__c ls = new BI_Linea_de_Servicio__c();
		ls.BI_Accion__c = 'test Action';
		ls.BI_Solicitud_descargada_BackOffice__c = false;
		ls.BI_Numero_de_telefono__c = '645885522';
		ls.BI_Modelo__c = MAP_MODELO_NAME.get('servicio').Id;
		//ls.BI_Linea_de_recambio__c = rec.Id;
		ls.BI_Validada__c = true;
		ls.BI_Estado_de_linea_de_servicio__c = 'Pendiente';

				
			lst_ls.add(ls);
	    }
	    
	    BI_GlobalVariables.stopTriggerIMPExcel = true;

		insert lst_ls;
		system.assert(!lst_ls.isEmpty());

		sol.BI_Estado_de_ventas__c = 'Enviando';
		sol.BI_Estado_de_recambios__c = 'Enviando';
		update sol;

		List<User> lst_usr = BI_Dataload.loadUsers(1, BI_Dataload.searchAdminProfile(), Label.BI_Administrador);
		system.assert(!lst_usr.isEmpty());

		Test.startTest();
			Database.executeBatch(new BI_BatchDescargaLVLR(5, lst_usr[0].Id), BI_MAXLIMIT_BATCHDESCARGALVLR);
		Test.stopTest();


		List<BI_Linea_de_Servicio__c> lst_ls_assert = [SELECT ID, BI_Usuario_BackOffice__c, BI_Solicitud_descargada_BackOffice__c FROM BI_Linea_de_Servicio__c WHERE ID IN :lst_ls ]; 
		system.debug('lst_ls_assert: ' + lst_ls_assert);
		system.assert(!lst_ls_assert.isempty());
		for(BI_Linea_de_Servicio__c lv :lst_ls_assert){
			system.assertEquals(lst_usr[0].Id,lv.BI_Usuario_BackOffice__c);
			system.assertEquals(true,lv.BI_Solicitud_descargada_BackOffice__c);
		}


	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        -------
	    Company:       -------
	    Description:   -------
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    03/02/2016                      Guillermo Muñoz             Fix bugs caused by news validations rules
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void solicitudcontingenciaLineaVentaCHI_option_6() {

		List<Account> lst_acc = BI_DataLoad.loadAccounts(1,new List<String>{'Chile'});
		system.assert(!lst_acc.isEmpty());
		User usu = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema AND Id !=: UserInfo.getUserId() LIMIT 1];

		BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(BI_Tipo_solicitud__c = 'Contingencia',
																		BI_Cliente__c = lst_acc[0].Id);
		System.runAs(usu){
			insert sol;
		}
		sol.BI_Lineas_de_venta__c = true;
		sol.BI_Estado_de_ventas__c = 'Aprobado';
		update sol;

        list<BI_Modelo__c> lstModelo = new list<BI_Modelo__c>();	
		
		BI_Modelo__c equipo = new BI_Modelo__c(Name = 'modelo' , RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'Smartphone' );
		BI_Modelo__c servicio = new BI_Modelo__c(Name = 'servicio' , RecordTypeId = MAP_RT_NAME.get('BI_Servicio') );
		BI_Modelo__c sim = new BI_Modelo__c(Name = 'sim', RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'SIMCARD');

		List<BI_Modelo__c> lst_eq_serv = new List<BI_Modelo__c>();
			lst_eq_serv.add(equipo);
			lst_eq_serv.add(servicio);
			lst_eq_serv.add(sim);

		insert lst_eq_serv;

		Map<String,BI_Modelo__c> MAP_MODELO_NAME = new Map<String,BI_Modelo__c>();
		for(BI_Modelo__c mod :lst_eq_serv){
			MAP_MODELO_NAME.put(mod.Name,mod);
		}

		List<BI_Linea_de_Venta__c> lst_lv = new List<BI_Linea_de_Venta__c>();
		
		for(integer i=0; i<50;i++){
	        
	        BI_Linea_de_Venta__c lv = new BI_Linea_de_Venta__c();

			//'SELECT Id, BI_Usuario_Logistica__c, BI_Solicitud_descargada_Logistica__c FROM BI_Linea_de_Venta__c WHERE BI_Estado_de_linea_de_venta__c = \'Pendiente\'
			//	AND BI_Solicitud_descargada_Logistica__c = false AND BI_Validada__c = true AND BI_Contingencia__c = true ';

			//lv.BI_Solicitud_descargada_BackOffice__c = false;
			//lv.BI_Estado_de_courier__c = 'Entregado';

			lv.BI_Solicitud_descargada_Logistica__c = false;
			lv.BI_Validada__c = true;
			lv.BI_Estado_de_linea_de_venta__c = 'Pendiente';

			lv.BI_Solicitud__c = sol.Id ;
			lv.BI_Modelo__c = MAP_MODELO_NAME.get('modelo').Id;
			lv.BI_Modelo_TEM__c = String.valueOf(MAP_MODELO_NAME.get('equipo'));
			lv.BI_Codigo_de_cliente_final__c = 'nuevo';
			lv.BI_Modalidad_de_venta__c = 'test modalidad';
			lv.BI_Tipo_de_Contrato__c = 'test contrato';
			lv.BI_Cantidad_de_cuotas__c = '12';
			lv.BI_Valor_de_cuota__c = 50;
			lv.BI_Plan__c = 'test plan';
			lv.BI_Otro_plan__c = 'test otro plan';
			lv.BI_Q_activado__c = 15;
			lv.BI_Cargo_basico__c = 100;
			lv.BI_Inlcuir_tarjeta_SIM__c = 'Si';
			lv.BI_Cantidad_de_equipos__c = 15;
			lv.BI_Precio_de_pie__c = 10;
			lv.BI_Precio_base_de_modelo__c = 10;
			lv.BI_Precio_base_de_SIM__c = 10;
			lv.BI_Precio_del_modelo_definido_manualmen__c = 'Si';
			lv.BI_Precio_de_SIM_definido_manualmente__c = 'Si';
			lv.BI_Ciclo_de_facturacion__c = 'test ciclo';
			lv.BI_Marca_de_cambio__c = false;
			//lv.BI_Beneficio_de_bolsa_de_dinero_de_model__c = 3000000;
			//lv.BI_Beneficio_de_bolsa_de_dinero_de_SIM__c = 1500000;
			lv.BI_Catalogo_de_Servicio_1_TEM__c = 'serv1-serv2';
			lv.BI_Catalogo_de_Servicio_2_TEM__c = 'serv1-serv2';
			lv.BI_Catalogo_de_Servicio_3_TEM__c = 'serv1-serv2';
			lv.BI_Catalogo_de_Servicio_4_TEM__c = 'serv1-serv2';
			lv.BI_Catalogo_de_Servicio_5_TEM__c = 'serv1-serv2';
			//lv.BI_Codigo_de_descuento_de_SIM__c = descSIM.Id;
			lv.BI_ID_de_simcard_TEM__c = String.valueOf(MAP_MODELO_NAME.get('sim').Id);
			//lv.BI_Codigo_de_Descuento_TEM__c = String.valueOf(descModelo.Id);
			//lv.BI_Descuento_SIM_TEM__c = String.valueOf(descSIM.Id);
			lv.BI_ID_de_simcard__c = MAP_MODELO_NAME.get('sim').Id;
				
			lst_lv.add(lv);
	    }
	    
	    BI_GlobalVariables.stopTriggerIMPExcel = true;

		insert lst_lv;
		system.assert(!lst_lv.isEmpty());
		sol.BI_Estado_de_ventas__c = 'Enviando';
		sol.BI_Estado_de_recambios__c = 'Enviando';
		update sol;

		List<User> lst_usr = BI_Dataload.loadUsers(1, BI_Dataload.searchAdminProfile(), Label.BI_Administrador);
		system.assert(!lst_usr.isEmpty());

		Test.startTest();
			Database.executeBatch(new BI_BatchDescargaLVLR(6, lst_usr[0].Id), BI_MAXLIMIT_BATCHDESCARGALVLR);
		Test.stopTest();


		List<BI_Linea_de_Venta__c> lst_lv_assert = [SELECT ID, BI_Usuario_Logistica__c, BI_Solicitud_descargada_Logistica__c FROM BI_Linea_de_Venta__c WHERE ID IN :lst_lv ]; 
		system.debug('lst_lv_assert: ' + lst_lv_assert);
		system.assert(!lst_lv_assert.isempty());
		for(BI_Linea_de_Venta__c lv :lst_lv_assert){
			system.assertEquals(lst_usr[0].Id,lv.BI_Usuario_Logistica__c);
			system.assertEquals(true,lv.BI_Solicitud_descargada_Logistica__c);
		}

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        -------
	    Company:       -------
	    Description:   -------
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    17/02/2016                      Guillermo Muñoz             Fix bugs caused by news validations rules
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void solicitudcontingenciaLineaRecambioCHI_option_7() {

		User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();
		List<Account> lst_acc = BI_DataLoad.loadAccounts(1,new List<String>{'Chile'});
		system.assert(!lst_acc.isEmpty());

		BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(BI_Tipo_solicitud__c = 'Contingencia',
																		BI_Estado_de_ventas__c = 'Enviando',
																		BI_Estado_de_recambios__c = 'Vacío',
 		    															BI_Cliente__c = lst_acc[0].Id);
		System.runAs(usu){
			insert sol;
			//List<BI_linea_de_recambio__c> lstRecambio = BI_DataLoadAgendamientos.loadLineaRecambio(1 ,sol.Id ,lst_acc[0]);
		}

        list<BI_Modelo__c> lstModelo = new list<BI_Modelo__c>();	
		
		BI_Modelo__c equipo = new BI_Modelo__c(Name = 'modelo' , RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'Smartphone' );
		BI_Modelo__c servicio = new BI_Modelo__c(Name = 'servicio' , RecordTypeId = MAP_RT_NAME.get('BI_Servicio') );
		BI_Modelo__c sim = new BI_Modelo__c(Name = 'sim', RecordTypeId = MAP_RT_NAME.get('BI_Modelo'), BI_Familia_Local__c = 'SIMCARD');

		List<BI_Modelo__c> lst_eq_serv = new List<BI_Modelo__c>();
			lst_eq_serv.add(equipo);
			lst_eq_serv.add(servicio);
			lst_eq_serv.add(sim);

		insert lst_eq_serv;

		Map<String,BI_Modelo__c> MAP_MODELO_NAME = new Map<String,BI_Modelo__c>();
		for(BI_Modelo__c mod :lst_eq_serv){
			MAP_MODELO_NAME.put(mod.Name,mod);
		}

		List<BI_Linea_de_Recambio__c> lst_lr = new List<BI_Linea_de_Recambio__c>();
		
		for(integer i=0; i<50;i++){
	        
	        BI_Linea_de_Recambio__c lr = new BI_Linea_de_Recambio__c();


			//'SELECT Id, BI_Usuario_Logistica__c, BI_Solicitud_descargada_Logistica__c FROM BI_Linea_de_Recambio__c WHERE BI_Estado_de_linea_de_recambio__c = \'Pendiente\' 
			//AND BI_Solicitud_descargada_Logistica__c = false AND BI_Validada__c = true AND BI_Contingencia__c = true';

			lr.BI_Solicitud__c = sol.Id ;
			lr.BI_Solicitud_descargada_Logistica__c = false;
			lr.BI_Estado_de_linea_de_recambio__c = 'Pendiente';
			//lr.BI_Estado_de_courier__c  = 'Entregado';
			//lr.BI_Solicitud_descargada_BackOffice__c = false;
			lr.BI_Modelo__c = MAP_MODELO_NAME.get('modelo').Id;
			lr.BI_Precio_final_de_modelo__c = 210;
			lr.BI_Precio_final_de_SIM__c = 100 ;
			lr.BI_Estado__c = 'test estado';
			lr.BI_Razon_para_recambio__c = 'test razon';
			lr.BI_Telefono_de_contacto__c = '666563221';
			lr.BI_Modalidad_de_venta__c = 'test modalidad';
			lr.BI_Tipo_de_contrato__c = 'test tipo contrato';
			lr.BI_Plan__c = 'test plan';
			lr.BI_Validada__c = true;
			lr.BI_Marca_de_cambio__c = true;
			lr.BI_Descuenta_puntos_Movistar__c = 'Sí';
			lr.BI_Descuenta_puntos_Movistar_SIM__c = 'Sí';
			lr.BI_Numero_de_telefono__c = '916102144';
				
			lst_lr.add(lr);
	    }
	    
	    BI_GlobalVariables.stopTriggerIMPExcel = true;

		insert lst_lr;
		system.assert(!lst_lr.isEmpty());

		sol.BI_Estado_de_ventas__c = 'Enviando';				
		sol.BI_Estado_de_recambios__c = 'Enviando';
		update sol;

		List<User> lst_usr = BI_Dataload.loadUsers(1, BI_Dataload.searchAdminProfile(), Label.BI_Administrador);
		system.assert(!lst_usr.isEmpty());
		List<BI_Solicitud_envio_productos_y_servicios__c> lst_sol = [SELECT Id, BI_Tipo_solicitud__c, BI_Estado_de_ventas__c, BI_Estado_de_recambios__c FROM BI_Solicitud_envio_productos_y_servicios__c WHERE Id = :sol.Id];
		System.debug('lst_sol: ' + lst_sol);

		Test.startTest();
			Database.executeBatch(new BI_BatchDescargaLVLR(7, lst_usr[0].Id), BI_MAXLIMIT_BATCHDESCARGALVLR);
		Test.stopTest();


		List<BI_Linea_de_Recambio__c> lst_lr_assert = [SELECT ID, BI_Usuario_Logistica__c, BI_Solicitud_descargada_Logistica__c FROM BI_Linea_de_Recambio__c WHERE ID IN :lst_lr ]; 
		system.debug('lst_lr_assert: ' + lst_lr_assert);
		system.assert(!lst_lr_assert.isempty());
		for(BI_Linea_de_Recambio__c lr :lst_lr_assert){
			system.assertEquals(lst_usr[0].Id,lr.BI_Usuario_Logistica__c);
			system.assertEquals(true,lr.BI_Solicitud_descargada_Logistica__c);
		}

	}

	
}