public without sharing class BI_LineaDeServicioHelper {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Methods Helper of Object BI_Linea_de_Recambio__c.
   
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon      Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Method for eliminating associated service lines
    
    IN:            ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   
	public static void eliminarLineaDeServicio(list<BI_Linea_de_Servicio__c> lLineaServicio){
		try{
			if (lLineaServicio.size()>0) delete lLineaServicio;
        }catch (exception exc){
			BI_LogHelper.generate_BILog('BI_LineaDeServicioHelper.eliminarLineaDeServicio', 'BI_EN', exc, 'Trigger');
		}		
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Method for Insert BI_Linea_de_Servicio__c in BI_Linea_de_Venta__c || BI_Linea_de_Recambio__c
    
    IN:            ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void insertLineaDeServicioIMP(list<SObject> lNew){
		//Insertar Líneas de Servicio.
	    set<string> auxNameServi = new set<string>{};
	    for (SObject aux: lNew){
	    	for (integer i = 1; i < 6; i++ ){
		    	if (aux.get('BI_Catalogo_de_Servicio_'+i+'_TEM__c') != '' && aux.get('BI_Catalogo_de_Servicio_'+i+'_TEM__c') != null){
		    		auxNameServi.add(String.valueOf(aux.get('BI_Catalogo_de_Servicio_'+i+'_TEM__c')));
		    	}
	    	}    		    	
	    }
	    //Buscar Id de los catalogos de servicio
    	if (auxNameServi.size()>0){ 
			list<RecordType> lRT = new list<RecordType>([SELECT Id,DeveloperName FROM RecordType WHERE SobjectType = 'BI_Modelo__c' AND DeveloperName = 'BI_Servicio' ORDER BY Id]);
			if (lRT.size()>0){
		    	list<BI_Modelo__c> lauxServal = [SELECT Id,Name FROM BI_Modelo__c  WHERE RecordTypeId=:lRT[0].Id AND (Name IN :auxNameServi) ORDER BY Name, Id];
				if (lauxServal.size()>0){
					list<BI_Linea_de_Servicio__c> lauxLinSer = new list<BI_Linea_de_Servicio__c>{}; 
					integer tipLinea = 0;
					if 		(lNew.getsObjectType() == BI_Linea_de_Venta__c.sObjectType)		tipLinea = 1; //Línea de Venta.
					else if (lNew.getsObjectType() == BI_Linea_de_Recambio__c.sObjectType)	tipLinea = 2; //Línea de Recambio.
					for (SObject aux: lNew){
						for (integer i = 1; i < 6; i++ ){
	    					Id IdLinea = (Id) aux.get('Id');
	    					string catServ = String.valueOf(aux.get('BI_Catalogo_de_Servicio_'+i+'_TEM__c'));
	    					string Acc = String.valueOf(aux.get('BI_Accion_'+i+'_TEM__c'));
	    					if (catServ != '' && catServ != null) findServicio(tipLinea, IdLinea, catServ, Acc, lauxLinSer, lauxServal);
						}    								
					}

					try{
						if (lauxLinSer.size()>0)	insert lauxLinSer;
			        }catch (exception exc){
						BI_LogHelper.generate_BILog('BI_LineaDeServicioHelper.insertLineaDeServicioIMP', 'BI_EN', exc, 'Trigger');
					}			  				
				}
			}
    	}
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Lozon
    Company:       Aborda.es
    Description:   Method for Find associated service lines
    
    IN:            insertLineaDeServicioIMP
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    25/02/2014        Pablo Lozon       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
	private static void findServicio(integer tipLinea, Id IdLinea, string catServ, string Acc, list<BI_Linea_de_Servicio__c> lauxLinSer, list<BI_Modelo__c> lauxServal){

    	for (BI_Modelo__c auxServal: lauxServal){
	    	if (auxServal.Name == catServ){
				boolean flag_add = true; //No se permiten servicios repetidos por Linea de Venta y Línea de Recambio.
				for (BI_Linea_de_Servicio__c findServ: lauxLinSer){	
		 			if (findServ.BI_Modelo__c == auxServal.Id 
		 				&& ((findServ.BI_Linea_de_Venta__c == IdLinea && tipLinea == 1) || (findServ.BI_Linea_de_Recambio__c == IdLinea && tipLinea == 2)) ){ 
		 						flag_add = false; break; }
				}	  		
	    		if (flag_add){	
		    		BI_Linea_de_Servicio__c auxLinSer = new BI_Linea_de_Servicio__c();
		    		if 		(tipLinea == 1)	auxLinSer.BI_Linea_de_Venta__c = IdLinea;
		    		else if (tipLinea == 2) auxLinSer.BI_Linea_de_Recambio__c = IdLinea;	
		    					    		
		    		auxLinSer.BI_Modelo__c = auxServal.Id;		    		
		    		if (Acc != '' && Acc != null) auxLinSer.BI_Accion__c = Acc;
		    		lauxLinSer.add(auxLinSer);
	    		}
	    		//else auxServal.addError('Para una misma línea ne se permiten servicios repetidos.');
	    		break;
	    	}
		}				
	}

}