public class BI_NoteMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   Validation to avoid modification on Attachment
    IN:           List <Attachment>
    OUT:               
    History:
    
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version
    24/10/2017        Gawron, Julian    Adding BI_Standard_PER to query
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void verificarPermisosContract(List<Note> news , List<Note> olds) {
        try
        { 
            if(BI_TestUtils.isRunningTest()){ throw new BI_Exception('Test');}
            
            List <Note> toProcess = new List <Note>();
            List <Note> toCheck = news != null ? news : olds;//Trabaja sobre olds si es un delete
            for(Note n : toCheck){
                if(n.ParentId.getSobjectType().getDescribe().getName() == 'NE__Contract_Header__c' ||
                    n.ParentId.getSobjectType().getDescribe().getName() == 'NE__Contract__c'){
                    toProcess.add(n);
                }
            }
            if(!toProcess.isEmpty()){
                Set<String> set_creacion = new Set<String>{Label.BI_Administrador, 'Administrador de Contrato', Label.BI_SuperUsuario};
                Set<String> set_modificacion = new Set<String>{Label.BI_Administrador, 'Administrador de Contrato', Label.BI_SuperUsuario};
                Set<String> set_eliminacion = new Set<String>{Label.BI_Administrador, Label.BI_SuperUsuario};
        
                List<User> me = [SELECT Id, BI_Permisos__c FROM User WHERE Id = :UserInfo.getUserId() and Profile.name = 'BI_Standard_PER' limit 1];
                
                if(me.isEmpty()) return; //Si la lista está vacía, el usuario no era de Perú, salimos.
               
                if(olds == null){//Insert
                    if(!set_creacion.contains(me[0].BI_Permisos__c)){
                        for(Note n : news) n.addError(Label.BI_PER_SinPermisos);
                    }
                }else if(news == null){ //delete
                    if(!set_eliminacion.contains(me[0].BI_Permisos__c)){
                        for(Note n : olds) n.addError(Label.BI_PER_SinPermisos);
                    }
                }else{ // es update
                    if(!set_modificacion.contains(me[0].BI_Permisos__c)){
                       for(Note n : news) n.addError(Label.BI_PER_SinPermisos);
                    }
                }
            }    
        }catch (exception Exc){
               BI_LogHelper.generate_BILog('BI_AttachmentMethods.verificarPermisos', 'BI_EN', Exc, 'Trigger');
        } 
    }
}