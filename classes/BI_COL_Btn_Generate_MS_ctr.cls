/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       22-Abr-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
public class BI_COL_Btn_Generate_MS_ctr 
{
    /** Parametros VF */
    public BI_COL_Anexos__c fun {get;set;}

    public boolean mostrarRegresar {get;set;}
    public boolean todosMarcados {get;set;}
    public boolean todosMarcadosAc {get;set;}
    public boolean rtable {get;set;}
    public boolean rtableAc {get;set;}
    public boolean errorServiciosGen;
    public boolean errorServiciosAc;

    public List<WrapperMS> lstMS;
    public List<WrapperMS> lstMSAutoconsumo;
    public List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta;
    public List<BI_COL_Modificacion_de_Servicio__c> lstMSAutoconsumoConsulta;

    public List<SelectOption> lstOppty {get;set;}
    private List<WrapperMS> pageMS;
    private List<WrapperMS> pageMSAc;
    public List<BI_COL_Modificacion_de_Servicio__c> lstMSSel;
    List<BI_COL_Modificacion_de_Servicio__c> lstMSDesSel;

    public String idOppty {get;set;}
    public String idOportunidad{get;set;}
    public String idFUN;

    public Map<String, List<BI_COL_Modificacion_de_Servicio__c>> mapOpptyMS;
    public Map<String, List<BI_COL_Modificacion_de_Servicio__c>> mapOpptyMSAc;
    /** Parametros del paginador */
    private Integer pageNumber;
    private Integer pageNumberAc;
    private Integer pageSize;
    private Integer pageSizeAc;
    private Integer totalPageNumber;
    private Integer totalPageNumberAc;
    //Constructor Standard Controller
    public BI_COL_Btn_Generate_MS_ctr( ApexPages.StandardController ctr )
    {
        rtable = true;
        rtableAc = true;
        idFUN = ctr.getId();
        if( idFUN != null )
        {
            //Obtenemos la lista de FUN asociados al id recibido como par?metro
            List<BI_COL_Anexos__c> lstFUN = [
                SELECT  BI_COL_Contrato__c, Id, Name, BI_COL_Fecha_de_venta__c,
                        BI_COL_Contrato__r.ContractNumber, BI_COL_Contrato__r.Account.Id,
                        BI_COL_Contrato__r.Account.AccountNumber,
                        BI_COL_Contrato__r.Account.Name, 
//20150918 PLL          BI_COL_Contrato__r.Account.BillingAddress, 
                        BI_COL_Contrato__r.Account.Phone,  
                        BI_COL_Contrato__r.ContractTerm,
                        BI_COL_Contrato__r.StartDate, 
                        BI_COL_Contrato__r.Status, 
                        BI_COL_Contrato__r.BI_COL_Monto_ejecutado__c,
                        BI_COL_Contrato__r.BI_COL_Presupuesto_contrato__c, 
                        BI_COL_Contrato__r.Name
                FROM    BI_COL_Anexos__c
                WHERE Id = :idFUN
                AND BI_COL_Estado__c <> 'Liberado'
                LIMIT 1 ];

            if(lstFUN.size() > 0)
            {
                mostrarRegresar = true;
                this.fun = lstFUN.get(0);
                if( this.fun.BI_COL_Fecha_de_venta__c != null )
                {
                    ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_MSconFechaEstablecida ) );
                    rtable = false;
                    rtableAc = false;
                    return;
                }
           
                contarRegistros( this.fun.BI_COL_Contrato__r.Account.Id );

                List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta = getMSSolicitudServicio( this.fun.BI_COL_Contrato__r.Account.Id );

                List<BI_COL_Modificacion_de_Servicio__c> lstMSAutoconsumoConsulta = getMSSolicitudServicioAutoconsumo( this.fun.BI_COL_Contrato__r.Account.Id );

                if( lstMSConsulta == null || lstMSConsulta.size() == 0 )
                {
                    errorServiciosGen = true; //saber que no hay servicios para mostrar
                    mostrarRegresar = false;
                }
                else
                {
                    errorServiciosGen = false;
                    //Llenar listado de MS Wrapper que se muestran en pantalla
                    Map<String,Opportunity> mapOppty = new Map<String,Opportunity>();
                    mapOpptyMS = new Map<String, List<BI_COL_Modificacion_de_Servicio__c>>();

                    lstMS = new List<WrapperMS>(); 
                    for(BI_COL_Modificacion_de_Servicio__c ms: lstMSConsulta)
                    {
                        WrapperMS wMS;
                        if( ms.BI_COL_FUN__c == null )
                        wMS = new WrapperMS( ms, false );
                        else
                        {
                            if( ms.BI_COL_FUN__c == this.fun.Id )
                                wMS = new WrapperMS( ms, true );
                            else
                                wMS = new WrapperMS( ms, false );
                        }
                        this.lstMS.add( wMS );
                    }
                    //Inicializar valores del paginador
                    pageNumber = 0;
                    totalPageNumber = 0;
                    pageSize = 40;
                    ViewData();
                }
                if( lstMSAutoconsumoConsulta == null || lstMSAutoconsumoConsulta.size() == 0 )
                {
                    //Ver si no hay y poner el mensaje para mostrar
                    //MessageUtil_cls.mostrarMensaje(MessageUtil_cls.ERROR, Label.lblClienteSinMSFamiliaDatos);
                    system.debug('\n@-->>lstMSConsulta'+lstMSConsulta);
                    if( lstMSConsulta == null || lstMSConsulta.size() == 0 )
                    {
                        errorServiciosAc = true;
                        mostrarRegresar = false;
                    }
                }
                else
                {
                    errorServiciosAc = false;
                    //Llenar listado de MS Wrapper que se muestran en pantalla Autoconsumo
                    lstMSAutoconsumo = new List<WrapperMS>(); 
                    for( BI_COL_Modificacion_de_Servicio__c ms : lstMSAutoconsumoConsulta )
                    {
                        WrapperMS wMSAc;
                        if( ms.BI_COL_FUN__c == null )
                            wMSAc = new WrapperMS(ms,false);
                        else
                        {
                            if( ms.BI_COL_FUN__c == this.fun.Id )
                                wMSAc = new WrapperMS( ms, true );
                            else
                                wMSAc = new WrapperMS( ms, false );
                                                                    
                        }
                        this.lstMSAutoconsumo.add( wMSAc );
                    }
                    //Inicializar valores del paginador
                    pageNumberAc = 0;
                    totalPageNumberAc = 0;
                    pageSizeAc = 40;
                    ViewDataAc();
                }
            }
            else
            {
                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblIdInvalido ) );
                mostrarRegresar = false;
            }
        }
        else
        {
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblEspecificarFUN ) );
            mostrarRegresar = false;
        }
    }

    public void contarRegistros( String idCliente )
    {
        Integer cant = 0;
        String ini1 = 'Select count() ';
        String ini2 = 'Select id, BI_COL_Oportunidad__c, BI_COL_Oportunidad__r.Name, BI_COL_Oportunidad__r.Id ';
        
        String sql =' from BI_COL_Modificacion_de_Servicio__c '
            + 'where BI_COL_Oportunidad__r.Account.Id = \'' + idCliente + '\' '
            + 'and BI_COL_Estado__c = \'Pendiente\' '
           // + 'and BI_COL_Producto__c <> Null '
            + 'and ( BI_COL_FUN__c = Null or BI_COL_FUN__c = \'' + idFUN + '\')';
        system.debug( '\n\n##SQL: ' + ini1 + sql +'\n\n' );
        /* consultar cantidad de registros para la consulta general */
        cant = Database.countQuery( ini1 + sql );
        /* traer el listado de todas las oportunidades asociadas a las MS de la consulta para llenar el combo en la FV */
        List<BI_COL_Modificacion_de_Servicio__c> lsms = Database.query( ini2 + sql );

        map<Id,Opportunity> mapIdOpp = new map<Id,Opportunity>(); 
        
        for( BI_COL_Modificacion_de_Servicio__c ms : lsms )
        {
            if(!mapIdOpp.containsKey( ms.BI_COL_Oportunidad__c ))
                mapIdOpp.put( ms.BI_COL_Oportunidad__c, ms.BI_COL_Oportunidad__r );
        }

        lstOppty = new List<SelectOption>();

        if( cant > 1000 )       
            idOppty = mapIdOpp.values()[0].Id;
        else
            lstOppty.add(new SelectOption('','--Ninguna--') );
            
        for( Opportunity oppty : mapIdOpp.values() )
            lstOppty.add( new SelectOption( oppty.id, oppty.Name ) );
    }
    /**
    * Genear asociacion de MS a la solicitud de Servicios
    * @return PageReference
    */
    public PageReference action_generarMS()
    {
        lstMSSel = new List<BI_COL_Modificacion_de_Servicio__c>(); 
        lstMSDesSel = new List<BI_COL_Modificacion_de_Servicio__c>();
          system.debug('\n@-->>lstMSAutoconsumo1\n'+lstMSAutoconsumo+'\nlstMS\n'+lstMS);
        if ( this.lstMS != null || this.lstMSAutoconsumo != null )
        {
            if( this.lstMS != null ) 
            {
                for( WrapperMS wMS : this.lstMS )
                {
                    if( wMS.seleccionado )
                        lstMSSel.add( wMS.ms );
                    else if( wMS.ms.BI_COL_FUN__c != null )
                    {
                        wMS.ms.BI_COL_FUN__c=null;
                        lstMSDesSel.add( wMS.ms );
                    }
                }
            }
            system.debug('\n@-->>lstMSAutoconsumo2'+lstMSAutoconsumo); 
            if( this.lstMSAutoconsumo != null ) 
            {
                //Autoconsumo
                for( WrapperMS wMS : this.lstMSAutoconsumo )
                {
                    if( wMS.seleccionado )
                        lstMSSel.add( wMS.ms );
                    else if( wMS.ms.BI_COL_FUN__c != null )
                    {
                        wMS.ms.BI_COL_FUN__c = null;
                        lstMSDesSel.add( wMS.ms );
                    }
                }
            }
            try
            {

            update lstMSDesSel;

                }catch(Exception e)
                {
                        System.debug('Error ==== '+e.getMessage());        
                       // ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR,'Error ===== '+e.getMessage() ) );
                }
            System.debug('\n@--> lstMSSel <--@\n'+lstMSSel);     
            if( lstMSSel == null || lstMSSel.size()==0)
            {
                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblSeleccionarRegistro ) );
                return null;
            }

            Boolean control = true;
            String strMensajeContrato = '';

            //Iterar ms seleccionadas y asociarlas a la solicitud de servicio
            for( BI_COL_Modificacion_de_Servicio__c ms : lstMSSel )
                ms.BI_COL_FUN__c = this.fun.Id;

            if( control )
            {
                Savepoint sp;
                try
                {
                    //Actualizar registros MS
                    sp = Database.setSavepoint();
                    update lstMSSel;
                    ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblActualizacionBD ) );
                }
                catch( DMLException e )
                {
                    ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblErrorActualizacionBD + e.getMessage() ) );
                    System.debug('\n\n Lista no se actualiza'+e.getMessage());
                    Database.rollback(sp);
                    return null;
                }
                //Consultar las MS que no tengan asociaci?n
                List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta = getMSSolicitudServicio(this.fun.BI_COL_Contrato__r.Account.Id );
                //Autoconsumo
                List<BI_COL_Modificacion_de_Servicio__c> lstMSAutoconsumoConsulta = getMSSolicitudServicioAutoconsumo( this.fun.BI_COL_Contrato__r.Account.Id );

                if(lstMSConsulta == null || lstMSConsulta.size() == 0 )
                {
                    ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblNoMasMSAsociables ) );
                    mostrarRegresar = false;
                }
                else
                    reiniciarConsultaMS(lstMSConsulta);
                

                if( lstMSConsulta == null || lstMSConsulta.size() == 0 )
                {
                    ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, Label.BI_COL_lblNoMasMSAsociables ) );
                    mostrarRegresar = false;
                }
                else
                    reiniciarConsultaMSAc( lstMSAutoconsumoConsulta );

                PageReference page = new PageReference('/' + this.fun.Id);
                page.setRedirect(true);
                return page;
            }
            else
                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, strMensajeContrato ) );
        }
        return null;
    }
    /**
    * Formatea la pantalla con las MS que no se han generado
    * @return
    */
    public void reiniciarConsultaMS (List<BI_COL_Modificacion_de_Servicio__c> lstMSConsulta)
    {
        this.lstMS = new List<WrapperMS>(); 
        this.pageMS = new List<WrapperMS>();
        
        for( BI_COL_Modificacion_de_Servicio__c ms: lstMSConsulta )
        {
            WrapperMS wMS = new WrapperMS( ms, false );
            this.lstMS.add( wMS );
        }
        //Inicializar valores del paginador
        pageNumber = 0;
        totalPageNumber = 0;
        pageSize = 40;
        ViewData();
    }

    public void reiniciarConsultaMSAc( List<BI_COL_Modificacion_de_Servicio__c> lstMSConsultalstMSConsulta )
    {
        this.lstMSAutoconsumo = new List<WrapperMS>(); 
        this.pageMSAc = new List<WrapperMS>();
        for( BI_COL_Modificacion_de_Servicio__c ms : lstMSConsultalstMSConsulta )
        {
            WrapperMS wMS = new WrapperMS( ms, false );
            this.lstMSAutoconsumo.add( wMS );
        }
        //Inicializar valores del paginador
        pageNumberAc = 0;
        totalPageNumberAc = 0;
        pageSizeAc = 40;
        ViewDataAc();
    }
    /**
    * Accion para seleccionar todos los registros de MS
    * @return PageReference
    */
    public PageReference action_seleccionarTodos()
    {
        if( this.lstMS != null )
        {
            for( WrapperMS wMS : this.lstMS )
                wMS.seleccionado = this.todosMarcados;
        }
        return null;
    }

    public PageReference action_seleccionarTodosAc()
    {
        if( this.lstMSAutoconsumo != null ) 
        {
            for( WrapperMS wMS : this.lstMS )
                wMS.seleccionado = this.todosMarcadosAc;
        }
        return null;
    }
    //Clase Wrapper que administra la selecci?n de registros
    public class WrapperMS
    {
        public boolean seleccionado {get;set;}
        public BI_COL_Modificacion_de_Servicio__c ms {get;set;}

        public WrapperMS(BI_COL_Modificacion_de_Servicio__c ms, boolean seleccionado)
        {
            this.ms = ms;
            this.seleccionado = seleccionado;
        }
    }
    /**
    * Obtiene el listado de MS asociadas al FUN
    * @param idCliente Id del cliente asociado al FUN
    * @return  Lista de MS
    */
    public List<BI_COL_Modificacion_de_Servicio__c> getMSSolicitudServicio( String idCliente )
    {
        List<BI_COL_Modificacion_de_Servicio__c> lstMS = new List<BI_COL_Modificacion_de_Servicio__c>();

        String sql = 'Select Id, Name, BI_COL_Oportunidad__c, '
        + ' BI_COL_Producto__r.NE__ProdId__r.Rama_Local__c , '
        + ' BI_COL_Producto__r.Name, BI_COL_FUN__c, '
        + ' BI_COL_Producto__r.NE__ProdId__r.Family_Local__c, '
        + ' BI_COL_Oportunidad__r.Name, '
        + ' BI_COL_Producto__r.NE__ProdId__r.Name '
        + ' from BI_COL_Modificacion_de_Servicio__c '
        + ' where BI_COL_Oportunidad__r.Account.Id = \'' + idCliente + '\' '
        + ' and BI_COL_Estado__c = \'Pendiente\' ' //Pruebas LMJ
        //+ ' and BI_COL_Producto__c <> Null '
        + ' and BI_COL_Autoconsumo__c != true '
        + ' and ( BI_COL_FUN__c= Null or  BI_COL_FUN__c= \'' + idFUN + '\')';

        if(idOppty!=null && !idOppty.equals(''))
            sql+='and BI_COL_Oportunidad__c =\''+idOppty+'\' ';

        String sqlOrder = 'order by Name Asc '; 

        sql += sqlOrder;
        system.debug('\n\n##getMSSolicitudServicio SQL: '+ sql +'\n\n');
        lstMS = Database.query(sql);
        return lstMS;
    }

    public List<BI_COL_Modificacion_de_Servicio__c> getMSSolicitudServicioAutoconsumo( String idCliente )
    {
        List<BI_COL_Modificacion_de_Servicio__c> lstMSAutoconsumo = new List<BI_COL_Modificacion_de_Servicio__c>();

        String sql = 'Select Id, Name, BI_COL_Oportunidad__c, '
            + ' BI_COL_Producto__r.NE__ProdId__r.Rama_Local__c, BI_COL_FUN__c, '
            + ' BI_COL_Producto__r.Name ,BI_COL_Producto__r.NE__ProdId__r.Family_Local__c, '
            + ' BI_COL_Oportunidad__r.Name, '
            + ' BI_COL_Producto__r.NE__ProdId__r.Name ' 
            + ' from BI_COL_Modificacion_de_Servicio__c '
            + ' where BI_COL_Oportunidad__r.Account.Id = \'' + idCliente + '\' '
            + ' and BI_COL_Estado__c = \'Pendiente\' '  
            //+ ' and BI_COL_Producto__c <> Null '
            + ' and BI_COL_Autoconsumo__c = true '
            + ' and ( BI_COL_FUN__c= Null or BI_COL_FUN__c= \'' +idFUN + '\')';

        if( idOppty != null && !idOppty.equals('') )
            sql+=' and BI_COL_Oportunidad__c =\''+idOppty+'\' ';

        String sqlOrder = 'order by Name Asc '; 

        sql += sqlOrder;
        system.debug('\n\n##getMSSolicitudServicioAutoconsumo SQL: '+ sql +'\n\n');
        lstMSAutoconsumo = Database.query( sql );

        return lstMSAutoconsumo;
    }
    //IMPLEMENTACION PAGINADOR
    public Integer getPageNumber() 
    {
        return pageNumber;
    }

    public Integer getPageNumberAc()
    {
        return pageNumberAc;
    }

    public Integer getPageSize() 
    {
        return pageSize;
    }

    public Integer getPageSizeAc() 
    {
        return pageSizeAc;
    }

    public Boolean getPreviousButtonEnabled() 
    {
        return !(pageNumber > 1);
    }

    public Boolean getPreviousButtonEnabledAc() 
    {
        return !(pageNumberAc > 1);
    }

    public Boolean getNextButtonDisabled() 
    {
        if (lstMS == null){ return true;}
        else{return ((pageNumber * pageSize) >= lstMS.size());}
    }

    public Boolean getNextButtonDisabledAc() 
    {
        if (lstMSAutoconsumo == null){  return true;}
        else{ return ((pageNumberAc * pageSizeAc) >= lstMSAutoconsumo.size());}
    }
    /**
    * Obtiene el numer de paginas de la tabla 
    * @return numero de paginas
    */
    public Integer getTotalPageNumber()
    {
        if (totalPageNumber == 0 && lstMS !=null)
        {
            totalPageNumber = lstMS.size() / pageSize;
            Integer mod = lstMS.size() - (totalPageNumber * pageSize);
            if (mod > 0)
                totalPageNumber++;
        }
        return totalPageNumber;
    }
    //Autoconsumo
    public Integer getTotalPageNumberAc() 
    {
        if( totalPageNumberAc == 0 && lstMSAutoconsumo != null )
        {
            totalPageNumberAc = lstMSAutoconsumo.size() / pageSizeAc;
            Integer mod = lstMSAutoconsumo.size() - ( totalPageNumberAc * pageSizeAc);
            if( mod > 0 )
                totalPageNumberAc++;
        }
        return totalPageNumberAc;
    }

    public PageReference ViewData()
    {
        totalPageNumber = 0;
        BindData(1);
        return null;
    }

    public PageReference ViewDataAc()
    {
        totalPageNumberAc = 0;
        BindDataAc(1);
        return null;
    }
    /**
    * Posiciona el registro segun el numero de pagina
    * @param newPageIndex Indice de la pagina
    * @return 
    */
    private void BindData(Integer newPageIndex)
    {
        try
        {
            pageMS = new List<WrapperMS>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;

            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }

            for(WrapperMS wMS : lstMS)
            {
                counter++;
                if( counter > min && counter <= max )
                    pageMS.add(wMS);
            }
            
            pageNumber = newPageIndex;

            if( pageMS == null || pageMS.size() <= 0 )
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, Label.BI_COL_lblDatosNoDisponiblesVista + ' sin Autoconsumo'));
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message( ApexPages.severity.FATAL, ex.getMessage() ) );
        }   
    }

    private void BindDataAc(Integer newPageIndexAc)
    {
        try 
        {
            pageMSAc = new List<WrapperMS>();
            Transient Integer counterAc = 0;
            Transient Integer minAc = 0;
            Transient Integer maxAc = 0;
            if (newPageIndexAc > pageNumberAc) 
            {
                minAc = pageNumberAc * pageSizeAc;
                maxAc = newPageIndexAc * pageSizeAc;
            }
            else 
            {
                maxAc = newPageIndexAc * pageSizeAc;
                minAc = maxAc - pageSizeAc;
            }

            for( WrapperMS wMS : lstMSAutoconsumo ) 
            {
                counterAc++;
                if( counterAc > minAc && counterAc <= maxAc )
                    pageMSAc.add(wMS);
            }
            
            pageNumberAc = newPageIndexAc;

            if( pageMSAc == null || pageMSAc.size() <= 0 )
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, Label.BI_COL_lblDatosNoDisponiblesVista + ' con Autoconsumo'));
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message( ApexPages.severity.FATAL, ex.getMessage() ) );
        }
    }

    public PageReference nextBtnClick() 
    {
        BindData(pageNumber + 1);
        return null;
    }

    public PageReference nextBtnClickAc() 
    {
        BindDataAc(pageNumberAc + 1);
        return null;
    }

    public PageReference previousBtnClick()
    {
        BindData(pageNumber - 1);
        return null;
    }

    public PageReference previousBtnClickAc() 
    {
        BindDataAc(pageNumberAc - 1);
        return null;
    }

    public List<WrapperMS> getLstMS() 
    {
        return pageMS;
    }

    public List<WrapperMS> getLstMSAutoconsumo() 
    {
        return pageMSAc;
    }
    /**
    * Accion para filtrar las MS seg?n las oportunidades
    * @return PageReference
    */
    public PageReference action_filtrarMSbyOppty ()
    {
        lstMSConsulta = getMSSolicitudServicio( this.fun.BI_COL_Contrato__r.Account.Id );

        lstMSAutoconsumoConsulta = getMSSolicitudServicioAutoconsumo( this.fun.BI_COL_Contrato__r.Account.Id );
        todosMarcados = false;
        todosMarcadosAc = false;
        if( idOppty == null || idOppty.trim().length() == 0 )
        {
            lstMS = new List<WrapperMS>(); 
            for( BI_COL_Modificacion_de_Servicio__c ms: lstMSConsulta )
            {
                WrapperMS wMS;
                if( ms.BI_COL_FUN__c == null )
                    wMS = new WrapperMS( ms, false );
                else
                    wMS = new WrapperMS( ms, true );
                this.lstMS.add( wMS );
            }
            //Inicializar valores del paginador
            pageNumber = 0;
            totalPageNumber = 0;
            pageSize = 50;
            ViewData();
        }
        else if( idOppty != null && idOppty.trim().length() > 0 )
        {
            //Llenar listado de MS Wrapper que se muestran en pantalla
            lstMS = new List<WrapperMS>(); 
            for( BI_COL_Modificacion_de_Servicio__c ms: lstMSConsulta )
            {
                WrapperMS wMS;
                if( ms.BI_COL_FUN__c == null )
                    wMS = new WrapperMS( ms, false );
                else
                    wMS = new WrapperMS( ms, true );
                this.lstMS.add( wMS );
            }
            //Inicializar valores del paginador
            pageNumber = 0;
            totalPageNumber = 0;
            pageSize = 50;
            ViewData();
        }

        if( idOppty == null || idOppty.trim().length() == 0 )
        {
            lstMSAutoconsumo = new List<WrapperMS>(); 
            for( BI_COL_Modificacion_de_Servicio__c ms : lstMSAutoconsumoConsulta )
            {
                WrapperMS wMS;
                
                if( ms.BI_COL_FUN__c == null )
                    wMS = new WrapperMS( ms, false );
                else
                    wMS = new WrapperMS( ms, true );
                this.lstMSAutoconsumo.add( wMS );
            }
            //Inicializar valores del paginador
            pageNumberAc = 0;
            totalPageNumberAc = 0;
            pageSizeAc = 50;
            ViewDataAc();
        }
        else if( idOppty != null && idOppty.trim().length() > 0)
        {
            //Llenar listado de MS Wrapper que se muestran en pantalla
            lstMSAutoconsumo = new List<WrapperMS>(); 
            for( BI_COL_Modificacion_de_Servicio__c ms: lstMSAutoconsumoConsulta )
            {
                WrapperMS wMS;
                if( ms.BI_COL_FUN__c == null )
                    wMS = new WrapperMS( ms, false );
                else
                    wMS = new WrapperMS( ms, true );

                this.lstMSAutoconsumo.add( wMS );
            }
            //Inicializar valores del paginador
            pageNumberAc = 0;
            totalPageNumberAc = 0;
            pageSizeAc = 50;
            ViewDataAc();
        }
        return null;
    }
    
}