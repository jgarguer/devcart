@isTest(SeeAllData=true)
public class PCA_Content_Controller_TEST {
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Beatriz Garcés
    Company:       Deloitte
    Description:   Test class to manage the coverage code for PCA_Content_Controller class 

    <Date>                  <Author>                <Description>
    28/05/2015              Beatriz Garcés          Initial Version
    09/02/2017              Pedro Párraga           Solved problem that made the test fail
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static testMethod void getloadInfo() {
            Test.startTest();
            PCA_Content_Controller controller = new PCA_Content_Controller();
            controller.Search();
            Test.stopTest(); 
    }
}