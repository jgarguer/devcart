@isTest
private class PCA_EventMethods_TEST {

    static testMethod void getEventOn() {
     
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
		PCA_EventMethods EvMeth = new PCA_EventMethods();
		DateTime day = datetime.newInstance(2014, 9, 15, 12, 30, 0);
        DateTime day10 = datetime.newInstance(2014, 9, 15, 13, 30, 0);
        Id idProf = [select id, name from Profile where name = 'System Administrator' or name = 'Administrador del sistema' limit 1].Id;	
        User usr = new User (alias = 'stttt6',email='6u@testorg.com',emailencodingkey='UTF-8', lastname='Testing6',languagelocalekey='en_US',localesidkey='en_US',ProfileId = idProf, BI_Permisos__c=Label.BI_Administrador, timezonesidkey=Label.BI_TimeZoneLA, username= '6ur23@testorg.com');        
        insert usr;	
        system.runAs(usr){
       	
		//Region__c pais = new Region__c(Name = 'test04',BI_Codigo_ISO__c = String.valueOf(0000045));
		//insert pais;
		Map<String, BI_Code_ISO__c> map_region = BI_DataLoad.loadRegions();
    	BI_Code_ISO__c region;

       	for(BI_Code_ISO__c biCode: map_region.values()){
       		if(biCode.BI_Country_ISO_Code__c == 'CHI'){
       				region = biCode;
       		}
       	}
		
		Account acc = new Account(Name = 'test5',
                             BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                             BI_Activo__c = Label.BI_Si,
                             BI_Country__c = region.Name,
                             OwnerId=usr.Id,
                             BI_Segment__c = 'test', //28/09/2017
                             BI_Subsegment_Regional__c = 'test', //28/09/2017
                             BI_Territory__c = 'test' //28/09/2017
                             ); //Region__c = pais.id,
		insert acc;
		Event ev1 = new Event(subject='Nuevo Evento', description='Test Evento', ownerId = acc.ownerId, DurationInMinutes=60,ActivityDateTime=DAY,ActivityDate=Date.today(),EndDateTime=day10,StartDateTime=day);
		insert ev1;
		
		ev1.subject = 'NuevoTest';
		update ev1;

		system.assertEquals(ev1.StartDateTime,day);
        }

    }
    static testMethod void getEventOn2() {
     
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		PCA_EventMethods EvMeth = new PCA_EventMethods();
		DateTime day = datetime.newInstance(2014, 9, 15, 12, 30, 0);
        DateTime day10 = datetime.newInstance(2014, 9, 15, 13, 30, 0);
        Id idProf = [select id, name from Profile where name = 'System Administrator' or name = 'Administrador del sistema' limit 1].Id;	
        User usr = new User (alias = 'stttt6',email='6u@testorg.com',emailencodingkey='UTF-8', lastname='Testing6',languagelocalekey='en_US',localesidkey='en_US',ProfileId = idProf, BI_Permisos__c=Label.BI_Administrador, timezonesidkey=Label.BI_TimeZoneLA, username= '6ur23@testorg.com');        
        insert usr;	
        system.runAs(usr){
       	Map<String, BI_Code_ISO__c> map_region = BI_DataLoad.loadRegions();
    	BI_Code_ISO__c region;
    	
       	for(BI_Code_ISO__c biCode: map_region.values()){
       		if(biCode.BI_Country_ISO_Code__c == 'ARG'){
       				region = biCode;
       		}
       	}
		//Region__c pais = new Region__c(Name = 'test04',BI_Codigo_ISO__c = String.valueOf(0000045));
		//insert pais;
		Account acc = new Account(Name = 'test5',
                             BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                             BI_Activo__c = Label.BI_Si,
                             BI_Country__c = region.Name,
                             OwnerId=usr.Id,
                             BI_Segment__c = 'test', //28/09/2017
                             BI_Subsegment_Regional__c = 'test', //28/09/2017
                             BI_Territory__c = 'test' //28/09/2017
                             );
		insert acc;
		Event ev1 = new Event(subject='Nuevo Evento', description='Test Evento', ownerId = acc.ownerId, DurationInMinutes=60,ActivityDateTime=DAY,ActivityDate=Date.today(),EndDateTime=day10,StartDateTime=day);
		insert ev1;
		
		ev1.subject = 'NuevoTest';
		update ev1;
        }

    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}