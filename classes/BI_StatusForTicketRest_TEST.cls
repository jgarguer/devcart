@isTest
private class BI_StatusForTicketRest_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_StatusForTicketRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/09/2014              Pablo Oliva             Initial Version
    17/08/2015              Francisco Ayllon        Solved problems with class BI_TestUtils (whole class mod)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_StatusForTicketRest.getStatusForTicket()
    History:
    
    <Date>              <Author>                <Description>
    19/09/2014          Pablo Oliva             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getStatusForTicketTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_TestUtils.throw_exception = false;
        
        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');                                                
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        List <Case> lst_tickets = BI_DataLoadRest.loadTickets(1, lst_acc[0].Id, lst_region[0]);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/ticketing/v1/tickets/'+lst_tickets[0].BI_Id_del_caso_Legado__c+'/satus';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        //INTERNAL_SERVER_ERROR
        BI_TestUtils.throw_exception = true;
        BI_StatusForTicketRest.getStatusForTicket();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c where BI_Asunto__c = 'BI_StatusForTicketRest.getStatusForTicket'];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        //BAD REQUEST
        BI_TestUtils.throw_exception = false;
        BI_StatusForTicketRest.getStatusForTicket();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //NOT FOUND
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        req.requestURI = '/ticketing/v1/tickets/idTicket';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;
        
        BI_StatusForTicketRest.getStatusForTicket();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.requestURI = '/ticketing/v1/tickets/'+lst_tickets[0].BI_Id_del_caso_Legado__c+'/status';
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_StatusForTicketRest.getStatusForTicket();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();

    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}