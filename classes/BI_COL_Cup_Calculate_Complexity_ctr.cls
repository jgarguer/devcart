public with sharing class BI_COL_Cup_Calculate_Complexity_ctr {
	
	public BI_COL_PreFactibilidad__c objPrefactibilidad{get;set;}
	public list<BI_COL_MatrizP__c> lstConfMatrizP;
	public boolean calculate{get;set;}
	public map<String,BI_COL_MatrizP__c> mpConfPMatrizP;
	public set<String> stCampoLista;
	public set<String> stCampoTexto;
	public String strValorCampo;
	public Decimal NumValorCampo;
	public String consulta;

	private ID idprecomp;
	
	public BI_COL_Cup_Calculate_Complexity_ctr()
	{
		if(ApexPages.currentPage().getParameters().get('idprecomp') != null)
		{
			this.idprecomp = ApexPages.currentPage().getParameters().get('idprecomp');				
						
			Map<String, Schema.SObjectField> mapSch = new Map<String, Schema.SObjectField>();
       	
	        mapSch = Schema.SObjectType.BI_COL_PreFactibilidad__c.fields.getMap();
	        
	        Set<String> stExceptions = new set<String>();
	        
	        stExceptions.add('systemmodstamp');
			stExceptions.add('pricebook2id');
			stExceptions.add('lastmodifieddate');
			stExceptions.add('lastmodifiedbyid');
			stExceptions.add('lastactivitydate');
			stExceptions.add('isdeleted');
			stExceptions.add('createdbyid');
			stExceptions.add('status');	
			stExceptions.add('id');
		
			consulta='SELECT Id ';
		
	        for(string s : mapSch.keySet()){
	        	
				if(!stExceptions.contains(s.toLowerCase()))
				{
					consulta+=', '+s;
				}
			}
			
			String idprecomp = ApexPages.currentPage().getParameters().get('idprecomp');
			consulta += ' FROM BI_COL_PreFactibilidad__c WHERE Id =: idprecomp LIMIT 1';
			System.debug('\n\n=======>CONSULTA==========\n'+consulta);
			objPrefactibilidad = Database.query(consulta);		
			
			lstConfMatrizP = [SELECT Campo__c, ValorLista__c, PuntajePrefactibilidad__c, FactorComplejidad__c, PuntoACalcular__c, CalcularPref__c FROM BI_COL_MatrizP__c];
			
			calculate = true;
		}
	}
		
	public PageReference calcularComplejidadyPrefactibilidad(){
		
		PageReference p = null;
		
		if(idprecomp == null){
			
			p = new PageReference('apex/error');
            p.getParameters().put('error', 'noParam');
            
		}else{
			
			stCampoLista = new set<String>();
			stCampoTexto = new set<String>();		
			mpConfPMatrizP = new map<String,BI_COL_MatrizP__c>();
			
			for(BI_COL_MatrizP__c elem : lstConfMatrizP)
			{
				System.debug('=======>'+elem.Campo__c+' -- '+elem.ValorLista__c);
				if(elem.ValorLista__c != null)
				{
					mpConfPMatrizP.put(elem.Campo__c+''+elem.ValorLista__c,elem);
					System.debug('\n\n===========mpConfPMatrizPACTUAL=============>'+elem.Campo__c+''+elem.ValorLista__c+', '+elem);
				}
				else
				{
					mpConfPMatrizP.put(elem.Campo__c,elem);
				}

			}
			System.debug('\n\n===========mpConfPMatrizP=============\n'+mpConfPMatrizP);
							
			for(BI_COL_MatrizP__c punt : mpConfPMatrizP.values())
			{
				System.debug('puntACTUAL=======>'+punt);
				System.debug('punt.CalcularPref__c--------------->>>>'+punt.CalcularPref__c);
				if(punt.CalcularPref__c){
				
					if(punt.Campo__c != '' && punt.ValorLista__c != '' && punt.ValorLista__c != null){
					
						stCampoLista.add(punt.Campo__c);
						System.debug('AGREGO1');					
					}
				
				}else{
				
					if(punt.Campo__c != '' && punt.ValorLista__c != '' && punt.ValorLista__c != null ){
					
						stCampoLista.add(punt.Campo__c);
						System.debug('AGREGO2');
					
					}else if(punt.Campo__c != '' && (punt.ValorLista__c == '' || punt.ValorLista__c == null) && punt.FactorComplejidad__c != null ){
						
						stCampoTexto.add(punt.Campo__c);
						System.debug('AGREG3');
					}
						
				}
			}
			
			BI_COL_MatrizP__c tem = new BI_COL_MatrizP__c();
			Decimal Puntos = 0;
			Decimal PuntosVerticales = 0;
			Decimal PuntosHorizontales = 0;
			Decimal PuntosUC = 0;
			Decimal PuntosIT = 0;
			Decimal PuntosMovil = 0;
			Decimal PuntosParciales = 0;

			for(String Clistas : stCampoLista)
			{
				System.debug('\n\n ============================Clistas===================\n'+Clistas);
				strValorCampo = String.valueOf(objPrefactibilidad.get(Clistas));
				System.debug('\n\n ============================strValorCampo===================\n'+strValorCampo);
				System.debug('\n\n ============================strValorCampostrValorCampo===================\n'+Clistas+''+strValorCampo);
				tem = mpConfPMatrizP.get(Clistas+''+strValorCampo);
				System.debug(' mpConfPMatrizP------>>>'+tem);
				if(tem != null){
					
					if(tem.PuntoACalcular__c == 'PuntosUC')
					{						
						if(!tem.CalcularPref__c)
						{						
							PuntosUC += tem.FactorComplejidad__c;
							System.debug('###### sumo ' + tem.FactorComplejidad__c + '   tengo ' +  PuntosUC + ' campo ' + Clistas);					
						}
						
					}

					if(tem.PuntoACalcular__c == 'PuntosIT')
					{						
						if(!tem.CalcularPref__c)
						{						
							PuntosIT += tem.FactorComplejidad__c;						
						}
						
					}

					if(tem.PuntoACalcular__c == 'PuntosMovil')
					{						
						if(!tem.CalcularPref__c)
						{						
							PuntosMovil += tem.FactorComplejidad__c;
						
						}
						
					}

					if( tem.PuntoACalcular__c == 'Puntos'){
						
						if(tem.CalcularPref__c){
					
							Puntos += tem.PuntajePrefactibilidad__c;
						
						}else{
							
							Puntos += tem.FactorComplejidad__c;
							
						}
					
					}

					if(tem.PuntoACalcular__c == 'PuntosVerticales'){
						
						if(tem.CalcularPref__c){
				
							PuntosVerticales += tem.PuntajePrefactibilidad__c;
					
						}else{
						
							PuntosVerticales += tem.FactorComplejidad__c;
						
						}
							
					}

					if(tem.PuntoACalcular__c == 'PuntosHorizontales'){
							
						if(tem.CalcularPref__c){
				
							PuntosHorizontales += tem.PuntajePrefactibilidad__c;
					
						}else{
						
							PuntosHorizontales += tem.FactorComplejidad__c;
						
						}
				
					}	
				}
			}
			
			System.debug('\n\n ============================stCampoTexto===================\n\n'+stCampoTexto);
			for(String Ctexto : stCampoTexto){
				
				
				System.debug('\n\n ============================Ctexto===================\n'+Ctexto);
				String numero=String.valueOf(objPrefactibilidad.get(Ctexto));
				//NumValorCampo = numero.isNumeric()?Decimal.valueOf(numero):1; //eHelp 01463085 
				NumValorCampo = isNumericPattern(numero)?Decimal.valueOf(numero):0;  //eHelp 01463085 
				System.debug('NumValorCampo----->>>'+NumValorCampo);
				tem = mpConfPMatrizP.get(Ctexto);
				
				if(tem != null){
					
					if(tem.PuntoACalcular__c == 'PuntosUC'){
						
						if(!tem.CalcularPref__c){
						
							PuntosUC += (tem.FactorComplejidad__c * NumValorCampo);
							System.debug('NumValoPuntosUC----->>>'+PuntosUC + ' nombreCampo---->' + Ctexto);
						}
						
					} 

					if(tem.PuntoACalcular__c == 'PuntosIT'){
						
						if(!tem.CalcularPref__c){
						
							PuntosIT += (tem.FactorComplejidad__c * NumValorCampo);
							System.debug('NumValoPuntosIT----->>>'+PuntosIT);
						}
						
					} 

					if(tem.PuntoACalcular__c == 'PuntosMovil'){
						
						if(!tem.CalcularPref__c){
						
							PuntosMovil += (tem.FactorComplejidad__c * NumValorCampo);
							System.debug('NumValoPPuntosMovil----->>>'+PuntosMovil);
						}
						
					}

					if( tem.PuntoACalcular__c == 'Puntos'){
						
						if(tem.CalcularPref__c){
					
							Puntos += (tem.PuntajePrefactibilidad__c * NumValorCampo);
							System.debug('NumValoPuntos----->>>'+Puntos);
						}else{
							
							Puntos += (tem.FactorComplejidad__c * NumValorCampo);
							
						}
					}
				}
			}
			
			System.debug('ValoresPuntosUC------>>>'+PuntosUC);//+'\n'+PuntosIT+'\n'+PuntosMovil);
			System.debug('ValoresPuntosIT------>>>'+PuntosIT);
			System.debug('ValoresPuntosMovil------>>>'+PuntosMovil);
			PuntosParciales = PuntosUC + PuntosIT + PuntosMovil;
			System.debug('PuntosParciales=========>>>>'+PuntosParciales);
            Puntos += PuntosParciales;
            System.debug('Valores333------>>>'+Puntos);
			objPrefactibilidad.BI_COL_Puntuacion_deseno_com_colab__c = PuntosUC;
			objPrefactibilidad.BI_COL_Puntuacion_IT__c = PuntosIT;
			objPrefactibilidad.BI_COL_Puntuacion_movil__c = PuntosMovil;
			objPrefactibilidad.BI_COL_Total_Vertical__c = PuntosVerticales;
			objPrefactibilidad.BI_COL_Total_Horizontal__c = PuntosHorizontales;
			objPrefactibilidad.BI_COL_Puntuacion_Total__c = Puntos;
			try
			{
			
			objPrefactibilidad.BI_COL_Porcentaje_diseno_com_colab__c = (PuntosUC * 100) / PuntosParciales;
			objPrefactibilidad.BI_COL_Porcentaje_IT__c = (PuntosIT * 100) / PuntosParciales;
			objPrefactibilidad.BI_COL_Porcentaje_movil__c = (PuntosMovil * 100) / PuntosParciales;

			}catch(Exception e ){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Debe diligenciar algun valor en los campos numericos obligatorios'));	
			}
			
			 if(Puntos < 90)
            {
                objPrefactibilidad.BI_COL_Complejidad__c = 'Baja';
            }
            else if(Puntos >= 91 && puntos <= 200)
            {
                objPrefactibilidad.BI_COL_Complejidad__c = 'Media';
            }
            else if(Puntos > 201 && puntos < 350)
            {
                objPrefactibilidad.BI_COL_Complejidad__c = 'Alta';
            }
            else if(Puntos >= 351)
            {
                objPrefactibilidad.BI_COL_Complejidad__c = 'Especial';
            }
			


			if(PuntosVerticales >0 && PuntosHorizontales >0)
			{
				objPrefactibilidad.BI_COL_Resultado__c = 'Factible';
			}

			else if (PuntosVerticales >0 && PuntosHorizontales <0)
			{
				objPrefactibilidad.BI_COL_Resultado__c = 'Comite';	
			}
			
			else if (PuntosVerticales <0 && PuntosHorizontales <0)
			{
				objPrefactibilidad.BI_COL_Resultado__c = 'No Factible';
			}

			else if (PuntosVerticales <0 && PuntosHorizontales >0)
			{
				objPrefactibilidad.BI_COL_Resultado__c = 'Comite';
			}

			try
			{
			
				update objPrefactibilidad;
				p = new PageReference('/'+idprecomp);
				p.setRedirect(true);
			
			}
			catch(Exception ex)
			{
				Apexpages.Message result = new Apexpages.Message(ApexPages.Severity.INFO, ex.getMessage());
				ApexPages.addMessage(result);
			}

		}

		return p;
		
	}


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Micah Burgos  
	Company:       Aborda.es
	Description:   Method that check if String txt can casting to Decimal number.
	
	History: 
	
	<Date>                  <Author>                <Change Description>
	04/01/2016              Micah Burgos          	Initial Version     
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@TestVisible
	private static boolean isNumericPattern(String txt){
		if(String.isNotBlank(txt)){
			
			String regexpression = '[0-9]*\\.?[0-9]*'; //https://regex101.com/
			Pattern MyPattern = Pattern.compile(regexpression);
			Matcher MyMatcher = MyPattern.matcher(txt);
			
			if (MyMatcher.matches()){
				return true;
			}
		}
		return false;
	}

}