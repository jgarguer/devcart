public with sharing class PCA_PortalesCtrl extends PCA_HomeController{
    public List<BI_Portal_local__c> GetPortals {get;set;}
    public List<String> ImageToshow {get;set;}
    public Map<BI_Portal_local__c,String> mapPortalsURL {get;set;}
    public Map<BI_Portal_local__c,String> mapPortalsLINK {get;set;}
    public String currentUserCountry {get;set;}
    public Id currentAcc {get;set;}
    public string accSeg {get;set;}
    public BI_Portal_local__c PortLoc {get;set;}
    // INI - Everis - Evo CWP - Portales - 13/03/2017
    public Profile p{get;set;}
    // FIN - Everis - Evo CWP - Portales - 13/03/2017
    // INI - Everis - Evo CWP - Portales - 13/03/2017 - Perfil añadido al constructor
    public PCA_PortalesCtrl() {p=[SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>              <Description>
    22/04/2015        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_PortalesCtrl.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   Constructor custom for Portales
    
    History:
    
    <Date>            <Author>              <Description>
    22/04/2015        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfo(){      
        try{
            
            currentAcc = BI_AccountHelper.getCurrentAccountId();
            PortLoc = new BI_Portal_local__c();
            if(currentAcc != null){
                Account AccHome = [SELECT Id,BI_Imagen_Inicio_PP__c,BI_Country__c,
                                   BI_Segment__c FROM Account WHERE Id = :currentAcc LIMIT 1];
                
                if(AccHome.BI_Country__c != null){
                    currentUserCountry = AccHome.BI_Country__c;
                }
                accSeg = AccHome.BI_Segment__c;
                if(accSeg == null){
                    accSeg = 'Empresas';
                }
                
                // INI - Everis - Evo CWP - Portales - 13/03/2017 - Añadida diferenciación por perfil - La query original está comentada /**/
                
                //p = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
                if(p.Name.contains('TGS')){
                    GetPortals = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
                                  WHERE BI_Activo__c=TRUE AND CWP_TGS_Users__c = true ORDER BY BI_Posicion__c asc];
                    system.debug('PORTALES 1 TGS:'+GetPortals);
                }else{
                    GetPortals = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
                                  WHERE (BI_Country__c =: currentUserCountry OR BI_Country__c = null) AND (BI_Segment__c =: accSeg OR BI_Segment__c= null) AND BI_Activo__c=TRUE AND CWP_TGS_Users__c = false
                                  ORDER BY BI_Posicion__c asc];
                    system.debug('PORTALES 2 BIEN:'+GetPortals);
                }
                /* Query Original: GetPortals = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
WHERE (BI_Country__c =: currentUserCountry OR BI_Country__c = null) AND (BI_Segment__c =: accSeg OR BI_Segment__c= null) AND BI_Activo__c=TRUE ORDER BY BI_Posicion__c asc];*/
                
                // FIN - Everis - Evo CWP - Portales - 13/03/2017 - Añadida diferenciación por perfil
                List<String> stringsSRC = new List<String>();
                mapPortalsURL = new Map<BI_Portal_local__c,String>();
                mapPortalsLINK = new Map<BI_Portal_local__c,String>();
                for(BI_Portal_local__c portalsImg :GetPortals){
                    stringsSRC = portalsImg.BI_Imagen_del_portal__c.split('"');
                    String SRCInter = stringsSRC[3].replace('amp;','');
                    stringsSRC = SRCInter.split('empresasplatino');
                    String SRCFinal2 = stringsSRC[1];
                    String SRCFinal = SRCFinal2;
                    mapPortalsURL.put(portalsImg,SRCFinal);
                    mapPortalsLINK.put(portalsImg,portalsImg.BI_Link_del_portal__c);
                }
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_PortalesCtrl.loadInfo', 'Portal Platino', Exc, 'Class');
        }
        
    } 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   Constructor custom for Portales
    
    History:
    
    <Date>            <Author>              <Description>
    23/04/2015        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void getObj(){
        try{
            String PortalHover = ApexPages.currentPage().getParameters().get('myParam');
            system.debug('PortalHover**: '+PortalHover);
            //INI - Everis - Evo CWP - Diferenciación perfiles TGS - 16/03/2017
            if(p.Name.contains('TGS')){
                PortLoc = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
                           WHERE BI_Activo__c=TRUE AND Id = :PortalHover AND CWP_TGS_Users__c = true ORDER BY BI_Posicion__c asc];
            }else{
                PortLoc = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
                           WHERE (BI_Country__c =: currentUserCountry OR BI_Country__c = null) AND (BI_Segment__c =: accSeg OR BI_Segment__c= null) AND Id =:PortalHover AND BI_Activo__c=TRUE   ORDER BY BI_Posicion__c DESC];
            }
            /* Query Original: PortLoc = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
WHERE (BI_Country__c =: currentUserCountry OR BI_Country__c = null) AND (BI_Segment__c =: accSeg OR BI_Segment__c= null) AND Id =:PortalHover AND BI_Activo__c=TRUE   ORDER BY BI_Posicion__c DESC];*/
            //FIN - Everis - Evo CWP - Diferenciación perfiles TGS - 16/03/2017
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_PortalesCtrl.getObj', 'Portal Platino', Exc, 'Class');
        }
    }
}