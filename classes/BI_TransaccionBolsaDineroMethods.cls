public with sharing class BI_TransaccionBolsaDineroMethods {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by Account Triggers 
    Test Class:    BI_AccountMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if BI_Bolsa_de_dinero__c Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void blockEdition(List <BI_Transaccion_de_bolsa_de_dinero__c> news){
        try{
            if(BI_GlobalVariables.stopTriggerDesc == false){
            	Set <Id> set_disc = new Set <Id>();
            	for(BI_Transaccion_de_bolsa_de_dinero__c trans:news){        		
            		set_disc.add(trans.BI_Codigo_de_bolsa_de_dinero__c);        		
            	}

            	List <BI_Bolsa_de_dinero__c> lst_bolsa = [SELECT Id, BI_Estado_del_Proceso__c FROM BI_Bolsa_de_dinero__c WHERE Id IN :set_disc];
            	for (BI_Bolsa_de_dinero__c bolsa:lst_bolsa){
            		for(BI_Transaccion_de_bolsa_de_dinero__c trans:news){
            			if (bolsa.Id == trans.BI_Codigo_de_bolsa_de_dinero__c && 
                            (bolsa.BI_Estado_del_Proceso__c == label.BI_BolsaDineroCancelado ||
                             bolsa.BI_Estado_del_Proceso__c == 'Rechazado' ||
                             bolsa.BI_Estado_del_Proceso__c == 'Cerrado' )){
            				trans.addError(label.BI_TransaccionBloqueada);
            			}	
            		}
            		
            	}
            }
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoModeloMethods.blockEdition', 'BI_EN', exc, 'Trigger');
        }
    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if BI_Bolsa_de_dinero__c Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void blockDelete(List <BI_Transaccion_de_bolsa_de_dinero__c> olds){
        try{
            Set <Id> set_disc = new Set <Id>();
            for(BI_Transaccion_de_bolsa_de_dinero__c trans:olds){               
                set_disc.add(trans.BI_Codigo_de_bolsa_de_dinero__c);                
            }

            List <BI_Bolsa_de_dinero__c> lst_bolsa = [SELECT Id, BI_Estado_del_Proceso__c FROM BI_Bolsa_de_dinero__c WHERE Id IN :set_disc];
            for (BI_Bolsa_de_dinero__c bolsa:lst_bolsa){
                for(BI_Transaccion_de_bolsa_de_dinero__c trans:olds){
                    if (bolsa.Id == trans.BI_Codigo_de_bolsa_de_dinero__c && 
                        (bolsa.BI_Estado_del_Proceso__c == label.BI_BolsaDineroCancelado ||
                         bolsa.BI_Estado_del_Proceso__c == 'Rechazado' ||
                         bolsa.BI_Estado_del_Proceso__c == 'Cerrado' )){
                        trans.addError(label.BI_BorradoTransaccion);
                    }   
                }
                
            }
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoModeloMethods.blockEdition', 'BI_EN', exc, 'Trigger');
        }
    }


     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if BI_Bolsa_de_dinero__c Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void recalculateMoneyBag(List <BI_Transaccion_de_bolsa_de_dinero__c> news, List <BI_Transaccion_de_bolsa_de_dinero__c> olds){
        //try{
            Set <Id> set_bolsas = new Set <Id>();
            Set <Id> set_error = new Set <Id>();
            Integer i = 0;
            Boolean negativo = false;

            for(BI_Transaccion_de_bolsa_de_dinero__c trans:news){ 
                if(olds == null || olds !=null && trans.BI_Movimiento__c != olds[i].BI_Movimiento__c){
                    set_bolsas.add(trans.BI_Codigo_de_bolsa_de_dinero__c); 
                }
                i++;
            }
            if(!set_bolsas.IsEmpty()){
                List <BI_Bolsa_de_dinero__c> lst_bolsa = [SELECT Id, BI_Estado_del_Proceso__c, BI_Monto_Inicial__c, 
                                                        (select Id, BI_Movimiento__c ,BI_Codigo_de_bolsa_de_dinero__c from Transacci_n_de_bolsa_de_dinero__r) FROM BI_Bolsa_de_dinero__c WHERE Id IN :set_bolsas];
                if (!lst_bolsa.IsEmpty()){
                    for (BI_Bolsa_de_dinero__c bolsa:lst_bolsa){
                        Decimal saldo = 0;
                        for(BI_Transaccion_de_bolsa_de_dinero__c trans:bolsa.Transacci_n_de_bolsa_de_dinero__r){ 
                            saldo += trans.BI_Movimiento__c;
                            if(bolsa.BI_Monto_Inicial__c - saldo < 0){
                                set_error.add(trans.BI_Codigo_de_bolsa_de_dinero__c);                                                               
                            }
                        }
                        bolsa.BI_Saldo_disponible__c = bolsa.BI_Monto_Inicial__c - saldo;                         
                    }
                    if(!set_error.IsEmpty()){
                        for(BI_Transaccion_de_bolsa_de_dinero__c trans:news){
                            if(set_error.contains(trans.BI_Codigo_de_bolsa_de_dinero__c)){                           
                                negativo = true;
                                trans.addError(label.BI_SaldoNegativo); 
                            }
                        }
                    }
                    if(!negativo)
                    update lst_bolsa;
                }
            }    
        /*}catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoModeloMethods.blockEdition', 'BI_EN', exc, 'Trigger');
        }*/
    }  


      /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if BI_Bolsa_de_dinero__c Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void recalculateMoneyBagDelete(List <BI_Transaccion_de_bolsa_de_dinero__c> olds){
        try{
            Set <Id> set_bolsas = new Set <Id>();
            for(BI_Transaccion_de_bolsa_de_dinero__c trans:olds){ 
                set_bolsas.add(trans.BI_Codigo_de_bolsa_de_dinero__c);                
            }

            List <BI_Bolsa_de_dinero__c> lst_bolsa = [SELECT Id, BI_Estado_del_Proceso__c, BI_Monto_Inicial__c, 
                                                    (select Id, BI_Movimiento__c from Transacci_n_de_bolsa_de_dinero__r) FROM BI_Bolsa_de_dinero__c WHERE Id IN :set_bolsas];
            
            for (BI_Bolsa_de_dinero__c bolsa:lst_bolsa){
                Decimal saldo = 0;
                for(BI_Transaccion_de_bolsa_de_dinero__c trans:bolsa.Transacci_n_de_bolsa_de_dinero__r){ 
                    saldo += trans.BI_Movimiento__c;
                }
                bolsa.BI_Saldo_disponible__c = bolsa.BI_Monto_Inicial__c - saldo; 
            }
            update lst_bolsa;
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoModeloMethods.blockEdition', 'BI_EN', exc, 'Trigger');
        }
    }

   
}