/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 16/09/2016          Fernando Arteaga           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_QueueableUpdateParentOpps implements Queueable
{
	List<Opportunity> listParentOppties;
	
    public BI_O4_QueueableUpdateParentOpps (List<Opportunity> listOpp)
    {
		this.listParentOppties = listOpp;
		System.debug(LoggingLevel.INFO, listOpp);
    }
    
    public void execute(QueueableContext context)
    {
    	try
    	{
        	update this.listParentOppties;
    	}
    	catch (Exception exc)
    	{
    		BI_LogHelper.generate_BILog('BI_O4_QueueableUpdateParentOpps.execute', 'BI_EN', Exc, 'Trigger');
    	}
    }
}