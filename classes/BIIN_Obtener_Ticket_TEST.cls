@isTest(seeAllData = false)
public class BIIN_Obtener_Ticket_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José María Martín
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_Obtener_Ticket_WS

    <Date>          <Author>                        <Change Description>
    11/2015         José María Martín               Initial Version
    05/2016         José Luis González Beltrán      Adapt TEST to UNICA interface modifications in tested class
    20/09/2017      Angel F. Santaliestra           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
    27/09/2017      Jaime Regidor                   Add the mandatory fields on account creation: BI_Sector__c and BI_Subsector__c in method test_BIIN_Obtener_Ticket_Colombia_TEST
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
    private final static String LE_NAME = 'TestAccount_Obtener_Ticket_TEST';

    @testSetup static void dataSetup()
    {
        Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert account;

        Account[] acc = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketTest = BIIN_UNICA_TestDataGenerator.createTicket(acc[0]);
        insert ticketTest;
    }

    @isTest static void test_BIIN_Obtener_Ticket_TEST()
    {
        String authorizationToken = 'OBTENER_TICKET';
        BI_TestUtils.throw_exception = false;

        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketIncidenciaTecnica = [SELECT Id, BI_Id_del_caso_legado__c FROM Case WHERE AccountId =: account.Id];

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerTicket'));
        BIIN_Obtener_Ticket_WS controller = new BIIN_Obtener_Ticket_WS();
        controller.detalleTicket(authorizationToken, ticketIncidenciaTecnica.Id);

        Test.stopTest();
    }

    //Prueba con cuenta y ticket Colombia
    @isTest static void test_BIIN_Obtener_Ticket_Colombia_TEST()
    {
        //Creación cuenta país Colombia
        //RECORDTYPE id = 012w0000000iT34AAE => TGS_Legal_Entity
        Account accountCOL = new Account(
            Name                               = 'cuentaCOL',
            BI_Activo__c                       = Label.BI_Si,
            BI_Country__c                      = 'Colombia',
            BI_Segment__c                      = 'Empresas',
            BI_Subsector__c                    = 'test', //27/09/2017
            BI_Sector__c                       = 'test', //27/09/2017
            BI_Subsegment_Regional__c          = 'test',
            BI_Territory__c                    = 'test',
            BI_Tipo_de_identificador_fiscal__c = 'NIT',
            BI_No_Identificador_fiscal__c      = '800000000',
            BI_Validador_Fiscal__c             = 'COL_NIT_800000000',
            RecordTypeId                       = '012w0000000iT34AAE'
        );
        insert accountCOL;
        //Creción ticket país Colombia
        //RECORDTYPE  id = 012w000000071OcAAI => BIIN_Solicitud_Incidencia_Tecnica
        Case ticketCOL = new Case(
            BI_Id_del_caso_legado__c = '00000001',
            Subject                  = 'Subject test',
            Description              = 'Description test',
            BI_Country__c            = 'Colombia',
            AccountId                =  accountCOL.Id,
            Priority                 = 'High',
            Status                   = 'Assigned',
            RecordTypeId             = '012w000000071OcAAI',
            TGS_Urgency__c           = '2-High',
            Origin                   = 'Correo electrónico',
            TGS_SLA_Light__c         = 'Green'
        );
        insert ticketCOL;
        String authorizationToken = 'OBTENER_TICKET';
        BI_TestUtils.throw_exception = false;

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerTicketCOL'));
        BIIN_Obtener_Ticket_WS controller = new BIIN_Obtener_Ticket_WS();
        BIIN_Obtener_Ticket_WS.Worklog wl=new BIIN_Obtener_Ticket_WS.Worklog();
        controller.detalleTicket(authorizationToken, ticketCOL.Id);
        String IncId=wl.IncId;
        String z2AFWorkLog03=wl.z2AFWorkLog03;
        String viewAccess=wl.viewAccess;
        BIIN_Obtener_Ticket_WS.SLA sl=new BIIN_Obtener_Ticket_WS.SLA();
        String UpTime=sl.UpTime;
        String MetMissedAmount=sl.MetMissedAmount;
        Test.stopTest();
    }

}