/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    22015-06-02      Daniel ALexander Lopez (DL)     Cloned Web service      
*************************************************************************************/

global class BI_COL_Gestion_Quiebres_sw {
	
	global class GestionQuiebrestRequest
	  {
	    
	    webservice String Estado;
	    webservice String Ds;
	    webservice String Causal;
	    webservice String IdUsuario;
	    webservice String Observaciones;
	    webservice Date  FechaAplazamiento;
	    webservice String CodTrs;
	    webservice String Ms;
	  }
  
	  global class UsuariosResponse
	  {
	    webservice String Idsfdc;
	    webservice String Nombre;
	    webservice String Cargo;
	  }
  
	     global class GestionQuiebrestResponse
	    {
	        webservice boolean IsSuccess;           // Indica si la operación fue exitosa o no (true/false).
	        webservice List<ErrorLog> ErrorList;       // Lista de códigos y mensajes de error. Si la respuesta IsSuccess = true esta lista estará vacía.             
	    }
	  
	  global class ErrorLog
	  {
	    webservice Integer Code;             // Indica el código del error generado
	    webservice String Message;             // Texto del mensaje de error correspondiente 
	    }
    
    
     webservice static List<UsuariosResponse> ConsultarUsarios(){
       List<UsuariosResponse> respuesta=new List<UsuariosResponse>();
       UsuariosResponse usuario=null;
       
       BI_COL_PerfilesUsuario__c perf = BI_COL_PerfilesUsuario__c.getInstance('Ingeniero');
       List<User> lstUsuario=[select Id,Title, FirstName, LastName, Name from User where pais__C='Colombia' and BI_Permisos__c ='Ingeniería/Preventa' and IsActive=true];
       
       if(lstUsuario.size()>0){
         for(User us:lstUsuario){
           usuario=new UsuariosResponse();
           usuario.Idsfdc=us.Id;
           usuario.Cargo=us.Title;
           usuario.Nombre=us.Name;
           respuesta.add(usuario);
         }
       }
       
       return respuesta;
     }
    
    
    webservice static GestionQuiebrestResponse insertarQuiebre(GestionQuiebrestRequest obj){
       GestionQuiebrestResponse respuesta=new GestionQuiebrestResponse();
       system.debug('------ insertarQuiebre ----------->'+obj);
       BI_COL_Seguimiento_Quiebre__c quiebre=new BI_COL_Seguimiento_Quiebre__c();
       try{
         quiebre.BI_COL_Causal_quiebre_inicial__c=obj.Causal;
         quiebre.BI_COL_Codigo_Quiebre_Sisgot__c=obj.CodTrs;
         //quiebre.Descripcion_de_servicio_DSR__c=getDRS(respuesta,obj.Ds);
         quiebre.BI_COL_Modificacion_de_Servicio__c=getMs(respuesta,obj.Ms);
         quiebre.OwnerId=obj.IdUsuario;
         if(obj.Estado.equals('Aplazada'))
           quiebre.BI_COL_Estado_de_Gestion__c='Abierto';
         else
           quiebre.BI_COL_Estado_de_Gestion__c='Cerrado';
         quiebre.BI_COL_Estado_envio__c='Creado TRS';
         quiebre.BI_COL_Transaccion__c='TRS';
         quiebre.BI_COL_Fecha_fin_aplazamiento__c=obj.FechaAplazamiento;
         quiebre.BI_COL_Observaciones__c=obj.Observaciones;
      //   quiebre.BI_COL_Actualizacion_gestion__c=obj.Observaciones;
       
       
         insert quiebre;
         System.debug('Quiebre insertado desde WSW-->'+quiebre);
         
         System.debug('Creando Tarea');
         respuesta.IsSuccess=true; 
         Task tarea=new Task();
         tarea.OwnerId=obj.IdUsuario;
         tarea.Description='Se ha creado el registro de Seguimiento de Quiebre: '+quiebre.id;
         insert tarea;
         
       }catch(Exception e){
         System.debug('Error en la inserción del quiebre::'+e);
         adicionaError(respuesta,1,''+e);
       }
       return respuesta;
     }
     
  webservice static GestionQuiebrestResponse actualizarQuiebre(GestionQuiebrestRequest obj)
  {
    GestionQuiebrestResponse respuesta=new GestionQuiebrestResponse();
     system.debug('------ actualizarQuiebre ----------->'+obj);
       if(obj.CodTrs!=null && !obj.CodTrs.equals('') && !obj.CodTrs.equals('null'))
       {
      try
      {
        BI_COL_Seguimiento_Quiebre__c quiebre=[select Id,BI_COL_Causal_quiebre_inicial__c,BI_COL_Codigo_Quiebre_Sisgot__c,
                   BI_COL_Modificacion_de_Servicio__c,OwnerId,BI_COL_Estado_de_Gestion__c,BI_COL_Estado_envio__c,BI_COL_Observaciones__c,BI_COL_Actualizacion_gestion__c from BI_COL_Seguimiento_Quiebre__c where BI_COL_Codigo_Quiebre_Sisgot__c=:obj.CodTrs limit 1];
        
        quiebre.BI_COL_Causal_quiebre_inicial__c=obj.Causal;
           quiebre.BI_COL_Codigo_Quiebre_Sisgot__c=obj.CodTrs;
           System.debug('\n\n obj.CodTrs='+obj.CodTrs+'\n\n');
           quiebre.OwnerId=obj.IdUsuario;
           
           if(obj.Estado.equals('Aplazada'))
           {
             quiebre.BI_COL_Estado_de_Gestion__c='Abierto';
             quiebre.BI_COL_Fecha_fin_aplazamiento__c=obj.FechaAplazamiento;
           }
           else
           {
             quiebre.BI_COL_Estado_de_Gestion__c='Cerrado';
             quiebre.BI_COL_Fecha_fin_aplazamiento__c=System.now().date();
           }
             
           quiebre.BI_COL_Estado_envio__c='Actualizado TRS';
           quiebre.BI_COL_Transaccion__c='TRS';
           
           
           
           if(obj.Observaciones!=null && !obj.Observaciones.equals(''))
           {
             quiebre.BI_COL_Observaciones__c=obj.Observaciones;
        }
       
         
           update quiebre;
       
           respuesta.IsSuccess=true; 
           Task tarea=new Task();
           tarea.OwnerId=obj.IdUsuario;
           tarea.Description='Se ha actualizado el registro de Seguimiento de Quiebre: '+quiebre.id;
           insert tarea;
           
         }
         catch(QueryException e)
         {
           System.debug('---Error SOQL-->'+e);
           adicionaError(respuesta,1,'Error en el proceso de actualización NO existe un quiebre con el cod de TRS '+obj.CodTrs);
         }
         catch(Exception e)
      {
        System.debug('---Error general-->'+e);
        adicionaError(respuesta,1,'Error en el proceso de actualización '+e);
         }
       }else{
         System.debug('---Error codigo trs vacio-->');
         adicionaError(respuesta, 1, 'No se encontró el codigo Sisgot en la solicitud: '+obj.CodTrs);
       }
       
       return respuesta;
     }
     
     
      // Metodo que se encarga de generar el response del consumo del WS añadiendo a una lista los errores generados de la ejecución
    public static void adicionaError(GestionQuiebrestResponse resp, Integer codigo, String mensaje)
    {
        ErrorLog objError = new ErrorLog();
        objError.Code = codigo;
        objError.Message = mensaje;
        
        resp.IsSuccess = false;
        if(resp.ErrorList != null)
            resp.ErrorList.add(objError);
        else
        {
            resp.ErrorList = new List<ErrorLog>();
            resp.ErrorList.add(objError);
        }
    }
    
    
   /* public static String getDRS(GestionQuiebrestResponse resp,String nombre){
      List<Nueva_DS__c> retorno=new List<Nueva_DS__c>();
      System.debug('----DRS a buscar==='+nombre);
      
      retorno=[select Id from Nueva_DS__c where Name=:nombre limit 1];
        
       if(retorno.size()==0){
         adicionaError(resp,1,'No se encontró la DRS: '+nombre);
         return '';
       }
      return retorno[0].Id;
    }*/
    
    public static String getMs(GestionQuiebrestResponse resp,String nombre){
      List<BI_COL_Modificacion_de_Servicio__c> retorno=new List<BI_COL_Modificacion_de_Servicio__c>();
      System.debug('----MS a buscar==='+nombre);
      
      retorno=[select Id from BI_COL_Modificacion_de_Servicio__c where Name=:nombre limit 1];
      
      if(retorno.size()==0){
         adicionaError(resp,1,'No se encontró la Ms: '+nombre);
         return '';
      }
      return retorno[0].Id;
    }
 }