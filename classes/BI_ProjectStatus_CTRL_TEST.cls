@isTest 
private class BI_ProjectStatus_CTRL_TEST {
	
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Micah Burgos
Company:       Salesforce.com
Description:   Test class to manage the coverage code for BI_ProjectStatus_CTRL class 

History: 

<Date>                  <Author>                <Change Description>
23/07/2014              Micah Burgos 	        Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void projectStatusTest(){
		
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		//COMPLETED PROJECT
		List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List<Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);
        
        Opportunity o = new Opportunity(Name = 'Test',
                                              CloseDate = Date.today(),
                                              BI_Requiere_contrato__c = true,
                                              Generate_Acuerdo_Marco__c = false,
                                              AccountId = lst_acc[0].id,
                                              BI_Ciclo_ventas__c = 'Completo',
                                              //StageName = 'F1 - Closed Won',
											  BI_Ultrarapida__c = true,
											  BI_Duracion_del_contrato_Meses__c = 3);

        
		list<Milestone1_Project__c> list_projects = new list<Milestone1_Project__c>();
		
        Milestone1_Project__c proj_1 = new Milestone1_Project__c();
        	proj_1.Name='Project 1';
        	proj_1.BI_Oportunidad_asociada__c = o.Id; 
        	proj_1.BI_Etapa_del_proyecto__c = 'Kickoff de inicio';
            proj_1.BI_O4_Project_Customer__c = 'uno';
        list_projects.add(proj_1);
        
        Milestone1_Project__c proj_2 = new Milestone1_Project__c();
        	proj_2.Name='Project 2';
        	proj_2.BI_Oportunidad_asociada__c = o.Id; 
        	proj_2.BI_Etapa_del_proyecto__c = 'Inicio';
            proj_2.BI_O4_Project_Customer__c = 'uno';
        list_projects.add(proj_2);
        
        Milestone1_Project__c proj_3 = new Milestone1_Project__c();
        	proj_3.Name='Project 3';
        	proj_3.BI_Oportunidad_asociada__c = o.Id; 
        	proj_3.BI_Etapa_del_proyecto__c = 'Planificación';
            proj_3.BI_O4_Project_Customer__c = 'uno';
        list_projects.add(proj_3);
        
       Milestone1_Project__c proj_4 = new Milestone1_Project__c();
        	proj_4.Name='Project 4';
        	proj_4.BI_Oportunidad_asociada__c = o.Id; 
        	proj_4.BI_Etapa_del_proyecto__c = 'Ejecución';
            proj_4.BI_O4_Project_Customer__c = 'uno';
        list_projects.add(proj_4);
        
        Milestone1_Project__c proj_5 = new Milestone1_Project__c();
        	proj_5.Name='Project 5';
        	proj_5.BI_Oportunidad_asociada__c = o.Id; 
        	proj_5.BI_Etapa_del_proyecto__c = 'Control';
            proj_5.BI_O4_Project_Customer__c = 'uno';
        list_projects.add(proj_5);
        
        Milestone1_Project__c proj_6 = new Milestone1_Project__c();
        	proj_6.Name='Project 6';
        	proj_6.BI_Oportunidad_asociada__c = o.Id; 
        	proj_6.BI_Etapa_del_proyecto__c = 'Cierre';
            proj_6.BI_O4_Project_Customer__c = 'uno';
        list_projects.add(proj_6);
        
        Milestone1_Project__c proj_7 = new Milestone1_Project__c();
        	proj_7.Name='Project 7';
        	proj_7.BI_Oportunidad_asociada__c = o.Id; 
        	proj_7.BI_Etapa_del_proyecto__c = 'Kickoff de cierre';
            proj_7.BI_O4_Project_Customer__c = 'uno';
        list_projects.add(proj_7);
        
        Milestone1_Project__c proj_8 = new Milestone1_Project__c();
            proj_8.Name='Project 7';
            proj_8.BI_Oportunidad_asociada__c = o.Id; 
            proj_8.BI_Etapa_del_proyecto__c = null;
            proj_8.BI_O4_Project_Customer__c = 'uno';
        list_projects.add(proj_8);
        
		insert list_projects;
		

		 for(Milestone1_Project__c item :list_projects){
		 	ApexPages.StandardController sc = new ApexPages.StandardController(item);
		 	BI_ProjectStatus_CTRL controller = new BI_ProjectStatus_CTRL(sc);
		 	
		 		system.assert(controller.StatusColor1 != null && controller.StatusColor1 != '');
				system.assert(controller.StatusColor2 != null && controller.StatusColor2 != '');
				system.assert(controller.StatusColor3 != null && controller.StatusColor3 != '');
				system.assert(controller.StatusColor4 != null && controller.StatusColor4 != '');
				system.assert(controller.StatusColor5 != null && controller.StatusColor5 != '');
				system.assert(controller.StatusColor6 != null && controller.StatusColor6 != '');
				system.assert(controller.StatusColor7 != null && controller.StatusColor7 != '');
				
				
		 }
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 
    @isTest static void projectStatusTest2(){


        String assign = 'Assign Project Manager';
        String provision = 'Provisioning';
        String review = 'Review Project Manager Remarks';
        String closeMeeting = 'Project Closing Meeting';
        String accept = 'Accept Transition Package';

        Id rtId = [SELECT Id FROM RecordType WHERE SObjectType = 'Milestone1_Project__c' AND DeveloperName = 'BI_O4_Provisioning_Steady_State' LIMIT 1].Id;

        list<Milestone1_Project__c> list_projects = new list<Milestone1_Project__c>();

        Milestone1_Project__c proj_1 = new Milestone1_Project__c();
            proj_1.Name='Project 1'; 
            proj_1.BI_Etapa_del_proyecto__c = assign;
            proj_1.BI_O4_Project_Customer__c = 'uno';
            proj_1.RecordTypeId = rtId;
        list_projects.add(proj_1);
        
        Milestone1_Project__c proj_2 = new Milestone1_Project__c();
            proj_2.Name='Project 2'; 
            proj_2.BI_Etapa_del_proyecto__c = provision;
            proj_2.BI_O4_Project_Customer__c = 'uno';
            proj_2.RecordTypeId = rtId;
        list_projects.add(proj_2);
        
        Milestone1_Project__c proj_3 = new Milestone1_Project__c();
            proj_3.Name='Project 3'; 
            proj_3.BI_Etapa_del_proyecto__c = review;
            proj_3.BI_O4_Project_Customer__c = 'uno';
            proj_3.RecordTypeId = rtId;
        list_projects.add(proj_3);
        
       Milestone1_Project__c proj_4 = new Milestone1_Project__c();
            proj_4.Name='Project 4'; 
            proj_4.BI_Etapa_del_proyecto__c = closeMeeting;
            proj_4.BI_O4_Project_Customer__c = 'uno';
            proj_4.RecordTypeId = rtId;
        list_projects.add(proj_4);
        
        Milestone1_Project__c proj_5 = new Milestone1_Project__c();
            proj_5.Name='Project 5'; 
            proj_5.BI_Etapa_del_proyecto__c = accept;
            proj_5.BI_O4_Project_Customer__c = 'uno';
            proj_5.RecordTypeId = rtId;
        list_projects.add(proj_5);
        
        Milestone1_Project__c proj_6 = new Milestone1_Project__c();
            proj_6.Name='Project 6'; 
            proj_6.BI_Etapa_del_proyecto__c = null;
            proj_6.BI_O4_Project_Customer__c = 'uno';
            proj_6.RecordTypeId = rtId;
        list_projects.add(proj_6);

        insert list_projects;

        for(Milestone1_Project__c item :list_projects){
            ApexPages.StandardController sc = new ApexPages.StandardController(item);
            BI_ProjectStatus_CTRL controller = new BI_ProjectStatus_CTRL(sc);
            
                system.assert(controller.StatusColor1 != null && controller.StatusColor1 != '');
                system.assert(controller.StatusColor2 != null && controller.StatusColor2 != '');
                system.assert(controller.StatusColor3 != null && controller.StatusColor3 != '');
                system.assert(controller.StatusColor4 != null && controller.StatusColor4 != '');
                system.assert(controller.StatusColor5 != null && controller.StatusColor5 != '');
                system.assert(controller.StatusColor6 != null && controller.StatusColor6 != '');
                system.assert(controller.StatusColor7 != null && controller.StatusColor7 != '');
                
                
         }

    }

}