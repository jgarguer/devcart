@isTest 
private class BI_CurrencyHelper_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_CurrencyHelper class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    04/09/2014              Micah Burgos 	        Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void convertCurrency_TEST() {
        system.assertEquals(BI_CurrencyHelper.convertCurrency(null, null, null),null);
        
        system.assert(BI_CurrencyHelper.convertCurrency('EUR', 'USD', Double.valueOf(235324.23)) != null && BI_CurrencyHelper.convertCurrency('EUR', 'USD', Double.valueOf(235324.23)) != 235324.23); 
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}