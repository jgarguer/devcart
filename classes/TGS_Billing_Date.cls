/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author: Luis Miguel Alonso
    Company: DxD
    Description: Update Billing Date.
    
    History

    <Date>          <Author>                <Description>
    29/09/2015      Luis Miguel Alonso      Initial version
    17/03/2016      Fernando Arteaga        Set TGS_billing_end_date__c using TGS_RFS_date__c from the ci 
    12/07/2016      Juan Carlos Terrón      Filter the assignment of TGS_billing_start_date__c using TGS_billing_start_date__c as criteria.
    14/07/2016      Juan Carlos Terrón      Filter the assignment of TGS_billing_end_date__c depending on which type of are they made 
    19/07/2016      Juan Carlos Terrón      Added Filters to check the category of the catalog items on the Order Items meant to be processed.
    20/07/2016      Juan Carlos Terrón      Changed the mapping between Orders' CI and Asset's CI, changed the assignment of BSD and BED depending on CI's catalogItem category.
    30/08/2016      Juan Carlos Terrón      Changed the date assignment on BSD and BED, deleted .addDays(-1) sentence for non-GM products.
    29/09/2016      Juan Carlos Terrón      Added &&myCase.type == Constants.TYPE_CHANGE condition.Line 104. Issue 8 ( INC000000092201).
                                            Changed where condition in query. Line 49.
    14/11/2016      Juan Carlos Terrón      Added BED fullfillment checking on if condition. Line 128.
    21/11/2016      Juan Carlos Terrón      Version 2.0, replaces the older version.
    08/03/2017      Álvaro López            Fixes to assign all dates manually without datamap dates assignment.
    15/03/2017      Álvaro López            Added assigner_BillingDatesFromPrevOrd.
    12/02/2018      Álvaro López            Avoid null checking map_OI_RTAsset
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class TGS_Billing_Date {
    /*Catalog Items maps*/
    private static  Map<Id,NE__Catalog_Item__c> map_GM_CItems;
    private static  Map<Id,NE__Catalog_Item__c> map_mSip_CItems;  
    private static  Map<Id,NE__Catalog_Item__c> map_mWan_CItems;
    private static  Map<Id,NE__Catalog_Item__c> map_Other_CItems;
    /* Álvaro López 15/03/2017 */
    public static Boolean inFutureContext = false;

    static 
    {
        /*Catalog Item maps fullfillment*/
        map_GM_CItems = new Map<Id,NE__Catalog_Item__c>();
        map_mSip_CItems = new Map<Id,NE__Catalog_Item__c>();  
        map_mWan_CItems = new Map<Id,NE__Catalog_Item__c>();
        map_Other_CItems = new Map<Id,NE__Catalog_Item__c>();
        for (
                NE__Catalog_Item__c catalogItem :
                [
                    SELECT  Id, NE__Root_Catalog_Item__r.NE__Catalog_Category_Name__r.Name, NE__Catalog_Category_Name__r.Name
                    FROM    NE__Catalog_Item__c
                    WHERE   NE__Catalog_Id__r.Name = 'TGSOL Catalog' and NE__Catalog_Id__r.NE__Active__c = true //Álvaro López 28/03/2017 - Added filter
                ]
            )
        {
            /* Álvaro López 08/03/2017 - Added condition for parent CIs. Parent's NE__Root_Catalog_Item__c field is not filled*/
            if(catalogItem.NE__Root_Catalog_Item__r.NE__Catalog_Category_Name__r.Name == Constants.GLOBAL_MOBILITY_FOR_MNCS || catalogItem.NE__Catalog_Category_Name__r.Name == Constants.GLOBAL_MOBILITY_FOR_MNCS)
            {
                map_GM_CItems.put(catalogItem.Id, catalogItem);
            }
            else if(catalogItem.NE__Root_Catalog_Item__r.NE__Catalog_Category_Name__r.Name == 'Managed SIP Trunking Numbering' || catalogItem.NE__Root_Catalog_Item__r.NE__Catalog_Category_Name__r.Name == 'Managed SIP Trunking Site')
            {
                map_mSip_CItems.put(catalogItem.Id, catalogItem);
            }
            else if(catalogItem.NE__Root_Catalog_Item__r.NE__Catalog_Category_Name__r.Name == 'mWan')
            {
                map_mWan_CItems.put(catalogItem.Id, catalogItem);
            }
            else
            {
                map_Other_CItems.put(catalogItem.Id, catalogItem);
            }
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Juan Carlos Terrón
        Company:       Accenture
        Description:   Assign billing dates to order items assigned to an Order Management Case given.
        
        IN:            Case
        OUT:           Void
        
        History:   
        <Date>                  <Author>                <Change Description>
        21/11/2016              Juan Carlos Terrón      Version 2.0. 
        08/03/2017              Álvaro López            Fixes to assign all dates manually without datamap dates assignment       
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    public static void setCiBillingDate(Case myCase){
        System.debug('ENTRANDO EN TGS_Billing_Date.setCiBillingDate');
        /*Set to update the processed records.*/
        List<NE__OrderItem__c> list_OrderItems_ToUpdate = new List<NE__OrderItem__c>();

        /*Aux Simple Variables*/
        String serviceCategory = '';

        /*Maps to process*/
        Map<Id,NE__OrderItem__c> map_OI_RTOrder = new Map<Id,NE__OrderItem__c>();
        Map<Id,NE__OrderItem__c> map_OI_RTAsset = new Map<Id,NE__OrderItem__c>();
        List<NE__Order_Item_Attribute__c> list_Attributes = new List<NE__Order_Item_Attribute__c>();

        /*Maps to store Dates*/
        Map<String,Date> map_BSD = new Map<String,Date>();
        Map<String,Date> map_BED = new Map<String,Date>();

        /*Product Filtering Set/Maps*/
        Set<String> set_mSip = new Set<String>();
        Map<Id,NE__Catalog_Item__c> map_GM       = TGS_Billing_Date.map_GM_CItems;
        Map<Id,NE__Catalog_Item__c> map_mSip     = TGS_Billing_Date.map_mSip_CItems;
        Map<Id,NE__Catalog_Item__c> map_mWan     = TGS_Billing_Date.map_mWan_CItems;
        Map<Id,NE__Catalog_Item__c> map_Other    = TGS_Billing_Date.map_Other_CItems;

        /*Álvaro López 08/03/2017 - Added ciAsset map*/
        Map<Id, NE__OrderItem__c> map_cisAsset = new Map<Id, NE__OrderItem__c>();

        /*Maps to proccesing fullfillment*/
        /*Order Item maps.*/
        System.debug('Order del caso: ' + myCase.Order__c);
        for (
                NE__OrderItem__c OI :
                [
                    SELECT      Id, NE__Action__c, NE__ProdId__c, TGS_Billing_end_date__c, TGS_billing_start_date__c, TGS_Flag_BSD__c, TGS_Flag_BED__c,
                                TGS_CSUID1__c,TGS_CSUID2__c,TGS_CSUID1_Old__c,TGS_CSUID2_Old__c,
                                TGS_Change_MRC__c, TGS_Change_NRC__c, TGS_Attributes_modified__c, NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name,NE__AssetItemEnterpriseId__c,
                                TGS_Provisional_RFS_date__c, TGS_Flag_Update_msg__c, TGS_RFS_date__c,
                                NE__OrderId__r.RecordType.DeveloperName
                    FROM        NE__OrderItem__c
                    WHERE       NE__OrderId__c =:myCase.Order__c OR
                                NE__OrderId__c =:myCase.Asset__c
                    ORDER BY    CreatedDate ASC
                ]
            )
        {
            System.debug('Recordtype: ' + OI.NE__OrderId__r.RecordType.DeveloperName );
            System.debug('Billing start date: ' + OI.TGS_billing_start_date__c);
            if(OI.NE__OrderId__r.RecordType.DeveloperName == 'Order')
            {
                /*Order Items from the associated order.*/
                map_OI_RTOrder.put(OI.Id,OI);
            }
            else if(OI.NE__OrderId__r.RecordType.DeveloperName == 'Asset')
            {
                /*Order Items from the associated asset.*/
                map_OI_RTAsset.put(OI.Id,OI);
            }
        }
        System.debug('Mapa de ORDERITEMS ASSET '+map_OI_RTAsset);
        System.debug('Mapa de ORDERITEMS ORDER '+map_OI_RTOrder);
        /*Attributes maps.*/
        if(!map_OI_RTOrder.isEmpty() && !map_OI_RTAsset.isEmpty())
        {
            list_Attributes =   [
                                    SELECT      Id, TGS_CSUID__c, NE__Order_Item__c, Name, NE__Value__c, NE__Old_Value__c, TGS_Flag_Update_msg__c,  
                                                TGS_CSUID2_Old__c, TGS_CSUID2__c
                                    FROM        NE__Order_Item_Attribute__c
                                    WHERE       isDeleted = false AND
                                                NE__Order_Item__c IN :map_OI_RTOrder.keySet()
                                    ORDER BY    CreatedDate ASC
                                ];
        }
        /*-----End map fullfillment-----*/
        System.debug('Lista de atributos vacia: ' + list_Attributes.isEmpty());
        if(!list_Attributes.isEmpty())
        {
            /*---Records Processing-BSD & BED Assignment---*/
            for(NE__OrderItem__c OI : map_OI_RTOrder.values())
            {
                serviceCategory = assigner_ServiceCategory(OI);
                TGS_Billing_Date.assigner_BillingDates(myCase,serviceCategory,OI,list_OrderItems_ToUpdate,map_BSD,map_BED,map_OI_RTAsset);
            }
            for(NE__OrderItem__c OI : map_OI_RTAsset.values())
            {
                System.debug('Entro asignación de fechas en el asset.');
                /*BSD Assignment*/
                /* Álvaro López 08/03/2017 - Comentada la comprobación del TGS_billing_start_date__c == null para evitar
                    que no se actualicen las fechas cuando un usuario ha introducido una manualmente*/
                //if(OI.TGS_billing_start_date__c == null)
                //{
                    if(map_BSD.containsKey(OI.NE__AssetItemEnterpriseId__c))
                    {
                        System.debug('Asigno BSD');
                        OI.TGS_billing_start_date__c = map_BSD.get(OI.NE__AssetItemEnterpriseId__c);
                        //list_OrderItems_ToUpdate.add(OI);
                        map_cisAsset.put(OI.Id, OI);
                    }
                //}
                /*BED Assignment*/
                /* Álvaro López 08/03/2017 - Comentada la comprobación del TGS_billing_start_date__c == null para evitar
                    que no se actualicen las fechas cuando un usuario ha introducido una manualmente*/
                //if(OI.TGS_Billing_end_date__c == null)
                //{
                    if(map_BED.containsKey(OI.NE__AssetItemEnterpriseId__c))
                    {
                        System.debug('Asigno BED');
                        OI.TGS_Billing_end_date__c = map_BED.get(OI.NE__AssetItemEnterpriseId__c);
                        //list_OrderItems_ToUpdate.add(OI);
                        map_cisAsset.put(OI.Id, OI);
                    }
                //}
            }
            /*Álvaro López 08/03/2017 - Added asset cis to the list to update*/
            for(NE__OrderItem__c OI : map_cisAsset.values())
            {
                list_OrderItems_ToUpdate.add(OI);
            }
            /*---End BSD & BED Assignment---*/
            System.debug('Lista para actualizar : '+list_OrderItems_ToUpdate);
            if(!list_OrderItems_ToUpdate.isEmpty())
            {
                /*Order Items Update after processing them*/
                update list_OrderItems_ToUpdate;
            }
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Álvaro López
        Company:       Accenture
        Description:   Assign billing dates to order items assigned to an Order Management Case when the it is created.
        
        IN:            Case
        OUT:           Void
        
        History:   
        <Date>                  <Author>                <Change Description>
        08/03/2017              Álvaro López            Version 1.0.        
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    @future /* Álvaro López 15/03/2017 - Execute method in future context*/
    public static void setCiBillingDateFromPrevOrder(String myCase_toProcess){
        /* Álvaro López 15/03/2017 - Deserialize the entry case*/
        Case myCase = (Case) JSON.deserialize(myCase_toProcess, Case.class);
        System.debug('ENTRANDO EN TGS_Billing_Date.setCiBillingDateFromPrevOrder');
        //throw new BI_Exception('-AOAOAOO');
        /*Set to update the processed records.*/
        List<NE__OrderItem__c> list_OrderItems_ToUpdate = new List<NE__OrderItem__c>();

        /*Aux Simple Variables*/
        String serviceCategory = '';

        /*Maps to process*/
        Map<Id,NE__OrderItem__c> map_OI_RTOrder = new Map<Id,NE__OrderItem__c>();
        Map<Id,NE__OrderItem__c> map_OI_RTAsset = new Map<Id,NE__OrderItem__c>();
        List<NE__Order_Item_Attribute__c> list_Attributes = new List<NE__Order_Item_Attribute__c>();

        /*Maps to store Dates*/
        Map<String,Date> map_BSD = new Map<String,Date>();
        Map<String,Date> map_BED = new Map<String,Date>();

        /*Product Filtering Set/Maps*/
        Set<String> set_mSip = new Set<String>();
        Map<Id,NE__Catalog_Item__c> map_GM       = TGS_Billing_Date.map_GM_CItems;
        Map<Id,NE__Catalog_Item__c> map_mSip     = TGS_Billing_Date.map_mSip_CItems;
        Map<Id,NE__Catalog_Item__c> map_mWan     = TGS_Billing_Date.map_mWan_CItems;
        Map<Id,NE__Catalog_Item__c> map_Other    = TGS_Billing_Date.map_Other_CItems;


        if(myCase.Type != Constants.TYPE_NEW){
            /*Maps to proccesing fullfillment*/
            /*Order Item maps.*/
            System.debug('Order del caso: ' + myCase.Order__c);
            for (
                    NE__OrderItem__c OI :
                    [
                        SELECT      Id, NE__Action__c, NE__ProdId__c, TGS_Billing_end_date__c, TGS_billing_start_date__c, TGS_Flag_BSD__c, TGS_Flag_BED__c,
                                    TGS_CSUID1__c,TGS_CSUID2__c,TGS_CSUID1_Old__c,TGS_CSUID2_Old__c,
                                    TGS_Change_MRC__c, TGS_Change_NRC__c, TGS_Attributes_modified__c, NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name,NE__AssetItemEnterpriseId__c,
                                    TGS_Provisional_RFS_date__c, TGS_Flag_Update_msg__c, TGS_RFS_date__c,
                                    NE__OrderId__r.RecordType.DeveloperName
                        FROM        NE__OrderItem__c
                        WHERE       NE__OrderId__c =:myCase.Order__c
                        ORDER BY    CreatedDate ASC
                    ]
                )
            {
                System.debug('Recordtype: ' + OI.NE__OrderId__r.RecordType.DeveloperName );
                System.debug('Billing start date: ' + OI.TGS_billing_start_date__c);
                
                /*Order Items from the associated order.*/
                map_OI_RTOrder.put(OI.Id,OI);
                
            }

            /*Attributes maps.*/
            System.debug('MAP SIZE: '+map_OI_RTOrder.size());

            Set<Id> set_CIAsset = new Set<Id>();
            if(!map_OI_RTOrder.isEmpty())
            {
                for(NE__OrderItem__c ci : map_OI_RTOrder.values()){
                    set_CIAsset.add(ci.NE__AssetItemEnterpriseId__c);
                }

                for(
                        NE__OrderItem__c OI : 
                        [
                            SELECT      Id, NE__Action__c, NE__ProdId__c, TGS_Billing_end_date__c, TGS_billing_start_date__c, TGS_Flag_BSD__c, TGS_Flag_BED__c,
                                        TGS_CSUID1__c,TGS_CSUID2__c,TGS_CSUID1_Old__c,TGS_CSUID2_Old__c,
                                        TGS_Change_MRC__c, TGS_Change_NRC__c, TGS_Attributes_modified__c, NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name,NE__AssetItemEnterpriseId__c,
                                        TGS_Provisional_RFS_date__c, TGS_Flag_Update_msg__c, TGS_RFS_date__c,
                                        NE__OrderId__r.RecordType.DeveloperName
                            FROM        NE__OrderItem__c
                            WHERE       Id IN :set_CIAsset
                        ]
                    )
                {   
                    map_OI_RTAsset.put(OI.Id, OI);
                }
                System.debug('MAP SIZE: '+map_OI_RTAsset.size());
                /*---Records Processing-BSD & BED Assignment---*/
                if(!map_OI_RTAsset.isEmpty()){
                    for(NE__OrderItem__c OI : map_OI_RTOrder.values())
                    {
                        /* Álvaro López 15/03/2017 */
                        System.debug('PREV BSD: '+ OI.TGS_billing_start_date__c);
                        serviceCategory = assigner_ServiceCategory(OI);
                        TGS_Billing_Date.assigner_BillingDatesFromPrevOrd(myCase,serviceCategory,OI,list_OrderItems_ToUpdate,map_BSD,map_BED,map_OI_RTAsset);
                        System.debug('BSD: ' + OI.TGS_billing_start_date__c);
                    }
                }
                
                /*---End BSD & BED Assignment---*/
                System.debug('Lista para actualizar : '+list_OrderItems_ToUpdate);
                if(!list_OrderItems_ToUpdate.isEmpty())
                {
                    /* Álvaro López 15/03/2017 - Turning future context flag on*/
                    TGS_Billing_Date.inFutureContext = true;
                    /*Order Items Update after processing them*/
                    update list_OrderItems_ToUpdate;
                }
            }
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Juan Carlos Terrón
        Company:       Accenture
        Description:   Method to assign BSD and BED depending on Service Category
        
        IN:            Case, NE__OrderItem__c, String(OI Service)
        OUT:           Void
        
        History:   
        <Date>                  <Author>                <Change Description>
        21/11/2016              Juan Carlos Terrón      Initial Version.
        08/03/2017              Álvaro López            Added map_OI_RTAsset as a parameter and fix to assign BSD and BED.
        12/02/2018              Álvaro López            Avoid null checking map_OI_RTAsset       
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    public static void assigner_BillingDates(
        Case                        myCase, 
        String                      serviceCategory,
        NE__OrderItem__c            orderItem,
        List<NE__OrderItem__c>      list_OrderItems_ToUpdate,
        Map<String,Date>            map_BSD,
        Map<String,Date>            map_BED,
        Map<Id,NE__OrderItem__c>    map_OI_RTAsset
    ){
        /*Order Item Variables*/
        String ACTION = '';
        ACTION = orderItem.NE__Action__c;
        Date BSD = orderItem.TGS_billing_start_date__c;
        Date BED = orderItem.TGS_billing_end_date__c;

        /*START - BSD & BED Assignment*/
        if(myCase.Type == Constants.TYPE_NEW)
        {
            System.debug('BSD: ' + BSD);
            if(BSD==null)
            {
                /*
                -For Registration cases the BSD assignment is the same, BED assignment is not needed.
                -A call to aux method checker_CaseDateFields is made, then this Date is assigned to the order item
                and the BSD map is filled, also the set with the records up to process.
                 */
                BSD = TGS_Billing_Date.checker_CaseDateFields(myCase,orderItem);
                orderItem.TGS_billing_start_date__c =  BSD;
                map_BSD.put(orderItem.Id,BSD);
                list_OrderItems_ToUpdate.add(orderItem);
            }
            else
            {
               map_BSD.put(orderItem.Id,orderItem.TGS_billing_start_date__c); 
            }
        }
        else if(myCase.Type == Constants.TYPE_CHANGE)
        {
            /*BSD Assignment*/
            if(ACTION == Constants.CI_ACTION_ADD)
            {
                /*Adding a new order item takes the same casuistry as registration cases.*/
                if(BSD==null)
                {
                    BSD =  TGS_Billing_Date.checker_CaseDateFields(myCase,orderItem);
                    orderItem.TGS_billing_start_date__c =  BSD;
                    map_BSD.put(orderItem.Id,BSD);
                    list_OrderItems_ToUpdate.add(orderItem);
                }
                else
                {
                   map_BSD.put(orderItem.Id,orderItem.TGS_billing_start_date__c); 
                }
            }
            else if(ACTION == Constants.CI_ACTION_CHANGE)
            {
                if(BSD==null)
                {
                    if  (
                            (
                                serviceCategory == 'mSip' 
                                && ((orderItem.TGS_CSUID1__c!=orderItem.TGS_CSUID1_Old__c) || (orderItem.TGS_CSUID2__c!=orderItem.TGS_CSUID2_Old__c))
                            )
                            || serviceCategory == 'mWan' 
                            || serviceCategory == 'Other'
                        )
                    {
                        /*MSIP services have to check if their CSUID fields have changed in order assign or not a new BSD*/
                            BSD =  TGS_Billing_Date.checker_CaseDateFields(myCase,orderItem);
                            orderItem.TGS_billing_start_date__c =  BSD;
                            map_BSD.put(orderItem.Id,BSD);
                            list_OrderItems_ToUpdate.add(orderItem);
                    }
                    else
                    {
                        if(map_OI_RTAsset.containsKey(orderItem.NE__AssetItemEnterpriseId__c)){
                            BSD = map_OI_RTAsset.get(orderItem.NE__AssetItemEnterpriseId__c).TGS_billing_start_date__c;
                        }
                        else{
                           BSD = Date.today(); 
                        }
                        orderItem.TGS_billing_start_date__c =  BSD;
                        map_BSD.put(orderItem.Id,BSD);
                        list_OrderItems_ToUpdate.add(orderItem);
                    }
                }
            }
            else if(ACTION == Constants.CI_ACTION_REMOVE)
            {
                /*Regardless of the Service Category a removed order item has to get it's BED assigned, BSD takes no change at all.*/
                if(BED == null)
                {
                    if(serviceCategory == 'GM')
                    {
                        if(map_OI_RTAsset.containsKey(orderItem.NE__AssetItemEnterpriseId__c)){
                            BSD = map_OI_RTAsset.get(orderItem.NE__AssetItemEnterpriseId__c).TGS_billing_start_date__c;
                        }
                        else{
                           BSD = Date.today(); 
                        }
                        orderItem.TGS_billing_start_date__c =  BSD;
                        map_BSD.put(orderItem.Id,BSD);

                        /*For GM Services the BED takes 1 day less for the assignment.*/
                        BED =  TGS_Billing_Date.checker_CaseDateFields(myCase,orderItem).addDays(-1);
                        orderItem.TGS_billing_end_date__c = BED;
                        map_BED.put(orderItem.Id, BED);
                        list_OrderItems_ToUpdate.add(orderItem);
                    }
                    else
                    {
                        if(map_OI_RTAsset.containsKey(orderItem.NE__AssetItemEnterpriseId__c)){
                            BSD = map_OI_RTAsset.get(orderItem.NE__AssetItemEnterpriseId__c).TGS_billing_start_date__c;
                        }
                        else{
                           BSD = Date.today(); 
                        }
                        orderItem.TGS_billing_start_date__c =  BSD;
                        map_BSD.put(orderItem.Id,BSD);

                        /*For other Services we dont take 1 day less, the BED map is filled and also the set with records up to process.*/
                        BED =  TGS_Billing_Date.checker_CaseDateFields(myCase,orderItem);
                        orderItem.TGS_billing_end_date__c = BED;
                        map_BED.put(orderItem.Id, BED);
                        list_OrderItems_ToUpdate.add(orderItem);  
                    }
                }
                else
                {
                    if(map_OI_RTAsset.containsKey(orderItem.NE__AssetItemEnterpriseId__c)){
                            BSD = map_OI_RTAsset.get(orderItem.NE__AssetItemEnterpriseId__c).TGS_billing_start_date__c;
                        }
                        else{
                           BSD = Date.today(); 
                        }
                    orderItem.TGS_billing_start_date__c =  BSD;
                    map_BSD.put(orderItem.Id,BSD);
                    list_OrderItems_ToUpdate.add(orderItem);

                    map_BED.put(orderItem.Id, orderItem.TGS_billing_end_date__c);
                }
            }
            else if(ACTION == Constants.CI_ACTION_NONE)
            {
                if(BSD==null)
                {
                    if(serviceCategory != 'GM')
                    {
                        BSD =  TGS_Billing_Date.checker_CaseDateFields(myCase,orderItem);
                        orderItem.TGS_billing_start_date__c =  BSD;
                        map_BSD.put(orderItem.Id,BSD);
                        list_OrderItems_ToUpdate.add(orderItem);
                    }
                    else
                    {
                        if(map_OI_RTAsset.containsKey(orderItem.NE__AssetItemEnterpriseId__c)){
                            BSD = map_OI_RTAsset.get(orderItem.NE__AssetItemEnterpriseId__c).TGS_billing_start_date__c;
                        }
                        else{
                           BSD = Date.today(); 
                        }
                        orderItem.TGS_billing_start_date__c =  BSD;
                        map_BSD.put(orderItem.Id,BSD);
                        list_OrderItems_ToUpdate.add(orderItem);
                    }
                    
                }
                else
                {
                    map_BSD.put(orderItem.Id,orderItem.TGS_billing_start_date__c);
                }
            }
        }
        else if(myCase.Type == Constants.TYPE_DISCONNECT)
        {
            /*For the Disconnect Cases the casuistry is the same as deleted order items on modification orders/cases.*/
            if(BED == null)
            {
                if(serviceCategory == 'GM')
                {
                    if(map_OI_RTAsset.containsKey(orderItem.NE__AssetItemEnterpriseId__c)){
                        BSD = map_OI_RTAsset.get(orderItem.NE__AssetItemEnterpriseId__c).TGS_billing_start_date__c;
                    }
                    else{
                       BSD = Date.today(); 
                    }
                    orderItem.TGS_billing_start_date__c =  BSD;
                    map_BSD.put(orderItem.Id,BSD);

                    BED =  TGS_Billing_Date.checker_CaseDateFields(myCase,orderItem).addDays(-1);
                    orderItem.TGS_billing_end_date__c = BED;
                    map_BED.put(orderItem.Id, BED);
                    list_OrderItems_ToUpdate.add(orderItem);    
                }
                else
                {
                    if(map_OI_RTAsset.containsKey(orderItem.NE__AssetItemEnterpriseId__c)){
                        BSD = map_OI_RTAsset.get(orderItem.NE__AssetItemEnterpriseId__c).TGS_billing_start_date__c;
                    }
                    else{
                       BSD = Date.today(); 
                    }
                    orderItem.TGS_billing_start_date__c =  BSD;
                    map_BSD.put(orderItem.Id,BSD);

                    BED =  TGS_Billing_Date.checker_CaseDateFields(myCase,orderItem);
                    orderItem.TGS_billing_end_date__c = BED;
                    map_BED.put(orderItem.Id, BED);
                    list_OrderItems_ToUpdate.add(orderItem);    
                }
                
            }
            else
            {
                if(map_OI_RTAsset.containsKey(orderItem.NE__AssetItemEnterpriseId__c)){
                    BSD = map_OI_RTAsset.get(orderItem.NE__AssetItemEnterpriseId__c).TGS_billing_start_date__c;
                }
                else{
                   BSD = Date.today(); 
                }
                orderItem.TGS_billing_start_date__c =  BSD;
                map_BSD.put(orderItem.Id,BSD);
                list_OrderItems_ToUpdate.add(orderItem);
                
                map_BED.put(orderItem.Id, orderItem.TGS_billing_end_date__c);
            }
        }
        /*ENDS - BSD & BED Assignment*/
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Álvaro López
        Company:       Accenture
        Description:   Method to assign BSD and BED depending on Service Category
        
        IN:            Case, NE__OrderItem__c, String(OI Service)
        OUT:           Void
        
        History:   
        <Date>                  <Author>                <Change Description>
        15/03/2017              Álvaro López            Initial Version. 
        12/02/2018              Álvaro López            Avoid null checking map_OI_RTAsset      
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    public static void assigner_BillingDatesFromPrevOrd(
        Case                        myCase, 
        String                      serviceCategory,
        NE__OrderItem__c            orderItem,
        List<NE__OrderItem__c>      list_OrderItems_ToUpdate,
        Map<String,Date>            map_BSD,
        Map<String,Date>            map_BED,
        Map<Id,NE__OrderItem__c>    map_OI_RTAsset
    ){
        /*Order Item Variables*/
        String ACTION = '';
        ACTION = orderItem.NE__Action__c;
        Date BSD = orderItem.TGS_billing_start_date__c;
        Date BED = orderItem.TGS_billing_end_date__c;

        System.debug('Case Type y Action: '+myCase.Type+' - '+ACTION+' - '+orderItem.Id);
        /*START - BSD & BED Assignment*/
        if(myCase.Type == Constants.TYPE_CHANGE)
        {
            if(ACTION == Constants.CI_ACTION_CHANGE)
            {
                if(serviceCategory == 'GM')
                {
                    if(map_OI_RTAsset.containsKey(orderItem.NE__AssetItemEnterpriseId__c)){
                        BSD = map_OI_RTAsset.get(orderItem.NE__AssetItemEnterpriseId__c).TGS_billing_start_date__c;
                    }
                    else{
                       BSD = Date.today(); 
                    }
                    orderItem.TGS_billing_start_date__c =  BSD;
                    map_BSD.put(orderItem.Id,BSD);

                    list_OrderItems_ToUpdate.add(orderItem);
                }
            }
            else if(ACTION == Constants.CI_ACTION_REMOVE)
            {
              
                if(map_OI_RTAsset.containsKey(orderItem.NE__AssetItemEnterpriseId__c)){
                    BSD = map_OI_RTAsset.get(orderItem.NE__AssetItemEnterpriseId__c).TGS_billing_start_date__c;
                }
                else{
                   BSD = Date.today(); 
                }
                System.debug('BSD antes de asignación: '+BSD);
                orderItem.TGS_billing_start_date__c =  BSD;
                map_BSD.put(orderItem.Id,BSD);

                list_OrderItems_ToUpdate.add(orderItem);
               
            }
            else if(ACTION == Constants.CI_ACTION_NONE)
            {
                if(serviceCategory == 'GM')
                {
                    if(map_OI_RTAsset.containsKey(orderItem.NE__AssetItemEnterpriseId__c)){
                        BSD = map_OI_RTAsset.get(orderItem.NE__AssetItemEnterpriseId__c).TGS_billing_start_date__c;
                    }
                    else{
                       BSD = Date.today(); 
                    }
                    orderItem.TGS_billing_start_date__c =  BSD;
                    map_BSD.put(orderItem.Id,BSD);
                    
                    list_OrderItems_ToUpdate.add(orderItem);
                }
            }
        }
        else if(myCase.Type == Constants.TYPE_DISCONNECT)
        {
            if(map_OI_RTAsset.containsKey(orderItem.NE__AssetItemEnterpriseId__c)){
                BSD = map_OI_RTAsset.get(orderItem.NE__AssetItemEnterpriseId__c).TGS_billing_start_date__c;
            }
            else{
               BSD = Date.today(); 
            }
            System.debug('BSD antes de asignación: '+BSD);
            orderItem.TGS_billing_start_date__c =  BSD;
            map_BSD.put(orderItem.Id,BSD);

            list_OrderItems_ToUpdate.add(orderItem);  
        }
        /*ENDS - BSD & BED Assignment*/
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Juan Carlos Terrón
        Company:       Accenture
        Description:   Return a String with a generic name for Service Category for a given Order Item
        
        IN:            NE__OrderItem__c
        OUT:           String
        
        History:   
        <Date>                  <Author>                <Change Description>
        21/11/2016              Juan Carlos Terrón      Initial Version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    public static String assigner_ServiceCategory(NE__OrderItem__c orderItem){
        String category = '';
        if(TGS_Billing_Date.map_GM_CItems.containskey(orderItem.NE__CatalogItem__c))
        {
            category = 'GM';
        }
        else if(TGS_Billing_Date.map_mSip_CItems.containskey(orderItem.NE__CatalogItem__c))
        {
            category = 'mSip';
        }
        else if(TGS_Billing_Date.map_mWan_CItems.containskey(orderItem.NE__CatalogItem__c))
        {
            category = 'mWan';
        }
        else if(TGS_Billing_Date.map_Other_CItems.containskey(orderItem.NE__CatalogItem__c))
        {
            category = 'Other';
        }
        return category;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Juan Carlos Terrón
        Company:       Accenture
        Description:   Method to assign BSD and BED depending on field fullfillment of associated Case, used to calrify the Code
        
        IN:            Case,NE__OrderItem__c
        OUT:           Date
        
        History:   
        <Date>                  <Author>                <Change Description>        
        21/11/2016              Juan Carlos Terrón      Initial Version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    
    public static Date checker_CaseDateFields(Case myCase, NE__OrderItem__c orderItem){
        if(myCase.TGS_RFB_date__c!=null)
        {
            System.debug('Entro en 1');
            /*Case RFS Date isn't null*/
            return myCase.TGS_RFB_date__c;
        }
        else if(myCase.TGS_RFS_date__c!=null)
        {
            System.debug('Entro en 2');
            /*Case Provisional RFS Date isn't null but RFS Date isn't filled*/
            return myCase.TGS_RFS_date__c;
        }
        else
        {
            System.debug('Entro en 3');
            /*Both Provisional and RFS date have null values*/
            return Date.today();
        }
    }
}