@isTest
private class TestNEInsertBillingProfileExtension {

    static testMethod void NEInsertBillingProfileExtension_Test() 
    {
        RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name LIKE '%Holding%' LIMIT 1];

        Account acc = new Account(Name='TestAccount', RecordTypeId=rt.Id);
        insert acc;
        
        NE__Billing_Profile__c billProf1 = new NE__Billing_Profile__c(Name='Test', NE__Account__c=acc.Id, NE__Payment__c='Credit Card', NE__First_Name__c='BillingName', NE__Last_Name__c='BillingLastName', NE__Card_Type__c='MasterCard', NE__Card_Number__c='123456789', NE__Expiration__c='12', NE__Expiration_Year__c='23', NE__CSC__c='121', NE__Security_Code__c='123');
		NE__Billing_Profile__c billProf2 = new NE__Billing_Profile__c(Name='Test', NE__Account__c=acc.Id, NE__Payment__c='Automatic Debt', NE__First_Name__c='BillingName', NE__Last_Name__c='BillingLastName', NE__Iban__c='123456789');
		NE__Billing_Profile__c billProf3 = new NE__Billing_Profile__c(Name='Test', NE__Account__c=acc.Id, NE__Payment__c='Statement');
        insert billProf1;
        insert billProf2;
        insert billProf3;
        
		NE__Billing_Profile__c billProfErr1 = new NE__Billing_Profile__c(Name='Test', NE__Account__c=acc.Id, NE__Payment__c='Credit Card', NE__Card_Type__c='American Express', NE__Security_Code__c='asd', NE__Expiration__c='33', NE__Expiration_year__c='a', NE__Card_Number__c='asdf');
		NE__Billing_Profile__c billProfErr2 = new NE__Billing_Profile__c(Name='Test', NE__Account__c=acc.Id, NE__Payment__c='Automatic Debt', NE__Iban__c='12');
        insert billProfErr1;
        insert billProfErr2;
		
		try
        {
	        ApexPages.StandardController contr = new ApexPages.StandardController(billProf1);
	        NEInsertBillingProfileExtension ibpExt = new NEInsertBillingProfileExtension(contr);
	        ibpExt.getLabel();
	        ibpExt.getItems();
            ibpExt.getPayment();
	        ibpExt.saveNew();
	        
            contr = new ApexPages.StandardController(billProf2);
            ibpExt = new NEInsertBillingProfileExtension(contr);
            ibpExt.getLabel();
	        ibpExt.getItems();
            ibpExt.getPayment();
	        ibpExt.saveNew();
            
            contr = new ApexPages.StandardController(billProf3);
            ibpExt = new NEInsertBillingProfileExtension(contr);
            ibpExt.getLabel();
            ibpExt.getItems();
            ibpExt.getPayment();
            ibpExt.save();
            
            contr = new ApexPages.StandardController(billProfErr1);
            ibpExt = new NEInsertBillingProfileExtension(contr);
            ibpExt.save();
            
			contr = new ApexPages.StandardController(billProfErr2);
            ibpExt = new NEInsertBillingProfileExtension(contr);
            ibpExt.save();
		}
		catch(Exception e){
			System.debug(e);
		}
    }
}