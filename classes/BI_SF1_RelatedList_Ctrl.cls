/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Pardo Corral
    Company:       Accenture
    Description:   Return a list of objects filtered by the given params

    IN:             String filter - fields separated by ';' used in the where clause
                    String objectType - ApiName of the object
                    String textValue - Value entered by the user in the search bar
                    String descriptionField - fields to be displayed as a description
                    
    OUT:            List<Object>  - returns a list with the records
                    null - when no records are found for that filter

    History: 
    <Date>                  <Author>                <Change Description>
    30/11/2017              Antonio Pardo corral    Initial Version 
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_SF1_RelatedList_Ctrl {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Antonio Pardo Corral
        Company:       Accenture
        Description:   Return a list of objects filtered by the given params

        IN:             Id recordId - id of the record
                        String jsonRelatedList - JSON of the object BI_SF1_Related_list_configuration__mdt 
                        String objName - ApiName of the object
                        
        OUT:            List<Map<String, object>>  - returns the info needed for the related lists

        History: 
        <Date>                  <Author>                <Change Description>
        30/11/2017              Antonio Pardo corral    Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static List<Map<String, object>> getRecords(Id recordId, String jsonRelatedList, String objName){
        List<Map<String, object>> lst_return = new List<Map<String, object>>();

        List<BI_SF1_Related_list_configuration__mdt> lst_config = (List<BI_SF1_Related_list_configuration__mdt>)System.JSON.deserializeStrict(jsonRelatedList, List<BI_SF1_Related_list_configuration__mdt>.class);

        Map<String, BI_SF1_Related_list_configuration__mdt> map_config = new Map<String, BI_SF1_Related_list_configuration__mdt>();

        String dynamicQuery = 'SELECT Id,';
        //Doing subQuerys to retrieve the relatedLists
        for(BI_SF1_Related_list_configuration__mdt config : lst_config){
            map_config.put(config.MasterLabel, config);
           dynamicQuery += ' ( SELECT Id FROM ' + config.MasterLabel + ' LIMIT 101) ,';

        }
        dynamicQuery = dynamicQuery.removeEnd(',') + 'FROM ' + objName + ' WHERE Id = \'' +  recordId + '\' LIMIT 1';
        
        System.debug(dynamicQuery);
        System.debug(Database.query(dynamicQuery));

        Sobject record = Database.query(dynamicQuery);

        for(String related : map_config.keySet()){

            Map<String, Object> map_relatedObj = new Map<String, Object>();

            System.debug(record.getSObjects(related));
            
            List<Sobject> relatedObj = record.getSObjects(related);
            
            map_relatedObj.put('relatedObj', relatedObj!=null ? relatedObj : new List<Sobject>());
            map_relatedObj.put('config', map_config.get(related));

            lst_return.add(map_relatedObj);
            
        }

        return lst_return;
    }
}