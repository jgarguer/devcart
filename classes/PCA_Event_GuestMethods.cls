public class PCA_Event_GuestMethods {
         /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Methods executed by Event__c Triggers 
    
    History:
    
    <Date>                    <Author>               <Description>
    01/02/2014              Jorge Longarela         Initial Version 
    21/07/2014              Ana Escrich             create methods
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich Acero
        Company:       Salesforce.com
        Description:   Method that returns a list of guest users  given a string formed by guest users separated by ;
        
        History: 
        
        <Date>                  <Author>                <Change Description>
        21/07/2014              Ana Escrich Acero           Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static List<string> retrieveGuestList(string guestList){
        try{
            List<string> result = new List<string>();
            string [] gList = guestList.split(';');
            for (string guest: gList){
                result.add(guest);
            }
            return result;
        }catch(exception Exc){
            BI_LogHelper.generate_BILog('PCA_Event_GuestMethods.retrieveGuestList', 'Portal Platino', Exc, 'Class');
            return null;
        }
            
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich Acero
        Company:       Salesforce.com
        Description:   Calculates duration of the event
        
        History: 
        
        <Date>                  <Author>                <Change Description>
        21/07/2014              Ana Escrich Acero           Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static decimal calculateDuration(DateTime init, DateTime finish){
        try{
            decimal iHour = init.hour();
            decimal fHour = finish.hour();
            if(init.minute()==30){
                iHour += 0.5;
            }
            if(finish.minute()==30){
                fHour += 0.5;
            }
            return (fHour-iHour)*60;
        }catch(exception Exc){
            BI_LogHelper.generate_BILog('PCA_Event_GuestMethods.calculateDuration', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich Acero
        Company:       Salesforce.com
        Description:   Modify standard event with the new values
        
        History: 
        
        <Date>                  <Author>                <Change Description>
        21/07/2014              Ana Escrich Acero           Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static Event modifyEvent(Event event, Event__c customEvent){
        try{
            event.Description = customEvent.Description__c;
            event.Subject = customEvent.Subject__c;
            event.StartDateTime = customEvent.StartDateTime__c;
            event.EndDateTime = customEvent.EndDateTime__c;
            event.ActivityDateTime = customEvent.StartDateTime__c;
            event.DurationInMinutes = integer.valueof(calculateDuration(customEvent.StartDateTime__c, customEvent.EndDateTime__c));
            return event;
        }catch(exception Exc){
            BI_LogHelper.generate_BILog('PCA_Event_GuestMethods.modifyEvent', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich Acero
        Company:       Salesforce.com
        Description:   Delete event
        
        History: 
        
        <Date>                  <Author>                <Change Description>
        21/07/2014              Ana Escrich Acero           Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void deleteEvent(List <Event__c> olds){
        try{
            Event__c customEvent = olds[0];
            List<Event> event = [select Id, Description, Subject, StartDateTime, EndDateTime, DurationInMinutes, ActivityDateTime from Event where Id = :customEvent.StandardEventId__c or BI_ParentId__c = :customEvent.StandardEventId__c];
            List<EventRelation> evRelations = [select Id, EventId, RelationId from EventRelation where EventId= :customEvent.StandardEventId__c];
            delete evRelations;
            delete event;
        }catch(exception Exc){
            BI_LogHelper.generate_BILog('PCA_Event_GuestMethods.deleteEvent', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich Acero
        Company:       Salesforce.com
        Description:   Updates data of the event
        
        History: 
        
        <Date>                  <Author>                <Change Description>
        21/07/2014              Ana Escrich Acero       Initial Version     
        07/12/2015              Guillermo Muñoz         Prevent the email send in Test class 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static void updateEvent(Event__c customEvent){
        try{
            List<Event> eventsToUpdate = new List<Event>();
            List<Event> event = [select Id, Description, Subject, StartDateTime, EndDateTime, DurationInMinutes, ActivityDateTime, OwnerId, Owner.Email from Event where Id = :customEvent.StandardEventId__c];
            if(!event.isEmpty()){
                
                //Compare the events to see differences
                
                /*system.debug('EVENT[0]: '+event[0]);
                event[0] = modifyEvent(event[0], customEvent);
                system.debug('EVENT[0]: '+event[0]);
                eventsToUpdate.add(event[0]);*/
            
            
                //customEvent.GuestList__c
                List<String> gList = retrieveGuestList(customEvent.GuestList__c);
                List<String> contacts = new List<String>();
                List<String> users = new List<String>();
                Map<String, String> mapUsers = new Map<String, String>(); 
                Map<String, String> mapContacts = new Map<String, String>(); 
                for(string eRelation: gList){
                    string rel = eRelation+'';
                    rel = rel.substring(0,3);
                    if(rel=='003') {
                        contacts.add(eRelation);
                        mapContacts.put(eRelation, eRelation);
                    }
                    else if(rel=='005'){
                        users.add(eRelation);
                        mapUsers.put(eRelation, eRelation);
                    }
                }
                //delete, update and insert related events
                //event = new List<Event>(); 
                //event = [select Id, Description, Subject, StartDateTime, EndDateTime, DurationInMinutes, ActivityDateTime, OwnerId from Event where BI_ParentId__c = :customEvent.StandardEventId__c];
                Map<String, String> mapNotDelete = new Map<String, String>();
                List<Event> eventsToDelete = new List<Event>();
                String[] toaddressDelete = new String[]{};
                String[] toaddressUpdate = new String[]{};
                String[] toaddressInsert = new String[]{};
                String subjectEvent = event[0].Subject;
                String activityDate = ''+event[0].ActivityDateTime;
                for(Event item: event){
                    system.debug(mapUsers.containsKey(item.OwnerId));
                    system.debug('**mapUsers**'+mapUsers);
                    //if(mapUsers.containsKey(item.OwnerId)){
                        mapNotDelete.put(item.OwnerId, item.OwnerId);
                        item = modifyEvent(item, customEvent);
                        eventsToUpdate.add(item);
                        toaddressUpdate.add(item.Owner.Email);
                    /*}
                    else{
                        eventsToDelete.add(item);
                        toaddressDelete.add(item.Owner.Email);
                    }*/
                }
                delete eventsToDelete;
                
                for(string item: users){
                    if(!mapNotDelete.containsKey(item)){
                        Event newEvent = new Event();
                        newEvent = modifyEvent(newEvent, customEvent);
                        newEvent.OwnerId = item;
                        newEvent.BI_ParentId__c = customEvent.StandardEventId__c;
                        eventsToUpdate.add(newEvent);
                    }
                }
                upsert eventsToUpdate;
                
                List<User> userList = [select Id, Email from User where Id in :users];
                for(User item : userList){
                    toaddressInsert.add(item.Email);
                }
                
                //delete and insert EventRelation
                List<EventRelation> evRelations = [select Id, EventId, RelationId from EventRelation where EventId= :customEvent.StandardEventId__c]; 
                List<EventRelation> evRelationsToDelete = new List<EventRelation>();
                Map<string, EventRelation> evRelationsToNotDelete = new Map<string, EventRelation>();
                List<EventRelation> evRelationsToInsert = new List<EventRelation>();
                List<String> contactsEmails = new List<String>();
                //List<String> contactsToNotDeleteIds = new List<String>();
                Map<String, Integer> mapTypeContacts = new Map<String, Integer>();
                for(EventRelation eRel: evRelations){
                    if(!mapContacts.containsKey(eRel.RelationId)){//new contacts
                        evRelationsToDelete.add(eRel);
                        mapTypeContacts.put(eRel.RelationId, 1);
                    }
                    else{
                        evRelationsToNotDelete.put(eRel.RelationId,eRel);
                        mapTypeContacts.put(eRel.RelationId, 2);
                    }
                    contactsEmails.add(eRel.RelationId);
                }
                delete evRelationsToDelete;
                
                for(string item: contacts){
                    if(!evRelationsToNotDelete.containsKey(item)){
                        EventRelation eRelation = new EventRelation();
                        eRelation.EventId = customEvent.StandardEventId__c;
                        eRelation.RelationId = item;
                        evRelationsToInsert.add(eRelation);
                        contactsEmails.add(item);
                        mapTypeContacts.put(item, 3);
                    }
                }
                
                insert evRelationsToInsert;
                List<Contact> cEmails = [select Id, Email from Contact where Id in :contactsEmails];
                
                for(Contact item : cEmails){
                    if(mapTypeContacts.containsKey(item.Id)){
                        if(mapTypeContacts.get(item.Id)==1){
                            toaddressDelete.add(item.Email);
                        }
                        else if(mapTypeContacts.get(item.Id)==2){
                            toaddressUpdate.add(item.Email);
                        }
                        else if(mapTypeContacts.get(item.Id)==3){
                            toaddressInsert.add(item.Email);
                        }
                    }
                }
                //List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject, DeveloperName, Body from EmailTemplate where DeveloperName='BI_Comunicacion_nuevo_evento' or DeveloperName='BI_Comunicacion_modificacion_evento'];
                List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject, DeveloperName, Body from EmailTemplate where DeveloperName='BI_comunicacion_cancela_evento'];
                List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
                if(!templates.isEmpty()){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            
                        mail.setToAddresses(toaddressDelete);
                        string body = templates[0].Body;
                        system.debug('body: '+body);
                        body = body.replace('{!Event.Subject}', subjectEvent);
                        body = body.replace('{!Event.ActivityDate}', activityDate);
                        //body = body.replace('{!Event.Subject}', item.Subject);
                        mail.setHtmlBody(body);
                        mail.setSubject(templates[0].Subject);
                        mail.setPlainTextBody(body);
                        emails.add(mail);
                        //system.debug('emailAddresses: '+emailAddresses);
                }
                /*for(EmailTemplate item : templates){
                    /*if(item.DeveloperName=='BI_Comunicacion_modificacion_evento'){
                        if(!toaddressUpdate.isEmpty()){
                            Messaging.SingleEmailMessage singleMail = sendEmail(item, toaddressUpdate);
                            emails.add(singleMail);
                        }
                    }   
                    else if(item.DeveloperName=='BI_Comunicacion_nuevo_evento'){
                        if(!toaddressInsert.isEmpty()){
                            Messaging.SingleEmailMessage singleMail = sendEmail(item, toaddressInsert);
                            emails.add(singleMail);
                        }
                    }*/
                    /*if(item.DeveloperName=='BI_Comunicacion_modificacion_evento'){
                        if(!toaddressDelete.isEmpty()){
                            Messaging.SingleEmailMessage singleMail = sendEmail(item, toaddressDelete);
                            emails.add(singleMail);
                        }
                    }   
                    //emails to delete
                    /*else if(){
                        if(!toaddressDelete.isEmpty()){
                            Messaging.SingleEmailMessage singleMail = sendEmail(item, toaddressDelete);
                            emails.add(singleMail);
                        }
                    }*/
                /*}*/
                if(!Test.isRunningTest()){
                    Messaging.sendEmail(emails);
                }
            }
        }catch(exception Exc){
            BI_LogHelper.generate_BILog('PCA_Event_GuestMethods.updateEvent', 'Portal Platino', Exc, 'Class');
        }
    } 
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich Acero
        Company:       Salesforce.com
        Description:   Insert or update events
        
        History: 
        
        <Date>                  <Author>                <Change Description>
        21/07/2014              Ana Escrich Acero           Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Messaging.SingleEmailMessage sendEmail(EmailTemplate template,String[] toaddress){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toaddress);
        mail.setHtmlBody(template.HTMLValue);
        mail.setSubject('delete event');
        mail.setPlainTextBody(template.Body);
        return mail;
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich Acero
        Company:       Salesforce.com
        Description:   Insert or update events
        
        History: 
        
        <Date>                  <Author>                <Change Description>
        21/07/2014              Ana Escrich Acero           Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void PCA_InsertEvent(List <Event__c> news){
        try{
            List<EventRelation> eventRelToInsert = new List<EventRelation>();
            List<Event> eventsToInsert = new List<Event>(); 
            Event__c customEvent = news[0];
            system.debug('customEvent: '+customEvent);
            if(customEvent.Delete__c){
                deleteEvent(news);
            }
            else if(customEvent.StandardEventId__c!=null){
                updateEvent(customEvent);
            }
            
            else{
                Event eventToInsert = new Event();
                eventToInsert = modifyEvent(eventToInsert, customEvent);
                
                eventToInsert.OwnerId = customEvent.OwnerId;
                                  
                    List<String> gList = retrieveGuestList(customEvent.GuestList__c);
                    system.debug('gList: '+gList);
                    for(string eRelation: gList){
                        string rel = eRelation+'';
                        rel = rel.substring(0,3);
                        if(rel=='003') {
                            eventToInsert.whoId=eRelation;
                            break;
                        }
                    }
                    //eventToInsert.whoCount = countC;
                    /*Database.DMLOptions dlo = new Database.DMLOptions();

                    dlo.EmailHeader.triggerUserEmail = true;
                    */
                    /*database.insert(eventToInsert, dlo);
                    system.debug('eventToInsert: '+eventToInsert);*/
                    /*List<EventRelation> eRelations = [SELECT EventId,Id,RelationId FROM EventRelation WHERE EventId in :eventToInsert.Id];
                    system.debug('eRelations1: '+eRelations);*/
                    Database.Saveresult result = Database.insert(eventToInsert);
                    if(result.isSuccess()){
                        List<Event> eventToInsertIds = [Select Id, CreatedBy.Email from Event where Id = :eventToInsert.Id];
                        List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject, Body from EmailTemplate where DeveloperName='BI_Comunicacion_nuevo_evento'];
                        if(!eventToInsertIds.isEmpty() && !templates.isEmpty()){
                            List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
                            //Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
                            String[] toaddress = new String[]{};
                            toaddress.add(eventToInsertIds[0].createdBy.Email); 
                            
                            List<string> contactIds = new List<string>();
                            List<string> userIds = new List<string>();
                            for(string eRelation: gList){
                                string rel = eRelation+'';
                                rel = rel.substring(0,3);
                                if(rel=='003') {
                                    if(eventToInsert.whoId!=eRelation){
                                        EventRelation eRel = new EventRelation();
                                        eRel.EventId = eventToInsert.Id;
                                        eRel.RelationId = eRelation;
                                        eRel.isInvitee = false;
                                        eRel.IsParent = true;
                                        eRel.IsWhat = false;
                                        eventRelToInsert.add(eRel);
                                        contactIds.add(eRelation);
                                    }
                                }
                                else if(rel=='005'){
                                    Event newEvent = new Event();
                                    newEvent =  modifyEvent(newEvent, customEvent);
                                    newEvent.OwnerId = eRelation;
                                    newEvent.BI_ParentId__c = eventToInsert.Id;
                                    eventsToInsert.add(newEvent);
                                    userIds.add(eRelation);
                                }
                            }
                            
                                system.debug('contactIds: '+contactIds);
                            List<Database.SaveResult> results = database.insert(eventsToInsert);
                            boolean error = false;
                            for(Database.SaveResult item : results){
                                if(!item.isSuccess())
                                    error = true;
                            }
                            if(!error){
                                List<User> users = [select Id, Name, Email from User where Id in :userIds];
                                for(User item: users){
                                    toaddress.add(item.Email);
                                }   
                            }
                            results = database.insert(eventRelToInsert);
                            error = false;
                            for(Database.SaveResult item : results){
                                if(!item.isSuccess())
                                    error = true;
                            }
                            if(!error){
                                List<Contact> contacts = [select Id, Name, Email from Contact where Id in :contactIds];
                                for(Contact item: contacts){
                                    toaddress.add(item.Email);
                                }
                            }
                            system.debug('toaddress: '+toaddress);
                            //Messaging.SingleEmailMessage singleMail = sendEmail(templates[0], toaddress); 
                            //emails.add(singleMail);
                            //system.debug('singleMail: '+singleMail);
                            //Messaging.sendEmail(emails);
                        }
                        /*eRelations = [SELECT EventId,Id,RelationId FROM EventRelation WHERE EventId in :eventToInsert.Id];
                        system.debug('eRelations2: '+eRelations);*/
                        List<Event> currentEvent = [SELECT ActivityDateTime,CreatedById,Description,EndDateTime,Id,OwnerId,StartDateTime,Subject FROM Event WHERE Id = : eventToInsert.Id ];
                        createChatterLinks(currentEvent[0]);    
                    }
            }
        }catch(exception Exc){
            BI_LogHelper.generate_BILog('PCA_Event_GuestMethods.PCA_InsertEvent', 'Portal Platino', Exc, 'Class');
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Ana Escrich Acero
        Company:       Salesforce.com
        Description:   Creates links for chatter to redirect to the event
        
        History: 
        
        <Date>                  <Author>                <Change Description>
        21/07/2014              Ana Escrich Acero           Initial Version     
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static void createChatterLinks(Event event){
        try{
            ConnectApi.FeedType feedType = ConnectApi.FeedType.UserProfile;
                        
            ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
            ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegment;
            ConnectApi.LinkSegmentInput linkSegment;
            ConnectApi.LinkAttachmentInput linkAttachment;
            ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
                    
            messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
                
            mentionSegment.id = event.CreatedById;                                        
            messageInput.messageSegments.add(mentionSegment);
                
            textSegment = new ConnectApi.TextSegmentInput();
            textSegment.text = '. He solicitado una reunión ';
                    
            linkSegment = new ConnectApi.LinkSegmentInput();
            messageInput.messageSegments.add(textSegment);
            
            //String url = 'www.google.es';
            String url = Site.getPrefix()+'/PCA_EventRedirect?id='+event.Id;
            linkSegment.url = url;
            messageInput.messageSegments.add(linkSegment);
            
            linkAttachment = new ConnectApi.LinkAttachmentInput();
            linkAttachment.url = url;
            linkAttachment.urlName = 'Ver evento';
            input.attachment = linkAttachment;
                     
            input.body = messageInput;
            
            if(!Test.isRunningTest()){
                
                //system.debug(feedType + '---' + eGItem.Master_Event__r.OwnerId + '---' +input + '@@@' +System.Site.getPrefix());
                system.debug(feedType + '---' + event.createdById + '---' +input + '@@@' +System.Site.getPrefix());
                system.debug(feedType + '---' + event.OwnerId + '---' +input + '@@@' +System.Site.getPrefix());
                ConnectApi.FeedItem feedItemRep = ConnectApi.ChatterFeeds.postFeedItem('0DBw00000000pEgGAI', feedType, event.createdById, input, null);
            }
        }catch(exception Exc){
            BI_LogHelper.generate_BILog('PCA_Event_GuestMethods.createChatterLinks', 'Portal Platino', Exc, 'Class');
        }
    } 
}