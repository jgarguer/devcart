/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Ramón Pernas
Company:        Everis España
Description:    TEST para el Web Service para recibir datos de los problem tickets de Remedy.

History:
 
<Date>                      <Author>                        <Change Description>
01/10/2016                  Ramón Pernas                    Versión Inicial.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
public class TGS_RemedyProblemService_TEST { 
    
    public static testmethod void testUtil() {
        
        Account ho = new Account();
        ho.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = '1.Holding'].Id;
        ho.Name = 'G4S';
        insert ho;
        
        TGS_RemedyProblemService.ProblemInfo p2 = new TGS_RemedyProblemService.ProblemInfo();
        p2.ProblemID = 'ProblemID';
        p2.CoordinatorGroup = 'CoordinatorGroup';
        p2.LocationCompany = 'G4S';
        p2.ProblemLocation = 'Location3';
        p2.TicketSite = 'TicketSite';
        p2.Assignee = 'assignee';
        p2.ProblemCoordinator = 'ProblemCoordinator';
        p2.Summary = 'SOAP prueba Ramon2';
        p2.Notes = 'Notes';
        p2.Impact = '3-Moderate/Limited';
        p2.Urgency = '4-Low';
        p2.Priority = 'Low';
        p2.Status = 'Assigned';
        p2.InvestigationDriver = 'Access';
        p2.INF6_Pbm_Visible = 'N';
        p2.Service = 'Service';
        p2.CI = 'CI';
        p2.TargetDate = 'TargetDate';
        p2.AssignedGroup = 'AssignedGroup';
        p2.Vendor = 'Vendor';
        p2.VendorTicketNumber = 'VendorTicketNumber';
        p2.StatusReason = 'StatusReason';
        p2.Workaround = 'Workaround';
        p2.Resolution = 'Resolution';
        p2.OperationalCategorizationTier1 = 'OperationalCategorizationTier1';
        p2.OperationalCategorizationTier2 = 'OperationalCategorizationTier2';
        p2.OperationalCategorizationTier3 = 'OperationalCategorizationTier3';
        p2.ProductCategorizationTier1 = 'ProductCategorizationTier1';
        p2.ProductCategorizationTier2 = 'ProductCategorizationTier2';
        p2.ProductCategorizationTier3 = 'ProductCategorizationTier3';
        p2.ProductName = 'ProductName';
        p2.ModelVersion = 'ModelVersion';
        p2.Manufactured = 'Manufactured';
        p2.MasterID = 'MasterID';
        p2.CreateToVendor = 'CreateToVendor';
        p2.INF2_Migracion = 'INF2_Migracion';
        p2.DelegatedCompany = 'DelegatedCompany';
        p2.WorkaroundDeterminatedON = 'WorkaroundDeterminatedON';
        p2.WorkDetailNotes = 'WorkDetailNotes // WorkDetailNotes2';
        p2.KnownErrorCreated = 'KnownErrorCreated';
        p2.LastCompletedDate = 'LastCompletedDate';
        p2.Submitter = 'Submitter';
        p2.SubmitDate = 'SubmitDate';
        p2.LastMdifiedBy = 'LastMdifiedBy';
        p2.LastMdifiedDate = 'LastMdifiedDate';
        TGS_RemedyProblemService.createProblem(p2);
        
        TGS_RemedyProblemService.ProblemInfo p3 = new TGS_RemedyProblemService.ProblemInfo();
        p3.ProblemID = 'ProblemID';
        p3.CoordinatorGroup = 'CoordinatorGroup';
        p3.LocationCompany = 'G4S';
        p3.ProblemLocation = 'Location3';
        p3.TicketSite = 'TicketSite';
        p3.Assignee = 'assignee';
        p3.ProblemCoordinator = 'ProblemCoordinator';
        p3.Summary = 'SOAP prueba Ramon2';
        p3.Notes = 'Notes';
        p3.Impact = '3-Moderate/Limited';
        p3.Urgency = '4-Low';
        p3.Priority = 'Low';
        p3.Status = 'Assigne';
        p3.InvestigationDriver = 'Access';
        p3.INF6_Pbm_Visible = 'NO';
        p3.Service = 'Service';
        p3.CI = 'CI';
        p3.TargetDate = 'TargetDate';
        p3.AssignedGroup = 'AssignedGroup';
        p3.Vendor = 'Vendor';
        p3.VendorTicketNumber = 'VendorTicketNumber';
        p3.StatusReason = 'StatusReason';
        p3.Workaround = 'Workaround';
        p3.Resolution = 'Resolution';
        p3.OperationalCategorizationTier1 = 'OperationalCategorizationTier1';
        p3.OperationalCategorizationTier2 = 'OperationalCategorizationTier2';
        p3.OperationalCategorizationTier3 = 'OperationalCategorizationTier3';
        p3.ProductCategorizationTier1 = 'ProductCategorizationTier1';
        p3.ProductCategorizationTier2 = 'ProductCategorizationTier2';
        p3.ProductCategorizationTier3 = 'ProductCategorizationTier3';
        p3.ProductName = 'ProductName';
        p3.ModelVersion = 'ModelVersion';
        p3.Manufactured = 'Manufactured';
        p3.MasterID = 'MasterID';
        p3.CreateToVendor = 'CreateToVendor';
        p3.INF2_Migracion = 'INF2_Migracion';
        p3.DelegatedCompany = 'DelegatedCompany';
        p3.WorkaroundDeterminatedON = 'WorkaroundDeterminatedON';
        p3.WorkDetailNotes = 'WorkDetailNotes ';
        p3.KnownErrorCreated = 'KnownErrorCreated';
        p3.LastCompletedDate = 'LastCompletedDate';
        p3.Submitter = 'Submitter';
        p3.SubmitDate = 'SubmitDate';
        p3.LastMdifiedBy = 'LastMdifiedBy';
        p3.LastMdifiedDate = 'LastMdifiedDate';
        TGS_RemedyProblemService.createProblem(p3);
        
        TGS_RemedyProblemService.createProblem(p2);
        
        TGS_RemedyProblemService.createProblem(p3); 
        
        p3.ProblemID = NULL;
        TGS_RemedyProblemService.createProblem(p3); 
        p3.LocationCompany ='error';
        p3.ProblemID = 'ProblemID';
        TGS_RemedyProblemService.createProblem(p3); 
        p3.LocationCompany ='G4S';
        p3.INF6_Pbm_Visible = NULL;
        TGS_RemedyProblemService.createProblem(p3);
    }
}