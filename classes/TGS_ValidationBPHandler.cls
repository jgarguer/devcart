public class TGS_ValidationBPHandler {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Vital
    Company:       Deloitte
    Description:   Method to validate Billing Profiles from case update.

    History

    <Date>          <Author>                <Description>
    14/12/2015      Ignacio Vital           Initial Version
    11/02/2016      Fernando Arteaga        Changes to save queries
    08/03/2016      Fernando Arteaga        GAP 103: Centralized Billing
    03/04/2016      Jose Miguel Fierro      Added Field to SOQL query
    20/07/2016      Jose Miguel Fierro      Changed validation of CC from belonging to BU, to belonging to BU's country
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    public static void addErrorBP(List<Case> newCase, Map <Id,Case> Cases){
        
        //set<id> orderId = new set<id>();    //This variable is created to save the Orders
        // FAR 08/03/2016 - Rename variable
        Set<Id> setOrderId = new Set<Id>();    //This variable is created to save the Orders
        set<id> legaleId = new set<id>();   //This variable is created to save the Legal Entities 
        boolean cbp;                        //This variable is created to check if there is a centralized bp for the service
        
        // FAR 08/03/2016 - Map containing: key --> order id, value --> billing profile id
        Map<Id, Id> mapOrderBP = new Map<Id, Id>();
        
        for(Case ca: newCase){                                  
            // FAR 29/02/2016 - GAP 103: Centralized Billing
            //if(ca.Status == Constants.CASE_STATUS_RESOLVED){
            if((ca.Status == Constants.CASE_STATUS_RESOLVED && ca.TGS_Service__c != Constants.PRODUCT_MWAN_INTERNET && ca.TGS_Service__c != Constants.PRODUCT_MWAN_SU) || ca.Status == Constants.CASE_STATUS_CLOSED){
                //orderId.add(ca.Order__c);
                setOrderId.add(ca.Order__c); // FAR 08/03/2016 - Rename variable
            }    
        }

        // FAR 11/02/2016 - Execute logic when the set is not empty, to save queries
        // FAR 08/03/2016 - Rename variable
        if (!setOrderId.isEmpty())
        {  
            //List<NE__Order__c> listorder = [SELECT id, Case__r.TGS_Service__c, NE__BillAccId__r.Parent.Parent.Parentid, NE__ServAccId__c, NE__ServAccId__r.Parentid, NE__BillAccId__r.Parentid FROM NE__Order__c WHERE id IN :orderId];
            // FAR 08/03/2016 - Rename variable
            // JMF 03/04/2016 - Added missing field NE__BillingProfId__c to query
            List<NE__Order__c> listorder = [SELECT id, Case__r.TGS_Service__c, NE__BillAccId__r.Parent.Parent.Parentid, NE__ServAccId__c, NE__ServAccId__r.Parentid, NE__ServAccId__r.Parent.ParentId, NE__BillAccId__r.Parentid, NE__BillingProfId__c, NE__AssetEnterpriseId__c FROM NE__Order__c WHERE id IN :setOrderId];  
            
            for(NE__Order__c order: listorder){
                legaleId.add(order.NE__ServAccId__r.Parentid);
            }
          
            List<NE__Billing_Profile__c> listbp = [SELECT id, NE__Account__c, TGS_Product__r.Name, TGS_Customer_Country__c FROM NE__Billing_Profile__c WHERE NE__Account__c IN :legaleId];     
            
            List<NE__Order__c> ordersToUpdate = new List<NE__Order__c>();
            for(NE__Order__c orders: listorder){
                        
                cbp = false;
                //Cases.get(orders.Case__c).addError('Order (' + orders.Id + ')\'s billing profiles: ' + listbp);
                
                for(NE__Billing_Profile__c bps: listbp ){ 
                       
                    //Cases.get(orders.Case__c).addError('(orders.NE__ServAccId__r.Parentid == bps.NE__Account__c) = ' + (orders.NE__ServAccId__r.Parentid == bps.NE__Account__c) + '(bps.TGS_Product__c != null) = ' + (bps.TGS_Product__c != null));
                    if((orders.NE__ServAccId__r.Parentid == bps.NE__Account__c) && (bps.TGS_Product__c != null)){                                                                           
                        
                        //Cases.get(orders.Case__c).addError('bps.TGS_Product__r.Name: ' + bps.TGS_Product__r.Name + ' orders.Case__r.TGS_Service__c: ' + orders.Case__r.TGS_Service__c);
                        if(bps.TGS_Product__r.Name == orders.Case__r.TGS_Service__c){                             
                                   
                            cbp = true;
                            if(orders.NE__BillAccId__r.Parent.Parent.Parentid != bps.TGS_Customer_Country__c){ //If there is a Billing Profile Centralized and it has different Customer Country than the Order's Cost Center                 
                                System.debug('Cost Center is not valid for the Centralized Billing Profile: orders.NE__BillAccId__r.Parent.Parent.ParentId (' + orders.NE__BillAccId__r.Parent.Parent.Parentid + ') != ps.TGS_Customer_Country__c (' + bps.TGS_Customer_Country__c + ')');
                                Cases.get(orders.Case__c).addError('Cost Center is not valid for the Centralized Billing Profile ');
                            }
                            // FAR 08/03/2016 - Gap 103: Centralized billing --> save billing profile in the order
                            else
                            {
                                if (orders.NE__BillingProfId__c != bps.Id){
                                    //ordersToUpdate.add(orders);
                                    //orders.NE__BillingProfId__c = bps.Id;
                                }
                                
                                System.debug('case status:' + Cases.get(orders.Case__c).Status);
                                if (orders.NE__AssetEnterpriseId__c != null)
                                    // Fill the billing profile in the asset too
                                    mapOrderBP.put(orders.NE__AssetEnterpriseId__c, bps.Id);
                            }
                        }
                    }
                }
                
                // START JMF 20/07/2016 - CC should belong to same country as BU, not belong to the BU
                //if((cbp == false) && (orders.NE__BillAccId__r.ParentId != orders.NE__ServAccId__c)){  //If there is not Billing Profile Centralized and CC parent account is not Order's Business Unit
                if((cbp == false) && (orders.NE__BillAccId__r.Parent.Parent.ParentId != orders.NE__ServAccId__r.Parent.ParentId)) {
                    Cases.get(orders.Case__c).addError('Cost Center is not valid for the Distributed Billing Profile');
                }
                // END JMF 20/07/2016
            }
            if(ordersToUpdate.size() > 0)
                update ordersToUpdate;
            
            // JMF 12/04/2016: do not set NE__BillingProfId__c until further notice (delete this comment at that point)
            // FAR 08/03/2016: Save billing profile in the asset
            //if (!mapOrderBP.isEmpty())
            //  setAssetBillingProfile(mapOrderBP);
            
        }
    }

    /*------------------------------------------------------------
    Author:         Fernando Arteaga
    Company:        New Energy Aborda
    Description:    Sets the billing profile in the asset
    IN:             mapOrderBP - map of (order ids, billing profile ids)
    OUT:            
    History
    <Date>      <Author>            <Description>
    08/03/2016  Fernando Arteaga    Initial version
    20/07/2016  Jose Miguel Fierro  Commented because of lack of use
    ------------------------------------------------------------*/
    /*@future(callout = false)
    private static void setAssetBillingProfile(Map<Id, Id> mapOrderBP)
    {
        List<NE__Order__c> listToUpdate = new List<NE__Order__c>();
        // Get the assets 
        for (NE__Order__c asset: [SELECT Id, NE__BillingProfId__c
                                  FROM NE__Order__c
                                  WHERE Id IN: mapOrderBP.keySet()])
        {
            // Set the billing profile getting the key value in the map
            asset.NE__BillingProfId__c = mapOrderBP.get(asset.Id) != null ?  mapOrderBP.get(asset.Id) : null;
            listToUpdate.add(asset);
        }
        
        if (!listToUpdate.isEmpty())
            update listToUpdate;
    }*/
}