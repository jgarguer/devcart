/*
---------------------------------------------------------------------------------------
Author:        Julio Laplaza
Company:       HPE
Description:   Manager for Ticket for UNICA Integration

<Date>                 <Author>                <Change Description>
19/05/2016             Julio Laplaza            Initial Version
---------------------------------------------------------------------------------------
*/
public class BIIN_UNICA_TicketManager
{
    public static String getAdditionalParamsURL(String url, Case caso)
    {
        url = BIIN_UNICA_Utils.addParameterAdditional(url, 'IncidentNumber', caso.BI_Id_del_caso_Legado__c);
        url = BIIN_UNICA_Utils.addParameterAdditional(url, 'FirstName', caso.BI2_Nombre_Contacto_Final__c);
        url = BIIN_UNICA_Utils.addParameterAdditional(url, 'LastName', caso.BI2_Apellido_Contacto_Final__c);
        url = BIIN_UNICA_Utils.addParameterAdditional(url, 'InternetEmail', caso.BI2_Email_Contacto_Final__c);
        url = BIIN_UNICA_Utils.addParameterAdditional(url, 'Site', caso.BIIN_Site__c);
        url = BIIN_UNICA_Utils.addParameterAdditional(url, 'CategorizationTier1', caso.BIIN_Categorization_Tier_1__c);
        url = BIIN_UNICA_Utils.addParameterAdditional(url, 'CategorizationTier2', caso.BIIN_Categorization_Tier_2__c);
        url = BIIN_UNICA_Utils.addParameterAdditional(url, 'CategorizationTier3', caso.BIIN_Categorization_Tier_3__c);
        url = BIIN_UNICA_Utils.addParameterAdditional(url, 'Name', caso.BIIN_Id_Producto__c);
        url = BIIN_UNICA_Utils.addParameterAdditional(url, 'SLMStatus', BIIN_UNICA_Utils.translateFromSalesForce('SLM-Status', caso.TGS_SLA_Light__c));

        return url;
    }

    public static Case getTicketByCaseNumber(String caseNumber)
    {
        Case[] ticket = [SELECT 
        Id,
        CaseNumber, 
        ContactId, 
        OwnerId, 
        ParentId, 
        BI_Id_del_caso_legado__c,
        CreatedDate,
        LastModifiedDate,
        BI_COL_Fecha_Radicacion__c,
        AccountId
        FROM Case 
        WHERE RecordType.DeveloperName 
        IN ('BIIN_Incidencia_Tecnica', 'BIIN_Solicitud_Incidencia_Tecnica') 
        AND CaseNumber =: caseNumber
        LIMIT 1];

        System.debug(' BIIN_UNICA_RestHelper.getTicketByCaseNumber | Ticket: ' + ticket);

        if (ticket.size() > 0) 
        {
            return ticket[0];
        } 
        else 
        {
            return null;
        }
    }

    public static Case getTicketByCorrelatorId(String correlatorId)
    {
        Case[] ticket = [SELECT CaseNumber 
        FROM Case 
        WHERE RecordType.DeveloperName 
        IN ('BIIN_Incidencia_Tecnica', 'BIIN_Solicitud_Incidencia_Tecnica') 
        AND BI_Id_del_caso_legado__c =: correlatorId
        LIMIT 1];

        System.debug(' BIIN_UNICA_RestHelper.getTicketByCorrelatorId | Ticket: ' + ticket);

        if (ticket.size() > 0) 
        {
            return ticket[0];
        } 
        else 
        {
            return new Case();
        }
    }

    public static String getTicketCaseNumber(String correlatorId)
    {
        Case[] ticketCaseNumber = [SELECT CaseNumber
        FROM Case 
        WHERE RecordType.DeveloperName 
        IN ('BIIN_Incidencia_Tecnica', 'BIIN_Solicitud_Incidencia_Tecnica') 
        AND BI_Id_del_caso_legado__c =: correlatorId
        LIMIT 1];

        System.debug(' BIIN_UNICA_RestHelper.getTicketCaseNumber | ticketCaseNumber: ' + ticketCaseNumber);

        return ticketCaseNumber[0].CaseNumber;
    }

    public static String getTicketValidadorFiscal(Id accountId)
    {
        System.debug(' BIIN_UNICA_RestHelper.getTicketValidadorFiscal | accountId: ' + accountId);

        Account[] accs = [SELECT BI_Validador_Fiscal__c FROM Account WHERE Id =: accountId LIMIT 1];

        System.debug(' BIIN_UNICA_RestHelper.getTicketValidadorFiscal | Account Validador Fiscal: ' + accs);

        if (accs.size() > 0) 
        {
            return accs[0].BI_Validador_Fiscal__c;
        }
        else
        {
            return null;
        }
    }
}