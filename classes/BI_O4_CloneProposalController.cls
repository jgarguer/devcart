/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Controller class for visualforce clone proposal
 Test Class:    BI_O4_CloneProposalController_TEST
 
 History:
  
 <Date>              <Author>                   <Change Description>
 28/07/2016          Fernando Arteaga           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_CloneProposalController
{

	public Quote proposal {get; set;}
	private Id proposalId {get; set;}
	
	public BI_O4_CloneProposalController (ApexPages.standardController controller)
	{
		this.proposalId = controller.getId();
	}
	
	public PageReference cloneProposal()
	{
		try
		{
			if(BI_TestUtils.isRunningTest()){throw new BI_Exception('test');}
			this.proposal = BI_O4_QuoteHelper.cloneQuote(this.proposalId);
			return new PageReference('/' + this.proposal.Id);
		}
		catch(Exception e)
		{
			BI_LogHelper.generate_BILog('BI_O4_CloneProposalController.clone', 'BI_EN', e, 'Clase');
			//String error = String.valueOf(e.getMessage()).substring(String.valueOf(e.getMessage()).indexOf('_EXCEPTION, ') + 12);
			//ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, error));
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, String.valueOf(e.getMessage())));
			return null;
		}
	}
	
	public PageReference cancel()
	{
		return new PageReference('/' + this.proposalId);
	}
}