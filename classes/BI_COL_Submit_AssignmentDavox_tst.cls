/**
* Avanxo Colombia
* @author           Jeisson Rojas href=<jrojas@avanxo.com>
* Proyect:
* Description:
*
* Changes (Version)
* -------------------------------------------------------------------------
*           No.     Fecha           Autor                   Descripción
*           -----   ----------      --------------------    ---------------
* @version   1.0    2015-07-24      Jeisson Rojas (JR)      Clase de prueba para el controlador BI_COL_Submit_AssignmentDavox_ctr
*			 1.1	2015-08-19		Jeisson Rojas (JR)		Cambio realizado a la Query de Usuarios del campo Country por Pais__c
*			 1.2	2017-02-01		Pedro Párraga 			Increase in coverage
*			 1.3	23-02-2017      Jaime Regidor           Adding Campaign Values, Adding Catalog Country
					13/03/2017		Marta Gonzalez(Everis)  REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
				    20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
*************************************************************************************************************/
@isTest
Global class BI_COL_Submit_AssignmentDavox_tst {

	public static BI_COL_Submit_AssignmentDavox_ctr 							objSubAssi;
	public static BI_COL_Modificacion_de_Servicio__c 							objMS;
	public static Account 														objCuenta;
	public static Account 														objCuenta2;
	public static Contact 														objContacto;
	public static Opportunity 													objOportunidad;
	public static BI_COL_Anexos__c 												objAnexos;
	public static BI_COL_Anexos__c 												objAnexos2;
	public static BI_COL_Descripcion_de_servicio__c 							objDesSer;
	public static Contract 														objContrato;
	public static Campaign 														objCampana;
	public static BI_Col_Ciudades__c 											objCiudad;
	public static BI_Sede__c 													objSede;
	public static User 															objUsuario;
	public static BI_Punto_de_instalacion__c 									objPuntosInsta;
	public static BI_Log__c 													objBiLog;
	public static BI_COL_Submit_AssignmentDavox_ctr.wrapperCesionDavox  		ObjCesionDavox;

	public static list <Profile> 												lstPerfil;
	public static list <UserRole> 												lstRoles;
	public static list <Campaign> 												lstCampana;
	public static list<BI_COL_manage_cons__c> 									lstManege;
	public static List<RecordType> 												lstRecTyp;
	public static List<User> 													lstUsuarios;
	public static List<BI_COL_Modificacion_de_Servicio__c> 						lstMS;
	public static List<BI_COL_Descripcion_de_servicio__c> 						lstDS;
	public static List<BI_COL_Submit_AssignmentDavox_ctr.wrapperCesionDavox> 	lstWraperCesion;
	public static list<BI_Log__c> 												lstLog = new list <BI_Log__c>();
	public static BI_COL_Configuracion_campos_interfases__c objConfCamposInterfases1;

	public static void crearData() {
		//Roles
		lstRoles                = new list <UserRole>();
		lstRoles                = [Select id, Name from UserRole where Name = 'Telefónica Global'];
		system.debug('datos Rol ' + lstRoles);
		//Ciudad
		objCiudad = new BI_Col_Ciudades__c ();
		objCiudad.Name 						= 'Test City';
		objCiudad.BI_COL_Pais__c 			= 'Test Country';
		objCiudad.BI_COL_Codigo_DANE__c 	= 'TestCDa';
		insert objCiudad;



		//Cuentas
		BI_bypass__c objBibypass = new BI_bypass__c();
		objBibypass.BI_migration__c = true;
		insert objBibypass;
		objCuenta 										= new Account();
		objCuenta.Name                                  = 'prueba';
		objCuenta.BI_Country__c                         = 'Argentina';
		objCuenta.TGS_Region__c                         = 'América';
		objCuenta.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta.CurrencyIsoCode                       = 'COP';
		objCuenta.BI_Fraude__c                          = false;
		objCuenta.BI_COL_Ciudad_Departamento__c			= objCiudad.id;
		objCuenta.BI_No_Identificador_fiscal__c			= '12345';
		objCuenta.BI_Segment__c                         = 'test';
        objCuenta.BI_Subsegment_Regional__c             = 'test';
        objCuenta.BI_Territory__c                       = 'test';
		insert objCuenta;

		objCuenta2                                       = new Account();
		objCuenta2.Name                                  = 'prueba';
		objCuenta2.BI_Country__c                         = 'Argentina';
		objCuenta2.TGS_Region__c                         = 'América';
		objCuenta2.BI_Tipo_de_identificador_fiscal__c    = '';
		objCuenta2.CurrencyIsoCode                       = 'COP';
		objCuenta2.BI_Fraude__c                          = true;
		objCuenta.BI_Segment__c                         = 'test';
        objCuenta.BI_Subsegment_Regional__c             = 'test';
        objCuenta.BI_Territory__c                       = 'test';
		insert objCuenta2;

		//Contactos
		objContacto                                     = new Contact();
		objContacto.LastName                        	= 'Test';
		objContacto.BI_Country__c             		    = 'Argentina';
		objContacto.CurrencyIsoCode           		    = 'COP';
		objContacto.AccountId                 		    = objCuenta.Id;
		objContacto.BI_Tipo_de_contacto__c				= 'Administrador Canal Online';
		objContacto.Phone 								= '1234567';
		objContacto.MobilePhone							= '31047859';
		objContacto.Email								= 'pruebas@pruebas1.com';
		objContacto.Title								= 'tiulo de prueba';
        //REING-INI
        objContacto.BI_Tipo_de_documento__c 			= 'Otros';
        objContacto.BI_Numero_de_documento__c 			= '00000000X';
        objContacto.FS_CORE_Acceso_a_Portal_Platino__c  = true;
        //REING_FIN
		Insert objContacto;
		Contact cont = [select id, name from Contact];

		//catalogo
		NE__Catalog__c objCatalogo 						= new NE__Catalog__c();
		objCatalogo.Name 								= 'prueba Catalogo';
		objCatalogo.BI_country__c 						= 'Argentina';
		insert objCatalogo;

		//Campaña
		objCampana 										= new Campaign();
		objCampana.Name 								= 'Campaña Prueba';
		objCampana.BI_Catalogo__c 						= objCatalogo.Id;
		objCampana.BI_COL_Codigo_campana__c 			= 'prueba1';
		objCampana.BI_Country__c 						= 'Argentina';
        objCampana.BI_Opportunity_Type__c 				= 'Fijo';
        objCampana.BI_SIMP_Opportunity_Type__c 			= 'Alta'; 
		insert objCampana;
		lstCampana 										= new List<Campaign>();
		lstCampana 										= [select Id, Name from Campaign where Id = : objCampana.Id];

		//Tipo de registro
		lstRecTyp 										= [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];

		//Contrato
		objContrato 									= new Contract ();
		objContrato.AccountId 							= objCuenta.Id;
		objContrato.StartDate 							=  System.today().addDays(+5);
		objContrato.BI_COL_Formato_Tipo__c 				= 'Contrato Marco';
		objContrato.ContractTerm 						= 12;
		objContrato.BI_COL_Presupuesto_contrato__c 		= 1000000;
		objContrato.StartDate							= System.today().addDays(+5);
		objContrato.BI_COL_Numero_de_documento__c		= '1234567890';
		insert objContrato;
		objContrato.Status								= 'Activated';
		update objContrato;

		//FUN
		objAnexos               						= new BI_COL_Anexos__c();
		objAnexos.Name          						= 'FUN-0041414';
		objAnexos.RecordTypeId  						= lstRecTyp[0].Id;
		objAnexos.BI_COL_Contrato__c   					= objContrato.Id;
		objAnexos.BI_COL_Contrato_Anexos__C				= objContrato.Id;
		insert objAnexos;

		System.debug('Contrato ID Test: '+objContrato.Id);

		objAnexos2               						= new BI_COL_Anexos__c();
		objAnexos2.Name          						= 'FUN-0041414';
		objAnexos2.RecordTypeId  						= lstRecTyp[0].Id;
		objAnexos2.BI_COL_Contrato__c   				= objContrato.Id;
		objAnexos2.BI_COL_Contrato_Anexos__C			= objContrato.Id;
		objAnexos2.BI_COL_FUN_Anterior__c 				= objAnexos.Id;
		objAnexos2.BI_COL_FUN__c 						= objAnexos.Id;
		insert objAnexos2;

		//Configuracion Personalizada
		BI_COL_manage_cons__c objConPer 				= new BI_COL_manage_cons__c();
		objConPer.Name                  				= 'Viabilidades';
		objConPer.BI_COL_Numero_Viabilidad__c 			= 46;
		objConPer.BI_COL_ConsProyecto__c     			= 1;
		objConPer.BI_COL_ValorConsecutivo__c  			= 1;
		lstManege                      					= new list<BI_COL_manage_cons__c >();
		lstManege.add(objConPer);
		insert lstManege;

		//Oportunidad
		objOportunidad                                      	= new Opportunity();
		objOportunidad.Name                                   	= 'prueba opp';
		objOportunidad.AccountId                              	= objCuenta.Id;
		objOportunidad.BI_Country__c                          	= 'Argentina';
		objOportunidad.CloseDate                              	= System.today().addDays(+5);
		objOportunidad.StageName                              	= 'F5 - Solution Definition';
		objOportunidad.CurrencyIsoCode                        	= 'COP';
		objOportunidad.Certa_SCP__contract_duration_months__c 	= 12;
		objOportunidad.BI_Plazo_estimado_de_provision_dias__c 	= 0 ;
		objOportunidad.OwnerId                                	= objUsuario.id;
		insert objOportunidad;

		list<Opportunity> lstOportunidad 	= new list<Opportunity>();
		lstOportunidad 						= [select Id from Opportunity where Id = : objOportunidad.Id];

		//Direccion
		objSede 									= new BI_Sede__c();
		objSede.BI_COL_Ciudad_Departamento__c 		= objCiudad.Id;
		objSede.BI_Direccion__c 					= 'Test Street 123 Number 321';
		objSede.BI_Localidad__c 					= 'Test Local';
		objSede.BI_COL_Estado_callejero__c 			= System.Label.BI_COL_LbValor3EstadoCallejero;
		objSede.BI_COL_Sucursal_en_uso__c 			= 'Libre';
		objSede.BI_Country__c 						= 'Colombia';
		objSede.Name 								= 'Test Street 123 Number 321, Test Local Colombia';
		objSede.BI_Codigo_postal__c 				= '12356';

		insert objSede;
		BI_Sede__c sede = [select id, name, BI_COL_Direccion_sin_espacio__c from BI_Sede__c];

		//Sede
		objPuntosInsta 						= new BI_Punto_de_instalacion__c ();
		objPuntosInsta.BI_Cliente__c       = objCuenta.Id;
		objPuntosInsta.BI_Sede__c          = objSede.Id;
		objPuntosInsta.BI_Contacto__c      = objContacto.id;
		objPuntosInsta.Name 				= 'Pruebas ade bacaneria';
		insert objPuntosInsta;

		//DS
		objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
		objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
		objDesSer.CurrencyIsoCode               			= 'COP';
		objDesSer.BI_COL_Sede_Destino__c                    = objPuntosInsta.id;
		objDesSer.BI_COL_Sede_Origen__c                     = objPuntosInsta.id;
		objDesSer.BI_COL_Estado_de_servicio__c              = 'Activado';
		//objDesSer.Name 										= 'prueba';
		insert objDesSer;

		objDesSer = [Select BI_COL_Oportunidad__c, CurrencyIsoCode,
		             BI_COL_Sede_Destino__c, BI_COL_Sede_Origen__c, BI_COL_Estado_de_servicio__c,
		             Name, Id, BI_COL_Autoconsumo__c From BI_COL_Descripcion_de_servicio__c limit 1
		            ];

		lstDS 	= [select id, name , BI_COL_Autoconsumo__c from BI_COL_Descripcion_de_servicio__c];

		//MS
		objMS                         						= new BI_COL_Modificacion_de_Servicio__c();
		objMS.BI_COL_FUN__c           						= objAnexos2.Id;
		objMS.BI_COL_Codigo_unico_servicio__c 				= objDesSer.Id;
		objMS.BI_COL_Clasificacion_Servicio__c  			= 'CESION CONTRATO';
		objMS.BI_COL_Oportunidad__c 						= objOportunidad.Id;
		objMS.BI_COL_Bloqueado__c 							= false;
		objMS.BI_COL_Estado__c 								= 'Activa';
		objMS.BI_COL_Sucursal_de_Facturacion__c 			= objPuntosInsta.Id;
		objMs.BI_COL_Sucursal_Origen__c 					= objPuntosInsta.Id;
		objMs.BI_COL_Estado_orden_trabajo__c				= 'Ejecutado';
		objMs.BI_COL_Fecha_fin__c							= System.today().addDays(+5);
		objMs.BI_COL_Fecha_inicio_de_cobro_RFB__c			= System.today().addDays(+1);
		//objMs.BI_COL_Cuenta_facturar_davox__c				= 'prueba'; //10/11/2016 GSPM: Se comentó el campo antiguo
		objMs.BI_COL_Cuenta_facturar_davox1__c				= 'prueba';  //10/11/2016 GSPM: Campo nuevo facturar davox1 
		objMs.BI_COL_Fecha_liberacion_OT__c					= System.today().addDays(+2);
		//objms.BI_COL_Tipo_de_Moneda__c						= 'Prueba';
		insert objMS;
		objMS.BI_COL_Estado__c 								= 'Activa';
		update objMs;

		lstMS = [select Name, BI_COL_Codigo_unico_servicio__c, BI_COL_Producto__r.Name, BI_COL_Descripcion_Referencia__c,
		         BI_COL_Codigo_unico_servicio__r.Name, BI_COL_Sucursal_de_Facturacion__c,
		         BI_COL_Codigo_unico_servicio__r.BI_COL_Sede_Destino__c,
		         BI_COL_Sucursal_Origen__c, /*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c, BI_COL_Tipo_de_Moneda__c, CurrencyIsoCode
		         from 	BI_COL_Modificacion_de_Servicio__c ]; //10/11/2016 GSPM: Se comentó el campo antiguo y se adiciono nuevo campo davox1
		//Configuracion_campos_interfases
		BI_COL_Configuracion_campos_interfases__c objConfCamposInterfases = new BI_COL_Configuracion_campos_interfases__c();
		objConfCamposInterfases.BI_COL_Adicion__c = false;
		objConfCamposInterfases.BI_COL_Altas__c = false;
		objConfCamposInterfases.BI_COL_Bajas__c = false;
		objConfCamposInterfases.BI_COL_Calificador_Texto__c = null;
		objConfCamposInterfases.BI_COL_Cambio_Cuenta__c = false;
		objConfCamposInterfases.BI_COL_Cambio_Plan__c = false;
		objConfCamposInterfases.BI_COL_Cambio_servicio_alta__c = false;
		objConfCamposInterfases.BI_COL_Configuracion_Interfaz__c = null;
		objConfCamposInterfases.BI_COL_Convertir_mayuscula__c = false;
		objConfCamposInterfases.BI_COL_Decimales__c = null;
		objConfCamposInterfases.BI_COL_Descripcion__c = 'Envio de Instalaciones';
		objConfCamposInterfases.BI_COL_Direccion__c = 'SALIENTE';
		objConfCamposInterfases.BI_COL_Downgrade__c = false;
		objConfCamposInterfases.BI_COL_Formato_Campo__c = null;
		objConfCamposInterfases.BI_COL_Funcion_Especial__c = null;
		objConfCamposInterfases.BI_COL_Identificador__c = false;
		objConfCamposInterfases.BI_COL_ID_Colombia__c = 'a3Qg0000000ExpJEAS';
		objConfCamposInterfases.BI_COL_Inactivacion__c = false;
		objConfCamposInterfases.BI_COL_Ingreso__c = false;
		objConfCamposInterfases.BI_COL_Interfaz__c = 'DAVOXPLUS';
		objConfCamposInterfases.BI_COL_Linea__c = null;
		objConfCamposInterfases.BI_COL_Longitud__c = null;
		objConfCamposInterfases.BI_COL_Nombre_campo__c = null;
		objConfCamposInterfases.BI_COL_Objeto__c = 'BI_COL_Modificacion_de_Servicio__c';
		objConfCamposInterfases.BI_COL_Observaciones__c = null;
		objConfCamposInterfases.BI_COL_Orden__c = 15;
		objConfCamposInterfases.BI_COL_Permite_nulos__c = false;
		objConfCamposInterfases.BI_COL_Renegociacion__c = false;
		objConfCamposInterfases.BI_COL_Retiro__c = false;
		objConfCamposInterfases.BI_COL_Segmento__c = null;
		objConfCamposInterfases.BI_COL_Separador_Decimal__c = 'Õ';
		objConfCamposInterfases.BI_COL_Separador_Interno__c = '.';
		objConfCamposInterfases.BI_COL_Separador__c = 'Õ';
		objConfCamposInterfases.BI_COL_Servicios_de_IngenierIa__c = false;
		objConfCamposInterfases.BI_COL_Susp_Temporal_Facturacion__c = false;
		objConfCamposInterfases.BI_COL_Tipo_campo__c = null;
		objConfCamposInterfases.BI_COL_Tipo_transaccion__c = 'SERV. CORPORATIVO - NOVEDADES';
		objConfCamposInterfases.BI_COL_Traslado__c = false;
		objConfCamposInterfases.BI_COL_Upgrade__c = false;
		objConfCamposInterfases.BI_COL_Valor_predeterminado__c = null;
		objConfCamposInterfases.Name = 'Cabecera';
		insert objConfCamposInterfases;

		objConfCamposInterfases1 = new BI_COL_Configuracion_campos_interfases__c();
		objConfCamposInterfases1.BI_COL_Adicion__c = false;
		objConfCamposInterfases1.BI_COL_Altas__c = false;
		objConfCamposInterfases1.BI_COL_Bajas__c = false;
		objConfCamposInterfases1.BI_COL_Calificador_Texto__c = null;
		objConfCamposInterfases1.BI_COL_Cambio_Cuenta__c = false;
		objConfCamposInterfases1.BI_COL_Cambio_Plan__c = false;
		objConfCamposInterfases1.BI_COL_Cambio_servicio_alta__c = false;
		objConfCamposInterfases1.BI_COL_Configuracion_Interfaz__c = objConfCamposInterfases.id;
		objConfCamposInterfases1.BI_COL_Convertir_mayuscula__c = false;
		objConfCamposInterfases1.BI_COL_Decimales__c = null;
		objConfCamposInterfases1.BI_COL_Descripcion__c = 'Envio de Instalaciones';
		objConfCamposInterfases1.BI_COL_Direccion__c = 'SALIENTE';
		objConfCamposInterfases1.BI_COL_Downgrade__c = false;
		objConfCamposInterfases1.BI_COL_Formato_Campo__c = null;
		objConfCamposInterfases1.BI_COL_Funcion_Especial__c = 'Obtener_Primer_NoNull';
		objConfCamposInterfases1.BI_COL_Identificador__c = false;
		objConfCamposInterfases1.BI_COL_ID_Colombia__c = 'a3Qg0000000ExsKEAS';
		objConfCamposInterfases1.BI_COL_Inactivacion__c = false;
		objConfCamposInterfases1.BI_COL_Ingreso__c = false;
		objConfCamposInterfases1.BI_COL_Interfaz__c = 'DAVOXPLUS';
		objConfCamposInterfases1.BI_COL_Linea__c = null;
		objConfCamposInterfases1.BI_COL_Longitud__c = null;
		objConfCamposInterfases1.BI_COL_Nombre_campo__c = 'Opportunity.Account.Id, BI_COL_Sucursal_Origen__r.BI_COL_Contacto_instalacion__r.Email';
		objConfCamposInterfases1.BI_COL_Objeto__c = 'BI_COL_Modificacion_de_Servicio__c';
		objConfCamposInterfases1.BI_COL_Observaciones__c = null;
		objConfCamposInterfases1.BI_COL_Orden__c = 115;
		objConfCamposInterfases1.BI_COL_Permite_nulos__c = false;
		objConfCamposInterfases1.BI_COL_Renegociacion__c = false;
		objConfCamposInterfases1.BI_COL_Retiro__c = false;
		objConfCamposInterfases1.BI_COL_Segmento__c = null;
		objConfCamposInterfases1.BI_COL_Separador_Decimal__c = '.';
		objConfCamposInterfases1.BI_COL_Separador_Interno__c = ',';
		objConfCamposInterfases1.BI_COL_Separador__c = '~';
		objConfCamposInterfases1.BI_COL_Servicios_de_IngenierIa__c = false;
		objConfCamposInterfases1.BI_COL_Susp_Temporal_Facturacion__c = false;
		objConfCamposInterfases1.BI_COL_Tipo_campo__c = null;
		objConfCamposInterfases1.BI_COL_Tipo_transaccion__c = 'SERV. CORPORATIVO - NOVEDADES';
		objConfCamposInterfases1.BI_COL_Traslado__c = false;
		objConfCamposInterfases1.BI_COL_Upgrade__c = false;
		objConfCamposInterfases1.BI_COL_Valor_predeterminado__c = null;
		objConfCamposInterfases1.Name = 'Cabecera';
		insert objConfCamposInterfases1;

		objBiLog											= new BI_Log__c();
		objBiLog.BI_COL_Informacion_recibida__c						= 'OK, Respuesta Procesada';
		//objBiLog.BI_COL_Contacto__c 									= lstContacts[0].Id;
		objBiLog.BI_COL_Estado__c										= 'FALLIDO';
		objBiLog.BI_COL_Interfaz__c									= 'NOTIFICACIONES';
		lstLog.add(objBiLog);
		insert lstLog;
	}

	static testMethod void test_method_one() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			// TO DO: implement unit test
			Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
			Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );

			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_SendSessionDavox_pag;
			Test.setCurrentPage(pageRef);

			String strError = 'prueba Error';
			String strDato1 = 'Error Prueba 1';
			String strDato2 = 'Error Prueba 2';
			List<String> lstStringErr = new List<String>();

			lstStringErr.add(objPuntosInsta.Id);

			Test.startTest();

			Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			String mspag;
			mspag = pageRef.getParameters().put('id', objContrato.Id);

			objSubAssi = new BI_COL_Submit_AssignmentDavox_ctr();

			objSubAssi.enviar();

			Test.stopTest();
		}
	}

	static testMethod void test_method_one1() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			// TO DO: implement unit test
			Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
			Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_SendSessionDavox_pag;
			Test.setCurrentPage(pageRef);

			String strError = 'prueba Error';
			String strDato1 = 'Error Prueba 1';
			String strDato2 = 'Error Prueba 2';
			List<String> lstStringErr = new List<String>();

			lstStringErr.add(objPuntosInsta.Id);

			Test.startTest();

			Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			String mspag;
			mspag = pageRef.getParameters().put('id', objContrato.Id);

			objSubAssi = new BI_COL_Submit_AssignmentDavox_ctr();

			objSubAssi.cancelar();
			//objSubAssi.contratoCadenaInterfaz();
			objSubAssi.creaLogTransaccionError(strError, lstStringErr);

			Test.stopTest();
		}
	}

	static testMethod void test_method_one2() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			// TO DO: implement unit test
			Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
			Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_SendSessionDavox_pag;
			Test.setCurrentPage(pageRef);

			String strError = 'prueba Error';
			String strDato1 = 'Error Prueba 1';
			String strDato2 = 'Error Prueba 2';
			List<String> lstStringErr = new List<String>();

			lstStringErr.add(objPuntosInsta.Id);

			Test.startTest();

			Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			String mspag;
			mspag = pageRef.getParameters().put('id', objContrato.Id);

			objSubAssi = new BI_COL_Submit_AssignmentDavox_ctr();

			objSubAssi.creaLogTransaccionError(strError, lstStringErr);

			Test.stopTest();
		}
	}

	static testMethod void test_method_three() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			// TO DO: implement unit test
			Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
			Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_SendSessionDavox_pag;
			Test.setCurrentPage(pageRef);



			ObjCesionDavox = new BI_COL_Submit_AssignmentDavox_ctr.wrapperCesionDavox (objMS, true);

			ObjCesionDavox.ms = objMs;
			ObjCesionDavox.esCorrecto = true;

			List<String> lstStringErr 	= new List<String>();
			lstWraperCesion 			= new List<BI_COL_Submit_AssignmentDavox_ctr.wrapperCesionDavox>();

			list<BI_COL_Configuracion_campos_interfases__c> confIntf = new list<BI_COL_Configuracion_campos_interfases__c>([SELECT BI_COL_Nombre_campo__c,
			        BI_COL_Orden__c, BI_COL_Formato_Campo__c, BI_COL_Tipo_campo__c, BI_COL_Decimales__c, BI_COL_Separador_Interno__c, BI_COL_Longitud__c,
			        BI_COL_Convertir_mayuscula__c FROM BI_COL_Configuracion_campos_interfases__c WHERE BI_COL_Configuracion_Interfaz__r.BI_COL_Direccion__c = 'SALIENTE'
			                and BI_COL_Configuracion_Interfaz__r.BI_COL_Interfaz__c = 'DAVOXPLUS'
			                        and BI_COL_Configuracion_Interfaz__r.BI_COL_Tipo_transaccion__c = 'SERV. CORPORATIVO - NOVEDADES' AND
			                                (BI_COL_Nombre_campo__c LIKE 'Oportunidad__r.Account.%' OR BI_COL_Nombre_campo__c LIKE 'FUN__r.Contrato__r.%')]);


			Test.startTest();

			//Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			//Test.setMock(WebServiceMock.class, new MockResponseGenerator2());
			String mspag;
			mspag = pageRef.getParameters().put('id', objContrato.Id);

			objSubAssi = new BI_COL_Submit_AssignmentDavox_ctr();

			objSubAssi.enviar();
			//objSubAssi.cancelar();
			//objSubAssi.contratoCadenaInterfaz();
			//objSubAssi.creaLogTransaccionError('prueba Error', lstStringErr);
			Test.stopTest();
		}
	}

	static testMethod void test_method_three1() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			// TO DO: implement unit test
			Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
			Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_SendSessionDavox_pag;
			Test.setCurrentPage(pageRef);


			ObjCesionDavox = new BI_COL_Submit_AssignmentDavox_ctr.wrapperCesionDavox (objMS, true);

			ObjCesionDavox.ms = objMs;
			ObjCesionDavox.esCorrecto = true;

			List<String> lstStringErr 	= new List<String>();
			lstWraperCesion 			= new List<BI_COL_Submit_AssignmentDavox_ctr.wrapperCesionDavox>();

			list<BI_COL_Configuracion_campos_interfases__c> confIntf = new list<BI_COL_Configuracion_campos_interfases__c>([SELECT BI_COL_Nombre_campo__c,
			        BI_COL_Orden__c, BI_COL_Formato_Campo__c, BI_COL_Tipo_campo__c, BI_COL_Decimales__c, BI_COL_Separador_Interno__c, BI_COL_Longitud__c,
			        BI_COL_Convertir_mayuscula__c FROM BI_COL_Configuracion_campos_interfases__c WHERE BI_COL_Configuracion_Interfaz__r.BI_COL_Direccion__c = 'SALIENTE'
			                and BI_COL_Configuracion_Interfaz__r.BI_COL_Interfaz__c = 'DAVOXPLUS'
			                        and BI_COL_Configuracion_Interfaz__r.BI_COL_Tipo_transaccion__c = 'SERV. CORPORATIVO - NOVEDADES' AND
			                                (BI_COL_Nombre_campo__c LIKE 'Oportunidad__r.Account.%' OR BI_COL_Nombre_campo__c LIKE 'FUN__r.Contrato__r.%')]);


			Test.startTest();

			//Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			//Test.setMock(WebServiceMock.class, new MockResponseGenerator2());
			String mspag;
			mspag = pageRef.getParameters().put('id', objContrato.Id);

			objSubAssi = new BI_COL_Submit_AssignmentDavox_ctr();

			objSubAssi.cancelar();
			Test.stopTest();
		}
	}

	static testMethod void test_method_three2() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			// TO DO: implement unit test
			Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
			Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_SendSessionDavox_pag;
			Test.setCurrentPage(pageRef);



			ObjCesionDavox = new BI_COL_Submit_AssignmentDavox_ctr.wrapperCesionDavox (objMS, true);

			ObjCesionDavox.ms = objMs;
			ObjCesionDavox.esCorrecto = true;

			List<String> lstStringErr 	= new List<String>();
			lstWraperCesion 			= new List<BI_COL_Submit_AssignmentDavox_ctr.wrapperCesionDavox>();

			list<BI_COL_Configuracion_campos_interfases__c> confIntf = new list<BI_COL_Configuracion_campos_interfases__c>([SELECT BI_COL_Nombre_campo__c,
			        BI_COL_Orden__c, BI_COL_Formato_Campo__c, BI_COL_Tipo_campo__c, BI_COL_Decimales__c, BI_COL_Separador_Interno__c, BI_COL_Longitud__c,
			        BI_COL_Convertir_mayuscula__c FROM BI_COL_Configuracion_campos_interfases__c WHERE BI_COL_Configuracion_Interfaz__r.BI_COL_Direccion__c = 'SALIENTE'
			                and BI_COL_Configuracion_Interfaz__r.BI_COL_Interfaz__c = 'DAVOXPLUS'
			                        and BI_COL_Configuracion_Interfaz__r.BI_COL_Tipo_transaccion__c = 'SERV. CORPORATIVO - NOVEDADES' AND
			                                (BI_COL_Nombre_campo__c LIKE 'Oportunidad__r.Account.%' OR BI_COL_Nombre_campo__c LIKE 'FUN__r.Contrato__r.%')]);


			Test.startTest();

			//Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			//Test.setMock(WebServiceMock.class, new MockResponseGenerator2());
			String mspag;
			mspag = pageRef.getParameters().put('id', objContrato.Id);

			objSubAssi = new BI_COL_Submit_AssignmentDavox_ctr();

			objSubAssi.contratoCadenaInterfaz();
			Test.stopTest();
		}
	}

	static testMethod void test_method_three3() {
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario) {
			// TO DO: implement unit test
			Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
			Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );
			crearData();

			ApexPages.StandardController  controler         = new ApexPages.StandardController(objContrato);
			PageReference                 pageRef           = Page.BI_COL_SendSessionDavox_pag;
			Test.setCurrentPage(pageRef);


			ObjCesionDavox = new BI_COL_Submit_AssignmentDavox_ctr.wrapperCesionDavox (objMS, true);

			ObjCesionDavox.ms = objMs;
			ObjCesionDavox.esCorrecto = true;

			List<String> lstStringErr 	= new List<String>();
			lstWraperCesion 			= new List<BI_COL_Submit_AssignmentDavox_ctr.wrapperCesionDavox>();

			list<BI_COL_Configuracion_campos_interfases__c> confIntf = new list<BI_COL_Configuracion_campos_interfases__c>([SELECT BI_COL_Nombre_campo__c,
			        BI_COL_Orden__c, BI_COL_Formato_Campo__c, BI_COL_Tipo_campo__c, BI_COL_Decimales__c, BI_COL_Separador_Interno__c, BI_COL_Longitud__c,
			        BI_COL_Convertir_mayuscula__c FROM BI_COL_Configuracion_campos_interfases__c WHERE BI_COL_Configuracion_Interfaz__r.BI_COL_Direccion__c = 'SALIENTE'
			                and BI_COL_Configuracion_Interfaz__r.BI_COL_Interfaz__c = 'DAVOXPLUS'
			                        and BI_COL_Configuracion_Interfaz__r.BI_COL_Tipo_transaccion__c = 'SERV. CORPORATIVO - NOVEDADES' AND
			                                (BI_COL_Nombre_campo__c LIKE 'Oportunidad__r.Account.%' OR BI_COL_Nombre_campo__c LIKE 'FUN__r.Contrato__r.%')]);


			Test.startTest();

			//Test.setMock( WebServiceMock.class, new BI_COL_Davox_wsdl_mws());
			//Test.setMock(WebServiceMock.class, new MockResponseGenerator2());
			String mspag;
			mspag = pageRef.getParameters().put('id', objContrato.Id);

			objSubAssi = new BI_COL_Submit_AssignmentDavox_ctr();

			objSubAssi.creaLogTransaccionError('prueba Error', lstStringErr);
			try{
				List<String> log = new List<String>();
				log.add('Log');
				objSubAssi.creaLogTransaccion(null, log);
			}catch(Exception ex){

			}
			

			Test.stopTest();
		}
	}
}