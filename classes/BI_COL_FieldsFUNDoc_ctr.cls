public with sharing class BI_COL_FieldsFUNDoc_ctr {
    
  public boolean CFM{get;set;}
  public boolean CNX{get;set;}
  public boolean nombreSucursal{get;set;}
  public boolean ciudadDestino{get;set;}
  public boolean bwPorExceso{get;set;}
  public boolean costoBwPorExceso{get;set;}
  public boolean camposVisible{get;set;}
  public boolean habilitarGenDoc{get;set;}
  public String idFun{get;set;}
  public String tipoOrden{get;set;}
  public String anexoServicios{get;set;}
  public String demo{get;set;}
  public Boolean blnpageBlock1{get; set;}
  public list<SelectOption> SelectContratos {get; set;}
  public list<BI_COL_Anexos__c> lstAnexos;
  public list<BI_COL_Anexos__c> lstContratoFUN;
  public list<BI_COL_Modificacion_de_Servicio__c> lstMS;
  
  
  public BI_COL_FieldsFUNDoc_ctr(){
    
    
    idFun = ApexPages.currentPage().getParameters().get('idFUN');
    lstAnexos = new list<BI_COL_Anexos__c>();
    
    if(idFun.contains('?'))
      idFun = idFun.substring(0,idFun.lastIndexOf('?'));
    CFM=false;  
    CNX=false;
    blnpageBlock1=true;
    nombreSucursal=false;
    ciudadDestino=false;
    bwPorExceso=false;
    costoBwPorExceso=false;
    camposVisible=false;
    habilitarGenDoc = true;
    if(!Test.isRunningTest())
    {
      demo='seleccionar';
    }
    
    lstContratoFUN = [Select BI_COL_Contrato__c From BI_COL_Anexos__c Where BI_COL_Contrato__c =: idFun];
    system.debug('lstContratoFUN--------------->>>>'+lstContratoFUN);
    for(BI_COL_Anexos__c elem : lstContratoFUN ){
        
        String IDContrato = ''+elem;
        lstAnexos = [Select id, BI_COL_Grupo__c from BI_COL_Anexos__c where BI_COL_Contrato_Anexos__c =: IDContrato ];
        
    }
    System.debug('\n lstAnexos========>>>>'+lstAnexos);
    SelectContratos = new list<SelectOption>();
    SelectContratos.add(new SelectOption('N/A','N/A'));
    
    for(BI_COL_Anexos__c item : lstAnexos){
        
        String items = ''+item.BI_COL_Grupo__c;
        SelectContratos.add(new SelectOption(items,items));
    }
    
  }
  
  public PageReference crearPdf(){
    
    
    
    String pagina='/apex/BI_COL_ContractExcel_pag?idFUN='+idFun+'&CFM='+CFM+'&CNX='+CNX+'&nombreSucursal='+nombreSucursal+'&ciudadDestino='+ciudadDestino+
    '&bwPorExceso='+bwPorExceso+'&costoBwPorExceso='+costoBwPorExceso+'&tipoOrden='+tipoOrden+'&anexoServicios='+anexoServicios;
    system.debug('***valor demo ' + this.demo);
    if(this.demo=='Si'){
      pagina='/apex/BI_COL_FUN_Demo_pag?Id='+idFun;
    }
    //LMJ 20140826
    if(this.demo=='Otro'){
      
      lstMS = [Select m.Id, m.BI_COL_Codigo_unico_servicio__r.BI_COL_Autoconsumo__c
                from BI_COL_Modificacion_de_Servicio__c m
                where BI_COL_FUN__c =: idFun and BI_COL_Codigo_unico_servicio__r.BI_COL_Autoconsumo__c = true limit 1];
      if (lstMS != null && lstMS.size() > 0){
        pagina = '/apex/BI_COL_Autoconsumo_pag?idFUN='+idFun;
      }else{
        BI_COL_Useful_messages_cls.mostrarMensaje(BI_COL_Useful_messages_cls.ERROR, 'No hay MS de autoconsumo.');
        PageReference paginaInicio = new PageReference('/apex/BI_COL_FieldsFUNDoc_pag?idFUN='+idFun);
        paginaInicio.setRedirect(false); 
        return paginaInicio;
        
      }    
      //Descripcion_Servicio_R__r.Autoconsumo__c
      //pagina = '/apex/PageTestLM';
      //validar que se tenga DS de autoconsumo, en caso de tenerse, se puede generar el archivo.
      //Si no es de autoconsumo, entonces poner el mensaje y no seguir con la generaci?n del archivo
      //PageReference pageRef = ApexPages.currentPage();
    }
    
    system.debug('Redireccionando a-------->'+pagina);
    PageReference paginaInicio = new PageReference(pagina);
    paginaInicio.setRedirect(true); 
    return paginaInicio;
  }
  
  public PageReference retorno(){
    system.debug('\n\nRedireccionando a la pagina:::::::'+'/'+idFun+'\n\n');
    //PageReference paginaInicio = new PageReference('/apex/contratoExcel_pag?idFUN='+idFun);
    
    PageReference paginaInicio = new PageReference('/'+idFun);
    paginaInicio.setRedirect(true); 
    return paginaInicio;
  }
  
  public Pagereference cambioDemo(){
    //MOD LMJ 20140826
    
    if(demo.equals('seleccionar')){
        
            habilitarGenDoc = true;
            camposVisible = false;  
            
        } else if(demo.equals('Si')){
            
            habilitarGenDoc = false;
            camposVisible = false;
            
        } else if (demo.equals('Otro')){
            
            if (validarAutoconsumo()){
                
                habilitarGenDoc = false;
                camposVisible = false;
                
            }else{
                
                habilitarGenDoc = true;
                camposVisible = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No hay ms de autoconsumo'));
            }
        }
        else{
            
            habilitarGenDoc = false;
            camposVisible=true;
            
        }
        
    return null;  
  }
  
  public boolean validarAutoconsumo(){
    
    lstMS = [Select m.Id, m.BI_COL_Codigo_unico_servicio__r.BI_COL_Autoconsumo__c
                from BI_COL_Modificacion_de_Servicio__c m
                where BI_COL_FUN__c = :idFUN and BI_COL_Codigo_unico_servicio__r.BI_COL_Autoconsumo__c = true limit 1];
                
        system.debug('***Lista ms validar autoconsumo ' + lstMS);
        
      if (lstMS == null || lstMS.size() == 0){
            return false;       
      }else
        return true;
  }
  
  public void validarFraude()
  {
    list<BI_COL_Anexos__c> ListFraude = new list<BI_COL_Anexos__c>([SELECT BI_COL_Contrato__r.Account.BI_Fraude__c,BI_COL_Contrato__r.Account.ID,BI_COL_Contrato__r.Account.BI_No_Identificador_fiscal__c 
                      FROM BI_COL_Anexos__c  WHERE id=:idFun]);
    List<ID> lstID=new List<ID>();
    
    if(ListFraude != null && ListFraude.size() > 0){
        
        for(BI_COL_Anexos__c f:ListFraude)
        { 
          lstID.add(f.BI_COL_Contrato__r.Account.ID);
        }
        
        Map<String,List<String>> mpConsulta=BI_COL_validationFraud_cls.Consultar_Fraude(lstID); 
        List<String> lstRespuesta = mpConsulta.get(ListFraude[0].BI_COL_Contrato__r.Account.BI_No_Identificador_fiscal__c);
        
        //if(!ListFraude.isEmpty() && ListFraude[0].Contrato__r.Account.Estado_Fraude__c)
        if(!ListFraude.isEmpty() && lstRespuesta[0]=='1')
        {
          blnpageBlock1 = false; 
          system.debug('------>'+System.Label.BI_COL_LbValidationFraud);
          BI_COL_Useful_messages_cls.mostrarMensaje(BI_COL_Useful_messages_cls.WARNING, System.Label.BI_COL_LbValidationFraud+'  '+lstRespuesta[1]);// 'El Cliente se encuentra en fraude, no se puede continuar con el proceso '
        }
    }
  }
  
   /*static testMethod void myTestMethod(){
     Apexpages.currentPage().getParameters().put('idFUN','a1AQ0000006K5nu');
     pagCamposFunDoc_ctr ctr= new pagCamposFunDoc_ctr();  
     ctr.crearPdf();
     ctr.cambioDemo(); 
     ctr.demo = 'Si';
     ctr.crearPdf();
     ctr.cambioDemo();
     ctr.demo = 'Otro';
     ctr.crearPdf();
     ctr.cambioDemo();
     ctr.retorno();
   }*/
    

}