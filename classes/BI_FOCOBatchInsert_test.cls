@isTest
private class BI_FOCOBatchInsert_test {



    
    
    @isTest static void test_method_one() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List<Account>  aList= new list<Account>();

        BI_FOCO_Pais__c cs = new BI_FOCO_Pais__c(Name='Testx001',Batch_Size__c=10, Currency_ISO_Code__c='ARS', Opportunity_Probability__c=50, Notify_Email_List__c='testing@somebody.com'); 
        upsert cs;
        //Region__c pais = new Region__c(CurrencyIsoCode = 'ARS', Name='Testx001');
        String pais;
        Map<String, BI_Code_ISO__c> region = BI_DataLoad.loadRegions();
        for(BI_Code_ISO__c biCode: region.values()){
            if(biCode.BI_Country_Currency_ISO_Code__c == 'ARS'){
                pais = biCode.Name;
            }
        }

        BI_Periodo__c per = new BI_Periodo__c(BI_Mes_Ano__c='01-2016', Name='Enero 2016');
        upsert per;

        BI_Rama__c rama = new BI_Rama__c(BI_Nombre__c='Testx001');
        upsert rama;

        //Create Test Accounts
        Account a1 = new Account(Name='Testx001', BI_Country__c  = pais, BI_Validador_Fiscal__c='Testx001',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');
        Account a2 = new Account(Name='Testx002', BI_Country__c  = pais, BI_Validador_Fiscal__c='Testx002',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');
        Account a3 = new Account(Name='Testx003', BI_Country__c  = pais, BI_Validador_Fiscal__c='Testx003',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');
        Account a4 = new Account(Name='Testx004', BI_Country__c  = pais, BI_Validador_Fiscal__c='Testx004',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');
        Account a5 = new Account(Name='Testx005', BI_Country__c  = pais, BI_Validador_Fiscal__c='Testx005',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');
        Account a6 = new Account(Name='Testx006', BI_Country__c  = pais, BI_Validador_Fiscal__c='Testx006',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');
        Account a7 = new Account(Name='Testx007', BI_Country__c  = pais, BI_Validador_Fiscal__c='Testx007',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');
        Account a8 = new Account(Name='Testx008', BI_Country__c  = pais, BI_Validador_Fiscal__c='Testx008',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');
        Account a9 = new Account(Name='Testx009', BI_Country__c  = pais, BI_Validador_Fiscal__c='Testx009',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');
        Account a10 = new Account(Name='Testx010', BI_Country__c  = pais, BI_Validador_Fiscal__c='Testx010',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');

        aList.add(a1);
        aList.add(a2);
        aList.add(a3);
        aList.add(a4);
        aList.add(a5);
        aList.add(a6);
        aList.add(a7);
        aList.add(a8);
        aList.add(a9);
        aList.add(a10);

        insert aList;
        Integer count = [select count() from Account where Name like '%Testx%' ];
        System.assertEquals(10, count);


        //Load the Stage Table
        List<BI_Registros_Datos_FOCO_Stage__c> sList = new List<BI_Registros_Datos_FOCO_Stage__c> ();

        aList = [Select BI_Validador_Fiscal__c from Account];

        Opportunity o = new Opportunity(AccountId =aList[0].Id, Name='Testx001', CloseDate=Date.newInstance(2036, 01, 05), Stagename=Label.BI_F6Preoportunidad, RecordTypeId=[SELECT Id from RecordType where name = 'Oportunidad Local'][0].Id, BI_Probabilidad_de_exito__c='75', Probability=75,BI_Fecha_de_vigencia__c=Date.newInstance(2016, 01, 06),BI_Country__c  = pais, CurrencyIsoCode='ARS', BI_Comienzo_estimado_de_facturacion__c=Date.newInstance(2016, 01, 07), BI_Duracion_del_contrato_Meses__c=3);
        insert o;

        for(Account a: aList){
            BI_Registros_Datos_FOCO_Stage__c s = new BI_Registros_Datos_FOCO_Stage__c(BI_Cliente__c=a.BI_Validador_Fiscal__c, BI_Monto__c='1000', BI_Country__c='Testx001', BI_Periodo__c='1/1/2016', BI_Tipo__c='Backlog (Recurrente)');

            sList.add(s);
        }

        //Bad amt for creating tyoe Exception
        sList[sList.size()-1].BI_Monto__c = '1ooo';

        insert sList;
        Test.startTest();


        //insert records into main table from stage
        try{
            BI_FOCOBatchInsert batchIns = new BI_FOCOBatchInsert('Testx001');
            Id insBatchId = Database.executeBatch(batchIns, Integer.valueOf(cs.Batch_Size__c));
        }
        catch(BI_FOCO_Exception e){
            System.debug('------------------------------Exception caught: '+e);
        }
        Test.stopTest();

        //count = [select count() from BI_Registros_Datos_FOCO_Stage__c where BI_Cliente__c like '%Testx%'];
        //System.assertEquals(9, count);

        
    }

    @isTest static void test_method_two() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_FOCO_Pais__c cs = new BI_FOCO_Pais__c(Name='Testx001',Batch_Size__c=10, Currency_ISO_Code__c='ARS', Opportunity_Probability__c=50, Notify_Email_List__c='testing@somebody.com'); 
        upsert cs;
        //Region__c pais = new Region__c(CurrencyIsoCode = 'ARS', Name='Testx001');
        //upsert pais;

        List<String> lstPickvals = new List<String>();
        Schema.DescribeFieldResult fieldResult = Account.BI_Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry a : ple)
            lstPickvals.add(a.getValue());

        BI_Periodo__c per = new BI_Periodo__c(BI_Mes_Ano__c='01-2015', Name='Enero 2015');
        upsert per;

        BI_Rama__c rama = new BI_Rama__c(BI_Nombre__c='Testx001');
        upsert rama;    

        Account a1 = new Account(Name='Testx001', BI_Country__c = lstPickvals[0], BI_Validador_Fiscal__c='Testx001',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');
        insert a1;

        List<Account>  aList = [Select BI_Validador_Fiscal__c from Account];
        List<BI_Registros_Datos_FOCO_Stage__c> sList = new List<BI_Registros_Datos_FOCO_Stage__c> ();

        for(Account a: aList){
            BI_Registros_Datos_FOCO_Stage__c s = new BI_Registros_Datos_FOCO_Stage__c(BI_Cliente__c=a.BI_Validador_Fiscal__c, BI_Monto__c='1000', BI_Country__c='Testx001', BI_Periodo__c='1/1/2015', BI_Tipo__c='Backlog (Recurrente)');

            sList.add(s);
        }
        insert sList;
        Test.startTest();
        //insert records into main table from stage
        try{
            BI_FOCOSBatchDelete batchDel = new BI_FOCOSBatchDelete('Testx001', 'BI_Registro_Datos_FOCO__c');
        Id delBatchId = Database.executeBatch(batchDel, 1);
        }
        catch(BI_FOCO_Exception e){
            System.debug('------------------------------Exception caught: '+e);
        }
        Test.stopTest();
    }


    @isTest static void test_method_three() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        BI_FOCO_Pais__c cs = new BI_FOCO_Pais__c(Name='Testx001',Batch_Size__c=10, Currency_ISO_Code__c='ARS', Opportunity_Probability__c=50, Notify_Email_List__c='testing@somebody.com'); 
        upsert cs;
        //Region__c pais = new Region__c(CurrencyIsoCode = 'ARS', Name='Testx001');
        //upsert pais;

        List<String> lstPickvals = new List<String>();
        Schema.DescribeFieldResult fieldResult = Account.BI_Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry a : ple)
            lstPickvals.add(a.getValue());

        BI_Periodo__c per = new BI_Periodo__c(BI_Mes_Ano__c='01-2015', Name='Enero 2015');
        upsert per;

        BI_Rama__c rama = new BI_Rama__c(BI_Nombre__c='Testx001');
        upsert rama;    

        Account a1 = new Account(Name='Testx001', BI_Country__c = lstPickvals[0], BI_Validador_Fiscal__c='Testx001',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');
        insert a1;

        List<Account>  aList = [Select BI_Validador_Fiscal__c from Account];
        List<BI_Registros_Datos_FOCO_Stage__c> sList = new List<BI_Registros_Datos_FOCO_Stage__c> ();

        for(Account a: aList){
            BI_Registros_Datos_FOCO_Stage__c s = new BI_Registros_Datos_FOCO_Stage__c(BI_Cliente__c=a.BI_Validador_Fiscal__c, BI_Monto__c='1000', BI_Country__c='Testx001', BI_Periodo__c='1/1/2021', BI_Tipo__c='Backlog (Recurrente)');

            sList.add(s);
        }
        insert sList;
        Test.startTest();
        //insert records into main table from stage
        try{
            BI_FOCOBatchInsert batchIns = new BI_FOCOBatchInsert('Testx001');
            Id insBatchId = Database.executeBatch(batchIns, Integer.valueOf(cs.Batch_Size__c));
        }
        catch(BI_FOCO_Exception e){
            System.debug('------------------------------Exception caught: '+e);
        }
        Test.stopTest();

    }

    @isTest static void test_method_four() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_FOCO_Pais__c focoPais;
        BI_Periodo__c periodo;
        BI_Rama__c rama;
        List<BI_FOCO_Pais__c> focoPaisList = new List<BI_FOCO_Pais__c>();
        Set<String> countriesSet = new Set<String>();
        BI_FOCOBatchInsert batchIns;

        countriesSet.add('Argentina');
        countriesSet.add('Centroamérica');
        countriesSet.add('Chile');
        countriesSet.add('Colombia');
        countriesSet.add('Costa Rica');
        countriesSet.add('Ecuador');
        countriesSet.add('El Salvador');
        countriesSet.add('Guatemala');
        countriesSet.add(Label.BI_Mexico);
        countriesSet.add('Nicaragua');
        countriesSet.add(Label.BI_Panama);
        countriesSet.add(Label.BI_Peru);
        countriesSet.add('Uruguay');
        countriesSet.add('Venezuela');

        for(String country : countriesSet) {
            focoPais = new BI_FOCO_Pais__c(Name = country, Batch_Size__c = 10, Currency_ISO_Code__c = 'USD', Opportunity_Probability__c = 50, Notify_Email_List__c = 'testing@somebody.com'); 
            focoPaisList.add(focoPais);
        }

        insert focoPaisList;

        periodo = new BI_Periodo__c(Name = 'Enero 2015', BI_Mes_Ano__c = '01-2015');
        insert periodo;

        rama = new BI_Rama__c(BI_Nombre__c = 'Test_Rama');
        insert rama;

        Test.startTest();
        batchIns = new BI_FOCOBatchInsert('Argentina');
        batchIns = new BI_FOCOBatchInsert('Centroamérica');
        batchIns = new BI_FOCOBatchInsert('Chile');
        batchIns = new BI_FOCOBatchInsert('Colombia');
        batchIns = new BI_FOCOBatchInsert('Costa Rica');
        batchIns = new BI_FOCOBatchInsert('Ecuador');
        batchIns = new BI_FOCOBatchInsert('El Salvador');
        batchIns = new BI_FOCOBatchInsert('Guatemala');
        batchIns = new BI_FOCOBatchInsert(Label.BI_Mexico);
        batchIns = new BI_FOCOBatchInsert('Nicaragua');
        batchIns = new BI_FOCOBatchInsert(Label.BI_Panama);
        batchIns = new BI_FOCOBatchInsert(Label.BI_Peru);
        batchIns = new BI_FOCOBatchInsert('Uruguay');
        batchIns = new BI_FOCOBatchInsert('Venezuela');
        Test.stopTest();
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}