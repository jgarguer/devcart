/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Marta Laliena
    Company:        Deloitte
    Description:    SIGMA -> SF mWan integration

    History
    <Date>          <Author>                                        <Change Description>
    23-Jun-2015     David Carnicer                                  Initial Version
    01-Jul-2015     Ana Cirac                                       Version 1.0
    21-Jul-2015     Marta Laliena                                   Version 2.0
    18-sep-2015     jlopezmanrique                                  Cambiado campo formula TGS_RFS_date__c por campo fecha TGS_RFS_date_CI__c de NE__OrderItem__c
    29-sep-2015     jlopezmanrique                                  Revertido Cambio de campo formula TGS_RFS_date__c por campo fecha TGS_RFS_date_CI__c de NE__OrderItem__c
    05-oct-2015     Marta Laliena                                   Añadido código para tratar el Data_Template 'Split Tunneling'
    14-oct-2015     Marta Laliena                                   Añadido código para tratar el Resiliency profile. Añadir dichos campos al custom setting TGS_Mapping_mWanSU
    16-oct-2015     Marta Laliena                                   Multicurrency y assetización orden de modificación
    19-ene-2016     F. Arteaga                                      Decrease number of soql queries
    18-feb-2016     P. Oliva                                
    19-feb-2016     M. Garcia                                       Do not proccess 'Remove' CIs when defining attributes
    03-mar-2016     F. Arteaga                                      Save billing end date to a static variable to be used when creating termination case
    06-jun-2016     Bruno Aliaga                                    Actualización SW para que se calcule la suma del precio total solo con los mensajes Create de SIGMA WH
    07-jun-2016     F. Arteaga                                      Get products from TGSOL Catalog
    21-jun-2016     F. Arteaga                                      Add Internet Access to mapChargesDataTemplateInt
    28-jun-2016     J.Galindo                                       Exclude some products from MWan price calculation
    04-jul-2016     J.Galindo                                       Exclude Internet Access Price in products list for not including in MWan price calculation
    07-Jul-2016     P.Castillo                                      Fill CSUID old for mWan modifications/terminations
    11-Jul-2016     P.Castillo                                      Fix over last modification to avoid dereference null object (~771)
    18-Jul-2016     P.Castillo                                      Set case status as Closed if resolve message is related to Termination order
    21-Sep-2016     J.M. Fierro                                     Modified spacing in some methods, fields and properties, in the hopes of improving readability
    21-Sep-2016     J.M. Fierro                                     Modified modifyData for when resolving a case allow more products to be sent then originally in the order
    29-Sep-2016     J.M. Fierro                                     Commented diagnostic messages from response message in creationCIs and modifyData
    29-Sep-2016     J.M. Fierro                                     Fill in TGS_Holding_Name__c when creating orders
    04-Oct-2016     Álvaro López                                    Added "Customer Loopback" as a map's key mapping_ip_address
    14-Oct-2016     Álvaro López                                    setParentChildrenRelationships method optimization
    13-Oct-2016     Álvaro López                                    Added System.debugs in modifyData method    
    18-Oct-2016     Álvaro López                                    Added control filter to mapOrderAccessCIs loop in modifyData method
    18-Oct-2016     Álvaro López                                    Added control filter to retramitationCIs method
    20-Oct-2016     J.M. Fierro                                     Added error logging
    27-Oct-2016     Álvaro López                                    Added control filter to mapOrderCPECIs loop in modifyData method
    20-Jan-2017     Guillermo Muñoz and Jose Miguel Fierro          Class overwriten
    17-Mar-2017     Jose Miguel Fierro                              Avoid -if possible- returning unhandled exceptions.
    19-Abr-2017     Jose Miguel Fierro                              Add more conditions to order generation
    28-Jul-2017     Álvaro López                                    Changed attribute name from "SIGMA WH Adminitrative Number" to "SIGMA WH Administrative Number"
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

global with sharing class TGS_Salesforce_Ordering_Global{
    global with sharing class AttachmentReq{ 
        webservice String Attachment_ID;
        webservice String Attachment_Name;
        webservice Blob Attachment_Data;
    }

    global with sharing class AttachmentList{ 
        webservice List<AttachmentReq> attachment; 
    }

    global with sharing class OutputMapping6{
        webservice String                Error_Code;
        webservice String                Error_Msg;
        webservice String                Operation_Instance_ID; // CaseNumber
        webservice Operation_StatusType  Operation_Status;
        webservice String                SRM_Request_ID;        // ParentOI.Name
        webservice String                Incident_ID;           // CaseNumber
    }

    public enum Operation_StatusType {Processed, Cancelled, Idle}

    public static Date billingStartDate;
    public static Date billingEndDate;
    public static String use_case_aux;

    @testVisible class RequestInfo extends TGS_ErrorIntegrations_Helper.RequestInfo  {
        public String System_ID;
        public String Use_Case;
        public String Operation_Number;
        public String External_ID;
        public String Attributtes;
        public String ListAttributes;
        public AttachmentList Attachments;
    }

    webservice static OutputMapping6 Creation(
        String System_ID,
        String Use_Case,
        String Operation_Number,
        String External_ID,
        String Attributtes,
        String ListAttributes,
        AttachmentList Attachments
    ){
        OutputMapping6 ret = new OutputMapping6();
        Savepoint svp = Database.setSavepoint();
        RequestInfo incomingXML = new RequestInfo();
        incomingXML.System_ID        = System_ID;        incomingXML.Use_Case       = Use_Case;
        incomingXML.Operation_Number = Operation_Number; incomingXML.External_ID    = External_ID;
        incomingXML.Attributtes      = Attributtes;      incomingXML.ListAttributes = ListAttributes;
        incomingXML.Attachments      = Attachments;
        use_case_aux = Use_Case;

        String recordIdentifier;
        try {
            try {
                TGS_RecordTypes_Util.loadRecordTypes(new List<Schema.SObjectType> { Account.SObjectType, Case.SObjectType, NE__Product__c.SObjectType, NE__Order__c.SObjectType, NE__OrderItem__c.SObjectType, NE__DynamicPropertyDefinition__c.SObjectType });
                    
                TGS_Salesforce_Ordering_Global_Helper.processAttachments(Attachments.attachment);
                Map <String, List <TGS_Salesforce_Ordering_Global_Helper.Wrapper_CI>> map_products = TGS_Salesforce_Ordering_Global_Helper.processListAttributes(ListAttributes);
                String service_name = TGS_Salesforce_Ordering_Global_Helper.processDataTemplate(map_products);
                String divisa = TGS_Salesforce_Ordering_Global_Helper.processCharges(service_name, map_products);
                if(divisa == null){
                    divisa = 'EUR';
                }
                // JMF 19-Abr-2017 - Se necesita use_case también para diferenciar lo que se debe hacer
                NE__Order__c order = TGS_Salesforce_Ordering_Global_Helper.generateOrder(Use_Case, Attributtes, service_name, map_products, divisa, External_ID);
                TGS_Salesforce_Ordering_Global_Helper.translateProducts(map_products, divisa);
    
                recordIdentifier = order.TGS_SIGMA_ID__c + ' | ' + map_products.get('Parent')[0].attributes.get('SIGMA WH Administrative Number');
    
                for(String k : map_products.keySet()) {
                    System.debug('.* ' + k);
                    for(TGS_Salesforce_Ordering_Global_Helper.Wrapper_CI ci : map_products.get(k)){
                        for(String str : ci.attributes.keySet()){
                            System.debug('.*********' + str + '---->' + ci.attributes.get(str));
                        }
                        System.debug('***************************');
                    }
    
                }
    
                System.debug(LoggingLevel.ERROR, 'map_products: ' + JSON.serialize(map_products));
    
                // Prevent this from running; We do not need the case's service to be updated
                NETriggerHelper.setTriggerFired('NECheckOrderItems.TGS_NEOrderItemMethods.setCaseService');
    
                /*
                    Estados válidos para cada operación:
                        - createOrder:   caso nuevo; se actualiza inmediatamente a 'In Progress'
                        - retramitation: caso en 'In Progress'; no cambia el estado del caso
                        - resolveOrder:  caso en 'In Progress'; sobre una terminación actualiza el caso a 'Closed', en los demás a 'Resolved'
                        - modifyData:    caso en 'Resolved', 'Closed'; no cambia el estado del caso
                        - modifySLA:     caso en 'Resolved','Closed'; no cambia el estado del caso
                        - modifyContractual: N/A; crea un caso nuevo que automáticamente se progresa a 'Closed'
                */
                if(Use_Case == 'createOrder') {
                    System.debug('##*** createOrder');
                    ret = TGS_Salesforce_Ordering_Global_Helper.createOrder(map_products, order, divisa, true);
                } else if(Use_Case == 'resolveOrder') {
                    ret = TGS_Salesforce_Ordering_Global_Helper.resolveCase(order, map_products);
                    System.debug('##*** resolveOrder');
                } else if(Use_Case == 'modifyData') {
                    ret = TGS_Salesforce_Ordering_Global_Helper.modifyData(order, map_products);
                    System.debug('##*** modifyData');
                } else if(Use_Case == 'retramitation') {
                    ret = TGS_Salesforce_Ordering_Global_Helper.retramitation(order, map_products);
                } else if(Use_Case == 'modifySLA') {
                    ret = TGS_Salesforce_Ordering_Global_Helper.modifySLA(order, map_products);
                } else if(Use_Case == 'modifyContractual') {
                    ret = TGS_Salesforce_Ordering_Global_Helper.modifyContractual(map_products, order, divisa);
                } else {            
                    inflateErrorResponse(ret, '11420', null);
                }
    
                /*System.debug(LoggingLevel.FINE, '##** ' + order.Id);
                System.debug(LoggingLevel.FINE, '##*** ' + order);
    
                System.debug(LoggingLevel.FINE, '.** CIs **.');
                for(String prod : map_products.keyset()) {
                    System.debug(LoggingLevel.FINE, '.**** ' + prod);
                    for(TGS_Salesforce_Ordering_Global_Helper.Wrapper_CI ci : map_products.get(prod)) {
                        System.debug(LoggingLevel.FINE, '.****** OI.Curr: ' + ci.oi.CurrencyISOCode + ' OI.NRC: ' + ci.oi.NE__OneTimeFeeOv__c + ' OI.MRC: ' + ci.oi.NE__RecurringChargeOv__c);
                        System.debug(LoggingLevel.FINE, '.****** Catalog Item: ' + ci.sourceId);
                        for(String k : ci.attributes.keySet()) {
                            System.debug(LoggingLevel.FINE, '.****** ' + k + ': ' + ci.attributes.get(k));
                        }
                        System.debug(LoggingLevel.FINE, '.**********.');
                    }
                }*/
    
                if(ret.Error_Code != null && ret.Error_Code != '0') {
                    Database.rollback(svp);
                    ret.Operation_Instance_ID = null;
                    ret.SRM_Request_ID = null;
                    ret.Incident_ID = null;
                    // This will not have a stacktrace
                    TGS_ErrorIntegrations_Helper.createExceptionLog('SigmaWH', Use_Case, new IntegrationException(ret.Error_Msg), incomingXML, recordIdentifier);
                } else {
                    ret.Operation_Status = Operation_StatusType.Processed;
                    recordIdentifier += ' | ' + ret.Incident_ID + ' | ' +   ret.SRM_Request_ID;
                }
    
            } catch (Exception exc) {
                Database.rollback(svp);
                System.debug(LoggingLevel.ERROR, 'ERROR: ' + exc.getMessage() + '\nAt: ' + exc.getStackTraceString());
                if(inflateErrorResponse(ret, exc.getMessage(), null).Error_Code == null) {
                    inflateErrorResponse(ret, '11015', exc.getMessage());
                }
                ret.Operation_Instance_ID = null;
                ret.SRM_Request_ID = null;
                ret.Incident_ID = null;
                TGS_ErrorIntegrations_Helper.createExceptionLog('SigmaWH', Use_Case, exc, incomingXML, recordIdentifier);
            }
    
            //// Insert status
            
            if(ret.Error_Code == '0') {
                TGS_ErrorIntegrations_Helper.createSuccessLog('SigmaWH', Use_Case, incomingXML, recordIdentifier);
            }
        } catch(Exception exc) {
            Database.rollback(svp);
            inflateErrorResponse(ret, '11015', exc.getMessage());
            ret.Operation_Instance_ID = null;
            ret.SRM_Request_ID = null;
            ret.Incident_ID = null;
            TGS_ErrorIntegrations_Helper.createExceptionLog('SigmaWH', Use_Case, new IntegrationException('Unexpected error', exc), incomingXML, recordIdentifier);
            System.debug('Exception: ' + exc.getMessage());
            System.debug('Stacktrace: ' + exc.getStackTraceString());
        }
        

        return ret;
    }



    public static OutputMapping6 inflateErrorResponse(OutputMapping6 res, String ecode, String additionalInfo) {
        res.Operation_Status = Operation_StatusType.Cancelled;

        if(ecode == '11420'){////////////////////////done
            res.Error_Code = '11420';
            res.Error_Msg = 'Error(s) while checking the mandatory fields: Use_Case (invalid operation)';
        } else if(ecode == '54321'){
            res.Error_Code = '54321';
            res.Error_Msg = 'Invalid customer name';
        } else if(ecode == '54322'){
            res.Error_Code = '54322';
            res.Error_Msg = 'Invalid contact';
        } else if(ecode == '54323'){
            res.Error_Code = '54323';
            res.Error_Msg = 'The order\'s case was not created';
        } else if(ecode == '54324'){
            res.Error_Code = '54324';
            res.Error_Msg = 'Invalid external id';
        } else if(ecode == '54325'){
            res.Error_Code = '54325';
            res.Error_Msg = 'Invalid SIGMA WH Administrative Number';
        } else if(ecode == '54326'){
            res.Error_Code = '54326';
            res.Error_Msg = 'The order\'s configurations were not created';
        } else if(ecode == '54327'){
            res.Error_Code = '54327';
            res.Error_Msg = 'The order has no CIs';
        } else if(ecode == '11004'){
            res.Error_Code = '11004';
            res.Error_Msg = 'CI class not found';
        } else if(ecode == ('12022')){
            res.Error_Code = '12022';
            res.Error_Msg = 'Unable to find the attachment '+additionalInfo+' in the system';
        } else if(ecode == '12021'){
            res.Error_Code = '12021';
            res.Error_Msg = 'Attachments size limit exceeded';
        } else if(ecode == '12007'){
            res.Error_Code = '12007';
            res.Error_Msg = 'The next attributes don\'t belong to this service: '+additionalInfo;
        } else if(ecode == '11009'){////////////////////////done
            res.Error_Code = '11009';
            res.Error_Msg = 'There is a request in progress with this CI';
        } else if(ecode == '54328'){////////////////////////done
            res.Error_Code = '54328';
            res.Error_Msg = 'Previous case asset missing';
        } else if(ecode == '54329'){
            res.Error_Code = '54329';
            res.Error_Msg = 'Case status invalid for this operation';
        } else if (ecode == '11015'){
            res.Error_Code = '11015';
            res.Error_Msg = 'An unexpected error has occurred while parsing the information: '+additionalInfo;
        }
        return res;
    }
}