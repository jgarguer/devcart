@isTest(seeAllData = false)
public class BIIN_UNICA_ContactHandler_TEST 
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        José Luis González Beltrán
  Company:       HPE
  Description:   Test method to manage the code coverage for BIIN_UNICA_ContactHandler
  
  <Date>             <Author>                        			<Change Description>
  20/06/2016         José Luis González Beltrán           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
  private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
  private final static String LE_NAME = 'TestAccount_ContactHandler_TEST';
  private static final String CONTACT_EMAIL = 'Test_UNICA_ContactHandler_TEST@email.com';
  private static final String CONTACT_EMAIL_1 = 'Test_UNICA_ContactHandler_TEST_1@email.com';

  @testSetup static void dataSetup() 
  {
  	Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
    insert account;

    Contact con = BIIN_UNICA_TestDataGenerator.generateContact(CONTACT_EMAIL, account.Id);
    insert con;

    Contact conNoSede = BIIN_UNICA_TestDataGenerator.generateContact(CONTACT_EMAIL_1, account.Id);
    insert conNoSede;

    BI_Punto_de_instalacion__c sede = BIIN_UNICA_TestDataGenerator.generateSede('Test_Sede_ContactHandler_TEST', con.Id, account.Id);
    insert sede;
  }

  @isTest static void test_BIIN_UNICA_ContactHandler_TEST()
  {
  	Contact con = [SELECT Id, FirstName, LastName, BI_Activo__c, Email FROM Contact WHERE Email =: CONTACT_EMAIL LIMIT 1];
  	Contact conNoSede = [SELECT Id, FirstName, LastName, BI_Activo__c, Email FROM Contact WHERE Email =: CONTACT_EMAIL_1 LIMIT 1];

  	String conStr = JSON.serialize(con);
    con.Email = 'Test_UNICA_ContactHandler_TEST_UPDATE@email.com';
    String conDifMailStr = JSON.serialize(con);
  	String conNoSedeStr = JSON.serialize(conNoSede);

  	Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA(''));
    Test.startTest();

    BIIN_UNICA_ContactHandler contactHandler = new BIIN_UNICA_ContactHandler();
    
    contactHandler.bulkBefore();
    contactHandler.bulkAfter();
    contactHandler.beforeInsert('');
    contactHandler.beforeUpdate('', '');
    contactHandler.beforeDelete('');
    BIIN_UNICA_ContactHandler.afterInsert(conStr);
    BIIN_UNICA_ContactHandler.afterInsert(conNoSedeStr);
    BIIN_UNICA_ContactHandler.afterUpdate(conStr, conDifMailStr);
    contactHandler.afterDelete('');
    contactHandler.andFinally();
        
    Test.stopTest();
  }
}