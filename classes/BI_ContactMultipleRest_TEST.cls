@isTest(seeAllData=true)
private class BI_ContactMultipleRest_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_ContactMultipleRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    15/09/2014              Pablo Oliva             Initial Version
    10/11/2015		    Fernando Arteaga	    BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_ContactMultipleRest.getMultipleContacts()
    History:
    
    <Date>              <Author>                <Description>
    15/09/2014          Pablo Oliva             Initial version
    10/11/2015		Fernando Arteaga	BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getMultipleContactsTest() {

        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }
        
        //Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        //BI_Code_ISO__c region = biCodeIso.get('ARG');                                                
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        List <Contact> lst_con = BI_DataLoadRest.loadContacts(200, lst_acc);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/accountresources/v1/contacts';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        BI_TestUtils.throw_exception = true;
        
        //INTERNAL_SERVER_ERROR
        BI_ContactMultipleRest.getMultipleContacts();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c  where BI_Asunto__c = 'BI_ContactMultipleRest.getMultipleContacts' order by CreatedDate DESC limit 1];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        //BAD REQUEST missing required parameters
        BI_TestUtils.throw_exception = false;
        BI_ContactMultipleRest.getMultipleContacts();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //BAD REQUEST countryISO
        req.addParameter('accountId', lst_acc[0].BI_Id_del_cliente__c);
        req.addParameter('status', 'active');
        RestContext.request = req;
        BI_ContactMultipleRest.getMultipleContacts();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //OK
        RestContext.request.addHeader('countryISO', 'ARG');
        BI_ContactMultipleRest.getMultipleContacts();
        system.assertEquals(200,RestContext.response.statuscode);
        
        //NOT FOUND
        req.requestURI = '/accountresources/v1/contacts';  
        req.httpMethod = 'GET';
        req.addParameter('accountId', 'aaa');

        RestContext.request = req;
        RestContext.response = res;
        
        BI_ContactMultipleRest.getMultipleContacts();
        system.assertEquals(404,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_ContactMultipleRest.getMultipleContacts()
    History:
    
    <Date>              <Author>                <Description>
    16/09/2014          Pablo Oliva             Initial version
    10/11/2015		Fernando Arteaga		BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getMultipleContactsCustomerTest() {

        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }
        
        //Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        //BI_Code_ISO__c region = biCodeIso.get('ARG');
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        
        List <Contact> lst_con = BI_DataLoadRest.loadContacts(200, lst_acc);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/accountresources/v1/contacts';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        BI_TestUtils.throw_exception = false;
        
        //OK
        req.addParameter('customerId', lst_acc[0].BI_Id_del_cliente__c);
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', 'ARG');
        
        BI_ContactMultipleRest.getMultipleContacts();
        
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_ContactMultipleRest.createContact()
    History:
    
    <Date>              <Author>                <Description>
    15/09/2014          Pablo Oliva             Initial version
    10/11/2015		Fernando Arteaga		BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
	04/05/2017			Marta Glez				REING- Reingenieria de contactos--> nationalIDType y nationalID se deben informar
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createContactTest() {

        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }
        
        //Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        //BI_Code_ISO__c region = biCodeIso.get('ARG');                                              
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/accountresources/v1/contacts';  
        req.httpMethod = 'POST';
        
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        
        //BAD REQUEST countryISO
        BI_ContactMultipleRest.createContact();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //INTERNAL_SERVER_ERROR
        Blob body = Blob.valueof('{"accountDetails": }}');
        req.requestBody = body;

        RestContext.request = req;
        //RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        RestContext.request.addHeader('countryISO', 'ARG');
        BI_ContactMultipleRest.createContact();
        
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c where BI_Asunto__c = 'BI_ContactMultipleRest.createContact' order by CreatedDate DESC limit 1];
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals(1, lst_log.size());
        
        //MISSING REQUIRED FIELDS
        //body = Blob.valueof('{ "roles": ["Administrativo"], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+region.BI_Country_ISO_Code__c+'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": [ {"key": "xxx", "value": "yyy"}], "accounts": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ] }');
        //REING-INI body = Blob.valueof('{ "roles": ["Administrativo"], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+ 'ARG' +'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": [ {"key": "xxx", "value": "yyy"}], "accounts": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ] }');
        body = Blob.valueof('{ "roles": ["Administrativo"], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "nationalIDType": "Otros", "nationalID": "1234567", "country": "'+ 'ARG' +'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": [ {"key": "xxx", "value": "yyy"}], "accounts": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ] }');
        
        req.requestBody = body;
        RestContext.request = req;
        BI_ContactMultipleRest.createContact();
        
        system.assertEquals(400,RestContext.response.statuscode);
        
        //OK
        //body = Blob.valueof('{ "roles": ["Administrativo"], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+region.BI_Country_ISO_Code__c+'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": [ { "key": "contactId", "value": "aaa" } ], "accounts": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ] }');
         //REING-INI body = Blob.valueof('{ "roles": ["Administrativo"], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+ 'ARG' +'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": [ {"key": "xxx", "value": "yyy"}], "accounts": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ] }');
        body = Blob.valueof('{ "roles": ["Administrativo"], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "nationalIDType": "Otros", "nationalID": "1234567", "country": "'+ 'ARG' +'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": [ { "key": "contactId", "value": "aaa" } ], "accounts": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ] }');
        req.requestBody = body;
        RestContext.request = req;
        BI_ContactMultipleRest.createContact();
        system.assertEquals(201,RestContext.response.statuscode);
        
        //NOT FOUND
        //body = Blob.valueof('{ "roles": ["Administrativo"], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+region.BI_Country_ISO_Code__c+'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": [ { "key": "contactId", "value": "aaa" } ], "accounts": [ "bbb" ] }');
         //REING-INI body = Blob.valueof('{ "roles": ["Administrativo"], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+ 'ARG' +'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": [ {"key": "xxx", "value": "yyy"}], "accounts": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ] }');
        body = Blob.valueof('{ "roles": ["Administrativo"], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "nationalIDType": "Otros", "nationalID": "1234567", "country": "'+ 'ARG' +'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": [ { "key": "contactId", "value": "aaa" } ], "accounts": [ "bbb" ] }');
        req.requestBody = body;
        RestContext.request = req;
        
        BI_ContactMultipleRest.createContact();
        system.assertEquals(404,RestContext.response.statuscode);
        
        Test.stopTest();
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_ContactMultipleRest.createContact()
    History:
    
    <Date>              <Author>                <Description>
    15/09/2014          Pablo Oliva             Initial version
    10/11/2015		Fernando Arteaga		BI_EN - CSB Integration (adapt to UNICA Draft10B_5Oct2015)
	04/05/2017			Marta Glez				REING- Reingenieria de contactos--> nationalIDType y nationalID se deben informar
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void createContactCustomerTest() {
        
        TGS_User_Org__c userTGS = TGS_User_Org__c.getInstance(UserInfo.getUserId());
        if(userTGS.TGS_Is_BI_EN__c == false || userTGS.TGS_Is_TGS__c == true){

            userTGS.TGS_Is_BI_EN__c = true;
            userTGS.TGS_Is_TGS__c = false;

            if(userTGS.Id != null){
                update userTGS;
            }
            else{
                insert userTGS;
            }
        }
        
        //Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        	
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        //BI_Code_ISO__c region = biCodeIso.get('ARG');                                                
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, true);
        
        List <Contact> lst_con = BI_DataLoadRest.loadContacts(1, lst_acc);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/accountresources/v1/contacts/'+lst_con[0].BI_Id_del_contacto__c;  
        req.httpMethod = 'PUT';
        
        //Blob body = Blob.valueof('{ "roles": ["Administrativo"], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+region.BI_Country_ISO_Code__c+'" }], "customers": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ], "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": [ { "key": "contactId", "value": "aaa" } ], "accounts": null }');
        //REING-INI body = Blob.valueof('{ "roles": ["Administrativo"], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "docType": "Otros", "docNumber": "1234567", "country": "'+ 'ARG' +'" }], "customers": null, "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": [ {"key": "xxx", "value": "yyy"}], "accounts": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ] }');
        Blob body = Blob.valueof('{ "roles": ["Administrativo"], "contactStatus": "active", "name": {"familyName": "Test", "givenName": "contact"}, "legalId": [{ "nationalIDType": "Otros", "nationalID": "1234567", "country": "'+ 'ARG' +'" }], "customers": [ "'+lst_acc[0].BI_Id_del_cliente__c+'" ], "contactingModes": [ { "details": "rafael.garciamunoz@telefonica.com", "contactMode": "email" }, { "details": "0039000930", "contactMode": "phone" }, { "details": "0039000930", "contactMode": "fax" }, { "details": "0039000930", "contactMode": "mobile" } ], "addresses": [ { "region": "Buenos Aires", "postalCode": "1107", "normalized": null, "locality": "Ciudad de Buenos Aires", "isDangerous": null, "floor": null, "country": "Argentina", "coordinates": { "longitude": null, "latitude": null }, "borough": null, "apartment": null, "addressNumber": null, "addressName": "Comunicación, 33", "additionalData": null } ], "additionalData": [ { "key": "contactId", "value": "aaa" } ], "accounts": null }');
        req.requestBody = body;
        
        RestContext.request = req;
        RestContext.response = res;
        
        //RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        RestContext.request.addHeader('countryISO', 'ARG');
        
        Test.startTest();
        
        //OK
        BI_ContactMultipleRest.createContact();
        system.assertEquals(201,RestContext.response.statuscode);
        
        Test.stopTest();
    
    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}