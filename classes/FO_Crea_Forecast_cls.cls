/************************************************
 * Author: Javier Lopez Andradas
 * Company: gCTIO
 * Description:  
 * 
 * **********************************************/
public class FO_Crea_Forecast_cls {
    /******************************************************
     * Author: Javier Lopez Andradas
     * Company: gCTIO
     * Description: retrieves the actual period for doing the forecast
     * 
     * 
     * ******************************************************/
    @AuraEnabled
    public static BI_Periodo__c getPer(){
        BI_Periodo__c per = [Select Id,NAme from BI_Periodo__c where FO_Type__c='Semana' AND BI_FVI_Fecha_Inicio__c<=:Date.today() AND BI_FVI_Fecha_Fin__c>=:DAte.today() limit 1];
        System.debug(per);
        return per;
    }
    /***************************************************
     * Author; Javier Lopez Andradas
     * Company:gCTIO
     * Description: retrieves all the Nodes that the actual user is the owner
     * 
     * *************************************/
    @AuraEnabled
    public static List<BI_FVI_Nodos__c> getNodes(){
        List<BI_FVI_Nodos__c> ls_nd =  [Select Name, Id from BI_FVI_Nodos__c where OwnerId=:UserInfo.getUserId()];
        return ls_nd;
    }
}