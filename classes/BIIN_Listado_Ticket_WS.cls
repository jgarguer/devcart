public class BIIN_Listado_Ticket_WS extends BIIN_UNICA_Base
{
/*----------------------------------------------------------------------------------------------------------------------
    Author:        José María Martín
    Company:       Deloitte
    Description:   

    <Date>                  <Author>                        <Change Description>
    11/2015                 José María Martín               Initial Version
    07/04/2016              José Luis González              Adapt to UNICA modifications
    01/07/2016              José Luis González              Fix field values
----------------------------------------------------------------------------------------------------------------------*/
  public TicketSalida listadoTicket(String authorizationToken, Case caso, DateTime fechaInicio, DateTime fechaFin) 
  {
    system.debug('BIIN_Listado_Ticket_WS.listadoTicket: Caso: ' + caso);
    // Prepare URL
    String url = 'callout:BIIN_WS_UNICA/ticketing/v1/tickets';

    // Add parameters
    if (String.isNotBlank(caso.BI_Id_del_caso_Legado__c))
    {
      // If ID provided, no need to fill rest of params
      url = BIIN_UNICA_Utils.addParameterAdditional(url, 'IncidentNumber', caso.BI_Id_del_caso_Legado__c);
    }
    else
    {
      // If ID is not provided, then client is mandatory 
      if (String.isEmpty(caso.accountId))
      {
        throw new IntegrationException('Debe proveer al menos el ID del caso ó el Cliente');
      }

      // Get account
      Account account = getAccount(caso.accountId);
      if (account != null)
      {
        url = BIIN_UNICA_Utils.addParameter(url, 'customerId', account.BI_Validador_Fiscal__c);
      }

      url = BIIN_UNICA_Utils.addParameter(url, 'ticketStatus', caso.Status);
      url = BIIN_UNICA_Utils.addParameter(url, 'priority', BIIN_UNICA_Utils.translateFromRemedyValueToRemedyID('Prioridad', caso.Priority));
      url = BIIN_UNICA_Utils.addParameterAdditional(url, 'InitialReportedDate', BIIN_UNICA_Utils.dateTimeToSeconds(fechaInicio));
      url = BIIN_UNICA_Utils.addParameterAdditional(url, 'EndedReportedDate', BIIN_UNICA_Utils.dateTimeToSeconds(fechaFin));

      url = BIIN_UNICA_TicketManager.getAdditionalParamsURL(url, caso);
    }

    // Prepare Request
    HttpRequest req = getRequest(url, 'GET');

    // Call and Get Response
    BIIN_UNICA_Pojos.ResponseObject response = getResponse(req, BIIN_UNICA_Pojos.TicketsListType.class);
    BIIN_UNICA_Pojos.TicketsListType ticketsListType = (BIIN_UNICA_Pojos.TicketsListType) response.responseObject;

    // Convert to expected SF types
    return convert(ticketsListType);

  }

  private TicketSalida convert(BIIN_UNICA_Pojos.TicketsListType ticketsListType)
  {
    TicketSalida ticketSalida = new TicketSalida();
    ticketSalida.outputs = new Outputs();
    ticketSalida.outputs.output = new List<Output>();

    if(ticketsListType != null && ticketsListType.ticketList != null)
    {
      for (BIIN_UNICA_Pojos.TicketDetailType ticketDetailType : ticketsListType.ticketList)
      {      
        // Add new element
        Output out = new Output();
        ticketSalida.outputs.output.add(out);

        // Read additional fields
        Map<String, String> mapa = BIIN_UNICA_Utils.additionalDataToMap(ticketDetailType.additionalData);

        // Set data
        out.BAO_CaseNumberID = ticketDetailType.correlatorId;
        out.incidentNumber = ticketDetailType.ticketId;
        out.Priority = BIIN_UNICA_Utils.translateFromRemedyByID('Prioridad', ticketDetailType.priority);
        out.reportedDate = ticketDetailType.reportedDate;
        out.lastModifiedDate = ticketDetailType.statusChangeDate;
        out.Description = ticketDetailType.subject;
        String cleanStatus = ticketDetailType.ticketStatus.name().replaceAll('IN_PROGRESS', 'IN PROGRESS').replaceAll('NEW_STAUS', 'NEW');
        out.Status = BIIN_UNICA_Utils.translateFromRemedy('Estado', cleanStatus);
        out.SLMStatus = BIIN_UNICA_Utils.translateFromRemedy('SLM-Status', mapa.get('slmStatus'));
        out.Company = TicketDetailType.customerId;
        out.Site = mapa.get('site');
        out.HPDCI = mapa.get('ci');
        out.assignedGroup = !String.isEmpty(ticketDetailType.responsibleParty) ? ticketDetailType.responsibleParty : '';
      }
    }

    return ticketSalida;
  }


  // ############## POJOS VIEJOS ############## 
  public class TicketSalida 
  {
    public Outputs outputs;
  }

  public class Outputs
  {
    public List<Output> output;
  }

  public class Output 
  {
    public String incidentNumber;
    public String BAO_CaseNumberID;
    public String Company;
    public String Priority;
    public String reportedDate;
    public String reportedSource;
    public String lastModifiedDate;
    public String Site;
    public String HPDCI;
    public String Description;
    public String assignedGroup;
    public String SLMStatus;
    public String Status;
  }

}