/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis 
    Company:       Everis
    Description:   Test Methods executed to test CWP_Catalogo_Producto_Controller.cls 
    Test Class:    
    
    History:
     
    <Date>                  <Author>                <Change Description>
    29/03/2016              Everis                   Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class CWP_LHelper_TEST {
  
    
    @testSetup 
    private static void dataModelSetup() {  
         
         //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
        
            set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
           //ACCOUNT
        map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
            
          
         //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        insert contactTest;
        
        User usuario;
              
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
    
         Market_Place_PP__c newMarketplace = new Market_Place_PP__c();
        insert newMarketplace;
        /*
        BI_Contact_Customer_Portal__c newRecordCCP = new BI_Contact_Customer_Portal__c();
        insert newRecordCCP;*/
        
        BI_Catalogo_PP__c ProductoJSON = new BI_Catalogo_PP__c();
        ProductoJSON.BI_Familia_del_producto__c=newMarketplace.Id;
        ProductoJSON.Name = 'JSONproduct';
        insert ProductoJSON;
        
        NE__Product__c product;
        
         System.runAs(new User(id=userInfo.getUserId())){
         product = CWP_TestDataFactory.createCommercialProduct('producto1'); 
         insert product;
            } 
             
               
    }

     @isTest
    private static void createTaskTest() {
        BI_TestUtils.throw_exception=false; 
        system.debug('test 1');
         
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];   
        NE__Product__c producto = [SELECT id FROM NE__Product__c WHERE Name =: 'producto1'];
        BI_Catalogo_PP__c ProductJSON = [SELECT id FROM BI_Catalogo_PP__c WHERE Name =: 'JSONproduct'];
      
        String test1 = producto.id;
        String test2 = JSON.serialize(ProductJSON);
       
        
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_LHelper.calendarEventWrap wr= new CWP_LHelper.calendarEventWrap(usuario.id,'','');
            Event theEvent= new Event();
            theEvent.startDateTime=System.now();
            theEvent.endDateTime=System.now();
             CWP_LHelper.calendarEventWrap wr2= new CWP_LHelper.calendarEventWrap(theEvent,false);
            
            
            CWP_LHelper.inventoryWrap wrI= new CWP_LHelper.inventoryWrap('p',usuario.id,'desc',5);
            wrI.addAmount();
            Test.stopTest();
        }       
        
    } 



}