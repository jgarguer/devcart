public class TGS_Billing_Queueable implements Queueable{
   private Case newCase;

   public TGS_Billing_Queueable(Case caseN ){
        newCase =	caseN;
        
   }
   public void execute(QueueableContext context){
       
       TGS_CallRodWs.inFutureContextCI = true;
    // JCT 27/09/2016 Commented IF condition due to generate CSUID in every type of order, regardless of its Service.
       // PCP 07/07/2016
    /*if (newCase.Type != Constants.TYPE_DISCONNECT || // FAR 06/06/2016 - Don't generate CSUIDs if case type is Disconnect
		  (newCase.Type == Constants.TYPE_DISCONNECT && 
		  (newCase.TGS_Service__c.equals(Constants.PRODUCT_MWAN_SU) || newCase.TGS_Service__c.equals(Constants.PRODUCT_MWAN_INTERNET))))//PCP 07/07/2016 - Allow call TGS_fill_CSUID class also if disconnect and service mWan
		{
      //15/09/2016 JCT Added TGS_fill_CSUID.fill_CSUID_EnterpriseID method call instead of csuid() method call.
      //25/09/2016 JCT Changed TGS_fill_CSUID.fill_CSUID_EnterpriseID method call for TGS_fill_CSUID.fill_EnterpriseID(Trigger.newMap); call
      //               Also Added TGS_fill_CSUID.csuid(newCase); method call.
			Map<Id,Case> aux_map = new Map<Id,Case>();
      aux_map.put(newCase.Id,newCase);
      TGS_fill_CSUID.fill_EnterpriseID(aux_map);
      TGS_fill_CSUID.csuid(newCase);
    }*/


    Map<Id,Case> aux_map = new Map<Id,Case>();
    aux_map.put(newCase.Id,newCase);
    //27/092016 JCT  Changed the csuid method call to get a TGS_fill_CSUID.fill_EnterpriseID(Trigger.newMap) call as parameter after the 
    //               csuid method definition reported on TGS_fill_CSUID class.
    TGS_fill_CSUID.csuid(newCase,TGS_fill_CSUID.fill_EnterpriseID(aux_map));
       TGS_Billing_Date.setCiBillingDate(newCase);
       TGS_CallRodWs.inFutureContextCI = false;
        
    }  
    	
}