public class PCA_External_B2W_Controller {    
    //Datos de constantes
    public final static String H1_DEV_NAME = 'TGS_Holding'; // Será el Holding
    public final static String H2_DEV_NAME = 'TGS_Customer_Country'; // Será el Customer Country
    
    //Datos de contacto
    public User AU {get; set;}
    public integer rTypeAU{get; set;}
    
    //Datos de los picklist
    public String holdingSelect {get; set;}
    public String holdingSelectName {get; set;}    
    public List<SelectOption> userList {get; set;}
    public String userSelect {get; set;}
    public List<SelectOption> payerList {get; set;}
    public String payerSelect {get; set;}
    public List<SelectOption> siteList {get; set;}
    public String siteSelect {get; set;}
    public String endSite {get; set;}
    public Boolean mandatorySite{get; set;}
    public String serviceUnit{get; set;}
    public String none{get{
        return Label.TGS_CWP_NONE;
    }set;}
    
    //Datos del cId / UserId
    public String cId{get; set;}
    public String suid{get; set;}

    public Id userId{get; set;}
    private String accId;

    public NE__Order__c myOrder{get; set;}
    public String quantity{get; set;}
    public Boolean bacc{get; set;}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Description:  Controller of the external B2W view
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_External_B2W_Controller() {
        quantity = '1';
        myOrder = new NE__Order__c();
        
        cId = ApexPages.currentPage().getParameters().get('cId');
        suid = ApexPages.currentPage().getParameters().get('suId');
        
        System.debug('[PCA_External_B2W_Controller Constructor] cId: ' + cId);
        System.debug('[PCA_External_B2W_Controller Constructor] suid: ' + suid);
       
        /*mandatorySite = false;
        mandatorySite  = 0<[SELECT Id 
                                    FROM NE__Catalog_Item__c 
                                    WHERE Id =: cid AND NE__Technical_Behaviour__c INCLUDES ('Sitio requerido')].size();*/
        mandatorySite = true; // JMF - 11/08/2016
        
        System.debug('[PCA_External_B2W_Controller Constructor] mandatorySite: ' + mandatorySite);
           
        userId = UserInfo.getUserId();
        
        System.debug('[PCA_External_B2W_Controller Constructor] userId: ' + userId);
        
        //Loading Holding User: Used in the Site Lookup
        Account accHolding = TGS_Portal_Utils.getLevel1(userId, 0)[0];
        accId = accHolding.Id;
        
        System.debug('[PCA_External_B2W_Controller Constructor] accId: ' + accId);
         
        if(accId != null) {
            myOrder.TGS_Holding_Name__c = accHolding.Name;
            Account acc = TGS_Portal_Utils.getAccount(accId);
            myOrder.NE__AccountId__c = accId;
            myOrder.HoldingId__c = acc.HoldingId__c;
        }  
        
        System.debug('[PCA_External_B2W_Controller Constructor] myOrder: ' + myOrder);
        
        //Inicializamos todos los datos necesarios sobre el contact
        loadHolding();
        loadUser();
        loadPayer();
    }
    
    /**
     * Method:         loadHolding
     * Description:    Carga los datos del holding
     */
    public void loadHolding(){
        Account auxAC = TGS_Portal_Utils.getUserAccount(userId);
        holdingSelect = auxAC.Id;
        holdingSelectName = auxAC.Name;
        
        System.debug('[PCA_External_B2W_Controller.loadHolding] holdingSelect: ' + holdingSelect);
        System.debug('[PCA_External_B2W_Controller.loadHolding] holdingSelectName: ' + holdingSelectName);
    }

    /**
     * Method:         loadUser
     * Description:    Carga los datos del usuario
     */
    public void loadUser(){
        userList = new List<SelectOption>();
        userList.add(new SelectOption('', Label.TGS_CWP_NONE));

        userSelect = '';
        payerSelect = '';
        siteSelect = '';
        
        if(holdingSelect != '' && holdingSelect != null){        
            for (Account acc : TGS_Portal_Utils.getLevel4(userId,0)) {
                userList.add(new SelectOption(acc.Id, acc.Name));
            }
        }
        
        System.debug('[PCA_External_B2W_Controller.loadUser] userList: ' + userList);
        
    }
    
    /**
     * Method:         loadPayer
     * Description:    Carga los datos del payer
     */
    public void loadPayer(){
        try {
            payerList = new List<SelectOption>();
            
            myOrder.Business_Unit__c = userSelect;
            
            if(userSelect != '' && userSelect != null){
                List<Account> accsLevel5 = TGS_Portal_Utils.getLevel5(userSelect,1);
                if (accsLevel5.size() > 1) { payerList.add(new SelectOption('', Label.TGS_CWP_NONE));   }
                for (Account acc : accsLevel5 ) {
                    payerList.add(new SelectOption(acc.Id, acc.Name));
                }
            }
            
            System.debug('[PCA_External_B2W_Controller.loadPayer] payerList: ' + payerList);
    
        } catch (Exception e) {
            System.debug('[PCA_External_B2W_Controller.loadPayer] ERROR: ' + e);
        }
    }
    
    //BOTONES
    /**
     * Method:          cancelar
     * Description:     Método llamado cuando se pulsa el botón Cancel.
     */
    public PageReference cancel() {
        String fId = [SELECT NE__ProductId__r.TGS_CWP_Tier_1__c FROM NE__Catalog_Item__c WHERE Id =: cId LIMIT 1][0].NE__ProductId__r.TGS_CWP_Tier_1__c;
        
        System.debug('[PCA_External_B2W_Controller.cancel] fId: ' + fId);
        PageReference pr = new PageReference('/empresasplatino/PCA_Installed_Services');
        pr.getParameters().put('fId', fId);
        pr.setRedirect(true);
        return pr;
    }
    
    /**
     * Method:          nextStep
     * Description:     Método llamado cuando se pulsa el botón Next.
     */
    public PageReference nextStep() {
         Integer x = 0;
        try {
            x = Integer.valueOf(quantity);
        }
        Catch (exception e) {
            return null;
        }
        if(x<=0){
            return null;
        }
        
        
        
        if (myOrder.Site__c != null) siteSelect = myOrder.Site__c;
        System.debug('[PCA_External_B2W_Controller.nextStep] siteSelect: ' + siteSelect);
        
        //String userId = UserInfo.getUserId();
        Account userAC = TGS_Portal_Utils.getUserAccount(UserInfo.getUserId());
        
        System.debug('[PCA_External_B2W_Controller.nextStep] userAC: '+ userAC);
        /*
        AU = [SELECT Name, ContactId,Contact.BI_Cuenta_activa_en_portal__c, AccountId FROM User WHERE Id =:UserInfo.getUserId()]; 
        
        System.debug('[PCA_External_B2W_Controller.nextStep] Authorized User: '+ AU);
        
        myOrder.Authorized_User__c=AU.Id;
        insert myOrder;
        */
        System.debug('[PCA_External_B2W_Controller.nextStep] myOrder: '+ myOrder);
        
        PageReference pr = new PageReference('/empresasplatino/PCA_Order_Redirect?fromSummary=true&cId=' + cId +'&accId=' + userAC.Id + '&servAccId=' + userSelect + '&billAccId=' + payerSelect + 
                                             '&site=' + siteSelect + '&quantity=' + x + '&cType=New' + '&isExtBw=true');
        
        pr.setRedirect(true); 
        
        System.debug('[PCA_External_B2W_Controller.nextStep] pr: '+ pr);
        
        return pr;          
    }
}