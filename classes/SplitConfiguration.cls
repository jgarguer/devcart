//ET 11/06/2014
//31/05/2017		Cristina Rodriguez Commented DB_Competitor__c (Opportunity) from SELECTs
global with sharing class SplitConfiguration {


	WebService static String splitFailedItems(String orderID)
	{
		Opportunity optyOld;
		List<NE__Order__c> orderOld;
		List<NE__OrderItem__c> ord_it;
		String newOrdId = '';
		String errorCode;
		List<NE__OrderItem__c> ord_it_correct;
		List<NE__OrderItem__c> ord_it_failed;
		List<NE__Order__c> ordNew;
		List<NE__Order__c> ordNew2;
		List<NE__OrderItem__c> orderItem_ko;
		List<NE__OrderItem__c> orderItem_ok;
		String urlPage= null;
		Map<String, String> mapOfOOpty;
		Opportunity newOpty;
		List<OpportunityLineItem> stdProdToIns;
		RecordType recType;
		RecordType optyRecType;
		Savepoint savePointCheckout = Database.setSavepoint();

		try
		{
			recType		 = [SELECT Id FROM RecordType WHERE (SobjectType = 'Order__c' OR SobjectType = 'NE__Order__c') AND Name = 'Opty' limit 1];
			optyRecType	 = [SELECT Id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'BI_Renegociacion' limit 1];


			orderOld		=	 [SELECT id,SumOfNoProcOI__c,SumOfOI__c, Name, NE__OrderStatus__c, NE__Version__c, NE__OptyId__c,NE__OpportunityId__c, currencyIsoCode, (SELECT id, Name FROM NE__Orders_Header__r),
									 (SELECT id, Name, NE__FulfilmentStatus__c, NE__Status__c FROM NE__Order_Items__r)
									 FROM NE__Order__c
									 WHERE id = :orderID AND NE__OrderStatus__c != 'Pending' AND NE__OrderStatus__c='Sent' order by NE__Version__c desc LIMIT 1];

			if(orderOld.size() > 0 && orderOld[0].SumOfNoProcOI__c > 0)
			{

				optyOld		 =	 [SELECT AccountId, BI_Acuerdo_Marco__c, Amount, BI_Analisis_de_riesgo__c, BI_Bloqueo_de_la_oportunidad__c, Certa_SCP__Buying_Cycle__c, CampaignId, Certa_SCP__BIG__c, Certa_SCP__Estimated_FCV__c, Certa_SCP__Total_Forecasted_FCV__c, Certa_SCP__Full_Contract_Value__c, Certa_SCP__Estimated_FCV_Calculated__c, Certa_SCP__Calculated_Win_Probability__c, BI_Categoria_de_la_oferta__c, BI_Causas_de_retroceso__c, BI_Ciclo_ventas__c, Certa_SCP__Ciclo_ventas__c, CloseDate, IsClosed, BI_ColorEstado1__c, BI_ColorEstado2__c, BI_ColorEstado3__c, BI_ColorEstado4__c, BI_ColorEstado5__c, BI_Comienzo_estimado_de_facturacion__c, BI_Comite_de_negocios__c, NE__Contract_Duration_Months__c, Certa_SCP__contract_duration_months__c, BI_ControlBotonDuplicar__c, BI_ControlBotonRenegociacion__c, CreatedById, CreatedDate, IsDeleted, BI_Delta__c, Description, BI_Descuento__c, BI_Descuento_bloqueado__c, Certa_SCP__Duracion_proyecto_meses__c, BI_Duracion_del_contrato_Meses__c, BI_Estado__c, Certa_SCP__Estimated_Billing_Start_Date__c, Certa_SCP__Fecha_Fin_Plan_Comercial__c, BI_Fecha_inicio_contrato__c, BI_Fecha_de_vigencia__c, Certa_SCP__Fecha_estimada_aceptacion__c, BI_Fecha_estimada_de_aceptacion__c, Certa_SCP__Estimated_end_of_contract__c, BI_Fecha_de_instalacion__c, BI_Fecha_de_fin_de_contrato__c, Fiscal, FiscalQuarter, FiscalYear, ForecastCategory, ForecastCategoryName, Generate_Acuerdo_Marco__c, HasOpportunityLineItem, NE__HaveActiveLineItem__c, Certa_SCP__CIF_Cliente__c, BI_Ingreso_por_unica_vez__c, Certa_SCP__Ingresos_Netos_a_o_fiscal__c, Certa_SCP__Ingr_Netos_A_o_fiscal__c, Certa_SCP__Ingr_netos_Primeros_12_meses__c, Certa_SCP__Ingresos_Netos_PONDERADOS_1_s_12m__c, Certa_SCP__Estimated_Year_End_Revenue__c, Certa_SCP__Weighted_Estimated_Year_End_Revenue__c, Certa_SCP__Ingr_Brutos_ponderados_1_s_12_meses__c, Certa_SCP__Estimaci_n_Ingresos_primeros_12_meses__c, NE__Order_Generated__c, LastActivityDate, LastModifiedById, LastModifiedDate, LeadSource, BI_Legalizada__c, Certa_SCP__MainCompetitors__c, BI_Motivo_de_perdida__c, Name, NextStep, BI_Nivel_de_aprobacion__c, Certa_SCP__Notas_adicionales__c, BI_Oferta_economica__c, BI_Oferta_tecnica__c, NE__One_Time_Fee__c, Certa_SCP__Unique_Charges__c, BI_Oportunidad_Padre__c, BI_Oportunidad_origen__c, CurrencyIsoCode, Id, Type, NE__OrderNumber__c, BI_Origen_de_la_oferta_tecnica__c, BI_Otro_motivo_de_cancelacion__c, OwnerId, Pedido__c, BI_Presupuesto_Licitacion__c, Pricebook2Id, Probability, BI_Producto_bloqueado__c, RecordTypeId, BI_Recurrente_bruto_mensual__c, BI_Recurrente_bruto_mensual_anterior__c, Certa_SCP__Cargo_recurrente_anterior__c, NE__Recurring_Charge__c, BI_Requiere_contrato__c, StageName, SystemModstamp, BI_Tipo_de_renovacion__c, Certa_SCP__Monthly_Charges__c, NE__TrackingNumber__c, BI_Ultrarapida__c, Certa_SCP__Valor_Total_del_Contrato_calculado__c, Certa_SCP__Unsold_Pipeline__c, BI_Venta_Bruta_Total_FCV_estimado__c, Certa_SCP__Valor_Total_Contrato_Venta_Neta__c, Certa_SCP__Venta_Neta_PONDERADA__c, IsWon, BI_Es_posible_crear_una_nueva_version__c
										FROM Opportunity WHERE id = : orderOld[0].NE__OptyId__c LIMIT 1];
										//DB_Competitor__c,

				//generate new order
				NE.DataMap.DataMapRequest dReq = new NE.DataMap.DataMapRequest();
				dReq.mapName = 'Order2Order';
				dReq.sourceId = orderID;
				Map<String,String> mapOfq = new Map<String,String>();
				list<String> listOfph = new list<String>();
				listOfph.add('No procesado'); //KO

				String customQuery = 'NE__Status__c =: {!0}'; //get the 0 index -> give me the 'No procesado' of the place holder
				mapOfq.put('NoPromoOrderItem',customQuery);
				mapOfq.put('PromoOrderItem',customQuery);
				mapOfq.put('OrderItem',customQuery);

				dReq.mapOfCustomQueries = mapOfq;
				dReq.listOfPlaceHolders = listOfph;

				NE.DataMap obj = new NE.DataMap();
				//call dataMap
				NE.DataMap.DataMapResponse dResp = obj.callDataMap(dReq);

				if(dResp.ErrorCode.equalsIgnoreCase('0'))
				{

					newOrdId = dResp.ParentId;
					ordNew = [SELECT id, NE__OpportunityId__c, NE__OptyId__c, Name, NE__Version__c,
							 (Select id, name, NE__ProdId__r.NE__ForecastProd__c,
							 NE__RecurringChargeOv__c,NE__RecurringChargeFrequency__c,NE__Onetimefeeov__c, CurrencyIsoCode,
							 NE__Qty__c,NE__Root_Order_Item__r.NE__Qty__c,NE__OrderItemPromoId__r.NE__Qty__c, NE__CatalogItem__r.NE__Currency__c
							 from NE__Order_items__r)
							 FROM NE__Order__c WHERE id =:newOrdId];

					ordNew[0].NE__Version__c	= 1;
					ordNew[0].RecordTypeId	 = recType.Id;//Schema.SObjectType.NE__Order__c.RecordTypeInfosByName.get('Opty').RecordTypeId;

					newOpty			 = optyOld.clone(false);
					newOpty.Name		= optyOld.Name+'_Regularización';
					newOpty.StageName = 'F6 - Prospecting';
					newOpty.RecordTypeId= optyRecType.id;
					newOpty.Note__c	 = 'Generado por regularización: Aprovisionamiento Fallido' ;
					newOpty.Pedido__c = null;
					insert newOpty;

					ordNew[0].NE__OptyId__c		 = newOpty.Id;
					ordNew[0].NE__OpportunityId__c = newOpty.Id;
					ordNew[0].NE__OrderStatus__c	= 'Active';

					update ordNew;
				}
			}
		}
		catch(Exception e)
		{
			DataBase.rollback(savePointCheckout);
			newOrdId = 'Error';
		}

		return newOrdId;

	}

	WebService static String splitSuccessedItems(String orderID)
	{
		Opportunity optyOld;
		List<NE__Order__c> orderOld;
		List<NE__OrderItem__c> ord_it;
		String newOrdId = '';
		String errorCode;
		List<NE__OrderItem__c> ord_it_correct;
		List<NE__OrderItem__c> ord_it_failed;
		List<NE__Order__c> ordNew;
		List<NE__Order__c> ordNew2;
		List<NE__OrderItem__c> orderItem_ko;
		List<NE__OrderItem__c> orderItem_ok;
		String urlPage= null;
		Map<String, String> mapOfOOpty;
		Opportunity newOpty;
		List<OpportunityLineItem> stdProdToIns;
		RecordType recType;
		RecordType optyRecType;
		Savepoint savePointCheckout = Database.setSavepoint();

		try
		{

			recType		 = [SELECT Id FROM RecordType WHERE (SobjectType = 'Order__c' OR SobjectType = 'NE__Order__c') AND Name = 'Opty' limit 1];
			optyRecType	 = [SELECT Id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'BI_Renegociacion' limit 1];


			orderOld		=	 [SELECT id,SumOfNoProcOI__c,SumOfOI__c, Name, NE__OrderStatus__c, NE__Version__c, NE__OptyId__c, NE__OpportunityId__c, currencyIsoCode, (SELECT id, Name FROM NE__Orders_Header__r),
									 (SELECT id, Name, NE__FulfilmentStatus__c, NE__Status__c FROM NE__Order_Items__r)
									 FROM NE__Order__c
									 WHERE id = :orderID AND NE__OrderStatus__c != 'Pending' AND NE__OrderStatus__c='Sent' order by NE__Version__c desc LIMIT 1];

			optyOld		 =	 [SELECT AccountId, BI_Acuerdo_Marco__c, Amount, BI_Analisis_de_riesgo__c, BI_Bloqueo_de_la_oportunidad__c, Certa_SCP__Buying_Cycle__c, CampaignId, Certa_SCP__BIG__c, Certa_SCP__Estimated_FCV__c, Certa_SCP__Total_Forecasted_FCV__c, Certa_SCP__Full_Contract_Value__c, Certa_SCP__Estimated_FCV_Calculated__c, Certa_SCP__Calculated_Win_Probability__c, BI_Categoria_de_la_oferta__c, BI_Causas_de_retroceso__c, BI_Ciclo_ventas__c, Certa_SCP__Ciclo_ventas__c, CloseDate, IsClosed, BI_ColorEstado1__c, BI_ColorEstado2__c, BI_ColorEstado3__c, BI_ColorEstado4__c, BI_ColorEstado5__c, BI_Comienzo_estimado_de_facturacion__c, BI_Comite_de_negocios__c, NE__Contract_Duration_Months__c, Certa_SCP__contract_duration_months__c, BI_ControlBotonDuplicar__c, BI_ControlBotonRenegociacion__c, CreatedById, CreatedDate, IsDeleted, BI_Delta__c, Description, BI_Descuento__c, BI_Descuento_bloqueado__c, Certa_SCP__Duracion_proyecto_meses__c, BI_Duracion_del_contrato_Meses__c, BI_Estado__c, Certa_SCP__Estimated_Billing_Start_Date__c, Certa_SCP__Fecha_Fin_Plan_Comercial__c, BI_Fecha_inicio_contrato__c, BI_Fecha_de_vigencia__c, Certa_SCP__Fecha_estimada_aceptacion__c, BI_Fecha_estimada_de_aceptacion__c, Certa_SCP__Estimated_end_of_contract__c, BI_Fecha_de_instalacion__c, BI_Fecha_de_fin_de_contrato__c, Fiscal, FiscalQuarter, FiscalYear, ForecastCategory, ForecastCategoryName, Generate_Acuerdo_Marco__c, HasOpportunityLineItem, NE__HaveActiveLineItem__c, Certa_SCP__CIF_Cliente__c, BI_Ingreso_por_unica_vez__c, Certa_SCP__Ingresos_Netos_a_o_fiscal__c, Certa_SCP__Ingr_Netos_A_o_fiscal__c, Certa_SCP__Ingr_netos_Primeros_12_meses__c, Certa_SCP__Ingresos_Netos_PONDERADOS_1_s_12m__c, Certa_SCP__Estimated_Year_End_Revenue__c, Certa_SCP__Weighted_Estimated_Year_End_Revenue__c, Certa_SCP__Ingr_Brutos_ponderados_1_s_12_meses__c, Certa_SCP__Estimaci_n_Ingresos_primeros_12_meses__c, NE__Order_Generated__c, LastActivityDate, LastModifiedById, LastModifiedDate, LeadSource, BI_Legalizada__c, Certa_SCP__MainCompetitors__c, BI_Motivo_de_perdida__c, Name, NextStep, BI_Nivel_de_aprobacion__c, Certa_SCP__Notas_adicionales__c, BI_Oferta_economica__c, BI_Oferta_tecnica__c, NE__One_Time_Fee__c, Certa_SCP__Unique_Charges__c, BI_Oportunidad_Padre__c, BI_Oportunidad_origen__c, CurrencyIsoCode, Id, Type, NE__OrderNumber__c, BI_Origen_de_la_oferta_tecnica__c, BI_Otro_motivo_de_cancelacion__c, OwnerId, Pedido__c, BI_Presupuesto_Licitacion__c, Pricebook2Id, Probability, BI_Producto_bloqueado__c, RecordTypeId, BI_Recurrente_bruto_mensual__c, BI_Recurrente_bruto_mensual_anterior__c, Certa_SCP__Cargo_recurrente_anterior__c, NE__Recurring_Charge__c, BI_Requiere_contrato__c, StageName, SystemModstamp, BI_Tipo_de_renovacion__c, Certa_SCP__Monthly_Charges__c, NE__TrackingNumber__c, BI_Ultrarapida__c, Certa_SCP__Valor_Total_del_Contrato_calculado__c, Certa_SCP__Unsold_Pipeline__c, BI_Venta_Bruta_Total_FCV_estimado__c, Certa_SCP__Valor_Total_Contrato_Venta_Neta__c, Certa_SCP__Venta_Neta_PONDERADA__c, IsWon, BI_Es_posible_crear_una_nueva_version__c
									FROM Opportunity WHERE id = : orderOld[0].NE__OptyId__c LIMIT 1];
									//DB_Competitor__c,

			//generate new order
			NE.DataMap.DataMapRequest dReq2 = new NE.DataMap.DataMapRequest();
			dReq2.mapName = 'Order2Order';
			dReq2.sourceId = orderID;
			Map<String,String> mapOfq2 = new Map<String,String>();
			list<String> listOfph2 = new list<String>();
			listOfph2.add('RFS'); //OK
			listOfph2.add('RFB'); //OK
			listOfph2.add('Procesado con cambios'); //OK

			String customQuery2 = 'NE__Status__c =: {!0} or NE__Status__c =: {!1} or NE__Status__c =: {!2}'; //get the 0 index -> give me the KO of the place holder

			mapOfq2.put('NoPromoOrderItem',customQuery2);
			mapOfq2.put('PromoOrderItem',customQuery2);
			mapOfq2.put('OrderItem',customQuery2);

			dReq2.mapOfCustomQueries = mapOfq2;
			dReq2.listOfPlaceHolders = listOfph2;

			NE.DataMap obj2 = new NE.DataMap();
			//call dataMap
			NE.DataMap.DataMapResponse dResp2;
			if(orderOld[0].SumOfNoProcOI__c != orderOld[0].SumOfOI__c)
				dResp2 = obj2.callDataMap(dReq2);
			else
			{
				dResp2 = new NE.DataMap.DataMapResponse();
				dResp2.ErrorCode	= '0';
				dResp2.ParentId	 = '';
			}

			if(dResp2.ErrorCode.equalsIgnoreCase('0'))
			{
				if(dResp2.ParentId != '' && dResp2.ParentId != ' ')
				{
					//Update the new opty version
					newOrdId = dResp2.ParentId;

					ordNew2 = [SELECT id, Name, NE__Version__c, NE__ServAccId__c, NE__BillAccId__c, NE__OrderStatus__c,NE__AccountId__c, NE__Type__c,
							 NE__ConfigurationStatus__c,NE__OpportunityId__c,NE__OptyId__c, (Select id, name, NE__ProdId__r.NE__ForecastProd__c,
							 NE__RecurringChargeOv__c,NE__RecurringChargeFrequency__c,NE__Onetimefeeov__c, CurrencyIsoCode,
							 NE__Qty__c,NE__Root_Order_Item__r.NE__Qty__c,NE__OrderItemPromoId__r.NE__Qty__c, NE__CatalogItem__r.NE__Currency__c
							 from NE__Order_items__r)
							 FROM NE__Order__c WHERE id =:newOrdId];

					//Update the last version of the opty configuration (if any)
					NE__Order__c ordOldVersion = [SELECT id, Name, NE__Version__c, NE__ServAccId__c, NE__BillAccId__c, NE__OrderStatus__c,NE__AccountId__c, NE__Type__c,NE__ConfigurationStatus__c,NE__OpportunityId__c,NE__OptyId__c
												FROM NE__Order__c
												WHERE RecordType.name = 'Opty' AND NE__OptyId__c =: ordNew2[0].NE__OptyId__c ORDER BY NE__Version__c desc nulls last LIMIT 1];
					ordOldVersion.NE__OrderStatus__c	= 'Revised';
					update ordOldVersion;

					Decimal vers					= ordOldVersion.NE__Version__c;
					ordNew2[0].NE__Version__c	 = vers + 1;
					ordNew2[0].RecordTypeId		 = recType.Id;//Schema.SObjectType.NE__Order__c.RecordTypeInfosByName.get('Opty').RecordTypeId;
					ordNew2[0].NE__OrderStatus__c = 'F1 - Closed Won';
					ordNew2[0].NE__OptyId__c		= orderOld[0].NE__OptyId__c;
					ordNew2[0].NE__OpportunityId__c = orderOld[0].NE__OpportunityId__c;
					update ordNew2;

				}
				else
					newOrdId	= '';

				//Update the order splitted
				orderOld[0].NE__OrderStatus__c = 'No instalado';
				update orderOld[0];

				//Update the original opty
				if(orderOld[0].SumOfNoProcOI__c != orderOld[0].SumOfOI__c)
				{
					optyOld.StageName		 = 'F1 - Closed Won';
					if(optyOld.BI_Requiere_contrato__c)
						optyOld.BI_Legalizada__c	= 'Pendiente de firma';

					//optyOld.Pedido__c = null;
				}
				else
					optyOld.StageName		 = 'F1 - Cancelled | Suspended';

				update optyOld;


				urlPage = optyOld.id;

			}
		}
		catch(Exception e)
		{
			DataBase.rollback(savePointCheckout);
			newOrdId = 'Error';
		}

		return newOrdId;
	}


	WebService static String regeneratePriceDefinitions(String failedOrder, String successedOrder)
	{
		Opportunity optyOld;
		List<NE__Order__c> orderOld;
		List<NE__OrderItem__c> ord_it;
		String optyId = '';
		String errorCode;
		List<NE__OrderItem__c> ord_it_correct;
		List<NE__OrderItem__c> ord_it_failed;
		List<NE__Order__c> ordNew;
		List<NE__Order__c> ordNew2;
		List<NE__OrderItem__c> orderItem_ko;
		List<NE__OrderItem__c> orderItem_ok;
		String urlPage= null;
		Map<String, String> mapOfOOpty;
		Opportunity newOpty;
		List<OpportunityLineItem> stdProdToIns;
		RecordType recType;
		RecordType optyRecType;
		list<String> listofids = new list<String>();
		list<String> listofOps = new list<String>();
		List<NE__Order__c> orderUpd;
		Savepoint savePointCheckout = Database.setSavepoint();

		try
		{
			if(failedOrder != '')
				listofids.add(failedOrder);
			if(successedOrder != '')
				listofids.add(successedOrder);

			orderUpd		=	 [SELECT id,SumOfNoProcOI__c,SumOfOI__c, Name, NE__OrderStatus__c, NE__Version__c, NE__OptyId__c,NE__OpportunityId__c, currencyIsoCode, (SELECT id, Name FROM NE__Orders_Header__r),
									 (SELECT id, Name, NE__FulfilmentStatus__c, NE__Status__c,NE__ProdId__r.NE__ForecastProd__c,
											NE__RecurringChargeOv__c,NE__RecurringChargeFrequency__c,NE__Onetimefeeov__c, CurrencyIsoCode,
											NE__Qty__c,NE__Root_Order_Item__r.NE__Qty__c,NE__OrderItemPromoId__r.NE__Qty__c, NE__CatalogItem__r.NE__Currency__c
											FROM NE__Order_Items__r)
									 FROM NE__Order__c
									 WHERE id IN: listofids];

		 Map<ID, ID> mapOfOptyIDs = new Map<ID, ID>();
		 for(NE__Order__c ord: orderUpd){
				mapOfOptyIDs.put(ord.Id, ord.NE__OptyId__c);
		 }

		 List<Task> tareas = [SELECT id, status, isClosed, WhatId FROM Task WHERE WhatId IN :mapOfOptyIDs.values() AND Type = 'Tarea Obligatoria'];

		 for(NE__Order__c ordertoUpd:orderUpd)
		 {
			 ordertoUpd.NE__One_Time_Fee_Total__c	 = 0;
			 ordertoUpd.NE__Recurring_Charge_Total__c = 0;
			 listofOps.add(ordertoUpd.NE__OptyId__c);
		 }

		 String pBisoCode = 'Standard_' + orderUpd[0].currencyIsoCode;
		 PriceBook2 pricebook;

		 try
		 {
				pricebook = [SELECT Id, Name, (SELECT Id, UnitPrice, Product2Id FROM PriceBookEntries) FROM PriceBook2 WHERE name = :pBisoCode];
		 }
		 catch(Exception e)
		 {
				pricebook = null;
		 }

		 Map<String,String> mapOfEntryProducts	 = new Map<String,String>();

		 if(pricebook!=null)
		 {
				List<PriceBookEntry> entries		 =	pricebook.PriceBookEntries;
				for(PriceBookEntry pbe:entries)
				{
					 mapOfEntryProducts.put(pbe.Product2Id, pbe.Id);

				}
		 }


		 Map < String , Integer>	 freqValues = new Map < String , Integer>();
		 freqValues.put('Monthly',12);
		 freqValues.put('Bi-Monthly',6);
		 freqValues.put('Quarterly',4);
		 freqValues.put('Four-Monthly',3);
		 freqValues.put('Semiannual',2);
		 freqValues.put('Annual',1);

		 //Manage currencies
		 list<CurrencyType>	listOfCurrencies		 =	 [SELECT ConversionRate,CreatedById,CreatedDate,DecimalPlaces,Id,IsActive,IsCorporate,IsoCode,LastModifiedById,LastModifiedDate,SystemModstamp FROM CurrencyType];
		 Map<String,Decimal> mapOfCurrsConversion	 =	 new map<String,Decimal>();

		 for(CurrencyType currObj:listOfCurrencies)
		 {
				 mapOfCurrsConversion.put(currObj.IsoCode,currObj.ConversionRate);
		 }

		 //Find the optys to load
		 map<String, Opportunity> optymap = new map<String, Opportunity>([SELECT (SELECT id FROM OpportunityLineItems), AccountId, BI_Acuerdo_Marco__c, Amount, BI_Analisis_de_riesgo__c, BI_Bloqueo_de_la_oportunidad__c, Certa_SCP__Buying_Cycle__c, CampaignId, Certa_SCP__BIG__c, Certa_SCP__Estimated_FCV__c, Certa_SCP__Total_Forecasted_FCV__c, Certa_SCP__Full_Contract_Value__c, Certa_SCP__Estimated_FCV_Calculated__c, Certa_SCP__Calculated_Win_Probability__c, BI_Categoria_de_la_oferta__c, BI_Causas_de_retroceso__c, BI_Ciclo_ventas__c, Certa_SCP__Ciclo_ventas__c, CloseDate, IsClosed, BI_ColorEstado1__c, BI_ColorEstado2__c, BI_ColorEstado3__c, BI_ColorEstado4__c, BI_ColorEstado5__c, BI_Comienzo_estimado_de_facturacion__c, BI_Comite_de_negocios__c, NE__Contract_Duration_Months__c, Certa_SCP__contract_duration_months__c, BI_ControlBotonDuplicar__c, BI_ControlBotonRenegociacion__c, CreatedById, CreatedDate, IsDeleted, BI_Delta__c, Description, BI_Descuento__c, BI_Descuento_bloqueado__c, Certa_SCP__Duracion_proyecto_meses__c, BI_Duracion_del_contrato_Meses__c, BI_Estado__c, Certa_SCP__Estimated_Billing_Start_Date__c, Certa_SCP__Fecha_Fin_Plan_Comercial__c, BI_Fecha_inicio_contrato__c, BI_Fecha_de_vigencia__c, Certa_SCP__Fecha_estimada_aceptacion__c, BI_Fecha_estimada_de_aceptacion__c, Certa_SCP__Estimated_end_of_contract__c, BI_Fecha_de_instalacion__c, BI_Fecha_de_fin_de_contrato__c, Fiscal, FiscalQuarter, FiscalYear, ForecastCategory, ForecastCategoryName, Generate_Acuerdo_Marco__c, HasOpportunityLineItem, NE__HaveActiveLineItem__c, Certa_SCP__CIF_Cliente__c, BI_Ingreso_por_unica_vez__c, Certa_SCP__Ingresos_Netos_a_o_fiscal__c, Certa_SCP__Ingr_Netos_A_o_fiscal__c, Certa_SCP__Ingr_netos_Primeros_12_meses__c, Certa_SCP__Ingresos_Netos_PONDERADOS_1_s_12m__c, Certa_SCP__Estimated_Year_End_Revenue__c, Certa_SCP__Weighted_Estimated_Year_End_Revenue__c, Certa_SCP__Ingr_Brutos_ponderados_1_s_12_meses__c, Certa_SCP__Estimaci_n_Ingresos_primeros_12_meses__c, NE__Order_Generated__c, LastActivityDate, LastModifiedById, LastModifiedDate, LeadSource, BI_Legalizada__c, Certa_SCP__MainCompetitors__c, BI_Motivo_de_perdida__c, Name, NextStep, BI_Nivel_de_aprobacion__c, Certa_SCP__Notas_adicionales__c, BI_Oferta_economica__c, BI_Oferta_tecnica__c, NE__One_Time_Fee__c, Certa_SCP__Unique_Charges__c, BI_Oportunidad_Padre__c, BI_Oportunidad_origen__c, CurrencyIsoCode, Id, Type, NE__OrderNumber__c, BI_Origen_de_la_oferta_tecnica__c, BI_Otro_motivo_de_cancelacion__c, OwnerId, Pedido__c, BI_Presupuesto_Licitacion__c, Pricebook2Id, Probability, BI_Producto_bloqueado__c, RecordTypeId, BI_Recurrente_bruto_mensual__c, BI_Recurrente_bruto_mensual_anterior__c, Certa_SCP__Cargo_recurrente_anterior__c, NE__Recurring_Charge__c, BI_Requiere_contrato__c, StageName, SystemModstamp, BI_Tipo_de_renovacion__c, Certa_SCP__Monthly_Charges__c, NE__TrackingNumber__c, BI_Ultrarapida__c, Certa_SCP__Valor_Total_del_Contrato_calculado__c, Certa_SCP__Unsold_Pipeline__c, BI_Venta_Bruta_Total_FCV_estimado__c, Certa_SCP__Valor_Total_Contrato_Venta_Neta__c, Certa_SCP__Venta_Neta_PONDERADA__c, IsWon, BI_Es_posible_crear_una_nueva_version__c
												FROM Opportunity WHERE id IN: listofOps]);
		 //DB_Competitor__c,


			for(Opportunity opItemsdel:optymap.values())
			{
				delete opItemsdel.OpportunityLineItems;
			}

			stdProdToIns = new List<OpportunityLineItem>();

			for(NE__Order__c ordToUpd:orderUpd)
			{
				String orderid = ordToUpd.id;
				if(successedOrder == orderid)
					optyId = ordToUpd.NE__OptyId__c;
				else if(successedOrder == '')
					optyId = ordToUpd.NE__OptyId__c;

				Opportunity opp = optymap.get(ordToUpd.NE__OptyId__c);
				for(NE__OrderItem__c oi: ordToUpd.NE__Order_Items__r)
				{
					if(oi.NE__ProdId__r.NE__ForecastProd__c != null)
					{
						String productEntry = mapOfEntryProducts.get(oi.NE__ProdId__r.NE__ForecastProd__c);

						if(productEntry != null)
						{
							OpportunityLineItem l_item		 =		new OpportunityLineItem();
							l_item.OpportunityId			 =		ordToUpd.NE__OptyId__c;

							decimal monthlyOptyItem			=		(oi.NE__RecurringChargeOv__c * freqValues.get(oi.NE__RecurringChargeFrequency__c))/12;
							decimal onetimeoi				 =		oi.NE__Onetimefeeov__c;

							monthlyOptyItem					=		NEBit2winApi.convertCurrency(monthlyOptyItem,oi.CurrencyIsoCode,opp.CurrencyIsoCode,mapOfCurrsConversion);
							onetimeoi						 =		NEBit2winApi.convertCurrency(onetimeoi,oi.CurrencyIsoCode,opp.CurrencyIsoCode,mapOfCurrsConversion);

							l_item.UnitPrice				 =		onetimeoi + (monthlyOptyItem * opp.BI_Duracion_del_contrato_Meses__c);
							l_item.PricebookEntryId			=		productEntry;

							//GC 3.4: calculate the qty
							l_item.Quantity					=		oi.NE__Qty__c;

							if(oi.NE__Root_Order_Item__r.NE__Qty__c != null)
								l_item.Quantity	 =		l_item.Quantity*oi.NE__Root_Order_Item__r.NE__Qty__c;

							if(oi.NE__OrderItemPromoId__r.NE__Qty__c != null)
								l_item.Quantity	 =		l_item.Quantity*oi.NE__OrderItemPromoId__r.NE__Qty__c;


							if(oi.NE__Onetimefeeov__c != null)
							{
								 ordToUpd.NE__One_Time_Fee_Total__c		 += oneTimeoi*oi.NE__Qty__c*l_item.Quantity;
							}
							if(oi.NE__RecurringChargeOv__c != null)
							{
								 ordToUpd.NE__Recurring_Charge_Total__c	 += monthlyOptyItem*freqValues.get(oi.NE__RecurringChargeFrequency__c)* oi.NE__Qty__c*l_item.Quantity;
							}

							stdProdToIns.add(l_item);
						}
				 }
				 else
				 {
				 }
				}

				//edit added 3/7/2014
				List<Task> tklst = new List<Task>();

				for(Task t: tareas){
					if(t.isClosed == false){
						t.status = 'Completada';
						tklst.add(t);
					}
				}
				update tklst;
			}

		 insert stdProdToIns;
		 update orderUpd;

		}
		catch(Exception e)
		{
			DataBase.rollback(savePointCheckout);
			optyId = 'Error';
		}

		return optyId;
	}

	WebService static String splitOrder(String orderID){

		Opportunity optyOld;
		List<NE__Order__c> orderOld;
		List<NE__OrderItem__c> ord_it;
		String newOrdId;
		String errorCode;
		List<NE__OrderItem__c> ord_it_correct;
		List<NE__OrderItem__c> ord_it_failed;
		List<NE__Order__c> ordNew;
		List<NE__Order__c> ordNew2;
		List<NE__OrderItem__c> orderItem_ko;
		List<NE__OrderItem__c> orderItem_ok;
		String urlPage= null;
		Map<String, String> mapOfOOpty;
		Opportunity newOpty;
		List<OpportunityLineItem> stdProdToIns;
		RecordType recType;
		RecordType optyRecType;
		Savepoint savePointCheckout = Database.setSavepoint();

		try{
			recType		 = [SELECT Id FROM RecordType WHERE (SobjectType = 'Order__c' OR SobjectType = 'NE__Order__c') AND Name = 'Opty' limit 1];
			optyRecType	 = [SELECT Id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'BI_Renegociacion' limit 1];


			orderOld		=	 [SELECT id,SumOfNoProcOI__c,SumOfOI__c, Name, NE__OrderStatus__c, NE__Version__c, NE__OptyId__c, currencyIsoCode, (SELECT id, Name FROM NE__Orders_Header__r),
									 (SELECT id, Name, NE__FulfilmentStatus__c, NE__Status__c FROM NE__Order_Items__r)
									 FROM NE__Order__c
									 WHERE id = :orderID AND NE__OrderStatus__c != 'Pending' AND NE__OrderStatus__c='Sent' order by NE__Version__c desc LIMIT 1];

			if(orderOld.size() > 0 && orderOld[0].SumOfNoProcOI__c > 0)
			{

				optyOld		 =	 [SELECT AccountId, BI_Acuerdo_Marco__c, Amount, BI_Analisis_de_riesgo__c, BI_Bloqueo_de_la_oportunidad__c, Certa_SCP__Buying_Cycle__c, CampaignId, Certa_SCP__BIG__c, Certa_SCP__Estimated_FCV__c, Certa_SCP__Total_Forecasted_FCV__c, Certa_SCP__Full_Contract_Value__c, Certa_SCP__Estimated_FCV_Calculated__c, Certa_SCP__Calculated_Win_Probability__c, BI_Categoria_de_la_oferta__c, BI_Causas_de_retroceso__c, BI_Ciclo_ventas__c, Certa_SCP__Ciclo_ventas__c, CloseDate, IsClosed, BI_ColorEstado1__c, BI_ColorEstado2__c, BI_ColorEstado3__c, BI_ColorEstado4__c, BI_ColorEstado5__c, BI_Comienzo_estimado_de_facturacion__c, BI_Comite_de_negocios__c, NE__Contract_Duration_Months__c, Certa_SCP__contract_duration_months__c, BI_ControlBotonDuplicar__c, BI_ControlBotonRenegociacion__c, CreatedById, CreatedDate, IsDeleted, BI_Delta__c, Description, BI_Descuento__c, BI_Descuento_bloqueado__c, Certa_SCP__Duracion_proyecto_meses__c, BI_Duracion_del_contrato_Meses__c, BI_Estado__c, Certa_SCP__Estimated_Billing_Start_Date__c, Certa_SCP__Fecha_Fin_Plan_Comercial__c, BI_Fecha_inicio_contrato__c, BI_Fecha_de_vigencia__c, Certa_SCP__Fecha_estimada_aceptacion__c, BI_Fecha_estimada_de_aceptacion__c, Certa_SCP__Estimated_end_of_contract__c, BI_Fecha_de_instalacion__c, BI_Fecha_de_fin_de_contrato__c, Fiscal, FiscalQuarter, FiscalYear, ForecastCategory, ForecastCategoryName, Generate_Acuerdo_Marco__c, HasOpportunityLineItem, NE__HaveActiveLineItem__c, Certa_SCP__CIF_Cliente__c, BI_Ingreso_por_unica_vez__c, Certa_SCP__Ingresos_Netos_a_o_fiscal__c, Certa_SCP__Ingr_Netos_A_o_fiscal__c, Certa_SCP__Ingr_netos_Primeros_12_meses__c, Certa_SCP__Ingresos_Netos_PONDERADOS_1_s_12m__c, Certa_SCP__Estimated_Year_End_Revenue__c, Certa_SCP__Weighted_Estimated_Year_End_Revenue__c, Certa_SCP__Ingr_Brutos_ponderados_1_s_12_meses__c, Certa_SCP__Estimaci_n_Ingresos_primeros_12_meses__c, NE__Order_Generated__c, LastActivityDate, LastModifiedById, LastModifiedDate, LeadSource, BI_Legalizada__c, Certa_SCP__MainCompetitors__c, BI_Motivo_de_perdida__c, Name, NextStep, BI_Nivel_de_aprobacion__c, Certa_SCP__Notas_adicionales__c, BI_Oferta_economica__c, BI_Oferta_tecnica__c, NE__One_Time_Fee__c, Certa_SCP__Unique_Charges__c, BI_Oportunidad_Padre__c, BI_Oportunidad_origen__c, CurrencyIsoCode, Id, Type, NE__OrderNumber__c, BI_Origen_de_la_oferta_tecnica__c, BI_Otro_motivo_de_cancelacion__c, OwnerId, Pedido__c, BI_Presupuesto_Licitacion__c, Pricebook2Id, Probability, BI_Producto_bloqueado__c, RecordTypeId, BI_Recurrente_bruto_mensual__c, BI_Recurrente_bruto_mensual_anterior__c, Certa_SCP__Cargo_recurrente_anterior__c, NE__Recurring_Charge__c, BI_Requiere_contrato__c, StageName, SystemModstamp, BI_Tipo_de_renovacion__c, Certa_SCP__Monthly_Charges__c, NE__TrackingNumber__c, BI_Ultrarapida__c, Certa_SCP__Valor_Total_del_Contrato_calculado__c, Certa_SCP__Unsold_Pipeline__c, BI_Venta_Bruta_Total_FCV_estimado__c, Certa_SCP__Valor_Total_Contrato_Venta_Neta__c, Certa_SCP__Venta_Neta_PONDERADA__c, IsWon, BI_Es_posible_crear_una_nueva_version__c
										FROM Opportunity WHERE id = : orderOld[0].NE__OptyId__c LIMIT 1];
										//DB_Competitor__c,


				//generate new order
				NE.DataMap.DataMapRequest dReq = new NE.DataMap.DataMapRequest();
				dReq.mapName = 'Order2Order';
				dReq.sourceId = orderID;
				Map<String,String> mapOfq = new Map<String,String>();
				list<String> listOfph = new list<String>();
				listOfph.add('No procesado'); //KO

				String customQuery = 'NE__Status__c =: {!0}'; //get the 0 index -> give me the 'No procesado' of the place holder
				mapOfq.put('NoPromoOrderItem',customQuery);
				mapOfq.put('PromoOrderItem',customQuery);
				mapOfq.put('OrderItem',customQuery);

				dReq.mapOfCustomQueries = mapOfq;
				dReq.listOfPlaceHolders = listOfph;

				NE.DataMap obj = new NE.DataMap();
				//call dataMap
				NE.DataMap.DataMapResponse dResp = obj.callDataMap(dReq);

				if(dResp.ErrorCode.equalsIgnoreCase('0')){

					newOrdId = dResp.ParentId;
					ordNew = [SELECT id, NE__OpportunityId__c, NE__OptyId__c, Name, NE__Version__c,
							 (Select id, name, NE__ProdId__r.NE__ForecastProd__c,
							 NE__RecurringChargeOv__c,NE__RecurringChargeFrequency__c,NE__Onetimefeeov__c, CurrencyIsoCode,
							 NE__Qty__c,NE__Root_Order_Item__r.NE__Qty__c,NE__OrderItemPromoId__r.NE__Qty__c, NE__CatalogItem__r.NE__Currency__c
							 from NE__Order_items__r)
							 FROM NE__Order__c WHERE id =:newOrdId];

					ordNew[0].NE__Version__c	= 1;
					ordNew[0].RecordTypeId	 = recType.Id;//Schema.SObjectType.NE__Order__c.RecordTypeInfosByName.get('Opty').RecordTypeId;

					newOpty			 = optyOld.clone(false);
					newOpty.Name		= optyOld.Name+'_Regularización';
					newOpty.StageName = 'F6 - Prospecting';
					if(!Test.isRunningTest()){
					 newOpty.RecordTypeId= optyRecType.id;
					}

					newOpty.Note__c	 = 'Generado por regularización: Aprovisionamiento Fallido';
					newOpty.Pedido__c = null;
					insert newOpty;

					ordNew[0].NE__OptyId__c		 = newOpty.Id;
					ordNew[0].NE__OpportunityId__c = newOpty.Id;
					ordNew[0].NE__OrderStatus__c	= 'Active';

					update ordNew;
				}


				//generate new order
				NE.DataMap.DataMapRequest dReq2 = new NE.DataMap.DataMapRequest();
				dReq2.mapName = 'Order2Order';
				dReq2.sourceId = orderID;
				Map<String,String> mapOfq2 = new Map<String,String>();
				list<String> listOfph2 = new list<String>();
				listOfph2.add('RFS'); //OK
				listOfph2.add('RFB'); //OK
				listOfph2.add('Procesado con cambios'); //OK

				String customQuery2 = 'NE__Status__c =: {!0} or NE__Status__c =: {!1} or NE__Status__c =: {!2}'; //get the 0 index -> give me the KO of the place holder
				mapOfq2.put('NoPromoOrderItem',customQuery2);
				mapOfq2.put('PromoOrderItem',customQuery2);
				mapOfq2.put('OrderItem',customQuery2);

				dReq2.mapOfCustomQueries = mapOfq2;
				dReq2.listOfPlaceHolders = listOfph2;

				NE.DataMap obj2 = new NE.DataMap();
				//call dataMap
				NE.DataMap.DataMapResponse dResp2 = obj2.callDataMap(dReq2);

				if(dResp2.ErrorCode.equalsIgnoreCase('0'))
				{
					if(dResp2.ParentId != '' && dResp2.ParentId != ' ')
					{
						//Update the new opty version
						newOrdId = dResp2.ParentId;

						ordNew2 = [SELECT id, Name, NE__Version__c, NE__ServAccId__c, NE__BillAccId__c, NE__OrderStatus__c,NE__AccountId__c, NE__Type__c,
								 NE__ConfigurationStatus__c,NE__OpportunityId__c,NE__OptyId__c, (Select id, name, NE__ProdId__r.NE__ForecastProd__c,
								 NE__RecurringChargeOv__c,NE__RecurringChargeFrequency__c,NE__Onetimefeeov__c, CurrencyIsoCode,
								 NE__Qty__c,NE__Root_Order_Item__r.NE__Qty__c,NE__OrderItemPromoId__r.NE__Qty__c, NE__CatalogItem__r.NE__Currency__c
								 from NE__Order_items__r)
								 FROM NE__Order__c WHERE id =:newOrdId];

						Decimal vers					= ordNew2[0].NE__Version__c;
						ordNew2[0].NE__Version__c	 = vers + 1;
						ordNew2[0].RecordTypeId		 = recType.Id;//Schema.SObjectType.NE__Order__c.RecordTypeInfosByName.get('Opty').RecordTypeId;
						ordNew2[0].NE__OrderStatus__c = 'F1 - Closed Won';
						ordNew2[0].NE__OptyId__c = optyOld.Id;
						update ordNew2;

						if(Test.isRunningTest()){
							vers = vers -1;
						}

						//Update the order splitted
						orderOld[0].NE__OrderStatus__c = 'No instalado';
						update orderOld[0];

						if(newOrdId != null || newOrdId != '' || newOrdId != ' ')
							urlPage = optyOld.id;
						else
							urlPage = null;

						//Update the last version of the opty configuration (if any)
						NE__Order__c ordOldVersion = [SELECT id, Name, NE__Version__c, NE__ServAccId__c, NE__BillAccId__c, NE__OrderStatus__c,NE__AccountId__c, NE__Type__c,NE__ConfigurationStatus__c,NE__OpportunityId__c,NE__OptyId__c
													FROM NE__Order__c
													WHERE RecordType.name = 'Opty' AND NE__OptyId__c =: ordNew2[0].NE__OptyId__c AND NE__Version__c =: vers];
						ordOldVersion.NE__OrderStatus__c	= 'Revised';
						update ordOldVersion;

					}

					//Update the original opty
					if(orderOld[0].SumOfNoProcOI__c != orderOld[0].SumOfOI__c)
					{
						optyOld.StageName		 = 'F1 - Closed Won';
						if(optyOld.BI_Requiere_contrato__c)
							optyOld.BI_Legalizada__c	= 'Pendiente de firma';

						//optyOld.Pedido__c = null;
					}
					else
						optyOld.StageName		 = 'F1 - Cancelled | Suspended';

					update optyOld;


					urlPage = optyOld.id;

				}

			 /***Set the opportunity line items***/

			 String pBisoCode = 'Standard_' + orderOld[0].currencyIsoCode;
			 PriceBook2 pricebook;

			 try
			 {
				pricebook = [SELECT Id, Name, (SELECT Id, UnitPrice, Product2Id FROM PriceBookEntries) FROM PriceBook2 WHERE name = :pBisoCode];
			 }
			 catch(Exception e)
			 {
				pricebook = null;
			 }

			 Map<String,String> mapOfEntryProducts	 = new Map<String,String>();

			 if(pricebook!=null){
					List<PriceBookEntry> entries		 =	pricebook.PriceBookEntries;
					for(PriceBookEntry pbe:entries){
						 mapOfEntryProducts.put(pbe.Product2Id, pbe.Id);

					}
			 }


			 Map < String , Integer> freqValues = new Map < String , Integer>();
			 freqValues.put('Monthly',12);
			 freqValues.put('Bi-Monthly',6);
			 freqValues.put('Quarterly',4);
			 freqValues.put('Four-Monthly',3);
			 freqValues.put('Semiannual',2);
			 freqValues.put('Annual',1);

			 //Manage currencies
			 list<CurrencyType>	listOfCurrencies		 =	 [SELECT ConversionRate,CreatedById,CreatedDate,DecimalPlaces,Id,IsActive,IsCorporate,IsoCode,LastModifiedById,LastModifiedDate,SystemModstamp FROM CurrencyType];
			 Map<String,Decimal> mapOfCurrsConversion	 =	 new map<String,Decimal>();

			 for(CurrencyType currObj:listOfCurrencies){
					 mapOfCurrsConversion.put(currObj.IsoCode,currObj.ConversionRate);
			 }

			 List<Opportunity> optyList = new List<Opportunity>();
			 optyList.add(optyOld);
			 optyList.add(newOpty);

			 //delete line items for updated old opty
			 List<OpportunityLineItem> optyToCycle = optyList[0].OpportunityLineItems;
			 if(optyToCycle.size() > 0)
					delete optyToCycle;

			 stdProdToIns = new List<OpportunityLineItem>();

			 List<NE__Order__c> orderUpd = new list<NE__Order__c>();

			 ordNew[0].NE__One_Time_Fee_Total__c	 = 0;
			 ordNew[0].NE__Recurring_Charge_Total__c = 0;

			 update ordNew[0];

			 orderUpd.add(ordNew[0]);

			 if(ordNew2 != null)
			 {
				 ordNew2[0].NE__One_Time_Fee_Total__c	 = 0;
				 ordNew2[0].NE__Recurring_Charge_Total__c = 0;
				 update ordNew2[0];
				 orderUpd.add(ordNew2[0]);
			 }

			 Integer i=0;

			 for(Opportunity opp: optyList)
			 {

					for(NE__OrderItem__c oi: orderUpd[i].NE__Order_Items__r)
					{
						if(oi.NE__ProdId__r.NE__ForecastProd__c != null)
						{
							String productEntry = mapOfEntryProducts.get(oi.NE__ProdId__r.NE__ForecastProd__c);

							if(productEntry != null){
								OpportunityLineItem l_item		 =		new OpportunityLineItem();
								l_item.OpportunityId			 =		opp.Id;

								decimal monthlyOptyItem			=		(oi.NE__RecurringChargeOv__c * freqValues.get(oi.NE__RecurringChargeFrequency__c))/12;
								decimal onetimeoi				 =		oi.NE__Onetimefeeov__c;

								monthlyOptyItem					=		NEBit2winApi.convertCurrency(monthlyOptyItem,oi.CurrencyIsoCode,opp.CurrencyIsoCode,mapOfCurrsConversion);
								onetimeoi						 =		NEBit2winApi.convertCurrency(onetimeoi,oi.CurrencyIsoCode,opp.CurrencyIsoCode,mapOfCurrsConversion);

								l_item.UnitPrice				 =		onetimeoi + (monthlyOptyItem * opp.BI_Duracion_del_contrato_Meses__c);
								l_item.PricebookEntryId			=		productEntry;

								//GC 3.4: calculate the qty
								l_item.Quantity					=		oi.NE__Qty__c;

								if(oi.NE__Root_Order_Item__r.NE__Qty__c != null)
											 l_item.Quantity	 =		l_item.Quantity*oi.NE__Root_Order_Item__r.NE__Qty__c;

								if(oi.NE__OrderItemPromoId__r.NE__Qty__c != null)
											 l_item.Quantity	 =		l_item.Quantity*oi.NE__OrderItemPromoId__r.NE__Qty__c;

								//MANAGE MULTYCURRENCY

								//decimal oneTime = oi.NE__Onetimefeeov__c;
								//decimal recurring = oi.NE__RecurringChargeOv__c;

								//oneTime	 = NEBit2winApi.convertCurrency(oneTime,oi.NE__CatalogItem__r.NE__Currency__c, opp.CurrencyIsoCode, mapOfCurrsConversion);
								//recurring = NEBit2winApi.convertCurrency(recurring,oi.NE__CatalogItem__r.NE__Currency__c, opp.CurrencyIsoCode, mapOfCurrsConversion);

								if(oi.NE__Onetimefeeov__c != null){
									 orderUpd[i].NE__One_Time_Fee_Total__c		 += oneTimeoi*oi.NE__Qty__c*l_item.Quantity;
								}
								if(oi.NE__RecurringChargeOv__c != null){
									 orderUpd[i].NE__Recurring_Charge_Total__c	 += monthlyOptyItem*freqValues.get(oi.NE__RecurringChargeFrequency__c)* oi.NE__Qty__c*l_item.Quantity;
								}


								stdProdToIns.add(l_item);
							}
					 }
					 else{

					 }
					}
					i++;
			 }


			 insert stdProdToIns;
			 update orderUpd;
			}

		}catch(Exception e){
			DataBase.rollback(savePointCheckout);
			urlPage = null;
		}

		return urlPage;

	}



}