public class PCA_ShowProfileController extends PCA_HomeController{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longarela
    Company:       Salesforce.com
    Description:   Controller of show profile page
    
    History: 
    
    <Date>            <Author>          	<Description>
    27/06/2014        Jorge Longarela       Initial version 
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public PCA_ShowProfileController(){
		
	}
	
	public PageReference checkPermissions (){
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			
			PageReference page = enviarALoginComm();
			return page;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_ShowProfileController.checkPermissions', 'Portal Platino', Exc, 'Class');
		   return null;
		}
	}
}