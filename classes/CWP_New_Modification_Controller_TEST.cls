@isTest
public class CWP_New_Modification_Controller_TEST {
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Everis 
Company:       Everis
Description:   Test Methods executed to comprobe failure coverage 
Test Class:    CWP_Installed_Services_Controller.cls
CWP_New_Incident_Controller

History:

<Date>                  <Author>                <Change Description>
21/03/2016               Everis                   Initial Version

--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    
    
    
    @isTest
    private static void crearModTest() { 
        
        /*public static NE__Order__c createOrder(id idRecordType, id idTestCatalog, id idAsset, string orderType, id idAccount){
NE__Order__c testOrder = new NE__Order__c(
RecordTypeId = idRecordType,                //rtId,
NE__CatalogId__c = idTestCatalog,           //testCatalog.Id,
NE__Asset__c = idAsset,                     //commAsset.Id,
NE__Configuration_Type__c = orderType,
NE__AccountId__c = idAccount                //user.Contact.AccountId
);
return testOrder;
}*/
        
        set <string> setStrings = new set <string>{
            'Account', 
                'Case',
                'NE__Order__c',
                'NE__OrderItem__c'
                };
                    //ACCOUNT
                    map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            //rtMapBydevName.put(i.DeveloperName, i.id);
            rtMapByName.put(i.DeveloperName, i.id);
        }
        Id idRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'NE__Order__c' AND Name = 'Order'].Id;
        
        //Account legal entity
        Id holdingRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = '1.Holding'].Id;
        Id customerCountryRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = '2.Customer Country'].Id;
        Id legalEntityRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = '3.Legal Entity'].Id;
        Id businessUnitRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = '4.Business Unit'].Id;
        Id costCenterRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = '5.Cost Center'].Id;
        
        //Account legal entity    
        /*Account le = new Account(Name='legal entity');
le.RecordTypeId = legalEntityRecordType;
insert le;
//Account BU
Account acct = new Account(Name='cuenta test');
acct.ParentId = le.Id;
insert acct;*/
        //Contact
        /*Contact NewContact = new Contact (
FirstName = 'xyzFirst',
LastName = 'XyZLast',
Account = acct,
Email = 'xyzmail@mail.com'
);*/
        //Asset
        /*NE__Asset__c newAsset = CWP_TestDataFactory.createAsset(acct.Id);
newAsset.NE__Disable_Button__c = 'disable_button';
insert newAsset;
String orderType;
//Case
Case caso = new Case(); 
caso.Status = 'Assigned';
insert caso;
//Order        
NE__Order__c newOrder = CWP_TestDataFactory.createOrder(idRecordType, testCatalog.id, newAsset.id, orderType, acct.id);
newOrder.Case__c = caso.Id;
insert newOrder;
//Product
NE__Product__c testProduct = new NE__Product__c(
Name='productTest'
);
//Order Item        
//NE__OrderItem__c oi = CWP_TestDataFactory.dummyConfiguration(UserInfo.getUserId(), orderType);
//insert oi;*/
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Account holding
        Account ho = CWP_TestDataFactory.createHolding(holdingRecordType, 'Holding Acc');
        insert ho;
        Account cc = CWP_TestDataFactory.createCustomerCountry(customerCountryRecordType, 'Customer Country Acc', ho.Id);
        insert cc;
        Account le = CWP_TestDataFactory.createLegalEntity(legalEntityRecordType, 'Legal Entity Acc', ho.Id, cc.Id);
        insert le;
        Account bu = CWP_TestDataFactory.createBussinesUnit(businessUnitRecordType, 'Business Unit Acc', ho.Id, le.Id);
        insert bu;
        Account costC = CWP_TestDataFactory.createCostCenter(costCenterRecordType, 'Cost Center Acc', ho.Id, bu.Id, bu.Id, cc.Id, le.Id);
        insert costC;
        
        Contact con = CWP_TestDataFactory.createContact(bu.Id, 'ContactoPrueba');
        insert con;
        
        Profile prof = CWP_TestDataFactory.getProfile('TGS System Administrator');
        
        UserRole rol = CWP_TestDataFactory.createRole('Test Rol');
        //insert rol;
        
        User usuario = CWP_TestDataFactory.createUser('UsuTest', rol.Id, prof.Id);
        insert usuario;
        
        
        //Catalog
        /*NE__Catalog__c testCatalog = new NE__Catalog__c(Name='Test Catalog');
insert testCatalog;

NE__Catalog_Category__c catalogCategory = CWP_TestDataFactory.createCatalogCategory('category', testCatalog.Id, Null);*/
        
        
        //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        insert producto;
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Producto', catalogCategoryChildren.id, catalogo.id, producto.id);
        insert catalogoItem;
        
        
        
        //CASO     
        list <Case> listaDeCasos = new list <Case>();                
        Case newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.contactId = con.id;        
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('TGS_Billing_Inquiry'), 'sub2', 'Assigned', 'Solicitud de alta');
        newCase.contactId = con.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('TGS_Change'), 'sub3', 'Assigned', 'Solicitud de alta');
        newCase.contactId = con.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('TGS_Complaint'), 'sub4', 'Assigned', 'Solicitud de alta');
        newCase.contactId = con.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('TGS_Problem'), 'sub5', 'Assigned', 'Solicitud de alta');
        newCase.contactId = con.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('TGS_Query'), 'sub6', 'Assigned', 'Solicitud de alta');
        newCase.contactId = con.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(le.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub7', 'Assigned', 'Solicitud de alta');
        newCase.contactId = con.id;                
        listaDeCasos.add(newCase);
        insert listaDeCasos;   
        
        
        
        // ORDER   
        NE__Order__c testOrder1= CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', le.id);
        testOrder1.Case__c = listaDeCasos[1].id;
        insert testOrder1;
        
        NE__Order__c testOrder2 = CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', le.id);
        testOrder2.Case__c = listaDeCasos[0].id;
        insert testOrder2;        
        
        // ORDER ITEM
        NE__OrderItem__c newOI;        
        newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder1.id, producto.id, catalogoItem.id,  1);
        insert newOI;       
        
        newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder2.id, producto.id, catalogoItem.id,  2);
        insert newOI;      
        
        NE__Family__c family = CWP_TestDataFactory.createFamily('corleone');
        insert family;
        
        NE__DynamicPropertyDefinition__c dynProp = CWP_TestDataFactory.createDynamiyProperty('dynamic');
        insert dynProp;
        
        NE__ProductFamilyProperty__c famProp = CWP_TestDataFactory.createFamilyProperty(family.id, dynProp.id); 
        famProp.TGS_Is_key_attribute__c = true;
        famProp.CWP_KeyValue__c = true;
        insert famProp;
        
        NE__Order_Item_Attribute__c oia1 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        insert oia1;
        
        NE__Order_Item_Attribute__c oia2 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        insert oia2;     
        
        
        
        
        
        
        
        
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        CWP_New_Modification_Controller nmc = new CWP_New_Modification_Controller();
        //CWP_Installed_Services_Controller isc = new CWP_Installed_Services_Controller();
        //CWP_New_Modification_Controller nmc2 = new CWP_New_Modification_Controller(isc);
        
        nmc.orderIdForTest = testOrder1.Id;
        system.debug('RRR='+nmc.orderIdForTest);
        nmc.newModifError = false;
        //nmc.caseDescriptionParam = 'Modification order';
        
        nmc.detailIdNewModification = newOI.Id;
        String segundo; 
        //segundo = nmc.crearMod();
        
        String descripcion = 'Descripcion de prueba';
        Attachment attachment = new Attachment();
        attachment.Body = Blob.valueOf('attachment body example'); 
        attachment.Description = 'descripcion para el attachment';
        attachment.Name = String.valueOf('local.CWPexcelExport.adjuntosMap');
        attachment.ParentId = newCase.id; 
        insert attachment;
        
        /*Attachment attachment2 = new Attachment();
        attachment2.Body = Blob.valueOf('attachment body example 2'); 
        attachment2.Name = String.valueOf('local.CWPexcelExport.adjuntosMap');
        attachment2.ParentId = caso.id; 
        insert attachment2;*/
        
        List<Attachment> listaAtt = new List<Attachment>();
        listaAtt.add(attachment);
        //listaAtt.add(attachment2);
        string name=attachment.Name.left(attachment.Name.indexOf('.'));
        Cache.Session.put('local.CWPexcelExport.'+name, 'prueba');
        
        Map<String,List<String>> newMap = new Map<String,List<String>>();
        List<String> newList = new List<String>();
        newList.add('prueba');
        newMap.put(name, newList);
        Cache.Session.put('local.CWPexcelExport.adjuntosMap', newMap);
        String attachmenStringBody='';
        for (String s : newList) {
            Cache.Session.put('local.CWPexcelExport.'+s,s);
        }
        
        //string name2=attachment2.Name.left(attachment2.Name.indexOf('.'));
        //Cache.Session.put(name2, 'prueba');
        nmc.crearWorkinfo(newCase.id,descripcion,listaAtt);
        nmc.crearMod();
        nmc.getSelectedIdNewModification();
        //nmc.getSelectedIdNewModification();
        nmc.attachmentNameMod1 = 'qqq';
    	nmc.attachmentNameMod2 = 'qqq';
    	nmc.attachmentNameMod3 = 'qqq';
    
    	nmc.attachmentDescMod1 = 'qqq';
    	nmc.attachmentDescMod2 = 'qqq';
    	nmc.attachmentDescMod3 = 'qqq'; 
        
        nmc.myAttachedBodyMod = Blob.valueOf('attachment body example');
    	nmc.myAttachedBodyMod2 = Blob.valueOf('attachment body example');
    	nmc.myAttachedBodyMod3 = Blob.valueOf('attachment body example');
        
        nmc.listCaseAttachmentsMod = listaAtt;
        nmc.newAttachmentMod = attachment;
        nmc.listCaseAttachmentsNameMod = newList;
        
        //
    }
    
}