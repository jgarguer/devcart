public with sharing class BI_ClonePlanDeAccionController {

	public BI_Plan_de_accion__c coordCliente { get; set; }
	public List<wrapper> lst_task { get; set; }
	public List<wrapper> lst_event { get; set; }
	public List<wrapper> lst_eventRelation { get; set; }
	public List<wrapper> lst_isc { get; set; }
	
	public class wrapper{
		
		public Boolean check { get; set; }
		public Task task { get; set; }
		public Event event { get; set; }
		public BI_ISC2__c isc { get; set; }
		public EventRelation eventRelation { get; set; }
		
		public wrapper(Task ta, Event ev, BI_ISC2__c is,EventRelation er){
			task = ta;
			event = ev;
			isc = is;
			check = false;
			eventRelation = er;
		}
		
	}

	public BI_ClonePlanDeAccionController(ApexPages.StandardController controller){
		
		coordCliente = (BI_Plan_de_accion__c) controller.getRecord();
		
		lst_task = new List<wrapper>();
		
		String query = BI_DynamicQueryHelper.dynamicQuery('Task') + ',Owner.Name,Who.Name from Task where WhatId = \''+coordCliente.Id+'\'';
        
        system.debug('QUERY Task: '+query);
		
		for(Task task:Database.query(query))
			lst_task.add(new wrapper(task, null, null,null));
			
		lst_event = new List<wrapper>();
		
		query = BI_DynamicQueryHelper.dynamicQuery('Event') + ' from Event where WhatId = \''+coordCliente.Id+'\' AND IsChild = false';
        
        system.debug('QUERY Event: '+query);
		
		for(Event event:Database.query(query))
			lst_event.add(new wrapper(null, event, null,null));

		lst_eventRelation = new List<wrapper>();
		if(!lst_event.isEmpty()){
			query = BI_DynamicQueryHelper.dynamicQuery('EventRelation') + ' , Relation.Name from EventRelation where EventId = \''+lst_event[0].event.Id+'\' AND IsInvitee = true';
	        system.debug('QUERY EventRelation: '+query);
			for(EventRelation evRel:Database.query(query))
				lst_eventRelation.add(new wrapper(null, null , null,evRel));	
		}



		lst_isc = new List<wrapper>();
		if(coordCliente.BI_Termometro_ISC__c != null){
			query = BI_DynamicQueryHelper.dynamicQuery('BI_ISC2__c') + ' from BI_ISC2__c where Id = \''+coordCliente.BI_Termometro_ISC__c+'\'';
        
        	system.debug('QUERY ISC: '+query);
		
			for(BI_ISC2__c isc:DataBase.query(query))
				lst_isc.add(new wrapper(null, null, isc,null));
			
			system.debug(lst_isc);	
		}
        
		
	}
	
	public PageReference CloneCustom(){
		try{		
			system.debug(lst_task);
			
			system.debug(lst_event);
			system.debug(lst_isc);
			
			Boolean cloneISC = false;
			for(wrapper isc_wrapper:lst_isc){
				if(isc_wrapper.check)
					cloneISC = true;
			}
			
			BI_ISC2__c isc_new;
			
			if(cloneISC){
				isc_new = lst_isc[0].isc.clone(false, false, false, false);
				insert isc_new;
			}
			
			BI_Plan_de_accion__c coord = coordCliente.clone(false, false, false, false);
			coord.Name = coord.Name + ' - Duplicado';
			coord.BI_Estado__c = Label.BI_En_creacion;
			coord.BI_Plan_original__c = coordCliente.Id;
			coord.BI_Fecha_de_inicio__c = null;
			coord.BI_Fecha_de_fin__c = null;
			
			if(cloneISC){
				coord.BI_Termometro_ISC__c = isc_new.Id;
			}else{
				coord.BI_Termometro_ISC__c = null;
			}
			
			insert coord;
			
			List<Task> lst_task_insert = new List<Task>();
			Boolean allTaskChecked = (!lst_task.isEmpty())?true:false;

			for(wrapper task_wrapper:lst_task){
				if(task_wrapper.check){
					Task ta = task_wrapper.task.clone(false, false, false, false);
					ta.BI_Acuerdo_original__c = task_wrapper.task.Id;
					ta.WhatId = coord.Id;
					ta.Status = Label.BI_TaskStatus_InCreation;
					lst_task_insert.add(ta);
				}else{
					allTaskChecked = false;
				}	
			}
			insert lst_task_insert;

			coord.BI_Todos_los_acuerdos_duplicados__c = allTaskChecked;
			update coord;
			
			List<Event> lst_event_insert = new List<Event>();
			for(wrapper event_wrapper:lst_event){
				if(event_wrapper.check){
					Event ev = event_wrapper.event.clone(false, false, false, false);
					ev.WhatId = coord.Id;
					lst_event_insert.add(ev);
				}
			}
			
			insert lst_event_insert;

			List<EventRelation> lst_eventRelation_insert = new List<EventRelation>();
			if(!lst_event_insert.isEmpty() && !lst_eventRelation.isEmpty()){
				for(wrapper eventRel_wrapper:lst_eventRelation){
					EventRelation evRel = eventRel_wrapper.eventRelation.clone(false,false,false,false);
					evRel.EventId = lst_event_insert[0].Id;
					lst_eventRelation_insert.add(evRel);
				}
			}

			insert lst_eventRelation_insert;

	        PageReference newPlanAccion = new ApexPages.StandardController(coord).view();
	        newPlanAccion.setRedirect(true);
	        return newPlanAccion;

		}catch(Exception exc){
			Id log_id = BI_LogHelper.generate_BILog('BI_ClonePlanDeAccionController.CloneCustom', 'BI_EN', Exc, 'Controller');
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.BI_Msg_Error_ClonePlanAccion + ' - Log Id: ' + log_id));
			return null;
		}
	}

}