@isTest
private class PCA_Jobs_TEST {

	    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Micah Burgos
	    Company:       Salesforce.com
	    Description:   Test class to manage coverage of PCA_Jobs class.
	   	
	
	    History: 
	    
	     <Date>                     <Author>                <Change Description>
	    06/10/2014                  Micah Burgos             Initial Version
	    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	

	@isTest static void sendMails_try_TEST() {
		BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		//To compare systemAsserts:
		list<CronTrigger> ct_old = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger];
		Integer ct_oldSize = ct_old.Size();

		//Simular events

		//List<Event> events = [select  Id, Description, Subject, StartDateTime, EndDateTime, DurationInMinutes, ActivityDateTime, ActivityDate 
		//from Event where BI_Correo_electronico_enviado__c=false and LastModifiedDate=today and BI_ParentId__c=null limit 100];
		Datetime nowDateTime = Datetime.now();
		Event evento = new Event(
			Description = 'Test',
			Subject = 'Test',
			StartDateTime = nowDateTime,
			EndDateTime = nowDateTime.addMinutes(60) ,
			DurationInMinutes = 60,
			ActivityDateTime = nowDateTime,
			BI_Correo_electronico_enviado__c=false,
			ActivityDate = Date.today()
		);

		insert evento;



		//Simular relations

		//Crear contactos.
			list<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
			list<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
			list<Contact> lst_cont = BI_DataLoad.loadContacts(3, lst_acc);

			list<EventRelation> lst_evRel = new list<EventRelation>();

		for(Contact contact :lst_cont){

			//List<EventRelation> contacts = [SELECT EventId,Id,RelationId FROM EventRelation WHERE EventId in :eventsMap.keySet()];				
			list<User> lst_user = new list<User>(); 
			EventRelation evenRelat = new EventRelation (
	 			EventId = evento.Id,
	 			RelationId = contact.Id
				);

				lst_evRel.add(evenRelat);

				lst_user.add(BI_DataLoad.loadPortalUser(contact.Id, BI_DataLoad.searchPortalProfile()));
		}

		insert lst_evRel;
		
		//Crear user = Crear CCP


		PCA_Jobs.sendEmails();

		// Get the information from the CronTrigger API object
		list<CronTrigger> ct_new = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger];

	}

	@isTest static void sendMails_catch_TEST() {
		BI_TestUtils.throw_exception = true;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		PCA_Jobs.sendEmails();
	}
	
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}