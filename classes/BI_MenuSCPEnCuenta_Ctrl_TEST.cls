/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Francisco Javier Ayllón Martínez
    Company:       Aborda
    Description:   Test class to manage the coverage code for BI_menuSCPEnCuenta class and menuSCPEnCuenta VFpage
    
    History: 
    <Date>                  <Author>                <Change Description>
    16/06/2015              Francisco Ayllón        Initial version
    09/12/2016              Gawron, Julián          Add BI_Dataload.SKIP_PARAMETER
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest 
private class BI_MenuSCPEnCuenta_Ctrl_TEST{
    static testMethod void BI_MenuSCPEnCuenta_Ctrl() {
 
        //Creating 4 users
        //0 -> No permissionSet / Shows Sección no disponible / SCPpermission = false
        //1 -> One permissionSet (BI_SCP) / Shows Sección no disponible / SCPpermission = false
        //2 -> One permissionSet (SCP) / Shows Sección no disponible / SCPpermission = false
        //4 -> The two permissionSets (SCP, BI_SCP) / Shows SCP / SCPpermission = true
        BI_Dataload.SKIP_PARAMETER = false; //JEG
        List<User> users = BI_DataLoad.loadUsers(4, BI_DataLoad.searchAdminProfile(), Label.BI_Administrador_del_sistema);
        System.assertEquals(4, users.size() , 'Error in users number');
        //Query, position 0 -> PermissionSet BI_SCP, position 1 -> PermissionSet SCP
        List<PermissionSet> listPS = [SELECT Id, Name FROM PermissionSet WHERE Name='BI_SCP' OR Name='SCP' ORDER BY Name];
        List<String> pais = new List<String>(); 
        pais.add('Argentina');
        List<Account> clients = BI_DataLoad.loadAccounts(1,pais);

        if(listPS.size()>=2){
            //We need to insert this records running as an administrator or exception is thrown
            System.runAs(users[0]){
                List<PermissionSetAssignment> listPSA = new List<PermissionSetAssignment>();
                PermissionSetAssignment PSA_SCP = new PermissionSetAssignment(PermissionSetId = listPS[0].Id, AssigneeId = users[1].Id);
                listPSA.add(PSA_SCP);
                PermissionSetAssignment PSA_SCP2 = new PermissionSetAssignment(PermissionSetId = listPS[0].Id, AssigneeId = users[3].Id);
                listPSA.add(PSA_SCP2);
                PermissionSetAssignment PSA_BI_SCP = new PermissionSetAssignment(PermissionSetId = listPS[1].Id, AssigneeId = users[2].Id);
                listPSA.add(PSA_BI_SCP);
                PermissionSetAssignment PSA_BI_SCP2 = new PermissionSetAssignment(PermissionSetId = listPS[1].Id, AssigneeId = users[3].Id);
                listPSA.add(PSA_BI_SCP2);
                insert listPSA;
            }

            Test.startTest();
            BI_TestUtils.throw_exception = false;
            PageReference pageRef = new PageReference('BI_menuSCPEnCuenta');
            ApexPages.StandardController sc = new ApexPages.StandardController(clients[0]);
            System.runAs(users[0]){
                BI_menuSCPEnCuenta_Ctrl controller = new BI_menuSCPEnCuenta_Ctrl(sc);
                Test.setCurrentPage(pageRef);
                System.assertEquals(false, controller.SCPpermission , 'Error value: SCPpermission for users[0] must be false');
            }
            System.runAs(users[1]){
                BI_menuSCPEnCuenta_Ctrl controller = new BI_menuSCPEnCuenta_Ctrl(sc);
                Test.setCurrentPage(pageRef);
                System.assertEquals(false, controller.SCPpermission , 'Error value: SCPpermission for users[1] must be false');
            }
            System.runAs(users[2]){
                BI_menuSCPEnCuenta_Ctrl controller = new BI_menuSCPEnCuenta_Ctrl(sc);
                Test.setCurrentPage(pageRef);
                System.assertEquals(false, controller.SCPpermission , 'Error value: SCPpermission for users[2] must be false');
            }
            System.runAs(users[3]){
                BI_menuSCPEnCuenta_Ctrl controller = new BI_menuSCPEnCuenta_Ctrl(sc);
                Test.setCurrentPage(pageRef);
                System.assertEquals(true, controller.SCPpermission , 'Error value: SCPpermission for users[3] must be false');
            }
            System.runAs(users[3]){
                BI_TestUtils.throw_exception = true;
                BI_menuSCPEnCuenta_Ctrl controller = new BI_menuSCPEnCuenta_Ctrl(sc);
                Test.setCurrentPage(pageRef);
                System.assertEquals(null, controller.SCPpermission , 'Error value: Exception thrown, SCPpermission value must be null');
            }

            Test.stopTest();

        }
    }
}