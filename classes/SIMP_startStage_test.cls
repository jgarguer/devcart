@IsTest
public class SIMP_startStage_test {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Javier Suarez
    Company:       Salesforce.com
    Description:   Test class for SIMP_startStage
    
    History:
    
    <Date>            <Author>          <Description>
    02/02/2016        Javier Suarez      Initial version
    20/07/2016		Manuel Medina	 Added OwnerId and other missing fields, added method testsetOptyMember.
    27/09/2017        Jaime Regidor      Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c and BI_Territory__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static testMethod void testSetStageName(){
        
        Account a = new Account(Name = 'test',
                                BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                BI_Activo__c = Label.BI_Si,
                                BI_Country__c = 'Argentina',
                                BI_Segment__c = 'test', // 27/09/2017
                                BI_Subsegment_Regional__c = 'test', // 27/09/2017
                                BI_Territory__c = 'test', // 27/09/2017
                               	OwnerId = UserInfo.getUserId()
                                );

        insert a;

//        system.debug('Esta es la cuenta: '+a);
        
        Opportunity o = new Opportunity(AccountId = a.id,
                                                      BI_Ciclo_ventas__c = 'Completo',
                                                      CurrencyIsoCode = UserInfo.getDefaultCurrency(),
													  StageName = Label.BI_F6Preoportunidad,
                                        			  CloseDate = Date.today(),
                                                      BI_Ultrarapida__c = true,
                                                      BI_Duracion_del_contrato_Meses__c = 12,
                                                      BI_Country__c = 'Argentina',
                                                      BI_Opportunity_Type__c = 'Convergente',
                                        			  BI_SIMP_Opportunity_Type__c = 'Alta',
                                                      Name = 'test',
                                                      OwnerId = UserInfo.getUserId());
        
        List<Opportunity> los = new List<Opportunity>();
        los.add(o);
        
        insert los;

//        system.debug('Esta es la oportunidad'+los);

            Test.startTest();
        
        SIMP_startStage.setStageName(los);

        
        Test.stopTest();
        
        System.debug('Estado de la opty que vamos a evaluar'+los.get(0).StageName);
        
        System.assertEquals(los.get(0).StageName,'F5 - Solution Definition');
    }

    static testMethod void testsetOptyMember(){
        
        Account a = new Account(Name = 'test',
                                BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                BI_Activo__c = Label.BI_Si,
                                BI_Country__c = 'Argentina',
                                BI_Segment__c = 'test', // 27/09/2017
                                BI_Subsegment_Regional__c = 'test', // 27/09/2017
                                BI_Territory__c = 'test', // 27/09/2017
                               	OwnerId = UserInfo.getUserId()
                                );

        insert a;

//        system.debug('Esta es la cuenta: '+a);
        
        Opportunity o = new Opportunity(AccountId = a.id,
                                                      BI_Ciclo_ventas__c = 'Completo',
                                                      CurrencyIsoCode = UserInfo.getDefaultCurrency(),
													  StageName = Label.BI_F6Preoportunidad,
                                        			  CloseDate = Date.today(),
                                                      BI_Ultrarapida__c = true,
                                                      BI_Duracion_del_contrato_Meses__c = 12,
                                                      BI_Country__c = 'Argentina',
                                                      BI_Opportunity_Type__c = 'Convergente',
                                        			  BI_SIMP_Opportunity_Type__c = 'Alta',
                                                      Name = 'test',
                                                      OwnerId = UserInfo.getUserId());
        
        List<Opportunity> los = new List<Opportunity>();
        los.add(o);
        
        insert los;

        system.debug('Esta es la oportunidad'+los);

            Test.startTest();
        
//        SIMP_startStage.setOptyMember(los);

        
        Test.stopTest();
        
        System.debug('Estado de la opty que vamos a evaluar'+los.get(0).StageName);
        
        System.assertEquals(los.get(0).StageName,Label.BI_F6Preoportunidad);
    }
}