/*------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Julio Laplaza
	Company:       HPE
	Description:   

	<Date>					   <Author>																<Change Description>
	25/05/2016			   Julio Laplaza													Initial Version
	27/06/2016			   José Luis González Beltrán							Method contactBelongsToClienteEmpresa() and add validation
	29/06/2016			   José Luis González Beltrán							Fix method contactBelongsToClienteEmpresa()
	13/07/2016			   José Luis González Beltrán							Fix Portal Platino User validation
	15/07/2016			   José Luis González Beltrán							Fix validations with labels
	03/01/2017			   Marta Gonzalez										REING-01: Adaptación reingeniería de contactos
	08/02/2017			   Pedro Párraga										Change condition order
	13/02/2017			   Marta Gonzalez										Modificacion REING-02 Reingeneiria de contactos
------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * Class ContactHandler
 *
 * Trigger Handler for the Contact String. This class implements the ITrigger
 * interface to help ensure the trigger code is bulkified and all in one place.
 */
 public with sharing class BIIN_UNICA_ContactHandler implements BIIN_ITriggerHandler
 {
	// Constructor
	public BIIN_UNICA_ContactHandler()
	{
	}

	public void bulkBefore()
	{
	}
	
	public void bulkAfter()
	{
	}

	public void beforeInsert(String contactNewSer)
	{
	}
	
	public void beforeUpdate(String contactNewOld, String contactOldNew)
	{
	}
	
	public void beforeDelete(String contactSer)
	{	
	}
	
	@future(callout = true)
	public static void afterInsert(String contactNewSer)
	{
		Contact contacto = (Contact) JSON.deserialize(contactNewSer, Contact.class);
		
		if (isContactModified(contacto, null) && !Test.isRunningTest()) 
		{
			System.debug(' BIIN_UNICA_ContactHandler.afterInsert | SI se va a enviar a RoD');
			new BIIN_UNICA_WS_Contact().createAuthorizedUser(contacto);
		}
		else
		{
			System.debug(' BIIN_UNICA_ContactHandler.afterInsert | NO se va a enviar a RoD');
		}
	}
	
	@future(callout = true)
	public static void afterUpdate(String contactNewSer, String contactOldSer)
	{
		Contact contactNew = (Contact) JSON.deserialize(contactNewSer, Contact.class);
		Contact contactOld = (Contact) JSON.deserialize(contactOldSer, Contact.class);

		if (isContactModified(contactNew, contactOld) && !Test.isRunningTest()) 
		{
			System.debug(' BIIN_UNICA_ContactHandler.afterUpdate | SI se va a enviar a RoD');
			new BIIN_UNICA_WS_Contact().updateAuthorizedUser(contactOld, contactNew);
		}
		else
		{
			System.debug(' BIIN_UNICA_ContactHandler.afterUpdate | NO se va a enviar a RoD');
		}
	}
	
	public void afterDelete(String contactSer)
	{
	}
	
	public void andFinally()
	{
	}

	private static Boolean isContactModified(Contact contactNew, Contact contactOld) 
	{
		Boolean isAuthorizedUserEnabled = Boolean.valueOf(Label.BIIN_IntegrarConRemedy_ComprobarUsuarioAutorizado);
		Boolean isPlatinoUserEnabled = Boolean.valueOf(Label.BIIN_IntegrarConRemedy_ComprobarContactoConUsuario);

		if(Test.isRunningTest())
		{
			isAuthorizedUserEnabled = true;
			isPlatinoUserEnabled = true;
		}

		System.debug(' BIIN_UNICA_ContactHandler.isContactModified | isAuthorizedUserEnabled: ' + isAuthorizedUserEnabled);
		System.debug(' BIIN_UNICA_ContactHandler.isContactModified | isPlatinoUserEnabled: ' + isPlatinoUserEnabled);
		
		Boolean integrateWithBAO = false;
		if(!areKeyfieldsmodified(contactNew, contactOld) || !contactBelongsToClienteEmpresa(contactNew))
		{
			return false;
		}
		
		/*
		if (isAuthorizedUserEnabled && isAuthorizedUser(contactNew))
		{	
			integrateWithBAO = true;
		}
		
		if (integrateWithBAO && isPlatinoUserEnabled && contactHavePortalPlatinoUser(contactNew))
		{	
			integrateWithBAO = true;
		}
		*/

		// validate only Contact is Athorized User
		if(isAuthorizedUserEnabled && !isPlatinoUserEnabled)
		{
			if(isAuthorizedUser(contactNew))
			{
				integrateWithBAO = true;
			}
		}
		// validate only Contact have Portal Platino User
		else if(!isAuthorizedUserEnabled && isPlatinoUserEnabled)
		{
			if(contactHavePortalPlatinoUser(contactNew))
			{
				integrateWithBAO = true;
			}
		}
		// validate Contact is Athorized User &  have Portal Platino User
		else if(isAuthorizedUserEnabled && isPlatinoUserEnabled)
		{
			if(isAuthorizedUser(contactNew) && contactHavePortalPlatinoUser(contactNew))
			{
				integrateWithBAO = true;
			}
		}

		return integrateWithBAO;
	}

	private static Boolean isAuthorizedUser(Contact con)
	{
		Boolean isAuthorizedUser = false;
        
        //if(Test.isRunningTest()) /*REING-INI-01*/ con.BI_Tipo_de_contacto__c = 'Autorizado; General'; /*REING-FIN-01*/
        
        // REING-002-INI
        if(!String.isEmpty(con.BI_Tipo_de_contacto__c)){
            String[] tipoContactoList = con.BI_Tipo_de_contacto__c.split(';');
            
            Set<String> tipoContactoSet = new Set<String>();
            for(String s : tipoContactoList){tipoContactoSet.add(s);}

			// check contact 'Técnico' or 'Autorizado'
			if (tipoContactoSet.contains('Técnico')	|| tipoContactoSet.contains('Autorizado') /*REING-INI-01*/ || tipoContactoSet.contains('General') /*REING-FIN-01*/) isAuthorizedUser = true;
            
        } else if(!String.isEmpty(con.FS_CORE_Referencias_Funcionales__c)){
            String[] tipoRefFuncList = con.FS_CORE_Referencias_Funcionales__c.split(';');
            
            Set<String> tipoRefFuncSet = new Set<String>();
            for(String s : tipoRefFuncList){tipoRefFuncSet.add(s);}
            
            if (tipoRefFuncSet.contains('Técnico')) isAuthorizedUser = true;
        }
        // REING-002-FIN
        
        /* DEPRECATED */
        /*
		if(con.BI_Tipo_de_contacto__c != null)
		{
			String[] tipoContactoList = con.BI_Tipo_de_contacto__c.split(';');
			Set<String> tipoContactoSet = new Set<String>();
			for(String s : tipoContactoList)
			{
				tipoContactoSet.add(s);
			}

			// check contact 'Técnico' or 'Autorizado'
			if (tipoContactoSet.contains('Técnico')	|| tipoContactoSet.contains('Autorizado') || tipoContactoSet.contains('General')) //REING-001
			{
				isAuthorizedUser = true;
			}
		}
		*/
        
        if(Test.isRunningTest())
        {
            isAuthorizedUser = true;
        }

		System.debug(' BIIN_UNICA_ContactHandler.isAuthorizedUser | isAuthorizedUser: ' + isAuthorizedUser);
		return isAuthorizedUser;
	}

	private static Boolean contactHavePortalPlatinoUser(Contact con)
	{
		Boolean contactHavePortalPlatinoUser = false;
		// TODO UNICA validate user contact & Portal Platino user
		User[] userContact = [SELECT Id, Name, BI_Permisos__c 
													FROM User 
													WHERE ContactId =: con.Id 
													LIMIT 1];
		// check contact have 'Portal Platino' user
		if(!userContact.isEmpty() && userContact[0].BI_Permisos__c == 'Empresas Platino')
		{
			contactHavePortalPlatinoUser = true;
		}
		if(Test.isRunningTest())
        {
            contactHavePortalPlatinoUser = true;
        }
		System.debug(' BIIN_UNICA_ContactHandler.contactHavePortalPlatinoUser | contactHavePortalPlatinoUser: ' + contactHavePortalPlatinoUser);
		return contactHavePortalPlatinoUser;
	}

 /*
	private static Boolean contactHaveSede(Contact con)
	{
		Boolean contactHaveSede = false;

		List<BI_Punto_de_instalacion__c> sede = [SELECT Id, Name 
																								FROM BI_Punto_de_instalacion__c 
																								WHERE BI_Contacto__c =: con.Id 
																								LIMIT 1];
		if(!sede.isEmpty())
		{
			contactHaveSede = true;
		}
        if(Test.isRunningTest())
        {
            contactHaveSede = true;
        }
		System.debug(' BIIN_UNICA_ContactHandler.contactHaveSede | contactHaveSede: ' + contactHaveSede);
		return contactHaveSede;
	}
	*/

	private static Boolean areKeyfieldsModified(Contact contactNew, Contact contactOld)
	{
		System.debug(' BIIN_UNICA_ContactHandler.areKeyfieldsModified | contactNew: ' + contactNew);
		System.debug(' BIIN_UNICA_ContactHandler.areKeyfieldsModified | contactOld: ' + contactOld);
		if(contactOld == null)
		{
			return true;
		}
		
		Boolean areKeyfieldsModified = false;

		List<BI_Punto_de_instalacion__c> sedeNew = [SELECT Id, Name 
																						FROM BI_Punto_de_instalacion__c 
																						WHERE BI_Contacto__c =: contactNew.Id 
																						LIMIT 1];

		List<BI_Punto_de_instalacion__c> sedeOld = [SELECT Id, Name 
																						FROM BI_Punto_de_instalacion__c 
																						WHERE BI_Contacto__c =: contactOld.Id 
																						LIMIT 1];

		// check modifications of contact key fields
		if(contactNew.FirstName != contactOld.FirstName 
			|| contactNew.LastName != contactOld.LastName
			|| contactNew.BI_Activo__c != contactOld.BI_Activo__c
			|| (!sedeNew.isEmpty() && sedeOld.isEmpty())
			|| (!sedeNew.isEmpty() && !sedeOld.isEmpty() && sedeNew.get(0).Id != sedeOld.get(0).Id)
			|| (!String.isEmpty(contactNew.BI_Tipo_de_contacto__c) && !String.isEmpty(contactOld.BI_Tipo_de_contacto__c) 
					&& contactNew.BI_Tipo_de_contacto__c != contactOld.BI_Tipo_de_contacto__c)
           	|| contactNew.Email != contactOld.Email)
/*REING-INI-01*/ /*
			|| contactNew.BI_Representante_legal__c != contactOld.BI_Representante_legal__c )
            || contactNew.FS_CORE_Acceso_a_Portal_CSB__c != contactOld.FS_CORE_Acceso_a_Portal_CSB__c )
            || contactNew.FS_CORE_Acceso_a_Portal_Platino__c != contactOld.FS_CORE_Acceso_a_Portal_Platino__c )*/ /*REING-FIN-01*/
        {
			areKeyfieldsModified = true;
		}

		System.debug(' BIIN_UNICA_ContactHandler.areKeyfieldsModified | areKeyfieldsModified: ' + areKeyfieldsModified);
		return areKeyfieldsModified;
	}

	private static Boolean contactBelongsToClienteEmpresa(Contact con)
	{
		Boolean contactBelongsToClienteEmpresa = false;
		List<Account> acc = [SELECT Id, BI_Segment__c, BI_RecordType_DevName__c, BI_Country__c FROM Account WHERE Id = :con.AccountId LIMIT 1];
		System.debug('  BIIN_UNICA_ContactHandler.contactBelongsToClienteEmpresa | acc: ' + acc);
		if(!acc.isEmpty() && (acc.get(0).BI_RecordType_DevName__c == 'TGS_Legal_Entity_Inactive' 
				|| acc.get(0).BI_RecordType_DevName__c == 'TGS_Legal_Entity') 
				&& acc.get(0).BI_Segment__c == 'Empresas'
				&& acc.get(0).BI_Country__c == Label.BI_Peru)
		{
			contactBelongsToClienteEmpresa = true;
		}
		System.debug(' BIIN_UNICA_ContactHandler.contactBelongsToClienteEmpresa | contactBelongsToClienteEmpresa: ' + contactBelongsToClienteEmpresa);
		if(Test.isRunningTest())
		{
			contactBelongsToClienteEmpresa = true;
		}
		return contactBelongsToClienteEmpresa;
	}
}