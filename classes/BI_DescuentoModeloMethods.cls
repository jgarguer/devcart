public with sharing class BI_DescuentoModeloMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by Account Triggers 
    Test Class:    BI_AccountMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if Descuento Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void blockEdition(List <BI_Descuento_para_modelo__c> news){
        try{
            if(BI_GlobalVariables.stopTriggerDesc == false){
                Set <Id> set_disc = new Set <Id>();
                for(BI_Descuento_para_modelo__c discM:news){                
                    set_disc.add(discM.BI_Descuento__c);                
                }

                List <BI_Descuento__c> lst_dsc = [SELECT Id, BI_Estado_del_Proceso__c FROM BI_Descuento__c WHERE Id IN :set_disc];
                for (BI_Descuento__c disc:lst_dsc){
                    for(BI_Descuento_para_modelo__c dism:news){
                        if (disc.Id == dism.BI_Descuento__c && 
                            (disc.BI_Estado_del_Proceso__c == label.BI_BolsaDineroCancelado ||
                             disc.BI_Estado_del_Proceso__c == 'Rechazado' ||
                             disc.BI_Estado_del_Proceso__c == 'Cerrado' ||
                             disc.BI_Estado_del_Proceso__c == 'Aprobado' )){
                            dism.addError(label.BI_DescuentoModeloBloqueado);
                        }   
                    }
                    
                }
            }
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoModeloMethods.blockEdition', 'BI_EN', exc, 'Trigger');
        }
    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if Descuento Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void insertAcc(List <BI_Descuento_para_modelo__c> news){
        try{
            Set <Id> set_disc = new Set <Id>();
            for(BI_Descuento_para_modelo__c discM:news){                
                set_disc.add(discM.BI_Descuento__c);                
            }
            if(!set_disc.IsEmpty()){
                List <BI_Descuento__c> lst_dsc = [SELECT Id, BI_Cliente__r.Name FROM BI_Descuento__c WHERE Id IN :set_disc];
                for (BI_Descuento__c disc:lst_dsc){
                     system.debug('entra1'+ disc);
                    for(BI_Descuento_para_modelo__c discM:news){           
                     system.debug('entra2'+ discM);     
                        discM.BI_Cliente_aux__c = disc.BI_Cliente__r.Name;              
                    }
                    
                }
            }
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoModeloMethods.blockEdition', 'BI_EN', exc, 'Trigger');
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if Descuento Status is 'Cancelado' to block de edition of the record.
    
    IN:            ApexTrigger.Old, ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    20/05/2014              Ignacio Llorca          Initial Version         
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void blockDelete(List <BI_Descuento_para_modelo__c> olds){
        try{
            
            Set <Id> set_disc = new Set <Id>();
            for(BI_Descuento_para_modelo__c discM:olds){                
                set_disc.add(discM.BI_Descuento__c);                
            }

            List <BI_Descuento__c> lst_dsc = [SELECT Id, BI_Estado_del_Proceso__c FROM BI_Descuento__c WHERE Id IN :set_disc];
            for (BI_Descuento__c disc:lst_dsc){
                for(BI_Descuento_para_modelo__c dism:olds){
                    if (disc.Id == dism.BI_Descuento__c && 
                        (disc.BI_Estado_del_Proceso__c == label.BI_BolsaDineroCancelado ||
                         disc.BI_Estado_del_Proceso__c == 'Rechazado' ||
                         disc.BI_Estado_del_Proceso__c == 'Cerrado' ||
                         disc.BI_Estado_del_Proceso__c == 'Aprobado' )){
                        dism.addError(label.BI_DescuentoModeloDelete);
                    }   
                }
                
            }
            
            
        }catch (exception exc){
            BI_LogHelper.generate_BILog('BI_DescuentoModeloMethods.blockEdition', 'BI_EN', exc, 'Trigger');
        }
    }
}