/*---------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Pardo
Company:       Accenture
Description:   Controller for the component CWP_AC_OrderQuoteSummary to retrieve OrderItem data
Test class :   CWP_AC_OrderQuoteSummaryCtrl_TEST
History
<Date>      <Author>     	<Description>
26-01-2018	Antonio Pardo	v1.0
------------------------------------------------------------------------------------------------------------------------------*/
public class CWP_AC_OrderQuoteSummary_Ctrl {
   	/*---------------------------------------------------------------------------------------------------------------------------
	Author:        Antonio Pardo
    Company:       Accenture
    Description:   Retrieves a data structure for the lightning table containing quote information, for the given user and account
	IN:			   	String recordId - Order Id

	OUT:			List<Object> - List with the headers on the first position and, orderItem info, containing the isoCode for the currency fields
    
	History
	<Date>      <Author>     	<Description>
	26-01-2018	Antonio Pardo	v1.0
	------------------------------------------------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static List<Object> getOi(String recordId){
        List<NE__OrderItem__c> lst_oi = [select Id, Name, (select Name, NE__Value__c from NE__Order_Item_Attributes__r),NE__Root_Order_Item__r.Name, NE__Parent_Order_Item__r.Name, NE__ProdId__r.Name, NE__BaseOneTimeFee__c, format(NE__BaseOneTimeFee__c) FormatBaseOneTimeFee,NE__BaseRecurringCharge__c ,format(NE__BaseRecurringCharge__c) FormatBaseRecurringCharge, CurrencyIsoCode from NE__OrderItem__c where NE__OrderId__c =: recordId];
        Map<String, Map<String,Object>> map_oi = new Map<String, Map<String,Object>>();
        System.debug(lst_oi[0].NE__BaseOneTimeFee__c == 0);
        System.debug(lst_oi[0].get('FormatBaseOneTimeFee'));
        
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get('NE__OrderItem__c');
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        
        //List with the table headers
        List<String> lst_labels = new List<String>{Label.CWP_AC_ProductName, 'Total NRC','Total MRC'};
          
        Map<String, Object> headersAux = new Map<String, Object>();
        headersAux.put('lstLabels', lst_labels);
        map_oi.put('headers', headersAux);
        
        for(NE__OrderItem__c oi : lst_oi){
            
            if(oi.NE__Root_Order_Item__c == null){
                //Parent Oi
                Map<String, Object> map_obj = new Map<String, Object>();
                Map<String, Object> map_rootAux = new Map<String, Object>();
                map_rootAux.put('Name', oi.NE__ProdId__r.Name);
                
                Map<String, Object> map_currencyAux = new Map<String, Object>();
                map_currencyAux.put('value', oi.NE__BaseOneTimeFee__c);
                map_currencyAux.put('currencyIso', oi.CurrencyIsoCode);
                map_rootAux.put('baseOneTime', map_currencyAux);
                
                map_currencyAux = new Map<String, Object>();
                map_currencyAux.put('value', oi.NE__BaseRecurringCharge__c);
                map_currencyAux.put('currencyIso', oi.CurrencyIsoCode);
                map_rootAux.put('BaseRecurring', map_currencyAux);
               
                Map<String, String> map_siteInfo = new Map<String, String>();
                for(NE__Order_Item_Attribute__c oiatr : oi.NE__Order_Item_Attributes__r){        
                    map_siteInfo.put(oiatr.Name, oiatr.NE__Value__c); 
                }
                String siteInfo = map_siteInfo.get('Country') + ' - ' + map_siteInfo.get('City') + ' - ' + map_siteInfo.get('Address');
                map_obj.put('siteInfo', siteInfo);
                map_obj.put('lstOi', new List<Object>());
                map_obj.put('rootOi', map_rootAux);
                map_oi.put(oi.Id, map_obj);
            }
            
        }
        for(NE__OrderItem__c oi : lst_oi){     
            if(map_oi.containsKey(oi.NE__Root_Order_Item__c)){
                List<Object> lst_aux = (List<Object>)map_oi.get(oi.NE__Root_Order_Item__c).get('lstOi');
				Map<String, Object> map_oiAux = new Map<String, Object>();
                if(oi.NE__ProdId__r.Name=='MPLS VPN'){
                    map_oiAux.put('Name', Label.CWP_AC_VPN_MPLS);
                } else if(oi.NE__ProdId__r.Name=='INTERNET ACCESS'){
                    map_oiAux.put('Name', Label.CWP_AC_Internet);
                } else {
                    map_oiAux.put('Name', oi.NE__ProdId__r.Name);
                }
                
                Map<String, Object> map_currencyAux = new Map<String, Object>();
                map_currencyAux.put('value', oi.NE__BaseRecurringCharge__c == 0 ? Label.CWP_AC_Calculating : String.valueOf(oi.get('FormatBaseOneTimeFee')));
                map_currencyAux.put('type','STRING');
                map_currencyAux.put('currencyIso', oi.CurrencyIsoCode);
                map_oiAux.put('baseOneTime', map_currencyAux);
                
                map_currencyAux = new Map<String, Object>();
                map_currencyAux.put('value', oi.NE__BaseRecurringCharge__c == 0 ? Label.CWP_AC_Calculating : String.valueOf(oi.get('FormatBaseRecurringCharge')));
                map_currencyAux.put('type','STRING');
                map_currencyAux.put('currencyIso', oi.CurrencyIsoCode);
                map_oiAux.put('BaseRecurring', map_currencyAux);
                
                lst_aux.add(map_oiAux);
                
            }
            
        }
        
        List<Object> lst_return = new List<Object>();
        for(Object obj : map_oi.values()){
            lst_return.add(obj);
        }
        
        return lst_return;
    }
}