/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Alejandro García Olmedo
Company:       Everis
Description:   Test class for CWP_MyService_services_insrvt_controller_TEST class caseMethod
Test Class:    

History:

<Date>              <Author>                   <Change Description>
11/05/2017          Julio Asenjo             Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class CWP_MyService_services_insrvt_test {
    
    
    @testSetup 
    private static void dataModelSetup() {               
        
        //USER
        UserRole rolUsuario;
        User unUsuario;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, userInfo.getProfileId());
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{
            'Account', 
                'Case',
                'NE__Order__c',
                'NE__OrderItem__c'
                };
                    
                    //ACCOUNT
                    map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            //rtMapBydevName.put(i.DeveloperName, i.id);
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(unUsuario){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }          
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        insert contactTest;
        
        User usuario;
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');         
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        //CATALOG
        NE__Catalog__c catalogo = CWP_TestDataFactory.createCatalog('CatalogoTest');
        insert catalogo;
        NE__Catalog_Category__c catalogCategoryParent = CWP_TestDataFactory.createCatalogCategory('Catalogo padre', catalogo.id, NULL);
        insert catalogCategoryParent;
        NE__Catalog_Category__c catalogCategoryChildren = CWP_TestDataFactory.createCatalogCategory('Catalogo hijo', catalogo.id, catalogCategoryParent.id);
        insert catalogCategoryChildren;
        NE__Product__c producto = CWP_TestDataFactory.createCommercialProduct('Producto Comercial');
        producto.TGS_CWP_Tier_1__c = 'Catalogo padre';
        producto.TGS_CWP_Tier_2__c = 'Catalogo hijo';
        insert producto;
        NE__Catalog_Item__c catalogoItem = CWP_TestDataFactory.createCatalogoItem('Product', catalogCategoryChildren.id, catalogo.id, producto.id);
        insert catalogoItem;
        
        //ORDER
        
        //NE__Asset__c testAsset = CWP_TestDataFactory.createAsset();
        //insert testAsset;
        //Id rtId = TGS_RecordTypes_Util.getRecordTypeId(NE__Order__c.SObjectType, 'Order');
        //NE__Order__c testOrder = new NE__Order__c(RecordTypeId = rtId, NE__Type__c = 'Asset');        
        //NE__Order__c testOrder= CWP_TestDataFactory.createOrder(rtMapByName.get('Order'), catalogo.id, NULL, 'New', accLegalEntity.id);
        //insert testOrder;
        
        
        
        //private final set<string> REQUESTRT = new set<string>{'Order_Management_Case'};
        //private final set<string> CASERT = new set<string>{'TGS_Billing_Inquiry', 'TGS_Change', 'TGS_Complaint', 'TGS_Incident', 'TGS_Problem', 'TGS_Query'};
        
        //CASO     
        list <Case> listaDeCasos = new list <Case>();                
        Case newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Incident'), 'sub1', 'Assigned', '');
        newCase.contactId = contactTest.id;        
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Billing_Inquiry'), 'sub2', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Change'), 'sub3', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Complaint'), 'sub4', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Problem'), 'sub5', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('TGS_Query'), 'sub6', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;
        listaDeCasos.add(newCase);
        newCase = CWP_TestDataFactory.createCase(accLegalEntity.id,NULL,rtMapByName.get('Order_Management_Case'), 'sub7', 'Assigned', 'Solicitud de alta');
        newCase.contactId = contactTest.id;                
        listaDeCasos.add(newCase);
        insert listaDeCasos;        
        
        
        // ORDER   
        NE__Order__c testOrder1= CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', accLegalEntity.id);
        testOrder1.Case__c = listaDeCasos[1].id;
        insert testOrder1;
        
        NE__Order__c testOrder2 = CWP_TestDataFactory.createOrder(rtMapByName.get('Asset'), catalogo.id, NULL, 'New', accLegalEntity.id);
        testOrder2.Case__c = listaDeCasos[0].id;
        insert testOrder2;        
        
        // ORDER ITEM
        NE__OrderItem__c newOI;        
        newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder1.id, producto.id, catalogoItem.id,  1);
        insert newOI;       
        
        newOI = CWP_TestDataFactory.createOrderItem(usuario.accountId, testOrder2.id, producto.id, catalogoItem.id,  2);
        newOI.NE__Status__c='Activo';
        insert newOI;      
        
        NE__Family__c family = CWP_TestDataFactory.createFamily('corleone');
        insert family;
        
        NE__DynamicPropertyDefinition__c dynProp = CWP_TestDataFactory.createDynamiyProperty('dynamic');
        insert dynProp;
        
        NE__ProductFamilyProperty__c famProp = CWP_TestDataFactory.createFamilyProperty(family.id, dynProp.id); 
        famProp.TGS_Is_key_attribute__c = true;
        famProp.CWP_KeyValue__c = true;
        insert famProp;
        
        NE__Order_Item_Attribute__c oia1 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        oia1.Name = 'picked';
        oia1.NE__Value__c = 'filter';
        insert oia1;
        NE__Order_Item_Attribute__c oia3 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        oia3.Name = 'picked2';
        oia3.NE__Value__c = 'filter';
        insert oia3;
        
        NE__Order_Item_Attribute__c oia2 = CWP_TestDataFactory.createOrderItemAttribute(newOI.id, famProp.id);
        insert oia2;      
        
        // PRE QUERY
        List<CWP_CatalogItemConfiguration__c> listToInsert = new List<CWP_CatalogItemConfiguration__c>();
        CWP_CatalogItemConfiguration__c prequery = new CWP_CatalogItemConfiguration__c();
        prequery.CWP_Query__c = 'AND  Id!=null';
        prequery.CWP_QueryLabel__c = 'prequery test';
        prequery.CWP_Prequery_Type__c='Order_Item_Prequery';
        prequery.CWP_CatalogItem__c = catalogoItem.Id;
        prequery.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'CWP_CatalogItemConfiguration__c' AND DeveloperName = 'CWP_CustomQuery'].Id;
        listToInsert.add(prequery);
        CWP_CatalogItemConfiguration__c prequery2 = new CWP_CatalogItemConfiguration__c();
        prequery2.CWP_Query__c = 'Id!=null';
        prequery2.CWP_QueryLabel__c = 'prequery test';
        prequery2.CWP_Prequery_Type__c='Attribute_Prequery';
        prequery2.CWP_CatalogItem__c = catalogoItem.Id;
        prequery2.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'CWP_CatalogItemConfiguration__c' AND DeveloperName = 'CWP_CustomQuery'].Id;
        CWP_CatalogItemConfiguration__c prequery3 = new CWP_CatalogItemConfiguration__c();
        prequery3.CWP_Query__c = 'AND  Id!=null';
        prequery3.CWP_QueryLabel__c = 'prequery test';
        prequery3.CWP_Prequery_Type__c='Order_Item_Prequery';
        prequery3.CWP_CatalogItem__c = catalogoItem.Id;
        prequery3.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'CWP_CatalogItemConfiguration__c' AND DeveloperName = 'CWP_CustomQuery'].Id;
        listToInsert.add(prequery3);
        
        CWP_CatalogItemConfiguration__c fieldsToQuery = new CWP_CatalogItemConfiguration__c();
        fieldsToQuery.CWP_CatalogItemOrderField__c = catalogoItem.Id;
        fieldsToQuery.CWP_FieldAPIName__c = 'TGS_RFS_date__c; NE__Country__c; NE__City__c; NE__Status__c';
        fieldsToQuery.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'CWP_CatalogItemConfiguration__c' AND DeveloperName = 'CWP_OrderField'].Id;
        listToInsert.add(fieldsToQuery);
        insert listToInsert;
        system.debug('prequerys insertadas: ' + listToInsert);
        TGS_GME_GM_Product_Categorization__c aTGS_GME= new  TGS_GME_GM_Product_Categorization__c();
        aTGS_GME.name='001';  
        aTGS_GME.TGS_Service__c='Producto Comercial';
        
        insert aTGS_GME;
        
    }
    
           @isTest
    private static void Test1() {     
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();                        
            CWP_MyService_services_insrvt_controller.getResumeList();
            
            CWP_MyService_services_insrvt_controller.LightningContext c= new CWP_MyService_services_insrvt_controller.LightningContext();
            c.pickListServiceUnitValue='Catalogo padre';
            c.pickListServiceValue='Catalogo hijo';
            c.pickListFamilyServiceValue='Producto Comercial';
            
            
            CWP_MyService_services_insrvt_controller.loadInfoFromSummary(JSON.serialize(c));
            
            c.tableControl=new CWP_MyService_services_insrvt_controller.TableController('table',1,5,5,5);
            c.hayPrequery=false;
            c.orderByAtt=false;
            c.filterByFieldOrderedList= new List <String>();
            c.filterByField=New Map<String,String>();
            c.filterByAttribute= new list<String>();
            c.filterByFieldMap = new Map<String, String>();
            c.filterByFieldMap.put('Country', 'Spain');
            c.filtrosExAplicados =new map<String,String>();
            c.filterByFieldMapJSON=JSON.serialize(c.filterByFieldMap);
            c.accs=CWP_MyService_services_insrvt_controller.allAccountSet();
            c.filterByField = new Map<String, String>();
            c.pickListFamilyServiceOptions=New List<CWP_MyService_services_insrvt_controller.PickListItem>();
            c.pickListServiceOptions =New List<CWP_MyService_services_insrvt_controller.PickListItem>();
            c.pickListServiceUnitOptions=New List<CWP_MyService_services_insrvt_controller.PickListItem>();
            c.resumeList=new Map<String,Map<String,Map<String,String>>>();
            c.filterByField.put('Country', 'NE__Country__c');
            
            System.debug('@@@@@@'+c.accs);
            
            c.pagination='forward';
            CWP_MyService_services_insrvt_controller.getNewTableController(JSON.serialize(c));
            c.pagination='10';
            CWP_MyService_services_insrvt_controller.getNewTableController(JSON.serialize(c));
            CWP_MyService_services_insrvt_controller.loadExcelModal(JSON.serialize(c));
            CWP_MyService_services_insrvt_controller.aplyFiltersFromAdvanced(JSON.serialize(c));
            CWP_MyService_services_insrvt_controller.onchangeServiceFamily(JSON.serialize(c));
            CWP_MyService_services_insrvt_controller.onchangeService(JSON.serialize(c));
            
            Test.stopTest();
        }       
        
    }   
    @isTest
    private static void Test2() {     
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();                        
            
            
            CWP_MyService_services_insrvt_controller.LightningContext c= new CWP_MyService_services_insrvt_controller.LightningContext();
            c.pickListServiceUnitValue='Producto Comercial';
            c.pickListServiceValue='Catalogo hijo';
            c.pickListFamilyServiceValue='Catalogo padre';
            CWP_MyService_services_insrvt_controller.loadInfoFromSummary(JSON.serialize(c));
            c.tableControl=new CWP_MyService_services_insrvt_controller.TableController('table',1,5,5,5);
            c.hayPrequery=false;
            c.orderByAtt=false;
            c.filterByFieldOrderedList= new List <String>();
            c.filterByField=New Map<String,String>();
            c.filterByAttribute= new list<String>();
            c.filterByFieldMap = new Map<String, String>();
            c.filterByFieldMap.put('Country', 'Spain');
            c.filterByFieldMap.put('City', 'Madrid');
            c.filtrosExAplicados =new map<String,String>();
            c.filterByFieldMapJSON=JSON.serialize(c.filterByFieldMap);
            c.accs=CWP_MyService_services_insrvt_controller.allAccountSet();
            c.filterByField = new Map<String, String>();
            c.pickListFamilyServiceOptions=New List<CWP_MyService_services_insrvt_controller.PickListItem>();
            c.pickListServiceOptions =New List<CWP_MyService_services_insrvt_controller.PickListItem>();
            c.pickListServiceUnitOptions=New List<CWP_MyService_services_insrvt_controller.PickListItem>();
            c.resumeList=new Map<String,Map<String,Map<String,String>>>();
            c.filterByField.put('Country', 'NE__Country__c');
            c.filterByField.put('City', 'NE__City__c');
            c.pickedRow='Name';
            c.typeExcelFilter='field';
            c.filterByFieldOrderedListValue=new Map<String, String>();
            c.filterByFieldOrderedListValue.put('Name','Name');
            CWP_MyService_services_insrvt_controller.loadExcelModal(JSON.serialize(c));
            
            c.hayPrequery=true;
            c.typePrequery='Attribute_Prequery';
            for(CWP_CatalogItemConfiguration__c cat:[SELECT Id, CWP_Query__c,CWP_Prequery_Type__c  FROM CWP_CatalogItemConfiguration__c WHERE CWP_QueryLabel__c = 'prequery test']) {
                
                c.hayPrequery=true;
                c.typePrequery=cat.CWP_Prequery_Type__c;
                c.selPreQuery=cat.Id;
                c.filterByAttMap= new Map<String, String>();
                c.filterByAttMap.put('picked','filter');
                c.filterByFieldOrderedList= new List <String>();
                c.filterByFieldOrderedList.add('Contry');
                c.filterByField = new Map<String, String>();
                c.filterByField.put('Country', 'NE__Country__c');
                c.filterByField.put('City', 'NE__City__c');
                
                
                CWP_MyService_services_insrvt_controller.loadExcelModal(JSON.serialize(c));
                // 
            }
            List<NE__OrderItem__c> orderItems=[SELECT Id, NE__OrderId__r.Case__r.AccountId,
                                               NE__OrderId__r.RecordType.DeveloperName,
                                               NE__CatalogItem__r.NE__Type__c,NE__ProdId__r.TGS_CWP_Tier_1__c,NE__ProdId__r.TGS_CWP_Tier_2__c,NE__ProdId__r.Name,NE__Country__c
                                               FROM NE__OrderItem__c ];
            system.debug('<<@@>>'+orderItems); 
            system.debug('<<@@>>'+orderItems[0].NE__CatalogItem__r.NE__Type__c+'--'+orderItems[0].NE__ProdId__r.TGS_CWP_Tier_1__c+'--'+orderItems[0].NE__ProdId__r.TGS_CWP_Tier_2__c+'--'+orderItems[0].NE__ProdId__r.Name+'---'); 
            
            String deserialize= ''; 
            CWP_MyService_services_insrvt_controller.getResumeList();
            
            c.pagination='forward';
            CWP_MyService_services_insrvt_controller.getNewTableController(JSON.serialize(c));
            
            
            CWP_MyService_services_insrvt_controller.onchangeService(JSON.serialize(c));
            CWP_MyService_services_insrvt_controller.onchangeServiceFamily(JSON.serialize(c));     
            Test.stopTest();
        }
    }
    @isTest
    private static void Test3() {
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();                        
            CWP_MyService_services_insrvt_controller.LightningContext c= new CWP_MyService_services_insrvt_controller.LightningContext();
            c.pickListServiceUnitValue='Producto Comercial';
            c.pickListServiceValue='Catalogo hijo';
            c.pickListFamilyServiceValue='Catalogo padre'; 
            String d= CWP_MyService_services_insrvt_controller.loadInfoFromSummary(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            c.sortedRow='Name';
            c.sortedOrder='DESC';
            CWP_MyService_services_insrvt_controller.sortExcelTable(JSON.serialize(c));
            
            
            d=CWP_MyService_services_insrvt_controller.getPreFilterQuerysList(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            
            c.selPreQuery=c.preFilterQuerysList[1].value;
            CWP_MyService_services_insrvt_controller.getSelectedPreQuery(JSON.serialize(c));
            Test.stopTest();
        }
    }
	 @isTest
    private static void Test4() {
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();                        
            CWP_MyService_services_insrvt_controller.LightningContext c= new CWP_MyService_services_insrvt_controller.LightningContext();
            c.pickListServiceUnitValue='Producto Comercial';
            c.pickListServiceValue='Catalogo hijo';
            c.pickListFamilyServiceValue='Catalogo padre'; 
            String d= CWP_MyService_services_insrvt_controller.loadInfoFromSummary(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            System.debug('\n \n-----------------------------------------\n');
            c.filterByFieldMap = new Map<String, String>();
            c.filterByFieldMap.put('Status', 'Activo');
            
            
            c.filterByFieldMapJSON=Json.serialize(c.filterByFieldMap);
            d=CWP_MyService_services_insrvt_controller.aplyFiltersFromAdvanced(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            c.filterByFieldMap = new Map<String, String>();
            c.filterByFieldMap.put('Status', 'Activo');
            c.filterByFieldMap.put('RFS date', '2017-03-08');
            
            c.filterByFieldMapJSON=Json.serialize(c.filterByFieldMap);
            d=CWP_MyService_services_insrvt_controller.aplyFiltersFromAdvanced(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            Test.stopTest();
        }
    }
    
    @isTest
    private static void Test5() {
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();                        
            CWP_MyService_services_insrvt_controller.LightningContext c= new CWP_MyService_services_insrvt_controller.LightningContext();
            c.pickListServiceUnitValue='Producto Comercial';
            c.pickListServiceValue='Catalogo hijo';
            c.pickListFamilyServiceValue='Catalogo padre'; 
            String d= CWP_MyService_services_insrvt_controller.loadInfoFromSummary(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            System.debug('\n \n-----------------------------------------\n');
            c.filterByAttMap= new Map<String,String>();
            c.filterByAttMap.put('picked','filter');
            c.filterByAttMapJSON=Json.serialize(c.filterByAttMap);
            d=CWP_MyService_services_insrvt_controller.aplyFiltersFromAdvanced(JSON.serialize(c));
            // c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            
            c.filterByFieldMap = new Map<String, String>();
            c.filterByFieldMap.put('Status', 'Activo');
            c.filterByFieldMap.put('RFS date', '2017-03-08');
            d=CWP_MyService_services_insrvt_controller.aplyFiltersFromAdvanced(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            CWP_MyService_services_insrvt_controller.exportPDF(JSON.serialize(c),'landscape');
            
            Test.stopTest();
        }
    }
    @isTest
    private static void Test6() {
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();                        
            CWP_MyService_services_insrvt_controller.LightningContext c= new CWP_MyService_services_insrvt_controller.LightningContext();
            c.pickListServiceUnitValue='Producto Comercial';
            c.pickListServiceValue='Catalogo hijo';
            c.pickListFamilyServiceValue='Catalogo padre'; 
            String d= CWP_MyService_services_insrvt_controller.loadInfoFromSummary(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
           
            c.pickedRow='Name';
            c.inputFilterValue='OI';
            d=CWP_MyService_services_insrvt_controller.aplicaFiltroExcel(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            //
            //quitaFiltroExcel
            c.inputFilterValue='Name';
            d=CWP_MyService_services_insrvt_controller.quitaFiltroExcel(JSON.serialize(c));
            
            CWP_MyService_services_insrvt_controller.TableRecord a= new CWP_MyService_services_insrvt_controller.TableRecord();
            a.identifier='';
            a.name='';
            a.status='';
            a.backStatus='';
            a.catalogCategoryId='';
            a.productName='';
            a.catalogItemId='';
            CWP_MyService_services_insrvt_controller.SearchField t= new CWP_MyService_services_insrvt_controller.SearchField('','');
            t.label='';
            t.name='';
            t.value='';
            t.type='';
            

            
            
            Test.stopTest();
        }
    }
    @isTest
    private static void Test7() {
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();                        
            CWP_MyService_services_insrvt_controller.LightningContext c= new CWP_MyService_services_insrvt_controller.LightningContext();
            c.pickListServiceUnitValue='Producto Comercial';
            c.pickListServiceValue='Catalogo hijo';
            c.pickListFamilyServiceValue='Catalogo padre'; 
            
            String d= CWP_MyService_services_insrvt_controller.loadInfoFromSummary(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            c.filterByAttMap= new Map<String,String>();
            c.filterByAttMap.put('picked','filter');
            c.filterByAttMap.put('picked2','filter');
            c.filterByAttribute=new List<String>();
            c.filterByAttribute.add('picked');
            c.filterByAttribute.add('picked2');
            c.filterByAttMapJSON=Json.serialize(c.filterByAttMap);
            d=CWP_MyService_services_insrvt_controller.aplyFiltersFromAdvanced(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            System.debug (c);
            c.filterByAttribute=new List<String>();
            
            Test.stopTest();
        }
    }
    @isTest
    private static void Test8() {
        
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];             
        System.runAs(usuario){
            Test.startTest();                        
            CWP_MyService_services_insrvt_controller.LightningContext c= new CWP_MyService_services_insrvt_controller.LightningContext();
            c.pickListServiceUnitValue='Producto Comercial';
            c.pickListServiceValue='Catalogo hijo';
            c.pickListFamilyServiceValue='Catalogo padre'; 
            String d= CWP_MyService_services_insrvt_controller.loadInfoFromSummary(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            CWP_MyService_services_insrvt_controller.exportPDF(JSON.serialize(c),'landscape');
            CWP_ExportServices_ctrl exprt= new CWP_ExportServices_ctrl();
            exprt.getMapLabel();
            exprt.getOrientation();
            
            c.pickedRow='Name';
            c.inputFilterValue='OI';
            d=CWP_MyService_services_insrvt_controller.aplicaFiltroExcel(JSON.serialize(c));
            c=(CWP_MyService_services_insrvt_controller.LightningContext)JSON.deserialize(d, CWP_MyService_services_insrvt_controller.LightningContext.class);
            CWP_MyService_services_insrvt_controller.exportPDF(JSON.serialize(c),'landscape');
            
            exprt= new CWP_ExportServices_ctrl();
            
            Test.stopTest();
        }
    }
}