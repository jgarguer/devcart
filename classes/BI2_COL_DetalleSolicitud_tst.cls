/**
********************************************************************************************************
* @company          Avanxo Colombia
* @author           Antonio Torres href=<atorres@avanxo.com>
* @project          BI_EN Fase 2 Colombia
* @name             BI2_COL_DetalleSolicitud_tst
* @description      Test class that covers the following classes/triggers:
*
*                   Based on "BIIN_Detalle_Solicitud_TEST" class.
* @dependencies
* @changes (Version)
* --------   ---   ----------   ---------------------------   ------------------------------------------
*            No.   Date         Author                        Description
* --------   ---   ----------   ---------------------------   ------------------------------------------
* @version   1.0   2017-02-24   Antonio Torres (AT)           Initial version.
********************************************************************************************************
**/

@isTest(seeAllData = false)
public class BI2_COL_DetalleSolicitud_tst {
    private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
    private final static String LE_NAME = 'TestAccount_Detalle_Incidencia_TEST';

    @testSetup static void dataSetup() {
        Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert account;
        List<Account> acc = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];

        List<RecordType> rT = [SELECT Id, Name FROM Recordtype WHERE Name = 'Solicitud Incidencia Técnica'];
        Case caseTest = new Case(RecordTypeId = rT.get(0).Id, AccountId = acc.get(0).Id, BI_Otro_Tipo__c = 'test', Status = 'Assigned',
                                    Priority = 'Media', Type = 'Reclamo', Subject = 'Test', CurrencyIsoCode = 'ARS', Origin = 'Portal Cliente',
                                    BI_Confidencial__c = false, Reason = 'Reclamos administrativos', Description = 'testdesc',
                                    BI_Id_del_caso_Legado__c = 'INC000000022352', BIIN_Tiempo_Neto_Apertura__c = 324234);
        insert caseTest;
        //Fin creacion de caso
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Torres (AT)
    Company:       Avanxo
    Description:   Test method to manage the code coverage for BI2_COL_CreacionIncidencia_Ctrl

     <Date>                 <Author>                        <Change Description>
    11/2015              Ignacio Morales Rodríguez          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest static void BI2_COL_DetalleSolicitud_tst_Solicitud() {
        BI_TestUtils.throw_exception=false;
        Profile prof = [Select Id From Profile Where Name='BI_Standard_PER'];
        User actualUser = BI_DataLoad.loadUsers(1, prof.id, 'Asesor')[0];

        System.runAs(actualUser){
            BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
            RecordType rt=[SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName='BIIN_Solicitud_Incidencia_Tecnica'];
            Account acc=BIIN_Test_Data_Load.createAccount('ACME');
            acc.OwnerId=actualUser.id;
            insert acc;
            Contact cont=BIIN_Test_Data_Load.createContact('Pachamama', acc);
            insert cont;
            Case caso = new Case(RecordTypeId = rt.id, AccountId = acc.id, OwnerId= actualUser.id, ContactId = cont.id, TGS_Urgency__c = '2-High');
            insert caso;
            Attachment attach = new Attachment(Name='Attachment Test',ParentId=caso.id ,Body=Blob.valueOf('Unit Test Attachment Body'));
            insert attach;
            Test.startTest();

            PageReference p = Page.BI2_COL_DetalleSolicitud_pag;
            p.getParameters().put('Idcaso', caso.Id);
            Test.setCurrentPage(p);

            ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
            BI2_COL_DetalleSolicitud_ctr ceController = new BI2_COL_DetalleSolicitud_ctr(controller);
            ceController.modBoolean=true;

            ceController.Modificar();
            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicket'));
            ceController.GuardarModif();
            ceController.Cancelar();
            ceController.attach_Id=attach.id;
            ceController.showAttachment();
            //nuevas
            ceController.MostrarPopUpEstado();
            ceController.NOOKdetalle();
            ceController.OKdetalle();
            ceController.TimeOUT();
            Test.stopTest();
        }
    }

    @isTest static void BI2_COL_DetalleSolicitud_tst_Incidencia() {
        BI_TestUtils.throw_exception=false;
        Profile prof = [Select Id From Profile Where Name='BI_Standard_PER'];
        User actualUser = BI_DataLoad.loadUsers(1, prof.id, 'Asesor')[0];

        System.runAs(actualUser){
            BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
            RecordType rt=[SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName='BIIN_Incidencia_Tecnica'];
            Account acc=BIIN_Test_Data_Load.createAccount('ACME');
            acc.OwnerId=actualUser.id;
            insert acc;
            Contact cont=BIIN_Test_Data_Load.createContact('Pachamama', acc);
            insert cont;
            Case caso = new Case(RecordTypeId = rt.id, AccountId = acc.id, OwnerId= actualUser.id, ContactId = cont.id, TGS_Urgency__c = '2-High');
            insert caso;
            Attachment attach = new Attachment(Name='Attachment Test',ParentId=caso.id ,Body=Blob.valueOf('Unit Test Attachment Body'));
            insert attach;
            Test.startTest();

            PageReference p = Page.BI2_COL_DetalleSolicitud_pag;
            p.getParameters().put('Idcaso', caso.Id);
            Test.setCurrentPage(p);

            ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
            BI2_COL_DetalleSolicitud_ctr ceController = new BI2_COL_DetalleSolicitud_ctr(controller);
            ceController.modBoolean=true;

            ceController.Cancelar();
            ceController.attach_Id=attach.id;
            ceController.showAttachment();
            //nuevas
            ceController.MostrarPopUpEstado();
            ceController.NOOKdetalle();
            ceController.OKdetalle();
            ceController.TimeOUT();

            ceController.loadData();
            try {
            ceController.detalleIncidencia.worklogId='worklogId';
            ceController.detalleIncidencia.posicionWI='200';
            ceController.detalleIncidencia.attachName='attachName';
            ceController.getAttBody();
            } catch(Exception e) {}

            Test.stopTest();
        }
    }

    @isTest static void BI2_COL_DetalleSolicitud_tst_fail()
    {
        BI_TestUtils.throw_exception=false;
        Profile prof = [Select Id From Profile Where Name='BI_Standard_PER'];
        User actualUser = BI_DataLoad.loadUsers(1, prof.id, 'Asesor')[0];

        System.runAs(actualUser)
        {
            BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
            RecordType rt=[SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName='BIIN_Solicitud_Incidencia_Tecnica'];
            Account acc=BIIN_Test_Data_Load.createAccount('ACME');
            acc.OwnerId=actualUser.id;
            insert acc;
            Contact cont=BIIN_Test_Data_Load.createContact('Pachamama', acc);
            insert cont;
            Case caso = new Case(RecordTypeId = rt.id, AccountId = acc.id, OwnerId= actualUser.id, ContactId = cont.id, TGS_Urgency__c = '2-High');
            insert caso;
            Attachment attach = new Attachment(Name='Attachment Test',ParentId=caso.id ,Body=Blob.valueOf('Unit Test Attachment Body'));
            insert attach;

            Test.startTest();

            PageReference p = Page.BIIN_Detalle_Solicitud;
            p.getParameters().put('Idcaso', caso.Id);
            Test.setCurrentPage(p);

            ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
            BI2_COL_DetalleSolicitud_ctr ceController = new BI2_COL_DetalleSolicitud_ctr(controller);
            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicketException500'));
            ceController.GuardarModif();
            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerAnexo'));
            ceController.attachName    = '0-#-0';
            ceController.worklogId     = 'LogId';
            ceController.posicionWI    = 'Pos01';
            ceController.getAttBody();

            ceController.StringWSCreacion='OK';
            ceController.MostrarPopUpEstado();
            Test.stopTest();
        }
    }


    @isTest static void BI2_COL_DetalleSolicitudGetBoddy_tst() {
        BI_TestUtils.throw_exception=false;

        Case caseTest = [SELECT Id, RecordTypeId, BI_Otro_Tipo__c, Status, Priority, Type, Subject, CurrencyIsoCode, Origin, BI_Confidencial__c,
                                Reason, Description, BI_Id_del_caso_Legado__c, BIIN_Tiempo_Neto_Apertura__c
                            FROM Case WHERE BI_Id_del_caso_Legado__c =: 'INC000000022352'];

        Test.startTest();
        PageReference p = Page.BI2_COL_DetalleIncidencia_pag;
        p.getParameters().put('Idcaso', caseTest.Id);
        Test.setCurrentPageReference(p);

        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caseTest);
        BI2_COL_DetalleSolicitud_ctr ceController = new BI2_COL_DetalleSolicitud_ctr(controller);
        ceController.attachName    = '0-#-0';
        ceController.worklogId     = 'LogId';
        ceController.posicionWI    = 'Pos01';
        ceController.rutaBase      = '';
        ceController.IdAdjunto     = '';
        ceController.caso=caseTest;
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerAnexo'));
        ceController.getAttBody();
        //ceController.crearWIRoD();

        /*PageReference Gotomenu = ceController.Gotomenu();
        ceController.ClosePopupError();
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerTicket'));
        ceController.loadData();
        ceController.doCancel();
        ceController.GuardarCampoConfidencial();
        ceController.ModificarCampoConfidencial();*/
        Test.stopTest();
    }
}