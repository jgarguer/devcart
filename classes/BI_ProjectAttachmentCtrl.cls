public with sharing class BI_ProjectAttachmentCtrl {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ana Escrich
	    Company:       Salesforce.com
	    Description:   Class to attach files to cases
	   	Test Class:	   BI_ProjectAttachmentCtrl_TEST
	    
	    History:
	    
	    <Date>            <Author>          	<Description>
	    10/07/2014        Diego Arenas      	 	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public Id attachmentParentId 		{get;set;}
	public string attachmentDescription	{get;set;}
	public boolean haveError			{get;set;}
	public boolean noError				{get;set;}
	public boolean alreadyAttch			{get;set;}
	public string msgError				{get;set;}
	public Attachment myDoc				{get;set;}
	   
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Constructor
    
    History:
    
    <Date>            <Author>          	<Description>
    25/06/2014        Diego Arenas       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public BI_ProjectAttachmentCtrl(){
		haveError=false;
		noError=false;
		alreadyAttch = false;
		if((Apexpages.currentPage().getParameters().get('Id')!=null)&&(Apexpages.currentPage().getParameters().get('description')!=null)){
			attachmentDescription = String.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('description'));
			attachmentParentId = String.escapeSingleQuotes(Apexpages.currentPage().getParameters().get('Id'));
		}
		list<Milestone1_Project__c> listProject = [Select Id From Milestone1_Project__c where id = : attachmentParentId];
		if(listProject.isEmpty()){
			haveError = true;
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.BI_InvalidProject);
		}
		myDoc = new Attachment();
	}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Attach files in Reclamos
    
    History:
    
    <Date>            <Author>          	<Description>
    25/06/2014        Diego Arenas       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	public void attachFile(){
		try{
			system.debug(myDoc);
			if(myDoc.Name!=null){
	        	myDoc.ParentId = attachmentParentId;
	        	myDoc.Description = attachmentDescription;
	        	insert myDoc;
	        	haveError=false;
	        	noError=true;
	        	alreadyAttch = true;
	        	myDoc = new Attachment();
	        	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'my error msg');
	        	ApexPages.addMessage(myMsg);
	    	}
	    	else{
	    		haveError=true;
	    		noError=false;
	    		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.BI_InvalidFile);
	    	}
			
		}catch (exception Exc){
			haveError = true;
			noError=false;
		   	BI_LogHelper.generate_BILog('BI_ProjectAttachmentCtrl.attachFile', 'BIEN', Exc, 'Class');
		}
	}
	
	public pagereference returnpage (){
		Pagereference pageNew = new Pagereference('/apex/BI_DocumentacionProyectos2?id='+attachmentParentId);
		//Pagereference pageNew = new Pagereference('/'+attachmentParentId);
		return pageNew;
	}
}