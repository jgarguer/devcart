public virtual class PCA_TemplateController {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jorge Longalera
    Company:       Salesforce.com
    Description:   Class for show Template 
    
    History:
    
    <Date>            <Author>              <Description>
    09/06/2014        Jorge Longalera      Initial version
    30/06/2014        Micah Burgos         v2
    15/07/2014        Antonio Moruno       v3
    28/07/2014        Antonio Moruno       v4
    30/05/2016        Jonathan Becerril    v5
    13/10/2016        Sara Nuñez           Added var for tracking the activity in Google Analytics
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public list<Account> ActAcc {get;set;} 
    public list<Account> ActAccs {get;set;} 
    public string ValueOption {get;set;}
    public static final String HOME_PAGE_URL = 'PCA_login';
    //public boolean login {get;set;}  
    public list <User> UserActual {get;set;}
    public list <BI_Contact_Customer_Portal__c> CCPActual {get;set;}
    
    public String templateStyleCommon{get; set;}
    public String resourceStyleCommon{get; set;}
    public User portalUser{get;set;}
    public String AccName{get;set;}
    public String AccSegmento{get;set;} 

    public List<BI_Multimarca_PP__c> footerLinks {get;set;}
    public List<BI_Multimarca_PP__c> SocialNetwork {get;set;}
    
    public map<string,string> mapVisibility {get; private set;}
    public String oneDriveLink {get;set;}
    public String googleAnalyticsId {get{
        BI_PP_GoogleAnalytics__c portalPlatinoGAId = BI_PP_GoogleAnalytics__c.getInstance('Id');
        String idGA ='';
        if(portalPlatinoGAId != null){
            idGA = portalPlatinoGAId.BI_Google_Analytics_Id__c;
        }else{
            System.debug('The Google Analytics Id is not correctly configured');
        }
        return idGA;
    } set;} //added 13/10/2016
    //START DELOITTE
    
    public final String catalogName = 'TGSOL Catalog';
    
    public String ticketsPage {get{
        Id pId = UserInfo.getProfileId();
        Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
        if (p.size()>0){
            String pName = p[0].Name;
            System.debug('###Current profile: ' + pName);
            if (pName.startsWith('TGS')){
                System.debug('###Starts with TGS: true');
                //return 'PCA_Cases';
                return 'CWP_Cases'; //changed return of tha method to open the page developer by everis. - everis 23022017
            }
        }
        return 'PCA_ReclamosPedidos';
    }
    set;}
    
    
    /*public List<Categoria> listaCategorias{get{
        List<Categoria> listaCat = new List<Categoria>();
        Map<Id, NE__Catalog_Category__c> mapCat = new Map<Id, NE__Catalog_Category__c>();
        
        System.debug('########: ANTES PRODUCT LIST:');       
        List<NE__Catalog_Item__c> productList = TGS_Portal_Utils.getVisibleCatalogItems(UserInfo.getUserId());
        System.debug('########: ANTES PRODUCT LIST: ' + productList.size());
        
        for (NE__Catalog_Item__c ci: productList){
            System.debug('#Category: ' + ci.NE__Catalog_Category_Name__r.Name);
            System.debug('#Parent Category: ' + ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name);
            System.debug('#GrandParent Category: ' + ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.NE__Parent_Category_Name__r.Name);
            
            if (ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name == catalogName) {
                if (mapCat.put(ci.NE__Catalog_Category_Name__c, ci.NE__Catalog_Category_Name__r) == null) {
                    listaCat.add(new Categoria(ci.NE__Catalog_Category_Name__c, ci.NE__Catalog_Category_Name__r.Name));
                }
            } else if (ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.NE__Parent_Category_Name__r.Name == catalogName) {
                if (mapCat.put(ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__c, ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r) == null) {
                    listaCat.add(new Categoria(ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__c, ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name));
                }
            } 
        }
      
        if (listaCat.size() == 0){
            listaCat.add(new Categoria('','(No services available)'));
        }
        return listaCat;
        
    }set;}*/
    
    public List<String> listaGrupos {get{
        Set<String> setGroups = new Set<String>();
        List<String> listGroups = new List<String>();
        String userId = UserInfo.getUserId();
        List<CollaborationGroupMember> collGroupMemberList = [SELECT CollaborationGroupId FROM CollaborationGroupMember WHERE MemberId= :userId ];
        
        
        Set<Id> records = new Set<Id>();
        for(CollaborationGroupMember collMember: collGroupMemberList){
            records.add(collMember.CollaborationGroupId);
        }
        
        List<CollaborationGroup> listGroup = [SELECT Id, Name FROM CollaborationGroup where Id IN: records ORDER BY createdDate DESC LIMIT 2000];
        
        if (listGroup.size() == 0){
            listGroups.add(Label.TGS_CWP_GROUPS);
        }
        else{
            for(CollaborationGroup collGroup :listGroup){
                setGroups.add(collGroup.Name);
            }
            listGroups.addAll(setGroups);
        }
        return listGroups;
    }set;}
    
    public List<String> listaCategorias{get{
        Set<String> setCat = new Set<String>();
        List<String> listaCat = new List<String>();
        //Map<Id, NE__Catalog_Category__c> mapCat = new Map<Id, NE__Catalog_Category__c>();
        
        System.debug('########: ANTES PRODUCT LIST:');       
        List<NE__Catalog_Item__c> productList = TGS_Portal_Utils.getVisibleCatalogItems(UserInfo.getUserId());
        
        System.debug('########: ANTES PRODUCT LIST: ' + productList.size());
        
        Integer maxLength = 0;
        
        ///////////////////////////////////////// INI JBF
        setCat.add('All Services'); 
        ///////////////////////////////////////// FIN JBF 
        
        for (NE__Catalog_Item__c ci: productList){
            System.debug('#Category: ' + ci.NE__Catalog_Category_Name__r.Name);
            System.debug('#Parent Category: ' + ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name);
            System.debug('#Parent Category Length: ' + ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name.length());
            System.debug('#GrandParent Category: ' + ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.NE__Parent_Category_Name__r.Name);
            ////START DELOITE
            /*if(ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name!=null){
                String myCat=ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name;
                system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ DELOITTE MYCAT: '+ myCat);
                if (myCat.length()>maxLength){
                    maxLength=myCat.length();
                }
                system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ DELOITTE MAXLENGTH: '+ maxLength);
                
            }*/
            /////END DELOITTE
            setCat.add(ci.NE__ProductId__r.TGS_CWP_Tier_1__c);
            
            /*if (ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name == catalogName) {
                if (mapCat.put(ci.NE__Catalog_Category_Name__c, ci.NE__Catalog_Category_Name__r) == null) {
                    listaCat.add(new Categoria(ci.NE__Catalog_Category_Name__c, ci.NE__Catalog_Category_Name__r.Name));
                }
            } else if (ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.NE__Parent_Category_Name__r.Name == catalogName) {
                if (mapCat.put(ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__c, ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r) == null) {
                    listaCat.add(new Categoria(ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__c, ci.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name));
                }
            }*/ 
        }
        
        
        listaCat.addAll(setCat);
        
        if (listaCat.size() == 0){
            listaCat.add('(No services available)');
        }
        
        return listaCat;
        
        
    }set;}
        
    public String servicesPage {get{
        Id pId = UserInfo.getProfileId();
        Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
        if (p.size()>0){
            String pName = p[0].Name;
            System.debug('###Current profile: ' + pName);
            if (pName.startsWith('TGS')){
                System.debug('###Starts with TGS: true');
                return 'PCA_Installed_Services';
            }
        }
        return 'PCA_Inventory';
    }
    set;}
    
    public boolean esTGS {get{
        Id pId = UserInfo.getProfileId();
        Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
        if (p.size()>0){
            String pName = p[0].Name;
            System.debug('###Current profile: ' + pName);
            return pName.startsWith('TGS');
        }
        return false;
    }
    set;}
    
    public String profilePhotoURL {get{
        User owner = [SELECT SmallPhotoUrl FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1]; 
        return owner.SmallPhotoUrl;
    } set;}
    
    
    
    public Class Categoria{
        public String tId {get; set;}
        public String tName {get; set;}
        
        public Categoria(String tId, String tName) {
            this.tId = tId;
            this.tName = tName;
        }
    }
    
    public String idioma {get{
        return UserInfo.getLanguage();
    } set;}
    //END DELOITTE
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Load accounts of Portal user 
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Micah Burgos          Initial version
    21/03/2017        Everis                Redirect Documentation Change
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public PCA_TemplateController(){
        
            system.debug('PCA_TemplateController--> Start Constructor ');
            mapVisibility = PCA_ProfileHelper.getPermissionSet();
            
            List<User> listPortalUser = [Select id, Pais__c, ContactId, ProfileId, Profile.Name from User where id=:UserInfo.getUserId() limit 1];
            
            if(listPortalUser!= null && listPortalUser.size()>0) {
                this.portalUser = listPortalUser.get(0); 
            }
            
            //findStyles();
            
                
                
    
                //login = false;
                ActAccs = BI_AccountHelper.getAccounts();
                
                Id ActAccId = BI_AccountHelper.getCurrentAccountId();
                
                
                if (ActAccId!=null){
                    UserActual = [Select Name, ContactId,Contact.BI_Cuenta_activa_en_portal__c, AccountId FROM User where Id =:UserInfo.getUserId()];
                    //system.debug('UserActual' +UserActual[0]);
                    if(ActAccs.size() == 1){
                        system.debug('ActAccs' +ActAccs);
                        system.debug('ActAccs.size' +ActAccs.size());
                        String TotalName = ActAccs[0].Name;
                        if(TotalName.length() > 32){
                            AccName = TotalName.substring(0,32);
                            AccName = AccName + '...';
                        }else{
                            AccName = TotalName;
                        }
                        
                    }
                    ValueOption = ActAccId;
                    Account SegAccount = [Select Id, Name,BI_Country__c, BI_Segment__c, CWP_OneDriveAddress__c FROM Account Where Id=:ActAccId];
                    if(SegAccount!=null){
                        //List<String> accSegmentoList = SegAccount[0].BI_SegmentoRef__r.Name;
                        AccSegmento =  SegAccount.BI_Segment__c;
                        system.debug('AccSegmento**'+AccSegmento);
                    }
                    buildFooter(SegAccount); 
                    
                    /*---------------------
                    Start Redirect Documentation Change
                    -----------------------*/
                    system.debug('perfil: ' + portalUser.Profile.Name);
                    system.debug('visibilidad de documentos: ' + mapVisibility.get('pca_documentmanagement'));
                    system.debug('link a one drive: ' + SegAccount.CWP_OneDriveAddress__c);
                    if(portalUser.Profile.Name.containsIgnoreCase('TGS') && mapVisibility.get('pca_documentmanagement') != null && mapVisibility.get('pca_documentmanagement') == 'true'){
                        if(SegAccount.CWP_OneDriveAddress__c != null){
                            oneDriveLink = SegAccount.CWP_OneDriveAddress__c;
                        }else{
                            mapVisibility.put('pca_documentmanagement', 'false');
                        }
                    }
                    
                    /*---------------------
                    End Redirect Documentation Change
                    -----------------------*/

                }else{
                    ValueOption = ' '; 
                }

                
                system.debug('****ActAcc' +ActAcc);
                system.debug('****ActAccs' +ActAccs);
            
        
    }   
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   Create Footer for Platino
    
    History:
    
    <Date>            <Author>              <Description>
    29/10/2014        Antonio Moruno        Initial version
    25/08/2016        Jose Miguel Fierro    Normalize URLs
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public void buildFooter(Account Acc){   

        //String Seg = Acc.BI_Segmento_Ref__r.Name;
        if(Acc.BI_Segment__c != null){
            footerLinks = [SELECT Name, BI_Country__c, BI_Segment__c, BI_Posicion__c, BI_URL__c,BI_Activo__c FROM BI_Multimarca_PP__c WHERE BI_Activo__c=true AND BI_Segment__c =: Acc.BI_Segment__c AND BI_Country__c =:Acc.BI_Country__c AND RecordType.DeveloperName ='BI_Pie_de_pagina' order by BI_Posicion__c];
            SocialNetwork = [SELECT Name, BI_Country__c, BI_Segment__c, BI_Posicion__c, BI_URL__c,BI_Activo__c,BI_Facebook__c,BI_Google__c,BI_Instagram__c,BI_Tuenti__c,BI_Twitter__c,BI_Yammer__c FROM BI_Multimarca_PP__c WHERE BI_Activo__c=true AND BI_Segment__c =: Acc.BI_Segment__c AND BI_Country__c =:Acc.BI_Country__c AND RecordType.DeveloperName='BI_Redes_sociales'];
        }else{  
            footerLinks = [SELECT Name, BI_Country__c, BI_Segment__c, BI_Posicion__c, BI_URL__c,BI_Activo__c FROM BI_Multimarca_PP__c WHERE BI_Activo__c=true AND BI_Segment__c = 'Empresas' AND BI_Country__c =:Acc.BI_Country__c AND RecordType.DeveloperName='BI_Pie_de_pagina' order by BI_Posicion__c];
            SocialNetwork = [SELECT Name, BI_Country__c, BI_Segment__c, BI_Posicion__c, BI_URL__c,BI_Activo__c,BI_Facebook__c,BI_Google__c,BI_Instagram__c,BI_Tuenti__c,BI_Twitter__c,BI_Yammer__c FROM BI_Multimarca_PP__c WHERE BI_Activo__c=true AND BI_Segment__c = 'Empresas' AND BI_Country__c =:Acc.BI_Country__c AND RecordType.DeveloperName='BI_Redes_sociales'];
        }

        // START JMF 25/08/2016 - Normalize URLS
        System.debug('[LIMIT CPU.1]: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime());
        Pattern pat = Pattern.compile('^\\w*?\\:\\/\\/.*');
        if(footerLinks != null) {
            for(BI_Multimarca_PP__c multimarca : footerLinks) {
                if(multimarca.BI_URL__c != null && !pat.matcher(multimarca.BI_URL__c).matches()) {
                    multimarca.BI_URL__c = 'http://' + multimarca.BI_URL__c;
                }
            }
        }
        if(SocialNetwork != null) {
            for(BI_Multimarca_PP__c multimarca : SocialNetwork) {
                if(!String.isBlank(multimarca.BI_URL__c) && !pat.matcher(multimarca.BI_URL__c).matches()) {
                    multimarca.BI_URL__c = 'http://' + multimarca.BI_URL__c;
                }
                if(!String.isBlank(multimarca.BI_Facebook__c) && !pat.matcher(multimarca.BI_Facebook__c).matches()) {
                    multimarca.BI_Facebook__c = 'http://' + multimarca.BI_Facebook__c;
                }
                if(!String.isBlank(multimarca.BI_Google__c) && !pat.matcher(multimarca.BI_Google__c).matches()) {
                    multimarca.BI_Google__c = 'http://' + multimarca.BI_Google__c;
                }
                if(!String.isBlank(multimarca.BI_Instagram__c) && !pat.matcher(multimarca.BI_Instagram__c).matches()) {
                    multimarca.BI_Instagram__c = 'http://' + multimarca.BI_Instagram__c;
                }
                if(!String.isBlank(multimarca.BI_Tuenti__c) && !pat.matcher(multimarca.BI_Tuenti__c).matches()) {
                    multimarca.BI_Tuenti__c = 'http://' + multimarca.BI_Tuenti__c;
                }
                if(!String.isBlank(multimarca.BI_Twitter__c) && !pat.matcher(multimarca.BI_Twitter__c).matches()) {
                    multimarca.BI_Twitter__c = 'http://' + multimarca.BI_Twitter__c;
                }
                if(!String.isBlank(multimarca.BI_Yammer__c) && !pat.matcher(multimarca.BI_Yammer__c).matches()) {
                    multimarca.BI_Yammer__c = 'http://' + multimarca.BI_Yammer__c;
                }
            }
        }
        System.debug('[LIMIT CPU.2]: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime());
        // END JMF 25/08/2016 - Normalize URLS
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Retrieves picklist of portal user accounts
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Micah Burgos          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public List<SelectOption> getAccs() {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            List<SelectOption> options = new List<SelectOption>();
            if(!ActAccs.isEmpty()){
            for(Account accs :ActAccs){
                options.add(new SelectOption(accs.Id,accs.Name));
            }
            }
            return options;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_TemplateController.getAccs', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Retrieves portal user
    
    History:
    
    <Date>            <Author>              <Description>
    30/06/2014        Micah Burgos          Initial version
    31/05/2017        Cristina Rodriguez    Commented Usuario_SolarWinds__c, Usuario_BO__c, Sector__c, pass_BO__c, token_SolarWinds__c (User) from SELECT
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public User usuarioPortal{
        get{
            try{
                if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
                if(usuarioPortal==null){
                System.debug('se carga cada vez?');
                usuarioPortal = [Select id, FullPhotoUrl, name, CompanyName, ContactId, MobilePhone from User where id=:UserInfo.getUserId()];
                //Usuario_SolarWinds__c, Usuario_BO__c, Sector__c, pass_BO__c, token_SolarWinds__c
                }
                return usuarioPortal;
            }catch (exception Exc){
                 BI_LogHelper.generate_BILog('PCA_TemplateController.usuarioPortal', 'Portal Platino', Exc, 'Class');
                 return null;
            }
        }
        set;
    }
    
    /*
      Fills style text variables
    TO DO: Ojo que aquí sale el estilo por pais, podría ser por marca comercial, ademas´, para futuro controlar que el usuario no sea de holding.
    */
    /*private void findStyles() {
        try{
            String paisValue = this.portalUser.Pais__c != null?(this.portalUser.Pais__c.substring(0,3)).toUpperCase() :'STD';
            System.debug('--- paisValue: ' + paisValue);
            
            this.templateStyleCommon ='';
            this.resourceStyleCommon = 'Index'; 
            
            list<PCA_CSS_Style__c> listStyles = [
                Select CSS_Body__c, prueba_imagen_1__c
                From PCA_CSS_Style__c 
                where Pais__c includes (: paisValue)
                and Paginas_web__c includes (: getCurrentPageName() )  
                order by template__c desc
            ];
            
            for(PCA_CSS_Style__c myStyle: listStyles ) {
                this.templateStyleCommon += myStyle.CSS_Body__c;
                this.resourceStyleCommon += myStyle.prueba_imagen_1__c;
            }
            
            System.debug('templateStyleCommon: ' + this.templateStyleCommon);   
            System.debug('resourceStyleCommon: ' + this.resourceStyleCommon);
        }catch (exception Exc){
             BI_LogHelper.generate_BILog('PCA_TemplateController.findStyles', 'Portal Platino', Exc, 'Class');
        }
    }*/
    /*  
    private String getCurrentPageName() {
        try {
            list<String> urlParts = Apexpages.currentPage().getUrl().split('/');
            return urlParts.get(urlParts.size()-1);
            
        } catch( Exception e ) {
            BI_LogHelper.generate_BILog('PCA_TemplateController.getCurrentPageName', 'Portal Platino', Exc, 'Class');
            return '';
        } 
    }*/
    
    /*public PageReference enviarALoginComm(){
        List<NetworkMember> listNetworkMember = [
            Select MemberId 
            From NetworkMember 
            where NetworkId= :Network.getNetworkId()
        ];
        
        for(NetworkMember oneNetMember :listNetworkMember ) {
            System.debug('-----> ID usuario portal: ' + UserInfo.getUserId()+ '. ID member network: ' + oneNetMember.MemberId); 
            if(oneNetMember.MemberId ==  UserInfo.getUserId()){
                return null;
            }
        }
        //TODO quiza convenga pasar las label y las static final a custom settings
        //String finalURL = Test.isRunningTest()?System.Label.PCA_Test_Current_site_url:Site.getBaseUrl();
        Map<String, String> parameters = System.currentPageReference().getParameters();
        System.debug('paramenters: '+parameters);
        String currentURL = System.currentPageReference().getUrl().replace('/apex/', '');
        System.debug('ABORDA ' + currentURL);
        
        if ( ! currentURL.equalsIgnoreCase( HOME_PAGE_URL ) ) {
            Pagereference pageRef = Page.PCA_Login;
            pageRef.getParameters().put('retURL',currentURL);
            pageRef.setRedirect(true);
            return pageref;
        }
        return null;
    }*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Method that checks if credentials are correct
    
    History:
    
    <Date>            <Author>              <Description>
    15/07/2014        Antonio Moruno          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    /*public PageReference enviarALoginComm(){
        system.debug('Network.getNetworkId(): '+Network.getNetworkId());
        system.debug('UserInfo.getUserId(): '+UserInfo.getUserId());
        try{
            System.debug('***TEMPLATE CONTROLLER: enviarALoginComm***');
            if(!Apexpages.currentPage().getUrl().contains('PCA_Login')){
                // Informativo: System.debug('#### communitiesLanding: ' + Network.communitiesLanding().getUrl());
                
                List<NetworkMember> nMList = [Select MemberId From NetworkMember where NetworkId= :Network.getNetworkId() and MemberId = : UserInfo.getUserId()];
                
                if(!nMList.isEmpty()){
                    if(!PCA_ProfileHelper.checkPermissionSet(getCurrentPage())){
                        System.debug('***TEMPLATE CONTROLLER: CHECK PERMISSION REDIRECT ***');
                        return new PageReference(Site.getBaseUrl() + 'PCA_Home');
                        //return new PageReference('PCA_Home'); 
                    }
                    return null;
                }else{
                    String urlFinal = Test.isRunningTest()?System.Label.PCA_Test_Current_site_url:Site.getBaseUrl();
                    //String urlFinal = Site.getBaseUrl();
                    return new PageReference(urlFinal + 'PCA_Login?retURL='+System.currentPageReference().getUrl().replace('/apex', ''));   
                }
            }else{
                system.debug('login');
                return null;
            }
            
            /*
            // Informativo: System.debug('#### communitiesLanding: ' + Network.communitiesLanding().getUrl());
            List<NetworkMember> nMList = [Select MemberId From NetworkMember where NetworkId= :Network.getNetworkId()];
            if(nMList!=null && nMList.size() > 0)
                for(NetworkMember oneNetMember :nMList){
                    System.debug('-----> ID usuario portal: ' + UserInfo.getUserId()+ '. ID member network: ' + oneNetMember.MemberId); 
                    if(oneNetMember.MemberId ==  UserInfo.getUserId()){
                        return null;
                    }
                }
            String urlFinal = Test.isRunningTest()?System.Label.PCA_Test_Current_site_url:Site.getBaseUrl();
            system.debug('urlFinal: '+urlFinal);
            system.debug('urlFinal1: '+urlFinal + 'PCA_Login?retURL='+System.currentPageReference().getUrl().replace('/apex', ''));
            return new PageReference(urlFinal + 'PCA_Login?retURL='+System.currentPageReference().getUrl().replace('/apex', ''));
            */  
        /*}catch( Exception e ) {
            BI_LogHelper.generate_BILog('PCA_TemplateController.enviarALoginComm', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }*/
    
    public PageReference enviarALoginComm(){
        
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            System.debug('***HOME CONTROLLER: enviarALoginComm***');
            String pcaLoginStr = (Apexpages.currentPage().getUrl()!=null)?String.escapeSingleQuotes(Apexpages.currentPage().getUrl()):null;
            if(!pcaLoginStr.contains('PCA_Login')){
                // Informativo: System.debug('#### communitiesLanding: ' + Network.communitiesLanding().getUrl());
                List<NetworkMember> nMList = [Select MemberId From NetworkMember where NetworkId= :Network.getNetworkId() and MemberId = : UserInfo.getUserId()];
                if(!nMList.isEmpty()){
                    return null;
                }else{
                    //String urlFinal = Test.isRunningTest()?System.Label.PCA_Test_Current_site_url:Site.getBaseUrl();
                    String urlFinal = Site.getBaseUrl() + '/';
                    String currentURL = String.escapeSingleQuotes(System.currentPageReference().getUrl());
                    return new PageReference(urlFinal + 'PCA_Login?retURL='+currentURL.replace('/apex', ''));   
                }
            }else{
                return null;
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_HomeController.enviarALoginComm', 'Portal Platino', Exc, 'Class');
            return null;
        }   
        
        /*
        if(nMList!=null && nMList.size() > 0)
            for(NetworkMember oneNetMember :nMList){
                System.debug('-----> ID usuario portal: ' + UserInfo.getUserId()+ '. ID member network: ' + oneNetMember.MemberId); 
                if(oneNetMember.MemberId ==  UserInfo.getUserId()){
                    return null;
                }
            }
        String urlFinal = Test.isRunningTest()?System.Label.PCA_Test_Current_site_url:Site.getBaseUrl();
        system.debug(urlFinal + 'PCA_Login?retURL='+System.currentPageReference().getUrl().replace('/apex', ''));
        system.debug(System.currentPageReference().getUrl().replace('/apex', ''));
        return new PageReference(urlFinal + 'PCA_Login?retURL='+System.currentPageReference().getUrl().replace('/apex', ''));
        */
            
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   method to change account of user
    
    History:
    
    <Date>            <Author>              <Description>
    15/07/2014        Antonio Moruno          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public void changeAcc(){
        //BI_AccountHelper.setCurrentAccountId(ValueOption);
        
        //system.debug(ValueOption);
        
        //Id ActAccId = BI_AccountHelper.getCurrentAccountId();
        //system.debug(ActAccId);
        //CCPActual = [Select Name, BI_Contacto__c,     BI_Cliente__c FROM BI_Contact_Customer_Portal__c where BI_User__c =:UserInfo.getUserId() and BI_Cliente__c = : ValueOption];
        //system.debug(CCPActual);
        
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            
            //UserActual[0].ContactId = CCPActual[0].BI_Contacto__c;
            //UserActual[0].AccountId = CCPActual[0].BI_Cliente__c;
            UserActual[0].Contact.BI_Cuenta_activa_en_portal__c = ValueOption;
            system.debug('UserActual:' +UserActual); 
            update UserActual[0].Contact;
        }catch (Exception Exc){
            BI_LogHelper.generate_BILog('PCA_TemplateController.changeAcc', 'Portal Platino', Exc, 'Class');
            Apexpages.Message msg = new Apexpages.Message(Apexpages.Severity.Fatal, Exc.getMessage());
            Apexpages.addMessage(msg);    
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Retrieves formatted current page
    
    History:
    
    <Date>            <Author>              <Description>
    15/07/2014        Antonio Moruno          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public String getCurrentPage(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            String url = Apexpages.currentPage().getUrl();
            String[] pageSplit = url.split('/');
            String pageFinal = '';
            String currentPage = pageSplit[pageSplit.size()-1];
            if(pageSplit[pageSplit.size()-1].contains('?')){
                String[] pageSplit2 = pageSplit[pageSplit.size()-1].split('\\?');
                pageFinal = pageSplit2[0];
            }else{
                pageFinal = pageSplit[pageSplit.size()-1];
            }
            return pageFinal;
        }catch(Exception Exc) {
            BI_LogHelper.generate_BILog('PCA_TemplateController.getCurrentPage', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    
    //START DELOITTE
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jara Minguillon
    Company:       Deloitte
    Description:   Changes user language to english
    
    History:
    
    <Date>            <Author>              <Description>
    16/03/2015        Jara Minguillon          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    public PageReference changeToEN() {
        User thisUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        thisUser.LanguageLocaleKey = 'en_US';
        update thisUser;
        PageReference pr = ApexPages.currentPage();
        pr.setRedirect(true);
        return pr;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jara Minguillon
    Company:       Deloitte
    Description:   Changes user language to spanish
    
    History:
    
    <Date>            <Author>              <Description>
    16/03/2015        Jara Minguillon          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference changeToES() {
        User thisUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        thisUser.LanguageLocaleKey = 'es';
        update thisUser;
        PageReference pr = ApexPages.currentPage();
        pr.setRedirect(true);
        return pr;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jara Minguillon
    Company:       Deloitte
    Description:   Changes user language to french
    
    History:
    
    <Date>            <Author>              <Description>
    22/04/2015        Jara Minguillon          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference changeToFR() {
        User thisUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        thisUser.LanguageLocaleKey = 'fr';
        update thisUser;
        PageReference pr = ApexPages.currentPage();
        pr.setRedirect(true);
        return pr;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jara Minguillon
    Company:       Deloitte
    Description:   Changes user language to portuguese
    
    History:
    
    <Date>            <Author>              <Description>
    22/04/2015        Jara Minguillon          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference changeToPT() {
        User thisUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        thisUser.LanguageLocaleKey = 'pt_PT';
        update thisUser;
        PageReference pr = ApexPages.currentPage();
        pr.setRedirect(true);
        return pr;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Jara Minguillon
    Company:       Deloitte
    Description:   Changes user language to deutch
    
    History:
    
    <Date>            <Author>              <Description>
    22/04/2015        Jara Minguillon          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference changeToDE() {
        User thisUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        thisUser.LanguageLocaleKey = 'de';
        update thisUser;
        PageReference pr = ApexPages.currentPage();
        pr.setRedirect(true);
        return pr;
    }
    
    //END DELOITTE

}