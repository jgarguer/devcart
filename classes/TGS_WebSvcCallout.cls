public class TGS_WebSvcCallout {
    public static TGS_RodTicketingWs.OutputMapping9 createIncident() {
        TGS_RodTicketingWs.Rod_InterfaceSoap  sample = new TGS_RodTicketingWs.Rod_InterfaceSoap();
        sample.endpoint_x = 'http://api.salesforce.com/foo/bar';
    
        // This invokes the EchoString method in the generated class
        TGS_RodTicketingWs.OutputMapping9 echo = sample.CreateIncident('','','','','','','','','','','','','','', '','','','','','','','','','',new TGS_RodTicketingWs.InputMappingMwan());
        
        return echo;
    }  
    public static TGS_RodTicketingWs.OutputMapping10 modifyIncident() {
        TGS_RodTicketingWs.Rod_InterfaceSoap  sample = new TGS_RodTicketingWs.Rod_InterfaceSoap();
        sample.endpoint_x = 'http://api.salesforce.com/foo/bar';
        
        // This invokes the EchoString method in the generated class
        TGS_RodTicketingWs.OutputMapping10 echo = sample.ModifyIncident('','','SF','','',new TGS_RodTicketingWs.InputMappingMwan());
        
        return echo;
    } 
    
    public static TGS_RodTicketingWs.OutputMapping7 createChange() {
        TGS_RodTicketingWs.Rod_InterfaceSoap  sample = new TGS_RodTicketingWs.Rod_InterfaceSoap();
        sample.endpoint_x = 'http://api.salesforce.com/foo/bar';
        
        // This invokes the EchoString method in the generated class
        TGS_RodTicketingWs.OutputMapping7 echo = sample.CreateChange('','','','','',
                             '','','','','','','','','',
                             '','','', '' ,'','','','','');
        
        return echo;
        
    }  
    
    public static TGS_RodTicketingWs.OutputMapping8 modifyChange() {
        TGS_RodTicketingWs.Rod_InterfaceSoap  sample = new TGS_RodTicketingWs.Rod_InterfaceSoap();
        sample.endpoint_x = 'http://api.salesforce.com/foo/bar';
        
        // This invokes the EchoString method in the generated class
        TGS_RodTicketingWs.OutputMapping8 echo = sample.ModifyChange('','','','','');  
        
        return echo;
    }  
    
    public static TGS_RodTicketingWs.OutputMapping5 workinfos() {
        TGS_RodTicketingWs.Rod_InterfaceSoap  sample = new TGS_RodTicketingWs.Rod_InterfaceSoap();
        sample.endpoint_x = 'http://api.salesforce.com/foo/bar';
        
        // This invokes the EchoString method in the generated class
        TGS_RodTicketingWs.OutputMapping5 echo = sample.WorkInfos('','',1,'','',1,'','',1, '', '','','','','','', '','','',null,null,'', '', '', '');
        
        return echo;
    }
     public static TGS_RoDAttASTWs.OutputMapping1 Create_Modify_Asset() {
        TGS_RoDAttASTWs.InterfaceSoap   sample = new TGS_RoDAttASTWs.InterfaceSoap();
        sample.endpoint_x = 'http://api.salesforce.com/foo/bar';
        
        // This invokes the EchoString method in the generated class
        TGS_RoDAttASTWs.OutputMapping1 echo = sample.Create_Modify_Asset(new TGS_RoDAttASTWs.Arguments());
        
        return echo;
    }  
    public static TGS_Site_Sync.OutputMapping1 Create_Update_Site() {
        TGS_Site_Sync.Port0Soap  sample = new TGS_Site_Sync.Port0Soap();
        sample.endpoint_x = 'http://api.salesforce.com/foo/bar';
        
        // This invokes the EchoString method in the generated class
        TGS_Site_Sync.OutputMapping1 echo = sample.Create0('', 'Region_SiteName', 'SiteGroup_SiteName', 'Site_old', 'Site_New',
                                                    'Additional', 'City',  'Country',  'LocationID',  'Phone',
                                                     'Fax',  'Province',  'Street',  'Zone',  'CP',  'Description',  'z1DAction');
        
        return echo;
    }  
     
}