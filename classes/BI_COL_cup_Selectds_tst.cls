/************************************************************************************
* Avanxo Colombia
* @author           Raul Mora href=<rmora@avanxo.com>
* Proyect:          Telefonica
* Description:      Test class for BI_COL_cup_Selectds_ctr
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-07-30      Raul Mora (RM)				    Create Class
* @version   1.1    2015-08-10      Dolly Fierro (DF)		
* @version   1.2    2015-08-31      Oscar Jimenez (OJ)				Mejorar cobertura
*			 1.3    2017-17-17		Pedro Párraga					Add Method myUnitTestController2 to test builder BI_COL_cup_Selectds_ctr
*            1.2    23-02-2017      Jaime Regidor           		Adding Campaign Values, Adding Catalog Country
					20/09/2017      Angel F. Santaliestra           Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private class BI_COL_cup_Selectds_tst 
{
	public static list<Profile> lstPerfil;
	public static list <UserRole> lstRoles;
	public static list<User> lstUsuarios;
	public static list<User> lstUsuariosCrear;
	public static list <Campaign> lstCampana;
	public static List<RecordType> lstRecTyp;
	public static list<BI_COL_Modificacion_de_Servicio__c> lstMSSel;
	
	public static User objUsuario  = new User();
	public static Account objCuenta;
	public static Opportunity objOportunidad;
	public static Campaign objCampana;
	public static BI_COL_cup_Selectds_ctr objCupSelectds;
	public static BI_COL_Modificacion_de_Servicio__c objMS;
	public static BI_COL_Anexos__c objAnexos;
	public static BI_COL_Descripcion_de_servicio__c objDesSer;
	
	public static String idOpt {get;set;}

	public static void crearData()
	{
			
		//lstPerfil             										= new list <Profile>();
		//lstPerfil               									= [Select id,Name from Profile where Name = 'Administrador del sistema'];
		//system.debug('datos Perfil '+lstPerfil);
		
		/*lstRoles                									= new list <UserRole>();
		lstRoles                									= [Select id,Name from UserRole where Name = 'Telefónica Global'];
		system.debug('datos Rol '+lstRoles);
		
		//perfiles
		lstPerfil             			= new List <Profile>();
		lstPerfil               		= [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
		system.debug('datos Perfil '+lstPerfil);
				
		////usuarios
		//lstUsuariosCrear = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];
		//system.debug('lstUsuariosCrear ====> '+lstUsuariosCrear);

		//ObjUsuario
        objUsuario = new User();
        objUsuario.Alias = 'standt';
        objUsuario.Email ='pruebas@test.com';
        objUsuario.EmailEncodingKey = '';
        objUsuario.LastName ='Testing';
        objUsuario.LanguageLocaleKey ='en_US';
        objUsuario.LocaleSidKey ='en_US'; 
        objUsuario.ProfileId = lstPerfil.get(0).Id;
        objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        objUsuario.UserName ='pruebas1@test.com';
        objUsuario.EmailEncodingKey ='UTF-8';
        objUsuario.UserRoleId = lstRoles.get(0).Id;
        objUsuario.BI_Permisos__c ='Sucursales';
        objUsuario.Pais__c='Colombia';
        //insert objUsuario;
        
        lstUsuariosCrear = new list<User>();
        lstUsuariosCrear.add(objUsuario);
        insert lstUsuariosCrear;

		System.runAs(lstUsuariosCrear[0])
		{	*/

			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass;
			
			/*objUsuario 													= new User();
			objUsuario.Alias 											= 'standt';
			objUsuario.Email											='pruebas@test.com';
			objUsuario.EmailEncodingKey 								= '';
			objUsuario.LastName											='Testing';
			objUsuario.LanguageLocaleKey								='en_US';
			objUsuario.LocaleSidKey										='en_US'; 
			objUsuario.ProfileId										= lstPerfil.get(0).Id;
			objUsuario.TimeZoneSidKey									='America/Los_Angeles';
			objUsuario.UserName											='pruebas@test.com';
			objUsuario.EmailEncodingKey									='UTF-8';
			objUsuario.UserRoleId 										= lstRoles.get(0).Id;
			objUsuario.BI_Permisos__c 									='Sucursales';
			//objUsuario.Division_Allocation__c='COP';
			
			lstUsuarios 												= new List<User>();
			lstUsuarios.add(objUsuario);
			insert lstUsuarios;
			system.debug('datos Usuario '+lstUsuarios);*/
			
			 
			NE__Catalog__c objCatalogo 									= new NE__Catalog__c();
			objCatalogo.Name 											= 'prueba Catalogo';
			objCatalogo.BI_country__c                       			= 'Colombia';
			insert objCatalogo; 
			
			objCampana = new Campaign();
			objCampana.Name = 'Campaña Prueba';
			objCampana.BI_Catalogo__c = objCatalogo.Id;
			objCampana.BI_COL_Codigo_campana__c = 'prueba1';
			objCampana.BI_Country__c = 'Colombia';
            objCampana.BI_Opportunity_Type__c = 'Fijo';
            objCampana.BI_SIMP_Opportunity_Type__c = 'Alta';
			insert objCampana;
			lstCampana = new List<Campaign>();
			lstCampana = [select Id, Name from Campaign where Id =: objCampana.Id];
			system.debug('datos Cuenta '+lstCampana);
			
			objCuenta                                       			= new Account();
			objCuenta.Name                                  			= 'prueba';
			objCuenta.BI_Country__c                         			= 'Colombia';
			objCuenta.TGS_Region__c                         			= 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c    			= '';
			objCuenta.CurrencyIsoCode                       			= 'COP';
			objCuenta.BI_Segment__c                         = 'test';
			objCuenta.BI_Subsegment_Regional__c             = 'test';
			objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			system.debug('datos Cuenta '+objCuenta);
			
			objOportunidad                                        		= new Opportunity();
			objOportunidad.Name                                   		= 'prueba opp';
			objOportunidad.AccountId                              		= objCuenta.Id;
			objOportunidad.BI_Country__c                          		= 'Colombia';
			objOportunidad.CloseDate                              		= System.today().addDays(+5);
			objOportunidad.StageName                              		= 'F5 - Solution Definition';
			objOportunidad.CurrencyIsoCode                        		= 'COP';
			objOportunidad.Certa_SCP__contract_duration_months__c 		= 12;
			objOportunidad.BI_Plazo_estimado_de_provision_dias__c 		= 0 ;
			//objOportunidad.OwnerId                                		= lstUsuarios[0].id;
			//GMN 21/01/2017 - added to avoid errors in test
			objOportunidad.RecordTypeId                                 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Oportunidad Local').getRecordTypeId();
			//
			insert objOportunidad;
			system.debug('Datos Oportunidad'+objOportunidad);
			
			lstRecTyp = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
			system.debug('datos de FUN '+lstRecTyp);
			
			objAnexos               = new BI_COL_Anexos__c();
			objAnexos.Name          = 'FUN-0041414';
			objAnexos.RecordTypeId  = lstRecTyp[0].Id;
			insert objAnexos;
			System.debug('\n\n\n Sosalida objAnexos '+ objAnexos +'\n ====> objAnexos.Id '+objAnexos.Id);
			
			objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
			objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
			objDesSer.CurrencyIsoCode               = 'COP';
			insert objDesSer;
			System.debug('\n\n\n Sosalida objDesSer '+ objDesSer);
			
			objMS                         				= new BI_COL_Modificacion_de_Servicio__c();
			objMS.BI_COL_FUN__c           				= objAnexos.Id;
			objMS.BI_COL_Codigo_unico_servicio__c 		= objDesSer.Id;
			objMS.BI_COL_Clasificacion_Servicio__c  	= 'ALTA';
			objMS.BI_COL_Oportunidad__c					= objOportunidad.Id;
			objMS.BI_COL_Bloqueado__c 					= false;
			objMS.BI_COL_Estado_orden_trabajo__c		= 'Ejecutado';
			
			
			lstMSSel = new List<BI_COL_Modificacion_de_Servicio__c>();
			lstMSSel.add(objMS); 
			
			System.debug('\n\n\n Modificación de Servicio '+ objMS);
		//}
	}
	static testMethod void myUnitTest() 
    {
    	objUsuario=BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario)
		{
	    	Test.setMock(WebServiceMock.class, new ws_TrsMasivo_mws());
	    	crearData();
	 		objMS.BI_COL_Estado__c 						= 'Pendiente';	
	 		insert objMS;
			
			Test.startTest();
				ApexPages.currentPage().getParameters().put('idOpt',objOportunidad.Id);
				ApexPages.currentPage().getParameters().put('etapaOpt', objOportunidad.StageName);
	     		ApexPages.currentPage().getParameters().put('Name', objOportunidad.Name);
	     		
				BI_COL_cup_Selectds_ctr.WrapperMS wrpMs 				= new BI_COL_cup_Selectds_ctr.WrapperMS(objMS, true, false);
				BI_COL_cup_Selectds_ctr.WrapperMS wrpMsTwo 				= new BI_COL_cup_Selectds_ctr.WrapperMS(objMS, false, true);
				
				list<BI_COL_cup_Selectds_ctr.WrapperMS> lstWrpMs 		= new list<BI_COL_cup_Selectds_ctr.WrapperMS>();
				lstWrpMs.add(wrpMs);
				lstWrpMs.add(wrpMsTwo);
				
				objCupSelectds 											= new BI_COL_cup_Selectds_ctr();
				objCupSelectds.lstDS = lstWrpMs;
				
				System.debug('\n\n ***lstDS ---> ' + objCupSelectds.lstDS);
				
				objCupSelectds.action_seleccionarDS();
				objCupSelectds.action_seleccionarTodos();  
				
			Test.stopTest();
		}
	}
	
	static testMethod void myUnitTestTwo() 
    {
    	objUsuario=BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario)
		{
	    	crearData();
	    	objMS.BI_COL_Estado__c 						= 'Cerrada perdida';
	    	insert objMS;
	    	User usr = new User();
	    	usr = [SELECT id FROM User WHERE profile.Name NOT IN ('Administrador del sistema', 'Jefe de Ventas', 'Ingeniería/Preventa') AND IsActive = true LIMIT 1].get(0);
			
	     		PageReference pageRef = Page.BI_COL_cup_Selectds_pag;
	            Test.setCurrentPage(pageRef);
	            Test.startTest();
	            System.runAs(usr){
	            
	            	objCupSelectds 											= new BI_COL_cup_Selectds_ctr();
	            	ApexPages.currentPage().getParameters().put('idOpt',objOportunidad.Id);
					ApexPages.currentPage().getParameters().put('etapaOpt', objOportunidad.StageName);
		     		ApexPages.currentPage().getParameters().put('Name', objOportunidad.Name);
					BI_COL_cup_Selectds_ctr.WrapperMS wrpMs 				= new BI_COL_cup_Selectds_ctr.WrapperMS(objMS, true, false);
					BI_COL_cup_Selectds_ctr.WrapperMS wrpMsTwo 				= new BI_COL_cup_Selectds_ctr.WrapperMS(objMS, false, true);
					
					list<BI_COL_cup_Selectds_ctr.WrapperMS> lstWrpMs 		= new list<BI_COL_cup_Selectds_ctr.WrapperMS>();
					lstWrpMs.add(wrpMs);
					lstWrpMs.add(wrpMsTwo);
					
					objCupSelectds 											= new BI_COL_cup_Selectds_ctr();
					objCupSelectds.lstDS = lstWrpMs;
					
					System.debug('\n\n ***lstDS ---> ' + objCupSelectds.lstDS);
					
					objCupSelectds.action_seleccionarDS();
					objCupSelectds.action_seleccionarTodos();  
					objCupSelectds.BindData(1);
					objCupSelectds.getPageNumber();
					objCupSelectds.getPageSize();
					objCupSelectds.getPreviousButtonEnabled();
					objCupSelectds.totalPageNumber = 0;
					objCupSelectds.pageNumber = 1;
					objCupSelectds.pageSize = 5;
					objCupSelectds.getTotalPageNumber();
					objCupSelectds.ViewData();
					objCupSelectds.getNextButtonDisabled();
					objCupSelectds.nextBtnClick();
					objCupSelectds.previousBtnClick();
					objCupSelectds.getLstMS();
					BI_COL_cup_Selectds_ctr.getPermisionSetUsuario(objUsuario.Id);
	            }
			Test.stopTest();
		}
		

	}
	static testMethod void myUnitTestController() 
    {
    	objUsuario=BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario)
		{
	    	crearData();
	    	objMS.BI_COL_Estado_orden_trabajo__c		= 'Cancelado';
			insert objMS;		
			lstMSSel = new List<BI_COL_Modificacion_de_Servicio__c>();
			lstMSSel.add(objMS); 
	    	
	    	User usr = new User();
	    	usr = [SELECT id FROM User WHERE BI_Permisos__c IN ('Administrador del sistema', 'Jefe de Ventas', 'Ingeniería/Preventa') AND IsActive = true LIMIT 1].get(0);
			
	     		PageReference pageRef = Page.BI_COL_cup_Selectds_pag;
	            Test.setCurrentPage(pageRef);
	            Test.startTest();
	            System.runAs(usr){
	            
	            	objCupSelectds 											= new BI_COL_cup_Selectds_ctr();
	            	ApexPages.currentPage().getParameters().put('idOpt',objOportunidad.Id);
					ApexPages.currentPage().getParameters().put('etapaOpt', objOportunidad.StageName);
		     		ApexPages.currentPage().getParameters().put('Name', objOportunidad.Name);
					BI_COL_cup_Selectds_ctr.WrapperMS wrpMs 				= new BI_COL_cup_Selectds_ctr.WrapperMS(objMS, true, false);
					BI_COL_cup_Selectds_ctr.WrapperMS wrpMsTwo 				= new BI_COL_cup_Selectds_ctr.WrapperMS(objMS, false, true);
					
					list<BI_COL_cup_Selectds_ctr.WrapperMS> lstWrpMs 		= new list<BI_COL_cup_Selectds_ctr.WrapperMS>();
					lstWrpMs.add(wrpMs);
					lstWrpMs.add(wrpMsTwo);
					
					objCupSelectds 											= new BI_COL_cup_Selectds_ctr();
					objCupSelectds.lstDS = lstWrpMs;
					
					System.debug('\n\n ***lstDS ---> ' + objCupSelectds.lstDS);
					
					objCupSelectds.action_seleccionarDS();
					objCupSelectds.action_seleccionarTodos();  
					objCupSelectds.BindData(1);
					objCupSelectds.getPageNumber();
					objCupSelectds.getPageSize();
					objCupSelectds.getPreviousButtonEnabled();
					objCupSelectds.totalPageNumber = 0;
					objCupSelectds.pageNumber = 1;
					objCupSelectds.pageSize = 5;
					objCupSelectds.getTotalPageNumber();
					objCupSelectds.ViewData();
					objCupSelectds.getNextButtonDisabled();
	            }
			Test.stopTest();
		}

	}
	
	static testMethod void myUnitTestController2() 
    {
    	objUsuario=BI_COL_CreateData_tst.getCreateUSer();
		System.runAs(objUsuario)
		{
	    	crearData();
	    	Test.setMock(WebServiceMock.class, new ws_TrsMasivo_mws());
	    	objMS.BI_COL_Estado_orden_trabajo__c		= 'Cancelado';
	    	objMS.BI_COL_Motivo_cancelacion__c='Motivo de cancelamiento';
			insert objMS;		
			lstMSSel = new List<BI_COL_Modificacion_de_Servicio__c>();
			lstMSSel.add(objMS); 
	    	
	    	User usr = new User();
	    	usr = [SELECT id FROM User WHERE BI_Permisos__c IN ('Administrador del sistema', 'Jefe de Ventas', 'Ingeniería/Preventa') AND IsActive = true LIMIT 1].get(0);
			
	     		PageReference pageRef = Page.BI_COL_cup_Selectds_pag;
	            Test.setCurrentPage(pageRef);
	            Test.startTest();
	            System.runAs(usr){
	            
	            	objCupSelectds 											= new BI_COL_cup_Selectds_ctr();
	            	ApexPages.currentPage().getParameters().put('idOpt',objOportunidad.Id);
					ApexPages.currentPage().getParameters().put('etapaOpt', objOportunidad.StageName);
		     		ApexPages.currentPage().getParameters().put('Name', objOportunidad.Name);
					BI_COL_cup_Selectds_ctr.WrapperMS wrpMs 				= new BI_COL_cup_Selectds_ctr.WrapperMS(objMS, true, false);
					BI_COL_cup_Selectds_ctr.WrapperMS wrpMsTwo 				= new BI_COL_cup_Selectds_ctr.WrapperMS(objMS, false, true);
					
					list<BI_COL_cup_Selectds_ctr.WrapperMS> lstWrpMs 		= new list<BI_COL_cup_Selectds_ctr.WrapperMS>();
					lstWrpMs.add(wrpMs);
					lstWrpMs.add(wrpMsTwo);
					
					objCupSelectds 											= new BI_COL_cup_Selectds_ctr();
					objCupSelectds.lstDS = lstWrpMs;
					
					System.debug('\n\n ***lstDS ---> ' + objCupSelectds.lstDS);
					
					objCupSelectds.action_seleccionarDS();
					objCupSelectds.action_seleccionarTodos();  
					objCupSelectds.BindData(1);
					objCupSelectds.getPageNumber();
					objCupSelectds.getPageSize();
					objCupSelectds.getPreviousButtonEnabled();
					objCupSelectds.totalPageNumber = 0;
					objCupSelectds.pageNumber = 1;
					objCupSelectds.pageSize = 5;
					objCupSelectds.getTotalPageNumber();
					objCupSelectds.ViewData();
					objCupSelectds.getNextButtonDisabled();
					List<String> lstIdMS =new List<String>();
					lstIdMS.add(objMS.Id);
					//BI_COL_cup_Selectds_ctr.envioDatosTR(lstIdMS);
	            }
			Test.stopTest();
		}

	}


	static testMethod void myUnitTestController3() 
    {
    	//objUsuario=BI_COL_CreateData_tst.getCreateUSer();

    	User usr = new User();
	    usr = [SELECT id FROM User WHERE BI_Permisos__c IN ('Administrador del sistema', 'Jefe de Ventas', 'Ingeniería/Preventa') AND IsActive = true LIMIT 1].get(0);
		System.runAs(usr)
		{
	    	Test.setMock(WebServiceMock.class, new ws_TrsMasivo_mws());
	    	crearData();

	    	objOportunidad.StageName = 'F1 - Closed Won';
	    	update objOportunidad;

	 		objMS.BI_COL_Estado__c 						= 'Enviado';
	 		//objMS.BI_COL_Motivo_cancelacion__c			= 'Motivo de cancelamiento';	
	 		objMS.BI_COL_Estado_orden_trabajo__c        = 'Recibido';
	 		insert objMS;


			
			Test.startTest();
				ApexPages.currentPage().getParameters().put('idOpt',objOportunidad.Id);
				ApexPages.currentPage().getParameters().put('etapaOpt', objOportunidad.StageName);
	     		ApexPages.currentPage().getParameters().put('Name', objOportunidad.Name);
	     		
				BI_COL_cup_Selectds_ctr.WrapperMS wrpMs 				= new BI_COL_cup_Selectds_ctr.WrapperMS(objMS, true, false);
				BI_COL_cup_Selectds_ctr.WrapperMS wrpMsTwo 				= new BI_COL_cup_Selectds_ctr.WrapperMS(objMS, false, true);
				
				list<BI_COL_cup_Selectds_ctr.WrapperMS> lstWrpMs 		= new list<BI_COL_cup_Selectds_ctr.WrapperMS>();
				lstWrpMs.add(wrpMs);
				lstWrpMs.add(wrpMsTwo);
				
				objCupSelectds 											= new BI_COL_cup_Selectds_ctr();
				objCupSelectds.lstDS = lstWrpMs;
				
				System.debug('\n\n ***lstDS ---> ' + objCupSelectds.lstDS);
				
				objCupSelectds.action_seleccionarDS();
				objCupSelectds.action_seleccionarTodos();  
				
			Test.stopTest();
		}
	}
}