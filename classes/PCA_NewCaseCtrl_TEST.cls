@isTest
private class PCA_NewCaseCtrl_TEST {

	    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Micah Burgos
	    Company:       Salesforce.com
	    Description:   Test Class that manage coverage of PCA_NewCaseCtrl Class
	    				
	    History: 
	    
	     <Date>                     <Author>                <Change Description>
	    06/10/2014                  Micah Burgos             Initial Version
	    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	@isTest static void PCA_NewCaseCtrl_try_TEST() {
		BI_TestUtils.throw_exception = false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        PCA_NewCaseCtrl controller = new PCA_NewCaseCtrl();
        controller.checkPermissions();    
	}
	
	
	@isTest static void PCA_NewCaseCtrl_catch_TEST() {
		BI_TestUtils.throw_exception = true;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        PCA_NewCaseCtrl controller = new PCA_NewCaseCtrl();
        controller.checkPermissions();    
	}
	
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}