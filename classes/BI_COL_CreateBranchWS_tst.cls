/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Test Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           		Descripción
*           -----   ----------      --------------------            		---------------
* @version   1.0    2015-08-05      Manuel Esthiben Mendez Devia (MEMD)     Test Class
* @version   2.0    2015-08-28      William H.Castillo  modificación clases de pruebas subio al 86 %
					13/03/2017		Marta Gonzalez(Everis)			REING_Reingenieria de Contactos (Adaptación a la nueva lógica de contactos)
*************************************************************************************************************/
@isTest
private class BI_COL_CreateBranchWS_tst 
{
	
	Public static list <UserRole> 					lstRoles;
	Public static User 								objUsuario = new User();
	public static List<String> lstIdSede = new List<String>();
	public static List<BI_Punto_de_instalacion__c> lstSedes = new List<BI_Punto_de_instalacion__c>();
	public static list<BI_Log__c> lstLog = new list <BI_Log__c>();
		
	static void createData()
	{
		//List<Profile> lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
		////list<User> lstUsuarios = [Select Id FROM User where Pais__c = 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

		////lstRol
  //      lstRoles = new list <UserRole>();
  //      lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

  //      //ObjUsuario
  //      objUsuario = new User();
  //      objUsuario.Alias = 'standt';
  //      objUsuario.Email ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey = '';
  //      objUsuario.LastName ='Testing';
  //      objUsuario.LanguageLocaleKey ='en_US';
  //      objUsuario.LocaleSidKey ='en_US'; 
  //      objUsuario.ProfileId = lstPerfil.get(0).Id;
  //      objUsuario.TimeZoneSidKey ='America/Los_Angeles';
  //      objUsuario.UserName ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey ='UTF-8';
  //      objUsuario.UserRoleId = lstRoles.get(0).Id;
  //      objUsuario.BI_Permisos__c ='Sucursales';
  //      objUsuario.Pais__c='Colombia';
  //      insert objUsuario;
        
  //      list<User> lstUsuarios = new list<User>();
  //      lstUsuarios.add(objUsuario);

		//System.runAs( lstUsuarios[0] )
		//{
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c = true;

			Account objAccount											= new Account();
			objAccount.Name												= 'TSTAccount';
			objAccount.BI_Country__c = Label.BI_Colombia;
			insert objAccount;

			BI_Col_Ciudades__c objCity									= new BI_Col_Ciudades__c();
			objCity.BI_COL_Codigo_ciudad__c								= 'BOG';
			objCity.BI_COl_ID_Colombia__c								= 'COL';
			objCity.BI_COL_Pais__c										= 'Colombia';
			insert objCity;

			BI_Sede__c objAddress										= new BI_Sede__c();
			objAddress.BI_COL_Ciudad_Departamento__c = objCity.Id;
			objAddress.BI_Direccion__c = 'Test Street 123 Number 321';
			objAddress.BI_Localidad__c = 'Test Local';
			//objAddress.BI_COL_Estado_callejero__c = System.Label.BI_COL_LbValor3EstadoCallejero;
			objAddress.BI_COL_Sucursal_en_uso__c = 'Libre';
			objAddress.BI_Country__c = 'Colombia';
			objAddress.Name = 'Test Street 123 Number 321, Test Local Colombia';
			objAddress.BI_Codigo_postal__c = '12356';
			objAddress.BI_COL_Estado_callejero__c = 'Validado por callejero';
			insert objAddress;

			BI_Sede__c objAddress2										= new BI_Sede__c();
			objAddress2.BI_COL_Ciudad_Departamento__c = objCity.Id;
			objAddress2.BI_Direccion__c = 'Test Street 123 Number 321';
			objAddress2.BI_Localidad__c = 'Test Local';
			objAddress2.BI_COL_Estado_callejero__c = 'Validado por callejero';
			objAddress2.BI_COL_Sucursal_en_uso__c = 'Libre';
			objAddress2.BI_Country__c = 'Colombia';
			objAddress2.Name = 'Test Street 123 Number 321, Test Local Colombia';
			objAddress2.BI_Codigo_postal__c = '1235';
			insert objAddress2;

			BI_Log__c objLog											= new BI_Log__c();
			objLog.BI_COL_Informacion_recibida__c						= 'OK, Respuesta Procesada';
			//objLog.BI_COL_Contacto__c 									= lstContacts[0].Id;
			objLog.BI_COL_Estado__c										= 'FALLIDO';
			objLog.BI_COL_Interfaz__c									= 'NOTIFICACIONES';
			lstLog.add(objLog);	
			insert lstLog;	

			Contact objContact										= new Contact();
			objContact.FirstName									= 'pruebaContac';
			objContact.LastName										= 'pruebaContac';
			objContact.MobilePhone									= '3681269853';
			objContact.BI_Activo__c									= true;
			objContact.BI_Contacto_mercadeo__c						= false;
			objContact.BI_No_recibir_llamadas__c					= false;
			objContact.BI_Numero_de_documento__c					= '1085763264';
			objContact.BI_Proveedor__c								= false;
			objContact.BI_Regalos_empresariales__c					= false;
			objContact.BI_Representante_legal__c					= false;
			objContact.Estado__c									= '';
			objContact.Idioma__c									= '';
			objContact.Invitar_a_Eventos_de_Relacionamiento__c		= false;
			objContact.BI_Filtro_de_busqueda__c						= 'all';
			objContact.TEMPEXT_ID__c								= objAccount.Id;
			objContact.Certa_SCP__active__c							= true;
			objContact.TGS_Circulo_Allegados__c						= false;
			objContact.TGS_No_Enviar_Newsletter__c					= false;
			objContact.TGS_Surveys__c								= false;
			objContact.TGS_Enviar_Email_Billing_Inquiry__c			= true;
			objContact.TGS_Enviar_Email_Change__c					= true;
			objContact.TGS_Enviar_Email_Complaint__c				= true;
			objContact.TGS_Enviar_Email_Incident__c					= true;
			objContact.TGS_Enviar_Email_Order_Management_Case__c	= true;
			objContact.TGS_Enviar_Email_Query__c					= true;
			objContact.BI_COL_Contacto_principal_holding__c			= false;
			objContact.BI_COL_Potestad_para_firmar__c				= false;
			objContact.BI_COL_Principal__c							= false;
			objContact.BI_COL_Fotocopia_Cedula__c					= false;
			objContact.Phone										= '68544912';
			objContact.Email										= 'testContact1@test.com';
        	//REING-INI
        	//objContact.BI_Tipo_de_contacto__c						= 'TST';
        	objContact.BI_Tipo_de_contacto__c          = 'General';
			objContact.BI_Tipo_de_documento__c 			= 'DNI';
        	//REING_FIN
			objContact.BI_COL_Ciudad_Depto_contacto__c				= objCity.Id;
			objContact.BI_COL_Direccion_oficina__c					= 'K 54';
			objContact.AccountId = objAccount.id;
			insert objContact;

			Map<String, List<Object>> mapSedeInfo = new Map<String, List<Object>>
			{
				'Sede1'	=> new List<Object>{
					'prueba', objContact.Id, objAccount.Id, objContact.Id,
					'Principal', objAddress.Id, 'Si'
				},
				'Sede2'	=> new List<Object>{
					'prueba2', objContact.Id, objAccount.Id, objContact.Id,
				 	'Principal', objAddress.Id, 'No'
				},
				'Sede3'	=> new List<Object>{
					'prueba3', objContact.Id, objAccount.Id, objContact.Id,
					'Principal', objAddress2.Id, 'Si'
				}
			};

			for( String strKeySede : mapSedeInfo.keySet() )
			{
				BI_Punto_de_instalacion__c objSede					= new BI_Punto_de_instalacion__c();
				objSede.Name										= 'Nombre Sede' + String.valueOf( mapSedeInfo.get( strKeySede ).get( 0 ) );
				objSede.BI_Contacto__c								= String.valueOf( mapSedeInfo.get( strKeySede ).get( 1 ) );
				objSede.BI_Cliente__c								= String.valueOf( mapSedeInfo.get( strKeySede ).get( 2 ) );
				objSede.BI_COL_Contacto_instalacion__c				= String.valueOf( mapSedeInfo.get( strKeySede ).get( 3 ) );
				objSede.BI_COL_Tipo_sede__c							= String.valueOf( mapSedeInfo.get( strKeySede ).get( 4 ) );
				objSede.BI_Sede__c									= String.valueOf( mapSedeInfo.get( strKeySede ).get( 5 ) );
				objSede.BI_COL_Banda_ancha__c						= String.valueOf( mapSedeInfo.get( strKeySede ).get( 6 ) );
				lstSedes.add( objSede );
			}	
			insert lstSedes;
		//}
	}

 	static testmethod void test1()
 	{
 		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {	
			createData();
			String accion = null;
			String spl = null;

	 		Test.startTest();
			
			for( BI_Punto_de_instalacion__c objSede : lstSedes )
				lstIdSede.add(objSede.Id);

			Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );
			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );

	 		BI_COL_CreateBranchWS_cls.crearSucursalTelefonica( lstIdSede, accion );

	 		BI_COL_CreateBranchWS_cls.Validate_Addressduplicate( lstSedes, lstSedes );
	 		BI_COL_CreateBranchWS_cls.processSplit(spl);
			
			Test.stopTest();
		}
	}
	
	static testmethod void test2()
 	{
 		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			createData();
			String accion = 'Eliminar';
			String spl = null;
			
			Test.startTest();

			Test.setMock( WebServiceMock.class, new BI_COL_CreateContactWS_mws() );
			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );

	 		BI_COL_CreateBranchWS_cls.crearSucursalTelefonica(lstIdSede,accion);
	 		
	 		lstIdSede = new List<String>();
	 		lstIdSede.add('00Nw0000007jIzd');
	 		
	 		BI_COL_CreateBranchWS_cls.crearSucursalTelefonica( lstIdSede, accion );
	 		
	 		BI_COL_CreateBranchWS_cls.processSplit( '-\\x7c0-\\x7c-' );
	 		
	 		ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] res = new List<ws_wwwTelefonicaComNotificacionessalesfo.respuesta>();
	 		List<BI_Punto_de_instalacion__c> punInst = new List<BI_Punto_de_instalacion__c> ();

	 		Map<ID,ws_wwwTelefonicaComNotificacionessalesfo.sucursal> enviado = new Map<ID,ws_wwwTelefonicaComNotificacionessalesfo.sucursal>();

	 		BI_COL_CreateBranchWS_cls.creaLogTransaccion(res,punInst , enviado);

			Test.stopTest();
		}
	}
}