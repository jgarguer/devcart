global without sharing class BI_BackOffice_JOB implements Schedulable {

	global String email_user;
	global List<Id> lst_ven;
	global Integer pos;
	global Map<Id, String> result;
	global Integer option;

	global void execute(SchedulableContext sch) {
        
        system.abortJob(sch.getTriggerId());
        if(option == 0)
			BI_Siscel_Helper.BackOfficeVenta_Mass(email_user, lst_ven, pos, result);
		else if(option == 1)
			BI_Siscel_Helper.BackOfficeRecambio_Mass(email_user, lst_ven, pos, result);
		else if(option == 2)	
        	BI_Siscel_Helper.BackOfficeServicio_Mass(email_user, lst_ven, pos, result);
	}

    public BI_BackOffice_JOB(String email, List<Id> lst, Integer posi, Map<Id, String> res, Integer op){
    	
    	email_user = email;
    	lst_ven = lst;
    	pos = posi;
    	result = res;
    	option = op;
    	
    }

}