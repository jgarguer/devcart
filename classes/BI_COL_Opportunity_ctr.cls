/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-05-04      Daniel ALexander Lopez (DL)     New Controller
* @version   1.0    2015-05-11      Javier Tibamoza Cubillos (JATC) Ajuste para utilizar la clase como StandardController
* @version   1.0    2018-04-26      Alfonso Alvarez Elbal (AJAE)    Demanda 594: Se añade método para trasladar campo duración de contrato de Opty a MS para oportunidades de Colombia.
*************************************************************************************/
public class BI_COL_Opportunity_ctr
{
    public static final string F1_CLOSED_WON = 'F1 - Closed Won';
    public static final string F2_NEGOTATION = 'F2 - Negotiation';
    public static final string F3_OFFER_PRESENTED = 'F3 - Offer Presented';

    public BI_COL_Opportunity_ctr( ApexPages.StandardController stdController )
    {
        List<Opportunity> lstNew = new List<Opportunity>();
        BI_COL_Modificacion_de_Servicio__c oMS = (BI_COL_Modificacion_de_Servicio__c)stdController.getRecord();
        Opportunity opp;
        if( oMS.BI_COL_Oportunidad__r != null )
        {
            opp = oMS.BI_COL_Oportunidad__r;
        }
        else
        {
            opp = [ Select Id, StageName, BI_COL_Autoconsumo__c, AccountId, BI_COL_Clasificacion_Preventa__c
                    From Opportunity
                    Where Id =: oMS.BI_COL_Oportunidad__c ];
        }
        
        lstNew.add( opp );

        //cup_validar_Cierre_Opt( lstNew );
        fnValidarCierreOportunidad( lstNew, new List<Opportunity>() );
    }
    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public static void fnValidarCierreOportunidad(list<Opportunity> lstNew, list<Opportunity> lstOld )
    {
        if(BI_Opportunity_TriggerHelper.map_User.get(UserInfo.getUserId())== null){
            BI_Opportunity_TriggerHelper.loader_TriggerData(new List<Opportunity>());
        }
        User usu = BI_Opportunity_TriggerHelper.map_User.get(UserInfo.getUserId());
        system.debug( '\n\n ,.-.,.- lstNew.size() '+lstNew.size()+' \n\n lstNew.get(0) '+lstNew.get(0)+' \n\n lstOld '+lstOld );
        if( lstNew.size() == 1 && usu.Pais__c == label.BI_COL_lblColombia )
        {
            List<String> listMensajesError = new List<String>();

            Opportunity oppNew = (lstNew.size()> 0 ? lstNew[0] : null);
            Opportunity oppOld = (lstOld.size()> 0 ? lstOld[0] : null);
            system.debug( '\n\n ,.-.,.- oppNew.StageName '+oppNew.StageName );
            String mensajeError = '';
            //Valida solo si es F1
            if( oppNew.StageName == F1_CLOSED_WON) 
            { 
                BI_MigrationHelper.setSkippedTrigger('NE__OrderItem__c');
                listMensajesError = fnSearchErrorF1( oppNew, listMensajesError);
                BI_MigrationHelper.cleanSkippedTriggers();
                System.debug('\n\n Linea 65 listMensajesError.size(): '+ listMensajesError.size()+'\n\n');
            }
            //Valida para F1 o F2
            if( oppNew.StageName == F1_CLOSED_WON || oppNew.StageName == F2_NEGOTATION ) 
            {
                //listMensajesError=fnSearchErrorAccountMidas( oppNew,listMensajesError );
                BI_MigrationHelper.setSkippedTrigger('NE__OrderItem__c');
                listMensajesError=fnSearchError( oppNew, oppOld,listMensajesError);
                BI_MigrationHelper.cleanSkippedTriggers();
            }
            System.debug('\n\n Linea 32 listMensajesError.size(): '+ listMensajesError.size()+'\n\n');
            //Cuando está en F3
            if( oppNew.StageName == F3_OFFER_PRESENTED )
            {
                fnSearchErrorOtherStage( oppNew );
                listMensajesError = fnSearchErrorF1( oppNew ,listMensajesError);
            }
            System.debug('\n\n Linea 80 listMensajesError.size(): '+ listMensajesError.size()+'\n\n');
            if( listMensajesError.size() > 0 )
            {
                String tmp = '';
                System.debug('\n\n Linea 84 listMensajesError: '+listMensajesError+' \n\n');
                for( String msj : listMensajesError )
                    tmp += '<li><i>' + msj + '</i></li>';
                //tmp+='</ul>';
                oppNew.addError( tmp ,false);
            }
        }           
    }
    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public static List<String> fnSearchError( Opportunity oppNew, Opportunity oppOld, List<String> listMensajesError)
    {
        string sEtapaOppNew = ( oppNew != null ? oppNew.StageName : null );
        string sEtapaOppOld = ( oppOld != null ? oppOld.StageName : null );
        string mensajeError = '';

        List<BI_COL_Modificacion_de_Servicio__c> lstModUp = new List<BI_COL_Modificacion_de_Servicio__c>(); 
        //List<String> listMensajesError = new List<String>();
        Set<string> opCv = new Set<string>{'SERVICIO INGENIERIA','TRASLADO','COBRO UNICO'};

        List<BI_COL_Modificacion_de_Servicio__c> lstMS = fnFindMS( oppNew, '1' );
        
        List<BI_COL_CamposValidarEtapaOpp__c> lConf = [
            SELECT  ID, NAME, Api__c, Etapa_1__c, Etapa_2__c, Mensaje_Error__c,ValidacionEspecial__c, Api2__c
            FROM    BI_COL_CamposValidarEtapaOpp__c ];
        System.debug('\n\n oMS-->'+lstMS);
        for( BI_COL_Modificacion_de_Servicio__c objMS : lstMS )
        {
            sObject oMS = objMS;
            mensajeError = '';
            System.debug('\n\n oMS-->'+oMS+' -- '+objMS);
            for( BI_COL_CamposValidarEtapaOpp__c oConf : lConf )
            {
                System.debug('\n\n ----> oConf.Api__c: '+oConf.Api__c);
                if(!oConf.ValidacionEspecial__c  && 
                 oMS.get( oConf.Api__c ) == null //&& 
                    //sEtapaOppOld == oConf.Etapa_1__c && 
                    //sEtapaOppNew == oConf.Etapa_2__c
                    )
                {
                    mensajeError = ( mensajeError == '' ? oConf.Mensaje_Error__c : mensajeError + ' ' + oConf.Mensaje_Error__c );
                }
                if( oConf.ValidacionEspecial__c )
                {
                    if( oConf.Api__c == 'BI_COL_Bloqueado__c' )
                    {
                        if( !objMS.BI_COL_Bloqueado__c )
                        {
                            objMS.BI_COL_Bloqueado__c = true;
                            lstModUp.add( objMS );
                        }
                    }

                    if( oConf.Api__c == 'BI_Contacto__c' && objMS.BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__c == null )
                    {
                        mensajeError = ( mensajeError == '' ? oConf.Mensaje_Error__c : mensajeError + ' ' + oConf.Mensaje_Error__c );
                    }
                    system.debug('\n\n .-.-. api '+oConf.Api__c+'\n calif '+objMS.BI_COL_Clasificacion_Servicio__c+'\n cargo '+objMS.BI_COL_Cargo_conexion__c);
                    if( oConf.Api__c == 'BI_COL_Cargo_conexion__c'  &&
                        opCv.contains( objMS.BI_COL_Clasificacion_Servicio__c ) &&
                        objMS.BI_COL_Cargo_conexion__c == 0 )
                    {
                        mensajeError = ( mensajeError == '' ? 
                            objMS.Name + oConf.Mensaje_Error__c : 
                            mensajeError + ' ' + objMS.Name + + oConf.Mensaje_Error__c );
                    }

                    if( oConf.Api__c == 'BI_COL_Autoconsumo__c'  &&
                        oConf.Api2__c == 'BI_COL_Renovable__c'  &&
                        oppNew.BI_COL_Autoconsumo__c &&
                        objMS.BI_COL_Autoconsumo__c &&
                        objMS.BI_COL_Renovable__c )
                    {
                        mensajeError = ( mensajeError == '' ? 
                            objMS.Name + oConf.Mensaje_Error__c : 
                            mensajeError + ' ' + objMS.Name + + oConf.Mensaje_Error__c );
                    }

                    if( oConf.Api__c == 'BI_COL_Autoconsumo__c'  &&
                        oConf.Api2__c == 'BI_COL_Renovable__c'  &&
                        oppNew.BI_COL_Autoconsumo__c &&
                        objMS.BI_COL_Autoconsumo__c &&
                        objMS.BI_COL_Estado_aprobacion_autoconsumo__c != 'Aprobado' )
                    {
                        mensajeError = ( mensajeError == '' ? 
                            objMS.Name + oConf.Mensaje_Error__c : 
                            mensajeError + ' ' + objMS.Name + + oConf.Mensaje_Error__c );
                    }
                }
                System.debug('\n\n mensajeError ' +mensajeError+'\n\n');
            }
            if(mensajeError!='')
            {
                listMensajesError.add( objMS.Name +': '+ mensajeError );    
            }
            
        }

        if( listMensajesError.size() <= 0 && oppNew.StageName.equals( F1_CLOSED_WON ) )
        {
            fnUpdateDS( oppNew, lstModUp );
        }
        return listMensajesError;
    }
    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public static List<String> fnSearchErrorF1( Opportunity oppNew,List<String> listMensajesError )
    {
        System.debug('\n\n Ingresa a fnSearchErrorF1 \n\n');
        //List<String> listMensajesError = new List<String>();

        List<BI_COL_Modificacion_de_Servicio__c> ListMS = fnFindMS( oppNew, '3' );

        for( BI_COL_Modificacion_de_Servicio__c lms : ListMS )
        {
            

        System.debug('\n\n For Ingresa a fnSearchErrorF1 ms.BI_COL_Autoconsumo__c'+lms.BI_COL_Autoconsumo__c+' lms.BI_COL_Renovable__c '+ lms.BI_COL_Renovable__c+' \n\n');
            if( ( lms.BI_COL_Clasificacion_Servicio__c == label.BI_COL_lblALTA_DEMO || 
                  lms.BI_COL_Clasificacion_Servicio__c == label.BI_COL_lblUpgradeDemo ) && 
                  lms.BI_COL_Renovable__c == true)
            {
                listMensajesError.add( lms.Name+': Si la clasificacion del servicio de la MS asociada es ALTA DEMO o UPGRADE DEMO, ' + 
                          ' no debe tener la casilla Renovable seleccionada');
            }
            System.debug('\n\n linea 219 label.BI_COL_lblALTA_DEMO '+label.BI_COL_lblALTA_DEMO+ 'label.BI_COL_lblUpgradeDemo: '+label.BI_COL_lblUpgradeDemo+' lms.BI_COL_Estado_aprobacion__c '+lms.BI_COL_Estado_aprobacion__c+ ' lms.BI_COL_Clasificacion_Servicio__c: '+lms.BI_COL_Clasificacion_Servicio__c+'\n\n');
            if( ( lms.BI_COL_Clasificacion_Servicio__c == label.BI_COL_lblALTA_DEMO || 
                  lms.BI_COL_Clasificacion_Servicio__c == label.BI_COL_lblUpgradeDemo ) && 
                  lms.BI_COL_Estado_aprobacion__c !='Aprobado')
            {
                listMensajesError.add( lms.Name+': Si la clasificacion del servicio de la MS asociada es ALTA DEMO o UPGRADE DEMO, ' + 
                          ' Debe solocitar aprobacion');
            }

            //CFC Validacion Demos
            if( ( lms.BI_COL_Servicio_ocasional__c = false ) &&
                ( lms.BI_COL_Clasificacion_Servicio__c == label.BI_COL_lblDemo)  && 
                ( lms.BI_COL_Estado_aprobacion_autoconsumo__c == label.BI_PendienteAprobacion_Comite || 
                  lms.BI_COL_Estado_aprobacion_autoconsumo__c == label.BI_Rechazado ) )
            {
                listMensajesError.add( lms.Name+': Esta pendiente de solicitar aprobacion o Aprobar');
            }
            if(lms.BI_COL_Autoconsumo__c==true && lms.BI_COL_Renovable__c==true)
            {
                listMensajesError.add( lms.Name+': Tiene el check renovable activo y para autoconsumo no requiere');
            }
        }
        System.debug('\n\n Linea 241 listMensajesError: '+listMensajesError+'\n\n'); 
        return listMensajesError;
    }
    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public static List<String> fnSearchErrorAccountMidas( Opportunity oppNew,List<String> listMensajesError )
    {
        //List<String> listMensajesError = new List<String>();
        //Seleccionamos la cuenta (cliente) de la oportunidad
        //Account account = [ SELECT Id, BI_Estado_de_la_deuda__c FROM Account WHERE Id =: oppNew.AccountId  LIMIT 1 ];
        //Validamos que la cuenta tenga al d?a la cartera
        //if( account.BI_Estado_de_la_deuda__c != null && !account.BI_Estado_de_la_deuda__c.equals('OK') )
        //{
            //listMensajesError.add(Label.BI_COL_Error_Oportunidad_Estado_Aprobacion_Cliente);
        //}
        
        /*List<BI_COL_MIDAS__c> lstMidas=[
            select Id,BI_COL_Ingreso_Conectividad__c, BI_COL_Proyecto__r.Name
            from    BI_COL_MIDAS__c
            where   BI_COL_Proyecto__c =: oppNew.Id
            and     BI_COL_Estado_de_la_aprobacion__c = 'Aprobado'];*/
  
        /*if( lstMidas.size()== 0 && oppNew.BI_COL_Clasificacion_Preventa__c == label.BI_COL_lblProyecto )
        {
            listMensajesError.add( Label.BI_COL_Error_Oportunidad_MIDAS_Aprobado + ' ' );
        }*/
        return listMensajesError;
    }
    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public static List<String> fnSearchErrorOtherStage( Opportunity oppNew )
    {
        List<String> listMensajesError = new List<String>();

        List<BI_COL_Modificacion_de_Servicio__c> lstMS = fnFindMS( oppNew, '1' );
        //Validacion de descuentos
        /*for( BI_COL_Modificacion_de_Servicio__c objMS : lstMS )
        {
            if( objMS.BI_COL_Servicio_sin_cobro__c == null )
                listMensajesError.add( objMS.Name + ': ' + Label.BI_COL_lblErrorMSDescuento ); 
        }*/
        return listMensajesError;
    }
    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public static void fnUpdateDS( Opportunity oppNew, List<BI_COL_Modificacion_de_Servicio__c> lstModUp )
    {
        List<BI_COL_Descripcion_de_servicio__c> descripcionesServicio = fnFindMS( oppNew, '2' );
        //Recorremos todas las DS
        for( BI_COL_Descripcion_de_servicio__c ds : descripcionesServicio )
            ds.BI_COL_Bloqueado__c = true;
        
        oppNew.BI_COL_Bloqueado__c = true;
        
        try
        {
          update descripcionesServicio;
          update lstModUp;
        }
        catch(Exception ex)
        {
          oppNew.addError( ex.getMessage() );
        }
    }
    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public static List<sObject> fnFindMS( Opportunity oppNew, string sOpc )
    {
        System.debug('\n\n ---- Linea 299 sOpc'+sOpc+' --- \n\n oppNew '+oppNew);
        if( sOpc == '1' )
        {
            return [
                SELECT  Id, Name,
                        BI_COL_Tipo_Facturacion__c, 
                        BI_COL_Plazo_Vencimiento_Factura__c, 
                        BI_COL_Cargo_fijo_mes__c,
                        BI_COL_Clasificacion_Servicio__c,
                        BI_COL_Cargo_conexion__c,
                        BI_COL_Fecha_inicio_de_cobro_RFB__c, 
						/*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c, 
                        BI_COL_Medio_Vendido__c,
                        BI_COL_Sucursal_de_Facturacion__r.BI_Contacto__c,
                        BI_COL_Bloqueado__c,
                        BI_COL_Cobro_unico_adicionales__c, 
                        BI_COL_Cobro_mensual_adicionales__c, 
                        BI_COL_Servicio_sin_cobro__c,
                        BI_COL_Autoconsumo__c, 
                        BI_COL_Renovable__c, 
                        BI_COL_Estado_aprobacion_autoconsumo__c,
                        (SELECT Id, Name, BI_COL_Presupuesto_Area_Origen__c FROM Viabilidad_T_cnica__r where BI_COL_Estado__c = 'Entregado')
                FROM    BI_COL_Modificacion_de_Servicio__c
                WHERE   BI_COL_Oportunidad__c <> null
                and     BI_COL_Oportunidad__c =: oppNew.Id 
                AND     BI_COL_Estado__c = 'Pendiente'
                limit 500 ];
        }
        else if( sOpc == '2' )
        {
            return [
                SELECT  Id, BI_COL_Oportunidad__c, 
                        BI_COL_Oportunidad__r.AccountId, 
                        BI_COL_Sede_Origen__c
                FROM    BI_COL_Descripcion_de_servicio__c 
                WHERE   BI_COL_Oportunidad__c =: oppNew.Id 
                and     BI_COL_Bloqueado__c=false 
                limit 500];
        }
        else
        {
            return [
                SELECT  Id, 
                        BI_COL_Oportunidad__c, 
                        BI_COL_Clasificacion_Servicio__c, 
                        BI_COL_Renovable__c,
                        BI_COL_Autoconsumo__c,
                        BI_COL_Estado_aprobacion_autoconsumo__c, Name, BI_COL_Estado_aprobacion__c
                FROM    BI_COL_Modificacion_de_Servicio__c
                WHERE   BI_COL_Oportunidad__c =: oppNew.Id 
                AND     BI_COL_Estado__c='Pendiente'];
        }
            
    }
    //Shipping Opportunity Controller
    @Future(callout=true)
    public static void enviarOptTrs( List<Id> lstIdOpt )
    {
    
        list<Opportunity> lstOportunidades =[
            Select  Id, BI_Numero_id_oportunidad__c, BI_Id_de_la_oportunidad__c,
                    (Select BI_COL_Capex_Total__c, BI_COL_Capex_Ingreso__c, BI_COL_Cobro_Unico__c, BI_COL_OIBDA__c, BI_COL_OPEX__c,
                    BI_COL_Capex_No_Efectivo__c,        
                    BI_COL_Payback__c, BI_COL_PRI__c ,BI_COL_Recurrente__c, BI_COL_VPN__c, BI_COL_VPN_Capex__c, BI_COL_TIR__c,
                    BI_COL_Margen_Directo__c, BI_COL_Markup__c From MIDAS__r where BI_COL_Estado_de_la_aprobacion__c='Aprobado') 
            From    Opportunity 
            where ID In: lstIdOpt ];
     
        List<BI_COL_MIDAS__c> lstMidasTest =[ 
            select  BI_COL_Capex_Total__c,BI_COL_Margen_Directo__c,BI_COL_OIBDA__c,BI_COL_OPEX__c,BI_COL_Capex_No_Efectivo__c,
                    BI_COL_PRI__c, BI_COL_VPN__c,BI_COL_VPN_Capex__c,BI_COL_TIR__c,BI_COL_Capex_Ingreso__c,
                    BI_COL_Cobro_Unico__c,BI_COL_Markup__c,BI_COL_Payback__c,BI_COL_Recurrente__c 
            from    BI_COL_MIDAS__c 
            where   BI_COL_Proyecto__c IN: lstIdOpt];  
                 
        ws_TrsMasivo.datosmidasCodigoOportunidad[] lstDatosOptEnviar = new ws_TrsMasivo.datosmidasCodigoOportunidad[]{};
        
        for( Opportunity opt:lstOportunidades )
        {
            ws_TrsMasivo.datosmidasCodigoOportunidad datosOpt=new ws_TrsMasivo.datosmidasCodigoOportunidad();
            List<BI_COL_MIDAS__c> midas = opt.MIDAS__r;
            if( test.isRunningTest() )
                midas = lstMidasTest;
            System.debug('\n\n midas.size() '+midas.size()+'\n\n');
            if( midas.size() > 0 )
            {
                datosOpt.capex_total = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_Capex_Total__c);
                datosOpt.codigoproyectosalesforce = opt.BI_Numero_id_oportunidad__c;
                datosOpt.ebitda = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_Margen_Directo__c);
                datosOpt.margendirecto = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_Margen_Directo__c);
                datosOpt.oibda = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_OIBDA__c);
                datosOpt.opex = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_OPEX__c);
                datosOpt.preciopromedio = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_Capex_No_Efectivo__c);
                datosOpt.pri = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_PRI__c);
                datosOpt.vpn = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_VPN__c);
                datosOpt.vpncapex = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_VPN_Capex__c);
                datosOpt.tir = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_TIR__c);
                datosOpt.payback = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_Payback__c);
                datosOpt.capex_ingreso = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_Capex_Ingreso__c);
                datosOpt.markup = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_Markup__c);
                datosOpt.cobro_unico = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_Cobro_Unico__c);
                datosOpt.recurrente = BI_COL_Shipping_Opportunity_Trs_ctr.quitaDecimal(midas[0].BI_COL_Recurrente__c);
                lstDatosOptEnviar.add( datosOpt );
            }
        }
        try
        {
            if( lstDatosOptEnviar.size() > 0 )
            {
                ws_TrsMasivo.serviciosSISGOTSOAP sw = new ws_TrsMasivo.serviciosSISGOTSOAP();
                sw.timeout_x = 120000;
                ws_TrsMasivo.midasCodigoOportunidadResponse[] resp=sw.midasCodigoOportunidad(lstDatosOptEnviar);
                creaLogTransaccion(resp[0].mensajeError,lstOportunidades,'Exitoso','Respuesta Servicio Web TRS OPT F1');
                System.debug('\n\n respuesta ws '+resp+'\n\n');
            }
        }
        catch(Exception e)
        {
            creaLogTransaccion(''+e,lstOportunidades,'FALLIDO','Error Servicio Web TRS OPT F1 MIDAS');    
        }
    }
    
    public static String quitaDecimal( Decimal numero )
    {
        String retorno = '';
        retorno = '' + numero;
        if( retorno.contains('.00') )
            retorno = retorno.substring(0, retorno.lastIndexOf('.'));      
        else if( retorno.contains('.') )
            retorno = retorno.replace('.', '');
        return retorno;
    }
    /**** OA: Método de creación de log de transacciones fallidas para el objeto Oportunidad ****/
    public static void creaLogTransaccionError(String error, List<Opportunity> lstIdClientes,String infEnviada)
     {
          List<BI_Log__c> lstLogTran=new List<BI_Log__c>();
          BI_Log__c log=null; 
          system.debug('métod de log de transacciones::Exception::'+error+'\n\n'+lstIdClientes);
        
          for(Opportunity acc:lstIdClientes)
          {
              log=new BI_Log__c(BI_COL_Informacion_recibida__c=error,BI_COL_Oportunidad__c=acc.id,BI_COL_Estado__c='FALLIDO',BI_COL_Interfaz__c='NOTIFICACIONES',BI_COL_Informacion_Enviada__c=infEnviada);
              lstLogTran.add(log);
          }
          
          system.debug('Insertando log de transacciones::::'+lstLogTran);
          insert lstLogTran;
     }
     
    public static void creaLogTransaccion( String error, List<Opportunity> lstIdOpt, String estado, String tipoLog )
    {
        List<BI_Log__c> lstLogTran = new List<BI_Log__c>();
        BI_Log__c log = null;
        
        for(Opportunity suc:lstIdOpt)
        {
            List<BI_COL_MIDAS__c> midas = suc.MIDAS__r;
            log = new BI_Log__c();
            log.BI_COL_Informacion_recibida__c = error;
            log.BI_COL_Oportunidad__c = suc.id;
            log.BI_COL_Informacion_Enviada__c=String.valueOf(midas);
            log.BI_COL_Estado__c = estado;
            log.BI_COL_Tipo_Transaccion__c='ENVIO MIDAS';
            lstLogTran.add( log );
        }
        insert lstLogTran;
        System.debug('\n\n lstLogTran :'+lstLogTran+'\n\n');
    }
    @InvocableMethod
    public static void envioProyectoTRS(List<opportunity> lstOpp)
    {
        List<String> opportunityIds = new List<String>();
        for (Opportunity opt : lstOpp) 
        {
            opportunityIds.add(opt.Id);
        }
        enviarOptTrs(opportunityIds);
    }
    
    
   /***
    * @Author: Alfonso José Álvarez Elbal
    * @Company: Telefónica Global Techonolgy
    * @Description: Demanda 495. Punto 1: Creación Oportunidades /MS .b. Cuando una opty está abierta (<> F1) la duración del a oportunidad al ser modificada debe replicarse a todas las MS asociadas
    * a excepción de las MS que tengan duración 1 mes.
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
 public static void duracionOpty2MS(list<Opportunity> olds, list<Opportunity> news )
 {
   System.debug('BI_COL_Opportunity_ctr.duracionOpty2MS() BEGIN');
   //Mapa para almacenar los ids de las oportunidad y las duraciones de contrato de cada una  
   Map <Id,Decimal> idOptys=new Map<Id,Decimal>();
   // Filtramos por aquellas oportunidades de Colombia y que hayan modificado la fecha de duración del contrato  
   // Almacenamos en idOptys las oportunidades y las duraciones
   for(Integer i=0;i<olds.size();i++){
       Opportunity optyOld=olds.get(i);
       Opportunity optyNew=news.get(i);
       //System.debug('BI_COL_Opportunity_ctr.duracionOpty2MS(): Oportunidad ('+optyNew.Name+') de Colombia con cambio de duración contrato meses: '+optyOld.BI_Duracion_del_contrato_Meses__c+ ' valor nuevo: ' + optyNew.BI_Duracion_del_contrato_Meses__c);
	//Query comentada en el bucle, innecesaria - JMP
       //Opportunity opt=(([Select Id,Name, BI_Duracion_del_contrato_Meses__c from Opportunity where Id =: optyNew.Id]));
       //System.debug('BI_COL_Opportunity_ctr.duracionOpty2MS(): Valor actual de Oportunidad ('+optyNew.Name+') duracion:'+ opt.BI_Duracion_del_contrato_Meses__c);
       if(optyOld.BI_Country__c=='Colombia' && optyOld.BI_Duracion_del_contrato_Meses__c!=optyNew.BI_Duracion_del_contrato_Meses__c){
           idOptys.put(optyNew.Id,optyNew.BI_Duracion_del_contrato_Meses__c);
       }//if(optyOld.BI_Country__c=='Colombia' && optyOld.BI_Duracion_del_contrato_Meses__c!=optyNew.BI_Duracion_del_contrato_Meses__c){
       
   }//for(Integer i=0;i<olds.size();i++){
   //System.debug('BI_COL_Opportunity_ctr.duracionOpty2MS(): idOptys: '+idOptys.keySet());   
   //System.debug('BI_COL_Opportunity_ctr.duracionOpty2MS(): idOptys.Duración: '+idOptys.values());     
   // Obtener las MS de las oportunidades anteriormente filtradas
   //Mapa para almacenar las MS que deben modificarse.   
   Map<Id,BI_COL_Modificacion_de_Servicio__c> mp_ms= new Map<Id,BI_COL_Modificacion_de_Servicio__c>(([Select Id,BI_COL_Oportunidad__c,BI_COL_Duracion_meses__c from BI_COL_Modificacion_de_Servicio__c where BI_COL_Oportunidad__c in :idOptys.keySet()]));
   //System.debug('BI_COL_Opportunity_ctr.duracionOpty2MS(): Cantidad de MS a actualizar: '+ mp_ms.size());
  
   // Modificar las MS con las nuevas duraciones de contrato SOLO si la duracion de la MS es distinto de 1
   for(BI_COL_Modificacion_de_Servicio__c ms : mp_ms.values()){
     String idOpty=ms.BI_COL_Oportunidad__c;
     Decimal durOpty=idOptys.get(idOpty);
     if(ms.BI_COL_Duracion_meses__c!=1){ //Solo
       System.debug('BI_COL_Opportunity_ctr.duracionOpty2MS(): MS['+ms.Id+'] de oportunidad ['+idOpty+'] con valor de duración: '+ms.BI_COL_Duracion_meses__c+' se actualiza a valor:'+durOpty);   
       ms.BI_COL_Duracion_meses__c = durOpty;   
       
     }else{
       System.debug('BI_COL_Opportunity_ctr.duracionOpty2MS(): MS['+ms.Id+'] de oportunidad ['+idOpty+'] con valor de duración: '+ms.BI_COL_Duracion_meses__c+' NO SE ACTUALIZA');   
     }
   }
   //System.debug('BI_COL_Opportunity_ctr.duracionOpty2MS() mp_ms.values:'+mp_ms.values());      
   update(mp_ms.values());  
   System.debug('BI_COL_Opportunity_ctr.duracionOpty2MS() END');      
 }//public static void fnValidarCierreOportunidad(list<Opportunity> lstNew, list<Opportunity> lstOld )
       
}