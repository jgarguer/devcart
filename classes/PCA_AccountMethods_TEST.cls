@isTest

private class PCA_AccountMethods_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_AccountMethods class 
    
    History: 
    <Date> 					<Author> 				<Change Description>
    12/08/2014      		Ana Escrich	    		Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
        
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Ana Escrich
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for PCA_AccountMethods.createChatterGroup. CollaborationGroups are not accesible through
 				   test classes.
		    
 	History: 
 	
 	 <Date> 					<Author> 				<Change Description>
    12/08/2014      Ana Escrich	    		Initial Version
    13/10/2014      Ignacio Llorca      SeeAllData = true added to createChatterGroupTest in order to find the CollaborationGroup needed in the method
 	--------------------------------------------------------------------------------------------------------------------------------------------------------*/		    
    @isTest(SeeAllData=true) 
    static void createChatterGroupTest() {  
		List<CollaborationGroup> cgList = new List<CollaborationGroup>();
		cgList.add(new CollaborationGroup(Name='Comercio', CollaborationType='Public'));
		cgList.add(new CollaborationGroup(Name='Transporte', CollaborationType='Public'));
		cgList.add(new CollaborationGroup(Name='Industria', CollaborationType='Public'));
		insert cgList;
	//List<String> lst_country = BI_DataLoad.loadPaisFromPickList(2);
    /*Set<String>pais=new Set<String>();
    pais.add('México');
    pais.add('Chile');
 	List<String> lst_country = BI_DataLoad.loadPaisFromPickListNotBeing(1,pais);*/
 	
 	List<String> lst_country = new List<String>();
 	lst_country.add('Guatemala');
 	lst_country.add('Guatemala');
    
    system.debug('lst_country: '+lst_country);
    
 	List<Account> lst_acc = BI_DataLoad.loadAccounts(2, lst_country);
 	
 	List<Contact> lst_con = BI_DataLoad.loadContacts(2, lst_acc);	
 	
 	Community comm = [Select Id from Community where Name= :LABEL.PCA_Nombre_comunidad_ideas];
    Network netw = [Select Id from Network where Name= 'Empresas Platino'];
    Profile prof1 = [Select Id from Profile where Name='BI_Customer Communities'];
	
	system.debug('lst_acc1: '+lst_acc);
	//Crear nuevo "Valor de selección" 
    
    lst_acc[0].BI_Sector__c= 'Marketing';
    lst_acc[1].BI_Sector__c= 'Sector';
  	system.debug('lst_acc2: '+lst_acc);
  	
  	update lst_acc;
	  
	}

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
  Author:        Ignacio Llorca
  Company:       Salesforce.com
  Description:   Test method to manage the code coverage for PCA_AccountMethods.createChatterGroup. 
        
  History: 
  
   <Date>           <Author>        <Change Description>
    12/08/2014      Ignacio Llorca        Initial Version
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/     
  @isTest(SeeAllData=false) 
  static void createChatterGroupTest2() {  
      
  List<String> lst_country = BI_DataLoad.loadPaisFromPickList(2);
    
    system.debug('lst_country: '+lst_country);
    
  List<Account> lst_acc = BI_DataLoad.loadAccounts(2, lst_country);
     
  List<Contact> lst_con = BI_DataLoad.loadContacts(2, lst_acc); 
  
  Community comm = [Select Id from Community where Name= :LABEL.PCA_Nombre_comunidad_ideas];
    Network netw = [Select Id from Network where Name= 'Empresas Platino'];
    Profile prof1 = [Select Id from Profile where Name='BI_Customer Communities'];
  
  system.debug('lst_acc1: '+lst_acc);
  //Crear nuevo "Valor de selecci�n" 
 
    lst_acc[0].BI_Sector__c= 'Marketing';
    lst_acc[1].BI_Sector__c= 'Sector';
    system.debug('lst_acc2: '+lst_acc);
    
      
    update lst_acc;
    
  }
    
    
}