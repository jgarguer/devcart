public without sharing class CWP_PortalesCtrl extends PCA_HomeController{
    public List<BI_Portal_local__c> GetPortals {get;set;}
    public List<String> ImageToshow {get;set;}
    public Map<BI_Portal_local__c,String> mapPortalsURL {get;set;}
    public String currentUserCountry {get;set;}
    public Id currentAcc {get;set;}
    public string accSeg {get;set;}
    public BI_Portal_local__c PortLoc {get;set;}
    // INI - Everis - Evo CWP - Portales - 13/03/2017
    public Profile p{get;set;}
    // FIN - Everis - Evo CWP - Portales - 13/03/2017
    // INI - Everis - Evo CWP - Portales - 13/03/2017 - Perfil añadido al constructor
    public CWP_PortalesCtrl() {p=[SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];}
    // FIN - Everis - Evo CWP - Portales - 13/03/2017
    // INI - Everis - Evo lightning CWP - Portales - 14/03/2017
    public static List<BI_Portal_local__c> portals {get;set;}
    public static String userCountry {get;set;}
    public static string accountSeg {get;set;}
    // FIN - Everis - Evo lightning CWP - Portales - 14/03/2017
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Moruno
Company:       Aborda
Description:   check permissions of current user

History:

<Date>            <Author>              <Description>
22/04/2015        Antonio Moruno       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_PortalesCtrl.checkPermissions', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Moruno
Company:       Aborda
Description:   Constructor custom for Portales

History:

<Date>            <Author>              <Description>
22/04/2015        Antonio Moruno       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfo(){      
        try{
            
            currentAcc = BI_AccountHelper.getCurrentAccountId();
            PortLoc = new BI_Portal_local__c();
            if(currentAcc != null){
                Account AccHome = [SELECT Id,BI_Imagen_Inicio_PP__c,BI_Country__c,
                                   BI_Segment__c FROM Account WHERE Id = :currentAcc LIMIT 1];
                
                if(AccHome.BI_Country__c != null){
                    currentUserCountry = AccHome.BI_Country__c;
                }
                accSeg = AccHome.BI_Segment__c;
                if(accSeg == null){
                    accSeg = 'Empresas';
                }
                
                // INI - Everis - Evo CWP - Portales - 13/03/2017 - Añadida diferenciación por perfil - La query original está comentada /**/
                
                //p = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
                if(p.Name.contains('TGS')){
                    GetPortals = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
                                  WHERE BI_Activo__c=TRUE AND CWP_TGS_Users__c = true ORDER BY BI_Posicion__c asc];
                }else{
                    GetPortals = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
                                  WHERE (BI_Country__c =: currentUserCountry OR BI_Country__c = null) AND (BI_Segment__c =: accSeg OR BI_Segment__c= null) AND BI_Activo__c=TRUE 
                                  ORDER BY BI_Posicion__c asc];
                }
                /*GetPortals = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
WHERE (BI_Country__c =: currentUserCountry OR BI_Country__c = null) AND (BI_Segment__c =: accSeg OR BI_Segment__c= null) AND BI_Activo__c=TRUE ORDER BY BI_Posicion__c asc];*/
                
                // FIN - Everis - Evo CWP - Portales - 13/03/2017 - Añadida diferenciación por perfil
                
                List<String> stringsSRC = new List<String>();
                mapPortalsURL = new Map<BI_Portal_local__c,String>();
                for(BI_Portal_local__c portalsImg :GetPortals){
                    stringsSRC = portalsImg.BI_Imagen_del_portal__c.split('"');
                    //ImageToShow.add(stringsSRC[3]);
                    String SRCFinal = stringsSRC[3].replace('amp;','');
                    
                    mapPortalsURL.put(portalsImg,SRCFinal);
                    
                }
            }
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_PortalesCtrl.loadInfo', 'Portal Platino', Exc, 'Class');
        }
        
    } 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Antonio Moruno
Company:       Aborda
Description:   Constructor custom for Portales

History:

<Date>            <Author>              <Description>
23/04/2015        Antonio Moruno       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void getObj(){
        try{
            String PortalHover = ApexPages.currentPage().getParameters().get('myParam');
            system.debug('PortalHover**: '+PortalHover);
            // INI - Everis - Evo CWP - Portales - 13/03/2017
            if(p.Name.contains('TGS')){
                PortLoc = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
                           WHERE BI_Activo__c=TRUE AND Id = :PortalHover AND CWP_TGS_Users__c = true ORDER BY BI_Posicion__c asc];
            }else{
                PortLoc = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
                           WHERE (BI_Country__c =: currentUserCountry OR BI_Country__c = null) AND (BI_Segment__c =: accSeg OR BI_Segment__c= null) AND Id =:PortalHover AND BI_Activo__c=TRUE   ORDER BY BI_Posicion__c DESC];
            }
            /*PortLoc = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
WHERE (BI_Country__c =: currentUserCountry OR BI_Country__c = null) AND (BI_Segment__c =: accSeg OR BI_Segment__c= null) AND Id =:PortalHover AND BI_Activo__c=TRUE   ORDER BY BI_Posicion__c DESC];*/
            // FIN - Everis - Evo CWP - Portales - 13/03/2017
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_PortalesCtrl.getObj', 'Portal Platino', Exc, 'Class');
        }
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Alberto Pina
Company:       Everis
Description:   Method to load Portal configuration. Lightning.

History: 

<Date>                          <Author>                    <Change Description>
14/03/2017                      Alberto Pina                Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    
    // INI - Everis - Evo lightning CWP - Portales - 14/03/2017
    @auraEnabled
    public static  List<BI_Portal_local__c> getPortals(){
        /*Calculamos si es TGS */
        Boolean esTGS=false; 
        Id pId = UserInfo.getProfileId();
        //Id pId = '00ew00000012p8HAAQ';
        Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
        if (p.size()>0){
            String pName = p[0].Name;
            esTGS= pName.startsWith('TGS');
        }else 
            esTGS= false;
        
        String idUser=UserInfo.getUserId();                
        //String idUser='00525000001fgBrAAI';
        
        /*Calculamos El usuario que está ejecutando */
        User usuarioPortal = [Select id, SmallPhotoUrl, FullPhotoUrl, name, Usuario_SolarWinds__c, token_SolarWinds__c, CompanyName, Sector__c,  MobilePhone, Usuario_BO__c, pass_BO__c,ContactId,Contact.BI_Cuenta_activa_en_portal__c, AccountId  from User where id=:idUser];
        String ActAccs = usuarioPortal.AccountId;
        
        if(ActAccs != null){
            
            /*Obtenemos los portales de un usuario*/
            if(esTGS){
                system.debug('perfil tgs');
                portals = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
                           WHERE BI_Activo__c=TRUE AND CWP_TGS_Users__c = true ORDER BY BI_Posicion__c asc];
            }else{
                system.debug('perfil bien');
                Account AccHome = [SELECT Id,BI_Country__c,BI_Segment__c FROM Account WHERE Id = :ActAccs LIMIT 1];
                
                if(AccHome.BI_Country__c != null){
                    userCountry = AccHome.BI_Country__c;
                }
                accountSeg = AccHome.BI_Segment__c;
                if(accountSeg == null){
                    accountSeg = 'Empresas';
                }
                portals = [SELECT BI_Descripcion_del_portal__c,BI_Imagen_del_portal__c,BI_Link_del_portal__c,BI_Country__c,BI_Posicion__c,BI_Segment__c,Id,Name FROM BI_Portal_local__c
                           WHERE (BI_Country__c =: userCountry OR BI_Country__c = null) AND (BI_Segment__c =: accountSeg OR BI_Segment__c= null) AND BI_Activo__c=TRUE AND CWP_TGS_Users__c = false
                           ORDER BY BI_Posicion__c asc];
            }
            
        }        
        List<String> stringsSRC = new List<String>();
        system.debug(portals);
        for(BI_Portal_local__c portalsImg :portals){
            stringsSRC = portalsImg.BI_Imagen_del_portal__c.split('src="');
            String SRCInter = stringsSRC[1].replace('amp;','');
            stringsSRC = SRCInter.split('"');
            String SRCFinal2 = stringsSRC[0].replace('/CWP/','/');
            SRCFinal2 = SRCFinal2.replace('/empresasplatino/','/');
            String SRCFinal = SRCFinal2;
            portalsImg.BI_Imagen_del_portal__c=SRCFinal;
        }
        system.debug(portals);
        return portals;
    }
    // FIN - Everis - Evo lightning CWP - Portales - 13/03/2017
    
}