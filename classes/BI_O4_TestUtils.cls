public class BI_O4_TestUtils {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Wrapper class used to determine if it is necessary to throw an exception during the test execution
    
    History:
    
    <Date>            <Author>          <Description>
    26/08/2016        Miguel Cabrera    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

  	public static Boolean throw_exception = false;

	public static Boolean isRunningTest() {
	    return Test.isRunningTest() && throw_exception;
	}
}