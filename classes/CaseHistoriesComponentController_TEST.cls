@isTest(seeAllData = false)
public class CaseHistoriesComponentController_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José María Martín
    Company:       Deloitte
    Description:   Test method to manage the code coverage for  CaseHistoriesComponentController_TEST
    
    <Date>                 <Author>               <Change Description>
    11/2015              José María Martín           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void test_CaseHistoriesComponentController_TEST() {
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        
        System.runAs(usr)
        {      
            List<Contact> CaseContacts=new List<Contact>();
            CaseContacts.add(new Contact(FirstName='Ramon',LastName='Superman'));
            CaseContacts.add(new Contact(FirstName='Pepe',LastName='Perez'));
            CaseContacts.add(new Contact(FirstName='Peter',LastName='Parker'));
            insert CaseContacts;
            
            Case caso = new Case(ContactId=CaseContacts[0].ID,Description='Description');
            insert caso;
            caso.ContactId=CaseContacts[1].ID;
            caso.Description='Description2';
            update caso;
            caso.ContactId=CaseContacts[2].ID;
            caso.Description='Description3';
            update caso;
            //caso.ContactId
            //Select CreatedDate, CreatedBy.Name, CreatedBy.Id, Field, NewValue, OldValue from CaseHistory where CaseId = :caseId order by CreatedDate desc limit :QueryLimit offset :OffsetSize
            Test.startTest(); 
             
            CaseHistoriesComponentController controller = new CaseHistoriesComponentController();
            controller.getHistories();
            CaseHistoriesComponentController.returnFieldLabel('NombreCampo');
            controller.Next();
            controller.Previous();
            
            Test.stopTest();
        }
    }
}