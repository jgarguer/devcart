@IsTest
public without sharing class TGS_BusinessHoursUpdate_Test {

	static testMethod void insertCase() {
		
		
		
		/*BusinessHours busih=new BusinessHours();
		busih.Name = 'horas';
		Time myTime = Time.newInstance(17, 30, 0, 0);
		
		busih.SundayStartTime = myTime;
		busih.SundayEndTime = myTime;
		busih.MondayStartTime = myTime;
		busih.MondayEndTime = myTime;
		busih.TuesdayStartTime = myTime;
		busih.TuesdayEndTime = myTime;
		busih.WednesdayStartTime = myTime;
		busih.WednesdayEndTime = myTime;
		busih.ThursdayStartTime = myTime;
		busih.ThursdayEndTime = myTime;
		busih.FridayStartTime = myTime;
		busih.FridayEndTime = myTime;
		busih.SaturdayStartTime = myTime;
		busih.SaturdayEndTime = myTime;
		
		insert busih;*/
		
		BusinessHours [] busih = [Select Id
		From BusinessHours
		Where Name = 'Horario oficina ARG'];
        
        String profile = 'TGS System Administrator';
        
       Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
                TGS_User_Org__c uO = new TGS_User_Org__c();
                uO.TGS_Is_TGS__c = True;
                uO.SetupOwnerId = miProfile.Id;
                insert uO;
                
        }

		Test.startTest();
            User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');		
            u.BI_Permisos__c = 'TGS';
            insert u;
        Test.stopTest();
        
		TGS_User_Org__c.getInstance().TGS_Is_TGS__c = true;
		TGS_Business_Hours__c bH = new TGS_Business_Hours__c();
		
		bH.Name = 'Business Hours Test';
		bH.lookup_Business_Hours__c = busih[0].Id;
		System.runAs(u) {
			insert bH;
		}
	}
}