/**
* Avanxo Colombia
* @author           Manuel Medina href=<mmedina@avanxo.com>
* Project:          Telefonica
* Description:      Clase tipo WebServiceMock del servicio NotificacionesSalesForceWSDL.
*
* Changes (Version)
* -------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    ---------------
* @version  1.0     2015-07-30      Manuel Medina (MM)      Definicion de la clase implments WebServiceMock
*************************************************************************************************************/
@isTest
global class BI_COL_CreateContactWS_mws implements WebServiceMock {

    global Map<String, Object> mapWSResponseBySOAPAction                                    = new Map<String, Object>();

    global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){
        
        System.debug(' soapAction ===============> '+soapAction);
        System.debug(' requestName ==============> '+requestName);
        ws_wwwTelefonicaComNotificacionessalesfo.arrayResponseClientes restNew = new ws_wwwTelefonicaComNotificacionessalesfo.arrayResponseClientes();
        ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] lstWSRESRegistrarClientes      = new List<ws_wwwTelefonicaComNotificacionessalesfo.respuesta>();
        ws_wwwTelefonicaComNotificacionessalesfo.respuesta wsRESRegistrarClientes           = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta();
        wsRESRegistrarClientes.descripcionError                                             = 'RegistrarClientesResponse';
        wsRESRegistrarClientes.idError                                                      = '0';
        lstWSRESRegistrarClientes.add( wsRESRegistrarClientes );
        
        restNew.response = lstWSRESRegistrarClientes;
        
        ws_wwwTelefonicaComNotificacionessalesfo.respuesta wsCambiarSucursal                = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta();
        wsCambiarSucursal.descripcionError                                                  = 'CambiarSucursalResponse';
        wsCambiarSucursal.idError                                                           = '0';
        
        ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] lstWSRESRegistrarContactos     = new List<ws_wwwTelefonicaComNotificacionessalesfo.respuesta>();
        ws_wwwTelefonicaComNotificacionessalesfo.respuesta wsRESRegistrarContactos          = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta();
        wsRESRegistrarContactos.descripcionError                                            = 'RegistrarContactosResponse';
        wsRESRegistrarContactos.idError                                                     = '0';
        lstWSRESRegistrarContactos.add( wsRESRegistrarContactos );
        
        ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] lstWSRESRegistrarSucursales    = new List<ws_wwwTelefonicaComNotificacionessalesfo.respuesta>();
        ws_wwwTelefonicaComNotificacionessalesfo.respuesta wsRESRegistrarSucursales         = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta();
        wsRESRegistrarSucursales.descripcionError                                           = 'RegistrarSucursalesResponse';
        wsRESRegistrarSucursales.idError                                                    = '0';
        lstWSRESRegistrarSucursales.add( wsRESRegistrarSucursales );
        
        ws_wwwTelefonicaComNotificacionessalesfo.respuesta[] lstWSRESRegistrarUsuarios      = new List<ws_wwwTelefonicaComNotificacionessalesfo.respuesta>();
        ws_wwwTelefonicaComNotificacionessalesfo.respuesta wsRESRegistrarUsuarios           = new ws_wwwTelefonicaComNotificacionessalesfo.respuesta();
        wsRESRegistrarUsuarios.descripcionError                                             = 'RegistrarUsuariosResponse';
        wsRESRegistrarUsuarios.idError                                                      = '0';
        lstWSRESRegistrarUsuarios.add( wsRESRegistrarUsuarios );

        ws_wwwTelefonicaComNotificacionessalesfo.arrayResponseContactos objArrayResponseContactos = new ws_wwwTelefonicaComNotificacionessalesfo.arrayResponseContactos();
        objArrayResponseContactos.response = new List <ws_wwwTelefonicaComNotificacionessalesfo.respuesta>(); 
        objArrayResponseContactos.response = lstWSRESRegistrarContactos;
        System.debug('Datos objArrayResponseContactos ================> '+objArrayResponseContactos);

        ws_wwwTelefonicaComNotificacionessalesfo.arrayResponseSucursal objarrayResponseSucursal = new ws_wwwTelefonicaComNotificacionessalesfo.arrayResponseSucursal();
        objarrayResponseSucursal.response = lstWSRESRegistrarSucursales;

        ws_wwwTelefonicaComNotificacionessalesfo.arrayResponseUsuarios objarrayResponseUsuarios = new ws_wwwTelefonicaComNotificacionessalesfo.arrayResponseUsuarios ();
        objarrayResponseUsuarios.response = lstWSRESRegistrarUsuarios;

        mapWSResponseBySOAPAction.put( 'RegistrarClientes', restNew );
        mapWSResponseBySOAPAction.put( 'CambiarSucursal', wsCambiarSucursal );
        mapWSResponseBySOAPAction.put( 'http://www.telefonica.com/NotificacionesSalesForce/RegistrarContactos', objArrayResponseContactos );
        mapWSResponseBySOAPAction.put( 'http://www.telefonica.com/NotificacionesSalesForce/RegistrarContactos', new ws_wwwTelefonicaComNotificacionessalesfo.arrayResponseContactos() );
        mapWSResponseBySOAPAction.put( 'RegistrarContactos', objArrayResponseContactos );
        mapWSResponseBySOAPAction.put( 'RegistrarSucursales', objarrayResponseSucursal );
        mapWSResponseBySOAPAction.put( 'RegistrarUsuarios', objarrayResponseUsuarios );
    
        response.put( 'response_x', mapWSResponseBySOAPAction.get( requestName ) );
    }
}