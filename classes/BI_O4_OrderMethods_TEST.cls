@isTest
private class BI_O4_OrderMethods_TEST {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ricardo Pereira
	    Company:       New Energy Aborda
	    Description:   Method that manage the code coverage of BI_O4_OrderMethods
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    26/09/2016                      Ricardo Pereira             Initial version
	    11/11/2016						Alvaro Garc�a	
	    02/02/2017						Pedro Párraga				Increase coverage
	    24/02/2017						Gawron, Julián				deleting setOrderTdCP_TEST() 		
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	private static Map<String, RecordType> mapRT;

	static{
		BI_TestUtils.throw_exception = false;

		mapRT = new Map<String, RecordType>();
		List<RecordType> lstRT = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Quote' OR DeveloperName = 'Opty'];
		for(RecordType rt: lstRT){
			mapRT.put(rt.DeveloperName, rt);
		}

	}

	@isTest static void setOpptyFieldsOnQuoteDelivery() {
		BI_TestUtils.throw_exception = false;
		//User BI
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
		//Create Account
		List<Account> lst_acc = BI_O4_DisconnectionMethods_TEST.loadAccounts(1, new List<String>{'Argentina'});

		//Create Opportunity
		Opportunity opp = new Opportunity(
			Name = 'Test',
            CloseDate = Date.today(),
            StageName = Label.BI_F5DefSolucion,
            AccountId = lst_acc[0].Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Opportunity_Type__c = 'Alta',
            BI_Country__c = 'Argentina'
            );
		Insert opp;

		

														///////INSERT ORDERS/////////
		////////////////////////////////////////////////////	First Insert 	/////////////////////////////////////////////////////////
		NE__Order__c order1 = new NE__Order__c(
			NE__OptyId__c = opp.Id,
			RecordTypeId = mapRT.get('Quote').Id,
			NE__OrderStatus__c = 'In progress'		
		);
		Insert order1;

		
		order1.NE__OrderStatus__c = 'Delivered';
		update order1;
		
		
		///////////////////////////////////////////////// 	Second Insert 	////////////////////////////////////////////////////////
		NE__Order__c order2 = new NE__Order__c(
			NE__OptyId__c = opp.Id
		);
		Insert order2;

		order2.BI_O4_Flag_Activation__c = true;
		update order2;

		///////////////////////////////////////////////// 	Third Insert 	////////////////////////////////////////////////////////
		NE__Order__c order3 = new NE__Order__c(
			NE__OptyId__c = opp.Id,
			NE__OrderStatus__c = 'Revised',
			RecordTypeId = mapRT.get('Quote').Id
		);
		Insert order3;

		order3.NE__OrderStatus__c = Label.BI_Active;
		update order3;

	}

	@isTest static void setStatus() {
		BI_TestUtils.throw_exception = false;
		//User BI
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
		//Create Account
		List<Account> lst_acc = BI_O4_DisconnectionMethods_TEST.loadAccounts(1, new List<String>{'Argentina'});

		//Create Opportunity
		Opportunity opp = new Opportunity(
			Name = 'Test',
            CloseDate = Date.today(),
            StageName = Label.BI_F5DefSolucion,
            AccountId = lst_acc[0].Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Opportunity_Type__c = 'Alta',
            BI_Country__c = 'Argentina'
            );
		Insert opp;


														///////INSERT ORDERS/////////
		////////////////////////////////////////////////////	First Insert 	/////////////////////////////////////////////////////////
		NE__Order__c order1 = new NE__Order__c(
			NE__OptyId__c = opp.Id,
			RecordTypeId = mapRT.get('Opty').Id,
			NE__OrderStatus__c = 'In progress'		
		);
		Insert order1;
		
		//order1.NE__OrderStatus__c = 'Delivered';
		//update order1;		
		
		///////////////////////////////////////////////// 	Second Insert 	////////////////////////////////////////////////////////
		NE__Order__c order2 = new NE__Order__c(
			NE__OptyId__c = opp.Id,
			RecordTypeId = mapRT.get('Quote').Id,
			NE__OrderStatus__c = 'Draft'
		);
		Insert order2;

		order2.RecordTypeId = mapRT.get('Opty').Id;
		update order2;

		/////////////////////////////////////////////////// 	Third Insert 	////////////////////////////////////////////////////////
		//NE__Order__c order3 = new NE__Order__c(
		//	NE__OptyId__c = opp.Id,
		//	NE__OrderStatus__c = 'Revised',
		//	RecordTypeId = mapRT.get('Quote').Id
		//);
		//Insert order3;

		//order3.NE__OrderStatus__c = Label.BI_Active;
		//update order3;

	}

	@isTest static void desactivateQuote() {
		BI_TestUtils.throw_exception = false;
		//User BI
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
		//Create Account
		List<Account> lst_acc = BI_O4_DisconnectionMethods_TEST.loadAccounts(1, new List<String>{'Argentina'});

		//Create Opportunity
		Opportunity opp = new Opportunity(
			Name = 'Test',
            CloseDate = Date.today(),
            StageName = Label.BI_F5DefSolucion,
            AccountId = lst_acc[0].Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Opportunity_Type__c = 'Alta',
            BI_Country__c = 'Argentina'
            );
		Insert opp;

		NE__Order__c quote = new NE__Order__c(
			NE__OptyId__c = opp.Id,
			RecordTypeId = mapRT.get('Quote').Id,
			NE__OrderStatus__c = 'Active'
		);
		Insert quote;

		Test.startTest();

		NE__Order__c opty = new NE__Order__c(
			NE__OptyId__c = opp.Id,
			RecordTypeId = mapRT.get('Opty').Id,
			NE__OrderStatus__c = 'Active'
		);
		Insert opty;

		System.assertEquals('Delivered', [SELECT Id, NE__OrderStatus__c FROM NE__Order__c WHERE Id = :quote.Id LIMIT 1].NE__OrderStatus__c);

		Test.stopTest();

	}

	@isTest static void setQuoteVersion() {
		BI_TestUtils.throw_exception = false;
		//User BI
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
		//Create Account
		List<Account> lst_acc = BI_O4_DisconnectionMethods_TEST.loadAccounts(1, new List<String>{'Argentina'});

		//Create Opportunity
		Opportunity opp = new Opportunity(
			Name = 'Test',
            CloseDate = Date.today(),
            StageName = Label.BI_F5DefSolucion,
            AccountId = lst_acc[0].Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Opportunity_Type__c = 'Alta',
            BI_Country__c = 'Argentina'
            );
		Insert opp;

		Test.startTest();

		NE__Order__c order = new NE__Order__c(
			NE__OptyId__c = opp.Id,
			RecordTypeId = mapRT.get('Quote').Id,
			NE__OrderStatus__c = 'Draft'
		);
		Insert order;

		System.assertEquals(1,[SELECT Id, NE__Version__c FROM NE__Order__c WHERE Id = :order.Id LIMIT 1].NE__Version__c);

		Test.stopTest();

	}

		@isTest static void setNumberProduct() {
		BI_TestUtils.throw_exception = false;
		//User BI
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
		//Create Account
		List<Account> lst_acc = BI_O4_DisconnectionMethods_TEST.loadAccounts(1, new List<String>{'Argentina'});

		//Create Opportunity
		Opportunity opp = new Opportunity(
			Name = 'Test',
            CloseDate = Date.today(),
            StageName = Label.BI_F5DefSolucion,
            AccountId = lst_acc[0].Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Opportunity_Type__c = 'Alta',
            BI_Country__c = 'Argentina'
            );
		Insert opp;

		Test.startTest();

		NE__Order__c order = new NE__Order__c(
			NE__OptyId__c = opp.Id,
			RecordTypeId = mapRT.get('Quote').Id,
			NE__OrderStatus__c = 'Draft'
		);
		Insert order;
		order.RecordTypeId = mapRT.get('Opty').Id;
		order.NE__OrderStatus__c = 'Active';
		update order;

		System.assertEquals(1,[SELECT Id, NE__Version__c FROM NE__Order__c WHERE Id = :order.Id LIMIT 1].NE__Version__c);

		Test.stopTest();

	}

		@isTest static void exceptions() {
		BI_TestUtils.throw_exception = true;

		Test.startTest();
		BI_O4_OrderMethods.setOpptyFieldsOnQuoteDelivery(null, null);
		BI_O4_OrderMethods.setStatus(null, null);
		BI_O4_OrderMethods.desactiveQuote(null);
		BI_O4_OrderMethods.setQuoteVersion(null);
		BI_O4_OrderMethods.setNumberProduct(null, null);

		Test.stopTest();
		
	}
	
}