global class BI_CheckNews_SLAProcess_JOB implements Schedulable {

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Scheduled class that updates Account fields.
    Test Class:    BI_Account_JOB_TEST

    History: 
    
     <Date>                     <Author>                <Change Description>
    24/02/2015                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    global Map<Id,SlaProcess> map_OLD_SLAProcess;
    global Map<Id,SlaProcess> map_NEW_SLAProcess;
    global List<SlaProcess> lst_SLAProcess_toAdd;

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Method that executes the job, and reassing Entitlements

    History: 
    
     <Date>                     <Author>                <Change Description>
    24/02/2015                  Micah Burgos             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	global void execute(SchedulableContext sch) {
        try{
        	lst_SLAProcess_toAdd = new List<SlaProcess>();
     	
          	system.abortJob(sch.getTriggerId());
           
            map_NEW_SLAProcess = new Map<Id, SlaProcess>([SELECT CreatedById,CreatedDate,Description,Id,IsActive,IsDeleted,IsVersionDefault,LastModifiedById,LastModifiedDate,Name,NameNorm,StartDateField,
                                                    SystemModstamp,VersionMaster,VersionNotes,VersionNumber FROM SlaProcess WHERE Name Like '%Chile%' AND IsVersionDefault = true AND IsActive = true ]);

            System.debug('[map_OLD_SLAProcess]: '+ map_OLD_SLAProcess);
            System.debug('[map_NEW_SLAProcess]: '+ map_NEW_SLAProcess);

            if(!map_NEW_SLAProcess.isEmpty()){
            	if (map_OLD_SLAProcess!=null && !map_OLD_SLAProcess.isEmpty()){
            		
            		for(SLAProcess proc :map_NEW_SLAProcess.values()){
            			if(!map_OLD_SLAProcess.containsKey(proc.Id)){
            				lst_SLAProcess_toAdd.add(proc);
            			}
            		}
            	}else{
            		lst_SLAProcess_toAdd.addAll(map_NEW_SLAProcess.values());
            	}

            	if(!lst_SLAProcess_toAdd.isEmpty()){
            		BI_Jobs.updateEntitlementsAcc(null,lst_SLAProcess_toAdd,false);
            	}

            }else{
                system.debug('NOT HAVE SlaProcess WHERE Name Like %Chile%');
            }

            BI_Jobs.start_checkNews_SLAProcess_JOB(map_NEW_SLAProcess);
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('BI_CheckNews_SLAProcess_JOB', 'BI_EN', Exc, 'Schedulable Job');
        }
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Salesforce.com
    Description:   Constructor method
    
    IN:            Account Id
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    24/02/2015        Micah Burgos       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI_CheckNews_SLAProcess_JOB(Map<Id,SlaProcess> map_old_SLAProceso){
        map_OLD_SLAProcess = map_old_SLAProceso;
    }
}