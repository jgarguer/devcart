public class BI_LoadAllKindQueable implements Queueable {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:        Guillermo Muñoz
        Company:       Accenture
        Description:   Class that insert, update, delete or undelete a collection of sObject using batch size
        
        History: 
        
        <Date>                          <Author>                    <Change Description>
        26/04/2017                      Guillermo Muñoz             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	final String AVALIABLE_TRANSACTIONS = 'INSERT,UPDATE,DELETE,UNDELETE';

	List <SObject> lst_data;
	Integer batch_size;
	String dml_type;
	String transaction_name;
	Boolean finish = false;
	List <SObject> lst_current_dml = new List <SObject>();
	List <sObject> lst_next_iteration = new List <sObject>();

	public BI_LoadAllKindQueable(List <SObject> lst_data, Integer batch_size, String dml_type, String transaction_name){

		this.lst_data = lst_data;
		this.batch_size = batch_size;
		this.dml_type = dml_type;
		this.transaction_name = transaction_name;

		System.debug('!!@@--->' + lst_data.size());

		if(lst_data.size() <= batch_size){

	    	lst_current_dml = lst_data;
	    	finish = true;
	    }
	    else{

	        for(Integer i = 0; i<batch_size; i++){

	        	lst_current_dml.add(lst_data[0]);
	        	lst_data.remove(0);
	        }
	        for(sObject sObj : lst_data){

	        	lst_next_iteration.add(sObj);
	        }
	    }

		if(lst_data == null || batch_size == null || dml_type == null || transaction_name == null){

			throw new BI_Exception('The arguments cannot be null');
		}

		if(!AVALIABLE_TRANSACTIONS.contains(dml_type.toUpperCase())){

			throw new BI_Exception('Invalid DML type');
		}

		if(batch_size <= 0){

			throw new BI_Exception('The batch size cannot be 0 or less than 0');
		}
	}

	public void execute(QueueableContext context) {

		try{	        
	        
	        try{

	        	if(BI_TestUtils.isRunningTest()){

	        		throw new BI_Exception('Test');
	        	}

		        if(dml_type.toUpperCase() == 'INSERT'){

		        	insert lst_current_dml;
		        }
		        else if(dml_type.toUpperCase() == 'UPDATE'){

		        	update lst_current_dml;
		        }
		        else if(dml_type.toUpperCase() == 'DELETE'){

		        	delete lst_current_dml;
		        }
		        if(dml_type.toUpperCase() == 'UNDELETE'){

		        	undelete lst_current_dml;
		        }
	    	}
	    	catch(Exception exc){

	    		BI_LogHelper.generate_BILog('BI_LoadAllKindQueable DML error in ' + transaction_name, 'BI_EN', Exc, 'Batch');
	    	}

	    	if(BI_TestUtils.isRunningTest()){
	        		
	        	throw new BI_Exception('Test');
	        }

	        if(!finish){
	        	
	        	BI_LoadAllKindQueable obj = new BI_LoadAllKindQueable(lst_next_iteration, batch_size,dml_type, transaction_name);
	        	if(!Test.isRunningTest()){
	        		System.enqueueJob(obj);
	        	}
	        }

        }
    	catch(Exception exc){

    		BI_LogHelper.generate_BILog('BI_LoadAllKindQueable internal error in ' + transaction_name, 'BI_EN', Exc, 'Trigger');
    	}
	}
}