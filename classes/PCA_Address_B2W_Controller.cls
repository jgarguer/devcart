public class PCA_Address_B2W_Controller {

    public Case myCase {get; set;}
    public String userId {get; set;}
    public NE__OrderItem__c theOrderItem {get; set; }
      
    // ATTACHMENT
    public String productName {get; set;}
    public boolean mandatoryAttachment {get; set;}
    public boolean isRegistration {get; set;}
    public String mySite {get; set;}
    
    //Variable global control trigger BI_Case metodo TGS_CaseMethods.caseEmailSend()
    public static boolean fireSendEmail = false;

    public Attachment myAttachment {
      get {
          if (myAttachment == null)
            myAttachment = new Attachment();
          return myAttachment ;
        }
      set;
      }
    
    public PCA_Address_B2W_Controller() {
        //mySite= '';
        myAttachment = new Attachment();
        String cId = ApexPages.currentPage().getParameters().get('cId');
        String orderId = ApexPages.currentPage().getParameters().get('orderId');
        userId = UserInfo.getUserId();
        
        System.debug('[PCA_Address_B2W_Controller Constructor] cId: ' + cId);
        System.debug('[PCA_Address_B2W_Controller Constructor] orderId: ' + orderId);
        
        theOrderItem = [SELECT Id, NE__OrderId__c, NE__OrderId__r.case__r.caseNumber,                       
                        NE__CatalogItem__c, NE__CatalogItem__r.NE__ProductId__c, NE__CatalogItem__r.NE__ProductId__r.Name, NE__CatalogItem__r.NE__Technical_Behaviour__c
                        FROM NE__OrderItem__c WHERE NE__OrderId__c = :orderId LIMIT 1];
        
        System.debug('[PCA_Address_B2W_Controller Constructor] theOrderItem: ' + theOrderItem);
        
        productName = theOrderItem.NE__CatalogItem__r.NE__ProductId__r.Name;
        mandatoryAttachment = theOrderItem.NE__CatalogItem__r.NE__Technical_Behaviour__c != null && theOrderItem.NE__CatalogItem__r.NE__Technical_Behaviour__c.contains('Attachment Required');
        
        System.debug('[PCA_Address_B2W_Controller Constructor] productName: ' + productName);
        System.debug('[PCA_Address_B2W_Controller Constructor] mandatoryAttachment: ' + mandatoryAttachment);
        
        if(cId!=''&& cId!=null){//NEW ORDER
            isRegistration = true;
        }else{//MODIFICATION
           isRegistration = false;
        }
        
        NE__Order__c myOrder = [SELECT Case__c,Site__c FROM NE__Order__c WHERE ID =:orderId LIMIT 1];
        
        
        
        myCase = [SELECT Id, CaseNumber, TGS_Direccion__c, TGS_Pais__c, TGS_Ciudad__c, TGS_Codigo_Postal__c, TGS_Calle__c, TGS_Numero__c, TGS_Planta__c FROM Case WHERE ID =:myOrder.Case__c LIMIT 1];
        
        
        System.debug('[PCA_Address_B2W_Controller Constructor] isRegistration: ' + isRegistration);
        System.debug('[PCA_Address_B2W_Controller Constructor] myCase: ' + myCase);
        System.debug('[PCA_Address_B2W_Controller Constructor] mySite: ' + mySite);
        
    }
    
    //PAGEREFERENCE BUTTONS
    /**
     * Method:          nextStep
     * Description:     Método llamado cuando se pulsa el botón Next.
     */
    public PageReference nextStep(){
        try { 
            
            User AU = [SELECT Name, ContactId,Contact.BI_Cuenta_activa_en_portal__c, AccountId FROM User WHERE Id =:UserInfo.getUserId()]; 
        
            System.debug('[PCA_Address_B2W_Controller.nextStep] Authorized User: '+ AU);
            
            NE__Order__c myOrder= [SELECT Authorized_User__c, Site__c, HoldingId__c, NE__AccountId__c FROM NE__Order__c WHERE Id IN (SELECT Order__c FROM Case WHERE Id=:myCase.Id)][0];
            Account account = TGS_Portal_Utils.getLevel1(myOrder.NE__AccountId__c,1)[0];
            
            System.debug('[PCA_Address_B2W_Controller.nextStep] account: ' + account);
            
            NE__OrderItem__c myOrderItem= [SELECT NE__OrderId__c, Installation_point__c FROM NE__OrderItem__c WHERE NE__OrderId__c=:myOrder.Id LIMIT 1];
            
            System.debug('[PCA_Address_B2W_Controller.nextStep] myOrderItem: ' + myOrderItem);
            
            String holdingId=account.Id;
            myOrder.HoldingId__c=holdingId.substring(0,15);
            
            System.debug('[PCA_Address_B2W_Controller.nextStep] holdingId: '+myOrder.HoldingId__c);
            
            myOrder.Authorized_User__c=AU.Id;
            
            if(mySite!=null && mySite !=''){
               myOrder.Site__c=mySite; 
               myOrderItem.Installation_point__c=mySite;
            }
			update myOrderItem;
			update myOrder;
            
            System.debug('[PCA_Address_B2W_Controller.nextStep] myOrder: ' + myOrder);
            System.debug('[PCA_Address_B2W_Controller.nextStep] myOrderItem: ' + myOrderItem);
            
            fireSendEmail = true;
            
            update myCase;
            
            System.debug('[PCA_Address_B2W_Controller.nextStep] myCase despues del update: ' + myCase);
            
            
        } catch (Exception e) {
            System.debug('[PCA_Address_B2W_Controller.nextStep] ERROR updating case: ' + e);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating configuration'));
            return ApexPages.currentPage();
        }
        
        if(mandatoryAttachment && (isRegistration || myattachment.Body!=null)){
            myattachment.OwnerId = UserInfo.getUserId();
            myattachment.ParentId = theOrderItem.Id; 
            //myattachment.IsPrivate = true;
            myattachment.IsPrivate = false;
            
            try {
                System.debug('[PCA_Address_B2W_Controller.nextStep] myattachment: ' + myattachment);
                
                insert myattachment;
            } catch (DMLException e) {
                System.debug('[PCA_Address_B2W_Controller.nextStep] ERROR inserting attachment: ' + e);
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
                return null;
            } finally {
                myattachment = new Attachment(); 
            }
        }
        PageReference pageRef = new PageReference('/PCA_Order_Detail?caseId=' + myCase.Id);
        return pageRef;
    }
}