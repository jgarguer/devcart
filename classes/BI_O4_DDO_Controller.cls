/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Miguel Cabrera
 Company:       New Energy Aborda
 Description:   Controler class for "Asignar a oficinas" DDO Case button
 Test Class:    
 
 History:
  
 <Date>              <Author>                   <Change Description>
 28/08/2016          Miguel Cabrera             Initial Version
 13/06/2017          Oscar Bartolo              Change who to fill the subject. Now it use custom settings
 18/08/2017          Guillermo Muñoz            Changed to use new BI_MigrationHelper functionality
 26/09/2017			 Oscar Bartolo				Move functionality to queueable class BI_O4_CreateDDOCases
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public without sharing class BI_O4_DDO_Controller {
    
    public List<Case> lstCase {get; set;}

    public BI_O4_DDO_Controller(ApexPages.standardController controller) {
        
        Case caso = (Case) controller.getRecord();
        this.lstCase = [Select Id, OwnerId, BI_O4_DDO__c, RecordTypeId, Subject, Account.Name, Priority, Description, CreatedDate, BI_Type__c,
                               BI_Nombre_de_la_Oportunidad__c, BI_O4_Cliente_final_nombre__c, BI_O4_Es_proyecto_especial__c, BI_O4_Proyecto_Especial__c,
                               BI_O4_Tipo_gen_rico_Preventa__c, BI_O4_URL_WANQtool__c, BI_O4_URL_Request__c, BI_O4_Tipo_de_Facturaci_n__c, BI_O4_Tipo_de_movimiento__c, BI_O4_Periodo_de_contrataci_n_Meses__c, Status,
                               BI_O4_Fecha_presentaci_n_oferta__c, BI_O4_Identificativo_Origen__c, CaseNumber, BI_O4_Cliente_no_existente_nombre__c, BI_O4_URL_Cotizadores__c, BI_O4_Oportunidad_Local__c,
                               BI_Department__c, TGS_Service__c, BI_O4_Service__c, BI_O4_Main_Topics__c, Account.TGS_Account_Mnemonic__c, BI_O4_Main_Topics_Rechazados__c, BI_O4_Big_Deal__c, BI_Country__c
                        From Case
                        Where Id =: caso.Id Limit 1];
    }

    public PageReference assignOffice(){
        List<Case> updateCase = new List<Case>();
        String str_ddo = '';
        
        if(!this.lstCase.isEmpty()){
            for(Case item : this.lstCase){
                if (item.Status != 'Pendiente Valoración') {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.BI_O4_DDO_Status));
                    return null;
                }
                
                if(item.BI_O4_DDO__c != null){
                    str_ddo = item.BI_O4_DDO__c;
                    item.BI_O4_DDO__c = '';
                    item.Status = 'Pendiente Cotización';
                    updateCase.add(item);
                }
                else {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.BI_O4_DDO_Empty));
                    return null;
                }
            }
        }
        
        if (!updateCase.isEmpty()) {
            Map<Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(Userinfo.getUserId(), true, false, false, false);
            try{
                update updateCase;
            }catch(Exception e){
            	BI_LogHelper.generate_BILog('BI_O4_CaseMethods.closeChildCase', 'BI_O4', e, 'Trigger');
            }

            finally{
                if(mapa != null){
                    BI_MigrationHelper.disableBypass(mapa);
                }
            }
        }

        if(!this.lstCase.isEmpty()){
            List<String> lst_strDDO = str_ddo.split(';');
            BI_O4_CreateDDOCases creatCases = new BI_O4_CreateDDOCases(this.lstCase, str_ddo);
            if (lst_strDDO.size() > 10) {
                ID jobID = System.enqueueJob(creatCases);
            } else {
				creatCases.createCases();                
            }
        }
        
        return new PageReference('/' + this.lstCase[0].Id);
    }

    public PageReference cancel(){
        return new PageReference('/' + this.lstCase[0].Id);
    }
}