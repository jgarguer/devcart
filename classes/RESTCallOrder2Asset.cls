@RestResource(urlMapping='/RESTCallOrder2Asset/*')
global class RESTCallOrder2Asset 
{
    @HttpPost
	global static String callOrder2Asset(String confId, String caseId) 
    {
        return FutureCallOrder2Asset.callOrder2AssetMethod(confId,caseId);
    }
}