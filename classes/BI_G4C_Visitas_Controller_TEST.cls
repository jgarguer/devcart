@isTest
private class BI_G4C_Visitas_Controller_TEST {
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Miguel Molina Cruz
Company:       everis
Description:   Test class for BI_G4C_Visitas_Controller	    
History: 

<Date>                          <Author>                    <Change Description>
20/06/2016                      Miguel Molina Cruz          Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    @isTest
    public static void testg4c() {
        Test.startTest();
        Account CuentaPrueba = BI_G4C_Test_Auxiliar.InsertAccount();
        insert CuentaPrueba;
        Id IdAcc = CuentaPrueba.Id;
        event EventoPrueba = BI_G4C_Test_Auxiliar.InsertEvent(IdAcc);
        insert EventoPrueba;
        Contact ContactoPrueba = BI_G4C_Test_Auxiliar.InsertContact(IdAcc);
        insert ContactoPrueba;
        
        Opportunity OportunidadPrueba = BI_G4C_Test_Auxiliar.InsertOpportunity(IdAcc);
        insert OportunidadPrueba;

        //string a = 'aaaa';
        string nue = '{"attributes":{"type":"Event","url":"/services/data/v37.0/sobjects/Event/'+EventoPrueba.Id+'"},"Id":"'+EventoPrueba.Id+'","BI_FVI_Estado__c":"Nueva","WhoId":"'+ContactoPrueba.Id+'","ActivityDateTime":"2017-02-25T15:28:00.000Z","BI_ECU_Gestion__c":"Digital","BI_FVI_Resultado__c":"Prueba","BI_G4C_ResultadoVisita__c":"Dirección incorrecta","DurationInMinutes":15,"Subject":"987654321","Description":"987654321","IsReminderSet":false,"WhatId":"0012600000C44TBAAZ","BI_G4C_Oportunidad__c":"'+OportunidadPrueba.Id+'","BI_G4C_Tipo_de_Visita__c":"Presencial"}';
        string b = '{"attributes":{"type":"Event","url":"/services/data/v37.0/sobjects/Event/'+EventoPrueba.Id+'"},"Id":"'+EventoPrueba.Id+'","BI_FVI_Estado__c":"Planificada","WhoId":"'+ContactoPrueba.Id+'","ActivityDateTime":"2017-02-25T15:28:00.000Z","BI_ECU_Gestion__c":"Digital","BI_FVI_Resultado__c":"Prueba","BI_G4C_ResultadoVisita__c":"Dirección incorrecta","DurationInMinutes":15,"Subject":"987654321","Description":"987654321","IsReminderSet":false,"WhatId":"0012600000C44TBAAZ","BI_G4C_Oportunidad__c":"'+OportunidadPrueba.Id+'","BI_G4C_Tipo_de_Visita__c":"Presencial"}';
        string c = '{"name":"Prueba miguel","ContactId":"'+ContactoPrueba.Id+'","BI_G4C_Oportunidad__c":"'+OportunidadPrueba.Id+'","ContactName":"Miguel Molina","ContactPhone":"645464838","ContactMail":"miwemolina@gmail.com"}';
        string ini = '{"attributes":{"type":"Event","url":"/services/data/v37.0/sobjects/Event/'+EventoPrueba.Id+'"},"Id":"'+EventoPrueba.Id+'","BI_FVI_Estado__c":"Iniciada","WhoId":"'+ContactoPrueba.Id+'","ActivityDateTime":"2017-02-25T15:28:00.000Z","BI_ECU_Gestion__c":"Digital","BI_FVI_Resultado__c":"Prueba","BI_G4C_ResultadoVisita__c":"Dirección incorrecta","DurationInMinutes":15,"Subject":"987654321","Description":"987654321","IsReminderSet":false,"WhatId":"0012600000C44TBAAZ","BI_G4C_Oportunidad__c":"'+OportunidadPrueba.Id+'","BI_G4C_Tipo_de_Visita__c":"Presencial"}';
        string fin = '{"attributes":{"type":"Event","url":"/services/data/v37.0/sobjects/Event/'+EventoPrueba.Id+'"},"Id":"'+EventoPrueba.Id+'","BI_FVI_Estado__c":"Finalizada","WhoId":"'+ContactoPrueba.Id+'","ActivityDateTime":"2017-02-25T15:28:00.000Z","BI_ECU_Gestion__c":"Digital","BI_FVI_Resultado__c":"Prueba","BI_G4C_ResultadoVisita__c":"Dirección incorrecta","DurationInMinutes":15,"Subject":"987654321","Description":"987654321","IsReminderSet":false,"WhatId":"0012600000C44TBAAZ","BI_G4C_Oportunidad__c":"'+OportunidadPrueba.Id+'","BI_G4C_Tipo_de_Visita__c":"Presencial"}';
        string cua = '{"attributes":{"type":"Event","url":"/services/data/v37.0/sobjects/Event/'+EventoPrueba.Id+'"},"Id":"'+EventoPrueba.Id+'","BI_FVI_Estado__c":"Cualificada","WhoId":"'+ContactoPrueba.Id+'","ActivityDateTime":"2017-02-25T15:28:00.000Z","BI_ECU_Gestion__c":"Digital","BI_FVI_Resultado__c":"Prueba","BI_G4C_ResultadoVisita__c":"Dirección incorrecta","DurationInMinutes":15,"Subject":"987654321","Description":"987654321","IsReminderSet":false,"WhatId":"0012600000C44TBAAZ","BI_G4C_Oportunidad__c":"'+OportunidadPrueba.Id+'","BI_G4C_Tipo_de_Visita__c":"Presencial"}';
		
      
        //Account CuentaPrueba = BI_G4C_Test_Auxiliar.InsertAccount();
        //insert CuentaPrueba;
        //Id IdAcc = CuentaPrueba.Id;
        
        //event EventoPrueba1 = BI_G4C_Test_Auxiliar.InsertEvent(IdAcc);
        //insert EventoPrueba1;
        BI_G4C_Visitas_Controller.getEventByAccount(IdAcc);
        
        NE__Catalog__c CatalogoPrueba = BI_G4C_Test_Auxiliar.InsertCatalogo();
        insert CatalogoPrueba;
        Id IdCat = CatalogoPrueba.Id;	
        
        Campaign CampanaPrueba = BI_G4C_Test_Auxiliar.InsertCampaign(IdCat);
        insert CampanaPrueba;
        Id IdCam = CampanaPrueba.Id;          	
        
        BI_G4C_Visitas_Controller.getOpportunityData(IdAcc);
        
        Opportunity opoPrueba = BI_G4C_Test_Auxiliar.InsertOpportunity(IdAcc);
        opoPrueba.BI_No_Identificador_fiscal__c = 'prueba';
        insert opoPrueba;

        BI_G4C_Visitas_Controller.updateEvent(nue,c,null);
        BI_G4C_Visitas_Controller.updateEvent(b,c,null);
        BI_G4C_Visitas_Controller.updateEvent(ini,c,null);
        BI_G4C_Visitas_Controller.updateEvent(fin,c,null);
        BI_G4C_Visitas_Controller.updateEvent(cua,c,null);
        BI_G4C_Visitas_Controller.getEvent();
        BI_G4C_Visitas_Controller.getContactData(IdAcc);	
        BI_G4C_Visitas_Controller.datosContacto(ContactoPrueba.Id);
        BI_G4C_Visitas_Controller.datosCuenta(IdAcc, IdCam);	
        Contact ContactoPrueba1 = BI_G4C_Test_Auxiliar.InsertContact(IdAcc);
        
        insert ContactoPrueba1;
        
        BI_G4C_Visitas_Controller.getContactData(IdAcc);
        BI_G4C_Visitas_Controller.datosCuenta(IdAcc, IdCam);
        BI_G4C_Visitas_Controller.datosContacto(ContactoPrueba1.Id);
        
        BI_G4C_Visitas_Controller.getEventData(EventoPrueba.Id);
        BI_G4C_Visitas_Controller.deleteVisita(EventoPrueba.Id);
        Test.stopTest();
    }
    @istest
    public static void TestRandom(){

        Test.startTest();
        Account CuentaPrueba = BI_G4C_Test_Auxiliar.InsertAccount();
        insert CuentaPrueba;
        Id IdAcc = CuentaPrueba.Id;
        event EventoPrueba = BI_G4C_Test_Auxiliar.InsertEvent(IdAcc);
        insert EventoPrueba;
		Contact ContactoPrueba = BI_G4C_Test_Auxiliar.InsertContact(IdAcc);
        insert ContactoPrueba;
        
        Opportunity OportunidadPrueba = BI_G4C_Test_Auxiliar.InsertOpportunity(IdAcc);
        insert OportunidadPrueba;
        
        string nue = '{"attributes":{"type":"Event","url":"/services/data/v37.0/sobjects/Event/'+EventoPrueba.Id+'"},"Id":"'+EventoPrueba.Id+'","BI_FVI_Estado__c":"Nueva","WhoId":"'+ContactoPrueba.Id+'","ActivityDateTime":"2017-02-25T15:28:00.000Z","BI_ECU_Gestion__c":"Digital","BI_FVI_Resultado__c":"Prueba","BI_G4C_ResultadoVisita__c":"Dirección incorrecta","DurationInMinutes":15,"Subject":"987654321","Description":"987654321","IsReminderSet":false,"WhatId":"0012600000C44TBAAZ","BI_G4C_Oportunidad__c":"'+OportunidadPrueba.Id+'","BI_G4C_Tipo_de_Visita__c":"Presencial"}';
        string b = '{"attributes":{"type":"Event","url":"/services/data/v37.0/sobjects/Event/'+EventoPrueba.Id+'"},"Id":"'+EventoPrueba.Id+'","BI_FVI_Estado__c":"Planificada","WhoId":"'+ContactoPrueba.Id+'","ActivityDateTime":"2017-02-25T15:28:00.000Z","BI_ECU_Gestion__c":"Digital","BI_FVI_Resultado__c":"Prueba","BI_G4C_ResultadoVisita__c":"Dirección incorrecta","DurationInMinutes":15,"Subject":"987654321","Description":"987654321","IsReminderSet":false,"WhatId":"0012600000C44TBAAZ","BI_G4C_Oportunidad__c":"'+OportunidadPrueba.Id+'","BI_G4C_Tipo_de_Visita__c":"Presencial"}';
        string c = '{"name":"Prueba miguel","ContactId":"'+ContactoPrueba.Id+'","BI_G4C_Oportunidad__c":"'+OportunidadPrueba.Id+'","ContactName":"Miguel Molina","ContactPhone":"645464838","ContactMail":"miwemolina@gmail.com"}';
        string ini = '{"attributes":{"type":"Event","url":"/services/data/v37.0/sobjects/Event/'+EventoPrueba.Id+'"},"Id":"'+EventoPrueba.Id+'","BI_FVI_Estado__c":"Iniciada","WhoId":"'+ContactoPrueba.Id+'","ActivityDateTime":"2017-02-25T15:28:00.000Z","BI_ECU_Gestion__c":"Digital","BI_FVI_Resultado__c":"Prueba","BI_G4C_ResultadoVisita__c":"Dirección incorrecta","DurationInMinutes":15,"Subject":"987654321","Description":"987654321","IsReminderSet":false,"WhatId":"0012600000C44TBAAZ","BI_G4C_Oportunidad__c":"'+OportunidadPrueba.Id+'","BI_G4C_Tipo_de_Visita__c":"Presencial"}';
        string fin = '{"attributes":{"type":"Event","url":"/services/data/v37.0/sobjects/Event/'+EventoPrueba.Id+'"},"Id":"'+EventoPrueba.Id+'","BI_FVI_Estado__c":"Finalizada","WhoId":"'+ContactoPrueba.Id+'","ActivityDateTime":"2017-02-25T15:28:00.000Z","BI_ECU_Gestion__c":"Digital","BI_FVI_Resultado__c":"Prueba","BI_G4C_ResultadoVisita__c":"Dirección incorrecta","DurationInMinutes":15,"Subject":"987654321","Description":"987654321","IsReminderSet":false,"WhatId":"0012600000C44TBAAZ","BI_G4C_Oportunidad__c":"'+OportunidadPrueba.Id+'","BI_G4C_Tipo_de_Visita__c":"Presencial"}';
        string cua = '{"attributes":{"type":"Event","url":"/services/data/v37.0/sobjects/Event/'+EventoPrueba.Id+'"},"Id":"'+EventoPrueba.Id+'","BI_FVI_Estado__c":"Cualificada","WhoId":"'+ContactoPrueba.Id+'","ActivityDateTime":"2017-02-25T15:28:00.000Z","BI_ECU_Gestion__c":"Digital","BI_FVI_Resultado__c":"Prueba","BI_G4C_ResultadoVisita__c":"Dirección incorrecta","DurationInMinutes":15,"Subject":"987654321","Description":"987654321","IsReminderSet":false,"WhatId":"0012600000C44TBAAZ","BI_G4C_Oportunidad__c":"'+OportunidadPrueba.Id+'","BI_G4C_Tipo_de_Visita__c":"Presencial"}';
		
        BI_G4C_Visitas_Controller.getLocaleToDateTimeFmtMap();
        BI_G4C_Visitas_Controller.getUserDateFormat();
        //geoloc
        BI_G4C_Visitas_Controller.geoloc(EventoPrueba.id,'41.641489 -0.8782392');
        //customSearchAndOrder
        BI_G4C_Visitas_Controller.customSearchAndOrder(CuentaPrueba.id,'',true,'BI_Identificador_Interno__c');
        BI_G4C_Visitas_Controller.customSearchAndOrder(CuentaPrueba.id,'',true,'Contacto');
        BI_G4C_Visitas_Controller.customSearchAndOrder(CuentaPrueba.id,'',true,'FechaHora');
        BI_G4C_Visitas_Controller.customSearchAndOrder(CuentaPrueba.id,'',true,'Asignado');
        BI_G4C_Visitas_Controller.customSearchAndOrder(CuentaPrueba.id,'',false,'Estado');
        //customSearch
        BI_G4C_Visitas_Controller.customSearch(CuentaPrueba.id,'');
        //getEventByAccount
        BI_G4C_Visitas_Controller.getEventByAccount(CuentaPrueba.id);
        //getEventByAccountOrderASC
        BI_G4C_Visitas_Controller.getEventByAccountOrderASC(CuentaPrueba.id,'BI_Identificador_Interno__c', '',true);
        BI_G4C_Visitas_Controller.getEventByAccountOrderASC(CuentaPrueba.id,'Contacto', '',true);
        BI_G4C_Visitas_Controller.getEventByAccountOrderASC(CuentaPrueba.id,'FechaHora', '',true);
        BI_G4C_Visitas_Controller.getEventByAccountOrderASC(CuentaPrueba.id,'Asignado', '',true);
        BI_G4C_Visitas_Controller.getEventByAccountOrderASC(CuentaPrueba.id,'Estado', '',false);
        //getAccountById
        BI_G4C_Visitas_Controller.getAccountById(CuentaPrueba.id);
        //getCurrentUserData
        BI_G4C_Visitas_Controller.getCurrentUserData();
        //updateEvent
        BI_G4C_Visitas_Controller.updateEvent(nue,c,null);
        BI_G4C_Visitas_Controller.updateEvent(b,c,null);
        BI_G4C_Visitas_Controller.updateEvent(ini,c,null);
        BI_G4C_Visitas_Controller.updateEvent(fin,c,null);
        //BI_G4C_Visitas_Controller.updateEvent(cua,c,null);
        //getEvent
         BI_G4C_Visitas_Controller.getEvent();
        //getContactData
        BI_G4C_Visitas_Controller.getContactData(CuentaPrueba.id);
        //getOpportunityData
        BI_G4C_Visitas_Controller.getOpportunityData(CuentaPrueba.id);
        //datosCuenta
        BI_G4C_Visitas_Controller.datosCuenta(CuentaPrueba.id,'');
        //datosContacto
        BI_G4C_Visitas_Controller.datosContacto(ContactoPrueba.Id);
        //getEventData
        BI_G4C_Visitas_Controller.getEventData(EventoPrueba.Id);
        //getCurrentUser
        BI_G4C_Visitas_Controller.getCurrentUser();
        //updatesend
         BI_G4C_Visitas_Controller.updatesend(EventoPrueba.Id);
        //sendPdf
        BI_G4C_Visitas_Controller.sendPdf(EventoPrueba.Id,ContactoPrueba.Id);


        
        //other special test
         //BI_G4C_Visitas_Controller classToTest=new BI_G4C_Visitas_Controller();
        //getRedirectEsVisita
         //classToTest.getRedirectEsVisita();
        //Variables
        //Id accid=classToTest.accountId;
        //Event oEvent=classToTest.oEvent;
        //String accountName=classToTest.accountName;
        //String accountCountry=classToTest.accountCountry;
        //String eventLocation=classToTest.eventLocation;
        
        
        
        //deleteVisita
        BI_G4C_Visitas_Controller.deleteVisita(EventoPrueba.Id);
        Test.stopTest();
    }

    
}