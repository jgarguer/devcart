/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Test for BI_O4_NewQuoteController
    Test Class:    BI_O4_NewQuoteController_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    18/08/2016              Miguel Cabrera          Initial version
    02/02/2017				Pedro Párraga		    Increase coverage
    20/09/2017              Antonio Mendivil        Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c 
    25/09/2017              Jaime Regidor           Fields BI_Subsector__c and BI_Sector__c added 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_O4_NewQuoteController_TEST {
	
	static{
   		BI_TestUtils.throw_exception = false;
  	}

	@isTest static void test_method_one() {

		RecordType rt_op = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Opty'];
		
		BI_TestUtils.throw_exception = false;
		RecordType rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];
        User usu = BI_DataLoadAgendamientos.loadValidAdministratorUser();
		       
    	Account acc = new Account();
    	acc.Name = 'test';
		acc.BI_Envio_a_aprobacion_agrupacion_cliente__c = true;
		acc.BI_Activo__c = Label.BI_Si;
		acc.BI_Segment__c = 'Empresas';
		acc.BI_Subsector__c = 'test'; //25/09/2017
		acc.BI_Sector__c = 'test'; //25/09/2017
		acc.BI_Subsegment_local__c = 'Top 1';
		acc.RecordtypeId = rt.Id;
		acc.BI_Activo__c = Label.BI_Si;
        acc.BI_Country__c = 'Spain';
        acc.BI_Subsegment_Regional__c = 'test';
		acc.BI_Territory__c = 'test';
        System.runAs(usu){
        insert acc;
    	}

    	RecordType rto = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
		opp.CloseDate = Date.today();
		opp.StageName = Label.BI_F6Preoportunidad;
		opp.BI_Ciclo_ventas__c = Label.BI_Completo;
		opp.RecordTypeId = rto.Id;
		opp.NE__HaveActiveLineItem__c = true;
		opp.NE__Order_Generated__c = 'Quote';
		opp.AccountId = acc.Id;
		opp.CurrencyIsoCode = 'EUR';
		insert opp;

		//RecordType rtn = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Quote'];

		NE__Order__c neo = new NE__Order__c();
		neo.NE__OpportunityId__c = opp.Id;
		neo.NE__OptyId__c = opp.Id;
		neo.NE__Version__c = 1;
		neo.RecordTypeId = rt_op.Id;
		neo.NE__OrderStatus__c = 'Revised';
		neo.NE__SerialNum__c = 'OR-9999999999';
		
		insert neo;

        NE__OrderItem__c orderItem = new NE__OrderItem__c(NE__OrderId__c = neo.Id,
		                                                   NE__Qty__c = 2,
		                                                   NE__Asset_Item_Account__c = acc.Id,
		                                                   NE__Account__c = acc.Id,
		                                                   //NE__Catalog__c = catalogobj.Id,
		                                                   //NE__CatalogItem__c = catalogItem1.Id,
		                                                   NE__Status__c = 'Active');
                                                                
        insert orderItem;
        
		Test.startTest();

		ApexPages.StandardController standardC = new ApexPages.StandardController(opp); 
		BI_O4_NewQuoteController controller = new BI_O4_NewQuoteController(standardC);

		controller.newQuote();
		controller.cancel();
		System.assertNotEquals(null, opp.Id);
		

		//------------------------------------------------------------------------------------------------------
		BI_TestUtils.throw_exception = true;
		RecordType rt1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'TGS_Legal_Entity'];
		       
    	Account acc1 = new Account();
    	acc1.Name = 'test';
		acc1.BI_Envio_a_aprobacion_agrupacion_cliente__c = true;
		acc1.BI_Activo__c = Label.BI_Si;
		acc1.BI_Segment__c = 'Empresas';
		acc1.BI_Subsector__c = 'test'; //25/09/2017
		acc1.BI_Sector__c = 'test'; //25/09/2017
		acc1.BI_Subsegment_local__c = 'Top 1';
		acc1.RecordtypeId = rt1.Id;
		acc1.BI_Activo__c = Label.BI_Si;
        acc1.BI_Country__c = 'Spain';
        acc1.BI_Subsegment_Regional__c = 'test';
		acc1.BI_Territory__c = 'test';
        System.runAs(usu){
        insert acc1;
    	}

    	RecordType rto1 = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Oportunidad_Regional' limit 1];

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test';
		opp1.CloseDate = Date.today();
		opp1.StageName = Label.BI_F6Preoportunidad;
		opp1.BI_Ciclo_ventas__c = Label.BI_Completo;
		opp1.RecordTypeId = rto1.Id;
		opp1.NE__HaveActiveLineItem__c = true;
		opp1.NE__Order_Generated__c = 'Quote';
		opp1.AccountId = acc1.Id;
		opp1.CurrencyIsoCode = 'EUR';
		insert opp1;

		//RecordType rtn1 = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Quote'];

		NE__Order__c neo1 = new NE__Order__c();
		neo1.NE__OpportunityId__c = opp1.Id;
		neo1.NE__OptyId__c = opp1.Id;
		neo1.NE__Version__c = 1;
		neo1.RecordTypeId = rt_op.Id;
		neo1.NE__OrderStatus__c = 'Active';
		neo1.NE__SerialNum__c = 'OR-9999999999';
		
		insert neo1;

        NE__OrderItem__c orderItem2 = new NE__OrderItem__c(NE__OrderId__c = neo1.Id,
		                                                   NE__Qty__c = 2,
		                                                   NE__Asset_Item_Account__c = acc1.Id,
		                                                   NE__Account__c = acc1.Id,
		                                                   //NE__Catalog__c = catalogobj.Id,
		                                                   //NE__CatalogItem__c = catalogItem1.Id,
		                                                   NE__Status__c = 'Active');
                                                                
        insert orderItem2;
        
		ApexPages.StandardController standardC1 = new ApexPages.StandardController(opp1); 
		BI_O4_NewQuoteController controller1 = new BI_O4_NewQuoteController(standardC1);

		controller1.newQuote();
		controller1.cancel();
		System.assertNotEquals(null, opp1.Id);
		Test.stopTest();
	}	

	@isTest static void exceptions() {

		BI_TestUtils.throw_exception = true;

		test_method_one();
	}

	@isTest static void noExceptions() {

		BI_TestUtils.throw_exception = false;

		test_method_one();
	}
}