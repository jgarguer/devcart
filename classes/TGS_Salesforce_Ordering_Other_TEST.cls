@isTest(seeAllData = true)
@testVisible
private class TGS_Salesforce_Ordering_Other_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test class to manage the coverage code for TGS_Salesforce_Ordering_Other class 

    <Date>                  <Author>                <Change Description>
    13/09/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        
    static String attributesRegistration = 'Action="CREATE_SERVICE";Sigma_WH_Site_ID="20150903-TEST-0000495671";CSP_Catalog="Managed WAN";ServiceID="MWANSU0000";RequesterLoginName="mw_gen";Country="Germany";Itemized_Invoice="Yes";Delivery_Contact_FirstName="IntegrationSIGMA2";Delivery_Contact_Phone="+59133426060";Delivery_Contact_Email="deloitteinfinity2@gmail.com";Resiliency="Basic";Common_Name="Customer 11";Measurement_Site="20150903-SITEMPLS-0000495671";SIGMA_WH_Service_Request="A-TIWS-ACME-ACME-SITEMPLS-0000268612";VPN_SITE_ID="ALE-ACME-0987";Maintenance_Option="6*12";CPE_Modality="Leased";MultiVPN_Modality="None";LAN_Routing_Protocol="RIP";Technical_Contact_Name="User";Technical_Contact_Phone="+59133426060";Technical_Contact_e-mail="user@mail.test";Order_term_months="12";Multicast="No";IPV6="No";DHCP_Server="No";NAT="No";Access_List_ACL="No";GLBP="No";Network_Performance_Reporting="No";Spread_Route_Control="No";Lan_Traffic_Marking="No";Provision_Date="'+date.today().format()+'";Acceptance_Date="'+date.today().format()+'";Comments="PILOTO "';
    static String listAttributesOpt = 'VPN(Name="VPN Acme2";|)Additional_Equipment(Brand="Brand"|)Access(AL_ID="1";CPE_ID="1";IPSec="No";Access_Technology="Ethernet";Access_Connector="Undefined";Types_of_Interface="Ethernet";Access_Speed="10 Mbps";IP_Bandwidth_Kb="1024";International_IP_Bandwidth_Kb="111";National_IP_Bandwidth_Kb="913";Jumboframe="No";Notes="PILOTO"|)CPE(CPE_ID="1";Name="Undefined";Manufacturer="1";Brand="1";Model="Cisco 800 Series";Serial_Number="Undefined";|)IP_Address(CPE_ID="1";IP_Address="0.0.0.0";Purpose="HSRP";|CPE_ID="1";IP_Address="0.0.0.0";Purpose="Loopback";|CPE_ID="1";IP_Address="0.0.0.0";Purpose="Customer";|CPE_ID="1";IP_Address="0.0.0.0";Purpose="WAN";|CPE_ID="1";IP_Address="0.0.0.0";Purpose="LAN";|)CoS(BWPercentage="100";CosType="Data-Bronze";AL_ID="1";VPN="VPN Acme2";|)SLA(Compromished_Target="10";SLA_Name="Delivery Time";|)SLA_per_CoS(Compromished_Target="10";SLA_Name="Delivery Time";CosType="costype"|)Charges(CPE_Main_price="0.00 EUR";CPE_Backup_price="0.00 EUR";Access_1_price="1.00 EUR";Access_2_price="0.00 EUR";Access_3_price="0.00 EUR";Access_4_price="0.00 EUR";IP_Bandwidth_1_price="0.00 EUR";IP_Bandwidth_2_price="0.00 EUR";IP_Bandwidth_3_price="0.00 EUR";IP_Bandwidth_4_price="0.00 EUR";Additional_equipment_price="0.00 EUR";Additional_features_price="0.00 EUR";Multicast_price="0.00 EUR";Service_maintenance_price="0.00 EUR";Service_management_price="0.00 EUR";Activation_price="0.00 EUR";BC_ID="MWANCC0001";|CPE_Main_price="0.00 EUR";CPE_Backup_price="0.00 EUR";Access_1_price="100.00 EUR";Access_2_price="0.00 EUR";Access_3_price="0.00 EUR";Access_4_price="0.00 EUR";IP_Bandwidth_1_price="0.00 EUR";IP_Bandwidth_2_price="0.00 EUR";IP_Bandwidth_3_price="0.00 EUR";IP_Bandwidth_4_price="0.00 EUR";Additional_equipment_price="0.00 EUR";Additional_features_price="0.00 EUR";Multicast_price="0.00 EUR";Service_maintenance_price="0.00 EUR";Service_management_price="0.00 EUR";Monthly_fee="0.00 EUR";BC_ID="MWANCC0010";|)Data_Template(Attribute_ID="7";Attribute_name="WAN Optimization";Attribute_value="Yes";|Attribute_ID="8";Attribute_name="WAN Optimization Service";Attribute_value="Visibilidad";|Attribute_ID="9";Attribute_name="Special Applications OPT - SSL Acceleration";Attribute_value="No";|Attribute_ID="10";Attribute_name="Special Applications OPT - eMAPI Acceleration";Attribute_value="No";|Attribute_ID="11";Attribute_name="Special Applications OPT - File Acceleration";Attribute_value="No";|Attribute_ID="12";Attribute_name="Special Applications OPT - HTTPS Protocol";Attribute_value="No";|Attribute_ID="13";Attribute_name="Special Applications OPT - Video";Attribute_value="No";|Attribute_ID="14";Attribute_name="Technical Solution ";Attribute_value="WAAS express";|Attribute_ID="15";Attribute_name="WAVE Model";Attribute_value="Cisco WAVE-294";|Attribute_ID="16";Attribute_name="WAVE Redundancy ";Attribute_value="No";|Attribute_ID="17";Attribute_name="WAVE Maintenance";Attribute_value="7x24";|Attribute_ID="18";Attribute_name="SRE Model";Attribute_value="SRE 710";|Attribute_ID="19";Attribute_name="Redundancia de SRE ";Attribute_value="No";|Attribute_ID="20";Attribute_name="Additional Features OPT - Technical Consulting";Attribute_value="No";|Attribute_ID="21";Attribute_name="Additional Features OPT - Pilot Test";Attribute_value="No";|)Data_Template_charges(name="WAAS Device Activation";type="NRC";movement_type="new";bc_id="WOPTCC00011";precio="10.00 EUR";|name="WAAS Device Recurrent";type="MRC";movement_type="new";bc_id="WOPTCC00012";precio="0.00 EUR";|name="WAN Optimization Activation";type="NRC";movement_type="new";bc_id="WOPTCC00021";precio="0.00 EUR";|name="WAN Optimization Recurrent";type="MRC";movement_type="new";bc_id="WOPTCC00022";precio="0.00 EUR";|name="Application Visibility Activation";type="NRC";movement_type="new";bc_id="WOPTCC00031";precio="0.00 EUR";|name="Application Visibility Recurrent";type="MRC";movement_type="new";bc_id="WOPTCC00032";precio="0.00 EUR";|name="Application Acceleration Activation";type="NRC";movement_type="new";bc_id="WOPTCC00041";precio="12.00 EUR";|name="Application Acceleration Recurrent";type="MRC";movement_type="new";bc_id="WOPTCC00042";precio="14.00 EUR";|name="WAAS.Management Activation";type="NRC";movement_type="new";bc_id="WOPTCC00051";precio="15.00 EUR";|name="WAAS.Management Recurrent";type="MRC";movement_type="new";bc_id="WOPTCC00052";precio="12.00 EUR";|name="WAAS.Additional Features Activation";type="NRC";movement_type="new";bc_id="WOPTCC00061";precio="22.00 EUR";|name="WAAS.Additional Features Recurrent";type="MRC";movement_type="new";bc_id="WOPTCC00062";precio="2.00 EUR";|)'; 
    static TGS_Salesforce_Ordering_Global.AttachmentList attachmentList = createListAttachment(); 
    
    //Dummy data
    static String attAcc = 'Customer_Name="ACME";';
    static String attCase = '';
    static TGS_Salesforce_Ordering_Global.OutputMapping6 result = null;
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Salesforce_Ordering_Other.pendingClarification
            
     <Date>                 <Author>                <Change Description>
    13/09/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void pendingClarificationTest() {
        attAcc = 'Customer_Name="ACME";';
        // Registration order
        result = TGS_Salesforce_Ordering_Global.Creation('SigmaWH', 'createOrder', '1', '970200', attAcc+attributesRegistration, listAttributesOpt, attachmentList);
        if(result.Error_Code.equals('0')){
            System.assertNotEquals(null, result.Incident_ID);
            System.debug('caseNumber: '+result.Incident_ID);
            attCase = 'Incident_Number="'+result.Incident_ID+'";';
            // PendingClarification
            Test.startTest();
            TGS_Salesforce_Ordering_Other.OutputMapping1 result2 = TGS_Salesforce_Ordering_Other.Create('SigmaWH', '1', attCase+'Status="Pending";', '', Blob.valueof(''));        
            System.assertEquals('0', result2.Error_Code);
            Test.stopTest();
        }
    }
    
    static testMethod void errorPendingClarificationInvalidCaseTest() {
        attCase = 'Incident_Number="000";';
        // PendingClarification
        Test.startTest();
        TGS_Salesforce_Ordering_Other.OutputMapping1 result2 = TGS_Salesforce_Ordering_Other.Create('SigmaWH', '1', attCase+'Status="Pending";', '', Blob.valueof(''));        
        System.assertEquals('11015', result2.Error_Code);
        Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Salesforce_Ordering_Other.createEvolutionNote
            
     <Date>                 <Author>                <Change Description>
    13/09/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void evolutionNoteTest() {
        attAcc = 'Customer_Name="ACME";';
        // Registration order
        result = TGS_Salesforce_Ordering_Global.Creation('SigmaWH', 'createOrder', '1', '970200', attAcc+attributesRegistration, listAttributesOpt, attachmentList);
        if(result.Error_Code.equals('0')){
            System.assertNotEquals(null, result.Incident_ID);
            System.debug('caseNumber: '+result.Incident_ID);
            attCase = 'Incident_Number="'+result.Incident_ID+'";';
            // Evolution Note
            Test.startTest();
            TGS_Salesforce_Ordering_Other.OutputMapping1 result2 = TGS_Salesforce_Ordering_Other.Create('SigmaWH', '1', attCase+'Work_Info_Summary="Evolution Note Summary";Work_Info_Notes="New evolution note SF - SIGMA";', 'TestFile1.txt', Blob.valueof('QXJjaGl2byBkZSBwcnVlYmEuDQpUZXN0IGZpbGUu'));     
            System.assertEquals('0', result2.Error_Code);
            Test.stopTest();
        }
    }
    
    static testMethod void errorEvolutionNoteInvalidCaseTest() {
        attCase = 'Incident_Number="000";';
        // Evolution Note
        Test.startTest();
        TGS_Salesforce_Ordering_Other.OutputMapping1 result2 = TGS_Salesforce_Ordering_Other.Create('SigmaWH', '1', attCase+'Work_Info_Summary="Evolution Note Summary";Work_Info_Notes="New evolution note SF - SIGMA";', '', Blob.valueof(''));     
        System.assertEquals('11015', result2.Error_Code);
        Test.stopTest();
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Marta Laliena
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_Salesforce_Ordering_Other.creation
            
     <Date>                 <Author>                <Change Description>
    13/09/2015              Marta Laliena           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void errorMandatoryFieldsTest() {
        attAcc = 'Customer_Name="ACME";';
        // Registration order
        result = TGS_Salesforce_Ordering_Global.Creation('SigmaWH', 'createOrder', '1', '970200', attAcc+attributesRegistration, listAttributesOpt, attachmentList);
        if(result.Error_Code.equals('0')){
            System.assertNotEquals(null, result.Incident_ID);
            System.debug('caseNumber: '+result.Incident_ID);
            // Resolve
            Test.startTest();
            List<Case> listCase = [SELECT Id, CaseNumber FROM Case WHERE CaseNumber = :result.Incident_ID];
            System.assertNotEquals(0, listCase.size());
            Case caseN = listCase[0];
            caseN.Status = 'Resolved';
            caseN.TGS_Status_Reason__c = '';
            update CaseN;
            attCase = 'Incident_Number="'+result.Incident_ID+'";';
            // Invalid operation -> Error(s) while checking the mandatory fields
            TGS_Salesforce_Ordering_Other.OutputMapping1 result2 = TGS_Salesforce_Ordering_Other.Create('SigmaWH', '1', attCase, '', Blob.valueof(''));      
            System.assertEquals('11420', result2.Error_Code);
            Test.stopTest();
        }
   }
    
    static testMethod void errorParsingInfoTest() {
        attAcc = 'Customer_Name="ACME";';
        // Registration order
        result = TGS_Salesforce_Ordering_Global.Creation('SigmaWH', 'createOrder', '1', '970200', attAcc+attributesRegistration, listAttributesOpt, attachmentList);
        if(result.Error_Code.equals('0')){
            System.assertNotEquals(null, result.Incident_ID);
            System.debug('caseNumber: '+result.Incident_ID);
            // Resolve
            Test.startTest();
            List<Case> listCase = [SELECT Id, CaseNumber FROM Case WHERE CaseNumber = :result.Incident_ID];
            System.assertNotEquals(0, listCase.size());
            Case caseN = listCase[0];
            caseN.Status = 'Resolved';
            caseN.TGS_Status_Reason__c = '';
            update CaseN;
            attCase = 'Incident_Number="'+result.Incident_ID+'";';
            // Error parsing
            TGS_Salesforce_Ordering_Other.OutputMapping1 result2 = TGS_Salesforce_Ordering_Other.Create('SigmaWH', '1', attCase+'att', '', Blob.valueof(''));      
            System.assertEquals('11015', result2.Error_Code);
            Test.stopTest();
        }
   }
    
    
    private static TGS_Salesforce_Ordering_Global.AttachmentList createListAttachment(){
        TGS_Salesforce_Ordering_Global.AttachmentList attList = new TGS_Salesforce_Ordering_Global.AttachmentList();
        TGS_Salesforce_Ordering_Global.AttachmentReq att = new TGS_Salesforce_Ordering_Global.AttachmentReq();
        att.Attachment_ID = 'ATTACHMENT1';
        att.Attachment_Name = 'File1.txt';
        att.Attachment_Data = Blob.valueof('QXJjaGl2byBkZSBwcnVlYmEuDQpUZXN0IGZpbGUu');
        attList.attachment = new List<TGS_Salesforce_Ordering_Global.AttachmentReq>();
        attList.attachment.add(att);        
        return attList;
    }
    
    private static Account dummyAccountContactCommunityUser(){
        // User  
        User userWithRole;
        if(UserInfo.getUserRoleId() == null) {
            UserRole r = new UserRole(name = 'TEST ROLE');
            insert r;
            
            userWithRole = new User(alias = 'hasrole', email='userwithrole@roletest1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(), 
                                    timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com',
                                    BI_Permisos__c = 'TGS', isActive = True);
        } else {
            userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
        } 
        System.assert(userWithRole.userRoleId != null, 
                      'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        
        Account a;
        Contact c;
        // An user with userRole is needed in the runAs() for the CWP user creation 
        System.runAs(userWithRole) {
            a = TGS_Dummy_Test_data.dummyHierarchy();
            //Database.insert(a);   Originally in the method; not neccesary since dummyHierachly() do the insert call      
            c = new Contact(AccountId = a.Id,
                            FirstName='IntegrationSIGMA2',
                            LastName = 'IntegrationSIGMA2',
                            Email = 'deloitteinfinity2@gmail.com',
                            BI_Activo__c = true );
            insert c;
        }
        
        // Get any profile for the given type.
        Profile p = [select Id 
                       from profile 
                      where usertype = 'PowerCustomerSuccess' and Name='TGS Customer Community Plus'
                      limit 1];   
        
        User pu = new User(profileId = p.id, username = 'deloitteinfinity2@gmail.com', email = 'deloitteinfinity2@gmail.com', 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='inte', LastName='IntegrationSIGMA2', contactId = c.Id, BI_Permisos__c = 'TGS',
                          isActive = True);
        insert pu;
        
        Account customerCountry = [SELECT Id, Name, ParentId FROM Account
                                  WHERE Id = :a.ParentId];
        Account holding = [SELECT Id, Name FROM Account
                            WHERE Id = :customerCountry.ParentId];

        return holding;
    }
    
    private static void dummyCaseRegmWan(String status){
     /*   result = TGS_Salesforce_Ordering_Global.Creation('SigmaWH', 'createOrder', '1', '970200', attAcc+attributesRegistration, listAttributesOpt, attachmentList);
        Case caseN = [SELECT Id, CaseNumber, Status, TGS_Status_Reason__c, Order__c FROM Case WHERE CaseNumber = :result.Operation_Instance_ID LIMIT 1];
        if(status.equals(Constants.CASE_STATUS_RESOLVED) || status.equals(Constants.CASE_STATUS_CLOSED)){
            NE__Order__c orderN = [SELECT Id, Name, NE__ServAccId__c, NE__BillAccId__c, Site__c FROM NE__Order__c WHERE Id = :caseN.Order__c];
            Account bu = TGS_Portal_Utils.getLevel4(account.Id,1)[0];
            orderN.NE__ServAccId__c = bu.Id;
            orderN.NE__BillAccId__c = TGS_Portal_Utils.getLevel4(bu.Id,1)[0].Id;
            orderN.Site__c = TGS_Portal_Utils.getSites(bu.Id)[0].Id;
            update orderN;
            caseN.Status = Constants.CASE_STATUS_RESOLVED;
            caseN.TGS_Status_Reason__c = '';
            update caseN;
        }
        if(status.equals(Constants.CASE_STATUS_CLOSED)){
            caseN.Status = Constants.CASE_STATUS_CLOSED;
            update caseN;
        }*/
    }

}