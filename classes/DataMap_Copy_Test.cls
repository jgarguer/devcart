@IsTest 
public class DataMap_Copy_Test {
    private static String processCode;
    private static DataMap_Copy dmc;
    private static String mapName;
    private static String sourceId;
    
  static testMethod void testCallDataMap() {
            // Update processCode__c field for OrderItem
            if(processCode==null){
                processCode = generateRandomNumber();
            }
/*            for(NE__Order__c ord : orders) {
/                ord.processCode__c = processCode;
            }
            update orders;
            for(NE__OrderItem__c ordIt : orderItems) {
                ordIt.processCode__c = processCode;
            }
            update orderItems;
*/            
            // Call datamapper
            NE.DataMap.DataMapRequest dReq = new NE.DataMap.DataMapRequest();
            dReq.mapName = 'Order2Order';
            dReq.sourceId = null;
            Map<String,String> mapOfq = new Map<String,String>();
            list<String> listOfph = new list<String>();
            listOfph.add(processCode);

            String customQuery = processCode;
            mapOfq.put('Order',customQuery);
            mapOfq.put('NoPromoOrderItem',customQuery);
            mapOfq.put('PromoOrderItem',customQuery);
            mapOfq.put('OrderItem',customQuery);

            dReq.mapOfCustomQueries = mapOfq;
            dReq.listOfPlaceHolders = listOfph;
            dReq.updateActive= true;
            dmc = new DataMap_Copy();
            System.debug('Lista listOfPlaceHolders: '+ listOfph);
            NE.DataMap obj = new NE.DataMap();
            NE.DataMap.DataMapResponse dResp = obj.callDataMap(dReq);
            System.debug(LoggingLevel.ERROR, dmc);
            //System.debug(LoggingLevel.ERROR, response);
            
            //
            NE__Order__c testOi = TGS_Dummy_Test_Data.dummyOrder();
            testOi.NE__Delivery_Date__c= Date.newInstance(2017, 12, 11);
            update testOi;
            NE__OrderItem__c testOit= TGS_Dummy_Test_Data.dummyOrderItem();
            testOi.NE__Country__c='Spain';
            update testOi;
            RecordType rec1= [SELECT Id FROM RecordType limit 1];
            mapName = 'Order2Order';
            //sourceId = testOi.Id;
            sourceId =null;

            NE__Map__c mp = new NE__Map__c(NE__Map_Name__c = mapName, NE__Type__c = 'Object2Object');
            insert mp;
            NE__MapObject__c mpo = new NE__MapObject__c(Name = 'Order', NE__Sequence__c=0,NE__Activate_Sort__c= false,NE__Sort_Source__c='', NE__Sort_Operation__c='ASC NULLS FIRST', NE__Source__c='NE__Order__c', NE__Target__c='NE__Order__c', NE__Type__c= 'Object',NE__Map__c= mp.Id,NE__Filter__c='', NE__Activate_Filter__c=true, NE__Filter_Operation__c= '=:', NE__Filter_Source__c='NE__Delivery_Date__c',NE__Filter_Target__c='2017/12/11');
            insert mpo;
            NE__MapObject__c mpo2 = new NE__MapObject__c(Name = 'NoPromoOrderItem', NE__Sequence__c=1,NE__Activate_Sort__c= true,NE__Sort_Source__c='id', NE__Sort_Operation__c='ASC NULLS FIRST', NE__Source__c='NE__OrderItem__c', NE__Target__c='orderitem__c', NE__Type__c= 'Object',NE__Map__c= mp.Id, NE__Parent__c=mpo.Id, NE__Filter__c='Generate_Asset_Item__c = true', NE__Activate_Filter__c=true, NE__Filter_Operation__c= '=:', NE__Filter_Source__c='BI_Allow_Recurring_Charge_Discount__c',NE__Filter_Target__c='False');
            insert mpo2;
            NE__MapObject__c mpo6 = new NE__MapObject__c(Name = 'NoPromoOrderItem2', NE__Sequence__c=4,NE__Activate_Sort__c= true,NE__Sort_Source__c='id', NE__Sort_Operation__c='ASC NULLS FIRST', NE__Source__c='NE__OrderItem__c', NE__Target__c='orderitem__c', NE__Type__c= 'Object',NE__Map__c= mp.Id, NE__Parent__c=mpo.Id, NE__Filter__c='Generate_Asset_Item__c = true', NE__Activate_Filter__c=true, NE__Filter_Operation__c= '=:', NE__Filter_Source__c='NE__Description__c',NE__Filter_Target__c='');
            insert mpo6;
            NE__MapObject__c mpo3 = new NE__MapObject__c(Name = 'PromoOrderItem', NE__Sequence__c=5,NE__Activate_Sort__c= false,NE__Sort_Source__c='', NE__Sort_Operation__c='ASC NULLS FIRST', NE__Source__c='NE__OrderItem__c', NE__Target__c='orderitem__c', NE__Type__c= 'Object',NE__Map__c= mp.Id, NE__Parent__c=mpo.Id, NE__Filter__c='Generate_Asset_Item__c = true', NE__Activate_Filter__c=true, NE__Filter_Operation__c= '=:', NE__Filter_Source__c='createddate',NE__Filter_Target__c='23/11/2016 17:12:10');
            insert mpo3;
            NE__MapObject__c mpo4 = new NE__MapObject__c(Name = 'OrderItem', NE__Sequence__c=3,NE__Activate_Sort__c= false,NE__Sort_Source__c='id', NE__Sort_Operation__c='ASC NULLS FIRST', NE__Source__c='NE__OrderItem__c', NE__Target__c='orderitem__c', NE__Type__c= 'Object',NE__Map__c= mp.Id, NE__Parent__c=mpo.Id, NE__Filter__c='Generate_Asset_Item__c = true', NE__Activate_Filter__c=true, NE__Filter_Operation__c= '=:', NE__Filter_Source__c='BI_Probabilidad_exito__c',NE__Filter_Target__c='5');
            insert mpo4;
            NE__MapObject__c mpo5 = new NE__MapObject__c(Name = 'PromoOrderItem3', NE__Sequence__c=2,NE__Activate_Sort__c= false,NE__Sort_Source__c='', NE__Sort_Operation__c='ASC NULLS FIRST', NE__Source__c='NE__OrderItem__c', NE__Target__c='orderitem__c', NE__Type__c= 'Object',NE__Map__c= mp.Id, NE__Parent__c=mpo.Id, NE__Filter__c='Generate_Asset_Item__c = true', NE__Activate_Filter__c=true, NE__Filter_Operation__c= '=:', NE__Filter_Source__c='BI_Probabilidad_exito__c',NE__Filter_Target__c='5');
            insert mpo5;


            List<NE__MapObjectItem__c> lst_moi = new List<NE__MapObjectItem__c>();

            NE__MapObjectItem__c mapObjItem1 = new NE__MapObjectItem__c(NE__Type__c = 'Field',NE__SourceField__c = 'BI_O4_OPEX_no_recurrente_total__c',NE__RecordTypeId__c = '',NE__Map_Object__c = mpo.Id,NE__MapAsTag__c = false,NE__TargetField__c = 'BI_O4_OPEX_no_recurrente_total__c',NE__QueryOnly__c = false,NE__IsDateField__c = false,NE__Relationship__c = '');
            lst_moi.add(mapObjItem1);
            NE__MapObjectItem__c mapObjItem2 = new NE__MapObjectItem__c(NE__Type__c = 'Value',NE__SourceField__c = '',NE__RecordTypeId__c = '',NE__Map_Object__c = mpo.Id,NE__MapAsTag__c = false,NE__TargetField__c = 'NE__OrderStatus__c',NE__Value__c='Active',NE__QueryOnly__c = false,NE__IsDateField__c = false,NE__Relationship__c = '');
            lst_moi.add(mapObjItem2);
            NE__MapObjectItem__c mapObjItem3 = new NE__MapObjectItem__c(NE__Type__c = 'UpdateKey',NE__SourceField__c = 'NE__Country__c',NE__RecordTypeId__c = '',NE__Map_Object__c = mpo.Id,NE__MapAsTag__c = false,NE__TargetField__c = 'NE__Country__c',NE__Value__c='',NE__QueryOnly__c = false,NE__IsDateField__c = false,NE__Relationship__c = '');
            lst_moi.add(mapObjItem3);
             NE__MapObjectItem__c mapObjItem4 = new NE__MapObjectItem__c(NE__Type__c = 'Field',NE__SourceField__c = 'NE__OneTimeFeeOv__c',NE__RecordTypeId__c = '',NE__Map_Object__c = mpo2.Id,NE__MapAsTag__c = false,NE__TargetField__c = 'onetimefeeov__c',NE__QueryOnly__c = false,NE__IsDateField__c = false,NE__Relationship__c = '');
            lst_moi.add(mapObjItem4);
            NE__MapObjectItem__c mapObjItem5 = new NE__MapObjectItem__c(NE__Type__c = 'Value',NE__SourceField__c = '',NE__RecordTypeId__c = '',NE__Map_Object__c = mpo2.Id,NE__MapAsTag__c = false,NE__TargetField__c = 'NE__OrderStatus__c',NE__Value__c='Active',NE__QueryOnly__c = false,NE__IsDateField__c = false,NE__Relationship__c = '');
            lst_moi.add(mapObjItem5);
            NE__MapObjectItem__c mapObjItem6 = new NE__MapObjectItem__c(NE__Type__c = 'Keyfield',NE__SourceField__c = 'NE__OrderId__c',NE__RecordTypeId__c = '',NE__Map_Object__c = mpo2.Id,NE__MapAsTag__c = false,NE__TargetField__c = 'NE__OrderId__c',NE__Value__c='',NE__QueryOnly__c = false,NE__IsDateField__c = false,NE__Relationship__c = '');
            lst_moi.add(mapObjItem6);
            NE__MapObjectItem__c mapObjItem7 = new NE__MapObjectItem__c(NE__Type__c = 'Parent',NE__SourceField__c = 'NE__OrderId__c',NE__RecordTypeId__c = '',NE__Map_Object__c = mpo2.Id,NE__MapAsTag__c = false,NE__TargetField__c = 'NE__OrderId__c',NE__Value__c='',NE__QueryOnly__c = false,NE__IsDateField__c = false,NE__Relationship__c = '');
            lst_moi.add(mapObjItem7);
            NE__MapObjectItem__c mapObjItem8 = new NE__MapObjectItem__c(NE__Type__c = 'UpdateKey',NE__SourceField__c = 'NE__AssetItemEnterpriseId__c',NE__RecordTypeId__c = '',NE__Map_Object__c = mpo3.Id,NE__MapAsTag__c = false,NE__TargetField__c = 'id',NE__Value__c='',NE__QueryOnly__c = false,NE__IsDateField__c = false,NE__Relationship__c = '');
            lst_moi.add(mapObjItem8);
            NE__MapObjectItem__c mapObjItem9 = new NE__MapObjectItem__c(NE__Type__c = 'RecordType',NE__SourceField__c = '',NE__RecordTypeId__c = '012E0000000aUYJIA2',NE__Map_Object__c = mpo3.Id,NE__MapAsTag__c = false,NE__TargetField__c = 'recordtypeid',NE__Value__c='Order',NE__QueryOnly__c = false,NE__IsDateField__c = false,NE__Relationship__c = '');
            lst_moi.add(mapObjItem9);
            insert lst_moi;
            dmc.GenerateMapObjects(mapName, sourceId);

            DataMap_Copy.DataMapRequest dRe1 = new DataMap_Copy.DataMapRequest();
            dRe1.mapName = 'Order2Order';
            dRe1.sourceId = testOi.Id;
            listOfph.add(processCode);
            listOfph.add(processCode);
            listOfph.add(processCode);
            listOfph.add(processCode);

            mapOfq.put('Order',customQuery);
            mapOfq.put('NoPromoOrderItem',customQuery);
            mapOfq.put('PromoOrderItem',customQuery);
            mapOfq.put('OrderItem',customQuery);
            dRe1.mapOfCustomQueries = mapOfq;
            dRe1.listOfPlaceHolders = listOfph;
            dRe1.updateActive= true;
            System.debug('Lista listOfPlaceHolders: '+ listOfph);

            dmc.callDataMap(dRe1);
            //sourceId = testOi.Id;
            //dmc.GenerateMapObjects(mapName, sourceId);

  }

    public static String generateRandomNumber() {
        String randomNumber = generate();

        if (randomNumber.length() < 10) {
        String randomNumber2 = generate();
            randomNumber = randomNumber + randomNumber2.substring(0, 10 - randomNumber.length());
        }
        
        return randomNumber;
    }
    
    
    private static String generate() {
        return String.valueOf(Math.abs(Crypto.getRandomInteger()));
    }


}