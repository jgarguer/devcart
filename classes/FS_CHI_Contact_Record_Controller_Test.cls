/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:							Paloma De La Lastra Pinacho
Company:						Everis España
Description:					Clase de prueba: FS_CHI_Contact_Record_Controller

History:
<Date>							<Author>						<Change Description>
07/06/2017						Paloma De La Lastra Pinacho			Versión inicial

---------------------------------------------------------------------------------------*/
@isTest
private class FS_CHI_Contact_Record_Controller_Test {
    
    @testSetup 
    private static void init() {
      
 		Account account = new Account(Name = 'Test [Integración] - Chile', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20552003609', BI_Country__c = 'Chile');
 		insert account;
    }
    
    @isTest private static void createContact() {
        /* Test */
        System.Test.startTest();
        
        Id accountId = [Select Id from Account limit 1][0].Id;
        try{
            System.assert([SELECT Count() FROM Contact] == 0);
            FS_CHI_Contact_Record_Controller.createContact('General','Institucional',true, 'RUT', '20552003609', '205520036', 'Test [Integración]','Contacto','test.integracion@prueba.everis.csa', 'Jose Donoso', '2', 'Madrid','Madrid', accountId, 'CLP',true,'654321987','654987321');
            System.assert([SELECT Count() FROM Contact] == 1);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
    
    @isTest private static void reintentar() {
        /* Setup */
        Contact contact = new Contact(BI_Tipo_de_documento__c = 'RUT', BI_Numero_de_documento__c = '20552003609', FS_CHI_Numero_Serie__c = '205520036' , FirstName = 'Test [Integración]', LastName ='Contacto', Email = 'test.integracion@prueba.everis.csa', CurrencyIsoCode ='CLP', BI_Country__c ='Chile', accountId= [Select Id from Account limit 1][0].Id, Phone='654321987',MobilePhone='654987321');
        insert contact;
         FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba';
        insert config_integracion;
        /* Test */
        //System.Test.startTest();
        
        try{
            FS_CHI_Contact_Record_Controller.reintentar(contact.Id);
            System.assertEquals('Validado', [SELECT FS_CHI_Estado_Validacion__c FROM Contact LIMIT 1][0].FS_CHI_Estado_Validacion__c);
       }catch(Exception exc){
           System.assert(false);
       }
        
        //System.Test.stopTest();
    }
    
    @isTest private static void reintent() {
        /* Setup */
        Contact contact = new Contact(BI_Tipo_de_documento__c = 'RUT', BI_Numero_de_documento__c = '20552003609', FS_CHI_Numero_Serie__c = '205520036' , FirstName = 'Test2 [Integración]', LastName ='Contacto', Email = 'test.integracion@prueba.everis.csa', CurrencyIsoCode ='CLP', BI_Country__c ='Chile', BI_Tipo_de_contacto__c =  'Administrativo', accountId= [Select Id from Account limit 1][0].Id, Phone='654321987',MobilePhone='654987321');
        insert contact;
         FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba2';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba2';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba2';
        insert config_integracion;
        /* Test */
        //System.Test.startTest();
        
        try{
            FS_CHI_Contact_Record_Controller.reintentarL(contact.Id);
            System.assertEquals('Validado', [SELECT FS_CHI_Estado_Validacion__c FROM Contact LIMIT 1][0].FS_CHI_Estado_Validacion__c);
       }catch(Exception exc){
           System.assert(false);
       }
        
        //System.Test.stopTest();
    }
    
    @isTest private static void reintent2() {
        /* Setup */
        Contact contact = new Contact(BI_Tipo_de_documento__c = 'RUT', BI_Numero_de_documento__c = '20552003609', FS_CHI_Numero_Serie__c = '205520036' , FirstName = 'Test3 [Integración]', LastName ='Contacto', Email = 'test.integracion@prueba.everis.csa', CurrencyIsoCode ='CLP', BI_Country__c ='Chile', accountId= [Select Id from Account limit 1][0].Id, Phone='654321987',MobilePhone='654987321');
        insert contact;
         FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba3';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba3';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba3';
        insert config_integracion;
        /* Test */
        //System.Test.startTest();
        
        try{
            FS_CHI_Contact_Record_Controller.reintentarL(contact.Id);
            System.assertEquals('No aplica', [SELECT FS_CHI_Estado_Validacion__c FROM Contact LIMIT 1][0].FS_CHI_Estado_Validacion__c);
       }catch(Exception exc){
           System.assert(false);
       }
        
        //System.Test.stopTest();
    }
    
    @isTest private static void reintent3() {
        /* Setup */
        Contact contact = new Contact(FS_CHI_Estado_Validacion__c = 'Validado', BI_Tipo_de_documento__c = 'RUT', BI_Numero_de_documento__c = '20552003609', FirstName = 'Test4 [Integración]', LastName ='Contacto', Email = 'test.integracion@prueba.everis.csa', CurrencyIsoCode ='CLP', BI_Country__c ='Chile', BI_Tipo_de_contacto__c =  'Administrativo', accountId= [Select Id from Account limit 1][0].Id, Phone='654321987',MobilePhone='654987321');
        insert contact;
         FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba4';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba4';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba4';
        insert config_integracion;
        /* Test */
        //System.Test.startTest();
        
        try{
            FS_CHI_Contact_Record_Controller.doInitController(contact.Id);
            System.assertEquals('Validado', [SELECT FS_CHI_Estado_Validacion__c FROM Contact LIMIT 1][0].FS_CHI_Estado_Validacion__c);
       }catch(Exception exc){
           System.assert(false);
       }
        
        //System.Test.stopTest();
    }
    
    @isTest private static void reintent4() {
        /* Setup */
        Contact contact = new Contact(BI_Tipo_de_documento__c = 'RUT', BI_Numero_de_documento__c = '120047787', FS_CHI_Numero_Serie__c = '205520036' , FirstName = 'Test17 [Integración]', LastName ='Contacto', Email = 'test.integracion@prueba.everis.csa', CurrencyIsoCode ='CLP', BI_Country__c ='Chile', BI_Tipo_de_contacto__c =  'Administrativo', accountId= [Select Id from Account limit 1][0].Id, Phone='654321987',MobilePhone='654987321');
        insert contact;
         FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba5';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba5';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba5';
        insert config_integracion;
        /* Test */
        //System.Test.startTest();
        
        try{
            FS_CHI_Contact_Record_Controller.reintentarL(contact.Id);
            System.assertEquals('No validado', [SELECT FS_CHI_Estado_Validacion__c FROM Contact LIMIT 1][0].FS_CHI_Estado_Validacion__c);
       }catch(Exception exc){
           System.assert(false);
       }
        
        //System.Test.stopTest();
    }
    
    @isTest private static void reintent5() {
        /* Setup */
        Contact contact = new Contact(BI_Tipo_de_documento__c = 'RUT', BI_Numero_de_documento__c = '91600307', FS_CHI_Numero_Serie__c = '205520036' , FirstName = 'Test18 [Integración]', LastName ='Contacto', Email = 'test.integracion@prueba.everis.csa', CurrencyIsoCode ='CLP', BI_Country__c ='Chile', BI_Tipo_de_contacto__c =  'Administrativo', accountId= [Select Id from Account limit 1][0].Id, Phone='654321987',MobilePhone='654987321');
        insert contact;
         FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba6';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba6';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba6';
        insert config_integracion;
        /* Test */
        //System.Test.startTest();
        
        try{
            FS_CHI_Contact_Record_Controller.reintentarL(contact.Id);
            System.assertEquals('No validado', [SELECT FS_CHI_Estado_Validacion__c FROM Contact LIMIT 1][0].FS_CHI_Estado_Validacion__c);
       }catch(Exception exc){
           System.assert(false);
       }
        
        //System.Test.stopTest();
    }
    
    @isTest private static void reintent6() {
        /* Setup */
        Contact contact = new Contact(BI_Tipo_de_documento__c = 'RUT', BI_Numero_de_documento__c = '20552003609', FS_CHI_Numero_Serie__c = '205520036' , FirstName = 'Test2 [Integración]', LastName ='Contacto', Email = 'test.integracion@prueba.everis.csa', CurrencyIsoCode ='CLP', BI_Country__c ='', BI_Tipo_de_contacto__c =  'Administrativo', accountId= [Select Id from Account limit 1][0].Id, Phone='654321987',MobilePhone='654987321');
        insert contact;
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba2';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba2';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba2';
        insert config_integracion;
        /* Test */
        //System.Test.startTest();
        
        try{
            FS_CHI_Contact_Record_Controller.reintentarL(contact.Id);
            System.assertEquals('Validado', [SELECT FS_CHI_Estado_Validacion__c FROM Contact LIMIT 1][0].FS_CHI_Estado_Validacion__c);
       }catch(Exception exc){
           System.assert(false);
       }
        
        //System.Test.stopTest();
    }
    
    @isTest private static void validateContact() { 
        /* Test */
        FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba3';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba3';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba3';
        insert config_integracion;
        
        System.Test.startTest();        
        //try{ System.assertEquals(JSON.serialize((BI_RestWrapper.ContactsListType) JSON.deserialize('{"contacts":[{"contactDetails":{"name":{"givenName":"Agustín","familyName":"González"},"addresses":[{"addressName":"Av. Providencia","addressNumber":{"value": "111"},"locality":"Santiago","region":"Metropolitana"}]}}],"totalResults":1}', BI_RestWrapper.ContactsListType.class)).length(), FS_CHI_Contact_Record_Controller.validateContact('RUT', '20552003609', '205520036').length());
        try{  
            string resultExpected = '{"totalResults":1,"errorDescription":null,"contacts":[{"contactId":null,"contactDetails":{"roles":null,"personal":null,"name":{"title":null,"suffix":null,"middleName":null,"givenName":"Agustín","fullName":null,"familyName":"González","displayName":null},"legalId":null,"customers":null,"correlatorId":null,"contactStatus":null,"contactingModes":null,"addresses":[{"region":"Metropolitana","postalCode":null,"normalized":null,"municipality":null,"locality":"Santiago","isDangerous":null,"floor":null,"country":null,"coordinates":null,"area":null,"apartment":null,"addressType":null,"addressNumber":{"value":"111","range":null},"addressName":"Av. Providencia","addressExtension":null,"additionalData":null}],"additionalData":null,"accounts":null}}]}';
            System.assertEquals(resultExpected.length(), FS_CHI_Contact_Record_Controller.validateContact('RUT', '20552003609', '205520036').length());
    	}catch(Exception exc){
            system.assert(false);
        }
        
        System.Test.stopTest();        
        
    }
    
     @isTest private static void retrieveInfo() { 
        /* Test */
        System.Test.startTest();
        
        try{ FS_CHI_Contact_Record_Controller.retrieveInfo('Telefónica');
        }catch(Exception exc){        
            System.Test.stopTest();
        }      
     }
    
    @isTest private static void retrieveInfo2() { 
        /* Test */
        System.Test.startTest();
        
        try{ FS_CHI_Contact_Record_Controller.retrieveInfo('');
            FS_CHI_Contact_Record_Controller.retrieveInfo('Telefonicaaaaaa');
        }catch(Exception exc){        
            System.Test.stopTest();
        }      
     }
    
    @isTest private static void reintentWrongCountry() {
        /* Setup */
        Account account = new Account(Name = 'Test [Integración] - Spain', BI_Tipo_de_identificador_fiscal__c = 'RUC', BI_No_Identificador_fiscal__c = '20552003609', BI_Country__c = 'Spain');
 		insert account;
        Contact contact = new Contact(BI_Tipo_de_documento__c = 'RUT', BI_Numero_de_documento__c = '20552003609', FS_CHI_Numero_Serie__c = '205520036' , FirstName = 'Test2 [Integración]', LastName ='Contacto', Email = 'test.integracion@prueba.everis.csa', CurrencyIsoCode ='CLP', BI_Country__c ='Spain', BI_Tipo_de_contacto__c =  'Administrativo', accountId= [Select Id from Account WHERE Name LIKE 'Test [Integración] - Spain'  limit 1][0].Id, Phone='654321987',MobilePhone='654987321');
        insert contact;
         FS_CHI_Integraciones__c config_integracion = new FS_CHI_Integraciones__c();
        config_integracion.FS_CHI_ApiKey__c = 'prueba2';
        config_integracion.FS_CHI_AuthorizationID__c = 'prueba2';
        config_integracion.FS_CHI_creditTypeMap__c = 'prueba2';
        insert config_integracion;
        /* Test */
        //System.Test.startTest();
        
        try{
            FS_CHI_Contact_Record_Controller.reintentarL(contact.Id);
            System.assertEquals('No aplica', [SELECT FS_CHI_Estado_Validacion__c FROM Contact LIMIT 1][0].FS_CHI_Estado_Validacion__c);
       }catch(Exception exc){
           System.assert(false);
       }
        
        //System.Test.stopTest();
    }
    
    @isTest private static void getSelectOptionsContact() {
        /* Test */
        Contact contTest = new Contact();
        string fldTest = 'BI_COL_Rol__c';
        System.Test.startTest();
        
        try{
            List <String> listTest= FS_CHI_Contact_Record_Controller.getSelectOptions(contTest,fldTest);
            integer sizeListTest = listTest.size();
            System.assert(sizeListTest>0);
        }catch(Exception exc){
            System.assert(false);
        }
        
        System.Test.stopTest();
    }
}