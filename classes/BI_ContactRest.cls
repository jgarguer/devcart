@RestResource(urlMapping='/accountresources/v1/contacts/*')
global class BI_ContactRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Contact Rest WebServices.
    
    History:
    
    <Date>            <Author>          <Description>
    20/07/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains basic information stored in the server for a specific contact.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.ContactDetailsType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    20/07/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.ContactType getContact() {
		
		BI_RestWrapper.ContactType res;
		
		try{
			
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
			
			if(RestContext.request.headers.get('countryISO') == null){
					
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
			
			}else{
					
				//ONE OPPORTUNITY
				res = BI_RestHelper.getOneContact(String.valueOf(RestContext.request.requestURI).split('/')[4], RestContext.request.headers.get('countryISO'),
												  RestContext.request.headers.get('systemName'));
				
				RestContext.response.statuscode = (res == null)?404:200;//404 NOT_FOUND; 200 OK
					
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_ContactRest.getContact', 'BI_EN', Exc, 'Web Service');
			
		}
		
		return res;
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Modifies an existing contact in the server receiving the request.
    
    IN:            RestContext.request
    OUT:           RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    20/07/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpPut
	global static void updateContact() {
		
		try{
			
			String idContact;
				
			if(RestContext.request.headers.get('countryISO') == null){
							
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);			
							
			}else{
				
				idContact = RestContext.request.requestURI.split('/')[4];
				idContact += ' '+RestContext.request.headers.get('countryISO');
				if(RestContext.request.headers.get('systemName') != null)
					idContact += ' '+RestContext.request.headers.get('systemName');
				
				List<Contact> lst_con = [select Id from Contact where BI_Validador_Id_de_legado__c = :idContact limit 1];
		
				if(!lst_con.isEmpty()){	
					
					RestRequest req = RestContext.request;
		
					Blob blob_json = req.requestBody; 
		
					String string_json = blob_json.toString(); 
					
					BI_RestWrapper.ContactDetailsType conDetType = (BI_RestWrapper.ContactDetailsType) JSON.deserialize(string_json, BI_RestWrapper.ContactDetailsType.class);
					
					Contact con = lst_con[0];
					
					con = BI_RestHelper.generateContact(con, conDetType, RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'), null);
					
					if(con == null){
						
						RestContext.response.statuscode = 404;//NOT_FOUND
						RestContext.response.headers.put('errorMessage', Label.BI_AccountOrCustomerNotFound);
						
					}else{
						
						update con;
						RestContext.response.statuscode = 200;//OK
						
					}
					
				}else{
					
					RestContext.response.statuscode = 404;//NOT_FOUND
					
				}
				
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_ContactRest.updateContact', 'BI_EN', Exc, 'Web Service');
			
		}
		
	}
	
}