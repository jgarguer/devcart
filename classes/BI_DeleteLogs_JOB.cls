global class BI_DeleteLogs_JOB implements Database.Batchable<sObject>, Schedulable {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Job that delete the BI_Log__c records.
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    09/03/2016                      Guillermo Muñoz             Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	String query;
	final Date FLAG_DATE;
	
	global BI_DeleteLogs_JOB() {
		if(!Test.isRunningTest()){
			FLAG_DATE =Date.today().addMonths(-3);
		}
		else{
			FLAG_DATE =Date.today().addDays(1);
		}
		query = 'SELECT Id FROM BI_Log__c WHERE (BI_Asunto__c != null OR BI_Delete__c = true) AND CreatedDate <: FLAG_DATE';
		System.debug('### query--> ' + query);
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator(query);
	}

	global void execute(SchedulableContext sc){
    	BI_DeleteLogs_JOB obj = new BI_DeleteLogs_JOB();
    	Database.executeBatch(obj,2000);
    }

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
	
   		
   		List <BI_Log__c> lst_logs = scope;
   		System.debug('### scope--> ' + scope.size());
   		if(!lst_logs.isEmpty()){
   			delete lst_logs;
   		}

	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}