@isTest(seeAllData = false)
public class CaseCommentsComponentController_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        José María Martín
    Company:       Deloitte
    Description:   Test method to manage the code coverage for  CaseCommentsComponentController
    
    <Date>                 <Author>               <Change Description>
    11/2015              José María Martín           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void test_CaseCommentsComponentController_TEST() {
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        
        System.runAs(usr)
        {      
            Case caso = new Case();
            insert caso;
            List<CaseComment> comments = new List<CaseComment>();
            comments.add(new CaseComment(IsPublished=false, CommentBody='texto', ParentId=caso.Id));
            comments.add(new CaseComment(IsPublished=true, CommentBody='texto2', ParentId=caso.Id));
            comments.add(new CaseComment(IsPublished=false, CommentBody='texto3', ParentId=caso.Id));
                       
        	insert comments;            
            
            Test.startTest(); 
             
            CaseCommentsComponentController controller = new CaseCommentsComponentController();
            controller.caseId=caso.Id;
            controller.NewComment();
            PageReference p = Page.BIIN_Detalle_Incidencia;
            p.getParameters().put('CommentId_p', comments[2].id);
            Test.setCurrentPageReference(p); 
            controller.deleteComment();
            p.getParameters().put('CommentId_p', comments[1].id);
            Test.setCurrentPageReference(p); 
            controller.makePublicPrivate();
            
            Test.stopTest();
        }
    }
}