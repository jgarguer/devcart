@isTest
private class TGS_ErrorIntegrations_Helper_TEST {

	class TEST_X {
		String teststring = 'TT';
	}

	@isTest static void test_createLogs() {
		try {
			Id.valueOf(null);
		} catch(Exception exc) {
			Test.startTest();
			TGS_ErrorIntegrations_Helper.createExceptionLog('TestWH', 'nullpointer', new BI_Exception(exc), new TEST_X());
			TGS_ErrorIntegrations_Helper.createSuccessLog('TestWH', 'Good', null);
			Test.stopTest();
		}
		System.assertEquals(2, [SELECT Id FROM TGS_Error_Integrations__c].size());
	}

	@isTest static void test_deleteLogs() {
		try {
			Id.valueOf(null);
		} catch(Exception exc) {
			TGS_ErrorIntegrations_Helper.createExceptionLog('TestWH', 'assert', new BI_Exception(exc), new TEST_X());
		}
		System.assertEquals(1, [SELECT Id FROM TGS_Error_Integrations__c].size());
		Test.startTest();
		TGS_ErrorIntegrations_Helper logHelper = new TGS_ErrorIntegrations_Helper();
		System.schedule('XXX...XXX...TEST TGS_ErrorIntegrations_Helper TEST...XXX...XXX', '0 25 2 ? * 6', logHelper);
		Database.executeBatch(logHelper, 2000); // Remove if batch job execution _ends_ before stopTest returns (like with scheduled jobs)
		Test.stopTest();
		System.assertEquals(0, [SELECT Id FROM TGS_Error_Integrations__c].size());
	}

	@isTest static void test_createLogs_future() {
		try {
			Id.valueOf(null);
		} catch(Exception exc) {
			Test.startTest();
			TGS_ErrorIntegrations_Helper.createExceptionLog_future('TestWH', 'nullpointer', TGS_ErrorIntegrations_Helper.exc2json(new BI_Exception(exc)), JSON.serialize(new TEST_X()));
			Test.stopTest();
		}
		System.assertEquals(1, [SELECT Id FROM TGS_Error_Integrations__c].size());
	}

	@isTest static void test_createLogs_queueable() {
		try {
			Id.valueOf(null);
		} catch(Exception exc) {
			Test.startTest();
			System.enqueueJob(new TGS_ErrorIntegrations_Helper.InsertErrorException_Queue(JSON.serialize(new TGS_Error_Integrations__c(TGS_Interface__c='TestWH')), null));
			Test.stopTest();
		}
		System.assertEquals(1, [SELECT Id FROM TGS_Error_Integrations__c].size());
	}

	@isTest static void generateFastLog_TEST(){

		TGS_ErrorIntegrations_Helper.generateFastLog('Test', 'Test', 'Test', 'Test', 'Test', 'Test');

		System.assertEquals(1, [SELECT Id FROM TGS_Error_Integrations__c].size());
		System.assertEquals(1, [SELECT Id FROM Attachment].size());
	}

}