@RestResource(urlMapping='/ticketing/v1/tickets')
global class BI_TicketMultipleRest {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Class for Ticket Rest WebServices.
    
    History:
    
    <Date>            <Author>          										<Description>
    20/08/2014        Pablo Oliva       										Initial version
    28/04/2016        José Luis González										Adapted resource to UNICA standard.
	04/07/2016        José Luis González										Set Case Owner before insert
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Obtains basic information stored in the server for a list of tickets.
    
    IN:            RestContext.request
    OUT:           BI_RestWrapper.TicketsListType structure
    			   RestContext.response
    
    History:
    
    <Date>            <Author>          <Description>
    20/08/2014        Pablo Oliva       Initial version.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@HttpGet
	global static BI_RestWrapper.TicketsListType getMultipleTickets() {
		
		BI_RestWrapper.TicketsListType res;
		
		try{
			
			if(BI_TestUtils.isRunningTest())
				throw new BI_Exception('test');
		
			if(RestContext.request.params.get('accountId') != null || RestContext.request.params.get('customerId') != null || 
			   RestContext.request.params.get('country') != null || RestContext.request.params.get('status') != null || 
			   RestContext.request.params.get('endCreationDate') != null || RestContext.request.params.get('startCreationDate') != null || 
			   RestContext.request.params.get('endLastModificationDate') != null || RestContext.request.params.get('startLastModificationDate') != null ||
			   RestContext.request.params.get('limit') != null || RestContext.request.params.get('offset') != null)
			{
				
				if((RestContext.request.params.get('limit') == null || 
				   (RestContext.request.params.get('limit') != null && 
				    RestContext.request.params.get('limit').isNumeric() && 
				    Integer.valueOf(RestContext.request.params.get('limit')) < 50000 && 
				    Integer.valueOf(RestContext.request.params.get('limit')) > 0
				   )
				  ) 
				  
				  &&
				  
				  (RestContext.request.params.get('offset') == null || 
				   (RestContext.request.params.get('offset') != null && 
				    RestContext.request.params.get('offset').isNumeric() && 
				    Integer.valueOf(RestContext.request.params.get('offset')) < 2001 && 
				    Integer.valueOf(RestContext.request.params.get('offset')) > 0
				   )
				  ))
				{
					
					Datetime sdate;
					Datetime edate;
					
					Datetime sModDate;
					Datetime eModDate;
					
					if(RestContext.request.params.get('startCreationDate') != null)
						sdate = BI_RestHelper.stringToDateTime(RestContext.request.params.get('startCreationDate'));
						
					if(RestContext.request.params.get('endCreationDate') != null)
						edate = BI_RestHelper.stringToDateTime(RestContext.request.params.get('endCreationDate'));
						
					if(RestContext.request.params.get('startLastModificationDate') != null)
						sModDate = BI_RestHelper.stringToDateTime(RestContext.request.params.get('startLastModificationDate'));
						
					if(RestContext.request.params.get('endLastModificationDate') != null)
						eModDate = BI_RestHelper.stringToDateTime(RestContext.request.params.get('endLastModificationDate'));
						
					if((RestContext.request.params.get('startCreationDate') != null && sdate == null) ||
					   (RestContext.request.params.get('endCreationDate') != null && edate == null) || 
					   (RestContext.request.params.get('endLastModificationDate') != null && eModDate == null) || 
					   (RestContext.request.params.get('startLastModificationDate') != null && sModDate == null))
					{
						
						RestContext.response.statuscode = 400;//BAD_REQUEST
						RestContext.response.headers.put('errorMessage', Label.BI_InvalidDate);
						
					}else{
						
						if(RestContext.request.headers.get('countryISO') == null){
								
							RestContext.response.statuscode = 400;//BAD_REQUEST
							RestContext.response.headers.put('errorMessage', Label.BI_MissingCountryISO);
							
						}else{
							
							//MULTIPLE TICKETS
							res = BI_RestHelper.getMultipleTickets(RestContext.request.params.get('accountId'), RestContext.request.params.get('customerId'), 
																   RestContext.request.params.get('country'), RestContext.request.params.get('status'), 
																   RestContext.request.params.get('endCreationDate'), RestContext.request.params.get('startCreationDate'), 
																   RestContext.request.params.get('endLastModificationDate'), RestContext.request.params.get('startLastModificationDate'),
																   RestContext.request.params.get('limit'), RestContext.request.params.get('offset'), 
																   RestContext.request.headers.get('countryISO'), RestContext.request.headers.get('systemName'));
															  	  
							RestContext.response.statuscode = (res == null)?404:200;//404 NOT_FOUND, 200 OK
							
						}
															  
					}
					
				}else{ 
					
					RestContext.response.statuscode = 400;//BAD_REQUEST
					RestContext.response.headers.put('errorMessage', Label.BI_IncorrectLimitOffset);
					
				}
				
			}else{
				
				RestContext.response.statuscode = 400;//BAD_REQUEST
				RestContext.response.headers.put('errorMessage', Label.BI_MissingRequiredParameters);
				
			}
			
		}catch(Exception exc){
			
			RestContext.response.statuscode = 500;//INTERNAL_SERVER_ERROR
			RestContext.response.headers.put('errorMessage',exc.getMessage());
			BI_LogHelper.generate_BILog('BI_TicketMultipleRest.getMultipleTickets', 'BI_EN', exc, 'Web Service');
			
		}
		
		return res;
		
	}
	
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Creates a new ticket in the server receiving the request.
    
    IN:            RestContext.request & BIIN_UNICA_Pojos.TicketRequestType ticketRequest
    OUT:           RestContext.response & BIIN_UNICA_Pojos.TicketDetailType
    
    History:
    
    <Date>            <Author>          	<Description>
    20/08/2014        Pablo Oliva       	Initial version.
    28/04/2016        José Luis González	Adapted resource to UNICA standard.
    28/12/2016		  Adrián Caro Moure 	Add a ticket.CaseNumber in error 'SVC0005'
  --------------------------------------------------------------------------------------------------------------------------------------------------------*/
  @HttpPost  
  global static void doPost(BIIN_UNICA_Pojos.TicketRequestType ticketRequest)
  {
    	
  	system.debug(' BI_TicketMultipleRest.doPost | ticketRequest entrada: ' + JSON.serialize(ticketRequest));
    	
  	RestContext.response.addHeader('Content-Type', 'application/json');
    	
    try 
    {

    	BIIN_UNICA_RestHelper.checkTicketRequestMandatoryFields(ticketRequest);

    	Case ticket = BIIN_UNICA_TicketManager.getTicketByCorrelatorId(ticketRequest.correlatorId);

    	// check ticket id to avoid duplicate tickets
        if(ticket.CaseNumber == null) 
        {
            ticket = BIIN_UNICA_RestHelper.generateTicket(new Case(), ticketRequest);
                
            List<Case> clist = new List<Case>();
            clist.add(ticket);
    
            //BI_CaseMethods.setCaseOwner(clist);
            BI2_CaseMethods.updateOwner(clist, null);
    
            insert ticket;

			String ticketCaseNumber = BIIN_UNICA_TicketManager.getTicketCaseNumber(ticket.BI_Id_del_caso_legado__c);
					
			RestContext.response.statuscode = 201; // CREATED

			if(RestContext.request.headers.get('UNICA-ServiceId') != null
				|| RestContext.request.headers.get('UNICA-ServiceId') != '') 
			{
				RestContext.response.addHeader('UNICA-ServiceId', RestContext.request.headers.get('UNICA-ServiceId'));
			}
			// TODO UNICA set transactionId URL - async call
			RestContext.response.headers.put('Location','https://' + System.URL.getSalesforceBaseURL().getHost() + '/services/apexrest/ticketing/v1/tickets/' + ticketCaseNumber);

			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(BIIN_UNICA_RestHelper.generateTicketDetail(ticket)));
        } 
        else 
        {
        	RestContext.response.statuscode = 400; // BAD_REQUEST
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new BIIN_UNICA_Pojos.ExceptionType2('SVC0005', 'Correlator ' + ticketRequest.correlatorId + ' specified in message part "correlatorId" is a duplicate','CaseNumber: ' + ticket.CaseNumber)));

        }
	} 
	catch(BIIN_UNICA_BadRequestException ex) 
	{
		RestContext.response.statuscode = 400; // BAD REQUEST
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new BIIN_UNICA_Pojos.ExceptionType('SVC1000', ex.getMessage())));
	} 
	catch(Exception ex) 
	{
		RestContext.response.statuscode = 500; // INTERNAL SERVER ERROR
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new BIIN_UNICA_Pojos.ExceptionType('SVC0001', 'Generic Client Error: ' + ex.getMessage())));
	}
  }
}