/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gonzalo Álvaro
    Company:       Deloitte
    Description:   Test class to manage the coverage code for TGS_CaseEditController class
    
    History: 
    <Date>                  <Author>                <Change Description>
    29/05/2015              Gonzalo Álvaro          Initial Version 
    03/05/2015              Gonzalo Álvaro          seeAllDate = false, changed test_TGS_CaseEditController_1
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest(seeAllData = false)

public class TGS_CaseEditController_TEST 
{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gonzalo Álvaro
    Company:       Deloitte
    Description:   Test method to manage the code coverage for TGS_CaseEditController
            
    History: 
    
    <Date>                  <Author>                <Change Description>
    29/05/2015              Gonzalo Álvaro          Initial Version
    03/05/2015              Gonzalo Álvaro          seeAllDate = false and improvements to avoid crashes    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void test_TGS_CaseEditController_1()
    {
        User ActualUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        User adminUser=TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');        
        adminUser.BI_Permisos__c = 'TGS';
        insert adminUser;
        
         /*NE__Product__c testProduct = new NE__Product__c(RecordTypeId = Schema.SObjectType.NE__Product__c.getRecordTypeInfosByName().get(Label.TGS_Standard).getRecordTypeId(),
                                                        Name = 'Test Product');*/
        NE__Product__c testProduct = new NE__Product__c(RecordTypeId = TGS_RecordTypes_Util.getRecordTypeId(NE__Product__c.SObjectType, 'Standard'),
                                                        Name = 'Test Product');
        
        insert testProduct;
        
        TGS_Status_machine__c testStatusMachine = new TGS_Status_machine__c(TGS_Prior_Status__c = Constants.CASE_STATUS_ASSIGNED,
                                                                            TGS_Status__c = 'Test possible next Status',
                                                                            TGS_Status_reason__c = 'Test possible next Status reason',
                                                                            TGS_Service__c = testProduct.Id);
        insert testStatusMachine;
        
        /*Case testCase = new Case(RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RTYPE_ORDER_MNGMNT).getRecordTypeId(),
                                 //TGS_Acting_as__c = Constants.CASE_ACTING_AS_ON_BEHALF,  Eliminacion Acting As
                                 TGS_Service__c = testProduct.Name,
                                 Status = testStatusMachine.TGS_Prior_Status__c,
                                 TGS_Status_reason__c = testStatusMachine.TGS_Prior_Status_reason__c);*/
        
         Case testCase = new Case(RecordTypeId = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, Constants.CASE_RTYPE_DEVNAME_ORDER_MNGMNT),
                                 //TGS_Acting_as__c = Constants.CASE_ACTING_AS_ON_BEHALF,  Eliminacion Acting As
                                 TGS_Service__c = testProduct.Name,
                                 Status = testStatusMachine.TGS_Prior_Status__c,
                                 TGS_Status_reason__c = testStatusMachine.TGS_Prior_Status_reason__c);
        insert testCase;
        
        System.runAs(adminUser)
        {               
            Test.startTest();
            
            PageReference p = Page.TGS_CaseEdit;
            p.getParameters().put('id', testCase.Id);
            Test.setCurrentPageReference(p);
            
            ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(testCase);
            TGS_CaseEditController ceController = new TGS_CaseEditController(controller);
            ceController.getUserAccessToCase();
            ceController.getStatusOptions();
            ceController.getStatusReasonOptions();
            ceController.perfiles();
            
            Test.stopTest();
        }
    }
}