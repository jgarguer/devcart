public without sharing  class CWP_MonitoringCtrl extends PCA_HomeController{
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Micah Burgos
Company:       Aborda
Description:   Controller of service tracking page

History:

<Date>            <Author>              <Description>
26/06/2014        Micah Burgos       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static List<String> AppNames {get;set;}
    public static Map<String,String> AppValues{get;set;}
    public static BI_Contact_Customer_Portal__c usuarioCustomerPortal{
        get{
            try{
                /*if(BI_TestUtils.isRunningTest()){
                    throw new BI_Exception('test');
                }*/
                if(usuarioCustomerPortal==null) {
                    usuarioCustomerPortal = [Select id,BI_BO_Pwd__c,BI_BO_User__c,BI_SW_User__c, BI_SW_Pwd__c, BI_Cliente__r.BI_Country__c from BI_Contact_Customer_Portal__c where BI_User__c=:UserInfo.getUserId() and BI_Cliente__c =: BI_AccountHelper.getCurrentAccountId() LIMIT 1];           
                }
                return usuarioCustomerPortal;
            }catch (exception Exc){
                BI_LogHelper.generate_BILog('PCA_HomeController.usuarioCustomerPortal', 'Portal Platino', Exc, 'Class');
                return null;
            }
        }
        set;
    }
        
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Constructor
    History:
    
    <Date>            <Author>              <Description>
    26/06/2014        Micah Burgos       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/   
    public CWP_MonitoringCtrl(){AppValues = new Map<String,String>();}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Method that performs the Callout to Oracle WS to get the visible Apps for the User Monitoring
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    4/04/2017                       Alberto Pina                Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    public static Map<String,String> invokeApps(){
        if(!Cache.Session.contains('local.CWPexcelExport.AppValues')){
            try{
                system.debug('entrando ws');  
                AppValues  = new Map<String,String>();
                List<List<String>> x = new List<List<String>>();
                User SSOUser = [SELECT FederationIdentifier FROM User WHERE Id = :UserInfo.getUserId()];
                String varAux= (String)SSOUser.FederationIdentifier;
                if(Test.isRunningTest()){
                    List<String> y = new List<String>();
                    List<String> TestApp = new List<String>();
                    y.add('ok');
                    TestApp.add('SalesForce');
                    TestApp.add('www.mockURL.com');
                    x.add(y);
                    x.add(TestApp);
                }else{
                    //if(varAux.contains('@cspad-premad.wh.telefonica')) 
                    x = CWP_Monitoreo_WebService.getApplications(SSOUser.FederationIdentifier);
                    system.debug('lista final:'+x);   
                }
                if(x[0][0].toLowerCase().equals('ok')){
                    for(Integer i = 1; i < x.size(); i++){
                        AppValues.put(x[i][0],x[i][1]);
                    }
                    Cache.Session.put('local.CWPexcelExport.AppValues', AppValues);
                }else{
                    BI_LogHelper.generateLog('PCA_Monitoreo.invokeApps', 'Portal Platino', x[0][0] + ': ' + x[0][1], 'Oracle Callout');
                    System.debug('Error la llamada: '+ '\n'+'codigo de error '+x[0][0] + '\n'+'descripción '+x[0][1]);
                }
            }catch(Exception e){
                BI_LogHelper.generate_BILog('PCA_Monitoreo.invokeApps', 'Portal Platino', e, 'Class');
            }
        }else{
            AppValues = new Map<String, String>();
            AppValues = (Map<String,String>)Cache.Session.get('local.CWPexcelExport.AppValues');
        }              
        System.debug('DBG '+AppNames);
        return AppValues;
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Method to load Monitoring configuration. Lightning.

    History: 

    <Date>                          <Author>                    <Change Description>
    4/04/2017                       Alberto Pina                Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    @auraEnabled
    public static List<BI_Service_Tracking_URL_PP__c> getAplis(){
        usuarioCustomerPortal = [Select id,BI_BO_Pwd__c,BI_BO_User__c,BI_SW_User__c, BI_SW_Pwd__c, BI_Cliente__r.BI_Country__c from BI_Contact_Customer_Portal__c where BI_User__c=:UserInfo.getUserId() and BI_Cliente__c =: BI_AccountHelper.getCurrentAccountId() LIMIT 1];
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            List<BI_Service_Tracking_URL_PP__c> lst_st = new List<BI_Service_Tracking_URL_PP__c>();
            system.debug('entrando método loadStu');         
            
            TGS_User_Org__c p = TGS_User_Org__c.getInstance();
            System.debug(' TGS_User_Org__c p-->'+p );
            
            if(p!=null && p.TGS_Is_TGS__c){
                List<String> privateApps= new List<String>(); 
                AppValues=invokeApps();
                system.debug('lista resultado1:'+AppValues.isEmpty());
                //lst_st=[SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c,CWP_App_DisplayName__c  FROM BI_Service_Tracking_URL_PP__c WHERE (CWP_TGS_Users__c = true AND CWP_isPublic__c = true) OR CWP_App_Name__c IN :AppNames];
                if(!AppValues.keySet().isEmpty() && AppValues  != null )
                    lst_st=[SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c,CWP_App_DisplayName__c  FROM BI_Service_Tracking_URL_PP__c WHERE (CWP_TGS_Users__c = true AND CWP_isPublic__c = true) OR CWP_App_Name__c IN :AppValues.keySet()];
                    //lst_st=[SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c,CWP_App_DisplayName__c  FROM BI_Service_Tracking_URL_PP__c WHERE CWP_App_Name__c IN :AppNames ];
                else
                lst_st=[SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c,CWP_App_DisplayName__c  FROM BI_Service_Tracking_URL_PP__c WHERE CWP_TGS_Users__c = true AND CWP_isPublic__c = true];
                system.debug('lista resultado:'+lst_st);
                 
                if(!lst_st.isEmpty()){
                    for(BI_Service_Tracking_URL_PP__c stu : lst_st){                        
                        if(AppValues.containsKey(stu.CWP_App_Name__c) && AppValues.get(stu.CWP_App_Name__c) != null){
                            stu.BI_URL__c=AppValues.get(stu.CWP_App_Name__c);
                            system.debug('Sobreescribimos url: ' + stu.BI_URL__c);
                        }else{
                            if(stu.BI_URL__c == null) stu.BI_URL__c=Label.TGS_CWP_No_URL;
                        }
                        IF(stu.CWP_App_Name__c==null)   stu.CWP_App_Name__c=Label.TGS_CWP_NO_Name;
                        IF(stu.CWP_Description__c==null)    stu.CWP_Description__c= Label.TGS_CWP_NO_Descr;
                    }
                }
            }else{
                lst_st = [SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c,CWP_App_DisplayName__c  FROM BI_Service_Tracking_URL_PP__c WHERE BI_Country__c =: usuarioCustomerPortal.BI_Cliente__r.BI_Country__c];
                //System.debug('lst_st--<'+lst_st+'>');
            }
            //Query Original: BI_Service_Tracking_URL_PP__c [] lst_stu = [SELECT BI_URL__c,BI_Section__c,CWP_App_DisplayName__c  FROM BI_Service_Tracking_URL_PP__c WHERE BI_Country__c =: usuarioCustomerPortal.BI_Cliente__r.BI_Country__c];
            
            return lst_st;
        }catch (Exception exc){
            BI_LogHelper.generate_BILog('PCA_Monitoreo.loadStu', 'Portal Platino', Exc, 'Class');
            return null;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Method for BI users to load Monitoring configuration. Lightning.

    History: 

    <Date>                          <Author>                    <Change Description>
    4/04/2017                       Alberto Pina                Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/     
    public static list<BI_Service_Tracking_URL_PP__c> getMapaBIEN(){
        list<BI_Service_Tracking_URL_PP__c> lista = new List<BI_Service_Tracking_URL_PP__c>();
        String[] ImageURL = new List<String>();
        String[] Labels = new List<String>();
        String[] Description = new List<String>();
        AppNames = new List<String>{'VisionConsolidada','Top10Desempeno','CalidadDeRed','GestionDeTrafico'};
        StaticResource sr = [SELECT Id, Name, NamespacePrefix, SystemModStamp FROM StaticResource WHERE Name = 'PCA_Template_Resources'];
        String BaseURL = '';
        if(sr != null){
           //Resource URL
           BaseURL = '/empresasplatino/resource/' + sr.SystemModStamp.getTime() + '/' + (sr.NamespacePrefix != null && sr.NamespacePrefix != '' ? sr.NamespacePrefix + '__' : '') + 'PCA_Template_Resources/platino_zoomin/Imagens/Icones/';
           
           BI_Service_Tracking_URL_PP__c aux= new BI_Service_Tracking_URL_PP__c();
           aux.CWP_Logo__c=BaseURL+'Buscar.png';
           aux.CWP_App_Name__c=Label.PCA_Vision_Consolidada;
           aux.CWP_Description__c='Consulte el estado de su red de forma centralizada.';
           aux.BI_URL__c='https://www.clientesestrategicos.tdatabrasil.net.br'; //:447/Padesempenho_network.aspx?cod_usuario='+usuarioSW+'&token='+tokenSW+'&idioma=ES'
           lista.add(aux); 
           BI_Service_Tracking_URL_PP__c aux2= new BI_Service_Tracking_URL_PP__c();
           aux2.CWP_Logo__c=BaseURL+'top10.png';
           aux2.CWP_App_Name__c=Label.PCA_Top_10_Desempeno;
           aux2.CWP_Description__c='Identifique los casos mas relevantes.';
           aux2.BI_URL__c='https://www.clientesestrategicos.tdatabrasil.net.br'; //:447/Pages/desempenho_top10.aspx?cod_usuario='+usuarioSW+'&token='+tokenSW+'&idioma=ES'
           lista.add(aux2);
            BI_Service_Tracking_URL_PP__c aux3= new BI_Service_Tracking_URL_PP__c();
            aux3.CWP_Logo__c=BaseURL+'Analise.png';
            aux3.CWP_App_Name__c=Label.PCA_Calidad_de_Red;
            aux3.CWP_Description__c='Supervise sus rutas y el tráfico de voz.';
            aux3.BI_URL__c='https://www.clientesestrategicos.tdatabrasil.net.br';
            lista.add(aux3);
            BI_Service_Tracking_URL_PP__c aux4= new BI_Service_Tracking_URL_PP__c();
            aux4.CWP_Logo__c=BaseURL+'Gestion.png';
            aux4.CWP_App_Name__c=Label.PCA_Gestion_de_trafico;
            aux4.CWP_Description__c='Conozca el tráfico cursado en tiempo real.';
            aux4.BI_URL__c='https://www.clientesestrategicos.tdatabrasil.net.br';
            lista.add(aux4);
        }
        return lista;
    }
    
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        everis
Company:       everis
Description:   Aux method to get Profile by User Id

History: 

<Date>                          <Author>                    <Change Description>
4/04/2017                       everis                       Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Profile getProfile()
    {   
        Profile p = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        return p;
    }
    
    @auraEnabled
    public static Boolean getTgs(){
        TGS_User_Org__c p = TGS_User_Org__c.getInstance();
        return p.TGS_Is_TGS__c;
    }
    @auraEnabled
    public static List<BI_Service_Tracking_URL_PP__c> getBien(){
        //String url = 'KO';
        List<BI_Service_Tracking_URL_PP__c> lst_st = new List<BI_Service_Tracking_URL_PP__c>(); 
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            lst_st = [SELECT BI_URL__c,BI_Section__c, CWP_Description__c, CWP_Logo__c, CWP_App_Name__c,CWP_App_DisplayName__c  FROM BI_Service_Tracking_URL_PP__c 
                      WHERE BI_Country__c =: usuarioCustomerPortal.BI_Cliente__r.BI_Country__c AND 
                      (CWP_App_Name__c='Consolidated View' OR
                       CWP_App_Name__c='Top Performance' OR
                       CWP_App_Name__c='Network Quality' OR
                       CWP_App_Name__c='Traffic Management' )];
            
            if(!lst_st.isEmpty()){
                    for(BI_Service_Tracking_URL_PP__c stu:lst_st){
                        system.debug('consolidado lista:'+stu.CWP_App_Name__c);
                        if(stu.CWP_App_Name__c=='Consolidated View'){
                            if(usuarioCustomerPortal.BI_SW_User__c != null && usuarioCustomerPortal.BI_SW_Pwd__c != null){
                                String url = stu.BI_URL__c;
                                url = url.replace('{user}', usuarioCustomerPortal.BI_SW_User__c);
                                stu.BI_URL__c = url.replace('{pass}', usuarioCustomerPortal.BI_SW_Pwd__c);
                            }
                        }   
                        if(stu.CWP_App_Name__c=='Top Performance'){
                            if(usuarioCustomerPortal.BI_SW_User__c != null && usuarioCustomerPortal.BI_SW_Pwd__c != null){
                                String url = stu.BI_URL__c;
                                url = url.replace('{user}', usuarioCustomerPortal.BI_SW_User__c);
                                stu.BI_URL__c = url.replace('{pass}', usuarioCustomerPortal.BI_SW_Pwd__c);
                            }
                        }
                        
                        if(stu.CWP_App_Name__c=='Network Quality'){
                            if(usuarioCustomerPortal.BI_SW_User__c != null && usuarioCustomerPortal.BI_SW_Pwd__c != null){
                                String url = stu.BI_URL__c;
                                url = url.replace('{user}', usuarioCustomerPortal.BI_SW_User__c);
                                stu.BI_URL__c = url.replace('{pass}', usuarioCustomerPortal.BI_SW_Pwd__c);
                            }
                        }
                        
                        if(stu.CWP_App_Name__c=='Traffic Management'){
                            if(usuarioCustomerPortal.BI_SW_User__c != null && usuarioCustomerPortal.BI_SW_Pwd__c != null){
                                String url = stu.BI_URL__c;
                                url = url.replace('{user}', usuarioCustomerPortal.BI_SW_User__c);
                                stu.BI_URL__c = url.replace('{pass}', usuarioCustomerPortal.BI_SW_Pwd__c);
                            }
                        }
                    }
            } 
        }catch (Exception exc){
            BI_LogHelper.generate_BILog('PCA_Monitoreo.getUrltrafficManagement', 'Portal Platino', Exc, 'Class');
        }
        return lst_st;
    }
    
}