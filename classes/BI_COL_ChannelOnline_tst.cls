/**
* Avanxo Colombia
* @author           Dolly Fierro href=<dfierro@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Test Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-08-13      Dolly Fierro (DF)     					New Test Class
*					20/09/2017      Angel F. Santaliestra					Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest
private class BI_COL_ChannelOnline_tst 
{
	public static Account objCuentaPrincipal;
	public static Account objCuenta;
	public static Contact objContact;
	public static BI_Col_Ciudades__c objCiudad;
	public static User objUsuario;
	public static AccountTeamMember objAccTeamMember;
	
	public static list<Profile> lstPerfil;
	public static list <UserRole> lstRoles;
	public static list<User> lstUsuarios;
	
	public static void crearDatos( boolean blCrearContacto )
	{
		////Perfil
		//lstPerfil = new list <Profile>();
		//lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
		//system.debug('datos Perfil '+lstPerfil);
		////Rol
		//lstRoles = new list <UserRole>();
		//lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];
		//system.debug('datos Rol '+lstRoles);
		////usuarios
		////lstUsuarios = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

		////ObjUsuario
  //      objUsuario = new User();
  //      objUsuario.Alias = 'standt';
  //      objUsuario.Email ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey = '';
  //      objUsuario.LastName ='Testing';
  //      objUsuario.LanguageLocaleKey ='en_US';
  //      objUsuario.LocaleSidKey ='en_US'; 
  //      objUsuario.ProfileId = lstPerfil.get(0).Id;
  //      objUsuario.TimeZoneSidKey ='America/Los_Angeles';
  //      objUsuario.UserName ='pruebas@test.com';
  //      objUsuario.EmailEncodingKey ='UTF-8';
  //      objUsuario.UserRoleId = lstRoles.get(0).Id;
  //      objUsuario.BI_Permisos__c ='Sucursales';
  //      objUsuario.Pais__c='Colombia';
  //      insert objUsuario;

  //      lstUsuarios = new list<User>();
  //      lstUsuarios.add(objUsuario);
		////Ejecutar inserción con un usuario especial
		//System.runAs( lstUsuarios[0] )
		//{
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c = true;
			insert objBibypass;
			//Crear Usuario
			//objUsuario = new User();
			//objUsuario.Alias = 'standt';
			//objUsuario.Email ='pruebas@test.com';
			//objUsuario.EmailEncodingKey = '';
			//objUsuario.LastName ='Testing';
			//objUsuario.LanguageLocaleKey ='en_US';
			//objUsuario.LocaleSidKey ='en_US'; 
			//objUsuario.ProfileId = lstPerfil.get(0).Id;
			//objUsuario.TimeZoneSidKey ='America/Los_Angeles';
			//objUsuario.UserName ='pruebas@test.com';
			//objUsuario.EmailEncodingKey ='UTF-8';
			//objUsuario.UserRoleId = lstRoles.get(0).Id;
			//objUsuario.BI_Permisos__c ='Sucursales';
			//insert objUsuario;
			//Crea Cuenta
			objCuenta = new Account();
			objCuenta.Name = 'prueba';
			objCuenta.BI_Country__c = 'Colombia';
			objCuenta.TGS_Region__c = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c = '';
			objCuenta.CurrencyIsoCode = 'COP';
			objCuenta.BI_No_Identificador_fiscal__c = '1234567890';
			//objCuenta.OwnerId = objUsuario.Id;
			objCuenta.BI_Segment__c                         = 'test';
			objCuenta.BI_Subsegment_Regional__c             = 'test';
			objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			//Team Member
			objAccTeamMember = new AccountTeamMember();
			objAccTeamMember.TeamMemberRole = 'Service Manager';
			objAccTeamMember.UserId = objUsuario.Id;
			//Crea Ciudad
			objCiudad = new BI_Col_Ciudades__c();
			objCiudad.Name = 'BOGOTA DC / CUNDINAMARCA';
			objCiudad.BI_COL_Codigo_DANE__c = '11001000';
			objCiudad.BI_COL_Pais__c = 'Colombia';
			objCiudad.BI_COL_Departamento__c = 'CUNDINAMARCA';
			insert objCiudad;
			//Crea Contacto
			if( blCrearContacto )
			{
				objContact = new Contact();
				objContact.LastName = 'Prueba';
				objContact.FirstName = 'Test';
				objContact.BI_Representante_legal__c = false;
				objContact.Fax = '1234567';
				objContact.Phone = '1234567';
				objContact.MobilePhone = '3102563698';
				objContact.BI_Tipo_de_documento__c = 'CC';
				objContact.BI_Numero_de_documento__c = '85458589658';
				objContact.BI_Relacion_con_Telefonica__c = '12569';
				objContact.BI_Tipo_de_contacto__c = 'Administrador Canal Online';
				objContact.Email = 'email@email.com';
				objContact.BI_COL_Direccion_oficina__c = 'DirOficina';
				objContact.AccountId = objCuenta.Id;
				objContact.BI_Activo__c = true;
				objContact.BI_Country__c = objCiudad.BI_COL_Pais__c;
				objContact.BI_COL_Ciudad_Depto_contacto__c = objCiudad.id;
				objContact.BI_COL_Principal__c = true;
				insert objContact;
			}			
		//}
	}

	public static void crearDatos2()
	{
		////Perfil
		//lstPerfil = new list <Profile>();
		//lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
		//system.debug('datos Perfil '+lstPerfil);
		////Rol
		//lstRoles = new list <UserRole>();
		//lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];
		//system.debug('datos Rol '+lstRoles);
		////perfiles
		//lstPerfil = new List <Profile>();
		//lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
		////usuarios
		//lstUsuarios = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];
		////Ejecutar inserción con un usuario especial
		//System.runAs( lstUsuarios[0] )
		//{
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c = true;
			insert objBibypass;
			////Crear Usuario
			//objUsuario = new User();
			//objUsuario.Alias = 'standt';
			//objUsuario.Email ='pruebas@test.com';
			//objUsuario.EmailEncodingKey = '';
			//objUsuario.LastName ='Testing';
			//objUsuario.LanguageLocaleKey ='en_US';
			//objUsuario.LocaleSidKey ='en_US'; 
			//objUsuario.ProfileId = lstPerfil.get(0).Id;
			//objUsuario.TimeZoneSidKey ='America/Los_Angeles';
			//objUsuario.UserName ='pruebas@test.com';
			//objUsuario.EmailEncodingKey ='UTF-8';
			//objUsuario.UserRoleId = lstRoles.get(0).Id;
			//objUsuario.BI_Permisos__c ='Sucursales';
			//insert objUsuario;
			//Crea Cuenta Principal
			objCuentaPrincipal = new Account();
			objCuentaPrincipal.Name = 'prueba';
			objCuentaPrincipal.BI_Country__c = 'Colombia';
			objCuentaPrincipal.TGS_Region__c = 'América';
			objCuentaPrincipal.BI_Tipo_de_identificador_fiscal__c = '';
			objCuentaPrincipal.CurrencyIsoCode = 'COP';
			objCuentaPrincipal.BI_No_Identificador_fiscal__c = '1234567891';
			objCuentaPrincipal.OwnerId = objUsuario.Id;
			objCuentaPrincipal.BI_Segment__c                         = 'test';
			objCuentaPrincipal.BI_Subsegment_Regional__c             = 'test';
			objCuentaPrincipal.BI_Territory__c                       = 'test';
			insert objCuentaPrincipal;
			//Crea Cuenta
			objCuenta = new Account();
			objCuenta.ParentId = objCuentaPrincipal.Id;
			objCuenta.Name = 'prueba';
			objCuenta.BI_Country__c = 'Colombia';
			objCuenta.TGS_Region__c = 'América';
			objCuenta.BI_Tipo_de_identificador_fiscal__c = '';
			objCuenta.CurrencyIsoCode = 'COP';
			objCuenta.BI_No_Identificador_fiscal__c = '1234567890';
			objCuenta.OwnerId = objUsuario.Id;
			objCuenta.BI_Segment__c                         = 'test';
			objCuenta.BI_Subsegment_Regional__c             = 'test';
			objCuenta.BI_Territory__c                       = 'test';
			insert objCuenta;
			//Team Member
			objAccTeamMember = new AccountTeamMember();
			objAccTeamMember.TeamMemberRole = 'Service Manager';
			objAccTeamMember.UserId = objUsuario.Id;
			//Crea Ciudad
			objCiudad = new BI_Col_Ciudades__c();
			objCiudad.Name = 'BOGOTA DC / CUNDINAMARCA';
			objCiudad.BI_COL_Codigo_DANE__c = '11001000';
			objCiudad.BI_COL_Pais__c = 'Colombia';
			objCiudad.BI_COL_Departamento__c = 'CUNDINAMARCA';
			insert objCiudad;
			//Crea Contacto
			objContact = new Contact();
			objContact.LastName = 'Prueba';
			objContact.FirstName = 'Test';
			objContact.BI_Representante_legal__c = false;
			objContact.Fax = '1234567';
			objContact.Phone = '1234567';
			objContact.MobilePhone = '3102563698';
			objContact.BI_Tipo_de_documento__c = 'CC';
			objContact.BI_Numero_de_documento__c = '85458589658';
			objContact.BI_Relacion_con_Telefonica__c = '12569';
			objContact.BI_Tipo_de_contacto__c = 'Administrador Canal Online';
			objContact.Email = 'email@email.com';
			objContact.BI_COL_Direccion_oficina__c = 'DirOficina';
			objContact.AccountId = objCuenta.Id;
			objContact.BI_Activo__c = true;
			objContact.BI_Country__c = objCiudad.BI_COL_Pais__c;
			objContact.BI_COL_Ciudad_Depto_contacto__c = objCiudad.id;
			objContact.BI_COL_Principal__c = true;
			insert objContact;
		//}
	}
    static testMethod void myUnitTestOne() 
    {
    	objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
	        crearDatos( true );

	        Test.startTest();

	        System.runAs( objUsuario )
	        {
				objAccTeamMember.AccountId = objCuenta.Id;
				insert objAccTeamMember;
	        }

	        Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );

	        BI_COL_ChannelOnline_cls.insertarContacto(	
	        	objContact.LastName,
				objContact.LastName, 
				objContact.BI_Tipo_de_documento__c, 
				objContact.BI_Numero_de_documento__c, 
				'Cargo', 
				objContact.Email,
				objContact.BI_COL_Direccion_oficina__c, 
				objContact.BI_Country__c, 
				objCiudad.BI_COL_Departamento__c, 
				objCiudad.Name, 
				objContact.Phone, 
				objContact.MobilePhone, 
				'String',
				'1234567890', 
				'Activo', 
				objContact.BI_Tipo_de_contacto__c );
			
			BI_COL_ChannelOnline_cls.insertarContacto(	
	        	objContact.LastName,
				objContact.LastName, 
				objContact.BI_Tipo_de_documento__c, 
				objContact.BI_Numero_de_documento__c, 
				'Cargo', 
				objContact.Email,
				objContact.BI_COL_Direccion_oficina__c, 
				objContact.BI_Country__c, 
				objCiudad.BI_COL_Departamento__c, 
				objCiudad.Name, 
				objContact.Phone, 
				objContact.MobilePhone, 
				'String',
				'', //'1234567890'
				'Activo', 
				objContact.BI_Tipo_de_contacto__c );

			BI_COL_ChannelOnline_cls.insertarContacto(	
	        	objContact.LastName,
				objContact.LastName, 
				objContact.BI_Tipo_de_documento__c, 
				objContact.BI_Numero_de_documento__c, 
				'Cargo', 
				objContact.Email,
				objContact.BI_COL_Direccion_oficina__c, 
				objContact.BI_Country__c, 
				'',//objCiudad.BI_COL_Departamento__c 
				objCiudad.Name, 
				objContact.Phone, 
				objContact.MobilePhone, 
				'String',
				'1234567890',
				'Activo',  
				objContact.BI_Tipo_de_contacto__c );

			BI_COL_ChannelOnline_cls.insertarContacto(	
	        	objContact.LastName,
				objContact.LastName, 
				objContact.BI_Tipo_de_documento__c, 
				objContact.BI_Numero_de_documento__c, 
				'Cargo', 
				objContact.Email,
				objContact.BI_COL_Direccion_oficina__c, 
				objContact.BI_Country__c, 
				objCiudad.BI_COL_Departamento__c ,
				objCiudad.Name, 
				objContact.Phone, 
				objContact.MobilePhone, 
				'String',
				'1234567890',
				'',//objContact.BI_Activo__c 
				objContact.BI_Tipo_de_contacto__c );

			BI_COL_ChannelOnline_cls.insertarContacto(	
	        	objContact.LastName,
				objContact.LastName,//null, 
				null, 
				null, 
				'Cargo', 
				objContact.Email,
				objContact.BI_COL_Direccion_oficina__c, 
				objContact.BI_Country__c, 
				objCiudad.BI_COL_Departamento__c, 
				objCiudad.Name, 
				objContact.Phone, 
				objContact.MobilePhone, 
				'String',
				'1234567890', 
				'Activo',  
				objContact.BI_Tipo_de_contacto__c );

			Test.stopTest();
		}
	}

	
	
	static testMethod void myUnitTestTwo() 
    {
    	objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
	        crearDatos( true );

	        Test.startTest();

	        System.runAs( objUsuario )
	        {
				objAccTeamMember.AccountId = objCuenta.Id;
				insert objAccTeamMember;
	        }

	        Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );

			BI_COL_ChannelOnline_cls.administrarContacto(
				null, 
				objContact.LastName,
				objContact.FirstName, 
				objContact.BI_Tipo_de_documento__c, 
				objContact.BI_Numero_de_documento__c, 
				'Cargo', 
				objContact.Email,
				objContact.BI_COL_Direccion_oficina__c, 
				objContact.BI_Country__c, 
				objCiudad.BI_COL_Departamento__c, 
				objCiudad.Name, 
				objContact.Phone, 
				objContact.MobilePhone, 
				'String',
				'1234567890', 
				'Activo', 
				objContact.BI_Tipo_de_contacto__c);

			BI_COL_ChannelOnline_cls.administrarContacto(
				objContact.Id, 
				objContact.LastName,
				objContact.FirstName, 
				objContact.BI_Tipo_de_documento__c, 
				objContact.BI_Numero_de_documento__c, 
				'Cargo', 
				objContact.Email,
				objContact.BI_COL_Direccion_oficina__c, 
				objContact.BI_Country__c, 
				objCiudad.BI_COL_Departamento__c, 
				objCiudad.Name, 
				objContact.Phone, 
				objContact.MobilePhone, 
				'String',
				'1234567890', 
				'Activo',  
				objContact.BI_Tipo_de_contacto__c);

			BI_COL_ChannelOnline_cls.administrarContacto(
				objContact.Id, 
				objContact.LastName,
				objContact.FirstName, 
				objContact.BI_Tipo_de_documento__c, 
				objContact.BI_Numero_de_documento__c, 
				'Cargo', 
				objContact.Email,
				objContact.BI_COL_Direccion_oficina__c, 
				objContact.BI_Country__c, 
				objCiudad.BI_COL_Departamento__c, 
				objCiudad.Name, 
				objContact.Phone, 
				objContact.MobilePhone, 
				'String',
				'',//'1234567890' 
				'Activo', 
				objContact.BI_Tipo_de_contacto__c);
			
			BI_COL_ChannelOnline_cls.administrarContacto(
				objContact.Id, 
				null,
				objContact.LastName, 
				null, 
				null, 
				'Cargo', 
				objContact.Email,
				null, 
				objContact.BI_Country__c, 
				objCiudad.BI_COL_Departamento__c, 
				objCiudad.Name, 
				objContact.Phone, 
				objContact.MobilePhone, 
				'String',
				'1234567890', 
				'Activo', 
				null);

			Test.stopTest();
		}
    }

    
	static testMethod void myUnitTestThree() 
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			crearDatos( true );

			Test.startTest();

			System.runAs( objUsuario )
			{
				objAccTeamMember.AccountId = objCuenta.Id;
				insert objAccTeamMember;
			}

			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
											        
			BI_COL_ChannelOnline_cls.listarEmpresasService( 'pruebas@test.com' );

			BI_COL_ChannelOnline_cls.listarEmpresasService( 'nodebeexisitr@prueba.test.co' );

			Test.stopTest();
		}
	}

	static testMethod void myUnitTestThree_2()
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			crearDatos( false );

			Test.startTest();

			System.runAs( objUsuario )
			{
				objAccTeamMember.AccountId = objCuenta.Id;
				insert objAccTeamMember;
			}

			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
											        
			BI_COL_ChannelOnline_cls.listarEmpresasService( objUsuario.Email );

			Test.stopTest();
		}
	}
    
    static testMethod void myUnitTestFour() 
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			crearDatos( true );

			Test.startTest();

			System.runAs( objUsuario )
			{
				objAccTeamMember.AccountId = objCuenta.Id;
				insert objAccTeamMember;
			}

			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
											        
			BI_COL_ChannelOnline_cls.contactCanalOnline( '', '' );

			BI_COL_ChannelOnline_cls.contactCanalOnline( objContact.Id, 'novedad x' );

			Test.stopTest();
		}
	}

	static testMethod void myUnitTestFive() 
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			crearDatos2();

			Test.startTest();

			System.runAs( objUsuario )
			{
				objAccTeamMember.AccountId = objCuenta.Id;
				insert objAccTeamMember;
			}

			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
											        
			BI_COL_ChannelOnline_cls.accountCanalOnline( objCuenta.Id, 'novedad y' );

			Test.stopTest();
		}
	}

	static testMethod void myUnitTestSeven() 
	{
		objUsuario = BI_COL_CreateData_tst.getCreateUSer();
        System.runAs(objUsuario) 
        {
			crearDatos( true );

			Test.startTest();
			
			System.runAs( objUsuario )
			{
				objAccTeamMember.AccountId = objCuenta.Id;
				insert objAccTeamMember;
			}

			Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );

			list<User> lstUser = new list<User>();

			lstUser.add( objUsuario );

			BI_COL_ChannelOnline_cls.fnSendUser( lstUser );

			Test.stopTest();
		}
	}
}