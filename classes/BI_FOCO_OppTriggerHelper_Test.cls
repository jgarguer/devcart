@isTest
private class BI_FOCO_OppTriggerHelper_Test {

    @testsetup
    private static void setUpTest(){
        List<BI_FOCO_Dates_by_Country__c> lista = new List<BI_FOCO_Dates_by_Country__c>();
        BI_FOCO_Dates_by_Country__c cf = new BI_FOCO_Dates_by_Country__c();
        cf.Name = 'Argentina';
        cf.Max_Date__c = Date.newInstance(2015,12,31);
        cf.Min_Date__c = Date.newInstance(2014, 1, 1);
        BI_FOCO_Dates_by_Country__c cf1 = new BI_FOCO_Dates_by_Country__c();
        cf1.Name = 'Ecuador';
        cf1.Max_Date__c = Date.newInstance(2015,12,31);
        cf1.Min_Date__c = Date.newInstance(2014, 1, 1);

        lista.add(cf);
        lista.add(cf1);

        insert lista;

        BI_FOCO_Pais__c cfoco1 = new BI_FOCO_Pais__c();
        cfoco1.Name = 'General';
        cfoco1.Notify_Email_List__c = 'testing@testtest.com';
        cfoco1.Opportunity_Probability__c = 50;
        cfoco1.BI_BatchDelete_Size__c = 150;
        BI_FOCO_Pais__c cfoco2 = new BI_FOCO_Pais__c();
        cfoco2.Name = 'Peru';
        cfoco2.Notify_Email_List__c = 'testing@testtest.com';
        cfoco2.Opportunity_Probability__c = 50;
        cfoco2.BI_BatchDelete_Size__c = 150;
        List<BI_FOCO_Pais__c> laLista2 = new List<BI_FOCO_Pais__c>();
        laLista2.add(cfoco2);
        laLista2.add(cfoco1);
        
        insert laLista2;

  

    }
    
    @isTest static void test_method_one() {
		
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
        Date sd = Date.newInstance(2036, 01, 05);
        Date nd = Date.newInstance(2037, 12, 01);
        Date closeDate = sd.addDays( 10 );

        Id rt1, rt2;
        RecordType[] recTypes = [SELECT Id from RecordType where Name in ('Venta nueva ciclo rápido', 'Renegociación ciclo completo')];
        if(recTypes.size()>1){
            rt1 = recTypes[0].id;
            rt2 = recTypes[1].id;

        }
        Decimal prob = 70.0;
        String now = '' + String.valueOf(DateTime.now());

        List<Account>  aList= new list<Account>();
        String pais = 'Argentina';
        //Data Prep
        BI_FOCO_Pais__c cs = new BI_FOCO_Pais__c(Name=pais,Batch_Size__c=10,BI_BatchDelete_Size__c=10, Currency_ISO_Code__c='ARS', Opportunity_Probability__c=50, Notify_Email_List__c='bejoy.babu@salesforce.com'); 
        upsert cs;

        BI_FOCO_Trigger__c cs1 =  new BI_FOCO_Trigger__c(Name = 'Opportunity Insert', On_Off__c=true);
        BI_FOCO_Trigger__c cs2 =  new BI_FOCO_Trigger__c(Name = 'Opportunity Update', On_Off__c=true);
        BI_FOCO_Trigger__c cs3 =  new BI_FOCO_Trigger__c(Name = 'Opportunity Delete', On_Off__c=true);
        //upsert cs1;
        //upsert cs2;
        //upsert cs3;

        

        BI_Periodo__c per = new BI_Periodo__c(BI_Mes_Ano__c='01-2016', Name='Enero 2016');
        upsert per;

        BI_Rama__c rama = new BI_Rama__c(BI_Nombre__c='Testx001');
        upsert rama;

        Account a1 = new Account(Name='Testx001', BI_Country__c  = pais, BI_Validador_Fiscal__c='Testx001',BI_Segment__c = 'test',BI_Subsegment_Regional__c = 'test', BI_Territory__c = 'test');
        aList.add(a1);

        insert aList;

        aList = [Select Id, BI_Validador_Fiscal__c from Account];

        //insert foco records
        BI_Registro_Datos_FOCO__c f = new BI_Registro_Datos_FOCO__c(BI_Cliente__c=aList[0].Id, BI_Monto__c = 100.40, BI_Periodo__c=per.Id, BI_Rama__c=rama.Id, BI_Tipo__c='Backlog (Recurrente)', CurrencyIsoCode='ARS');
        insert f;
        List<BI_Registro_Datos_FOCO__c> fList = [select BI_Acumulado_Este_Ano__c, BI_Cliente__c, BI_Fecha_Inicio_Periodo__c, BI_Monto__c, BI_Nombre_Rama__c, BI_Oportunidad__c, BI_Country__c, BI_Periodo__c, BI_Proximos_12_Meses__c, BI_Rama__c, BI_Tipo__c, CreatedById, CreatedDate, CurrencyIsoCode, Id, IsDeleted, LastModifiedById, LastModifiedDate, Name, SystemModstamp from BI_Registro_Datos_FOCO__c];
        System.debug('------------------------------------------Inserted Foco:'+fList[0]);



        List<Opportunity> ol = new List<Opportunity>();

        //non-renegaociacion rectype
        Opportunity o1 = new Opportunity(AccountId =aList[0].Id,
                                            Name=now+'Test0001x',
                                            CloseDate=closeDate,
                                            Stagename=System.Label.BI_F5DefSolucion,
                                            //RecordTypeId=rt1,
                                            BI_Probabilidad_de_exito__c='75',
                                            Probability=75,
                                            BI_Fecha_de_vigencia__c=closeDate.addDays(2),
                                            CurrencyIsoCode='ARS',
                                            BI_Comienzo_estimado_de_facturacion__c=closeDate.addDays(5),
                                            BI_Duracion_del_contrato_Meses__c=3,
                                            BI_Opportunity_Type__c ='Producto Standard',
                                            BI_Country__c = pais);
        ol.add(o1);

        
        Test.startTest();
        insert ol;

        List<BI_Registro_Datos_FOCO__c> focos = new List<BI_Registro_Datos_FOCO__c>();

        //Query an existing product and clone for the current opportunity
        NE__OrderItem__c prod, prodClone1, prodClone2;
        NE__Order__c  ord, ordClone1, ordClone2; 
        

        List<NE__OrderItem__c> prods = [SELECT BI_Contrato__c, Bys__c, Commissioning_Cost__c, Configuration_SubType__c, Configuration_Type__c, Contacto__c, Contacto_tecnico__c, CreatedById, CreatedDate, Cuenta__c, CurrencyIsoCode, Details__c, Discount_Perc_OneTimeFee__c, Discount_Perc_RecurringCharge__c, Equipo_Chip__c, Equipo_Contratacion__c, Equipo_Cuenta__c, Equipo_Gama__c, Equipo_IMEI__c, Equipo_IMSI__c, Equipo_Modelo__c, Gregorian__c, Id, Inscripcion_LEGADO__c, Installation_point__c, Integration_Id__c, IsDeleted, IsRoot__c, LastModifiedById, LastModifiedDate, Name, NE__Account__c, NE__Action__c, NE__Activation_Date__c, NE__Asset_Item_Account__c, NE__assetItemActionCalc__c, NE__AssetItemEnterpriseCalc__c, NE__AssetItemEnterpriseId__c, NE__assetItemStatusCalc__c, NE__BaseOneTimeFee__c, NE__BaseRecurringCharge__c, NE__Billing_Account__c, NE__Billing_Account_Asset_Item__c, NE__Billing_Profile__c, NE__BillingProfId__c, NE__CatalogItem__c, NE__Commitment__c, NE__Commitment_Expiration_Date__c, NE__Commitment_Period__c, NE__Commitment_UOM__c, NE__Contract__c, NE__Delivery_Date__c, NE__Description__c, NE__DiscountId__c, NE__EndDate__c, NE__FulfilmentStatus__c, NE__Generate_Asset_Item__c, NE__Generate_Asset_Item_Calc__c, NE__Generate_Promo_Asset_Item_Calc__c, NE__IsPromo__c, NE__Multiply_Configuration_Item__c, NE__OneTimeFeeCode__c, NE__OneTimeFeeOv__c, NE__Optional__c, NE__OrderId__c, NE__OrderItemPromoId__c, NE__Parent_Order_Item__c, NE__Penalty__c, NE__Penalty_Activated__c, NE__Penalty_Fee__c, NE__PenaltyImage__c, NE__Picked_By__c, NE__ProdId__c, NE__PromotionId__c, NE__Qty__c, NE__RecurringChargeCode__c, NE__RecurringChargeFrequency__c, NE__RecurringChargeOv__c, NE__Root_Order_Item__c, NE__SerialNum__c, NE__Service_Account__c, NE__Service_Account_Asset_Item__c, NE__Shipped_Qty__c, NE__SmartPart__c, NE__StartDate__c, NE__Status__c, NE__Test_Prezzi__c, NE__UnitCreditCode__c, NE__UnitCreditDescription__c, NE__UsageCode__c, NE__UsageDescription__c, NE_Phone_Number__c, Numero_de_circutos__c, One_Time_Cost__c, Order_Item_Code_Linea_Pedido__c, OrderItemCodeSequential__c, Parque_Ani__c, Parque_CUIT__c, Parque_Nivel__c, Post__c, Process_in_progress__c, processCode__c, RecordTypeId, Recurring_Cost__c, Recurring_Cost_Frequency__c, SVA_Bonif__c, SVA_Bonif_Post__c, SVA_BYS__c, SVA_Cuenta__c, SVA_Gregorian__c, SVA_Meses__c, SVA_Post__c, SystemModstamp, Tax__c FROM NE__OrderItem__c WHERE NE__OrderId__r.NE__OptyId__c != null  and (NE__OrderId__r.NE__OrderStatus__c='Active' or NE__OrderId__r.NE__OrderStatus__c='Active - Rápido') and NE__OneTimeFeeOv__c > 0.0 and NE__RecurringChargeOv__c > 0.0 ORDER BY LastModifiedDate DESC LIMIT 1];
        if(prods !=null && prods.size() >0){
            prod = prods[0];
            List<NE__Order__c> ords = [SELECT Arrearage_Percentage__c, BI_Descuento_bloqueado__c, BI_Id_legado__c, BI_Sistema_legado__c, BI_Validador_Id_de_legado__c, CreatedById, CreatedDate, CurrencyIsoCode, Estado_de_aprovisionamiento__c, Etapa__c, Factor_Annual_Depreciation_Equipment__c, Factor_Annual_Depreciation_Infrastructur__c, Factor_Commission_Security_Family__c, Fecha_de_cierre_del_pedido__c, Id, Income_Tax_Percentage__c, Integration_Id__c, IsDeleted, LastModifiedById, LastModifiedDate, Management_Committee_Percentage__c, Monthly_Discount_Rate__c, Months_Return_of_Investiment__c, Name, NE__AccountId__c, NE__Asset__c, NE__Asset_Account__c, NE__Asset_Configuration__c, NE__AssetEnterpriseCalc__c, NE__AssetEnterpriseId__c, NE__AssetStatusCalc__c, NE__BillAccId__c, NE__Billing_Account_Asset__c, NE__BillingProfId__c, NE__BillingRegion__c, NE__CatalogId__c, NE__CommercialModelId__c, NE__Configuration_SubType__c, NE__Configuration_Type__c, NE__ConfigurationStatus__c, NE__Contract_Account__c, NE__Contract_Header__c, NE__CurrencyCode__c, NE__Delivery_Date__c, NE__Description__c, NE__FulfilmentStatus__c, NE__One_Time_Fee_Total__c, NE__OpportunityId__c, NE__OptyId__c, NE__Order_date__c, NE__OrderStatus__c, NE__Promotion_Code__c, NE__Quote__c, NE__Recurring_Charge_Total__c, NE__SerialNum__c, NE__ServAccId__c, NE__Service_Account_Asset__c, NE__Shipping_Address__c, NE__Shipping_City__c, NE__Shipping_Country__c, NE__Shipping_State_Province__c, NE__Shipping_Street__c, NE__Shipping_Zip_Postal_Code__c, NE__ShippingRegion__c, NE__Sync_oppty__c, NE__TotalRecurringFrequency__c, NE__Type__c, NE__Version__c, Numero_de_lineas__c, Opty_Name__c, OptyAmount__c, Order_Code_Pedido__c, Order_installation_point__c, OrderCodeSequential__c, OwnerId, Parque_Account__c, PayBack__c, PayBack_Status__c, processCode__c, RecordTypeId, SumOfNoProcOI__c, SumOfOI__c, SystemModstamp, Url_Payback__c, Van__c, Van_on_Cost__c FROM NE__Order__c WHERE Id=:prod.NE__OrderId__c];
            if(ords !=null && ords.size() >0 ){
                ord = ords[0];
                ordClone1 = ord.clone(false, true, false, false);
                ordClone1.NE__OptyId__c = o1.Id;
                insert ordClone1;

                prodClone1  = prod.clone(false, true, false, false);
                prodClone1.NE__OrderId__c = ordClone1.Id;
                insert prodClone1;

                System.debug('Adding Product:\n'+prodClone1+' to Opportunity:\n'+o1);

                /*ordClone2 = ord.clone(false, true, false, false);
                ordClone2.NE__OptyId__c = o2.Id;
                insert ordClone2;*/
                
                prodClone2  = prod.clone(false, true, false, false);
                prodClone2.NE__OrderId__c = ordClone2.Id;
                insert prodClone2;
                //System.debug('Adding Product:\n'+prodClone2+' to Opportunity:\n'+o2);

            }
        }
        if(prodClone1 !=null && prodClone1.id !=null){
            //do the assertions
            focos = [SELECT Id, BI_Monto__c, BI_Tipo__c, BI_Rama__r.BI_Nombre__c, BI_Oportunidad__c, BI_Periodo__r.Name, BI_Oportunidad__r.Closedate, CurrencyIsoCode, LastModifiedDate FROM BI_Registro_Datos_FOCO__c WHERE BI_Oportunidad__c = :o1.id];
        }

        //update Opp 
        o1.closeDate = o1.closeDate.addDays(5) ;
        /*o2.closeDate = o2.closeDate.addDays(5) ;*/
        //update ol;
        
        Test.stopTest();

        //Test.startTest();
        //update Opp so that the close date falls out of thr date range
        o1.closeDate = Date.newInstance(2199, 12, 01);
        o1.stageName = Label.BI_F5DefSolucion;
        update o1;
        focos = [Select Id from BI_Registro_Datos_FOCO__c where BI_Oportunidad__c=:o1.Id];
        //System.assertEquals(0, focos.size());

        /*Opportunity o3 = o2.clone(false, false, false, false);
        insert o3;
        delete o3;*/
        //focos = [Select Id from BI_Registro_Datos_FOCO__c where BI_Oportunidad__c=:o2.Id];
        //System.assertEquals(0, focos.size());

        //delete lst_opp1;
        //Test.stopTest();

        BI_FOCO_OppTriggerHelper.notifyByEmail('Test Execution', null, 0, null);
        List<Id> lst_ids = new List<Id>();
        lst_ids.add(o1.Id);
        BI_FOCO_OppTriggerHelper.afterIns(lst_ids);
         BI_FOCO_OppTriggerHelper.afterUpd(lst_ids);
         BI_FOCO_OppTriggerHelper.afterDel(lst_ids);
    }


    @isTest static void test_afterIns() {

      BI_FOCO_OppTriggerHelper.afterIns(null);
    }

    @isTest static void test_afterUpd() {
      BI_FOCO_OppTriggerHelper.afterUpd(null);
    }

    @isTest static void test_afterDel() {
        BI_FOCO_OppTriggerHelper.afterDel(null);
    }

}