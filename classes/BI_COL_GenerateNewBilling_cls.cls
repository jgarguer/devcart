global class BI_COL_GenerateNewBilling_cls implements Database.Batchable<SObject>
{
	public Boolean tst{get;set;}
	
	public BI_COL_GenerateNewBilling_cls(Boolean bndTst) 
	{	
		tst=bndTst;
	}

	global Database.Querylocator start(Database.Batchablecontext bc)
	{
		String query = 'SELECT BI_COL_Fecha_envio__c, BI_COL_MS__c, BI_COL_No_Renovable__c, BI_COL_Procesado__c, BI_COL_Renovable__c, BI_COL_Valor_Cobro__c, Name '+
						' FROM BI_COL_Cobro_Parcial__c c '+
						' where c.BI_COL_Procesado__c=false'+ 
						' and c.BI_COL_Fecha_envio__c=today';
						
		return Database.getQueryLocator(query);
	
	
	}
	
	global void execute(Database.BatchableContext bc, List<BI_COL_Cobro_Parcial__c> scope)
	{
		List<BI_COL_Cobro_Parcial__c> listCPNovedad=scope;
		List<String> idCP = new List<String>();


		for(BI_COL_Cobro_Parcial__c cp:listCPNovedad)
		{
			cp.BI_COL_Fecha_procesado__c=System.today();
			cp.BI_COL_Procesado__c=true;
			idCP.add(cp.ID);
		}
		
		List<BI_Log__c> nFact=[Select n.BI_COL_Modificacion_Servicio__c, n.BI_COL_Estado__c, n.Id, n.BI_COL_Tipo_Transaccion__c,BI_COL_Cobro_Parcial__c
												from BI_Log__c n 
												where n.BI_COL_Cobro_Parcial__c in:idCP and n.BI_COL_Estado__c <> 'Cerrado'];
				
				System.debug('List<Novedad_Fcaturacion_c> nFact = ' + nFact);
				for(BI_Log__c nf:nFact)
				{
					System.debug('Recorriendo la lista nFact');
					nf.BI_COL_Estado__c='Cerrado';
					System.debug('nf.Estado__c cambiado a cerrado Linea 44');
				}
				System.debug('Llega a actualizar nFact');
				update nFact;
		
		if(listCPNovedad.size()>0)
		{
			List<BI_COL_Cobro_Parcial__c> listCPold=new List<BI_COL_Cobro_Parcial__c>();
			System.debug('\n\n A punto de generar la novedad de facturacion de cobro parcial \n\n');
			BI_COL_NoveltyBilling_cls.RegistrarCobrosParciales_ws(listCPNovedad, listCPold, true);
			System.debug('\n\n Finaliza la creacion de Cobros parciales \n\n');
			
			try {update listCPNovedad;}catch(Exception ex){}
		}
		
	}
	

	global void finish(Database.BatchableContext bc)
	{
		System.debug('Entra al finish de DAVOXPLUS_GeneraNovedadesBatch');
		BI_COL_ScheduleSendDavox_cls ia=new BI_COL_ScheduleSendDavox_cls(2);
		DateTime fechaActual= System.now().addMinutes(1);
		Integer minutos=fechaActual.minute();
		Integer hora=fechaActual.hour();
		Integer dia=fechaActual.day();
		integer mes=fechaActual.month();
		Integer anio=fechaActual.year();
		String sch = '0 '+minutos+' '+hora+' '+dia+' '+mes+' ? '+anio;
		if(!tst)
		{
			Id tarea=System.schedule('davox_BatchEnviarADavox: '+System.now(), sch, ia);
		}
	}

}