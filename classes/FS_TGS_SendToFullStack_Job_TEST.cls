/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Accenture - New Energy Aborda
    Description:   Test class to manage the code coverage from FS_TGS_SendToFullStack_Job
    
  
    
    <Date>            <Author>                              <Description>
    02/10/2017		  Guillermo Muñoz						Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class FS_TGS_SendToFullStack_Job_TEST {

	private static Map <Id, Account> map_holdingToProcess;
    private static Map <Id, Account> map_customerCountryToProcess;
    private static Map <Id, Account> map_legalEntityToProcess;
    private static Map <Id, List <Contact>> map_contacts;
    private static Map <Id, List <BI_Punto_de_instalacion__c>> map_sites;

	@testSetup static void insertDate(){

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();

        TGS_Dummy_Test_Data.insertFullStackData();
	}
	
	@isTest static void twoHoldingWithContacts() {

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);
		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//sincronización de holding con contactos
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(map_holdingToProcess, null, null, map_contacts, null, null, null);
		obj.executeBody();
		
		Test.stopTest();
	}

	@isTest static void oneHoldingWithCustomerCountries() {

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);
		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//Dado que el mapa tiene 2 holding, eliminamos uno de ellos
		map_holdingToProcess.remove(new List <Id>(map_holdingToProcess.keySet())[0]);

		//sincronización de holding con customer countries
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, null, null, null, null, null);
		obj.executeBody();
		
		Test.stopTest();
	}

	@isTest static void failedHoldingWithContactsAndSites() {

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);
		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(400, 'BAD_REQUEST', 'TRUE', responseheaders));

		Id accId = new List <Id>(map_holdingToProcess.keySet())[0];
		//Puesto que en el mapa están los contactos y sites de todos los clientes, creamos un auxiliar con solo los del holding que va a fallar
		Map <Id, List <Contact>> map_auxContacts = new Map <Id, List <Contact>>{accId => map_contacts.get(accId)};
		Map <Id, List <BI_Punto_de_instalacion__c>> map_auxSites = new Map <Id, List <BI_Punto_de_instalacion__c>>{accId => map_sites.get(accId)};

		//sincronización de holding con contactos y sites que fallará
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(map_holdingToProcess, null, null, map_auxContacts, map_auxSites, null, null);
		obj.executeBody();
		
		Test.stopTest();
	}

	@isTest static void oneCustomerCountry() {

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);

		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//Rellenamos el identificador externo de su padre para que pase por la condición de sincronización
		Account customerCountry =  map_customerCountryToProcess.values()[0];
		customerCountry.Parent.BI_Identificador_Externo__c = 'Test';
		Map <Id, Account> map_auxCustomerCountry = new Map <Id, Account>{customerCountry.Id => customerCountry};

		//sincronización de customer country
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(null, map_auxCustomerCountry, null, null, null, null, null);
		obj.executeBody();
		
		Test.stopTest();
	}

	@isTest static void oneCustomerCountryWithMapIdParents() {

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);

		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//Rellenamos map_idParents para que no falle la sincronización
		Account customerCountry =  map_customerCountryToProcess.values()[0];
		Map <Id, String> map_idParents = new Map <Id, String>{customerCountry.ParentId => 'Test'};
		Map <Id, Account> map_auxCustomerCountry = new Map <Id, Account>{customerCountry.Id => customerCountry};
		
		//sincronización de customer country
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(null, map_auxCustomerCountry, null, null, null, null, map_idParents);
		obj.executeBody();
		
		Test.stopTest();
	}

	@isTest static void oneLegalEntityWithMapIdParents() {

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);

		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//Rellenamos map_idParents para que no falle la sincronización
		Account legalEntity =  map_LegalEntityToProcess.values()[0];
		Map <Id, String> map_idParents = new Map <Id, String>{legalEntity.ParentId => 'Test'};
		Map <Id, Account> map_auxLegalEntity = new Map <Id, Account>{LegalEntity.Id => legalEntity};
		
		//sincronización de legal entity
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(null, null, map_auxLegalEntity, null, null, null, map_idParents);
		obj.executeBody();
		
		Test.stopTest();
	}

	@isTest static void allContacts(){

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);

		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//Rellenamos map_idParents para que no falle la sincronización
		Account acc = new Account(Id = map_contacts.values()[0][0].AccountId);
		Map <Id, String> map_idParents = new Map <Id, String>{acc.Id => 'Test'};

		//sincronización de contactos
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(null, null, null, map_contacts, null, acc, map_idParents);
		obj.executeBody();

		Test.stopTest();
	}

	@isTest static void oneContactWithHoldings(){

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);

		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//Rellenamos map_idParents para que no falle la sincronización
		Contact con = map_contacts.values()[0][0];
		Account acc = new Account(Id = con.AccountId);
		Map <Id, String> map_idParents = new Map <Id, String>{acc.Id => 'Test'};
		Map <Id, List <Contact>> map_auxContacts = new Map <Id, List<Contact>>{acc.Id => new List <Contact>{con}};

		//sincronización del contacto
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(map_holdingToProcess, null, null, map_auxContacts, null, acc, map_idParents);
		obj.executeBody();

		Test.stopTest();
	}

	@isTest static void allSites(){

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);

		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//Rellenamos map_idParents para que no falle la sincronización
		Account acc = new Account(Id = map_sites.values()[0][0].BI_Cliente__c);
		Map <Id, String> map_idParents = new Map <Id, String>{acc.Id => 'Test'};

		//sincronización de sites
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(null, null, null, null, map_sites, acc, map_idParents);
		obj.executeBody();

		Test.stopTest();
	}

	@isTest static void oneSiteWithHoldings(){

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);

		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//Rellenamos map_idParents para que no falle la sincronización
		BI_Punto_de_instalacion__c punt = map_sites.values()[0][0];
		Account acc = new Account(Id = punt.BI_Cliente__c);
		Map <Id, String> map_idParents = new Map <Id, String>{acc.Id => 'Test'};
		Map <Id, List <BI_Punto_de_instalacion__c>> map_auxSites = new Map <Id, List<BI_Punto_de_instalacion__c>>{acc.Id => new List <BI_Punto_de_instalacion__c>{punt}};

		//sincronización del site
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(map_holdingToProcess, null, null, null, map_auxSites, acc, map_idParents);
		obj.executeBody();

		Test.stopTest();
	}

	@isTest static void nextAccountExecution(){

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);
		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//Dado que el mapa tiene 2 holding, eliminamos uno de ellos y lo pasamos como currentAccount
		Account currentAccount = map_holdingToProcess.remove(new List <Id>(map_holdingToProcess.keySet())[0]);

		//sincronización de holding con customer countries
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, null, null, null, currentAccount, null);
		obj.executeBody();
		
		Test.stopTest();
	}
	
	@isTest static void onlyContacts_AllContacts(){

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);

		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//Rellenamos el identificador externo del cliente para que no falle la sincronización
		map_contacts.values()[0][0].Account.BI_Identificador_Externo__c = 'Test';

		//sincronización de sólo contactos
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(map_contacts, null);
		obj.executeBody();

		Test.stopTest();		
	}

	@isTest static void onlyContacts_oneAccountContact(){

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);

		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//Rellenamos el identificador externo del cliente para que no falle la sincronización
		map_contacts.values()[0].remove(0);
		map_contacts.values()[0][0].Account.BI_Identificador_Externo__c = 'Test';

		//sincronización de sólo contactos
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(map_contacts, null);
		obj.executeBody();

		Test.stopTest();		
	}

	@isTest static void onlyContacts_oneContact(){

		BI_TestUtils.throw_exception = false;

        BI_MigrationHelper.skipAllTriggers();
		//new FS_TGS_SendToFullStack_Job(map_holdingToProcess, map_customerCountryToProcess, map_legalEntityToProcess, map_contacts, map_sites, currentAccount, map_idParents);

		parseData();

		Map<String, String> responseHeaders = new Map<String, String>();
    	responseHeaders.put('Location', 'TestResponse');

		Test.startTest();

		Test.setMock(HttpCalloutMock.class, new FS_TGS_FullStackRestMock(201, 'CREATED', 'TRUE', responseheaders));

		//Rellenamos el identificador externo del cliente para que no falle la sincronización
		Contact con = map_contacts.values()[0][0];
		con.Account.BI_Identificador_Externo__c = 'Test';
		Map <Id, List <Contact>> map_auxContacts = new Map <Id, List <Contact>>{con.AccountId => new List <Contact>{con}};

		//sincronización de sólo contactos
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(map_auxContacts, null);
		obj.executeBody();

		Test.stopTest();		
	}

	@isTest static void exceptions(){

		BI_TestUtils.throw_exception = true;
		FS_TGS_SendToFullStack_Job obj = new FS_TGS_SendToFullStack_Job(null, null);
		obj.executeBody();
    }

	private static void parseData(){

        Map <String, Id> map_rts = new Map <String, Id>();

        map_rts.put(Constants.RECORD_TYPE_TGS_HOLDING, TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING));
        map_rts.put(Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY, TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY));
        map_rts.put(Constants.RECORD_TYPE_TGS_LEGAL_ENTITY, TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_LEGAL_ENTITY));

        List <Account> lst_allAcc = [SELECT Id, Name, RecordType.DeveloperName, Fax, Phone, NumberOfEmployees, BI_No_Identificador_fiscal__c, AnnualRevenue,
                            BI_Tipo_de_identificador_fiscal__c,BI_Country__c,Website, BI_Inicio_actividad_del_cliente__c, Rating, BI_Riesgo__c,
                            BI_Activo__c,BI_Denominacion_comercial__c, TGS_Account_Mnemonic__c, TGS_Account_Category__c, TGS_Es_MNC__c, 
                            TGS_Account_Leading_MNC_Unit__c, SAP_ID__c, TGS_Invoice_Language__c, ParentId, Parent.BI_Identificador_Externo__c,
                            TGS_Aux_Holding__c, TGS_Aux_Holding__r.BI_Identificador_Externo__c, BI_Identificador_Externo__c,
                            (SELECT Birthdate, TGS_Language__c, Account.BI_Identificador_Externo__c, Email, Phone, Fax, Department, 
                            Name, FirstName, LastName, Salutation, BI_Genero__c, BI_Activo__c, FS_TGS_Id_Contacto_FullStack__c
                            FROM Contacts),
                            (SELECT Id, Name, BI_Tipo_de_sede__c, BI_Cliente__c, BI_Cliente__r.BI_Identificador_Externo__c, BI_Sede__r.Id,
                            BI_Sede__r.Name, BI_Sede__r.BI_Direccion__c, BI_Sede__r.BI_Numero__c, BI_Sede__r.BI_Country__c, BI_Sede__r.BI_Provincia__c, 
                            BI_Sede__r.BI_Localidad__c, BI_Sede__r.BI_Piso__c, BI_Sede__r.BI_Apartamento__c, BI_Sede__r.BI_Codigo_postal__c, BI_Sede__r.BI_ID_de_la_sede__c  
                            FROM Puntos_de_instalacion__r)
                            FROM Account];

        if(!lst_allAcc.isEmpty()){

            for(Account acc : lst_allAcc){
                //Si la cuenta es un holding la metemos en el mapa de holding
                if(acc.RecordType.DeveloperName == Constants.RECORD_TYPE_TGS_HOLDING){
                    if(map_holdingToProcess != null){

                        map_holdingToProcess.put(acc.Id, acc);
                    }
                        else{
                            map_holdingToProcess = new Map <Id, Account>{acc.Id => acc};
                        }
                    }
                //Si la cuenta es un customer country la metemos en el mapa de customer country
                else if(acc.RecordType.DeveloperName == Constants.RECORD_TYPE_TGS_CUSTOMER_COUNTRY){
                    if(map_customerCountryToProcess != null){

                        map_customerCountryToProcess.put(acc.Id, acc);
                    }
                    else{
                        map_customerCountryToProcess = new Map <Id, Account>{acc.Id => acc};
                    }
                }
                //Si la cuenta es una legal entity la metemos en el mapa del legal entity
                else if(acc.RecordType.DeveloperName == Constants.RECORD_TYPE_TGS_LEGAL_ENTITY){
                    if(map_legalEntityToProcess != null){

                        map_legalEntityToProcess.put(acc.Id, acc);
                    }
                    else{
                        map_legalEntityToProcess = new Map <Id, Account>{acc.Id => acc};
                    }
                }
                //Si la cuenta tiene contactos asociados, los introducimos en el mapa de contactos
                if(!acc.Contacts.isEmpty()){

                    if(map_contacts != null){
                        map_contacts.put(acc.Id, acc.Contacts);
                    }
                    else{
                        map_contacts = new Map <Id, List <Contact>>{acc.Id => acc.Contacts};
                    }
                }
                //Si la cuenta tiene sedes asociadas, los introducimos en el mapa de sedes
                if(!acc.Puntos_de_instalacion__r.isEmpty()){

                    if(map_sites != null){
                        map_sites.put(acc.Id, acc.Puntos_de_instalacion__r);
                    }
                    else{
                        map_sites = new Map <Id, List <BI_Punto_de_instalacion__c>>{acc.Id => acc.Puntos_de_instalacion__r};
                    }
                }
            }                    
        }            
	}
}