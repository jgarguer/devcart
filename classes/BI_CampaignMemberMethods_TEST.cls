@isTest
private class BI_CampaignMemberMethods_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_CampaignMemberMethods class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    16/04/2014              Ignacio Llorca          Initial Version
    03/06/2014              Pablo Oliva             Method  "preventDeleteTest" updated
    05/06/2014              Pablo Oliva             General: A new user is created for inserting campaigns
    06/06/2014              Pablo Oliva             Method "CampaignMemberStatusTest" updated
    31/05/2017              Cristina Rodriguez      Commented BI_segment__c(Campaign)
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    static {
        BI_TestUtils.throw_exception = false;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CampaignMemberMethods.preventDelete
    History:
    
    <Date>              <Author>                <Description>
    16/04/2014          Ignacio Llorca          Initial version
    03/06/2014          Pablo Oliva             Users with Profile 'System Administrator' can delete records
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    static testMethod void preventDeleteTest_deniedUser() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
      
        List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1);
              
        List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);
       
        List <Contact> lst_con =BI_DataLoad.loadContacts(1, lst_acc);
       
        List <Lead> lst_ld =BI_DataLoad.loadLeads(1);
             
        List <Campaign> lst_camp = new List<Campaign>();
        //Profile prof = [select Id from Profile where Name = :Label.BI_Administrador  limit 1];
        
        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId =  BI_DataLoad.searchAdminProfile(),
                          BI_Permisos__c = Label.BI_Administrador_del_sistema, //Label.BI_Desarrollo_Comercial,//BI_Desarrollo_Comercial
                          timezonesidkey = Label.BI_TimeZoneLA,
                          UserPermissionsMarketingUser = true,
                          username = 'usertestuser@testorg.com');
        
        insert usertest;
        
        
        lst_camp = BI_DataLoad.loadCampaigns(1);
        
       
        List <CampaignMember> lst_cmpm =BI_DataLoad.loadCampaignMembers(lst_camp, lst_ld, lst_con, 1); 

        Integer lsiz=lst_cmpm.size();
        system.assert(!lst_cmpm.isEmpty());
        
        list<Id> lst_cmpmIds = new list<Id> ();
        for(CampaignMember campm :lst_cmpm){
            lst_cmpmIds.add(campm.Id);
        }
        
        Test.startTest();
        
        //User Allowed
        system.runAs(usertest){
            

           
            try{
                delete lst_cmpm;
                             
            }catch (DmlException e) {

                
                CampaignMember [] camp = [SELECT Id, BI_Editable__c, IsDeleted FROM CampaignMember];// WHERE Id IN :lst_cmpmIds
                system.debug('CAMP LIST AFTER DELETE: '+ camp);
                
                system.assertequals (camp.size(), lst_cmpm.size());
            }

        }
        Test.stopTest();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos         
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CampaignMemberMethods.preventDelete for Allow Users
    History: 
    //////////////////////////////////////DISABLED BECAUSE NOT FOUND ALLOWED USERS THAT CAN DELETE CAMPAIGN MEMBERS/////////////////////////////////////////
    
    <Date>              <Author>                <Description>
    26/09/2014          Micah Burgos            Initial version
    static testMethod void preventDeleteTest_allowedUser() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
      
        List <Region__c> lst_pais =BI_DataLoad.loadPais(1);
              
        List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);
       
        List <Contact> lst_con =BI_DataLoad.loadContacts(1, lst_acc);
       
        List <Lead> lst_ld =BI_DataLoad.loadLeads(1);
             
        List <Campaign> lst_camp = new List<Campaign>();
        
        Profile prof = [select Id from Profile where Name = 'BI_Standard'  limit 1];
        
        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId =  prof.Id,
                          BI_Permisos__c = Label.BI_Pricing2,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          UserPermissionsMarketingUser = true,
                          username = 'usertestuser@testorg.com');
        
        insert usertest;
        
        
        lst_camp = BI_DataLoad.loadCampaigns(1);
        
       
        List <CampaignMember> lst_cmpm =BI_DataLoad.loadCampaignMembers(lst_camp, lst_ld, lst_con, 1); 
        
        Integer lsiz=lst_cmpm.size();
        system.assert(!lst_cmpm.isEmpty());
        
        list<Id> lst_cmpmIds = new list<Id> ();
        for(CampaignMember campm :lst_cmpm){
            lst_cmpmIds.add(campm.Id);
        }
        
        Test.startTest();
        
        //User Allowed
        system.runAs(usertest){
            try{
                delete lst_cmpm;
               system.assert(true);                
            }catch (DmlException e) {
               system.assert(false);   
            }
        
        }

        Test.stopTest();
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CampaignMemberMethods.preventStatusBlockRecord and BI_CampaignMemberMethods.sendToApproval 
                   methods.
    History:
    
    <Date>              <Author>                <Description>
    16/04/2014          Ignacio Llorca          Initial version
    08/12/2015          Francisco Ayllon        Changes due to "demanda 136"
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void preventStatusBlockRecordandsendToApproval() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
       
        List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
        system.assert(!lst_pais.isEmpty());
        List<Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);
        system.assert(!lst_acc.isEmpty());    
        List<Contact> lst_con = BI_DataLoad.loadContacts(1, lst_acc);
        system.assert(!lst_con.isEmpty());   
        List<Lead> lst_ld = BI_DataLoad.loadLeads(1);
        system.assert(!lst_ld.isEmpty());
                
        List<User> users = [select Id, Profile.Name from User where Profile.Name = 'BI_Standard_ARG' AND isActive = true LIMIT 2];
        
        List<User> user_lst = new List<User>();
        User usu = new User (alias = 'XXXTest',
                                            email = 'XXX@testXXX.com',
                                            emailencodingkey = 'UTF-8',
                                            lastname = 'TestingXXX',
                                            languagelocalekey = 'en_US',
                                            localesidkey = 'en_US',
                                            ProfileId = [SELECT Id FROM Profile WHERE Name =: Label.BI_Administrador_Sistema LIMIT 1].Id,
                                            BI_Permisos__c = 'Super Usuario',
                                            timezonesidkey = Label.BI_TimeZoneLA,
                                            username = 'ewrwe325ee@testXXX.com',
                                            IsActive = true);
        user_lst.add(usu);

        insert user_lst;

        system.assert(!users.isEmpty());
        //Campaign[] camp;
        
        List<Campaign> lst_camp = new List<Campaign>();
        
        Campaign camp = new Campaign( Name = 'camp1',
                                      CurrencyIsoCode = 'USD',
                                     //BI_Segment__c = 'Empresas',
                                      Status = Label.BI_EnPlanificacion,
                                      BI_Crear_oportunidad_contactos__c = Label.BI_No,
                                      isActive = false,
                                      BI_Aprobacion_miembros_de_campana__c = false,
                                      EndDate = date.TODAY(),
                                      BI_COL_Codigo_campana__c = 'XXX');

        //lst_camp = BI_DataLoad.loadCampaigns(1);
    
        //camp = [SELECT Status, isActive, Id, BI_Aprobacion_miembros_de_campana__c  FROM Campaign WHERE Id IN:lst_camp];
        
        /*camp[0].isActive = true;
        camp[0].Status = Label.BI_EnCurso;
        camp[0].BI_Aprobacion_miembros_de_campana__c = false;
        update camp;*/

        Test.startTest();
            System.runAs(users[0]){
                lst_camp.add(camp);
                insert lst_camp;
                //camp.isActive = true;
                //camp.status = 'In Progress';
                //update lst_camp;

            }

            System.runAs(user_lst[0]){
              System.assert(!lst_camp.isEmpty());
              List<CampaignMember> lst_cmpm = BI_DataLoad.loadCampaignMembers(lst_camp, lst_ld, lst_con, 1);
              System.assert(!lst_cmpm.isEmpty());

            }        
        
            BI_Aprobacion_Miembros_Campana__c [] app = [SELECT Id FROM BI_Aprobacion_Miembros_Campana__c];
            List<CampaignMember> lst_cmpm2 = [SELECT Id FROM CampaignMember WHERE BI_Bloqueo__c = true AND Status = :Label.BI_PendienteAprobacion];
            
            system.assert(!lst_cmpm2.isEmpty()); 
            system.assert(!app.isEmpty()); 
        
        Test.stopTest();
    }
     
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CampaignMemberMethods.preventUpdate
    
    History:
    
    <Date>              <Author>                <Description>
    16/04/2014          Ignacio Llorca          Initial version
    09/12/2016          Gawron, Julián          Change asserts for new dataload
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void preventUpdate() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
        List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1);
              
        List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);
               
        List <Contact> lst_con =BI_DataLoad.loadContacts(100, lst_acc);
               
        List <Lead> lst_ld =BI_DataLoad.loadLeads(100);
                     
        List <Campaign> lst_camp = new List<Campaign>();
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name =: Label.BI_Administrador limit 1];
        
        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof.Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com');
        
        insert usertest;
        
        system.runAs(usertest){
            lst_camp = BI_DataLoad.loadCampaigns(1);
        }
        
        Campaign[] camp = [SELECT Id FROM Campaign WHERE Id IN:lst_camp];
        
        camp[0].isActive = true;
        camp[0].Status = Label.BI_EnCurso;
        update camp;
            
        List <CampaignMember> lst_cmpm =BI_DataLoad.loadCampaignMembers(lst_camp, lst_ld, lst_con, 100);
        
        BI_Aprobacion_Miembros_Campana__c [] app = [SELECT Id FROM BI_Aprobacion_Miembros_Campana__c];
        lst_cmpm = [SELECT Id FROM CampaignMember WHERE BI_Bloqueo__c = true];
        
        Test.startTest();
            
        for (CampaignMember campm:lst_cmpm){
            campm.BI_Bloqueo__c = false;
        }
        
        try{
            update lst_cmpm;
                   
        }catch(DmlException e) {
            lst_cmpm = [SELECT Id FROM CampaignMember WHERE BI_Bloqueo__c = true];
            system.assertequals (4,lst_cmpm.size(), 'CampaignMember BI_Bloqueo__c = true'); //JEG 
        }
            
        Test.stopTest();
    }
    
     
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CampaignMemberMethods.preventStatusBlockRecord (Case "Status Aprobado or Rechazado")
    History:
    
    <Date>              <Author>                <Description>
    22/04/2014          Pablo Oliva             Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void preventStatusTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
      
        List <String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
              
        List <Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_pais);
       
        List <Contact> lst_con = BI_DataLoad.loadContacts(100, lst_acc);
       
        List <Lead> lst_ld = BI_DataLoad.loadLeads(100);
        
        List <Campaign> lst_camp = new List<Campaign>();
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name =: Label.BI_Administrador limit 1];
        
        User usertest = new User(alias = 'test1u',
                          email = 't1u@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Tes1t',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof.Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com');
        
        insert usertest;
        
        system.runAs(usertest){

            lst_camp = BI_DataLoad.loadCampaigns(1);
            
        }
       
        Test.startTest();
           
        try{
            BI_DataLoad.loadCampaignMembers2(lst_camp, lst_ld, lst_con);
            
                 
        }catch (DmlException e) {
            CampaignMember [] camp = [SELECT Id FROM CampaignMember];
            system.assertequals (camp.size(), 0);
        }
                
        Test.stopTest();
        
    }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CampaignMemberHelper.createCampaignMemberStatus and createCMS
    History:
    
    <Date>              <Author>                <Description>
    22/04/2014          Ignacio Llorca          Initial version
    06/06/2014          Pablo Oliva             Number of records changed: Apex CPU time limit exception
    18/11/2016          Adrián Caro             Apex CPU time limit exceeded solved
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void CampaignMemberStatusTest(){  
            
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List <String> lst_pais =BI_DataLoad.loadPaisFromPickList(1);
          
        List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);
    
        List <Contact> lst_con =BI_DataLoad.loadContacts(25, lst_acc);
   
        List <Lead> lst_ld =BI_DataLoad.loadLeads(25);
  
        List <Campaign> lst_camp = new List<Campaign>();
        
        Profile prof = [select Id from Profile where Name = 'System Administrator' OR Name =: Label.BI_Administrador limit 1];
        
        User usertest = new User(alias = 'testu',
                          email = 'tu@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof.Id,
                          BI_Permisos__c = Label.BI_Desarrollo_Comercial,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'usertestuser@testorg.com');
        
        insert usertest;
        
        system.runAs(usertest){
            lst_camp = BI_DataLoad.loadCampaigns(50);
        }
        
        for (Campaign camp:lst_camp){
            camp.IsActive = true;
            camp.Status = Label.BI_EnCurso;
        }
            
        update lst_camp;
   
        List <CampaignMember> lst_cm = BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);
        
        CampaignMemberStatus [] cms = [SELECT Id, Label FROM CampaignMemberStatus WHERE CampaignId IN :lst_camp AND (Label =: Label.BI_Aprobado OR Label =: Label.BI_Rechazado)];
                
        delete cms;
        CampaignMemberStatus [] cms2 = [SELECT Id, Label FROM CampaignMemberStatus WHERE CampaignId IN :lst_camp];
                
        system.debug (cms + 'DEBUG CMS');
        
        Set<Id> set_c = new Set<Id>();
        
        for (CampaignMember cmem:lst_cm){ 
            set_c.add(cmem.Id);
        }
        
        Test.StartTest();
        
        BI_Aprobacion_Miembros_Campana__c [] amc = [SELECT Id, BI_Miembro_de_Campana__c FROM BI_Aprobacion_Miembros_Campana__c WHERE BI_Miembro_de_Campana__c IN :set_c];
        
        for (BI_Aprobacion_Miembros_Campana__c amemc:amc){
            amemc.BI_Estado__c = Label.BI_Rechazado;
        }
        update amc;
        
        cms = [SELECT Id, Label FROM CampaignMemberStatus WHERE CampaignId IN :lst_camp AND (Label =: Label.BI_Aprobado OR Label =: Label.BI_Rechazado)];
        

        system.assert(!cms.isEmpty());

        
        Test.stopTest();
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_CampaignMemberMethods.manageCampanasCuenta
    History:
    
    <Date>              <Author>                <Description>
    25/11/2015          Antonio Masferrer       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void manageCampanasCuentasTest(){

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        List <String> lst_pais = new List <String>();
        lst_pais.add('Peru');
        
        List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);
    
        List <Contact> lst_con =BI_DataLoad.loadContacts(1, lst_acc);

        List <Lead> lst_ld =BI_DataLoad.loadLeads(10);

        List <Campaign> lst_camp = BI_DataLoad.loadCampaigns(10);

        List <CampaignMember> lst_cm = BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);

        Test.startTest();
        BI_CampaignMemberMethods.manageCampanasCuenta(lst_cm, 1);
        Test.stopTest();


    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Masferrer
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code try/catch block for BI_CampaignMemberMethods
    History:
    
    <Date>              <Author>                <Description>
    25/11/2015          Antonio Masferrer       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
     static testMethod void catchExceptions(){
    
    List <String> lst_pais = new List <String>();
    lst_pais.add('Peru');
    List <Account> lst_acc =BI_DataLoad.loadAccounts(1, lst_pais);
    List <Contact> lst_con =BI_DataLoad.loadContacts(1, lst_acc);
    List <Lead> lst_ld =BI_DataLoad.loadLeads(50);
    List <Campaign> lst_camp = BI_DataLoad.loadCampaigns(10);
    List <CampaignMember> lst_cm;

   // CampaignMember cmp = [Select Id, BI_Editable__c, BI_Bloqueo__c, Status from CampaignMember Where BI_Bloqueo__c = false And BI_Editable__c = true And Status <> 'Rechazado' LIMIT 1];
    
    Test.startTest(); 

    BI_TestUtils.throw_exception = true;
    BI_GlobalVariables.stopPreventUpdate = true;
    lst_cm = BI_DataLoad.loadTwoCampaignMember(lst_camp, lst_ld, lst_con);
   
    
    //System.assertEquals(BI_GlobalVariables.stopPreventUpdate, false);
    //System.assertEquals( lst_cm[0].BI_Editable__c, true);
    System.assertEquals( lst_cm[0].BI_Bloqueo__c, false);
    System.assertEquals( lst_cm[0].Status, 'Sent');

    update lst_cm;
   
    delete lst_cm;
    BI_GlobalVariables.stopPreventUpdate = false;
    Test.stopTest();

    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}