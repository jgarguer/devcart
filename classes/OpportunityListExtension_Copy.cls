public with sharing class OpportunityListExtension_Copy {
	
  ApexPages.standardController stdController = null;
    public NE__Quote__c qt;    
    //TEST public List <Order__c> ord                  {get;set;}
    public Id       orderSelectedId             {get;set;} // id dell'ordine selezionato
    public Opportunity  opty                    {get;set;}
    //TEST public Order__c ordSelected                 {get;set;}
    //TEST public Order__c newOrd                      {get;set;}
    public boolean catchClone                   {get;set;}
    public List<NE__OrderItem__c> listaNoPromo      {get;set;}
    public List<NE__OrderItem__c> listaPromo        {get;set;} 
    public DataMap_Copy dm;
    public List <NE__OrderItem__c>  ordItemList     {get;set;}
    public NE__IntegrationAdministration__c cartAdmin{get;set;}
    
    //TEST
    public transient List<NE__Order__c> selectedOrders {get;set;}
    public transient RecordType rec2 			   {get;set;}
    public transient Opportunity optys			   {get;set;}
    public transient Map<String,String> result     {get;set;}
    public transient NE__Order__c newOrd                      {get;set;}
    public transient List<List<SObject>> listInt		{get;set;}
    public transient List <NE__Order__c> ord                  {get;set;}
    public transient NE__Order__c ordSelected                 {get;set;}
    
    public String urlPage {get{if(urlPage == null ) urlPage ='';return urlPage;}set;}
    public OpportunityListExtension_Copy(ApexPages.StandardController stdCon)   
    {
        stdController   =   stdCon;
        opty            =   (Opportunity)stdController.GetRecord(); 
        NE__Order__c newOrd;
        dm = new DataMap_Copy(); 
        
      /*  //GC: getting info for dynamic url
        User currentUser                =   [select Profile.id,Name from User where id = :Userinfo.getUserId()];            
        String usrProfileName       =   '*;nextCartProfile;'+currentUser.Profile.id+';nextCartProfile;*';
        //TEST List<List<SObject>> listInt =   [FIND :usrProfileName IN ALL FIELDS RETURNING IntegrationAdministration__c (CartType__c,OrderFrequency__c,SetRedirect__c, RedirectWithData__c, RedirectTo__c, CSS_Page__c, SearchBarFields__c, ColumsVisible__c,Value_received__c, Value_interpreted_as__c, Username__c, SystemModstamp, RecordTypeId, AbilitatedProfiles__c, Password__c, OwnerId, Name__c, Name, LastModifiedDate, LastModifiedById, ItemsForPage__c, IsDeleted, Id, EndPoint__c, DVECall__c, CreatedDate, CreatedById, CertificateName__c, CatalogVisibility__c, AlternativePassword__c) LIMIT 1];
        listInt =   [FIND :usrProfileName IN ALL FIELDS RETURNING NE__IntegrationAdministration__c (NE__CartType__c,NE__OrderFrequency__c,NE__SetRedirect__c, NE__RedirectWithData__c, NE__RedirectTo__c, NE__CSS_Page__c, NE__SearchBarFields__c, NE__ColumsVisible__c,NE__Value_received__c, NE__Value_interpreted_as__c, NE__Username__c, SystemModstamp, RecordTypeId, NE__AbilitatedProfiles__c, NE__Password__c, OwnerId, NE__Name__c, Name, LastModifiedDate, LastModifiedById, NE__ItemsForPage__c, IsDeleted, Id, NE__EndPoint__c, NE__DVECall__c, CreatedDate, CreatedById, NE__CertificateName__c, NE__CatalogVisibility__c, NE__AlternativePassword__c) LIMIT 1];
        
        if(listInt != null)
        {
            if(listInt.size()>0)
                if(listInt.get(0).size()>0)
                    cartAdmin = (NE__IntegrationAdministration__c)listInt.get(0).get(0);
        }*/
        
        //RDF FIX QUERY FIND
        List <RecordType> recordlist                           =      [select Id,Name from RecordType WHERE Name ='Cart Configuration'  LIMIT 1];
        User currentUser                                            =   [select Profile.id,Name from User where id = :Userinfo.getUserId()];
        String usrProfileName                                  =   currentUser.Profile.id;
        List<NE__IntegrationAdministration__c> listIntAdm   =      new  List<NE__IntegrationAdministration__c>();
        try{
            
			listIntAdm = [Select NE__CartType__c,NE__OrderFrequency__c,NE__SetRedirect__c, NE__RedirectWithData__c, NE__RedirectTo__c, NE__CSS_Page__c, NE__SearchBarFields__c, NE__ColumsVisible__c,NE__Value_received__c, NE__Value_interpreted_as__c, NE__Username__c, SystemModstamp, RecordTypeId, NE__AbilitatedProfiles__c, NE__Password__c, OwnerId, NE__Name__c, Name, LastModifiedDate, LastModifiedById, NE__ItemsForPage__c, IsDeleted, Id, NE__EndPoint__c, NE__DVECall__c, CreatedDate, CreatedById, NE__CertificateName__c, NE__CatalogVisibility__c, NE__AlternativePassword__c from NE__IntegrationAdministration__c where RecordType.Name='Cart Configuration' order by Id desc];
        
            Boolean checkIA = false;  
            for(Integer k=0; (k<listIntAdm.size() && checkIA!=true);k++){
                   if(listIntAdm[k].NE__AbilitatedProfiles__c.contains(usrProfileName)){
                          cartAdmin= listIntAdm[k];
                          checkIA = true;
                   }
           }
        
        }
        catch(Exception e){}

    }
    public List<NE__Order__c> getOrderListTable() 
    {
        ord = [select Url_Payback__c,NE__OrderStatus__c,NE__Order_date__c,Id,NE__OptyId__c,NE__AccountId__c,RecordTypeId,Name,NE__Version__c  FROM NE__Order__c WHERE RecordType.Name = 'Opty' AND NE__OptyId__c =: opty.Id ORDER BY NE__Version__c  DESC ];
        if(ord.isEmpty()==true)
        {    /*ATTENZIONE DA GESTIRE    
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,System.Label.NE__Mex_Error_Line_item_not_found));
            */        
        }
        return ord;
    }
    public void processSelected()
    {
    	// prefix
    	String prefix = Site.getPrefix();
		if(prefix == null || prefix == '')
			prefix = '';
    	
        //TEST   List<Order__c> selectedOrders   =   new List<Order__c>(); 
        selectedOrders                  =   [select Id, NE__OrderStatus__c,NE__Order_date__c,NE__OptyId__c,NE__AccountId__c,RecordTypeId,Name ,NE__Version__c  FROM NE__Order__c WHERE RecordType.Name = 'Opty' AND NE__OrderStatus__c = 'Active' AND NE__OptyId__c =: opty.Id  LIMIT 1];
        //TEST RecordType rec2                 =   [select Name,Id from RecordType WHERE Name = 'Quote' AND (SObjectType = 'Order__c' OR SObjectType = 'NE__Order__c') LIMIT 1];
        rec2                 =   [select Name,Id from RecordType WHERE Name = 'Quote' AND (SObjectType = 'Order__c' OR SObjectType = 'NE__Order__c') LIMIT 1];
        
        //TEST Opportunity optys               =   [select id, AccountId , Name , HaveActiveLineItem__c , Amount ,ExpectedRevenue,Order_Generated__c from Opportunity where id=: opty.Id LIMIT 1];
        optys               =   [select id, AccountId , Name , NE__HaveActiveLineItem__c , Amount ,ExpectedRevenue,NE__Order_Generated__c from Opportunity where id=: opty.Id LIMIT 1]; 
        
        
        if(selectedOrders.isEmpty()== false)
        {
            system.debug('entrata if lista non vuota' + selectedOrders);
            for(NE__Order__c oc : selectedOrders)
            {
                system.debug('entrata for valore lista' + oc);
                if(oc.NE__OrderStatus__c == 'Active')
                {
                    //TEST Map<String,String> result = dm.GenerateMapObjects('Order2Order' , oc.Id);
                    result = dm.GenerateMapObjects('Order2Order' , oc.Id);
                    if(result.get('ErrorCode')=='0')
                    {
                        if(Test.IsRunningTest())
                            newOrd = [SELECT Id,Name,NE__Version__c,RecordTypeId,NE__OrderStatus__c,NE__SerialNum__c FROM NE__Order__c WHERE Id =: oc.Id];
                        else
                            newOrd = [SELECT Id,Name,NE__Version__c,RecordTypeId,NE__OrderStatus__c,NE__SerialNum__c FROM NE__Order__c WHERE Id =: result.get('ParentId')];
                        qt  = new NE__Quote__c (NE__AccountId__c = optys.AccountId ,NE__Amount__c = optys.Amount,NE__Expected_Revenue__c =optys.ExpectedRevenue , NE__Opportunity__c=optys.Id,NE__Orders__c=newOrd.Id);
                        newOrd.RecordTypeId         =  rec2.Id;
                        newOrd.NE__OrderStatus__c       = 'Pending';
                        newOrd.NE__SerialNum__c         = newOrd.Name;                      
                        optys.NE__Order_Generated__c    ='Quote';                   
                        oc.NE__OrderStatus__c           = 'Active';
                        optys.NE__HaveActiveLineItem__c = false;
                        Decimal vr                  = oc.NE__Version__c;
                        newOrd.NE__Version__c           = vr+1;
                        insert qt;
                        
                        newOrd.NE__Quote__c = qt.Id;
                        
                        update optys;
                        update selectedOrders;
                        update newOrd;
                        
                        if(cartAdmin == null) //ET v3.5 prefix removed for Page.
                            urlPage = URL.getSalesforceBaseUrl().toExternalForm() + /*prefix +*/ Page.NE__NewConfiguration.geturl() + '?ordId='+newOrd.Id;
                        else if(cartAdmin.NE__CartType__c == 'B2C')
                            urlPage = URL.getSalesforceBaseUrl().toExternalForm() + /*prefix +*/ Page.NE__NewConfiguration.geturl() + '?ordId='+newOrd.Id;
                        else
                            urlPage = URL.getSalesforceBaseUrl().toExternalForm() + /*prefix +*/ Page.NE__NewConfiguration.geturl() + '?ordId='+newOrd.Id;
                    }
                }
                else    
                {/*ATTENZIONE DA GESTIRE
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.No_Line_item_active));
                */         
                }               
            }
        }
        else
        {/*ATTENZIONE DA GESTIRE
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.No_Line_item_active));       
        */
        }
    }
   public pageReference cloneOrder ()
   { 
   		// prefix
   		String prefix = Site.getPrefix();
		if(prefix == null || prefix == '')
			prefix = '';
		
        try{
            ordSelected         =   
                    [select 
                        NE__AccountId__c,
                        NE__AssetEnterpriseCalc__c,  
                        NE__AssetEnterpriseId__c,
                        NE__AssetStatusCalc__c,
                        NE__BillAccId__c,
                        NE__BillingProfId__c,
                        NE__BillingRegion__c,
                        NE__CatalogId__c, 
                        NE__CommercialModelId__c, 
                        NE__ConfigurationStatus__c,
                        CreatedById,
                        CreatedDate,
                        NE__CurrencyCode__c, 
                        NE__FulfilmentStatus__c,
                        Id,
                        IsDeleted,
                        LastModifiedById,
                        LastModifiedDate,
                        Name,
                        NE__One_Time_Fee_Total__c,
                        NE__OpportunityId__c,
                        NE__OptyId__c,
                        NE__OrderStatus__c,
                        NE__Order_date__c,
                        OwnerId,
                        NE__Promotion_Code__c,
                        NE__Quote__c,
                        RecordTypeId,
                        NE__Recurring_Charge_Total__c,
                        NE__SerialNum__c,
                        NE__ServAccId__c,
                        NE__ShippingRegion__c,
                        NE__Sync_oppty__c,
                        SystemModstamp,
                        NE__Type__c ,
                        NE__Version__c
                    FROM 
                        NE__Order__c 
                    WHERE 
                        RecordType.Name = 'Opty' 
                        AND NE__OrderStatus__c = 'Active' 
                        AND NE__OptyId__c =: opty.Id 
                    LIMIT 1
                    ];
            //  String mapName = 'Order2Order';
            Map<String,String> result = dm.GenerateMapObjects('Order2Order' , ordSelected.Id);
            
            if(result.get('ErrorCode')=='0')
            {
                system.debug('cartAdmin: '+cartAdmin);
                newOrd = [SELECT Id,Name,NE__Version__c FROM NE__Order__c WHERE Id =: result.get('ParentId')];
                if(cartAdmin == null) //ET v3.5 prefix removed for Page.
                    urlPage = URL.getSalesforceBaseUrl().toExternalForm() + /*prefix +*/ Page.NE__NewConfiguration.geturl() + '?ordId='+newOrd.Id;
                else if(cartAdmin.NE__CartType__c == 'B2C')
                    urlPage = URL.getSalesforceBaseUrl().toExternalForm() + /*prefix +*/ Page.NE__NewConfiguration.geturl() + '?ordId='+newOrd.Id;
                else
                    urlPage = URL.getSalesforceBaseUrl().toExternalForm() + /*prefix +*/ Page.NE__NewConfiguration.geturl() + '?ordId='+newOrd.Id;
                
                system.debug('urlPage: '+urlPage);
                    
                ordSelected.NE__OrderStatus__c = 'Revised';
                Decimal vr = ordSelected.NE__Version__c;
                newOrd.NE__Version__c = vr+1;
                
                update newOrd;
                update ordSelected;
            }
            else
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,result.get('ErrorMessage')+' '+result.get('ParentId')));
            
        }
        catch(exception e)
        {

                System.debug('error: '+e+' at line: '+e.getLineNumber());
        }

        return null;
    }


 
    // metodo che cambia il recordtype dell ordine in stato active
    public PageReference recordOrder () 
    {
    	// prefix
    	String prefix = Site.getPrefix();
		if(prefix == null || prefix == '')
			prefix = '';
    	
        try
        {
            Opportunity optys   =   [select id, AccountId , Name , NE__HaveActiveLineItem__c, Amount,NE__Order_Generated__c from Opportunity where id=: opty.Id LIMIT 1];   
            ordSelected         =   [select NE__OrderStatus__c,NE__Order_date__c,Id,NE__OptyId__c,NE__AccountId__c,RecordTypeId,NE__Version__c, Name  FROM NE__Order__c WHERE RecordType.Name = 'Opty' AND NE__OrderStatus__c = 'Active' AND NE__OptyId__c =: opty.Id LIMIT 1];
            RecordType rec2     =   [select Name,Id from RecordType WHERE Name = 'Order' AND (SObjectType = 'Order__c' OR SObjectType = 'NE__Order__c') LIMIT 1];     
            Map<String,String> result = dm.GenerateMapObjects('Order2Order' , ordSelected.Id);
            if(result.get('ErrorCode')=='0')
            {
                newOrd = [SELECT Id,Name,NE__Version__c,RecordTypeId,NE__OrderStatus__c,NE__SerialNum__c FROM NE__Order__c WHERE Id =: result.get('ParentId')];
                newOrd.RecordTypeId =  rec2.Id;
                newOrd.NE__OrderStatus__c = 'Pending';
                newOrd.NE__SerialNum__c   = newOrd.Name;
                optys.NE__Order_Generated__c    ='Order';
                ordSelected.NE__OrderStatus__c  = 'Active';
                Decimal vr = ordSelected.NE__Version__c;
                newOrd.NE__Version__c = vr+1;
                update optys;
                update ordSelected;
                update newOrd;
                                
                if(cartAdmin == null) //ET v3.5 prefix removed for Page.
                    urlPage = URL.getSalesforceBaseUrl().toExternalForm() + /*prefix +*/ Page.NE__NewConfiguration.geturl() + '?ordId='+newOrd.Id;
                else if(cartAdmin.NE__CartType__c == 'B2C')
                    urlPage = URL.getSalesforceBaseUrl().toExternalForm() + /*prefix +*/ Page.NE__NewConfiguration.geturl() + '?ordId='+newOrd.Id;
                else
                    urlPage = URL.getSalesforceBaseUrl().toExternalForm() + /*prefix +*/ Page.NE__NewConfiguration.geturl() + '?ordId='+newOrd.Id;
            }           
        }
        catch(Exception e){
        	/*ATTENZIONE DA GESTIRE
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.No_Line_item_active));
            */
        } 
    return null;
    }    
}