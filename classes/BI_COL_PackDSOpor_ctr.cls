/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-04-13      Daniel ALexander Lopez (DL)     Cloned Controller      
*************************************************************************************/


public with sharing class BI_COL_PackDSOpor_ctr {
	
    public Map<String,List<Wrapempaquetar>> mapPaquetXDS {get;set;}
    public List<BI_COL_Descripcion_de_servicio__c> lstDs {get;set;}
    public Boolean actMensajes{get;set;}
    public List<String> lstPaquetes{get;set;}
    public Map<Id,BI_COL_Descripcion_de_servicio__c> mapIdXnuevaDS;
    public List<List<Wrapempaquetar>> lstPrincipal{get;set;}
    public List<Wrapempaquetar> lstMostrar{get;set;} 
    public List<BI_COL_Descripcion_de_servicio__c> lstDsEmpaquetar{get;set;}
    public Integer iterador;
    public Integer limite{get;set;}
    public boolean actRegion{get;set;}
    public Boolean disableBoton1{get;set;}
    public boolean disableBoton2{get;set;}
    public List<Selectoption> lstPaquetesSel{get;set;}
    public List<Selectoption> lstPaquetesNuevo{get;set;}
    public string opcEmpaquetar {get;set;}
    public string opcEmpaquetarNuevo {get;set;}
    public Map<String, BI_COL_Empaquetamiento__c> mapEmpaquetamiento;
    public Account cliente;
    public String segmento;
    public boolean habilitarPaquete {set;}
    public List<Account> lstCliente;
    
    /** Paramétros del paginador */
    private List<Wrapempaquetar> pageEmpaquetar;
    private Integer pageNumber;
    private Integer pageSize;
    private Integer totalPageNumber;
    
    public BI_COL_PackDSOpor_ctr(ApexPages.StandardController controller)
    {
        actRegion=false;
        actMensajes=false;
        disableBoton1=true;
        disableBoton2=true;
        lstDs=new List<BI_COL_Descripcion_de_servicio__c>();
        
        String idCliente = Apexpages.currentPage().getParameters().get('id');
        lstCliente=[select id,Name,BI_COL_Segmento_Telefonica__c,
                                    (Select id,Name,StageName
                                        From Opportunities 
                                        //where StageName <> 'F1 - Cerrada/Legalizada'
                                        ) 
                                    from Account where id= :idCliente limit 1];
        
        if(lstCliente==null||lstCliente.size()==0)
        {
            Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.INFO, 'No existe cliente consultado' );
            Apexpages.addMessage( pm );
            actMensajes=true;
        }
        else
        {
            this.cliente = lstCliente.get(0); 
            segmento = this.cliente.BI_COL_Segmento_Telefonica__c;
            List<Opportunity> lstOportunidades = this.cliente.Opportunities;
            this.lstDS = getDS(this.cliente.Opportunities);
            //Opciones de paquetes
            mapEmpaquetamiento = BI_COL_Empaquetamiento__c.getAll();
            
            
            //Llenar lista con opc de paquetes
            constructor(segmento,mapEmpaquetamiento.values(),lstDs);
            
            //Inicializar valores del paginador
                pageNumber = 0;
                totalPageNumber = 0;
                pageSize = 5;
                ViewData(); 
        }
            
        system.debug('lst' + lstds);
        
    }
    
    public void constructor(String segmento, List<BI_COL_Empaquetamiento__c> lstEmp, List<BI_COL_Descripcion_de_servicio__c> lstDS)
    {
        if(lstDs==null||lstDs.size()==0)
        {
            Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.INFO, 'No existen Ds para empaquetar' );
            Apexpages.addMessage( pm );
            actMensajes=true;
            
        }
        else
        {
            lstPrincipal=new List<List<Wrapempaquetar>>();
            llenarPaquetes();
            if(lstPrincipal.size()>0)
            {
                iterador=0;
                lstMostrar=lstPrincipal.get(iterador);
                if(lstPrincipal.size()>1)
                {
                    iterador++;
                    disableBoton1=false;
                }   
                actRegion=true;
                llenarSelectOption(segmento,lstEmp,lstDS);
            }
            else
            {
                actMensajes=true;
                actRegion=false;
                Apexpages.addMessage(new ApexPages.Message(Apexpages.Severity.INFO,'No se han encontrado DS para la oportunidad'));
                
            }
        }
    }
    
    public PageReference ViewData()
    {
        totalPageNumber = 0;
        BindData(1);
        return null;
    }
    
    public class Wrapempaquetar
    {
        public id idDs {get;set;}
        public boolean seleccion {get;set;}
        public String dsName {get;set;}
        public String paquete{get;set;}
        public String linea {get;set;}
        public String producto{get;set;}
        public String descripcionRef{get;set;}
        public boolean disable{get;set;}
        public String NombrePaquete{get;set;}
        
        public Wrapempaquetar()
        {
            seleccion=false;
            paquete=label.BI_COL_lblSinEmpaquetar;
            NombrePaquete=label.BI_COL_lblSinEmpaquetar;
        }
        
    }
    public void llenarPaquetes()
    {
        mapIdXnuevaDS=new Map<id,BI_COL_Descripcion_de_servicio__c>();
        mapPaquetXDS=new Map<String,list<Wrapempaquetar>>();
        List <BI_COL_Descripcion_de_servicio__c>lsttmp=lstDs.clone();
        System.debug('tamLista'+lstDs.size());
        List<Wrapempaquetar>lstAdd;
        Integer numListas=calculoListas(lstDs.size());
        integer contador2=0;
        Integer tamlistatmp=lstTmp.size();
        User usu=[Select CompanyName,Profile.Name from User where id=: userinfo.getuserid()];
        for(integer contador=0;contador<numListas;contador++)
        {
            System.debug('tamLista for'+lsttmp.size());
            lstadd=new List<Wrapempaquetar>();
            
            for(integer contador3=0;contador3<10;contador3++)
            {
                BI_COL_Descripcion_de_servicio__c DsTemp=lsttmp.get(contador2);
                mapIdXnuevaDs.put(DsTemp.id,DsTemp);
                Wrapempaquetar wrap=nuevoWrap(dsTemp,usu);
                lstadd.add(wrap);
                llenarPaquetes(wrap);
                contador2++;
                if(lsttmp.isEmpty()||contador2==tamlistatmp)
                {
                    break;
                }
            }
            lstPrincipal.add(lstadd);
            
        }
        actRegion=true;
    }
    public Integer calculoListas(Integer tam)
    {
        Decimal calculo=tam/10;
        Integer retorna=calculo.intValue();
        if(Math.mod(tam,10)>0)
        {
            retorna=retorna+1;
        }
        return retorna;         
    }
    public Wrapempaquetar nuevoWrap(BI_COL_Descripcion_de_servicio__c ds,User usu)
    {
        Wrapempaquetar wr=new Wrapempaquetar();
        wr.idDs=ds.id;
        wr.dsName=ds.name;
        //wr.linea=ds.producto_telefonica__r.linea__c;
        //wr.producto=ds.producto_telefonica__r.producto__c;
        //wr.descripcionRef=ds.producto_telefonica__r.descripcion_referencia__c;
        
        Boolean bandera=false;
        if(ds.BI_COL_Oportunidad__r.StageName!='E7 - Legalizada')
        {
            bandera=true;
        }else 
        if(usu.Profile.Name=='Admon Ventas Master')
        {
            bandera=true;
        }
        
        if(ds.BI_COL_Codigo_paquete__c==null && bandera==true)
        {
            wr.disable=false;
        }
        else
        {
            wr.disable=true;
            wr.paquete=ds.BI_COL_Codigo_paquete__c;
            wr.NombrePaquete=ds.BI_COL_Nombre_de_paquete__c;
        }
        return wr;
        
    }
    public Pagereference siguiente()
    {
        
        lstMostrar=lstPrincipal.get(iterador);
        iterador++;
        disableboton2=false;
        if(iterador==lstPrincipal.size())
        {
            disableboton1=true;
            iterador--;
            
        }
        
        return null;
    }
    public Pagereference anterior()
    {
        iterador--;
        lstMostrar=lstPrincipal.get(iterador);
        
        disableboton1=false;
        if(iterador==0)
        {
            disableboton2=true;
            iterador++;
        }
        return null;
    }
    
    public PageReference empaquetar()
    {
        Double num;
        List<BI_COL_manage_cons__c> lmc;
        if(opcEmpaquetar == null || opcEmpaquetar.trim().length()==0)
        {
            Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.ERROR, 'No se selecciono la opción para empaquetar' );
            Apexpages.addMessage( pm );
            actMensajes=true;
            return null;
        }
        
        if(this.opcEmpaquetar.equalsIgnoreCase(label.BI_COL_lblNuevo_Paquete)
            &&(this.opcEmpaquetarNuevo == null||this.opcEmpaquetarNuevo.trim().length()==0)){
            
            Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.ERROR, 'Debe seleccionar el nombre del nuevo paquete' );
            Apexpages.addMessage( pm );
            actMensajes=true;
            return null;
        }
    
        lmc=[Select m.Name, m.BI_COL_Numero_Viabilidad__c from BI_COL_manage_cons__c m where Name='ConsPaquetes'];
        num=lmc[0].BI_COL_Numero_Viabilidad__c+1;
        
        lstDsEmpaquetar=new List<BI_COL_Descripcion_de_servicio__c>();
        for(List<Wrapempaquetar> lstTemp:lstPrincipal)
        {
            String nombrePkg = this.opcEmpaquetar.equalsIgnoreCase(label.BI_COL_lblNuevo_Paquete) ? this.opcEmpaquetarNuevo: getNamePaquete(this.opcEmpaquetar);
            String CodPaquete = this.opcEmpaquetar.equalsIgnoreCase(label.BI_COL_lblNuevo_Paquete) ? 'PQ-'+num : this.opcEmpaquetar;
            buscarDsAempaquetar(lstTemp, nombrePkg,CodPaquete );
        }
        
        if(lstDsEmpaquetar.size()==0)
        {
            Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.ERROR, 'No se seleccionarón DS para empaquetar' );
            Apexpages.addMessage( pm );
            actMensajes=true;
            return null;
        }
        else
        {
            Savepoint sp;
            try
            {
                sp= Database.setSavepoint();
                update lstDsEmpaquetar;
                
                if(lmc!=null)
                {
                    lmc[0].BI_COL_Numero_Viabilidad__c=num;
                    database.update(lmc);
                }                   
                Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.CONFIRM, 'Se han empaquetado las DS seleccionadas en el paquete: '
                                        + this.opcEmpaquetar);
                Apexpages.addMessage( pm );
                actMensajes=true;
                this.opcEmpaquetar = '';
                constructor(this.segmento,this.mapEmpaquetamiento.values(),this.lstDS);
            }
            catch(System.exception e)
            {
                Apexpages.Message pm = new Apexpages.Message( Apexpages.Severity.INFO, 'se ha presentado un error informe al administrador del sistema: '+e.getMessage() );
                Apexpages.addMessage( pm );
                this.lstDS = getDS(this.cliente.Opportunities);
                constructor(this.segmento,this.mapEmpaquetamiento.values(),this.lstDS);
                actMensajes=true;
                Database.rollback(sp);
            }
        }
        return null;    
    }
    
    public List<BI_COL_Descripcion_de_servicio__c> getDS (List<Opportunity> lstOportunidades)
    {
        //List<BI_COL_Descripcion_de_servicio__c> lstDS = [Select n.Name, n.Id, n.BI_COL_Codigo_paquete__c,
        //                                //producto_telefonica__c, 
        //                                //producto_telefonica__r.linea__c, 
        //                                //producto_telefonica__r.producto__c,
        //                                //producto_telefonica__r.descripcion_referencia__c,
        //                                BI_COL_Nombre_de_paquete__c,BI_COL_Oportunidad__r.StageName
        //                                From BI_COL_Descripcion_de_servicio__c n
        //                                where Oportunidad__c = :lstOportunidades
        //                                //and BI_COL_Codigo_paquete__c = null
        //                                //and BI_COL_Oportunidad__r.StageName in ('F2 - Ganada','E6 - Ganada')
        //                                AND producto_telefonica__c <>   NULL 
        //                                limit 1000];
                                        
        return lstDS;
    }
    
    public void buscarDsAempaquetar(List<Wrapempaquetar> lstTemp, String opcEmpaquetar,String CodPaquete )
    {
        BI_COL_Descripcion_de_servicio__c dsTemp ;
        for(Wrapempaquetar wr: lstTemp )
        {
            if(wr.seleccion && !wr.disable)
            {
                dsTemp=mapIdXnuevaDS.get(wr.idDs);
                wr.disable=true;
                wr.paquete=CodPaquete;
                wr.NombrePaquete=opcEmpaquetar;
                dsTemp.BI_COL_Codigo_paquete__c=wr.paquete;
                dsTemp.BI_COL_Nombre_de_paquete__c=wr.NombrePaquete;
                lstDsEmpaquetar.add(dsTemp);
            }
        }
    }
    public void llenarPaquetes(WrapEmpaquetar dsTemp)
    {
        if(mapPaquetXDS.containsKey(dsTemp.paquete))
        {
            mapPaquetXDS.get(dsTemp.paquete).add(dsTemp);
        }
        else
        {
            List <Wrapempaquetar> lstWrapemp=new List<Wrapempaquetar>();
            lstWrapemp.add(dsTemp);
            mapPaquetXDS.put(dsTemp.paquete,lstWrapemp);
        }
    }
    
    public void llenarSelectOption(String segmento, List<BI_COL_Empaquetamiento__c> lstEmp, List<BI_COL_Descripcion_de_servicio__c> lstDS)
    {
        Set<String> setPckg = new Set<String>();
        this.lstPaquetesSel=new List<SelectOption>();
        this.lstPaquetesNuevo = new List<SelectOption>();
        this.lstPaquetesSel.add(new SelectOption('','--Ninguno--'));
        this.lstPaquetesNuevo.add(new SelectOption('','--Ninguno--'));
        
        //Opciones de paquete segun segmento
        for(BI_COL_Empaquetamiento__c emp : mapEmpaquetamiento.values()){
            if(emp.BI_COL_Segmento__c.equalsIgnoreCase(segmento))
                this.lstPaquetesNuevo.add(new SelectOption(emp.Name,emp.Name)); 
        }
        
        //Paquetes creados
        for(BI_COL_Descripcion_de_servicio__c ds : lstDS){
            if(ds.BI_COL_Codigo_paquete__c !=null&&ds.BI_COL_Codigo_paquete__c.trim().length()>0)
                setPckg.add(ds.BI_COL_Codigo_paquete__c);
        }
        
        //Iterar nuevos paquetes y agregarlos a la lista
        for(String opcPckg : setPckg){
            this.lstPaquetesSel.add(new SelectOption(opcPckg,opcPckg));
        }
        
        //Llenar con ultima opcion, nuevo paquete
        this.lstPaquetesSel.add(new SelectOption(label.BI_COL_lblNuevo_Paquete,label.BI_COL_lblNuevo_Paquete));
        
    }
    
    public boolean getHabilitarPaquete(){
        if(this.opcEmpaquetar ==label.BI_COL_lblNuevo_Paquete)
            return false;
        
        else{
            this.opcEmpaquetarNuevo = '';
            return true;
        }
    }
    
    /**
    * Posiciona el registro segun el numero de pagina
    * @param newPageIndex Indice de la pagina
    * @return 
    */
    private void BindData(Integer newPageIndex)
    {
        try
        {
            pageEmpaquetar = new List<Wrapempaquetar>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            
            if (newPageIndex > pageNumber)
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
            
            for(Wrapempaquetar wMS : lstMostrar)
            {
                counter++;
                if (counter > min && counter <= max){
                    pageEmpaquetar.add(wMS);
                }
                    
            }
            pageNumber = newPageIndex;
            
            if (pageEmpaquetar == null || pageEmpaquetar.size() <= 0)
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Los daros no estan disponibles para visualizar'));
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }   
    }
    
    /**
    * Acción para pasar a la siguiente página de registros
    * @return Pagereference
    */
    public PageReference nextBtnClick() {
            BindData(pageNumber + 1);
        return null;
    
    }
    
    /**
    * Acción para pasar a la anterior página de registros
    * @return Pagereference
    */
    public PageReference previousBtnClick() {
            BindData(pageNumber - 1);
        return null;
    
    }
    
    public Integer getPageNumber()
    {
        return pageNumber;
    }
    
    /**
    * Obtiene el numer de paginas de la tabla 
    * @return numero de paginas
    */
    public Integer getTotalPageNumber()
    {
        if (totalPageNumber == 0 && lstMostrar !=null)
        {
            totalPageNumber = lstMostrar.size() / pageSize;
            Integer mod = lstMostrar.size() - (totalPageNumber * pageSize);
            if (mod > 0)
            totalPageNumber++;
        }
        return totalPageNumber;
    }
    
    public Boolean getPreviousButtonEnabled()
    {
        return !(pageNumber > 1);
    }
    
    public Boolean getNextButtonDisabled()
    {
        if (lstMostrar == null){    return true;}
        else{return ((pageNumber * pageSize) >= lstMostrar.size());}
    
    }
    
    public static String getNamePaquete(String CodPaquete)
    {
        List<BI_COL_Descripcion_de_servicio__c> lstDS= [SELECT BI_COL_Nombre_de_paquete__c
                                        FROM BI_COL_Descripcion_de_servicio__c
                                        WHERE BI_COL_Codigo_paquete__c=:CodPaquete LIMIT 1];
        if(lstDS.size()>0)
        {
            return lstDS[0].BI_COL_Nombre_de_paquete__c;
        }
        else
        {
            return 'N/A';
        }
    }
}