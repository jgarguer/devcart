@isTest
private class PCA_IdeasCornerDetailController_TEST {

    //GMN 09/05/2016 - BI_TestUtils.throw_exception = false added
    static{
        BI_TestUtils.throw_exception = false;
    }
    //END GMN 09/05/2016

    static testMethod void getloadinfo() {
        
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
         User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
         Community comm = [Select Id from Community where Name= :LABEL.PCA_Nombre_comunidad_ideas];
         Network netw = [Select Id from Network where Name= 'Empresas Platino'];
         Profile prof1 = [Select Id from Profile where Name='BI_Customer Communities'];
	    //insert usr;
	    system.debug('usr: '+usr);
	    User user1;
	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais); 
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            
            user1 = BI_DataLoad.loadPortalUser(con[0].Id, prof1.Id);
            
	    	system.runAs(user1){
	    		
	    		PageReference pageRef = new PageReference('PCA_IdeasCornerDetail');
	       		Test.setCurrentPage(pageRef);
	       		
	    		Idea idea1 = new Idea(Body='Idea test',Title='Idea',CommunityId=comm.Id);
	    		insert idea1;
	    		IdeaComment ideaComm = new IdeaComment(CommentBody='Comentario test',IdeaId=idea1.Id);
	    		insert ideaComm;  
	    		ApexPages.currentPage().getParameters().put('Id', idea1.Id);
        		PCA_IdeasCornerDetailController constructor = new PCA_IdeasCornerDetailController();
        		BI_TestUtils.throw_exception = false;
        		constructor.checkPermissions2();
        		constructor.getRecord();
        		constructor.loadInfoDetail();
        		constructor.voteIdea();
        		constructor.recargar();
        		constructor.goToIdeas();
                system.assertEquals(idea1.Id,[SELECT Id,Body From Idea Where Title = 'Idea'].Id);
	    	}
	    }
    }
   static testMethod void getloadinfo2() {
        
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
         User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
         Community comm = [Select Id from Community where Name= :LABEL.PCA_Nombre_comunidad_ideas];
         Network netw = [Select Id from Network where Name= 'Empresas Platino'];
         Profile prof1 = [Select Id from Profile where Name='BI_Customer Communities'];
	    //insert usr;
	    system.debug('usr: '+usr);
	    User user1;
	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais); 
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            
            user1 = BI_DataLoad.loadPortalUser(con[0].Id, prof1.Id);	    	
	    	system.runAs(user1){
	    		
	    		PageReference pageRef = new PageReference('PCA_IdeasCornerDetail');
	       		Test.setCurrentPage(pageRef);
	       		
	    		Idea idea1 = new Idea(Body='Idea test2',Title='Idea2',CommunityId=comm.Id);
	    		insert idea1;
	    		ApexPages.currentPage().getParameters().put('Id', idea1.Id);
        		PCA_IdeasCornerDetailController constructor = new PCA_IdeasCornerDetailController();
        		BI_TestUtils.throw_exception = false;
        		constructor.checkPermissions2();
        		constructor.getRecord();
        		constructor.loadInfoDetail();
        		constructor.voteIdea();
        		constructor.createComment();
        		constructor.recargar();
        		constructor.goToIdeas();

	    	}
	    }
    }
   static testMethod void getloadinfo3() {
        
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
         User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
         Community comm = [Select Id from Community where Name= :LABEL.PCA_Nombre_comunidad_ideas];
         Network netw = [Select Id from Network where Name= 'Empresas Platino'];
         Profile prof1 = [Select Id from Profile where Name='BI_Customer Communities'];
	    //insert usr;
	    system.debug('usr: '+usr);
	    User user1;
	    system.runAs(usr){
	    	List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
	    	List<Account> acc = BI_DataLoad.loadAccounts(1, lst_pais); 
            List<Account> accList = new List<Account>();
            for(Account item: acc){
            	item.OwnerId = usr.Id;
            	accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            
            user1 = BI_DataLoad.loadPortalUser(con[0].Id, prof1.Id);	    	
	    	system.runAs(user1){
	    		
	    		PageReference pageRef = new PageReference('PCA_IdeasCornerDetail');
	       		Test.setCurrentPage(pageRef);
	       		
	    		Idea idea1 = new Idea(Body='Idea test2',Title='Idea2',CommunityId=comm.Id);
	    		insert idea1;
	    		ApexPages.currentPage().getParameters().put('Id', idea1.Id);
        		PCA_IdeasCornerDetailController constructor = new PCA_IdeasCornerDetailController();
        		BI_TestUtils.throw_exception = true;
        		constructor.checkPermissions2();
        		constructor.getRecord();
        		constructor.loadInfoDetail();
        		constructor.voteIdea();
        		constructor.createComment();
        		constructor.recargar();
        		constructor.goToIdeas();
        		constructor.inicializeNewComment();
        		
	    	}
	    }
    }    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}