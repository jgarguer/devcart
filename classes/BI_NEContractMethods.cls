public without sharing class BI_NEContractMethods {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   Methods exceuted by NEContract Trigger
    
    History:
    
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   Validation to avoid modification on NE__Contract__c
    IN:           List <Contract>
    OUT:               
    History:
    
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version
    24/10/2017        Gawron, Julian    Adding BI_Standard_PER to query
    02/11/2017        Gawron, Julián    Adding Label.BI_PER_SinPermisos
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void verificarPermisos(List<NE__Contract__c> news , List<NE__Contract__c> olds)
    {
    	Set<String> set_creacion = new Set<String>{Label.BI_Administrador,'Administrador de Contrato', Label.BI_SuperUsuario};
    	Set<String> set_modificacion = new Set<String>{Label.BI_Administrador, 'Administrador de Contrato', Label.BI_SuperUsuario};
    	Set<String> set_eliminacion = new Set<String>{Label.BI_Administrador, Label.BI_SuperUsuario};

        try
        { 
          if(BI_TestUtils.isRunningTest()){ throw new BI_Exception('Test');}
          List<User> me = [SELECT Id, BI_Permisos__c FROM User WHERE Id = :UserInfo.getUserId() and Profile.name = 'BI_Standard_PER' limit 1];
          if(me.isEmpty()) return; //Si la lista está vacía, el usuario no era de Perú, salimos.

           if(olds == null){
           		//Insert
           		if(!set_creacion.contains(me[0].BI_Permisos__c)){
               		for(NE__Contract__c contr : news){
               			contr.addError(Label.BI_PER_SinPermisos);
            		}
           		}
           	}else if(news == null){
           		//delete
           		if(!set_eliminacion.contains(me[0].BI_Permisos__c)){
               		for(NE__Contract__c contr : olds){
               			contr.addError(Label.BI_PER_SinPermisos);
            		}
           		}
           	}else{
           		if(!set_modificacion.contains(me[0].BI_Permisos__c)){
               		for(NE__Contract__c contr : news){
               			contr.addError(Label.BI_PER_SinPermisos);
            		}
           		}
           	}
        }
        catch (exception Exc)
        {
           BI_LogHelper.generate_BILog('BI_NEContractMethods.verificarPermisos', 'BI_EN', Exc, 'Trigger');
        } 
    }    

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   SetStartEndDateContractHeader old trigger on NE__Contract__c
    IN:           List <Contract>
    OUT:               
    History:
    
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void SetStartEndDateContractHeader (List<NE__Contract__c> news, List<NE__Contract__c> olds,  boolean isInsert , boolean isUpdate) {
          
             list<String> chToSearch = new list<String>();
             for(NE__Contract__c contract : news)
                 chToSearch.add(contract.NE__Contract_Header__c);
             
             map<String,NE__Contract_Header__c> mapOfCh  =   new map<String,NE__Contract_Header__c>([SELECT Id, NE__MaxVersion__c, Max_Active_Version_Start_Date__c, Max_Active_Version_End_Date__c 
                                                                                                  FROM NE__Contract_Header__c
                                                                                                  WHERE Id =: chToSearch]); 
             
             list<NE__Contract_Header__c> contractHeadersToUpdate = new list<NE__Contract_Header__c>();
             for(NE__Contract__c contract : news) {
                 
                 NE__Contract_Header__c ch = mapOfCh.get(contract.NE__Contract_Header__c);
                 
                 if(isInsert || (isUpdate && contract.NE__Version__c == ch.NE__MaxVersion__c)) {
                     ch.Max_Active_Version_Start_Date__c = contract.NE__Start_Date__c;
                     ch.Max_Active_Version_End_Date__c = contract.NE__End_Date__c;
                     contractHeadersToUpdate.add(ch);
                 }
                 
             }
             
             if(contractHeadersToUpdate.size() > 0)
             {
                 Set<NE__Contract_Header__c> hSet   = new Set<NE__Contract_Header__c>();
                 hSet.addAll(contractHeadersToUpdate);            
                 
                 list<NE__Contract_Header__c> cHToUpd =   new list<NE__Contract_Header__c>();
                 cHToUpd.addAll(hSet);  
                 
                 update contractHeadersToUpdate;    
             }  
     } //fin de SetStartEndDateContractHeader
}