@isTest
public class PCA_Order_Redirect_Controller_TEST {
    
    @isTest(SeeAllData=true) static void createRegistrationTest() {

        BI_TestUtils.throw_exception = false;
         BI_MigrationHelper.skipAllTriggers();
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        NE__Catalog_Item__c testCatalogItem = TGS_Dummy_Test_Data.dummyCatalogItem();
        System.runAs(userTest){
            NE.NewConfigurationController configuration = new NE.NewConfigurationController();
            Test.startTest();
            ApexPages.currentPage().getParameters().put('cId', testCatalogItem.Id);
            ApexPages.currentPage().getParameters().put('quantity', '1');
            ApexPages.currentPage().getParameters().put('cType', 'New');
            ApexPages.currentPage().getParameters().put('isExtBw', 'true');
            PCA_Order_Redirect_Controller controller = new PCA_Order_Redirect_Controller(configuration);
            controller.redirectToConfigurator();
            BI_MigrationHelper.cleanSkippedTriggers();
            Test.stopTest();
        }
    }
    
    @isTest(SeeAllData=true) static void createModificationTest() {

        BI_TestUtils.throw_exception = false;
         BI_MigrationHelper.skipAllTriggers();
        User userTest = TGS_Dummy_Test_Data.getPortalUser(TGS_Dummy_Test_Data.PortalType.PowerCustomerSuccess, null, true);
        System.runAs(userTest){
            NE.NewConfigurationController configuration = new NE.NewConfigurationController();
            Test.startTest();
            ApexPages.currentPage().getParameters().put('cType', 'Change');
            ApexPages.currentPage().getParameters().put('isExtBw', 'true');
            PCA_Order_Redirect_Controller controller = new PCA_Order_Redirect_Controller(configuration);
            controller.redirectToConfigurator();
            BI_MigrationHelper.cleanSkippedTriggers();
            Test.stopTest();
        }
    }
    
    static testMethod void changeLanguageTest() {

        BI_TestUtils.throw_exception = false;
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        system.runAs(usr){
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            
            List<Contact> con = BI_DataLoad.loadContacts(1, accList);
            User user1 = BI_DataLoad.loadSeveralPortalUsers(con[0].Id, BI_DataLoad.searchPortalProfile());          
            system.runAs(user1){
                Test.startTest();
                BI_TestUtils.throw_exception = false;
                Test.stopTest();
                PCA_Order_Redirect_Controller controller = new PCA_Order_Redirect_Controller();
                String ppurl=controller.profilePhotoURL;
                User up=controller.usuarioPortal;
                system.assert(up != null);
                controller.changeToES();
                controller.changeToDE();
                controller.changeToFR();
                controller.changeToEN();
                controller.changeToPT();
            }
        }
    }
}