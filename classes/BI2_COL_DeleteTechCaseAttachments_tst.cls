/**
**************************************************************************************************************
* @company          Avanxo Colombia
* @author           Antonio Torres href=<atorres@avanxo.com>
* @project          Telefonica BI_EN Fase 2 Colombia
* @name             BI2_COL_DeleteTechCaseAttachments_tst
* @description      Test class for "BI2_COL_DeleteTechCaseAttachments_sch" schedulable class.
* @dependencies     Class BI2_COL_DeleteTechCaseAttachments_sch
* @changes (Version)
* --------   ---   ----------   ---------------------------   ------------------------------------------------
*            No.   Date         Author                        Description
* --------   ---   ----------   ---------------------------   ------------------------------------------------
* @version   1.0   2017-03-01   Antonio Torres (AT)           Initial version.
**************************************************************************************************************
**/

@isTest
private class BI2_COL_DeleteTechCaseAttachments_tst {
    @isTest
    static void scheduleTest() {
        Test.StartTest();
            BI2_COL_DeleteTechCaseAttachments_sch objDeleteTechCaseAttachmentsSchedule = new BI2_COL_DeleteTechCaseAttachments_sch();
            String strSchedule = '0 0 23 * * ?';
            System.schedule('Test Delete Technical Case Attachments', strSchedule, objDeleteTechCaseAttachmentsSchedule);
        Test.stopTest();
    }
}