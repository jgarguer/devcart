/****************************************************************************************************
    Información general
    -------------------
    author: Javier Tibamoza Cubillos
    company: Avanxo Colombia
    Project: Implementación Salesforce
    Customer: TELEFONICA
    Description: 
    
    Information about changes (versions)
    -------------------------------------
    Number    Dates           Author                       Description
    ------    --------        --------------------------   -----------
    1.0       10-Abr-2015     Javier Tibamoza              Creación de la Clase
****************************************************************************************************/
public class BI_COL_DAVOXCuentaAccount_ctr 
{
    public String NombreCliente { get; set; }
    public String cuentas {get;set;}
    public String separador = '';
    public String respuestaxml{get;set;}
    public String CuentaDx;
    public String cuentaNueva = '';
    public String idMS {get;set;}
       public String idOpp {get;set;}

    Integer pageNumber {get;set;}
    Integer pageSize {get;set;}
    Integer totalPageNumber {get;set;}
    
    public string tipo = 'CONSULTA_CUENTA';
    public string packcode = 'ASD654AS';

    public BI_COL_Modificacion_de_Servicio__c dsto;

    public Opportunity dstoMS;

    public List<String> lstCuentas { get; set; }
    public List<String> lDireccion = new List<String>();
    public List<String> lstLabelCuenta = new List<String>();
    public List<SelectOption> options {get;set;}

    public Map<String,String> mapCuenta = new Map<String,String>();
    public list<BI_COL_Modificacion_de_Servicio__c> lstMS; 

    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public BI_COL_DAVOXCuentaAccount_ctr()
    {

    }

    public void cuentaCesionContr()
    {
        pageNumber=0;
        pageSize=50;
        totalPageNumber=0;
        string id=ApexPages.currentPage().getParameters().get('id');
        Account acc =new Account();
        acc=[select id,BI_No_Identificador_fiscal__c from Account where id=:id];
        
        cuentas=counsultarCuentasWS(acc.BI_No_Identificador_fiscal__c);
        
        XmlStreamReader xml=new XmlStreamReader(cuentas);
            try
            {
                Dom.Document doc = new Dom.Document();
                doc.load(cuentas);
                Dom.XMLNode WS = doc.getRootElement();
                List<dom.XMLNode> lsxmlValues=WS.getChildElements(); 
                for(dom.XMLNode dxm:lsxmlValues)
                {
                    if(dxm.getName()==Label.BI_COL_LbSeparator)
                    {
                        separador=dxm.getText();
                        System.debug('\nSEPARATOR'+separador);
                    }
                    if(dxm.getName()==Label.BI_COL_LbValues)
                    {
                        List<dom.XMLNode> values=dxm.getChildElements();
                        for(dom.XMLNode dxmv: values)
                        {
                            respuestaxml=dxmv.getText();
                            System.debug('\nrespuestaxml'+respuestaxml);
                            
                        }
                    }
                }
                armarOpciones();
            } 
            catch(Exception ex)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Error al tratar de conectarse con DAVOX, por favor comuníquese con el administrador: '+cuentas+ex.getMessage()));
            }
    
    }
    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public void fnCargarDatos()
    {
        string sMSMError = '';
        pageNumber = 0;
        pageSize = 50;
        totalPageNumber = 0;
        String idAcc = ApexPages.currentPage().getParameters().get('id');
        System.debug('id de prueba =======> '+idAcc);
        
        idMS = ApexPages.currentPage().getParameters().get('idMS');
        System.debug('idMS =======> '+idMS);
        dsto = new BI_COL_Modificacion_de_Servicio__c( id = idMS );

        //START 28/10/2016 ANCR: Id de la oportunidad

        idOpp = ApexPages.currentPage().getParameters().get('idOpp');
        System.debug('idOpp1 =======> '+idOpp);

        //END 28/10/2016 ANCR: Id de la oportunidad

        if (idMS != null ) // 28/10/2016 ANCR: Se incluye la condición de MS
        {
       
            lstMS = [
                Select BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c, BI_COL_Oportunidad__r.Account.Name 
                From BI_COL_Modificacion_de_Servicio__c 
                Where ID =: dsto.Id
                Limit 1];
                  System.debug('\n\n##lstMS: '+ lstMS +'\n\n');
                  System.debug('\n\n===== dsto.Id :  '+ dsto.Id +'\n\n');
            try{    

            list<Account> lstAcc = [ Select BI_No_Identificador_fiscal__c, Name From Account Where id =: idAcc Limit 1];
                 System.debug('CONSULTA CUENTA ====== '+lstAcc );
            }catch(Exception e)
                {
                    System.debug(' ERROR EN CONSULTA CUENTA ====== \n '+e.getMessage());
                }
            
            System.debug('\n\n idms '+idms +'\n\n');
            if(lstMS.size() > 0)
            {
                NombreCliente = lstMS[0].BI_COL_Oportunidad__r.Account.Name;
                cuentas = lstMS[0].BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c;
                System.debug('\n\n##cuentas: '+ cuentas +'\n\n');
                if( cuentas != null )
                {   
                    cuentas = counsultarCuentasWS( cuentas );
                    
                    XmlStreamReader xml = new XmlStreamReader( cuentas );
                    try
                    {
                        Dom.Document doc = new Dom.Document();
                        doc.load( cuentas );
                        Dom.XMLNode WS = doc.getRootElement();
                        list<dom.XMLNode> lsxmlValues = WS.getChildElements(); 
                        //
                        for( dom.XMLNode dxm : lsxmlValues )
                        {
                            if( dxm.getName() == 'SEPARATOR' )
                                separador = dxm.getText();
                            //
                            if( dxm.getName() == 'VALUES' )
                            {
                                list<dom.XMLNode> values = dxm.getChildElements();

                                for( dom.XMLNode dxmv: values )
                                    respuestaxml = dxmv.getText();
                            }
                        }
                        armarOpciones();
                    }
                    catch(Exception ex)
                    {
                        sMSMError = 'Error al tratar de conectarse con DAVOX, por favor comuníquese con el administrador: ' + 
                            ex.getMessage() + ex.getStackTraceString() + ' Linea: ' + ex.getLineNumber();
                        ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.FATAL, sMSMError ) );
                    } 
                }
                else
                {
                    sMSMError = 'La modificacion de servicio no tiene Oportunidad Asociada';
                    ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.FATAL, sMSMError ) );
                }
            }
            else
            {
                NombreCliente = 'Cliente no existe No tiene modificaciones de Servicio';
            }
        }else if (idOpp != null ){ // 28/10/2016 ANCR: Se incluye la condición de Opp

            System.debug('id idOpp=======> '+idOpp);
            fnCargarDatosMS(idOpp); // 28/10/2016 ANCR: Se incluye llamado al metodo fncargardatos
        }

    }


    /***
    * @Author: Andrea Castañeda     
    * @Company: New Energy Aborda   
    * @Description: Metodo para traer enviar el parametro del Id de la Oportunidad 
    * @History: 
    * Date           |   Author  |   Description
    * 18/10/2016         ANCR        Initial Version
    ***/
    public void fnCargarDatosMS(string idOpp)  // 28/10/2016 ANCR: Se incluye parametro de id de la oportunidad
    {
        //START 18/10/2016 ANCR: Arma el xml para consumir el servicio web DAVOX
        string sMSMError = '';
        pageNumber = 0;
        pageSize = 50;
        totalPageNumber = 0;
       
        String idAcc = ApexPages.currentPage().getParameters().get('id');
        System.debug('id de prueba =======> '+idAcc);

        // 18/10/2016 ANCR: Obtiene el id de la oportunidad

        // 28/10/2016 ANCR: Se incluye parametro de id de la oportunidad
        //idOpp = ApexPages.currentPage().getParameters().get('idOpp');
        //System.debug('idOpp =======> '+idOpp);
        dstoMS = new Opportunity( id = idOpp );

        // 18/10/2016 ANCR: Consulta las oportunidades de acuerdo al id de la oportundiad que viene por parametro
              
        list<Opportunity> lstOpp = [ Select Id, Account.BI_No_Identificador_fiscal__c, Account.Name, Name 
                                     From Opportunity Where id =: dstoMS.Id Limit 1];

         System.debug(' opportunidadMS1 \n '+lstOpp);

        try{

        list<Account> lstAcc = [ Select BI_No_Identificador_fiscal__c, Name From Account Where id =: idAcc Limit 1];
             System.debug('CONSULTA CUENTA MS1====== '+lstAcc );
        }catch(Exception e)
            {
                System.debug(' ERROR EN CONSULTA CUENTA MS1====== \n '+e.getMessage());
            }
        
        System.debug('\n\n idOpp111 '+idOpp +'\n\n');
        System.debug('\n\n lstopp11 '+lstOpp.size() +'\n\n');

        if(lstOpp.size() > 0)
        {
            NombreCliente = lstOpp[0].Account.Name;
            System.debug('\n\n nombreCliente ms '+NombreCliente +'\n\n');
            cuentas = lstOpp[0].Account.BI_No_Identificador_fiscal__c;

             System.debug('\n\n##cuentas11: '+ cuentas +'\n\n');
            if( cuentas != null )
            {   
                cuentas = counsultarCuentasWS( cuentas );
                
                System.debug('\n\n##cuentasWS: '+ cuentas +'\n\n');

                XmlStreamReader xml = new XmlStreamReader( cuentas );
                try
                {
                    Dom.Document doc = new Dom.Document();
                    doc.load( cuentas );
                    Dom.XMLNode WS = doc.getRootElement();
                    list<dom.XMLNode> lsxmlValues = WS.getChildElements(); 
                    //
                    for( dom.XMLNode dxm : lsxmlValues )
                    {
                        if( dxm.getName() == 'SEPARATOR' )
                            separador = dxm.getText();
                        //
                        if( dxm.getName() == 'VALUES' )
                        {
                            list<dom.XMLNode> values = dxm.getChildElements();

                            for( dom.XMLNode dxmv: values )
                                respuestaxml = dxmv.getText();
                        }
                    }
                    armarOpciones();
                }
                catch(Exception ex)
                {
                    sMSMError = 'Error al tratar de conectarse con DAVOX, por favor comuníquese con el administrador: ' + 
                        ex.getMessage() + ex.getStackTraceString() + ' Linea: ' + ex.getLineNumber();
                    ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.FATAL, sMSMError ) );
                } 
            }
            else
            {
                sMSMError = 'La Oportunidad no tiene cuentas asociadas';
                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.FATAL, sMSMError ) );
            }
        }
        else
        {
            NombreCliente = 'Cliente no existe';
        }
         //START 18/10/2016 ANCR: fin  Arma el xml para consumir el servicio web DAVOX
    }
       
    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public String counsultarCuentasWS( String nroIdentificacion )
    {
        String cuentas = '';
        try
        {
            BI_COL_Davox_wsdl.BasicServiceWSSOAP wsDavox = new BI_COL_Davox_wsdl.BasicServiceWSSOAP();
            wsDavox.timeout_x = 120000;
            String xml = armadoXML( nroIdentificacion );
            System.debug('\n\n xml:'+xml+'\n\n');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,xml));
            //xml=xml.replace('<', '&lt;');
            //xml=xml.replace('>','&gt;');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,xml));
            cuentas = wsDavox.find(xml);
            System.debug('\n\n xml CONSULTA:'+cuentas+'\n\n');
            //String xmlParaenvio=xml.replace('<', '&lt;');
            //xmlParaenvio=xmlParaenvio.replace('>','&gt;');
            //System.debug('\n\n xml xmlParaenvio: '+xmlParaenvio+'\n\n');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,cuentas));
            
        } 
        catch( Exception ex ) 
        {
            cuentas = ex.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,cuentas));
        }
        return cuentas;
    }
    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public String armadoXML(String nroIdentificacion)
    {
        return BI_COL_Validator_cls.armadoXML( nroIdentificacion, 'DAVOXCuentasAccount', packCode, tipo);
    }
    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public void armarOpciones()
    {
        System.debug('=================>>>>> respuestaxml ** \n '+respuestaxml);
        if( respuestaxml != null )
        {
            List<String> valores = respuestaxml.split( separador );
            lstCuentas = new List<String>();

            System.debug('=================>>>>> lista de valores ** \n '+valores);
            System.debug('=================>>>>> lista de valores ** \n '+valores.size());
            if( valores.size() > 6 )
            {   
                lstCuentas.add( valores[1] );
                System.debug('=================>>>>> lista de cuentas \n '+lstCuentas);
                lstLabelCuenta.add('Nueva Cuenta ' + valores[1] );
                cuentaNueva = valores[1];
                List<String> lvalCuenta=valores[2].split(',');
                List<String> lvalCiudad=valores[3].split(',');
                lDireccion = valores[4].split(',');
                List<String> lTipoNegocio=valores[5].split(',');
                List<String> lstCicloFact=valores[6].split(',');
                List<String> lstTapaDetalle=valores[7].split(',');
                List<String> lstCiclo=valores[8].split(',');
                List<String> lstNombreCiclo=valores[9].split(',');
                Integer cont = 0;
                String cicloFact = '';
                System.debug('=================>>>>> lista de valores \n '+valores);
                for( String vCuenta : lvalCuenta )
                {
                    String tipoNegocio;
                    if( lTipoNegocio[cont] == 'SC' )
                        tipoNegocio = 'SERVICIOS CORPORATIVO';
                    else
                        tipoNegocio = 'VOZ CORPORATIVO';
                    
                    if( lstCicloFact[cont] == 'VE' )
                        cicloFact='VENCIDA';
                    else
                        cicloFact='ANTICIPADA';
                    
                    lstCuentas.add( vCuenta );
                    
                    lstLabelCuenta.add( 'Cuenta ' + vCuenta + 
                        '  -  ' + lvalCiudad[cont] + '  -  ' + 
                        lDireccion[cont] + '  -  ' + tipoNegocio + 
                        ' - ' + cicloFact + ' - ' + lstTapaDetalle[cont] +
                        ' - CICLO:' + lstCiclo[cont] + ' ' + lstNombreCiclo[cont] );
                    
                    mapCuenta.put( vCuenta, lvalCiudad[cont] + 'Ç' + lDireccion[cont] );
                    cont++;
                }
                ViewData();
            }
            else if( valores.size() > 1 )
            {
                lstCuentas.add(valores[1]);
                lstLabelCuenta.add('Nueva Cuenta ' + valores[1] );
                cuentaNueva = valores[1];
                ViewData();
            }
            else
            {
                lstCuentas.add('0');
                lstLabelCuenta.add('Nueva Cuenta 0');
                cuentaNueva = '0';
                ViewData();
            }
        }
    }
    /***
    * @Author: 
    * @Company: 
    * @Description: 
    * @History: 
    * Date      |   Author  |   Description
    * 
    ***/
    public PageReference guardar()
    {
        PageReference pgRe;
        System.debug('\n@-->>CuentaDx\n'+CuentaDx);

        if( CuentaDx != null )
        {
            //START 18/10/2016 ANCR: Validación si llega el Id de la MS
            if(idms != null)
            {
            //END 18/10/2016 ANCR: Validación si llega el Id de la MS
           
                List<BI_COL_Modificacion_de_Servicio__c> updste = [
                    select  Id, BI_COL_Clasificacion_Servicio__c,
                            BI_COL_Oportunidad__r.Account.BI_No_Identificador_fiscal__c,
                            /*BI_COL_Cuenta_facturar_davox__c,*/ BI_COL_Cuenta_facturar_davox1__c
                    from    BI_COL_Modificacion_de_Servicio__c 
                    where   Id =: idms ]; //10/11/2016 ANRC: Se comentó el campo antiguo y se adiciono nuevo campo davox1
                System.debug('\n@-->>updste\n'+updste);

                if( updste.size() > 0 )
                {
                    String data = mapCuenta.get( CuentaDx );
                   //updste[0].BI_COL_Cuenta_facturar_davox__c = CuentaDx; //10/11/2016 ANRC: Se comentó el campo antiguo
                   updste[0].BI_COL_Cuenta_facturar_davox1__c = CuentaDx; //10/11/2016 ANRC: Se adiciono nuevo campo davox1

           
                    try
                    {
                        update updste;
                        pgRe = new PageReference( '/' + idms );
                        pgRe.setRedirect( true );
                    } 
                    catch( Exception ex )
                    {
                        pgRe = null;
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage() ) );
                    }
                }
            //START 18/10/2016 ANCR: Validación si llega el Id de la Oppty
            }else if (idOpp != null){
               
                try
                {
                    //START 03/11/2016 ANCR:
                   system.debug('CuentaDx*********'+CuentaDx);
                    pgRe = new PageReference( '/apex/BI_COL_SelectClassificationServices_pag?idOpp=' + idOpp+'&CuentaDx='+CuentaDx); //28/10/2016 ANCR: Se incluye redireccion a la visualforce
                    
                    pgRe.setRedirect( true );
                    //END 03/11/2016 ANCR:
                } 
                catch( Exception ex )
                {
                    pgRe = null;
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage() ) );
                }
            }
            //END 18/10/2016 ANCR: Validación si llega el Id de la MS

        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Por Favor escoja una cuenta'));
        }
        return pgRe;
    }

    public Integer getPageNumber()
    {
        return pageNumber;
    }

    public Integer getPageSize()
    {
        return pageSize;
    }

    public Boolean getPreviousButtonEnabled()
    {
        return !( pageNumber > 1);
    }

    public Boolean getNextButtonDisabled()
    {
        if (this.lstCuentas == null)
        {
            return true;
        }
        else
        {
            return ((pageNumber * pageSize) >= lstCuentas.size());
        }

    }

    public Integer getTotalPageNumber()
    {
        if (totalPageNumber == 0 && this.lstCuentas !=null)
        {
            totalPageNumber = this.lstCuentas.size() / pageSize;
            Integer mod = this.lstCuentas.size() - (totalPageNumber * pageSize);
            if (mod > 0)
            totalPageNumber++;
        }
        return totalPageNumber;
    }

    public PageReference previousBtnClick() 
    {
        if(pageNumber != null)
            BindData(pageNumber - 1);
        return null;
    
    }

    public PageReference nextBtnClick() 
    {
        if(pageNumber != null)
            BindData(pageNumber + 1);
        return null;

    }

    public PageReference ViewData()
    {
        totalPageNumber = 0;
        BindData(1);
        return null;
    }

    public List<SelectOption> getItems( Integer desde, Integer hasta )
    {
        list<SelectOption> options = new list<SelectOption>();
        System.debug('=======================>>>>>>> lista de cuentas**** '+lstCuentas);
        System.debug('=======================>>>>>>> lista de lstLabelCuenta '+lstLabelCuenta);
        for( Integer i = desde; i < hasta; i++ )
            options.add(new SelectOption( lstCuentas[i], lstLabelCuenta[i] ) );
        return options;
    }
    
    public String getCuentaDx()
    {
        return CuentaDx;
    }

    public void setCuentaDx( String CuentaDx) { this.CuentaDx = CuentaDx; }

    /**
    * Posiciona el registro segun el numero de pagina
    * @param newPageIndex Indice de la pagina
    * @return 
    */
    public void BindData(Integer newPageIndex)
    {
        try
        {
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;

            if( newPageIndex > pageNumber )
            {
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }
            else
            {
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
            
            if( max>lstCuentas.size() )
                max = lstCuentas.size();
            
            options = getItems(min,max);
            pageNumber = newPageIndex;

            if( lstCuentas == null || lstCuentas.size() <= 0 )
                ApexPages.addmessage(new ApexPages.message( ApexPages.severity.INFO, Label.BI_COL_lblDatosNoDisponiblesVista ) );
        }
        catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()+' StackTrac '+ex.getStackTraceString() +' Linea'+ex.getLineNumber()));
        }   
    }
}