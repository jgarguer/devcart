public with sharing class BI_PlanDeAccion_PDF_Ctrl {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Controller for "BI_PlanDeAccion_PDF" page

    History: 

    <Date>                          <Author>                    <Change Description>
    05/02/2014                      Micah Burgos                Initial Version
    30/06/2015                      Francisco Ayllon            Changes due to client petition
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public BI_Plan_de_accion__c coordCliente          {get; set;}

    public list<Note>              lst_notes          {get; set;}
    public list<Attachment>        lst_attachs        {get; set;}
    public list<ActivityHistory>   lst_closedAct      {get; set;}
    public list<OpenActivity>      lst_openAct        {get; set;}

    public list<Task>              lst_taskResp       {get; set;}
    public list<Task>              lst_taskNoResp     {get; set;}
    public list<Event>             lst_event          {get; set;}
    //public list<EventRelation>     lst_eventRelation      {get; set;}

    public list<AcceptedEventRelation>     lst_eventRelationTypeAccepted   {get; set;}
    public list<UndecidedEventRelation>    lst_eventRelationTypeUndecided  {get; set;}
    public list<DeclinedEventRelation>     lst_eventRelationTypeDeclined   {get; set;}

    public BI_PlanDeAccion_PDF_Ctrl(ApexPages.StandardController Controller){

        coordCliente = (BI_Plan_de_accion__c) controller.getRecord();

        //List <BI_Plan_de_accion__c> lst_coordCliente = [SELECT Id ,(Select Id, IsNote, Title, Owner.Name, LastModifiedDate From NotesAndAttachments ORDER BY Title limit 500),
        //                                        (SELECT LastModifiedDate, Description, EndDateTime,ActivityDate, Subject, IsTask, Owner.Name FROM ActivityHistories order by ActivityDate desc, LastModifiedDate desc limit 500), 
        //                                        (SELECT Id, Who.Name, Status, Subject,BI_Categoria__c, ActivityDate, IsTask, Priority, Owner.Name FROM OpenActivities order by ActivityDate asc, LastModifiedDate desc limit 500) 
        //                                        FROM BI_Plan_de_accion__c WHERE Id =: coordCliente.Id limit 1];

        List <BI_Plan_de_accion__c> lst_coordCliente = [SELECT Id ,(SELECT Id, Title, Body, Owner.Name, LastModifiedDate, IsPrivate FROM Notes WHERE IsPrivate = false ORDER BY CreatedDate limit 500),
                                                                (SELECT Id, Name, Owner.Name, LastModifiedDate, IsPrivate FROM Attachments WHERE IsPrivate = false ORDER BY CreatedDate limit 500),
                                                                (SELECT ActivityDate,BI_Temas_tratados__c,BI_Tipo__c,Description,Id,IsArchived,Who.Name,IsClosed,IsDeleted,IsRecurrence,IsReminderSet,LastModifiedById,LastModifiedDate,Owner.Name,Priority,RecordType.Name,Status,Subject,Type,WhatCount,WhatId,WhoCount,WhoId,BI_Responsable_es_el_cliente__c  FROM Tasks), 
                                                                (SELECT Id,Description, BI_Temas_tratados__c, Type, WhatId,WhoId, Who.Name FROM Events WHERE IsChild = false AND RecordType.DeveloperName = 'BI_Asistentes_Plan_de_accion' ) 
                                                            FROM BI_Plan_de_accion__c WHERE Id =: coordCliente.Id limit 1];


        lst_notes = lst_coordCliente[0].Notes;
        lst_attachs = lst_coordCliente[0].Attachments;
        //lst_closedAct = lst_coordCliente[0].ActivityHistories;
        //lst_openAct = lst_coordCliente[0].OpenActivities;
        lst_taskResp = new List<Task>();
        lst_taskNoResp = new List<Task>();
        for(Task currenTask: lst_coordCliente[0].Tasks){
            if(currenTask.BI_Responsable_es_el_cliente__c){
                lst_taskNoResp.add(currenTask);
            }else{
                lst_taskResp.add(currenTask);
            }
        }
        lst_event = lst_coordCliente[0].Events;

        //for([SELCT]){

        //}


        Set<Id> set_eventIds = new Set<Id>();
        for(Event ev :lst_event){
            set_eventIds.add(ev.Id);
        }

        //lst_eventRelation = [SELECT Event.Subject,EventId,Id,IsDeleted,IsInvitee,IsParent,IsWhat,Relation.Name, RelationId,RespondedDate,Response,Status FROM EventRelation WHERE IsInvitee = true AND EventId IN :set_eventIds];

        lst_eventRelationTypeAccepted = [SELECT RelationId,Relation.Name, Type,RespondedDate,Response  FROM AcceptedEventRelation  WHERE EventId IN: set_eventIds];//AcceptedEventRelation
        lst_eventRelationTypeUndecided = [SELECT RelationId,Relation.Name,Type,RespondedDate,Response FROM UndecidedEventRelation WHERE EventId IN: set_eventIds];//UndecidedEventRelation
        lst_eventRelationTypeDeclined = [SELECT RelationId,Relation.Name, Type,RespondedDate,Response FROM DeclinedEventRelation  WHERE EventId IN: set_eventIds];//DeclinedEventRelation

        //SELECT Event.Subject,EventId,Id,IsDeleted,IsInvitee,IsParent,IsWhat,Relation.Name, RelationId,RespondedDate,Response,Status FROM EventRelation

        //for(Event evRel :[SELECT Id, (SELECT Relation.Name, Status FROM EventRelations) WHERE Id IN :set_eventIds]){

        //}

    }
}