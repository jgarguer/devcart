/*---------------------------------------------------------------------------------------------------------
Author:        Angel Felipe Santaliestra Pasias
Company:       Accenture.
Description:   Helper class that controls Bit2WinHUB__Bulk_Configuration_Request__c methods.

History:

<Date>                  <Author>					<Description>
22/03/2018				Angel F Santaliestra		Initial Version
-----------------------------------------------------------------------------------------------------------*/
public class BI_O4_BT_BulkConfigRequestMethods {
	/*---------------------------------------------------------------------------------------------------------
	Author:			Angel Felipe Santaliestra Pasias
	Company:		Accenture.
	Description:	Method that update the NE_OrderStatus__c of the Bit2WinHUB__Bulk_Configuration_Request__c


	History:

	<Date>                  <Author>					<Description>
	22/03/2018				Angel F Santaliestra		Initial Version
	03/04/2018				Manuel Ochoa				Added changes - launch method only when BCR is completed
	04/04/2018 				Manuel Ochoa 				Added NE__OptyId__c to query
	19/04/2018 				Manuel Ochoa 				Added set_IdOder and call to updateSumatoria
	08/05/2018				Carlos Vicente				Added if/else for API's and method
	-----------------------------------------------------------------------------------------------------------*/
	public static void checkStatus(List<Bit2WinHUB__Bulk_Configuration_Request__c> listItems) {
		System.debug('Trigger <> BI_B2W_BulkConfigRequestMethods##checkStatus =>> Trigger.new : ' + listItems);
		List<Id> listIds = new List<Id>();
        Set <Id> setIdOrders = new Set <Id>(); // 19-04-2018 - Manuel Ochoa - add set id 
		for (Bit2WinHUB__Bulk_Configuration_Request__c item : listItems) {
			if (item.Bit2WinHUB__Status__c == 'Completed') {
                system.debug('BCR Completed: ' + item.id);
				listIds.add(item.Bit2WinHUB__Configuration__c);
			}
		}
        
        //03/04/2018				Manuel Ochoa				Added changes - launch method only when BCR is completed
        //04/04/2018 				Manuel Ochoa 				Added NE__OptyId__c to query
        if(!listIds.isEmpty()){
            List<NE__Order__c> listB2WHUBConfig = [	SELECT Id,NE__OrderStatus__c,BI_O4_Nombre_Tipo_Registro__c,NE__OptyId__c,NE__Version__c
												FROM NE__Order__c
												WHERE Id in :listIds];
            List<NE__Order__c> listOrders = new List<NE__Order__c>();
            for (NE__Order__c order : listB2WHUBConfig) {
                if (order.BI_O4_Nombre_Tipo_Registro__c == 'Opty') {
                    system.debug('Order to update: ' + order.id);
                    listOrders.add(order);
                    // 19-04-2018 - Manuel Ochoa - Added setIdOrders to update prices in optys
                	setIdOrders.add(order.Id);
                }
            }
            if(!listOrders.isEmpty()){
            	BI_OrderMethods.setOrderStatus(listOrders);
                // 19-04-2018 - Manuel Ochoa - Added new call to update prices in optys
                system.debug('orders to updateSumatoria: ' + setIdOrders);
                BI_NEOrderItemMethods.updateSumatoria(setIdOrders);
            }  
        }	
		// 09/05/2018 - Carlos Vicente - Call checkStatusQuotations
		if(!listIds.isEmpty()){
			checkStatusQuotations(listItems);
		}
	}


	/*---------------------------------------------------------------------------------------------------------
	Author:			Carlos Vicente Aylagas
	Company:		Accenture.
	Description:	Method that manages the litItems for API's Quotations and call class CWP_EP_SendDataController

	History:

	<Date>                  <Author>					<Description>
	08/05/2018				Carlos Vicente				Initial Version
	-----------------------------------------------------------------------------------------------------------*/

	public static void checkStatusQuotations(List<Bit2WinHUB__Bulk_Configuration_Request__c> listItems) {

		System.debug('Trigger <> BI_B2W_BulkConfigRequestMethods##checkStatusQuotations =>> Trigger.new : ' + listItems);
		
		if(listItems.size() == 1){
			
			String urlCallback = [Select Bit2WinHUB__Session_Parameters__c,Bit2WinHUB__Request_Id__c from Bit2WinHUB__Bulk_Import_Request__c where Bit2WinHUB__Request_Id__c = :listItems[0].Bit2WinHUB__Request_Id__c].Bit2WinHUB__Session_Parameters__c;
			if (urlCallback != null && urlCallback != '') {
				System.debug('##checkStatusQuotations# listItems : ' + listItems);
				System.debug('##checkStatusQuotations# urlCallback : ' + urlCallback);
				CWP_EP_SendDataController.queryData(listItems,urlCallback);
			}			
		}else{
			return;
		}
	}
}