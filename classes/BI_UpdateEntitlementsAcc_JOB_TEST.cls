/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga
    Company:       NEA
    Description:   Test class to manage the coverage code for BI_UpdateEntitlementsAcc_JOB class 
    Test Class:    BI_Account_JOB_TEST

    History: 
    
     <Date>                     <Author>                <Change Description>
    03/02/2017                  Pedro Párraga            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class BI_UpdateEntitlementsAcc_JOB_TEST {
    
    static testMethod void UpdateEntitlementsAcc_addAll_Test() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        

        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Chile'});
        system.assert(!lst_acc.isEmpty());

        List<SlaProcess> lst_news_SLAProceso = new List<SlaProcess>();
        lst_news_SLAProceso = [SELECT CreatedById,CreatedDate,Description,Id,IsActive,IsDeleted,IsVersionDefault,LastModifiedById,LastModifiedDate,Name,NameNorm,StartDateField,
                                                    SystemModstamp,VersionMaster,VersionNotes,VersionNumber FROM SlaProcess WHERE Name Like '%Chile%' AND IsVersionDefault = true AND IsActive = true ];
        
        List <BusinessHours> lst_hours = [SELECT FridayEndTime,FridayStartTime,Id,MondayEndTime,MondayStartTime,Name,SaturdayEndTime,SaturdayStartTime,
                              SundayEndTime,SundayStartTime,ThursdayEndTime,ThursdayStartTime,TuesdayEndTime,
                              TuesdayStartTime,WednesdayEndTime,WednesdayStartTime FROM BusinessHours WHERE Name like 'Horario oficina CHI' AND IsActive = true limit 1];    
        
        List<Entitlement> lst_ent = new List<Entitlement>();
        Entitlement ent = new Entitlement(Name = 'Name',
                                        Type = 'Web Support',
                                        SlaProcessId = lst_news_SLAProceso[0].Id,
                                        AccountId = lst_acc[0].Id,
                                        StartDate = Date.today(),
                                        BusinessHoursId = lst_hours[0].Id);
        insert ent;

        for(Account acc :[SELECT Id, (SELECT Id FROM Entitlements) FROM Account WHERE Id IN :lst_acc]){
            lst_ent.addAll(acc.Entitlements);
        }

        if(!lst_news_SLAProceso.isEmpty()){ 
            Test.startTest();
            BI_Jobs.updateEntitlementsAcc(null, lst_news_SLAProceso, false);
            Test.stopTest();

            List<CronTrigger> lst_cronTrigg = [SELECT CreatedById,CreatedDate,CronExpression,CronJobDetail.Name,EndTime,Id,LastModifiedById,NextFireTime,OwnerId,PreviousFireTime,StartTime,State,TimesTriggered,TimeZoneSidKey FROM CronTrigger WHERE CronJobDetail.Name LIKE '%Update Cases%'];
            System.debug('lst_cronTrigg: ' + lst_cronTrigg);
        }else{
            System.debug('Exception: NOT HAVE SlaProcess');
        }
        //System.assert(!lst_cronTrigg.isEmpty()); Test not see CronTrigger recall.*/
    }

    static testMethod void UpdateEntitlementsAcc_addOnlyOne_Test() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        

        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Chile'});
        system.assert(!lst_acc.isEmpty());

        List<Entitlement> lst_ent = new List<Entitlement>();
        for(Account acc :[SELECT Id, (SELECT Id FROM Entitlements) FROM Account WHERE Id IN :lst_acc]){
            if(!acc.Entitlements.isEmpty()){
                lst_ent.add(acc.Entitlements[0]);   
            }
        }
        delete lst_ent;
        

        List<SlaProcess> lst_news_SLAProceso = new List<SlaProcess>();
        lst_news_SLAProceso = [SELECT CreatedById,CreatedDate,Description,Id,IsActive,IsDeleted,IsVersionDefault,LastModifiedById,LastModifiedDate,Name,NameNorm,StartDateField,
                                                    SystemModstamp,VersionMaster,VersionNotes,VersionNumber FROM SlaProcess WHERE Name Like '%Chile%' AND IsVersionDefault = true AND IsActive = true ];
        if(!lst_news_SLAProceso.isEmpty()){                                                    
            Test.startTest();
            BI_Jobs.updateEntitlementsAcc(null, lst_news_SLAProceso, false);
            Test.stopTest();

            List<CronTrigger> lst_cronTrigg = [SELECT CreatedById,CreatedDate,CronExpression,CronJobDetail.Name,EndTime,Id,LastModifiedById,NextFireTime,OwnerId,PreviousFireTime,StartTime,State,TimesTriggered,TimeZoneSidKey FROM CronTrigger WHERE CronJobDetail.Name LIKE '%Update Cases%'];
            System.debug('lst_cronTrigg: ' + lst_cronTrigg);
        }else{
            System.debug('Exception: NOT HAVE SlaProcess');
        }

        //System.assert(!lst_cronTrigg.isEmpty()); Test not see CronTrigger recall.*/
    }
    
	static testMethod void UpdateEntitlementsAcc_Exception_Test()
	{
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        

        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Chile'});
        system.assert(!lst_acc.isEmpty());

        List<Entitlement> lst_ent = new List<Entitlement>();
        for(Account acc :[SELECT Id, (SELECT Id FROM Entitlements) FROM Account WHERE Id IN :lst_acc]){
            if(!acc.Entitlements.isEmpty()){
                lst_ent.add(acc.Entitlements[0]);   
            }
        }
        delete lst_ent;
        

        List<SlaProcess> lst_news_SLAProceso = new List<SlaProcess>();
        lst_news_SLAProceso = [SELECT CreatedById,CreatedDate,Description,Id,IsActive,IsDeleted,IsVersionDefault,LastModifiedById,LastModifiedDate,Name,NameNorm,StartDateField,
                                                    SystemModstamp,VersionMaster,VersionNotes,VersionNumber FROM SlaProcess WHERE Name Like '%Chile%' AND IsVersionDefault = true AND IsActive = true ];
        if(!lst_news_SLAProceso.isEmpty()){                                                    
            Test.startTest();
	    	try
	    	{
	    		BI_Jobs.updateEntitlementsAcc(null, lst_news_SLAProceso, null);
	    	}
	    	catch (Exception e)
	    	{
	    		System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
	    	}
            Test.stopTest();
        }else{
            System.debug('Exception: NOT HAVE SlaProcess');
        }
	}

    static testMethod void test_method_one()
    {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;

        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Chile'});
                                                 
            Test.startTest();
            try{
                String jobName = 'Update Accounts Entitlements'; 
                System.schedule(BI_Jobs.generateStrNameProg(Datetime.now().addMinutes(1), jobName), 
                                BI_Jobs.generateStrProg(Datetime.now().addMinutes(1)),
                                 new BI_UpdateEntitlementsAcc_JOB(lst_acc[0].Id, null));
            }catch (Exception e)
            {
                System.assert(e.getMessage().contains('Attempt to de-reference a null object'));
            }
            Test.stopTest();
    }
}