public without sharing class BI_MinutasReunion_Controler {
      Event evt;
      Contact contact;
      public String cPhone {get; set;}
      public String cAccOwner {get; set;}
      public String cCountry {get; set;}
      public String cEmail {get; set;}
	  public String cimg{get; set;} 
      public String csegmento{get; set;} 
    public BI_MinutasReunion_Controler(ApexPages.StandardController stdController) {
        System.debug('INNIT -- BI_MinutasReunion_Controler');
       
        
      this.evt = (Event)stdController.getRecord();
	  System.debug('evento-->'+evt);
      contact=[Select id,Name,Phone,Email,BI_Segment__c, Account.BI_Country__c, Account.Owner.Name From Contact where Id=:evt.WhoId];
      System.debug('contact DATA-->'+contact);
      cPhone=contact.Phone;
      cEmail=contact.Email;
	    csegmento=contact.BI_Segment__c; 
      cCountry= contact.Account.BI_Country__c;
      cAccOwner = contact.Account.Owner.Name;
        
      System.debug('DONE -- BI_MinutasReunion_Controler');

      //System.debug('IMG--URL'+PageReference.forResource('BI_Visitas_Logo_Empresas').getUrl());

    }
}