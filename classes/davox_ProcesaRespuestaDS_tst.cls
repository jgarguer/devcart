/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Indefined
    Company:       Indefined
    Description:   Test method to manage the code coverage for davox_ProcesaRespuestaDS class
            
     <Date>                 <Author>                <Change Description>
     Indefined              Indefined               Initial Version
     30/12/2016             Gawron, Julián          Adding testSetup
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

@isTest(SeeAllData=false)
private class davox_ProcesaRespuestaDS_tst 
{
    public static list<Profile> lstPerfil;
    public static list <UserRole> lstRoles;
    public static list<User> lstUsuarios;
    public static list <Campaign> lstCampana;
    public static List<RecordType> lstRecTyp;
    public static list<BI_COL_Modificacion_de_Servicio__c> lstMSSel;
    
    public static User objUsuario;
    public static Account objCuenta;
    public static Opportunity objOportunidad;
    public static Campaign objCampana;
    public static BI_COL_cup_Selectds_ctr objCupSelectds;
    public static BI_COL_Modificacion_de_Servicio__c objMS;
    public static BI_COL_Anexos__c objAnexos;
    public static BI_COL_Descripcion_de_servicio__c objDesSer;
    
    public static String idOpt {get;set;}

    /*
     *
     *       30/12/2016             Gawron, Julián          Adding testSetup
     *       23/02/2017             Jaime Regidor           Adding Campaign Values, Adding Catalog Country
     */
    @testSetup public static void crearData()
    {
        //perfiles
        lstPerfil = new list <Profile>();
        lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1 ];

        //lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = BI_DataLoad.createRandomUserCOL(lstPerfil.get(0).Id, lstRoles.get(0).Id);      
        System.runAs(new User(Id=UserInfo.getUserId())){ //prevent mixed opp JEG
            insert objUsuario;
        }

        System.runAs(objUsuario)
        {
            NE__Catalog__c objCatalogo                                  = new NE__Catalog__c();
            objCatalogo.Name                                            = 'prueba Catalogo';
            objCatalogo.BI_Country__c                                   =  'Colombia';
            insert objCatalogo; 

            objCampana = new Campaign();
            objCampana.Name = 'Campaña Prueba';
            objCampana.BI_Catalogo__c = objCatalogo.Id;
            objCampana.BI_COL_Codigo_campana__c = 'prueba1';
            objCampana.BI_Country__c = 'Colombia';
            objCampana.BI_Opportunity_Type__c = 'Fijo';
            objCampana.BI_SIMP_Opportunity_Type__c = 'Alta'; 
            insert objCampana;

            lstCampana = new List<Campaign>();
            lstCampana = [select Id, Name from Campaign where Id =: objCampana.Id];

            objCuenta                                                   = new Account();
            objCuenta.Name                                              = 'prueba';
            objCuenta.BI_Country__c                                     = 'Colombia';
            objCuenta.TGS_Region__c                                     = 'América';
            objCuenta.BI_Tipo_de_identificador_fiscal__c                = '';
            objCuenta.CurrencyIsoCode                                   = 'COP';
            insert objCuenta;

            objOportunidad                                              = new Opportunity();
            objOportunidad.Name                                         = 'prueba opp';
            objOportunidad.AccountId                                    = objCuenta.Id;
            objOportunidad.BI_Country__c                                = 'Colombia';
            objOportunidad.CloseDate                                    = System.today().addDays(+5);
            objOportunidad.StageName                                    = 'F6 - Prospecting';
            objOportunidad.CurrencyIsoCode                              = 'COP';
            objOportunidad.Certa_SCP__contract_duration_months__c       = 12;
            objOportunidad.BI_Plazo_estimado_de_provision_dias__c       = 0 ;
            objOportunidad.OwnerId                                      = objUsuario.id;
            insert objOportunidad;

            lstRecTyp = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
            system.debug('datos de FUN '+lstRecTyp);

            objAnexos               = new BI_COL_Anexos__c();
            objAnexos.Name          = 'FUN-0041414';
            objAnexos.RecordTypeId  = lstRecTyp[0].Id;
            insert objAnexos;

            objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
            objDesSer.BI_COL_Oportunidad__c                     = objOportunidad.Id;
            objDesSer.CurrencyIsoCode               = 'COP';
            insert objDesSer;

            objMS                                       = new BI_COL_Modificacion_de_Servicio__c();
            objMS.BI_COL_FUN__c                         = objAnexos.Id;
            objMS.BI_COL_Codigo_unico_servicio__c       = objDesSer.Id;
            objMS.BI_COL_Clasificacion_Servicio__c      = 'ALTA';
            objMS.BI_COL_Oportunidad__c                 = objOportunidad.Id;
            objMS.BI_COL_Bloqueado__c                   = false;
            objMS.BI_COL_Estado_orden_trabajo__c        = 'Ejecutado';
            insert objMS;

            lstMSSel = new List<BI_COL_Modificacion_de_Servicio__c>();
            lstMSSel.add(objMS); 


            BI_COL_RespuestasDavox__c rta =new BI_COL_RespuestasDavox__c ();
            rta.BI_COL_Descripcion_Codigo__c ='Prueba del error';
            rta.Name='9000';
            insert rta;

            BI_COL_RespuestasDavox__c rta2 =new BI_COL_RespuestasDavox__c ();
            rta2.BI_COL_Descripcion_Codigo__c ='Prueba del error';
            rta2.Name='101';
            insert rta2;
        }
    }

    @isTest static void test_method_one() 
    {
        Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
        objMS  = [Select Id,BI_COL_FUN__c,BI_COL_Codigo_unico_servicio__c, BI_COL_Clasificacion_Servicio__c,
        BI_COL_Oportunidad__c, BI_COL_Bloqueado__c, BI_COL_Estado_orden_trabajo__c
         from BI_COL_Modificacion_de_Servicio__c limit 1];
        System.assert(objMS != null, 'BI_COL_Modificacion_de_Servicio__c vacío');

        BI_COL_Modificacion_de_Servicio__c ms = [Select id from BI_COL_Modificacion_de_Servicio__c where id=:objMS.ID];
        System.assert(ms != null, 'Falta modificación del servicio');
        
        String datoPrueba = '<?xml version="1.0" encoding="UTF-8"?><WS><SESSIONCODE>123</SESSIONCODE><DATEPROCESS>12/11/2010</DATEPROCESS><PACKCODE>ASD654AS</PACKCODE><SEPARATOR>Õ</SEPARATOR><TYPE>ADDNOVEDAD</TYPE><VALUES><VALUE>ASD654ASÕ'+ms.ID+'Õ101</VALUE></VALUES></WS>';

        BI_Log__c nf = new BI_Log__c();
        nf.BI_COL_Usuario2__c = 'test2';
        nf.BI_COL_Informacion_Enviada__c = 'test2';
        nf.BI_COL_Modificacion_Servicio__c = ms.id;
        insert nf;
        
        davox_ProcesaRespuestaDS obj = new davox_ProcesaRespuestaDS(); 
        obj = new davox_ProcesaRespuestaDS(datoPrueba);
        davox_ProcesaRespuestaDS.ElaboraDescripcion('Test,Test');
        obj = new davox_ProcesaRespuestaDS('');

        string datoPrueba2 = '<?xml version="1.0" encoding="UTF-8"?><WS><SESSIONCODE>123</SESSIONCODE><DATEPROCESS>12/11/2010</DATEPROCESS><PACKCODE>ASD654AS</PACKCODE><SEPARATOR>Õ</SEPARATOR><TYPE>ADDNOVEDAD</TYPE><VALUES><VALUE>ASD654ASÕ'+ms.ID+'Õ9000</VALUE></VALUES></WS>';
        obj = new davox_ProcesaRespuestaDS(datoPrueba2);

        string datoPrueba3 = '<?xml version="1.0" encoding="UTF-8"?><WS>123</SESSIONCODE><DATEPROCESS>12/11/2010</DATEPROCESS><PACKCODE>ASD654AS</PACKCODE><SEPARATOR>Õ</SEPARATOR><TYPE>ADDNOVEDAD</TYPE><VALUES><VALUE>ASD654ASÕ'+ms.ID+'Õ9000</VALUE></VALUES></WS>';
        obj = new davox_ProcesaRespuestaDS(datoPrueba3);
    }


    @isTest static void test_method_1() 
    {
        Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
        objMS  = [Select Id,BI_COL_FUN__c,BI_COL_Codigo_unico_servicio__c, BI_COL_Clasificacion_Servicio__c,
        BI_COL_Oportunidad__c, BI_COL_Bloqueado__c, BI_COL_Estado_orden_trabajo__c
         from BI_COL_Modificacion_de_Servicio__c limit 1];

        BI_COL_Modificacion_de_Servicio__c ms=[Select id from BI_COL_Modificacion_de_Servicio__c where id=:objMS.ID];
        string datoPrueba = '<?xml version="1.0" encoding="UTF-8"?><WS><SESSIONCODE>123</SESSIONCODE><DATEPROCESS>12/11/2010</DATEPROCESS><PACKCODE>ASD654AS</PACKCODE><SEPARATOR>Õ</SEPARATOR><TYPE>ADDNOVEDAD</TYPE><VALUES><VALUE>ASD654ASÕ'+ms.ID+'Õ100ÕDATOS CUENTA</VALUE></VALUES></WS>';

        BI_Log__c nf = new BI_Log__c();
        nf.BI_COL_Usuario2__c = 'test2';
        nf.BI_COL_Informacion_Enviada__c = 'test2';
        nf.BI_COL_Modificacion_Servicio__c = ms.id;
        insert nf;
        
        davox_ProcesaRespuestaDS obj = new davox_ProcesaRespuestaDS(); 
        obj = new davox_ProcesaRespuestaDS(datoPrueba);
        davox_ProcesaRespuestaDS.ElaboraDescripcion('Test,Test');
        obj = new davox_ProcesaRespuestaDS('');

        string datoPrueba2 = '<?xml version="1.0" encoding="UTF-8"?><WS><SESSIONCODE>123</SESSIONCODE><DATEPROCESS>12/11/2010</DATEPROCESS><PACKCODE>ASD654AS</PACKCODE><SEPARATOR>Õ</SEPARATOR><TYPE>ADDNOVEDAD</TYPE><VALUES><VALUE>ASD654ASÕ'+ms.ID+'Õ100ÕCOBROS PARCIALES</VALUE></VALUES></WS>';
        obj = new davox_ProcesaRespuestaDS(datoPrueba2);
    }
    
    
    @isTest static void test_method_two() 
    {
        Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );

        String strDato1, strDato2;
         
         Map<String, String> mapMsErr = new Map<String, String> ();
         List<String> lstCobroParcial = new List<String>();

        davox_ProcesaRespuestaDS.actualizarDatosCuenta(lstCobroParcial,mapMsErr);
        davox_ProcesaRespuestaDS.actualizarDatosCuenta(lstCobroParcial,mapMsErr);
        davox_ProcesaRespuestaDS.crearLogContrato(lstCobroParcial , mapMsErr, strDato1);
        davox_ProcesaRespuestaDS.actualizarCobroParcial(lstCobroParcial,mapMsErr);
    }

    @isTest static void test_method_2() 
    {
        Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
        objMS  = [Select Id,BI_COL_FUN__c,BI_COL_Codigo_unico_servicio__c, BI_COL_Clasificacion_Servicio__c,
        BI_COL_Oportunidad__c, BI_COL_Bloqueado__c, BI_COL_Estado_orden_trabajo__c
         from BI_COL_Modificacion_de_Servicio__c limit 1];

        BI_COL_Modificacion_de_Servicio__c ms=[Select id from BI_COL_Modificacion_de_Servicio__c where id=:objMS.ID];
        string datoPrueba = '<?xml version="1.0" encoding="UTF-8"?><WS><SESSIONCODE>123</SESSIONCODE><DATEPROCESS>12/11/2010</DATEPROCESS><PACKCODE>ASD654AS</PACKCODE><SEPARATOR>Õ</SEPARATOR><TYPE>ADDNOVEDAD</TYPE><VALUES><VALUE>ASD654ASÕ'+ms.ID+'Õ101ÕMODIFICACION CONTRATO</VALUE></VALUES></WS>';

        BI_Log__c nf = new BI_Log__c();
        nf.BI_COL_Usuario2__c = 'test2';
        nf.BI_COL_Informacion_Enviada__c = 'test2';
        nf.BI_COL_Modificacion_Servicio__c = ms.id;
        insert nf;
        
        davox_ProcesaRespuestaDS obj = new davox_ProcesaRespuestaDS(); 
        obj = new davox_ProcesaRespuestaDS(datoPrueba);
        davox_ProcesaRespuestaDS.ElaboraDescripcion('Test,Test');

    }

    @isTest static void test_method_3() 
    {
        Test.setMock( WebServiceMock.class, new BI_COL_ChannelOnline_mws() );
        objMS  = [Select Id,BI_COL_FUN__c,BI_COL_Codigo_unico_servicio__c, BI_COL_Clasificacion_Servicio__c,
        BI_COL_Oportunidad__c, BI_COL_Bloqueado__c, BI_COL_Estado_orden_trabajo__c
         from BI_COL_Modificacion_de_Servicio__c limit 1];

        BI_COL_Modificacion_de_Servicio__c ms=[Select id from BI_COL_Modificacion_de_Servicio__c where id=:objMS.ID];
        string datoPrueba = '<?xml version="1.0" encoding="UTF-8"?><WS><SESSIONCODE>123</SESSIONCODE><DATEPROCESS>12/11/2010</DATEPROCESS><PACKCODE>ASD654AS</PACKCODE><SEPARATOR>Õ</SEPARATOR><TYPE>ADDNOVEDAD</TYPE><VALUES><VALUE>ASD654ASÕ'+ms.ID+'Õ101ÕCESION CONTRATO</VALUE></VALUES></WS>';
        BI_Log__c nf = new BI_Log__c();
        nf.BI_COL_Usuario2__c = 'test2';
        nf.BI_COL_Informacion_Enviada__c = 'test2';
        nf.BI_COL_Modificacion_Servicio__c = ms.id;
        insert nf;
        
        davox_ProcesaRespuestaDS obj = new davox_ProcesaRespuestaDS(); 
        obj = new davox_ProcesaRespuestaDS(datoPrueba);
        davox_ProcesaRespuestaDS.ElaboraDescripcion('Test,Test');
    }
}