/**
* Avanxo Colombia
* @author           Sebastián Ortiz href=<seortiz@avanxo.com>
* Proyect:          Telefonica BI2
* Description:      Clase controladora encargada de obtener los campos para ser mostrados en la Página "BI2_COL_DetalleSolicitud_pag"
* Changes (Version)
* -------------------------------------
*            No.    Fecha           Autor                           Descripción
*            ----   ----------      ---------------------------     -------------
* @version   1.0    03-02-2017     	Sebastián Ortiz Niño     		Class Created
************************************************************************************************************************/
public with sharing class BI2_COL_DetalleSolicitud_ctr extends BIIN_BaseController
{
	public Account AccountName                {get; set;}
	public case caso                          {get; set;}
	public boolean displayPopup               {get; set;}
	public String caseId;
	public Attachment attach                  {get; set;}
	public boolean modBoolean                 {get; set;}
	public List <Attachment> ListaAdjuntos    {get; set;}
	public String attach_Id                   {get; set;}
	public attachment adjunto                 {get; set;}
	public boolean displayPopUp2    		  {get; set;}
	public boolean displayPopUp3    	      {get; set;}
	public boolean displayWorkInfo    	      {get; set;}
	public String StringWSCreacion 			  {get; set;}
	public List<BIIN_Obtener_Ticket_WS.Worklog> lworklog{get; set;}
	//public Boolean adjuntos 			  {get; set;}
	public String attachName 				{get; set;}
	public String worklogId					{get;set;}
	public String posicionWI				{get;set;}
	public BI2_COL_DetalleIncidencia_ctr detalleIncidencia;
	public String rutaBase {get; set;}
	public String IdAdjunto {get; set;}
	public ApexPages.StandardController controller {get; set;}
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
		Author:        Sebastián Ortiz
		Company:       Avanxo
		Description:   Class construct for variables initialization.

		In:							Standard Controller (Case)
		Out: 						Void

		<Date>            <Author>          				<Description>
		07/02/2017       Sebastián Ortiz   					Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public BI2_COL_DetalleSolicitud_ctr(ApexPages.StandardController controller)
	{
		caseId 										= System.currentPagereference().getParameters().get('Idcaso');
		System.debug(' \n\n caseId = '+ caseId +' \n\n');
		caseId=caseId == null?System.currentPagereference().getParameters().get('id'):caseId;
		System.debug(' \n\n caseId = '+ caseId +' \n\n');
		ListaAdjuntos  								= new List <Attachment> ();
		adjunto 									= new Attachment();
		caso 										= new case();
		displayPopUp 								= false;
		displayPopUp2								= false;
		displayPopUp3								= false;
		modBoolean 									= true;
		StringWSCreacion 							= '';
		this.controller=controller;
		displayWorkInfo=false;
		/*Querys*/
		caso = [SELECT /*Campos nuevos Start SON*/
                       BI_Confidencial__c
                      ,BI2_COL_Contacto_de_cierre__c
                      ,BI_ECU_Telefono_fijo__c
                      ,BI_ECU_Telefono_movil__c
                       /*Campos nuevos END*/
                      ,toLabel(Type)
                      ,Reason
                      ,BI_Product_Categorization_Tier_1__c
                      ,ParentId
                      ,BI_Product_Categorization_Tier_2__c
                      ,BI_Product_Categorization_Tier_3__c
                      ,CaseNumber
                      ,AccountId
                      ,ContactId
                      ,toLabel(Status)
                      ,BIIN_Form_Name__c
                      ,TGS_Ticket_Site__c
                      ,OwnerId
                      ,Owner.Name
                      ,BI_COL_Fecha_Radicacion__c
                      ,BI_Codigo_del_producto__c
                      ,BIIN_Descripcion_error_integracion__c
                      ,BI2_Nombre_Contacto_Final__c
                      ,toLabel(Origin)
                      ,BI2_Apellido_Contacto_Final__c
                      ,TGS_Impact__c
                      ,BI2_Email_Contacto_Final__c
                      ,TGS_Urgency__c
                      ,BIIN_Categorization_Tier_1__c
                      ,BIIN_Categorization_Tier_2__c
                      ,BIIN_Categorization_Tier_3__c
                      ,RecordTypeId
                      ,BI_COL_Codigo_CUN__c
                      ,toLabel(BI_Line_of_Business__c)
                      ,BI_Product_Service__c
                      ,BIIN_Site__c
                      ,BIIN_Id_Producto__c
                      ,Subject
                      ,Description
                      ,Id
                      ,BI2_PER_Descripcion_de_producto__c
                      ,Contact.Name
                      ,RecordType.DeveloperName
                  FROM Case WHERE Id =: caseId];

        AccountName = [SELECT Name, BI2_asesor__c, BI_No_Identificador_fiscal__c FROM Account WHERE Id =: caso.AccountId];
		ListaAdjuntos = [SELECT Id, Name, Body,CreatedDate,CreatedBy.Name  FROM Attachment WHERE ParentId  =: caseId];
		System.debug(' \n\n ListaAdjuntos = '+ ListaAdjuntos.size() +' \n\n');
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
		Author:        Sebastián Ortiz
		Company:       Avanxo
		Description:   Page Reference method for showing attachments

		In:							None
		Out: 						PageReference showing the standard controller of attachments

		<Date>            <Author>          				<Description>
		07/02/2017       Sebastián Ortiz   					Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public PageReference showAttachment()
	{
		adjunto = [SELECT Id  FROM Attachment WHERE Id =: attach_Id];
		return new ApexPages.Standardcontroller(adjunto).view();
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
		Author:        Sebastián Ortiz
		Company:       Avanxo
		Description:   methods which controls the booleans that rerender the visualforce

		In:							None
		Out: 						Void

		<Date>            <Author>          				<Description>
		07/02/2017       Sebastián Ortiz   					Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	Public void Modificar()
	{
		modBoolean = false;
	}

	Public void GuardarModif()
	{
		Relanzar();
		modBoolean = true;
	}

	Public void Cancelar()
	{
		modBoolean = true;
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
		Author:        Sebastián Ortiz
		Company:       Avanxo
		Description:   Method used for consuming Remedy Webservice

		In:							NONE
		Out: 						Void

		<Date>            <Author>          				<Description>
		07/02/2017       Sebastián Ortiz   					Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	Public void Relanzar()
	{
		System.debug('\n\n @*@*@*@*Relanzar Entro@*@*@*@* \n\n'+ +'\n\n @*@*@*@*@*@*@* \n\n');
		BIIN_Creacion_Ticket_WS ct 		= new BIIN_Creacion_Ticket_WS();
		String authorizationToken = '';
		Integer nIntentos=0;
		Boolean exito=false;
		system.debug('CASO' + caso);

		// Mount attachment list
		List<BIIN_Creacion_Ticket_WS.Adjunto> la = new List<BIIN_Creacion_Ticket_WS.Adjunto>();
		for (Attachment att: ListaAdjuntos)
		{
			system.debug('Adjunto nombre: '+ att);
			BIIN_Creacion_Ticket_WS.Adjunto a = new BIIN_Creacion_Ticket_WS.Adjunto(att.Name, att.Body, att.Id);
			system.debug('Adjunto nombre: '+ a);
			la.add(a);
		}
		System.debug('\n\n @*@*@*@*Relanzar@*@*@*@* \n\n'+ la+'\n\n @*@*@*@*@*@*@* \n\n');
		// Make rest callout
		do
		{
			exito=ct.crearTicket(authorizationToken, caso, la, null);
			nIntentos++;
			System.debug('\n\n @*@*@*@*Relanzar@*@*@*@* \n\n'+ exito+'\n\n @*@*@*@*@*@*@* \n\n');
		}
		while((nIntentos<3)&&(exito==false));

		if(Test.isRunningTest())
		{
			exito = false;
		}

		StringWSCreacion = ct.StringWS;
		system.debug('BooleanWSCreacion----->' + StringWSCreacion);
		if(!exito)
		{//lanzar la tarea porque no hemos podido contactar con el bao
			caso.Status='Confirmation pending';
			StringWSCreacion = 'PENDING';
			try{
				update caso;
				}catch(DmlException e){
					system.debug('Error al insertar el estado del caso: '+e);
				}
				Task tarea = new Task();
				tarea.WhatId = caso.Id;
				tarea.WhoId = caso.ContactId;
				tarea.Subject = 'Other';
				tarea.OwnerId = Userinfo.getUserId();
				tarea.Description = 'Esto es una tarea de prueba';
				tarea.Status = 'Not Started';
				tarea.Priority = 'High';
				insert tarea;
		}else{
			system.debug('DETALLE SOLICITUD OK');
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
		Author:        Sebastián Ortiz
		Company:       Avanxo
		Description:   Method used for Show a popout in the visualforce with the result of remedy Webservice

		In:							NONE
		Out: 						Void

		<Date>            <Author>          				<Description>
		07/02/2017       Sebastián Ortiz   					Initial version
	 --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public void MostrarPopUpEstado()
	{
		System.debug('\n @*@*@*@*MostrarPopUpEstado Entro@*@*@*@* \n'+StringWSCreacion+'\n @*@*@*@*@*@*@* \n');
		if(StringWSCreacion == 'PENDING')
		{
			displayPopUp = true;
			caso.BIIN_Descripcion_error_integracion__c = 'Es necesario relanzar manualmente el ticket,pulse el botón Relanzar';
			update caso;
		}
		else if(StringWSCreacion == 'OK')
		{
			displayPopUp2 = true;
			caso.Status = 'Being processed';
			caso.BIIN_Descripcion_error_integracion__c = 'Petición en curso. Mientras el ticket no se haya procesado podrá consultar su ticket 	como Solicitud Incidencia Técnica. En breves momentos recibirá una notificación y podrá consultar su ticket como una Incidencia Técnica.';
			update caso;
		}
		else
		{
			displayPopUp3 = true;
			caso.BIIN_Descripcion_error_integracion__c = StringWSCreacion;
			update caso;
		}
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
		Author:        Sebastián Ortiz
		Company:       Avanxo
		Description:   Method that controls the open and close of the popout

		In:							NONE
		Out: 						Void

		<Date>            <Author>          				<Description>
		07/02/2017       Sebastián Ortiz   					Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public void NOOKdetalle()
	{
		displayPopUp = false;
		displayPopUp2 = false;
		displayPopUp3 = false;
	}

	public pageReference OKdetalle()
	{
		return new PageReference('/500/o');
	}

	public pageReference TimeOUT()
	{
		return new ApexPages.Standardcontroller(caso).view();
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
		Author:        Carlos Carvajal
		Company:       Avanxo
		Description:   Metodo encargado de consultar el caso en RoD si es una incidencia Tecnica

		In:							NONE
		Out: 						Void

		<Date>            <Author>          				<Description>
		07/02/2017       Carlos Carvajal   					Initial version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public void loadData()
    {
    	if(caso.RecordType.DeveloperName=='BIIN_Incidencia_Tecnica')
    	{
			ListaAdjuntos=null;
			System.debug('\n\n-=#=-\n>>>>>>>>>>   ' + 'Incidencia Tecnica' + '   <<<<<<<<<<\n-=#=-\n');
			detalleIncidencia=new BI2_COL_DetalleIncidencia_ctr(controller);
    		detalleIncidencia.caseId=caseId;
    		detalleIncidencia.loadData();
    		System.debug('\n\n-=#=-\n' + 'detalleIncidencia.caso' + ': ' + detalleIncidencia.caso + '\n-=#=-\n');
    		caso=detalleIncidencia.caso;
    		lworklog=detalleIncidencia.lworklog;
    		//system.debug('\n\n lworklog '+lworklog+'\n\n');
    		System.debug('\n\n rutaBase '+  URL.getSalesforceBaseUrl().toExternalForm()+'   ###IdAdjunto### '+IdAdjunto+'\n\n');
    		displayWorkInfo=true;
    		ListaAdjuntos=null;
		 }

    	//ListaAdjuntos=detalleIncidencia.ListaAdjuntos2;
    }
		/*-------------------------------------------------------------------------------------------------------------------------------------------------------
			Author:        Carlos Carvajal
			Company:       Avanxo
			Description:   Metodo encargado de consultar el adjunto en RoD

			In:							NONE
			Out: 						Void

			<Date>            <Author>          				<Description>
			07/02/2017       Carlos Carvajal   					Initial version
		--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	public void getAttBody()
	{
		String cuerpo;
		String adjId = '';

		BIIN_Obtener_Token_BAO ot = new BIIN_Obtener_Token_BAO();
		BIIN_Obtener_Anexo_WS oa = new BIIN_Obtener_Anexo_WS();
		String authorizationToken = ''; // ot.obtencionToken();
		cuerpo = oa.obtenerAnexo(authorizationToken, caso, attachName, worklogId, posicionWI);
		System.debug('\n\n-=#=-\n>>>>>>>>>>  cuerpo = ' + cuerpo + '   <<<<<<<<<<\n-=#=-\n');
		if(String.isNotEmpty(cuerpo))
		{
			Attachment adjuntoIns = new Attachment();

			List<Attachment> adjunto = new List<Attachment>();
			adjunto = [SELECT Name, Body, ParentId, Id FROM Attachment WHERE ParentId=:caso.Id];
			adjuntoIns.Body     = EncodingUtil.base64Decode(cuerpo);
			adjuntoIns.Name     = attachName;
			adjuntoIns.ParentId = caso.Id;

			if(!Test.isRunningTest())
			{
				insert adjuntoIns;
			}
			IdAdjunto = adjuntoIns.Id;
			rutaBase = URL.getSalesforceBaseUrl().toExternalForm();
		}
	}


}