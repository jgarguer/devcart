@isTest
private class BI_CompareControllerTest 
{
    static testMethod void myUnitTest()
    {
        try
        {
            List<NE__Product__c> prodList = new List<NE__Product__c>();
            
            NE__Product__c p1 = new NE__Product__c();
            p1.name = 'product1';
            p1.NE__Thumbnail_Image__c = '00P61000001IHP2EAO';
            prodList.add(p1);
            NE__Product__c p2 = new NE__Product__c();
            p2.name = 'product2';
            p2.NE__Thumbnail_Image__c = '00P61000001IHP3EAO';
            prodList.add(p2);
            NE__Product__c p3 = new NE__Product__c();
            p3.name = 'product3';
            prodList.add(p3);
            NE__Product__c p4 = new NE__Product__c();
            p4.name = 'product4';
            prodList.add(p4);    
            
            insert prodList;
            
            List<NE__DocumentationCharacterist__c> documentList = new List<NE__DocumentationCharacterist__c>();
            
            NE__DocumentationCharacterist__c dc1 = new NE__DocumentationCharacterist__c();
            dc1.name = 'documentationTest1';
            dc1.NE__Product__c = p1.id;
            documentList.add(dc1);
            
            NE__DocumentationCharacterist__c dc2 = new NE__DocumentationCharacterist__c();
            dc2.name = 'documentationTest2';
            dc2.NE__Product__c = p1.id;
            documentList.add(dc2);
            
            insert documentList;
            
            PageReference testPage  = Page.BI_CompareMobile; 
            Test.setCurrentPage(testPage); 
            testPage.getParameters().put('id', p1.id+':'+p2.id+':'+p3.id+':'+p4.id);  
              
            
            Test.StartTest();
            BI_CompareController cc = new BI_CompareController();            
            cc.getListOfCar();
             
            cc.getProduct_1();
            cc.getProduct_2();
            cc.getProduct_3();
            cc.getProduct_4();
            
            cc.getImageUrlProd_1();
            cc.getImageUrlProd_2();
            cc.getImageUrlProd_3();
            cc.getImageUrlProd_4();
            
            cc.getNameProd_1();
            cc.getNameProd_2();
            cc.getNameProd_3();
            cc.getNameProd_4();
            
            cc.getDettagliProdotto_1();
            cc.getDettagliProdotto_2();
            cc.getDettagliProdotto_3();
            cc.getDettagliProdotto_4();                     
            Test.StopTest();          
             
        }
        catch(Exception e)
        {
            System.debug(e);
        }
    }
    
    static testMethod void wrapperClassCaratteristicaTest()
    {
        try{
            BI_CompareController cc = new BI_CompareController();
            String name = 'name';  
            String val1 = 'val1';
            String val2 = 'val2';
            String val3 = 'val3';
            String val4 = 'val4';
            BI_CompareController.Caratteristica wrcl1 = new BI_CompareController.Caratteristica(name, val1, val2);
            BI_CompareController.Caratteristica wrcl2 = new BI_CompareController.Caratteristica(name, val1, val2, val3);
            BI_CompareController.Caratteristica wrcl3 = new BI_CompareController.Caratteristica(name, val1, val2, val3, val4);
        }
        catch(Exception e){}
    }         
}