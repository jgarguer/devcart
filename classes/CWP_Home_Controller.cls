global with sharing class CWP_Home_Controller {
    
    @AuraEnabled
    public static User getUserInfo(){
        User usuarioPortal = [Select id, FullPhotoUrl, name,Usuario_SolarWinds__c, token_SolarWinds__c, CompanyName, Sector__c, ContactId, MobilePhone, Usuario_BO__c, pass_BO__c  from User where Id = : userInfo.getUserId()];
        return usuarioPortal;
    }
    
    @AuraEnabled
    public static List<Account> getUserAccounts(){
        return [SELECT Id, Name FROM Account LIMIT 5]; 
    }
}