@isTest
private class BI_assignEntitlementsBatch_TEST {
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Gawron, Julián
     Company:       Accenture
     Description:   Class to Update Entitlements on Cases 
     Test Class from:    BI_assignEntitlementsBatch
    
     History:
     
     <Date>                     <Author>                    <Change Description>

 --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    @testSetup public static void testSetup() {
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);

        insert new BI2_Stop_Status__c(Name='Pending');

        List<QueueSObject> lst_colas = [SELECT Id, Queue.Id, Queue.DeveloperName FROM QueueSObject WHERE SObjectType='Case' Limit 1]; //JEG Limitamos la query
        List<Account> lst_clients = new List<Account>();
        for(Integer iter = 0; iter < 2; iter++) {
            lst_clients.add(new Account(Name='TestAccount_0'+iter, BI_Country__c = Label.BI_Peru, BI_Segment__c = 'test' , BI_Subsegment_Regional__c = 'test',BI_Territory__c = 'test'));
        }
        lst_clients[0].BI2_asesor__c=UserInfo.getUserId();
        lst_clients[1].BI2_cuadrilla__c = lst_colas[0].Queue.DeveloperName;
        insert lst_clients;

        List<Contact> lst_contacts = new List<Contact>();
        for(Integer iter = 0; iter < lst_clients.size(); iter++) {
            lst_contacts.add(new Contact(LastName='TestContact_0' + iter, AccountId=lst_clients[iter].Id, email='Test@testest.test'));
        }
        insert lst_contacts;

        BI_Mapeo_criterios_asignacion__c mca1 = new BI_Mapeo_criterios_asignacion__c(
            Name = 'PER-Tipología',
            BI_Nombre_API_Casos__c = 'BI2_per_tipologia__c',
            BI_Nombre_API_Criterios_de_asignacion__c = 'BI2_PER_Tipologia__c',
            BI_Country__c = 'Peru'
        );
        insert mca1;

        BI_Mapeo_criterios_asignacion__c mca2 = new BI_Mapeo_criterios_asignacion__c(
            Name = 'PER-Subtipología',
            BI_Nombre_API_Casos__c = 'BI2_per_subtipologia__c',
            BI_Nombre_API_Criterios_de_asignacion__c = 'BI2_PER_SubTipologia__c',
            BI_Country__c = 'Peru'
        );
        insert mca2;

        List<BI_Proceso_de_asignacion__c> lst_proc_toInsert = new List<BI_Proceso_de_asignacion__c>();
        BI_Proceso_de_asignacion__c proc = new BI_Proceso_de_asignacion__c(Name = 'Test');
        lst_proc_toInsert.add(proc);

        BI_Proceso_de_asignacion__c proc1 = new BI_Proceso_de_asignacion__c(Name = 'Test1');
        lst_proc_toInsert.add(proc1);

        BI_Proceso_de_asignacion__c proc2 = new BI_Proceso_de_asignacion__c(Name = 'Test2');
        lst_proc_toInsert.add(proc2);

        BI_Proceso_de_asignacion__c proc3 = new BI_Proceso_de_asignacion__c(Name = 'Test3');
        lst_proc_toInsert.add(proc3);
        insert lst_proc_toInsert;

        List<BI_Criterio_de_asignacion__c> lst_cda_toInsert = new List<BI_Criterio_de_asignacion__c>();
        BI_Criterio_de_asignacion__c cda = new BI_Criterio_de_asignacion__c(
            BI_Country__c = Label.BI_Peru,
            BI2_PER_Motivo_del_caso__c = 'Test',
            BI2_PER_Tipologia__c = 'Test',
            BI2_PER_Linea_de_Negocio__c = 'Test',
            BI2_PER_Limite_inferior__c = 60,
            BI2_PER_Territorio__c = 'Test',
            BI_Proceso_de_asignacion__c = proc.Id
        );
        lst_cda_toInsert.add(cda);

        BI_Criterio_de_asignacion__c cda1 = new BI_Criterio_de_asignacion__c(
            BI_Country__c = Label.BI_Peru,
            BI2_PER_Motivo_del_caso__c = 'Test1',
            BI2_PER_Tipologia__c = 'Test1',
            BI2_PER_Linea_de_Negocio__c = 'Test1',
            BI2_PER_Limite_inferior__c = 60,
            BI2_PER_Limite_superior__c = null,
            BI2_PER_Territorio__c = 'Test1',
            BI_Proceso_de_asignacion__c = proc1.Id
        );
        lst_cda_toInsert.add(cda1);

        BI_Criterio_de_asignacion__c cda2 = new BI_Criterio_de_asignacion__c(
            BI_Country__c = Label.BI_Peru,
            BI2_PER_Motivo_del_caso__c = 'Test2',
            BI2_PER_Tipologia__c = 'Test2',
            BI2_PER_Linea_de_Negocio__c = 'Test2',
            BI2_PER_Limite_inferior__c = null,
            BI2_PER_Limite_superior__c = 200,
            BI2_PER_Territorio__c = 'Test2',
            BI_Proceso_de_asignacion__c = proc2.Id
        );
        lst_cda_toInsert.add(cda2);

        BI_Criterio_de_asignacion__c cda3 = new BI_Criterio_de_asignacion__c(
            BI_Country__c = Label.BI_Peru,
            BI2_PER_Motivo_del_caso__c = 'Test3',
            BI2_PER_Tipologia__c = 'Test3',
            BI2_PER_Linea_de_Negocio__c = 'Test3',
            BI2_PER_Limite_inferior__c = null,
            BI2_PER_Limite_superior__c = null,
            BI2_PER_Territorio__c = 'Test3',
            BI_Proceso_de_asignacion__c = proc3.Id
        );
        lst_cda_toInsert.add(cda3);
        insert lst_cda_toInsert;

        SlaProcess sla = [SELECT Id FROM SlaProcess WHERE isActive = true LIMIT 1];
        Entitlement ent = new Entitlement(
            Name = 'Test',
            AccountId = lst_clients[0].Id,
            SlaProcessId = sla.Id,
            StartDate = Date.today()
        );
        insert ent;
    }
    
    @isTest static void test_method_one() {

		BI_TestUtils.throw_exception = false;
        
        insert new TGS_User_Org__c(TGS_Is_Admin__c=true, TGS_Is_BI_EN__c=true, TGS_Is_TGS__c=false);

        Map <String, RecordType> map_rt = new Map <String, RecordType>();
        for(RecordType rt : [SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName IN ('BI2_caso_comercial', 'BI2_Caso_Padre') AND sObjectType = 'Case']){
            map_rt.put(rt.DeveloperName, rt);
        }

        Account acc = [SELECT Id FROM Account WHERE BI_Country__c = :Label.BI_Peru LIMIT 1];
        Contact con = [SELECT Id FROM Contact WHERE Name LIKE 'TestContact_0%' Limit 1];

        List<Case> lst_toInsert = new List<Case>();
        Test.startTest();
        Case cas2 = new Case(
            RecordTypeId = map_rt.get('BI2_Caso_Padre').Id,
            Reason = 'Test',
            BI_Product_Service__c = 'Test',
            BI_Line_of_Business__c = 'Test',
            TGS_Disputed_number_of_lines__c = 120,
            BI2_PER_Territorio_caso__c = 'Test',
            BI2_per_tipologia__c = 'Test',
            BI2_per_subtipologia__c = 'Test',
            ContactId = con.Id,
            AccountId = acc.Id,
            BI_Country__c = Label.BI_Peru
        );
        lst_toInsert.add(cas2);

        Case cas3 = new Case(
            RecordTypeId = map_rt.get('BI2_caso_comercial').Id,
            Reason = 'Test1',
            BI_Product_Service__c = 'Test',
            BI_Line_of_Business__c = 'Test1',
            TGS_Disputed_number_of_lines__c = 65,
            BI2_PER_Territorio_caso__c = 'Test1',
            BI2_per_tipologia__c = 'Test1',
            BI2_per_subtipologia__c = 'Test1',
            ContactId = con.Id,
            AccountId = acc.Id,
            BI_Country__c = Label.BI_Peru
        );
        lst_toInsert.add(cas3);

        Case cas4 = new Case(
            RecordTypeId = map_rt.get('BI2_caso_comercial').Id,
            BI_Product_Service__c = 'Test',
            Reason = 'Test2',
            BI_Line_of_Business__c = 'Test2',
            TGS_Disputed_number_of_lines__c = 65,
            BI2_PER_Territorio_caso__c = 'Test2',
            BI2_per_tipologia__c = 'Test2',
            BI2_per_subtipologia__c = 'Test2',
            ContactId = con.Id,
            AccountId = acc.Id,
            BI_Country__c = Label.BI_Peru
        );
        lst_toInsert.add(cas4);

        Case cas5 = new Case(
            RecordTypeId = map_rt.get('BI2_caso_comercial').Id,
            BI_Product_Service__c = 'Test',
            Reason = 'Test3',
            BI_Line_of_Business__c = 'Test3',
            TGS_Disputed_number_of_lines__c = 65,
            BI2_PER_Territorio_caso__c = 'Test3',
            BI2_per_tipologia__c = 'Test3',
            BI2_per_subtipologia__c = 'Test3',
            ContactId = con.Id,
            AccountId = acc.Id,
            BI_Country__c = Label.BI_Peru
        );
        lst_toInsert.add(cas5);

        ////////Insert caso Padre//////////////
        insert lst_toInsert;
        Case toAssert1 = [SELECT EntitlementId FROM Case WHERE Id =: cas2.Id LIMIT 1];
        System.assertNotEquals(toAssert1.EntitlementId, [SELECT Id FROM Entitlement WHERE Name = 'Test' LIMIT 1].Id);
  
        /////////////////Update////////////////////
        cas2.RecordTypeId = map_rt.get('BI2_caso_comercial').Id;
        update cas2;

        ///Aumentamos la cobertura modificando los limites
        //con solo limite superior
        BI_Criterio_de_asignacion__c cda = [SELECT Id FROM BI_Criterio_de_asignacion__c LIMIT 1];

        cda.BI2_PER_Limite_inferior__c = null;
        cda.BI2_PER_Limite_superior__c = 60;
        update cda;
        cas2.TGS_Disputed_number_of_lines__c = 40;
        update cas2;

        //sin limites
        cda = [SELECT Id FROM BI_Criterio_de_asignacion__c LIMIT 1];
        cda.BI2_PER_Limite_inferior__c = null;
        cda.BI2_PER_Limite_superior__c = null;
        update cda;
        cas2.TGS_Disputed_number_of_lines__c = null;
		//update cas2;
        cas3.EntitlementId = null;
        update cas3;

		Test.stopTest();

		Case toAssert = [SELECT EntitlementId FROM Case WHERE Id =: cas2.Id LIMIT 1];
		System.assertEquals(toAssert.EntitlementId, [SELECT Id FROM Entitlement WHERE Name = 'Test' LIMIT 1].Id);

		toAssert.EntitlementId = null;
		update toAssert;

		System.assertNotEquals(toAssert.EntitlementId, [SELECT Id FROM Entitlement WHERE Name = 'Test' LIMIT 1].Id);

		BI_assignEntitlementsBatch obj = new BI_assignEntitlementsBatch();
		Database.executeBatch(obj,200);

		Case[] toAssert4 = [SELECT EntitlementId FROM Case];
    }
}