/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for CWP_ClaimsRequests component
    Test Class:    CWP_ClaimsRequestCtrl_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    30/03/2017              Everis                    Initial Version
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public without sharing class CWP_UploadFileCtrl {
    
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    *
    * Author:        Eduardo Trujillo
    *
    * Company:       everis
    *
    * Description:   Method to set File data to a Case number
    * History:
    * <Date>                          <Author>                                <Code>              <Change Description>
    * 23/05/2017                      Eduardo Trujillo                                              Initial version
    */    
     @AuraEnabled
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType, String caso) { 
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = new Attachment();
        a.parentId = parentId;
        
        a.Body = EncodingUtil.base64Decode(base64Data);
        a.Name = fileName;
        a.ContentType = contentType;
        system.debug('Datos insert 2: '+parentId);
        system.debug('Datos insert 2: '+fileName);        
        system.debug('Datos insert 2: '+base64Data);        
        system.debug('Datos insert 2: '+contentType);        
        system.debug('Datos insert 2: '+caso);
        insert a;
        
        return a.Id;
    }
    
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    *
    * Author:        Eduardo Trujillo
    *
    * Company:       everis
    *
    * Description:   Method to set File data to a Case number
    * History:
    * <Date>                          <Author>                                <Code>              <Change Description>
    * 23/05/2017                      Eduardo Trujillo                                              Initial version
    */    
    @AuraEnabled
    public static Id saveTheChunk(String fileName, String base64Data, String contentType, String fileId, String caso) { 
        system.debug('Datos insert: '+fileName);        
        system.debug('Datos insert: '+base64Data);        
        system.debug('Datos insert: '+contentType);        
        system.debug('Datos insert: '+caso);
        system.debug('Datos insert: '+fileId);
        
        if (fileId == '') {
            //crear workinfo-> el parent Id cambiarlo por el id del workinfo
            TGS_Work_Info__c wi = new TGS_Work_Info__c();
            wi.TGS_Case__c = getCaseIdFromNumber(caso);
            insert wi;
            //fileId = saveTheFile(parentId, fileName, base64Data, contentType, caso);
            fileId = saveTheFile(wi.Id, fileName, base64Data, contentType, caso);
        } else {
            appendToFile(fileId, base64Data);
        }
        
        return Id.valueOf(fileId);
    }
    
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    *
    * Author:        Eduardo Trujillo
    *
    * Company:       everis
    *
    * Description:   Method to set File data to a Case number
    * History:
    * <Date>                          <Author>                                <Code>              <Change Description>
    * 23/05/2017                      Eduardo Trujillo                                              Initial version
    */    
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = [
            SELECT Id, Body
            FROM Attachment
            WHERE Id = :fileId
        ];
        
        String existingBody = EncodingUtil.base64Encode(a.Body);
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data); 
        
        update a;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    *
    * Author:        Eduardo Trujillo
    *
    * Company:       everis
    *
    * Description:   Method to get Id Case data from a Case number
    * History:
    * <Date>                          <Author>                                <Code>              <Change Description>
    * 23/05/2017                      Eduardo Trujillo                                              Initial version
    */    
   public static Id getCaseIdFromNumber(String caso){
        Case item = 
            [select Id from Case where CaseNumber = :caso];
        return item.id;
    }
}