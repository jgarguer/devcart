public with sharing class PCA_Facturacion_PopUpDetail  extends PCA_HomeController{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Class for show facturación details 
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public List<FieldSetHelper.FieldSetResult> fieldSetRecordA {get; set;}
    public List<FieldSetHelper.FieldSetResult> fieldSetRecordB {get; set;}
    public List<FieldSetHelper.FieldSetResult> fieldSetRecord{get;set;}
    public Id factId{get;set;}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   check permissions of current user
    
    History:
    
    <Date>            <Author>              <Description>
    28/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference checkPermissions (){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_Facturacion_PopUpDetail.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Constructor
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PCA_Facturacion_PopUpDetail() {}
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Load info of BI_Facturas__c records
    
    History:
    
    <Date>            <Author>              <Description>
    05/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void loadInfo (){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }
            String objectName = 'BI_Facturas__c';
            
            factId = (apexpages.currentpage().getparameters().get('id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('id')):null;
            String fieldSetName='PCA_DetailFacturas'; 
            if (factId != null){
                if(fieldSetName=='PCA_DetailFacturas')
                {
                    //Set<String> textValues = FieldSetHelper.textAreaFields(fieldSetName, objectName);
                    List<FieldSetHelper.FieldSetResult>  fieldSetRecord = FieldSetHelper.FieldSetHelperSimple(fieldSetName, objectName, factId);
                    
                    //this.fieldSetRecord = new List<FieldSetHelper.FieldSetResult>();          
                    Integer size = fieldSetRecord.size();
                    
                    this.fieldSetRecordA = new List<FieldSetHelper.FieldSetResult>();
                    this.fieldSetRecordB = new List<FieldSetHelper.FieldSetResult>();           
                    
                    
                    for (Integer i=0; i<(size/2); i++)
                    {
                        FieldSetHelper.FieldSetResult value = fieldSetRecord[i]; 
                        this.fieldSetRecordA.add(value);
                    }
                    
                    for (Integer i=(size/2); i<size-1; i++)
                    {
                        FieldSetHelper.FieldSetResult value = fieldSetRecord[i]; 
                        this.fieldSetRecordB.add(value);
                    }                                                      
                }
            }
        }catch(Exception Exc){
            BI_LogHelper.generate_BILog('PCA_Facturacion_PopUpDetail.loadInfo', 'Portal Platino', Exc, 'Class');
        }
        
    
}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Create Task for User
    
    History:
    
    <Date>            <Author>              <Description>
    18/06/2015        Antonio Moruno       Initial version
    07/12/2015        Guillermo Muñoz      Prevent the email send in Test class 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public void createTask (){
        try{
            if(BI_TestUtils.isRunningTest()){
              throw new BI_Exception('test');
            }
            Id FacturaId = (apexpages.currentpage().getparameters().get('id')!=null)?String.escapeSingleQuotes(apexpages.currentpage().getparameters().get('id')):null;
            List<BI_Facturas__c> factsToTask = [Select Id, Name FROM BI_Facturas__c where id = :FacturaId];
            Task newTask = new Task();
            if(!factsToTask.IsEmpty()){
            newTask.Subject = 'Factura '+factsToTask[0].Name;
            }else{
            newTask.Subject = 'Factura';
            }
            Id accId = BI_AccountHelper.getCurrentAccountId();
            newTask.OwnerId = BI_AccountHelper.getCurrentAccount(accId).Owner.Id;
            List<BI_Contact_Customer_Portal__c> CCP = [SELECT BI_Contacto__c FROM BI_Contact_Customer_Portal__c where BI_User__c =: Userinfo.getUserId() AND BI_Cliente__c=: BI_AccountHelper.getCurrentAccountId() LIMIT 1];
            
            if(!CCP.IsEmpty()){
                
                newTask.WhoId = CCP[0].BI_Contacto__c;
                system.debug('WhoId:' + newTask.WhoId);
            }
            system.debug('CCP** :' +CCP);
            if(FacturaId!=null){
                newTask.WhatId = FacturaId;
            } 
            newTask.Status = Label.BI_NoIniciada;
            newTask.Priority = 'Normal';
            //newTask.IsReminderSet = true;
            system.debug('newTask****: ' + newTask);
            Database.Saveresult res = Database.insert(newTask,false);
            system.debug('result****: ' + res);
            List<EmailTemplate> templates = [Select Id, Name, HTMLValue, Subject, DeveloperName, Body from EmailTemplate where DeveloperName='PCA_comunicacion_nueva_tarea'];
            //List<User> users = [select Id, Email from User where Id= :newTask.OwnerId];
            
            if(!templates.isEmpty()){
                List<Messaging.SingleEmailMessage> emailSend = new List<Messaging.SingleEmailMessage>();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                //String[] emailAddresses = new String[]{};
                //emailAddresses.add(users[0].Email);         
                //mail.setToAddresses(emailAddresses);
                mail.setTargetObjectId(newTask.OwnerId);
                string body = templates[0].Body;
                system.debug('body: '+body);
                body = body.replace('{!Task.Subject}', newTask.Subject);
                body = body.replace('{!Task.Status}', newTask.Status);
                body = body.replace('{!Task.Priority}', newTask.Priority);
                //body = body.replace('{!Event.Subject}', item.Subject);
                //mail.setPlainTextBody(body);
                mail.setSubject(templates[0].Subject);
                mail.setPlainTextBody(body);
                emailSend.add(mail);
                if(!Test.isRunningTest()){
                    Messaging.sendEmail(emailSend);
                }
            }
            ConnectApi.ChatterMessage Msg = ConnectAPI.ChatterMessages.sendMessage('El usuario '+UserInfo.getName() +' ha pedido información acerca de la factura '+factsToTask[0].Name+'. ', newTask.OwnerId);
        }
        catch (exception Exc){
            system.debug('error');
            BI_LogHelper.generate_BILog('PCA_CatalogoProdController.createTask', 'Portal Platino', Exc, 'Class');
        }
    }
}