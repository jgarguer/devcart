@isTest
private class ADQ_OpcionesFacturacionController_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Adrián Caro
    Company:       Accenture
    Description:   test class about ADQ_NotaCreditoController
    
    History:
    
    <Date>            <Author>              <Description>
    03/01/2017       Adrián Caro          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	private static ADQ_OpcionesFacturacionController ofc;
	private static Account acc;
	private static NE__Order__c order;
	private static NE__OrderItem__c orderitem;
	private static Opportunity opp;
	private static Integer next, count, totalCuentas, indiceMatriz;
	private static List<Account> listaAcc;

	@testSetup
	private static void setUpTest(){
		//ejecutar con un perfil administrador para que no salten las validation rules
		Id idProfile =BI_DataLoad.searchAdminProfile();
		List<User> lst_user = BI_DataLoad.loadUsers(1, idProfile, Label.BI_Administrador);
		system.runAs(lst_user[0]){	
		//lista pais
		List <String> lstpais = BI_DataLoad.loadPaisFromPickList(2);
		//lista cuentas
		List <Account> lstacc = BI_DataLoad.loadAccounts(2, lstpais);
		//lista opp
		List <Opportunity> lstopp = BI_DataLoad.loadOpportunities(lstacc[0].Id);
		//lista Orders
		List <NE__Order__c> lstorder = BI_DataLoad.generateOrders(2, lstacc[0].Id, lstopp[0].Id);
		//lista OrderItems
		List <NE__OrderItem__c> lstoi = BI_DataLoad.generateOrderitems(2, lstorder[0].Id, lstacc[0].Id);		
		//lista matrices
		List <List<Account>> lstacc2 = new List <List<Account>>();
		lstacc2.add(lstacc);

		}
	}	

	@isTest
	public static void refresh_test(){
		acc = [select Id from Account Limit 1];
		ofc = new ADQ_OpcionesFacturacionController();

		ofc.refresh();
	}

	@isTest
	public static void generarListas_test(){
		BI_TestUtils.throw_exception = false;
		Set<String> setOpp = new Set<String>();
		acc = [select Id from Account Limit 1];
		opp = [select Id, BI_Country__c, CloseDate, Probability, StageName from Opportunity Limit 1];		
		orderitem = [select Id, Factura__c, NE__OrderId__c from NE__OrderItem__c Limit 1];
		order = [select Id, NE__OptyId__c, Etapa__c, NE__OptyId__r.StageName from NE__Order__c  Limit 1];
		
        opp.BI_Country__c = Label.BI_Mexico;
		opp.Probability = 100;
		opp.CloseDate = Date.today();
		update opp;
            system.debug('oppopciones: '+opp );
        	system.debug('orderopciones: '+order );
		 Map<Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(Userinfo.getUserId(), true, true, false, false);
            try{
            	opp.StageName = Label.BI_F1Ganada;
                update opp;
            }catch(Exception e){}

            finally{

                if(mapa != null){

                    BI_MigrationHelper.disableBypass(mapa);
                }
            }
         
        order = [select Id, NE__OptyId__c, Etapa__c, NE__OptyId__r.StageName from NE__Order__c  Limit 1];
        system.debug('oppopciones2: '+opp );
        system.debug('orderopciones2: '+order );

		ofc = new ADQ_OpcionesFacturacionController();
		ofc.getListaGenerada();
		ofc.generarListas();

		acc = [select Id from Account Limit 1];
		order = [select Id from NE__Order__c Limit 1];
		orderitem = [select Id from NE__OrderItem__c Limit 1];
		ofc = new ADQ_OpcionesFacturacionController();
		ofc.cuentaSeleccionada = 'Todas';
		ofc.getListaGenerada();
		ofc.generarListas();

		acc = [select Id, BI_Segment__c, Name from Account Limit 1];
		order = [select Id from NE__Order__c Limit 1];
		orderitem = [select Id from NE__OrderItem__c Limit 1];
		ofc = new ADQ_OpcionesFacturacionController();
		ofc.cuentaSeleccionada = 'Otra';
		ofc.getListaGenerada();
		ofc.generarListas();

		ofc = new ADQ_OpcionesFacturacionController();
		ofc.count =-1;
		ofc.cuentaSeleccionada = 'Otra';
		List <Account> lstacc = new List <Account>();
		lstacc.add(acc);
		List <List <Account>> lstacc2 = new List <List<Account>>();
		lstacc2.add(lstacc);
		ofc.matrizCuentasBack =lstacc2;
		ofc.cuentaSeleccionada = acc.Id;
		update acc;
		ofc.getListaGenerada();
		ofc.generarListas();

		ofc = new ADQ_OpcionesFacturacionController();
		Map <String,String> mapAccount = new Map <String, String>();
		mapAccount.put(acc.Id,acc.Name);
		ofc.mapCuentas= mapAccount;
		ofc.getListaGenerada();
		ofc.generarListas();
/*
		ofc = new ADQ_OpcionesFacturacionController();
		acc = [select Id, BI_Segment__c from Account Limit 1];
		Map<String, Account> mapCuentas = new Map<String, Account>();
		mapCuentas.put('test1',acc);
		System.debug(mapCuentas);
		ofc.generarListas();*/
	}

	@isTest
	public static void getTotalCuentas_test(){
		ofc = new ADQ_OpcionesFacturacionController();
		ofc.getTotalCuentas();
	}

	@isTest
	public static void getListaCuentasNext_test(){
		acc = [select Id from Account Limit 1];
		order = [select Id from NE__Order__c Limit 1];
		orderitem = [select Id from NE__OrderItem__c Limit 1];

		ofc = new ADQ_OpcionesFacturacionController();
		ofc.getListaCuentasNext();

	}

	@isTest
	public static void getListaCuentas_test(){
		acc = [select Id from Account Limit 1];

		ofc = new ADQ_OpcionesFacturacionController();
		List <Account> lstacc = new List <Account>();
		lstacc.add(acc);
		List <List <Account>> lstacc2 = new List <List<Account>>();
		lstacc2.add(lstacc);
		ofc.matrizCuentas =lstacc2;
		ofc.getListaCuentas();
	}

	@isTest
	public static void Next_test(){
		acc = [select Id from Account Limit 1];
		List <List<Account>> matrizCuentas = new List<List<Account>>();

		ofc = new ADQ_OpcionesFacturacionController();
		ofc.getShownext();
		ofc.Next();

		ofc = new ADQ_OpcionesFacturacionController();
		ofc.next = 20;
		ofc.count = 30;
		List <Account> lst_acc = new List <Account>();
		lst_acc.add(acc);

		upsert lst_acc;
		ofc.Next();

		ofc = new ADQ_OpcionesFacturacionController();
		ofc.next = -4;
		ofc.count = -2;
		List <Account> lstacc = new List <Account>();
		lstacc.add(acc);

		upsert lstacc;
		system.debug('listacc: '+lstacc);
		system.debug('suma: '+ ofc.count + ofc.next);
		ofc.Next();
	}

	@isTest
	public static void Prev_test(){
		acc = [select Id from Account Limit 1];

		ofc = new ADQ_OpcionesFacturacionController();
		ofc.next = 40;
		ofc.count = 40;
		ofc.indiceMatriz = 10;
		ofc.getShowprev();
		ofc.Prev();

		ofc = new ADQ_OpcionesFacturacionController();
		ofc.next = 40;
		ofc.count = 10;
		ofc.indiceMatriz = 10;
		ofc.getShowprev();
		List <Account> lstacc = new List <Account>();
		ofc.listaCuentasNext = lstacc;
		ofc.Prev();

		ofc = new ADQ_OpcionesFacturacionController();
		ofc.next = 40;
		ofc.count = 40;
		ofc.getShowprev();
		ofc.Prev();
	}

	@isTest
	public static void gets_test(){
		ofc = new ADQ_OpcionesFacturacionController();
		ofc.getLabelTabla();
		ofc.getClientIP();

	}


}