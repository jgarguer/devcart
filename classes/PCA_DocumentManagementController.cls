public with sharing class PCA_DocumentManagementController extends PCA_HomeController {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Salesforce.com
    Description:   Class extension for PCA_HomeController in Portal Platino 
    
    History:
    
    <Date>            <Author>          	<Description>
    08/07/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
		
	public PageReference checkPermissions (){
		try{
			if(BI_TestUtils.isRunningTest()){
				throw new BI_Exception('test');
			}
			PageReference page = enviarALoginComm();
			if(page == null){
				loadInfo();
			}
			return page;
		}catch (exception Exc){
		   BI_LogHelper.generate_BILog('PCA_DocumentManagementController.checkPermissions', 'Portal Platino', Exc, 'Class');
		   return null;
		}
	}


	public void loadInfo (){}
	public PCA_DocumentManagementController() {
	}

}