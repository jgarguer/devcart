/**
* Avanxo Colombia
* @author           Manuel Esthiben Mendez Devia href=<mmendez@avanxo.com>
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           		Descripción
*           -----   ----------      --------------------            		---------------
* @version   1.0    2015-07-22      Manuel Esthiben Mendez Devia (MEMD)     Create Test Class
					20/09/2017      Angel F. Santaliestra           		Add the mandatory fields on account creation: BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************************************/
@isTest 
public class BI_COL_FieldsFUNDoc_tst 
{

	public static list<BI_COL_Anexos__c> lstAnexos = new list <BI_COL_Anexos__c>();
  	public static list<BI_COL_Anexos__c> lstContratoFUN = new list <BI_COL_Anexos__c>();
  	public static list<BI_COL_Anexos__c> ListFraude = new list<BI_COL_Anexos__c>();
  	Public static list <UserRole> 											lstRoles;
	Public static User 													objUsuario;

  	public static list<BI_COL_Modificacion_de_Servicio__c> lstMS = new list <BI_COL_Modificacion_de_Servicio__c>();
  	public static Account  objCuenta;
  	public static Contract objContract;
  	public static BI_COL_Anexos__c objFun;
  	public static BI_COL_Anexos__c objFun2;
  	public static List<RecordType>  rtBI_FUN;
  	public static List <Profile> 							lstPerfil;
	public static List <User>	 							lstUsuarios;

  	static void CreateData()
  	{	
  		//perfiles
		lstPerfil             			= new List <Profile>();
		lstPerfil               		= [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
		system.debug('datos Perfil '+lstPerfil);
				
		//usuarios
		//lstUsuarios = [Select Id FROM User where Pais__c != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];
		//system.debug('lstUsuarios ====> '+lstUsuarios);

		//lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = new User();
        objUsuario.Alias = 'standt';
        objUsuario.Email ='pruebas@test.com';
        objUsuario.EmailEncodingKey = '';
        objUsuario.LastName ='Testing';
        objUsuario.LanguageLocaleKey ='en_US';
        objUsuario.LocaleSidKey ='en_US'; 
        objUsuario.ProfileId = lstPerfil.get(0).Id;
        objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        objUsuario.UserName ='pruebas@test.com';
        objUsuario.EmailEncodingKey ='UTF-8';
        objUsuario.UserRoleId = lstRoles.get(0).Id;
        objUsuario.BI_Permisos__c ='Sucursales';
        objUsuario.Pais__c='Colombia';
        insert objUsuario;
        
        lstUsuarios = new list<User>();
        lstUsuarios.add(objUsuario);

		System.runAs(lstUsuarios[0])
		{
			BI_bypass__c objBibypass = new BI_bypass__c();
			objBibypass.BI_migration__c=true;
			insert objBibypass;
		
  			objCuenta 										= new Account();
	        objCuenta.Name 									= 'prueba';
	        objCuenta.BI_Country__c 						= 'Colombia';
	        objCuenta.TGS_Region__c 						= 'América';
	        objCuenta.BI_Tipo_de_identificador_fiscal__c 	= 'NIT';
	        objCuenta.CurrencyIsoCode 						= 'GTQ';
	        objCuenta.BI_Segment__c                         = 'test';
			objCuenta.BI_Subsegment_Regional__c             = 'test';
			objCuenta.BI_Territory__c                       = 'test';
	        insert objCuenta;

	        //objContract = new Contract();
	        //objContract.AccountId = objCuenta.Id;
	        ////objContract.Status = 'Cancelled';	        
	        //objContract.StartDate= Date.newInstance(2015, 06, 30);

	        objContract 									= new Contract ();
			objContract.AccountId 							= objCuenta.Id;
			objContract.StartDate 							=  System.today().addDays(+5);
			objContract.BI_COL_Formato_Tipo__c 				= 'Contrato Marco';
			objContract.ContractTerm 						= 12;
			objContract.BI_COL_Presupuesto_contrato__c 		= 1000000;			
	        insert objContract;


	        rtBI_FUN = [select id from recordType where SobjectType = 'BI_COL_Anexos__c' and DeveloperName = 'BI_FUN'];
  			objFun = new BI_COL_Anexos__c();
	        objFun.Name          = 'FUN-0040000';
            objFun.RecordTypeId  = rtBI_FUN[0].Id;
            objFun.BI_COL_Contrato__c=objContract.Id;            
       	    insert objFun;

       	    objFun2 = new BI_COL_Anexos__c();
	        objFun2.Name          = 'FUN-0040001';
            objFun2.RecordTypeId  = rtBI_FUN[0].Id;            
            objFun2.BI_COL_Contrato_Anexos__c = objContract.Id;     
       	    insert objFun2;
       	}

  	}

	static testmethod void myTestMethod1() 
	{
		CreateData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_FieldsFUNDoc_pag;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('idFUN',objContract.Id);
		BI_COL_FieldsFUNDoc_ctr controllerclass = new BI_COL_FieldsFUNDoc_ctr();
		controllerclass.demo = 'Si';
		controllerclass.crearPdf();
		controllerclass.demo = 'Otro';
		controllerclass.crearPdf();
		controllerclass.retorno();
		controllerclass.validarAutoconsumo();				
		Test.stopTest();

	}

	static testmethod void myTestMethod2() 
	{
		CreateData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_FieldsFUNDoc_pag;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('idFUN',objContract.Id);
		BI_COL_FieldsFUNDoc_ctr controllerclass = new BI_COL_FieldsFUNDoc_ctr();		
		controllerclass.demo = 'seleccionar';
		controllerclass.cambioDemo();
		controllerclass.demo = 'Si';
		controllerclass.cambioDemo();
		controllerclass.demo = 'Otro';
		controllerclass.cambioDemo();
		controllerclass.demo = 'nada';
		controllerclass.cambioDemo();
		Test.stopTest();

	}

	static testmethod void myTestMethod3() 
	{
		CreateData();
		Test.startTest();
		PageReference pageRef = Page.BI_COL_FieldsFUNDoc_pag;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('idFUN',objFun.Id);
		BI_COL_FieldsFUNDoc_ctr controllerclass = new BI_COL_FieldsFUNDoc_ctr();		
		controllerclass.validarFraude();
		Test.stopTest();

	}

}