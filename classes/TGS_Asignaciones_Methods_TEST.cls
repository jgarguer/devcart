@isTest
public class TGS_Asignaciones_Methods_TEST
 /*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Juan Carlos Terrón
     Company:       Accenture
     Description:   Test method 
            
     <Date>                 <Author>                <Change Description>
     20/04/2017             Juan Carlos Terrón       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    
{
	public static testMethod void generate_TGS_AssignId_TEST(){
		String profile = 'TGS System Administrator';

        Profile miProfile = [SELECT Id, Name
                             FROM Profile
                             WHERE Name = :profile
                             LIMIT 1];

        insert new TGS_User_Org__c(TGS_Is_TGS__c = true);
        User usr = TGS_Dummy_Test_Data.dummyUserTGS('TGS_Perfil 1');
        usr.BI_Permisos__c = 'TGS';
        insert usr;
        TGS_Dummy_Test_Data.setUserType(usr.Id, true, false);

		System.runAs(usr)
		{
			Map<String,Account> map_Accounts = new Map<String,Account>();
			map_Accounts = TGS_Dummy_Test_Data.getAccountHierarchy();

			List<TGS_Asignaciones__c> list_Asignaciones = new List<TGS_Asignaciones__c>();

			TGS_Asignaciones__c test_Asignacion_01 = new TGS_Asignaciones__c(); 
			TGS_Asignaciones__c test_Asignacion_02 = new TGS_Asignaciones__c();
			TGS_Asignaciones__c test_Asignacion_03 = new TGS_Asignaciones__c();

			test_Asignacion_01.TGS_Account__c = map_Accounts.get(Constants.RECORD_TYPE_HOLDING_ACCOUNT_LABEL).Id;
			test_Asignacion_02.TGS_Account__c = map_Accounts.get(Constants.RECORD_TYPE_HOLDING_ACCOUNT_LABEL).Id;
			test_Asignacion_03.TGS_Account__c = map_Accounts.get(Constants.RECORD_TYPE_HOLDING_ACCOUNT_LABEL).Id;

			test_Asignacion_01.BI_Tipo_de_Caso__c = 'TGS_Billing_Inquiry';
			test_Asignacion_02.BI_Tipo_de_Caso__c = 'Order_Management_Case';
			test_Asignacion_03.BI_Tipo_de_Caso__c = 'CWP_OrderFromCWP';

			test_Asignacion_01.TGS_Status_Reason__c = 'In SD Provision';
			test_Asignacion_02.TGS_Status_Reason__c = 'In Order Handling';
			test_Asignacion_03.TGS_Status_Reason__c = 'In SD Validation';
			
			list_Asignaciones.add(test_Asignacion_01);
			list_Asignaciones.add(test_Asignacion_02);
			list_Asignaciones.add(test_Asignacion_03);

			Integer pointer = 1;
			for(TGS_Asignaciones__c ASG : list_Asignaciones)
			{
				ASG.TGS_Cola__c = 'Queue - '+pointer;
				ASG.TGS_Service__c = 'Service - '+pointer;
				ASG.TGS_Status__c = 'In Progress';
				ASG.BI_Tipo_de_Caso__c ='Order_Management_Case';
				pointer++;
			}
			Test.starttest();
			insert list_Asignaciones;

			list_Asignaciones = new List<TGS_Asignaciones__c>();

			for(TGS_Asignaciones__c ASG : [SELECT Id, TGS_Status__c, TGS_Status_Reason__c  FROM TGS_Asignaciones__c])
			{
				list_Asignaciones.add(new TGS_Asignaciones__c(Id = ASG.Id,TGS_Status_Reason__c = ''));
			}
			update list_Asignaciones;
			Test.stoptest();
		}
	}	
}