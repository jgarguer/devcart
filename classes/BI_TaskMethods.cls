public class BI_TaskMethods {
      /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Methods executed by Task Triggers 
    Test Class:    BI_TaskMethods_TEST
    
    History:
     
    <Date>                  <Author>                <Change Description>
    19/05/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
           
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if the Task BI_Relacionado_con_etapa__c field is null and sends a label if it's not.
    
    IN:            ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/05/2014              Ignacio Llorca          Initial Version     
    12/09/2014              Ignacio Llorca          Try/catch commented      
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void preventDeleteTask(List <Task> olds){
        //try{ Try/catch commented. The method is created to force an error
            for (Task old:olds){
                if (old.BI_Relacionado_con_etapa__c != null){
                    old.addError(Label.BI_ControlBorradoTareaAutomatica);
                }
            }
        //}catch (exception e){
          // BI_LogHelper.generateLog('BI_PromocionesMethods.preventDeleteTask', 'BI_EN', e.getMessage(), 'Trigger');
        //}
        
    }

      
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Method that checks if the Task BI_Relacionado_con_etapa__c field is null and sends a label if it's not.
    
    IN:            ApexTrigger.Old
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/05/2014              Ignacio Llorca          Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateProjectTask(List <Task> news, List <Task> olds){
        try{
            NETriggerHelper.setTriggerFired('BI_Milestone1TaskMethods.updateProjectTask');
            Set <Id> set_tsks = new Set <Id> ();
    
            for (Task tasks:news){
                set_tsks.add(tasks.WhatId);
            }
            Integer i = 0;
        
            List <Milestone1_Task__c> lst_mtask = [SELECT Id, Complete__c, Task_Stage__c FROM Milestone1_Task__c WHERE Id IN :set_tsks];
            
            for (Task tasks:news){
                for (Milestone1_Task__c mtask:lst_mtask){
                    if (olds[i].Status != Label.BI_TaskStatus_Completed && tasks.Status == Label.BI_TaskStatus_Completed && mtask.Id == tasks.WhatId){              
                        mtask.Complete__c = true;
                        mtask.Task_Stage__c='Resolved';
                    }
                }
                i++;
            }
            update lst_mtask;
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('BI_TaskMethods.updateProjectTask', 'BI_EN', Exc, 'Trigger');
              }
            }
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   
    
    IN:            ApexTrigger.New
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>            <Change Description>
    29/01/2015              Pablo Oliva         Initial Version          
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void preventTaskForActionPlan(List<Task> news){
        
        try{
            
            List<Task> lst_task = new List<Task>();
            Set<Id> set_ap = new Set<Id>();
              
            Schema.DescribeSObjectResult r = BI_Plan_de_accion__c.sObjectType.getDescribe();
            String keyPrefix = r.getKeyPrefix();
              
            for(Task task:news){
                if(task.WhatId != null && String.valueOf(task.WhatId).startsWith(keyPrefix)){
                    lst_task.add(task);
                    set_ap.add(task.WhatId);
                }
            }
                
            if(!lst_task.isEmpty()){
                  
                Map<Id, BI_Plan_de_accion__c> map_ap = new Map<Id, BI_Plan_de_accion__c> ([select Id, BI_Estado__c from BI_Plan_de_accion__c WHERE ID IN :set_ap]);
                List<RecordType> lst_rt = [select Id from RecordType where SobjectType = 'Task' and DeveloperName = 'BI_Acuerdo_Plan_de_accion' limit 1];
                  
                for(Task task2:lst_task){
                     system.debug('#Debug task2'+datetime.now()+ task2.RecordTypeId);
                    if(task2.RecordTypeId == lst_rt[0].Id &&
                       (map_ap.containsKey(task2.WhatId) && map_ap.get(task2.WhatId).BI_Estado__c != Label.BI_En_creacion)
                       )
                    {
                        task2.addError(Label.BI_Tareas_Plan_Accion);
                    }       
                }
                
              }
              
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_TaskMethods.preventTaskForActionPlan', 'BI_EN', Exc, 'Trigger');
            }
        
      }
    
      
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   
    
    IN:            ApexTrigger.New
    OUT:           Void
    
    History: 
    
    <Date>                  <Author>            <Change Description>
    29/01/2015              Pablo Oliva         Initial Version    
    23/10/2015              Micah Burgos        Add Status :Label.BI_TaskStatus_DerivativeReplicate like BI_TaskStatus_Completed. eHelp 01405608.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateActionPlan(List<Task> news){
        
        try{
        
            List<BI_Plan_de_accion__c> lst_ap = new List<BI_Plan_de_accion__c>();
            Set<Id> set_ap = new Set<Id>();
            
            Schema.DescribeSObjectResult r = BI_Plan_de_accion__c.sObjectType.getDescribe();
            String keyPrefix = r.getKeyPrefix();
            
            for(Task task:news){
                system.debug('WHAT_ID: ' + String.valueOf(task.WhatId));
                system.debug('keyPrefix: ' + keyPrefix);
                if(String.valueOf(task.WhatId).startsWith(keyPrefix))
                    set_ap.add(task.WhatId);
            }
                
            if(!set_ap.isEmpty()){
              //(Derivado, Derivado por replica, Finalizado)
                for(BI_Plan_de_accion__c ap:[select Id, (select Id from Tasks where Status  NOT IN (:Label.BI_TaskStatus_Completed, :Label.BI_TaskStatus_DerivativeReplicate) AND RecordType.DeveloperName = 'BI_Acuerdo_Plan_de_accion') from BI_Plan_de_accion__c where Id IN :set_ap AND BI_Estado__c <> :Label.BI_Pendiente_finalizacion]){
                    system.debug('ap.Tasks: ' + ap.Tasks);
                    if(ap.Tasks.isEmpty()){
                        ap.BI_Estado__c = Label.BI_PlanAccion_Estado_Pendiente_finalizacion;
                        ap.BI_Casilla_desarrollo__c = true;
                        lst_ap.add(ap);
                    }
                }
                system.debug('LIST TO UPD: ' + lst_ap);
                update lst_ap;
                
            }
            
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_TaskMethods.updateActionPlan', 'BI_EN', Exc, 'Trigger');
        }
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Method that stop task update if this has a Plan de acción on State IN (Finalizado, Anulado)
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    03/02/2014                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void check_HasClosedPlanDeAccion(List<Task> news){
        
        try{
        
            List<BI_Plan_de_accion__c> lst_ap = new List<BI_Plan_de_accion__c>();
            Set<Id> set_ap = new Set<Id>();
            
            Schema.DescribeSObjectResult r = BI_Plan_de_accion__c.sObjectType.getDescribe();
            String keyPrefix = r.getKeyPrefix();
            
            List<Task> lst_task = new List<Task>();
            for(Task task:news){
                if(String.valueOf(task.WhatId).startsWith(keyPrefix))
                    set_ap.add(task.WhatId);
                    lst_task.add(task);
            }
            Integer i = 0;
            
            if(!set_ap.isEmpty()){
            
                Map<Id,BI_Plan_de_accion__c> map_ap = new Map<Id,BI_Plan_de_accion__c>([select Id from BI_Plan_de_accion__c where Id IN :set_ap AND (BI_Estado__c = :Label.BI_PlanAccion_Estado_Finalizado OR BI_Estado__c = :Label.BI_PlanAccion_Estado_Cancelado)]);
                for(Task tsk :lst_task){
                    if(map_ap.containsKey(tsk.WhatId)){
                        tsk.addError(Label.BI_Msg_NoModificarSiActionPlanCerrado);
                    }
                }
            }
            
        }catch(Exception exc){
            BI_LogHelper.generate_BILog('BI_TaskMethods.updateActionPlan', 'BI_EN', Exc, 'Trigger');
        }
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Guillermo Muñoz
    Company:       Aborda
    Description:   Method that update the value of the field BI_Recurrente if the Task have recurrence or not
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    30/09/2015                      Guillermo Muñoz             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void checkRecurrenceTask(List<Task> news){

        for(Task tsk : news){
            if(tsk.RecurrenceActivityId != null){
                tsk.BI_Recurrente__c=true;
            }
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Iñaki Frial
    Company:       Accenture
    Description:   Method that updates the value of the field HO_Task_Blockers_Pendants__c if it has blocking tasks
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    05/03/2018                     Iñaki Frial                  Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void checkBlockingTask (List<Task> news){
        List <Task> lst_tsks = new List <Task> ();
        Set<Id> lst_eSofs = new Set<Id>();
        Set<Id> lst_serviceUnit = new Set<Id>();
        List<HO_Service_Unit__c> lst_auxSerUnit= new List <HO_Service_Unit__c>();
        List<HO_Service_Unit__c> set_toProcessSerUnit= new List<HO_Service_Unit__c>();
        List<HO_e_SOF__c> lst_auxeSoft= new List <HO_e_SOF__c>();
        List<HO_e_SOF__c> set_toProcess= new List<HO_e_SOF__c>();
        Integer cont;
        try{
            Id devRecordTypeId = [Select id,SobjectType from RecordType where DeveloperName ='HO_Tarea_Bloqueante' and SobjectType ='Task'][0].Id;
            system.debug('RecordType' + devRecordTypeId);
            for (Task tasks:news){
                if(tasks.WhatId!= null){
                    if(tasks.RecordTypeId==devRecordTypeId)
                    lst_tsks.add(tasks);
                    Schema.SObjectType sobjectType = tasks.WhatId.getSObjectType();
                    String sobjectName = sobjectType.getDescribe().getName();
                    system.debug('Object name ' + sobjectName);
                    if(sobjectName== 'HO_e_SOF__c'){
                        lst_eSofs.add(tasks.WhatId);
                    }
                    else if(sobjectName=='HO_Service_Unit__c'){
                        lst_serviceUnit.add(tasks.WhatId);
                    }
                    system.debug('Lista Tareas' + lst_tsks);
                }
            }
            //Si las tareas son del objeto e-Soft
            if(!lst_eSofs.isEmpty()){
                lst_auxeSoft= [Select id, HO_Status__c, (Select id, Status from Tasks where Status!='Completed' OR Status!='Cancelled')from HO_e_SOF__c where Id IN:lst_eSofs];
                if(!lst_auxeSoft.isEmpty()){
                    for(HO_e_SOF__c eSof: lst_auxeSoft){
                        cont=0;
                        for(Task tarea: eSof.Tasks){
                            if(tarea.Status!='Completed' && tarea.Status!='Cancelled'){
                                cont = cont +1;
                                system.debug('Contador1 ' + cont);
                            }
                        }
                        system.debug('Lita tareas eSof ' + eSof.Tasks.size());
                        if(cont >= 1){
                            eSof.HO_Tareas_Bloqueantes_Pendientes__c= true;
                            set_toProcess.add(eSof);
                        }
                        else
                        {
                            eSof.HO_Tareas_Bloqueantes_Pendientes__c= false;
                            set_toProcess.add(eSof);
                        }
                    }
                }
            }
            if(!set_toProcess.isEmpty()){
                update set_toProcess;
            }
            //Si las tareas son del objeto Service Unit
            if(!lst_serviceUnit.isEmpty()){
                 lst_auxSerUnit= [Select id, HO_Status__c, (Select id, Status from Tasks where Status!='Completed' OR Status!='Cancelled')from HO_Service_Unit__c where Id IN:lst_serviceUnit];
                 if(!lst_auxSerUnit.isEmpty()){
                    for(HO_Service_Unit__c serUnit: lst_auxSerUnit){
                        cont=0;
                        for(Task tarea: serUnit.Tasks){
                             if(tarea.Status!='Completed' && tarea.Status!='Cancelled'){
                                cont = cont +1;
                                system.debug('Contador3 SerUnit  ' + cont);
                            }
                        }
                        if(cont >= 1){
                            serUnit.HO_Tareas_Bloqueantes_Pendientes__c= true;
                            set_toProcessSerUnit.add(serUnit);
                        }
                        else{
                            serUnit.HO_Tareas_Bloqueantes_Pendientes__c=false;
                            set_toProcessSerUnit.add(serUnit);
                        }
                    }
                 }
            }
            if(!set_toProcessSerUnit.isEmpty()){
                update set_toProcessSerUnit;
            }
        }catch(Exception Exc){
             BI_LogHelper.generate_BILog('BI_TaskMethods.checkBlockingTask', 'BI_EN', Exc, 'Trigger');
        }
    }
}