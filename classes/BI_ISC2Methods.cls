public class BI_ISC2Methods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Methods executed by the BI_ISC2 trigger.
    History:
    
    <Date>            <Author>          <Description>
    04/07/2014        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    //final static Map<String,Id> MAP_NAME_PAIS = new Map<String,Id>();
    
    static{
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType IN ('BI_ISC2__c', 'BI_Plan_de_accion__c')]){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }
    
        //for(Region__c region :[SELECT Id, Name FROM Region__c]){
        //    MAP_NAME_PAIS.put(region.Name, region.Id);
        //}
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Method that assign the account.BI_ISC__c field
    
    IN:            ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    04/07/2014        Pablo Oliva       Initial version
    18/09/2014        Kevin Pereira     Cambiado campo BI_Opinion_Global_media_telefonica__c por BI_Satisfaccion_general_TEF__c
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void assignISC(List<BI_ISC2__c> news){
        
        try{
            Set<Id> set_accounts = new Set<Id>();
            for(BI_ISC2__c isc:news){
                set_accounts.add(isc.BI_Cliente__c);
            }
                
            if(!set_accounts.isEmpty()){
                Boolean ISC_found_termometro;
                Boolean ISC_found_other;
                
                List<Account> accountList = new List<Account>();
                //Obtain Accounts relatet with BI_ISC2__c Object
                for(Account acc:[select Id,BI_ISC__c,BI_Fecha_de_ultimo_ISC__c, (select Id, BI_Satisfaccion_general_TEF__c,RecordTypeId, BI_Fecha__c,BI_CHI_Promedio_del_termometro__c from ISC2__r order by BI_Fecha__c desc, CreatedDate desc limit 500) from Account where Id IN :set_accounts]){
                	system.debug('ISC ACC: ');
                    if(!acc.ISC2__r.isEmpty()){
                        //Update account
                        ISC_found_termometro = false;
						ISC_found_other = false;

                        for(BI_ISC2__c isc :acc.ISC2__r){
                        	system.debug('ISC: ' + isc);
	                        if(isc.RecordTypeId == MAP_NAME_RT.get('BI_CHI_Termometro') && !ISC_found_termometro){
	                        	acc.BI_CHI_Fecha_del_ultimo_termometro__c = Date.valueOf(isc.BI_Fecha__c);
	                        	acc.BI_CHI_Promedio_del_ultimo_termometro__c = isc.BI_CHI_Promedio_del_termometro__c;
								ISC_found_termometro = true;

	                    	}else if(!ISC_found_other){
	                    		acc.BI_ISC__c = isc.BI_Satisfaccion_general_TEF__c;
	                    		ISC_found_other = true;	
	                    	}

	                    	if(ISC_found_other && ISC_found_termometro){
	                    		break;
	                    	}

                    	}

                        accountList.add(acc);
                    }
                }
                system.debug('ISC ACC LIST TO UPDATE: ' + accountList);
                
                
                //Update Account
                update accountList;
            }
        }catch(Exception Exc){
            BI_LogHelper.generate_BILog('BI_ISC2Methods.assignISC', 'BI_EN', Exc, 'Trigger');
        }       
        }       
    
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   
    
    IN:            ApexTrigger.new
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    02/02/2015        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateAverage(List<BI_ISC2__c> news){
        
        if(!BI_GlobalVariables.stopTriggerISC){
        	
        	BI_GlobalVariables.stopTriggerISC = true;
        
	        try{
	        	
	        	//List<RecordType> lst_rType = [select Id from RecordType where DeveloperName = 'BI_CHI_Termometro' and sObjectType = 'BI_ISC2__c' limit 1];
	            List<BI_ISC2__c> lst_isc = new List<BI_ISC2__c>();
	            
	            for(BI_ISC2__c isc:news){
	            	if(isc.RecordTypeId == MAP_NAME_RT.get('BI_CHI_Termometro'))
	                	lst_isc.add(isc);
	            }
	            
	            if(!lst_isc.isEmpty()){
	            	
	            	for(BI_ISC2__c isc:lst_isc){
	                	
	                	isc.BI_CHI_Promedio_del_termometro__c = 0;
	                	
	                	Double total = 0;
	                	Double div = 0;
	                	
	                	SObjectType objToken = Schema.getGlobalDescribe().get('BI_ISC2__c');
	        			DescribeSObjectResult objDef = objToken.getDescribe();  
	        			Map<String, SObjectField> fields = objDef.fields.getMap();
	        			Set<String> fieldSet = fields.keySet(); 
	        			list <DescribeFieldResult> selectedField=new list<DescribeFieldResult>();
	        
				        for (String S:fieldSet){           
				            SObjectField fieldToken = fields.get(s);        
				            selectedField.add(fieldToken.getDescribe());
				        }
				        
				        for(Integer i=0; i<selectedField.size(); i++){    
				        	                   
				            if(selectedField[i].isAccessible() && selectedField[i].getName().startsWith('BI_CHI_')){
				            	
				            	if(selectedField[i].getName().substring(7,8).isNumeric()){
				            	
					            	if(isc.get(selectedField[i].getName()) != 'No evaluado' && isc.get(selectedField[i].getName()) != null){
					            		
					            		system.debug(selectedField[i].getName());
					            		system.debug('value 2: '+isc.get(selectedField[i].getName()));
					            		
						            	total += Double.valueOf(isc.get(selectedField[i].getName()));
			                			div++;
			                			
			                			system.debug(total);
			                			system.debug(div);
					            	}
				            	
				            	}
				            
				            }
				        }
	                	
	                	if(div > 0)
	                		isc.BI_CHI_Promedio_del_termometro__c = total/div;
	            	}
	            	
	            }
	            
	        }catch(Exception Exc){
	            BI_LogHelper.generate_BILog('BI_ISC2Methods.updateAverage', 'BI_EN', Exc, 'Trigger');
	        }
	        
        }
               
    }

     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   
    
    IN:            ApexTrigger.new
    OUT:           Void
    
    History:		Método que crea un "Plan de Accion" cuando el campo BI_O4_CSI_TEF_Total__c del objeto ISC es menor que 6
    
    <Date>            <Author>          <Description>
    14/09/2016        Miguel Cabrera    Initial version
    30/11/2016		  Alvaro García		Fix the method funcionality
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public static void createPlandeAccion(List<BI_ISC2__c> news){

    	Map <Id, Contact> mapContact = new Map <Id, Contact>();
    	Map <Id, AccountTeamMember> mapAccTM = new Map <Id, AccountTeamMember>();

	    try{	

	    	if (BI_TestUtils.isRunningTest()) {
				throw new BI_Exception('test');
			}

	    	List<Id> lstAccountId = new List<Id>();
	    	for(BI_ISC2__c item : news){
	    		lstAccountId.add(item.BI_Cliente__c);
	    	}
	    	
	    	List<Contact> lstContactoCuenta = [SELECT Id, AccountId FROM Contact WHERE AccountId IN: lstAccountId];
	    	System.debug('lstContactoCuenta: ' + lstContactoCuenta);

	    	for (Contact currentContact: lstContactoCuenta) {
	    		if (!mapContact.containsKey(currentContact.AccountId)) {
	    			mapContact.put(currentContact.AccountId, currentContact);
	    		}
	    	}

	    	List<AccountTeamMember> lstAtm = [SELECT AccountId, Id, TeamMemberRole, UserId FROM AccountTeamMember WHERE TeamMemberRole = :Label.BI_O4_Profile AND AccountId IN: lstAccountId];
	    	System.debug('lstAtm: ' + lstAtm);

	    	for (AccountTeamMember currentAccTM: lstAtm) {
	    		if (!mapAccTM.containsKey(currentAccTM.AccountId)) {
	    			mapAccTM.put(currentAccTM.AccountId, currentAccTM);
	    		}
	    	}
	    	
	    	List<BI_Plan_de_accion__c> lstPlanAccionInsert = new List<BI_Plan_de_accion__c>();
	    	
	    	for(BI_ISC2__c item : news){
	    		
	    		System.debug('BI_O4_CSI_TEF_Total__c: ' + item.BI_O4_CSI_TEF_Total__c);
				//Si la nota de un ISC CSI Map es menor que 6 se crea un Plan de Accion con tipo de registro "CIP" y será asignada al Global Service Executive de esa cuenta
	    		
	    		if (mapContact.containsKey(item.BI_Cliente__c) && mapAccTM.containsKey(item.BI_Cliente__c) && item.BI_O4_CSI_TEF_Total__c < 6) {
				    			
	    			BI_Plan_de_accion__c plan = new BI_Plan_de_accion__c();
	    			plan.Name = 'Plan de acción ' + item.BI_Cliente__c;
	    			plan.BI_O4_MNCs_Account__c = item.BI_Cliente__c;
	    			plan.RecordTypeId = MAP_NAME_RT.get('BI_O4_CIP');
	       			plan.OwnerId = mapAccTM.get(item.BI_Cliente__c).UserId;
	       			plan.BI_Fecha_de_inicio__c = Date.today();
	       			plan.BI_Cliente__c = item.BI_Cliente__c;
	       			plan.BI_Estado__c = 'En creación';
	       			plan.BI_Termometro_ISC__c = item.Id;
	       			//plan.BI_O4_Complete__c = '100 %';
	    			plan.BI_O4_Customer_Contact__c = mapContact.get(item.BI_Cliente__c).Id;
	    			plan.BI_O4_Type__c = 'Customer';
	    			//plan.BI_O4_Origin__c = ;
	    			//plan.BI_O4_Document_Link_on_Box__c = ;
	    			//plan.BI_Fecha_de_fin__c = ;
	    			//plan.BI_O4_Key_Area__c = ;
	    			//plan.BI_O4_Presentation_Date__c = ;

	    			lstPlanAccionInsert.add(plan);

	    		}
		    	
		    }
		    if (!lstPlanAccionInsert.isEmpty()) {
		    	insert lstPlanAccionInsert;
		    }

    	}catch(Exception e){

    		BI_LogHelper.generate_BILog('BI_ISC2Methods.createPlandeAccion', 'BI_O4', e, 'Trigger');
    	}
    }
}