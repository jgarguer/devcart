/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for lightning component CWP_NewIncidentForm
    Test Class:    

    History:

    <Date>                  <Author>                <Change Description>
    22/04/2017              Everis                    Initial Version

    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

public with sharing class CWP_NewIncidentFormController extends CWP_CreationFormController {

	@auraEnabled
	public static boolean doSave(Case paramCase) {
		System.debug('psvl: '+paramCase);
		return true;
	}
}