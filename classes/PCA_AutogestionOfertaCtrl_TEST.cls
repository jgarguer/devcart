@isTest
private class PCA_AutogestionOfertaCtrl_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for PCA_AutogestionOfertaCtrl class 
    
    History: 
    <Date>                  <Author>                <Change Description>
    12/08/2014              Ana Escrich             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for PCA_AutogestionOfertaCtrl.getHasNextTest.
            
    History: 
    
     <Date>                     <Author>                <Change Description>
    12/08/2014              Ana Escrich             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*static testMethod void getHasNextTest() {
        PCA_AutogestionOfertaCtrl constructor = new PCA_AutogestionOfertaCtrl();
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        constructor.index = 0;
        constructor.pageSize = 10;
        constructor.totalRegs = 10;
        boolean result = constructor.getHasNext();
        constructor.index = 1;
        result = constructor.getHasNext();
    }*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for PCA_AutogestionOfertaCtrl.getHasPrevious
            
    History: 
    
     <Date>                     <Author>                <Change Description>
    12/08/2014              Ana Escrich             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*static testMethod void getHasPreviousTest() {
        PCA_AutogestionOfertaCtrl constructor = new PCA_AutogestionOfertaCtrl();
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        constructor.index = 0;
        boolean result = constructor.getHasPrevious();
        constructor.index = 1;
        result = constructor.getHasPrevious();
    }*/
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ana Escrich
    Company:       Salesforce.com
    Description:   Test method to manage the code coverage for PCA_AutogestionOfertaCtrl.loadInfo
            
    History: 
    
     <Date>                     <Author>                <Change Description>
    12/08/2014              Ana Escrich             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getloadInfo() {
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());
        /*TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;*/
        //insert usr;
        system.debug('usr: '+usr);
        
        system.runAs(usr){
            BI_TestUtils.throw_exception = false;
            //List<Region__c> lst_pais = BI_DataLoad.loadPais(1);
            List<String> lst_pais = BI_DataLoad.loadPaisFromPickList(1);
            List<Account> acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
            
            List<Account> accList = new List<Account>();
            for(Account item: acc){
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            /* try{
               for(Account item: acc){
                    item.ownerid = lst_pais[0].Id;
                }
               update acc;
               system.assert(false);
              }catch (DmlException e){
               system.assert(true);
            }*/
            
            system.debug('acc: '+accList);
            List<Contact> con = BI_DataLoad.loadPortalContacts(1, accList);
            BI_DataLoad.loadOpportunities(accList[0].Id);
            system.debug('con: '+con);
            User user1 = BI_DataLoad.loadPortalUser(con[0].Id, BI_DataLoad.searchPortalProfile());
               
            system.runAs(user1){
                PCA_AutogestionOfertaCtrl constructor = new PCA_AutogestionOfertaCtrl();    
                constructor.checkPermissions();         
                constructor.loadInfo();
                constructor.PageValue = '25';
                constructor.searchRecords();
                constructor.getHasNext();
                constructor.getHasPrevious();
                constructor.Next();
                constructor.Previous();
                constructor.getItemPage();
                constructor.valuesInPage();
                constructor.Order();
                system.assert(constructor.searchFields != null && !constructor.searchFields.isEmpty());
            }
            system.runAs(user1){
                PCA_AutogestionOfertaCtrl constructor = new PCA_AutogestionOfertaCtrl();    
                constructor.checkPermissions();     
                constructor.PageValue = '25';   
                BI_TestUtils.throw_exception = true;
            }
            system.runAs(user1){
                PCA_AutogestionOfertaCtrl constructor = new PCA_AutogestionOfertaCtrl();    
                constructor.checkPermissions();  
                constructor.PageValue = '25';       
                constructor.loadInfo();
                BI_TestUtils.throw_exception = true;
                constructor.searchRecords();
            }
            system.runAs(user1){
                PCA_AutogestionOfertaCtrl constructor = new PCA_AutogestionOfertaCtrl();    
                constructor.checkPermissions();   
                constructor.PageValue = '25';      
                constructor.loadInfo();
                constructor.searchRecords();
                BI_TestUtils.throw_exception = true;
                constructor.getHasNext();
            }
            system.runAs(user1){
                PCA_AutogestionOfertaCtrl constructor = new PCA_AutogestionOfertaCtrl();    
                constructor.checkPermissions();  
                constructor.PageValue = '25';       
                constructor.loadInfo();
                constructor.searchRecords();
                constructor.getHasNext();
                BI_TestUtils.throw_exception = true;
                constructor.getHasPrevious();
            }
            system.runAs(user1){
                PCA_AutogestionOfertaCtrl constructor = new PCA_AutogestionOfertaCtrl();    
                constructor.checkPermissions();    
                constructor.PageValue = '25';     
                constructor.loadInfo();
                constructor.searchRecords();
                constructor.getHasNext();
                constructor.getHasPrevious();
                BI_TestUtils.throw_exception = true;
                constructor.Next();
            }
            system.runAs(user1){
                PCA_AutogestionOfertaCtrl constructor = new PCA_AutogestionOfertaCtrl();    
                constructor.checkPermissions();   
                constructor.PageValue = '25';      
                constructor.loadInfo();
                constructor.searchRecords();
                constructor.getHasNext();
                constructor.getHasPrevious();
                BI_TestUtils.throw_exception = true;
                constructor.Previous();
            }
            system.runAs(user1){
                PCA_AutogestionOfertaCtrl constructor = new PCA_AutogestionOfertaCtrl();    
                constructor.checkPermissions();  
                constructor.PageValue = '25';       
                constructor.loadInfo();
                constructor.searchRecords();
                constructor.getHasNext();
                constructor.getHasPrevious();
                constructor.Previous();
                constructor.getItemPage();
                constructor.valuesInPage();
                constructor.PageValue = '25';
                BI_TestUtils.throw_exception = true;
                constructor.Order();
            }
            
        }
        
        //Test.stopTest();
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}