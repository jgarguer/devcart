@isTest
private class BI_O4_DynLookUpPage_Test {

    static testMethod void myUnitTest()
    {
    	NE__Family__c fam = new NE__Family__c(Name='Smartphone');
        insert fam;
        NE__DynamicPropertyDefinition__c dpd1 = new NE__DynamicPropertyDefinition__c(Name='Color', NE__Type__c='String');
        insert dpd1;
        NE__ProductFamilyProperty__c pfp1 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=fam.Id, NE__PropId__c=dpd1.Id);
        insert pfp1;
        NE__DynamicPropertyDefinition__c dpd2 = new NE__DynamicPropertyDefinition__c(Name='Screen', NE__Type__c='String');
        insert dpd2;
        NE__ProductFamilyProperty__c pfp2 = new NE__ProductFamilyProperty__c(NE__FamilyId__c=fam.Id, NE__PropId__c=dpd2.Id);
        insert pfp2;
        
        NE__Product__c productTest	=	new NE__Product__c(name='testDylookProd');
        insert productTest;
        
        NE__ProductFamily__c productFamily	=	new NE__ProductFamily__c(NE__FamilyId__c=fam.Id, NE__ProdId__c=productTest.Id);
    	insert productFamily;
    	
        Account acc1 = new Account(Name='First', NE__Active__c='true', NE__Type__c='Business', Phone='0123456',BI_Segment__c='Negocios',BI_Subsegment_Regional__c='Negocios');
        Account acc2 = new Account(Name='Second', NE__Active__c='true', NE__Type__c='Business',BI_Segment__c='Negocios',BI_Subsegment_Regional__c='Negocios');
        Account acc3 = new Account(Name='Third', NE__Active__c='false', NE__Type__c='Consumer', Phone='0123489',BI_Segment__c='Negocios',BI_Subsegment_Regional__c='Negocios');
        Account acc4 = new Account(Name='Fourth', NE__Active__c='true', NE__Type__c='Business', Phone='012345611',BI_Segment__c='Negocios',BI_Subsegment_Regional__c='Negocios');
        insert acc1;
        insert acc2;
        insert acc3;
        insert acc4;
        
        NE__Dynamic_Lookup__c dl = new NE__Dynamic_Lookup__c();
        dl.NE__Name__c = 'Test Dynamic Lookup';
        dl.NE__Type__c = 'Standard';
        dl.NE__sObject__c = 'account';
        dl.NE__Columns__c = 'id, name, NE__type__c';
        dl.NE__Search_specification__c = 'NE__type__c = \'Business\' AND NE__active__c != \'false\'';
        dl.NE__Search_enabled__c = 'id, name, phone';
    	insert dl;
    	
    	NE__Lookup_Map__c lm = new NE__Lookup_Map__c();
    	lm.NE__Dynamic_Lookup__c = dl.Id;
    	lm.NE__Family__c = fam.Id;
    	lm.NE__Name__c = 'Test Lookup Map';
    	insert lm;
    	
    	NE__Lookup_Map_Value__c lmv1 = new NE__Lookup_Map_Value__c();
    	lmv1.NE__Lookup_Map__c = lm.Id;
    	lmv1.NE__Product_Family_Property__c = pfp1.Id;
    	lmv1.NE__Type__c = 'Field';
    	lmv1.NE__Value__c = 'description';
    	insert lmv1;
    	
    	NE__Lookup_Map_Value__c lmv2 = new NE__Lookup_Map_Value__c();
    	lmv2.NE__Lookup_Map__c = lm.Id;
    	lmv2.NE__Product_Family_Property__c = pfp2.Id;
    	lmv2.NE__Type__c = 'Value';
    	lmv2.NE__Value__c = '5';
    	insert lmv2;
    	
    	ApexPages.currentPage().getParameters().put('name', dl.NE__Name__c);
    	ApexPages.currentPage().getParameters().put('offline', 'true');
    	ApexPages.currentPage().getParameters().put('attributePfp', pfp1.Id);
    	ApexPages.currentPage().getParameters().put('productId', productTest.Id);
    	BI_O4_DynLookUpPage popup = new BI_O4_DynLookUpPage();
    	
    	popup.getFields();
    	popup.goToNextPage();
    	popup.goToPreviousPage();
    	Boolean nextpage = popup.thereIsNextPage;
    	Boolean prevpage = popup.thereIsPreviousPage;
    	
    	popup.newConditionRow();
    	popup.listOfConditions[0].field = 'phone';
    	popup.listOfConditions[0].operation = '!=';
    	popup.listOfConditions[0].value = '000000';
    	popup.listOfConditions[1].field = 'phone';
    	popup.listOfConditions[1].operation = '!=';
    	popup.listOfConditions[1].value = '';
    	popup.newConditionRow();
    	list<SelectOption> listOp = popup.listOfConditions[2].listOfOperations;
    	popup.selConditionRow = 2;
    	popup.deleteConditionRow();
    	popup.searchText = '1 OR 2';
    	popup.filterResults();
    	
    	popup.searchText = '';
    	popup.filterResults();
    	
    	popup.selectedSobjectId = acc1.Id;
    	popup.generateAttributeMap();
    }
}