@isTest
private class test_UtilSync {
    
    static testMethod void myUnitTest() {
      
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        UtilSync.isMyTest = true;
        
        Account acc = new Account(Name = 'test',
                                 BI_Segment__c = 'test', //27/09/2017
                                 BI_Subsegment_Regional__c = 'test', //27/09/2017
                                 BI_Territory__c = 'test' //27/09/2017
                                 );                
        insert acc;
        
        Account AccountResult = [select id, BI_Country__c, BI_Holding__c from Account where id = :acc.id];
        system.assert(AccountResult.BI_country__c == 'Global' && AccountResult.BI_Holding__c == 'Sí');
        
        Contact c = new Contact(LastName = 'Contact Test', email = 'sample@mail.com', accountId = acc.id, accountSync__c = acc.id);
        insert c;
        
        Contact resutContact = [select id, BI_Activo__c, BI_Country__c from Contact where id = :c.id];
        system.assert(resutContact.BI_Activo__c && resutContact.BI_Country__c == AccountResult.BI_Country__c);
        
    }   
       
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}