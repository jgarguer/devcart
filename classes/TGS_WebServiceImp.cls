@isTest     
global class TGS_WebServiceImp{
    global class TGS_WebServiceCreateChangeImpl implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           TGS_RodTicketingWs.OutputMapping7 respElement = 
               new TGS_RodTicketingWs.OutputMapping7();
           respElement.Operation_Status = 'OK';
           response.put('response_x', respElement); 
       }
    }                   
    global class TGS_WebServiceModifyChangeImpl implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           TGS_RodTicketingWs.OutputMapping8 respElement = 
               new TGS_RodTicketingWs.OutputMapping8();
           respElement.Operation_Status = 'OK';
           response.put('response_x', respElement); 
       }
    }
    global class TGS_WebServiceCreateIncidentImpl implements WebServiceMock {
      global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       TGS_RodTicketingWs.OutputMapping9 respElement = 
           new TGS_RodTicketingWs.OutputMapping9();
       respElement.Operation_Status = 'OK';
       response.put('response_x', respElement); 
      }
    }
    global class TGS_WebServiceModifyIncidentImpl implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           TGS_RodTicketingWs.OutputMapping10 respElement = 
               new TGS_RodTicketingWs.OutputMapping10();
           respElement.Operation_Status = 'OK';
           response.put('response_x', respElement); 
       }
    }
    global class TGS_WebServiceCreateWorkinfoImpl implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           TGS_RodTicketingWs.OutputMapping5 respElement = 
               new TGS_RodTicketingWs.OutputMapping5();
           respElement.Operation_Status = 'OK';
           response.put('response_x', respElement); 
       }
    }      
    global class TGS_WebServiceCreateAssetImpl implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           TGS_RoDAttASTWs.OutputMapping1 respElement = 
               new TGS_RoDAttASTWs.OutputMapping1();
           respElement.Error_Code= '20O';
           response.put('response_x', respElement); 
       }
    } 
    global class TGS_WebServiceSiteImpl implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           TGS_Site_Sync.OutputMapping1 respElement = 
               new TGS_Site_Sync.OutputMapping1();
            respElement.ReturnCode = 0;
           response.put('response_x', respElement); 
       }
    }       
}