public with sharing class BI_PlanDeAccionMethods {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Methods executed by the BI_CoordinadorCliente trigger.
    History:
    
    <Date>            <Author>          <Description>
    30/01/2015        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   
    
    IN:            ApexTrigger.new, ApexTrigger.old
    OUT:           Void
    
    History:
    
    <Date>            <Author>          <Description>
    30/01/2015        Pablo Oliva       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void updateOrigin(List<BI_Plan_de_accion__c> news, List<BI_Plan_de_accion__c> olds){
        
        try{
        
            List<BI_Plan_de_accion__c> lst_coor = new List<BI_Plan_de_accion__c>();
            Set<Id> set_coor = new Set<Id>();
            Set<Id> set_coorTask = new Set<Id>();
            
            Integer i = 0;
            for(BI_Plan_de_accion__c coor:news){
                
                if(coor.BI_Plan_original__c != null && coor.BI_Estado__c == Label.BI_PlanAccion_Estado_En_curso && olds[i].BI_Estado__c == Label.BI_PlanAccion_Estado_En_creacion){
                    
                    set_coorTask.add(coor.Id);
                    
                    if(coor.BI_Todos_los_acuerdos_duplicados__c)
                        set_coor.add(coor.BI_Plan_original__c);
                    
                }
                
                i++;
                
            }
            
            if(!set_coorTask.isEmpty()){
                
                Set<Id> set_tasks = new Set<Id>();
                for(Task task:[select Id, BI_Acuerdo_original__c from Task where WhatId IN :set_coorTask]){
                    if(task.BI_Acuerdo_original__c != null)
                        set_tasks.add(task.BI_Acuerdo_original__c);
                }
                
                if(!set_tasks.isEmpty()){
                    List<Task> lst_task_update = new List<Task>();
                    for(Task task2:[select Id from Task where Id IN :set_tasks]){
                        task2.Status = Label.BI_TaskStatus_DerivativeReplicate;
                        lst_task_update.add(task2);
                    }
                    
                    update lst_task_update;
                }
                
                if(!set_coor.isEmpty()){
                    for(BI_Plan_de_accion__c coor2:[select Id from BI_Plan_de_accion__c where Id IN :set_coor]){
                        coor2.BI_Estado__c = Label.BI_PlanAccion_Estado_Finalizado;
                        coor2.BI_Casilla_desarrollo__c = true;
                        lst_coor.add(coor2);
                    }
                    
                    update lst_coor;
                }
                
            }
        
        }catch(Exception exc){
            
            BI_LogHelper.generateLog('BI_PlanDeAccionMethods.updateOrigin', 'BI_EN', exc.getMessage(), 'Trigger');
            
        }
        
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Method that check if BI_PlanDeAccion__c has task with status = "En curso"    
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    03/02/2014                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void check_HasOpenTask(Map<Id,BI_Plan_de_accion__c> map_news, List<BI_Plan_de_accion__c> olds){
        
        try{
        
            List<BI_Plan_de_accion__c> lst_coor = new List<BI_Plan_de_accion__c>();
            Set<Id> set_coorTask = new Set<Id>();
            
            Integer i = 0;
            for(BI_Plan_de_accion__c coor:map_news.values()){
                
                if(coor.BI_Estado__c == Label.BI_PlanAccion_Estado_Finalizado && olds[i].BI_Estado__c != Label.BI_PlanAccion_Estado_Finalizado && !coor.BI_Casilla_desarrollo__c){
                    set_coorTask.add(coor.Id);
                }
                i++;
                
            }
            
            if(!set_coorTask.isEmpty()){
                for(Task task:[select Id, Status, WhatId from Task where WhatId IN :set_coorTask]){
                    if(task.Status == Label.BI_TaskStatus_InProgress){
                        map_news.get(task.WhatId).addError(Label.BI_Msg_NoFinalizarSiHayTareasEnCurso);
                    }
                }
            }
        
        }catch(Exception exc){
            BI_LogHelper.generateLog('BI_PlanDeAccionMethods.check_HasOpenTask', 'BI_EN', exc.getMessage(), 'Trigger');
        }
        
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Method that check if BI_PlanDeAccion__c has task with status = "En curso"    
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    11/02/2014                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void check_status(Map<Id,BI_Plan_de_accion__c> map_news, List<BI_Plan_de_accion__c> olds){
        
        try{
            List<BI_Plan_de_accion__c> lst_pa_canceled = new List<BI_Plan_de_accion__c>();
            List<Id> lst_OwnerId = new List<Id>();

            Integer i = 0;
            for(BI_Plan_de_accion__c pa :map_news.values()){
                if(olds[i].BI_Estado__c == Label.BI_PlanAccion_Estado_Finalizado && !pa.BI_Casilla_desarrollo__c){
                    pa.addError(Label.BI_Plan_de_accion_control_estado_Finalizado);
                }else if(pa.BI_Estado__c == Label.BI_PlanAccion_Estado_Cancelado && olds[i].BI_Estado__c != Label.BI_PlanAccion_Estado_Cancelado && !pa.BI_Casilla_desarrollo__c){
                    lst_OwnerId.add(pa.OwnerId);
                    lst_pa_canceled.add(pa);
                }else if(olds[i].BI_Estado__c == Label.BI_PlanAccion_Estado_En_curso && pa.BI_Estado__c == Label.BI_PlanAccion_Estado_En_curso && !pa.BI_Casilla_desarrollo__c){
                    pa.addError(Label.BI_Msg_NoEditarEnCurso);
                }
                i++;
            }

            if(!lst_pa_canceled.isEmpty()){
                Map<Id, Id> map_user_manager = new Map<Id,Id>();
                for(User usu :[SELECT Id, ManagerId FROM User WHERE Id IN :lst_OwnerId]){
                    map_user_manager.put(usu.Id, usu.ManagerId);
                }

                for(BI_Plan_de_accion__c pa : lst_pa_canceled){
                    if(!map_user_manager.containsKey(pa.OwnerId) || map_user_manager.get(pa.OwnerId) != UserInfo.getUserId() ){
                        pa.addError(Label.BI_Plan_de_accion_control_estado_Cancelado);
                    }
                }
            }
        }catch(Exception exc){
            BI_LogHelper.generateLog('BI_PlanDeAccionMethods.check_status', 'BI_EN', exc.getMessage(), 'Trigger');
        }
        
        
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Method that update Task if BI_PlanDeAccion__c.status change to "En curso"    
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    20/02/2014                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void update_taskStatus(Map<Id,BI_Plan_de_accion__c> map_news, List<BI_Plan_de_accion__c> olds){
        
        try{
            List<BI_Plan_de_accion__c> lst_pa_inProgress = new List<BI_Plan_de_accion__c>();

            Integer i = 0;
            for(BI_Plan_de_accion__c pa :map_news.values()){
                if(olds[i].BI_Estado__c != Label.BI_PlanAccion_Estado_En_curso && pa.BI_Estado__c == Label.BI_PlanAccion_Estado_En_curso){
                    lst_pa_inProgress.add(pa);
                }
                i++;
            }

            if(!lst_pa_inProgress.isEmpty()){
                List<Task> lst_task_update = new List<Task>();
                for(BI_Plan_de_accion__c pa :[SELECT Id,(SELECT Id FROM Tasks) FROM BI_Plan_de_accion__c WHERE Id IN :lst_pa_inProgress]){
                    for(Task tsk :pa.Tasks){
                        tsk.Status = Label.BI_TaskStatus_InProgress;
                        lst_task_update.add(tsk);
                    }
                }

                update lst_task_update;
            }
        }catch(Exception exc){
            BI_LogHelper.generateLog('BI_PlanDeAccionMethods.update_taskStatus', 'BI_EN', exc.getMessage(), 'Trigger');
        }
        
        
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Method is used to update CIP Plan de accion user details required for starting the escalation once CIP record(CIP/IA) is newly inserted. 
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    13/09/2016                      Miguel Cabrera                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static Boolean escalationFlag = true;

    public static void escalationAfterInsert(List<BI_Plan_de_accion__c> cipInsertedLst){
         
        String[] toAddresses;
        List<BI_Plan_de_accion__c> cipToToBeUpdatedLst = new List<BI_Plan_de_accion__c>();
        List<RecordType> rt = [SELECT r.Id FROM RecordType r WHERE r.DeveloperName  = 'BI_O4_IA' AND SobjectType = 'BI_Plan_de_accion__c'];  
         
        try{
            cipToToBeUpdatedLst = new List<BI_Plan_de_accion__c>();
            for(BI_Plan_de_accion__c updateCIPRecord : [SELECT BI_O4_Complete__c, BI_O4_Is_Escalated__c, RecordTypeId, BI_O4_Other_Comments__c, BI_Estado__c, BI_Plan_original__c, BI_O4_Stop_Escalation__c,
                                        BI_O4_Original_CIP_User__c, BI_O4_Next_Escalated_User__c, BI_O4_Next_Escalated_User__r.ManagerId, BI_O4_CIP_User__c,
                                        BI_O4_Original_CIP_User__r.ManagerId, BI_O4_Next_Escalated_User__r.UserRole.Name, BI_O4_CIP_User__r.ManagerId, 
                                        BI_Plan_original__r.BI_O4_Original_CIP_User__c, BI_Plan_original__r.BI_O4_Original_CIP_User__r.ManagerId, Owner.Email,
                                        BI_O4_Last_Active_Date_Time__c, BI_O4_Next_Escalated_User__r.Manager.UserRole.Name, BI_O4_CIP_User__r.Email, BI_O4_Last_Escalation_Date_Time__c
                                        FROM BI_Plan_de_accion__c WHERE Id IN: cipInsertedLst]){
                 
                /*
                  Below block will allow to reset the CIP user details once IA child record of CIP is created newly.
                  It will also reset the isEscalation flag as false (isEscalation flag is used in Workflow rule "Escaaltion Rule")
                */ 
                if(rt.size() > 0){
                    if(updateCIPRecord.RecordTypeId == rt[0].Id){
                        BI_Plan_de_accion__c cipRecrd = new BI_Plan_de_accion__c (Id = updateCIPRecord.BI_Plan_original__c, BI_O4_Last_Active_Date_Time__c = datetime.Now(),BI_O4_Last_Escalation_Date_Time__c = datetime.now(), BI_O4_Is_Escalated__c = false,
                        BI_O4_CIP_User__c = updateCIPRecord.BI_Plan_original__r.BI_O4_Original_CIP_User__c != null ? updateCIPRecord.BI_Plan_original__r.BI_O4_Original_CIP_User__c:updateCIPRecord.BI_O4_CIP_User__c, 
                        BI_O4_Next_Escalated_User__c = updateCIPRecord.BI_Plan_original__r.BI_O4_Original_CIP_User__r.ManagerId != null ? updateCIPRecord.BI_Plan_original__r.BI_O4_Original_CIP_User__r.ManagerId: updateCIPRecord.BI_O4_Next_Escalated_User__c);
                        cipToToBeUpdatedLst.add(cipRecrd);
                        system.debug('parent update cipToToBeUpdatedLst---' + cipToToBeUpdatedLst);
                    }
                    else{
                        //Below block will allow to set the CIP user details related to escalation not for IA
                        updateCIPRecord.BI_O4_Last_Active_Date_Time__c = datetime.now();
                        updateCIPRecord.BI_O4_Last_Escalation_Date_Time__c = datetime.now();
                        updateCIPRecord.BI_O4_Next_Escalated_User__c = updateCIPRecord.BI_O4_CIP_User__r.ManagerId;
                        updateCIPRecord.BI_O4_Original_CIP_User__c = updateCIPRecord.BI_O4_CIP_User__c;
                        //updateCIPRecord.CIP_User__c = '123';
                        cipToToBeUpdatedLst.add(updateCIPRecord);
                        System.debug('after insert cipToToBeUpdatedLst---: ' + cipToToBeUpdatedLst);
                    }
                }                               
            }
         
            if(cipToToBeUpdatedLst.size() > 0){
              System.debug('Before update operation');
              update cipToToBeUpdatedLst;
            }

        }catch(Exception e){
          
            System.debug('Exception e while insertion----: ' + e.getMessage());   
            for(BI_Plan_de_accion__c bhc_Error : cipInsertedLst){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                toAddresses = new String[]{bhc_Error.Owner.Email};
                mail.setSubject('Exception has occured during the escalation process');
                mail.setToAddresses(toAddresses);
                mail.setPlainTextBody('Exception has occured during the processing of CIP: ' + bhc_Error.Id + '. Please contact Administrator.');
                mail.setSaveAsActivity(false);
                mail.setBccSender(false);
                if(bhc_Error.Owner.Email != null){
                   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
                System.debug('Done');
            }
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Method is used to update CIP Plan de accion user details required for starting the escalation on updation of CIP record. 
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    13/09/2016                      Miguel Cabrera                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void escalationBeforeAfterUpdate(List<BI_Plan_de_accion__c> cipUpdatedLst, Boolean isBefore, Boolean isAfter, Boolean isUpdate, Map<ID, BI_Plan_de_accion__c> oldCIPMap){
        
        List<BI_Plan_de_accion__c> cipToToBeUpdatedLst = new List<BI_Plan_de_accion__c>();
        List<RecordType> rt = [SELECT r.Id FROM RecordType r WHERE r.DeveloperName  = 'BI_O4_IA' AND SobjectType = 'BI_Plan_de_accion__c'];
        List<Task> taskToBeAssgned =  new List<Task>();
        List<EmailTemplate> templateList = null;
        String[] toAddresses;
        
        try{
            for(BI_Plan_de_accion__c updateCIPRecord : [SELECT Id, Name, BI_O4_Complete__c, BI_O4_Is_Escalated__c, RecordTypeId, BI_O4_Other_Comments__c, BI_Estado__c, BI_Plan_original__c, BI_O4_Stop_Escalation__c,
                                        BI_O4_Original_CIP_User__c, BI_O4_Next_Escalated_User__c, BI_O4_Next_Escalated_User__r.ManagerId, BI_O4_CIP_User__c,
                                        BI_O4_Original_CIP_User__r.ManagerId, BI_O4_Next_Escalated_User__r.UserRole.Name, BI_O4_CIP_User__r.ManagerId, Owner.Email,
                                        BI_Plan_original__r.BI_O4_Original_CIP_User__c, BI_Plan_original__r.BI_O4_Original_CIP_User__r.ManagerId, BI_O4_Last_Escalation_Date_Time__c,
                                        BI_O4_Last_Active_Date_Time__c, BI_O4_Next_Escalated_User__r.Manager.UserRole.Name, BI_O4_CIP_User__r.Email, BI_O4_Document_Link_on_Box__c
                                        FROM BI_Plan_de_accion__c WHERE Id IN: cipUpdatedLst]){
           
                //Below block will be executed for setting the user details of CIP record while updating the CIP user in that existing newly created record 
                if((isAfter && isUpdate && oldCIPMap.get(updateCIPRecord.Id).BI_O4_CIP_User__c != updateCIPRecord.BI_O4_CIP_User__c && !updateCIPRecord.BI_O4_Is_Escalated__c && escalationFlag)){
                    System.debug('after update---');
                    updateCIPRecord.BI_O4_Next_Escalated_User__c = updateCIPRecord.BI_O4_CIP_User__r.ManagerId;
                    updateCIPRecord.BI_O4_Original_CIP_User__c = updateCIPRecord.BI_O4_CIP_User__c;
                    cipToToBeUpdatedLst.add(updateCIPRecord);
                }
                 //Below block will be executed for resetting the user details of parent CIP record once BI_O4_IA child record is updated
                if(rt.size() > 0){
                    if(updateCIPRecord.RecordTypeId == rt[0].Id){
                        BI_Plan_de_accion__c cipRecrd =  new BI_Plan_de_accion__c (Id = updateCIPRecord.BI_Plan_original__c, BI_O4_Last_Active_Date_Time__c = datetime.Now(), BI_O4_Last_Escalation_Date_Time__c = datetime.now(), BI_O4_Is_Escalated__c = false,
                        BI_O4_CIP_User__c = updateCIPRecord.BI_Plan_original__r.BI_O4_Original_CIP_User__c != null ? updateCIPRecord.BI_Plan_original__r.BI_O4_Original_CIP_User__c:updateCIPRecord.BI_O4_CIP_User__c, 
                        BI_O4_Next_Escalated_User__c = updateCIPRecord.BI_Plan_original__r.BI_O4_Original_CIP_User__r.ManagerId != null ? updateCIPRecord.BI_Plan_original__r.BI_O4_Original_CIP_User__r.ManagerId: updateCIPRecord.BI_O4_Next_Escalated_User__c);
                        cipToToBeUpdatedLst.add(cipRecrd);
                    }
                }

                if(isUpdate){
                    System.debug('Inside if trigger update ');
                    //Below block is for updating the LastActiveDateTime field and for reseting the user details of CIP record for resetting the escalation based on the LastActiveDateTime field
                    if(isAfter && updateCIPRecord.RecordTypeId != rt[0].Id && ((oldCIPMap.get(updateCIPRecord.Id).BI_O4_Complete__c != updateCIPRecord.BI_O4_Complete__c) || (oldCIPMap.get(updateCIPRecord.Id).BI_O4_Other_Comments__c != updateCIPRecord.BI_O4_Other_Comments__c) || (oldCIPMap.get(updateCIPRecord.Id).BI_Estado__c != updateCIPRecord.BI_Estado__c && (updateCIPRecord.BI_Estado__c == 'Deferred' || updateCIPRecord.BI_Estado__c == 'Completed')))){
                        
                        BI_Plan_de_accion__c cipRecrd =  new BI_Plan_de_accion__c (Id = updateCIPRecord.Id, BI_O4_Last_Active_Date_Time__c = datetime.Now(), BI_O4_Last_Escalation_Date_Time__c = datetime.now(), BI_O4_Is_Escalated__c = false,
                        BI_O4_CIP_User__c = updateCIPRecord.BI_O4_Original_CIP_User__c != null ? updateCIPRecord.BI_O4_Original_CIP_User__c:updateCIPRecord.BI_O4_CIP_User__c,
                        BI_O4_Next_Escalated_User__c = updateCIPRecord.BI_O4_Original_CIP_User__r.ManagerId != null ? updateCIPRecord.BI_O4_Original_CIP_User__r.ManagerId: updateCIPRecord.BI_O4_Next_Escalated_User__c);
                        cipToToBeUpdatedLst.add(cipRecrd);
                        system.debug('cipToToBeUpdatedLst----: ' + cipToToBeUpdatedLst);
                    }
                    System.debug('isAfter: ' + isAfter);
                    System.debug('News isEscalated: ' + updateCIPRecord.BI_O4_Is_Escalated__c);
                    System.debug('Olds isEscalated: ' + oldCIPMap.get(updateCIPRecord.Id).BI_O4_Is_Escalated__c);
                    if(isAfter && updateCIPRecord.BI_O4_Is_Escalated__c &&  oldCIPMap.get(updateCIPRecord.Id).BI_O4_Is_Escalated__c != updateCIPRecord.BI_O4_Is_Escalated__c){
                        String managerRoleName = updateCIPRecord.BI_O4_Next_Escalated_User__r.UserRole.Name;
                        //Below if block will be executed for stopping the escalation process
                        if(Label.BI_O4_EscalationSpecificRole == managerRoleName){
                            updateCIPRecord.BI_O4_Stop_Escalation__c = true;
                            updateCIPRecord.BI_O4_Is_Escalated__c = false;
                            
                            if(updateCIPRecord.BI_O4_Original_CIP_User__c != null){
                                updateCIPRecord.BI_O4_CIP_User__c = updateCIPRecord.BI_O4_Original_CIP_User__c;
                            }
                            escalationFlag = false;   
                        }else{
                            Task task = new Task();
                            task.Subject = 'CIP Update Task: ' + updateCIPRecord.id;
                            task.WhatId = updateCIPRecord.Id;
                            System.debug('updateCIPRecord.BI_O4_Next_Escalated_User__c---: ' + updateCIPRecord.BI_O4_Next_Escalated_User__c);
                            task.OwnerID = updateCIPRecord.BI_O4_Next_Escalated_User__c;
                            taskToBeAssgned.add(task);
                            updateCIPRecord.BI_O4_CIP_User__c = updateCIPRecord.BI_O4_Next_Escalated_User__c;
                            updateCIPRecord.BI_O4_Next_Escalated_User__c = updateCIPRecord.BI_O4_Next_Escalated_User__r.ManagerId;
                            updateCIPRecord.BI_O4_Is_Escalated__c = false;
                            updateCIPRecord.BI_O4_Last_Escalation_Date_Time__c = datetime.Now();
                            escalationFlag = false;
                        }
                        cipToToBeUpdatedLst.add(updateCIPRecord);
                    }     
                }
            }
 
            if(cipToToBeUpdatedLst.size() > 0){
                System.debug('Before update operation cipToToBeUpdatedLst---: ' + cipToToBeUpdatedLst);
                update cipToToBeUpdatedLst;
            }
            if(taskToBeAssgned.size() > 0){
                insert taskToBeAssgned;
            }
        }catch(Exception e){
            System.debug('Exception e while updating----: ' + e.getMessage());  
            for(BI_Plan_de_accion__c bhc_Error : cipUpdatedLst){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                toAddresses = new String[]{bhc_Error.Owner.Email};
                mail.setSubject('Exception has occured during the escalation process');
                mail.setToAddresses(toAddresses);
                mail.setPlainTextBody('Exception has occured during the processing of CIP: ' + bhc_Error.Id + '. Please contact Administrator.');
                mail.setSaveAsActivity(false);
                mail.setBccSender(false);
                if(bhc_Error.Owner.Email != null){
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
            }
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Method is used to update CIP Plan de accion user details required for starting the escalation on updation of CIP record. 
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    13/09/2016                      Miguel Cabrera                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void escalationBeforeInsert(List<BI_Plan_de_accion__c> cipInsertedLst){
        
        String[] toAddresses;
        try{
            for(BI_Plan_de_accion__c updateCIPRecord : cipInsertedLst){
                updateCIPRecord.BI_O4_CIP_User__c = updateCIPRecord.OwnerId;
                System.debug('updateCIPRecord.BI_O4_CIP_User__c---: ' + updateCIPRecord.BI_O4_CIP_User__c);
            }
        }catch(Exception e){
            for(BI_Plan_de_accion__c bhc_Error : cipInsertedLst){
              Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
              toAddresses = new String[]{bhc_Error.Owner.Email};
              mail.setSubject('Exception has occured during the escalation process');
              mail.setToAddresses(toAddresses);
              mail.setPlainTextBody('Exception has occured during the processing of CIP: ' + bhc_Error.Id + '. Please contact Administrator.');
              mail.setSaveAsActivity(false);
              mail.setBccSender(false);
              Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
              System.debug('Done');
          }                                 
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Miguel Cabrera
    Company:       New Energy Aborda
    Description:   Method is used to the Account team members see all accounts associated with that account CIP.    
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    14/09/2016                      Miguel Cabrera                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void asociarUsuariosAccountTeam (List<BI_Plan_de_accion__c> nuevos){
        Set<String> setAccount = new Set<String>();
        Set<String> setCIPid = new Set<String>();
        Map<String,List<String>> mapAccountlistCIP = new Map<String,List<String>>();
        Map<String,List<String>> mapCIPlistUser = new Map<String,List<String>>();
        List<BI_Plan_de_accion__Share>listCIPShareInsert = new List<BI_Plan_de_accion__Share>();
        for(BI_Plan_de_accion__c n : nuevos){
            setAccount.add(n.BI_O4_MNCs_Account__c);
            setCIPid.add(n.Id);
            List<String> aux = mapAccountlistCIP.get(n.BI_O4_MNCs_Account__c);
            if(aux == null){
                aux = new List<String>();
            }
            aux.add(n.Id);
            mapAccountlistCIP.put(n.BI_O4_MNCs_Account__c, aux);
        }
        if(!setAccount.isEmpty()){
            List<AccountTeamMember> listAccountTeams = [SELECT Id, UserId, AccountId FROM AccountTeamMember WHERE AccountId IN : setAccount];
            if(!listAccountTeams.isEmpty()){
                for(AccountTeamMember l : listAccountTeams){
                    List<string> aux = mapAccountlistCIP.get(l.AccountId);
                    if(aux != null){
                        for(String s: aux){
                            List<string> aux2 = mapCIPlistUser.get(s);
                            if(aux2 == null){
                                aux2 = new List<String>();
                            }
                            aux2.add(l.UserId);
                            mapCIPlistUser.put(s,aux2);
                        }
                    }
                }
            }
        }
        if(!mapCIPlistUser.isEmpty()){
            Set<String> setCPIUser = new Set<String>();
            List<BI_Plan_de_accion__Share> listCIPShare = [SELECT UserOrGroupId, ParentId, Id FROM BI_Plan_de_accion__Share WHERE ParentId IN : setCIPid LIMIT 40000];
            for(BI_Plan_de_accion__Share l : listCIPShare){
                string staux = string.valueOf(l.ParentId) + string.valueOf(l.UserOrGroupId);
                setCPIUser.add(staux);
            }
            for(BI_Plan_de_accion__c n : nuevos){
                List<String> aux = mapCIPlistUser.get(n.Id);
                if(!aux.isEmpty()){
                    for(String s : aux){
                        String staux2 = n.Id+s;
                        if(setCPIUser.contains(staux2) == false){ //Insertamos
                            BI_Plan_de_accion__Share cipShare = new BI_Plan_de_accion__Share();
                            cipShare.ParentId = n.Id;
                            cipShare.UserOrGroupId = s;
                            cipShare.AccessLevel = 'Read';
                            listCIPShareInsert.add(cipShare);
                        }   
                    }
                }
            }
        }
        if(!listCIPShareInsert.isEmpty()){
            insert listCIPShareInsert;
        }
    }
}