@isTest
private class CWP_AC_OrderQuoteSummaryCtrl_TEST {
    
    @testSetup
    static void testSetup(){
        List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, new List<String>{'Spain'});
        Opportunity opp = new Opportunity(Name = 'TestquoteHistoryOPp',
                                          CloseDate = Date.today(),
                                          StageName = Label.BI_F6Preoportunidad,
                                          AccountId = lst_acc[0].Id,
                                          BI_Ciclo_ventas__c = Label.BI_Completo,
                                          BI_Licitacion__c = 'No',
                                          BI_Country__c = 'Spain');
        Insert opp;
        
        NE__Order__c order = new NE__Order__c(NE__OrderStatus__c = 'Pending',
                                              NE__AccountId__c = lst_acc[0].Id,
                                              NE__OptyId__c = opp.Id,
                                              RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Quote' LIMIT 1].Id
                                             );
        Insert order;
        List<NE__OrderItem__c> lst_oi = BI_DataLoad.generateOrderitems(10, order.Id, lst_acc[0].Id);
        lst_oi[1].NE__Root_Order_Item__c = lst_oi[0].Id;
        lst_oi[1].NE__Parent_Order_Item__c = lst_oi[0].Id;
        update lst_oi;
    }
    
    @isTest
    static void getOi_Test(){
        List<Object> lstObj = CWP_AC_OrderQuoteSummary_Ctrl.getOi([SELECT Id FROM NE__Order__c].Id);
    }

}