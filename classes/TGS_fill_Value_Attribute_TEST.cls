@isTest
public class TGS_fill_Value_Attribute_TEST {
    

    
    static testMethod void value_attribute_test(){
       
        
        String profile = 'TGS System Administrator';
        
        Profile miProfile = [SELECT Id, Name
                               FROM Profile
                               Where Name = :profile
                               Limit 1];
        
        System.runAs(new User(Id = Userinfo.getUserId())) {
            TGS_User_Org__c uO = new TGS_User_Org__c();
            uO.TGS_Is_TGS__c = True;
            uO.SetupOwnerId = miProfile.Id;
            insert uO;
        }
            
        User userTest = TGS_Dummy_Test_Data.dummyUserTGS(profile);
        userTest.BI_Permisos__c = 'TGS';
        insert userTest;
        
        TGS_Dummy_Test_Data.dummyEndpointsTGS();
        
        System.runAs(userTest){
        
       
            NE__OrderItem__c testOrderItem = TGS_Dummy_Test_Data.dummyConfigurationOrder();
            NE__ProductFamily__c prodFam = [SELECT iD, NE__FamilyId__c FROM NE__ProductFamily__c WHERE  NE__ProdId__c =: testOrderItem.NE__ProdId__c];
                   
            NE__ProductFamilyProperty__c prodFamProp = [Select iD, TGS_Is_Key_attribute__c FROM NE__ProductFamilyProperty__c WHERE NE__FamilyId__c =: prodFam.NE__FamilyId__c];
            system.assertNotEquals(null, prodFamProp);
            prodFamProp.TGS_Is_Key_attribute__c = true;
            update prodFamProp;    
           

           Test.startTest(); 
            
           testOrderItem.TGS_Value_Attribute__c= 'test';
           update testOrderItem;

            
           
            
        List<NE__OrderItem__c> setConfItems = new List<NE__OrderItem__c>(); 
        setConfItems.add(testOrderItem);
        update setConfItems; 
        Set<Id> setConfItemsIds = new Set<Id>();
            for(NE__OrderItem__c ci: setConfItems){
                
                setConfItemsIds.add(ci.Id);
            }
        NE__OrderItem__c cis = [SELECT iD, TGS_Value_Attribute__c, (SELECT TGS_Key_Attribute__c, NE__Value__c FROM NE__Order_Item_Attributes__r Limit 1) FROM NE__OrderItem__c WHERE ID in :setConfItems];
        NE__Order_Item_Attribute__c attributes = cis.NE__Order_Item_Attributes__r;
   
        
        TGS_fill_Value_Attribute.value_attribute(setConfItemsIds);   
        Test.stopTest();
        
        }   
     }
}