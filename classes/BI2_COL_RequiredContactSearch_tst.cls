@isTest(seeAllData = false)
public class BI2_COL_RequiredContactSearch_tst {
    private final static String VALIDADOR_FISCAL = 'COL_NIT_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(9);
    private final static String LE_NAME = 'TestAccount_ContactoFinalSearch_TEST';

    @testSetup static void dataSetup() 
    {
        Account account = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert account;

        Account[] acc = [SELECT Id FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketTest = BIIN_UNICA_TestDataGenerator.createTicket(acc[0]);
        insert ticketTest;
    }

    static testMethod void test_BI2_COL_RequiredContactSearch_tst() 
    {
        BI_TestUtils.throw_exception = false;

        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case caso = [SELECT Id, BI_Id_del_caso_legado__c FROM Case WHERE AccountId =: account.Id];

        Test.startTest();  
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerContacto'));
        PageReference p = Page.BIIN_Detalle_Incidencia;
        p.getParameters().put('namefield', 'LongitudName');
        p.getParameters().put('surnamefield', 'LongitudSurname');
        p.getParameters().put('emailfield', 'LongitudEmail');
        p.getParameters().put('accountfield', account.Id);
        Test.setCurrentPageReference(p);
        
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
        BI2_COL_RequiredContactSearch_ctr ceController = new BI2_COL_RequiredContactSearch_ctr();
        ceController.getContact();
        ceController.HabilitarIr();
        ceController.getHttpRequest();
        ceController.getHttpResponse();
        //ceController.crearContancoRoD();
        Test.stopTest();
    }
    
    static testMethod void test_BI2_COL_RequiredContactSearch_tst_ELSE() 
    {
        BI_TestUtils.throw_exception = false;
        
        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case caso = [SELECT Id, BI_Id_del_caso_legado__c FROM Case WHERE AccountId =: account.Id];

        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerContacto'));
        PageReference p = Page.BIIN_Detalle_Incidencia;
        p.getParameters().put('namefield', '');
        p.getParameters().put('surnamefield', '');
        p.getParameters().put('emailfield', '');
        p.getParameters().put('accountfield', null);
        Test.setCurrentPageReference(p);
        
        ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(caso);
        BI2_COL_RequiredContactSearch_ctr ceController = new BI2_COL_RequiredContactSearch_ctr();
        ceController.getContact();
        ceController.HabilitarIr();
        ceController.getHttpRequest();
        ceController.getHttpResponse();
        ceController.validateEmail('testBien@test.com');
        ceController.validateEmail('testBientest.com');
        ceController.cancelarNuevoContacto();
        ceController.mostrarNuevoContacto();
        String scriptvar =ceController.scriptvar;
        Case casoCrearContact =ceController.casoCrearContact;
        ceController.crearContancoRoD();
        ceController.caso.BI2_Email_Contacto_Final__c='testBien@test.com';
        ceController.crearContancoRoD();
        Test.stopTest();
    }
}