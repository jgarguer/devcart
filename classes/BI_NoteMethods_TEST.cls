@isTest
private class BI_NoteMethods_TEST
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   Methods to test NEContract Trigger and BI_NoteMethods
    History:
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static{   
        BI_TestUtils.throw_exception = false;
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@testSetup
	static void loadData()
	{
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        BI_TestUtils.throw_exception = false;
		Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();      
        List<String>lst_region = new List<String>();
        lst_region.add('Perú');
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('PER');  
        List <Account> lst_acc = BI_DataLoad.loadAccounts(1, lst_region);

        List <NE__Contract__c> lst_con = new List<NE__Contract__c>();
        List <NE__Contract_Header__c> lst_head = new List<NE__Contract_Header__c>();
        List <NE__Product__c> lst_prod = new List<NE__Product__c>();
        List <NE__Contract_Line_Item__c> lst_cli = new List<NE__Contract_Line_Item__c>();

        Integer i = 2;

        
        for(Integer j=0; j<i; j++){
            NE__Contract_Header__c header = new NE__Contract_Header__c(NE__Actual_Qty__c = 3,
                                                                       NE__Delta_Qty__c = 2,
                                                                       NE__Name__c ='Header'+String.valueOf(j));                                                             
            lst_head.add(header);           
            NE__Product__c prod = new NE__Product__c(Name = 'Prod');       
            lst_prod.add(prod);
            
        }
        
        insert lst_head;

        NE__Contract_Account_Association__c newCAA = new NE__Contract_Account_Association__c(
            NE__Contract_Header__c = lst_head[0].id,
            NE__Account__c = lst_acc[0].id
            
        );
        insert newCAA;

        insert lst_prod;
        
        for(Integer k=0; k<i; k++){
            NE__Contract__c contract = new NE__Contract__c(NE__Contract_Header__c = lst_head[k].Id,
                                                           NE__Status__c = 'Active');
            lst_con.add(contract);
        }
        insert lst_con;
        for(Integer l=0; l<i; l++){
            
            NE__Contract_Line_Item__c line = new NE__Contract_Line_Item__c(NE__Contract__c = lst_con[l].Id,
                                                                           NE__Commercial_Product__c = lst_prod[l].Id,
                                                                           NE__Allow_Qty_Override__c = false);  
            lst_cli.add(line);
        }        
        insert lst_cli;

        Note newNote = new Note();
			newNote.parentId = lst_con[0].Id;
			newNote.title = 'Titulo';
			newNote.body = 'Texto';
        
        insert newNote;

        Id profileId = [SELECT ID FROM PROFILE WHERE Name = 'BI_Standard_PER' limit 1][0].Id;
        BI_Dataload.loadUsers(1, profileId, 'Administrador de Contrato');
        BI_Dataload.loadUsers(1, profileId, 'Super Usuario');
		BI_Dataload.loadUsers(1, profileId, 'Ejecutivo de Cliente');

	}


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   
    IN:          
    OUT:               
    History:
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void verificarPermisos_adminContrac()
	{
		User u = [SELECT ID FROM User WHERE BI_Permisos__c = 'Administrador de Contrato' and isActive = true and Profile.Name = 'BI_Standard_PER' limit 1];
		Note laNota = [SELECT ID, Title FROM Note limit 1];
		system.runAs(u){
			try{
				delete laNota;
				System.assert(false); //El usuario Administrador de contrato no puede eliminar.
			}catch(Exception e){
				System.debug('verificarPermisos_adminContrac ' + e);
			}
		}
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   Comprobamos que el superUser pueda realizar todas las tareas requeridas.
    IN:          
    OUT:               
    History:
    
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void verificarPermisos_superUser()
	{
		User u = [SELECT ID FROM User WHERE BI_Permisos__c = 'Super Usuario' AND isActive = true limit 1];
		Note laNota = [SELECT ID, Title FROM Note limit 1];
        laNota.OwnerId = u.Id;
        update laNota;
        system.runAs(u){
            BI_TestUtils.throw_exception = false;
			laNota.Title = 'all';
			update laNota;
			delete laNota;
		}
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   Probamos que no se pueda crear ni modificar ni borrar con un usuario
    IN:          
    OUT:               
    History:
    
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void verificarPermisos_all()
	{
		User u = [SELECT ID FROM User WHERE BI_Permisos__c = 'Ejecutivo de Cliente' AND isActive = true AND Profile.Name = 'BI_Standard_PER' limit 1];
		NE__Contract__c elContract = [Select id from NE__Contract__c limit 1];
		Note laNota = [SELECT ID, Title FROM Note limit 1];
        laNota.OwnerId = u.Id;
        update laNota;

		system.runAs(u){
            Note newNote1 = new Note();
			newNote1.parentId = elContract.Id;
			newNote1.title = 'Titulo1';
			newNote1.body = 'Texto1';
        	try{
				 insert newNote1;
				 System.assert(false);//
			}catch(Exception e){
				System.debug('verificarPermisos_all ' + e);
			}
			laNota.title = 'asdfasd';
			try{
				 update laNota;
				 System.assert(false);
			}catch(Exception e){
				System.debug('verificarPermisos_all ' + e);
			}
           
			try{
				delete laNota;
				System.assert(false);
			}catch(Exception e){
				System.debug('verificarPermisos_all ' + e);
			}
		}
	}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julian Gawron
    Company:       Accenture
    Description:   
    IN:          
    OUT:               
    History:  
    <Date>            <Author>          <Description>
    26/09/2017        Gawron, Julian    Initial version D-000577
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/    

    @isTest static void exceptions2(){
        BI_TestUtils.throw_exception = true;
        BI_NoteMethods.verificarPermisosContract(null, null);
    }
}