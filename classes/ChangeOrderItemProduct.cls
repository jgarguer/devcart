//ET 3/06/2014
public class ChangeOrderItemProduct {
	
	String catID;
	String prodID;
	String catItemID;
	Integer i;
	List<String> mCat;
	Map<String, String> mCatItem; 
	Map<String, Map<String,String>> mOrditCatIt;
	
	public String changeOrderItemProd(Map<String, String> mapOrdit, String description){

		List<String> mCat = new List<String>();
		mCatItem = new Map<String, String>();				
		mOrditCatIt = new Map<String, Map<String,String>>();
		List<NE__Catalog_Item__c> catItemNew;
		List<NE__Catalog_Item__c> catItemOld;
		List<NE__OrderItem__c> orderItems;
		List<NE__OrderItem__c> dummyItems;
		map<String,String> mapOfProductCI	=	new map<String,String>();
		String errorMsg = '';
		
		try{
			
			Set<String> ordersitId = new Set<String>();
			ordersitId = mapOrdit.keySet();
			system.debug('**ordersIdSIZE ' + mapOrdit.size() + ' ' + ordersitId); 
			
			
			List<NE__OrderItem__c> ord_it	 =  [SELECT id, Name, NE__CatalogItem__c, NE__ProdId__c, NE__Description__c, NE__CatalogItem__r.NE__Catalog_Id__c
										 		FROM NE__OrderItem__c WHERE id IN :ordersitId];
							
			system.debug('**ord_it '+ ord_it); 
			system.debug('**ORDIT SIZE ' + ord_it.size());
			orderItems = new List<NE__OrderItem__c>();
			dummyItems = new List<NE__OrderItem__c>();
					
			if(ord_it.size() > 0){
				
				system.debug('**ord_it SIZE ' + ord_it.size());
								
				for(NE__OrderItem__c oi:ord_it)
				{
					catID 		= oi.NE__CatalogItem__r.NE__Catalog_Id__c;	
					catItemID 	= oi.NE__CatalogItem__c;	
					mCat.add(catItemID); //set				
				}
							
				system.debug('**MCAT? ' + mCat); 		
				
				NE__Catalog_Item__c dummyItem;
				try
				{
					 dummyItem 	= 	 [SELECT NE__ProductId__c, NE__Catalog_Id__c, Is_Dummy__c FROM NE__Catalog_Item__c 
													      WHERE NE__Catalog_Id__c =: catID AND Is_Dummy__c = true 
													      LIMIT 1];	
				}
				catch(Exception e){}
				
				catItemOld 						=	 [SELECT id,  Name, NE__ProductId__c, NE__Catalog_Id__c, Is_Dummy__c FROM NE__Catalog_Item__c 
													  WHERE id IN :mCat];
													  
				catItemNew 						=	 [SELECT id,  Name, NE__ProductId__c, NE__Catalog_Id__c, Is_Dummy__c FROM NE__Catalog_Item__c 
													  WHERE NE__ProductId__c IN: mapOrdit.values()];	
													  
				for(NE__Catalog_Item__c ci:catItemNew)
					mapOfProductCI.put(ci.NE__ProductId__c,ci.id);												  
													  
					
				if(catItemNew.size() > 0)
				{								
					for(NE__OrderItem__c oi: ord_it){
						
						system.debug('**MAPORDIT_GET ' + mapordIt.get(oi.Id));						
						String ptranslationId	=	mapOrdit.get(oi.id);
						String newcitemId		=	mapOfProductCI.get(ptranslationId);
							
						if(newcitemId != null)
						{
							system.debug('**CI NORMAL ');
							oi.NE__CatalogItem__c 	= newcitemId;		
							oi.NE__ProdId__c 		= ptranslationId;
							oi.NE__Description__c 	= description;
							orderItems.add(oi);
						}
						else if(dummyItem != null)
						{
							system.debug('**CI DUMMY ');
							oi.NE__CatalogItem__c 	= dummyItem.id;	
							oi.NE__ProdId__c 		= dummyItem.NE__ProductId__c;
							oi.NE__Description__c 	= description;
							dummyItems.add(oi); //set
						}
						else
						{
							errorMsg = 'No dummy product defined.';
						}
						system.debug('**ERRMSG? ' + errorMsg);
					}
	
					system.debug('**OrderItemsUPDATE? ' + orderItems);
					system.debug('**DummyItemsUPDATE? ' + dummyItems);
					update orderItems;
					update dummyItems;	

				}					
			}
			return errorMsg;
				
		}catch (Exception e){
			system.debug('Line ' + e.getLineNumber() + ': ' + e.getMessage()); 
			return null;
		}	
			
	}
	
}