@isTest
private class BI_CSB_Controller_TEST
{  
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_RestRequestHelper.createAccount()
    History:
    
    <Date>              <Author>                <Description>
    02/09/2014          Fernando Arteaga        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
     static testMethod void suspendAccountTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List<String> lst_region = loadRegionData();

        BI_CSB_WS_Retry__c cs = new BI_CSB_WS_Retry__c();
        cs.BI_CSB_Reintentos__c = 5;
        cs.Name = 'TEST1';
        insert cs;

        List<Profile> prof = [select Id from Profile where Name = 'BI_Administrator' limit 1];

        User usertest = new User(alias = 'cctest',
                          email = 'cctest@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof[0].Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'cctest@testorg.com');
        
        insert usertest;
        
        System.runAs(usertest)
        {
            PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'BI_CORE_Gestion_de_Cobros'];
            PermissionSetAssignment perm = new PermissionSetAssignment(AssigneeId = usertest.Id, PermissionSetId = ps.Id);
            insert perm;
        
            List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);

            Test.startTest();

            lst_acc[0].BI_CSB_Flag_Suspension__c = true;
            update lst_acc[0];
            
            Test.stopTest();
            
            //List<BI_Log__c> logs = [SELECT BI_Asunto__c,BI_Bloque_funcional__c,BI_Descripcion__c,BI_Tipo_de_componente__c FROM BI_Log__c WHERE Id = :trans.Id AND BI_COL_Estado__c = 'Procesado'];
            //system.assertEquals(logs.size(), 1);
        }

    }
    
     static testMethod void createAccountTest() {

        Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
        for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'Account' ]){
            MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
        }
    
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        List<String> lst_region = loadRegionData();

        BI_CSB_WS_Retry__c cs = new BI_CSB_WS_Retry__c();
        cs.BI_CSB_Reintentos__c = 5;
        cs.Name = 'TEST1';
        insert cs;

        List<Profile> prof = [select Id from Profile where Name = 'BI_Administrator' limit 1];

        User usertest = new User(alias = 'cctest',
                          email = 'cctest@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof[0].Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'cctest@testorg.com');
        
        insert usertest;
        
        System.runAs(usertest)
        {
            Integer externalId = 1000;
                
            Account acc = new Account(Name = 'test1',
                                      BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                      BI_Activo__c = Label.BI_Si,
                                      BI_Id_del_cliente__c = String.valueOf(externalId),
                                      BI_Country__c = lst_region[0],
                                      BillingCountry = 'country',
                                      BillingState = 'state',
                                      BillingCity = 'city',
                                      BillingStreet = 'street',
                                      BillingPostalCode = '12345',
                                      ShippingCountry = 'country',
                                      ShippingState = 'state',
                                      ShippingCity = 'city',
                                      ShippingStreet = 'street',
                                      ShippingPostalCode = '12345',
                                      Fax = '12345',
                                      WebSite = 'www.test.com',
                                      Phone = '123456789',
                                      BI_Riesgo__c = '1 - test',
                                      BI_Segment__c = 'Empresas',
                                      BI_Subsegment_Regional__c = 'Mayoristas',
                                      BI_Subsector__c = 'Banca',
                                      BI_Sector__c = 'Industria',
                                        RecordTypeId = MAP_NAME_RT.get('TGS_Legal_Entity'),
                                        BI_Tipo_de_Identificador_Fiscal__c = 'RFC',
                                        BI_No_Identificador_fiscal__c = 'VECB380321XXX',
                                     BI_Territory__c = 'test'
                                      );
            
            insert acc;
            
            PageReference p = Page.BI_CSB_Integration;
            
            Test.startTest();
            Test.setCurrentPageReference(p);
            
            BI_TestUtils.throw_exception = false; // FAR 14/01/2016
            BI_CSB_Controller controller = new BI_CSB_Controller();
            controller.doAction();
            
            BI_TestUtils.throw_exception = false; // FAR 14/01/2016

            BI_CSB_Credenciales_Pais__c cred = new BI_CSB_Credenciales_Pais__c();
            cred.Name = lst_region[0];
            //cred.BI_CSB_Credencial__c = 'BI_CSB_API_UNICA_TEST';
            insert cred;
            
            controller = new BI_CSB_Controller();
            controller.doAction();
        
            cred.BI_CSB_Credencial__c = 'TEST';
            upsert cred;
            
            BI_TestUtils.throw_exception = false; // FAR 14/01/2016
            
            controller = new BI_CSB_Controller();
            controller.doAction();
            
            cred.Name = 'test';
            cred.BI_CSB_Credencial__c = 'BI_CSB_API_UNICA_TEST';
            upsert cred;
            
            BI_TestUtils.throw_exception = false; // FAR 14/01/2016

            p.getParameters().put('id', acc.Id);
            p.getParameters().put('fromAccount', '1');
            controller = new BI_CSB_Controller();
            controller.doAction();
            
            cred.Name = lst_region[0];
            upsert cred;
            
            BI_TestUtils.throw_exception = false; // FAR 14/01/2016

            controller = new BI_CSB_Controller();
            controller.doAction();
            
            BI_TestUtils.throw_exception = false; // FAR 14/01/2016
            
            controller = new BI_CSB_Controller();
            controller.doAction();
                            
            Test.stopTest();
            
            //List<BI_Log__c> logs = [SELECT BI_Asunto__c,BI_Bloque_funcional__c,BI_Descripcion__c,BI_Tipo_de_componente__c FROM BI_Log__c WHERE Id = :trans.Id AND BI_COL_Estado__c = 'Procesado'];
            //system.assertEquals(logs.size(), 1);
        }
    }

    static testMethod void createOrderTest() {
        List<Profile> prof = [select Id from Profile where Name = 'Administrador del sistema' OR Name = 'System Administrator' limit 1];

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User usertest = new User(alias = 'cctest',
                          email = 'cctest@testorg.com',
                          emailencodingkey = 'UTF-8',
                          lastname = 'Test',
                          languagelocalekey = 'en_US',
                          localesidkey = 'en_US',
                          ProfileId = prof[0].Id,
                          BI_Permisos__c = Label.BI_Administrador,
                          timezonesidkey = Label.BI_TimeZoneLA,
                          username = 'cctest@testorg.com');
                              
        System.runAs(me)
        {
            insert usertest;
        }
        
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true; 
        insert userTGS;

        Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Peru');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('PER');                                                
        
        System.runAs(usertest)
        {
            List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
            
            System.debug('account: ' + lst_acc[0].Id);
            System.debug('ownerId: ' + [SELECT OwnerId FROM Account WHERE Id = :lst_acc[0].Id].OwnerId);
            //List <Opportunity> lst_opp = BI_DataLoadRest.loadOpportunitiesWithPais(1, lst_acc[0].Id, lst_region[0], null);
            
            //List <NE__Order__c> lst_orders = BI_DataLoadRest.loadOrders(1, lst_acc[0], lst_opp[0]);
            
            //List <NE__OrderItem__c> lst_oi = BI_DataLoadRest.loadOrderItem(lst_acc[0], lst_orders);

            List<RecordType> lst_rt = [select Id from RecordType where SobjectType = 'NE__Order__c' AND DeveloperName = 'Order' limit 1];

            List <NE__Order__c> lst_orders = new List <NE__Order__c>();
            
            NE__Order__c ne_order = new NE__Order__c(RecordTypeId = lst_rt[0].Id,
                                                     BI_Id_legado__c = '1000',
                                                     //NE__OptyId__c = lst_opp[0].Id,
                                                     NE__OrderStatus__c = 'Pending',
                                                     NE__AccountId__c = lst_acc[0].Id,
                                                     NE__Recurring_Charge_Total__c = 100,
                                                     NE__One_Time_Fee_Total__c = 200);
                                                       
            lst_orders.add(ne_order);
        
            insert lst_orders;
            
            NE__Order_Header__c header = new NE__Order_Header__c(NE__OrderId__c = lst_orders[0].Id);
            insert header;
            
            NE__Catalog__c cat = new NE__Catalog__c();
            insert cat;
                
            NE__Product__c prod1 = new NE__Product__c();
            prod1.Name = 'Prod1';
            prod1.NE__Recurring_Cost__c = 100;
            prod1.NE__One_Time_Cost__c = 25;
            
            NE__Product__c prod2 = new NE__Product__c();
            prod2.Name = 'Prod2';
            prod2.NE__Recurring_Cost__c = 100;
            prod2.NE__One_Time_Cost__c = 25;
            
            NE__Product__c prod3 = new NE__Product__c();
            prod3.Name = 'Prod3';
            prod3.NE__Recurring_Cost__c = 100;
            prod3.NE__One_Time_Cost__c = 25;
            
            insert new List<NE__Product__c>{prod1, prod2, prod3};
            
            NE__Catalog_Item__c catItem1 = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod1.Id, BI_CSB_Code__c = 'OFFICE_365');
            NE__Catalog_Item__c catItem2 = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod2.Id, BI_CSB_Code__c = 'OFFICE_365_2');
            NE__Catalog_Item__c catItem3 = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = prod3.Id);
    
            insert new List<NE__Catalog_Item__c>{catItem1, catItem2, catItem3};
            
            List<NE__OrderItem__c> lst_oi = new List<NE__OrderItem__c>();
    
            NE__OrderItem__c oit = new NE__OrderItem__c(NE__OrderId__c = lst_orders[0].Id,
                                                        NE__Qty__c = 2,
                                                        NE__Asset_Item_Account__c = lst_acc[0].Id,
                                                        NE__Account__c = lst_acc[0].Id,
                                                        NE__Status__c = 'Active',
                                                        NE__ProdId__c = prod1.Id,
                                                        NE__CatalogItem__c = catItem1.Id,
                                                        NE__OneTimeFeeOv__c = 100,
                                                        NE__RecurringChargeOv__c = 50);
                                                        
            NE__OrderItem__c oit2 = new NE__OrderItem__c(NE__OrderId__c = lst_orders[0].Id,
                                                        NE__Qty__c = 2,
                                                        NE__Asset_Item_Account__c = lst_acc[0].Id,
                                                        NE__Account__c = lst_acc[0].Id,
                                                        NE__Status__c = 'Active',
                                                        NE__ProdId__c = prod2.Id,
                                                        NE__CatalogItem__c = catItem2.Id,
                                                        NE__OneTimeFeeOv__c = 100,
                                                        NE__RecurringChargeOv__c = 50);
    
            NE__OrderItem__c oit3 = new NE__OrderItem__c(NE__OrderId__c = lst_orders[0].Id,
                                                        NE__Qty__c = 2,
                                                        NE__Asset_Item_Account__c = lst_acc[0].Id,
                                                        NE__Account__c = lst_acc[0].Id,
                                                        NE__Status__c = 'Active',
                                                        NE__ProdId__c = prod3.Id,
                                                        NE__CatalogItem__c = catItem3.Id,
                                                        NE__OneTimeFeeOv__c = 100,
                                                        NE__RecurringChargeOv__c = 50);
                                                        
            insert new List<NE__OrderItem__c>{oit, oit2, oit3};
    
            BI_CSB_WS_Retry__c cs = new BI_CSB_WS_Retry__c();
            cs.BI_CSB_Reintentos__c = 5;
            cs.Name = 'TEST1';
            insert cs;

            BI_CSB_Credenciales_Pais__c cred = new BI_CSB_Credenciales_Pais__c();
            cred.Name = lst_region[0];
            cred.BI_CSB_Credencial__c = 'BI_CSB_API_UNICA_TEST';
            insert cred;
            
            
            PageReference p = Page.BI_CSB_Integration;
            
            p.getParameters().put('id', lst_acc[0].Id);
            Test.setCurrentPageReference(p);
            
            Test.startTest();
            BI_TestUtils.throw_exception = false; // FAR 14/01/2016
            
            p.getParameters().put('orderId', null);
            
            BI_CSB_Controller  controller = new BI_CSB_Controller();
            controller.doAction();
            
            BI_TestUtils.throw_exception = false; // FAR 14/01/2016
            
            p.getParameters().put('orderId', lst_orders[0].Id);
            controller = new BI_CSB_Controller();
            controller.doAction();

            BI_TestUtils.throw_exception = true; // FAR 14/01/2016
            
            controller = new BI_CSB_Controller();
            controller.doAction();
            controller.sendAccountNotFinished(new BI_RestWrapper.GenericResponse());
            controller.createOrderNotFinished(new BI_RestWrapper.GenericResponse());
            
            PageReference p2 = controller.exit();
                            
            Test.stopTest();
            
            //List<BI_Log__c> logs = [SELECT BI_Asunto__c,BI_Bloque_funcional__c,BI_Descripcion__c,BI_Tipo_de_componente__c FROM BI_Log__c WHERE Id = :trans.Id AND BI_COL_Estado__c = 'Procesado'];
            //system.assertEquals(logs.size(), 1);
        }
    }

    private static List<String> loadRegionData(){
        Map<String, BI_Code_ISO__c> map_region = BI_DataLoad.loadRegions();
        List<String> lst_region = new List<String>();

       for(BI_Code_ISO__c biCode: map_region.values()){
            lst_region.add(biCode.Name);
       }
       return lst_region;
    }
    
}