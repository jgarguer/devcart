@isTest(seeAllData = false)
public class BIIN_CreacionIncidencia_TEST {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Morales Rodríguez
    Company:       Deloitte
    Description:   Test method to manage the code coverage for BIIN_CreacionIncidencia_Ctrl
            
    <Date>              <Author>                            <Change Description>
    11/2015             Ignacio Morales Rodríguez           Initial Version
    22/06/2016          José luis González Beltrán          Adapt test to UNICA interfaz modifications
    08/02/2017          Pedro Párraga                       Increase coverage
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private final static String VALIDADOR_FISCAL = 'PER_RUC_' + BIIN_UNICA_TestDataGenerator.getRandomNumber(11);
    private final static String LE_NAME = 'TestAccount_CreacionIncidencia_TEST';

    @testSetup static void dataSetup()
    {
        BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();

        Account accTest = BIIN_UNICA_TestDataGenerator.createLegalEntity(LE_NAME, VALIDADOR_FISCAL);
        insert accTest;

        Account[] acc = [SELECT Id FROM Account WHERE Name = :LE_NAME LIMIT 1];
        Case cTest = BIIN_UNICA_TestDataGenerator.createTicket(acc[0]);
        cTest.BI_COL_Fecha_Radicacion__c = DateTime.newInstance(1997, 1, 31, 7, 8, 16);
        insert cTest;
    }

    @isTest static void test_BIIN_CreacionIncidencia_Ctrl_1()
    {
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());

        System.runAs(usr)
        {
            BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
            List<Case> caseList = new List<Case>();
            List<Account> accounts = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
            Case ticketIncidenciaTecnica = [SELECT Id, BI_Id_del_caso_legado__c, AccountId, BI_Line_of_Business__c, BI_Product_Service__c, BI2_PER_Descripcion_de_producto__c, BIIN_Categorization_Tier_1__c, BIIN_Categorization_Tier_2__c, BIIN_Categorization_Tier_3__c FROM Case WHERE AccountId =: accounts[0].Id];
            caseList.add(ticketIncidenciaTecnica);
            
            List<Account> acc = accounts;
            List<Account> accList = new List<Account>();
    
            for(Account item : acc)
            {
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            List<Contact> con = new List<Contact>();
            con = BI_DataLoad.loadContacts(1, accList);
            
            Test.startTest();
            
            PageReference p = Page.BIIN_CreacionIncidencia;
            p.getParameters().put('def_contact_id', con[0].Id);
            p.getParameters().put('def_account_id', ticketIncidenciaTecnica.AccountId);
            p.getParameters().put('NombreContactoFinal', 'Nombre');
            p.getParameters().put('ApellidoContactoFinal', 'Apellildo');
            p.getParameters().put('EmailContactoFinal', 'Email'); 
            p.getParameters().put('UbicacionController', 'Ubicacion');
            p.getParameters().put('CIController', 'CI'); 
            p.getParameters().put('Categoria1', 'SERVICIO ACONDICIONAMIENTO DATACENTER'); 
            p.getParameters().put('Categoria2', 'MANTENIMIENTO');
            p.getParameters().put('Categoria3', 'PRUEBAS CLIENTE'); 
            Test.setCurrentPageReference(p);
            
            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicket'));
            ApexPages.StandardSetController setcon = new ApexPages.StandardSetController(caseList);
            BIIN_CreacionIncidencia_Ctrl controller = new BIIN_CreacionIncidencia_Ctrl(setcon);
            PageReference insertarSolicitudTecnica = controller.insertarSolicitudTecnica();
    
            Test.stopTest();
        }        
    }
    
    @isTest static void test_BIIN_CreacionIncidencia_Ctrl_methods()
    {
        User usr = BI_DataLoad.loadPortalUserWithRole(BI_DataLoad.searchUserRole(),BI_DataLoad.searchAdminProfile());

        System.runAs(usr)
        {
            BIIN_UNICA_TestDataGenerator.loadTablaCorrespondencia();
            List<Case> caseList = new List<Case>();
            List<Account> accounts = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
            Case ticketIncidenciaTecnica = [SELECT Id, BI_Id_del_caso_legado__c, AccountId, BI_COL_Fecha_Radicacion__c, BI_Line_of_Business__c, BI_Product_Service__c, BI2_PER_Descripcion_de_producto__c, BIIN_Categorization_Tier_1__c, BIIN_Categorization_Tier_2__c, BIIN_Categorization_Tier_3__c FROM Case WHERE AccountId =: accounts[0].Id];
            caseList.add(ticketIncidenciaTecnica);
            
            List<Account> acc = accounts;
            List<Account> accList = new List<Account>();
    
            for(Account item : acc)
            {
                item.OwnerId = usr.Id;
                accList.add(item);
            }
            update accList;
            List<Contact> con = new List<Contact>();
            con = BI_DataLoad.loadContacts(1, accList);
            
            Test.startTest();
            
            PageReference p = Page.BIIN_CreacionIncidencia;
            p.getParameters().put('def_contact_id', con[0].Id);
            p.getParameters().put('def_account_id', ticketIncidenciaTecnica.AccountId);
            p.getParameters().put('NombreContactoFinal', 'Nombre');
            p.getParameters().put('ApellidoContactoFinal', 'Apellildo');
            p.getParameters().put('EmailContactoFinal', 'Email'); 
            p.getParameters().put('UbicacionController', 'Ubicacion');
            p.getParameters().put('CIController', 'CI'); 
            p.getParameters().put('Categoria1', 'SERVICIO ACONDICIONAMIENTO DATACENTER'); 
            p.getParameters().put('Categoria2', 'MANTENIMIENTO');
            p.getParameters().put('Categoria3', 'PRUEBAS CLIENTE'); 
            Test.setCurrentPageReference(p);
            
            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicket'));
            ApexPages.StandardSetController setcon = new ApexPages.StandardSetController(caseList);
            BIIN_CreacionIncidencia_Ctrl controller = new BIIN_CreacionIncidencia_Ctrl(setcon);
            controller.caso = ticketIncidenciaTecnica;
            PageReference onClickGuardar = controller.onClickGuardar();
            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicketException500'));
            onClickGuardar = controller.onClickGuardar();

            BIIN_Tabla_Cat_Operacionales__c tco = new BIIN_Tabla_Cat_Operacionales__c(Name = 'tco', Cat_Operacional1__c = 'SERVICIO ACONDICIONAMIENTO DATACENTER',
                                                                                        Cat_Operacional2__c = 'MANTENIMIENTO',
                                                                                        Cat_Operacional3__c = 'PRUEBAS CLIENTE');

            insert tco;

            controller.nombre1 = 'testad1';
            controller.adjunto1 = Blob.valueOf('test1');
            controller.nombre2 = 'testad2';
            controller.adjunto2 = Blob.valueOf('test2');
            controller.nombre3 = 'testad3';
            controller.adjunto3 = Blob.valueOf('test3');
            Boolean GuardarTicketAdjuntoSF = controller.GuardarTicketAdjuntoSF();
            controller.MostrarPopUpEstado();  
            PageReference OKdetalle = controller.OKdetalle();
            PageReference Cancel = controller.Cancel();
            controller.closePopup3();
            controller.showPopup3();
            controller.GuardarDatos();
            controller.GuardarUbicacion();
            controller.GuardarCI(); 
            controller.ValidarCIUbicacion();
            Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('ObtenerContacto'));
            controller.validarUserBackground();
            controller.ClosePopUpCategOperac();
            controller.ReiniciarCampos();
            controller.CambioCatOperacional();
    
            Test.stopTest();
        }        
    }
    
    @isTest static void test_BIIN_CreacionIncidencia_Ctrl_onClickGuardarFuturo()
    {
        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketIncidenciaTecnica = [SELECT Id, BI_Id_del_caso_legado__c, AccountId, BI_Line_of_Business__c, BI_Product_Service__c, BI2_PER_Descripcion_de_producto__c, BIIN_Categorization_Tier_1__c, BIIN_Categorization_Tier_2__c, BIIN_Categorization_Tier_3__c FROM Case WHERE AccountId =: account.Id];

        List<BIIN_Creacion_Ticket_WS.Adjunto> attachments = new List<BIIN_Creacion_Ticket_WS.Adjunto>();
        attachments.add(new BIIN_Creacion_Ticket_WS.Adjunto('name1', Blob.valueOf('test1'), '1'));
        attachments.add(new BIIN_Creacion_Ticket_WS.Adjunto('name2', Blob.valueOf('test2'), '2'));

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicket'));
        BIIN_CreacionIncidencia_Ctrl.onClickGuardarFuturo(JSON.serialize(ticketIncidenciaTecnica), JSON.serialize(attachments));

        Test.stopTest();
    }
    
    @isTest static void test_BIIN_CreacionIncidencia_Ctrl_onClickGuardarFuturo_fail()
    {
        Account account = [SELECT Id, BI_Validador_Fiscal__c FROM Account WHERE Name =: LE_NAME LIMIT 1];
        Case ticketIncidenciaTecnica = [SELECT Id, BI_Id_del_caso_legado__c, AccountId, BI_Line_of_Business__c, BI_Product_Service__c, BI2_PER_Descripcion_de_producto__c, BIIN_Categorization_Tier_1__c, BIIN_Categorization_Tier_2__c, BIIN_Categorization_Tier_3__c FROM Case WHERE AccountId =: account.Id];

        List<BIIN_Creacion_Ticket_WS.Adjunto> attachments = new List<BIIN_Creacion_Ticket_WS.Adjunto>();
        attachments.add(new BIIN_Creacion_Ticket_WS.Adjunto('name1', Blob.valueOf('test1'), '1'));
        attachments.add(new BIIN_Creacion_Ticket_WS.Adjunto('name2', Blob.valueOf('test2'), '2'));

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new BIIN_MockHttpResponseGenerator_UNICA('CreacionTicketException500'));
        BIIN_CreacionIncidencia_Ctrl.onClickGuardarFuturo(JSON.serialize(ticketIncidenciaTecnica), JSON.serialize(attachments));

        Test.stopTest();

    }
}