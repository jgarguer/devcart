/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        --
     Company:       --
     Description:   Controller to get user permissions from BI_Permisos_PP__c, to control visibility on Portal platino
                   
     Test Class:    TGS_Carousel_Ctrl_Test
     History:
     
     <Date>            <Author>             <Description>
      --                --       			First version
	05-03-2018			Antonio Pardo		Added pca_portalsdn permission
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class CWP_ProfileHelper {
    global static final set<String> APODERADO_PERMISSION_SET = new set<String>();
    global static final set<String> TECNICO_PERMISSION_SET   = new set<String>();
    global static final set<String> COMERCIAL_PERMISSION_SET = new set<String>();
  global static final set<String> DYNAMIC_PERMISSION_SET = new set<String>();
    global static final set<String> ADMINISTRATIVO_PERMISSION_SET = new set<String>();
    
    global static final set<String> MENU_PERMISSION_SET = new set<String>();
    
    global static final map<String, set<String>> MAP_PERMISION_SET  = new map<String,set<String>>();
    
    static {
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

 /*---------------------------------MENU---------------------------------*/
          MENU_PERMISSION_SET.add('pca_home');
          
          MENU_PERMISSION_SET.add('pca_equipotelefonica');
          //Escalamiento, Equipo Cliente son apartados de la página.
          MENU_PERMISSION_SET.add('pca_customcalendar');
          MENU_PERMISSION_SET.add('pca_chattercustom'); 
          MENU_PERMISSION_SET.add('pca_relationmanagement'); 
          
          //Service Tracking
          MENU_PERMISSION_SET.add('pca_servicetracking');
          MENU_PERMISSION_SET.add('pca_report');
          MENU_PERMISSION_SET.add('pca_monitoreo');
          MENU_PERMISSION_SET.add('pca_reclamospedidos');
          MENU_PERMISSION_SET.add('pca_portales');

          
          //My Service
          MENU_PERMISSION_SET.add('pca_myservices');
          MENU_PERMISSION_SET.add('pca_inventory');
          MENU_PERMISSION_SET.add('pca_documentmanagement');
          MENU_PERMISSION_SET.add('pca_facturacioncobranza'); //faltan subpages
          MENU_PERMISSION_SET.add('pca_catalogo');
          MENU_PERMISSION_SET.add('pca_pestanaCobranzas');
          //Reclamos y pedidos también se usan en MarketPlace
          
          //Ideas Corner
          MENU_PERMISSION_SET.add('pca_ideascorner');

          //Quotation tool
          MENU_PERMISSION_SET.add('pca_quotationtool');

            
          //Portal SDN
          MENU_PERMISSION_SET.add('pca_portalsdn');
        
        
        /*---------------------------------Permiso dinámico---------------------------------*/
    
    List<BI_Contact_Customer_Portal__c> contpp = [Select BI_Perfil__c, BI_Cliente__c, BI_Permisos_PP__c, BI_Contacto__c, BI_User__c, BI_Activo__c from BI_Contact_Customer_Portal__c where BI_User__c = :UserInfo.getUserId() and BI_Cliente__c = :BI_AccountHelper.getCurrentAccountId() limit 1];    
    Set<Id> CCPPermId = new Set<Id>();
    for(BI_Contact_Customer_Portal__c contPortal :contpp){
        System.debug(contPortal);
        CCPPermId.add(contPortal.BI_Permisos_PP__c);
    }
    List<BI_Permisos_PP__c> cpp = [Select Id, BI_Agenda_Compartida__c,BI_Pestana_Cobranzas__c,BI_Comunidad_de_Expertos__c,BI_Country__c,BI_Default__c,BI_Documentacion__c,BI_Equipo_de_Telefonica__c,BI_Escalamiento_en_Telefonica__c,BI_Facturacion_y_cobranzas__c,BI_Ideas_Corner__c,BI_MarketPlace__c,BI_Monitoreo__c,BI_Portales__c,BI_Reclamos_y_pedidos__c,BI_Reportes__c,BI_Segment__c,BI_Servicios_Instalados__c,CWP_AC_Quotation_tool__c ,TGS_Portal_SDN__c,Name FROM BI_Permisos_PP__c where Id IN :CCPPermId];  
    System.debug(cpp[0]);
    if(cpp.size()!=0){
      DYNAMIC_PERMISSION_SET.add('pca_home');
      DYNAMIC_PERMISSION_SET.add('pca_helpgroup');
      if(cpp[0].BI_Equipo_de_Telefonica__c == true || cpp[0].BI_Agenda_Compartida__c == true || cpp[0].BI_Comunidad_de_Expertos__c){
        DYNAMIC_PERMISSION_SET.add('pca_relationmanagement'); 
        if(cpp[0].BI_Equipo_de_Telefonica__c == true){
        DYNAMIC_PERMISSION_SET.add('pca_equipotelefonica');     
            //Escalamiento, Equipo Cliente son apartados de la página.
            DYNAMIC_PERMISSION_SET.add('subsection_equipotelefonica');
            DYNAMIC_PERMISSION_SET.add('subsection_escalamiento');
            DYNAMIC_PERMISSION_SET.add('subsection_equipocliente');
            //DYNAMIC_PERMISSION_SET.add('pca_showuserprofile');
            DYNAMIC_PERMISSION_SET.add('pca_editcontact_popup');
            DYNAMIC_PERMISSION_SET.add('pca_equipotelefonica_popup');
            DYNAMIC_PERMISSION_SET.add('pca_sendinfo');
        }
        if(cpp[0].BI_Agenda_Compartida__c == true){
            DYNAMIC_PERMISSION_SET.add('pca_customcalendar');
            DYNAMIC_PERMISSION_SET.add('pca_customeventdetail');
            DYNAMIC_PERMISSION_SET.add('pca_eventredirect');
        }
        if(cpp[0].BI_Comunidad_de_Expertos__c == true){
            DYNAMIC_PERMISSION_SET.add('pca_eventredirect');
            DYNAMIC_PERMISSION_SET.add('pca_chattercustom'); 
        }
      }
      
      //Service Tracking
      if(cpp[0].BI_Reportes__c == true || cpp[0].BI_Monitoreo__c == true || cpp[0].BI_Reclamos_y_pedidos__c == true || cpp[0].BI_Portales__c == true){
        DYNAMIC_PERMISSION_SET.add('pca_servicetracking');
        if(cpp[0].BI_Reportes__c == true ){
            DYNAMIC_PERMISSION_SET.add('pca_report');
            DYNAMIC_PERMISSION_SET.add('pca_reportdetail');
        }
        if(cpp[0].BI_Monitoreo__c == true){
        DYNAMIC_PERMISSION_SET.add('pca_monitoreo');
        }
        if(cpp[0].BI_Reclamos_y_pedidos__c == true){
        DYNAMIC_PERMISSION_SET.add('pca_reclamospedidos'); 
        DYNAMIC_PERMISSION_SET.add('bi_invoicestreeview_portal');
          DYNAMIC_PERMISSION_SET.add('pca_reclamo_popupnew');
          DYNAMIC_PERMISSION_SET.add('bi2_col_pca_creacionincidencia_pag');
          DYNAMIC_PERMISSION_SET.add('pca_pedido_popupdetail'); 
          DYNAMIC_PERMISSION_SET.add('pca_reclamo_newattachment');
          DYNAMIC_PERMISSION_SET.add('pca_reclamo_popupdetail');
          DYNAMIC_PERMISSION_SET.add('pca_casecomment');
          DYNAMIC_PERMISSION_SET.add('pca_cobranza_popupdetail');
          DYNAMIC_PERMISSION_SET.add('pca_newcase');
        }
        if(cpp[0].BI_Portales__c == true){
          DYNAMIC_PERMISSION_SET.add('pca_portales');
        }
      }
      
      
      //My Service
      system.debug('Servicios instalados: '+cpp[0].BI_Servicios_Instalados__c);
      system.debug('Reportes: '+cpp[0].BI_Reportes__c );
      system.debug('Reclamos pedidos: '+cpp[0].BI_Reclamos_y_pedidos__c );
      system.debug('Portales: '+cpp[0].BI_Portales__c);
      if(cpp[0].BI_Servicios_Instalados__c == true || cpp[0].BI_Documentacion__c == true || cpp[0].BI_Facturacion_y_cobranzas__c == true || cpp[0].BI_MarketPlace__c == true){
      DYNAMIC_PERMISSION_SET.add('pca_myservices');
      if(cpp[0].BI_Servicios_Instalados__c == true){
        DYNAMIC_PERMISSION_SET.add('pca_inventory');
      }
      if(cpp[0].BI_Documentacion__c == true){
        DYNAMIC_PERMISSION_SET.add('pca_documentmanagement');
            DYNAMIC_PERMISSION_SET.add('pca_eventredirect');
      }
      if(cpp[0].BI_Facturacion_y_cobranzas__c == true){
        DYNAMIC_PERMISSION_SET.add('pca_facturacioncobranza'); //faltan subpages
        DYNAMIC_PERMISSION_SET.add('pca_facturacion_popupdetail');
        if(cpp[0].BI_Pestana_Cobranzas__c){
            DYNAMIC_PERMISSION_SET.add('pca_pestanaCobranzas');
        }
      }
      if(cpp[0].BI_MarketPlace__c == true){
        DYNAMIC_PERMISSION_SET.add('pca_marketplace');
          DYNAMIC_PERMISSION_SET.add('pca_catalogo');
          DYNAMIC_PERMISSION_SET.add('pca_catalogo_product');
          DYNAMIC_PERMISSION_SET.add('pca_autogestionoferta');
          DYNAMIC_PERMISSION_SET.add('pca_autogestionoferta_popupdetail');
          //Reclamos y pedidos también se usan en MarketPlace
      }
      }
        //Ideas Corner
        if(cpp[0].BI_Ideas_Corner__c == true){
            DYNAMIC_PERMISSION_SET.add('pca_ideascorner');
            DYNAMIC_PERMISSION_SET.add('pca_ideasfaq');
            DYNAMIC_PERMISSION_SET.add('pca_ideascornerdetails');
            DYNAMIC_PERMISSION_SET.add('pca_ideascornernewpopup');
            DYNAMIC_PERMISSION_SET.add('pca_ideascornerpopup');
            DYNAMIC_PERMISSION_SET.add('pca_jwplayer');
        }
        //Quotation tool
        if(cpp[0].CWP_AC_Quotation_tool__c == true){
            System.debug('ENTRO quotation');
            DYNAMIC_PERMISSION_SET.add('pca_quotationtool');
        }
        //Portal SDN
        if(cpp[0].TGS_Portal_SDN__c == true){
            System.debug('ENTRO portalSDN');
            DYNAMIC_PERMISSION_SET.add('pca_portalsdn');
        }
        
        DYNAMIC_PERMISSION_SET.add('pca_showprofile');
        DYNAMIC_PERMISSION_SET.add('pca_showuserprofile');
        DYNAMIC_PERMISSION_SET.add('pca_eventredirect');
        DYNAMIC_PERMISSION_SET.add('pca_template');
    }
        
/*---------------------------------APODERADO---------------------------------*/
          APODERADO_PERMISSION_SET.add('pca_home');
          APODERADO_PERMISSION_SET.add('pca_relationmanagement');
          //APODERADO_PERMISSION_SET.add('pca_equipotelefonica');
                //Escalamiento, Equipo Cliente son apartados de la página.
                //APODERADO_PERMISSION_SET.add('subsection_equipotelefonica');
                //APODERADO_PERMISSION_SET.add('subsection_escalamiento');
                //APODERADO_PERMISSION_SET.add('subsection_equipocliente');
                //APODERADO_PERMISSION_SET.add('pca_showuserprofile');
          //APODERADO_PERMISSION_SET.add('pca_customcalendar');
          APODERADO_PERMISSION_SET.add('pca_chattercustom'); 
          
          //Service Tracking
          //APODERADO_PERMISSION_SET.add('pca_servicetracking');
          //APODERADO_PERMISSION_SET.add('pca_report');
          //APODERADO_PERMISSION_SET.add('pca_monitoreo');
          //APODERADO_PERMISSION_SET.add('pca_reclamospedidos');
            //APODERADO_PERMISSION_SET.add('pca_reclamo_popupdetail');
            //APODERADO_PERMISSION_SET.add('pca_Pedido_popupdetail');
            //APODERADO_PERMISSION_SET.add('pca_reclamo_newattachment');
            //APODERADO_PERMISSION_SET.add('pca_reclamo_popupnew');
          APODERADO_PERMISSION_SET.add('pca_portales');
          
          //My Service
          APODERADO_PERMISSION_SET.add('pca_myservices');
          //APODERADO_PERMISSION_SET.add('pca_inventory');
          APODERADO_PERMISSION_SET.add('pca_documentmanagement');
         // APODERADO_PERMISSION_SET.add('pca_facturacioncobranza'); //faltan subpages
          //APODERADO_PERMISSION_SET.add('pca_marketplace');
            //APODERADO_PERMISSION_SET.add('pca_catalogo');
            //APODERADO_PERMISSION_SET.add('pca_autogestionoferta');
            //APODERADO_PERMISSION_SET.add('pca_autogestionoferta_popupdetail');
            //Reclamos y pedidos también se usan en MarketPlace
          
          //Ideas Corner
          APODERADO_PERMISSION_SET.add('pca_ideascorner');
          APODERADO_PERMISSION_SET.add('pca_ideasfaq');
          APODERADO_PERMISSION_SET.add('pca_ideascornerdetails');
          APODERADO_PERMISSION_SET.add('pca_ideascornernewpopup');
          APODERADO_PERMISSION_SET.add('pca_ideascornerpopup');
            
            //Quotation tool
			APODERADO_PERMISSION_SET.add('pca_quotationtool');

            //Portal SDN
			APODERADO_PERMISSION_SET.add('pca_portalsdn');

          APODERADO_PERMISSION_SET.add('pca_showprofile');
          APODERADO_PERMISSION_SET.add('pca_eventredirect');
          
          
/*---------------------------------TECNICO---------------------------------*/
          TECNICO_PERMISSION_SET.add('pca_home');
          //TECNICO_PERMISSION_SET.add('pca_equipotelefonica');
                //Escalamiento, Equipo Cliente son apartados de la EquipoTelefonica.
                //TECNICO_PERMISSION_SET.add('subsection_equipotelefonica');
                //TECNICO_PERMISSION_SET.add('subsection_escalamiento');
                //TECNICO_PERMISSION_SET.add('subsection_equipocliente');
                //TECNICO_PERMISSION_SET.add('pca_showuserprofile');
          //TECNICO_PERMISSION_SET.add('pca_customcalendar');
          TECNICO_PERMISSION_SET.add('pca_chattercustom'); 
         // TECNICO_PERMISSION_SET.add('pca_relationmanagement'); 
          
          //Service Tracking
          TECNICO_PERMISSION_SET.add('pca_servicetracking');
          TECNICO_PERMISSION_SET.add('pca_report');
          TECNICO_PERMISSION_SET.add('pca_monitoreo');
          TECNICO_PERMISSION_SET.add('pca_reclamospedidos');
                TECNICO_PERMISSION_SET.add('pca_reclamo_popupdetail');
                TECNICO_PERMISSION_SET.add('pca_Pedido_popupdetail');
                TECNICO_PERMISSION_SET.add('pca_reclamo_newattachment');
                TECNICO_PERMISSION_SET.add('pca_reclamo_popupnew');
                TECNICO_PERMISSION_SET.add('bi2_col_pca_creacionincidencia_pag');
          TECNICO_PERMISSION_SET.add('pca_portales');
          
          //My Service
          TECNICO_PERMISSION_SET.add('pca_myservices');
          TECNICO_PERMISSION_SET.add('pca_inventory');
          TECNICO_PERMISSION_SET.add('pca_documentmanagement');
          //TECNICO_PERMISSION_SET.add('pca_facturacioncobranza');
          //TECNICO_PERMISSION_SET.add('pca_marketplace');
            //TECNICO_PERMISSION_SET.add('pca_catalogo');
            //TECNICO_PERMISSION_SET.add('pca_autogestionoferta');
            //TECNICO_PERMISSION_SET.add('pca_autogestionoferta_popupdetail');
            //Reclamos y pedidos también se usan en MarketPlace
          
          //Ideas Corner
          TECNICO_PERMISSION_SET.add('pca_ideascorner');
          TECNICO_PERMISSION_SET.add('pca_ideasfaq');
          TECNICO_PERMISSION_SET.add('pca_ideascornerdetails');
          TECNICO_PERMISSION_SET.add('pca_ideascornernewpopup');
          TECNICO_PERMISSION_SET.add('pca_ideascornerpopup');
            
            //Quotation tool
          	TECNICO_PERMISSION_SET.add('pca_quotationtool');
            
            //Portal SDN
          	TECNICO_PERMISSION_SET.add('pca_portalsdn');
            
          TECNICO_PERMISSION_SET.add('pca_showprofile');
          TECNICO_PERMISSION_SET.add('pca_eventredirect');
          
/*---------------------------------COMERCIAL---------------------------------*/
          COMERCIAL_PERMISSION_SET.add('pca_home');
          COMERCIAL_PERMISSION_SET.add('pca_equipotelefonica');
                //Escalamiento, Equipo Cliente son apartados de la página.
                COMERCIAL_PERMISSION_SET.add('subsection_equipotelefonica');
                COMERCIAL_PERMISSION_SET.add('subsection_escalamiento');
                COMERCIAL_PERMISSION_SET.add('subsection_equipocliente');
                COMERCIAL_PERMISSION_SET.add('pca_showuserprofile');
          
          COMERCIAL_PERMISSION_SET.add('pca_customcalendar');
          COMERCIAL_PERMISSION_SET.add('pca_chattercustom'); 
          COMERCIAL_PERMISSION_SET.add('pca_relationmanagement'); 
          
          //Service Tracking
          //COMERCIAL_PERMISSION_SET.add('pca_servicetracking');
          //COMERCIAL_PERMISSION_SET.add('pca_report');
          //COMERCIAL_PERMISSION_SET.add('pca_monitoreo');
          //COMERCIAL_PERMISSION_SET.add('pca_reclamospedidos'); 
                //COMERCIAL_PERMISSION_SET.add('pca_reclamo_popupdetail');
                //COMERCIAL_PERMISSION_SET.add('pca_Pedido_popupdetail'); 
                //COMERCIAL_PERMISSION_SET.add('pca_reclamo_newattachment');
                //COMERCIAL_PERMISSION_SET.add('pca_reclamo_popupnew');
          
          //My Service
          COMERCIAL_PERMISSION_SET.add('pca_myservices');
          COMERCIAL_PERMISSION_SET.add('pca_inventory');
          COMERCIAL_PERMISSION_SET.add('pca_documentmanagement');
          COMERCIAL_PERMISSION_SET.add('pca_facturacioncobranza'); //faltan subpages
          COMERCIAL_PERMISSION_SET.add('pca_marketplace');
            COMERCIAL_PERMISSION_SET.add('pca_catalogo');
            COMERCIAL_PERMISSION_SET.add('pca_autogestionoferta');
            COMERCIAL_PERMISSION_SET.add('pca_autogestionoferta_popupdetail');
            COMERCIAL_PERMISSION_SET.add('pca_reclamospedidos');
                COMERCIAL_PERMISSION_SET.add('pca_Pedido_popupdetail');
          COMERCIAL_PERMISSION_SET.add('pca_portales');
          
          //Ideas Corner
          COMERCIAL_PERMISSION_SET.add('pca_ideascorner');
          COMERCIAL_PERMISSION_SET.add('pca_ideasfaq');
          COMERCIAL_PERMISSION_SET.add('pca_ideascornerdetails');
          COMERCIAL_PERMISSION_SET.add('pca_ideascornernewpopup');
          COMERCIAL_PERMISSION_SET.add('pca_ideascornerpopup');
            
            //Quotation tool
          	COMERCIAL_PERMISSION_SET.add('pca_quotationtool');
            
            //Portal SDN
          	COMERCIAL_PERMISSION_SET.add('pca_portalsdn');
          
          COMERCIAL_PERMISSION_SET.add('pca_showprofile');
          COMERCIAL_PERMISSION_SET.add('pca_eventredirect');


/*---------------------------------ADMINISTRATIVO---------------------------------*/
          ADMINISTRATIVO_PERMISSION_SET.add('pca_home');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_equipotelefonica');
                 //Escalamiento, Equipo Cliente son apartados de la página.
                ADMINISTRATIVO_PERMISSION_SET.add('subsection_equipotelefonica');
                ADMINISTRATIVO_PERMISSION_SET.add('subsection_escalamiento');
                ADMINISTRATIVO_PERMISSION_SET.add('subsection_equipocliente');
                ADMINISTRATIVO_PERMISSION_SET.add('pca_showuserprofile');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_customcalendar');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_chattercustom'); 
          ADMINISTRATIVO_PERMISSION_SET.add('pca_relationmanagement'); 
          
          
          //Service Tracking
          ADMINISTRATIVO_PERMISSION_SET.add('pca_servicetracking');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_report');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_monitoreo');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_reclamospedidos'); 
                ADMINISTRATIVO_PERMISSION_SET.add('pca_reclamo_popupdetail');
                ADMINISTRATIVO_PERMISSION_SET.add('pca_Pedido_popupdetail'); 
                ADMINISTRATIVO_PERMISSION_SET.add('pca_reclamo_newattachment');
                ADMINISTRATIVO_PERMISSION_SET.add('pca_reclamo_popupnew');
                ADMINISTRATIVO_PERMISSION_SET.add('bi2_col_pca_creacionincidencia_pag');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_portales');
          
          
          
          //My Service
          ADMINISTRATIVO_PERMISSION_SET.add('pca_myservices');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_inventory');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_documentmanagement');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_facturacioncobranza'); //faltan subpages
          ADMINISTRATIVO_PERMISSION_SET.add('pca_marketplace');
            ADMINISTRATIVO_PERMISSION_SET.add('pca_catalogo');
            ADMINISTRATIVO_PERMISSION_SET.add('pca_autogestionoferta');
            ADMINISTRATIVO_PERMISSION_SET.add('pca_autogestionoferta_popupdetail');
            //Reclamos y pedidos también se usan en MarketPlace
          
          //Ideas Corner
          ADMINISTRATIVO_PERMISSION_SET.add('pca_ideascorner');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_ideasfaq');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_ideascornerdetails');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_ideascornernewpopup');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_ideascornerpopup');
            
            //Quotation tool
            ADMINISTRATIVO_PERMISSION_SET.add('pca_quotationtool');   
         
            //Portal SDN
            ADMINISTRATIVO_PERMISSION_SET.add('pca_portalsdn');
          
          ADMINISTRATIVO_PERMISSION_SET.add('pca_showprofile');
          ADMINISTRATIVO_PERMISSION_SET.add('pca_eventredirect');
          
          
/*---------------------------------------------------------------------------------*/   
        if(cpp.IsEmpty()){        
          MAP_PERMISION_SET.put(Label.BI_Apoderado,APODERADO_PERMISSION_SET);
          MAP_PERMISION_SET.put(Label.BI_ResponsableTecnico,TECNICO_PERMISSION_SET);
          MAP_PERMISION_SET.put(Label.BI_ResponsableComercial,COMERCIAL_PERMISSION_SET);
          MAP_PERMISION_SET.put(Label.BI_ResponsableAdministrativo,ADMINISTRATIVO_PERMISSION_SET);
        }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_ProfileHelper.static', 'Portal Platino', Exc, 'Class');
        }
    }    
    
    global static boolean checkPermissionSet(String page){
            system.debug('UserInfo.getUserId()' +UserInfo.getUserId());
            system.debug('BI_AccountHelper.getCurrentAccountId()' +BI_AccountHelper.getCurrentAccountId());
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            page = page.toLowerCase();
            system.debug('checkPermissionSet()-->page: ' + page);
            List<BI_Contact_Customer_Portal__c> cpp = [Select BI_Perfil__c,BI_User__c from BI_Contact_Customer_Portal__c 
                                                    where BI_User__c = :UserInfo.getUserId() and BI_Cliente__c = :BI_AccountHelper.getCurrentAccountId() limit 1];
            
            if(!cpp.IsEmpty()){                                        
            system.debug('checkPermissionSet()-->cpp' + cpp[0]);
            system.debug('checkPermissionSet()-->Perfil: '+cpp[0].BI_Perfil__c);
            System.debug('\n\n-=#=-\n' + 'DYNAMIC_PERMISSION_SET' + ': ' + DYNAMIC_PERMISSION_SET + '\n-=#=-\n');
            System.debug('\n\n-=#=-\n' + 'DYNAMIC_PERMISSION_SET.contains(page)' + ': ' + DYNAMIC_PERMISSION_SET.contains(page) + '\n-=#=-\n');
                    if(DYNAMIC_PERMISSION_SET.IsEmpty()){
                        system.debug('checkPermissionSet()-->return: ' +  MAP_PERMISION_SET.get(cpp[0].BI_Perfil__c).contains(page));
                        system.debug('checkPermissionSet()-->MAP_PERMISION_SET.get(Perfil): ' + MAP_PERMISION_SET.get(cpp[0].BI_Perfil__c));
                        return MAP_PERMISION_SET.get(cpp[0].BI_Perfil__c).contains(page);   
                    } else{
                        return DYNAMIC_PERMISSION_SET.contains(page);   
                    }
            }else{
                return false;
            }
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('CWP_ProfileHelper.checkPermissionSet', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    global static map<string,string> getPermissionSet(){
        try{
            if(BI_TestUtils.isRunningTest()){
                throw new BI_Exception('test');
            }

            Id AccId = BI_AccountHelper.getCurrentAccountId();
            Map<String,String> mapToVisualForce = new Map<String,String>();
            
            
            if(AccId != null){
                system.debug('AccId ' +AccId);
                system.debug('User ' +UserInfo.getUserId());
                List<BI_Contact_Customer_Portal__c> cpp = [Select BI_Perfil__c,BI_User__c from BI_Contact_Customer_Portal__c 
                                                        where BI_User__c = :UserInfo.getUserId() and BI_Cliente__c = :AccId limit 1];
                                                        
                /*system.debug('getPermissionSet()-->cpp: ' + cpp);
                system.debug('getPermissionSet()-->MAP_PERMISION_SET.get(Perfil): ' + MAP_PERMISION_SET.get(cpp.BI_Perfil__c));*/
                
                if(!cpp.isEmpty()){
                    
                    for(String  Perm : MENU_PERMISSION_SET){
                        System.debug('ENTRO PERM ' + Perm);
                        System.debug('ENTRO?????' + DYNAMIC_PERMISSION_SET.contains(Perm));
                        if(DYNAMIC_PERMISSION_SET.contains(Perm)){
                            mapToVisualForce.put(Perm,'true');
                        }else{
                            mapToVisualForce.put(Perm,'false');
                        }
                    }
                }else{
                    for(String  Perm : MENU_PERMISSION_SET){
                        mapToVisualForce.put(Perm,'false');
                    }
                }
            }else{
                for(String  Perm : MENU_PERMISSION_SET){
                    mapToVisualForce.put(Perm,'true');
                }
            }
            
            return mapToVisualForce;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('CWP_ProfileHelper.getPermissionSet', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }

}