/************************************************************************************
* Avanxo Colombia
* @author           Daniel Alexander Lopez href=<dlopez@avanxo.com>
* Proyect:          Telefonica
* Description:      
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                           Descripción        
*           -----   ----------      ---------------------------     ---------------    
* @version   1.0    2015-08-12      Daniel ALexander Lopez (DL)     New test Class
*                   20/09/2017       Antonio Mendivil Azagra -Add the mandatory fields on account creation: 
*                                            BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c
*************************************************************************************/
@isTest
private class BI_COL_managementBreakPag_tst
{
    Public static list <UserRole>                                           lstRoles;
    Public static User                                                  objUsuario;

    public static Opportunity                                       objOppty;
      
    public static Account                                           objCuenta;
    
    public static List <BI_COL_Modificacion_de_Servicio__c>          lstModSer = new List <BI_COL_Modificacion_de_Servicio__c>();
    public static BI_COL_Modificacion_de_Servicio__c                objModSer;
    
    public static List<BI_COL_Descripcion_de_servicio__c>           lstDesSer;
    public static BI_COL_Descripcion_de_servicio__c                 objDesSer;
    public static BI_COL_Seguimiento_Quiebre__c                     objSegMien;
    public static BI_Log__c                                         objLog;
    
    public static List<RecordType>                                  rtBI_FUN;
    public static List<String>                                      lstIdquiebre = new List<String>();
    public static list<User>                                        lstUsuarios = new list<User>();
    public static list<BI_COL_Seguimiento_Quiebre__c>                lstQuiebre = new List<BI_COL_Seguimiento_Quiebre__c>();                                
    public static String Enviada;
    public static String Respuesta;
    
    
    
    static void crearData()
    {
            List<Profile> lstPerfil = [SELECT Id, Name FROM Profile Where PermissionsCustomizeApplication = true ORDER BY CreatedDate ASC limit 1];
            //lstUsuarios = [Select Id FROM User where country != 'Colombia' AND ProfileId =: lstPerfil[0].Id And isActive = true  Limit 1 ];

            //lstRol
        lstRoles = new list <UserRole>();
        lstRoles = [Select id,Name from UserRole where Name = 'Telefónica Global'];

        //ObjUsuario
        objUsuario = new User();
        objUsuario.Alias = 'standt';
        objUsuario.Email ='pruebas@test.com';
        objUsuario.EmailEncodingKey = '';
        objUsuario.LastName ='Testing';
        objUsuario.LanguageLocaleKey ='en_US';
        objUsuario.LocaleSidKey ='en_US'; 
        objUsuario.ProfileId = lstPerfil.get(0).Id;
        objUsuario.TimeZoneSidKey ='America/Los_Angeles';
        objUsuario.UserName ='pruebas@test.com';
        objUsuario.EmailEncodingKey ='UTF-8';
        objUsuario.UserRoleId = lstRoles.get(0).Id;
        objUsuario.BI_Permisos__c ='Sucursales';
        objUsuario.Pais__c='Colombia';
        insert objUsuario;
        
        lstUsuarios = new list<User>();
        lstUsuarios.add(objUsuario);
            
        System.runAs(lstUsuarios[0])
        {
            BI_bypass__c objBibypass = new BI_bypass__c();
            objBibypass.BI_migration__c=true;
            insert objBibypass;
            
            objCuenta                     = new Account();
            objCuenta.Name                  = 'prueba';
            objCuenta.BI_Country__c             = 'Colombia';
            objCuenta.TGS_Region__c             = 'América';
            objCuenta.BI_Tipo_de_identificador_fiscal__c  = 'NIT';
            objCuenta.CurrencyIsoCode             = 'GTQ';
            objCuenta.BI_Segment__c                         = 'test';
            objCuenta.BI_Subsegment_Regional__c             = 'test';
            objCuenta.BI_Territory__c                       = 'test';
            insert objCuenta;
            
          System.debug('\n\n\n ======= CUENTA ======\n '+ objCuenta );
            
            objOppty                            = new Opportunity();
            objOppty.Name                      = 'TEST AVANXO OPPTY';
            objOppty.AccountId                = objCuenta.Id;
            //objOppty.TGS_Region__c            = 'América';
            objOppty.BI_Country__c              = 'Colombia';
            objOppty.CloseDate                = System.today().addDays(+5);
            objOppty.StageName                = 'F6 - Prospecting';
            objOppty.BI_Ciclo_ventas__c           = Label.BI_Completo;
            insert objOppty; 
         
          
            
          
            objDesSer                                           = new BI_COL_Descripcion_de_servicio__c();
            objDesSer.BI_COL_Oportunidad__c                     = objOppty.Id;
            //objDesSer.CurrencyIsoCode                           = 'COP';
            insert objDesSer;
          
          System.debug('\n\n\n ======= objDesSer '+ objDesSer);
            
          
            objModSer                                   = new BI_COL_Modificacion_de_Servicio__c();
            objModSer.BI_COL_Codigo_unico_servicio__c   = objDesSer.Id;
            objModSer.BI_COL_Clasificacion_Servicio__c  = 'ALTA';
            objModSer.BI_COL_Estado__c                  = 'Pendiente';
            objModSer.BI_COL_Fecha_instalacion_servicio_RFS__c  = System.today().addDays(+5);
            objModSer.BI_COL_Oportunidad__c                      = objOppty.Id;
            //objModSer.BI_COL_Direccion_IP__c            = i+'';
            //insert objModSer;
            lstModSer.add(objModSer);
          
            
            insert lstModSer;
            
          System.debug('\n\n\n ======= lstModSer '+ lstModSer);
            
            
            
            objSegMien                                  = new BI_COL_Seguimiento_Quiebre__c();
            objSegMien.BI_COL_Observaciones__c          = 'Prueba avanxo';
            objSegMien.BI_COL_Modificacion_de_Servicio__c = lstModSer[0].Id;
            objSegMien.BI_COL_Tipo_gestion__c           = 'Informativo';
            objSegMien.BI_COL_Transaccion__c            =null;
            objSegMien.BI_COL_Aplazar_MS__c             =false;
            objSegMien.BI_COL_Codigo_Quiebre_Sisgot__c  ='Fallido Conexion';
            objSegMien.BI_COL_Estado_de_Gestion__c      ='Abierto';
            objSegMien.BI_COL_Estado_envio__c           ='Creado SFDC';
            
            lstQuiebre.add(objSegMien);
            
            insert lstQuiebre;
            //lstIdquiebre.add(objSegMien.Id);
            
         }
    }
        
        
        static testmethod void test1()
        {
            crearData();
            
            System.runAs(lstUsuarios[0])
            {
                PageReference                 pageRef           = Page.BI_COL_managementBreak_Pag;
                Test.setCurrentPage(pageRef);
                ApexPages.StandardController  controler         = new ApexPages.StandardController(objSegMien);
                
                BI_COL_managementBreakPag_ctr.WrapperGestion wrapper = new BI_COL_managementBreakPag_ctr.WrapperGestion(true,lstQuiebre[0]);
                
                list<BI_COL_managementBreakPag_ctr.WrapperGestion> lstWrapper = new list<BI_COL_managementBreakPag_ctr.WrapperGestion>();
                
                lstWrapper.add(wrapper);
                
                Test.startTest();
                    Test.setMock( WebServiceMock.class, new ws_TrsMasivo_mws());
                    BI_COL_managementBreakPag_ctr controlador   = new BI_COL_managementBreakPag_ctr(controler);
                    controlador.lstQui                          = lstQuiebre;
                    controlador.listaQuiebres                   = lstWrapper;
                    controlador.lstImpresion                    = lstWrapper;
                    controlador.pageSize                        = 1;
                    controlador.pageNumber                      = 1;
                    controlador.todosMarcados                   = false;
                    controlador.strIdResponsable                = lstUsuarios[0].id;
                    controlador.modificarResponsable();
                    controlador.action_seleccionarTodos();
                    controlador.nextBtnClick();
                    controlador.previousBtnClick();
                    Boolean boolaux = controlador.getPreviousButtonEnabled();
                    boolaux = controlador.getNextButtonDisabled();
                    Integer a = controlador.getTotalPageNumber();
                    
                    system.debug('Test'+controlador.listaResponsables);
                
                Test.stopTest();
            }
        }


}