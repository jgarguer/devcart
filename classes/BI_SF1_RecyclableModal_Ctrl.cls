/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Pardo Corral
    Company:       Accenture
    Description:   Controller for the component BI_SF1_RecyclableModal, to manage the actions
    History: 
    <Date>                  <Author>                <Change Description>
    30/11/2017              Antonio Pardo corral    Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_SF1_RecyclableModal_Ctrl {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Pardo Corral
    Company:       Accenture
    Description:   Delete one record

    IN:             String recordId - Id of the record to delete
                    String objName - ApiName of the object

    History: 
    <Date>                  <Author>                <Change Description>
    30/11/2017              Antonio Pardo corral    Initial Version 
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@AuraEnabled
	public static void eliminar(String recordId, String objName){
		Delete database.query('SELECT Id FROM '+objName+' WHERE Id =\''+recordId+'\' LIMIT 1');
	}

}