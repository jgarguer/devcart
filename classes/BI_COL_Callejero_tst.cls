/**
* Avanxo Colombia
* @author           Oscar Alejandro Jimenez Forero
* Proyect:          Telefonica
* Description:      Apex Class
*
* Changes (Version)
* -------------------------------------
*           No.     Fecha           Autor                                   Descripción
*           -----   ----------      --------------------                    ---------------
* @version   1.0    2015-08-26      Oscar Alejandro Jimenez Forero (OAJF)   Class Created
*************************************************************************************************************/
@isTest
global class BI_COL_Callejero_tst implements WebServiceMock{
	
	global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType ){
		BI_COL_Callejero_wsdl.Interfaz1Response_element objResp = new BI_COL_Callejero_wsdl.Interfaz1Response_element();
    	objResp.Interfaz1Result = 'resp';

		response.put( 'response_x', objResp);
	}

	@isTest
	static void test_method_1() {
		
		BI_COL_Callejero_wsdl.ArrayOfParametrosGeo misParametrosGeo = new BI_COL_Callejero_wsdl.ArrayOfParametrosGeo();
		BI_COL_Callejero_wsdl.WebServiceSoap webser = new BI_COL_Callejero_wsdl.WebServiceSoap();
		BI_COL_Callejero_wsdl.ParametrosGeo par = new BI_COL_Callejero_wsdl.ParametrosGeo();
		Test.startTest();		
		Test.setMock( WebServiceMock.class, new BI_COL_Callejero_tst() );
		webser.Interfaz1(misParametrosGeo);
		Test.stopTest();		
	}
	
	
}