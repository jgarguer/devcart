@isTest
private class BI_InvoiceMultipleRest_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_InvoiceMultipleRest class
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    17/09/2014              Pablo Oliva             Initial Version
    27/05/2016              Guillermo Muñoz         BI_TestUtils.throw_exception set to false to avoid test errors
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static{
        BI_TestUtils.throw_exception = false;
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_InvoiceMultipleRest.getMultipleInvoices()
    History:
    
    <Date>              <Author>                <Description>
    17/09/2014          Pablo Oliva             Initial version
    27/05/2016          Guillermo Muñoz         Changed BI_TestUtils.throw_exception values caused by changes in the global value
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getMultipleInvoicesTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
		//List <Region__c> lst_region = BI_DataLoad.loadRegions().values();
		Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        List<String>lst_region = new List<String>();
       
        
        lst_region.add('Argentina');
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');
        
        //BI_Pickup_Option__c pickupOption = BI_DataLoadRest.loadPickUpOptions(lst_region);          							   			 
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        BI_DataLoadRest.SKIP_PARAMETER = true;
        BI_DataLoadRest.MAX_LOOP = 5;

        List <BI_Facturas__c> lst_inv = BI_DataLoadRest.loadInvoices(200, lst_region, lst_acc[0]);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/accountresources/v1/invoices';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

        BI_TestUtils.throw_exception = true;
        
        //INTERNAL_SERVER_ERROR
        BI_InvoiceMultipleRest.getMultipleInvoices();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
       
       	system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        //BAD REQUEST missing required parameters
        BI_TestUtils.throw_exception = false;
        BI_InvoiceMultipleRest.getMultipleInvoices();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //BAD REQUEST invalid parameters
        req.addParameter('limit', 'test');
        RestContext.request = req;
        BI_InvoiceMultipleRest.getMultipleInvoices();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //BAD REQUEST invalid date
        req.addParameter('limit', '2');
        req.addParameter('startCreationDate', 'test');
        RestContext.request = req;
        BI_InvoiceMultipleRest.getMultipleInvoices();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //BAD REQUEST countryISO
        req.addParameter('limit', '2');
        req.addParameter('offset', '2');
        
        req.addParameter('startCreationDate', '2000-05-30T09:30:10Z');
        req.addParameter('endCreationDate', '2100-05-30T09:30:10Z');
        
        req.addParameter('accountId', lst_acc[0].BI_Id_del_cliente__c);
        req.addParameter('status', 'pending');
        req.addParameter('country', region.BI_Country_ISO_Code__c);
        
        RestContext.request = req;
        BI_InvoiceMultipleRest.getMultipleInvoices();
        system.assertEquals(400,RestContext.response.statuscode);
        
        //NOT FOUND
        req.addParameter('accountId', 'test');
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_InvoiceMultipleRest.getMultipleInvoices();
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        req.addParameter('accountId', lst_acc[0].BI_Id_del_cliente__c);
        RestContext.request = req;
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_InvoiceMultipleRest.getMultipleInvoices();
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();

    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}