/*---------------------------------------------------------------------------------------------------------------------------------------------------------------
Author:			Eduardo Ventura
Company:		Everis España
Description:	RFG-INT-CHI-01: Validación del identificador fiscal.

History:

<Date>                      <Author>                        <Change Description>
10/10/2016                  Eduardo Ventura					Versión Inicial
24/10/2017									   				Ever01:Inclusion de logs
---------------------------------------------------------------------------------------------------------------------------------------------------------------*/

public class FS_CHI_Contact_Management {
    /* DECLARACIÓN DE VARIABLES */
    /* Vacío */
    
    public static BI_RestWrapper.ContactsListType Sync(String documentType, String documentNumber, String serialNumber){
        System.debug('Nombre de clase: FS_CHI_Contact_Management   Nombre de método: Sync   Comienzo del método');
        /* Declaración de variables */
        BI_RestWrapper.ContactsListType contactsList;
        BI_RestWrapper.ContactsListWrapper contactsListAux;        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        
        /*REQUEST CONFIGURATION*/
        FS_CHI_Integraciones__c config_integracion = FS_CHI_Integraciones__c.getInstance();
        //Ever01-INI
        String docNum = EncodingUtil.urlEncode(documentNumber,'UTF-8');
        String serialNum = EncodingUtil.urlEncode(serialNumber,'UTF-8');
        String endPoint =docNum+'&ad_SerialNumber='+serialNum;
        request.setEndpoint('callout:FS_CHI_DataPower/accountresources/v1/contacts?nationalID='+ endPoint +'&apikey='+config_integracion.FS_CHI_ApiKey__c);
        //Ever01-FIN
        request.setMethod('GET');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', config_integracion.FS_CHI_AuthorizationID__c);
        request.setTimeout(25000);
        if(Test.isRunningTest()){response = FS_CHI_Contact_Management_Mock.respond(request);}
        else response = http.send(request);
        System.debug('Nombre de clase: FS_CHI_Contact_Management   Nombre de método: Sync   AfterSend    Body: ' + response.getBody() + 'FIN BODY'); 
        /* Process */
        if(response.getStatusCode() == 200 && response.getBody()!= null) {
            contactsListAux = (BI_RestWrapper.ContactsListWrapper) JSON.deserialize(response.getBody(), BI_RestWrapper.ContactsListWrapper.class);
            contactsList = contactsListAux.contactList;
            contactsList.totalResults = contactsListAux.totalResults;
        }else{
            throw new FS_CHI_Contact_Record_Controller.FS_CHI_IntegrationException('Error:'+response.getStatusCode()+ ' ' + response.getBody());
            contactsListAux = (BI_RestWrapper.ContactsListWrapper) JSON.deserialize(response.getBody(), BI_RestWrapper.ContactsListWrapper.class);
            //contactsList.description =  contactsListAux.description;
            System.debug('getBody() ' + response.getBody());
        }         
        //Ever01-FIN
        System.debug('Nombre de clase: FS_CHI_Contact_Management   Nombre de método: Sync   Finalización del método    Resultado: ' + contactsList);        
        return contactsList;
    }
}