@isTest
private class BI_TicketsForAccountRest_TEST {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_TicketsForAccountRest class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    19/09/2014              Pablo Oliva             Initial Version
    27/05/2016              Guillermo Muñoz         BI_TestUtils.throw_exception set to false to avoid test errors
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    static{
        BI_TestUtils.throw_exception = false;
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pablo Oliva
    Company:       Salesforce.com
    Description:   Test method to manage the coverage code for BI_TicketsForAccountRest.getTickets()
    History:
    
    <Date>              <Author>                <Description>
    19/09/2014          Pablo Oliva             Initial version
    27/05/2016          Guillermo Muñoz         Changed BI_TestUtils.throw_exception values caused by changes in the global value
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void getTicketsTest() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        
		Map<String, BI_Code_ISO__c> biCodeIso = BI_DataLoad.loadRegions();
        
        List<String>lst_region = new List<String>();
       
        lst_region.add('Argentina');
        
        //retrieve the country
        BI_Code_ISO__c region = biCodeIso.get('ARG');
        
        List <Account> lst_acc = BI_DataLoadRest.loadAccountsExternalId(1, lst_region, false);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
             
        req.requestURI = '/customeraccounts/v1/accounts/'+lst_acc[0].BI_Id_del_cliente__c+'/tickets';  
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();
        
        BI_TestUtils.throw_exception = true;

        //INTERNAL_SERVER_ERROR
        BI_TicketsForAccountRest.getTickets();
        List<BI_Log__c> lst_log = [select Id, BI_Asunto__c, BI_Bloque_funcional__c, BI_Descripcion__c, BI_Tipo_de_componente__c from BI_Log__c];
       
        system.assertEquals(500,RestContext.response.statuscode);
        system.assertEquals('test', lst_log[0].BI_Descripcion__c);
        system.assertEquals(1, lst_log.size());
        
        BI_TestUtils.throw_exception = false;
        
        //BAD REQUEST
        req.requestURI = '/customeraccounts/v1/accounts/'+lst_acc[0].BI_Id_del_cliente__c+'/tickets';  
        RestContext.request = req;
        BI_TicketsForAccountRest.getTickets();
        
        system.assertEquals(400,RestContext.response.statuscode);
        
        //NOT FOUND
//        RestContext.request.addHeader('countryISO', lst_region[0].BI_Codigo_ISO__c);
        RestContext.request.addHeader('countryISO', region.BI_Country_ISO_Code__c);
        BI_TicketsForAccountRest.getTickets();
        
        system.assertEquals(404,RestContext.response.statuscode);
        
        //OK
        List <Case> lst_tickets = BI_DataLoadRest.loadTickets(1, lst_acc[0].Id, lst_region[0]);
        BI_TicketsForAccountRest.getTickets();
        
        system.assertEquals(200,RestContext.response.statuscode);
        
        Test.stopTest();

    }
    
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}