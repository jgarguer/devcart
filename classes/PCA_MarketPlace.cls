public with sharing class PCA_MarketPlace extends PCA_HomeController{

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   Controller of relationship management page
    
    History:
    
    <Date>            <Author>              <Description>
    16/12/2014        Antonio Moruno        Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/  
    
    public BI_ImageHome__c ImgBack {get;set;}
    public Attachment AttImg {get;set;}
    public BI_ImageHome__c RelationImage {get;set;}
    public BI_ImageHome__c RelationImageMovi {get;set;}
    public BI_ImageHome__c RelationImageTel {get;set;}
    public String AccSegmento {get;set;}
    
    public PCA_MarketPlace() {}
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   method that check permissions
    History:
    
    <Date>            <Author>              <Description>
    16/12/2014        Antonio Moruno       Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
    public PageReference checkPermissions (){
        try{
            PageReference page = enviarALoginComm();
            if(page == null){
                loadInfo();
            }
            return page;
        }catch (exception Exc){
           BI_LogHelper.generate_BILog('PCA_Cobranza_PopUpDetail.checkPermissions', 'Portal Platino', Exc, 'Class');
           return null;
        }
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Antonio Moruno
    Company:       Aborda
    Description:   Return the image and description of the global page
    
    History:
    
    <Date>                    <Author>               <Description>
    16/12/2014                Antonio Moruno         Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public void loadInfo(){
    try{
        Id ActAccId = BI_AccountHelper.getCurrentAccountId();
        Account SegAccount = [Select Id, Name,BI_Country__c, BI_Segment__c FROM Account Where Id=:ActAccId];
        List<BI_ImageHome__c> RelImg = new List<BI_ImageHome__c>();
            if(SegAccount.BI_Segment__c!=null){
                RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'MarketPlace' AND  RecordType.Name= 'General' AND BI_Segment__c =: SegAccount.BI_Segment__c LIMIT 1];
                
                if(!RelImg.isEmpty()) {
                    if(RelImg[0].BI_Segment__c!=null){
                        
                        if(RelImg[0].BI_Segment__c=='Empresas'){
                            RelationImageTel = RelImg[0];
                        }else if(RelImg[0].BI_Segment__c=='Negocios'){
                            RelationImageMovi = RelImg[0];
                        }
                    }
                }
            }else{
                    RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'MarketPlace' AND  RecordType.Name= 'General'  AND BI_Segment__c='Empresas' LIMIT 1];
                    if(!RelImg.isEmpty()){
                        RelationImageTel = RelImg[0];
                    }else{
                        RelImg = [Select BI_Pestana__c, BI_Titulo__c, BI_url__c, BI_Informacion_Menu__c, BI_Segment__c FROM BI_ImageHome__c Where BI_Pestana__c = 'MarketPlace' AND  RecordType.Name= 'General'  AND BI_Segment__c='Negocios' LIMIT 1];
                        if(!RelImg.isEmpty()){
                            RelationImageMovi = RelImg[0];
                        }
                    
                    }
                }
        if(RelationImageTel!=null){
            
            ImgBack = RelationImageTel;
            
            Attachment AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImageTel.Id Limit 1];
            if(AttTarget!=null){
                AttImg = AttTarget;
            }
        }else if(RelationImageMovi!=null){
            
            ImgBack = RelationImageMovi;
            
            Attachment AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImageMovi.Id Limit 1];
            if(AttTarget!=null){
                AttImg = AttTarget;
            }
        }/*else if(RelationImage!=null){
            
            ImgBack = RelationImage;
            
            Attachment AttTarget = [SELECT Id, parentId FROM Attachment WHERE parentId =: RelationImage.Id Limit 1];
            if(AttTarget!=null){
                AttImg = AttTarget;
            }
        
        }*/
        
        }catch (exception Exc){
            BI_LogHelper.generate_BILog('PCA_MarketPlace.loadInfo', 'Portal Platino', Exc, 'Class');
        
        }
    }

}