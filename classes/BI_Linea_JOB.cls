global without sharing class BI_Linea_JOB implements Schedulable {

	global String email_user;
	global List<Id> lst_ven;
	global Integer pos;
	global Map<Id, String> result;
	global Map<Id, String> map_lin;
	global Integer option;

	global void execute(SchedulableContext sch) {
        
        system.abortJob(sch.getTriggerId());
        if(option == 0)
			BI_Siscel_Helper.LineaVenta_Mass(email_user, lst_ven, pos, result, map_lin);	
		else if(option == 1)
        	BI_Siscel_Helper.LineaRecambio_Mass(email_user, lst_ven, pos, result, map_lin);
        else if(option == 2)	
        	BI_Siscel_Helper.LineaServicio_Mass(email_user, lst_ven, pos, result);
	}

    public BI_Linea_JOB(String email, List<Id> lst, Integer posi, Map<Id, String> res, Map<Id, String> mlin, Integer op){
    	
    	email_user = email;
    	lst_ven = lst;
    	pos = posi;
    	result = res;
    	map_lin = mlin;
    	option = op;
    	
    }

}