@isTest
private class TestChangeOrderItemProduct {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio Llorca
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for ChangeOrderItemProduct class 
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    01/07/2014              Ignacio Llorca	        Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void testChange() {
        
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        NE__Order__c ord = new NE__Order__c();
        insert ord;
        
        NE__Product__c prod = new NE__Product__c();
        prod.Name = 'TestProd';
        insert prod;
        
        NE__Catalog_Header__c catH = new NE__Catalog_Header__c();
        catH.Name='t';
        catH.NE__Name__c ='t' ;
        insert catH;
        
        NE__Catalog__c cat = new NE__Catalog__c();
        cat.NE__Catalog_Header__c = catH.Id;
        insert cat;
        
        NE__Catalog_Item__c catItem = new NE__Catalog_Item__c();
        catItem.NE__ProductId__c = prod.Id;
        catItem.NE__Catalog_Id__c = cat.Id;
        insert catItem;
        
        NE__OrderItem__c ordItem = new NE__OrderItem__c();
        ordItem.NE__OrderId__c = ord.Id;
        ordItem.NE__CatalogItem__c = catItem.Id;
        ordItem.NE__Qty__c = 1;
        insert ordItem;
            
        Map<String, String> mapOfProds = new Map<String, String>(); 
        String newProdID;
        String description='Desc';
        
        Test.startTest();
        try{
        	newprodID = prod.Id;
        	mapOfProds.put(ordItem.Id ,  newprodID);
        	ChangeOrderItemProduct p = new ChangeOrderItemProduct();
        	p.changeOrderItemProd(mapOfProds, description);
        	
        	catItem.Is_Dummy__c = true;
         	update catItem;
         	newprodID = null;
         	mapOfProds.put(ordItem.Id ,  newprodID);
        	p.changeOrderItemProd(mapOfProds, description);
        	
        	catItem.Is_Dummy__c = false;
        	update catItem;
         	newprodID = null;
         	mapOfProds.put(ordItem.Id ,  newprodID);
       		p.changeOrderItemProd(mapOfProds, description);
       		
        }catch(Exception e){
        	
        }                 
        
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}