public with sharing class BI_G4C_BI_Sede_Methods {
    
     /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Julio Alberto Asenjo García
    Company:       Everis 
    Description:   Aux class to Bi_Sede.trigger / Copia la sede de tipo "comercial principal" a la ShippingAddress
    
    IN:           List<Account> news 
    OUT:          void
    
    History:
    
    <Date>            <Author>                            <Description>
    31/05/2016        Julio Alberto Asenjo García         Initial Version
    10/06/2016        Julio Alberto Asenjo García         Include method shippingAddressToDirComercial
    28/10/2016        Julio Alberto Asenjo García         Cambios para quitar los campos latitud y longitud que no se usan. [C1]
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static void shippingAddressToDirComercial (List<BI_Punto_de_instalacion__c> news){
       
        
        List<Account> accountUpdate=new List<Account>();
        List<Id> lsId=new List<Id>();
        List<Id> lsIdClientes=new List<Id>();
        for (BI_Punto_de_instalacion__c aux : news){
            lsId.add(aux.BI_Sede__c); 
            lsIdClientes.add(aux.BI_Cliente__c)  ;
        }
         
        List<BI_Sede__c> ls = [select Id,Name,BI_Localidad__c, BI_Numero__c, BI_Provincia__c, BI_Country__c,BI_Longitud__c, BI_COL_Latitud__c,BI_COL_Longitud__c, BI_Codigo_postal__c, BI_COL_Direccion_sin_espacio__c from BI_Sede__c WHERE Id IN : lsId];
        List<Account> lsAcc=[SELECT id, ShippingStreet FROM Account Where id IN:lsIdClientes AND ShippingStreet!=null];
        Map <Id,BI_Sede__c> mapaSedes=new Map<Id, BI_Sede__c>(ls);
        Map <Id,Account> mapaCuentas=new Map<Id, Account>(lsAcc);
        Account c;
        for (BI_Punto_de_instalacion__c aux : news){
            if(aux.BI_Tipo_de_sede__c=='Comercial Principal' && !mapaCuentas.containsKey(aux.BI_Cliente__c)){
                c=new Account();
                c.Id=aux.BI_Cliente__c; 
                c.ShippingStreet=mapaSedes.get(aux.BI_Sede__c).BI_COL_Direccion_sin_espacio__c+' '+mapaSedes.get(aux.BI_Sede__c).BI_Numero__c;
                c.ShippingCity=mapaSedes.get(aux.BI_Sede__c).BI_Localidad__c;
                c.ShippingState=mapaSedes.get(aux.BI_Sede__c).BI_Provincia__c;
                c.ShippingPostalCode=mapaSedes.get(aux.BI_Sede__c).BI_Codigo_postal__c;
                c.ShippingCountry=mapaSedes.get(aux.BI_Sede__c).BI_Country__c;
                
                
                
              /* [C1]
               *  if(mapaSedes.get(aux.BI_Sede__c).BI_Longitud__Latitude__s!=null && mapaSedes.get(aux.BI_Sede__c).BI_Longitud__Longitude__s!=null){
                    c.BI_G4C_location__latitude__s=mapaSedes.get(aux.BI_Sede__c).BI_Longitud__Latitude__s;
                    c.BI_G4C_location__longitude__s=mapaSedes.get(aux.BI_Sede__c).BI_Longitud__Longitude__s;
                }
                *[C1]
                */
                  
             
                System.debug('El punto de instalación es '+aux+'la cuenta es '+c);
                accountUpdate.add(c);
            }
        }
        if (accountUpdate.size()>0){
            update accountUpdate;
        }
    }
    
}