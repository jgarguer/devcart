@isTest
private class BI_CSB_MonitorController_TEST
{
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando Arteaga
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_CSB_MonitorController
    History:
    
    <Date>              <Author>                <Description>
    17/11/2015          Fernando Arteaga        Initial version
    30/05/2016			Fernando Arteaga		Fill BI_CSB_Record_Id__c field in trasactions
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    static testMethod void myUnitTest()
    {

		BI_Log__c trans = new BI_Log__c(BI_COL_Estado__c = 'Pendiente',
        								BI_Asunto__c = 'Transacción CSB',
        								BI_Descripcion__c = 'Creación de pedido en CSB',
        								BI_CSB_Record_Id__c = 'xxxx');
        								
        BI_Log__c trans2 = new BI_Log__c(BI_COL_Estado__c = 'Pendiente',
        								 BI_Asunto__c = 'Transacción CSB',
        								 BI_Descripcion__c = 'Creación de pedido en CSB',
        								 BI_CSB_Record_Id__c = 'yyyy');
        								 
		insert new List<BI_Log__c>{trans, trans2};
		
		PageReference p = Page.BI_CSB_Monitor;
		Test.setCurrentPageReference(p);
		Test.startTest();
		
		ApexPages.StandardController controller = new ApexPages.StandardController(new BI_Log__c()); 
        BI_CSB_MonitorController monitor = new BI_CSB_MonitorController(controller);
        List<BI_Log__c> listAux = monitor.getTransactions();
        monitor.delId = trans.Id;
        monitor.deleteTransaction();
        p = monitor.refreshPageSize();
        Test.stopTest();
        
    }
}