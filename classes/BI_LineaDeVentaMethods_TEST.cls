@isTest
private class BI_LineaDeVentaMethods_TEST {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Fernando González
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for BI_LineaDeVentaMethods class
    
    History: 
    <Date> 					<Author> 				<Change Description>
    27/03/2015      		Fernando González    	Initial Version		
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
    static{
    	
    	BI_TestUtils.throw_exception = false;
    }
	
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 	Author:        Fernando González
 	Company:       Salesforce.com
 	Description:   Test method to manage the code coverage for BI_LineaDeVentaMethods
		    
 	History: 
 	
 	<Date> 					<Author> 				<Change Description>
 	27/03/2015      		Fernando González    	Initial Version	
 	03/02/2016              Guillermo Muñoz         Fix bugs caused by news validations rules
 	---------------------------------------------------------------------------------------------------------------------------------------------------------*/
 	
 	static testMethod void LineaDeVentaMethodTest(){
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
				
		list<BI_Linea_de_Venta__c> lstNewLineaVenta = new list<BI_Linea_de_Venta__c>();
		list<BI_Linea_de_Venta__c> lstNewLineaVenta2 = new list<BI_Linea_de_Venta__c>();
		list<BI_Linea_de_Venta__c> lstUpd = new list<BI_Linea_de_Venta__c>();
		User usu = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema AND Id !=: UserInfo.getUserId() LIMIT 1];

		System.runAs(Usu){
			lstNewLineaVenta = BI_DataLoadAgendamientos.loadLineaVentaWithSolicitud(1);
		}
		
		test.startTest();
		
		BI_DataLoadAgendamientos.UpdateLineaVenta(lstNewLineaVenta);
		BI_DataLoadAgendamientos.UpdateLineaVentaStatus(lstNewLineaVenta);
		
		// for coverage of  BI_LineaDeVentaMethods.updateSol method
		list<BI_Linea_de_Venta__c> lstLineaVenta2 = [SELECT Id FROM BI_Linea_de_Venta__c WHERE BI_Estado_de_linea_de_venta__c != NULL];
		
		if ( !lstLineaVenta2.isEmpty() ){
			system.debug('lstLineaVenta2 es : ' + lstLineaVenta2 );
			lstLineaVenta2[0].BI_Estado_de_linea_de_venta__c = 'new estado linea test';
			lstUpd.Add(lstLineaVenta2[0]);
			system.debug('lstUpd es : ' + lstUpd );
			update lstUpd;
		}
		
		test.stopTest();
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Guillermo Muñoz
	    Company:       Aborda
	    Description:   Test method that manage the code coverage of BI_LineaDeVentaMethods
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    21/12/2015                      Guillermo Muñoz             Initial Version
	    03/02/2016                      Guillermo Muñoz             Fix bugs caused by news validations rules
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void updateDescTest(){

		BI_TestUtils.throw_exception=false;
		List <Recordtype> lst_rt = [SELECT Id,DeveloperName FROM Recordtype WHERE DeveloperName = 'BI_Modelo' OR DeveloperName = 'BI_Servicio'];
		List <Account> lst_acc = BI_Dataload.loadAccountsPaisStringRef(1,new List<String>{label.BI_Chile});
		List <BI_Descuento_para_modelo__c> lst_dm = new List <BI_Descuento_para_modelo__c>();
		List <BI_Linea_de_Venta__c> lst_lv = new List <BI_Linea_de_Venta__c>();
		List <BI_Linea_de_Servicio__c> lst_ls = new List <BI_Linea_de_Servicio__c>();
		Id rtId;
		User usu = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name =: Label.BI_Administrador_Sistema AND Id !=: UserInfo.getUserId() LIMIT 1];

		BI_Solicitud_envio_productos_y_servicios__c sol = new BI_Solicitud_envio_productos_y_servicios__c(
			BI_Estado__c = 'Abierto',
			BI_Cliente__c = lst_acc[0].id
		);
		System.runAs(usu){
			insert sol;
		}

		sol.BI_Estado_de_ventas__c = 'Aprobado';
		sol.BI_Lineas_de_venta__c = true;
		update sol;

		BI_Descuento__c descuento = new BI_Descuento__c(
			BI_Cliente__c = lst_acc[0].id,
			BI_Descuento_aplica__c = 'Línea de venta',
			BI_Oferta__c = 'OfertaTest',
			BI_Origen_de_descuento__c = 'Pricing',
			BI_Periodo_de_recambio__c = 12,
			BI_Estado_del_Proceso__c = 'Pricing'
		);
		insert descuento;

		for(Recordtype rt : lst_rt){
			if(rt.DeveloperName == 'BI_Modelo'){
				rtId=rt.id;
			}
		}

		BI_Modelo__c sim = new BI_Modelo__c(
				BI_Nombre_de_producto__c = 'sim',
				RecordTypeId = rtId,
				BI_Familia_Local__c = 'SIMCARD'
		);
		insert sim;

		BI_Modelo__c modelo = new BI_Modelo__c(
			BI_Nombre_de_producto__c = 'movil',
			RecordTypeId = rtId,
			BI_Tipo_de_SIM__c = sim.id,
			BI_Familia_Local__c = '03 Smartphone Entry'
		);
		insert modelo;

		for(Recordtype rt : lst_rt){
			if(rt.DeveloperName == 'BI_Servicio'){
				rtId=rt.id;
			}
		}

		BI_Modelo__c servicio = new BI_Modelo__c(
			BI_Nombre_de_producto__c = 'Servicio Smartphone',
			BI_Familia_Local__c = 'Smartphone',
			RecordTypeId = rtId
		);
		insert servicio;

		for(Integer i = 0; i < 4 ; i++){
				
			BI_Descuento_para_modelo__c descM = new BI_Descuento_para_modelo__c(
				BI_Cantidad__c = 100,
				BI_Cantidad_disponible__c = 100,
				BI_Descuento__c = descuento.Id,
				BI_Descuento_porcentaje__c = 20,
				BI_Estado__c = 'Activo',
				BI_ID_de_Modelo__c = modelo.Id
			);	
			lst_dm.add(descM);
		}

		for(Integer i = 0; i < 4 ; i++){
				
			BI_Descuento_para_modelo__c descM = new BI_Descuento_para_modelo__c(
				BI_Cantidad__c = 120,
				BI_Cantidad_disponible__c = 100,
				BI_Descuento__c = descuento.Id,
				BI_Descuento_porcentaje__c = 20,
				BI_Estado__c = 'Activo',
				BI_ID_de_Modelo__c = sim.Id
			);	
			lst_dm.add(descM);
		}

		insert lst_dm;

		for(Integer i = 0; i < 4; i++){

			BI_Linea_de_Venta__c lv = new BI_Linea_de_Venta__c(
				BI_Estado_de_linea_de_venta__c = 'Pendiente',
	            BI_Modalidad_de_venta__c = '1',
	            BI_Solicitud__c = sol.Id,
	            BI_Cantidad_de_equipos__c = 1,
	            BI_Inlcuir_tarjeta_SIM__c = 'Sí',
	            BI_Descuenta_puntos_Movistar_SIM__c = 'No',
	            BI_Descuenta_puntos_Movistar__c = 'No',
	            BI_Codigo_de_descuento_de_modelo__c = null,
	            BI_Codigo_de_Descuento_TEM__c = lst_dm[i].Id,
	            BI_Codigo_de_descuento_de_SIM__c = null,
	            BI_Descuento_SIM_TEM__c =lst_dm[i+4].Id,
	            BI_Modelo__c = modelo.Id,
	            BI_ID_de_simcard__c = sim.Id,
	            BI_Plan__c = 'Plan en construcción',
	            BI_Nuevo_codigo_de_cliente__c = true,
	            BI_Codigo_de_cliente_final__c = lst_acc[0].Name,
	            BI_RUT_hijo__c = 'RutHijo',
	            BI_Nombre_persona__c = 'Patricia Arredondo',
	            BI_Codigo_cliente_hijo__c = 'codigoHijo',
	            BI_Tipo_de_Contrato__c = '91'
	        );

			lst_lv.add(lv);
		}
		
       
        insert lst_lv;
        for(Integer i = 0; i < 4; i++){
	    
	        BI_Linea_de_Servicio__c ls = new BI_Linea_de_Servicio__c(
	        	BI_Modelo__c = servicio.Id,
	        	BI_Linea_de_Venta__c = lst_lv[i].Id
	        );
        	lst_ls.add(ls);
    	}
        
        insert lst_ls;

		delete lst_lv;
	}
}