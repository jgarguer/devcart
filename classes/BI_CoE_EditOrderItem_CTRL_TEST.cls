/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Unknown
    Company:       Unknown
    Description:   Test class for BI_CoE_EditOrderItem_CTRL
    
    History:
    
    <Date>            <Author>          <Description>
    Unknown           Unknown   Initial version
    13/07/2016        Jorge Galindo   	Promotion price recalculation functionality added to test
	  05/08/2016		  Jorge Galindo		several changes for meet new object's requirements (Opp, OI, Account)
	  07/10/2016		  Jorge Galindo		Changes for not depending on BI_DataLoad (UAT Problem)
    16/01/2017      Pedro Párraga   Add Method test_method_three() to test method recurrenteAnteriorEditable()
    23/01/2017      Pedro Párraga   Modify Method test_method_three() to test method recurrenteAnteriorEditable()
    17/02/2017      Alvaro Garcia   Created start y stop test
    18/02/2017      Guillermo Muñoz Activate bypass in Data_Load and added start and stop test
    18/08/2017      Guillermo Muñoz     Changed to use new BI_MigrationHelper functionality
    25/09/2017      Jaime Regidor       Fields BI_Subsector__c and BI_Sector__c added

 --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class BI_CoE_EditOrderItem_CTRL_TEST {
    
    public static NE__OrderItem__c ordIt;
    public static NE__OrderItem__c childOrdIt;
    public static Opportunity opp;
    public static NE__Order__c ord;
    public static BI_FVI_OfflineCatalog__c promoObj;
	
    @isTest static void Data_Load() {

        Map <Integer, BI_bypass__c> mapa = BI_MigrationHelper.enableBypass(UserInfo.getUserId(),true, false, false, false);
        //NETriggerHelper.setTriggerFired('NESetDefaultOIFieldsTest');
        BI_MigrationHelper.skipAllTriggers();


        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
        BI_Dataload.set_ByPass(false,false);
        BI_TestUtils.throw_exception=false;
		
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'BI_Caso_Interno' LIMIT 1];
		
        // JGL 07/10/2016
        
        //List<String> lst_pais = BI_DataLoad.loadPaisFromPickListNotBeing(1, new Set <String> {Label.BI_Argentina});
		// copied from BI_DataLoad
        Set<string> cantBe =  new Set <String> {Label.BI_Argentina};
        
        List<String> lst_pais = new List<String>();
        Schema.DescribeFieldResult fieldResult = Opportunity.BI_Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        
        for (Schema.PicklistEntry a : ple ) {
            if(!CantBe.contains(a.getValue()))
              lst_pais.add(a.getValue());
        }
        
        
        
        
		//List<Account> lst_acc = BI_DataLoad.loadAccountsPaisStringRef(1, lst_pais);
		// copied from BI_DataLoad
        
        List<Account> lst_acc = new List<Account>();
        
        
          String segmento;
          String subSegmento; 

          //'Argentina', 'Guatemala', 'Costa Rica'

          if(!lst_pais.isEmpty()){
            if(lst_pais[0] == 'Argentina'){
              segmento = 'Empresas';
              // FAR 10/06/2015: Gobierno y servicios is not a valid value for Argentina
              subSegmento = 'Grandes Clientes';
              //subSegmento = 'Gobierno y servicios'; 
            //}else if(lst_pais[0] == 'Guatemala'){  'Guatemala' AND 'Costa Rica' haven´t values(). Set by defualt (OLD)
            //  segmento = null;
            //  subSegmento = null; 
            //}else if(lst_pais[0] == 'Costa Rica'){
            //  segmento = null;
            //  subSegmento = null; 
            }else if (lst_pais[0] == Label.BI_Peru){
              segmento = 'Empresas';//Ecuador has this values
              subSegmento = 'Platino';
            }else{
              segmento = 'Empresas';//Ecuador has this values
              subSegmento = 'Multinacionales'; 
            }

          }else{
            segmento = 'Empresas';//Ecuador has this values
            subSegmento = 'Multinacionales'; 
          }
       

                for(Integer j = 0; j < 1; j++){
                    Account acc = new Account(Name = 'test'+ String.valueOf(j),
                                              BI_Envio_a_aprobacion_agrupacion_cliente__c = true,
                                              BI_Activo__c = Label.BI_Si,
                                              BI_Country__c = lst_pais[0],
                                              BI_Segment__c= segmento,//'Empresas',
                                              BI_Subsector__c = 'test', //25/09/2017
                                              BI_Sector__c = 'test', //25/09/2017
                                              BI_Subsegment_Local__c = subSegmento,
                                              BI_No_Identificador_fiscal__c = '1234567',
                                              BI_Subsegment_Regional__c = 'test',
                                              BI_Territory__c = 'test');//'Multinacionales');
                    
                    lst_acc.add(acc);
                }
        
        
         insert lst_acc; 	
       	 //update lst_acc; 
        
        // END JGL 07/10/2016
       
        
       

        	NE__Catalog__c cat = new NE__Catalog__c(name='Catalog Test');
			insert cat;
        
        
        
        	opp = new Opportunity(Name = 'Test',
												  CloseDate = Date.today(),
												  BI_Requiere_contrato__c = true,
												  Generate_Acuerdo_Marco__c = false,
												  AccountId = lst_acc[0].id,
												  StageName = 'F2 - Negotiation',
												  BI_Ultrarapida__c = true,
												  BI_Duracion_del_contrato_Meses__c = 3,
												  BI_Recurrente_bruto_mensual__c = 100.12,
												  BI_Ingreso_por_unica_vez__c = 12.1,
												  BI_Recurrente_bruto_mensual_anterior__c = 1,
												  BI_Opportunity_Type__c = 'Móvil',
												  Amount = 23,
												  BI_Country__c = lst_pais[0],
                                 				  BI_Licitacion__c = 'No');
											
			insert opp;
		
      //Test.startTest();

        	NE__Product__c pord = new NE__Product__c();
			pord.Name = 'INFOINTERNET';
        	pord.Offer_SubFamily__c = 'a-test;';
			pord.Offer_Family__c = 'b-test';
			
			insert pord;

        	//JGL 13/07/2016
			NE__Product__c childProd = new NE__Product__c();
			childProd.Offer_SubFamily__c = 'a-test;';
			childProd.Offer_Family__c = 'b-test';
            childProd.Payback_Family__c = 'Terminales';
			
			insert childProd;
        
        	NE__Product__c childProdPlan = new NE__Product__c();
			childProdPlan.Offer_SubFamily__c = 'a-test;';
			childProdPlan.Offer_Family__c = 'b-test';
            childProdPlan.Payback_Family__c = 'Planes';
			
			insert childProdPlan; 
        
        
            //END JGL 13/07/2016
			
			System.debug('BI_Buttons_TEST->updateProjectOppTest-> pord: '+ pord);
			
		
			NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = pord.Id, NE__Change_Subtype__c = 'test', NE__Disconnect_Subtype__c = 'test');
			insert catIt;
			
        	//JGL 13/07/2016
        	NE__Catalog_Item__c catItChild = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = childProd.Id, NE__Change_Subtype__c = 'test', NE__Disconnect_Subtype__c = 'test',NE__Base_OneTime_Fee__c = 10,NE__Type__c ='Child-Product');
			insert catItChild;
        
            NE__Catalog_Item__c catItChildPlan = new NE__Catalog_Item__c(NE__Catalog_Id__c = cat.Id, NE__ProductId__c = childProdPlan.Id, NE__Change_Subtype__c = 'test', NE__Disconnect_Subtype__c = 'test',NE__Base_OneTime_Fee__c = 10);
			insert catItChildPlan;
        
        	promoObj = new BI_FVI_OfflineCatalog__c(
                BI_FVI_RootProduct__c = pord.Id,
                BI_FVI_P1Product__c = childProdPlan.Id,
                BI_FVI_P2Product__c = childProd.id,
                BI_FVI_P1NRC__c = 20,
                BI_FVI_P2NRC__c = 40,
                BI_FVI_GenStartDate__c = Date.newInstance(2016, 1, 11), 
                BI_FVI_GenEndDate__c = Date.newInstance(2016, 12, 31)
    		);
        
        	insert promoObj;
            //END JGL 13/07/2016
            
			ord = new NE__Order__c(NE__AccountId__c = lst_acc[0].Id,
												NE__BillAccId__c = lst_acc[0].Id,
												NE__OptyId__c = opp.Id,
												NE__AssetEnterpriseId__c='testEnterprise',
												NE__ServAccId__c = lst_acc[0].Id,
												NE__OrderStatus__c = Label.BI_F2Negociacion);
												
				
			insert ord;

			ordIt = new NE__OrderItem__c(
				NE__OrderId__c = ord.Id,
				NE__Account__c = lst_acc[0].id,
				NE__ProdId__c = pord.Id,
				NE__Qty__c = 6,
				NE__CatalogItem__c = catIt.Id,
                //JGL 13/07/2016
                BI_FVI_PromotionCode__c = promoObj.id
			);
			insert ordIt;

        	//JGL 13/07/2016
			childOrdIt = new NE__OrderItem__c(
				NE__OrderId__c = ord.Id,
				NE__Account__c = lst_acc[0].id,
				NE__ProdId__c = childProd.Id,
				NE__Qty__c = 1,
				NE__CatalogItem__c = catItChild.Id,
                NE__Parent_Order_Item__c = ordIt.Id,
                NE__OneTimeFeeOv__c = 10,
                NE__One_Time_Cost__c = 1
			);
			insert childOrdIt;
        
        	NE__OrderItem__c childOrdItPlan = new NE__OrderItem__c(
				NE__OrderId__c = ord.Id,
				NE__Account__c = lst_acc[0].id,
				NE__ProdId__c = childProdPlan.Id,
				NE__Qty__c = 1,
				NE__CatalogItem__c = catItChild.Id,
                NE__Parent_Order_Item__c = ordIt.Id,
                NE__OneTimeFeeOv__c = 15,
                NE__One_Time_Cost__c = 1
			);
			insert childOrdItPlan;
        
      //Test.stopTest();
        
            //END JGL 13/07/2016
      BI_MigrationHelper.disableBypass(mapa);
      BI_MigrationHelper.cleanSkippedTriggers();
        
    }
    
        
    //JGL 13/07/2016
    @isTest static void test_method_two() {
		
			Data_Load();
        
			Test.startTest();
        	NETriggerHelper.setTriggerFired('BI_FVI_AutoSplit'); 
			PageReference page = new PageReference('/id=' + ordIt.Id);
			Test.setCurrentPage(page);
			BI_CoE_EditOrderItem_CTRL obj = new BI_CoE_EditOrderItem_CTRL(new ApexPages.StandardController(ordIt));			
         	obj.promotionSelectionEnabled = true;
        	obj.fillOfferId();
        	obj.save();
          	obj.updatePricesToOriginal(childOrdIt);
        
        	//END JGL 13/07/2016
      Test.stopTest();

	}
    
    
    
    
	@isTest static void test_method_one() {
		
			Data_Load();
        
			Test.startTest();
            NETriggerHelper.setTriggerFired('BI_FVI_AutoSplit'); 
			PageReference page = new PageReference('/id=' + ordIt.Id);
			Test.setCurrentPage(page);
			BI_CoE_EditOrderItem_CTRL obj = new BI_CoE_EditOrderItem_CTRL(new ApexPages.StandardController(ordIt));			
			obj.fillOfferId();
        	obj.getCostLayout();
			obj.redirect();
			obj.cancel();
        	obj.lst_OptyToUpd.add(opp);
        	obj.lst_ORToUpd.add(ord);
        	obj.saveAndNew();
        
			Test.stopTest();

	}


    @isTest static void test_method_three() {
    
      Data_Load();

      ordIt.NE__Parent_Order_Item__c = null;
      ordIt.BI_FVI_PromotionCode__c = null;
      update ordIt;
        
      Test.startTest();
            NETriggerHelper.setTriggerFired('BI_FVI_AutoSplit'); 
      PageReference page = new PageReference('/id=' + ordIt.Id);
      Test.setCurrentPage(page);
      BI_CoE_EditOrderItem_CTRL obj = new BI_CoE_EditOrderItem_CTRL(new ApexPages.StandardController(ordIt));     
      obj.fillOfferId();
          obj.getCostLayout();
      obj.redirect();
      obj.cancel();
          obj.lst_OptyToUpd.add(opp);
          obj.lst_ORToUpd.add(ord);
          obj.saveAndNew();
        
      Test.stopTest();

  }
		
}