@isTest
public class DashboardControllerTest {
    static testMethod void doLoadTest(){
        User u = TGS_Dummy_Test_Data.dummyUserTGS('TGS System Administrator');
        u.BI_Permisos__c = 'TGS';
        
        insert u;
        
        System.runAs(u){
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
            
            //1-SELECT RecordType.Name theName, COUNT(Id) theCount FROM Case WHERE RecordType.Name IN ('Order Management Case', 'Billing Inquiry', 'Complaint') AND OwnerId IN :ownerIds GROUP BY RecordType.Name 
            //create users to assign to Queues 'SMC Madrid','SMC Miami'
            List<Group> groupList = [SELECT Name FROM Group WHERE Type = 'Queue' AND DeveloperName LIKE 'TGS_SMC%' AND (NOT Name LIKE '%Technical%')];
            List<GroupMember>gmList=new List<GroupMember>();
            GroupMember gm;
            for(Group g:groupList){
                gm=new GroupMember();
                gm.GroupId=g.Id;
                gm.UserOrGroupId=u.Id;
                gmList.add(gm);
            }
            if(!gmList.isEmpty()) {
                System.debug('Group Member List is ' + GmList);
                insert gmList;
            }
            
            Contact CaseContact = TGS_Dummy_Test_Data.dummyContactTGS('Parker');
            Test.startTest();
            insert CaseContact;
            Case testCase=TGS_Dummy_Test_Data.dummyCaseTGS('Order_Management_Case',  'On Behalf');
            testCase.TGS_Assignee__c=u.id;
            testCase.OwnerId=groupList[0].Id;
            testCase.ContactId = CaseContact.id;
            testCase.AccountId = CaseContact.AccountId;
            insert testCase;
            testCase=TGS_Dummy_Test_Data.dummyCaseTGS('Order_Management_Case',  'On Behalf');
            testCase.TGS_Assignee__c=u.id;
            testCase.OwnerId=groupList[0].Id;
            testCase.TGS_SLA_Light__c='Green';
            testCase.ContactId = CaseContact.id;
            testCase.AccountId = CaseContact.AccountId;
            insert TestCase;
            testCase=TGS_Dummy_Test_Data.dummyCaseTGS('TGS_Billing_Inquiry',  'On Behalf');
            testCase.TGS_Assignee__c=u.id;
            TestCase.ContactId = CaseContact.id;
            TestCase.AccountId = CaseContact.AccountId;
            testCase.OwnerId=groupList[0].Id;
            insert TestCase;
            testCase=TGS_Dummy_Test_Data.dummyCaseTGS('TGS_Complaint',  'On Behalf');
            testCase.TGS_Assignee__c=u.id;
            TestCase.ContactId = CaseContact.id;
            TestCase.AccountId = CaseContact.AccountId;
            testCase.OwnerId=groupList[0].Id;
            insert TestCase;
            Test.stopTest();
            
            PageReference p = Page.DashboardVideowall;
            Test.setCurrentPage(p);
            DashboardController contr = new DashboardController();
            for(SelectOption opt:contr.listaSMCs){
                DashboardController.getCasesByType(opt.getValue());
                DashboardController.getCasesBySLA(opt.getValue());
                DashboardController.getOrphanCasesByType(opt.getValue());
            }
            //do test on resultList
        
        }
    }
}