public without sharing class CWP_ServiceTracking_TicketsPedidos_det {
    
    @AuraEnabled
    public static String getOrderItemFromCaseId(String caseid) {
        String item = '';
        Case aux =
            [select TGS_Customer_Services__c, TGS_Customer_Services__r.Name from Case where CaseNumber = :caseid];
        if (aux.TGS_Customer_Services__c != null){
            item = aux.TGS_Customer_Services__r.Name;
        }
        return item;
    }  
    
    @AuraEnabled
    public static NE__OrderItem__c getOrderItem(String id) {
        NE__OrderItem__c item = 
            [select 
             NE__ProdId__r.TGS_CWP_Tier_1__c,
             NE__ProdId__r.TGS_CWP_Tier_2__c,
             NE__ProdId__r.Name,
             NE__CatalogItem__r.NE__Catalog_Category_Name__r.NE__Parent_Category_Name__r.Name, 
             NE__CatalogItem__r.NE__Catalog_Category_Name__r.Name,
             NE__ProdName__c, NE__Service_Account__r.Name, NE__Billing_Account__r.Name,
             TGS_Service_status__c, NE__OrderId__r.Site__c, NE__Account__r.TGS_Aux_Holding__r.Name, NE__Account__r.Name,
             TGS_RFS_date__c, TGS_Provisional_RFS_date__c, TGS_Billing_end_date__c, Name from NE__OrderItem__c where Name = :id];
        return item;
    }
    
    @AuraEnabled
    public static String getAccountHierarchy(String id) {
        NE__OrderItem__c orig = 
            [select NE__Billing_Account__r.Name, NE__Billing_Account__r.ParentId from NE__OrderItem__c where Name = :id];
        String hierarchy = orig.NE__Billing_Account__r.Name;
        Id aux = orig.NE__Billing_Account__r.ParentId;
        while (aux != null){
            Account auxWhile =
                [select ParentId, Name from Account where id = :aux];
            hierarchy = auxWhile.Name + ' > ' + hierarchy;
            aux = auxWhile.ParentId;
        }        
        return hierarchy;
    }
    
    @AuraEnabled
    
    public static OiWrapper getDetailsAttributes(String id) {
        
        List<NE__Order_Item_Attribute__c[]> ret = new List<NE__Order_Item_Attribute__c[]>();
        Map<String,List<NE__Order_Item_Attribute__c>> oiAttsGroupByFamilyId=new Map<String,List<NE__Order_Item_Attribute__c>>();
        NE__Order_Item_Attribute__c[] items = 
            [SELECT 
             Name,
             NE__Order_Item__c,
             TGS_Status__c,
             TGS_Account_Id__c,
             TGS_Type_Order__c,
             NE__FamPropId__r.NE__FamilyId__c,
             NE__FamPropId__r.NE__FamilyId__r.Name,
             TGS_Key_Attribute__c,
             TGS_CSUID__c,
             NE__FamPropId__r.NE__PropId__r.NE__Description__c,
             NE__Value__c
             FROM
             NE__Order_Item_Attribute__c
             WHERE 
             TGS_Order_Item_Name__c =: id AND NE__Value__c!=null AND NE__FamPropId__r.TGS_Hidden_in_CWP__c=:false];
        for(NE__Order_Item_Attribute__c i : items){
            String fmKey=i.NE__FamPropId__r.NE__FamilyId__r.Name;
            if(oiAttsGroupByFamilyId.containsKey(fmKey))
                oiAttsGroupByFamilyId.get(fmKey).add(i);
            else {
                List<NE__Order_Item_Attribute__c> aux= new List<NE__Order_Item_Attribute__c>();
                aux.add(i);
                oiAttsGroupByFamilyId.put(fmKey, aux);
            }
        }
        OiWrapper retVar= new OiWrapper();        
        // Construimos la estructura que necesitamos en el componente como una matriz. 
        //  1-La primera lista hace referencia a los OIs que se muestran 
        //  2-La segunda es para cada recoger las familias de atributos
        //  3-La tercera hace referencia a los atributos.
        List<List<List<NE__Order_Item_Attribute__c>>> aux1= new List<List<List<NE__Order_Item_Attribute__c>>>();
        List<List<NE__Order_Item_Attribute__c>> aux2= new List<List<NE__Order_Item_Attribute__c>>(); 
        
        for (String field : oiAttsGroupByFamilyId.keySet()){
            aux2.add(oiAttsGroupByFamilyId.get(field));
        }
        aux1.add(aux2);
        retVar.OiList=aux1;
        return retVar;
    }
    
    @AuraEnabled 
    public static Boolean hasSecondLevel(String id) {
        NE__OrderItem__c[] itemId = 
            [SELECT Id FROM NE__OrderItem__c where Name = :id];
        NE__OrderItem__c[] attrs = 
            [SELECT TGS_Product_Name__c, NE__Parent_Order_Item__c FROM NE__OrderItem__c where NE__Parent_Order_Item__c =  :itemId.get(0).Id];
        return attrs.size() > 0; 
    }      
    
    @AuraEnabled
    public static OiWrapper getDetailsAttributes2(String id) {
        
        List<NE__Order_Item_Attribute__c[]> ret = new List<NE__Order_Item_Attribute__c[]>();
        
        NE__OrderItem__c[] names = 
            [SELECT Name FROM NE__OrderItem__c where NE__Parent_Order_Item__r.Name =: id];
        System.debug('@@ La lista de OI es'+ names);
        List<String>ids=new List<String>();
        
        for (NE__OrderItem__c n:names ){
            ids.add(n.Name);
            
        }
        
        
        
        NE__Order_Item_Attribute__c[] items = 
            [SELECT 
             Name,
             TGS_Order_Item_Name__c,
             NE__Order_Item__c,
             NE__Order_Item__r.TGS_Product_Name__c,
             TGS_Status__c,
             TGS_Account_Id__c,
             TGS_Type_Order__c,
             NE__FamPropId__r.NE__FamilyId__c,
             NE__FamPropId__r.NE__FamilyId__r.Name,
             TGS_Key_Attribute__c,
             TGS_CSUID__c,
             NE__FamPropId__r.NE__PropId__r.NE__Description__c,
             NE__Value__c
             FROM
             NE__Order_Item_Attribute__c
             WHERE NE__Value__c!=null AND NE__FamPropId__r.TGS_Hidden_in_CWP__c=:false AND TGS_Order_Item_Name__c IN: ids];
        
        List<List<List<NE__Order_Item_Attribute__c>>> oiList= new List<List<List<NE__Order_Item_Attribute__c>>>();
        List<List<NE__Order_Item_Attribute__c>> familyList= new List<List<NE__Order_Item_Attribute__c>>();
        List<NE__Order_Item_Attribute__c> oiAttList= new List<NE__Order_Item_Attribute__c>();
        
        Map<String,Map<String,List<NE__Order_Item_Attribute__c>>> oiMap=new Map<String,Map<String,List<NE__Order_Item_Attribute__c>>>();
        Map<String,List<NE__Order_Item_Attribute__c>> familyMap=new  Map<String,List<NE__Order_Item_Attribute__c>>();
        for (NE__Order_Item_Attribute__c at : items){
            // comprobamos si ya tenemos ese elemento en el mapa. 
            if(!oiMap.containsKey(at.TGS_Order_Item_Name__c)){
                //Lo insertamos si no existe. 
                oiMap.put(at.TGS_Order_Item_Name__c, new Map<String,List<NE__Order_Item_Attribute__c>>());
            }
            familyMap=oiMap.get(at.TGS_Order_Item_Name__c);
            //Comprobamos si tenemos la categoría
            if (familyMap.containsKey(at.NE__FamPropId__r.NE__FamilyId__r.Name)){
                //Metemos el atributo
                familyMap.get(at.NE__FamPropId__r.NE__FamilyId__r.Name).add(at);
            }else {
                //Inicializamos la lista y metemos el elemento.
                oiAttList= new List<NE__Order_Item_Attribute__c>();
                oiAttList.add(at);
                familyMap.put(at.NE__FamPropId__r.NE__FamilyId__r.Name, oiAttList);
            }
        }
        System.debug('oiMap->>'+ oiMap);
        //Construido el mapa montamos las listas 
        
        OiWrapper retVar= new OiWrapper();
        retVar.OiList=new List<List<List<NE__Order_Item_Attribute__c>>>();
        
        oiList= new List<List<List<NE__Order_Item_Attribute__c>>>();
        for (String oi :oiMap.keySet()){
            familyList= new List<List<NE__Order_Item_Attribute__c>>();
            oiAttList= new List<NE__Order_Item_Attribute__c>();
            for (String family :oiMap.get(oi).keySet()){
                familyList.add(oiMap.get(oi).get(family));
            }
            retVar.OiList.add(familyList); 
        }
        return retVar;
    }   
    
    @AuraEnabled
    public static NE__OrderItem__c[] getProductsNames(String id) {
        System.debug('Recibo id--'+id);
        OiWrapper  c= CWP_ServiceTracking_TicketsPedidos_det.getDetailsAttributes2(id);
        system.debug('@@@@@'+c);
        
        Set<String> se= new Set<String>();
        
        for (List<List<NE__Order_Item_Attribute__c>> l1: c.OiList){
            for (List<NE__Order_Item_Attribute__c> l2: l1){
                for (NE__Order_Item_Attribute__c l3: l2){
                    system.debug('añadimos a'+l3);
                    se.add(l3.TGS_Order_Item_Name__c);
                }
            }
        } 
        NE__OrderItem__c[] names = 
                [SELECT TGS_Product_Name__c, Name FROM NE__OrderItem__c WHERE Name IN: se ORDER BY TGS_Product_Name__c];
        return names;
    }
    @AuraEnabled
    public static OiWrapper getDetailsAttributes2Detailed(String id) {
        system.debug('recibo '+id);
        List<NE__Order_Item_Attribute__c[]> ret = new List<NE__Order_Item_Attribute__c[]>();
        List<String> parts = id.split(',');
        system.debug('parts--> '+parts);
        NE__Order_Item_Attribute__c[] items = 
            [SELECT 
             Name,
             TGS_Order_Item_Name__c,
             NE__Order_Item__c,
             NE__Order_Item__r.TGS_Product_Name__c,
             TGS_Status__c,
             TGS_Account_Id__c,
             TGS_Type_Order__c,
             NE__FamPropId__r.NE__FamilyId__c,
             NE__FamPropId__r.NE__FamilyId__r.Name,
             TGS_Key_Attribute__c,
             TGS_CSUID__c,
             NE__FamPropId__r.NE__PropId__r.NE__Description__c,
             NE__Value__c
             FROM
             NE__Order_Item_Attribute__c
             WHERE NE__Value__c!=null AND NE__FamPropId__r.TGS_Hidden_in_CWP__c=:false AND TGS_Order_Item_Name__c IN: parts];
        
        List<List<List<NE__Order_Item_Attribute__c>>> oiList= new List<List<List<NE__Order_Item_Attribute__c>>>();
        List<List<NE__Order_Item_Attribute__c>> familyList= new List<List<NE__Order_Item_Attribute__c>>();
        List<NE__Order_Item_Attribute__c> oiAttList= new List<NE__Order_Item_Attribute__c>();
        
        Map<String,Map<String,List<NE__Order_Item_Attribute__c>>> oiMap=new Map<String,Map<String,List<NE__Order_Item_Attribute__c>>>();
        Map<String,List<NE__Order_Item_Attribute__c>> familyMap=new  Map<String,List<NE__Order_Item_Attribute__c>>();
        for (NE__Order_Item_Attribute__c at : items){
            // comprobamos si ya tenemos ese elemento en el mapa. 
            if(!oiMap.containsKey(at.TGS_Order_Item_Name__c)){
                //Lo insertamos si no existe. 
                oiMap.put(at.TGS_Order_Item_Name__c, new Map<String,List<NE__Order_Item_Attribute__c>>());
            }
            familyMap=oiMap.get(at.TGS_Order_Item_Name__c);
            //Comprobamos si tenemos la categoría
            if (familyMap.containsKey(at.NE__FamPropId__r.NE__FamilyId__r.Name)){
                //Metemos el atributo
                familyMap.get(at.NE__FamPropId__r.NE__FamilyId__r.Name).add(at);
            }else {
                //Inicializamos la lista y metemos el elemento.
                oiAttList= new List<NE__Order_Item_Attribute__c>();
                oiAttList.add(at);
                familyMap.put(at.NE__FamPropId__r.NE__FamilyId__r.Name, oiAttList);
            }
        }
        System.debug('oiMap->>'+ oiMap);
        //Construido el mapa montamos las listas 
        
        OiWrapper retVar= new OiWrapper();
        retVar.OiList=new List<List<List<NE__Order_Item_Attribute__c>>>();
        
        oiList= new List<List<List<NE__Order_Item_Attribute__c>>>();
        for (String oi :oiMap.keySet()){
            familyList= new List<List<NE__Order_Item_Attribute__c>>();
            oiAttList= new List<NE__Order_Item_Attribute__c>();
            for (String family :oiMap.get(oi).keySet()){
                familyList.add(oiMap.get(oi).get(family));
            }
            retVar.OiList.add(familyList); 
        }
        return retVar;
    }   
    
    /**
* Class:            oiWrappe
*
* Description:      Clase wrapper para los resultados de la tabla del detalle de MyService.  
*/
    
    public class OiWrapper{
        @AuraEnabled
        public List<List<List<NE__Order_Item_Attribute__c>>> OiList {get; set;}
        
        
    }
}