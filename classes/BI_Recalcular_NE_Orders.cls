public without sharing class BI_Recalcular_NE_Orders implements Queueable {

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:        Micah Burgos  
Company:       Aborda.es
Description:   Queueable class that cancel preOpp if Campaings

IN:
	Set<Id> setId_param -> if is null, search all NE__OrderItem__c where BI_Recalcular_valores_economicos__c = true 

History: 

<Date>                  <Author>                <Change Description>
13/05/2015             Micah Burgos          	Initial Version     
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public Set<Id> setId;
    //public Integer option;
    final Integer MAX_RECORDS = 200;

    Set<Id> set_id_records_to_process;
    Set<Id> set_id_records_to_future_process;
    //Set<Id> setId_campToEmail;

    //final static Map<String,Id> MAP_NAME_RT = new Map<String,Id>();
    
    //static{
    //    for(RecordType rt :[SELECT Id, DeveloperName FROM RecordType WHERE sObjectType ='Opportunity']){
    //        MAP_NAME_RT.put(rt.DeveloperName, rt.Id);
    //    }
    //}

    public BI_Recalcular_NE_Orders(Set<Id> setId_param) {
        setId = setId_param;
        //option = option_param;
        //system.debug('CONSTRUCTOR : setId_campToEmail_param: ' + setId_campToEmail_param);
        //Set<Id> aux = setId_campToEmail_param;
        //system.debug('CONSTRUCTOR : aux: ' + setId_campToEmail_param);
        //setId_campToEmail = aux;
        system.debug('CONSTRUCTOR : setId_param: ' + setId_param);

        if(setId_param == null){//Change CampaignId to Opp Ids.
            List<NE__OrderItem__c> lst_Orders = [SELECT ID FROM NE__OrderItem__c WHERE BI_Recalcular_valores_economicos__c = true];
            setId = new Set<Id>();
            for(NE__OrderItem__c opp_id :lst_Orders){
                setId.add(opp_id.Id);
            }
        }        

        Map<Integer,Set<Id>> map_records = splitArray(setId, MAX_RECORDS);

        set_id_records_to_process = map_records.get(0);
        set_id_records_to_future_process = map_records.get(1);
    }

    public void execute(QueueableContext context) {
    	try{
    		List<NE__OrderItem__c> lst_ordItem = [SELECT Id, NE__OneTimeFeeOv__c, NE__RecurringChargeOv__c, CurrencyISOCode, NE__OrderId__c  FROM NE__OrderItem__c WHERE Id IN :set_id_records_to_process];
            Set<Id> set_ordID = new Set<Id>();
            for(NE__OrderItem__c ordIt :lst_ordItem){
                set_ordID.add(ordIt.NE__OrderId__c);
            }

            Map<Id,NE__order__c> map_orders = new Map<Id,NE__order__c>([SELECT Id, NE__One_Time_Fee_Total__c, NE__Recurring_Charge_Total__c, CurrencyISOCode, 
                                                                                (SELECT Id,NE__Qty__c, NE__OneTimeFeeOv__c, NE__RecurringChargeOv__c, CurrencyISOCode, NE__OrderId__c  
                                                                                FROM NE__Order_Items__r )  
                                                                        FROM NE__Order__c WHERE Id IN :set_ordID]);

    		List<NE__OrderItem__c> lst_ordItem_UPDATE= new List<NE__OrderItem__c>(); 
            for(NE__order__c ord :map_orders.values()){
                ord.NE__One_Time_Fee_Total__c = 0;
                ord.NE__Recurring_Charge_Total__c = 0;

                for(NE__OrderItem__c orItem :ord.NE__Order_Items__r){
                    if(orItem.NE__Qty__c == null){
                        orItem.NE__Qty__c = 1;
                    }

                    if(orItem.NE__OneTimeFeeOv__c != null){
                        ord.NE__One_Time_Fee_Total__c += orItem.NE__Qty__c * (BI_CurrencyHelper.convertCurrency(orItem.CurrencyISOCode, ord.CurrencyISOCode, Double.valueOf(orItem.NE__OneTimeFeeOv__c)));
                    }

                    if(orItem.NE__RecurringChargeOv__c != null){
                        ord.NE__Recurring_Charge_Total__c += orItem.NE__Qty__c * (BI_CurrencyHelper.convertCurrency(orItem.CurrencyISOCode, ord.CurrencyISOCode, Double.valueOf(orItem.NE__RecurringChargeOv__c)));
                    }
                    orItem.BI_Recalcular_valores_economicos__c = false;

                    lst_ordItem_UPDATE.add(orItem);
                }
                ord.BI_Recalcular_valores_economicos_Opp__C = true;
            }
            
            update map_orders.values();
            update lst_ordItem_UPDATE;
        
        }catch (exception  Exc){
            Id log_id = BI_LogHelper.generate_BILog('BI_Recalcular_NE_Orders', 'BI_EN', Exc, 'Trigger - Queueable');
        }

       
        system.debug('START ***RECALL***');

        try{
            BI_bypass__c bypass = BI_bypass__c.getInstance();
            if(!set_id_records_to_future_process.isEmpty() && !bypass.BI_stop_job__c){
                system.debug('RECALL : set_id_records_to_future_process: ' + set_id_records_to_future_process);
                System.enqueueJob(new BI_Recalcular_NE_Orders(set_id_records_to_future_process));
            }
            //else{
            //    List<EmailTemplate> lst_tmpl = [SELECT DeveloperName,Id,Name FROM EmailTemplate WHERE DeveloperName = 'BI_End_Cancel_Preopp_campaign'];
            //    if(!lst_tmpl.isEmpty() && setId_campToEmail != null && !setId_campToEmail.isEmpty()){


            //        Contact dummyContact = new Contact(LastName = 'noreply', Email = 'noreply@telefonica.com');
            //        List<Messaging.SingleEmailMessage> lst_mails = new List<Messaging.SingleEmailMessage>();
                    
            //        insert dummyContact;

            //        system.debug('QUERY : setId_campToEmail: ' + setId_campToEmail);
            //        List<Campaign> lst_campEmail = [SELECT Id, OwnerId ,Owner.Email, LastModifiedById, LastModifiedBy.Email FROM Campaign WHERE Id IN :setId_campToEmail];

            //        for(Campaign camp :lst_campEmail){
                        
            //            Messaging.SingleEmailMessage mailOwner = new Messaging.SingleEmailMessage();

            //                mailOwner.setTargetObjectId(dummyContact.Id);
                                                        
            //                mailOwner.setTemplateId(lst_tmpl[0].Id);
            //                mailOwner.setWhatId(camp.Id);  
            //                mailOwner.setReplyTo('micah.burgos@aborda.es');
            //                mailOwner.setSenderDisplayName('Telefónica BI_EN');
            //                mailOwner.setSaveAsActivity(false);

            //                List<String> lst_toBcc = new List<String>();
            //                lst_toBcc.add(camp.LastModifiedBy.Email);
            //                if(camp.LastModifiedById != camp.OwnerId){
            //                    lst_toBcc.add(camp.Owner.Email);
            //                }
            //                mailOwner.setToAddresses(lst_toBcc);

            //                lst_mails.add(mailOwner);
            //        }
            //        //system.debug('lst_mails' +lst_mails);
            //        try{
            //            Messaging.sendEmail(lst_mails);
            //        }catch(exception Exc){
            //            BI_LogHelper.generate_BILog('BI_Recalcular_NE_Orders.sendEmail', 'BI_EN', Exc, 'Trigger - Queueable');
            //        }
                    
            //        delete dummyContact;
            //    }
            //}
        }catch(Exception exc){
             BI_LogHelper.generate_BILog('BI_Recalcular_NE_Orders.recall_queueable', 'BI_EN', Exc, 'Trigger - Queueable');
        }
    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Micah Burgos
    Company:       Aborda
    Description:   Method that split list in 2 blocks, the first block size = max_regs
    
    History: 
    
    <Date>                          <Author>                    <Change Description>
    12/03/2015                      Micah Burgos                Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public Map<Integer,Set<Id>> splitArray(Set<Id> lst_param, Integer max_regs) {

        if(lst_param != null && !lst_param.isEmpty()){
            Map<Integer,Set<Id>> map_return = new Map<Integer,Set<Id>>();
            map_return.put(0,new Set<Id>());
            map_return.put(1,new Set<Id>());

            Integer i = 0;
            for(Id var :lst_param){
                if(i < max_regs){
                    map_return.get(0).add(var);
                }else{
                    map_return.get(1).add(var);
                }
                i++;
            }
            return map_return;
        }else{
            return null;
        }
    }
}