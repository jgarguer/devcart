/*-------------------------------------------------------------------------------------------------------------------------------------------------------
     Author:        Manuel Ochoa
     Company:       New Energy Aborda
     Description:   Class to retrieve order,order items and attributes to create valid Json for bulk pages
     				
     Test Class:    BI_O4_OrderStructureGenerator_TEST
     History:
     
     <Date>            <Author>             <Description>
     27-11-2017			Manuel Ochoa		First version
     01-02-2018 		Manuel Ochoa 		lokup params variables
     26-02-2018			Manuel Ochoa        Added fields for order version
	 13-03-2018			Manuel Ochoa		Variables set to public transient or only transient to reduce view state (listOrderItemAttributes,mapIdListOrderItemsAttr,thisOrderWrapper,mapAuxOrderItemWrapper)
	 30/04/2018         Manuel Ochoa        ALM4396 - added variables to save grid values when editing row
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_OrderStructureGenerator {

	// declaration zone
	public Id idOrder; // id of root order

	public List<NE__OrderItem__c> listOrderItems; // list of OI's related to the root order
    //13-03-2018		Manuel Ochoa		Variables set to public transient or only transient to reduce view state
	public transient List<NE__Order_Item_Attribute__c> listOrderItemAttributes; // list of OIa's related to the root order
	public transient Map<Id,List<NE__Order_Item_Attribute__c>>  mapIdListOrderItemsAttr; // key== id OrderItem. Map of attributes of each OI
	
	public NE__OrderItem__c orderItemAux; // order item to retrieve order data
	public Map<Id,NE__OrderItem__c> orderItemsFirstLevelMap; // root level OI's
	public Map<Id,NE__OrderItem__c> orderItemsSecondLevelMap;// second level OI's	
	public Map<Id,NE__OrderItem__c> orderItemsThirdLevelMap;// third level OI's	
	public Map<Id,NE__OrderItem__c> orderItemsFourthLevelMap;// fourth level OI's

	public Map<Id,Map<String,String>> mapIdProductAttrSeq;
	public Map<Id,NE__PropertyDomain__c> enumeratedPropertiesMap;// Enumerated values of a OIa with 'enumerated' type

	//13-03-2018		Manuel Ochoa		Variables set to public transient or only transient to reduce view state
    public transient OrderWrapper thisOrderWrapper; // Wrapper for this BI_O4_OrderStructureGenerator object
	transient Map<Id,OrderItemWrapper> mapAuxOrderItemWrapper; // aux map of orderItem.id, orderItem
	//public Map<Id,OrderItemWrapper> mapOrderItemRootWrapper;

	public Boolean enableStandardSalesforce; // boolean for custom B2W

	
	/*------------------------------------------------------------
		Author:        Manuel Ochoa
	    Company:       New Energy Aborda
	    Description:   Constructor - Initializes Lists and maps
	    Test Class:    
	    
		History
		<Date>      	<Author>     	<Description>
		27-11-2017		Manuel Ochoa	First version
	------------------------------------------------------------*/
	public BI_O4_OrderStructureGenerator() {
		listOrderItems=new List<NE__OrderItem__c>();
		listOrderItemAttributes = new List<NE__Order_Item_Attribute__c>();
		mapIdListOrderItemsAttr = new Map<Id,List<NE__Order_Item_Attribute__c>>();
	}

	/*------------------------------------------------------------
		Author:        Manuel Ochoa
	    Company:       New Energy Aborda
	    Description:   Stores each order item in a map of each level
	    Test Class:    
	    
		History
		<Date>      	<Author>     	<Description>
		27-11-2017		Manuel Ochoa	First version
	------------------------------------------------------------*/
	public void sortOrderItems(){

		// first pass -- root order items
		for(NE__OrderItem__c orderItem : listOrderItems){

			// create list of attributes for each order item and store in <idOrderItem,orderItemAttr> map
			List<NE__Order_Item_Attribute__c> listAux= new List<NE__Order_Item_Attribute__c>();
			for(NE__Order_Item_Attribute__c orderItemAttr : listOrderItemAttributes){
				if(orderItemAttr.NE__Order_Item__c==orderItem.Id){
					listAux.add(orderItemAttr);
				}
			}
			mapIdListOrderItemsAttr.put(orderItem.Id,listAux);

			if(orderItem.NE__Parent_Order_Item__c==null){
				if(orderItemsFirstLevelMap==null){
					orderItemsFirstLevelMap = new Map<Id,NE__OrderItem__c>();
					orderItemAux=orderitem;
				}
				orderItemsFirstLevelMap.put(orderItem.Id,orderitem);
				//mapOrderItems.put(orderItem.Id,orderitem);
			}
		}
		system.debug('@@@_MOF mapIdListOrderItemsAttr: ' + mapIdListOrderItemsAttr);
		system.debug('@@@_MOF orderItemsFirstLevelMap: ' + orderItemsFirstLevelMap);
		// 2nd pass -- 2nd level order items
		if(orderItemsFirstLevelMap!=null){
			for(NE__OrderItem__c orderItem : listOrderItems){
				if(orderItem.NE__Parent_Order_Item__c!=null && orderItemsFirstLevelMap.containsKey(orderItem.NE__Parent_Order_Item__c)){
					if(orderItemsSecondLevelMap==null){
						orderItemsSecondLevelMap = new Map<Id,NE__OrderItem__c>();
					}
					orderItemsSecondLevelMap.put(orderItem.Id,orderitem);
				}
			}
		}
		system.debug('@@@_MOF orderItemsSecondLevelMap: ' + orderItemsSecondLevelMap);
		// 3rd pass -- 3rd level order items
		if(orderItemsSecondLevelMap!=null){
			for(NE__OrderItem__c orderItem : listOrderItems){
				if(orderItem.NE__Parent_Order_Item__c!=null && orderItemsSecondLevelMap.containsKey(orderItem.NE__Parent_Order_Item__c)){
					if(orderItemsThirdLevelMap==null){
						orderItemsThirdLevelMap = new Map<Id,NE__OrderItem__c>();
					}
					orderItemsThirdLevelMap.put(orderItem.Id,orderitem);
				}
			}
		}
		system.debug('@@@_MOF orderItemsThirdLevelMap: ' + orderItemsThirdLevelMap);
		// 4th pass -- 4th level order items
		if(orderItemsThirdLevelMap!=null){
			for(NE__OrderItem__c orderItem : listOrderItems){
				if(orderItem.NE__Parent_Order_Item__c!=null && orderItemsThirdLevelMap.containsKey(orderItem.NE__Parent_Order_Item__c)){
					if(orderItemsFourthLevelMap==null){
						orderItemsFourthLevelMap = new Map<Id,NE__OrderItem__c>();
					}
					orderItemsFourthLevelMap.put(orderItem.Id,orderitem);
				}
			}
		}
		system.debug('@@@_MOF orderItemsFourthLevelMap: ' + orderItemsFourthLevelMap);

		generateOrderWrapper();

		// clear list of order items
		//listOrderItems.clear();
		//listOrderItemAttributes.clear();
	}

		
	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Retrieve enumerated values for attributes of this type
    Test Class:    
    
	History
	<Date>      	<Author>     	<Description>
	27-11-2017		Manuel Ochoa	First version
	------------------------------------------------------------*/
	public void getEnumeratedPropertyList(){
		List<Id> enumeratedPropertiesIdList = new List<Id>();
		for(NE__Order_Item_Attribute__c oia : listOrderItemAttributes){
			if(oia.NE__FamPropId__r.NE__PropId__r.NE__Type__c == 'Enumerated'){
				enumeratedPropertiesIdList.add(oia.NE__FamPropId__r.NE__PropId__c);
			}
		}
		enumeratedPropertiesMap = new Map<Id,NE__PropertyDomain__c>([select Id,Name,NE__PropId__c,NE__PropId__r.Name  from NE__PropertyDomain__c where NE__PropId__c IN :enumeratedPropertiesIdList]);
	}

	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Generates data for root wrapper and launches chained process to fill wrappers
    Test Class:    
    
	History
	<Date>      	<Author>     	<Description>
	27-11-2017		Manuel Ochoa	First version
	------------------------------------------------------------*/
	public void generateOrderWrapper(){

		// generate order data 
		thisOrderWrapper = new OrderWrapper();
		
		thisOrderWrapper.idOrder = idOrder;
		//26-02-2018 - Manuel Ochoa - Added version field
		thisOrderWrapper.doubVersion = orderItemAux.NE__OrderId__r.NE__Version__c;		
		thisOrderWrapper.strNameOrder = orderItemAux.NE__OrderId__r.Name;
		thisOrderWrapper.idOpty = orderItemAux.NE__OrderId__r.NE__OptyId__c;
		thisOrderWrapper.strNameOpty = orderItemAux.NE__OrderId__r.NE__OptyId__r.Name;
		thisOrderWrapper.idAccount = orderItemAux.NE__OrderId__r.NE__AccountId__c;
		thisOrderWrapper.strNameAccount = orderItemAux.NE__OrderId__r.NE__AccountId__r.Name;
		thisOrderWrapper.idCatalog = orderItemAux.NE__Catalog__c;
		thisOrderWrapper.strNameCatalog = orderItemAux.NE__Catalog__r.Name;
		thisOrderWrapper.idServAcc = orderItemAux.NE__Service_Account__c;
		thisOrderWrapper.idBillAcc = orderItemAux.NE__Billing_Account__c;
		thisOrderWrapper.idHolding = orderItemAux.NE__OrderId__r.HoldingId__c;
		thisOrderWrapper.idAuthUser = orderItemAux.NE__OrderId__r.Authorized_User__c;
		thisOrderWrapper.idSite = orderItemAux.NE__OrderId__r.Site__c;
		thisOrderWrapper.idContract = orderItemAux.NE__OrderId__r.NE__Contract_Account__c;
		thisOrderWrapper.idAsset = orderItemAux.NE__OrderId__r.NE__Asset__c;

		//orderItemAux fields
		// Id,Name,NE__OrderID__c,NE__OrderId__r.Name,NE__OrderId__r.NE__Asset__c,
		//NE__OrderId__r.NE__OptyId__c,NE__OrderId__r.NE__OptyId__r.Name,
		//NE__ProdId__c,NE__CatalogItem__c,NE__Catalog__c,NE__OrderId__r.NE__CatalogId__c,NE__OrderId__r.NE__CatalogId__r.Name,
		//NE__OrderId__R.Id,NE__OrderId__r.NE__AccountId__c,NE__OrderId__r.NE__AccountId__r.Name,
		//NE__OrderId__r.HoldingId__c,NE__OrderId__r.NE__ServAccId__c,NE__OrderId__r.NE__ServAccId__r.Name,NE__OrderId__r.Cost_Center__c,
		//NE__OrderId__r.NE__BillAccId__c,NE__OrderId__r.Site__c,NE__OrderId__r.Authorized_User__c,
		//NE__OrderId__r.NE__Contract_Account__c,NE__OrderId__r.NE__AssetEnterpriseId__c,
		//NE__Service_Account__c,NE__Billing_Account__c
		
		system.debug('@@@_MOF thisOrderWrapper: ' + thisOrderWrapper);

		generateOrderItemWrappers();

		// code for TGS 
		/*row.order.NE__AssetEnterpriseId__c = oia.NE__Order_Item__r.NE__OrderId__r.Id;

		if(isModification==true){
			row.order.NE__Type__c= 'ChangeOrder';
		}else if (isDisconnection==true){
			row.order.NE__Type__c= 'Disconnection';
		} 
		row.order.NE__OrderStatus__c = 'Pending';

		if(isModification==true){
			row.order.ne__configuration_type__c= 'Change';
		}else if (isDisconnection==true){
			row.order.ne__configuration_type__c= 'Disconnect';
		}*/		
	}

	
	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Generates one order item wrapper for each OI and populate Oia map related to this OI
    				Assigns level of hierarchy and configures id's of parent and root OI's
    Test Class:    
    
	History
	<Date>      	<Author>     	<Description>
	27-11-2017		Manuel Ochoa	First version
	30/04/2018      Manuel Ochoa    ALM4396 - set item as no edited by default
	------------------------------------------------------------*/
	public void generateOrderItemWrappers(){

		mapAuxOrderItemWrapper = new Map<Id,OrderItemWrapper>();

		getEnumeratedPropertyList();
		//mapOrderItemRootWrapper = new Map<Id,OrderItemWrapper>();
		// generate order items wrapper list
		for(Id idOrderItem : orderItemsFirstLevelMap.keySet()){
			OrderItemWrapper oiWrapper=new OrderItemWrapper();
			NE__OrderItem__c orderItem=orderItemsFirstLevelMap.get(idOrderItem);
			oiWrapper.strIdOrderItem=idOrderItem;
			//oiWrapper.orderItem=orderItemsFirstLevelMap.get(idOrderItem);
			oiWrapper.strNameOrderItem=orderItem.Name;
			oiWrapper.strProductName=orderItem.NE__ProdId__r.Name;
			oiWrapper.strIdCatalogItem=orderItem.NE__CatalogItem__c;
			oiWrapper.decQty=orderItem.NE__Qty__c;
			oiWrapper.bRoot=true;
			oiWrapper.intLevel=0;
            System.debug(mapIdProductAttrSeq);
            System.debug('generateOrderItemWrappers--idOrderItem-'+mapIdListOrderItemsAttr.get(idOrderItem)+'orderItem.NE__ProdId__c'+mapIdProductAttrSeq.get(orderItem.NE__ProdId__c));
			oiWrapper.oiaMap=generateOrderItemAttrWrappers(mapIdListOrderItemsAttr.get(idOrderItem),mapIdProductAttrSeq.get(orderItem.NE__ProdId__c));
			mapAuxOrderItemWrapper.put(idOrderItem,oiWrapper);
			//mapOrderItemRootWrapper.put(idOrderItem,oiWrapper);
			// 30/04/2018  -  Manuel Ochoa - ALM4396 - set item as no edited by default
			oiWrapper.bEdited = false;
		}

		if(orderItemsSecondLevelMap!=null){
			for(Id idOrderItem : orderItemsSecondLevelMap.keySet()){
				OrderItemWrapper oiWrapper=new OrderItemWrapper();
				NE__OrderItem__c orderItem=orderItemsSecondLevelMap.get(idOrderItem);
				oiWrapper.strIdOrderItem=idOrderItem;
				//oiWrapper.orderItem=orderItemsSecondLevelMap.get(idOrderItem);
				oiWrapper.strNameOrderItem=orderItem.Name;
				oiWrapper.strProductName=orderItem.NE__ProdId__r.Name;
				oiWrapper.strIdCatalogItem=orderItem.NE__CatalogItem__c;
				oiWrapper.decQty=orderItem.NE__Qty__c;
				oiWrapper.bRoot=false;
				oiWrapper.intLevel=1;
				oiWrapper.strRootId=orderItem.NE__Root_Order_Item__c;
				oiWrapper.strParentId=orderItem.NE__Parent_Order_Item__c;
                System.debug('generateOrderItemWrappers--orderItem-ProdID-'+orderItem.NE__ProdId__c);
				oiWrapper.oiaMap=generateOrderItemAttrWrappers(mapIdListOrderItemsAttr.get(idOrderItem),mapIdProductAttrSeq.get(orderItem.NE__ProdId__c));
				mapAuxOrderItemWrapper.put(idOrderItem,oiWrapper);
                // 30/04/2018  -  Manuel Ochoa - ALM4396 - set item as no edited by default
				oiWrapper.bEdited = false;
			}
		}
		if(orderItemsThirdLevelMap!=null){
			for(Id idOrderItem : orderItemsThirdLevelMap.keySet()){
				OrderItemWrapper oiWrapper=new OrderItemWrapper();
				NE__OrderItem__c orderItem=orderItemsThirdLevelMap.get(idOrderItem);
				oiWrapper.strIdOrderItem=idOrderItem;
				//oiWrapper.orderItem=orderItemsThirdLevelMap.get(idOrderItem);
				oiWrapper.strNameOrderItem=orderItem.Name;
				oiWrapper.strProductName=orderItem.NE__ProdId__r.Name;
				oiWrapper.strIdCatalogItem=orderItem.NE__CatalogItem__c;
				oiWrapper.decQty=orderItem.NE__Qty__c;
				oiWrapper.bRoot=false;
				oiWrapper.intLevel=2;
				oiWrapper.strRootId=orderItem.NE__Root_Order_Item__c;
				oiWrapper.strParentId=orderItem.NE__Parent_Order_Item__c;
				oiWrapper.oiaMap=generateOrderItemAttrWrappers(mapIdListOrderItemsAttr.get(idOrderItem),mapIdProductAttrSeq.get(orderItem.NE__ProdId__c));
				mapAuxOrderItemWrapper.put(idOrderItem,oiWrapper);
                // 30/04/2018  -  Manuel Ochoa - ALM4396 - set item as no edited by default
				oiWrapper.bEdited = false;
			}
		}
		if(orderItemsFourthLevelMap!=null){
			system.debug(orderItemsFourthLevelMap.keySet());
			for(Id idOrderItem : orderItemsFourthLevelMap.keySet()){
				OrderItemWrapper oiWrapper=new OrderItemWrapper();
				NE__OrderItem__c orderItem=orderItemsFourthLevelMap.get(idOrderItem);

				oiWrapper.strIdOrderItem=idOrderItem;
				//oiWrapper.orderItem=orderItemsFourthLevelMap.get(idOrderItem);
				oiWrapper.strNameOrderItem=orderItem.Name;
				oiWrapper.strProductName=orderItem.NE__ProdId__r.Name;
				oiWrapper.strIdCatalogItem=orderItem.NE__CatalogItem__c;
				oiWrapper.decQty=orderItem.NE__Qty__c;
				oiWrapper.bRoot=false;
				oiWrapper.intLevel=3;
				oiWrapper.strRootId=orderItem.NE__Root_Order_Item__c;
				oiWrapper.strParentId=orderItem.NE__Parent_Order_Item__c;
				oiWrapper.oiaMap=generateOrderItemAttrWrappers(mapIdListOrderItemsAttr.get(idOrderItem),mapIdProductAttrSeq.get(orderItem.NE__ProdId__c));
				mapAuxOrderItemWrapper.put(idOrderItem,oiWrapper);
                // 30/04/2018  -  Manuel Ochoa - ALM4396 - set item as no edited by default
				oiWrapper.bEdited = false;
			}
		}

		system.debug('@@@_MOF mapAuxOrderItemWrapper: ' + mapAuxOrderItemWrapper);

		linkOrderItemWrappers();
		debugWrapper();			
	}

	
	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Generates a map of order item attributes for each OI
    
    Test Class:    
    
	History
	<Date>      	<Author>     	<Description>
	27-11-2017		Manuel Ochoa	First version
	01-02-2018		Manuel Ochoa	Added code for new lookup window params
	25-02-2018		Manuel Ochoa	Added default value for attribute
	30/04/2018      Manuel Ochoa    ALM4396 - set item as no edited by default
	------------------------------------------------------------*/
	public Map<String, OrderItemAttributeWrapper> generateOrderItemAttrWrappers(List<NE__Order_Item_Attribute__c> listOia,Map<String,String> mapAttrSequence){
		Map<String, OrderItemAttributeWrapper> oiaMap = new Map<String, OrderItemAttributeWrapper>();				

		for(NE__Order_Item_Attribute__c oiaAux : listOia){
			OrderItemAttributeWrapper oiaWrapper=new OrderItemAttributeWrapper();			

			oiaWrapper.strIdOrderItemAttr=oiaAux.Id;
			if(oiaAux.NE__FamPropId__r.NE__Required__c == 'Yes'){
				oiaWrapper.required = true;
				oiaWrapper.selected = true;
			}else{
				oiaWrapper.required = false;
				oiaWrapper.selected = false;
			}
			//25-02-2018		Manuel Ochoa	Added default value for attribute
			oiaWrapper.defaultValue=oiaAux.NE__FamPropId__r.NE__defaultValue__c;
			oiaWrapper.attribute = oiaAux.NE__FamPropId__r.NE__PropId__r.Name;
			if(oiaAux.NE__FamPropId__r.NE__PropId__r.NE__Type__c==null){
				oiaWrapper.xType='String';
			}else{
				oiaWrapper.xType = oiaAux.NE__FamPropId__r.NE__PropId__r.NE__Type__c;
			}

			oiaWrapper.xType = oiaAux.NE__FamPropId__r.NE__PropId__r.NE__Type__c;
			if(oiaWrapper.xType=='Dynamic Lookup'){
				oiaWrapper.strIdDynLookUp = oiaAux.Id;
				oiaWrapper.strNameDynLookUp = oiaAux.NE__FamPropId__r.NE__Dynamic_Lookup__c;
			}else{
				oiaWrapper.strIdDynLookUp = 'false';
				oiaWrapper.strNameDynLookUp = 'false';
			}

			


			oiaWrapper.family = oiaAux.NE__FamPropId__r.NE__FamilyId__r.Name;
			oiaWrapper.strSequence=mapAttrSequence.get(oiaWrapper.attribute);

			if(oiaWrapper.xType == 'Enumerated'){

				List<String> auxValues = new List<String>();
				for(NE__PropertyDomain__c enumProp : enumeratedPropertiesMap.values()){
					if(enumProp.NE__PropId__c == oiaAux.NE__FamPropId__r.NE__PropId__c){
						auxValues.add(enumProp.Name);
					}
				}
				oiaWrapper.enumeratedValues = auxValues;
			}
			oiaWrapper.value = oiaAux.NE__value__c;

			// 01-02-2018 Manuel Ochoa
			oiaWrapper.accId=oiaAux.NE__Order_Item__r.NE__Account__c;
			oiaWrapper.famPropId=oiaAux.NE__FamPropId__c;
			oiaWrapper.productId=oiaAux.NE__Order_Item__r.NE__ProdId__c;
			oiaWrapper.categoryId=oiaAux.NE__Order_Item__r.NE__CatalogItem__r.NE__Catalog_Category_Name__c;
			oiaWrapper.catItemId=oiaAux.NE__Order_Item__r.NE__CatalogItem__c;
			// 01-02-2018
			 
			// 30/04/2018  -  Manuel Ochoa - ALM4396 - set item as no edited by default
			oiaWrapper.bEdited = false;

			oiaMap.put(oiaAux.id,oiaWrapper);
		}

		// loop oiaMap to link DynLookUp fields by family
		for(String oiaWrapperId : oiaMap.keySet()){
			OrderItemAttributeWrapper oiaWrapper=oiaMap.get(oiaWrapperId);
			// if type is DynLookUp loop again to search all the family
			if(oiaWrapper.xType=='Dynamic Lookup'){
				oiaWrapper.mapDynLookUpValues = new Map<String,String>();
				oiaWrapper.mapDynLookUpValues.put(oiaWrapper.attribute,oiaWrapper.value);
				for(String oiaWrapperIdAux : oiaMap.keySet()){
					OrderItemAttributeWrapper oiaWrapperAux=oiaMap.get(oiaWrapperIdAux);
					if(oiaWrapper.family==oiaWrapperAux.family && oiaWrapperId!=oiaWrapperIdAux){
						oiaWrapperAux.strIdDynLookUp=oiaWrapperId;
						//oiaWrapperAux.xType='DynLookUpField';
						oiaWrapper.mapDynLookUpValues.put(oiaWrapperAux.attribute,oiaWrapperAux.value);
					}
				}
			}
		}

		return oiaMap;
	}

	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Link OI's with his parent OI - Inserts OI in the parent's child OI's map
    				Finally generates list of OI's root items.    
    Test Class:    
    
	History
	<Date>      	<Author>     	<Description>
	27-11-2017		Manuel Ochoa	First version
	------------------------------------------------------------*/
	public void linkOrderItemWrappers(){

		//mapOrderItemRootWrapper = new Map<Id,OrderItemWrapper>();
		List<String> strIdRootOiWrapper	= new List<String>();	

		// fill OiWrapper Maps
		for(string strKeyOiWrapper :mapAuxOrderItemWrapper.keyset()){
			system.debug('@@@_MOF strKeyOiWrapper: ' + strKeyOiWrapper);
			OrderItemWrapper oiWrapper=mapAuxOrderItemWrapper.get(strKeyOiWrapper);
			system.debug('@@@_MOF oiWrapper: ' + oiWrapper);
			if(oiWrapper.bRoot){
				strIdRootOiWrapper.add(strKeyOiWrapper);
			}
			if(mapAuxOrderItemWrapper.containsKey(oiWrapper.strParentId)){
				if(mapAuxOrderItemWrapper.get(oiWrapper.strParentId).oiChildMap==null){
					mapAuxOrderItemWrapper.get(oiWrapper.strParentId).oiChildMap = new Map<String, OrderItemWrapper>();
				}
				mapAuxOrderItemWrapper.get(oiWrapper.strParentId).oiChildMap.put(oiWrapper.strIdOrderItem,oiWrapper);
			}
		}
		system.debug('@@@_MOF strIdRootOiWrapper: ' + strIdRootOiWrapper);
		system.debug('@@@_MOF thisOrderWrapper: ' + thisOrderWrapper);
		// extract root OiWrapper map
		for(string strIdRootOi : strIdRootOiWrapper){
			if(thisOrderWrapper.oiRootList==null){
				thisOrderWrapper.oiRootList = new List<OrderItemWrapper>();
			}
			thisOrderWrapper.oiRootList.add(mapAuxOrderItemWrapper.get(strIdRootOi));
		}

		system.debug('@@@_MOF thisOrderWrapper: ' + thisOrderWrapper);

	}

	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Debug of Order Wrapper - Deactivate for test   
    Test Class:    
    
	History
	<Date>      	<Author>     	<Description>
	27-11-2017		Manuel Ochoa	First version
	------------------------------------------------------------*/
	public void debugWrapper(){
		system.debug('Order Wrapper: ' + JSON.serialize(thisOrderWrapper));
	}

	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Root wrapper Order data and list with root Oi's
    Test Class:    
    
	History
	<Date>      <Author>     <Description>
	26-02-2018 - Manuel Ochoa - Added version field
	------------------------------------------------------------*/
	public class OrderWrapper {
		//public NE__Order__c order{get;set;}
		public String idOpty {get;set;} // opty ID
		public String strNameOpty {get;set;} // opty name
		public String idOrder {get;set;} // order ID
		public String strNameOrder {get;set;} // order name
		//26-02-2018 - Manuel Ochoa - Added version field
		public Double doubVersion {get;set;} // version of order
		public String idCatalog {get;set;} // catalog ID
		public String strNameCatalog {get;set;} // catalog name
		public String idAccount {get;set;} // account ID
		public String strNameAccount {get;set;} // account name
		public String idServAcc  {get;set;} // service account ID
		public String idBillAcc  {get;set;} // billing account ID
		public String idHolding  {get;set;} // holding account ID
		public String idAuthUser  {get;set;} // authenticated user ID
		public String idSite  {get;set;} // site ID
		public String idContract  {get;set;} // Contract ID
		public String idAsset  {get;set;} // asset order ID

		public List<OrderItemWrapper> oiRootList {get;set;} // list of every root order item
	}

	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Order Item wrapper - stores Oi data, map of attributes and map of child Oi's
    Test Class:    
    
	History
	<Date>      	<Author>     	<Description>
	27-11-2017		Manuel Ochoa	First version
	30/04/2018      Manuel Ochoa    ALM4396 - variables to save grid values when editing row
	------------------------------------------------------------*/
	public class OrderItemWrapper {
		public String strIdOrderItem {get;set;} // order item ID
		public String strNameOrderItem {get;set;} // order item name
		public integer intLevel {get;set;} // order item level
		public String strProductName {get;set;} // product name
		public String strIdCatalogItem {get;set;} // catalog item name
		public Decimal decQty {get;set;} // product quantity
		public boolean bRoot {get;set;} // boolean, true if is root order item
		public String strRootId {get;set;} // Root order item id
		public String strParentId {get;set;} // Parent order item ID
		//public NE__OrderItem__c orderItem{get;set;}	
		public Map<String, OrderItemAttributeWrapper> oiaMap{get;set;} // map of attributes wrappers
		public Map<String, OrderItemWrapper> oiChildMap {get;set;} // map of child order items wrappers - same as this
        // 30/04/2018  -  Manuel Ochoa - ALM4396 - variables to save grid values when editing row
		public boolean bEdited {get;set;} // boolean. True if value is edited in grid
	}

	/*------------------------------------------------------------
	Author:        Manuel Ochoa
    Company:       New Energy Aborda
    Description:   Order Item Attribute wrapper - stores Oia data, and possible enumerated values
    Test Class:    
    
	History
	<Date>      	<Author>     	<Description>
	27-11-2017		Manuel Ochoa	First version
	01-02-2018 		Manuel Ochoa	Added variables for dynamic lookups
	25-02-2018 		Manuel Ochoa 	attribute added default value field
	30/04/2018      Manuel Ochoa    ALM4396 - variables to save grid values when editing row
	------------------------------------------------------------*/
	public class OrderItemAttributeWrapper {
		public String strIdOrderItemAttr {get;set;} // oia ID
		public Boolean selected {get;set;}
		public String attribute {get;set;} // attribute name
		public String family {get;set;} // family ID
		public String value {get;set;} // value
		public Boolean required {get;set;} // check if attr is required
		//25-02-2018 - Manuel Ochoa - added default value field
		public String defaultValue {get;set;} // default value
		public String strSequence {get;set;} // sort sequence code
		public String xType {get;set;} // attribute type
		public String strIdDynLookUp {get;set;} // id of lookup if applicable 
		public String strNameDynLookUp {get;set;} // name of lookup if applicable
		public Map<String,String> mapDynLookUpValues {get;set;} // output values of lookup
		public List<String> enumeratedValues {get;set;} // values for enumerated list if applicable
		// 01-02-2018 Manuel Ochoa - lokup params variables
		public String accId {get;set;} // account ID
		public String famPropId {get;set;}	// Family property ID
		public String productId {get;set;} // product ID
		public String categoryId {get;set;} // category ID
		public String catItemId {get;set;} // catalog item ID
		// 01-02-2018
		// 30/04/2018  -  Manuel Ochoa - ALM4396 - variables to save grid values when editing row
		public boolean bEdited {get;set;} // boolean. True if value is edited in grid
	}
}