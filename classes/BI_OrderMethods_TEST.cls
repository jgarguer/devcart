@isTest
private class BI_OrderMethods_TEST {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Antonio Pardo
	    Company:       Aborda
	    Description:   Method that manage the code coverage of BI_OrderMethods.setOrderVersion() and BI_OrderMethods.setOrderStatus
	    
	    History: 
	    
	    <Date>                          <Author>                    <Change Description>
	    13/04/2016                      Antonio Pardo               Initial version
	    26/09/2016						Ricardo Pereira				NullPointerException Fixed
	    20/09/2017        			Angel F. Santaliestra        	Add the mandatory field on account creation: BI_Territory__c
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	@isTest static void setOrderVersionStatus_test() {
		
		//User BI
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
		//Create Account
		List<Account> lst_acc = BI_O4_DisconnectionMethods_TEST.loadAccounts(1, new List<String>{'Argentina'});

		//Create Opportunity
		Opportunity opp = new Opportunity(
			Name = 'Test',
            CloseDate = Date.today(),
            StageName = Label.BI_F5DefSolucion,
            AccountId = lst_acc[0].Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Opportunity_Type__c = 'Alta',
            BI_Country__c = 'Argentina'
            );
		Insert opp;

														///////INSERT ORDERS/////////
		////////////////////////////////////////////////////	First Insert 	/////////////////////////////////////////////////////////
		NE__Order__c order1 = new NE__Order__c(
			NE__OptyId__c = opp.Id
		);
		Insert order1;
		//Comprobar Version =  1 y Status = Active
		NE__Order__c orderaux1 = [SELECT Id, NE__Version__c, NE__OrderStatus__c FROM NE__Order__c WHERE NE__OptyId__c = :opp.Id LIMIT 1];
		//System.debug('1º: Order1: Version -> ' + orderaux1.NE__Version__c + ' Active -> ' + orderaux1.NE__OrderStatus__c);
		//Asserts 1º order1
		System.assertEquals(orderaux1.NE__Version__c, 1);
		System.assertEquals(orderaux1.NE__OrderStatus__c, Label.BI_Active);
		
		///////////////////////////////////////////////// 	Second Insert 	////////////////////////////////////////////////////////
		NE__Order__c order2 = new NE__Order__c(
			NE__OptyId__c = opp.Id
		);
		Insert order2;

		//Comprobar order1 cambió Version = 1 y Status = Revised
		//			order2 		  Version = 2 y Status = Active
		List<NE__Order__c> lst_orderAux = [SELECT Id, NE__Version__c, NE__OrderStatus__c FROM NE__Order__c WHERE NE__OptyId__c = :opp.Id ORDER BY Id LIMIT 2];
		/*System.debug('2º: Order1: Version -> ' + lst_orderAux[0].NE__Version__c + ' Active -> ' + lst_orderAux[0].NE__OrderStatus__c);
		System.debug('2º: Order2: Version -> ' + lst_orderAux[1].NE__Version__c + ' Active -> ' + lst_orderAux[1].NE__OrderStatus__c);*/
		//Asserts 2º order1
		System.assertEquals(lst_orderAux[0].NE__Version__c, 1);
		System.assertEquals(lst_orderAux[0].NE__OrderStatus__c, 'Revised');
		//Asserts 2º order2
		System.assertEquals(lst_orderAux[1].NE__Version__c, 2);
		System.assertEquals(lst_orderAux[1].NE__OrderStatus__c, Label.BI_Active);

		///////////////////////////////////////////////// 	Third Insert 	////////////////////////////////////////////////////////
		NE__Order__c order3 = new NE__Order__c(
			NE__OptyId__c = opp.Id
		);
		Insert order3;
		//Comprobar order1 cambió Version = 1 y Status = Revised
		//			order2 cambió Version = 2 y Status = Revised
		//			order3 		  Version = 3 y Status = Active
		lst_orderAux = [SELECT Id, NE__Version__c, NE__OrderStatus__c FROM NE__Order__c WHERE NE__OptyId__c = :opp.Id ORDER BY Id LIMIT 3];
		/*System.debug('3º: Order1: Version -> ' + lst_orderAux[0].NE__Version__c + ' Active -> ' + lst_orderAux[0].NE__OrderStatus__c);
		System.debug('3º: Order2: Version -> ' + lst_orderAux[1].NE__Version__c + ' Active -> ' + lst_orderAux[1].NE__OrderStatus__c);
		System.debug('3º: Order3: Version -> ' + lst_orderAux[2].NE__Version__c + ' Active -> ' + lst_orderAux[2].NE__OrderStatus__c);*/
		//Asserts 3º order1
		System.assertEquals(lst_orderAux[0].NE__Version__c, 1);
		System.assertEquals(lst_orderAux[0].NE__OrderStatus__c, 'Revised');
		//Asserts 3º order2
		System.assertEquals(lst_orderAux[1].NE__Version__c, 2);
		System.assertEquals(lst_orderAux[1].NE__OrderStatus__c, 'Revised');
		//Asserts 3º order3
		System.assertEquals(lst_orderAux[2].NE__Version__c, 3);
		System.assertEquals(lst_orderAux[2].NE__OrderStatus__c, Label.BI_Active);		

	}

	@isTest static void setEditableDiscount_test() {
		
		//User BI
		TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
		
		//Create Account
		List<Account> lst_acc = BI_O4_DisconnectionMethods_TEST.loadAccounts(1, new List<String>{'Argentina'});

		//Create Opportunity
		Opportunity opp = new Opportunity(
			Name = 'Test',
            CloseDate = Date.today(),
            StageName = Label.BI_F5DefSolucion,
            AccountId = lst_acc[0].Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Opportunity_Type__c = 'Alta',
            BI_Country__c = 'Argentina'
            );
		Insert opp;
		////////////////////////////////////////////First Insertion with BI_descuento_bloqueado__c = false//////////////////////////////////////////////
		NE__Order__c order1 = new NE__Order__c(
			NE__OptyId__c = opp.Id
		);
		Insert order1;

		System.assertEquals([SELECT BI_descuento_bloqueado__c FROM Opportunity WHERE Id =: opp.Id].BI_descuento_bloqueado__c, false);
		System.assertEquals([SELECT NE__OrderStatus__c FROM NE__Order__c WHERE Id =: order1.Id].NE__OrderStatus__c, Label.BI_Active);
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//change values of the opp to set BI_Descuento_bloqueado__c at true
		opp.BI_Oferta_economica__c = Label.BI_Pendiente_de_aprobacion;
		update opp;
		///////////////////////////////////////////////////////////////////

		////////////////////////////////////////////Second Insertion with BI_descuento_bloqueado__c = true//////////////////////////////////////////////
		NE__Order__c order2 = new NE__Order__c(
			NE__OptyId__c = opp.Id
		);
		Insert order2;

		System.assertEquals([SELECT BI_descuento_bloqueado__c FROM Opportunity WHERE Id =: opp.Id].BI_descuento_bloqueado__c, true);
		System.assertEquals([SELECT NE__OrderStatus__c FROM NE__Order__c WHERE Id =: order2.Id].NE__OrderStatus__c, Label.BI_Order_producto_bloqueado);
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}




	@isTest static void actualizarAccountCoberturaDigital_TEST() {
	
         BI_TestUtils.throw_exception=false;
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    
        Account acc = new Account(
          Name = 'Test1ARG', 
          Industry = 'Comercio', 
          CurrencyIsoCode = 'ARS', 
          BI_Activo__c = 'Sí', 
          BI_Cabecera__c = 'Sí',  
          BI_Estado__c = true, 
          BI_Country__c = Label.BI_Argentina, 
          BI_Sector__c = 'Comercio', 
          BI_Segment__c = 'Empresas', 
          BI_Subsector__c = 'Alimentos y Bebidas', 
          BI_Subsegment_Regional__c = 'Corporate', 
          BI_Territory__c = 'test',
          TGS_Es_MNC__c = false, 
          BI_No_Identificador_fiscal__c = '30111111118',
          Sector__c = 'Private'
        );
        insert acc;
            

      Test.startTest();  
        Opportunity opp = new Opportunity(
            Name = 'OppTest1',
            CloseDate = Date.today(),
            StageName = 'F5 - Solution Definition',
            BI_No_requiere_comite_arg__c = true,
            AccountId = acc.Id,
            BI_Ciclo_ventas__c = Label.BI_Completo,
            BI_Country__c = Label.BI_Argentina,
            BI_No_Identificador_fiscal__c = '30111111118',
            CurrencyIsoCode = 'ARS',
            BI_Licitacion__c = 'No',
            VE_Express__c = true
        );
        insert opp;



        System.debug('updateValPresupuestoOrder test opp'+ opp.Id);
        NE__Order__c ord = new NE__Order__c(NE__AccountId__c = acc.Id,
                                                NE__BillAccId__c = acc.Id,
                                                NE__OptyId__c = opp.Id,
                                                NE__AssetEnterpriseId__c='testEnterprise',
                                                NE__ServAccId__c = acc.Id,
                                                NE__OrderStatus__c = 'Active',
                                                CurrencyIsoCode = 'ARS');
        insert ord;
        System.debug('updateValPresupuestoOrder test ord'+ ord.Id);

        NE__Product__c pord = new NE__Product__c();
         pord.Offer_SubFamily__c = 'a-test;';
         pord.Offer_Family__c = 'b-test';
         pord.BI_Rama_Digital__c = 'asdfasdf';
        
        insert pord; 

        NE__Catalog__c cat = new NE__Catalog__c();
        insert cat;
        NE__Catalog_Item__c catIt = new NE__Catalog_Item__c(
            NE__Catalog_Id__c = cat.Id, 
            NE__ProductId__c = pord.Id, 
            NE__Change_Subtype__c = 'test', 
            NE__Disconnect_Subtype__c = 'test');
        insert catIt;
               
        //Insertamos el order item asociado a la order
        NE__OrderItem__c oi = new NE__OrderItem__c(
            NE__OrderId__c = ord.Id,
            NE__Account__c = acc.Id,
            NE__ProdId__c = pord.Id,
            NE__CatalogItem__c = catIt.Id,
            NE__qty__c = 1,
            NE__OneTimeFeeOv__c = 1000,
            BI_Ingreso_Recurrente_Anterior_Producto__c = 2000,
            NE__RecurringChargeOv__c = 3000,
            CurrencyISOCode = 'ARS'
        );
        
      
        List<NE__OrderItem__c> lst_itemToInsert = new List<NE__OrderItem__c>();
        lst_itemToInsert.add(oi);

        insert lst_itemToInsert;

    Test.stopTest();


        Account micuenta = [Select id, BI_Ultima_Oportunidad_Oferta_Digital__c from Account where Id = :acc.Id];
        System.debug('actualizarAccountCoberturaDigital micuenta' + micuenta);
         System.assertNotEquals(micuenta.BI_Ultima_Oportunidad_Oferta_Digital__c, null);
	}



}