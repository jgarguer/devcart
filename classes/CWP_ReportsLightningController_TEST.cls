/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Everis
    Company:       Everis
    Description:   Controller class for testing CWP_ReportsLightningController
    Test Class:    CWP_ReportsLightningController
    
    History:
     
    <Date>                  <Author>                <Change Description>
    19/04/2017              Everis                    Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class CWP_ReportsLightningController_TEST {

  
    @testSetup 
    private static void dataModelSetup() {  
        
       
        //USER
        UserRole rolUsuario;
        User unUsuario;
        User otroUsuario;
        Profile profileTGS;
        System.RunAs(new User(id=userInfo.getUserId())){
            rolUsuario = CWP_TestDataFactory.createRole('rol 0');
            insert rolUsuario;
            profileTGS = CWP_TestDataFactory.getProfile('TGS System Administrator');
            unUsuario = CWP_TestDataFactory.createUser('nombre0', rolUsuario.id, profileTGS.Id);
            insert unUsuario;
        }
        
        set <string> setStrings = new set <string>{'Account', 'Case', 'NE__Order__c', 'NE__OrderItem__c'};
            
            //ACCOUNT
            map<id, RecordType> rtMap = new map<id, recordType>([SELECT id, DeveloperName FROM RecordType WHERE sObjectType IN: setStrings]); 
        map<string, id> rtMapByName = new map<string, id>();
        for(RecordType i : rtMap.values()){
            rtMapByName.put(i.DeveloperName, i.id);
        }
        
        //User auxUser;
        Account accHolding;         // Level 1
        Account accCustomerCountry; // Level 2 
        Account accLegalEntity;     // Level 3
        Account accBussinesUnit;    // Level 4
        Account accCostCenter;      // Level 5
        System.runAs(new User(id=userInfo.getUserId())){        
            accHolding = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Holding'), 'holding1');
            insert accHolding;
            accCustomerCountry= CWP_TestDataFactory.createCustomerCountry(rtMapByName.get('TGS_Customer_Country'), 'customerCountry1', accHolding.id);
            insert accCustomerCountry;
            accLegalEntity = CWP_TestDataFactory.createHolding(rtMapByName.get('TGS_Legal_Entity'),'legalEntity1');
            accLegalEntity.ParentId = accCustomerCountry.id;
            accLegalEntity.TGS_Aux_Holding__c = accHolding.id;
            insert accLegalEntity;            
            accBussinesUnit = CWP_TestDataFactory.createBussinesUnit(rtMapByName.get('TGS_Business_Unit'), 'BussinesUnit1', accHolding.id, accLegalEntity.id);
            insert accBussinesUnit;            
            accCostCenter = CWP_TestDataFactory.createCostCenter(rtMapByName.get('TGS_Cost_Center'),'CostCenter1', accHolding.id, accBussinesUnit.id, accBussinesUnit.id, accCustomerCountry.id, accLegalEntity.id); 
            insert accCostCenter;
        }            
        
        
        //CONTACT
        Contact contactTest = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest');
        Contact contactTest1 = CWP_TestDataFactory.createContact(accLegalEntity.id, 'nombreDeTest1');
        
        contactTest.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest;
        contactTest1.BI_Cuenta_activa_en_portal__c=accLegalEntity.id;
        insert contactTest1;
        
        User usuario;
        User usuarioNoTGS;
        
        System.runAs(new User(id=userInfo.getUserId())){        
            Profile perfil = CWP_TestDataFactory.getProfile('TGS Customer Community Plus');       
            usuario = CWP_TestDataFactory.createUser('nombre1', null, perfil.id);
            usuario.contactId = contactTest.id;
            insert usuario;
        }
        
        System.runAs(unUsuario){        
            Profile perfil2 = CWP_TestDataFactory.getProfile('BI_Customer Communities');       
            usuarioNoTGS = CWP_TestDataFactory.createUser('nombre3', null, perfil2.id);
            usuarioNoTGS.contactId = contactTest1.id;
            insert usuarioNoTGS;
        }
        
        BI_Portal_local__c portal1 = new BI_Portal_local__c();
        portal1.Name='portal1';
        portal1.BI_Country__c=usuario.Country;
        portal1.BI_Imagen_del_portal__c = '"imagen" "del" "portal"';
        insert portal1;
        
         BI_Portal_local__c portal2 = new BI_Portal_local__c();
        portal2.Name='portal2';
        portal2.BI_Country__c=usuario.Country;
        portal2.BI_Imagen_del_portal__c = '"imagenes" "de" "los" "portales"';
        insert portal2;
        
         BI_ImageHome__c home = new BI_ImageHome__c();
        home.Name='home';
        home.BI_Fecha_Inicio__c=Date.today();
        home.BI_Country__c=null;
        home.BI_Segment__c=accLegalEntity.BI_Segment__c;
        home.BI_Activo__c=true;
        home.CWP_ManagedByTGS__c=true;
        insert home;
        
         Attachment att= new Attachment();
        att.Name = 'att';
        att.ParentId = home.Id;
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        att.Body=bodyBlob;
        insert att;
        


}
    
    @isTest
    private static void getNumTotalResultTest() {     
        system.debug('test 1');
         CWP_ReportsLightningController constructor = new CWP_ReportsLightningController();
        Attachment att =[SELECT id FROM Attachment WHERE Name =: 'att'];
        CWP_ReportsLightningController.AttImg = att;
        BI_ImageHome__c home= [SELECT id FROM BI_ImageHome__c WHERE Name =: 'home'];
        CWP_ReportsLightningController.ImgBack=home;
        CWP_ReportsLightningController.RelationImage=home;
        CWP_ReportsLightningController.RelationImageMovi=home;
        CWP_ReportsLightningController.RelationImageTel=home;
        CWP_ReportsLightningController.tokenApex = 'token';    
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        
  
        System.runAs(usuario){                          
            Test.startTest();                      
            CWP_ReportsLightningController.getNumTotalResult();   
            Test.stopTest();
        }       
        
    } 
    
    @isTest
    private static void getReportListTest() {     
        system.debug('test 2');
         CWP_ReportsLightningController constructor = new CWP_ReportsLightningController();
        Attachment att =[SELECT id FROM Attachment WHERE Name =: 'att'];
        CWP_ReportsLightningController.AttImg = att;
        BI_ImageHome__c home= [SELECT id FROM BI_ImageHome__c WHERE Name =: 'home'];
        CWP_ReportsLightningController.ImgBack=home;
        CWP_ReportsLightningController.RelationImage=home;
        CWP_ReportsLightningController.RelationImageMovi=home;
        CWP_ReportsLightningController.RelationImageTel=home;
        CWP_ReportsLightningController.tokenApex = 'token';    
        User usuario = [SELECT id FROM User WHERE FirstName =: 'nombre1'];  
        
        //List<String> reports = new List<String>{'00Ow0000006PI27EAG', '00Ow0000006PI2BEAW', '00Ow0000006PI2DEAW', '00Ow0000006PI2EEAW', '00Ow0000006PI2FEAW', '00Ow0000006PI2HEAW', '00Ow0000006PI2IEAW', '00Ow0000006PI2JEAW', '00Ow0000006nV0rEAE'};
        System.runAs(usuario){  
            List<Report> repList = [SELECT Id FROM Report LIMIT 5];
            if(!repList.isEmpty()){
                List<String> reports = new List<String>();
                for(Report r:repList){
                    reports.add(r.Id);
                }
                Test.startTest();                      
                CWP_ReportsLightningController.getReportList( 0,5, reports, 'LastModifiedDate', true);  
                Test.stopTest();
            }
        }       
        
    } 



}