public class BIIN_Detalle_Incidencia_Ctrl extends BIIN_BaseController
{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Ignacio Morales Rodríguez, Jose María Martín Castaño
	Company:       Deloitte
	Description:   Clase para ver el detalle del caso a través de Remedy
	    
	History:
	    
	<Date>            <Author>        							  	            <Description>
	10/12/2015        Ignacio Morales Rodríguez, Jose María Martín     		 	Initial version
    07/06/2016        Julio Laplaza                                             Error Handling
    04/07/2016        José Luis González Beltrán                                Fix bad status set in actualizarEstado() & change substatus when cancel ticket
    22/07/2016        José Luis González Beltrán                                Fix retrieve Case Owner, User or Gruop
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public BIIN_Obtener_Ticket_WS.ObtenerTicketRespuesta respuesta {get; set;} 
    public List<BIIN_Obtener_Ticket_WS.Worklog> lworklog{get; set;} 
    public List<BIIN_Obtener_Ticket_WS.SLA> lsla{get; set;} 
    public case caso {get; set;}
    public case q {get; set;}
    public String  casoNota{get; set;}   
    public boolean displayPopup {get; set;} 
    public integer ContAdj {get; set;}
    public boolean YaAdjuntado {get; set;}
    public boolean MasDeTres {get; set;}
    public List <String> ListaNombres {get; set;} 
    public boolean displayPopup2 {get; set;}
    public boolean displayPopup3{get; set;}
    public List<Case> lcases {get;set;}
    public String accountName{get; set;}
    public String nombreContact{get; set;} 
    public String nombreOwner{get; set;}
    public Boolean displayPopUp4 {get; set;} 
    public Boolean displayPopUp5 {get; set;} 
    public Boolean displayPopUp6 {get; set;} 
    public Boolean displayPopUp7 {get; set;} 
    public Boolean displayPopUp8 {get; set;} 
    public Boolean displayPopUp9 {get; set;}
    public String rutaBmc {get; set;}    
    public String rutaBase {get; set;}
    public boolean ModificarConfidencial {get; set;}
    public string GuardarCasoNota{get;set;}
    public case MostrarNumeroCasoPadre{get;set;}
    public String worklogId{get;set;}
    public String posicionWI{get;set;}
    public String attachName {get; set;}
    public String IdAdjunto {get; set;}
    public Attachment attach {get;set;}//BORRAR???
    
    public Transient  Blob adjunto1 {get; set;}
    public String nombre1 {get; set;}
    public Transient  Blob adjunto2 {get; set;}
    public String nombre2 {get; set;}
    public Transient  Blob adjunto3 {get; set;}
    public String nombre3 {get; set;}
    public List <Attachment> ListaAdjuntos2; 
    BIIN_Crear_WorkInfo_WS.CrearWISalida respuestaWI = new BIIN_Crear_WorkInfo_WS.CrearWISalida();
    public Boolean exito=false; 
    public String caseId;
    public Boolean privado {get; set;}

    ApexPages.standardController controller = null;
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:       Ignacio Morales Rodríguez, Jose María Martín Castaño
	    Company:       Deloitte
	    Description:   Constructor, donde se definen las variables globales y se obtienen a traves del WS todos los campos del caso.
	    
	    History:
	    
	    <Date>            <Author>          									<Description>
	    12/12/2015      Ignacio Morales Rodríguez, Jose María Martín Castaño   	Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference doCancel()
    {
        return controller.cancel();
        //return new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
    }

    public BIIN_Detalle_Incidencia_Ctrl(ApexPages.StandardController controller) 
    {
        this.controller = controller;

        try
        {
            GuardarCasoNota = '';
            ModificarConfidencial = false;
            respuesta  = new BIIN_Obtener_Ticket_WS.ObtenerTicketRespuesta();
            lworklog = new List<BIIN_Obtener_Ticket_WS.Worklog>();
            lsla = new List<BIIN_Obtener_Ticket_WS.SLA>();
            lcases = new List<Case>();
            casoNota = '';
            caso = new case();
            q = new case();
            MostrarNumeroCasoPadre = new case();
            attach = new Attachment();
            ContAdj = 0;
            YaAdjuntado = false;
            MasDeTres = false;       
            ListaAdjuntos2  = new List <Attachment> ();    
            ListaNombres  = new List <String> ();
            displayPopUp9 = false;
            displayPopUp8 = false;
            displayPopUp7 = false;
            displayPopUp6 = false;
            displayPopUp5 = false;
            displayPopUp4 = false;
            NamedCredential nc = [SELECT endpoint FROM NamedCredential WHERE developerName = 'BIIN_RoD' LIMIT 1];
            rutaBmc = nc.endpoint;
            
            caseId = System.currentPagereference().getParameters().get('Idcaso');  
            system.debug('idcaso--->' + caseId );
            // ListaAdjuntos = [SELECT Id, Name FROM Attachment WHERE ParentId  =: caseId];

        }
        catch(Exception ex)
        {
            displayPopupGeneric = true;
            errorMessageGeneric = ex.getMessage();
        }
    }

    public void loadData()
    {
        BIIN_Obtener_Token_BAO ot = new BIIN_Obtener_Token_BAO();
        BIIN_Obtener_Ticket_WS otc = new BIIN_Obtener_Ticket_WS();
        String authorizationToken = ''; // ot.obtencionToken();
        
        respuesta = otc.detalleTicket(authorizationToken, caseId);

        if(respuesta!=null)
        {
            caso = respuesta.c;
            q = [SELECT toLabel(Type), BI_Product_Service__c, BI_Line_of_Business__c, ParentId, BI_Confidencial__c, Reason FROM Case Where Id=:caseId];            
            caso.Type = q.Type; 
            system.debug('Traducir Valor -->' + q.Type);
            caso.BI_Product_Service__c = q.BI_Product_Service__c;
            caso.BI_Line_of_Business__c = q.BI_Line_of_Business__c;
            caso.Reason =q.Reason;
            system.debug('BIIN_Site__c --->' + caso.BIIN_Site__c);
            lworklog = respuesta.lw;
            system.debug('Adjuntos traidos -->' + lworklog);
            lsla = respuesta.lsla;
            system.debug('cuenta: '+caso.AccountId);            
            if(q.ParentId !=null){
                caso.ParentId = q.ParentId; 
                MostrarNumeroCasoPadre =  [SELECT caseNumber FROM case WHERE Id =: caso.ParentId LIMIT 1][0];
                system.debug('ParentId: '+ q.ParentId);
            }
            if(caso.AccountId!=null){
                Account account = [SELECT Name FROM Account WHERE Id =: caso.AccountId LIMIT 1];
                system.debug('cuentaName: '+account.Name);
                accountName=account.Name;
            }
            system.debug('contacto: '+caso.ContactId);
            if(caso.ContactId!=null){
                Contact contact = [SELECT FirstName, LastName FROM Contact WHERE Id =: caso.ContactId LIMIT 1];
                nombreContact=contact.FirstName+' '+contact.LastName;
            }
            system.debug('propietario: '+caso.OwnerId);
            if(caso.OwnerId != null)
            {
                List<User> owner = [SELECT FirstName, LastName FROM User WHERE Id =: caso.OwnerId LIMIT 1];
                if(!owner.isEmpty() && String.isNotEmpty(owner.get(0).FirstName) && String.isNotEmpty(owner.get(0).LastName))
                {
                    nombreOwner = owner.get(0).FirstName + ' ' + owner.get(0).LastName;
                }
                else
                {
                    List<Group> gr = [SELECT Name FROM Group WHERE Id =: caso.OwnerId LIMIT 1];
                    if(!gr.isEmpty() && String.isNotEmpty(gr.get(0).Name))
                    {
                        nombreOwner = gr.get(0).Name;
                    }
                }
            }

            if(caso.BIIN_Tiempo_Neto_Apertura__c  !=null){
               caso.BIIN_Tiempo_Neto_Apertura__c = caso.BIIN_Tiempo_Neto_Apertura__c.SetSCale(2);
           }
           system.debug('SEGUIMOS');
       }
       system.debug('caso--->' + caso);
   }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función para actualizar el estado del ticket a cancelado a través de la función actualizarEstado.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     Public void onClickCancelar()
     {
        if(Test.isRunningTest())
        {
            caso.Status='Asignado';   
        } 

        if(caso.Status=='Asignado')
        {
            exito = actualizarEstado('6','Cancelado por el cliente', caso.Status, caso.BI_Status_Reason__c);

            if(exito==true){
               displayPopup4 = true;
               system.debug('El ticket se ha actualizado en SF correctamente');
               }else{
                displayPopup5 = true; 
                system.debug('El ticket no se ha actualizado en SF correctamente');
            }
        }
        else{
            displayPopUp8 = true;
            system.debug('Solo se puede Cancelar un caso cuando está en estado Asignado');
        }
        displayPopup3 = false; 

    }
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función para reabrir un ticket a través de la función actualizarEstado.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public void GuardarNotaReabrir() {//HAY QUE HACER UN UPDATE EN SF
        if(Test.isRunningTest()){
            caso.Status='Resuelto';   
        } 
        
        if(caso.Status=='Resuelto'){
            exito=actualizarEstado('1','Reabierto',caso.Status,caso.BI_Status_Reason__c);
            if(exito==true){//Nombre del estado en RoD
                displayPopUp4 = true;
                system.debug('El ticket se ha actualizado en SF correctamente');
                }else{
                    displayPopUp5 = true;
                    system.debug('El ticket no se ha actualizado en SF correctamente');
                }
            }
            else{  
                displayPopUp7 = true;
                system.debug('Solo se puede Reabrir un caso cuando está en estado Resuelto');
            }
            displayPopup2 = false;

        }


   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Jose María Martín Castaño
	    Company:       Deloitte
	    Description:   Función principal para la actualización del caso.
	    
	    History:
	    
	    <Date>            		<Author>          				<Description>
	    12/12/2015         Jose María Martín Castaño  			Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public Boolean actualizarEstado(String estado, String subestado, String estadoAnterior, String subestadoAnterior)
     {
        caso.Status = estado;
        caso.BI_Status_Reason__c = subestado;

        Integer nIntentos=0;
        String WIDescription='';
        BIIN_Obtener_Token_BAO ot=new BIIN_Obtener_Token_BAO();
        BIIN_Actualizar_Ticket_WS at=new BIIN_Actualizar_Ticket_WS();
        String authorizationToken = ''; // ot.obtencionToken();

        system.debug('Lo que nos llega de ventana: '+casoNota);
        system.debug('El estado anterior es: ' + estadoAnterior);

        if(casoNota!=null)
        {
            WIDescription=casoNota;
        }

        system.debug('Lo que pasamos al WS: '+WIDescription);
        do
        {
            exito = at.actualizarTicket(authorizationToken, caso, WIDescription);
            nIntentos++;
        }
        while((nIntentos<3)&&(exito==false));

        if(exito)
        {
            try
            {
                /* El update del caso viene de RoD
                Case c = new Case( Id = caso.Id,
                    Status = BIIN_UNICA_Utils.translateFromRemedyByID('Estado', Integer.valueOf(estado)),
                    BI_Status_Reason__c = caso.BI_Status_Reason__c);
                update c; 
                */
            }
            catch(DmlException e){
                system.debug('No se ha podido realizar el cambio de estado'); 
            }
        }
        else
        {
            caso.Status = estadoAnterior;
            caso.BI_Status_Reason__c = subestadoAnterior;
        }
        return exito;
    }
    
  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función para Cerrar un ticket a través de la función actualizarEstado.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public void CerrarIncidencia(){
        if(Test.isRunningTest()){
            caso.Status='Resuelto';
        }
        if(caso.Status=='Resuelto'){
            exito=actualizarEstado('5','', caso.Status,caso.BI_Status_Reason__c);
            if(exito==true){
                displayPopUp4 = true;
                system.debug('El ticket se ha actualizado en SF correctamente');
                }else{
                    displayPopUp5 = true;
                    system.debug('El ticket no se ha actualizado en SF correctamente');
                }
            }
            else{
                displayPopUp6 = true;
                system.debug('Solo se puede Cerrar un caso cuando está en estado Resuelto');
            }      
        }

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función para visualizar el detalle de un adjunto asociado al ticket.
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public void getAttBody(){
        String cuerpo;
        String adjId = '';
        
        BIIN_Obtener_Token_BAO ot = new BIIN_Obtener_Token_BAO();
        BIIN_Obtener_Anexo_WS oa = new BIIN_Obtener_Anexo_WS();
        String authorizationToken = ''; // ot.obtencionToken();
        cuerpo = oa.obtenerAnexo(authorizationToken, caso, attachName, worklogId, posicionWI);
        if(String.isNotEmpty(cuerpo))
        {         
            Attachment adjuntoIns = new Attachment();
            
            List<Attachment> adjunto = new List<Attachment>();
            adjunto = [SELECT Name, Body, ParentId, Id FROM Attachment WHERE ParentId=:caso.Id];
            if(adjunto.size() >= 0)
            {
                delete adjunto;
            }

            adjuntoIns.Body     = EncodingUtil.base64Decode(cuerpo);
            adjuntoIns.Name     = attachName;
            adjuntoIns.ParentId = caso.Id;

            if(!Test.isRunningTest())
            {
                insert adjuntoIns;
            }
            
            IdAdjunto = adjuntoIns.Id;
            rutaBase = URL.getSalesforceBaseUrl().toExternalForm();
        }
    } 

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función que crea el work info del caso en Remedy. 
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public void crearWIRoD(){
        system.debug('Es privado: '+privado);
        if(Test.isRunningTest()){
            privado=false;   
        } 
        String viewAccess = (privado ? 'Internal' : 'Public');
        List<BIIN_Crear_WorkInfo_WS.Adjunto> la = new List<BIIN_Crear_WorkInfo_WS.Adjunto>();
        if((caso.Status != 'Cancelado') && (caso.Status != 'Cerrado')){
            if (adjunto1 != null) {
                system.debug('Adjuntamos 1');
                BIIN_Crear_WorkInfo_WS.Adjunto a = new BIIN_Crear_WorkInfo_WS.Adjunto(nombre1,adjunto1);
                la.add(a);
            }
            
            if (adjunto2 != null) {
                system.debug('Adjuntamos 2');
                BIIN_Crear_WorkInfo_WS.Adjunto a = new BIIN_Crear_WorkInfo_WS.Adjunto(nombre2,adjunto2);
                la.add(a);
            }
            
            if (adjunto3 != null) {   
                system.debug('Adjuntamos 3');         
                BIIN_Crear_WorkInfo_WS.Adjunto a = new BIIN_Crear_WorkInfo_WS.Adjunto(nombre3,adjunto3);
                la.add(a);
            } 
            
            if(GuardarCasoNota == '') {
                GuardarCasoNota  = Apexpages.currentPage().getParameters().get('CasoNotaJ');
            }
            BIIN_Obtener_Token_BAO ot=new BIIN_Obtener_Token_BAO();
            BIIN_Crear_WorkInfo_WS cwi=new BIIN_Crear_WorkInfo_WS();
            String authorizationToken = ''; // ot.obtencionToken();
            respuestaWI = cwi.crearWorkInfo(authorizationToken, caso, la, casoNota, viewAccess);
            if(respuestaWI!=null&&respuestaWI.code=='000'){
                displayPopUp = false;
                displayPopUp4 = true;
                system.debug('EXITO: Se ha creado el WI correctamente');
                }else{
                    displayPopUp = false;
                    displayPopUp5 = true;
                    system.debug('ERROR: No se ha creado el WI correctamente');
                }
            //ListaAdjuntos = new List<Attachment>(); 
            ListaAdjuntos2 = new List<Attachment>(); 
            ContAdj = 0;
            YaAdjuntado = false;
            }else{
                system.debug('No se crea en RoD ya que no es un estado válido');
            }
        }

   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función que actualiza el campo "Confidencial" del caso . 
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public void GuardarCampoConfidencial(){
        system.debug('Valor confidencial puesto a --->' + q.BI_Confidencial__c );
        update q;
    }
    
    public void ModificarCampoConfidencial(){
        ModificarConfidencial = true;
    }
    
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Función asociada al botón Aceptar cuando la actualización del Work Info  ha sido satisfactoria. 
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public pageReference Gotomenu(){
        return new PageReference('/500/o');
    }
    
    
   /*-------------------------------------------------------------------------------------------------------------------------------------------------------
	    Author:        Ignacio Morales Rodríguez
	    Company:       Deloitte
	    Description:   Cierre y Apertura  de PopUp. 
	    
	    History:
	    
	    <Date>            <Author>          				<Description>
	    12/12/2015       Ignacio Morales Rodríguez   		Initial version
     --------------------------------------------------------------------------------------------------------------------------------------------------------*/

     public void ClosePopupError(){
        displayPopUp5= false;
    }
    
    public void ClosePopupErrorReabrir(){
        displayPopUp7= false;
    }
    
    public void ClosePopupErrorCerrar(){
        displayPopUp6= false;
    }

    public void ClosePopupErrorCancelar(){
        displayPopUp8 = false;
    }
    
    public void ClosePopupErrorNota(){
        displayPopUp9 = false;
    }
    
    public void closePopup() {        
        displayPopup = false; 
        YaAdjuntado = false;
        //ListaAdjuntos = new List<Attachment>(); 
        ListaAdjuntos2 = new List<Attachment>();
        ContAdj = 0;
    }     

    public void showPopup() {  
        casoNota = '';           
        displayPopup = true;        
    }
    
    public void closePopup2() { 
        displayPopup2 = false;    
    }     
    public void showPopup2() {  
        casoNota = '';            
        displayPopup2 = true;        
    }
    
    public void closePopup3() { 
        displayPopup3 = false;    
    }     
    public void showPopup3() { 
        casoNota = '';            
        displayPopup3 = true;        
    }
    
}