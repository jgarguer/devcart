/**
 * Apex Controller for looking up an SObject via SOSL
 */
public without sharing class CWP_LookupSObjectController
{
    @AuraEnabled
    public static String getCurrentValue(String type, String value){
        String result;
        if (String.isBlank(type))
            result = null;

        ID lookupId = null;
        try {
            lookupId = (ID)value;
        } catch(Exception e) {
            result = null;
        }
        
        
        SObjectType objType = Schema.getGlobalDescribe().get(type);
        System.debug('test: '+lookupId);
        System.debug('objType: '+objType);
        
        if (
            String.isBlank(lookupId) || 
            lookupId == null || 
            objType == null){
            System.debug('test2');
            result = null;
        }else{
            System.debug('test3');
            String nameField = getSobjectNameField(objType);
            String query = 'Select Id, ' + nameField + ' From ' + type + ' Where Id = \'' + lookupId + '\'';
            System.debug('### Query: ' + query);
            List < SObject > oList = Database.query(query);
            if (oList.size() == 0)
                result = null;
            result = (String) oList[0].get(nameField);
            
        }     
        return result;
    }


    /*
     * Returns the "Name" field for a given SObject (e.g. Case has CaseNumber, Account has Name)
     */
    private static String getSobjectNameField(SobjectType sobjType)
    {
        //describes lookup obj and gets its name field
        String nameField = 'Name';
        Schema.DescribeSObjectResult dfrLkp = sobjType.getDescribe();
        for (schema.SObjectField sotype : dfrLkp.fields.getMap().values()) {
            Schema.DescribeFieldResult fieldDescObj = sotype.getDescribe();
            if (fieldDescObj.isNameField()) {
                nameField = fieldDescObj.getName();
                break;
            }
        }
        return nameField;
    }
    /**
     * Aura enabled method to search a specified SObject for a specific string
     */
    @AuraEnabled
    public static List < Result > lookup(String searchString, String sObjectAPIName, String filter, String queryLanguage) {
        System.debug('searchString: ' + searchString);
        System.debug('sObjectAPIName: ' + sObjectAPIName);

        String sanitizedSearchString = String.escapeSingleQuotes(searchString);
        String sanitizedSObjectAPIName = String.escapeSingleQuotes(sObjectAPIName);

        List < Result > results = new List < Result > ();

        String strAux;
        String searchQuery;
        system.debug('filter' + filter);


        if (queryLanguage == 'SOQL') {
            if (String.isNotEmpty(filter)) {
                String.escapeSingleQuotes(filter);
                strAux = 'AND ' + filter + ' Limit 15';
            } else {
                strAux = 'Limit 15';
            }
            // Build our SOQL query
            searchQuery = 'SELECT Id,Name FROM ' + sanitizedSObjectAPIName + ' WHERE Name LIKE \'%' + sanitizedSearchString + '%\' ' +  strAux;
            System.debug('searchQuery: ' + searchQuery);
            // Execute the Query
            List < SObject > searchList = Database.query(searchQuery);
            for (SObject so : searchList) {
                results.add(new Result((String)so.get('Name'), so.Id));
            }

        } else {
            if (String.isNotEmpty(filter)) {
                String.escapeSingleQuotes(filter);
                strAux = '(id,name WHERE ' + filter + ') Limit 15';
            } else {
                strAux = '(id,name) Limit 15';
            }
            // Build our SOSL query
            searchQuery = 'FIND {' + sanitizedSearchString + '*} IN Name FIELDS RETURNING ' + sanitizedSObjectAPIName + strAux;
            System.debug('searchQuery: ' + searchQuery);
            // Execute the Query
            List < List < SObject >> searchList = search.query(searchQuery);
            // Create a list of matches to return
            for (SObject so : searchList[0]) {
                results.add(new Result((String)so.get('Name'), so.Id));
            }
        }

        System.debug('END LookupSObjectController - lookup');
        System.debug('Result size: ' + results.size());
        return results;
    }

    /**
     * Inner class to wrap up an SObject Label and its Id
     */
    public class Result {
        @AuraEnabled public String SObjectLabel {get; set;}
        @AuraEnabled public Id SObjectId {get; set;}

        public Result(String sObjectLabel, Id sObjectId) {
            this.SObjectLabel = sObjectLabel;
            this.SObjectId = sObjectId;
        }
    }
}