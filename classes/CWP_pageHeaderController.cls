public without sharing class CWP_pageHeaderController {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------

Author:        Julio Alberto Asenjo García

Company:       everis

Description:   Controller for CWP_pageHeader. 
History:
<Date>                          <Author>                                <Code>              <Change Description>
27/02/2017                      Julio Alberto Asenjo  García                                 Initial version
20/12/2017						Javier López Andradas					N/A					Added without sharing to class header

--------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    @auraEnabled
    public static  map<string,string> CWP_doInit(){
        Map <String,String> ret= new Map <String,String>();

        /*Calculamos si es TGS */
        Boolean esTGS=false;
        Boolean esCarrier = false;
        
        Id pId = UserInfo.getProfileId();
        Profile[] p = [SELECT Name FROM Profile WHERE Id = :pId];
        if (p.size()>0){
            String pName = p[0].Name;
            if(pName == 'TGS Carrier Customer Community' || pName == 'TGS CWP Carrier Customer Community'){
                esCarrier = true;
            } else {
                esTGS= pName.startsWith('TGS');
            }
            
        }else 
            esTGS= false;
         /*Calculamos El usuario que está ejecutando */
        
        User usuarioPortal = [Select id, SmallPhotoUrl, FullPhotoUrl, name, Usuario_SolarWinds__c, token_SolarWinds__c, CompanyName, Sector__c,  MobilePhone, Usuario_BO__c, pass_BO__c,ContactId,Contact.BI_Cuenta_activa_en_portal__c, AccountId  from User where id=:UserInfo.getUserId()];
        /*Calculamos la lista de posibles cuentas */
        System.debug('@@@@@@$$');
        list<Account> ActAccs = BI_AccountHelper.getAccounts();
        System.debug('@@@@@@'+ ActAccs);
        ret.put('listUsers', JSON.serialize(ActAccs));
        ret.put('usuarioPortal', JSON.serialize(usuarioPortal));
        ret.put('dynamicHeader', JSON.serialize(PCA_ProfileHelper.getPermissionSet()));
        ret.put('esTGS', String.valueOf(esTGS));
        ret.put('esCarrier', String.valueOf(esCarrier));
        ret.put('FullPhotoUrl', String.valueOf(usuarioPortal.FullPhotoUrl));
        return ret;
    }
    
    @AuraEnabled
    public static Boolean redirectToChatter(){
        List<PermissionSetAssignment> permissionsList = [select Assignee.Name, PermissionSet.Name from PermissionSetAssignment where Assignee.Id =: UserInfo.getUserId() order by Assignee.Name, PermissionSet.Name ];
        system.debug('Redirect Permissions List '+permissionsList.size());
        if(permissionsList.size()>0){
            for (Integer a = 0; a < permissionsList.size(); a++){
            	system.debug('names '+permissionsList[a].PermissionSet.Name);
            	if (permissionsList[a].PermissionSet.Name == 'BI_CORE_Jefe_de_Ventas' || permissionsList[a].PermissionSet.Name == 'BI_CORE_Ejecutivo_de_Cliente' || permissionsList[a].PermissionSet.Name == 'ADM_BI_SuperUsuarios'){
                	return true;
            	} 
        	}
        }
        
        return false;
    }
    
    @auraEnabled
    public static map<string,string> getPermissionSet(){
        
        map<string,string> mapVisibility = CWP_ProfileHelper.getPermissionSet();
        system.debug('mapVisibility '+mapVisibility);
        List<User> listPortalUser = [Select id, Pais__c, ContactId, ProfileId, Profile.Name from User where id=:UserInfo.getUserId() limit 1];
        User portalUser = new User(); 
        if(listPortalUser!= null && listPortalUser.size()>0) {
            portalUser = listPortalUser.get(0); 
        }
    
        
        User UserActual = [Select Name, ContactId,Contact.BI_Cuenta_activa_en_portal__c, AccountId FROM User where Id =:UserInfo.getUserId()];
        
        Account SegAccount = [Select Id, Name,BI_Country__c, BI_Segment__c, CWP_OneDriveAddress__c FROM Account Where Id=:UserActual.Contact.BI_Cuenta_activa_en_portal__c];
          /*---------------------
        Start Redirect Documentation Change
        -----------------------*/
        system.debug('link a one drive: ' + SegAccount.CWP_OneDriveAddress__c);
        String oneDriveLink;
        if(portalUser.Profile.Name.containsIgnoreCase('TGS') && mapVisibility.get('pca_documentmanagement') != null && mapVisibility.get('pca_documentmanagement') == 'true'){
            if(SegAccount.CWP_OneDriveAddress__c != null){
                oneDriveLink = SegAccount.CWP_OneDriveAddress__c;
                mapVisibility.put('oneDriveLink', oneDriveLink);
            }else{
                mapVisibility.put('pca_documentmanagement', 'false');
            }
        }
        
        /*---------------------
        End Redirect Documentation Change
        -----------------------*/
                    
        return mapVisibility;
    }
    
    @auraEnabled
    public static User updateUser(Id newAccountId){     
        
        User usuarioPortal = [Select id, SmallPhotoUrl, FullPhotoUrl, name, Usuario_SolarWinds__c, token_SolarWinds__c, CompanyName, Sector__c,  MobilePhone, Usuario_BO__c, pass_BO__c,ContactId,Contact.BI_Cuenta_activa_en_portal__c, AccountId  from User where id=:UserInfo.getUserId()];
        System.debug('viejo usuario: ' + usuarioPortal);
        System.debug('viejo usuario cuenta: ' + usuarioPortal.Contact.BI_Cuenta_activa_en_portal__c);
        
        usuarioPortal.Contact.BI_Cuenta_activa_en_portal__c=newAccountId;
        update usuarioPortal;
        System.debug('nueva cuenta usuarioPortal.Contact.BI_Cuenta_activa_en_portal__c : ' + usuarioPortal.Contact.BI_Cuenta_activa_en_portal__c);
        
        Contact actualContact = [Select BI_Cuenta_activa_en_portal__c from Contact where id=:usuarioPortal.ContactId];
        System.debug('Cuenta activa en el actual contact : ' + actualContact.BI_Cuenta_activa_en_portal__c);
        actualContact.BI_Cuenta_activa_en_portal__c= newAccountId;
        if(!Test.isRunningTest()){
            update actualContact;
        }   
        Contact newContact = [Select BI_Cuenta_activa_en_portal__c from Contact where id=:usuarioPortal.ContactId];
        System.debug('Cuenta activa en el nuevo contact : ' + newContact.BI_Cuenta_activa_en_portal__c);
        
        User usuarioNuevo = [Select id, SmallPhotoUrl, FullPhotoUrl, name, Usuario_SolarWinds__c, token_SolarWinds__c, CompanyName, Sector__c,  MobilePhone, Usuario_BO__c, pass_BO__c,ContactId,Contact.BI_Cuenta_activa_en_portal__c, AccountId  from User where id=:UserInfo.getUserId()];
        System.debug('nuevo usuario: ' + usuarioPortal);
        System.debug('nuevo usuario cuenta: ' + usuarioNuevo.Contact.BI_Cuenta_activa_en_portal__c);
        return  usuarioPortal;   
    }
    
}