@isTest
private class BI_SitesRest_TEST {
	

	//APARTIR


	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test for createSite looking for exception

    
    History:
    
    <Date>            <Author>          <Description>
    12/06/2017   Ignacio G Schuhmacher   Initial version
    20/09/2017        Antonio Mendivil         -Add the mandatory fields on account creation: 
                                                                  BI_Segment__c, BI_Subsegment_Regional__c,BI_Territory__c  
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	static testMethod void test_createSite() {

		BI_TestUtils.throw_exception = false;
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();

		FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType site = new FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType();
		site.address = new FS_TGS_RestWrapperFullStack.addressInfoType();
		site.address.addressNumber = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.floor = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.apartment = new FS_TGS_RestWrapperFullStack.AddrValueType();
		
		site.name = 'UpdateSitesTest_1';
		site.status = FS_TGS_RestWrapperFullStack.SiteStatusType.active;
		site.address.addressName='UpdateSitesTest_1';		
		site.address.addressNumber.value='6'; 
		site.address.country='Spain'; 
		site.address.region='Madrid'; 
		site.address.locality='Madrid'; 
		site.address.floor.value='4'; 
		site.address.apartment.value='B'; 
		site.address.postalCode='50007';

		Account account = createAccount();
		
		restReq.requestURI = '/SiteManagement/v1/customers/123456789/sites/111222333';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		BI_SitesRest.createSite();

		BI_Sede__c address = [SELECT Id, Name, BI_Direccion__c, BI_Numero__c, BI_Country__c, BI_Provincia__c, BI_Localidad__c, BI_Piso__c,
		BI_Apartamento__c, BI_Codigo_postal__c, BI_ID_de_la_sede__c from BI_Sede__c where BI_ID_de_la_sede__c ='111222333' LIMIT 1];
		Account acc = [SELECT Id from Account where BI_Identificador_Externo__c =:account.BI_Identificador_Externo__c LIMIT 1];
		BI_Punto_de_instalacion__c createdSite = [SELECT Id from BI_Punto_de_instalacion__c where BI_Sede__c =:address.Id AND BI_Cliente__c=:acc.Id LIMIT 1];

		if(createdSite.Id != null){
			system.assertEquals(address.Name, site.address.addressName);
			system.assertEquals(address.BI_Direccion__c, site.address.addressName);
			system.assertEquals(address.BI_Numero__c, site.address.addressNumber.value);
			system.assertEquals(address.BI_Country__c, site.address.country);
			system.assertEquals(address.BI_Provincia__c, site.address.region);
			system.assertEquals(address.BI_Localidad__c, site.address.locality);
			system.assertEquals(address.BI_Piso__c, site.address.floor.value);
			system.assertEquals(address.BI_Apartamento__c, site.address.apartment.value);
			system.assertEquals(address.BI_Codigo_postal__c, site.address.postalCode);
		} else {
			system.assert(false);
		}

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test for createSite looking for exception

    
    History:
    
    <Date>            <Author>          <Description>
    12/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	static testMethod void test_createSiteWithOutSiteInUri() {

		BI_TestUtils.throw_exception = false;
		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();

		FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType site = new FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType();
		site.address = new FS_TGS_RestWrapperFullStack.addressInfoType();
		site.address.addressNumber = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.floor = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.apartment = new FS_TGS_RestWrapperFullStack.AddrValueType();
		
		site.name = 'UpdateSitesTest_1';
		site.status = FS_TGS_RestWrapperFullStack.SiteStatusType.active;
		site.address.addressName='UpdateSitesTest_1';		
		site.address.addressNumber.value='6'; 
		site.address.country='Spain'; 
		site.address.region='Madrid'; 
		site.address.locality='Madrid'; 
		site.address.floor.value='4'; 
		site.address.apartment.value='B'; 
		site.address.postalCode='50007';

		Account account = createAccount();
		//RestRequest restReq = new RestRequest();
		restReq.requestURI = '/SiteManagement/v1/customers/123456789/sites/';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		BI_SitesRest.createSite();
		

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test for createSite looking for exception

    
    History:
    
    <Date>            <Author>          <Description>
    12/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	static testMethod void test_createSiteWithOutCustomerInUri() {

		BI_TestUtils.throw_exception = false;
		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();

		FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType site = new FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType();
		site.address = new FS_TGS_RestWrapperFullStack.addressInfoType();
		site.address.addressNumber = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.floor = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.apartment = new FS_TGS_RestWrapperFullStack.AddrValueType();
		
		site.name = 'UpdateSitesTest_1';
		site.status = FS_TGS_RestWrapperFullStack.SiteStatusType.active;
		site.address.addressName='UpdateSitesTest';		
		site.address.addressNumber.value='6'; 
		site.address.country='Spain'; 
		site.address.region='Madrid'; 
		site.address.locality='Madrid'; 
		site.address.floor.value='4'; 
		site.address.apartment.value='B'; 
		site.address.postalCode='50007';

		Account account = createAccount();
		//RestRequest restReq = new RestRequest();
		restReq.requestURI = '/SiteManagement/v1/customers//sites/111222333';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		BI_SitesRest.createSite();


	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test for createSite looking for exception

    
    History:
    
    <Date>            <Author>          <Description>
    12/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static testMethod void test_createSiteWithExistingAddress() {

		BI_TestUtils.throw_exception = false;
		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();

		FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType site = new FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType();
		site.address = new FS_TGS_RestWrapperFullStack.addressInfoType();
		site.address.addressNumber = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.floor = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.apartment = new FS_TGS_RestWrapperFullStack.AddrValueType();
		
		site.address.addressName = 'UpdateSitesTest';		

		Account account = createAccount();
		//RestRequest restReq = new RestRequest();
		createAddress();
		restReq.requestURI = '/SiteManagement/v1/customers/123456789/sites/111222333';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		BI_SitesRest.createSite();

		BI_Sede__c address = [SELECT Id from BI_Sede__c where BI_ID_de_la_sede__c ='111222333' LIMIT 1];

		Account acc = [SELECT Id from Account where BI_Identificador_Externo__c =:account.BI_Identificador_Externo__c LIMIT 1];
		BI_Punto_de_instalacion__c createdSite = [SELECT Id from BI_Punto_de_instalacion__c where BI_Sede__c =:address.Id AND BI_Cliente__c=:acc.Id LIMIT 1];

		system.assert(createdSite.Id != null);
		

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test for createSite looking for exception

    
    History:
    
    <Date>            <Author>          <Description>
    12/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static testMethod void test_createSiteWithBadExistingAddress() {

		BI_TestUtils.throw_exception = false;
		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();

		FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType site = new FS_TGS_RestWrapperFullStack.AssociatedSiteRequestType();
		site.address = new FS_TGS_RestWrapperFullStack.addressInfoType();
		site.address.addressNumber = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.floor = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.apartment = new FS_TGS_RestWrapperFullStack.AddrValueType();
		
		site.address.addressName = 'UpdateSitesTest-bad';		

		Account account = createAccount();
		//RestRequest restReq = new RestRequest();
		createAddress();
		restReq.requestURI = '/SiteManagement/v1/customers/123456789/sites/111222333';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		BI_SitesRest.createSite();

		List<BI_Sede__c> address = [SELECT Id from BI_Sede__c where BI_ID_de_la_sede__c ='111222333' LIMIT 1];

		List<Account> acc = [SELECT Id from Account where BI_Identificador_Externo__c =:account.BI_Identificador_Externo__c LIMIT 1];
		
		List<BI_Punto_de_instalacion__c> createdSite = [SELECT Id from BI_Punto_de_instalacion__c where BI_Sede__c =:address[0].Id AND BI_Cliente__c=:acc[0].Id LIMIT 1];

		system.assert(createdSite.size() <= 0);
		

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test for createSite looking for exception

    
    History:
    
    <Date>            <Author>          <Description>
    12/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static testMethod void test_createSiteWithOutCustomer() {

		BI_TestUtils.throw_exception = false;
		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();

		FS_TGS_RestWrapperFullStack.SiteRequestType site = new FS_TGS_RestWrapperFullStack.SiteRequestType();
		site.address = new FS_TGS_RestWrapperFullStack.addressInfoType();
		site.address.addressNumber = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.floor = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.apartment = new FS_TGS_RestWrapperFullStack.AddrValueType();
		
		site.address.addressName = 'UpdateSitesTest';
		
		createAddress();
		restReq.requestURI = '/SiteManagement/v1/customers/12344321/sites/111222333';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		BI_SitesRest.createRelationshipSite();

		List<BI_Sede__c> address = [SELECT Id, Name, BI_Direccion__c, BI_Numero__c, BI_Country__c, BI_Provincia__c, BI_Localidad__c, BI_Piso__c,
			BI_Apartamento__c, BI_Codigo_postal__c, BI_ID_de_la_sede__c from BI_Sede__c where BI_ID_de_la_sede__c ='111222333' LIMIT 1];

		String externalCustomerId = '12344321';

		List<Account> acc = [SELECT Id from Account where BI_Identificador_Externo__c =:externalCustomerId LIMIT 1];
		String customerId = '';
		String addressId = '';

		if (acc.size() >= 1){
			customerId = acc[0].Id;
		}
		if (address.size() >= 1){
			addressId = address[0].Id;
		}

		List<BI_Punto_de_instalacion__c> createdSite = [SELECT Id from BI_Punto_de_instalacion__c where BI_Sede__c =:addressId AND BI_Cliente__c=:customerId LIMIT 1];

		system.assert(createdSite.size() <= 0);

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test for createSite looking for exception

    
    History:
    
    <Date>            <Author>          <Description>
    12/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
	static testMethod void test_createSiteWithOutAddress() {

		BI_TestUtils.throw_exception = false;
		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();

		FS_TGS_RestWrapperFullStack.SiteRequestType site = new FS_TGS_RestWrapperFullStack.SiteRequestType();
		site.address = new FS_TGS_RestWrapperFullStack.addressInfoType();
		site.address.addressNumber = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.floor = new FS_TGS_RestWrapperFullStack.AddrValueType();
		site.address.apartment = new FS_TGS_RestWrapperFullStack.AddrValueType();
		
		site.address.addressName = 'UpdateSitesTestfail';
		
		restReq.requestURI = '/SiteManagement/v1/customers/123456789/sites/111222334';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		Account account = createAccount();
		BI_SitesRest.createRelationshipSite();
		
		List<BI_Sede__c> address = [SELECT Id, Name, BI_Direccion__c, BI_Numero__c, BI_Country__c, BI_Provincia__c, BI_Localidad__c, BI_Piso__c,
			BI_Apartamento__c, BI_Codigo_postal__c, BI_ID_de_la_sede__c from BI_Sede__c where BI_ID_de_la_sede__c ='111222333' LIMIT 1];

		String externalCustomerId = '12344321';
		String externalSiteId = '111222334';

		List<Account> acc = [SELECT Id from Account where BI_Identificador_Externo__c =:externalCustomerId LIMIT 1];

		String customerId = '';
		String addressId = '';

		if (acc.size() >= 1){
			customerId = acc[0].Id;
		}
		if (address.size() >= 1){
			addressId = address[0].Id;
		}

		List<BI_Punto_de_instalacion__c> createdSite = [SELECT Id from BI_Punto_de_instalacion__c where BI_Sede__c =:addressId AND BI_Cliente__c=:customerId LIMIT 1];

		system.assert(createdSite.size() <= 0);

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test for createSite looking for exception

    
    History:
    
    <Date>            <Author>          <Description>
    12/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static testMethod void test_createRelationshipSite() {

		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		BI_TestUtils.throw_exception = false;
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();
		FS_TGS_RestWrapperFullStack.SiteRequestType site;
		BI_Sede__c address = createAddress();
		Account acc = createAccount();
		
		
		restReq.requestURI = '/SiteManagement/v1/customers/123456789/sites/111222333';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		BI_SitesRest.createRelationshipSite();

		BI_Punto_de_instalacion__c createdSite = [SELECT Id from BI_Punto_de_instalacion__c where BI_Sede__c =: address.Id  AND BI_Cliente__c=: acc.Id  LIMIT 1];
		
		system.assert(createdSite.Id != null);
		

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test for createSite looking for exception

    
    History:
    
    <Date>            <Author>          <Description>
    13/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static testMethod void test_createRelationshipSitewithBadUri() {

		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		BI_TestUtils.throw_exception = false;
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();
		FS_TGS_RestWrapperFullStack.SiteRequestType site;
		BI_Sede__c address = createAddress();
		Account acc = createAccount();
		
		restReq.requestURI = '/SiteManagement/v1/customers//sites/111222333';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;

		BI_SitesRest.createRelationshipSite();

		List<BI_Punto_de_instalacion__c> createdSite = [SELECT Id from BI_Punto_de_instalacion__c where BI_Sede__c =: address.Id  AND BI_Cliente__c=: acc.Id  LIMIT 1];
		
		system.assert(createdSite.size() <= 0);

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test for createSite looking for exception

    
    History:
    
    <Date>            <Author>          <Description>
    13/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static testMethod void test_createRelationshipSitewithOutCustomer() {

		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		BI_TestUtils.throw_exception = false;
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();
		FS_TGS_RestWrapperFullStack.SiteRequestType site;
		BI_Sede__c address = createAddress();

		restReq.requestURI = '/SiteManagement/v1/customers/123456789/sites/111222333';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;
		String accId = '123456789';

		BI_SitesRest.createRelationshipSite();

		List<BI_Punto_de_instalacion__c> createdSite = [SELECT Id from BI_Punto_de_instalacion__c where BI_Sede__c =: address.Id  AND BI_Cliente__c=: accId  LIMIT 1];
		
		system.assert(createdSite.size() <= 0);
		

	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test for createSite looking for exception

    
    History:
    
    <Date>            <Author>          <Description>
    13/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	static testMethod void test_createRelationshipSitewithOutSite() {

		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		BI_TestUtils.throw_exception = false;
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();
		FS_TGS_RestWrapperFullStack.SiteRequestType site;
		BI_Sede__c address = createAddress();
		Account acc = createAccount();
		
		restReq.requestURI = '/SiteManagement/v1/customers/123456789/sites/111222333';
		restReq.requestBody = Blob.valueOf(JSON.serialize(site));
		RestContext.request = restReq;
		String addId = '111222333';

		BI_SitesRest.createRelationshipSite();

		List<BI_Punto_de_instalacion__c> createdSite = [SELECT Id from BI_Punto_de_instalacion__c where BI_Sede__c =: addId  AND BI_Cliente__c=: acc.Id  LIMIT 1];
		
		system.assert(createdSite.size() <= 0);
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test deleteSite

    
    History:
    
    <Date>            <Author>          <Description>
    13/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	/*static testMethod void test_deleteRelationshipSite() {

		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		BI_TestUtils.throw_exception = false;
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();
		
		BI_Sede__c address = createAddress();
		Account acc = createAccount();
		
		restReq.requestURI = '/SiteManagement/v1/customers/123456789/sites/111222333';
		RestContext.request = restReq;
		
		createSite(acc.id, address.id);
		BI_SitesRest.deleteRelationshipSite();

		List<BI_Punto_de_instalacion__c> createdSite = [SELECT Id from BI_Punto_de_instalacion__c where BI_Sede__c =: address.Id  AND BI_Cliente__c=: acc.Id  LIMIT 1];
		
		system.assert(createdSite.isEmpty());
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test deleteSite looking for fail

    
    History:
    
    <Date>            <Author>          <Description>
    13/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	/*static testMethod void test_deleteRelationshipSitewithoutSite() {

		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		BI_TestUtils.throw_exception = false;
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();
		
		BI_Sede__c address = createAddress();
		Account acc = createAccount();
		
		restReq.requestURI = '/SiteManagement/v1/customers/123456789/sites/111222333';
		RestContext.request = restReq;
		BI_SitesRest.deleteRelationshipSite();

		List<BI_Punto_de_instalacion__c> createdSite = [SELECT Id from BI_Punto_de_instalacion__c where BI_Sede__c =: address.Id  AND BI_Cliente__c=: acc.Id  LIMIT 1];
		
		system.assert(createdSite.isEmpty());
		
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Ignacio G Schuhmacher
    Company:       New Energy Aborda
    Description:   Test deleteSite looking for exception

    
    History:
    
    <Date>            <Author>          <Description>
    13/06/2017   Ignacio G Schuhmacher   Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	/*static testMethod void test_deleteRelationshipSiteException() {

		//NECESITO UN CUSTOMER PARA ASOCIAR AL SITE
		BI_TestUtils.throw_exception = false;
		RestRequest restReq = new RestRequest();
		RestContext.response = new RestResponse();
		
		BI_Sede__c address = createAddress();
		Account acc = createAccount();
		
		restReq.requestURI = '/SiteManagement/v1/customers/123456789/sites/';
		RestContext.request = restReq;
		
		createSite(acc.id, address.id);
		BI_SitesRest.deleteRelationshipSite();

		//List<BI_Punto_de_instalacion__c> createdSite = [SELECT Id from BI_Punto_de_instalacion__c where BI_Sede__c =: address.Id  AND BI_Cliente__c=: acc.Id  LIMIT 1];
		
		//system.assert(createdSite.isEmpty());
		
	}*/

	public static void createSite(Id idCustomer, Id idAddress){
		BI_Punto_de_instalacion__c oSite = new BI_Punto_de_instalacion__c();

		oSite.Name = 'TEST';
		oSite.BI_Cliente__c = idCustomer;
		oSite.BI_Sede__c =idAddress;
		insert oSite;
	}
	

	public static Account createAccount(){
		Account oAccount = new Account();
		oAccount.Name = 'TEST';
		oAccount.BI_Identificador_Externo__c = '123456789';
		oAccount.RecordtypeId = TGS_RecordTypes_Util.getRecordTypeId(Account.SObjectType, Constants.RECORD_TYPE_TGS_HOLDING);
		oAccount.BI_Segment__c                         = 'test';
		oAccount.BI_Subsegment_Regional__c             = 'test';
		oAccount.BI_Territory__c                       = 'test';
				insert oAccount;
		return oAccount;
	}

	public static BI_Sede__c createAddress(){
		BI_Sede__c address = new BI_Sede__c();
		address.Name = 'UpdateSitesTest';
		address.BI_Direccion__c = 'UpdateSitesTest';
		address.BI_Numero__c = '1';
		address.BI_Country__c = 'Spain';
		address.BI_Provincia__c = 'Zaragoza';
		address.BI_Localidad__c = 'Zaragoza';
		address.BI_Piso__c = '5';
		address.BI_Apartamento__c = 'A';
		address.BI_Codigo_postal__c = '50006';
		address.BI_ID_de_la_sede__c = '111222333';
		insert address;
		return address;
	}
	
}