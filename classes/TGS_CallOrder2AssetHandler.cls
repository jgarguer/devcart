/*------------------------------------------------------------  
Author:         Ana Cirac   
Company:        Deloitte
Description:    Class that is called from BI_Case to call assetization.
                Before this class existed, there was another trigger
                that contained this code.
History
<Date>          <Author>        <Change Description>
14-oct-2015     Ana Cirac        Initial Version 
06-Oct-2016     JC Terrón        Added try catch block and null pointer avoiding checks.
21-Oct-2016     J.M. Fierro      Added CPU and query optimizations, and removed update inside FOR statement
08-May-2017     Guillermo Muñoz  Changed Order2AssetWithQueuable.order2asset call for Order2AssetWithQueuable.QueableHelper
12-Sep-2017     Guillermo Muñoz  Changed to prevent errors in TGS_CaseAutoClose job execution
------------------------------------------------------------*/
public class TGS_CallOrder2AssetHandler {
    
    static {
        // Load these constants here, and not pollute method flow
        System.debug(LoggingLevel.FINE, '### Loading Constants ###');
        String.isNotBlank(Constants.RECORD_TYPE_ORDER);
        System.debug(LoggingLevel.FINE, '### Constants Loaded ###');

        System.debug(LoggingLevel.FINE, '### Loading RecordTypes ###');
        TGS_RecordTypes_Util.loadRecordTypes(new Schema.SObjectType[] {Case.SObjectType, NE__Order__c.SObjectType, Account.SObjectType});
        System.debug(LoggingLevel.FINE, '### RecordTypes Loaded ###');
    }

    // Save information on the cases being processed
    class ProcessedData extends TGS_ErrorIntegrations_Helper.RequestInfo {
        List<Case> news;
        Map<Id, Case> oldMap;
        List<Case> lst_case2proc;
        List<NE__Order__c> confToUpd;
        List<NE__OrderItem__c> confItemsToUpd;
        Map<String, NE__Order__c> map_orders;
        List<String> lst_comments;
    }

    public static void callOrder2Asset(List<Case> cases, Map<Id,Case> casesOld){
        boolean wasFired            =   NETriggerHelper.getTriggerFired('CallOrder2Asset');
        Map <String,String> map_conf_cas = new Map <String, String>();

        Boolean autoclose; /* Álvaro López 15/02/2018 - AutoClose fix csuids*/

        if(!wasFired) {

            NETriggerHelper.setTriggerFired('CallOrder2Asset');
            Id rtCaseOrdManag = TGS_RecordTypes_Util.getRecordTypeId(Case.SObjectType, Constants.CASE_RTYPE_DEVNAME_ORDER_MNGMNT);
            ProcessedData pd = new ProcessedData();
            pd.news = cases;
            pd.oldMap = casesOld;
            pd.lst_comments = new List<String>();

            list<String> listOfOrdersId =   new list<String>();
            List<Case> lst_case2proc = new List<Case>(); // JMF 21/10/2016 - Only process OrderManagement Cases
            for(Case updCase : cases) 
            {
                // JMF 21/10/2016 - Moved record type filter here to filter cases _before_ performing queries
                if(updCase.RecordTypeId == rtCaseOrdManag) {
                    if(updCase.Order__c != null){
                        listOfOrdersId.add(updCase.Order__c);
                    } else {
                        pd.lst_comments.add('Id: ' + updCase.Id + ' - No Order__c ' + updCase.Order__c);
                    }
                    if(updCase.Asset__c != null) {
                        listOfOrdersId.add(updCase.Asset__c);
                    } else {
                        pd.lst_comments.add('Id: ' + updCase.Id + ' - No Asset__c ' + updCase.Asset__c);
                    }
                    
                    // JMF 25/10/2016 - Only add case once
                    if(updCase.Order__c != null || updCase.Asset__c != null) {
                        lst_case2proc.add(updCase);
                    }
                }
            }
            pd.lst_case2proc = lst_case2proc;


            //List<String> assetsConfGeneratedId           = new List<String>(); // JMF 21/10/2016 - Not used
            List<NE__Order__c> confToUpd                 = new List<NE__Order__c>();
            List<NE__OrderItem__c> confItemsToUpd        = new List<NE__OrderItem__c>();
            Map<String,NE__Order__c> mapOfConfigurations;
            pd.confToUpd = confToUpd;
            pd.confItemsToUpd = confItemsToUpd;

            boolean sendCis = false;
            if(!listOfOrdersId.isEmpty()) {
                mapOfConfigurations = new Map<String,NE__Order__c>([SELECT Owner_Profile__c,NE__OrderStatus__c,NE__Configuration_Type__c, NE__Type__c,
                                                                      NE__ServAccId__c, NE__BillAccId__c, NE__Asset__c, NE__Asset_Configuration__c, 
                                                                             (SELECT NE__ProdId__r.Name,TGS_Service_status__c, NE__Status__c 
                                                                              FROM NE__Order_Items__r)
                                                                      FROM NE__Order__c 
                                                                      WHERE Id IN: listOfOrdersId]);
                pd.map_orders = mapOfConfigurations;


                for(Case updCase : lst_case2proc) 
                {
                    try
                    {
                        NE__Order__c ord = mapOfConfigurations.get(updCase.Order__c);
                        Boolean profileTGS = NETriggerHelper.getTGSTriggerFired(ord.Owner_Profile__c);
                        if(!profileTGS) {
                            continue;
                        }

                        Case oldCase = casesOld.get(updCase.id);
                        system.debug('[trigger.CallOrder2Asset] oldCase.Status: ' + oldCase.Status);
                        system.debug('[trigger.CallOrder2Asset] updCase.Status: ' + updCase.Status);
                        system.debug('[trigger.CallOrder2Asset] updCase.Type: '+updCase.Type);
                        
                        /** Case Status updated to "Resolved" **/
                        if(oldCase.Status != 'Resolved' && updCase.Status == 'Resolved' && updCase.Order__c != null && updCase.Asset__c == null)
                        {
                            // Registration: Create Asset
                            if(updCase.Type == 'New')
                            {
                                system.debug('[trigger.CallOrder2Asset] Call future with order id: ' + updCase.Order__c + ' caseid: ' + updCase.id);
                                String sessionId = UserInfo.getSessionId();
                                //FutureCallOrder2Asset.callOrder2Asset(updCase.Order__c, updCase.id, sessionId);
                                //Order2AssetWithQueuable.order2asset(updCase.Order__c, updCase.id);
                                map_conf_cas.put(updCase.Order__c, updCase.id);
                                autoclose = updCase.TGS_Autoclosed__c; /* Álvaro López 15/02/2018 - AutoClose fix csuids*/
                            }
                        }
                        
                        /** Case Status updated to "Resolved" but Asset already created **/
                        else if(oldCase.Status != 'Resolved' && updCase.Status == 'Resolved' && updCase.Order__c != null && updCase.Asset__c != null)
                        {
                            system.debug('[trigger.CallOrder2Asset] Case Resolved and Asset already created');
                            if(updCase.Type == 'New')
                            {
                                NE.DataMap mp = new NE.DataMap();
                                NE.DataMap.DataMapRequest  dReq = new NE.DataMap.DataMapRequest();
                                dReq.mapName                    = 'Order2Order';
                                dReq.sourceId                   = updCase.Order__c;
                                dReq.updateActive               = true;
                                mp.callDataMap(dReq);
                                
                                NE__Order__c assetToUpd = mapOfConfigurations.get(updCase.Asset__c);
                                assetToUpd.NE__OrderStatus__c       =   'In progress';
                                assetToUpd.NE__Asset__c             =   assetToUpd.NE__Asset_Configuration__c;
                                assetToUpd.NE__Type__c              =   'Asset';                               
                                                            
                                for(NE__OrderItem__c assetItem : assetToUpd.NE__Order_Items__r)
                                {
                                    assetItem.NE__Status__c = 'Active';
                                    assetItem.TGS_Service_status__c = 'Received';
                                    confItemsToUpd.add(assetItem);
                                }
                                
                                confToUpd.add(assetToUpd);
                                sendCis = true; 
                            }
                        }
                        
                        /** Case Status updated to "Closed" **/
                        //JCT 06/10/2016 Added Order__c checking to avoid null pointer exception.
                        else if(oldCase.Status != 'Closed' && updCase.Status == 'Closed'&& updCase.Order__c != null)
                        {
                            // Modification and Termination: Create Asset 
                            if(updCase.Type == 'Change' || updCase.Type == 'Disconnect')
                            {
                                system.debug('[trigger.CallOrder2Asset] Call future with order id: ' + updCase.Order__c + ' caseid: ' + updCase.id);
                                String sessionId = UserInfo.getSessionId();
                                //FutureCallOrder2Asset.callOrder2Asset(updCase.Order__c, updCase.id, sessionId);
                                //Order2AssetWithQueuable.order2asset(updCase.Order__c, updCase.id);
                                map_conf_cas.put(updCase.Order__c, updCase.id);
                                autoclose = updCase.TGS_Autoclosed__c; /* Álvaro López 15/02/2018 - AutoClose fix csuids*/
                            }
                            else
                            {
                                NE__Order__c assetToUpd = mapOfConfigurations.get(updCase.Asset__c);
                                assetToUpd.NE__OrderStatus__c = 'Active';
                                confToUpd.add(assetToUpd);
                                
                                for(NE__OrderItem__c assetItem : assetToUpd.NE__Order_Items__r)
                                {
                                    assetItem.TGS_Service_status__c = 'Deployed';
                                    confItemsToUpd.add(assetItem);
                                }
                                sendCis = true; 
                            }
                            
                            NE__Order__c ordToUpdate        =   mapOfConfigurations.get(updCase.Order__c);
                            ordToUpdate.NE__OrderStatus__c  =   'Completed';
                            //update ordToUpdate;
                            confToUpd.add(ordToUpdate); // JMF 21/10/2016 - No updates in FORs, please

                            Set<Id>confItemsAdded = new Set<Id>();
                            //Se crea un set con los elementos agregados a la lista para verificar que no se agreguen 2 veces
                            for(NE__OrderItem__c orderItem : confItemsToUpd){
                                confItemsAdded.add(orderItem.id);
                            }
                            for(NE__OrderItem__c orderItem : ordToUpdate.NE__Order_Items__r)
                            {
                                orderItem.NE__Service_Account__c = ordToUpdate.NE__ServAccId__c;
                                orderItem.NE__Billing_Account__c = ordToUpdate.NE__BillAccId__c;
                                if(!confItemsAdded.contains(orderItem.id)){//Solo se agrega si no se ha agregado ya a la lista
                                    confItemsToUpd.add(orderItem);    
                                }
                            }
                        }
                        
                        /** Case reopened **/
                        else if(oldCase.Status == 'Resolved' && updCase.Status != 'Resolved' && updCase.Status != 'Closed' && updCase.Status != 'Cancelled' && updCase.Status != 'Rejected' && updCase.Order__c != null && updCase.Asset__c != null)
                        {
                            NE__Order__c assetConf = mapOfConfigurations.get(updCase.Asset__c);

                            for(NE__OrderItem__c assetItem : assetConf.NE__Order_Items__r)
                            {
                                assetItem.TGS_Service_status__c = 'Disposed';
                                confItemsToUpd.add(assetItem);
                            }
                            sendCis = true; 
                        }
                        
                        /** Case Status updated to "Cancelled" or "Rejected" **/
                        else if((oldCase.Status != 'Cancelled' && oldCase.Status != 'Rejected') && (updCase.Status == 'Cancelled' || updCase.Status == 'Rejected'))
                        {
                            // Cancel the order (and its order items), so that the asset can be changed/disconnected again.
                            NE.OrderSummaryButtonExtension.cancelButton(updCase.Order__c);
                            
                            if(updCase.Asset__c != null && updCase.Status == 'Cancelled')
                            {
                                NE__Order__c    caseAssetConf           =       mapOfConfigurations.get(updCase.Asset__c);

                                // Registration Case
                                if(updCase.Type == 'New')
                                {
                                    for(NE__OrderItem__c assetItem : caseAssetConf.NE__Order_Items__r)
                                    {
                                        assetItem.TGS_Service_status__c = 'Disposed';
                                    }
                                    confItemsToUpd.addAll(caseAssetConf.NE__Order_Items__r);
                                    sendCis = true;
                                }
                                // Modification or Termination Case
                                else
                                {
                                    for(NE__OrderItem__c assetItem : caseAssetConf.NE__Order_Items__r)
                                    {
                                        assetItem.TGS_Service_status__c = 'Deployed';
                                    }
                                    confItemsToUpd.addAll(caseAssetConf.NE__Order_Items__r);
                                    sendCis = true;
                                    caseAssetConf.NE__OrderStatus__c = 'Active';
                                    confToUpd.add(caseAssetConf);
                                }
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        //JCT 06/10/2016 Added Integration error register to trace errors.
                        /*TGS_Error_Integrations__c error = new TGS_Error_Integrations__c();
                        error.TGS_Interface__c = 'RoD';
                        error.TGS_Operation__c = 'Asset Creation';
                        error.TGS_ErrorType__c = e.getTypeName();
                        error.TGS_RequestInfo__c = 'Msg: ' + e.getMessage() + '\nStackTrace: ' + e.getStackTraceString() + (e.getCause() != null ? '\nCaused By: ' + e.getCause().getMessage() + '\nStack: ' + e.getCause().getStackTraceString() : '');
                        insert error;*/
                        // JMF 21/10/2016 - Replaced internal log creation with call to TGS_ErrorIntegrations_Helper
                        TGS_ErrorIntegrations_Helper.createExceptionLog('SFDC', 'Asset Creation', e, (TGS_ErrorIntegrations_Helper.RequestInfo)pd);
                    }
                }

                System.debug('!!@@Tamaño --->' + map_conf_cas.size());
                if(!map_conf_cas.isEmpty()){
                    //GMN 12/09/2017 - Modificado para evitar errores cuando salte el job de TGS_CaseAutoClose
                    System.debug('!!@@ Entro Order2AssetWithQueuable.QueableHelper ---->' + map_conf_cas);
                    if(map_conf_cas.size() > 1){
                        ID jobID = System.enqueueJob(new Order2AssetWithQueuable.QueableHelper(map_conf_cas));
                    }
                    else{
                        List <String> keys = new List <String>();
                        
                        keys.addall(map_conf_cas.keySet());
        
                        String confId  = keys[0];
                        String caseId = map_conf_cas.get(confId);
                        //GMN 11/10/2017 - Se lanza en un @future si el contexto no es @future
                        if(!System.isFuture()){ /* START Álvaro López 15/02/2018 - Case Manager fix csuids*/

                            Boolean queueableProcess = false;
                            /* Álvaro López 23/02/2018 - Changed conditions for queueable process*/
                            if(TGS_CaseManagerController.isCaseManagerProcess){
                                queueableProcess = true;
                            }
                            else if(autoclose){
                                queueableProcess = true;
                            }
                            System.debug('IsQueueableProcess: ' + queueableProcess);
                            /* Álvaro López 15/02/2018 - AutoClose fix csuids*/
                            Order2AssetWithQueuable.order2Asset(confId,caseId,queueableProcess);
                            /* END Álvaro López 15/02/2018 - Case Manager fix csuids*/
                        }
                        else{
                            String assetId  =   Order2AssetWithQueuable.callOrder2AssetMethod(confId,caseId);
                            //Avoid uncatchable errors during test
                            if(Test.isRunningTest() == false){
                                //ID jobID = System.enqueueJob(new QueuableSetAssetAdditionalFields(assetId, caseId, confId));
                                Order2AssetWithQueuable.QueuableSetAssetAdditionalFields(assetId, caseId, confId);
                            }
                        }                        
                    }
                }
                //END GMN 12/09/2017
            }
            
            system.debug('[trigger.CallOrder2Asset] Configuration Items to update: '+confItemsToUpd);
            //JCT 06/10/2016 Surrounded IF block with try-catch block to get error trace on bad-data situations.
            try
            {
                if(confItemsToUpd.size() > 0 && !confItemsToUpd.isEmpty())
                {
                    TGS_CallRodWs.inFutureContextCI = true;
                    update confItemsToUpd;
                    TGS_CallRodWs.inFutureContextCI = false;
                    if (sendCis){
                        /* Sends Integration with in ROD */
                        Set<Id> confitemsIds = new Set<Id>();
                        for(NE__OrderItem__c ci: confItemsToUpd){
                            
                            confitemsIds.add(ci.Id);
                        }
                        TGS_CallRoDWS.invokeWebServiceOrderItemUpdate(confitemsIds, true);            
                    }
                }
                // JMF 21/10/2016 - Moved NE__Order__c update to separate IF block
                if(!confToUpd.isEmpty()) {
                    update confToUpd; //JCT 06/10/2016 Moved sentence inside IF configuration to avoid innecesary checking.
                }
            }
            catch(Exception e)
            {
                /*TGS_Error_Integrations__c error = new TGS_Error_Integrations__c();
                error.TGS_Interface__c = 'RoD';
                error.TGS_Operation__c = 'Updating RoD CIS';
                error.TGS_ErrorType__c = e.getTypeName();
                error.TGS_RequestInfo__c = 'Msg: ' + e.getMessage() + '\nStackTrace: ' + e.getStackTraceString() + (e.getCause() != null ? '\nCaused By: ' + e.getCause().getMessage() + '\nStack: ' + e.getCause().getStackTraceString() : '');
                insert error;*/
                // JMF 21/10/2016 - Replaced internal log creation with call to TGS_ErrorIntegrations_Helper
                TGS_ErrorIntegrations_Helper.createExceptionLog('RoD', 'Updating RoD CIS', e, (TGS_ErrorIntegrations_Helper.RequestInfo)pd);
            }
        }
    }   
}