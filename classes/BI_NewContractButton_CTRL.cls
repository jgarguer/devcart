/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián
    Company:       Accenture
    Description:   Controller of BI_NewContractButton
    History:
    
    <Date>            <Author>          <Description>
    07/03/2017        Gawron, Julián    Initial version
 --------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_NewContractButton_CTRL {

	public Opportunity record;
   
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public BI_NewContractButton_CTRL(ApexPages.StandardController stdController) {
    	record = (Opportunity)stdController.getRecord();
    	System.debug('BI_NewContractButton_CTRL redirect record' + stdController.getRecord());
    	System.debug('BI_NewContractButton_CTRL redirect record' + record );  
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Gawron, Julián
    Company:       Aborda.es
    Description:   Method that redirect the user to the contract with the correct recordType
    
    IN:            
    OUT:           
    
    History:
    
    <Date>            <Author>          <Description>
    07/03/2017        Gawron, Julián    Initial version
    16/03/2017        Gawron, Julián    Adding fix on query Adding try catch
    21/03/2017        Gawron, Julián    Adding data to link the Oppty
    31/03/2017        Gawron, Julián    Adding more data to link the Oppty
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public PageReference redirect(){
//  String url = "{!$Site.Prefix}/800/e?RecordType={!$Label.BI_O4_RT_Contratos}&{!$Label.BI_O4_Cliente_Contratos}={!Opportunity.Account}&{!$Label.BI_O4_Oportunidad_Contratos}={!Opportunity.Name})";

     try{   
       Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
       Map<String, Schema.SObjectField> fieldMap = schemaMap.get('Account').getDescribe().fields.getMap();
       
       Boolean ver_TGS_Es_MNC = fieldMap.containsKey('TGS_Es_MNC__c');
       Boolean ver_BI_O4_Gesti_n_TGS = fieldMap.containsKey('BI_O4_Gesti_n_TGS__c');
       Boolean ver_BI_O4_Gesti_n_TNA = fieldMap.containsKey('BI_O4_Gesti_n_TNA__c');
       Boolean full_access = ver_TGS_Es_MNC && ver_BI_O4_Gesti_n_TGS && ver_BI_O4_Gesti_n_TNA;
       System.debug('BI_NewContractButton_CTRL');
       Opportunity theOppt;
       if(full_access){
            theOppt = [Select
                   Name,
                   BI_Duracion_del_contrato_Meses__c,
                   AccountId,
                   Account.Name,
                   Account.TGS_Es_MNC__c, 
                   Account.BI_O4_Gesti_n_TGS__c, 
                   Account.BI_O4_Gesti_n_TNA__c from Opportunity where id = :record.Id];
       }else{
             theOppt = [Select
                   Name,
                   AccountId,
                   Account.Name from Opportunity where id = :record.Id];
       }



       String toReturn = URL.getSalesforceBaseUrl().toExternalForm();
       
       Map<String, String> mapDynamicIds = BI_DynamicFieldId.getDynamicField('Contract', new Set<String>{'AccountId'});
       String AccountId_label =  mapDynamicIds.get('AccountId');
       if(AccountId_label == null)
           AccountId_label = 'ctrc7';
       

       Map<String, String> mapDynamicIds1 = BI_DynamicFieldId.getDynamicField('Contract', new Set<String>{'BI_Oportunidad__c'});
       String OptiId_label =  mapDynamicIds1.get('BI_Oportunidad__c');
       if(OptiId_label == null)
           OptiId_label = 'CF00Nw0000007jJ4G';
       
       Map<String, String> mapDynamicIds2 = BI_DynamicFieldId.getDynamicField('Contract', new Set<String>{'ContractTerm'});
       String duracion_label =  mapDynamicIds2.get('ContractTerm');
       if(duracion_label == null)
           duracion_label = 'ctrc40';


       RecordType rt;
       if(full_access &&(theOppt.Account.TGS_Es_MNC__c || theOppt.Account.BI_O4_Gesti_n_TGS__c || theOppt.Account.BI_O4_Gesti_n_TNA__c )){
            //Es Ola4 u otra cosa.
            rt = [Select Id from RecordType where DeveloperName = 'BI_O4_Contratos_BIEN'];
        }else{
            //Es de BI_EN
            rt = [Select Id from RecordType where DeveloperName = 'BI_Contrato'];
        }
       
       toReturn = toReturn + '/800/e?RecordType=' + rt.Id + '&'+ AccountId_label + '='+ theOppt.Account.Name + '&' + AccountId_label + '_lkid=' + theOppt.AccountId +
       '&'+ OptiId_label + '='+ theOppt.Name + '&' + OptiId_label + '_lkid=' + record.Id + '&'+ duracion_label + '='+ theOppt.BI_Duracion_del_contrato_Meses__c +
       '&'+ 'cancelURL=' + record.Id;

     
    PageReference page = new PageReference(toReturn);

        page.setRedirect(true);
        return page;
       

        }catch(Exception exc) {
            BI_LogHelper.generate_BILog('BI_NewContractButton_CTRL.redirect', 'BI_EN', exc, 'Trigger');
            return null;
        }
    }
}