/*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:        Fernando Arteaga
 Company:       New Energy Aborda
 Description:   Redirects to the new Opportunity Qualify page through record type selection, prepopulating some fields. 
 Test Class:    BI_O4_NewOpportunityQualify_TEST
 
 History:
  
 <Date>              <Author>                   <Change Description>
 28/07/2016          Fernando Arteaga           Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public with sharing class BI_O4_NewOpportunityQualify
{
    public Opportunity opp {get; set;}
    public BI_O4_Opportunity_Qualify__c qTool {get; set;}
    
    // Constructor
    public BI_O4_NewOpportunityQualify(ApexPages.StandardController controller)
    {
    	opp = (Opportunity) controller.getRecord();
    	List<BI_O4_Opportunity_Qualify__c> listQTool = [SELECT Id
    													FROM BI_O4_Opportunity_Qualify__c
    													WHERE BI_O4_Opportunity__c = :opp.Id
    													ORDER BY CreatedDate DESC
    													LIMIT 1];
    	if (!listQTool.isEmpty())
    		qTool = listQTool[0];
    }	
    
    // Action method called from visualforce page
    public PageReference newOpportunityQualify()
    {
        // Retrieve Custom Object Id
        String customObjectId = BI_O4_Utils.getCustomObjectId('Calificación de la Oportunidad');
        // Print the Id retrieved
        System.debug('BI_O4_Opportunity_Qualify__c Object Id ===>' + customObjectId);
       
		String url;
    	Map<String, String> mapDynamicIds = BI_DynamicFieldId.getDynamicField('BI_O4_Opportunity_Qualify__c', new Set<String>{'BI_O4_Opportunity__c'});

    	if (qTool == null) {
    		url = '/setup/ui/recordtypeselect.jsp?ent=' + customObjectId + '&save_new_url=' + Schema.sObjectType.BI_O4_Opportunity_Qualify__c.getKeyPrefix() + '/e?cancelURL=/' + opp.Id + '&' + mapDynamicIds.get('BI_O4_Opportunity__c') + '=' + opp.Name + '&' + mapDynamicIds.get('BI_O4_Opportunity__c') + '_lkid=' + opp.Id;
    	}
        else {
    		url = '/' + qTool.Id + '/e?retURL=%2F' + qTool.Id + '&cancelURL=%2F' + opp.Id;
        }
        
    	System.debug(LoggingLevel.FINEST, url);
    	
        PageReference retPage = new PageReference(url);
    	retPage.setRedirect(true);
    	return retPage;
    }

}