/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestPublishAllItems {

    static testMethod void myUnitTest() {

        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
        NE__Map__c maps = new NE__Map__c();
        maps.NE__Map_Name__c = 'ComplexProduct';
        insert maps;
        
        NE__Catalog_Header__c catH = new NE__Catalog_Header__c();
        catH.name = 'Test311014';
        catH.NE__Name__c = 'Test311014';
        insert catH;
        
        NE__Catalog__c cat = new NE__Catalog__c();
        cat.Name = 'Test311014';
        cat.NE__Catalog_Header__c = catH.Id;
        insert cat;
        
        NE__Product__c prod = new NE__Product__c();
        prod.name = 'Prod3110';
        insert prod;
        
        NE__Catalog_Item__c catItprod= new NE__Catalog_Item__c();
        catItprod.NE__type__c = 'root';
        catItprod.NE__Catalog_Id__c = cat.Id;
        catItprod.NE__ProductId__c = Prod.Id;
        catItprod.NE__Type__c = 'Product';
        insert catItprod;
        
        NE__Catalog_Item__c catItroot = new NE__Catalog_Item__c();
        catItroot.NE__type__c = 'root';
        catItroot.NE__Catalog_Id__c = cat.Id;
        catItroot.NE__Parent_Catalog_Item__c = catItprod.Id;        
        insert catItroot;
        
        Attachment att = new Attachment();
        att.Name = 'XML_Complex_Product.xml';
        att.Body = Blob.valueOf('body');
        att.ParentId = catItroot.Id;
        insert att;
        
        ApexPages.currentPage().getParameters().put('catId',cat.Id);
        PublishAllItemsController controller = new PublishAllItemsController();
        system.debug('*controller ' + controller);
        
        controller.mapp = maps;
        

        //ApexPages.currentPage().getParameters().put('catId', cat.Id);
        //controller.catId =  cat.Id;

        //controller.catalogItems = new List<Catalog_Item__c>();        
        //controller.totalItems = 10;
        controller.assignAttachments();
        controller.insAttach();
        //controller.callXmlMapper();
    }
/* Commented to avoid problems deploying in Spring15
    @testSetup static void userOrgCS() {
        TGS_User_Org__c userTGS = new TGS_User_Org__c();
        userTGS.TGS_Is_BI_EN__c = true;
        insert userTGS;
    
    }
*/ 

}