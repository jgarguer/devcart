/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:        Pedro Párraga
    Company:       Salesforce.com
    Description:   Test class to manage the coverage code for ADQ_BajaSitiosControllerNew class
    
    History: 
    
    <Date>                  <Author>                <Change Description>
    08/02/2017              Pedro Párraga            Initial Creation
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class Milestone1_General_Utility_Test {
	
	static testMethod void testTruncateString()
    {
        String example = 'NEW DESCRIPTION 1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZ THIS IS LONG DESCRIPTION GREATER THAN 80 CHARACTER LIMIT FOR NAME TEST TRUNCATING';
        String nameString = Milestone1_General_Utility.processTaskName(example);
        Boolean isAdmin = Milestone1_General_Utility.isSysAdmin();
        system.AssertEquals('NEW DESCRIPTION 1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZ THIS IS LONG...',nameString);
    } 
	
}